﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class F06
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oController As New F06Controller
    Private oCustomclass As New Parameter.F06
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Protected WithEvents GridNaviF06SLIK As ucGridNav
    Sub clean()
        txtbulandata.Text = ""
        txtid.Text = ""
        txtFlag.Text = ""
        txtNoRekFasilitas.Text = ""
        txtCIF.Text = ""
        txtSumberDana.Text = ""
        txtSukuBunga.Text = ""
        txtNominal.Text = ""
        txtNilaiMataUang.Text = ""
        txttunggakan.Text = ""
        txtjmlharitunggakan.Text = ""
        txtKeterangan.Text = ""
        txtKodeCabang.Text = ""
        txtOperasiData.Text = ""
        txtTglMulai.Text = ""
        txtTglJatuhTempo.Text = "'"
        txttglmacet.Text = ""
		txtTglKondisi.Text = ""

		txtSearch.Text = ""
	End Sub

#Region "Page Load"
    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviF06SLIK.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
		If Not IsPostBack Then
			Me.FormID = "F06"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If

		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub
#End Region
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviF06SLIK.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.F06

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetF06(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If

        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        FillCombo()
        FillCbo(cboKdJenisFasilitas, "dbo.TblJenisFasilitasLain")
        FillCbo1(cboKodeValuta, "dbo.MitraRefValuta")
        FillCbo2(cboKodeKualitas, "dbo.TblKodeKualitas")
        FillCbo3(cboKodeSebabMacet, "dbo.TblSebabMacet")
        FillCbo4(cboKodeKondisi, "dbo.TblKodeKondisi")

        If (isFrNav = False) Then
            GridNaviF06SLIK.Initialize(recordCount, pageSize)
        End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub

#Region "DropDown"
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable
        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.F06

        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.F06
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "JenisFasilitasLain"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo1(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.F06
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo1(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "KeteranganSandi"
        cboName.DataValueField = "Sandi"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo2(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.F06
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo2(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Kualitas"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo3(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.F06
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo3(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "SebabMacet"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo4(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.F06
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo4(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Kondisi"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
#End Region


    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#Region "GRID"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.F06
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "F06", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetF06Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboKdJenisFasilitas.SelectedIndex = cboKdJenisFasilitas.Items.IndexOf(cboKdJenisFasilitas.Items.FindByValue(oRow("KodeJenisFasilitasLain").ToString.Trim))
                    cboKodeValuta.SelectedIndex = cboKodeValuta.Items.IndexOf(cboKodeValuta.Items.FindByValue(oRow("KodeValuta").ToString.Trim))
                    cboKodeKualitas.SelectedIndex = cboKodeKualitas.Items.IndexOf(cboKodeKualitas.Items.FindByValue(oRow("KodeKualitas").ToString.Trim))
                    cboKodeSebabMacet.SelectedIndex = cboKodeSebabMacet.Items.IndexOf(cboKodeSebabMacet.Items.FindByValue(oRow("KodeSebabMacet").ToString.Trim))
                    cboKodeKondisi.SelectedIndex = cboKodeKondisi.Items.IndexOf(cboKodeKondisi.Items.FindByValue(oRow("KodeKondisi").ToString.Trim))
                    txtTglMulai.Text = Format(oRow("TanggalMulai"), "yyyy/MM/dd").ToString
                    txtTglJatuhTempo.Text = Format(oRow("TanggaljatuhTempo"), "yyyy/MM/dd").ToString
                    txttglmacet.Text = Format(oRow("TanggalMacet"), "yyyy/MM/dd").ToString
                    txtTglKondisi.Text = Format(oRow("TanggalKondisi"), "yyyy/MM/dd").ToString
                    If txttglmacet.Text = "1900/01/01" Or txtTglKondisi.Text = "1900/01/01" Then
                        txttglmacet.Text = ""
                        txtTglKondisi.Text = ""
                    End If
                End If

                txtbulandata.Text = oParameter.ListData.Rows(0)("BulanData")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtFlag.Text = oParameter.ListData.Rows(0)("FlagDetail")
                txtNoRekFasilitas.Text = oParameter.ListData.Rows(0)("NoRekeningFasilitas")
                txtCIF.Text = oParameter.ListData.Rows(0)("CIF")
                txtSumberDana.Text = oParameter.ListData.Rows(0)("SumberDana")
                txtSukuBunga.Text = oParameter.ListData.Rows(0)("SukuBungaatauImbalan")
                txtNominal.Text = oParameter.ListData.Rows(0)("Nominal")
                txtNilaiMataUang.Text = oParameter.ListData.Rows(0)("NilaiDalamMataUangAsal")
                txttunggakan.Text = oParameter.ListData.Rows(0)("Tunggakan")
                txtjmlharitunggakan.Text = oParameter.ListData.Rows(0)("JumlahHariTunggakan")
                txtKeterangan.Text = oParameter.ListData.Rows(0)("Keterangan")
                txtKodeCabang.Text = oParameter.ListData.Rows(0)("KodeKantorCabang")
                txtOperasiData.Text = oParameter.ListData.Rows(0)("OperasiData")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "F06", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.F06
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.GetF06Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.F06
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BulanData = txtbulandata.Text
                .NoRekeningFasilitas = txtNoRekFasilitas.Text
                .CIF = txtCIF.Text
                .KodeJenisFasilitasLain = cboKdJenisFasilitas.SelectedValue.Trim()
                .SumberDana = txtSumberDana.Text
                .TanggalMulai = txtTglMulai.Text
                .TanggalJatuhTempo = txtTglJatuhTempo.Text
                .SukuBungaatauImbalan = txtSukuBunga.Text
                .KodeValuta = cboKodeValuta.SelectedValue.Trim()
                .Nominal = txtNominal.Text
                .NilaiDalamMataUangAsal = txtNilaiMataUang.Text
                .KodeKualitas = cboKodeKualitas.SelectedValue.Trim()
                .TanggalMacet = txttglmacet.Text
                .KodeSebabMacet = cboKodeSebabMacet.SelectedValue.Trim()
                .Tunggakan = txttunggakan.Text
                .JumlahHariTunggakan = txtjmlharitunggakan.Text
                .KodeKondisi = cboKodeKondisi.SelectedValue.Trim()
                .TanggalKondisi = txtTglKondisi.Text
                .Keterangan = txtKeterangan.Text
                .KodeKantorCabang = txtKodeCabang.Text
                .OperasiData = "C"

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetF06Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter

                .ID = txtid.Text
                .BulanData = txtbulandata.Text
                .NoRekeningFasilitas = txtNoRekFasilitas.Text
                .CIF = txtCIF.Text
                .KodeJenisFasilitasLain = cboKdJenisFasilitas.SelectedValue.Trim()
                .SumberDana = txtSumberDana.Text
                .TanggalMulai = txtTglMulai.Text
                .TanggalJatuhTempo = txtTglJatuhTempo.Text
                .SukuBungaatauImbalan = txtSukuBunga.Text
                .KodeValuta = cboKodeValuta.SelectedValue.Trim()
                .Nominal = txtNominal.Text
                .NilaiDalamMataUangAsal = txtNilaiMataUang.Text
                .KodeKualitas = cboKodeKualitas.SelectedValue.Trim()
                .TanggalMacet = txttglmacet.Text
                .KodeSebabMacet = cboKodeSebabMacet.SelectedValue.Trim()
                .Tunggakan = txttunggakan.Text
                .JumlahHariTunggakan = txtjmlharitunggakan.Text
                .KodeKondisi = cboKodeKondisi.SelectedValue.Trim()
                .TanggalKondisi = txtTglKondisi.Text
                .Keterangan = txtKeterangan.Text
                .KodeKantorCabang = txtKodeCabang.Text
                .OperasiData = "U"

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetF06Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region
#Region "Generate Data"
    Public Function GetCopy(ByVal customclass As Parameter.F06) As Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKF06Generate", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Generate Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.F06
        Dim dtViewData As New DataTable
        With oEntities
            .BulanData = txtcopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "F06", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
			txtbulandata.Enabled = True
			txtCIF.ReadOnly = False
			txtKodeCabang.ReadOnly = False

			Me.Process = "ADD"
            clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
        Dim oParameter As New Maxiloan.Parameter.F06

        Dim ResClient As New StringBuilder
        Using sw As New StringWriter(ResClient)

            Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "F06" & "|" & Rc & "|" & Rc)
            sw.WriteLine(Header)
            For Each drow As DataRow In dtdata.Rows
                Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
                sw.WriteLine(lineoftext)
            Next

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
            Response.Write(ResClient.ToString())

            Using MyMemoryStream As New MemoryStream()
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using

        End Using
    End Sub

    Public Function GetToTXT(ByVal customclass As Parameter.F06) As Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData
            params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
            params(1).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKF06Txt", params).Tables(0)
            customclass.TotalRecord = CType(params(1).Value, Int64)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
        Dim oEntities As New Parameter.F06
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BulanData = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToTXT(oEntities)
            dtViewData = oEntities.ListData
            Rc = oEntities.TotalRecord
            ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BulanData.Substring(0, oEntities.BulanData.Length - 2) & "." & oEntities.BulanData.Substring(4) & "." & "F06.01")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub
    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region

#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region
End Class