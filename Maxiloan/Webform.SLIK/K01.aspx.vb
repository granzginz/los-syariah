﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class K01
	Inherits Maxiloan.Webform.WebBased
	Protected WithEvents GridNav As ucGridNav
	Protected WithEvents oKodeKota As ucKodeKota
#Region "Constanta"
	Private oController As New K01Controller
	Private oCustomclass As New Parameter.K01
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1
#End Region
#Region "Property"
	Private Property Process() As String
		Get
			Return CType(ViewState("Process"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("Process") = Value
		End Set
	End Property
#End Region
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		AddHandler GridNav.PageChanged, AddressOf PageNavigation
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.FormID = "SLIKK01"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")

			End If
		End If
		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub
	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
		Dim dtEntity As DataTable = Nothing
		Dim oParameter As New Maxiloan.Parameter.K01

		oParameter.strConnection = GetConnectionString()
		oParameter.WhereCond = cmdWhere
		oParameter.CurrentPage = currentPage
		oParameter.PageSize = pageSize
		oParameter.SortBy = SortBy
		oParameter = oController.GetK01(oParameter)

		If Not oParameter Is Nothing Then
			dtEntity = oParameter.ListData
			recordCount = oParameter.TotalRecord
		Else
			recordCount = 0
		End If

		dtgList.DataSource = dtEntity.DefaultView
		dtgList.CurrentPageIndex = 0
		dtgList.DataBind()

		FillCombo()

		If (isFrNav = False) Then
			GridNav.Initialize(recordCount, pageSize)
		End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub

	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub
	Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
		Dim oAssetData As New Parameter.K01

		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Table = Table
		oAssetData = oController.GetCboBulandataSIPP(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "ID"
		cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
	End Sub
	Sub FillCboKodeKota(ByVal cboName As DropDownList, ByVal Table As String)
		Dim oAssetData As New Parameter.K01
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Table = Table
		oAssetData = oController.GetCbo(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "KeteranganSandi"
		cboName.DataValueField = "Sandi"
		cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = ""
	End Sub
	Sub FillCboLembagapemeringkat(ByVal cboName As DropDownList, ByVal Table As String)
		Dim oAssetData As New Parameter.K01
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Table = Table
		oAssetData = oController.GetCbo(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "Lembaga"
		cboName.DataValueField = "Kode"
		cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = ""
	End Sub
	Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
		currentPage = e.CurrentPage
		DoBind(Me.SearchBy, Me.SortBy, True)
		GridNav.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
	End Sub
	Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
		Dim imbDelete As ImageButton
		If e.Item.ItemIndex >= 0 Then
			imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
			imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
		End If
	End Sub

	Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
		Dim oParameter As New Parameter.K01
		Dim dtEntity As New DataTable
		Dim err As String

		Try
			If e.CommandName = "Edit" Then
				If CheckFeature(Me.Loginid, "SLIKK01", "ALLOW", "MAXILOAN") Then
					If SessionInvalid() Then
						Exit Sub
					End If
				End If
				Me.Process = "EDIT"
				pnlAdd.Visible = True
				pnlList.Visible = False
				txtbulandata.Enabled = False

				oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
				oParameter.strConnection = GetConnectionString()
				oParameter = oController.GetK01Edit(oParameter)

				If Not oParameter Is Nothing Then
					dtEntity = oParameter.ListData
				End If

				ID.Text = oParameter.ListData.Rows(0)("ID")
				txtbulandata.Text = oParameter.ListData.Rows(0)("BulanData")
				FlagDetail.Text = oParameter.ListData.Rows(0)("FlagDetail").ToString.Trim
				KodeKantorCabang.Text = oParameter.ListData.Rows(0)("KodeKantorCabang").ToString.Trim
				txtOperasiData.Text = oParameter.ListData.Rows(0)("OperasiData").ToString.Trim

				CustomerId.Text = oParameter.ListData.Rows(0)("CustomerId").ToString.Trim
				TglLaporanKeuangan.Text = Format(oParameter.ListData.Rows(0)("TglLaporanKeuangan"), "yyyy/MM/dd").ToString
				AssetIDR.Text = oParameter.ListData.Rows(0)("AssetIDR").ToString.Trim
				AssetLancar.Text = oParameter.ListData.Rows(0)("AssetLancar").ToString.Trim
				KasSetaraKasAsetLancar.Text = oParameter.ListData.Rows(0)("KasSetaraKasAsetLancar").ToString.Trim
				PiutangUsahAsetLancar.Text = oParameter.ListData.Rows(0)("PiutangUsahAsetLancar").ToString.Trim
				InvestasiLainnyaAsetLancar.Text = oParameter.ListData.Rows(0)("InvestasiLainnyaAsetLancar").ToString.Trim
				AsetLancarLainnya.Text = oParameter.ListData.Rows(0)("AsetLancarLainnya").ToString.Trim
				AsetTidakLancar.Text = oParameter.ListData.Rows(0)("AsetTidakLancar").ToString.Trim
				PiutangUsahaAsetTidakLancar.Text = oParameter.ListData.Rows(0)("PiutangUsahaAsetTidakLancar").ToString.Trim
				InvestasiLainnyaAsetTidakLancar.Text = oParameter.ListData.Rows(0)("InvestasiLainnyaAsetTidakLancar").ToString.Trim
				AsetTidakLancarLainnya.Text = oParameter.ListData.Rows(0)("AsetTidakLancarLainnya").ToString.Trim
				Liabilitas.Text = oParameter.ListData.Rows(0)("Liabilitas").ToString.Trim
				LiabilitasJangkaPendek.Text = oParameter.ListData.Rows(0)("LiabilitasJangkaPendek").ToString.Trim
				PinjamanJangkaPendek.Text = oParameter.ListData.Rows(0)("PinjamanJangkaPendek").ToString.Trim
				UtangUsahaJangkaPendek.Text = oParameter.ListData.Rows(0)("UtangUsahaJangkaPendek").ToString.Trim
				LiabilitasJangkaPendekLainnya.Text = oParameter.ListData.Rows(0)("LiabilitasJangkaPendekLainnya").ToString.Trim
				LiabilitasJangkaPanjang.Text = oParameter.ListData.Rows(0)("LiabilitasJangkaPanjang").ToString.Trim
				PinjamanJangkaPanjang.Text = oParameter.ListData.Rows(0)("PinjamanJangkaPanjang").ToString.Trim
				UtangUsahaJangkaPanjang.Text = oParameter.ListData.Rows(0)("UtangUsahaJangkaPanjang").ToString.Trim
				LiabilitasJangkaPanjangLainnya.Text = oParameter.ListData.Rows(0)("LiabilitasJangkaPanjangLainnya").ToString.Trim
				Ekuitas.Text = oParameter.ListData.Rows(0)("Ekuitas").ToString.Trim
				PendapatanUsahaOpr.Text = oParameter.ListData.Rows(0)("PendapatanUsahaOpr").ToString.Trim
				BebanPokokPendapatanOpr.Text = oParameter.ListData.Rows(0)("BebanPokokPendapatanOpr").ToString.Trim
				LabaRugiBruto.Text = oParameter.ListData.Rows(0)("LabaRugiBruto").ToString.Trim
				PLLNonOpr.Text = oParameter.ListData.Rows(0)("PLLNonOpr").ToString.Trim
				BebanLainLainNonOpr.Text = oParameter.ListData.Rows(0)("BebanLainLainNonOpr").ToString.Trim
				LabaRugiSebelumPajak.Text = oParameter.ListData.Rows(0)("LabaRugiSebelumPajak").ToString.Trim
				LabaRugiTahunBerjalan.Text = oParameter.ListData.Rows(0)("LabaRugiTahunBerjalan").ToString.Trim




			ElseIf e.CommandName = "DEL" Then
				If CheckFeature(Me.Loginid, "SLIKK01", "ALLOW", "MAXILOAN") Then
					If SessionInvalid() Then
						Exit Sub
					End If
				End If
				Dim customClass As New Parameter.K01
				With customClass
					.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
					.strConnection = GetConnectionString()
				End With

				err = oController.GetK01Delete(customClass)
				If err <> "" Then
					ShowMessage(lblMessage, err, True)
				Else
					ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
				End If
				DoBind(Me.SearchBy, Me.SortBy)
			End If
		Catch ex As Exception
			ShowMessage(lblMessage, ex.Message, True)
		End Try
	End Sub
#Region "Save"
	Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
		Dim oParameter As New Parameter.K01
		Dim errMessage As String = ""

		If Me.Process = "ADD" Then
			With oParameter

				.BulanData = txtbulandata.Text
				.FlagDetail = "D"

				.CustomerId = customerid.Text.Trim
				.TglLaporanKeuangan = TglLaporanKeuangan.Text.Trim
				.AssetIDR = AssetIDR.Text.Trim
				.AssetLancar = AssetLancar.Text.Trim
				.KasSetaraKasAsetLancar = KasSetaraKasAsetLancar.Text.Trim
				.PiutangUsahAsetLancar = PiutangUsahAsetLancar.Text.Trim
				.InvestasiLainnyaAsetLancar = InvestasiLainnyaAsetLancar.Text.Trim
				.AsetLancarLainnya = AsetLancarLainnya.Text.Trim
				.AsetTidakLancar = AsetTidakLancar.Text.Trim
				.PiutangUsahaAsetTidakLancar = PiutangUsahaAsetTidakLancar.Text.Trim
				.InvestasiLainnyaAsetTidakLancar = InvestasiLainnyaAsetTidakLancar.Text.Trim
				.AsetTidakLancarLainnya = AsetTidakLancarLainnya.Text.Trim
				.Liabilitas = Liabilitas.Text.Trim
				.LiabilitasJangkaPendek = LiabilitasJangkaPendek.Text.Trim
				.PinjamanJangkaPendek = PinjamanJangkaPendek.Text.Trim
				.UtangUsahaJangkaPendek = UtangUsahaJangkaPendek.Text.Trim
				.LiabilitasJangkaPendekLainnya = LiabilitasJangkaPendekLainnya.Text.Trim
				.LiabilitasJangkaPanjang = LiabilitasJangkaPanjang.Text.Trim
				.PinjamanJangkaPanjang = PinjamanJangkaPanjang.Text.Trim
				.UtangUsahaJangkaPanjang = UtangUsahaJangkaPanjang.Text.Trim
				.LiabilitasJangkaPanjangLainnya = LiabilitasJangkaPanjangLainnya.Text.Trim
				.Ekuitas = Ekuitas.Text.Trim
				.PendapatanUsahaOpr = PendapatanUsahaOpr.Text.Trim
				.BebanPokokPendapatanOpr = BebanPokokPendapatanOpr.Text.Trim
				.LabaRugiBruto = LabaRugiBruto.Text.Trim
				.PLLNonOpr = PLLNonOpr.Text.Trim
				.BebanLainLainNonOpr = BebanLainLainNonOpr.Text.Trim
				.LabaRugiSebelumPajak = LabaRugiSebelumPajak.Text.Trim
				.LabaRugiTahunBerjalan = LabaRugiTahunBerjalan.Text.Trim

				.KodeKantorCabang = KodeKantorCabang.Text
				.OperasiData = "C"
				.strConnection = GetConnectionString()
			End With
			oParameter = oController.GetK01Add(oParameter)
		ElseIf Me.Process = "EDIT" Then
			With oParameter

				.ID = ID.Text
				.BulanData = txtbulandata.Text
				.FlagDetail = "D"

				.CustomerId = customerid.Text.Trim
				.TglLaporanKeuangan = TglLaporanKeuangan.Text.Trim
				.AssetIDR = AssetIDR.Text.Trim
				.AssetLancar = AssetLancar.Text.Trim
				.KasSetaraKasAsetLancar = KasSetaraKasAsetLancar.Text.Trim
				.PiutangUsahAsetLancar = PiutangUsahAsetLancar.Text.Trim
				.InvestasiLainnyaAsetLancar = InvestasiLainnyaAsetLancar.Text.Trim
				.AsetLancarLainnya = AsetLancarLainnya.Text.Trim
				.AsetTidakLancar = AsetTidakLancar.Text.Trim
				.PiutangUsahaAsetTidakLancar = PiutangUsahaAsetTidakLancar.Text.Trim
				.InvestasiLainnyaAsetTidakLancar = InvestasiLainnyaAsetTidakLancar.Text.Trim
				.AsetTidakLancarLainnya = AsetTidakLancarLainnya.Text.Trim
				.Liabilitas = Liabilitas.Text.Trim
				.LiabilitasJangkaPendek = LiabilitasJangkaPendek.Text.Trim
				.PinjamanJangkaPendek = PinjamanJangkaPendek.Text.Trim
				.UtangUsahaJangkaPendek = UtangUsahaJangkaPendek.Text.Trim
				.LiabilitasJangkaPendekLainnya = LiabilitasJangkaPendekLainnya.Text.Trim
				.LiabilitasJangkaPanjang = LiabilitasJangkaPanjang.Text.Trim
				.PinjamanJangkaPanjang = PinjamanJangkaPanjang.Text.Trim
				.UtangUsahaJangkaPanjang = UtangUsahaJangkaPanjang.Text.Trim
				.LiabilitasJangkaPanjangLainnya = LiabilitasJangkaPanjangLainnya.Text.Trim
				.Ekuitas = Ekuitas.Text.Trim
				.PendapatanUsahaOpr = PendapatanUsahaOpr.Text.Trim
				.BebanPokokPendapatanOpr = BebanPokokPendapatanOpr.Text.Trim
				.LabaRugiBruto = LabaRugiBruto.Text.Trim
				.PLLNonOpr = PLLNonOpr.Text.Trim
				.BebanLainLainNonOpr = BebanLainLainNonOpr.Text.Trim
				.LabaRugiSebelumPajak = LabaRugiSebelumPajak.Text.Trim
				.LabaRugiTahunBerjalan = LabaRugiTahunBerjalan.Text.Trim

				.KodeKantorCabang = KodeKantorCabang.Text
				.OperasiData = "U"
				.strConnection = GetConnectionString()
			End With
			oParameter = oController.GetK01Save(oParameter)
		End If

		Me.SearchBy = ""
		Me.SortBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		pnlAdd.Visible = False
		pnlList.Visible = True

		If Me.Process = "ADD" Then
			ShowMessage(lblMessage, "Simpan data berhasil.....", False)
		Else
			ShowMessage(lblMessage, "Updated data berhasil .....", False)
		End If
	End Sub
#End Region
#Region "HEADER ONLY"
	Public Sub ExportTableDataHeader(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
		Dim oParameter As New Maxiloan.Parameter.K01

		Dim ResClient As New StringBuilder
		Using sw As New StringWriter(ResClient)

			Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "K01" & "|" & Rc & "|" & Rc)
			sw.WriteLine(Header)

			Response.Clear()
			Response.Buffer = True
			Response.Charset = ""
			Response.ContentType = "text/plain"
			Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
			Response.Write(ResClient.ToString())

			Using MyMemoryStream As New MemoryStream()
				MyMemoryStream.WriteTo(Response.OutputStream)
				Response.Flush()
				Response.End()
			End Using

		End Using
	End Sub

	Private Sub BtnCetakHeader_Click(sender As Object, e As System.EventArgs) Handles btncetakheader.Click
		Dim oEntities As New Parameter.K01
		Dim dtViewData As New DataTable

		If cbobulandata.Text <> "" Then
			With oEntities
				.BulanData = cbobulandata.Text
				.strConnection = GetConnectionString()
			End With

			Dim dateString, format As String
			Dim result As Date
			Dim Tanggal As String
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			Tanggal = result.AddMonths(1).AddDays(-1).ToString("yyyyMMdd")

			ExportTableDataHeader(dtViewData, 0, "0201.251230." & Tanggal.Substring(0, 4) & "." & Tanggal.Substring(4, 2) & "." & "K01.01")
		Else
			ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
		End If
	End Sub
#End Region
#Region "Add"
	Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
		If CheckFeature(Me.Loginid, "SLIKK01", "ALLOW", "MAXILOAN") Then
			pnlAdd.Visible = True
			pnlList.Visible = False
			txtbulandata.Enabled = True

			customerid.ReadOnly = False
			KodeKantorCabang.ReadOnly = False

			Me.Process = "ADD"
			clean()
			'DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region
#Region "Generate Data"
	Public Function GetCopy(ByVal customclass As Parameter.K01) As Parameter.K01
		Dim params() As SqlParameter = New SqlParameter(0) {}
		Try
			params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
			params(0).Value = customclass.BulanData

			customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKK01Generate", params).Tables(0)
			Return customclass
		Catch exp As Exception
		End Try
		ShowMessage(lblMessage, "Generate Data Berhasil......", False)
	End Function

	Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
		Dim oEntities As New Parameter.K01
		Dim dtViewData As New DataTable
		With oEntities
			.BulanData = txtcopybulandata.Text
			.strConnection = GetConnectionString()
		End With
		GetCopy(oEntities)
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
#End Region
#Region "EXPORT"
	Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
		Dim oParameter As New Maxiloan.Parameter.K01

		Dim ResClient As New StringBuilder
		Using sw As New StringWriter(ResClient)

			Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "K01" & "|" & Rc & "|" & Rc)
			sw.WriteLine(Header)
			For Each drow As DataRow In dtdata.Rows
				Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
				sw.WriteLine(lineoftext)
			Next

			Response.Clear()
			Response.Buffer = True
			Response.Charset = ""
			Response.ContentType = "text/plain"
			Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
			Response.Write(ResClient.ToString())

			Using MyMemoryStream As New MemoryStream()
				MyMemoryStream.WriteTo(Response.OutputStream)
				Response.Flush()
				Response.End()
			End Using

		End Using
	End Sub

	Public Function GetToTXT(ByVal customclass As Parameter.K01) As Parameter.K01
		Dim params() As SqlParameter = New SqlParameter(1) {}
		Try
			params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
			params(0).Value = customclass.BulanData
			params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
			params(1).Direction = ParameterDirection.Output

			customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKK01Txt", params).Tables(0)
			customclass.TotalRecord = CType(params(1).Value, Int64)
			Return customclass
		Catch exp As Exception
		End Try
		Return customclass
	End Function

	Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
		Dim oEntities As New Parameter.K01
		Dim dtViewData As New DataTable

		If cbobulandata.Text <> "" Then
			With oEntities
				.BulanData = cbobulandata.Text
				.strConnection = GetConnectionString()
			End With
			oEntities = GetToTXT(oEntities)
			dtViewData = oEntities.ListData
			Rc = oEntities.TotalRecord
			ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BulanData.Substring(0, oEntities.BulanData.Length - 2) & "." & oEntities.BulanData.Substring(4) & "." & "K01.01")
		Else
			ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
		End If
	End Sub

	Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		'clean()
	End Sub
#End Region
	Sub clean()
		txtSearch.Text = ""
	End Sub
#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		'clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region

#Region "Sort"
	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
#End Region

#Region "Cancel"
	Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
		pnlList.Visible = True
		pnlAdd.Visible = False
		Me.SearchBy = ""
		Me.SortBy = ""

		clean()
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
#End Region
End Class