﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class D02
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New D02Controller
    Private oCustomclass As New Parameter.D02
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    'Protected WithEvents oLookUpZipCode As ucLookupZipCode
    Protected WithEvents oGolonganDebitur As UcGolonganDebitur
    Protected WithEvents oSektorEkonomi As ucSektorEkonomi
    Protected WithEvents oKodeKota As ucKodeKota

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Protected WithEvents GridNaviD02SLIK As ucGridNav
    Sub clean()
        txtbulandata.Text = ""
        txtid.Text = ""
        txtFlag.Text = ""
        txtCIF.Text = ""
        txtNoIdentitasBU.Text = ""
        txtNamaBU.Text = ""
        txtTmpPendirian.Text = ""
        txtNoAktaPendirian.Text = ""
        txtNoAktaPerubahan.Text = ""
        txtNoTelp.Text = ""
        txtNoTelpSelular.Text = ""
        txtEmail.Text = ""
        txtAlamat.Text = ""
        txtKelurahan.Text = ""
        txtKecamatan.Text = ""
        txtKota.Text = ""
        txtKodePos.Text = ""
        txtKodeNegara.Text = ""
        txtKodeBU.Text = ""
        txtTglAktaPendirian.Text = ""
        txtTglAktaPerubahan.Text = ""
        txtTglPemeringkat.Text = ""
        txtKodeGolongan.Text = ""
        txtPeringkat.Text = ""
        txtNmGroup.Text = ""
        txtKodeCabang.Text = ""
		txtOperasiData.Text = ""
		txtSearch.Text = ""
	End Sub
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviD02SLIK.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
		If Not IsPostBack Then
			Me.FormID = "D02"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If

		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub
#End Region

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviD02SLIK.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.D02

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetD02(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If

        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        FillCombo()
        FillCbo(cboKodeBU, "dbo.TblBadanUsaha")
        FillCbo1(cboKodeHubungan, "dbo.TblKodeHubunganPelapor")
        FillCbo2(cboLembagaPemeringkat, "dbo.TblLembagaPemeringkat")

		If (isFrNav = False) Then
			GridNaviD02SLIK.Initialize(recordCount, pageSize)
		End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub

    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.D02

        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
		cboName.Items.Insert(0, "ALL")
		cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.D02
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "NamaBadanUsaha"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo1(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.D02
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo1(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Hubungan"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo2(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.D02
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo2(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Lembaga"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = ""
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Protected Sub btnLookup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookup.Click
        oGolonganDebitur.CmdWhere = "ALL"
        oGolonganDebitur.Sort = "Kode ASC"
        oGolonganDebitur.Popup()
    End Sub

    Protected Sub btnLookup2_Click(ByVal sender As Object, e As EventArgs) Handles btnLookup2.Click
        oSektorEkonomi.CmdWhere = "ALL"
        oSektorEkonomi.Sort = "Kode ASC"
        oSektorEkonomi.Popup()
    End Sub
    Protected Sub btnLookup3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookup3.Click
        oKodeKota.CmdWhere = "All"
        oKodeKota.Sort = "DatiID ASC"
        oKodeKota.Popup()
    End Sub

    Protected Sub oKodeKota_CatSelected(ByVal DatiID As String, ByVal namakabkot As String)
		txtKota.Text = namakabkot
		hdfkota.Value = DatiID
	End Sub

    Public Sub CatSelectedSektorEkonomi(ByVal Kode As String, SektorEkonomi As String)
		txtKodeBU.Text = SektorEkonomi.ToString
	End Sub

    Public Sub CatSelectedGolonganDebitur(ByVal Kode As String, Golongan As String)
		txtKodeGolongan.Text = Golongan.ToString
	End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.D02
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "D02", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetD02Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If


                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboKodeBU.SelectedIndex = cboKodeBU.Items.IndexOf(cboKodeBU.Items.FindByValue(oRow("KodeJenisBadanUsaha").ToString.Trim))
                    cboKodeHubungan.SelectedIndex = cboKodeHubungan.Items.IndexOf(cboKodeHubungan.Items.FindByValue(oRow("KodeHubungandenganLJK").ToString.Trim))
                    cboLembagaPemeringkat.SelectedIndex = cboLembagaPemeringkat.Items.IndexOf(cboLembagaPemeringkat.Items.FindByValue(oRow("LembagaPemeringkat").ToString.Trim))
                    txtTglAktaPendirian.Text = Format(oRow("TanggalAktePendirian"), "yyyy/MM/dd").ToString
                    txtTglAktaPerubahan.Text = Format(oRow("TanggalAktePerubahanTerakhir"), "yyyy/MM/dd").ToString
                    txtTglPemeringkat.Text = Format(oRow("TanggalPemeringkatan"), "yyyy/MM/dd").ToString
                    If txtTglPemeringkat.Text = "1900/01/01" Then
                        txtTglPemeringkat.Text = ""
                    End If
                End If

                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtFlag.Text = oParameter.ListData.Rows(0)("FlagDetail")
                txtCIF.Text = oParameter.ListData.Rows(0)("CIF")
                txtNoIdentitasBU.Text = oParameter.ListData.Rows(0)("NoIdentitasBadanUsaha")
                txtNamaBU.Text = oParameter.ListData.Rows(0)("NamaBadanUsaha")
                txtTmpPendirian.Text = oParameter.ListData.Rows(0)("TempatPendirian")
                txtNoAktaPendirian.Text = oParameter.ListData.Rows(0)("NoAktePendirian")
                txtNoAktaPerubahan.Text = oParameter.ListData.Rows(0)("NoAktePerubahanTerakhir")
                txtNoTelp.Text = oParameter.ListData.Rows(0)("Telepon")
                txtNoTelpSelular.Text = oParameter.ListData.Rows(0)("Hp")
                txtEmail.Text = oParameter.ListData.Rows(0)("AlamatEmail")
                txtAlamat.Text = oParameter.ListData.Rows(0)("Alamat")
				txtKelurahan.Text = oParameter.ListData.Rows(0)("Kelurahan")
				txtKecamatan.Text = oParameter.ListData.Rows(0)("Kecamatan")
				txtKota.Text = oParameter.ListData.Rows(0)("KodeSandiKab")
				txtKodePos.Text = oParameter.ListData.Rows(0)("KodePos")
				txtKodeNegara.Text = oParameter.ListData.Rows(0)("KodeNegaraDomisili")
                txtKodeBU.Text = oParameter.ListData.Rows(0)("KodeBidangUsaha")
                rbMelanggar.SelectedValue = oParameter.ListData.Rows(0)("MelanggarBMPK")
                rbMelampaui.SelectedValue = oParameter.ListData.Rows(0)("MelampauiBMPK")
                rbGoPublic.SelectedValue = oParameter.ListData.Rows(0)("GoPublic")
                txtKodeGolongan.Text = oParameter.ListData.Rows(0)("KodeGolonganDebitur")
                txtPeringkat.Text = oParameter.ListData.Rows(0)("Peringkat")
                txtNmGroup.Text = oParameter.ListData.Rows(0)("NamaGroupDebitur")
                txtKodeCabang.Text = oParameter.ListData.Rows(0)("KodeKantorCabang")
				txtOperasiData.Text = oParameter.ListData.Rows(0)("OperasiData")

				Dim param As String
				param = "datiID LIKE '%" + txtKota.Text + "%'"
				oKodeKota.Sort = "DatiID ASC"
				oKodeKota.Setback(param)

			ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "D02", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.D02
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.GetD02Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.D02
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BulanData = txtbulandata.Text
                .CIF = txtCIF.Text
                .NoIdentitasBadanUsaha = txtNoIdentitasBU.Text
                .NamaBadanUsaha = txtNamaBU.Text
                .KodeJenisBadanUsaha = cboKodeBU.SelectedValue.Trim
                .TempatPendirian = txtTmpPendirian.Text
                .NoAktePendirian = txtNoAktaPendirian.Text
                .TanggalAktePendirian = txtTglAktaPendirian.Text
                .NoAktePerubahanTerakhir = txtNoAktaPerubahan.Text
                .TanggalAktePerubahanTerakhir = txtTglAktaPerubahan.Text
                .Telepon = txtNoTelp.Text
                .Hp = txtNoTelpSelular.Text
                .AlamatEmail = txtEmail.Text
                .Alamat = txtAlamat.Text
                '.Kelurahan = oLookUpZipCode.Kelurahan.Trim()
                '.Kecamatan = oLookUpZipCode.Kecamatan.Trim()
                '.KodePos = oLookUpZipCode.ZipCode.Trim()
                '.KodeSandiKab = oLookUpZipCode.City.Trim()
                .Kelurahan = txtKelurahan.Text
                .Kecamatan = txtKecamatan.Text
                .KodePos = txtKodePos.Text
				'.KodeSandiKab = txtKota.Text
				.KodeSandiKab = hdfkota.Value
				.KodeNegaraDomisili = txtKodeNegara.Text
                .KodeBidangUsaha = txtKodeBU.Text
                .KodeHubungandenganLJK = cboKodeHubungan.SelectedValue.Trim()
                .MelanggarBMPK = rbMelanggar.SelectedValue
                .MelampauiBMPK = rbMelampaui.SelectedValue
                .GoPublic = rbGoPublic.SelectedValue
                .KodeGolonganDebitur = txtKodeGolongan.Text
                .Peringkat = txtPeringkat.Text
                .LembagaPemeringkat = cboLembagaPemeringkat.SelectedValue.Trim()
                .TanggalPemeringkatan = txtTglPemeringkat.Text
                .NamaGroupDebitur = txtNmGroup.Text
                .KodeKantorCabang = txtKodeCabang.Text
                .OperasiData = "C"

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetD02Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter

                .ID = txtid.Text
                .BulanData = txtbulandata.Text
                .CIF = txtCIF.Text
                .NoIdentitasBadanUsaha = txtNoIdentitasBU.Text
                .NamaBadanUsaha = txtNamaBU.Text
                .KodeJenisBadanUsaha = cboKodeBU.SelectedValue.Trim
                .TempatPendirian = txtTmpPendirian.Text
                .NoAktePendirian = txtNoAktaPendirian.Text
                .TanggalAktePendirian = txtTglAktaPendirian.Text
                .NoAktePerubahanTerakhir = txtNoAktaPerubahan.Text
                .TanggalAktePerubahanTerakhir = txtTglAktaPerubahan.Text
                .Telepon = txtNoTelp.Text
                .Hp = txtNoTelpSelular.Text
                .AlamatEmail = txtEmail.Text
                .Alamat = txtAlamat.Text
                '.Kelurahan = oLookUpZipCode.Kelurahan.Trim()
                '.Kecamatan = oLookUpZipCode.Kecamatan.Trim()
                '.KodePos = oLookUpZipCode.ZipCode.Trim()
                '.KodeSandiKab = oLookUpZipCode.City.Trim()
                .Kelurahan = txtKelurahan.Text
                .Kecamatan = txtKecamatan.Text
                .KodePos = txtKodePos.Text
				'.KodeSandiKab = txtKota.Text
				.KodeSandiKab = hdfkota.Value
				.KodeNegaraDomisili = txtKodeNegara.Text
                .KodeBidangUsaha = txtKodeBU.Text
                .KodeHubungandenganLJK = cboKodeHubungan.SelectedValue.Trim()
                .MelanggarBMPK = rbMelanggar.SelectedValue
                .MelampauiBMPK = rbMelampaui.SelectedValue
                .GoPublic = rbGoPublic.SelectedValue
                .KodeGolonganDebitur = txtKodeGolongan.Text
                .Peringkat = txtPeringkat.Text
                .LembagaPemeringkat = cboLembagaPemeringkat.SelectedValue.Trim()
                .TanggalPemeringkatan = txtTglPemeringkat.Text
                .NamaGroupDebitur = txtNmGroup.Text
                .KodeKantorCabang = txtKodeCabang.Text
                .OperasiData = "U"

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetD02Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region

#Region "Generate Data"
    Public Function GetCopy(ByVal customclass As Parameter.D02) As Parameter.D02
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKD02Generate", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Generate Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.D02
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = txtcopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "D02", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
			txtbulandata.Enabled = True
			txtCIF.ReadOnly = False
			txtKodeCabang.ReadOnly = False

			Me.Process = "ADD"
            clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
        Dim oParameter As New Maxiloan.Parameter.D02

        Dim ResClient As New StringBuilder
        Using sw As New StringWriter(ResClient)

            Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "D02" & "|" & Rc & "|" & Rc)
            sw.WriteLine(Header)
            For Each drow As DataRow In dtdata.Rows
                Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
                sw.WriteLine(lineoftext)
            Next

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
            Response.Write(ResClient.ToString())

            Using MyMemoryStream As New MemoryStream()
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using

        End Using
    End Sub

    Public Function GetToTXT(ByVal customclass As Parameter.D02) As Parameter.D02
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData
            params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
            params(1).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKD02Txt", params).Tables(0)
            customclass.TotalRecord = CType(params(1).Value, Int64)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
        Dim oEntities As New Parameter.D02
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BulanData = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToTXT(oEntities)
            dtViewData = oEntities.ListData
            Rc = oEntities.TotalRecord
            ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BulanData.Substring(0, oEntities.BulanData.Length - 2) & "." & oEntities.BulanData.Substring(4) & "." & "D02.01")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub
    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region
#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region
End Class