﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="F01.aspx.vb" Inherits="Maxiloan.Webform.SLIK.F01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc2" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucsektorekonomi" Src="../Webform.UserController/UcSektorEkonomi.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>F01</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
 <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK F01 - FASILITAS PEMBIAYAAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
       
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULANDATA">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NAME" SortExpression="NAME" HeaderText="NAME">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Nomor_Rekening_Fasilitas" SortExpression="Nomor_Rekening_Fasilitas" HeaderText="No Rek Fasilitas">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Nomor_CIF_Debitur" SortExpression="NomorCIFDebitur" HeaderText="CIF">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Nilai_Proyek" SortExpression="Nilai_Proyek" HeaderText="Nilai Proyek">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Persentase_Suku_Bunga_Imbalan" SortExpression="Persentase_Suku_Bunga_Imbalan" HeaderText="Suku Bunga(%)">
                            </asp:BoundColumn>
<%--                            <asp:BoundColumn DataField="Kode_Kantor_Cabang" SortExpression="Kode_Kantor_Cabang" HeaderText="Kode Cabang">
                            </asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="Operasi_Data" SortExpression="OperasiData" HeaderText="OperasiData">
                            </asp:BoundColumn>
                            
                        </Columns>
                    </asp:DataGrid>
                    <uc2:ucGridNav id="GridNaviF01SLIK" runat="server"/> 
                </div>
            </div>
        </div>
         <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
        </div>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Nomor_CIF_Debitur">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="Nomor_Rekening_Fasilitas">No Fasilitas/Agreement</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>     
            
</asp:Panel>

<asp:Panel ID="pnlAdd" runat="server">
 <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Nomor Rekening Fasilitas</label>
                <asp:TextBox ID ="txtNoRekFasilitas" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105" ></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">CIF/ID Debitur</label>
                <asp:TextBox ID ="txtCIF" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105" ></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Sifat Pembiayaan</label>
                <%--<asp:TextBox ID ="txtSifatKreditdanPembiayaan" runat ="server" Width ="2%" MaxLength ="100"
                    Columns ="105"  ></asp:TextBox>--%>
                <asp:DropDownList ID="cboSifatKreditdanPembiayaan" runat="server" Width ="20%">
                    <asp:ListItem Value="1">1 - Pembiayaan yang Direstrukturisasi</asp:ListItem>
                    <asp:ListItem Value="2">2 - Pengambilalihan Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="3">3 - Pembiayaan Subordinasi</asp:ListItem>
                    <asp:ListItem Value="4">4 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Jenis Pembiayaan</label>
                <%--<asp:TextBox ID ="txtJenisKreditdanPembiayaan" runat ="server" Width ="2%" MaxLength ="100"
                    Columns ="105"  ></asp:TextBox>--%>
                <asp:DropDownList ID="cboJenisKreditdanPembiayaan" runat="server">
                     <asp:ListItem Value="05">05 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Akad Pembiayaan</label>
                <%--<asp:TextBox ID ="txtKodeAkadKredit" runat ="server" Width ="2%" MaxLength ="100"
                    Columns ="105" ></asp:TextBox>--%>
                <asp:DropDownList ID="cboKodeAkadKredit" runat="server">
                    <asp:ListItem Value="00">00 - Konvensional</asp:ListItem>
                    <asp:ListItem Value="01">01 - Murabahah</asp:ListItem>
                    <asp:ListItem Value="02">02 - Istishna’</asp:ListItem>
                    <asp:ListItem Value="03">03 - Salam</asp:ListItem>
                    <asp:ListItem Value="04">04 - Qardh</asp:ListItem>
                    <asp:ListItem Value="05">05 - Mudharabah</asp:ListItem>
                    <asp:ListItem Value="06">06 - Musyarakah</asp:ListItem>
                    <asp:ListItem Value="07">07 - Ijarah</asp:ListItem>
                    <asp:ListItem Value="08">08 - Mudharabah Muqayyadah</asp:ListItem>
                    <asp:ListItem Value="09">09 - Ijarah Muntahiya Bitamlik</asp:ListItem>
                    <asp:ListItem Value="10">10 - Rahn</asp:ListItem>
                    <asp:ListItem Value="99">99 - Skema atau Akad Syariah Lain</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Nomor Akad Awal</label>
                <asp:TextBox ID ="txtNoAkadAwal" runat ="server" Width ="6%" MaxLength ="100"
                    Columns ="105" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" display="Dynamic"
                    ControlToValidate="txtNoAkadAwal" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Tanggal Akad Awal</label>
                    <%--<uc1:ucDateCE id="txtTanggalAkadAwal" runat="server" DataFormatString = "{0:yyyy/MM/dd}" ></uc1:ucDateCE>--%>
                    <asp:TextBox ID ="txt_TanggalAkadAwal" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
                    <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True"
                    TargetControlID="txt_TanggalAkadAwal" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" display="Dynamic"
                    ControlToValidate="txt_TanggalAkadAwal" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Nomor Akad Akhir</label>
                <asp:TextBox ID ="txtNoAkadAkhir" runat ="server" Width ="6%" MaxLength ="100"
                    Columns ="105" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" display="Dynamic"
                    ControlToValidate="txtNoAkadAkhir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" display="Dynamic"
                    ControlToValidate="txtNoAkadAkhir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Tanggal Akad Akhir</label>
                    <%--<uc1:ucDateCE id="txtTanggalAkadAkhir" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                    <asp:TextBox ID ="txt_TanggalAkadAkhir" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
               <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                    TargetControlID="txt_TanggalAkadAkhir" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" display="Dynamic"
                    ControlToValidate="txt_TanggalAkadAkhir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                    
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Frekuensi Perpanjangan Ke</label>
                <%--<asp:TextBox ID ="txtFrekuensiPerpanjangan" runat ="server" Width ="1%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboFrekuensiPerpanjangan" runat="server" Width ="20%">
                    <asp:ListItem Value="00">00 - Fasilitas pembiayaan fasilitas baru</asp:ListItem>
                    <asp:ListItem Value="01">01 - Fasilitas pembiayaan fasilitas yang telah diperpanjang 1 (satu) kali</asp:ListItem>
                    <asp:ListItem Value="02">02 - Fasilitas pembiayaan fasilitas yang telah diperpanjang 2 (dua) kali</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Tanggal Awal Fasilitas Pembiayaan</label>
                    <%--<uc1:ucDateCE id="txtTanggalAwalFasilitas" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                        <asp:TextBox ID ="txt_TanggalAwalFasilitas" runat ="server" Width ="5%" MaxLength ="100"
                        Columns ="105" enabled ="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                        TargetControlID="txt_TanggalAwalFasilitas" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" display="Dynamic"
                        ControlToValidate="txt_TanggalAwalFasilitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Tanggal Mulai Fasilitas</label>
                       <%--<uc1:ucDateCE id="txtTanggalMulaiFasilitas" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                        <asp:TextBox ID ="txt_TanggalMulaiFasilitas" runat ="server" Width ="5%" MaxLength ="100"
                        Columns ="105" enabled ="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True"
                        TargetControlID="txt_TanggalMulaiFasilitas" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                        ControlToValidate="txt_TanggalMulaiFasilitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>                        
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Tanggal Jatuh Tempo Fasilitas</label>
                        <%--<uc1:ucDateCE id="txtTanggalJatuhTempoFasilitas" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                        <asp:TextBox ID ="txt_TanggalJatuhTempoFasilitas" runat ="server" Width ="5%" MaxLength ="100"
                        Columns ="105" enabled ="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender6" runat="server" Enabled="True"
                        TargetControlID="txt_TanggalJatuhTempoFasilitas" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" display="Dynamic"
                        ControlToValidate="txt_TanggalJatuhTempoFasilitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Kategori Debitur</label>
                <%--<asp:TextBox ID ="txtKodeKategoriDebitur" runat ="server" Width ="2%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboKodeKategoriDebitur" runat="server" Width ="20%">
                    <asp:ListItem Value="10">10 - Debitur UMKM–Dengan Penjaminan atau Asuransi Pembiayaan-Penjamin Tertentu–Mikro</asp:ListItem>
                    <asp:ListItem Value="20">20 - Debitur UMKM-Dengan Penjaminan atau Asuransi Pembiayaan-Penjamin Tertentu–Kecil</asp:ListItem>
                    <asp:ListItem Value="30">30 - Debitur UMKM-Dengan Penjaminan atau Asuransi Pembiayaan-Penjamin Tertentu-Menengah</asp:ListItem>
                    <asp:ListItem Value="40">40 - Debitur UMKM-Dengan Penjaminan atau Asuransi Pembiayaan-Penjamin Lainnya–Mikro</asp:ListItem>
                    <asp:ListItem Value="50">50 - Debitur UMKM-Dengan Penjaminan atau Asuransi Pembiayaan-Penjamin Lainnya-Kecil</asp:ListItem>
                    <asp:ListItem Value="60">60 - Debitur UMKM-Dengan Penjaminan atau Asuransi Pembiayaan-Penjamin Lainnya– Menengah</asp:ListItem>
                    <asp:ListItem Value="70">70 - Debitur UMKM- UMKM Lainnya–Mikro</asp:ListItem>
                    <asp:ListItem Value="80">80 - Debitur UMKM-UMKM Lainnya–Kecil</asp:ListItem>
                    <asp:ListItem Value="90">90 - Debitur UMKM-UMKM Lainnya–Menengah</asp:ListItem>
                    <asp:ListItem Value="99">99 - Bukan Debitur Usaha Mikro, Kecil, dan Menengah</asp:ListItem> 
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Jenis Penggunaan Fasilitas</label>
                <%--<asp:TextBox ID ="txtKodeJenisPenggunaanFasilitas" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboKodeJenisPenggunaanFasilitas" runat="server">
                    <asp:ListItem Value="1">1 - Modal Kerja</asp:ListItem>
                    <asp:ListItem Value="2">2 - Investasi</asp:ListItem>
                    <asp:ListItem Value="3">3 - Konsumsi</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Orientasi Penggunaan Fasilitas</label>
                <%--<asp:TextBox ID ="txtKodeOrientasiPenggunaanFasilitas" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboKodeOrientasiPenggunaanFasilitas" runat="server">
                    <asp:ListItem Value="1">1 - Ekspor</asp:ListItem>
                    <asp:ListItem Value="2">2 - Impor</asp:ListItem>
                    <asp:ListItem Value="3">3 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

     <asp:UpdatePanel runat="server">
         <ContentTemplate>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Sektor Ekonomi</label>
                <asp:TextBox ID ="txtKodeSektorEkonomi" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:Button ID="btnLookup" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                <uc2:ucsektorekonomi id="oSektorEkonomi" runat="server" oncatselected="CatSelectedSektorEkonomi"></uc2:ucsektorekonomi> 
                 <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeSektorEkonomi" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>
             </ContentTemplate>
     </asp:UpdatePanel>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">KodeKabupaten/Kota Lokasi Proyek</label>
                <asp:TextBox ID ="txtKodeKabKot" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeKabKot" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Nilai Proyek</label>
                <%--<asp:TextBox ID ="txtNilaiProyek" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucNilaiProyek" runat="server"/>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Valuta</label>
                <%--<asp:TextBox ID ="txtKodeValuta" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                
                <asp:DropDownList ID="cboKodeValuta" runat="server"></asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Suku Bunga Atau Imbalan</label>
                <asp:TextBox ID ="txtSukuBungaImbalan" runat ="server" Width ="2%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" display="Dynamic"
                    ControlToValidate="txtSukuBungaImbalan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                <label>%</label>
                
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Jenis Suku Bunga Atau Imbalan</label>
                <%--<asp:TextBox ID ="txtJenisSukuBungaImbalan" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboJenisSukuBungaImbalan" runat="server">
                    <asp:ListItem Value="1">1 - Suku Bunga Fixed</asp:ListItem>
                    <asp:ListItem Value="2">2 - Suku Bunga Floating</asp:ListItem>
                    <asp:ListItem Value="3">3 - Margin</asp:ListItem>
                    <asp:ListItem Value="4">4 - Bagi Hasil</asp:ListItem>
                    <asp:ListItem Value="5">5 - Ujroh</asp:ListItem>
                    <asp:ListItem Value="9">9 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Pembiayaan Program Pemerintah</label>
                <%--<asp:TextBox ID ="txtKreditPembiayaanPemerintah" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboKreditPembiayaanPemerintah" runat="server" Width ="20%">
                    <asp:ListItem Value="001">001 - Pembiayaan Bukan Program Pemerintah</asp:ListItem>
                    <asp:ListItem Value="002">002 - Pembiayaan Usaha Rakyat atau KUR Syariah</asp:ListItem>
                    <asp:ListItem Value="003">003 - Pembiayaan atau Pembiayaan Pemilikan Rumah Bersubsidi</asp:ListItem>
                    <asp:ListItem Value="900">900 - Pembiayaan atau Pembiayaan Program Pemerintah Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Asal Pembiayaan Takeover</label>
                <asp:TextBox ID ="txtAsalKreditPembiayaantakeover" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Sumber Dana</label>
                <asp:TextBox ID ="txtSumberDana" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Plafon Awal</label>
                <%--<asp:TextBox ID ="txtPlafonAwal" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucPlafonAwal" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Plafon Efektif (Outstanding Pokok Menurun)</label>
                <%--<asp:TextBox ID ="txtPlafonEfektif" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucPlafonEfektif" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Realisasi atau Pencairan Bulan Berjalan</label>
                <%--<asp:TextBox ID ="txtRealisasiPencairanBulanBerjalan" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucRealisasiPencairanBulanBerjalan" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Denda</label>
                <%--<asp:TextBox ID ="txtDenda" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucDenda" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Baki Debet</label>
                <%--<asp:TextBox ID ="txtBakiDebet" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucBakiDebet" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Nilai Dalam Mata Uang Asal</label>
                <%--<asp:TextBox ID ="txtNilaiDalamMataUangAsal" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucNilaiDalamMataUangAsal" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Kualitas Pembiayaan</label>
                <%--<asp:TextBox ID ="txtKodeKualitasKredit" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboKodeKualitasKredit" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                        <label >Tanggal Macet</label>
                       <%-- <uc1:ucDateCE id="txtTanggalMacet" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                        <asp:TextBox ID ="txt_TanggalMacet" runat ="server" Width ="5%" MaxLength ="100"
                        Columns ="105" enabled ="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender7" runat="server" Enabled="True"
                        TargetControlID="txt_TanggalMacet" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                        ControlToValidate="txt_TanggalMacet" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Sebab Macet</label>
                <%--<asp:TextBox ID ="txtSebabMacet" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboSebabMacet" runat="server">
                    <asp:ListItem Value="01">01 - Kesulitan Pemasaran</asp:ListItem>
                    <asp:ListItem Value="02">02 - Kesulitan Manajemen dan Permasalahan Tenaga Kerja</asp:ListItem>
                    <asp:ListItem Value="03">03 - Perusahaan Grup atau Afiliasi yang Sangat Merugikan Debitur</asp:ListItem>
                    <asp:ListItem Value="04">04 - Permasalahan Terkait Pengelolaan Lingkungan Hidup</asp:ListItem>
                    <asp:ListItem Value="05">05 - Penggunaan Dana Tidak Sesuai dengan Perjanjian Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="06">06 - Kelemahan Dalam Analisa Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="07">07 - Fluktuasi Nilai Tukar</asp:ListItem>
                    <asp:ListItem Value="08">08 - Itikad Tidak Baik</asp:ListItem>
                    <asp:ListItem Value="09">08 - Keadaan Kahar (Force Majeur)</asp:ListItem>
                    <asp:ListItem Value="10">10 - Pailit</asp:ListItem>
                    <asp:ListItem Value="11">11 - Uniform Classification</asp:ListItem>
                    <asp:ListItem Value="99">99 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Tunggakan Pokok</label>
                <%--<asp:TextBox ID ="txtTunggakanPokok" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucTunggakanPokok" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Tunggakan Bunga atau Imbalan</label>
                <%--<asp:TextBox ID ="txtTunggakanBungaImbalan" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <uc2:ucnumberformat id="ucTunggakanBungaImbalan" runat="server" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Jumlah Hari Tunggakan</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Frekwensi Tunggakan</label>
                <asp:TextBox ID ="txtFrekwensiTunggakan" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Frekwensi Restrukturisasi</label>
                <asp:TextBox ID ="txtFrekwensiRestrukturisasi" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                        <label >Tanggal Restrukturisasi Awal</label>
                        <%--<uc1:ucDateCE id="txtTglRestrukturisasiAwal" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                        <asp:TextBox ID ="txt_TglRestrukturisasiAwal" runat ="server" Width ="5%" MaxLength ="100"
                        Columns ="105" enabled ="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender8" runat="server" Enabled="True"
                        TargetControlID="txt_TglRestrukturisasiAwal" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                        ControlToValidate="txt_TglRestrukturisasiAwal" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                        <label >Tanggal Restrukturisasi Akhir</label>
                        <%--<uc1:ucDateCE id="txtTglRestrukturisasiAkhir" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                        <asp:TextBox ID ="txt_TglRestrukturisasiAkhir" runat ="server" Width ="5%" MaxLength ="100"
                        Columns ="105" enabled ="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender9" runat="server" Enabled="True"
                        TargetControlID="txt_TglRestrukturisasiAkhir" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" display="Dynamic"
                        ControlToValidate="txt_TglRestrukturisasiAkhir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>    
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Cara Restrukturisasi</label>
                <%--<asp:TextBox ID ="txtCaraRestrukturisasi" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboCaraRestrukturisasi" runat="server">
                    <asp:ListItem Value="01">01 - Penurunan suku bunga Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="02">02 - Perpanjangan jangka waktu Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="03">03 - Pengurangan tunggakan pokok Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="04">04 - Pengurangan tunggakan bunga Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="05">05 - Penambahan fasilitas Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="06">06 - Konversi Pembiayaan menjadi penyertaan modal sementara</asp:ListItem>
                    <asp:ListItem Value="07">07 - Penambahan fasilitas Pembiayaan dan pengurangan tunggakan bunga Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="08">08 - Penambahan fasilitas Pembiayaan dan perpanjangan jangka waktu Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="09">09 - Penambahan fasilitas Pembiayaan dan penurunan suku bunga Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="10">10 - Penambahan fasilitas Pembiayaan, pengurangan tunggakan bunga kedit dan penurunan suku bunga Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="11">11 - Penambahan fasilitas Pembiayaan, pengurangan tunggakan bunga Pembiayaan dan perpanjangan jangka waktu Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="12">12 - Penjadwalan Kembali (Syariah)</asp:ListItem>
                    <asp:ListItem Value="13">13 - Perubahan jadwal pembayaran (Syariah)</asp:ListItem>
                    <asp:ListItem Value="14">14 - Perubahan jumlah angsuran (Syariah)</asp:ListItem>
                    <asp:ListItem Value="15">15 - Perubahan jangka waktu (Syariah)</asp:ListItem>
                    <asp:ListItem Value="16">16 - Perubahan nisbah dalam pembiayaan Mudharabah atau Pembiayaan Musyarakah (Syariah)</asp:ListItem>
                    <asp:ListItem Value="17">17 - Perubahan Porsi Bagi Hasil (PBH) dalam pembiayaan Mudharabah atau Pembiayaan Musyarakah (Syariah)</asp:ListItem>
                    <asp:ListItem Value="18">18 - Pemberian potongan (Syariah)</asp:ListItem>
                    <asp:ListItem Value="19">19 - Penambahan dana fasilitas pembiayaan bank (Syariah)</asp:ListItem>
                    <asp:ListItem Value="20">20 - Konversi akad pembiayaan (Syariah)</asp:ListItem>
                    <asp:ListItem Value="21">21 - Konversi pembiayaan menjadi penyertaan modal pada perusahaan nasabah (Syariah)</asp:ListItem>
                    <asp:ListItem Value="99">99 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kondisi</label>
                <%--<asp:TextBox ID ="txtKodeKondisi" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:DropDownList ID="cboKodeKondisi" runat="server">
                    <asp:ListItem Value="00">00 - Fasilitas Aktif</asp:ListItem>
                    <asp:ListItem Value="01">01 - Dibatalkan</asp:ListItem>
                    <asp:ListItem Value="02">02 - Lunas</asp:ListItem>
                    <asp:ListItem Value="03">03 - Dihapusbukukan</asp:ListItem>
                    <asp:ListItem Value="04">04 - Hapus Tagih</asp:ListItem>
                    <asp:ListItem Value="05">05 - Lunas karena pengambilalihan agunan</asp:ListItem>
                    <asp:ListItem Value="06">06 - Lunas karena diselesaikan melalui pengadilan</asp:ListItem>
                    <asp:ListItem Value="07">07 - Dialihkan atau Dijual ke Pelapor lain</asp:ListItem>
                    <asp:ListItem Value="08">08 - Dialihkan ke Fasilitas lain</asp:ListItem>
                    <asp:ListItem Value="09">09 - Dialihkan atau dijual kepada pihak lain non-Pelapor</asp:ListItem>
                    <asp:ListItem Value="10">10 - Disekuritisasi (Ba'i/Mu'ajjir Asal sebagai Servicer)</asp:ListItem>
                    <asp:ListItem Value="11">11 - Disekuritisasi (Ba'i/Mu'ajjir Asal tidak sebagai Servicer)</asp:ListItem>
                    <asp:ListItem Value="12">12 - Lunas Dengan Diskon</asp:ListItem>
                    <asp:ListItem Value="13">13 - Diblokir Sementara</asp:ListItem>
                    <asp:ListItem Value="14">14 - Berhenti dari Keanggotaan Pembiayaan Joint Account</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                    <label >Tanggal Kondisi</label>                    
                    <%--<uc1:ucDateCE id="txtsTanggalKondisi" runat="server" DataFormatString = "{0:yyyy/MM/dd}"></uc1:ucDateCE>--%>
                    <asp:TextBox ID ="txt_TanggalKondisi" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender10" runat="server" Enabled="True"
                    TargetControlID="txt_TanggalKondisi" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                    ControlToValidate="txt_TanggalKondisi" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>    
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Keterangan</label>
                <asp:TextBox ID ="txtKeterangan" runat ="server" Width ="30%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Kantor Cabang</label>
                <asp:TextBox ID ="txtKodeKantorCabang" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105" ></asp:TextBox>
            </div>
        </div>

     <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>

        <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form> 
</body>
</html>