﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="F06.aspx.vb" Inherits="Maxiloan.Webform.SLIK.F06" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title>Fasilitas Lain</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK F06 - FASILITAS LAIN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>
                                <%--<asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>--%>   
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="15%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIF" SortExpression="CIF" HeaderText="CIF">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NoRekeningFasilitas" SortExpression="NoRekeningFasilitas" HeaderText="No Rek Fasilitas">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TanggalMulai" SortExpression="TanggalMulai" HeaderText="Tanggal Mulai">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TanggalJatuhTempo" SortExpression="TanggalJatuhTempo" HeaderText="Tanggal Jatuh Tempo">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SukuBungaatauImbalan" SortExpression="SukuBungaatauImbalan" HeaderText="Suku Bunga (%)">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Nominal" SortExpression="Nominal" HeaderText="Nominal">
                            </asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="KodeKantorCabang" SortExpression="KodeKantorCabang" HeaderText="Kode Cabang">
                            </asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="OperasiData" SortExpression="OperasiData" HeaderText="Operasi Data">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                        <uc2:ucGridNav id="GridNaviF06SLIK" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
                </div>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="CIF"  Selected="True">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="NoRekeningFasilitas">No Rek Fasilitas/No Agreement</asp:ListItem>
                    <asp:ListItem Value="Nominal">Nama Debitur</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>          

 <asp:Panel ID="pnlAdd" runat="server">
 <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>


     <div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           No Rekening Fasilitas
                </label>
                <asp:TextBox ID="txtNoRekFasilitas" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                    ControlToValidate="txtNoRekFasilitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           CIF
                </label>
                <asp:TextBox ID="txtCIF" runat="server" CssClass="medium_text" Width ="10%" MaxLength ="100" Columns ="105" readonly ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*"></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Jenis Fasilitas Lain
                </label>

                <asp:DropDownList ID="cboKdJenisFasilitas" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap Pilih Jenis Fasilitas"
                                ControlToValidate="cboKdJenisFasilitas" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" /> 
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Sumber Dana
                </label>
                <asp:TextBox ID="txtSumberDana" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" display="Dynamic"
                    ControlToValidate="txtSumberDana" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Tanggal Mulai
                </label>

                <asp:TextBox ID="txtTglMulai" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105"></asp:TextBox>

                 <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="txtTglMulai" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" display="Dynamic" ControlToValidate="txtTglMulai" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Tanggal Jatuh Tempo
                </label>
                <asp:TextBox ID="txtTglJatuhTempo" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtTglJatuhTempo" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                    ControlToValidate="txtTglJatuhTempo" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Suku Bunga atau Imbalan
                </label>
                <asp:TextBox ID="txtSukuBunga" runat="server" CssClass="medium_text"   Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" display="Dynamic"
                    ControlToValidate="txtSukuBunga" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode valuta
                </label>

                <asp:DropDownList ID="cboKodeValuta" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Harap Pilih Valuta"
                                ControlToValidate="cboKodeValuta" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
            	</div>
		    </div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Nominal
                </label>
                <asp:TextBox ID="txtNominal" runat="server" CssClass="medium_text" Width ="15%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" display="Dynamic"
                    ControlToValidate="txtNominal" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                          Nilai Dalam Mata uang Asal
                </label>
                <asp:TextBox ID="txtNilaiMataUang" runat="server" CssClass="medium_text"  Width ="15%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator17" runat="server" display="Dynamic"
                    ControlToValidate="txtNilaiMataUang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kualitas
                </label>

                <asp:DropDownList ID="cboKodeKualitas" runat="server" Width ="20
                    " MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Harap Pilih Kode Kualitas"
                                ControlToValidate="cboKodeKualitas" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
			</div>
		</div>

        <div class="form_box">
			<div class="form_single">
				<label>
                           Tanggal Macet
                </label>
                <asp:TextBox ID="txttglmacet" runat="server" CssClass="medium_text"  Width ="7%" MaxLength ="100" Columns ="105" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txttglmacet" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="txttglmacet" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Kode Sebab Macet
                </label>

                <asp:DropDownList ID="cboKodeSebabMacet" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Harap Pilih Kode Sebab Macet"
                  ControlToValidate="cboKodeSebabMacet" InitialValue="Select One" Display="Dynamic"
                  CssClass="validator_general" />
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Tunggakan
                </label>
                <asp:TextBox ID="txttunggakan" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" display="Dynamic"
                    ControlToValidate="txttunggakan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Jumlah Hari Tunggakan
                </label>
                <asp:TextBox ID="txtjmlharitunggakan" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="false" AutoPostBack="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator26" runat="server" display="Dynamic"
                    ControlToValidate="txtjmlharitunggakan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kondisi
                </label>
                <asp:DropDownList ID="cboKodeKondisi" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Harap Pilih Kondisi"
                                ControlToValidate="cboKodeKondisi" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />

                <asp:RequiredFieldValidator ID="Requiredfieldvalidator29" runat="server" display="Dynamic"
                    ControlToValidate="cboKodeKondisi" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Tanggal Kondisi
                </label>
                <asp:TextBox ID="txtTglKondisi" runat="server" CssClass="medium_text"  Width ="7%" MaxLength ="100" Columns ="105" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtTglKondisi" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" display="Dynamic"
                    ControlToValidate="txtTglKondisi" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

       <div class="form_box">
			<div class="form_single">
				<label >
                           Keterangan
                </label>
                <asp:TextBox ID="txtKeterangan" runat="server" CssClass="medium_text"  Width ="20%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" display="Dynamic"
                    ControlToValidate="txtKeterangan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

        <div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kantor Cabang
                </label>
                <asp:TextBox ID="txtKodeCabang" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeCabang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

                <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>


    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
          </asp:Panel>
  </form>
</body>
</html>
