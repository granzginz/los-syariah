﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="K01.aspx.vb" Inherits="Maxiloan.Webform.SLIK.K01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc5" TagName="uckodekota" Src="../Webform.UserController/uckodekota.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Debitur Perseorangan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script src="../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Data Yang Dihapus Hanya Akan Merubah Operasi Data Menjadi 'D' ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
 <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK K01 - AGUNAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btncetakheader" runat="server"  Text="Cetak Header K01" CssClass="small button green">
                    </asp:Button>               
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>
                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>                          
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomerId" SortExpression="CustomerId" HeaderText="CustomerId">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TglLaporanKeuangan" SortExpression="TglLaporanKeuangan" HeaderText="TGL LAPORAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AssetIDR" SortExpression="AssetIDR" HeaderText="ASSET IDR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Liabilitas" SortExpression="Liabilitas" HeaderText="LIABILITAS">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Ekuitas" SortExpression="Ekuitas" HeaderText="EKUITAS">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KodeKantorCabang" SortExpression="KodeKantorCabang" HeaderText="KODE KANTOR CABANG">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OperasiData" SortExpression="OperasiData" HeaderText="Operasi Data">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                   <uc2:ucGridNav id="GridNav" runat="server"/>
                    </div>
                </div>
            </div>
            <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
        </div>
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="NamaIdentitas" Selected="True">Nama Debitur</asp:ListItem>
                    <asp:ListItem Value="CIF">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="NIK">NIK Debitur</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
     </asp:Panel>
     <asp:Panel ID="pnlAdd" runat="server">
        <asp:TextBox ID="ID" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <asp:TextBox ID="FlagDetail" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Customer Id</label>		
		        <asp:TextBox id="customerid" runat="server"></asp:TextBox>
	        </div>			
        </div>	
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        Tgl Proyeksi
                </label>
                <asp:TextBox ID="TglLaporanKeuangan" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="TglLaporanKeuangan" Format="yyyy/MM/dd" PopupPosition="BottomRight" DefaultView="Days"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="TglLaporanKeuangan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Asset IDR</label>		
		        <asp:TextBox id="AssetIDR" runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>		
        <div class="form_box_header">
            <div class="form_single">
                <label>
                    <b><i>
                        ASSET LANCAR
                    </i></b>                            
                </label>
            </div>
        </div>               
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Asset Lancar</label>		
		        <asp:TextBox id="AssetLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator1" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Kas dan Setara Kas (L)</label>		
		        <asp:TextBox id="KasSetaraKasAsetLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator2" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Piutang Usaha/Pembiayaan (L)</label>		
		        <asp:TextBox id="PiutangUsahAsetLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator3" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Investasi/Aset Keuangan Lainnya (L) </label>		
		        <asp:TextBox id="InvestasiLainnyaAsetLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator4" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Aset Lancar Lainnya</label>		
		        <asp:TextBox id="AsetLancarLainnya"	runat="server"></asp:TextBox>
	        </div>	
                <asp:RangeValidator runat="server" ID="RangeValidator5" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>            
        </div>			
        <div class="form_box_header">
            <div class="form_single">
                <label>
                    <b><i>
                        ASSET TIDAK LANCAR
                    </i></b>                            
                </label>
            </div>
        </div>                
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Aset Tidak Lancar</label>		
		        <asp:TextBox id="AsetTidakLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator6" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Piutang Usaha/Pembiayaan (TL)</label>		
		        <asp:TextBox id="PiutangUsahaAsetTidakLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator7" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Investasi/Aset Keuangan Lainnya (TL)</label>		
		        <asp:TextBox id="InvestasiLainnyaAsetTidakLancar"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator8" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Aset Tidak Lancar Lainnya</label>		
		        <asp:TextBox id="AsetTidakLancarLainnya"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator9" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>		
        <div class="form_box_header">
            <div class="form_single">
                <label>
                    <b><i>
                        LIABILITAS
                    </i></b>                            
                </label>
            </div>
        </div>                  
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Liabilitas</label>		
		        <asp:TextBox id="Liabilitas"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator10" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Liabilitas Jangka Pendek</label>		
		        <asp:TextBox id="LiabilitasJangkaPendek"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator11" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Pinjaman Jangka Pendek</label>		
		        <asp:TextBox id="PinjamanJangkaPendek"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator12" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Utang Usaha Jangka Pendek</label>		
		        <asp:TextBox id="UtangUsahaJangkaPendek"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator13" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Liabilitas Jangka Pendek Lainnya</label>		
		        <asp:TextBox id="LiabilitasJangkaPendekLainnya"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator14" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator16" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Liabilitas Jangka Panjang</label>		
		        <asp:TextBox id="LiabilitasJangkaPanjang"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator15" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator17" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Pinjaman Jangka Panjang</label>		
		        <asp:TextBox id="PinjamanJangkaPanjang"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator16" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator18" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Utang Usaha Jangka Panjang </label>		
		        <asp:TextBox id="UtangUsahaJangkaPanjang"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator17" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator19" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Liabilitas Jangka Panjang Lainnya</label>		
		        <asp:TextBox id="LiabilitasJangkaPanjangLainnya"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator18" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator20" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>	
        <div class="form_box_header">
            <div class="form_single">
                <label>
                    <b><i>
                        EKUITAS
                    </i></b>                            
                </label>
            </div>
        </div>                
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Ekuitas</label>		
		        <asp:TextBox id="Ekuitas"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator19" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator21" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Pendapatan Usaha/Operasional</label>		
		        <asp:TextBox id="PendapatanUsahaOpr"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator20" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator22" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Beban Pokok Pendapatan/Beban Operasional</label>		
		        <asp:TextBox id="BebanPokokPendapatanOpr"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator21" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator23" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Laba/Rugi Bruto</label>		
		        <asp:TextBox id="LabaRugiBruto"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator22" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator24" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Pendapatan Lain-lain/Non Opr </label>		
		        <asp:TextBox id="PLLNonOpr"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator23" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator25" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Beban Lain-lain/Non Operasional </label>		
		        <asp:TextBox id="BebanLainLainNonOpr"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator24" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator26" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator25" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Laba/Rugi Sebelum Pajak </label>		
		        <asp:TextBox id="LabaRugiSebelumPajak"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator25" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator27" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>				
        <div class="form_box">				
	        <div class="form_single">			
		        <label>Laba/Rugi Tahun Berjalan</label>		
		        <asp:TextBox id="LabaRugiTahunBerjalan"	runat="server"></asp:TextBox>
                <asp:RangeValidator runat="server" ID="RangeValidator27" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator29" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
	        </div>			
        </div>	
		<div class="form_box">
			<div class="form_single">
				<label>
                           Kode Kantor Cabang
                </label>
                <asp:TextBox ID="KodeKantorCabang" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator30" runat="server" display="Dynamic"
                    ControlToValidate="KodeKantorCabang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                <asp:RangeValidator runat="server" ID="RangeValidator26" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                    ControlToValidate="AssetIDR" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator28" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                    ControlToValidate="AssetIDR" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server" 
                    ControlToValidate="AssetIDR" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
			</div>
		</div>

   <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
     </asp:Panel>
    </form>
</body>
</html>
