﻿#Region "Import"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class P01
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private oController As New P01Controller
    Private oCustomclass As New Parameter.P01
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property SortBy() As String
        Get
            Return CType(ViewState("SortBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy") = Value
        End Set
    End Property
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Protected WithEvents GridNaviP01SLIK As ucGridNav

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviP01SLIK.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
		If Not IsPostBack Then
			Me.FormID = "P01"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If

		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub
#End Region

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviP01SLIK.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.P01

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetP01(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If

        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        FillCombo()
        FillCbo(cboKodeGolongan, "dbo.TblKodeHubunganPelapor")

        If (isFrNav = False) Then
            GridNaviP01SLIK.Initialize(recordCount, pageSize)
        End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub

#Region "DropDown"
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable
        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.P01

        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.P01
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Hubungan"
        cboName.DataValueField = "Kode"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub clean()
        txtbulandata.Text = ""
        txtid.Text = ""
        txtFlag.Text = ""
        txtCIF.Text = ""
        txtNIK.Text = ""
        txtNamaIdentitas.Text = ""
        txtNamaLengkap.Text = ""
        txtKodeSegmen.Text = ""
        txtNomorRekening.Text = ""
        txtAlamat.Text = ""
        txtPresentaseFasilitas.Text = ""
        txtKeterangan.Text = ""
        txtKodeCabang.Text = ""
        txtOperasiData.Text = ""
    End Sub
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.P01
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "P01", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetP01Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If
                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboKodeGolongan.SelectedIndex = cboKodeGolongan.Items.IndexOf(cboKodeGolongan.Items.FindByValue(oRow("Kode_Jenis_Identitas_Penjamin").ToString.Trim))
                End If
                txtbulandata.Text = oParameter.ListData.Rows(0)("BulanData")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtFlag.Text = oParameter.ListData.Rows(0)("FlagDetail")
                txtCIF.Text = oParameter.ListData.Rows(0)("CIf")
                cboJenisIdentitas.SelectedItem.Value = oParameter.ListData.Rows(0)("Kode_Jenis_Identitas_Penjamin")
                txtNIK.Text = oParameter.ListData.Rows(0)("Nomor_Identitas_Penjamin")
                txtNamaIdentitas.Text = oParameter.ListData.Rows(0)("Nama_Penjamin_Sesuai_Identitas")
                txtNamaLengkap.Text = oParameter.ListData.Rows(0)("Nama_Lengkap_Penjamin")
                txtKodeSegmen.Text = oParameter.ListData.Rows(0)("Kode_Jenis_Segmen_Fasilitas")
                txtNomorRekening.Text = oParameter.ListData.Rows(0)("Nomor_Rekening_Fasilitas")
                txtAlamat.Text = oParameter.ListData.Rows(0)("Alamat_Penjamin")
                txtPresentaseFasilitas.Text = oParameter.ListData.Rows(0)("Keterangan")
                txtKeterangan.Text = oParameter.ListData.Rows(0)("Persentase_Fasilitas_yang_Dijamin")
                txtKodeCabang.Text = oParameter.ListData.Rows(0)("Kode_Kantor_Cabang")
                txtOperasiData.Text = oParameter.ListData.Rows(0)("Operasi_Data")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "P01", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.P01
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.GetP01Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.P01
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BulanData = txtbulandata.Text
                .FlagDetail = txtFlag.Text
                .Nomor_Identitas_Penjamin = txtNIK.Text
                .Nomor_Rekening_Fasilitas = txtNomorRekening.Text
                .CIF = txtCIF.Text
                .Kode_Jenis_Segmen_Fasilitas = txtKodeSegmen.Text
                .Kode_Jenis_Identitas_Penjamin = cboJenisIdentitas.SelectedValue.Trim()
                .Nama_Penjamin_Sesuai_Identitas = txtNamaIdentitas.Text
                .Nama_Lengkap_Penjamin = txtNamaLengkap.Text
                .Kode_Golongan_Penjamin = "8613"
                .Alamat_Penjamin = txtAlamat.Text
                .Persentase_Fasilitas_yang_Dijamin = txtPresentaseFasilitas.Text
                .Keterangan = txtKeterangan.Text
                .Kode_Kantor_Cabang = txtKodeCabang.Text
                .Operasi_Data = "C"

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetP01Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter

                .ID = txtid.Text
                .BulanData = txtbulandata.Text
                .BulanData = txtbulandata.Text
                .FlagDetail = txtFlag.Text
                .Nomor_Identitas_Penjamin = txtNIK.Text
                .Nomor_Rekening_Fasilitas = txtNomorRekening.Text
                .CIF = txtCIF.Text
                .Kode_Jenis_Segmen_Fasilitas = txtKodeSegmen.Text
                .Kode_Jenis_Identitas_Penjamin = cboJenisIdentitas.SelectedValue.Trim()
                .Nama_Penjamin_Sesuai_Identitas = txtNamaIdentitas.Text
                .Nama_Lengkap_Penjamin = txtNamaLengkap.Text
                .Kode_Golongan_Penjamin = "8613"
                .Alamat_Penjamin = txtAlamat.Text
                .Persentase_Fasilitas_yang_Dijamin = txtPresentaseFasilitas.Text
                .Keterangan = txtKeterangan.Text
                .Kode_Kantor_Cabang = txtKodeCabang.Text
                .Operasi_Data = "U"

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetP01Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Generate Data"
    Public Function GetCopy(ByVal customclass As Parameter.P01) As Parameter.P01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKP01Generate", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Generate Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.P01
        Dim dtViewData As New DataTable
        With oEntities
            .BulanData = txtcopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "P01", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
			txtbulandata.Enabled = True
			txtCIF.ReadOnly = False
			txtKodeCabang.ReadOnly = False

			Me.Process = "ADD"
            clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
        Dim oParameter As New Maxiloan.Parameter.P01

        Dim ResClient As New StringBuilder
        Using sw As New StringWriter(ResClient)

            Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "P01" & "|" & Rc & "|" & Rc)
            sw.WriteLine(Header)
            For Each drow As DataRow In dtdata.Rows
                Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
                sw.WriteLine(lineoftext)
            Next

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
            Response.Write(ResClient.ToString())

            Using MyMemoryStream As New MemoryStream()
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using

        End Using
    End Sub

    Public Function GetToTXT(ByVal customclass As Parameter.P01) As Parameter.P01
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData
            params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
            params(1).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKP01Txt", params).Tables(0)
            customclass.TotalRecord = CType(params(1).Value, Int64)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
        Dim oEntities As New Parameter.P01
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BulanData = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToTXT(oEntities)
            dtViewData = oEntities.ListData
            Rc = oEntities.TotalRecord
            ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BulanData.Substring(0, oEntities.BulanData.Length - 2) & "." & oEntities.BulanData.Substring(4) & "." & "P01.01")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub
    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " Like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region

#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region
End Class