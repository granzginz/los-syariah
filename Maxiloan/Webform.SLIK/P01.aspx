﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="P01.aspx.vb" Inherits="Maxiloan.Webform.SLIK.P01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>


  


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Debitur Perseorangan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>    
    <script language="javascript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>	
</head>

<body>
     <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK P01 - PENJAMIN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>
                                <%--<asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>--%>   
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="15%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIF" SortExpression="CIF" HeaderText="CIF Debitur">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Nomor_Identitas_Penjamin" SortExpression="Nomor_Identitas_Penjamin" HeaderText="No Identitas">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Nama_Penjamin_Sesuai_Identitas" SortExpression="Nama_Penjamin_Sesuai_Identitas" HeaderText="Nama Penjamin">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Alamat_Penjamin" SortExpression="Alamat_Penjamin" HeaderText="Alamat">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Persentase_Fasilitas_yang_Dijamin" SortExpression="Persentase_Fasilitas_yang_Dijamin" HeaderText="Presentase (%)">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Kode_Kantor_Cabang" SortExpression="Kode_Kantor_Cabang" HeaderText="Kode Cabang">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Operasi_Data" SortExpression="Operasi_Data" HeaderText="Operasi Data">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                  <uc2:ucGridNav id="GridNaviP01SLIK" runat="server"/>                   
                </div>
            </div>
        </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
                </div>

                <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Nama_Penjamin_Sesuai_Identitas" Selected="True">Nama Penjamin</asp:ListItem>
                    <asp:ListItem Value="CIF">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="Nomor_Identitas_Penjamin">NIK Penjamin</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
     </asp:Panel>

<asp:Panel ID="pnlAdd" runat="server">
 <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>


        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" id="lblCIF">CIF/ID Penjamin</label>
                <asp:TextBox ID ="txtCIF" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled ="false"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Jenis Identitas Penjamin</label>
                <%--<asp:TextBox ID ="txtJenisIdentitas" runat ="server" Width ="1%" MaxLength ="100"
                    Columns ="105" enabled ="false"></asp:TextBox>--%>
                <asp:DropDownList ID="cboJenisIdentitas" runat="server">
                    <asp:ListItem Value="1">1 - Kartu Tanda Penduduk</asp:ListItem>
                    <asp:ListItem Value="2">2 - Paspor</asp:ListItem>
                    <asp:ListItem Value="3">3 - NPWP</asp:ListItem>
                    <asp:ListItem Value="9">9 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">No Identitas Penjamin</label>
                <asp:TextBox ID ="txtNIK" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled ="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNIK"
                    Display="Dynamic" ErrorMessage="No. Identitas harus diisi" CssClass="validator_general" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Nama Sesuai Identitas Penjamin</label>
                <asp:TextBox ID ="txtNamaIdentitas" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled ="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNamaIdentitas"
                    Display="Dynamic" ErrorMessage="Nama harus diisi" CssClass="validator_general" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label">Nama Lengkap Penjamin</label>
                <asp:TextBox ID ="txtNamaLengkap" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Jenis Segmen Fasilitas</label>
                <asp:TextBox ID ="txtKodeSegmen" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtKodeSegmen"
                    Display="Dynamic" ErrorMessage="Kode Segmen harus diisi" CssClass="validator_general" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Nomor Rekening Fasilitas</label>
                <asp:TextBox ID ="txtNomorRekening" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtNomorRekening"
                    Display="Dynamic" ErrorMessage="No. Rekening harus diisi" CssClass="validator_general" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Golongan Penjamin</label>
                <asp:DropDownList ID="cboKodeGolongan" runat="server"></asp:DropDownList>
                <%--<asp:TextBox ID ="txtKodeGolongan" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cboKodeGolongan"
                    Display="Dynamic" ErrorMessage="Kode Golongan harus diisi" CssClass="validator_general" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Alamat Penjamin</label>
                <asp:TextBox ID ="txtAlamat" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAlamat"
                    Display="Dynamic" ErrorMessage="Kode Golongan harus diisi" CssClass="validator_general" />
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label">Persentase Fasilitas Yang Dijamin</label>
                <asp:TextBox ID ="txtPresentaseFasilitas" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label">Keterangan</label>
                <asp:TextBox ID ="txtKeterangan" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Kode Kantor Cabang</label>
                <asp:TextBox ID ="txtKodeCabang" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled="false"></asp:TextBox>
            </div>
        </div>

               <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>


    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
          </asp:Panel>
</form>
</body>
</html>
