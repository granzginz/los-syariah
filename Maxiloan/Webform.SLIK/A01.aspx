﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="A01.aspx.vb" Inherits="Maxiloan.Webform.SLIK.A01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc5" TagName="uckodekota" Src="../Webform.UserController/uckodekota.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Debitur Perseorangan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script src="../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Data Yang Dihapus Hanya Akan Merubah Operasi Data Menjadi 'D' ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
 <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK A01 - AGUNAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btncetakheader" runat="server"  Text="Cetak Header A01" CssClass="small button green">
                    </asp:Button>               
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>
                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>                          
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIF" SortExpression="CIF" HeaderText="CIF">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NamaPemilikAgunan" SortExpression="NamaPemilikAgunan" HeaderText="NAMA PEMILIK AGUNAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NOAGUNAN" SortExpression="NOAGUNAN" HeaderText="NO AGUNAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KodeKantorCabang" SortExpression="KodeKantorCabang" HeaderText="KODE KANTOR CABANG">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OperasiData" SortExpression="OperasiData" HeaderText="Operasi Data">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                   <uc2:ucGridNav id="GridNav" runat="server"/>
                    </div>
                </div>
            </div>
            <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
        </div>
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="NamaIdentitas" Selected="True">Nama Debitur</asp:ListItem>
                    <asp:ListItem Value="CIF">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="NIK">NIK Debitur</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
     </asp:Panel>
     <asp:Panel ID="pnlAdd" runat="server">
        <asp:TextBox ID="ID" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <asp:TextBox ID="FlagDetail" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">No Agunan</label>
                <asp:TextBox ID ="NoAgunan" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="NoAgunan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">No Rek. Fasilitas</label>
                <asp:TextBox ID ="NoRekFasilitas" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                    ControlToValidate="NoRekFasilitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">CIF</label>
                <asp:TextBox ID ="CIF" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                    ControlToValidate="CIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Jenis Segmen</label>
                <asp:DropDownList ID="KodeJenisSegmen" runat="server">
                    <asp:ListItem Value="F01">F01 - Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="F02">F02 - Joint Account</asp:ListItem>
                    <asp:ListItem Value="F03">F03 - Surat Berharga</asp:ListItem>
                    <asp:ListItem Value="F04">F04 - Irrevocable L/C</asp:ListItem>
                    <asp:ListItem Value="F05">F05 - Garansi yang Diberikan</asp:ListItem>
                    <asp:ListItem Value="F06">F06 - Fasilitas Lain</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Status Agunan</label>
                <asp:DropDownList ID="KodeStatusAgunan" runat="server">
                    <asp:ListItem Value="1">1 - Tersedia</asp:ListItem>
                    <asp:ListItem Value="2">2 - Indent</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Jenis Agunan</label>
                <asp:DropDownList ID="KodeJenisAgunan" runat="server">
                    <asp:ListItem Value="010">010 - Giro</asp:ListItem>
                    <asp:ListItem Value="020">020 - Tabungan</asp:ListItem>
                    <asp:ListItem Value="041">041 - Simpanan Berjangka</asp:ListItem>
                    <asp:ListItem Value="042">042 - Sertifikat Bank Indonesia (SBI)</asp:ListItem>
                    <asp:ListItem Value="043">043 - Surat Perbendaharaan Negara (SPN)</asp:ListItem>
                    <asp:ListItem Value="042">042 - Sertifikat Deposito Bank Indonesia</asp:ListItem>
                    <asp:ListItem Value="043">043 - Surat  Berharga  Bank  Indonesia  dalam Valuta Asing (SBBI Valas)</asp:ListItem>
                    <asp:ListItem Value="044">044 - Surat Perbendaharaan Negara Syariah</asp:ListItem>
                    <asp:ListItem Value="045">045 - Setoran Jaminan</asp:ListItem>
                    <asp:ListItem Value="046">046 - Emas</asp:ListItem>
                    <asp:ListItem Value="085">085 - Surat Berharga Syariah Negara (SBSN)</asp:ListItem>
                    <asp:ListItem Value="086">086 - Obligasi Negara (ON)</asp:ListItem>
                    <asp:ListItem Value="087">087 - Obligasi Ritel Indonesia (ORI)</asp:ListItem>
                    <asp:ListItem Value="091">091 - Saham</asp:ListItem>
                    <asp:ListItem Value="081">081 - Reksadana</asp:ListItem>
                    <asp:ListItem Value="080">080 - Sukuk Lainnya</asp:ListItem>
                    <asp:ListItem Value="092">092 - Resi Gudang</asp:ListItem>
                    <asp:ListItem Value="099">099 - Surat Berharga Lainnya</asp:ListItem>
                    <asp:ListItem Value="161">161 - Gedung</asp:ListItem>
                    <asp:ListItem Value="162">162 - Gudang</asp:ListItem>
                    <asp:ListItem Value="163">163 - Rumah Toko atau Rumah Kantor atau Kios</asp:ListItem>
                    <asp:ListItem Value="164">164 - Hotel</asp:ListItem>
                    <asp:ListItem Value="175">175 - Properti Komersial Lain</asp:ListItem>
                    <asp:ListItem Value="176">176 - Rumah Tinggal</asp:ListItem>
                    <asp:ListItem Value="177">177 - Apartemen atau Rumah Susun</asp:ListItem>
                    <asp:ListItem Value="187">187 - Tanah</asp:ListItem>
                    <asp:ListItem Value="189">189 - Kendaraan Bermotor</asp:ListItem>
                    <asp:ListItem Value="190">190 - Mesin</asp:ListItem>
                    <asp:ListItem Value="191">191 - Pesawat Udara</asp:ListItem>
                    <asp:ListItem Value="192">192 - Kapal Laut atau Alat Transportasi Air</asp:ListItem>
                    <asp:ListItem Value="193">193 - Persediaan</asp:ListItem>
                    <asp:ListItem Value="250">250 - Agunan Lain</asp:ListItem>
                    <asp:ListItem Value="251">251 - Standby L/C (SBLC)</asp:ListItem>
                    <asp:ListItem Value="252">252 - Garansi</asp:ListItem>
                    <asp:ListItem Value="254">254 - Asuransi Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="275">275 - Jaminan Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box" id="hideprkt">
			<div class="form_single">
				<label>
                           Peringkat Agunan
                </label>
                <asp:TextBox ID="PeringkatAgunan" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator26" runat="server" display="Dynamic"
                    ControlToValidate="PeringkatAgunan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
    	<div class="form_box">
			<div class="form_single">
				<label>
                     Lembaga Pemeringkat
                </label>
                 <asp:DropDownList ID="KodelembagaPemeringkat" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text" onchange="change()"/>

                   <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Harap pilih Lembaga Pemeringkat"
                                ControlToValidate="KodelembagaPemeringkat" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />                  
			</div>
		</div>
        <div class ="form_box">
            <div class ="form_single">
                <label >Jenis Pengikatan</label>
                <asp:DropDownList ID="KodeJenisPengikatan" runat="server">
                    <asp:ListItem Value="01">01 - Hak Tanggungan</asp:ListItem>
                    <asp:ListItem Value="02">02 - Gadai</asp:ListItem>
                    <asp:ListItem Value="03">03 - Fidusia</asp:ListItem>
                    <asp:ListItem Value="04">04 - Surat Kuasa Membebankan Hak Tanggungan (SKMHT)</asp:ListItem>
                    <asp:ListItem Value="05">05 - Cessie</asp:ListItem>
                    <asp:ListItem Value="06">06 - Belum Diikat atau Tidak Diikat</asp:ListItem>
                    <asp:ListItem Value="07">07 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
		<div class="form_box" id="hidetglprkt">
			<div class="form_single">
				<label>
                      Tanggal Pengikatan
                </label>
                <asp:TextBox ID="TanggalPengikatan" runat="server" CssClass="medium_text" Width ="7%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                            TargetControlID="TanggalPengikatan" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>

                <asp:RequiredFieldValidator ID="Requiredfieldvalidator28" runat="server" display="Dynamic"
                    ControlToValidate="TanggalPengikatan" CssClass="validator_general" ErrorMessage="Harap Pilih Tanggal Pemeringkat" ></asp:RequiredFieldValidator>
			</div>
		</div>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                      Nama Pemilik Agunan
                </label>
                <asp:TextBox ID="NamaPemilikAgunan" runat="server" CssClass="medium_text"  Width ="20%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
			</div>
		</div>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                      Bukti Kepemilikan
                </label>
                <asp:TextBox ID="BuktiKepemilikan" runat="server" CssClass="medium_text"  Width ="20%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
			</div>
		</div>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                          Alamat
                </label>
                <asp:TextBox ID="AlamatAgunan" runat="server" CssClass="medium_text"  Width ="30%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" display="Dynamic"
                    ControlToValidate="AlamatAgunan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </div>
		</div>
    <asp:updatepanel runat="server">
        <ContentTemplate>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kabupaten/Kota
                </label>
                <asp:TextBox ID="KodeKabKota" runat="server" CssClass="medium_text"  Width ="8%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:HiddenField id="hdfkota" runat="server" />
			    <asp:Button ID="btnLookupKota" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                <uc5:UcKodeKota id="oKodeKota" runat="server" OnCatSelected="oKodeKota_CatSelected"></uc5:UcKodeKota>
                 <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" display="Dynamic"
                    ControlToValidate="KodeKabKota" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>           
            </div>
		</div>
        </ContentTemplate>
   </asp:updatepanel>

         <div class="form_box">
			<div class="form_single">
				<label >
                           Nilai Agunan Sesuai NJOP
                </label>
                <asp:TextBox ID="NilaiAgunanNJOP" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" display="Dynamic"
                    ControlToValidate="NilaiAgunanNJOP" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
         <div class="form_box">
			<div class="form_single">
				<label >
                           Nilai Agunan Menurut Pelapor
                </label>
                <asp:TextBox ID="NilaiAgunanPelapor" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" display="Dynamic"
                    ControlToValidate="NilaiAgunanPelapor" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box" id="">
			<div class="form_single">
				<label>
                      Tanggal Penilaian Agunan Pelapor
                </label>
                <asp:TextBox ID="TanggalPenilaianAgunanPelapor" runat="server" CssClass="medium_text" Width ="7%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                            TargetControlID="TanggalPenilaianAgunanPelapor" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>

                <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" display="Dynamic"
                    ControlToValidate="TanggalPenilaianAgunanPelapor" CssClass="validator_general" ErrorMessage="Harap Pilih Tanggal Pemeringkat" ></asp:RequiredFieldValidator>
			</div>
		</div>

       <div class="form_box">
			<div class="form_single">
				<label >
                           Nilai Agunan Menurut Penilaian Independen
                </label>
                <asp:TextBox ID="NilaiAgunanPenilaiIndipenden" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" display="Dynamic"
                    ControlToValidate="NilaiAgunanPenilaiIndipenden" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

        <div class="form_box">
			<div class="form_single">
				<label >
                           Nama Penilai Independen
                </label>
                <asp:TextBox ID="NamaPenilaiIndependen" runat="server" CssClass="medium_text"  Width ="20%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                    ControlToValidate="NamaPenilaiIndependen" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
		<div class="form_box">
			<div class="form_single">
				<label >
                           Tanggal Penilai Independen
                </label>

                <asp:TextBox ID="TanggalPenilaianIndependen" runat="server" CssClass="medium_text"  Width ="7%" MaxLength ="100" Columns ="105"></asp:TextBox>
                 <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="TanggalPenilaianIndependen" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" display="Dynamic" ControlToValidate="TanggalPenilaianIndependen" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Status Paripasu</label>
                <asp:DropDownList ID="StatusParipasu" runat="server">
                    <asp:ListItem Value="Y">Agunan Paripasu</asp:ListItem>
                    <asp:ListItem Value="T">Bukan Agunan Paripasu</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
			<div class="form_single">
				<label >
                           Presentase Paripasu
                </label>
                <asp:TextBox ID="PersentaseParipasu" runat="server" CssClass="medium_text"   Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" display="Dynamic"
                    ControlToValidate="PersentaseParipasu" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Status Agunan Paripasu</label>
                <asp:DropDownList ID="PembiayaanJointAccount" runat="server">
                    <asp:ListItem Value="Y">Agunan dari fasilitas Joint Account</asp:ListItem>
                    <asp:ListItem Value="T">Bukan Agunan dari fasilitas Joint Account</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Diasuransikan</label>
                <asp:DropDownList ID="Diasuransikan" runat="server">
                    <asp:ListItem Value="Y">Agunan Diasuransikan</asp:ListItem>
                    <asp:ListItem Value="T">Agunan Tidak Diasuransikan</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
		<div class="form_box">
			<div class="form_single">
				<label >
                        Keterangan
                </label>
                <asp:TextBox ID="Keterangan" runat="server" CssClass="medium_text"  Width ="40%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" display="Dynamic"
                    ControlToValidate="Keterangan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kantor Cabang
                </label>
                <asp:TextBox ID="KodeKantorCabang" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator30" runat="server" display="Dynamic"
                    ControlToValidate="KodeKantorCabang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

   <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
     </asp:Panel>
    </form>
</body>
</html>
