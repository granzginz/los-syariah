﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="D01.aspx.vb"
    Inherits="Maxiloan.Webform.SLIK.D01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%--<%@ Register TagPrefix="uc1" TagName="uczipcode" Src="../Webform.UserController/ucLookupZipCode.ascx" %>--%>

<%@ Register TagPrefix="uc2" TagName="ucsektorekonomi" Src="../Webform.UserController/UcSektorEkonomi.ascx" %>
<%@ Register TagPrefix="uc3" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>
<%@ Register Src="../webform.UserController/ucKodeKota.ascx" TagName="UcKodeKota" TagPrefix="uc5" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Debitur Perseorangan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script src="../js/jquery-2.1.1.js" type="text/javascript"></script>	
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <style type="text/css">
        .rightAlign {
            text-align: right
        }
    </style> 
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK D01 - DEBITUR PERSEORANGAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>                          
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIF" SortExpression="CIF" HeaderText="CIF">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NamaIdentitas" SortExpression="Nama" HeaderText="Nama Debitur">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NIK" SortExpression="NIK" HeaderText="NIK">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Alamat" SortExpression="Alamat" HeaderText="Alamat">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OperasiData" SortExpression="OperasiData" HeaderText="Operasi Data">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                   <uc2:ucGridNav id="GridNaviD01SLIK" runat="server"/>
                    </div>
                </div>
            </div>
            <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
        </div>
        
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="NamaIdentitas" Selected="True">Nama Debitur</asp:ListItem>
                    <asp:ListItem Value="CIF">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="NIK">NIK Debitur</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>

     </asp:Panel>

<asp:Panel ID="pnlAdd" runat="server">
 <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>           
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" id="lblCIF">CIF/ID Debitur</label>
                <asp:TextBox ID ="txtCIF" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Jenis Identitas</label>
                <asp:DropDownList ID="cboJenisIdentitas" runat="server">
                    <asp:ListItem Value="1">1 - Kartu Tanda Penduduk</asp:ListItem>
                    <asp:ListItem Value="2">2 - Paspor</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                    ControlToValidate="cboJenisIdentitas" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >NIK</label>
                <asp:TextBox ID ="txtNIK" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" display="Dynamic"
                    ControlToValidate="txtNIK" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Nama Sesuai Identitas</label>
                <asp:TextBox ID ="txtNamaIdentitas" runat ="server" Width ="20%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" display="Dynamic"
                    ControlToValidate="txtNamaIdentitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Nama Lengkap</label>
                <asp:TextBox ID ="txtNamaLengkap" runat ="server" Width ="20%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Status Pendidikan</label>
                <asp:DropDownList ID="cboPEducation" runat="server">
                    <asp:ListItem Value="00">00 - Tanpa Gelar</asp:ListItem> 
                    <asp:ListItem Value="01">01 - Diploma 1</asp:ListItem> 
                    <asp:ListItem Value="02">02 - Diploma 2</asp:ListItem> 
                    <asp:ListItem Value="03">03 - Diploma 3</asp:ListItem> 
                    <asp:ListItem Value="04">04 - S-1</asp:ListItem> 
                    <asp:ListItem Value="05">05 - S-2</asp:ListItem> 
                    <asp:ListItem Value="06">06 - S-3</asp:ListItem> 
                    <asp:ListItem Value="99">99 - Lainnya</asp:ListItem>        
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                    ControlToValidate="cboPEducation" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req">Jenis Kelamin</label>
                <asp:DropDownList ID="cboJenisKelamin" runat="server">
                    <asp:ListItem Value="L">Laki-laki</asp:ListItem>
                    <asp:ListItem Value="P">Perempuan</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                    ControlToValidate="cboJenisKelamin" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Tempat Lahir</label>
                <asp:TextBox ID ="txtTempatLahir" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                    ControlToValidate="txtTempatLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>                
            </div>
        </div>

        <div class ="form_box">
            <div>
            <div class ="form_single">
                <label class="label_req" >Tanggal Lahir</label>
                <asp:TextBox id="txtTanggalLahir" runat="server"  enabled ="true" Width ="5%"></asp:TextBox>
                <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True"
                            TargetControlID="txtTanggalLahir" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" display="Dynamic"
                    ControlToValidate="txtTempatLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>     
            </div>       
            </div>
        </div>
        

        <div class ="form_box">
            <div class ="form_single">
                <label>NPWP</label>
                <asp:TextBox ID ="txtNPWP" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Alamat</label>
                <asp:TextBox ID ="txtAlamat" runat ="server" Width ="30%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" display="Dynamic"
                    ControlToValidate="txtAlamat" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>  
            </div>
        </div>

            <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kelurahan</label>
                <asp:TextBox ID ="txtKelurahan" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" display="Dynamic"
                    ControlToValidate="txtKelurahan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>

            <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kecamatan</label>
                <asp:TextBox ID ="txtKecamatan" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" display="Dynamic"
                    ControlToValidate="txtKecamatan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>

        <asp:updatepanel runat="server">     
           <ContentTemplate> 
            <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kota</label>
                <asp:TextBox ID ="txtKota" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:HiddenField ID ="hdfKota" runat ="server" />
                <asp:Button ID="btnLookup2" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                <uc5:UcKodeKota id="oKodeKota" runat="server" OnCatSelected="oKodeKota_CatSelected"></uc5:UcKodeKota>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" display="Dynamic"
                    ControlToValidate="txtKota" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
            </div>
           </ContentTemplate>
        </asp:updatepanel>

            <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Pos</label>
                <asp:TextBox ID ="txtKodePos" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" display="Dynamic"
                    ControlToValidate="txtKodePos" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>               

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Nomor Telepon</label>
                <asp:TextBox ID ="txtTelp" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" display="Dynamic"
                    ControlToValidate="txtTelp" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label>Nomor HP</label>
                <asp:TextBox ID ="txtHP" runat ="server" Width ="7%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label>E-Mail</label>
                <asp:TextBox ID ="txtEmail" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Negara Domisili</label>
                <asp:TextBox ID ="txtKodeNegara" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeNegara" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Pekerjaan</label>
                <asp:DropDownList ID="cboKodePekerjaan" runat="server"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator26" runat="server" display="Dynamic"
                    ControlToValidate="cboKodePekerjaan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator> 
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Tempat Bekerja</label>
                <asp:TextBox ID ="txtTempatBekerja" runat ="server" Width ="25%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator17" runat="server" display="Dynamic"
                    ControlToValidate="txtTempatBekerja" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <asp:updatepanel runat="server">     
           <ContentTemplate> 
        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Sektor Ekonomi</label>
                <asp:TextBox ID ="txtKodeBidangUsaha" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:Button ID="btnLookup" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                <uc2:ucsektorekonomi id="oSektorEkonomi" runat="server" oncatselected="CatSelectedSektorEkonomi"></uc2:ucsektorekonomi> 
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator18" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeBidangUsaha" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>            
        </div>
            </ContentTemplate>
        </asp:updatepanel>

        <div class ="form_box">
            <div class ="form_single">
                <label>Alamat Tempat Bekerja</label>
                <asp:TextBox ID ="txtAlamatTempatBekerja" runat ="server" Width ="25%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Penghasilan Bruto per-Tahun</label>
                <asp:TextBox ID ="txtPenghasilanPertahun" runat ="server" Width ="10%" MaxLength ="100"
                    CssClass="rightAlign" Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Sumber Penghasilan</label>
                <asp:DropDownList ID="cboKodeSumberPenghasilan" runat="server">
                    <asp:ListItem Value="1">1 - Gaji</asp:ListItem>
                    <asp:ListItem Value="2">2 - Usaha</asp:ListItem>
                    <asp:ListItem Value="3">3 - Lainnya</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Tanggungan</label>
                <asp:TextBox ID ="txtJumlahTanggungan" runat ="server" Width ="2%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Hubungan dengan Pelapor</label>
                <asp:DropDownList ID="cboKodeHubunganLJK" runat="server"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" display="Dynamic"
                    ControlToValidate="cboKodeHubunganLJK" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Golongan Debitur</label>
                <asp:DropDownList ID="cboKodeGolonganDebitur" runat="server">
                    <asp:ListItem Value="9000">Perseorangan (Penduduk)</asp:ListItem>
                    <asp:ListItem Value="9700">Perseorangan (Bukan Penduduk)</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator20" runat="server" display="Dynamic"
                    ControlToValidate="cboKodeGolonganDebitur" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Status Perkawinan</label>
                <asp:DropDownList ID="cboStatusPerkawinan" runat="server" onchange="change()">
                    <asp:ListItem Value="1">Kawin</asp:ListItem>
                    <asp:ListItem Value="2">Belum Kawin</asp:ListItem>
                    <asp:ListItem Value="3">Cerai</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box" id="hdNIKpasangan">
            <div class ="form_single">
                <label>Nomor Identitas Pasangan</label>
                <asp:TextBox ID ="txtNikPaspor" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" display="Dynamic"
                    ControlToValidate="txtNikPaspor" CssClass="validator_general" ErrorMessage="Harap Isi Nomor Identitas Pasangan!" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box" id="hdnNMpasangan">
            <div class ="form_single">
                <label>Nama Pasangan</label>
                <asp:TextBox ID ="txtNamaPasangan" runat ="server" Width ="20%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                    ControlToValidate="txtNamaPasangan" CssClass="validator_general" ErrorMessage="Harap Isi Nama Pasangan!" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box" id="hdnTGLHRpasangan">
            <div class ="form_single">
                <label>Tanggal Lahir Pasangan</label>
                <asp:TextBox ID ="txtTanggalLahirPasangan" runat ="server" Width ="5%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                            TargetControlID="txtTanggalLahirPasangan" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                    ControlToValidate="txtTanggalLahirPasangan" CssClass="validator_general" ErrorMessage="Harap Isi Tanggal Lahir Pasangan!" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_general">Perjanjian Pisah Harta</label>
                <asp:RadioButtonList ID="rbPerjanjianPisahHarta" runat="server" class="opt_single"  RepeatDirection="Horizontal">
                <asp:ListItem Value="Y">Ya</asp:ListItem>
                <asp:ListItem Value="T" Selected="True">Tidak</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Melanggar BMPK/BMPD/BMPP</label>
                <asp:RadioButtonList ID="rbMelanggarBMPK" runat="server" class="opt_single"  RepeatDirection="Horizontal">
                <asp:ListItem Value="Y">Ya</asp:ListItem>
                <asp:ListItem Value="T" Selected="True">Tidak</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator27" runat="server" display="Dynamic"
                    ControlToValidate="rbMelanggarBMPK" CssClass="validator_general" ErrorMessage="Harap Isi Tanggal Lahir Pasangan!" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Melampaui BMPK/BMPD/BMPP</label>
                <asp:RadioButtonList ID="rbMelampauiBMPK" runat="server" class="opt_single"  RepeatDirection="Horizontal">
                <asp:ListItem Value="Y">Ya</asp:ListItem>
                <asp:ListItem Value="T" Selected="true">Tidak</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator28" runat="server" display="Dynamic"
                    ControlToValidate="rbMelampauiBMPK" CssClass="validator_general" ErrorMessage="Harap Isi Tanggal Lahir Pasangan!" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Nama Gadis Ibu Kandung</label>
                <asp:TextBox ID ="txtNamaGadisIbuKandung" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator21" runat="server" display="Dynamic"
                    ControlToValidate="txtNamaGadisIbuKandung" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label class="label_req" >Kode Kantor Cabang</label>
                <asp:TextBox ID ="txtKodeKantorCabang" runat ="server" Width ="3%" MaxLength ="3" readonly ="true"
                    Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator22" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeKantorCabang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
            </div>
        </div>

            <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>

            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
          </asp:Panel>
    </form>
  <script type="text/javascript">
        $(document).ready(function () {
            change();
        });

        function change() {
            var DropdownList = document.getElementById('<%=cboStatusPerkawinan.ClientID %>');
            var SelectedIndex = DropdownList.selectedIndex;
            var x = document.getElementById('hdNIKpasangan');
            var y = document.getElementById('hdnNMpasangan');
            var z = document.getElementById('hdnTGLHRpasangan');
            var valx = document.getElementById('<%=Requiredfieldvalidator4.ClientID%>');
            var valy = document.getElementById('<%=Requiredfieldvalidator2.ClientID%>');
            var valz = document.getElementById('<%=Requiredfieldvalidator3.ClientID%>');


            if (SelectedIndex == 0) {
                x.style.display = "block";
                y.style.display = "block";
                z.style.display = "block";
                valx.enabled = true;
                valy.enabled = true;
                valz.enabled = true;
            }
            else {
                x.style.display = "none";
                y.style.display = "none";
                z.style.display = "none";
                valx.enabled = false;
                valy.enabled = false;
                valz.enabled = false;
                document.getElementById('txtNikPaspor').value = '';
                document.getElementById('txtNamaPasangan').value = '';
                document.getElementById('txtTanggalLahirPasangan').value = '';
            }
        }
    </script>  
</body>
</html>
