﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class S01
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New S01Controller
    Private oCustomclass As New Parameter.S01
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Protected WithEvents GridNaviS01SLIK As ucGridNav
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviS01SLIK.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
		If Not IsPostBack Then
			Me.FormID = "S01"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
				clean()
			End If
		End If
		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub

    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub clean()
        txtbulandata.Text = " "
        txtNo_Rek_Fasilitas.Text = " "
        txtCIF.Text = " "
        txtJumlahHariTunggakan1.Text = "0"
        txtJumlahHariTunggakan2.Text = "0"
        txtJumlahHariTunggakan3.Text = "0"
        txtJumlahHariTunggakan4.Text = "0"
        txtJumlahHariTunggakan5.Text = "0"
        txtJumlahHariTunggakan6.Text = "0"
        txtJumlahHariTunggakan7.Text = "0"
        txtJumlahHariTunggakan8.Text = "0"
        txtJumlahHariTunggakan9.Text = "0"
        txtJumlahHariTunggakan10.Text = "0"
        txtJumlahHariTunggakan11.Text = "0"
		txtJumlahHariTunggakan12.Text = "0"
		txtSearch.Text = ""
	End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.S01

        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
    End Sub
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "S01", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            txtbulandata.Enabled = True
			txtCIF.ReadOnly = False

			Me.Process = "ADD"
            clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviS01SLIK.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.S01

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetS01(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If

        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()


        If (isFrNav = False) Then
            GridNaviS01SLIK.Initialize(recordCount, pageSize)
        End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.S01
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "S01", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetS01Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If


                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboFasilitas1.SelectedIndex = cboFasilitas1.Items.IndexOf(cboFasilitas1.Items.FindByValue(oRow("KODE_JENIS_SEGMEN").ToString.Trim))
                    cboKodeKualitas1.SelectedIndex = cboKodeKualitas1.Items.IndexOf(cboKodeKualitas1.Items.FindByValue(oRow("KODE_KUALITAS_1").ToString.Trim))
                    cboKodeKualitas2.SelectedIndex = cboKodeKualitas2.Items.IndexOf(cboKodeKualitas2.Items.FindByValue(oRow("KODE_KUALITAS_2").ToString.Trim))
                    cboKodeKualitas3.SelectedIndex = cboKodeKualitas3.Items.IndexOf(cboKodeKualitas3.Items.FindByValue(oRow("KODE_KUALITAS_3").ToString.Trim))
                    cboKodeKualitas4.SelectedIndex = cboKodeKualitas4.Items.IndexOf(cboKodeKualitas4.Items.FindByValue(oRow("KODE_KUALITAS_4").ToString.Trim))
                    cboKodeKualitas5.SelectedIndex = cboKodeKualitas5.Items.IndexOf(cboKodeKualitas5.Items.FindByValue(oRow("KODE_KUALITAS_5").ToString.Trim))
                    cboKodeKualitas6.SelectedIndex = cboKodeKualitas6.Items.IndexOf(cboKodeKualitas6.Items.FindByValue(oRow("KODE_KUALITAS_6").ToString.Trim))
                    cboKodeKualitas7.SelectedIndex = cboKodeKualitas7.Items.IndexOf(cboKodeKualitas7.Items.FindByValue(oRow("KODE_KUALITAS_7").ToString.Trim))
                    cboKodeKualitas8.SelectedIndex = cboKodeKualitas8.Items.IndexOf(cboKodeKualitas8.Items.FindByValue(oRow("KODE_KUALITAS_8").ToString.Trim))
                    cboKodeKualitas9.SelectedIndex = cboKodeKualitas9.Items.IndexOf(cboKodeKualitas9.Items.FindByValue(oRow("KODE_KUALITAS_9").ToString.Trim))
                    cboKodeKualitas10.SelectedIndex = cboKodeKualitas10.Items.IndexOf(cboKodeKualitas10.Items.FindByValue(oRow("KODE_KUALITAS_10").ToString.Trim))
                    cboKodeKualitas11.SelectedIndex = cboKodeKualitas11.Items.IndexOf(cboKodeKualitas11.Items.FindByValue(oRow("KODE_KUALITAS_11").ToString.Trim))
                    cboKodeKualitas12.SelectedIndex = cboKodeKualitas12.Items.IndexOf(cboKodeKualitas12.Items.FindByValue(oRow("KODE_KUALITAS_12").ToString.Trim))
                End If

                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtCIF.Text = oParameter.ListData.Rows(0)("CIF")
                txtNo_Rek_Fasilitas.Text = oParameter.ListData.Rows(0)("No_Rek_Fasilitas")
                txtJumlahHariTunggakan1.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_1")
                txtJumlahHariTunggakan2.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_2")
                txtJumlahHariTunggakan3.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_3")
                txtJumlahHariTunggakan4.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_4")
                txtJumlahHariTunggakan5.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_5")
                txtJumlahHariTunggakan6.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_6")
                txtJumlahHariTunggakan7.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_7")
                txtJumlahHariTunggakan8.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_8")
                txtJumlahHariTunggakan9.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_9")
                txtJumlahHariTunggakan10.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_10")
                txtJumlahHariTunggakan11.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_11")
                txtJumlahHariTunggakan12.Text = oParameter.ListData.Rows(0)("Jml_Hari_Tunggakan_12")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "S01", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.S01
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.GetS01Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.S01
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter

                .FLAG = "D"
                .BULANDATA = txtbulandata.Text
                .NO_REK_FASILITAS = txtNo_Rek_Fasilitas.Text
                .CIF = txtCIF.Text
                .KODE_JENIS_SEGMEN = cboFasilitas1.SelectedValue
                .KODE_KUALITAS_1 = cboKodeKualitas1.SelectedValue
                .KODE_KUALITAS_2 = cboKodeKualitas2.SelectedValue
                .KODE_KUALITAS_3 = cboKodeKualitas3.SelectedValue
                .KODE_KUALITAS_4 = cboKodeKualitas4.SelectedValue
                .KODE_KUALITAS_5 = cboKodeKualitas5.SelectedValue
                .KODE_KUALITAS_6 = cboKodeKualitas6.SelectedValue
                .KODE_KUALITAS_7 = cboKodeKualitas7.SelectedValue
                .KODE_KUALITAS_8 = cboKodeKualitas8.SelectedValue
                .KODE_KUALITAS_9 = cboKodeKualitas9.SelectedValue
                .KODE_KUALITAS_10 = cboKodeKualitas10.SelectedValue
                .KODE_KUALITAS_11 = cboKodeKualitas11.SelectedValue
                .KODE_KUALITAS_12 = cboKodeKualitas12.SelectedValue
                .JML_HARI_TUNGGAKAN_1 = txtJumlahHariTunggakan1.Text
                .JML_HARI_TUNGGAKAN_2 = txtJumlahHariTunggakan2.Text
                .JML_HARI_TUNGGAKAN_3 = txtJumlahHariTunggakan3.Text
                .JML_HARI_TUNGGAKAN_4 = txtJumlahHariTunggakan4.Text
                .JML_HARI_TUNGGAKAN_5 = txtJumlahHariTunggakan5.Text
                .JML_HARI_TUNGGAKAN_6 = txtJumlahHariTunggakan6.Text
                .JML_HARI_TUNGGAKAN_7 = txtJumlahHariTunggakan7.Text
                .JML_HARI_TUNGGAKAN_8 = txtJumlahHariTunggakan8.Text
                .JML_HARI_TUNGGAKAN_9 = txtJumlahHariTunggakan9.Text
                .JML_HARI_TUNGGAKAN_10 = txtJumlahHariTunggakan10.Text
                .JML_HARI_TUNGGAKAN_11 = txtJumlahHariTunggakan11.Text
                .JML_HARI_TUNGGAKAN_12 = txtJumlahHariTunggakan12.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetS01Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter

                .ID = txtid.Text
                .FLAG = "D"
                .BULANDATA = txtbulandata.Text
                .NO_REK_FASILITAS = txtNo_Rek_Fasilitas.Text
                .CIF = txtCIF.Text
                .KODE_JENIS_SEGMEN = cboFasilitas1.SelectedValue
                .KODE_KUALITAS_1 = cboKodeKualitas1.SelectedValue
                .KODE_KUALITAS_2 = cboKodeKualitas2.SelectedValue
                .KODE_KUALITAS_3 = cboKodeKualitas3.SelectedValue
                .KODE_KUALITAS_4 = cboKodeKualitas4.SelectedValue
                .KODE_KUALITAS_5 = cboKodeKualitas5.SelectedValue
                .KODE_KUALITAS_6 = cboKodeKualitas6.SelectedValue
                .KODE_KUALITAS_7 = cboKodeKualitas7.SelectedValue
                .KODE_KUALITAS_8 = cboKodeKualitas8.SelectedValue
                .KODE_KUALITAS_9 = cboKodeKualitas9.SelectedValue
                .KODE_KUALITAS_10 = cboKodeKualitas10.SelectedValue
                .KODE_KUALITAS_11 = cboKodeKualitas11.SelectedValue
                .KODE_KUALITAS_12 = cboKodeKualitas12.SelectedValue
                .JML_HARI_TUNGGAKAN_1 = txtJumlahHariTunggakan1.Text
                .JML_HARI_TUNGGAKAN_2 = txtJumlahHariTunggakan2.Text
                .JML_HARI_TUNGGAKAN_3 = txtJumlahHariTunggakan3.Text
                .JML_HARI_TUNGGAKAN_4 = txtJumlahHariTunggakan4.Text
                .JML_HARI_TUNGGAKAN_5 = txtJumlahHariTunggakan5.Text
                .JML_HARI_TUNGGAKAN_6 = txtJumlahHariTunggakan6.Text
                .JML_HARI_TUNGGAKAN_7 = txtJumlahHariTunggakan7.Text
                .JML_HARI_TUNGGAKAN_8 = txtJumlahHariTunggakan8.Text
                .JML_HARI_TUNGGAKAN_9 = txtJumlahHariTunggakan9.Text
                .JML_HARI_TUNGGAKAN_10 = txtJumlahHariTunggakan10.Text
                .JML_HARI_TUNGGAKAN_11 = txtJumlahHariTunggakan11.Text
                .JML_HARI_TUNGGAKAN_12 = txtJumlahHariTunggakan12.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetS01Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region
#Region "Generate Data"
    Public Function GetCopy(ByVal customclass As Parameter.S01) As Parameter.S01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKS01Generate", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Generate Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.S01
        Dim dtViewData As New DataTable
        With oEntities
            .BulanData = txtcopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
        Dim oParameter As New Maxiloan.Parameter.S01

        Dim ResClient As New StringBuilder
        Using sw As New StringWriter(ResClient)

            Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "S01" & "|" & Rc & "|" & Rc)
            sw.WriteLine(Header)
            For Each drow As DataRow In dtdata.Rows
                Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
                sw.WriteLine(lineoftext)
            Next

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
            Response.Write(ResClient.ToString())

            Using MyMemoryStream As New MemoryStream()
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using

        End Using
    End Sub

    Public Function GetToTXT(ByVal customclass As Parameter.S01) As Parameter.S01
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA
            params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
            params(1).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKS01Txt", params).Tables(0)
            customclass.TotalRecord = CType(params(1).Value, Int64)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
        Dim oEntities As New Parameter.S01
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToTXT(oEntities)
            dtViewData = oEntities.ListData
            Rc = oEntities.TotalRecord
            ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BULANDATA.Substring(0, oEntities.BULANDATA.Length - 2) & "." & oEntities.BULANDATA.Substring(4) & "." & "S01.01")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region

#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region
End Class