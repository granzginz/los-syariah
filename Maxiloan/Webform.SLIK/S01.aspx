﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="S01.aspx.vb" Inherits="Maxiloan.Webform.SLIK.S01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title>Fasilitas Lain</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK S01 - SUMMARY FASILITAS
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>
                                <%--<asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>--%>   
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="5%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIF" SortExpression="CIF" HeaderText="CIF">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NO_REK_FASILITAS" SortExpression="NO_REK_FASILITAS" HeaderText="No Rek Fasilitas">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KODE_JENIS_SEGMEN" SortExpression="KODE_JENIS_SEGMEN" HeaderText="kd jns segmen">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_1" SortExpression="JML_HARI_TUNGGAKAN_1" HeaderText="Tunggakan 1"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_2" SortExpression="JML_HARI_TUNGGAKAN_2" HeaderText="Tunggakan 2"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_3" SortExpression="JML_HARI_TUNGGAKAN_3" HeaderText="Tunggakan 3"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_4" SortExpression="JML_HARI_TUNGGAKAN_4" HeaderText="Tunggakan 4"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_5" SortExpression="JML_HARI_TUNGGAKAN_5" HeaderText="Tunggakan 5"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_6" SortExpression="JML_HARI_TUNGGAKAN_6" HeaderText="Tunggakan 6"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_7" SortExpression="JML_HARI_TUNGGAKAN_7" HeaderText="Tunggakan 7"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_8" SortExpression="JML_HARI_TUNGGAKAN_8" HeaderText="Tunggakan 8"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_9" SortExpression="JML_HARI_TUNGGAKAN_9" HeaderText="Tunggakan 9"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_10" SortExpression="JML_HARI_TUNGGAKAN_10" HeaderText="Tunggakan 10"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_11" SortExpression="JML_HARI_TUNGGAKAN_11" HeaderText="Tunggakan 11"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JML_HARI_TUNGGAKAN_12" SortExpression="JML_HARI_TUNGGAKAN_12" HeaderText="Tunggakan 12"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                        <uc2:ucGridNav id="GridNaviS01SLIK" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
                </div>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="CIF">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="NO_REK_FASILITAS">NoRek Fasilitas/No Agreement</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>          

 <asp:Panel ID="pnlAdd" runat="server">

 <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>
        
       <div class ="form_box">
            <div class ="form_single">
                <label >No Rekening Fasilitas</label>
                <asp:TextBox ID ="txtNo_Rek_Fasilitas" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >No Cif Debitur</label>
                <asp:TextBox ID ="txtCIF" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" enabled ="true"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
            <label >Kode Jenis Segmen Fasilitas</label>
               <asp:DropDownList ID="cboFasilitas1" runat="server">
                    <asp:ListItem Value="F01">F01 - Pembiayaan</asp:ListItem> 
                    <asp:ListItem Value="F02">F02 - Pembiayaan Joint Account</asp:ListItem> 
                    <asp:ListItem Value="F03">F03 - Surat Berharga</asp:ListItem> 
                    <asp:ListItem Value="F04">F04 - Irrevocable L/C</asp:ListItem> 
                    <asp:ListItem Value="F05">F05 - Garansi Yang Diberikan</asp:ListItem> 
                    <asp:ListItem Value="F06">F06 - Fasilitas Lain</asp:ListItem>       
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 1</label>
                <asp:DropDownList ID="cboKodeKualitas1" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 1</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan1" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 2</label>
                <asp:DropDownList ID="cboKodeKualitas2" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 2</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan2" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 3</label>
                <asp:DropDownList ID="cboKodeKualitas3" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 3</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan3" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 4</label>
                <asp:DropDownList ID="cboKodeKualitas4" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 4</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan4" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 5</label>
                <asp:DropDownList ID="cboKodeKualitas5" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 5</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan5" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 6</label>
                <asp:DropDownList ID="cboKodeKualitas6" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 6</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan6" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 7</label>
                <asp:DropDownList ID="cboKodeKualitas7" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 7</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan7" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 8</label>
                <asp:DropDownList ID="cboKodeKualitas8" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 8</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan8" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 9</label>
                <asp:DropDownList ID="cboKodeKualitas9" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 9</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan9" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 10</label>
                <asp:DropDownList ID="cboKodeKualitas10" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 10</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan10" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 11</label>
                <asp:DropDownList ID="cboKodeKualitas11" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 11</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan11" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Kode Kualitas 12</label>
                <asp:DropDownList ID="cboKodeKualitas12" runat="server">
                    <asp:ListItem Value="1">1 - Lancar</asp:ListItem>
                    <asp:ListItem Value="2">2 - Dalam Perhatian Khusus</asp:ListItem>
                    <asp:ListItem Value="3">3 - Kurang Lancar</asp:ListItem>
                    <asp:ListItem Value="4">4 - Diragukan</asp:ListItem>
                    <asp:ListItem Value="5">5 - Macet</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class ="form_box">
            <div class ="form_single">
                <label >Jumlah Hari Tunggakan 12</label>
                <asp:TextBox ID ="txtJumlahHariTunggakan12" runat ="server" Width ="3%" MaxLength ="100"
                    Columns ="105"></asp:TextBox>
            </div>
        </div>
                <%--<asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>--%>


    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
          </asp:Panel>
  </form>
</body>
</html>
