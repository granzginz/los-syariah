﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPAssetDocViewer.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.SPAssetDocViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692FBEA5521E1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ImageButton ID="imbBack" Style="z-index: 102; left: 16px; position: absolute;
            top: 48px" runat="server" ImageUrl="../../images/ButtonBack.gif"></asp:ImageButton>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
            HasExportButton="False" ToolPanelView="None" />
    </div>
    </form>
</body>
</html>
