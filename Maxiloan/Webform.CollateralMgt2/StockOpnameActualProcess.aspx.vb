﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.IO.Ports
#End Region

Public Class StockOpnameActualProcess
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Private oCustomclass As New Parameter.DocRec
    Private pCustomclass As New Parameter.StockOpDoc
    Private oController As New StockOpnameDocController

#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property TableStockOpnameAct() As DataTable
        Get
            Return (CType(ViewState("TableStockOpnameAct"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TableStockOpnameAct") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private totalPages1 As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
#End Region
#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If

            If Me.TableStockOpnameAct Is Nothing Then
                Me.TableStockOpnameAct = GetStockOpnameAct()
            End If
        Else
            If txtApplicationId.Text.Trim <> "" Then
                If CheckDuplikasiApplicationID(txtApplicationId.Text.Trim) Then
                    SaveDataGridToDataTable()
                    Dim ds As New DataTable
                    With oCustomclass
                        .strConnection = GetConnectionString()
                        .ApplicationId = txtApplicationId.Text.Trim
                    End With
                    ds = oController.DocInformation(oCustomclass)
                    GenerateTable(ds)
                    DtgStockOpname.DataSource = Me.TableStockOpnameAct.DefaultView
                    DtgStockOpname.DataBind()
                End If
            End If
        End If
        txtApplicationId.Focus()
        txtApplicationId.Text = ""
    End Sub
#End Region

#Region "Stock Opname"
    Private Sub DtgStockOpname_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgStockOpname.ItemCommand
        If e.CommandName = "DELETE" Then
            Me.TableStockOpnameAct.Rows.RemoveAt(CInt(e.Item.ItemIndex()))
            DtgStockOpname.DataSource = Me.TableStockOpnameAct.DefaultView
            DtgStockOpname.DataBind()
        End If
    End Sub

    Private Sub btnAddStockOpname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStockOpname.Click
        If Me.TableStockOpnameAct Is Nothing Then
            Me.TableStockOpnameAct = GetStockOpnameAct()
        End If
        If txtApplicationId.Text.Trim <> "" Then
            If CheckDuplikasiApplicationID(txtApplicationId.Text.Trim) Then
                'SaveDataGridToDataTable()
                Dim ds As New DataTable
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ApplicationId = txtApplicationId.Text.Trim
                End With
                ds = oController.DocInformation(oCustomclass)
                GenerateTable(ds)
                DtgStockOpname.DataSource = Me.TableStockOpnameAct.DefaultView
                DtgStockOpname.DataBind()
                txtApplicationId.Focus()
                txtApplicationId.Text = ""
            End If
        End If
    End Sub

    Private Function GetStockOpnameAct() As DataTable
        Dim lObjDataTable As New DataTable("TableStockOpnameAct")
        With lObjDataTable
            .Columns.Add("BranchOnStatus", System.Type.GetType("System.String"))
            .Columns.Add("BranchID", System.Type.GetType("System.String"))
            .Columns.Add("ApplicationID", System.Type.GetType("System.String"))
            .Columns.Add("AssetTypeId", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocID", System.Type.GetType("System.String"))
            .Columns.Add("DocumentNo", System.Type.GetType("System.String"))
            .Columns.Add("DocumentDate", System.Type.GetType("System.String"))
            .Columns.Add("LicensePlate", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocStatus", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocRack", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocFilling", System.Type.GetType("System.String"))
            .Columns.Add("OwnerAsset", System.Type.GetType("System.String"))
            .Columns.Add("OwnerAddress", System.Type.GetType("System.String"))
            .Columns.Add("OwnerCity", System.Type.GetType("System.String"))
            .Columns.Add("OwnerZipCode", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function

    Private Sub GenerateTable(ByVal dtAgreement As DataTable)
        Dim lObjDataRow As DataRow
        Dim i As Integer
        For i = 0 To dtAgreement.Rows.Count - 1
            lObjDataRow = Me.TableStockOpnameAct.NewRow()
            If CheckDuplikasiApplicationID(dtAgreement.Rows(i).Item("ApplicationID").ToString) Then
                lObjDataRow("BranchOnStatus") = dtAgreement.Rows(i).Item("BranchOnStatus")
                lObjDataRow("BranchID") = dtAgreement.Rows(i).Item("BranchID")
                lObjDataRow("ApplicationID") = dtAgreement.Rows(i).Item("ApplicationID")
                lObjDataRow("AssetTypeId") = dtAgreement.Rows(i).Item("AssetTypeId")
                lObjDataRow("AssetDocID") = dtAgreement.Rows(i).Item("AssetDocID")
                lObjDataRow("DocumentNo") = dtAgreement.Rows(i).Item("DocumentNo")
                lObjDataRow("DocumentDate") = dtAgreement.Rows(i).Item("DocumentDate")
                lObjDataRow("LicensePlate") = dtAgreement.Rows(i).Item("LicensePlate")
                lObjDataRow("AssetDocStatus") = dtAgreement.Rows(i).Item("AssetDocStatus")
                lObjDataRow("AssetDocRack") = dtAgreement.Rows(i).Item("AssetDocRack")
                lObjDataRow("AssetDocFilling") = dtAgreement.Rows(i).Item("AssetDocFilling")
                lObjDataRow("OwnerAsset") = dtAgreement.Rows(i).Item("OwnerAsset")
                lObjDataRow("OwnerAddress") = dtAgreement.Rows(i).Item("OwnerAddress")
                lObjDataRow("OwnerCity") = dtAgreement.Rows(i).Item("OwnerCity")
                lObjDataRow("OwnerZipCode") = dtAgreement.Rows(i).Item("OwnerZipCode")
                Me.TableStockOpnameAct.Rows.Add(lObjDataRow)
            End If
        Next i
        AutoSortGrid()
    End Sub

    Sub AutoSortGrid()
        TableStockOpnameAct.DefaultView.Sort = "ApplicationID"
        TableStockOpnameAct = TableStockOpnameAct.DefaultView.ToTable()
    End Sub

    Private Sub SaveDataGridToDataTable()
        Dim lObjDataRow As DataRow
        Dim i As Integer
        Dim lblBranchOnStatus As Label
        Dim lblBranchID As Label
        Dim lblApplicationID As Label
        Dim lblAssetDoc As Label
        Dim lblAssetTypeId As Label
        Dim lblDocumentNo As Label
        Dim lblDocumentDate As Label
        Dim lblLicensePlate As Label
        Dim lblAssetDocStatus As Label
        Dim lblAssetDocRack As Label
        Dim lblAssetDocFilling As Label
        Dim lblOwnerAsset As Label
        Dim lblOwnerAddress As Label
        Dim lblOwnerCity As Label
        Dim lblOwnerZipCode As Label

        Me.TableStockOpnameAct.Clear()
        For i = 0 To DtgStockOpname.Items.Count - 1
            lObjDataRow = Me.TableStockOpnameAct.NewRow()
            lblBranchOnStatus = CType(DtgStockOpname.Items(i).FindControl("lblBranchOnStatus"), Label)
            lblBranchID = CType(DtgStockOpname.Items(i).FindControl("lblBranchID"), Label)
            lblApplicationID = CType(DtgStockOpname.Items(i).FindControl("lblApplicationID"), Label)
            lblAssetTypeId = CType(DtgStockOpname.Items(i).FindControl("lblAssetTypeId"), Label)
            lblAssetDoc = CType(DtgStockOpname.Items(i).FindControl("lblAssetDoc"), Label)
            lblDocumentNo = CType(DtgStockOpname.Items(i).FindControl("lblDocumentNo"), Label)
            lblDocumentDate = CType(DtgStockOpname.Items(i).FindControl("lblDocumentDate"), Label)
            lblLicensePlate = CType(DtgStockOpname.Items(i).FindControl("lblLicensePlate"), Label)
            lblAssetDocStatus = CType(DtgStockOpname.Items(i).FindControl("lblAssetDocStatus"), Label)
            lblAssetDocRack = CType(DtgStockOpname.Items(i).FindControl("lblAssetDocRack"), Label)
            lblAssetDocFilling = CType(DtgStockOpname.Items(i).FindControl("lblAssetDocFilling"), Label)
            lblOwnerAsset = CType(DtgStockOpname.Items(i).FindControl("lblOwnerAsset"), Label)
            lblOwnerAddress = CType(DtgStockOpname.Items(i).FindControl("lblOwnerAddress"), Label)
            lblOwnerCity = CType(DtgStockOpname.Items(i).FindControl("lblOwnerCity"), Label)
            lblOwnerZipCode = CType(DtgStockOpname.Items(i).FindControl("lblOwnerZipCode"), Label)

            lObjDataRow("BranchOnStatus") = lblBranchOnStatus.Text
            lObjDataRow("BranchID") = lblBranchID.Text
            lObjDataRow("ApplicationID") = lblApplicationID.Text
            lObjDataRow("AssetTypeId") = lblAssetTypeId.Text
            lObjDataRow("AssetDocID") = lblAssetDoc.Text
            lObjDataRow("DocumentNo") = lblDocumentNo.Text
            lObjDataRow("DocumentDate") = lblDocumentDate.Text
            lObjDataRow("LicensePlate") = lblLicensePlate.Text
            lObjDataRow("AssetDocStatus") = lblAssetDocStatus.Text
            lObjDataRow("AssetDocRack") = lblAssetDocRack.Text
            lObjDataRow("AssetDocFilling") = lblAssetDocFilling.Text
            lObjDataRow("OwnerAsset") = lblOwnerAsset.Text
            lObjDataRow("OwnerAddress") = lblOwnerAddress.Text
            lObjDataRow("OwnerCity") = lblOwnerCity.Text
            lObjDataRow("OwnerZipCode") = lblOwnerZipCode.Text
            Me.TableStockOpnameAct.Rows.Add(lObjDataRow)
        Next
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        If Me.TableStockOpnameAct Is Nothing Then
            Response.Redirect("StockOpnameActual.aspx")
        Else
            If Me.TableStockOpnameAct.Rows.Count = 0 Then
                Response.Redirect("StockOpnameActual.aspx")
            Else
                Me.TableStockOpnameAct.Clear()
            End If
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        SaveDataGridToDataTable()
        With pCustomclass
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .Branchid = Me.sesBranchId.Replace("'", "")
            .DocInformation = Me.TableStockOpnameAct
        End With
        Try
            oController.SaveStockOpnameActual(pCustomclass)
            Response.Redirect("StockOpnameActual.aspx")
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

#Region "Check Duplikasi Application ID"
    Private Function CheckDuplikasiApplicationID(ByVal strApplicationID As String) As Boolean
        Dim i As Integer
        Dim isValid As Boolean = True
        Dim hyApplicationID As Label
        For i = 0 To DtgStockOpname.Items.Count - 1
            hyApplicationID = CType(DtgStockOpname.Items(i).FindControl("lblApplicationID"), Label)
            If hyApplicationID.Text = strApplicationID Then
                isValid = False
                ShowMessage(lblMessage, "No Aplikasi sudah ada !", True)
            End If
        Next
        Return isValid
    End Function
#End Region
End Class

