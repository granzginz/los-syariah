﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.IO.Ports
#End Region

Public Class StockOpnameActual
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Private oCustomclass As New Parameter.DocRec
    Private oController As New StockOpnameDocController

#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property TableStockOpnameAct() As DataTable
        Get
            Return (CType(ViewState("TableStockOpnameAct"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TableStockOpnameAct") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private totalPages1 As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
#End Region
#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "StockOpAct"
            oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
            oSearchBy.BindData()

            Dim dtbranch As New DataTable
            dtbranch = m_controller.GetBranchAll(GetConnectionString)
            With cbobranch
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = dtbranch
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
            cbobranch.Enabled = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub
#End Region



#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spActualStockOpnameDocPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        pnlStockOpname.Visible = False
    End Sub
#End Region
#Region "Navigation"

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
        totalPages1 = e.TotalPage
    End Sub

#End Region
#Region "Databound"

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim HypPilih As HyperLink
        Dim hyStockOpnameNo As HyperLink
        If e.Item.ItemIndex >= 0 Then

            HypPilih = CType(e.Item.FindControl("HypPilih"), HyperLink)
            hyStockOpnameNo = CType(e.Item.FindControl("hyStockOpnameNo"), HyperLink)
            HypPilih.NavigateUrl = "StockOpnameActualList.aspx?StockOpnameNo=" & hyStockOpnameNo.Text.Trim & ""

        End If
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("StockOpnameActual.aspx")
    End Sub
#End Region
#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

            Me.SearchBy = ""
            Me.SearchBy = " sobb.BranchOnStatus = '" & cbobranch.SelectedItem.Value.Trim & "' "
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
            End If
            pnlDatagrid.Visible = True
            DtgAsset.Visible = True
            pnlList.Visible = True
            pnlStockOpname.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Stock Opname"
    Private Sub DtgStockOpname_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgStockOpname.ItemCommand
        If e.CommandName = "DELETE" Then
            Me.TableStockOpnameAct.Rows.RemoveAt(CInt(e.Item.ItemIndex()))
            DtgStockOpname.DataSource = Me.TableStockOpnameAct.DefaultView
            DtgStockOpname.DataBind()
        End If
    End Sub

    Private Sub btnSaveStockOpname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveStockOpname.Click
        pnlList.Visible = False
        pnlDatagrid.Visible = False
        If Me.TableStockOpnameAct Is Nothing Then
            Me.TableStockOpnameAct = GetStockOpnameAct()
        End If
        If CheckDuplikasiApplicationID(txtApplicationId.Text.Trim) Then
            Dim ds As New DataTable
            With oCustomclass
                .strConnection = GetConnectionString()
                .ApplicationId = txtApplicationId.Text.Trim
            End With
            ds = oController.DocInformation(oCustomclass)
            GenerateTable(ds)
            DtgStockOpname.DataSource = Me.TableStockOpnameAct.DefaultView
            DtgStockOpname.DataBind()
            pnlStockOpname.Visible = True
        End If
    End Sub

    Private Sub btnStockOpnameActual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockOpnameActual.Click
        'pnlList.Visible = False
        'pnlDatagrid.Visible = False
        'If Me.TableStockOpnameAct Is Nothing Then
        '    Me.TableStockOpnameAct = GetStockOpnameAct()
        'End If
        'GenerateTable(TableStockOpnameAct)
        'DtgStockOpname.DataSource = Me.TableStockOpnameAct.DefaultView
        'DtgStockOpname.DataBind()
        'pnlStockOpname.Visible = True
        Response.Redirect("StockOpnameActualProcess.aspx")
    End Sub
    Private Function GetStockOpnameAct() As DataTable
        Dim lObjDataTable As New DataTable("StockOpnameAct")
        With lObjDataTable
            .Columns.Add("BranchID", System.Type.GetType("System.String"))
            .Columns.Add("ApplicationID", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocID", System.Type.GetType("System.String"))
            .Columns.Add("DocumentNo", System.Type.GetType("System.String"))
            .Columns.Add("LicensePlate", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocRack", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocFilling", System.Type.GetType("System.String"))
            .Columns.Add("OwnerAsset", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function

    Private Sub GenerateTable(ByVal dtAgreement As DataTable)
        Dim lObjDataRow As DataRow
        Dim i As Integer
        'strZero = ""
        For i = 0 To dtAgreement.Rows.Count - 1
            lObjDataRow = Me.TableStockOpnameAct.NewRow()
            If CheckDuplikasiApplicationID(dtAgreement.Rows(i).Item("ApplicationID").ToString) Then
                lObjDataRow("BranchID") = dtAgreement.Rows(i).Item("BranchID")
                lObjDataRow("ApplicationID") = dtAgreement.Rows(i).Item("ApplicationID")
                lObjDataRow("AssetDocID") = dtAgreement.Rows(i).Item("AssetDocID")
                lObjDataRow("DocumentNo") = dtAgreement.Rows(i).Item("DocumentNo")
                lObjDataRow("LicensePlate") = dtAgreement.Rows(i).Item("LicensePlate")
                lObjDataRow("AssetDocRack") = dtAgreement.Rows(i).Item("AssetDocRack")
                lObjDataRow("AssetDocFilling") = dtAgreement.Rows(i).Item("AssetDocFilling")
                lObjDataRow("OwnerAsset") = dtAgreement.Rows(i).Item("OwnerAsset")
                Me.TableStockOpnameAct.Rows.Add(lObjDataRow)
            End If
        Next i
        AutoSortGrid()
    End Sub

    Sub AutoSortGrid()
        TableStockOpnameAct.DefaultView.Sort = "ApplicationID"
        TableStockOpnameAct = TableStockOpnameAct.DefaultView.ToTable()
    End Sub
#End Region

#Region "Check Duplikasi Application ID"
    Private Function CheckDuplikasiApplicationID(ByVal strApplicationID As String) As Boolean
        Dim i As Integer
        Dim isValid As Boolean = True
        Dim hyApplicationID As HyperLink
        For i = 0 To DtgStockOpname.Items.Count - 1
            hyApplicationID = CType(DtgStockOpname.Items(i).FindControl("hyApplicationID"), HyperLink)
            If hyApplicationID.Text = strApplicationID Then
                isValid = False
            End If
        Next
        Return isValid
    End Function
#End Region
End Class
