﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditDocumentList.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.EditDocumentList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddressCity" Src="../../Webform.UserController/ucAddressCity.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditDocumentList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
			var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
			
var hdnDetail;
var hdndetailvalue;
function ParentChange(pBranch,pRack,pHdnDetail,pHdnDetailValue, itemArray)
{
		hdnDetail = eval('document.forms[0].' + pHdnDetail);
		HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
		var i, j;
		for(i= eval('document.forms[0].' + pRack).options.length;i>=0;i--)
		{   
			eval('document.forms[0].' + pRack).options[i] = null
		
		}  ;
		if (itemArray==null) 
		{ 
			j = 0 ;
		}
		else
		{  
			j=1;
		};
		eval('document.forms[0].' + pRack).options[0] = new Option('Select One','0');
		if (itemArray!=null)
		{
			for(i=0;i<itemArray.length;i++)
			{	
				eval('document.forms[0].' + pRack).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
				
			};
			eval('document.forms[0].' + pRack).selected=true;
		}
		};
		
		function cboChildonChange(l,j)
			{
				hdnDetail.value = l; 
				HdnDetailValue.value = j;
			}
}-->
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnChild" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                EDIT DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="hyAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="hyCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDesc" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Supplier
            </label>
            <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Funding Bank
            </label>
            <asp:Label ID="lblFundingCoName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Status Jaminan
            </label>
            <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Peminjam
            </label>
            <asp:Label ID="lblBorrowerName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Pinjam
            </label>
            <asp:Label ID="lblBorrowDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                DATA ASSET
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                <asp:Label ID="serialno1label" runat="server"></asp:Label>
            </label>
            <%--<asp:TextBox ID="txtChasisNo" runat="server" CssClass="inptype" MaxLength="20" Enabled=false></asp:TextBox>--%>
            <asp:TextBox ID="txtChasisNo" runat="server" CssClass="inptype" MaxLength="20" Enabled=true></asp:TextBox>
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="serialno2label" runat="server"></asp:Label>
            </label>
            <%--<asp:TextBox ID="txtEngineNo" runat="server" CssClass="inptype" MaxLength="20" Enabled=false></asp:TextBox>--%>
            <asp:TextBox ID="txtEngineNo" runat="server" CssClass="inptype" MaxLength="20" Enabled=true></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polisi/No Registrasi
            </label>
            <asp:TextBox ID="txtLicPlate" runat="server" CssClass="inptype" MaxLength="20"></asp:TextBox>
            <asp:RegularExpressionValidator ID="rgv" ResourceName="rgv" ValidationGroup="valGroup" runat="server" ControlToValidate="txtLicPlate" ErrorMessage="No Polisi tidak valid." ValidationExpression="^([A-Za-z0-9 ]+)$" />
        </div>
        <div class="form_right">
            <label>
                Tanggal STNK
            </label>
            <asp:TextBox runat="server" ID="txttdate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txttdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div> 
    <div class="form_box_hide">
        <div class="form_left">
            <label>
                Lokasi Rak
            </label>
            <asp:Label ID="lblRack" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Lokasi Filling
            </label>
            <asp:Label ID="lblFLoc" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Warna
            </label>
            <asp:TextBox ID="txtColor" runat="server" MaxLength="20" CssClass="inptype"></asp:TextBox>
        </div>
        <div class="form_right">
        </div>
    </div>
    <asp:Panel ID="pnlDocumentList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    DAFTAR DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgDoc" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DOCUMENT ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA DOKUMEN">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO DOKUMEN">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDocNo" runat="server" Width="80%" CssClass="inptype" Text='<%#Container.DataItem("documentno")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL DOKUMEN">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtdocdate"></asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtdocdate"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtContentName" runat="server" CssClass="inptype" Text='<%#Container.DataItem("name")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TANGGAL TERIMA" >
                                <ItemTemplate>
                                    <asp:label ID="txtodate2" runat="server" DataField="statusdate">
                                    </asp:label>
                                    <asp:TextBox runat="server" ID="txtodate" Text="a">
                                    </asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtodate"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ASSETSTATUS" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="STATUS">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ISMAINDOC" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblisMainDoc" runat="server" Text='<%#Container.DataItem("isMainDoc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Temp_ValueDate" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblTempValueDate" runat="server" Text='<%#Container.DataItem("valuedate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Temp_DocDate" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblTempDocDate" runat="server" Text='<%#Container.DataItem("DocumentDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AssetHistorySeq" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetHistorySeq" runat="server" Text='<%#Container.DataItem("AssetHistorySeq")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                REGISTRASI DOKUMEN ASSET
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama</label>
            <asp:TextBox ID="txtName" runat="server" Width="355px" MaxLength="50" CssClass="inptype"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <uc1:ucAddressCity id="ucAddressCity1" runat="server">
        </uc1:ucAddressCity>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Catatan</label>
            <asp:TextBox ID="txtAssetNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
