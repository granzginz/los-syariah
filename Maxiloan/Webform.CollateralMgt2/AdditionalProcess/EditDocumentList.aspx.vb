﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditDocumentList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucAddressCity1 As ucAddressCity

#Region "Property"
    Private Property AssetDocumentStatus() As String
        Get
            Return (CType(Viewstate("AssetDocumentStatus"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AssetDocumentStatus") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(Viewstate("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(Viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property DocStatus() As String
        Get
            Return (CType(Viewstate("DocStatus"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("DocStatus") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(Viewstate("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("TotOH") = Value
        End Set
    End Property
    Private Property FlagRack() As String
        Get
            Return (CType(Viewstate("FlagRack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FlagRack") = Value
        End Set
    End Property

    Private Property FLocDesc() As String
        Get
            Return (CType(Viewstate("FLocDesc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLocDesc") = Value
        End Set
    End Property

    Private Property RackDesc() As String
        Get
            Return (CType(Viewstate("RackDesc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RackDesc") = Value
        End Set
    End Property
    Private Property BranchID2() As String
        Get
            Return (CType(Viewstate("BranchID2"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID2") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController

#End Region
#Region "page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid").Trim
            Me.BranchID2 = Request.QueryString("branchid2").Trim
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))
            Me.AssetDocumentStatus = Request.QueryString("AssetDocStatus")
            Me.FormID = "EDITDOC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then


                Dim oReceiveList As New Parameter.DocRec
                oReceiveList.ApplicationId = Me.ApplicationID
                oReceiveList.BranchId = Me.BranchID2
                oReceiveList.strConnection = GetConnectionString()
                oReceiveList = oController.GetTaxDate(oReceiveList)

                Dim strTaxDate As String
                If oReceiveList.taxdate = #1/1/1900# Then
                    strTaxDate = ""
                Else
                    strTaxDate = oReceiveList.taxdate.ToString("dd/MM/yyyy")
                End If

                txttdate.Text = strTaxDate

                DoBind()
                DoBindGrid()
                GetAssetRegistration()

                With ucAddressCity1
                    .TeleponFalse()
                    .BindAddress()
                    .ValidatorTrue()
                    .DivKdPosHide()
                End With

            End If
        End If
        If Me.AssetDocumentStatus = "P" Or Me.AssetDocumentStatus = "W" Then
            pnlDocumentList.Visible = False
        End If
    End Sub

#End Region
#Region "DoBind"
    Sub DoBind()
        Dim inCustID As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID2
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            Me.DocStatus = .AssetDocStatus
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            hyCustomerName.Text = .Customername
            inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustID) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & Server.UrlEncode(inSupId) & "', '" & "AssetDocument" & "')"
            lblAssetDesc.Text = .assetDesc
            txtEngineNo.Text = .EngineNo
            txtChasisNo.Text = .ChasisNo

            txtLicPlate.Text = .LicensePlate
            If Me.DocStatus.Trim = "P" Then
                btnSave.Visible = False
                ShowMessage(lblMessage, "Anda tidak Berhak. Status Dokumen masih Prospect", True)
            End If

            Me.Rack = .RackLocID
            Me.FLoc = .FLocID
            Me.FLocDesc = .FillingLoc
            Me.RackDesc = .RackLoc
            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
            lblBorrowerName.Text = .BorrowBy

            If .Itaxdate = "0" Then
                txttdate.Text = ""
            Else
                If .taxdate.ToString("dd/MM/yyyy") = "01/01/1900" Then
                    txttdate.Text = ""
                Else
                    txttdate.Text = .taxdate.ToString("dd/MM/yyyy")
                End If
            End If

            lblRack.Text = .RackDesc
            lblFLoc.Text = .FillingDesc
            txtColor.Text = .Color
        End With
    End Sub
#End Region
#Region "GetAssetRegistration"
    Sub GetAssetRegistration()
        Dim oAssetReg As New Parameter.DocRec

        oAssetReg.ApplicationId = Me.ApplicationID
        oAssetReg.BranchId = Me.BranchID2
        oAssetReg.strConnection = GetConnectionString
        oAssetReg = oController.GetAssetRegistration(oAssetReg)
        txtName.Text = oAssetReg.OwnerAsset

        Dim strOwnerAddress As String
        If IsDBNull(oAssetReg.OwnerAddress) Then
            strOwnerAddress = "-"
        Else
            strOwnerAddress = oAssetReg.OwnerAddress
        End If

        ucAddressCity1.Address = strOwnerAddress
        txtAssetNotes.Text = oAssetReg.Notes
        ucAddressCity1.City = oAssetReg.OwnerCity
    End Sub
#End Region
#Region "DoBindGrid"
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "'  and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()
    End Sub
#End Region
#Region "DataBound"
    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound
        Dim oDate As TextBox
        Dim oDocDate As TextBox
        Dim lblTempValueDate As New Label
        Dim lblTempDocDate As New Label
        Dim lblAssetDocStatus As New Label
        Dim txtContentName As New TextBox

        If e.Item.ItemIndex >= 0 Then
            oDate = CType(e.Item.FindControl("txtoDate"), TextBox)
            oDocDate = CType(e.Item.FindControl("txtDocDate"), TextBox)
            lblTempValueDate = CType(e.Item.FindControl("lblTempValueDate"), Label)
            lblTempDocDate = CType(e.Item.FindControl("lblTempDocDate"), Label)
            lblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            txtContentName = CType(e.Item.FindControl("txtContentName"), TextBox)

            Dim strTempValueDate As String = lblTempValueDate.Text.Trim
            Dim strTempDocDate As String = lblTempDocDate.Text.Trim

            If strTempValueDate = "-" Then
                lblTempValueDate.Text = ""
            End If
            If strTempDocDate = "-" Then
                lblTempDocDate.Text = ""
            End If

            If lblTempValueDate.Text.Trim <> "" Then
                oDate.Text = ConvertDate2(lblTempValueDate.Text.Trim).ToString("dd/MM/yyyy")
            Else
                oDate.Text = lblTempValueDate.Text.Trim
            End If

            If lblTempDocDate.Text.Trim <> "" Then
                oDocDate.Text = ConvertDate2(lblTempDocDate.Text.Trim).ToString("dd/MM/yyyy")
            Else
                oDocDate.Text = lblTempDocDate.Text.Trim
            End If

            If lblAssetDocStatus.Text = "I" Then
                oDate.Enabled = False
                txtContentName.ReadOnly = True
            Else
                oDate.Enabled = True
                txtContentName.ReadOnly = False
            End If
        End If

    End Sub
#End Region
#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("EditDocument.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
        Dim oHeader As New Parameter.DocRec
        oHeader.ChasisNo = txtChasisNo.Text
        oHeader.EngineNo = txtEngineNo.Text
        oHeader.LicensePlate = txtLicPlate.Text
        oHeader.BranchId = Me.BranchID2.Trim
        oHeader.ApplicationId = Me.ApplicationID.Trim
        oHeader.AssetDocStatus = Me.DocStatus.Trim
        oHeader.AssetSeqNo = Me.AssetSeqNo

        Dim Temptdate As String = txttdate.Text.Trim
        If Temptdate = "" Then
            Temptdate = "1/1/1900"
        End If

        oHeader.taxdate = ConvertDate2(Temptdate)
        oHeader.OwnerAsset = txtName.Text
        oHeader.OwnerAddress = ucAddressCity1.Address.Trim
        oHeader.OwnerRT = ""
        oHeader.OwnerRW = ""
        oHeader.OwnerKelurahan = ""
        oHeader.OwnerKecamatan = ""
        oHeader.OwnerCity = ucAddressCity1.City.Trim
        oHeader.Notes = txtAssetNotes.Text
        oHeader.strConnection = GetConnectionString()
        oHeader.Color = txtColor.Text

        If DtgDoc.Items.Count > 0 Then
            Dim i As Integer
            Dim j As Integer
            Dim k As Integer
            Dim dtAdd As New DataTable
            Dim dr As DataRow
            Dim isRec As New Label
            Dim inDocname As New Label
            Dim inDocNo As New TextBox
            Dim inDate As TextBox
            Dim inAssetDocStatus As New Label
            Dim inStatusDate As String
            Dim inisMainDoc As New Label
            Dim inlblassetdocid As New Label
            Dim inDocDate As TextBox
            Dim strGroupID As String
            Dim lblAssetHistorySeq As New Label
            Dim txtContentName As New TextBox

            strGroupID = ""


            dtAdd.Columns.Add("DocNo", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocDate", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("Date", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("AssetDocStatus", System.Type.GetType("System.String"))

            dtAdd.Columns.Add("AssetDocID", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("AssetHistorySeq", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("ContentName", System.Type.GetType("System.String"))

            j = 0
            For i = 0 To DtgDoc.Items.Count - 1
                isRec = CType(DtgDoc.Items(i).FindControl("lblrec"), Label)
                inDocNo = CType(DtgDoc.Items(i).FindControl("txtDocNo"), TextBox)
                inDocDate = CType(DtgDoc.Items(i).FindControl("txtDocDate"), TextBox)
                inDate = CType(DtgDoc.Items(i).FindControl("txtoDate"), TextBox)
                inAssetDocStatus = CType(DtgDoc.Items(i).FindControl("lblAssetDocStatus"), Label)

                inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)
                lblAssetHistorySeq = CType(DtgDoc.Items(i).FindControl("lblAssetHistorySeq"), Label)
                txtContentName = CType(DtgDoc.Items(i).FindControl("txtContentName"), TextBox)

                dr = dtAdd.NewRow()
                If inDocNo.Text.Trim = "" Then
                    dr("DocNo") = "-"
                Else
                    dr("DocNo") = inDocNo.Text.Trim
                End If

                If inDocDate.Text.Trim = "" Then
                    dr("DocDate") = "1/1/1900"
                Else
                    dr("DocDate") = ConvertDate2(inDocDate.Text.Trim).ToString("MM/dd/yyyy")
                End If

                If inDate.Text.Trim = "" Then
                    dr("Date") = "1/1/1900"
                Else
                    dr("Date") = ConvertDate2(inDate.Text.Trim).ToString("MM/dd/yyyy")
                End If

                If inAssetDocStatus.Text.Trim = "" Then
                    dr("AssetDocStatus") = ""
                Else
                    dr("AssetDocStatus") = inAssetDocStatus.Text.Trim
                End If
                dr("AssetDocID") = inlblassetdocid.Text.Trim
                dr("AssetHistorySeq") = lblAssetHistorySeq.Text.Trim
                dr("ContentName") = txtContentName.Text
                strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & inlblassetdocid.Text.Replace("'", "") & "'"
                dtAdd.Rows.Add(dr)
            Next

            Try
                oCustomClass = oController.SaveEdit(oHeader, dtAdd)
                If oCustomClass.strError <> "" Then
                    ShowMessage(lblMessage, "Update Dokumen Gagal", True)
                    Exit Sub
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    'Response.Redirect("EditDocument.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    Response.Redirect("EditDocument.aspx")
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
                lblMessage.Visible = True
                Exit Sub
            End Try
        End If

        'End If
    End Sub

#End Region

End Class