﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilCekBPKB.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.HasilCekBPKB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCustomerLookUpGlobal" Src="../Webform.UserController/ucLookUpCustomerGlobal.ascx" %>
<%@ Register Src="../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HasilCekBPKB</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function cboPMarital_IndexChanged(e) {
            if (e == 'M') {
                $('#divSuamiIstri').css("visibility", "");
                $('#txtNamaPasangan').removeAttr("style");

                ValidatorEnable(rfvNamaPasangan, true);
                $('#rfvNamaPasangan').show();

                ValidatorEnable(revNamaPasangan, true);
                $('#revNamaPasangan').hide();
            }
            else {
                $('#divSuamiIstri').css("visibility", "hidden");
                $('#txtNamaPasangan').attr("style", "display : none");

                ValidatorEnable(rfvNamaPasangan, false);
                $('#rfvNamaPasangan').hide();

                ValidatorEnable(revNamaPasangan, false);
                $('#revNamaPasangan').hide();
            }
        }

        function cboHasilPeriksa_onchange(e) {
            if (e === '1') {
                $('#divCatatan').css("display", "none");
                ValidatorEnable(rfvCatatan, false);
                $('#rfvCatatan').hide();
                return;
            }
            else {
                if (e === '') {
                    $('#divCatatan').css("display", "none");
                    ValidatorEnable(rfvCatatan, false);
                    $('#rfvCatatan').hide();
                    return;
                }

                if (e === '0') {
                    $('#divCatatan').css("display", "inherit");
                    ValidatorEnable(rfvCatatan, true);
                    return;
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                HASIL CEK BPKB
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    DAFTAR HASIL CEK BPKB
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" DataKeyField="AgreementNo"
                        CssClass="grid_general">
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.png"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="AgreementNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementNo" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblNoBPKB" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.NoBPKB") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblTglBPKB" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.TglBPKB") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblNamaBPKB" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.NamaBPKB") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblOldOwnerAddress" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.OldOwnerAddress") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblOldOwnerKelurahan" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.OldOwnerKelurahan") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblOldOwnerKecamatan" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.OldOwnerKecamatan") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblOldOwnerCity" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.OldOwnerCity") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblApplicationID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblHasilCekBPKB" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.HasilCekBPKB") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblHasilCekNote" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.HasilCekNote") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="NAMA"></asp:BoundColumn>
                            <asp:BoundColumn DataField="NoBPKB" HeaderText="NO BPKB"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TglBPKB" HeaderText="TGL BPKB"></asp:BoundColumn>
                            <asp:BoundColumn DataField="NamaBPKB" HeaderText="NAMA BPKB"></asp:BoundColumn>
                            <asp:BoundColumn DataField="OldOwnerAddress" HeaderText="ALAMAT BPKB"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h5>
                    MENCARI
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama</asp:ListItem>
                    <asp:ListItem Value="AlamatBPKB">Alamat</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlEdit">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    EDIT HASIL CEK BPKB
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Kontrak</label>
                <asp:Label runat="server" ID="lblNoKontrakE"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama</label>
                <asp:Label runat="server" ID="lblNamaE"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No BPKB</label>
                <asp:Label runat="server" ID="lblNoBPKBE"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal BPKB</label>
                <asp:Label runat="server" ID="lblTglBPKBE"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama BPKB</label>
                <asp:Label runat="server" ID="lblNamaBPKBE"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat BPKB</label>
                <asp:Label runat="server" ID="lblAlamatBPKBE"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Hasil Periksa
                </label>
                <asp:DropDownList ID="cboHasilPeriksa" runat="server" onchange="cboHasilPeriksa_onchange(this.value);">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="1">Terdaftar</asp:ListItem>
                    <asp:ListItem Value="0">Tidak Terdaftar</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboHasilPeriksa" runat="server" Display="Dynamic"
                    ControlToValidate="cboHasilPeriksa" ErrorMessage="Harap pilih Rak" InitialValue=""
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box" runat="server" id="divCatatan">
            <div class="form_single">
                <label>
                    Catatan
                </label>
                <asp:TextBox runat="server" ID="txtCatatan" CssClass="long_text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCatatan" runat="server" Display="Dynamic"
                    ControlToValidate="txtCatatan" ErrorMessage="Harap diisi" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:HiddenField runat="server" ID="hdfApplicationID" />
    <asp:HiddenField runat="server" ID="hdfHasilCekBPKB" />
    <asp:HiddenField runat="server" ID="hdfHasilCekNote" />
    </form>
</body>
</html>
