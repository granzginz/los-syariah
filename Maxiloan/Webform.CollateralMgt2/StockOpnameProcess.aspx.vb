﻿

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class StockOpnameProcess
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New DataUserControlController
    Private oController As New StockOpnameDocController
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents GridNavigator1 As ucGridNav



    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 10
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        AddHandler GridNavigator1.PageChanged, AddressOf PageNavigation1
        If Not IsPostBack Then
            Me.FormID = "STOCKOPNMPR"

            Dim dt_branch As New DataTable
            'dt_branch = m_controller.GetBranchAll(GetConnectionString)
            'With cbobranch
            '    .DataTextField = "Name"
            '    .DataValueField = "ID"
            '    .DataSource = dt_branch
            '    .DataBind()
            'End With
            With cbobranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "ALL"
                    .Enabled = True
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Enabled = False
                End If
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
            'cbobranch.Enabled = False
            If (Not Me.IsHoBranch) Then
                cbobranch.Enabled = False
            End If

             txttglOpname.Text = ""
            txttglOpnameAct.Text = ""
            lblStokOpNo.Text = ""
            hdnStokOpNo.Text = ""
            lblActStokOpName.Text = ""
            hdnStokOpNoAct.Text = ""

            pnlUpload.Visible = True
            pnlConfirmGrid.Visible = False


        End If
    End Sub

    Public Sub txttglOpnameChanged(sender As Object, e As EventArgs)
        Dim res = oController.getOpnameNo(GetConnectionString, sesBranchId.Replace("'", "").Trim, ConvertDate2(txttglOpname.Text), True)
        If (res = "") Then
            ShowMessage(lblMessage, "Tidak Ditemukan Stok Opmane Pada tanggal tersebut", True)
            lblStokOpNo.Text = ""
            hdnStokOpNo.Text = ""
        End If
        lblStokOpNo.Text = res
        hdnStokOpNo.Text = res
    End Sub

    Public Sub txttglOpnameActChanged(sender As Object, e As EventArgs)
        Dim res = oController.getOpnameNo(GetConnectionString, sesBranchId.Replace("'", "").Trim, ConvertDate2(txttglOpnameAct.Text), False)
        If (res = "") Then
            ShowMessage(lblMessage, "Tidak Ditemukan Stok Opmane Pada tanggal tersebut", True)
            lblActStokOpName.Text = ""
            hdnStokOpNoAct.Text = ""
        End If
        lblActStokOpName.Text = res
        hdnStokOpNoAct.Text = res
    End Sub


    Private Sub btnProses_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProses.Click

        If (lblStokOpNo.Text.Trim = "" Or lblActStokOpName.Text.Trim = "") Then
            ShowMessage(lblMessage, "Silahkan Pilih Data Stok Opname.", True)
        End If

        Try
            Dim result = oController.DoProsesCompareStockOpname(GetConnectionString, lblStokOpNo.Text.Trim, lblActStokOpName.Text.Trim, BusinessDate)
            If (result <> "OK") Then
                ShowMessage(lblMessage, "Gagal proses stock upname", True)
            End If

            pnlUpload.Visible = False
            pnlConfirmGrid.Visible = True


            lblMessage.Text = ""

            doBindOnhand()
            doBindOnhand1()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("StockOpnameProcess.aspx")
    End Sub

    Private Sub btncancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelProses.Click
        Response.Redirect("StockOpnameProcess.aspx")
    End Sub
     

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        doBindOnhand(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Protected Sub PageNavigation1(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        doBindOnhand1(True)
        GridNavigator1.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Private Sub doBindOnhand1(Optional isFrNav As Boolean = False)
        Dim result = oController.GetProcesResultActualOpnamePage(GetConnectionString(), currentPage, pageSize, lblActStokOpName.Text.Trim)
        recordCount = result.TotalRecords
        DtgAgreeAct.DataSource = result.DataSetResult.Tables(0)
        Try
            DtgAgreeAct.DataBind()
        Catch
            DtgAgreeAct.CurrentPageIndex = 0
            DtgAgreeAct.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator1.Initialize(recordCount, pageSize)
        End If
    End Sub

    Private Sub doBindOnhand(Optional isFrNav As Boolean = False)
        Dim result = oController.GetProcesResultOnhandOpnamePage(GetConnectionString(), currentPage, pageSize, lblStokOpNo.Text.Trim)
        recordCount = result.TotalRecords
        DtgAgree.DataSource = result.DataSetResult.Tables(0)
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If


        lblBranchOn.Text = result.DataSetResult.Tables(1).Rows(0)("BranchOnStatus")
        lblTglOpname.Text = CType(result.DataSetResult.Tables(1).Rows(0)("OpnameDate"), DateTime).ToString("dd/MM/yyyy HH:mm")

        lblStockOpnameNo.Text = result.DataSetResult.Tables(1).Rows(0)("StockOpnameNo")
        lblTotalOnhand.Text = result.DataSetResult.Tables(1).Rows(0)("QtyOnHand").ToString

        lblStockOpnameNoAct.Text = result.DataSetResult.Tables(1).Rows(0)("StockOpnameActNo")
        lblTotalActual.Text = result.DataSetResult.Tables(1).Rows(0)("QtyActual")

        lbldtmAct.Text = CType(result.DataSetResult.Tables(1).Rows(0)("DtmUpd"), DateTime).ToString("dd/MM/yyyy")
        lblVariance.Text = result.DataSetResult.Tables(1).Rows(0)("Variance")



    End Sub
End Class