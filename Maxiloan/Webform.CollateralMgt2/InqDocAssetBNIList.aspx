﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqDocAssetBNIList.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.InqDocAssetBNIList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocReleaseList</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>


<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENYERAHAN DOKUMEN ASSET
            </h3>
        </div>
    </div>
        <div class="form_box">
           <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:hyperlink ID="lblAgreementNo" runat="server"></asp:hyperlink>
          </div>
          <div class="form_right">
            <label>
                No Mesin
            </label>
            <asp:label ID="lblNoMesin" runat="server"></asp:label>
        </div>
    </div>
        <div class="form_box">
           <div class="form_left">
            <label>
                No Rangka
            </label>
            <asp:label ID="lblNoRangka" runat="server"></asp:label>
          </div>
          <div class="form_right">
            <label>
                Nasabah
            </label>
            <asp:label ID="LblNasabah" runat="server"></asp:label>
        </div>
    </div>
        <div class="form_box">
           <div class="form_left">
            <label>
                No Polisi
            </label>
            <asp:label ID="lblNoPolisi" runat="server"></asp:label>
          </div>
          <div class="form_right">
            <label>
                Nama Di BPKB
            </label>
            <asp:label ID="lblNamaDiBPKB" runat="server"></asp:label>
          </div>
        </div>
        <div class="form_box">
            <div class="form_left">            
                <label>
                    Tgl Penyerahan<label class="mandatory">
                    </label>
                </label>                
            <asp:label runat="server" ID="txtsdate"></asp:label>
         <%--   <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>--%>
           
        </div>
            <div class="form_right">
            <label>
                No BPKB
            </label>
            <asp:label ID="lblNoBPKB" runat="server"></asp:label>
        </div>
        </div>
        <div class="form_box">
           <div class="form_left">
            <label>
                RAKId
            </label>
            <asp:label ID="LblRAKId" runat="server"></asp:label>
          </div>
          <div class="form_right">
            <label>
                FillingId
            </label>
            <asp:label ID="LblFillingId" runat="server"></asp:label>
        </div>
        </div>
        <div class="form_box">
        <div class="form_left">            
                <label>Catatan</label>
            <asp:label ID="LblCatatan" runat="server"></asp:label>
               <%-- <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>--%>
        </div>
            <div class="form_right">
            <label>
                Tgl Terima BPKB
            </label>
            <asp:label ID="LblTglTerimaBPKB" runat="server"></asp:label>
            </div>
    </div>
        <div class="form_button">
        <%--<asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
        </asp:Button>--%>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
        </div>
    </form>
</body>
</html>
