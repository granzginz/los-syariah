﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocPledgingReceive
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property FundingBatchID() As String
        Get
            Return CStr(viewstate("FundingBatchID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchID") = Value
        End Set
    End Property
    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property WhereCond() As String
        Get
            Return (CType(ViewState("WhereCond"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If Not Me.IsPostBack Then
            Me.FormID = "DOCPLEDGERCV"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                PnlTop.Visible = True
                pnlDatagrid.Visible = True

                Me.SearchBy = ""
                Me.SortBy = ""
                If Request.QueryString("FundingCoyID") <> "" Then Me.CompanyID = Request.QueryString("FundingCoyID")
                If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo")
                If Request.QueryString("FundingBatchNO") <> "" Then Me.FundingBatchID = Request.QueryString("FundingBatchNO")
                If Request.QueryString("SearchBy") <> "" Then Me.SearchBy = Request.QueryString("SearchBy")
                If Me.SearchBy <> "" Then
                    Me.SearchBy = Me.SearchBy + " FundingBPKBtoDraw.FundingCoyID = '" & Me.CompanyID.Trim & "' and FundingBPKBtoDraw.FundingContractNO = '" & Me.FundingContractNo.Trim & "' and FundingBPKBtoDraw.FundingBatchNO = '" & Me.FundingBatchID.Trim & "' and aga.AssetDocStatus = 'G'"
                Else
                    Me.SearchBy = " FundingBPKBtoDraw.FundingCoyID = '" & Me.CompanyID.Trim & "' and FundingBPKBtoDraw.FundingContractNO = '" & Me.FundingContractNo.Trim & "' and FundingBPKBtoDraw.FundingBatchNO = '" & Me.FundingBatchID.Trim & "' and aga.AssetDocStatus = 'G'"
                End If
                lblFundingCoy.Text = Me.CompanyID.Trim
                LblFundingContractNo.Text = Me.FundingContractNo.Trim
                LblFundingBatch.Text = Me.FundingBatchID.Trim
                GetComboRack()
                DoBind(Me.SearchBy, Me.SortBy)

            End If
        End If
    End Sub
#Region "Button Click"
    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("DocPledgingReceiveList.aspx")
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If cboRack.SelectedItem.Value.Trim = "0" Then
            ShowMessage(lblMessage, "Harap Pilih Lokasi Rak", True)
            lblMessage.Visible = True
            GetComboRack()
            Exit Sub
        Else
            If hdnChildValue.Value.Trim = "0" Or hdnChildValue.Value.Trim = "" Then
                ShowMessage(lblMessage, "Harap Pilih Lokasi Filling", True)
                lblMessage.Visible = True
                GetComboRack()
                Exit Sub
            End If
        End If
        Me.Rack = cboRack.SelectedItem.Value
        Me.FLoc = hdnChildValue.Value.Trim
        Me.WhereCond = ("And ApplicationID in (") + Me.ApplicationID + ")"
        With oCustomClass
            .strConnection = GetConnectionString()
            .RackLoc = Me.Rack.Trim.Trim
            .FillingLoc = Me.FLoc.Trim
            .BusinessDate = Me.BusinessDate
            .FundingCoyID = Me.CompanyID.Trim
            .FundingContractNo = Me.FundingContractNo.Trim
            .FundingBatchNo = Me.FundingBatchID.Trim
            .WhereCond = Me.WhereCond
        End With
        Try
            oCustomClass = oController.DocPledgeRecSave(oCustomClass)
            ShowMessage(lblMessage, "Update Data Berhasil", False)
            lblMessage.Visible = True
            Response.Redirect("DocPledgingReceiveList.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
            lblMessage.Visible = True
            GetComboRack()
            Exit Sub

        End Try
    End Sub
#End Region
#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetDocPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
            If DtUserList.Rows.Count <= 0 Then
                ShowMessage(lblMessage, "Proses Terima Dokumen Asset tidak dapat dilakukan ", True)
                lblMessage.Visible = True
                btnSave.Visible = False
            End If
        Else
            recordCount = 0
            ShowMessage(lblMessage, "Proses Terima Dokumen Asset tidak dapat dilakukan ", True)
            lblMessage.Visible = True
            btnSave.Visible = False
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        PagingFooter()
        PnlTop.Visible = True
        pnlDatagrid.Visible = True
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data Not Found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "RackChange"

    Protected Function RackChange() As String
        Return "ParentChange('" & Trim(cboRack.ClientID) & "','" & Trim(cboFill.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

#End Region
#Region "GenerateScript"

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboRack.Items.Count - 1

            DataRow = DtTable.Select(" RackId = '" & cboRack.Items(j).Value & "'")


            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("RackId")).Trim Then
                        strType = CStr(DataRow(i)("RackId")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("filldesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("fillingid")), "null", DataRow(i)("fillingid"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("filldesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("fillingid")), "null", DataRow(i)("fillingid"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "PrivateSub"
    Private Sub GetComboRack()
      
        Dim dtFill As New DataTable
        
        dtFill = GetComboRackDT()
        Response.Write(GenerateScript(dtFill))
    End Sub

    Private Function GetComboRackDT() As DataTable
        Dim dtRack As New DataTable
        Dim dtEmp As New DataTable
        dtRack = oController.GetRack(GetConnectionString, Me.sesBranchId.Replace("'", ""))
        With cboRack
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtRack
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

        Dim dtFill As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
        End With
        dtFill = oController.GetFillLoc(oCustomClass)
        Return dtFill
    End Function
#End Region

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = GetComboRackDT()
        Dim i As Integer

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(cboFill.UniqueID, CStr(DtChild.Rows(i).Item("fillingid")).Trim)
        Next

        MyBase.Render(writer)
    End Sub

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        Dim lb As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            lb = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            If Me.ApplicationID Is Nothing Then
                Me.ApplicationID = "'" + CStr(lb.Text.Trim) + "'"
            Else
                Me.ApplicationID = Me.ApplicationID + ",'" + CStr(lb.Text.Trim) + "'"
            End If            
        End If
    End Sub
End Class