﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Drawing
Imports System.IO
#End Region

Public Class DocGenerateBarcodeList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController

#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'lblMessage.Visible = False
        BarcodeImage.Visible = False
        GetCookies()

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            'Me.cbPrinter.DataSource = System.Drawing.Printing.PrinterSettings.InstalledPrinters
            'Me.cbPrinter.DataBind()
            'Me.cbPrinter.SelectedIndex = 0
        End If

        Dim strImageURL As String
        Dim DataBarcode As String = ""

        'CheckConnection()
        Dim txtData As String = Me.ApplicationID
        Dim txtHeight As String = "100"
        Dim txtWidth As String = "300"
        Dim txtEncodeType As String = "Code 128-B"
        Dim txtImageFormat As String = "JPEG"


        strImageURL = "GenerateBarcodeImage.aspx?d=" + txtData.Trim() + "&h=" + txtHeight.Trim() + "&w=" + txtWidth.Trim() + "&bc=white&fc=black&t=" + txtEncodeType.Trim() + "&il=true&if=" + txtImageFormat.Trim() + "&align=c"
        BarcodeImage.ImageUrl = strImageURL
        BarcodeImage.Width = Convert.ToInt32(600)
        BarcodeImage.Height = Convert.ToInt32(300)
        BarcodeImage.Visible = True
    End Sub
#End Region
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PrintDocGenerateBarcode")
        Me.ApplicationID = cookie.Values("ApplicationId")
    End Sub

End Class