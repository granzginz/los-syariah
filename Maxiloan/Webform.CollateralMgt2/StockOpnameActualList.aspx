﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StockOpnameActualList.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.StockOpnameActualList" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>StockOpnameActual</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
   <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
    <div class="title_strip">
    </div>
    <div class="form_single">
    <h3>STOCK OPNAME ACTUAL</h3>
    </div>
    </div>
      <asp:Panel ID="pnllist" runat="server">
        <div class="form_box">
            <div class="form_left">
            <label>BranchOnStatus</label> 
            <asp:Label ID="lblBranchOn" class='lblNum' runat="server" />
                                
            </div>
            <div class="form_right">
              <label>Tanggal </label>
                <asp:Label ID="lblTglOpname" runat="server" />               
            </div>
        </div>

       <div class="form_box">
            <div class="form_left">
                <label>No. StockOpname</label> 
                <asp:Label ID="lblStockOpnameNo" class='lblNum' runat="server" />
             </div>
            <div class="form_right">
               <label>Total Qty Actual</label>
                <asp:Label ID="lblTotalQtyActual" runat="server" /> 
            </div>
        </div>
         
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0" BorderStyle="None" CssClass="grid_general"  AllowSorting="True">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:BoundColumn DataField="branchid" HeaderText="BRANCH" />
                    <asp:BoundColumn DataField="Applicationid" HeaderText="APLIKASI" />
                    <asp:BoundColumn DataField="AssetDocID" HeaderText="Asset Doc." />
                    <asp:BoundColumn DataField="DocumentNo" HeaderText="No. Dokumen" /> 
                    <asp:BoundColumn DataField="LicensePlate" HeaderText="NO. POLISI" />
                    <asp:BoundColumn DataField="AssetDocRack" HeaderText="RAK" />
                    <asp:BoundColumn DataField="AssetDocFilling" HeaderText="FILLING" />
                    <asp:BoundColumn DataField="OwnerAsset" HeaderText="PEMILIK" />
                 </Columns>
                </asp:DataGrid>
                   <uc2:ucGridNav id="GridNavigator" runat="server"/>  
                </div>
            </div>
            <div class="form_button"> 
                <asp:Button ID="btnCancelProses" runat="server" Text="OK" CssClass="small button gray"  CausesValidation="False" />
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
