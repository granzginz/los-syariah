﻿Imports BarcodeLib

Public Class GenerateBarcodeImage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Len(Request.QueryString("d")) > 0 Then
            'MsgBox(Request.QueryString("d"))

            'Read in the parameters
            Dim strData As String = Request.QueryString("d")
            Dim imageHeight As Integer = Convert.ToInt32(Request.QueryString("h"))
            Dim imageWidth As Integer = Convert.ToInt32(Request.QueryString("w"))
            Dim Forecolor As String = Request.QueryString("fc")
            Dim Backcolor As String = Request.QueryString("bc")
            Dim bIncludeLabel As Boolean = Request.QueryString("il").ToLower().Trim()
            Dim strImageFormat As String = Request.QueryString("if").ToLower().Trim()
            Dim strAlignment As String = Request.QueryString("align").ToLower().Trim()

            Dim Type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
            Type = BarcodeLib.TYPE.CODE39

            Dim barcodeImage As System.Drawing.Image = Nothing

            Try
                Dim b As BarcodeLib.Barcode = New BarcodeLib.Barcode()
                If (Type <> BarcodeLib.TYPE.UNSPECIFIED) Then
                    b.IncludeLabel = bIncludeLabel

                    b.Alignment = BarcodeLib.AlignmentPositions.CENTER

                    Forecolor = "000000"
                    Backcolor = "FFFFFF"

                    '===== Encoding performed here =====
                    barcodeImage = b.Encode(Type, strData.Trim(), System.Drawing.ColorTranslator.FromHtml("#" + Forecolor), System.Drawing.ColorTranslator.FromHtml("#" + Backcolor), imageWidth, imageHeight)
                    '===================================

                    '===== Static Encoding performed here =====
                    'barcodeImage = BarcodeLib.Barcode.DoEncode(type, this.txtData.Text.Trim(), this.chkGenerateLabel.Checked, this.btnForeColor.BackColor, this.btnBackColor.BackColor);
                    '==========================================

                    Response.ContentType = "image/" + strImageFormat
                    Dim MemStream As System.IO.MemoryStream = New System.IO.MemoryStream()

                    barcodeImage.Save(MemStream, Drawing.Imaging.ImageFormat.Jpeg)

                    MemStream.WriteTo(Response.OutputStream)
                End If
            Catch ex As Exception

            Finally
                If Not (barcodeImage Is Nothing) Then
                    'Clean up / Dispose...
                    barcodeImage.Dispose()
                    'MsgBox("Gagal Generate Barcode")
                End If
            End Try

        End If
    End Sub

End Class