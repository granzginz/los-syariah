﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController


Public Class InqDocAssetBNIList
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocAssetBNI
    Private oController As New DocReceiveController


#End Region

#Region "Property"

    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property

    Private Property NoRangka() As String
        Get
            Return (CType(ViewState("NoRangka"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("NoRangka") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return (CType(ViewState("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.BranchID = Request.QueryString("branchid")
            'Me.NoRangka = CInt(Request.QueryString("NoRangka"))
            Me.FormID = "InqDocAssetBNI"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind1()
                'DoBindGrid()
                'DoBindInfo()
            End If
        End If

    End Sub
#End Region

#Region "DoBind"
    Sub DoBind1()
        'Dim inCustid As String
        'Dim insupid As String
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.AgreementNo = Me.AgreementNo
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.InqListDocAssetBNI(oCustomClass)
        With oCustomClass
            'inCustid = .CustomerID
            'Me.CustomerID = inCustid
            'hyAgreementNo.Text = .AgreementNo
            'hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            'hyCustomerName.Text = .Customername
            'hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustid) & "')"
            'insupid = .Supplierid
            'lblSupplier.Text = .SupplierName
            'lblSupplier.NavigateUrl = "javascript:OpenWinSupplier(  '" & "AssetDocument" & "','" & Server.UrlEncode(insupid) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblNoMesin.Text = .NoMesin
            lblNoRangka.Text = .NoRangka
            lblNoBPKB.Text = .NoBPKB
            lblNoPolisi.Text = .NoPolisi
            lblNamaDiBPKB.Text = .NamaDiBPKB
            lblTglTerimaBPKB.Text = .TglTerimaBPKB
            LblCatatan.Text = .CatatanClose
            txtsdate.Text = .TglClose
            LblFillingId.Text = .FillingId
            LblRAKId.Text = .RAKId
            LblNasabah.Text = .Nasabah

            'lblEngineNo.Text = .EngineNo
            'Me.Chasis = .ChasisNo
            'lblLicPlate.Text = .LicensePlate
            'If .Itaxdate = "1" Then
            '    lblTaxDate.Text = .taxdate.ToString("dd/MM/yyyy")
            'Else
            '    lblTaxDate.Text = "-"
            'End If
            'lblCrossDefault.Text = .CrossDefault

            btnCancel.Visible = True
            'lblRack.Text = .RackLoc
            'lblFLoc.Text = .FillingLoc
            'serialno1label.Text = .seriallabel1
            'serialno2label.Text = .seriallabel2
            'txtChasisNo.Text = .ChasisNo.Trim
            'lblFundingCoName.Text = .FundingCoName
            'lblFundingPledgeStatus.Text = .FundingPledgeStatus
            'Me.AssetTypeID = .AssetTypeID
            'If Me.AssetTypeID <> "MOBIL" Then
            '    pnlMobiltipe.Visible = False
            'Else
            '    pnlMobiltipe.Visible = True
            'End If
        End With
    End Sub
#End Region

#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("InqDocAssetBNI.aspx")
    End Sub
#End Region


End Class