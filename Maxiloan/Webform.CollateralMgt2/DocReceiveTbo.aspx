﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocReceiveTbo.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.DocReceiveTbo" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DocReceive</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypReceive" runat="server" Text='TERIMA' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>' />
                                    <asp:Label id="hdnBranch"  name="hdnBranch" runat="server"   Text='<%#Container.DataItem("branchid")%>'/>
                                    <asp:Label id="lblIsFund"  name="lblIsFund" runat="server"   Text='<%#Container.DataItem("DocInFunding")%>'/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>' />

                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NamaBPKB" HeaderText="NAMA BPKB">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblNamaBPKB" runat="server" Text='<%#Container.DataItem("NamaBPKB")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>' />
                                    
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NOPOL">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Chasis" HeaderText="NO CHASIS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblChasis" runat="server" Text='<%#Container.DataItem("Chasis")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EngineNo" HeaderText="NO MESIN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblEngineNo" runat="server" Text='<%#Container.DataItem("EngineNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetstatus" HeaderText="ASS">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetstatus")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--<asp:TemplateColumn SortExpression="assetdocstatusdesc" HeaderText="DOK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            
                        </Columns>                        
                    </asp:DataGrid>
                     <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4> CARI DOKUMEN ASSET </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
              <label class ="label_req"> Cabang </label>
                <asp:DropDownList ID="cbobranch" runat="server" />
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" ControlToValidate="cbobranch" ErrorMessage="Harap Pilih Cabang" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
