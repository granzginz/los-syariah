﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region




Public Class PindahKeCustodianList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property BranchID2() As String
        Get
            Return (CType(ViewState("BranchID2"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID2") = Value
        End Set
    End Property
    Private Property BranchID_BPKB() As String
        Get
            Return (CType(ViewState("BranchID_BPKB"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID_BPKB") = Value
        End Set
    End Property
    Private Property BranchName_BPKB() As String
        Get
            Return (CType(ViewState("BranchName_BPKB"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchName_BPKB") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(ViewState("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(ViewState("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(ViewState("Rack"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(ViewState("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FLoc") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(ViewState("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotOH") = Value
        End Set
    End Property

    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(ViewState("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetSeqNo") = Value
        End Set
    End Property

    Private Property RackFillLoc() As IList(Of RackFill)
        Get
            Return (CType(ViewState("RackFillLoc"), IList(Of RackFill)))
        End Get
        Set(ByVal Value As IList(Of RackFill))
            ViewState("RackFillLoc") = Value
        End Set
    End Property



#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
    Private m_controller As New DataUserControlController

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub


        If Not IsPostBack Then

            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.BranchID2 = Request.QueryString("branchid2")
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))

            Me.FormID = "DOCCHANGELOC"


            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim oPosition As New Parameter.DocRec
                Dim oNewPosition As New Parameter.DocRec

                oPosition.strConnection = GetConnectionString()
                oPosition.BranchId = Me.BranchID2
                oPosition.sesBranchID = Me.BranchID
                oPosition.ApplicationId = Me.ApplicationID
                oNewPosition = oController.GetBPKBPosition(oPosition)
                Dim strBranchName As String
                Dim strBranchID As String
                strBranchID = oNewPosition.BranchId
                strBranchName = oNewPosition.BranchName

                Me.BranchID_BPKB = strBranchID.Trim
                Me.BranchName_BPKB = strBranchName

                If strBranchID <> Me.BranchID Then
                    ShowMessage(lblMessage, "Anda Tidak Berhak. Dokumen disimpan di Cabang  " & strBranchName & "!", True)
                    btnSave.Visible = False
                End If

                DoBind()
                DoBindGrid()

                If Me.TotOH <= 0 Then
                    ShowMessage(lblMessage, "Tidak ada dokumen on hand!", True)
                    btnSave.Visible = False
                End If



                'Dim dt_branch As New DataTable
                'dt_branch = m_controller.GetBranchAll(GetConnectionString)
                'For i = dt_branch.Rows.Count - 1 To 0 Step -1
                '    Dim _r = dt_branch.Rows(i)

                '    If Not (_r("ID") = Me.BranchID2 Or _r("ID") = "900") Then
                '        dt_branch.Rows.RemoveAt(i)
                '    End If

                'Next

                With cboBranch
                    '.DataTextField = "Name"
                    '.DataValueField = "ID"
                    '.DataSource = dt_branch
                    '.DataBind() 
                    .Items.Insert(0, "Custodian")
                    .Items(0).Value = "CUS"
                End With
                'cboBranch.Items.FindByValue(Me.BranchID).Selected = True
                cboBranch.Enabled = False
                'Me.RackFillLoc = RackFillHelper.GetRacks(GetConnectionString, Me.BranchID)

                'With cboRack
                '    .DataTextField = "Text"
                '    .DataValueField = "Value"
                '    .DataSource = Me.RackFillLoc
                '    .DataBind()
                '    .Items.Insert(0, "Select One")
                '    .Items(0).Value = "0"
                'End With
                'RefreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)



                cboRack.Enabled = False
                cboFill.Enabled = False

                cboRack.Visible = False
                cboFill.Visible = False
                rfvcboRack.Enabled = False
                rfvFilling.Enabled = False

            End If

        Else
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = ""
            End With
        End If
    End Sub


#Region "DoBind"
    Sub DoBind()
        Dim inCustID As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID2
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            hyCustomerName.Text = .Customername
            inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustID) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier( '" & "AssetDocument" & "','" & Server.UrlEncode(inSupId) & "' )"

            lblAssetDesc.Text = .assetDesc
            lblEngineNo.Text = .EngineNo
            Me.Chasis = .ChasisNo
            lblLicPlate.Text = .LicensePlate
            lblChasisNo.Text = .ChasisNo
            lblTaxDate.Text = .taxdate.ToString("dd/MM/yyyy")
            lblCurrRack.Text = .RackLoc
            lblCurrF.Text = .FillingLoc
            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
        End With
    End Sub
#End Region

#Region "DoBindGrid"
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "'and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()
    End Sub
#End Region



#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("PindahKeCustodian.aspx")
    End Sub
#End Region


#Region "DataBound"

    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound
        Dim inlblAssetDocStatus As Label

        If e.Item.ItemIndex >= 0 Then
            inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            'If inlblAssetDocStatus.Text.Trim = "F" Then OH += 1
            If inlblAssetDocStatus.Text.Trim = "O" Then OH += 1
        End If

        Me.TotOH = OH
    End Sub
#End Region



#Region "Save"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            'If (cboRack.SelectedItem.Text = lblCurrRack.Text.Trim) And (cboFill.SelectedItem.Text.Trim = lblCurrF.Text.Trim) Then
            '    ShowMessage(lblMessage, "Harap pilih Lokasi Lain", True)
            '    btnSave.Visible = False
            '    Exit Sub
            'End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .AssetSeqNo = Me.AssetSeqNo
                .ApplicationId = Me.ApplicationID
                .BranchId = Me.BranchID2
                .BusinessDate = Me.BusinessDate
                .BranchInTransit = cboBranch.SelectedItem.Value.Trim
                .RackLoc = "0"
                .FillingLoc = "0"
                'If cboRack.SelectedItem.Value.Trim = "" Then
                '    .RackLoc = ""
                '    .FillingLoc = ""
                'Else
                '    .FillingLoc = cboFill.SelectedItem.Value.Trim ' hdnChildValue.Value.Trim
                '    .RackLoc = cboRack.SelectedItem.Value
                'End If
            End With
            Try
                oCustomClass = oController.DocChangeLocSave(oCustomClass)
                If oCustomClass.strError <> "" Then
                    ShowMessage(lblMessage, "Update lokasi Gagal", True)
                    Exit Sub
                Else
                    Response.Redirect("PindahKeCustodian.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                lblMessage.Visible = True
                Exit Sub

            End Try
        End If
    End Sub
#End Region

End Class