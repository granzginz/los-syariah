﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PindahKeCustodianList.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.PindahKeCustodianList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DocChangeLocList</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnChild" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3> PINDAH KE CUSTODIAN DOKUMEN ASSET </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="hyAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="hyCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDesc" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Supplier
            </label>
            <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                <asp:Label ID="serialno1label" runat="server"></asp:Label>
            </label>
            <asp:Label ID="lblChasisNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="serialno2label" runat="server"></asp:Label>
            </label>
            <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polisi
            </label>
            <asp:Label ID="lblLicPlate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal STNK
            </label>
            <asp:Label ID="lblTaxDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Funding Bank
            </label>
            <asp:Label ID="lblFundingCoName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Status Jaminan
            </label>
            <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                PINDAH LOKASI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Cabang</label>
            <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true"  >
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ErrorMessage="Harap pilih Cabang"
                ControlToValidate="cboBranch" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lokasi Rak Sekarang
            </label>
            <asp:Label ID="lblCurrRack" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <%--<label>
                Lokasi Rak Tujuan
            </label>--%>
            <asp:DropDownList ID="cboRack" runat="server" AutoPostBack="true" >
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvcboRack" runat="server" Display="Dynamic" InitialValue="0"
                ErrorMessage="Harap Pilih Rak" ControlToValidate="cboRack" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lokasi Filling Sekarang
            </label>
            <asp:Label ID="lblCurrF" runat="server"></asp:Label>
        </div>
        <div class="form_right">
          <%--  <label>
                Lokasi Filling Tujuan
            </label>--%>
            <asp:DropDownList ID="cboFill" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvFilling" runat="server" Display="Dynamic" InitialValue="0"
                ErrorMessage="Harap pilih Lokasi Filling" ControlToValidate="cboFill" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DAFTAR DOKUMEN ASSET
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">
                            <ItemTemplate>
                                <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID DOKUMEN" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOKUMEN">
                            <ItemTemplate>
                                <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN">
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%#Container.DataItem("documentno")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="docreceiveddate" HeaderText="TGL TERIMA PERTAMA" DataFormatString="{0:dd/MM/yyyy}">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ASSETSTATUS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatus")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ISMAINDOC" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblisMainDoc" runat="server" Text='<%#Container.DataItem("isMainDoc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
