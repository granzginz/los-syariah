﻿Imports Maxiloan.Controller
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Webform.UserController

Public Class DocGenerateBarcodeViewer
    Inherits Maxiloan.Webform.WebBased

    Private ocustomclass As New Parameter.GeneralPaging
    Private ocontroller As New GeneralPagingController

    Public Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Public Property JmlRecorddicheck() As String
        Get
            Return CStr(ViewState("JmlRecorddicheck"))
        End Get
        Set(ByVal Value As String)
            ViewState("JmlRecorddicheck") = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return CStr(ViewState("Status"))
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub
        BindReport()
    End Sub


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PrintDocGenerateBarcode")
        Me.JmlRecorddicheck = cookie.Values("JmlRecorddicheck")
    End Sub

    Sub BindReport()
        GetCookies()

        Dim ds As New DataSet
        Dim objReport As DocGenerateBarcodePrint = New DocGenerateBarcodePrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions


        ds = GetData()
        objReport.SetDataSource(ds)
        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()
        CrystalReportViewer1.RefreshReport()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "DocGenerateBarcode.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("DocGenerateBarcode.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "DocGenerateBarcode")
    End Sub

    Public Function GetData() As DataSet
        Dim ds As New DataSet
        Dim adapter As New SqlDataAdapter
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)
        Dim iloop As Integer = 0

        Try
            objCommand.CommandType = CommandType.Text
            objCommand.Connection = objcon
            objCommand.CommandText = "SELECT a.BranchID,b.BranchFullName,a.ApplicationId, AgreementNo,a.CustomerID,c.Name,GoLiveDate,Tenor,am.Description AS Kendaraan,aa.LicensePlate FROM Agreement AS a " &
                                        " INNER JOIN dbo.Branch AS b ON b.BranchID=a.BranchID " &
                                        " LEFT JOIN dbo.AgreementAsset AS aa WITH (NOLOCK) ON aa.BranchID=a.BranchID AND aa.ApplicationID=a.ApplicationID " &
                                        " LEFT JOIN dbo.AssetMaster AS am WITH (NOLOCK) ON am.AssetCode=aa.AssetCode " &
                                        " LEFT JOIN dbo.Customer AS c ON c.CustomerID=a.CustomerID WHERE a.ApplicationId IN ('" & Replace(Me.JmlRecorddicheck, ",", "','") & "')"
            objCommand.CommandTimeout = 120

            adapter.SelectCommand = objCommand
            adapter.Fill(ds)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try
        Return ds
    End Function
End Class