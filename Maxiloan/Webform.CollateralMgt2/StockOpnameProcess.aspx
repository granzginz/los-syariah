﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StockOpnameProcess.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.StockOpnameProcess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Proses Stock Opname</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script  type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>

     <script type="text/javascript">
         $(document).ready(function () {
             $('#btnProses').click(function () {
                 if (confirm('Yakin Mau Melakukan Proses?'))
                     return true; 
                 return false;
             });
         });
         </script>

</head>
<body>
    <form id="form1" runat="server">
     
     <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip"> </div>
        <div class="form_single">
            <h3>STOCK OPNAME</h3>
        </div>
    </div>

     <asp:Panel ID="pnlUpload" runat="server">
         
    <div class="form_box_title"  >
        <div class="form_box_single"> <h4>Lokasi Stock Opname</h4></div>
    </div>

     <div class="form_box">
         <div class="form_single">
         <label class ="label_req"> Cabang </label>
         <asp:DropDownList ID="cbobranch" runat="server" />
         <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" ControlToValidate="cbobranch" ErrorMessage="Harap Pilih Cabang" InitialValue="0" CssClass="validator_general" />
        </div>
     </div>

      <div class="form_box">
           <div class="form_single">
             <label class ="label_req"> Tanggal Stok Opname</label>
             <asp:TextBox runat="server" ID="txttglOpname" CssClass="small_text"  OnTextChanged="txttglOpnameChanged" AutoPostBack="true"/> 
             <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txttglOpname" Format="dd/MM/yyyy" />
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Isi tanggal Opname!" Display="Dynamic" ControlToValidate="txttglOpname" CssClass="validator_general" />
         </div>
     </div>

      <div class="form_box">
           <div class="form_single">
           <label class ="label_req">Stok Opname No.</label> 
            <asp:Textbox runat="server" ID="hdnStokOpNo"  style="display:none;" />
           <asp:label runat="server" ID="lblStokOpNo" Enabled="false" CssClass="medium_text" /> 
         <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="Tidak ditemukan Opname" Display="Dynamic" ControlToValidate="hdnStokOpNo" CssClass="validator_general" /> 
     </div>
      </div>

     <div class="form_box">
           <div class="form_single">
             <label class ="label_req"> Tanggal Stok Opname Actual</label> 
             <asp:TextBox runat="server" ID="txttglOpnameAct" CssClass="small_text"  OnTextChanged="txttglOpnameActChanged" AutoPostBack="true"/> 
             <asp:CalendarExtender ID="CalendarExtender21" runat="server" TargetControlID="txttglOpnameAct" Format="dd/MM/yyyy" />
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Isi tanggal Opname!" Display="Dynamic" ControlToValidate="txttglOpnameAct" CssClass="validator_general" />
         </div>
     </div>

      <div class="form_box">
           <div class="form_single">
                <asp:Textbox runat="server" ID="hdnStokOpNoAct"  style="display:none;" />
           <label class ="label_req">Act. Stok Opname No.</label> 
           <asp:label runat="server" ID="lblActStokOpName" Enabled="false" CssClass="medium_text" /> 
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Tidak ditemukan Opname" Display="Dynamic" ControlToValidate="hdnStokOpNoAct" CssClass="validator_general" /> 
     </div>
      </div>
       <div class="form_button">
            <asp:Button ID="btnProses" runat="server" Text="Proses" CssClass="small button blue" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>

     </asp:Panel>


      <asp:Panel ID="pnlConfirmGrid" runat="server">

        <div class="form_box">
            <div class="form_left">
                <label>BranchOnStatus</label> 
                <asp:Label ID="lblBranchOn" class='lblNum' runat="server" />
            </div> 
            <div class="form_right">
                <label>Tanggal </label>
                <asp:Label ID="lblTglOpname" runat="server" />   
            </div>
        </div>

       <div class="form_box">
            <div class="form_left">
                <label>No. StockOpname</label> 
                <asp:Label ID="lblStockOpnameNo" class='lblNum' runat="server" />
             </div>
            <div class="form_right">
               <label>Jumlah OnHand</label>
                <asp:Label ID="lblTotalOnhand" runat="server" /> 
            </div>
        </div>

        <div class="form_box">
            <div class="form_left">
                <label>No. StockOpname Actual</label> 
                <asp:Label ID="lblStockOpnameNoAct" class='lblNum' runat="server" />
             </div>
            <div class="form_right">
               <label>Jumlah Actual</label>
                <asp:Label ID="lblTotalActual" runat="server" /> 
            </div>
        </div>


       <div class="form_box">
            <div class="form_left">
                <label>Tanggal Proses</label> 
                <asp:Label ID="lbldtmAct"  runat="server" />
             </div>
            <div class="form_right">
               <label>Total Variance</label>
                <asp:Label ID="lblVariance" runat="server" /> 
            </div>
        </div>


       <div class="form_box_title"  >
            <div class="form_box_single"> <h4>Daftar Dokumen Onhand yang selisih dengan ACTUAL </h4></div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0" BorderStyle="None" CssClass="grid_general"  AllowSorting="True">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:BoundColumn DataField="branchid" HeaderText="BRANCH" /> 
                    <asp:BoundColumn DataField="Applicationid" HeaderText="APLIKASI" />
                    <asp:BoundColumn DataField="AssetDocID" HeaderText="Asset Doc." />
                    <asp:BoundColumn DataField="DocumentNo" HeaderText="No. Dokumen" /> 
                    <asp:BoundColumn DataField="LicensePlate" HeaderText="NO. POLISI" />
                    <asp:BoundColumn DataField="AssetDocRack" HeaderText="RAK" />
                    <asp:BoundColumn DataField="AssetDocFilling" HeaderText="FILLING" />
                    <asp:BoundColumn DataField="OwnerAsset" HeaderText="PEMILIK" />
                 </Columns>
                </asp:DataGrid>
                   <uc2:ucGridNav id="GridNavigator" runat="server"/>  
                </div>
            </div>
          </div>


           <div class="form_box_title"  >
            <div class="form_box_single"> <h4>Daftar Dokumen Actual yang selisih dengan ONHAND</h4></div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgreeAct" runat="server" AutoGenerateColumns="False" BorderWidth="0" BorderStyle="None" CssClass="grid_general"  AllowSorting="True">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:BoundColumn DataField="branchid" HeaderText="BRANCH" /> 
                    <asp:BoundColumn DataField="Applicationid" HeaderText="APLIKASI" />
                    <asp:BoundColumn DataField="AssetDocID" HeaderText="Asset Doc." />
                 <%--   <asp:BoundColumn DataField="DocumentNo" HeaderText="No. Dokumen" /> --%>
                    <asp:BoundColumn DataField="LicensePlate" HeaderText="NO. POLISI" />
     <%--               <asp:BoundColumn DataField="AssetDocRack" HeaderText="RAK" />
                    <asp:BoundColumn DataField="AssetDocFilling" HeaderText="FILLING" />--%>
                    <asp:BoundColumn DataField="OwnerAsset" HeaderText="PEMILIK" />
                 </Columns>
                </asp:DataGrid>
                   <uc2:ucGridNav id="GridNavigator1" runat="server"/>  
                </div>
            </div>
          </div>


         <div class="form_button"> 
                <asp:Button ID="btnCancelProses" runat="server" Text="OK" CssClass="small button gray"  CausesValidation="False" />
            </div>
        </asp:Panel>
    </form>
</body>
</html>
