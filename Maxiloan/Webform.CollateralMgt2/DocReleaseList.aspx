﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocReleaseList.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.DocReleaseList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocReleaseList</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENYERAHAN DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="hyAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="hyCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDesc" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Supplier
            </label>
            <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cross Default
            </label>
            <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Funding Bank
            </label>
            <asp:Label ID="lblFundingCoName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Status Jaminan
            </label>
            <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                REGISTRATION
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">            
                <label>
                    Tgl Penyerahan<label class="mandatory">
                    *</label>
                </label>                
            <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">            
                <label>
                    <asp:Label ID="serialno1label" runat="server"></asp:Label>
                </label>                
            <asp:TextBox ID="txtChasisNo" runat="server" MaxLength="20" CssClass="inptype"></asp:TextBox>
<%--            <asp:RequiredFieldValidator
                ID="Requiredfieldvalidator2" runat="server" Visible="true" ControlToValidate="txtChasisNo"
                ErrorMessage="Harap isi No Rangka" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>--%>
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="serialno2label" runat="server"></asp:Label>
            </label>
            <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
     <asp:Panel ID="pnlMobiltipe" runat="server">
 
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polisi
            </label>
            <asp:Label ID="lblLicPlate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal STNK
            </label>
            <asp:Label ID="lblTaxDate" runat="server"></asp:Label>
        </div>
    </div>
             </asp:Panel>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lokasi Rak
            </label>
            <asp:Label ID="lblRack" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Lokasi Filling
            </label>
            <asp:Label ID="lblFLoc" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">            
                <label class="label_general">Catatan / Dokumen lainnya</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
   
     <div class="form_box_header">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DAFTAR DOKUMEN ASSET
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOCUMENT ID" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOKUMEN">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="35%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%#Container.DataItem("documentno")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="docreceiveddate" HeaderText="TGL TERIMA PERTAMA" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ASSETSTATUS" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatus")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ISMAINDOC" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblisMainDoc" runat="server" Text='<%#Container.DataItem("isMainDoc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                INFORMASI KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgInfo" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle HorizontalAlign="Center" Width="6%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="APPLICATIONID" Visible="False" SortExpression="ApplicationID">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblAppID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="AgreementNo">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblAgreeNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CUSTOMERID" SortExpression="CustomerID" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCustID" runat="server" Text='<%#Container.DataItem("CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH TUNGGAKAN" SortExpression="ODAmount">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblOvdAmount" runat="server" Text='<%#formatnumber(Container.DataItem("ODAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SISA POKOK" SortExpression="OSAmount">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblOSAmount" runat="server" Text='<%#formatnumber(Container.DataItem("OSAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS KONTRAK" SortExpression="ContractStatus">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("Contractstatus")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
