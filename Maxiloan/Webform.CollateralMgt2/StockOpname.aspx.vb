﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class StockOpname 
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New DataUserControlController
    Private oController As New StockOpnameDocController
    Protected WithEvents GridNavigator As ucGridNav
    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 10
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1

    Protected Property StockOpnameNo() As String
        Get
            Return CType(ViewState("SESSIONID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SESSIONID") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            Me.FormID = "STOCKOPNM"

            Dim dt_branch As New DataTable
            'dt_branch = m_controller.GetBranchAll(GetConnectionString)
            'With cbobranch
            '    .DataTextField = "Name"
            '    .DataValueField = "ID"
            '    .DataSource = dt_branch
            '    .DataBind()
            'End With
            With cbobranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "ALL"
                    .Enabled = True
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Enabled = False
                End If
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
            'cbobranch.Enabled = False
            If (Not Me.IsHoBranch) Then
                cbobranch.Enabled = False
            End If

            txttglOpname.Text = BusinessDate.ToString("dd/MM/yyyy")
            txtJamOpname.Text = DateTime.Now.Hour.ToString()
            txtMenitOpname.Text = DateTime.Now.Minute.ToString()

            pnlUpload.Visible = True
            pnlConfirmGrid.Visible = False
        End If
    End Sub

    Private Sub CancelProses(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelProses.Click
        Response.Redirect("StockOpname.aspx")
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("StockOpname.aspx")
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        doBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Private provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProses.Click
        Dim dt = ConvertDate2(txttglOpname.Text).ToString("yyyyMMdd")
        Dim rows As New Parameter.StockOpDoc

        rows.OpnameDate = Date.ParseExact(String.Format("{0}{1}{2}00", dt, txtJamOpname.Text, txtMenitOpname.Text), "yyyyMMddHHmmss", provider)
        rows.Branchid = cbobranch.SelectedValue
        Try
            StockOpnameNo = oController.DoCreateStockOpname(GetConnectionString, rows)
            If (StockOpnameNo = "") Then
                ShowMessage(lblMessage, "Gagal Create record stock upname", True)
            End If
            doBind()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try 
    End Sub


    Private Sub doBind(Optional isFrNav As Boolean = False)

        Dim result = oController.GetResultOpnamePage(GetConnectionString(), currentPage, pageSize, StockOpnameNo) 
        recordCount = result.TotalRecords 
        DtgAgree.DataSource = result.DataSetResult.Tables(0)
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        lblTotalOnhand.Text = result.DataSetResult.Tables(1).Rows(0)("QtyOnHand").ToString
        lblStockOpnameNo.Text = result.DataSetResult.Tables(1).Rows(0)("StockOpnameNo")

        lblTglOpname.Text =ctype(result.DataSetResult.Tables(1).Rows(0)("OpnameDate"),DateTime).ToString("dd/MM/yyyy HH:mm")
        lblBranchOn.Text = result.DataSetResult.Tables(1).Rows(0)("BranchOnStatus")
 
        pnlUpload.Visible = False
        pnlConfirmGrid.Visible = True
        lblMessage.Text = ""
    End Sub
End Class