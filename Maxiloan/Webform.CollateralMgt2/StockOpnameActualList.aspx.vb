﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class StockOpnameActualList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Private oController As New StockOpnameDocController
#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property StockOpnameNo() As String
        Get
            Return CStr(ViewState("StockOpnameNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("StockOpnameNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private totalPages1 As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
#End Region
#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            Me.StockOpnameNo = Request("StockOpnameNo")
            doBind()
        End If

    End Sub
#End Region

#Region "DoBind"

    Private Sub doBind(Optional isFrNav As Boolean = False)

        Dim result = oController.GetResultActualOpnamePage(GetConnectionString(), currentPage, pageSize, Me.StockOpnameNo)
        recordCount = result.TotalRecords
        DtgAgree.DataSource = result.DataSetResult.Tables(0)
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        lblTotalQtyActual.Text = result.DataSetResult.Tables(1).Rows(0)("QtyActual").ToString
        lblStockOpnameNo.Text = result.DataSetResult.Tables(1).Rows(0)("StockOpnameNo")

        lblTglOpname.Text = CType(result.DataSetResult.Tables(1).Rows(0)("OpnameDate"), DateTime).ToString("dd/MM/yyyy HH:mm")
        lblBranchOn.Text = result.DataSetResult.Tables(1).Rows(0)("BranchOnStatus")

        lblMessage.Text = ""
    End Sub
#End Region

    Private Sub CancelProses(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelProses.Click
        Response.Redirect("StockOpnameActual.aspx")
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        doBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
End Class
