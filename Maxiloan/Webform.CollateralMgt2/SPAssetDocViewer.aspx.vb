﻿
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class SPAssetDocViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(ViewState("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
        '        CreateData()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPAsset As New DataSet

        Dim objReport As SPAssetDocPrint = New SPAssetDocPrint
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhere
                .ApplicationId = Me.ApplicationID
                .BranchId = Me.Branch_ID
                .SortBy = Me.SortBy
            End With

            oCustomClass = m_coll.CreateSPAssetDocument(oCustomClass)
            dsSPAsset = oCustomClass.listReport
            objReport.SetDataSource(dsSPAsset)

            CrystalReportViewer1.ReportSource = objReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "SPAssetDocPrint.pdf"

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
            DiskOpts.DiskFileName = strFileLocation

            With objReport.ExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With

            With objReport
                .ExportOptions.DestinationOptions = DiskOpts
                .Export()
                .Close()
                .Dispose()
            End With

            CrystalReportViewer1.Dispose()
            Response.Redirect("SPAssetDoc.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "SPAssetDocPrint")

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
#End Region
#Region "CreateData"
    Private Sub CreateData()
        Dim m_controller As New DocReceiveController
        Dim oData As New DataSet
        Dim oMainDoc As New Parameter.DocRec
        oMainDoc.strConnection = GetConnectionString()
        oMainDoc.BranchId = Me.BranchID
        oMainDoc.ApplicationId = Me.ApplicationID
        oMainDoc.SortBy = Me.SortBy
        oMainDoc = m_controller.CreateSPAssetDocument(oMainDoc)
        oData = oMainDoc.listReport
        oData.WriteXmlSchema("D:\xmd_SPAssetDoc.xmd")
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("SPAssetDocPrint")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.Branch_ID = cookie.Values("BranchID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("SPAssetDoc.aspx")
    End Sub

End Class