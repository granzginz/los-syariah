﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class InqDokumenAssetBNI
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oaSearchBy As UcSearchBy
    Protected WithEvents oGridNavigator As ucGridNav


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
#End Region

#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler oGridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), False)
            End If



            Dim dtbranch As New DataTable

            With cbobranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "ALL"
                    .Enabled = True
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Enabled = False
                End If
            End With

            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True

            Me.FormID = "InqDocAssetBNI"
            oaSearchBy.ListData = "AgreementNo, No Kontrak-Nasabah, Nasabah-NoPolisi, No Polisi-NoBPKB, No BPKB"
            oaSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

#End Region

#Region "Search"

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

            Me.SearchBy = ""
            Me.SearchBy = " branchid = '" & cbobranch.SelectedItem.Value.Trim & "' "
            'Me.SearchBy = Me.SearchBy & " and  aga.assetstatus = 'RRA'"
            If oaSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oaSearchBy.ValueID & " like '%" & oaSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
            End If
            pnlDatagrid.Visible = True
            InqDocAsset.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

#End Region

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "SPDocAssetBNIPaging1"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        InqDocAsset.DataSource = DtUserList.DefaultView
        InqDocAsset.CurrentPageIndex = 0
        InqDocAsset.DataBind()
        'PagingFooter()
        If (isFrNav = False) Then
            oGridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub
#End Region

#Region "Navigation"

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        oGridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#End Region

#Region "Sort"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("InqDocAssetBNI.aspx")
    End Sub
#End Region

#Region "DataBound"
    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles InqDocAsset.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim HyappId As Label
        Dim inlblAssetSeqNo As New Label
        Dim m As Int32
        Dim view As HyperLink
        If e.Item.ItemIndex >= 0 Then


            HyappId = CType(e.Item.FindControl("LblAgreementNo"), Label)
            lblTemp = CType(e.Item.FindControl("lblNoRangka"), Label)
            view = CType(e.Item.FindControl("View"), HyperLink)
            view.NavigateUrl = "InqDocAssetBNIList.aspx?AgreementNo=" & HyappId.Text.Trim & "&branchid=" & cbobranch.SelectedItem.Value.Trim & "&NoRangka=" & lblTemp.Text.Trim


            'lblTemp = CType(e.Item.FindControl("lblNoRangka"), Label)
            'HyappId = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)

            'HyappId = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            'HyappId.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"

        End If
    End Sub
#End Region



End Class