﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController

Public Class DocAssetBNIList
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocAssetBNI
    Private oController As New DocReceiveController


#End Region

#Region "Property"

    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property

    Private Property NoRangka() As String
        Get
            Return (CType(ViewState("NoRangka"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("NoRangka") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return (CType(ViewState("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.BranchID = Request.QueryString("branchid")
            'Me.NoRangka = CInt(Request.QueryString("NoRangka"))
            Me.FormID = "DocAssetBNI"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind1()
                'DoBindGrid()
                'DoBindInfo()
            End If
        End If

    End Sub
#End Region

#Region "DoBind"
    Sub DoBind1()
        'Dim inCustid As String
        'Dim insupid As String
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.AgreementNo = Me.AgreementNo
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.ListDocAssetBNI(oCustomClass)
        With oCustomClass
            'inCustid = .CustomerID
            'Me.CustomerID = inCustid
            'hyAgreementNo.Text = .AgreementNo
            'hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            'hyCustomerName.Text = .Customername
            'hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustid) & "')"
            'insupid = .Supplierid
            'lblSupplier.Text = .SupplierName
            'lblSupplier.NavigateUrl = "javascript:OpenWinSupplier(  '" & "AssetDocument" & "','" & Server.UrlEncode(insupid) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblNoMesin.Text = .NoMesin
            lblNoRangka.Text = .NoRangka
            lblNoBPKB.Text = .NoBPKB
            lblNoPolisi.Text = .NoPolisi
            lblNamaDiBPKB.Text = .NamaDiBPKB
            LblTglTerimaBPKB.Text = .TglTerimaBPKB
            LblFillingId.Text = .FillingId
            LblRAKId.Text = .RAKId
            lblNasabah.Text = .Nasabah
            'lblEngineNo.Text = .EngineNo
            'Me.Chasis = .ChasisNo
            'lblLicPlate.Text = .LicensePlate
            'If .Itaxdate = "1" Then
            '    lblTaxDate.Text = .taxdate.ToString("dd/MM/yyyy")
            'Else
            '    lblTaxDate.Text = "-"
            'End If
            'lblCrossDefault.Text = .CrossDefault

            btnSave.Visible = True
            'lblRack.Text = .RackLoc
            'lblFLoc.Text = .FillingLoc
            'serialno1label.Text = .seriallabel1
            'serialno2label.Text = .seriallabel2
            'txtChasisNo.Text = .ChasisNo.Trim
            'lblFundingCoName.Text = .FundingCoName
            'lblFundingPledgeStatus.Text = .FundingPledgeStatus
            'Me.AssetTypeID = .AssetTypeID
            'If Me.AssetTypeID <> "MOBIL" Then
            '    pnlMobiltipe.Visible = False
            'Else
            '    pnlMobiltipe.Visible = True
            'End If
        End With
    End Sub
#End Region

#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("DocAssetBNI.aspx")
    End Sub
#End Region

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "Save", Me.AppId) Then
        Try
            Dim oParameter As New Parameter.DocAssetBNI
            Dim oNewPosition As New Parameter.DocAssetBNI

            With oParameter
                .AgreementNo = lblAgreementNo.Text
                .CatatanClose = txtNotes.Text
                .TglClose = ConvertDate2(txtsdate.Text)
                .strConnection = GetConnectionString()


            End With

            oController.SaveDocAssetBNI(oParameter)

            Response.Redirect("DocAssetBNI.aspx?message=Data Berhasil Disimpan")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        'oPosition.strConnection = GetConnectionString()
        '    oPosition.BranchId = Me.BranchID
        '    'oPosition.sesBranchID = Me.BranchID
        '    oPosition.AgreementNo = Me.AgreementNo
        '    oNewPosition = oController.GetBPKBPosition(oPosition)
        'Dim strBranchName As String
        'Dim strBranchID As String
        'strBranchID = oNewPosition.BranchId
        'strBranchName = oNewPosition.BranchName
        'If strBranchID <> Me.BranchID Then
        '    ShowMessage(lblMessage, "Anda tidak Berhak,  Dokumen ada di cabang " & strBranchName & "!", True)
        '    Exit Sub
        'End If

        'If Me.AssetTypeID = "MOBIL" Then
        '    If Me.Chasis <> txtChasisNo.Text.Trim Then
        '        ShowMessage(lblMessage, "No Rangka Salah", True)
        '        Exit Sub
        '    End If
        'End If


        'If DtgDoc.Items.Count > 0 Then
        '    Dim i As Integer
        '    Dim j As Integer
        '    Dim k As Integer
        '    Dim dtAdd As New DataTable
        '    Dim dr As DataRow
        '    Dim isRec As New Label
        '    Dim inDocname As New Label
        '    Dim inDocNo As New TextBox
        '    Dim inReceiveddate As String
        '    Dim inAssetDocStatus As New Label
        '    Dim inStatusDate As String
        '    Dim inChkSlct As New CheckBox
        '    Dim inisMainDoc As New Label
        '    Dim inlblassetdocid As New Label
        '    Dim strGroupID As String

        '    strGroupID = ""
        '    dtAdd.Columns.Add("AssetDocStatus", System.Type.GetType("System.String"))
        '    dtAdd.Columns.Add("IsMainDoc", System.Type.GetType("System.String"))
        '    dtAdd.Columns.Add("AssetDocID", System.Type.GetType("System.String"))

        '    k = 0
        '    For i = 0 To DtgDoc.Items.Count - 1
        '        inAssetDocStatus = CType(DtgDoc.Items(i).FindControl("lblAssetDocStatus"), Label)
        '        inisMainDoc = CType(DtgDoc.Items(i).FindControl("lblisMainDoc"), Label)
        '        inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)
        '        dr = dtAdd.NewRow()
        '        dr("AssetDocStatus") = inAssetDocStatus.Text.Trim
        '        dr("IsMainDoc") = CStr(inisMainDoc.Text.Trim)
        '        If inisMainDoc.Text.Trim = "True" Then
        '            k = k + 1
        '        End If
        '        dr("AssetDocID") = inlblassetdocid.Text.Trim
        '        strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & inlblassetdocid.Text.Replace("'", "") & "'"
        '        dtAdd.Rows.Add(dr)
        '    Next


        '    With oCustomClass
        '        .strConnection = GetConnectionString()
        '        .listData = dtAdd
        '        .BranchId = Me.BranchID2
        '        .ApplicationId = Me.ApplicationID
        '        .ReleaseDate = ConvertDate2(txtsdate.Text)
        '        .ChasisNo = txtChasisNo.Text.Trim
        '        .BusinessDate = Me.BusinessDate
        '        .AssetSeqNo = Me.AssetSeqNo
        '        .Notes = txtNotes.Text
        '        If k > 0 Then
        '            .isMainDoc = 1
        '        Else
        '            .isMainDoc = 0
        '        End If
        '        .strGroupId = strGroupID.Trim
        '    End With
        '    Try
        '        oCustomClass = oController.DocReleaseSave(oCustomClass)
        '        If oCustomClass.strError <> "" Then
        '            ShowMessage(lblMessage, "Update Pelepasan Dokumen Gagal", True)
        '            Exit Sub
        '        Else
        '            'Response.Redirect("DocRelease.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
        '            Dim cookie As HttpCookie = Request.Cookies("rptBPKBSerahTerima")
        '            If Not cookie Is Nothing Then
        '                cookie.Values("BranchID") = Me.BranchID2.Trim
        '                cookie.Values("ApplicationID") = Me.ApplicationID
        '                Response.AppendCookie(cookie)
        '            Else
        '                Dim cookieNew As New HttpCookie("rptBPKBSerahTerima")
        '                cookieNew.Values.Add("BranchID", Me.BranchID2.Trim)
        '                cookieNew.Values.Add("ApplicationID", Me.ApplicationID.Trim)
        '                Response.AppendCookie(cookieNew)
        '            End If
        '            Response.Redirect("BPKBSerahTerimaViewer.aspx")
        '        End If
        '    Catch ex As Exception
        '        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
        '        lblMessage.Visible = True
        '        Exit Sub

        'End Try
        'End If
        'End If
    End Sub

End Class