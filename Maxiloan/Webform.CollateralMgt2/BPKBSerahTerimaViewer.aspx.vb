
#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller

#End Region

Public Class BPKBSerahTerimaViewer
    Inherits Maxiloan.webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents crvConfirmationLetter As CrystalDecisions.Web.CrystalReportViewer
    Protected WithEvents crvBPKBSerahTerima As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property BDate() As String
        Get
            Return CType(viewstate("BDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BDate") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New DocReceiveController
        Dim oBPKB As New Parameter.DocRec

        Dim objReport As BPKBSerahTerima = New BPKBSerahTerima
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oBPKB.strConnection = GetConnectionString
        oBPKB.BranchId = Me.BranchID
        oBPKB.ApplicationId = Me.ApplicationID
        oBPKB = m_controller.PrintBPKBSerahTerima(oBPKB)
        oData = oBPKB.listReport

        objReport.SetDataSource(oBPKB.listReport)
        crvBPKBSerahTerima.ReportSource = objReport

        '========================================================
        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("CompanyName")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.sesCompanyName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Dim strHTTPServer As String
        'Dim StrHTTPApp As String
        'Dim strNameServer As String
        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_BPKBSerahTerima.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        Response.Redirect("DocRelease.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_BPKBSerahTerima")
        ''=====================================
        'strHTTPServer = Request.ServerVariables("PATH_INFO")
        'strNameServer = Request.ServerVariables("SERVER_NAME")
        'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" + Me.Session.SessionID + Me.Loginid + "rpt_ConfLetter.pdf"
        'Call file PDF 

        'Response.Write("<script language = javascript>" & vbCrLf _
        '            & "history.back(-1) " & vbCrLf _
        '            & "window.open('" & strFileLocation & "') " & vbCrLf _
        '            & "</script>")
        'Response.Redirect("PPK.aspx")
    End Sub
#End Region
#Region "CreateData"
    'Private Sub CreateData()
    '    Dim m_controller As New DocReceiveController
    '    Dim oData As New DataSet
    '    Dim oBPKB As New Parameter.DocRec
    '    oBPKB.strConnection = GetConnectionString
    '    oBPKB.BranchId = Me.BranchID
    '    oBPKB.ApplicationId = Me.ApplicationID
    '    oBPKB = m_controller.PrintBPKBSerahTerima(oBPKB)
    '    oData = oBPKB.ListReport
    '    oData.WriteXmlSchema("D:\project\Tunas\EStarTunas\AssetMgmt\xmd_BPKBSerahTerima.xmd")
    'End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("rptBPKBSerahTerima")
        Me.BranchID = cookie.Values("BranchID")
        Me.ApplicationID = cookie.Values("ApplicationID")
    End Sub
#End Region

End Class
