﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Globalization
#End Region

Public Class HasilCekBPKB
    Inherits Maxiloan.Webform.WebBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController    

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            txtPage.Text = "1"
            If CheckForm(Me.Loginid, "HCEKBPKB", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.Sort = "Name ASC"
            Me.CmdWhere = "All"
            BindGridEntity(Me.CmdWhere)
            pnlEdit.Visible = False
            pnlList.Visible = True
        End If
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable    
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .SpName = "spHasilCekBpkbPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        
        If Not oCustomClass Is Nothing Then
            dtEntity = oContract.ListData
            recordCount = oContract.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim lblAgreementNo As New Label
        Dim lblName As New Label
        Dim lblNoBPKB As New Label
        Dim lblTglBPKB As New Label
        Dim lblNamaBPKB As New Label        
        Dim lblApplicationID As New Label
        Dim lblHasilCekBPKB As New Label
        Dim lblHasilCekNote As New Label

        If e.CommandName = "Edit" Then
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            lblName = CType(e.Item.FindControl("lblName"), Label)
            lblNoBPKB = CType(e.Item.FindControl("lblNoBPKB"), Label)
            lblTglBPKB = CType(e.Item.FindControl("lblTglBPKB"), Label)
            lblNamaBPKB = CType(e.Item.FindControl("lblNamaBPKB"), Label)            
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblHasilCekBPKB = CType(e.Item.FindControl("lblHasilCekBPKB"), Label)
            lblHasilCekNote = CType(e.Item.FindControl("lblHasilCekNote"), Label)

            lblNoKontrakE.Text = lblAgreementNo.Text
            lblNamaE.Text = lblName.Text
            lblNoBPKBE.Text = lblNoBPKB.Text
            lblTglBPKBE.Text = IIf(lblTglBPKB.Text.Contains("1900"), "", CDate(lblTglBPKB.Text).ToString("dd/MM/yyy")).ToString
            lblNamaBPKBE.Text = lblNamaBPKB.Text
            lblAlamatBPKBE.Text = e.Item.Cells(6).Text
            hdfApplicationID.Value = lblApplicationID.Text.Trim
            hdfHasilCekBPKB.Value = lblHasilCekBPKB.Text.Trim
            hdfHasilCekNote.Value = lblHasilCekNote.Text.Trim

            If hdfHasilCekBPKB.Value.Trim <> "" Then
                If CBool(hdfHasilCekBPKB.Value) Then
                    cboHasilPeriksa.SelectedIndex = cboHasilPeriksa.Items.IndexOf(cboHasilPeriksa.Items.FindByValue("1"))
                Else
                    cboHasilPeriksa.SelectedIndex = cboHasilPeriksa.Items.IndexOf(cboHasilPeriksa.Items.FindByValue("0"))
                End If
            Else
                cboHasilPeriksa.SelectedIndex = 0
            End If

            ScriptManager.RegisterStartupScript(cboHasilPeriksa, GetType(Page), cboHasilPeriksa.ClientID, String.Format("cboHasilPeriksa_onchange('{0}');", cboHasilPeriksa.SelectedValue.Trim), True)
            txtCatatan.Text = lblHasilCekNote.Text.Trim

            pnlEdit.Visible = True
            pnlList.Visible = False
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Search"
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            If cboSearch.SelectedIndex = 0 Then
                Search = Replace(txtSearch.Text.Trim, "'", "''")
            Else
                Search = txtSearch.Text.Trim
            End If
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + Search + "%'"            
        Else
            Me.CmdWhere = "All"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        BindGridEntity(Me.CmdWhere)
        pnlEdit.Visible = False
        pnlList.Visible = True
        lblMessage.Visible = False
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblOldOwnerAddress As New Label
        Dim lblOldOwnerKelurahan As New Label
        Dim lblOldOwnerKecamatan As New Label
        Dim lblOldOwnerCity As New Label

        If e.Item.ItemIndex >= 0 Then
            lblOldOwnerAddress = CType(e.Item.FindControl("lblOldOwnerAddress"), Label)
            lblOldOwnerKelurahan = CType(e.Item.FindControl("lblOldOwnerKelurahan"), Label)
            lblOldOwnerKecamatan = CType(e.Item.FindControl("lblOldOwnerKecamatan"), Label)
            lblOldOwnerCity = CType(e.Item.FindControl("lblOldOwnerCity"), Label)

            e.Item.Cells(6).Text = CStr(IIf(lblOldOwnerAddress.Text.Trim <> "", lblOldOwnerAddress.Text.Trim, "") _
                                  + IIf(lblOldOwnerKelurahan.Text.Trim <> "", ", " & lblOldOwnerKelurahan.Text.Trim, "") _
                                  + IIf(lblOldOwnerKecamatan.Text.Trim <> "", ", " & lblOldOwnerKecamatan.Text.Trim, "") _
                                  + IIf(lblOldOwnerCity.Text.Trim <> "", ", " & lblOldOwnerCity.Text.Trim, ""))
            e.Item.Cells(4).Text = IIf(e.Item.Cells(4).Text.Contains("1900"), "", CDate(e.Item.Cells(4).Text).ToString("dd/MM/yyy")).ToString()
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        oCustomClass = New Parameter.DocRec

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .HasilCekBPKB = cboHasilPeriksa.SelectedValue
                .HasilCekNote = txtCatatan.Text.Trim
                .ApplicationId = hdfApplicationID.Value
            End With

            oCustomClass = oController.HasilCekBPKBSave(oCustomClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

        pnlEdit.Visible = False
        pnlList.Visible = True
    End Sub
End Class

