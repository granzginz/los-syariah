﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocStatusInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property


#End Region


    Private Property RackFillLoc() As IList(Of RackFill)
        Get
            Return (CType(ViewState("RackFillLoc"), IList(Of RackFill)))
        End Get
        Set(ByVal Value As IList(Of RackFill))
            ViewState("RackFillLoc") = Value
        End Set
    End Property

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), False)
            End If
            Me.FormID = "DOCSTATUSINQ"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                Dim dtbranch As New DataTable
                Dim dtBankAccount As New DataTable
                If Me.IsHoBranch Then
                    dtbranch = m_controller.GetBranchAll(GetConnectionString)
                Else
                    dtbranch = m_controller.GetBranchName2(GetConnectionString, Me.sesBranchId)
                End If

                With cbobranch
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
                'cbobranch.Enabled = False
                If (Not Me.IsHoBranch) Then
                    cbobranch.Enabled = False
                End If


                Me.RackFillLoc = RackFillHelper.GetRacks(GetConnectionString, Me.sesBranchId.Replace("'", "").Trim)
                With cboRack
                    .DataTextField = "Text"
                    .DataValueField = "Value"
                    .DataSource = Me.RackFillLoc
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                'Response.Write(GenerateScript(GetComboRack))

                bindComboCompany()
                cboRack.Visible = True
                lblRackloc.Visible = False
            End If
        Else
            cboRack.Visible = False
            lblRackloc.Visible = True
        End If
    End Sub

    Private Function GetComboRack() As DataTable
        Dim dtRack As New DataTable

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
        End With

        dtRack = oController.GetRackBranch(oCustomClass)
        Return dtRack

    End Function
#End Region
#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Sub
#End Region

#Region "RackChange"

    Public Function RackChange() As String
        Return "ParentChange('" & Trim(cbobranch.ClientID) & "','" & Trim(cboRack.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

#End Region

#Region "GenerateScript"

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cbobranch.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cbobranch.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("rackdesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("rackid")), "null", DataRow(i)("rackid"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("rackdesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("rackid")), "null", DataRow(i)("rackid"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spDocStatusInq"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataBound"

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim inlblAssetSeqNo As New Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink

        'Dim m As Int32
        Dim hypReceive As HyperLink
        'Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then

            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            inlblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)

            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hypReceive = CType(e.Item.FindControl("HypView"), HyperLink)
            hypReceive.NavigateUrl = "javascript:OpenViewDocument('" & Server.UrlEncode(HyappId.Text.Trim) & "', '" & Server.UrlEncode(cbobranch.SelectedItem.Value.Trim) & "', '" & Server.UrlEncode(inlblAssetSeqNo.Text.Trim) & "', '" & "AssetDocument" & "')"
            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub

#End Region

#Region "Reset"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DocStatusInq.aspx")
    End Sub
#End Region
#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Dim par As String
            par = ""

            Me.SearchBy = ""
            cboRack.Visible = False
            lblRackloc.Visible = True
            Me.Rack = hdnChildValue.Value.Trim
            If hdnChildName.Value.Trim = "" Then
                lblRackloc.Text = "ALL"
            Else
                lblRackloc.Text = hdnChildName.Value.Trim
            End If

            If Trim(cbobranch.SelectedValue) <> "000" Then
                Me.SearchBy = "  and aga.branchid = '" & cbobranch.SelectedItem.Value & "' "
                par = par & "Branch : " & cbobranch.SelectedItem.Text & " "
            End If


            If lblRackloc.Text <> "ALL" Then
                Me.SearchBy = Me.SearchBy & "  and  aga.assetdocrack = '" & Me.Rack & "'"
                If par <> "" Then
                    par = par & " , Rack : " & lblRackloc.Text.Trim & " "
                Else
                    par = par & "Rack : " & lblRackloc.Text.Trim & " "
                End If
            Else
                If Trim(cbobranch.SelectedValue) = "000" Then
                    Me.SearchBy = Me.SearchBy & "  and  isnull(aga.assetdocrack,'-') IN (select rackID from rackDocument where branchID = '000' union select '-')"
                    'Else
                    '    Me.SearchBy = Me.SearchBy & "  and  aga.assetdocrack IN (select rackID from rackDocument where branchID = '" & cbobranch.SelectedItem.Value & "')"
                End If
                par = par & " , Rack : ALL "
            End If




            If cboStatus.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & "  and  aga.assetdocstatus = '" & cboStatus.SelectedItem.Value & "'"
                If par <> "" Then
                    par = par & " , Status : " & cboStatus.SelectedItem.Text & " "
                Else
                    par = par & "Status : " & cboStatus.SelectedItem.Text & " "
                End If
            End If

            If drdCompany.SelectedItem.Value <> "All" Then
                Me.SearchBy = Me.SearchBy & " and agr.FundingBankID = '" & drdCompany.SelectedItem.Value.Trim & "'"
                If par <> "" Then
                    par = par & " , Funding Bank : " & drdCompany.SelectedItem.Text & " "
                Else
                    par = par & "Funding Bank : " & drdCompany.SelectedItem.Text & " "
                End If
            End If

            'Me.SearchBy = Me.SearchBy & " and aga.assetdocstatus in ('W','B','P')"
            If cboSearchBy.SelectedItem.Value <> "ALL" Then
                If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                    ShowMessage(lblMessage, "Harap isi Kondisi yang akan dicari", True)
                    lblMessage.Visible = True
                    Exit Sub
                Else

                    Me.SearchBy = Me.SearchBy & "  and " & cboSearchBy.SelectedItem.Value & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
                    'If par <> "" Then
                    '    par = par & " , " & cboSearchBy.SelectedItem.Value & " " & cboSearchIN.SelectedItem.Value & " " & txtSearchBy.Text.Trim & " "
                    'Else
                    '    par = par & "" & cboSearchBy.SelectedItem.Value & " " & cboSearchIN.SelectedItem.Value & " " & txtSearchBy.Text.Trim & ""
                    'End If
                    If par <> "" Then
                        par = par & " , " & cboSearchBy.SelectedItem.Value & " LIKE " & txtSearchBy.Text.Trim & " "
                    Else
                        par = par & "" & cboSearchBy.SelectedItem.Value & " LIKE " & txtSearchBy.Text.Trim & ""
                    End If

                End If
            End If
            'If oSearchBy.Text.Trim <> "" Then
            '    If Right(oSearchBy.Text.Trim, 1) = "%" Then
            '        Me.SearchBy = Me.SearchBy & "  and " & oSearchBy.ValueID & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
            '        If par <> "" Then
            '            par = par & " , " & oSearchBy.ValueID & " like " & oSearchBy.Text.Trim & " "
            '        Else
            '            par = par & "" & oSearchBy.ValueID & " like " & oSearchBy.Text.Trim & " "
            '        End If
            '    Else
            '        Me.SearchBy = Me.SearchBy & "  and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
            '        If par <> "" Then
            '            par = par & " , " & oSearchBy.ValueID & " = " & oSearchBy.Text.Trim & " "
            '        Else
            '            par = par & "" & oSearchBy.ValueID & " = " & oSearchBy.Text.Trim & " "
            '        End If
            '    End If

            'End If

            If Me.SearchBy.Trim <> "" Then
                Me.SearchBy = Right(Me.SearchBy, Len(Me.SearchBy) - 5)
            Else
                Me.SearchBy = ""
            End If

            Me.ParamReport = par
            pnlDatagrid.Visible = True
            DtgAsset.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

#End Region
#Region "Sort"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region


    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = GetComboRack()
        Dim i As Integer

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(cboRack.UniqueID, CStr(DtChild.Rows(i).Item("rackid")).Trim)
        Next

        Page.ClientScript.RegisterForEventValidation(cboRack.UniqueID, "")
        Page.ClientScript.RegisterForEventValidation(cboRack.UniqueID, "0")
        Page.ClientScript.RegisterForEventValidation(cboRack.UniqueID, "All")

        MyBase.Render(writer)
    End Sub

End Class