﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocStatusInq.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.DocStatusInq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocStatusInq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
			var hdnDetail;
			var hdndetailvalue;

			function ParentChange(pBranch, pRack, pHdnDetail, pHdnDetailValue, itemArray) {
			    hdnDetail = eval('document.forms[0].' + pHdnDetail);
			    HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
			    var i, j;
			    for (i = eval('document.forms[0].' + pRack).options.length; i >= 0; i--) {
			        eval('document.forms[0].' + pRack).options[i] = null

			    };
			    if (itemArray == null) {
			        j = 0;
			    }
			    else {
			        j = 1;
			    };
			    eval('document.forms[0].' + pRack).options[0] = new Option('ALL', '0');
			    if (itemArray != null) {
			        for (i = 0; i < itemArray.length; i++) {
			            eval('document.forms[0].' + pRack).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

			        };
			        eval('document.forms[0].' + pRack).selected = true;
			    }
			};

			function cboChildonChange(l, j) {
			    hdnDetail.value = l;
			    HdnDetailValue.value = j;
			}	
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnChild" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                               
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypView" runat="server" Text='VIEW'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NO POLISI">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="rackdesc" HeaderText="RAK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblRack" runat="server" Text='<%#Container.DataItem("rackdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="fillingdesc" HeaderText="FILLING">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblFilling" runat="server" Text='<%#Container.DataItem("fillingdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetdocstatusdesc" HeaderText="STATUS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" CssClass="InpType" Width="34px">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                            MinimumValue="1" ControlToValidate="txtpage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
      
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
               <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cbobranch" runat="server" onchange="<%#RackChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    ControlToValidate="cbobranch" ErrorMessage="Harap pilih Cabang" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Status
                </label>
                <asp:DropDownList ID="cboStatus" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="R">Release</asp:ListItem> 
                    <asp:ListItem Value="P">Prospect</asp:ListItem>
                    <asp:ListItem Value="W">Waiting</asp:ListItem>
                    <asp:ListItem Value="B">Borrow</asp:ListItem>
                    <asp:ListItem Value="I">Intransit</asp:ListItem>
                    <asp:ListItem Value="H">Hand Over</asp:ListItem>
                    <asp:ListItem Value="O">On Hand</asp:ListItem>
                    <asp:ListItem Value="C">Cancel</asp:ListItem> 
                    <asp:ListItem Value="G">On Ba'i/Mu'ajjir</asp:ListItem> 
                    <asp:ListItem Value="F">Funding</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Lokasi Rak
                </label>
                <asp:DropDownList ID="cboRack" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:Label ID="lblRackloc" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Funding Bank / Cabang
                </label>
                <asp:DropDownList ID="drdCompany" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">Semua</asp:ListItem>
                    <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="AgreementNO">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="aga.licenseplate">No Polisi</asp:ListItem>
                    <asp:ListItem Value="aga.SerialNo1">No Chasis</asp:ListItem>
                    <asp:ListItem Value="aga.SerialNo2">No Mesin</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <%--<asp:DropDownList ID="cboSearchIN" runat="server">
                    <asp:ListItem Value="In" Selected="True">In</asp:ListItem>
                    <asp:ListItem Value="Not In">Not In</asp:ListItem>
                </asp:DropDownList>--%>
                &nbsp;
                <asp:TextBox ID="txtSearchBy" CssClass="InpType" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
