﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocWaitInq.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.DocWaitInq" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../webform.UserController/UcSearchBy.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../webform.UserController/UcBranch.ascx" %>--%>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocWaitInq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnChild" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypView" runat="server" Text='VIEW'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SUPPLIER" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplier" runat="server" Text='<%#Container.DataItem("SupplierID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierName" HeaderText="SUPPLIER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HySuppname" runat="server" Text='<%#Container.DataItem("SupplierName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="APPLICATION ID" Visible="False">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NO POLISI">                               
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn SortExpression="expecteddate" DataField="expecteddate" HeaderText="TGL DIHARAPKAN"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="Aging" DataField="Aging" HeaderText="AGING">                                
                            </asp:BoundColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" CssClass="InpType" Width="34px">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtpage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                            MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtpage" ErrorMessage="No Halaman Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN ASSET DITUNGGU
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
            </div>
            <div class="form_right">
                <label>
                    Aging &nbsp;&gt;=
                </label>
                <asp:TextBox ID="txtaging" runat="server" CssClass="inptype"></asp:TextBox>(day)
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
