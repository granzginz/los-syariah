﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewDoc.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.ViewDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewDoc</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnChild" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="hyAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="hyCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDesc" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Supplier
            </label>
            <asp:label ID="lblSupplier" runat="server"></asp:label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                <asp:Label ID="serialno1label" runat="server"></asp:Label>
            </label>
            <asp:Label ID="lblChasisNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="serialno2label" runat="server"></asp:Label>
            </label>
            <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polisi
            </label>
            <asp:Label ID="lblLicPlate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal STNK
            </label>
            <asp:Label ID="lblTaxDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lokasi Rak Sekarang
            </label>
            <asp:Label ID="lblCurrRack" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Lokasi Filling
            </label>
            <asp:Label ID="lblCurrFill" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lokasi Doc Aset Sekarang
            </label>
            <asp:Label ID="lblCurrDocAset" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DAFTAR DOKUMEN ASSET
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgDoc" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOCUMENT ID" Visible="False">                            
                            <ItemTemplate>
                                <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOKUMEN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%#Container.DataItem("documentno")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="docreceiveddate" HeaderText="TGL TERIMA PERTAMA" DataFormatString="{0:dd/MM/yyyy}">                            
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="expecteddate" HeaderText="TGL DIHARAPKAN" DataFormatString="{0:dd/MM/yyyy}">                            
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="STATUS">                            
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">                            
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="NAMA">                            
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>                    
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                HISTORY
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgHistory" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="HISTORY SEQ">                            
                            <ItemTemplate>
                                <asp:Label ID="lblHistorySeq" runat="server" Text='<%#Container.DataItem("AssetHistorySeq")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOKUMEN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblDocumentName" runat="server" Text='<%#Container.DataItem("AssetDocName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS">                            
                            <ItemTemplate>
                                <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("AssetDocStatus")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL POSTING">                            
                            <ItemTemplate>
                                <asp:Label ID="lblPostingDate" runat="server" Text='<%#Format(Container.DataItem("PostingDate"),"dd/MM/yyyy")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL VALUTA">                            
                            <ItemTemplate>
                                <asp:Label ID="lblValueDate" runat="server" Text='<%#Container.DataItem("ValueDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LOKASI">                            
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%#Container.DataItem("Location")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">                            
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="APPLICATIONID" Visible="false">                            
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>                    
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
            <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        </div>
    </form>
</body>
</html>
