﻿Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class DocTboInq
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    'Protected WithEvents oBranch As UcBranch
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents GridNavigator As ucGridNav
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            'Tampil ALl branch
            If Me.IsHoBranch Then
                oBranch.IsAll = True
            Else
                oBranch.IsAll = False
                oBranch.ViewObranch()
            End If

            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "DOCTBOINQ"
            oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub DoBind(Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        

        Dim aging = New DateTime(1900, 1, 1)


        If txtaging.Text.Trim <> "" Then
            aging = Me.BusinessDate.AddDays(CDbl(txtaging.Text) * -1).ToString("MM/dd/yyyy")
        End If

        Dim wherestr As String = ""
        If oSearchBy.Text.Trim <> "" Then
            wherestr = String.Format("{0},{1}", oSearchBy.ValueID, oSearchBy.Text.Trim)
        End If
        oCustomClass = oController.ListTboInquiry(GetConnectionString(), currentPage, pageSize, oBranch.BranchID, BusinessDate, aging, wherestr)


        DtUserList = oCustomClass.listData
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAsset.DataSource = DvUserList

        Try
            DtgAsset.DataBind()
        Catch
            DtgAsset.CurrentPageIndex = 0
            DtgAsset.DataBind()
        End Try
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        ' PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True


    End Sub
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlDatagrid.Visible = True
        DtgAsset.Visible = True
        pnlList.Visible = True
        DoBind()
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

        '    'Dim par = String.Format("Branch : {0}", oBranch.BranchName) 
        '    'Me.SearchBy = String.Format(" aga.branchid = '{0}' and aga.assetdocstatus = 'W' ", oBranch.BranchID)

        '    'If txtaging.Text.Trim <> "" Then 
        '    '    Me.SearchBy = String.Format(" {0} and  adc.expectedreceiveddate <='{1}'", Me.SearchBy, Me.BusinessDate.AddDays(CDbl(txtaging.Text) * -1).ToString("MM/dd/yyyy"))

        '    '    'If par <> "" Then
        '    '    '    par = par & " , Aging : " & txtaging.Text.Trim & " "
        '    '    'Else
        '    '    '    par = par & "Aging : " & txtaging.Text.Trim & " "
        '    '    'End If
        '    'End If

        '    'If oSearchBy.Text.Trim <> "" Then
        '    '    Me.SearchBy = String.Format(" {0} and {1} like '%{2}%' ", Me.SearchBy, oSearchBy.ValueID, oSearchBy.Text.Trim.Replace("'", "''").Replace("%", ""))

        '    '    'If par <> "" Then
        '    '    '    par = par & " , " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("%", "") & "%' "
        '    '    'Else
        '    '    '    par = par & "" & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("%", "") & "%' "
        '    '    'End If
        '    'End If

        '    ' Me.ParamReport = par
        '    'pnlDatagrid.Visible = True
        '    'DtgAsset.Visible = True
        '    'pnlList.Visible = True
        '    'DoBind()
        'End If
    End Sub

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

         
        If e.Item.ItemIndex >= 0 Then
             
            'CType(e.Item.FindControl("HypView"), HyperLink).NavigateUrl =
            '    "javascript:OpenViewDocument('" & Server.UrlEncode(CType(e.Item.FindControl("hyApplicationId"), HyperLink).Text.Trim) & "', '" &
            '    Server.UrlEncode(Me.sesBranchId.Replace("'", "")) & "', '" & Server.UrlEncode(CType(e.Item.FindControl("lblAssetSeqNo"), Label).Text.Trim) & "', '" & "AssetDocument" & "')"


            CType(e.Item.FindControl("HySuppname"), HyperLink).NavigateUrl = "javascript:OpenWinSupplier('" & "AssetDocument" & "', '" &
                Server.UrlEncode(CType(e.Item.FindControl("lblSupplier"), Label).Text.Trim) & "' )"


            '*** Customer Link  
            CType(e.Item.FindControl("hyCustomerName"), HyperLink).NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" &
                Server.UrlEncode(CType(e.Item.FindControl("lblCustomerId"), Label).Text.Trim) & "')"

            '*** Agreement No link  
            CType(e.Item.FindControl("hyAgreementNo"), HyperLink).NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" &
                Server.UrlEncode(CType(e.Item.FindControl("hyApplicationId"), HyperLink).Text.Trim) & "')"

            '*** ApplicationId link
            Dim hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) 
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind()
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DocTboInq.aspx")
    End Sub
End Class