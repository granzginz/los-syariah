﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RRDInq
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    'Protected WithEvents oBranch As UcBranch
    Protected WithEvents oBranch As ucBranchAll
#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property



#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'Tampil ALl branch
            If Me.IsHoBranch Then
                oBranch.IsAll = True
            Else
                oBranch.IsAll = False
                oBranch.ViewObranch()
            End If

            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "RRDINQ"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                ' oSearchBy.ListData = "Name, Customer Name-ApplicationID,Application ID-Agreementno, Agreement No-AssetDesc, Asset Description-Licenseplate, License Plate"
                oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            End If
        End If
    End Sub

#End Region



#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spDocStatusInq"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataBound"

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim inlblAssetSeqNo As New Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink

        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then

            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            inlblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)

            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hypReceive = CType(e.Item.FindControl("HypView"), HyperLink)
            hypReceive.NavigateUrl = "javascript:OpenViewDocument('" & Server.UrlEncode(HyappId.Text.Trim) & "', '" & Server.UrlEncode(Me.sesBranchId.Replace("'", "")) & "', '" & Server.UrlEncode(inlblAssetSeqNo.Text.Trim) & "', '" & "AssetDocument" & "')"
            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub

#End Region

#Region "Reset"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("RRDInq.aspx")
    End Sub
#End Region
#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Dim par As String
            par = ""

            Me.SearchBy = ""
            Me.SearchBy = "  and aga.branchid = '" & oBranch.BranchID & "' "
            par = par & "Branch : " & oBranch.BranchName & " "
            Me.SearchBy = Me.SearchBy & "  and aga.assetstatus = 'RRA' "

            If txtsdate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & "  and  aga.RRDDAte <= '" & ConvertDate2(txtsdate.Text) & "'"
                If par <> "" Then
                    par = par & " , RRD Date <= " & ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy") & " "
                Else
                    par = par & "RRD Date <= " & ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy") & " "
                End If
            End If
            '    Me.SearchBy = Me.SearchBy & " and aga.assetdocstatus in ('W','B','P')"
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & "  and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
                If par <> "" Then
                    par = par & " , " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("%", "") & "%' "
                Else
                    par = par & "" & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("%", "") & "%' "
                End If
            End If
            If Me.SearchBy.Trim <> "" Then
                Me.SearchBy = Right(Me.SearchBy, Len(Me.SearchBy) - 5)
            Else
                Me.SearchBy = ""
            End If

            Me.ParamReport = par
            pnlDatagrid.Visible = True
            DtgAsset.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

#End Region
#Region "Sort"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region


End Class