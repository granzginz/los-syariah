﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewDoc
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(Viewstate("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(Viewstate("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("TotOH") = Value
        End Set
    End Property

    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(Viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property Style() As String
        Get
            Return (CType(Viewstate("Style"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Style") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController

#End Region
#Region "page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("Branchid")
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))
            Me.Style = Request.QueryString("Style")

            Me.FormID = "VIEWDOC"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                DoBind()
                DoBindGrid()
                DoBindGridAssetHistory()
            End If
        End If
    End Sub
#End Region
#Region "DoBind"
    Sub DoBind()
        Dim inCustID As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            hyCustomerName.Text = .Customername
            inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustID) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            'lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & Server.UrlEncode(inSupId) & "', '" & "AssetDocument" & "')"
            lblAssetDesc.Text = .assetDesc
            lblEngineNo.Text = .EngineNo
            Me.Chasis = .ChasisNo
            lblLicPlate.Text = .LicensePlate
            lblChasisNo.Text = .ChasisNo
            lblTaxDate.Text = .taxdate.ToString("dd/MM/yyyy")
            lblCurrRack.Text = .RackLoc
            lblCurrFill.Text = .FillingLoc
            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            lblCurrDocAset.Text = .BranchOnStatus
        End With
    End Sub
#End Region
#Region "DoBindGrid"
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = " adc.branchid = '" & Me.BranchID & "'  and applicationid = '" & Me.ApplicationID & "'and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()



    End Sub
#End Region
#Region "DoBindGridAssetHistory"
    Sub DoBindGridAssetHistory()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .ApplicationId = Me.ApplicationID.Trim
        End With

        oCustomClass = oController.GetAssetHistory(oCustomClass)
        DtUserList = oCustomClass.listData

        dtgHistory.DataSource = DtUserList.DefaultView
        dtgHistory.CurrentPageIndex = 0
        dtgHistory.DataBind()

    End Sub
#End Region

End Class