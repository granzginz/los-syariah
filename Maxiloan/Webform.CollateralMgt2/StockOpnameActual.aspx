﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StockOpnameActual.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.StockOpnameActual" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>StockOpnameActual</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI STOCK OPNAME ACTUAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="cbobranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap Pilih Cabang"
                    ControlToValidate="cbobranch" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR DOKUMEN ASSET
            </h3>
        </div>
    </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="StockOpnameNo" BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypPilih" runat="server" Text='DETAIL'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="StockOpnameNo" HeaderText="Stock Opname No">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyStockOpnameNo" runat="server" Text='<%#Container.DataItem("StockOpnameNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchOnStatus" HeaderText="Branch On Status" >                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchOnStatus" runat="server" Text='<%#Container.DataItem("BranchOnStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchFullName" HeaderText="Branch FullName">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchFullName" runat="server" Text='<%#Container.DataItem("BranchFullName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="OpnameDate" HeaderText="Opname Date">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyOpnameDate" runat="server" Text='<%#Container.DataItem("OpnameDate")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDocStatusDesc" HeaderText="AssetDoc Status Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDocStatusDesc" runat="server" Text='<%#Container.DataItem("AssetDocStatusDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="QtyActual" HeaderText="Qty Actual">
                                <ItemTemplate>
                                    <asp:Label ID="lblQtyActual" runat="server" Text='<%#Container.DataItem("QtyActual")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                     <uc2:ucGridNav id="GridNavigator" runat="server"/>
                    <div class="form_button">
                            <asp:Button ID="btnStockOpnameActual" runat="server" CausesValidation="False" Text="Stock Opname Actual"
                            CssClass="small button blue"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>    
    <asp:Panel ID="pnlStockOpname" runat="server" Visible="false">
            <div class="form_title">
            <div class="form_single">
                <h4>
                    STOCK OPNAME ACTUAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgStockOpname" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                        Width="100%" ShowFooter="True" DataKeyField="ApplicationID" AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="BranchID" HeaderText="BRANCH">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="APLIKASI">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="AssetDoc" HeaderText="Asset Doc.">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAssetDoc" runat="server" Text='<%#Container.DataItem("AssetDocID")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="DocumentNo" HeaderText="No. Dokumen">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDocumentNo" runat="server" Text='<%#Container.DataItem("DocumentNo")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="LicensePlate" HeaderText="NO. POLISI">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="AssetDocRack" HeaderText="RAK">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAssetDocRack" runat="server" Text='<%#Container.DataItem("AssetDocRack")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="AssetDocFilling" HeaderText="FILLING">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAssetDocFilling" runat="server" Text='<%#Container.DataItem("AssetDocFilling")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="OwnerAsset" HeaderText="PEMILIK">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtOwnerAsset" runat="server" Text='<%#Container.DataItem("OwnerAsset")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    No Aplikasi
                </label>
                <asp:TextBox ID="txtApplicationId" runat="server" MaxLength="20" ></asp:TextBox>
            </div>
            <div class="form_button">
                    <asp:Button ID="btnSaveStockOpname" runat="server" CausesValidation="False" Text="Save Stock Opname"
                    CssClass="small button green"></asp:Button>
            </div>
        </div>
    </asp:Panel>    
    </form>
</body>
</html>
