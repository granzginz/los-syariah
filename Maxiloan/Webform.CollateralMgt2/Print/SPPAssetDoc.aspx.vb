﻿#Region " Imports "
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SPPAssetDoc
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta"
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
    Private m_controller As New DataUserControlController
#End Region

#Region "  Event & Handlers "

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "SPPADocList"
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")
            End If
            pnlDtGrid.Visible = False
            txtSPDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            Dim dtBranch As New DataTable
            dtBranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)

            With cboParent
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"

                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End If
            End With
            'Me.SearchBy = " BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
            'Me.SearchBy = Me.SearchBy & " and  spdate = '" & ConvertDate2(txtSPDate.Text.Trim) & "'"
            'Me.SortBy = "AgreementNo ASC"
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
        'Else
        'Dim strHTTPServer As String
        'Dim strHTTPApp As String
        'Dim strNameServer As String
        'strHTTPServer = Request.ServerVariables("PATH_INFO")
        'strNameServer = Request.ServerVariables("SERVER_NAME")
        'strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        'Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        'End If

    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        pnlDtGrid.Visible = True
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = "PrintNum=0"
        Else
            Me.SearchBy = "PrintNum > 0"
        End If
        If cboParent.SelectedItem.Value <> "" Then
            Me.SearchBy = Me.SearchBy & " And BranchID = '" & cboParent.SelectedItem.Value & "'"
        End If
        If txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & cboSearchBy.SelectedItem.Value & " like '%" & _
                                     txtSearchBy.Text.Replace("%", "") & "%'"
        ElseIf txtSPDate.Text.ToString <> "" Then
            Me.SearchBy = Me.SearchBy & " And SPDate  = '" & ConvertDateSql(txtSPDate.Text.ToString) & "'"
        Else
            Me.SearchBy = Me.SearchBy
        End If

        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgSPAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSPAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)

            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chkPrint As CheckBox
        Dim lblAgreementNo As HyperLink
        Dim lblApplicationID As New Label
        Dim lblSPNO As New Label
        Dim hasil As Integer
        Dim cmdwhere As String
        Dim strMultiApplicationID As String = ""
        Dim strMultiBranch As String = ""

        With oDataTable
            .Columns.Add(New DataColumn("SPNo", GetType(String)))
            .Columns.Add(New DataColumn("BranchID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
        End With
        With dtgSPAssetDoc
            For intloop = 0 To .Items.Count - 1
                chkPrint = CType(.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                lblApplicationID = CType(.Items(intloop).Cells(1).FindControl("lblApplicationID"), Label)
                lblSPNO = CType(.Items(intloop).Cells(1).FindControl("lblSPNO"), Label)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("SPNo") = lblSPNO.Text.Trim
                    oRow("BranchID") = cboParent.SelectedItem.Value
                    oRow("ApplicationID") = lblApplicationID.Text.Trim
                    oDataTable.Rows.Add(oRow)
                    strMultiBranch = cboParent.SelectedItem.Value
                    If strMultiApplicationID.Trim = "" Then
                        strMultiApplicationID = strMultiApplicationID + "'" + lblApplicationID.Text.Trim + "'"
                    Else
                        strMultiApplicationID = strMultiApplicationID + ",'" + lblApplicationID.Text.Trim + "'"
                    End If
                End If
            Next
        End With

        cmdwhere = "ApplicationID in(" + strMultiApplicationID + ")"
        If oDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap Pilih Item", True)
            Exit Sub
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .listData = oDataTable
            .BranchId = cboParent.SelectedItem.Value
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
        End With
        oCustomClass = oController.SavePrintSPPADoc(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then
            ShowMessage(lblMessage, "Proses gagal", True)
            Exit Sub
        Else
            cmdwhere = cmdwhere & " and BranchID='" & cboParent.SelectedItem.Value & "'"
            cmdwhere = cmdwhere & " and spdate = '" & ConvertDateSql(txtSPDate.Text.ToString) & "'"
            'Dim cookie As HttpCookie = Request.Cookies("SPAssetDocument")
            Dim cookie As HttpCookie = Request.Cookies("SPPAssetDocPrint")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("ApplicationID") = strMultiApplicationID
                cookie.Values("BranchID") = strMultiBranch
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                'Dim cookieNew As New HttpCookie("SPAssetDocument")
                Dim cookieNew As New HttpCookie("SPPAssetDocPrint")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("ApplicationID", strMultiApplicationID)
                cookieNew.Values.Add("BranchID", strMultiBranch)
                cookieNew.Values.Add("sortby", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("SPPAssetDocViewer.aspx")
        End If

    End Sub
#End Region


#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        If Me.SortBy.Trim = "" Then
            Me.SortBy = "AgreementNo ASC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataSet
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy

        End With

        oCustomClass = oController.SPPADList(oCustomClass)
        Try
            DvUserList = oCustomClass.listDoc.DefaultView
            DvUserList.Sort = Me.SortBy
            dtgSPAssetDoc.DataSource = DvUserList
        Catch ex As Exception
            ex.Message.ToString()
        End Try

        Try
            dtgSPAssetDoc.DataBind()
        Catch
            dtgSPAssetDoc.CurrentPageIndex = 0
            dtgSPAssetDoc.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True

    End Sub
#End Region

End Class