﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class PrepaymentPrint
    Inherits Maxiloan.Webform.WebBased


#Region " Property "
    Private Property Cmdwhere() As String
        Get
            Return CStr(viewstate("Cmdwhere"))
        End Get
        Set(ByVal Value As String)
            viewstate("Cmdwhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("FilterBy"))
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property

    Private Property FundingCoyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Me.FormID = "PREPAYMENTREQPRINT"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If sessioninvalid() Then
                Exit Sub
            End If
        End If
        If Not Me.IsPostBack Then
            drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height - 100; window.open('" & strFileLocation & "','channeling', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            pnlSearch.Visible = True
            pnlList.Visible = False
            Panel1.Visible = False
            PnlInterest.Visible = False


            txtdebetDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            bindComboCompany()
            Response.Write(GenerateScript(getComboContract, getComboBatchDate))
        End If

    End Sub

    Private Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("PrepaymentPrint")

        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = Me.Cmdwhere
            cookie.Values("filterBy") = Me.FilterBy
            cookie.Values("txtUP") = txtUP.Text.Trim
            cookie.Values("DebetDate") = ConvertDate2(txtdebetDate.Text.Trim).ToString("dd/MM/yyyy")
            cookie.Values("ApplicationID") = Me.ApplicationID
            cookie.Values("BranchID") = Me.BranchID
            cookie.Values("FundingCoyID") = Me.FundingCoyID
            cookie.Values("FundingContractNO") = Me.FundingContractNo
            cookie.Values("FundingBatchNo") = Me.FundingBatchNo
            cookie.Values("Lampiran") = TxtLampiran.Text.Trim
            cookie.Values("PrintedBy") = TxtPrintedBy.Text.Trim
            cookie.Values("InterestNominal") = TxtInterest.Text.Trim

            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("PrepaymentPrint")
            cookieNew.Values.Add("cmdwhere", Me.Cmdwhere)
            cookieNew.Values.Add("FilterBy", Me.FilterBy)
            cookieNew.Values.Add("txtUP", txtUP.Text.Trim)
            cookieNew.Values.Add("DebetDate", ConvertDate2(txtdebetDate.Text.Trim).ToString("dd/MM/yyyy"))
            cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
            cookieNew.Values.Add("BranchID", Me.BranchID)
            cookieNew.Values.Add("FundingCoyID", Me.FundingCoyID)
            cookieNew.Values.Add("FundingContractNO", Me.FundingContractNo)
            cookieNew.Values.Add("FundingBatchNo", Me.FundingBatchNo)
            cookieNew.Values.Add("Lampiran", TxtLampiran.Text.Trim)
            cookieNew.Values.Add("PrintedBy", TxtPrintedBy.Text.Trim)
            cookieNew.Values.Add("InterestNominal", TxtInterest.Text.Trim)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("PrepaymentPrintViewer.aspx")
    End Sub
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region

#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContract"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Function
#End Region

#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Function
#End Region

#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Sub
#End Region

    Public Function drdCompanyChange() As String

        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"

    End Function

    Protected Function drdContractChange() As String

        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"

    End Function
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.Cmdwhere = ""
        Me.SortBy = ""
        Dim cmdWhere As String
        Dim FilterBy As String
        If drdCompany.SelectedItem.Value = "All" Then
            cmdWhere = ""
            FilterBy = ""
        Else
            If tempChild.Value.Trim = "-" Or tempChild.Value.Trim = "" Then
                cmdWhere = "Agreement.FundingCoyID = '" & drdCompany.SelectedItem.Value & "' "
                FilterBy = FilterBy + "Funding Company = '" & drdCompany.SelectedItem.Text.Trim & "'"
            Else
                If tempChild2.Value.Trim = "-" Or tempChild2.Value.Trim = "" Then
                    cmdWhere = "Agreement.FundingCoyID = '" & drdCompany.SelectedItem.Value & "' " & _
                        " and Agreement.FundingContractID = '" & tempChild.Value.Trim & "' "
                    FilterBy = FilterBy + " and Funding Contract No = '" & tempChildName.Value.Trim & "'"
                Else
                    cmdWhere = "Agreement.FundingCoyID = '" & drdCompany.SelectedItem.Value & "' " & _
                        " and Agreement.FundingContractID = '" & tempChild.Value.Trim & "' " & _
                        " and Agreement.FundingBatchID = '" & tempChild2.Value.Trim & "' "
                    'FilterBy = FilterBy + " and Funding Contract No = '" & tempGrandChildName.Value.Trim & "'"
                    FilterBy = " FundingCoyID = '" & drdCompany.SelectedItem.Value & "' " & _
                               " and FundingContractNo = '" & tempChild.Value.Trim & "' " & _
                               " and FundingBatchNo = '" & tempChild2.Value.Trim & "'"
                End If
            End If
        End If
        If txtsearch.Text.Trim <> "" And cmdWhere <> "" Then
            cmdWhere = cmdWhere + " and Agreement.AgreementNO like '%" & txtsearch.Text.Trim.Replace("%", "") & "%'"        
        End If
        pnlSearch.Visible = True
        pnlList.Visible = True
        Panel1.Visible = False
        PnlInterest.Visible = False
        Me.Cmdwhere = cmdWhere
        Me.FilterBy = FilterBy
        BindGridEntity(Me.Cmdwhere, Me.SortBy)
    End Sub
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingPrepaymentLetterListing"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblMessage.Visible = True
        Else
            'imgPrint.Enabled = True
        End If

        dtgDocPrepaymentList.DataSource = dtvEntity
        Try
            dtgDocPrepaymentList.DataBind()
        Catch
            dtgDocPrepaymentList.CurrentPageIndex = 0
            dtgDocPrepaymentList.DataBind()
        End Try
        drdCompany.SelectedIndex = 0
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblTotRec.Text = recordCount.ToString

        pnlList.Visible = True
        pnlSearch.Visible = True
        Panel1.Visible = False
        PnlInterest.Visible = False

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.Cmdwhere, Me.SortBy)
        pnlList.Visible = True
        pnlSearch.Visible = True
        Panel1.Visible = False
        PnlInterest.Visible = False

    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.Cmdwhere, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlSearch.Visible = True
        Panel1.Visible = False
        PnlInterest.Visible = False

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.Cmdwhere, Me.SortBy)
        pnlList.Visible = True
        pnlSearch.Visible = True
        Panel1.Visible = False
        PnlInterest.Visible = False
    End Sub

#End Region
    Private Sub dtgDocPrepaymentList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgDocPrepaymentList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label


        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"
        End If
    End Sub

    Private Sub dtgDocPrepaymentList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgDocPrepaymentList.ItemCommand
        Dim lblApplicationId As Label
        Dim lblBranchID As Label
        Dim lblFundingCoyID As Label
        Dim lblFundingContractNo As Label
        Dim lblFundingBatchNo As Label
        Select Case e.CommandName
            Case "Print"
                lblApplicationId = CType(dtgDocPrepaymentList.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                lblBranchID = CType(dtgDocPrepaymentList.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)
                lblFundingCoyID = CType(dtgDocPrepaymentList.Items(e.Item.ItemIndex).FindControl("lblFundingCoyID"), Label)
                lblFundingContractNo = CType(dtgDocPrepaymentList.Items(e.Item.ItemIndex).FindControl("lblFundingContractNo"), Label)
                lblFundingBatchNo = CType(dtgDocPrepaymentList.Items(e.Item.ItemIndex).FindControl("lblFundingBatchNo"), Label)
                Me.ApplicationID = lblApplicationId.Text.Trim
                Me.BranchID = lblBranchID.Text.Trim
                Me.FundingCoyID = lblFundingCoyID.Text.Trim
                Me.FundingContractNo = lblFundingContractNo.Text.Trim
                Me.FundingBatchNo = lblFundingBatchNo.Text.Trim
                If Me.Cmdwhere <> "" Then
                    Me.Cmdwhere = Me.Cmdwhere + " and Agreement.ApplicationID = '" & lblApplicationId.Text.Trim & "'"
                Else
                    Me.Cmdwhere = Me.Cmdwhere + " Agreement.ApplicationID = '" & lblApplicationId.Text.Trim & "'"
                End If
                pnlSearch.Visible = False
                pnlList.Visible = False
                Panel1.Visible = True
                PnlInterest.Visible = False

                lblMessage.Visible = False
        End Select
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("PrepaymentPrint.aspx")
    End Sub

    Private Sub btnResetSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetSearch.Click
        Response.Redirect("PrepaymentPrint.aspx")
    End Sub
    Private Sub FillInterestNominal()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Dim objReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingReportPrepaymentRequest"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 3).Value = Me.BranchID
            objCommand.Parameters.Add("@FundingCoyID", SqlDbType.VarChar, 20).Value = Me.FundingCoyID.Trim
            objCommand.Parameters.Add("@FundingContractNO", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNO", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo.Trim
            objCommand.Parameters.Add("@whereBy", SqlDbType.VarChar, 1000).Value = Me.Cmdwhere
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objCommand.Parameters.Add("@debetdate", SqlDbType.DateTime).Value = ConvertDate2(txtdebetDate.Text)
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()
            If objReader.Read Then
                If Not IsDBNull(objReader.Item("OSInterestAmount")) Then
                    TxtInterest.Text = FormatNumber(objReader.Item("OSInterestAmount"), 0)
                Else
                    TxtInterest.Text = "0"
                End If

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        pnlList.Visible = False
        pnlSearch.Visible = False
        Panel1.Visible = False
        PnlInterest.Visible = True
        lbldebetDate.Text = ConvertDate2(txtdebetDate.Text.Trim).ToString("dd/MM/yyyy")
        lblLampiran.Text = TxtLampiran.Text.Trim
        lblPrintedBy.Text = TxtPrintedBy.Text.Trim
        lblUP.Text = txtUP.Text.Trim
        FillInterestNominal()
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtContract As DataTable = getComboContract()
        Dim i As Integer

        For i = 0 To DtContract.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdContract.UniqueID, CStr(DtContract.Rows(i).Item("FundingContractNo")).Trim)
        Next

        RegisterFirstItem(drdContract)

        Dim DtBatchDate As DataTable = getComboBatchDate()
        Dim j As Integer

        For j = 0 To DtBatchDate.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdBatchDate.UniqueID, CStr(DtBatchDate.Rows(j).Item("FundingBatchNo")).Trim)
        Next
        RegisterFirstItem(drdBatchDate)

        MyBase.Render(writer)
    End Sub

    Private Sub RegisterFirstItem(ByVal ddl As DropDownList)
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "0")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "All")
    End Sub
End Class