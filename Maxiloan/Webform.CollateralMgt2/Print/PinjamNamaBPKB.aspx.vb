﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PinjamNamaBPKB
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height - 100; window.open('" & strFileLocation & "','accacq', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "PinjamNamaBPKB"
            Me.CmdWhere = " Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
            Me.CmdWhere = Me.CmdWhere & " and  DateEntryApplicationData >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and DateEntryApplicationData <= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"

            Me.SortBy = "AgreementNo ASC"
            DoBind(Me.CmdWhere, Me.SortBy)
        End If
    End Sub
#End Region
#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        If cmdWhere.Trim = "" Then
            cmdWhere = ""
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListPinjamNamaBPKB(oCustomClass)

        DtUserList = oCustomClass.listData
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgPPK.DataSource = DvUserList

        Try
            dtgPPK.DataBind()
        Catch
            dtgPPK.CurrentPageIndex = 0
            dtgPPK.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True

    End Sub
#End Region
#Region "LinkToCustomer"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function
#End Region
#Region "Print"
    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim cmdwhere As String = ""
            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With
            For intloop = 0 To dtgPPK.Items.Count - 1
                chkPrint = CType(dtgPPK.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(dtgPPK.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                    oRow("ApplicationID") = CType(dtgPPK.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                    oDataTable.Rows.Add(oRow)
                    If cmdwhere.Trim = "" Then
                        cmdwhere = "'" & CType(oRow("ApplicationID"), String) & "'"
                    Else
                        cmdwhere = cmdwhere & ",'" & CType(oRow("ApplicationID"), String) & "'"
                    End If
                End If
            Next
            cmdwhere = "AG.ApplicationID in (" & cmdwhere & ")"
            cmdwhere = cmdwhere & " and AG.BranchID ='" & Me.sesBranchId.Replace("'", "").Trim & "'"
            If oDataTable.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Harap Periksa Item", True)
                Exit Sub
            End If

            Dim cookie As HttpCookie = Request.Cookies("PinjamNamaBPKBPrint")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PinjamNamaBPKBPrint")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("PinjamNamaBPKBViewer.aspx")
        End If
    End Sub
#End Region
#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Dim SearchTemp As String = ""
        If txtPeriod1.Text.Trim <> "" And TxtPeriod2.Text.Trim <> "" Then
            Me.CmdWhere = " Agreement.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'" & " and DateEntryApplicationData >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and DateEntryApplicationData <= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"
        End If

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim.Replace("%", "") & "%'"
            Me.CmdWhere = Me.CmdWhere & "  and " & SearchTemp
        End If
        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        Me.CmdWhere = " Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Me.CmdWhere = Me.CmdWhere & " and  DateEntryApplicationData >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and DateEntryApplicationData <= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"

        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.CmdWhere, Me.SortBy)
        txtSearchBy.Text = ""
        cboSearchBy.ClearSelection()
    End Sub
#End Region
#Region "dtgPPK_ItemDataBound"
    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblCustomerName As New HyperLink
            Dim lblCustomerID As New Label
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region

End Class