﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class PemeriksaanBPKBViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property namasamsat() As String
        Get
            Return CType(ViewState("namasamsat"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("namasamsat") = Value
        End Set
    End Property
    Private Property alamatsamsat() As String
        Get
            Return CType(ViewState("alamatsamsat"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("alamatsamsat") = Value
        End Set
    End Property
    Private Property namaTTD() As String
        Get
            Return CType(ViewState("namaTTD"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("namaTTD") = Value
        End Set
    End Property

    Private Property jabatanTTD() As String
        Get
            Return CType(ViewState("jabatanTTD"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("jabatanTTD") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub
#End Region
#Region "BindReport"    
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New DocReceiveController
        Dim oTandaTerimaDokumen As New Parameter.DocRec        
        Dim objReport As PemeriksaanBPKBPrint = New PemeriksaanBPKBPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
        oTandaTerimaDokumen.strConnection = GetConnectionString()
        oTandaTerimaDokumen.WhereCond = Me.CmdWhere
        oTandaTerimaDokumen = m_controller.ListReportPemeriksaanBPKB(oTandaTerimaDokumen)
        oData = oTandaTerimaDokumen.ListDataReport

        objReport.SetDataSource(oData)

        AddParamField(objReport, "NamaSamsat", Me.namasamsat)
        AddParamField(objReport, "AlamatSamsat", Me.alamatsamsat)
        AddParamField(objReport, "NamaTTD", Me.namaTTD)
        AddParamField(objReport, "JabatanTTD", Me.jabatanTTD)
        'Wira 2015-07-02
        AddParamField(objReport, "CompanyName", GetCompanyName())

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "PemeriksaanBPKB.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("PemeriksaanBPKB.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "PemeriksaanBPKB&cmdwhere=" & Me.CmdWhere & "")

    End Sub

    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function

    Private Sub AddParamField(ByVal objReport As PemeriksaanBPKBPrint, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PemeriksaanBPKBPrint")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.namasamsat = cookie.Values("namasamsat")
        Me.alamatsamsat = cookie.Values("alamatsamsat")
        Me.namaTTD = cookie.Values("namaTTD")
        Me.jabatanTTD = cookie.Values("JabatanTTD")
    End Sub
#End Region


End Class