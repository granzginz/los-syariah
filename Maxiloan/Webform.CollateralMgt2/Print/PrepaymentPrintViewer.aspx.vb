﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PrepaymentPrintViewer
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property Cmdwhere() As String
        Get
            Return CStr(ViewState("Cmdwhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("Cmdwhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CStr(ViewState("FilterBy"))
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
    Private Property txtUP() As String
        Get
            Return CStr(ViewState("txtUP"))
        End Get
        Set(ByVal Value As String)
            ViewState("txtUP") = Value
        End Set
    End Property
    Private Property StrDebetDate() As String
        Get
            Return CStr(ViewState("StrDebetDate"))
        End Get
        Set(ByVal Value As String)
            ViewState("StrDebetDate") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property

    Private Property FundingCoyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property Lampiran() As String
        Get
            Return CStr(ViewState("Lampiran"))
        End Get
        Set(ByVal Value As String)
            ViewState("Lampiran") = Value
        End Set
    End Property
    Private Property PrintedBy() As String
        Get
            Return CStr(ViewState("PrintedBy"))
        End Get
        Set(ByVal Value As String)
            ViewState("PrintedBy") = Value
        End Set
    End Property
    Private Property InterestNominal() As Decimal
        Get
            Return CDec(ViewState("InterestNominal"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("InterestNominal") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Me.IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If
            BindReport()
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()

        Dim cookie As HttpCookie = Request.Cookies("PrepaymentPrint")
        Me.Cmdwhere = cookie.Values("cmdwhere")
        Me.FilterBy = cookie.Values("FilterBy")
        Me.txtUP = cookie.Values("txtUP")
        Me.StrDebetDate = cookie.Values("DebetDate")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.BranchID = cookie.Values("BranchID")
        Me.FundingCoyID = cookie.Values("FundingCoyID")
        Me.FundingContractNo = cookie.Values("FundingContractNo")
        Me.FundingBatchNo = cookie.Values("FundingBatchNo")
        Me.Lampiran = cookie.Values("Lampiran")
        Me.PrintedBy = cookie.Values("PrintedBy")
        Me.InterestNominal = CDec(cookie.Values("InterestNominal"))
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        GetCookies()

        'Dim oPVReport As New Parameter.GeneralPaging
        'Dim ocontroller As New GeneralPagingController
        'Dim oCustomClass As New Parameter.GeneralPaging
        Dim objreport As ReportDocument
        Dim spName As String

        objreport = New PrepaymentPrintPrint
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .ApplicationId = Me.ApplicationID
                .BranchId = Me.BranchID
                .FundingCoyID = Me.FundingCoyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .WhereCond = Me.Cmdwhere
                .BusinessDate = Me.BusinessDate
                .BorrowDate = ConvertDate2(Me.StrDebetDate)
                .SpName = "spFundingReportPrepaymentRequest"
            End With
            oCustomClass = oController.ProcessReportPrepaymentRequest(oCustomClass)

            objreport.SetDataSource(oCustomClass.ListDataReport)

            CrystalReportViewer1.ReportSource = objreport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()

            '========================================================
            Dim discrete As ParameterDiscreteValue
            Dim ParamFields As ParameterFields
            Dim ParamField As ParameterFieldDefinition
            Dim CurrentValue As ParameterValues


            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("LoginID")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.PrintedBy
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("CompanyFullName")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.sesCompanyName
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("UP")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.txtUP
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)


            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("DebitDate")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.StrDebetDate
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("Lampiran")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.Lampiran
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("InterestNominal")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.InterestNominal
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            '========================================================

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PrepaymentPrint.pdf"
            DiskOpts.DiskFileName = strFileLocation
            objreport.ExportOptions.DestinationOptions = DiskOpts
            objreport.Export()
            objreport.Close()
            objreport.Dispose()
            Response.Redirect("PrepaymentPrint.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PrepaymentPrint")
        Catch exp As Exception
            Response.Write(exp.Message)
        End Try


    End Sub
#End Region
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("PrepaymentPrint.aspx")
    End Sub


End Class