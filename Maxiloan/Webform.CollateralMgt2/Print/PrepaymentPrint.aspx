﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrepaymentPrint.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.PrepaymentPrint" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrepaymentPrint</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }	
			-->
    </script>
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms["form1"].tempChild').value = "";
            var tmp;
            var child = eval('document.forms["form1"].' + pChild);
            var tampung = eval('document.forms["form1"].' + pTampung);
            var tampungname = eval('document.forms["form1"].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms["form1"].' + tampungGrandChild).value = l;
            eval('document.forms["form1"].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms["form1"].' + tampungGrandChild2).value = l;
            eval('document.forms["form1"].' + tampungGrandChildName).value = j;
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempChild2" type="hidden" name="tempChild2" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ASSET PREPAYMENT
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    DAFTAR DOKUMEN ASSET PREPAYMENT
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgDocPrepaymentList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="CETAK">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgLetter" runat="server" ImageUrl="../../Images/iconLetter.gif"
                                        CommandName="Print" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchID" HeaderText="BRANCH ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="APPLICATION ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCust" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="description" HeaderText="NAMA ASSET">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NO POLISI">
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DocumentNO" HeaderText="NO DOKUMEN">
                                <ItemTemplate>
                                    <asp:Label ID="LblDocNo" runat="server" Text='<%#Container.DataItem("DocumentNO")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OSPrincipalAmount" HeaderText="SISA POKOK" DataFormatString="{0:N0}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OSInterestAmount" HeaderText="SISA MARGIN" DataFormatString="{0:N0}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrincipalPaidAmount" HeaderText="JUMLAH POKOK" DataFormatString="{0:N0}">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="FundingCoyID" HeaderText="FundingCoyID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblFundingCoyID" runat="server" Text='<%#Container.DataItem("FundingCoyID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="FundingContractNO" HeaderText="FundingContractNO"
                                Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblFundingContractNO" runat="server" Text='<%#Container.DataItem("FundingContractNO")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="FundingBatchNO" HeaderText="FundingBatchNO" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblFundingBatchNo" runat="server" Text='<%#Container.DataItem("FundingBatchNO")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="First" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="Prev" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="Next" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="Last" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="InpType">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtGopage" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h3>
                    CARI KONTRAK UNTUK CETAK SURAT PREPAYMENT
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank / Cabang</label>
                <asp:DropDownList ID="drdCompany" runat="server" onchange="<%#drdCompanyChange()%>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Kontrak Bank</label>
                <asp:DropDownList ID="drdContract" runat="server" onchange="<%#drdContractChange()%>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:DropDownList ID="drdBatchDate" runat="server" onchange="setCboBatchDate(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cbosearch" runat="server">
                    <asp:ListItem Value="1">No Kontrak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtsearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnResetSearch" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    INFORMASI DETAIL CETAK SURAT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    U/P</label>
                <asp:TextBox ID="txtUP" runat="server" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Lampiran</label>
                <asp:TextBox ID="TxtLampiran" runat="server" Width="10px" CssClass="InpType">1</asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Debet</label>
                <asp:TextBox runat="server" ID="txtdebetDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtdebetDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dicetak Oleh</label>
                <asp:TextBox ID="TxtPrintedBy" runat="server" Width="267px" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnOk" runat="server" CausesValidation="False" Text="OK" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlInterest" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    KONFIRMASI CETAK SURAT PREPAYMENT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    U/P</label>
                <asp:Label ID="lblUP" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Lampiran</label>
                <asp:Label ID="lblLampiran" runat="server" Width="10px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Debet</label>
                <asp:Label ID="lbldebetDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dicetak Oleh</label>
                <asp:Label ID="lblPrintedBy" runat="server" Width="267px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Margin</label>
                <asp:TextBox ID="TxtInterest" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnViewReport" runat="server" CausesValidation="False" Text="Print"
                CssClass="small button green"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
