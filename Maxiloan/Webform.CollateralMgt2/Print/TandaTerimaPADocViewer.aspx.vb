﻿
#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller

#End Region

Public Class TandaTerimaPADocViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property BDate() As String
        Get
            Return CType(ViewState("BDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BDate") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New DocReceiveController
        Dim oBPKB As New Parameter.DocRec

        Dim objReport As TandaTerimaPADocPrint = New TandaTerimaPADocPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oBPKB.strConnection = GetConnectionString()
        oBPKB.BranchId = Me.BranchID
        oBPKB.ApplicationId = Me.ApplicationID
        oBPKB = m_controller.PrintBPKBSerahTerima(oBPKB)
        oData = oBPKB.listReport

        objReport.SetDataSource(oBPKB.listReport)
        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()


        '========================================================
        'Dim discrete As ParameterDiscreteValue
        'Dim ParamFields As ParameterFields
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues

        'ParamFields = New ParameterFields
        'ParamField = objReport.DataDefinition.ParameterFields("CompanyName")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.sesCompanyName
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_BPKBSerahTerima.pdf"

        DiskOpts.DiskFileName = strFileLocation

        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4

        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("TandaTerimaPADoc.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_BPKBSerahTerima")

    End Sub
#End Region
#Region "CreateData"
    Private Sub CreateData()
        Dim m_controller As New DocReceiveController
        Dim oData As New DataSet
        Dim oBPKB As New Parameter.DocRec
        oBPKB.strConnection = GetConnectionString()
        oBPKB.BranchId = Me.BranchID
        oBPKB.ApplicationId = Me.ApplicationID
        oBPKB = m_controller.PrintBPKBSerahTerima(oBPKB)
        oData = oBPKB.listReport
        oData.WriteXmlSchema("D:\project\Maxiloan\AssetMgmt\xmd_BPKBSerahTerima.xmd")
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("TandaTerimaPADocPrint")
        Me.BranchID = cookie.Values("BranchID")
        Me.ApplicationID = cookie.Values("ApplicationID")
    End Sub
#End Region


End Class