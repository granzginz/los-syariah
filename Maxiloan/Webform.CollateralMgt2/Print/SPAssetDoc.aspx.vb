﻿#Region " Imports "
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SPAssetDoc
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta"
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region

#Region "  Event & Handlers "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If Not IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")
            End If
            pnlDtGrid.Visible = False
            txtSDate.Text = (DateAdd(DateInterval.Day, -1, Me.BusinessDate)).ToString("dd/MM/yyyy")
        End If


    End Sub
#End Region

#Region "Property"
    Private Property CmdWhereReport() As String
        Get
            Return CType(viewstate("CmdWhereReport"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhereReport") = Value
        End Set
    End Property
#End Region
#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        If Me.SortBy.Trim = "" Then
            Me.SortBy = "AssetDocumentSP.BrachID ASC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataSet
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy

        End With

        oCustomClass = oController.CreateSPAssetDocument(oCustomClass)
        Try
            DtUserList = oCustomClass.listReport
            DvUserList = DtUserList.Tables(0).DefaultView
            DvUserList.Sort = Me.SortBy
            dtgSPAssetDoc.DataSource = DvUserList
        Catch ex As Exception
            ex.Message.ToString()
        End Try

        Try
            dtgSPAssetDoc.DataBind()
        Catch
            dtgSPAssetDoc.CurrentPageIndex = 0
            dtgSPAssetDoc.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True

    End Sub
#End Region

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        pnlDtGrid.Visible = True
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = "AssetDocumentSP.IsPrint=0  And AssetDocumentSP.BranchID='" & Replace(Me.sesBranchId, "'", "") & "' and AssetDocumentSP.SPDate = '" & ConvertDate2(txtSDate.Text.Trim).ToString("yyyyMMdd") & "' "
        Else
            Me.SearchBy = "AssetDocumentSP.IsPrint > 0 And AssetDocumentSP.BranchID='" & Replace(Me.sesBranchId, "'", "") & "' and AssetDocumentSP.SPDate = '" & ConvertDate2(txtSDate.Text.Trim).ToString("yyyyMMdd") & "' "
        End If
        Me.CmdWhereReport = "AssetDocumentSP.BranchID='" & Replace(Me.sesBranchId, "'", "") & "' and AssetDocumentSP.SPDate = '" & ConvertDate2(txtSDate.Text.Trim).ToString("yyyyMMdd") & "' "

        If txtSearchBy.Text <> "" And cboSearchBy.SelectedItem.Value <> "" Then

            Me.SearchBy = Me.SearchBy & " And " & cboSearchBy.SelectedItem.Value & " like '%" & _
                                     txtSearchBy.Text.Replace("%", "") & "%'" & " And AssetDocumentSP.SPType  = '" & cboSPType.SelectedItem.Value & "'"
            Me.CmdWhereReport = Me.CmdWhereReport & " And " & cboSearchBy.SelectedItem.Value & " like '%" & _
                                 txtSearchBy.Text.Replace("%", "") & "%'" & " And AssetDocumentSP.SPType  = '" & cboSPType.SelectedItem.Value & "'"

        ElseIf cboSPType.SelectedItem.Value <> "" Then
            Me.SearchBy = Me.SearchBy & " And AssetDocumentSP.SPType  = '" & cboSPType.SelectedItem.Value & "'"
            Me.CmdWhereReport = Me.CmdWhereReport & " And AssetDocumentSP.SPType  = '" & cboSPType.SelectedItem.Value & "'"
        Else
            Me.SearchBy = Me.SearchBy
            Me.CmdWhereReport = Me.CmdWhereReport
        End If

        Me.SortBy = "BranchID ASC"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgSPAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSPAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)

            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("SPAssetDoc.aspx")
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chkPrint As CheckBox
        Dim lblAgreementNo As HyperLink
        Dim lblApplicationID As New Label
        Dim lblBranchID As New Label
        Dim hasil As Integer
        Dim cmdwhere As String
        Dim strMultiApplicationID As String = ""
        Dim strMultiBranch As String = ""

        With oDataTable
            .Columns.Add(New DataColumn("SPNo", GetType(String)))
            .Columns.Add(New DataColumn("BranchID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
        End With
        With dtgSPAssetDoc
            For intloop = 0 To .Items.Count - 1
                chkPrint = CType(.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                lblApplicationID = CType(.Items(intloop).Cells(1).FindControl("lblApplicationID"), Label)
                lblBranchID = CType(.Items(intloop).Cells(1).FindControl("lblBranchID"), Label)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("SPNo") = lblAgreementNo.Text.Trim
                    oRow("BranchID") = CType(lblBranchID.Text.Trim, String)
                    oRow("ApplicationID") = lblApplicationID.Text.Trim
                    oDataTable.Rows.Add(oRow)
                    strMultiBranch = lblBranchID.Text.Trim
                    If strMultiApplicationID.Trim = "" Then
                        'cmdwhere = "Agreement.ApplicationID='" & CType(oRow("ApplicationID"), String) & "'"
                        strMultiApplicationID = strMultiApplicationID + "'" + lblApplicationID.Text.Trim + "'"
                    Else
                        'cmdwhere = cmdwhere & " or Agreement.ApplicationID='" & CType(oRow("applicationID"), String) & "' "
                        strMultiApplicationID = strMultiApplicationID + ",'" + lblApplicationID.Text.Trim + "'"
                    End If
                End If
            Next
        End With

        cmdwhere = "AssetDocumentSP.ApplicationID in(" + strMultiApplicationID + ")"
        If oDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap Pilih Item", True)
            Exit Sub
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .listData = oDataTable
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .BusinessDate = ConvertDate2(Today.ToString("dd MMMM yyyy"))
            .LoginId = Me.Loginid
        End With
        oCustomClass = oController.SavePrintSPAssetDoc(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then
            ShowMessage(lblMessage, "Proses Gagal", True)
            Exit Sub
        Else
            cmdwhere = cmdwhere & " and AssetDocumentSP.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
            If Me.CmdWhereReport <> "" Then
                Me.CmdWhereReport = Me.CmdWhereReport.Trim + " And " + cmdwhere.Trim
            Else
                Me.CmdWhereReport = cmdwhere.Trim
            End If


            Dim cookie As HttpCookie = Request.Cookies("SPAssetDocPrint")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = Me.CmdWhereReport
                cookie.Values("ApplicationID") = strMultiApplicationID
                cookie.Values("BranchID") = strMultiBranch
                cookie.Values("sortby") = Me.SortBy.Replace("SPNo", "AgreementNo")
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("SPAssetDocPrint")
                cookieNew.Values.Add("cmdwhere", Me.CmdWhereReport)
                cookieNew.Values.Add("ApplicationID", strMultiApplicationID)
                cookieNew.Values.Add("BranchID", strMultiBranch)
                cookieNew.Values.Add("sortby", Me.SortBy.Replace("SPno", "AgreementNo"))
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("SPAssetDocViewer.aspx")
        End If
    End Sub

End Class