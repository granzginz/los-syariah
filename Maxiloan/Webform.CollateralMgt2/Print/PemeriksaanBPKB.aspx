﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PemeriksaanBPKB.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.PemeriksaanBPKB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PemeriksaanBPKB</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CETAK PERMOHONAN PEMERIKSAAN BPKB
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">        
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" CssClass="inptype"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Aplikasi</label>
                <asp:TextBox runat="server" ID="txtPeriod1"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtPeriod1"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                &nbsp; S/D &nbsp;
                <asp:TextBox runat="server" ID="TxtPeriod2"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtPeriod2"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DAFTAR PERMOHONAN PEMERIKSAAN BPKB
                </h4>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlParam">
            <div class="form_box">
                <div class="form_single">
                    <label>Nama Samsat</label>
                    <asp:TextBox ID="txtNama" runat="server" CssClass="long_text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="txtNama" ErrorMessage="Harap diisi"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Alamat Samsat</label>
                    <asp:TextBox ID="txtAlamat" runat="server" CssClass="long_text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="txtAlamat" ErrorMessage="Harap diisi"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Nama</label>
                    <asp:TextBox ID="txtnameSingnature" runat="server" CssClass="long_text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="txtnameSingnature" ErrorMessage="Harap diisi"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Jabatan</label>
                    <asp:TextBox ID="txtjabatan" runat="server" CssClass="long_text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="txtjabatan" ErrorMessage="Harap diisi"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </asp:Panel>        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPPK" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                <asp:Label  ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label Visible="False" ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%#Container.dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AGREEMENTDATE" SortExpression="AGREEMENTDATE" HeaderText="TGL KONTRAK"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPrint" runat="server" Visible="false" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
