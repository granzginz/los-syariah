﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SPPAssetDocViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(viewstate("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
    Private Property Biaya() As String
        Get
            Return CType(viewstate("Biaya"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Biaya") = Value
        End Set
    End Property
    Private Property PicJob() As String
        Get
            Return CType(viewstate("PicJob"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PicJob") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec
    Private oEntityGS As New Parameter.GeneralSetting
    Private m_UColl As New DataUserControlController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()

    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPAsset As New DataSet

        Dim objReport As SPPAssetDocPrint = New SPPAssetDocPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = Me.CmdWhere
                .ApplicationId = Me.ApplicationID
                .BranchId = Me.Branch_ID
                .SortBy = Me.SortBy
            End With
            oCustomClass = m_coll.SPPADList(oCustomClass)
            dtsEntity = oCustomClass.listDoc
            objReport.SetDataSource(dtsEntity)

            CrystalReportViewer1.ReportSource = objReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()
            ' CrystalReportViewer1.Dispose()

            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "SPPAssetDoc.pdf"
            DiskOpts.DiskFileName = strFileLocation
            'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
            objReport.Dispose()
            Response.Redirect("SPPAssetDoc.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "SPPAssetDoc")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        '========================================================
        'Dim discrete As ParameterDiscreteValue
        'Dim ParamFields As ParameterFields
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues

        'ParamFields = New ParameterFields
        'ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.BusinessDate
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = objReport.DataDefinition.ParameterFields("Biaya")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.Biaya
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = objReport.DataDefinition.ParameterFields("PicJob")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.PicJob
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#End Region
#Region "Event"
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("SPPAssetDoc.aspx")
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("SPPAssetDocPrint")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.Branch_ID = cookie.Values("BranchID")
        Me.SortBy = cookie.Values("sortby")
        oEntityGS.GSID = "DOCDEPOSITFEE"
        Me.Biaya = m_UColl.GetGeneralSetting(GetConnectionString, oEntityGS)
        oEntityGS.GSID = "DOCDEPTNAME"
        Me.PicJob = m_UColl.GetGeneralSetting(GetConnectionString, oEntityGS)
    End Sub
#End Region

End Class