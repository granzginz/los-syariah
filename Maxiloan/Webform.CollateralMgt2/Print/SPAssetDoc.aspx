﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPAssetDoc.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.SPAssetDoc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SPAssetDoc</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CARI SP DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">        
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="Supplier.SupplierName">Supplier</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" CssClass="inptype"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Jenis Surat Peringatan</label>
                <asp:DropDownList ID="cboSPType" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="SP1">SP 1</asp:ListItem>
                    <asp:ListItem Value="SP2">SP 2</asp:ListItem>
                    <asp:ListItem Value="SP3">SP 3</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RVcboSPType" runat="server" Display="Dynamic" ErrorMessage="Harap pilih Jenis SP"
                    InitialValue="0" ControlToValidate="cboSPType" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Proses</label>
                <asp:TextBox runat="server" ID="txtSDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtSDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Cetak</label>
                <asp:DropDownList ID="cboPrinted" runat="server">
                    <asp:ListItem Value="No">No</asp:ListItem>
                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DAFTAR DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgSPAssetDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="SPNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SPNo" HeaderText="NO SP">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("SPNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn SortExpression="AgreementNo" DataField="AgreementNo" HeaderText="NO KONTRAK">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.DataItem("Name") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA SUPPLIER">
                                <ItemTemplate>
                                    <asp:Label ID="lblSuppName" runat="server" Text='<%#Container.DataItem("SupplierName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DayAgeGoLive" HeaderText="TGL ACTIVATION">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrintBy" HeaderText="TERAKHIR CETAK">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrintNum" HeaderText="CTK KE">                                
                            </asp:BoundColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
