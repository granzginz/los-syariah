﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocReceive.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.DocReceive" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocReceive</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">       
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypReceive" runat="server" Text='TERIMA'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>' />
                                    <asp:Label id="hdnBranch"  name="hdnBranch" runat="server"   Text='<%#Container.DataItem("branchid")%>'/>
                                    <asp:Label id="lblIsFund"  name="lblIsFund" runat="server"   Text='<%#Container.DataItem("DocInFunding")%>'/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NamaBPKB" HeaderText="NAMA BPKB">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblNamaBPKB" runat="server" Text='<%#Container.DataItem("NamaBPKB")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                    
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NOPOL">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Chasis" HeaderText="NO CHASIS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblChasis" runat="server" Text='<%#Container.DataItem("Chasis")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EngineNo" HeaderText="NO MESIN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblEngineNo" runat="server" Text='<%#Container.DataItem("EngineNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetstatus" HeaderText="ASS">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetstatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetdocstatusdesc" HeaderText="DOK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                        </Columns>                        
                    </asp:DataGrid>
                     <uc2:ucGridNav id="GridNavigator" runat="server"/>
                 <%--   <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                            MinimumValue="1" ControlToValidate="txtpage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>--%>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
              <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cbobranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    ControlToValidate="cbobranch" ErrorMessage="Harap Pilih Cabang" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class ="label_req">
                    Status Dokumen
                </label>
                <asp:DropDownList ID="cboDocStatus" runat="server">
                    <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                    <asp:ListItem Value="R">Release</asp:ListItem>
                    <asp:ListItem Value="P">Prospect</asp:ListItem>
                    <asp:ListItem Value="W">Waiting</asp:ListItem>
                    <asp:ListItem Value="B">Borrow</asp:ListItem>
                    <asp:ListItem Value="I">Intransit</asp:ListItem>
                    <asp:ListItem Value="H">Hand Over</asp:ListItem>
                    <%--<asp:ListItem Value="G">Pledging</asp:ListItem>--%>
                    <asp:ListItem Value="O">On Hand</asp:ListItem>
                    <asp:ListItem Value="C">Cancel</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ControlToValidate="cboDocStatus" ErrorMessage="Harap Pilih Dokumen Status" InitialValue="" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
