﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocPledging.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.DocPledging" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocPledging</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function Supplier_Checked(pStrSupplierID, pStrSupplierName) {
            with (document.Form1) {
                hdnSupplierID.value = pStrSupplierID;
                hdnSupplierName.value = pStrSupplierName;
            }
        }

        function Select_Click() {
            with (document.Form1) {
                var lObjName = '<%= Request.QueryString("SupplierID")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnSupplierID.value;
                }
            }
            window.close();
        }	
				
    </script>
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms[0].' + tampungGrandChild2).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">    
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <input id="hdnSupplierID" type="hidden" name="hdnSupplierId" runat="server" />
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempChild2" type="hidden" name="tempChild2" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR DOKUMEN ASSET DIJAMINKAN
                </h3>
            </div>
        </div>
    <asp:Panel ID="pnlDocumentList" runat="server">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="False"
                        AutoGenerateColumns="False" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NO POLISI">
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetstatus" HeaderText="STATUS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetstatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetdocstatusdesc" HeaderText="STATUS DOK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDocRack" HeaderText="AssetDocRack" Visible="False">                               
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDocRack" runat="server" Text='<%#Container.DataItem("AssetDocRack")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RackID" HeaderText="RackID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblRackID" runat="server" Text='<%#Container.DataItem("RackID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RDBranchID" HeaderText="RDBranchID" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("RDBranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchAssetLocation" HeaderText="LOKASI" Visible="True">                                
                                <ItemTemplate>
                                    <asp:Label ID="LblBranchAssetLocation" runat="server" Text='<%#Container.DataItem("BranchAssetLocation")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                    <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSelect" runat="server" CausesValidation="False" Text="Select"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnExit" runat="server" CausesValidation="False" Text="Exit" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN ASSET DIJAMINKAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank / Cabang</label>
                <asp:DropDownList ID="drdCompany" runat="server" onchange="<%# drdCompanyChange() %>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Kontrak Bank</label>
                <asp:DropDownList ID="drdContract" runat="server" onchange="<%# drdContractChange() %>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:DropDownList ID="drdBatchDate" runat="server" onchange="setCboBatchDate(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_uc"> 
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlSearchDetail" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI DETAIL DOKUMEN ASSET DIJAMINKAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank / Cabang</label>
                <asp:Label ID="lblFundingCoy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Kontrak Bank</label>
                <asp:Label ID="LblContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:Label ID="LblBatchDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:Label ID="LblSearchBy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearchdtl" runat="server" CausesValidation="False" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnResetdtl" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSaveConfirm" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    KONFIRMASI DOKUMEN ASSET DIJAMINKAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dokumen Asset di Jaminkan</label>
                <asp:Label ID="LblDocPledge" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dokumen Asset Tidak di Jaminkan</label>
                <asp:Label ID="LblDocumentWillNotBePledge" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgDocPledgeConf" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationIdConf" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyAgreementNoConf" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="LblCustomeridConf" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyCustomerNameConf" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NO POLISI">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Container.DataItem("licenseplate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetstatus" HeaderText="STATUS">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Container.DataItem("assetstatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetdocstatusdesc" HeaderText="STATUS DOK">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
