﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocGenerateBarcode
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private totalPages1 As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
#End Region
#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "DOCGENBARCODE"
            oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
            oSearchBy.BindData()

            Dim dtbranch As New DataTable
            'dtbranch = m_controller.GetBranchName2(GetConnectionString, Me.sesBranchId)
            dtbranch = m_controller.GetBranchAll(GetConnectionString)
            With cbobranch
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = dtbranch
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
            cbobranch.Enabled = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#End Region



#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'Dim intloop As Integer
        'Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetDocPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub
#End Region
#Region "Navigation"

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
        totalPages1 = e.TotalPage
    End Sub

    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then
    '        ShowMessage(lblMessage, "Data tidak ditemukan!", True)
    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = CType(totalPages, String)
    '        rgvGo.MaximumValue = CType(totalPages, String)
    '    End If
    '    lblRecord.Text = CType(recordCount, String)

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If
    'End Sub

    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    If Me.SortBy Is Nothing Then
    '        Me.SortBy = ""
    '    End If
    '    DoBind(Me.SearchBy, Me.SortBy)
    'End Sub

    'Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
    '    If IsNumeric(txtPage.Text) Then
    '        If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '            currentPage = CType(txtPage.Text, Int16)
    '            If Me.SortBy Is Nothing Then
    '                Me.SortBy = ""
    '            End If
    '            DoBind(Me.SearchBy, Me.SortBy)
    '        End If
    '    End If
    'End Sub
#End Region
#Region "Databound"

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink
        Dim inlblAssetSeqNo As New Label
        'Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then

            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then
            inlblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            'hypBorrow = CType(e.Item.FindControl("HypBorrow"), HyperLink)
            'hypBorrow.NavigateUrl = "DocGenerateBarcodeList.aspx?Applicationid=" & HyappId.Text.Trim & "&branchid=" & Me.sesBranchId.Replace("'", "") & "&branchid2=" & cbobranch.SelectedItem.Value.Trim & "&AssetSeqNo=" & inlblAssetSeqNo.Text.Trim
            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DocGenerateBarcode.aspx")
    End Sub
#End Region
#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

            Me.SearchBy = ""
            Me.SearchBy = " aga.BranchOnStatus = '" & cbobranch.SelectedItem.Value.Trim & "' "
            'Me.SearchBy = Me.SearchBy & " and  aga.assetdocstatus='O' and aga.assetstatus <> 'RLS'"
            'Me.SearchBy = Me.SearchBy & " and agr.contractStatus = 'SSD'"
            Me.SearchBy = Me.SearchBy & " and  aga.assetdocstatus='O' and agr.contractStatus <> 'CAN'"
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
            End If
            pnlDatagrid.Visible = True
            DtgAsset.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "CheckStatus"
    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusBarcode As CheckBox

        For loopitem = 0 To CType(DtgAsset.Items.Count - 1, Int16)
            Context.Trace.Write("loopitem = " & loopitem)
            ChkStatusBarcode = New CheckBox
            ChkStatusBarcode = CType(DtgAsset.Items(loopitem).FindControl("ChkStatusBarcode"), CheckBox)

            If ChkStatusBarcode.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = True ")
                    ChkStatusBarcode.Checked = True
                Else
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = False ")
                    ChkStatusBarcode.Checked = False
                End If
            Else
                Context.Trace.Write("ChkStatusAccountPayableNo.Checked = False")
                ChkStatusBarcode.Checked = False
            End If
        Next

    End Sub
#End Region

    Private Sub btnGenerateBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateBarcode.Click
        FillViewStateHalaman()
        Dim cookie As HttpCookie = Request.Cookies("PrintDocGenerateBarcode")
        Dim AppIdChecked As String = JoinAllViewstateHalaman().Trim
        If AppIdChecked = "" Then
            Exit Sub
        End If
        Context.Trace.Write("ApplicationIdChecked sebelum replace= " & AppIdChecked)
        Dim JmlRecorddicheck As String = Replace(AppIdChecked, ",,", "")
        If Right(JmlRecorddicheck, 1) = "," Then
            JmlRecorddicheck = Left(JmlRecorddicheck, JmlRecorddicheck.Length - 1)
        End If
        Dim oDataTable As DataTable = New DataTable
        'Dim objRow As DataRow
        'Dim objrowResult As DataRow
        oDataTable.Columns.Add("ApplicationId", System.Type.GetType("System.String"))

        'Dim pisahkanAppId() As String
        'Dim pisahkanperbaris() As String
        'Dim iloop As Integer = 0
        'pisahkanAppId = Split(CStr(JmlRecorddicheck), ",")
        'Context.Trace.Write("UBound(pisahkanAppId) = " & UBound(pisahkanAppId))
        'Dim oEntities As New Parameter.SPPA
        'For iloop = 0 To UBound(pisahkanAppId)
        '    pisahkanperbaris = Split(pisahkanAppId(iloop), ",")
        '    Context.Trace.Write("iloop = " & iloop)
        '    Context.Trace.Write("Masukkan ke data table = " & pisahkanperbaris(0))

        'With oEntities
        '    .WhereCond = "Where AccountPayable.AccountPayableNo = '" & Replace(pisahkanperbaris(0), "'", "") & "' and Agreement.BranchId = '" & Left(Replace(pisahkanperbaris(0), "'", ""), 3) & "' "

        '    .PageSource = CommonVariableHelper.PAGE_SOURCE_GET_INSURANCE_BILLING_SELECT
        '    .strConnection = GetConnectionString()
        'End With
        Try
            If Not cookie Is Nothing Then
                'cookie.Values("ApplicationID") = Replace(pisahkanperbaris(0), "'", "")
                cookie.Values("JmlRecorddicheck") = JmlRecorddicheck
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PrintDocGenerateBarcode")
                'cookieNew.Values.Add("ApplicationID", Replace(pisahkanperbaris(0), "'", ""))
                cookieNew.Values.Add("JmlRecorddicheck", JmlRecorddicheck)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("DocGenerateBarcodeViewer.aspx")
            'Response.Redirect("DocGenerateBarcodeList.aspx")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        'Next

    End Sub

#Region "JoinAllViewstateHalaman"

    Private Function JoinAllViewstateHalaman() As String
        Dim totalpages As Integer = CType(totalPages1, Integer)
        Dim strAppId As String
        Dim koma As String
        Dim ihal As Integer
        Dim hal As String
        Context.Trace.Write("JoinAllViewstateHalaman")

        strAppId = ""
        For ihal = 1 To totalpages
            hal = CStr("hal" & CStr(ihal))
            If strAppId.Trim = "" Then koma = "" Else koma = ","
            strAppId = strAppId & koma & CType(ViewState(hal), String)
            ViewState("AllPage") = strAppId

        Next
        Context.Trace.Write("strAppId.Trim = " & strAppId.Trim)
        Return strAppId.Trim
    End Function

#End Region
#Region "FillViewStateHalaman"

    Private Sub FillViewStateHalaman()
        'Isi ViewState per halaman grid
        Context.Trace.Write("FillViewStateHalaman")
        Dim irecord As Int16
        Dim cb As CheckBox
        Dim hyApplicationId As HyperLink
        Dim strAppId As String
        Dim koma As String
        Dim recordDataGrid As Int16

        recordDataGrid = CType(DtgAsset.Items.Count, Int16)
        ' recordDataGrid = 10
        strAppId = ""
        ViewState("hal" & CStr(CInt(currentPage.ToString)).Trim) = ""
        Context.Trace.Write("recordDataGrid :" + CType(recordDataGrid, String))
        Try
            For irecord = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var Irecord :" + CType(irecord, String))
                Context.Trace.Write("Var PageSize :" + CType(PageSize, String))
                cb = CType(DtgAsset.Items(irecord).FindControl("ChkStatusBarcode"), CheckBox)
                hyApplicationId = CType(DtgAsset.Items(irecord).Cells(0).FindControl("hyApplicationId"), HyperLink)

                If cb.Checked = True Then
                    If strAppId = "" Then koma = "" Else koma = ","
                    strAppId = strAppId & koma & hyApplicationId.Text.Trim
                    ViewState("hal" & CStr(CInt(currentPage.ToString)).Trim) = strAppId.Trim
                End If

            Next

            Context.Trace.Write("strAppId = " & strAppId)

        Catch ex As Exception
            Response.Write(ex.Message)
            Dim oErr As New MaxiloanExceptions
            'oErr.WriteLog("InsuranceBilling.aspx", "Sub FillViewStateHalaman", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

    End Sub


#End Region
End Class
