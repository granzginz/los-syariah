﻿

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class DocReceiveTboList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0
#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(Viewstate("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(Viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property DocStatus() As String
        Get
            Return (CType(Viewstate("DocStatus"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("DocStatus") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(Viewstate("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("TotOH") = Value
        End Set
    End Property
    Private Property FlagRack() As String
        Get
            Return (CType(Viewstate("FlagRack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FlagRack") = Value
        End Set
    End Property

    Private Property FLocDesc() As String
        Get
            Return (CType(Viewstate("FLocDesc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLocDesc") = Value
        End Set
    End Property

    Private Property RackDesc() As String
        Get
            Return (CType(Viewstate("RackDesc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RackDesc") = Value
        End Set
    End Property
    Private Property BranchID2() As String
        Get
            Return (CType(Viewstate("BranchID2"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID2") = Value
        End Set
    End Property
    Private Property RackFillLoc() As IList(Of RackFill)
        Get
            Return (CType(ViewState("RackFillLoc"), IList(Of RackFill)))
        End Get
        Set(ByVal Value As IList(Of RackFill))
            ViewState("RackFillLoc") = Value
        End Set
    End Property

    Private Property IsDocInFunding() As Boolean
        Get
            Return (CType(ViewState("IsDocInFunding"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsDocInFunding") = Value
        End Set
    End Property
    Private Property SelectDocStatus() As String
        Get
            Return (CType(ViewState("SelectDocStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectDocStatus") = Value
        End Set
    End Property

    Private Property AssetUsageID() As String
        Get
            Return (CType(ViewState("AssetUsageID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsageID") = Value
        End Set
    End Property

    Private Property IsIzinTrayek() As Byte
        Get
            Return (CInt(ViewState("IsIzinTrayek")))
        End Get
        Set(ByVal Value As Byte)
            ViewState("IsIzinTrayek") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
    Protected WithEvents UCAddress As ucAddressCity

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid").Trim
            Me.BranchID2 = Request.QueryString("branchid2").Trim
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))
            Me.SelectDocStatus = Request.QueryString("DocStatus")

            Me.FormID = "DOCRECEIVETBO"
            ' If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

            txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            Dim oReceiveList As New Parameter.DocRec
            oReceiveList.ApplicationId = Me.ApplicationID
            oReceiveList.BranchId = Me.BranchID2
            oReceiveList.strConnection = GetConnectionString()
            oReceiveList = oController.GetTaxDate(oReceiveList)

            Dim strTaxDate As String
            If ConvertDate2(oReceiveList.taxdate.ToString("dd/MM/yyyy")) <= ConvertDate2("01/01/1900") Then
                strTaxDate = Me.BusinessDate.ToString("dd/MM/yyyy")
            Else
                strTaxDate = oReceiveList.taxdate.ToString("dd/MM/yyyy")
            End If

            'txtTdate.Text = strTaxDate

            DoBind()
            DoBindGrid()
            GetAssetRegistration()

            If Me.RackDesc <> "-" And Me.FLocDesc <> "-" Then
                lblRack.Visible = True
                lblFLoc.Visible = True
                cboRack.Visible = False
                cboFill.Visible = False
                lblRack.Text = Me.RackDesc
                lblFLoc.Text = Me.FLocDesc
                Me.FlagRack = "1"
            Else
                lblRack.Visible = False
                lblFLoc.Visible = False
                cboRack.Visible = True
                cboFill.Visible = True
                Me.FlagRack = "2"
                 
                Me.RackFillLoc = RackFillHelper.GetRacks(GetConnectionString, Me.BranchID, (Me.BranchID = "000" And IsDocInFunding = False))

                With cboRack
                    .DataTextField = "Text"
                    .DataValueField = "Value"
                    .DataSource = Me.RackFillLoc
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                RefreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)
            End If

        End If
        '  End If
    End Sub
    Sub DoBind()
        Dim inCustID As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID2
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            hyCustomerName.Text = .Customername
            inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustID) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & Server.UrlEncode(inSupId) & "', '" & "AssetDocument" & "')"
            lblAssetDesc.Text = .assetDesc
            lblKondisiAset.Text = .KondisiAset
            'txtEngineNo.Text = .EngineNo
            Me.Chasis = .ChasisNo
            'txtLicPlate.Text = .LicensePlate
            Me.DocStatus = .AssetDocStatus
            Me.Rack = .RackLocID
            Me.FLoc = .FLocID
            Me.FLocDesc = .FillingLoc
            Me.RackDesc = .RackLoc
            IsDocInFunding = .DocInFunding
            'serialno1label.Text = .seriallabel1
            'serialno2label.Text = .seriallabel2
            'txtChasisNo.Text = .ChasisNo.Trim
            txtReceiveFrom.Text = .SupplierName.Trim
            


            'If .NamaBPKBSama Then
            '    lblNamaBPKBSama.Text = "Ya"
            'Else
            '    lblNamaBPKBSama.Text = "Tidak"
            'End If

            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
            lblBorrowerName.Text = .BorrowBy

            Dim strBorrowDate As String
            strBorrowDate = .BorrowDate.ToString("dd/MM/yyyy")

            If strBorrowDate = "01/01/1900" Then
                lblBorrowDate.Text = "-"
            Else
                lblBorrowDate.Text = strBorrowDate
            End If

            lblAssetUsage.Text = .AssetUsageDesc
            Me.AssetUsageID = .AssetUsage
            Me.IsIzinTrayek = .IsIzinTrayek

        End With
    End Sub
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        'Modify by Wira 20170329
        Dim BPKBdanBadanUsaha As String = ""
        Dim IjinTrayek As String = ""
        Dim notIn As String = ""
        Dim IsCP As String = ""

        If Me.IsIzinTrayek = 1 Then
            IjinTrayek = "'IZNTRAY'"
        End If

        If IjinTrayek <> "" Then
            notIn = "and  adl.AssetDocID not in (" & IjinTrayek & ")"
        End If

        If Me.AssetUsageID = "N" Then
            IsCP = " and adl.ispassenger=1"
        Else
            IsCP = " and adl.isCommercial=1"
        End If

        With oCustomClass
            .strConnection = GetConnectionString
            '.WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "'  and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            .WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "'  and adc.assetseqno ='" & Me.AssetSeqNo & "'" + notIn + IsCP
            .SortBy = "  adc.applicationid"
        End With
        'End Modify

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind() 
    End Sub
    Sub GetAssetRegistration()
        'Dim oAssetReg As New Parameter.DocRec

        'oAssetReg.ApplicationId = Me.ApplicationID
        'oAssetReg.BranchId = Me.BranchID2
        'oAssetReg.strConnection = GetConnectionString
        'oAssetReg = oController.GetAssetRegistration(oAssetReg)
        ''txtName.Text = oAssetReg.OwnerAsset

        'Dim strOwnerAddress As String
        'If IsDBNull(oAssetReg.OwnerAddress) Then
        '    strOwnerAddress = "-"
        'Else
        '    strOwnerAddress = oAssetReg.OwnerAddress
        'End If 
        'UCAddress.Address = strOwnerAddress & IIf(oAssetReg.OwnerRT.Trim = "", "", " RT." & oAssetReg.OwnerRT.Trim & " ").ToString _
        '                    + " " & IIf(oAssetReg.OwnerRW.Trim = "", "", " RW." & oAssetReg.OwnerRW.Trim & " ").ToString _
        '                    + " " & IIf(oAssetReg.OwnerKelurahan.Trim = "", "", oAssetReg.OwnerKelurahan.Trim & " ").ToString _
        '                    + " " & IIf(oAssetReg.OwnerKecamatan.Trim = "", "", oAssetReg.OwnerKecamatan.Trim & "").ToString
        'UCAddress.City = oAssetReg.OwnerCity
        'With UCAddress
        '    .KodePosFalse()
        '    .TeleponFalse()
        '    .BindAddress()
        '    .ValidatorTrue()
        'End With
        'txtAssetNotes.Text = oAssetReg.Notes
    End Sub
    Function getBranchName(branchid As String) As String

        If (IsHoBranch = False) Then branchid = "000"
        Dim m_controller As New DataUserControlController
        Dim dt_branch As New DataTable
        dt_branch = m_controller.GetBranchAll(GetConnectionString)
        For Each row In dt_branch.Rows
            If row("ID").ToString.Trim = branchid Then
                Return row("Name")
            End If
        Next
        Return ""
    End Function

    
    Private Sub cboRack_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRack.SelectedIndexChanged

        Dim cbo = CType(sender, DropDownList).SelectedValue
        If (cbo = "0") Then
            refreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)
            Return
        End If

        Dim k = RackFillLoc.Where(Function(x) x.Value = cbo).SingleOrDefault()
        refreshCboFill(k.FillLocs, cboFill)

    End Sub
    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound
 
        'Dim inlblAssetDocStatus As Label
        'Dim inChkSlct As CheckBox
        'Dim inDocDate As TextBox
        'Dim inlblDocDate As New Label
        'Dim lblIsNoRequired As New Label
        'Dim txtDocNo As New TextBox
        'Dim RFVDocNo As New RequiredFieldValidator

        If e.Item.ItemIndex >= 0 Then 
            Dim inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            Dim inChkSlct = CType(e.Item.FindControl("ChkSlct"), CheckBox)
            Dim inDocDate = CType(e.Item.FindControl("txtDocDate"), TextBox)
            Dim inlblDocDate = CType(e.Item.FindControl("lblDocDate"), Label)
            Dim lblIsNoRequired = CType(e.Item.FindControl("lblIsNoRequired"), Label)
            Dim txtDocNo = CType(e.Item.FindControl("txtDocNo"), TextBox)
            Dim RFVDocNo = CType(e.Item.FindControl("RFVDocNo"), RequiredFieldValidator)
            Dim inlblPromisedate = CType(e.Item.FindControl("lblPromisedate"), Label)
            Dim intxtPromisedate = CType(e.Item.FindControl("txtPromisedate"), TextBox)
            Dim txtFollowUp = CType(e.Item.FindControl("txtFollowUp"), TextBox)

            inDocDate.Visible = True
            inDocDate.Enabled = True
            intxtPromisedate.Visible = True
            intxtPromisedate.Enabled = True
            txtFollowUp.Enabled = False

            If inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P" Then
                inDocDate.Visible = True
                inlblDocDate.Visible = False
                intxtPromisedate.Visible = True
                inlblPromisedate.Visible = False
                txtFollowUp.Enabled = True

                If CBool(lblIsNoRequired.Text) Then
                    txtDocNo.Visible = True
                    RFVDocNo.Visible = True
                Else
                    txtDocNo.Visible = False
                    RFVDocNo.Visible = False
                End If
            Else
                inDocDate.Visible = False
                inlblDocDate.Visible = True
                intxtPromisedate.Visible = False
                inlblPromisedate.Visible = True

                If CBool(lblIsNoRequired.Text) Then
                    txtDocNo.Visible = True
                    RFVDocNo.Visible = True
                Else
                    txtDocNo.Visible = False
                    RFVDocNo.Visible = False
                End If
            End If
            OH = 0
            'If inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P" Or inlblAssetDocStatus.Text.Trim = "B" Or inlblAssetDocStatus.Text.Trim = "I" Or inlblAssetDocStatus.Text.Trim = "G" Or inlblAssetDocStatus.Text.Trim = "H" Then
            '    If inlblAssetDocStatus.Text.Trim = Me.SelectDocStatus Then
            '        inChkSlct.Visible = True
            '        OH = OH + 1
            '    Else
            '        inChkSlct.Visible = False
            '    End If
            'Else
            '    inChkSlct.Visible = False
            'End If
            If inlblAssetDocStatus.Text.Trim = "W" Then
                OH = OH + 1
            End If
            inChkSlct.Visible = (inlblAssetDocStatus.Text.Trim = "W")
            'Me.TotOH = OH
            'If Me.OH = 0 Then
            '    '    ShowMessage(lblMessage, "Dokumen Sudah diterima", True)
            '    btnSave.Visible = False
            'Else
            '    '    btnSave.Visible = True
            '    'End If

            'End If
            btnSave.Visible = Me.OH > 0
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("DocReceiveTbo.aspx")
    End Sub

    Protected Function RackChange() As String
        Return "ParentChange('" & Trim(cboRack.ClientID) & "','" & Trim(cboFill.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function


    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
        If ConvertDate2(txtsdate.Text.Trim) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal Terima tidak boleh lebih besar dari hari ini", True)
            Exit Sub
        End If
         
        If DtgDoc.Items.Count > 0 Then
            Dim i As Integer
            Dim j As Integer
            Dim k As Integer
            Dim dtAdd, dtJanji As New DataTable
            Dim dr As DataRow
            Dim isRec As New Label
            Dim inDocname As New Label
            Dim inDocNo As New TextBox
            Dim inlblDocNo As New Label
            Dim inReceiveddate As String
            Dim inAssetDocStatus As New Label
            Dim inStatusDate As String
            Dim inChkSlct As New CheckBox
            Dim inisMainDoc As New Label
            Dim inlblassetdocid As New Label
            Dim lblIsNoRequired As New Label
            Dim inDocDate As TextBox
            Dim strGroupID As String
            Dim AssetDocNO As String
            Dim adaJanji As Boolean = False

            strGroupID = ""

            dtAdd.Columns.Add("isDocExist", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocName", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocNo", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocDate", System.Type.GetType("System.DateTime"))
            dtAdd.Columns.Add("Receiveddate", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("AssetDocStatus", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("StatusDate", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("IsMainDoc", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("AssetDocID", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("PromiseDate", System.Type.GetType("System.DateTime"))
            dtAdd.Columns.Add("FollowUpAction", System.Type.GetType("System.String"))

            For Each s As String In New String() {"AssetDocID", "PromiseDate", "FollowUpAction"}
                dtJanji.Columns.Add(New DataColumn(s))
            Next
            j = 0
            k = 0
            For i = 0 To DtgDoc.Items.Count - 1

                inChkSlct = CType(DtgDoc.Items(i).FindControl("ChkSlct"), CheckBox)

                If inChkSlct.Checked Then
                    j = j + 1
                    isRec = CType(DtgDoc.Items(i).FindControl("lblrec"), Label)
                    inDocname = CType(DtgDoc.Items(i).FindControl("lblDocname"), Label)
                    inDocNo = CType(DtgDoc.Items(i).FindControl("txtDocNo"), TextBox)
                    inlblDocNo = CType(DtgDoc.Items(i).FindControl("lblDocNo"), Label)
                    inDocDate = CType(DtgDoc.Items(i).FindControl("txtDocDate"), TextBox)
                    inReceiveddate = DtgDoc.Items(i).Cells(3).Text
                    inAssetDocStatus = CType(DtgDoc.Items(i).FindControl("lblAssetDocStatus"), Label)
                    inStatusDate = DtgDoc.Items(i).Cells(7).Text
                    inisMainDoc = CType(DtgDoc.Items(i).FindControl("lblisMainDoc"), Label)
                    inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)
                    lblIsNoRequired = CType(DtgDoc.Items(i).FindControl("lblIsNoRequired"), Label)
                    Dim pomisedate = CType(DtgDoc.Items(i).FindControl("txtPromisedate"), TextBox)
                    Dim followupaction = CType(DtgDoc.Items(i).FindControl("txtFollowUp"), TextBox)

                    If inAssetDocStatus.Text = "P" Or inAssetDocStatus.Text = "W" Or inAssetDocStatus.Text = "G" Then
                        AssetDocNO = inDocNo.Text.Trim
                    Else
                        AssetDocNO = inlblDocNo.Text.Trim
                    End If


                    If CBool(lblIsNoRequired.Text) And AssetDocNO = "" Then
                        ShowMessage(lblMessage, "Harap isi No Dokumen Asset", True)
                        Exit Sub
                    Else
                        dr = dtAdd.NewRow()
                        Dim dpromise = IIf(pomisedate.Text.Trim = "", "1/1/1900", pomisedate.Text.Trim)
                        dr("PromiseDate") = ConvertDate2(dpromise)
                        dr("FollowUpAction") = followupaction.Text
                        dr("isDocExist") = isRec.Text.Trim
                        dr("DocName") = inDocname.Text.Trim
                        dr("DocNo") = AssetDocNO.Trim
                        dr("DocDate") = ConvertDate2("1/1/1900") ' IIf(inDocDate.Text.Trim = "", ConvertDate2("1/1/1900"), ConvertDate2(inDocDate.Text.Trim))
                        dr("Receiveddate") = inReceiveddate

                        'Dim intDocStat = inAssetDocStatus.Text.Trim

                        'If (inAssetDocStatus.Text = "H") Then
                        '    intDocStat = IIf(IsDocInFunding = False, "F", "O")
                        'End If

                        dr("AssetDocStatus") = "O" ' intDocStat  'inAssetDocStatus.Text.Trim

                        dr("StatusDate") = inStatusDate
                        dr("IsMainDoc") = CStr(inisMainDoc.Text.Trim)
                        If inisMainDoc.Text.Trim = "True" Then
                            k = k + 1
                        End If
                        dr("AssetDocID") = inlblassetdocid.Text.Trim
                        strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & inlblassetdocid.Text.Replace("'", "") & "'"
                        dtAdd.Rows.Add(dr)
                    End If
                Else
                    If CType(DtgDoc.Items(i).FindControl("txtPromisedate"), TextBox).Text.Trim <> "" Or
                            CType(DtgDoc.Items(i).FindControl("txtFollowUp"), TextBox).Text.Trim <> "" Then
                        adaJanji = True
                        Dim pomisedate = CType(DtgDoc.Items(i).FindControl("txtPromisedate"), TextBox)
                        Dim followupaction = CType(DtgDoc.Items(i).FindControl("txtFollowUp"), TextBox)
                        Dim dpromise = IIf(pomisedate.Text.Trim = "", "1/1/1900", pomisedate.Text.Trim)
                        inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)

                        dr = dtJanji.NewRow()
                        dr("PromiseDate") = ConvertDate2(dpromise)
                        dr("FollowUpAction") = followupaction.Text
                        dr("AssetDocID") = inlblassetdocid.Text.Trim
                        dtJanji.Rows.Add(dr)
                    End If
                End If
            Next

            If j = 0 Then
                If adaJanji = False Then
                    ShowMessage(lblMessage, "harap pilih Kotaknya", True)
                    Exit Sub
                End If
            End If

            'If Nothing selected then save only changes on tanggal janji dan keterangan janji
            If adaJanji = True Then
                Dim pDoc As New Parameter.DocRec
                pDoc.BusinessDate = Me.BusinessDate
                pDoc.ApplicationId = Me.ApplicationID
                pDoc.BranchId = Me.BranchID
                pDoc.AssetSeqNo = Me.AssetSeqNo
                pDoc.strConnection = GetConnectionString()
                pDoc.listData = dtJanji
                Dim oCustomClass = oController.SaveDocReceiveJanji(pDoc)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                If j = 0 Then
                    Exit Sub
                End If
            End If

            Try
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .listData = dtAdd
                    .BranchId = Me.BranchID
                    .ApplicationId = Me.ApplicationID
                    .ReceiveDate = ConvertDate2(txtsdate.Text)
                    .ReceiveFrom = txtReceiveFrom.Text.Trim
                    .ChasisNo = "" 'txtChasisNo.Text.Trim
                    .EngineNo = "" 'txtEngineNo.Text.Trim
                    .LicensePlate = "" ' txtLicPlate.Text.Trim 
                    .taxdate = ConvertDate2("1/1/1900")
                    .RackLoc = IIf(lblRack.Visible = True, Me.Rack, cboRack.SelectedValue.Trim) '  Me.Rack.Trim
                    .FillingLoc = IIf(lblFLoc.Visible = True, Me.FLoc, cboFill.SelectedValue.Trim) '  Me.FLoc.Trim
                    .BusinessDate = Me.BusinessDate
                    .AssetSeqNo = Me.AssetSeqNo
                    .OwnerAsset = "" ' txtName.Text
                    .OwnerAddress = "" 'UCAddress.Address
                    .OwnerRT = ""
                    .OwnerRW = ""
                    .OwnerKelurahan = ""
                    .OwnerKecamatan = ""
                    .OwnerCity = ""
                    .Notes = "" 'txtAssetNotes.Text
                    .isMainDoc = k > 0
                    .strGroupId = strGroupID.Trim
                    .IsTbo = True
                    .BranchOnStatus = Me.sesBranchId.Replace("'", "")
                End With

                oCustomClass = oController.DocRecSave(oCustomClass)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Response.Redirect("DocReceiveTbo.aspx")
            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                lblMessage.Visible = True
                Exit Sub
                'Finally

            End Try
        End If

        'End If
    End Sub

End Class