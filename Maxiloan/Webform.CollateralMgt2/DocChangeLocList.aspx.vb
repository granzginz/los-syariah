﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocChangeLocList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property BranchID2() As String
        Get
            Return (CType(Viewstate("BranchID2"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID2") = Value
        End Set
    End Property
    Private Property BranchID_BPKB() As String
        Get
            Return (CType(Viewstate("BranchID_BPKB"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID_BPKB") = Value
        End Set
    End Property
    Private Property BranchName_BPKB() As String
        Get
            Return (CType(Viewstate("BranchName_BPKB"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchName_BPKB") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(Viewstate("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(Viewstate("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("TotOH") = Value
        End Set
    End Property

    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(Viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Private Property RackFillLoc() As IList(Of RackFill)
        Get
            Return (CType(ViewState("RackFillLoc"), IList(Of RackFill)))
        End Get
        Set(ByVal Value As IList(Of RackFill))
            ViewState("RackFillLoc") = Value
        End Set
    End Property



#End Region


    'Private Property FormType() As String
    '    Get
    '        Return (CType(ViewState("FormType"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("FormType") = Value
    '    End Set
    'End Property
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
    Private m_controller As New DataUserControlController

#End Region
#Region "page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub


        If Not IsPostBack Then


            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.BranchID2 = Request.QueryString("branchid2")
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))



            Me.FormID = "DOCCHANGELOC"


            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim oPosition As New Parameter.DocRec
                Dim oNewPosition As New Parameter.DocRec

                oPosition.strConnection = GetConnectionString()
                oPosition.BranchId = Me.BranchID2
                oPosition.sesBranchID = Me.BranchID
                oPosition.ApplicationId = Me.ApplicationID
                oNewPosition = oController.GetBPKBPosition(oPosition)
                Dim strBranchName As String
                Dim strBranchID As String
                strBranchID = oNewPosition.BranchId
                strBranchName = oNewPosition.BranchName

                Me.BranchID_BPKB = strBranchID.Trim
                Me.BranchName_BPKB = strBranchName

                If strBranchID <> Me.BranchID Then
                    ShowMessage(lblMessage, "Anda Tidak Berhak. Dokumen disimpan di Cabang  " & strBranchName & "!", True)
                    btnSave.Visible = False
                End If

                DoBind()
                DoBindGrid()

                If Me.TotOH <= 0 Then
                    ShowMessage(lblMessage, "Tidak ada dokumen on hand!", True)
                    btnSave.Visible = False
                End If



                Dim dt_branch As New DataTable
                dt_branch = m_controller.GetBranchAll(GetConnectionString)
                For i = dt_branch.Rows.Count - 1 To 0 Step -1
                    Dim _r = dt_branch.Rows(i)
                     
                    If Not (_r("ID") = Me.BranchID2 Or _r("ID") = "000") Then
                        dt_branch.Rows.RemoveAt(i)
                    End If
                   


                Next

                With cboBranch
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dt_branch
                    .DataBind()
                    
                    
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                cboBranch.Items.FindByValue(Me.BranchID).Selected = True

                Me.RackFillLoc = RackFillHelper.GetRacks(GetConnectionString, Me.BranchID)

                'Dim dtbranch As New DataTable
                'Dim dtEmp As New DataTable
                'dtbranch = oController.GetRack(GetConnectionString, Me.BranchID)
                'With cboRack
                '    .DataTextField = "Name"
                '    .DataValueField = "ID"
                '    .DataSource = dtbranch
                '    .DataBind()
                '    .Items.Insert(0, "Select One")
                '    .Items(0).Value = "0"
                'End With

                'Dim where = getRackIds()

                'Dim dtFill As New DataTable
                'With oCustomClass
                '    .strConnection = GetConnectionString()
                '    .WhereCond = where
                'End With
                With cboRack
                    .DataTextField = "Text"
                    .DataValueField = "Value"
                    .DataSource = Me.RackFillLoc
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                RefreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)

            End If

        Else
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = ""
            End With
        End If

    End Sub

    'Sub refreshCboFill(value As IList(Of Parameter.ValueTextObject), cbo As DropDownList)
    '    With cbo
    '        .DataTextField = "Text"
    '        .DataValueField = "Value"
    '        .DataSource = value
    '        .DataBind()
    '        .Items.Insert(0, "Select One")
    '        .Items(0).Value = "0"
    '    End With
    'End Sub

    'Function getRackIds() As String
    '    Dim builder = New StringBuilder
    '    Dim i = 0
    '    builder.Append(" where rackid in ( ")
    '    For Each item As ListItem In cboRack.Items
    '        builder.Append(String.Format(" {0} '{1}' ", IIf(i = 0, "", ","), item.Value))
    '        i += 1
    '    Next
    '    Return builder.Append(" ) ").ToString()
    'End Function


#End Region
#Region "DoBind"
    Sub DoBind()
        Dim inCustID As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID2
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            hyCustomerName.Text = .Customername
            inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustID) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier( '" & "AssetDocument" & "','" & Server.UrlEncode(inSupId) & "' )"

            lblAssetDesc.Text = .assetDesc
            lblEngineNo.Text = .EngineNo
            Me.Chasis = .ChasisNo
            lblLicPlate.Text = .LicensePlate
            lblChasisNo.Text = .ChasisNo
            lblTaxDate.Text = .taxdate.ToString("dd/MM/yyyy")
            lblCurrRack.Text = .RackLoc
            lblCurrF.Text = .FillingLoc
            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
        End With
    End Sub
#End Region
#Region "RackChange"

    Protected Function RackChange() As String
        Return "ParentChange('" & Trim(cboRack.ClientID) & "','" & Trim(cboFill.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

#End Region
#Region "GenerateScript"

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboRack.Items.Count - 1

            DataRow = DtTable.Select(" RackId = '" & cboRack.Items(j).Value & "'")


            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("RackId")).Trim Then
                        strType = CStr(DataRow(i)("RackId")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("filldesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("fillingid")), "null", DataRow(i)("fillingid"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("filldesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("fillingid")), "null", DataRow(i)("fillingid"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

#End Region
#Region "DoBindGrid"
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            '.WhereCond = " adc.branchid = '" & Me.BranchID2 & "' and adc.assetdocstatus = 'O'  and applicationid = '" & Me.ApplicationID & "'and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            '.WhereCond = " adc.branchid = '" & Me.BranchID2 & "' and adc.assetdocstatus IN ('I', 'O') and applicationid = '" & Me.ApplicationID & "'and adc.assetseqno ='" & Me.AssetSeqNo & "'"

            'Modify by Wira, 20170329, dokumen yang bisa pindah lokasi hanya BPKB saja
            'Modify by Wira, 20171002,dokumen yang bisa pindah lokasi adalah dokument utama (lihat asset document list)
            '.WhereCond = " adc.branchid = '" & Me.BranchID2 & "' and adc.assetdocstatus IN ('I', 'O') and applicationid = '" & Me.ApplicationID & "'and adc.assetseqno ='" & Me.AssetSeqNo & "'and adc.assetdocid ='BPKB'"
            .WhereCond = " adc.branchid = '" & Me.BranchID2 & "' and adc.assetdocstatus IN ('I', 'O') and applicationid = '" & Me.ApplicationID & "'and adc.assetseqno ='" & Me.AssetSeqNo & "'and adl.IsMainDoc ='1'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()
    End Sub
#End Region
#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("DocChangeLoc.aspx")
    End Sub
#End Region

#Region "DataBound"

    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound
        Dim inlblAssetDocStatus As Label

        If e.Item.ItemIndex >= 0 Then
            inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            'If inlblAssetDocStatus.Text.Trim = "O" Then OH += 1
            If inlblAssetDocStatus.Text.Trim = "I" Or inlblAssetDocStatus.Text.Trim = "O" Then OH += 1
        End If

        Me.TotOH = OH
    End Sub
#End Region
#Region "Save"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If (cboRack.SelectedItem.Text = lblCurrRack.Text.Trim) And (cboFill.SelectedItem.Text.Trim = lblCurrF.Text.Trim) Then
                ShowMessage(lblMessage, "Harap pilih Lokasi Lain", True)
                btnSave.Visible = False
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .AssetSeqNo = Me.AssetSeqNo
                .ApplicationId = Me.ApplicationID
                .BranchId = Me.BranchID2
                .BusinessDate = Me.BusinessDate
                .BranchInTransit = cboBranch.SelectedItem.Value.Trim

                If cboRack.SelectedItem.Value.Trim = "" Then
                    .RackLoc = ""
                    .FillingLoc = ""
                Else
                    .FillingLoc = cboFill.SelectedItem.Value.Trim ' hdnChildValue.Value.Trim
                    .RackLoc = cboRack.SelectedItem.Value
                End If
            End With
            Try
                oCustomClass = oController.DocChangeLocSave(oCustomClass)
                If oCustomClass.strError <> "" Then
                    ShowMessage(lblMessage, "Update lokasi Gagal", True)
                    Exit Sub
                Else
                    Response.Redirect("DocChangeLoc.aspx?success=false&message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                lblMessage.Visible = True
                Exit Sub

            End Try
        End If
    End Sub
#End Region

    Private Sub cboRack_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRack.SelectedIndexChanged

        Dim cbo = CType(sender, DropDownList).SelectedValue
        If (cbo = "0") Then
            refreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)
            Return
        End If

        Dim k = RackFillLoc.Where(Function(x) x.Value = cbo).SingleOrDefault()
        refreshCboFill(k.FillLocs, cboFill)

    End Sub
#Region "cboBranch_SelectedIndexChanged"
    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        If cboBranch.SelectedItem.Value <> Me.BranchID Then
            cboRack.Enabled = False
            cboFill.Enabled = False

            rfvcboRack.Enabled = False
            rfvFilling.Enabled = False
        Else
            cboRack.Enabled = True
            cboFill.Enabled = True
            rfvcboRack.Enabled = True
            rfvFilling.Enabled = True

            'Dim dtbranch As New DataTable
            'Dim dtEmp As New DataTable
            'dtbranch = oController.GetRack(Me.GetConnectionString, Me.BranchID)
            'With cboRack
            '    .DataTextField = "Name"
            '    .DataValueField = "ID"
            '    .DataSource = dtbranch
            '    .DataBind()
            '    .Items.Insert(0, "Select One")
            '    .Items(0).Value = "0"
            'End With

            'Dim dtFill As New DataTable
            'With oCustomClass
            '    .strConnection = Me.GetConnectionString()
            '    .WhereCond = ""
            'End With
            'dtFill = oController.GetFillLoc(oCustomClass)
            'Response.Write(GenerateScript(dtFill))
        End If
    End Sub
#End Region

    ''Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)       
    ''    Dim DtChild As DataTable = oController.GetFillLoc(oCustomClass)
    ''    Dim i As Integer

    ''    For i = 0 To DtChild.Rows.Count - 1
    ''        Page.ClientScript.RegisterForEventValidation(cboFill.UniqueID, CStr(DtChild.Rows(i).Item("fillingid")).Trim)
    ''    Next

    ''    MyBase.Render(writer)
    ''End Sub

End Class

<Serializable()>
Public Class RackFill
    Inherits Parameter.ValueTextObject
    Private _FillLocs As IList(Of Parameter.ValueTextObject)
    Sub New()
        _FillLocs = New List(Of Parameter.ValueTextObject)
    End Sub
    Public Property FillLocs As IList(Of Parameter.ValueTextObject)
        Get
            Return _FillLocs
        End Get
        Set(value As IList(Of Parameter.ValueTextObject))
            _FillLocs = value
        End Set
    End Property

End Class

Module RackFillHelper

    Public Function GetRacks(cnn As String, branchId As String, Optional isFund As Boolean = False) As IList(Of RackFill)
        Dim oController As New DocReceiveController
        Dim result = New List(Of RackFill)
        Dim dtRacks = oController.GetRack(cnn, branchId, isFund)

        For Each row In dtRacks.Rows
            Dim r = New RackFill() With {.Value = row("ID").ToString().Trim, .Text = row("Name")}
            result.Add(getFillLoc(cnn, r))
        Next

        Return result
    End Function

    Function getFillLoc(cnn As String, _row As RackFill) As RackFill
        Dim oController As New DocReceiveController
        Dim oCustomClass As New Parameter.DocRec With {.strConnection = cnn, .WhereCond = String.Format(" where rackid in ('{0}')", _row.Value)}
        Dim fillLocs = oController.GetFillLoc(oCustomClass)
        Dim _FillLocs = New List(Of Parameter.ValueTextObject)

        For Each row In fillLocs.Rows
            _FillLocs.Add(New Parameter.ValueTextObject(row("fillingid").ToString().Trim, row("filldesc")))
        Next
        _row.FillLocs = _FillLocs
        Return _row
    End Function


    Public Sub RefreshCboFill(value As IList(Of Parameter.ValueTextObject), cbo As DropDownList)
        With cbo
            .DataTextField = "Text"
            .DataValueField = "Value"
            .DataSource = value
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
End Module
