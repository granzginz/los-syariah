﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RackDoc.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.RackDoc" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RackDoc</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR RAK
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgRack" runat="server" AllowSorting="True" OnSortCommand="SortGrid"
                        AutoGenerateColumns="False" DataKeyField="RackID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                         <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Branchid" HeaderText="ID CABANG" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblIsfund" runat="server" Text='<%#Container.DataItem("IsFunding")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RackID" HeaderText="ID RAK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblRackID" runat="server" Text='<%#Container.DataItem("RackID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="NAMA RAK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsActive" HeaderText="STATUS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" Text='<%#Iif(Container.DataItem("IsActive")="1","Active","Not Active")%>'>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FILLING">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbFilling" runat="server" CausesValidation="False" ImageUrl="../../Images/Down.gif"
                                        CommandName="FILLING"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           
                        </Columns>                        
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                            OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                            OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                            OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                            OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px" CssClass="InpType">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtpage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                            MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtpage" ErrorMessage="No Halaman Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
           
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI RAK DAN FILLING
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlAdd" runat="server" Visible="false">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblJudul" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Rak</label>
                <asp:TextBox ID="txtRackID" runat="server" Width="101px" CssClass="inptype"></asp:TextBox>
                <asp:Label ID="lblRack" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Rak</label>
                <asp:TextBox ID="txtDesc" runat="server" Width="545px" CssClass="inptype"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Active</label>
                <asp:CheckBox ID="chkActive" runat="server"></asp:CheckBox>
            </div>
        </div>
           <div class="form_box" id="dvF" runat="server" visible="false">
            <div class="form_single">
                <label>
                   Rak Funding</label>
                <asp:CheckBox ID="chkIsFunding" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
