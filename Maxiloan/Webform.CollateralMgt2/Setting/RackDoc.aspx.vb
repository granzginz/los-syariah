﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class RackDoc
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property
    Private Property RackID() As String
        Get
            Return (CType(Viewstate("RackID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RackID") = Value
        End Set
    End Property
    Private Property IsAdd() As String
        Get
            Return (CType(Viewstate("IsAdd"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("IsAdd") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController

#End Region

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "RACKDOC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                oSearchBy.ListData = "RackID, Rack ID-description,Description"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spDocRackPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgRack.DataSource = DtUserList.DefaultView
        DtgRack.CurrentPageIndex = 0
        DtgRack.DataBind()
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        PnlAdd.Visible = False

    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("RackDoc.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then


            Dim par As String
            par = ""
            Me.SearchBy = ""

            Me.SearchBy = " BranchID = '" & Me.sesBranchId.Replace("'", "").Trim & "'"
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
                If par <> "" Then
                    par = par & " , " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("%", "") & "%' "
                Else
                    par = par & "" & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("%", "") & "%' "
                End If
            End If
            Me.ParamReport = par
            pnlDatagrid.Visible = True
            DtgRack.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region "ItemCommand"

    Private Sub DtgRack_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgRack.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                    txtRackID.Visible = False
                    lblRack.Visible = True
                    Me.IsAdd = "2"
                    Dim inRackID As Label
                    Dim inDesc As Label
                    Dim inActive As Label
                    inRackID = CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblRackID"), Label)
                    inDesc = CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblDescription"), Label)
                    inActive = CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblIsActive"), Label)


                    chkIsFunding.Checked = CBool(CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblIsfund"), Label).Text)



                    txtDesc.Text = inDesc.Text.Trim
                    If inActive.Text.Trim = "Active" Then
                        chkActive.Checked = True
                    Else
                        chkActive.Checked = False
                    End If
                    lblRack.Text = inRackID.Text.Trim
                    lblJudul.Text = "RACK - EDIT"
                    pnlDatagrid.Visible = False
                    PnlAdd.Visible = True
                    pnlList.Visible = False
                    dvF.Visible = IsHoBranch
                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim inRackID As Label
                    inRackID = CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblRackID"), Label)
                    oCustomClass.strConnection = GetConnectionString
                    oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                    oCustomClass.RackLocID = inRackID.Text
                    Dim strError As String
                    Try
                        oCustomClass = oController.DeleteRack(oCustomClass)
                        If oCustomClass.strError = "" Then
                            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        End If
                        DoBind(Me.SearchBy, Me.SortBy)
                    Catch Exp As Exception
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                    End Try
                End If
            Case "FILLING"
                Dim inRackID As Label
                Dim inRackDesc As Label
                inRackID = CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblRackID"), Label)
                inRackDesc = CType(DtgRack.Items(e.Item.ItemIndex).FindControl("lblDescription"), Label)
                Response.Redirect("FillingDoc.aspx?RackId=" & inRackID.Text.Trim & "&Rackdesc=" & inRackDesc.Text.Trim)
        End Select
    End Sub

#End Region

#Region "BindAdd"
    Sub BindAdd()

        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.RackLocID = Me.RackID
        oCustomClass.RackLoc = txtDesc.Text.Trim
        oCustomClass.IsAdd = Me.IsAdd
        If chkActive.Checked Then
            oCustomClass.IsActive = 1
        Else
            oCustomClass.IsActive = 0
        End If

        If chkIsFunding.Checked Then
            oCustomClass.IsFunding = 1
        Else
            oCustomClass.IsFunding = 0
        End If
        Try
            oCustomClass = oController.AddRack(oCustomClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            DoBind(Me.SearchBy, Me.SortBy)
            'If oCustomClass.strError = "" Then
            '    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            '    If Me.IsAdd = "1" Then
            '        Response.Redirect("RackDoc.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
            '    Else
            '        DoBind(Me.SearchBy, Me.SortBy)
            '    End If

            'End If

        Catch ex As Exception
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
            Exit Sub
        End Try
    End Sub

#End Region

#Region "imbAdd"

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        dvF.Visible = IsHoBranch

        txtRackID.Visible = True
        lblRack.Visible = False
        txtRackID.Text = ""
        txtDesc.Text = ""
        chkActive.Checked = False
        chkIsFunding.Checked = False
        Me.IsAdd = "1"
        lblJudul.Text = "RACK - ADD"
        pnlDatagrid.Visible = False
        PnlAdd.Visible = True
        pnlList.Visible = False
    End Sub

#End Region

#Region "Save"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Me.IsAdd = "1" Then
            Me.RackID = txtRackID.Text.Trim
        Else
            Me.RackID = lblRack.Text
        End If
        BindAdd()
    End Sub

#End Region

#Region "Cancel"

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("RackDoc.aspx")
    End Sub
#End Region



#Region "Databound"


    Private Sub DtgRack_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgRack.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return fConfirm()")
        End If
    End Sub

#End Region

End Class