﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PinjamDariBank.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.PinjamDariBank" %>

   <%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch2" Src="../webform.UserController/UcBranch2.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DocChangeLoc</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip" />
        <div class="form_single">
            <h3> DAFTAR DOKUMEN ASSET ON CREATOR </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate> 
                                <asp:HyperLink ID="HypChange" runat="server" Text='PINJAM' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetSeqNo" HeaderText="ASSET SEQ NO" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>' />
                                    <asp:Label id="hdnBranch"  name="hdnBranch" runat="server"   Text='<%#Container.DataItem("branchid")%>'/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="licenseplate" HeaderText="NO POLISI">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbllicplate" runat="server" Text='<%#Container.DataItem("licenseplate")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetstatus" HeaderText="STATUS">                               
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetstatus")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="assetdocstatusdesc" HeaderText="STATUS DOK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                     <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4> CARI DOKUMEN ASSET </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req"> Cabang</label>
                <asp:DropDownList ID="cbobranch" runat="server"/> 
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cbobranch" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>