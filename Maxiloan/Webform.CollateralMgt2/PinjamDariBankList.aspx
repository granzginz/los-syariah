﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PinjamDariBankList.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.PinjamDariBankList" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../Webform.UserController/ucAddressCity.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DocReceiveList</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        var hdnDetail;
        var hdndetailvalue;
        function ParentChange(pBranch, pRack, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pRack).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pRack).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 1;
            };
            eval('document.forms[0].' + pRack).options[0] = new Option('Select One', '0');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pRack).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pRack).selected = true;
            }
        };

        function cboChildonChange(l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnChild" runat="server" />
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip" />
        <div class="form_single">
            <h3>PINJAM DARI BANK</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>No Kontrak </label>
            <asp:HyperLink ID="hyAgreementNo" runat="server" />
        </div>
        <div class="form_right">
            <label>Nama Customer</label>
            <asp:HyperLink ID="hyCustomerName" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>Nama Asset</label>
            <asp:Label ID="lblAssetDesc" runat="server" />
        </div>
        <div class="form_right">
            <label>Nama Supplier</label>
            <asp:HyperLink ID="lblSupplier" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>Funding Bank </label>
            <asp:Label ID="lblFundingCoName" runat="server" />
        </div>
        <div class="form_right">
            <label> Status Jaminan </label>
            <asp:Label ID="lblFundingPledgeStatus" runat="server" />
        </div>
    </div>
<%--    <div class="form_box">
        <div class="form_left">
            <label> Nama Peminjam </label>
            <asp:Label ID="lblBorrowerName" runat="server" />
        </div>
        <div class="form_right">
            <label> Tanggal Pinjam </label>
            <asp:Label ID="lblBorrowDate" runat="server" />
        </div>
    </div>--%>
    <div class="form_box_header">
        <div class="form_single">
            <h4> REGISTRASI </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label> Tanggal Pinjam </label>
            <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate" Format="dd/MM/yyyy" />
        </div>
        <div class="form_right">            
            <label class ="label_req"> Pinjam Dari</label><label class="mandatory" />           
            <asp:TextBox ID="txtReceiveFrom" runat="server" CssClass="inptype" MaxLength="50" Width="180px">
            </asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Harap diisi Nama" Display="Dynamic" ControlToValidate="txtReceiveFrom" Visible="true" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
    </div>
      <div class="form_box">
    <div class="form_left">
            <label> Tanggal Kembali </label>
            <asp:TextBox runat="server" ID="txtRdate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtRdate" Format="dd/MM/yyyy" />
        </div>
        <div class="form_right">             
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">            
               <label class ="label_req"> <asp:Label ID="serialno1label" runat="server" /><label class="mandatory">*</label>  </label>                                   
            <asp:TextBox ID="txtChasisNo" runat="server" CssClass="inptype" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi No Rangka" ControlToValidate="txtChasisNo" Visible="true" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label class ="label_req"> <asp:Label ID="serialno2label" runat="server" /> </label>
            <%--<asp:Label ID="lblEngineNo" runat="server" />--%>
            <asp:TextBox ID="txtEngineNo" runat="server" CssClass="inptype" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage="Harap isi No Mesin" ControlToValidate="txtEngineNo" Visible="true" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
           <label class ="label_req"> No Polisi </label>
            <asp:TextBox ID="txtLicPlate" runat="server" CssClass="inptype" MaxLength="20"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Harap isi No Polisi" ControlToValidate="txtLicPlate" Visible="true" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label> Tanggal STNK </label>
            <asp:TextBox runat="server" ID="txtTdate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtTdate" Format="dd/MM/yyyy" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">            
                <label class ="label_req">
                    Lokasi Rak <label class="mandatory">
                </label>
                </label>                
            <asp:DropDownList ID="cboRack" runat="server"  AutoPostBack="true" />
            <asp:RequiredFieldValidator ID="rfvcboRack" runat="server" Display="Dynamic" ControlToValidate="cboRack" ErrorMessage="Harap pilih Rak" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            <asp:Label ID="lblRack" runat="server" />
        </div>
        <div class="form_right">            
               <label class ="label_req"> Lokasi Filling<label class="mandatory">
               </label>
                </label>                
            <asp:DropDownList ID="cboFill" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                ControlToValidate="cboFill" ErrorMessage="Harap Pilih Lokasi Filling" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            <asp:Label ID="lblFLoc" runat="server" />
        </div>
    </div>

     <div class="form_box">
        <div class="form_single">            
            <label class="label_general">Catatan</label>
            <asp:TextBox ID="txtAssetNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>

    <div class="form_box_header">
        <div class="form_single">
            <h4>
                DAFTAR DOKUMEN ASSET
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />                    
                    <Columns>
                        <asp:TemplateColumn HeaderText="PINJAM">                            
                            <ItemTemplate>
                                <asp:CheckBox ID="ChkSlct" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOCUMENT ID" Visible="False">                            
                            <ItemTemplate>
                                <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOKUMEN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN">                            
                            <ItemTemplate>
                                <asp:TextBox ID="txtDocNo" runat="server" Text='<%#Container.DataItem("documentno")%>' Width="80%" CssClass="inptype"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFVDocNo" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtDocNo" Display="Dynamic" 
                                            CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                                <asp:Label ID="lblDocNo" runat="server" Visible="false" Text='<%#Container.DataItem("documentno")%>'>
                                </asp:Label>
                                <asp:Label ID="lblIsNoRequired" runat="server"  Visible="false"  Text='<%#Container.DataItem("IsNoRequired")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL DOKUMEN">                            
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtDocdate" Text='<%#Container.DataItem("documentdate")%>'></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDocdate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:Label ID="lblDocDate" runat="server" Text='<%#Container.DataItem("documentdate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="docreceiveddate" HeaderText="TGL TERIMA PERTAMA" DataFormatString="{0:dd/MM/yyyy}">                            
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ASSETSTATUS" Visible="False">                            
                            <ItemTemplate>
                                <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatus")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS">                            
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">                            
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ISMAINDOC" Visible="False">                            
                            <ItemTemplate>
                                <asp:Label ID="lblisMainDoc" runat="server" Text='<%#Container.DataItem("isMainDoc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>                    
                </asp:DataGrid>
            </div>
        </div>
    </div>
    
   
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" Visible="true" CausesValidation="true" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>