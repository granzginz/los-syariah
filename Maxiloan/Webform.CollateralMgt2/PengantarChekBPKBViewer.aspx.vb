﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region


Public Class PengantarChekBPKBViewer
    Inherits Maxiloan.Webform.WebBased

    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub

    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim objReport As PengantarChekBPKBPrint = New PengantarChekBPKBPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .SpName = "spPengantarChekBPKBPrint"
        End With

        oCustomClass = m_coll.GetSPReport(oCustomClass)
        oDataSet = oCustomClass.ListDataReport

        objReport.SetDataSource(oDataSet)
        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()



        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PengantarChekBPKBPrint.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("DocReceive.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PengantarChekBPKBPrint")

    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PengantarChekBPKBPrint")

        Me.CmdWhere = cookie.Values("cmdwhere")
        
    End Sub
End Class