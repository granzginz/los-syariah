﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class DocReceiveList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0
#Region "Property"

    ''Private Property ApplicationID() As String
    ''    Get
    ''        Return (CType(Viewstate("ApplicationID"), String))
    ''    End Get
    ''    Set(ByVal Value As String)
    ''        Viewstate("ApplicationID") = Value
    ''    End Set
    ''End Property
    ''Private Property AgreementNo() As String
    ''    Get
    ''        Return (CType(Viewstate("AgreementNo"), String))
    ''    End Get
    ''    Set(ByVal Value As String)
    ''        Viewstate("AgreementNo") = Value
    ''    End Set
    ''End Property

    ''Private Property Chasis() As String
    ''    Get
    ''        Return (CType(Viewstate("Chasis"), String))
    ''    End Get
    ''    Set(ByVal Value As String)
    ''        Viewstate("Chasis") = Value
    ''    End Set
    ''End Property

    'Private Property Rack() As String
    '    Get
    '        Return (CType(Viewstate("Rack"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("Rack") = Value
    '    End Set
    'End Property

    'Private Property FLoc() As String
    '    Get
    '        Return (CType(Viewstate("FLoc"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("FLoc") = Value
    '    End Set
    'End Property
    'Private Property AssetSeqNo() As Integer
    '    Get
    '        Return (CType(Viewstate("AssetSeqNo"), Integer))
    '    End Get
    '    Set(ByVal Value As Integer)
    '        Viewstate("AssetSeqNo") = Value
    '    End Set
    'End Property
    'Private Property DocStatus() As String
    '    Get
    '        Return (CType(Viewstate("DocStatus"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("DocStatus") = Value
    '    End Set
    'End Property
    'Private Property TotOH() As Integer
    '    Get
    '        Return (CType(Viewstate("TotOH"), Integer))
    '    End Get
    '    Set(ByVal Value As Integer)
    '        Viewstate("TotOH") = Value
    '    End Set
    'End Property


    'Private Property FLocDesc() As String
    '    Get
    '        Return (CType(Viewstate("FLocDesc"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("FLocDesc") = Value
    '    End Set
    'End Property

    'Private Property RackDesc() As String
    '    Get
    '        Return (CType(Viewstate("RackDesc"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("RackDesc") = Value
    '    End Set
    'End Property
    ' ''Private Property BranchID2() As String
    ' ''    Get
    ' ''        Return (CType(Viewstate("BranchID2"), String))
    ' ''    End Get
    ' ''    Set(ByVal Value As String)
    ' ''        Viewstate("BranchID2") = Value
    ' ''    End Set
    ' ''End Property
    'Private Property IsDocInFunding() As Boolean
    '    Get
    '        Return (CType(ViewState("IsDocInFunding"), Boolean))
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        ViewState("IsDocInFunding") = Value
    '    End Set
    'End Property
    Private Property FlagRack() As String
        Get
            Return (CType(Viewstate("FlagRack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FlagRack") = Value
        End Set
    End Property
    Private Property RackFillLoc() As IList(Of RackFill)
        Get
            Return (CType(ViewState("RackFillLoc"), IList(Of RackFill)))
        End Get
        Set(ByVal Value As IList(Of RackFill))
            ViewState("RackFillLoc") = Value
        End Set
    End Property
      
    Private Property SelectDocStatus() As String
        Get
            Return (CType(ViewState("SelectDocStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectDocStatus") = Value
        End Set
    End Property


    Private Property DocRecord() As Parameter.DocRec
        Get
            Return (CType(ViewState("DOCUMENTRECORD"), Parameter.DocRec))
        End Get
        Set(ByVal Value As Parameter.DocRec)
            ViewState("DOCUMENTRECORD") = Value
        End Set
    End Property

    Private Property AssetUsageID() As String
        Get
            Return (CType(ViewState("AssetUsageID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsageID") = Value
        End Set
    End Property

    Private Property IsIzinTrayek() As Byte
        Get
            Return (CInt(ViewState("IsIzinTrayek")))
        End Get
        Set(ByVal Value As Byte)
            ViewState("IsIzinTrayek") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    'Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
    Protected WithEvents UCAddress As ucAddressCity

#End Region
#Region "page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim oReceiveList As New Parameter.DocRec
           
            Me.BranchID = Request.QueryString("branchid").Trim
            Me.SelectDocStatus = Request.QueryString("DocStatus")
            oReceiveList.BusinessDate = Me.BusinessDate
            oReceiveList.ApplicationId = Request.QueryString("applicationid")
            oReceiveList.BranchId = Request.QueryString("branchid2").Trim
            oReceiveList.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))
            oReceiveList.strConnection = GetConnectionString()
            DocRecord = oReceiveList

            Me.FormID = "DOCRECEIVE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy") 
               
                oReceiveList = oController.GetTaxDate(DocRecord)
                DocRecord = oReceiveList
                Dim strTaxDate As String
                If ConvertDate2(oReceiveList.taxdate.ToString("dd/MM/yyyy")) <= ConvertDate2("01/01/1900") Then
                    strTaxDate = Me.BusinessDate.ToString("dd/MM/yyyy")
                Else
                    strTaxDate = oReceiveList.taxdate.ToString("dd/MM/yyyy")
                End If

                txtTdate.Text = strTaxDate

                DoBind()
                DoBindGrid()
                GetAssetRegistration()
                If (DocRecord.FillingLoc <> "-" And DocRecord.RackLoc <> "-") Then
                    'If Me.RackDesc <> "-" And Me.FLocDesc <> "-" Then
                    lblRack.Visible = True
                    lblFLoc.Visible = True
                    cboRack.Visible = False
                    cboFill.Visible = False
                    lblRack.Text = DocRecord.RackLoc ' Me.RackDesc
                    lblFLoc.Text = DocRecord.FillingLoc ' Me.FLocDesc
                    Me.FlagRack = "1"
                Else
                    lblRack.Visible = False
                    lblFLoc.Visible = False
                    cboRack.Visible = True
                    cboFill.Visible = True
                    Me.FlagRack = "2"
                    Me.RackFillLoc = RackFillHelper.GetRacks(GetConnectionString, Me.BranchID, (Me.BranchID = "000" And DocRecord.DocInFunding = False))

                    With cboRack
                        .DataTextField = "Text"
                        .DataValueField = "Value"
                        .DataSource = Me.RackFillLoc
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End With
                    RefreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)
                End If

            End If
        End If

    End Sub

#End Region
#Region "DoBind"
    Sub DoBind()
        'Dim inCustID As String
        
         
        DocRecord = oController.ListDoc(DocRecord)
        With DocRecord
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(DocRecord.ApplicationId) & "')"
            hyCustomerName.Text = .Customername
            'Dim inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(.CustomerID) & "')"
            'inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & Server.UrlEncode(.Supplierid) & "', '" & "AssetDocument" & "')"
            lblAssetDesc.Text = .assetDesc
            lblKondisiAset.Text = .KondisiAset
            txtEngineNo.Text = .EngineNo

            txtLicPlate.Text = .LicensePlate 

            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            txtChasisNo.Text = .ChasisNo.Trim
            txtChasisNo.Enabled = True
            txtEngineNo.Enabled = True

            Select Case (.AssetDocStatus)
                Case "W"
                    txtReceiveFrom.Text = .SupplierName.Trim
                Case "I"
                    txtReceiveFrom.Text = getBranchName(.BranchId)
                Case "B"
                Case "H"
                    txtReceiveFrom.Text = IIf(.DocInFunding = False, "CustodianBPKB", "Funding") '.SupplierName.Trim 
            End Select


            If .NamaBPKBSama Then
                lblNamaBPKBSama.Text = "Ya"
            Else
                lblNamaBPKBSama.Text = "Tidak"
            End If

            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
            lblBorrowerName.Text = .BorrowBy

            Dim strBorrowDate As String
            strBorrowDate = .BorrowDate.ToString("dd/MM/yyyy")

            If strBorrowDate = "01/01/1900" Then
                lblBorrowDate.Text = "-"
            Else
                lblBorrowDate.Text = strBorrowDate
            End If

            lblAssetUsage.Text = .AssetUsageDesc
            Me.AssetUsageID = .AssetUsage
            Me.IsIzinTrayek = .IsIzinTrayek

        End With
    End Sub
#End Region

    Function getBranchName(branchid As String) As String

        If (IsHoBranch = False) Then branchid = "000"
        Dim m_controller As New DataUserControlController
        Dim dt_branch As New DataTable
        dt_branch = m_controller.GetBranchAll(GetConnectionString)
        For Each row In dt_branch.Rows
            If row("ID").ToString.Trim = branchid Then
                Return row("Name")
            End If
        Next
        Return ""
    End Function
#Region "GetAssetRegistration"
    Sub GetAssetRegistration()
        'Dim oAssetReg As New Parameter.DocRec

        'oAssetReg.ApplicationId = Me.ApplicationID
        'oAssetReg.BranchId = Me.BranchID2
        'oAssetReg.strConnection = GetConnectionString
        DocRecord = oController.GetAssetRegistration(DocRecord)
        txtName.Text = DocRecord.OwnerAsset

        Dim strOwnerAddress As String
        If IsDBNull(DocRecord.OwnerAddress) Then
            strOwnerAddress = "-"
        Else
            strOwnerAddress = DocRecord.OwnerAddress
        End If

        UCAddress.Address = strOwnerAddress & IIf(DocRecord.OwnerRT.Trim = "", "", " RT." & DocRecord.OwnerRT.Trim & " ").ToString _
                            + " " & IIf(DocRecord.OwnerRW.Trim = "", "", " RW." & DocRecord.OwnerRW.Trim & " ").ToString _
                            + " " & IIf(DocRecord.OwnerKelurahan.Trim = "", "", DocRecord.OwnerKelurahan.Trim & " ").ToString _
                            + " " & IIf(DocRecord.OwnerKecamatan.Trim = "", "", DocRecord.OwnerKecamatan.Trim & "").ToString
        UCAddress.City = DocRecord.OwnerCity
        With UCAddress
            .KodePosFalse()
            .TeleponFalse()
            .BindAddress()
            .ValidatorTrue()
        End With
        txtAssetNotes.Text = DocRecord.Notes
    End Sub
#End Region
#Region "RackChange"

    Protected Function RackChange() As String
        Return "ParentChange('" & Trim(cboRack.ClientID) & "','" & Trim(cboFill.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

#End Region
#Region "GenerateScript"

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String = ""
        Dim strScript1 As String = ""
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboRack.Items.Count - 1

            DataRow = DtTable.Select(" RackId = '" & cboRack.Items(j).Value & "'")


            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("RackId")).Trim Then
                        strType = CStr(DataRow(i)("RackId")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("filldesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("fillingid")), "null", DataRow(i)("fillingid"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("filldesc")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("fillingid")), "null", DataRow(i)("fillingid"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

#End Region
#Region "DoBindGrid"
    Sub DoBindGrid()
        'Modify by Wira 20170329
        Dim BPKBdanBadanUsaha As String = ""
        Dim IjinTrayek As String = ""
        Dim notIn As String = ""
        Dim IsCP As String = ""

        If Me.IsIzinTrayek = 1 Then
            IjinTrayek = "'IZNTRAY'"
        End If

        If IjinTrayek <> "" Then
            notIn = "and  adl.AssetDocID not in (" & IjinTrayek & ")"
        End If

        If Me.AssetUsageID = "N" Then
            IsCP = " and adl.ispassenger=1"
        Else
            IsCP = " and adl.isCommercial=1"
        End If

        DocRecord.WhereCond = String.Format("adc.branchid = '{0}'  and applicationid = '{1}'  and adc.assetseqno ='{2}'", DocRecord.BranchId, DocRecord.ApplicationId, DocRecord.AssetSeqNo)
        DocRecord.WhereCond = DocRecord.WhereCond + notIn + IsCP
        'End Modify

        DocRecord.SortBy = "  adc.applicationid"
        DocRecord = oController.DocListPaging(DocRecord)
        DtgDoc.DataSource = DocRecord.listDoc.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()


        'Dim DtUserList As New DataTable
        'Dim DvUserList As New DataView


        ''With oCustomClass
        ''    .strConnection = GetConnectionString
        ''    .WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "'  and adc.assetseqno ='" & Me.AssetSeqNo & "'"
        ''    .SortBy = "  adc.applicationid"
        ''End With


        'DtUserList = oCustomClass.listDoc 
    End Sub
#End Region
#Region "DataBound"
    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound
        Dim TotOH As Integer
        Dim inlblAssetDocStatus As Label
        Dim inChkSlct As CheckBox
        Dim inDocDate, intxtPromisedate As TextBox
        Dim inlblDocDate, inlblPromisedate As New Label
        Dim lblIsNoRequired As New Label
        Dim txtDocNo As New TextBox        
        Dim RFVDocNo As New RequiredFieldValidator

        If e.Item.ItemIndex >= 0 Then 
            inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            inChkSlct = CType(e.Item.FindControl("ChkSlct"), CheckBox)
            inDocDate = CType(e.Item.FindControl("txtDocDate"), TextBox)
            intxtPromisedate = CType(e.Item.FindControl("txtPromisedate"), TextBox)
            inlblDocDate = CType(e.Item.FindControl("lblDocDate"), Label)
            inlblPromisedate = CType(e.Item.FindControl("lblPromisedate"), Label)
            lblIsNoRequired = CType(e.Item.FindControl("lblIsNoRequired"), Label)
            txtDocNo = CType(e.Item.FindControl("txtDocNo"), TextBox)
            RFVDocNo = CType(e.Item.FindControl("RFVDocNo"), RequiredFieldValidator)
            Dim txtFollowUp = CType(e.Item.FindControl("txtFollowUp"), TextBox)

            inDocDate.Visible = True
            inDocDate.Enabled = True
            intxtPromisedate.Visible = True
            intxtPromisedate.Enabled = True
            txtFollowUp.Enabled = False

            inDocDate.Visible = (inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P")
            intxtPromisedate.Visible = (inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P")
            inlblDocDate.Visible = Not (inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P")
            inlblPromisedate.Visible = Not (inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P")

            txtDocNo.Visible = True
            RFVDocNo.Visible = CBool(lblIsNoRequired.Text)
          

            If inlblAssetDocStatus.Text.Trim = "W" Or inlblAssetDocStatus.Text.Trim = "P" Or inlblAssetDocStatus.Text.Trim = "B" Or inlblAssetDocStatus.Text.Trim = "I" Or inlblAssetDocStatus.Text.Trim = "G" Or inlblAssetDocStatus.Text.Trim = "H" Then
                If (inlblAssetDocStatus.Text.Trim = "W") Or (inlblAssetDocStatus.Text.Trim = "P") Or (inlblAssetDocStatus.Text.Trim = "B") Or (inlblAssetDocStatus.Text.Trim = "I") Or (inlblAssetDocStatus.Text.Trim = "G") Or (inlblAssetDocStatus.Text.Trim = "H") Then
                    inChkSlct.Visible = True
                    txtFollowUp.Enabled = True
                    OH = OH + 1
                Else
                    inChkSlct.Visible = False
                End If
            Else
                inChkSlct.Visible = False
            End If

            TotOH = OH

            If TotOH <= 0 Then
                ShowMessage(lblMessage, "Dokumen Sudah diterima", True)
                btnSave.Visible = False
            Else
                btnSave.Visible = True
            End If

        End If

    End Sub
#End Region
    Private Sub cboRack_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRack.SelectedIndexChanged

        Dim cbo = CType(sender, DropDownList).SelectedValue
        If (cbo = "0") Then
            refreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)
            Return
        End If

        Dim k = RackFillLoc.Where(Function(x) x.Value = cbo).SingleOrDefault()
        refreshCboFill(k.FillLocs, cboFill)

    End Sub

#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("DocReceive.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If ConvertDate2(txtsdate.Text.Trim) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Terima tidak boleh lebih besar dari hari ini", True)
                Exit Sub
            End If
            'Wira 20160623
            ' Jika bukan asset baru
            'If (DocRecord.KondisiAset = "Baru" And DocRecord.ChasisNo <> "" And DocRecord.ChasisNo <> txtChasisNo.Text.Trim) Or
            '    (DocRecord.KondisiAset <> "Baru" And DocRecord.ChasisNo <> "" And DocRecord.ChasisNo <> txtChasisNo.Text.Trim) Then
            '    ShowMessage(lblMessage, "Harap hubungi IT untuk ubah nomor rangka dan nomor mesin", True)
            '    Exit Sub
            'End If

            'If (DocRecord.KondisiAset = "Baru" And DocRecord.EngineNo <> "" And DocRecord.EngineNo <> txtEngineNo.Text.Trim) Or
            '    (DocRecord.KondisiAset <> "Baru" And DocRecord.EngineNo <> "" And DocRecord.EngineNo <> txtEngineNo.Text.Trim) Then
            '    ShowMessage(lblMessage, "Harap hubungi IT untuk ubah nomor rangka dan nomor mesin", True)
            '    Exit Sub
            'End If

            'If DocRecord.ChasisNo <> txtChasisNo.Text.Trim Then
            '    ShowMessage(lblMessage, "No Rangka Salah", True)
            '    Exit Sub
            'End If

            If DtgDoc.Items.Count > 0 Then
                Dim dtAdd As New DataTable
                Dim dtJanji As New DataTable
                Dim dr As DataRow
                Dim strGroupID As String
                Dim AssetDocNO As String
                Dim adaJanji As Boolean = False

                strGroupID = ""
                For Each s As String In New String() {"isDocExist", "DocName", "DocNo", "DocDate", "Receiveddate", "AssetDocStatus", "StatusDate", "IsMainDoc", "AssetDocID", "PromiseDate", "FollowUpAction"}
                    dtAdd.Columns.Add(New DataColumn(s))
                Next
                For Each s As String In New String() {"AssetDocID", "PromiseDate", "FollowUpAction"}
                    dtJanji.Columns.Add(New DataColumn(s))
                Next
                Dim j = 0
                Dim k = 0
                For i = 0 To DtgDoc.Items.Count - 1
                    If CType(DtgDoc.Items(i).FindControl("ChkSlct"), CheckBox).Checked Then
                        j = j + 1

                        Dim isRec = CType(DtgDoc.Items(i).FindControl("lblrec"), Label)
                        Dim inDocname = CType(DtgDoc.Items(i).FindControl("lblDocname"), Label)
                        Dim inDocNo = CType(DtgDoc.Items(i).FindControl("txtDocNo"), TextBox)
                        Dim inlblDocNo = CType(DtgDoc.Items(i).FindControl("lblDocNo"), Label)
                        Dim inDocDate = CType(DtgDoc.Items(i).FindControl("txtDocDate"), TextBox)
                        Dim inReceiveddate = DtgDoc.Items(i).Cells(3).Text
                        Dim inAssetDocStatus = CType(DtgDoc.Items(i).FindControl("lblAssetDocStatus"), Label)
                        Dim inStatusDate = DtgDoc.Items(i).Cells(7).Text
                        Dim inisMainDoc = CType(DtgDoc.Items(i).FindControl("lblisMainDoc"), Label)
                        Dim inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)
                        Dim lblIsNoRequired = CType(DtgDoc.Items(i).FindControl("lblIsNoRequired"), Label)
                        Dim pomisedate = CType(DtgDoc.Items(i).FindControl("txtPromisedate"), TextBox)
                        Dim followupaction = CType(DtgDoc.Items(i).FindControl("txtFollowUp"), TextBox)

                        AssetDocNO = IIf(inAssetDocStatus.Text = "P" Or inAssetDocStatus.Text = "W" Or inAssetDocStatus.Text = "G", inDocNo.Text.Trim, inlblDocNo.Text.Trim)

                        If CBool(lblIsNoRequired.Text) And AssetDocNO = "" Then
                            ShowMessage(lblMessage, "Harap isi No Dokumen Asset", True)
                            Exit Sub
                        End If

                        dr = dtAdd.NewRow()
                        dr("isDocExist") = isRec.Text.Trim
                        dr("DocName") = inDocname.Text.Trim
                        dr("DocNo") = AssetDocNO.Trim
                        Dim dtext = IIf(inDocDate.Text.Trim = "", "1/1/1900", inDocDate.Text.Trim)
                        dr("DocDate") = ConvertDate2(dtext)
                        dr("Receiveddate") = inReceiveddate
                        Dim dpromise = IIf(pomisedate.Text.Trim = "", "1/1/1900", pomisedate.Text.Trim)
                        dr("PromiseDate") = ConvertDate2(dpromise)
                        dr("FollowUpAction") = followupaction.Text

                        Dim intDocStat = inAssetDocStatus.Text.Trim

                        If (inAssetDocStatus.Text = "H") Then
                            intDocStat = IIf(DocRecord.DocInFunding = False, "F", "O")
                        End If

                        dr("AssetDocStatus") = intDocStat
                        dr("StatusDate") = inStatusDate
                        dr("IsMainDoc") = CStr(inisMainDoc.Text.Trim)

                        If inisMainDoc.Text.Trim = "True" Then
                            k = k + 1
                        End If

                        dr("AssetDocID") = inlblassetdocid.Text.Trim

                        strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & inlblassetdocid.Text.Replace("'", "") & "'"

                        dtAdd.Rows.Add(dr)

                    Else
                        If CType(DtgDoc.Items(i).FindControl("txtPromisedate"), TextBox).Text.Trim <> "" Or
                            CType(DtgDoc.Items(i).FindControl("txtFollowUp"), TextBox).Text.Trim <> "" Then
                            adaJanji = True
                            Dim pomisedate = CType(DtgDoc.Items(i).FindControl("txtPromisedate"), TextBox)
                            Dim followupaction = CType(DtgDoc.Items(i).FindControl("txtFollowUp"), TextBox)
                            Dim dpromise = IIf(pomisedate.Text.Trim = "", "1/1/1900", pomisedate.Text.Trim)
                            Dim inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)

                            dr = dtJanji.NewRow()
                            dr("PromiseDate") = ConvertDate2(dpromise)
                            dr("FollowUpAction") = followupaction.Text
                            dr("AssetDocID") = inlblassetdocid.Text.Trim
                            dtJanji.Rows.Add(dr)
                        End If
                    End If
                Next


                If j = 0 Then
                    If adaJanji = False Then
                        ShowMessage(lblMessage, "harap pilih Kotaknya", True)
                        Exit Sub
                    End If
                End If

                'If Nothing selected then save only changes on tanggal janji dan keterangan janji
                If adaJanji = True Then
                    Dim pDoc As New Parameter.DocRec
                    pDoc.BusinessDate = Me.BusinessDate
                    pDoc.ApplicationId = DocRecord.ApplicationId
                    pDoc.BranchId = DocRecord.BranchId
                    pDoc.AssetSeqNo = DocRecord.AssetSeqNo
                    pDoc.strConnection = GetConnectionString()
                    DocRecord.listData = dtJanji
                    Dim oCustomClass = oController.SaveDocReceiveJanji(DocRecord)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    If j = 0 Then
                        Exit Sub
                    End If
                End If

                Try
                    DocRecord.listData = dtAdd
                    DocRecord.ReceiveDate = ConvertDate2(txtsdate.Text)
                    DocRecord.ReceiveDate = ConvertDate2(txtsdate.Text)
                    DocRecord.ReceiveFrom = txtReceiveFrom.Text.Trim
                    DocRecord.ChasisNo = txtChasisNo.Text.Trim
                    DocRecord.EngineNo = txtEngineNo.Text.Trim
                    DocRecord.LicensePlate = txtLicPlate.Text.Trim
                    DocRecord.taxdate = IIf(txtTdate.Text.Trim = "", ConvertDate2("1/1/1900"), ConvertDate2(txtTdate.Text))

                    DocRecord.RackLoc = IIf(lblRack.Visible = True, DocRecord.RackLocID, cboRack.SelectedValue.Trim)
                    DocRecord.FillingLoc = IIf(lblFLoc.Visible = True, DocRecord.FLocID, cboFill.SelectedValue.Trim)

                    DocRecord.OwnerAsset = txtName.Text
                    DocRecord.OwnerAddress = UCAddress.Address
                    DocRecord.OwnerRT = ""
                    DocRecord.OwnerRW = ""
                    DocRecord.OwnerKelurahan = ""
                    DocRecord.OwnerKecamatan = ""
                    DocRecord.OwnerCity = UCAddress.City
                    DocRecord.Notes = txtAssetNotes.Text
                    DocRecord.isMainDoc = IIf(k > 0, 1, 0)
                    DocRecord.strGroupId = strGroupID.Trim
                    DocRecord.BranchOnStatus = Me.sesBranchId.Replace("'", "")

                    Dim oCustomClass = oController.DocRecSave(DocRecord)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    Response.Redirect("DocReceive.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True
                    Exit Sub

                End Try
            End If
        End If
    End Sub

                    'With oCustomClass
                    '    .strConnection = GetConnectionString()
                    '    .listData = dtAdd
                    '    .BranchId = Me.BranchID
                    '    .ApplicationId = Me.ApplicationID

                    '    .ReceiveDate = ConvertDate2(txtsdate.Text)
                    '    .ReceiveFrom = txtReceiveFrom.Text.Trim
                    '    .ChasisNo = txtChasisNo.Text.Trim
                    '    .EngineNo = txtEngineNo.Text.Trim
                    '    .LicensePlate = txtLicPlate.Text.Trim

                    '    If txtTdate.Text.Trim = "" Then
                    '        .taxdate = ConvertDate2("1/1/1900")
                    '    Else
                    '        .taxdate = ConvertDate2(txtTdate.Text)
                    '    End If

                    '    .RackLoc = IIf(lblRack.Visible = True, Me.Rack, cboRack.SelectedValue.Trim)
                    '    .FillingLoc = IIf(lblFLoc.Visible = True, Me.FLoc, cboFill.SelectedValue.Trim)

                    '    .BusinessDate = Me.BusinessDate
                    '    .AssetSeqNo = Me.AssetSeqNo

                    '    .OwnerAsset = txtName.Text
                    '    .OwnerAddress = UCAddress.Address
                    '    .OwnerRT = ""
                    '    .OwnerRW = ""
                    '    .OwnerKelurahan = ""
                    '    .OwnerKecamatan = ""
                    '    .OwnerCity = UCAddress.City
                    '    .Notes = txtAssetNotes.Text

                    '    If k > 0 Then
                    '        .isMainDoc = 1
                    '    Else
                    '        .isMainDoc = 0
                    '    End If
                    '    .strGroupId = strGroupID.Trim 

                    'End With

                    '                oCustomClass = oController.DocRecSave(DocRecord)
                    '                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    '                Response.Redirect("DocReceive.aspx")
                    '            Catch ex As Exception
                    '                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                    '                lblMessage.Visible = True
                    '                Exit Sub

                    '            End Try
                    '        End If 
                    '    End If
                    'End Sub 
#End Region

End Class