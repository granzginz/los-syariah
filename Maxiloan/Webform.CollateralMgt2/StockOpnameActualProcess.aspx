﻿<%@ Page EnableEventValidation="false" Language="vb" AutoEventWireup="false" CodeBehind="StockOpnameActualProcess.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.StockOpnameActualProcess" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>StockOpnameActualProcess</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        document.getElementById("txtApplicationId").addEventListener("keyup", function (event) {
        event.preventDefault();
        if (event.keyCode == 13) {
            document.getElementById("btnAddStockOpname").click();
        }
    });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <asp:Panel ID="pnlStockOpname" runat="server">
            <div class="form_title">
            <div class="form_single">
                <h4>
                    STOCK OPNAME ACTUAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgStockOpname" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                        Width="100%" ShowFooter="True" DataKeyField="ApplicationID" AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="BranchID" HeaderText="BRANCH">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchOnStatus" runat="server" Text='<%#Container.DataItem("BranchOnStatus")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchID")%>'
                                        Visible="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="APLIKASI">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'
                                        Visible="true">
                                    </asp:Label>
                                    <asp:Label ID="lblAssetTypeId" runat="server" Text='<%#Container.DataItem("AssetTypeId")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="AssetDoc" HeaderText="Asset Doc.">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDoc" runat="server" Text='<%#Container.DataItem("AssetDocID")%>'
                                        Visible="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="DocumentNo" HeaderText="No. Dokumen">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDocumentNo" runat="server" Text='<%#Container.DataItem("DocumentNo")%>'
                                        Visible="true">
                                    </asp:Label>
                                    <asp:Label ID="lblDocumentDate" runat="server" Text='<%#Container.DataItem("DocumentDate")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="LicensePlate" HeaderText="NO. POLISI">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'
                                        Visible="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="AssetDocRack" HeaderText="RAK">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("AssetDocStatus")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblAssetDocRack" runat="server" Text='<%#Container.DataItem("AssetDocRack")%>'
                                        Visible="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="AssetDocFilling" HeaderText="FILLING">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDocFilling" runat="server" Text='<%#Container.DataItem("AssetDocFilling")%>'
                                        Visible="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="OwnerAsset" HeaderText="PEMILIK">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOwnerAsset" runat="server" Text='<%#Container.DataItem("OwnerAsset")%>'
                                        Visible="true">
                                    </asp:Label>
                                    <asp:Label ID="lblOwnerAddress" runat="server" Text='<%#Container.DataItem("OwnerAddress")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblOwnerCity" runat="server" Text='<%#Container.DataItem("OwnerCity")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblOwnerZipCode" runat="server" Text='<%#Container.DataItem("OwnerZipCode")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    No Aplikasi
                </label>
                <asp:TextBox ID="txtApplicationId" runat="server" MaxLength="20" ></asp:TextBox>
            </div>
            <div class="form_button">
                 <asp:Button ID="btnAddStockOpname" runat="server" CausesValidation="False" Text="Add"
                    CssClass="small button green"></asp:Button>
                 <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                    </asp:Button>
                 <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </div>
    </asp:Panel>    
    </form>
</body>
</html>
