﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqAplikasiChanneling.aspx.vb" Inherits="Maxiloan.Webform.CollateralMgt2.InqAplikasiChanneling" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocRelease</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
               Inquery Aplikasi Channeling
            </h3>
        </div>
    </div>
        <asp:Panel ID="pnlList" runat="server">
     <%--   <div class="form_box_title">
            <div class="form_single">
                <h4>
                    INQUERY APLIKASI CHANNELING
                </h4>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="cbobranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap pilih Cabang"
                    ControlToValidate="cbobranch" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
     <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
         <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
         <div class="form_box_header">
            <div class="form_single">
                <div class="DtgDocPledgeConf">
                    <asp:DataGrid ID="InqAplikasiChanneling" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="TransactionNo"  BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>                       
                            <asp:TemplateColumn SortExpression="TransactionNo" HeaderText="NO REQUEST">                                
                                <ItemTemplate>
                                    <asp:label ID="lblTransactionNo" runat="server" Text='<%#Container.DataItem("TransactionNo")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <Columns>
                            <asp:TemplateColumn SortExpression="FileName" HeaderText="Nama File">                                
                                <ItemTemplate>
                                    <asp:label ID="lblFileName" runat="server" Text='<%#Container.DataItem("FileName")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <Columns>
                         <asp:TemplateColumn SortExpression="UploadDate" HeaderText="Upload Date">                                
                                <ItemTemplate>
                                    <asp:label ID="lblUploadDate" runat="server" Text='<%#Container.DataItem("UploadDate")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>    
                        </Columns>
                        <Columns>
                            <asp:TemplateColumn SortExpression="TotalAgreement" HeaderText="Total Agreement">                                
                                <ItemTemplate>
                                    <asp:label ID="lblTotalAgreement" runat="server" Text='<%#Container.DataItem("TotalAgreement")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <Columns>
                            <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="Total Amount">                                
                                <ItemTemplate>
                                    <asp:label ID="lblTotalAmount" runat="server" Text='<%#Container.DataItem("TotalAmount")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <Columns>
                            <asp:TemplateColumn SortExpression="Status" HeaderText="Status">                                
                                <ItemTemplate>
                                    <asp:label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                <uc2:ucGridNav id="GridNavigator" runat="server"/>
                </div>
            </div>
        </div>       
    </asp:Panel>
    </form>
</body>
</html>
