﻿

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class DocReceiveTbo
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If

            If Request.QueryString("message") <> "" Then
                'ShowMessage(lblMessage, Request.QueryString("message"), True)
                ShowMessage(lblMessage, Request.QueryString("message"), False)
            End If
            Me.FormID = "DOCTBORECEIVE"
            oSearchBy.ListData = "Name, Customer Name-ApplicationID,Application ID-Agreementno, Agreement No-description, Asset Description-Licenseplate, License Plate-AssetStatus, Status-SerialNo2, Engine No. - serialno1 , No. Chasis - OldOwnerAsset , Nama BPKB"
            oSearchBy.BindData()

            Dim dt_branch As New DataTable
            'dt_branch = m_controller.GetBranchAll(GetConnectionString)
            'With cbobranch
            '    .DataTextField = "Name"
            '    .DataValueField = "ID"
            '    .DataSource = dt_branch
            '    .DataBind()
            '    .Items.Insert(0, "Select One")
            '    .Items(0).Value = "0"
            'End With
            With cbobranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "ALL"
                    .Enabled = True
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Enabled = False
                End If
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
            'cbobranch.Enabled = False 
            If (Not Me.IsHoBranch) Then
                cbobranch.Enabled = False
            End If

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'Dim intloop As Integer
        'Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetDocPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub
    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim inlblAssetSeqNo As New Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink

        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then
            'Dim branch = IIf(cboDocStatus.SelectedItem.Value.Trim = "I", CType(e.Item.FindControl("hdnBranch"), Label).Text, cbobranch.SelectedItem.Value.Trim)

            Dim branch = cbobranch.SelectedItem.Value.Trim
            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            inlblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)

            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            hypReceive.NavigateUrl = "DocReceiveTboList.aspx?Applicationid=" & HyappId.Text.Trim & "&branchid=" & Me.sesBranchId.Replace("'", "").Trim & "&branchid2=" & branch & "&AssetSeqNo=" & inlblAssetSeqNo.Text.Trim & "&DocStatus=" '& cboDocStatus.SelectedValue.ToString.Trim
            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DocReceiveTbo.aspx")
    End Sub
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        ' If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

        '  Me.SearchBy = " aga.assetstatus <> 'RLS'"

        'Me.SearchBy = String.Format(" aga.assetstatus <> 'RLS'  and  applicationid in (  Select ApplicationId  from assetdocumentcontent aga where branchid='{0}' group by  applicationid   having  sum(case when assetDocstatus='W' then 1 else 0 end) >0 and  sum(case when assetDocstatus<> 'W' and isMaindoc=1 then 1 else 0 end) >0  )  and aga.branchid = '{0}'", cbobranch.SelectedItem.Value.Trim)
        'Me.SearchBy = String.Format(" aga.assetstatus <> 'RLS' and agr.ContractStatus NOT IN ('CAN','RJC') And agr.DefaultStatus NOT IN ('WO') and  applicationid in (  Select ApplicationId  from assetdocumentcontent aga where branchid='{0}' group by  applicationid   having  sum(case when assetDocstatus='W' then 1 else 0 end) >0 and  sum(case when assetDocstatus<> 'W' and isMaindoc=1 then 1 else 0 end) >0  )  and aga.branchid = '{0}'", cbobranch.SelectedItem.Value.Trim)
        Me.SearchBy = String.Format(" aga.assetstatus <> 'RLS' and agr.ContractStatus NOT IN ('CAN','RJC') and applicationid in (  Select ApplicationId  from assetdocumentcontent aga where branchid='{0}' group by  applicationid   having  sum(case when assetDocstatus='W' then 1 else 0 end) >0 and  sum(case when assetDocstatus<> 'W' and isMaindoc=1 then 1 else 0 end) >0  )  and aga.branchid = '{0}'", cbobranch.SelectedItem.Value.Trim)

        'If cboDocStatus.SelectedItem.Value.Trim <> "" Then
        '    'If cboDocStatus.SelectedItem.Value.Trim = "W" Then
        '    '    Me.SearchBy = Me.SearchBy & " And Agr.ContractStatus = 'PRP'"                    
        '    'Else
        '    '    Me.SearchBy = Me.SearchBy & " And Agr.ContractStatus = 'LIV'"                    
        '    'End If
        '    Dim extSearch = ""

        '    If (cboDocStatus.SelectedItem.Value.Trim = "I") Then
        '        extSearch = String.Format(" and aga.BranchInTransit = '{0}'", Me.sesBranchId.Replace("'", "").Trim)
        '    ElseIf cboDocStatus.SelectedItem.Value.Trim = "W" Then
        '        extSearch = String.Format(" and aga.branchid = {0}", cbobranch.SelectedItem.Value.Trim)
        '    End If


        '    Me.SearchBy = Me.SearchBy & String.Format(" and aga.AssetDocStatus='{0}' {1}", cboDocStatus.SelectedItem.Value.Trim, extSearch)
        '    'Me.SearchBy & " And aga.assetstatus <> 'RLS' And aga.AssetDocStatus='" & cboDocStatus.SelectedItem.Value.Trim & "'"


        'End If

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
        End If
        pnlDatagrid.Visible = True
        DtgAsset.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
        ' End If
    End Sub


    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class