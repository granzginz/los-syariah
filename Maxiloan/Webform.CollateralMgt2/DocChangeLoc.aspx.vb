﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocChangeLoc
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property

    'Private Property FormType() As String
    '    Get
    '        Return (CType(ViewState("FormType"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("FormType") = Value
    '    End Set
    'End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
#End Region

#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                'ShowMessage(lblMessage, Request.QueryString("message"), Request.QueryString("success"))
                ShowMessage(lblMessage, Request.QueryString("message"), False)
            End If
              
             
            Me.FormID = "DOCCHANGELOC"
            oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
            oSearchBy.BindData()

            Dim dtbranch As New DataTable
            'dtbranch = m_controller.GetBranchName2(GetConnectionString, Me.sesBranchId)
            'dtbranch = m_controller.GetBranchAll(GetConnectionString)
            'With cbobranch
            '    .DataTextField = "Name"
            '    .DataValueField = "ID"
            '    .DataSource = dtbranch
            '    .DataBind()
            '    .Items.Insert(0, "Select One")
            '    .Items(0).Value = "0"
            'End With
            With cbobranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "ALL"
                    .Enabled = True
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Enabled = False
                End If
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True
            'cbobranch.Enabled = False
            If (Not Me.IsHoBranch) Then
                cbobranch.Enabled = False
            End If

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#End Region

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetDocPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub

#End Region

#Region "Navigation"

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then
    '        ShowMessage(lblMessage, "Data tidak ditemukan!", True)
    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = CType(totalPages, String)
    '        rgvGo.MaximumValue = CType(totalPages, String)
    '    End If
    '    lblrecord.Text = CType(recordCount, String)

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If
    'End Sub

    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    If Me.SortBy Is Nothing Then
    '        Me.SortBy = ""
    '    End If
    '    DoBind(Me.SearchBy, Me.SortBy)
    'End Sub

    'Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
    '    If IsNumeric(txtPage.Text) Then
    '        If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '            currentPage = CType(txtPage.Text, Int16)
    '            If Me.SortBy Is Nothing Then
    '                Me.SortBy = ""
    '            End If
    '            DoBind(Me.SearchBy, Me.SortBy)
    '        End If
    '    End If
    'End Sub
#End Region

#Region "DataBound"

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink
        Dim inlblAssetSeqNo As New Label
        Dim m As Int32
        Dim hypCLoc As HyperLink
        Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then
            Dim branch = CType(e.Item.FindControl("hdnBranch"), Label).Text
            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then
            inlblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)

            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hypCLoc = CType(e.Item.FindControl("HypChange"), HyperLink)
            hypCLoc.NavigateUrl = "DocChangeLocList.aspx?Applicationid=" & HyappId.Text.Trim & "&branchid=" & Me.sesBranchId.Replace("'", "") & "&branchid2=" & branch & "&AssetSeqNo=" & inlblAssetSeqNo.Text.Trim
            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DocChangeLoc.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then


            'Me.SearchBy = String.Format(" aga.BranchOnStatus = '{0}' and isnull(aga.branchintransit,'')='{1}'  and aga.DocInFunding='0' and aga.assetdocstatus = '{2}' And Agr.ContractStatus in ('AKT','SSD')", cbobranch.SelectedItem.Value.Trim, IIf(Me.IsHoBranch = True, "000", ""), "O")
            'Me.SearchBy = String.Format(" aga.BranchOnStatus = '{0}' and isnull(aga.branchintransit,'')=''  and aga.DocInFunding='0' and aga.assetdocstatus = '{2}' And Agr.ContractStatus in ('AKT','SSD')", cbobranch.SelectedItem.Value.Trim, IIf(Me.IsHoBranch = True, "000", ""), "O")
            'Me.SearchBy = String.Format(" aga.BranchOnStatus = '{0}' and aga.DocInFunding='0' and aga.assetdocstatus IN ('I','O') And Agr.ContractStatus in ('AKT','SSD')", cbobranch.SelectedItem.Value.Trim)
            'Me.SearchBy = String.Format(" aga.BranchOnStatus = '{0}' and aga.DocInFunding='0' and aga.assetdocstatus IN ('I','O') And ((Agr.ContractStatus NOT IN ('CAN','RJC') And Agr.DefaultStatus NOT IN ('WO')) or (Agr.ContractStatus='SSD' And Agr.DefaultStatus='WO')) ", cbobranch.SelectedItem.Value.Trim)
            Me.SearchBy = String.Format(" aga.BranchOnStatus = '{0}' and aga.DocInFunding='0' and AssetDocumentContent.assetdocstatus IN ('I','O') And Agr.ContractStatus NOT IN ('CAN','RJC')  ", cbobranch.SelectedItem.Value.Trim)

            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
            End If
            pnlDatagrid.Visible = True
            DtgAsset.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region



End Class