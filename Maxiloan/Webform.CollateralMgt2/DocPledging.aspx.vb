﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocPledging
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Constanta"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oFundingContractBatch As New Parameter.FundingContractBatch
    Dim m_Company As New FundingCompanyController
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property TotalHalaman() As Double
        Get
            Return CType(viewstate("TotalHalaman"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalHalaman") = Value
        End Set
    End Property
    Private Property TotalApplicationID() As String
        Get
            Return CType(viewstate("TotalApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("TotalApplicationID") = Value
        End Set
    End Property
    Private Property TotalAgreementSelected() As Integer
        Get
            Return CType(viewstate("TotalAgreementSelected"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalAgreementSelected") = Value
        End Set
    End Property
    Private Property arrApplicationID() As String()
        Get
            Return CType(viewstate("arrApplicationID"), String())
        End Get
        Set(ByVal Value As String())
            viewstate("arrApplicationID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If Not Me.IsPostBack Then
            drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            Me.FormID = "DOCPLEDGE"
            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            ' oSearchBy.ListData = "Cust.Name, Customer Name-Agr.ApplicationID,Application ID-Agr.Agreementno, Agreement No-am.description, Asset Description-Aga.Licenseplate, License Plate-Aga.AssetStatus, Status"
            oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
            oSearchBy.BindData()
            bindComboCompany()
            Response.Write(GenerateScript(getComboContract, getComboBatchDate))
            pnlList.Visible = True
            PnlSearchDetail.Visible = False
            pnlDocumentList.Visible = False
            pnlSaveConfirm.Visible = False
        End If
    End Sub
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
        Next
        If (j > 0) Then strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf

        Next
        If (j > 0) Then strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region
#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContract"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Function
#End Region
#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Function
#End Region
#Region "GetComboCompany"
    Private Function getComboCompany() As DataTable        
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData           

            Return dtsEntity
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try
    End Function
#End Region
#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Sub
#End Region
    Protected Function drdCompanyChange() As String
        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"
    End Function
    Protected Function drdContractChange() As String
        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"
    End Function
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        Me.TotalHalaman = totalPages
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        IsiViewstate()
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
        Isicheck()
    End Sub
    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                IsiViewstate()
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.SearchBy, Me.SortBy)
                Isicheck()
            End If
        End If
    End Sub
#End Region
#Region "IsiViewstate"
    Sub IsiViewstate()
        Dim i, j As Integer
        Dim lBytIndex, counter As Byte
        Dim chkSelect As New CheckBox
        Dim totalrecord, totalhalaman As Integer
        Dim HyApplicationID As New HyperLink
        Dim curr_page As Integer = CInt(lblPage.Text) - 1
        Dim strApplicationID As String
        totalrecord = CInt(IIf(lblrecord.Text = "", 0, lblrecord.Text))
        totalhalaman = CInt(lblTotPage.Text)
        strApplicationID = ""
        '' 
        If curr_page <= totalrecord / pageSize - 1 Then
            For i = 0 To DtgAsset.PageSize - 1
                chkSelect = CType(DtgAsset.Items(i).FindControl("chkSelect"), CheckBox)
                HyApplicationID = CType(DtgAsset.Items(i).FindControl("HyApplicationID"), HyperLink)
                If chkSelect.Checked = True Then
                    strApplicationID &= CStr(IIf(strApplicationID = "", "", ",")) & HyApplicationID.Text
                    ViewState("hal" & CStr(curr_page).Trim) = strApplicationID
                End If
            Next i
        Else
            For i = 0 To (totalrecord Mod pageSize) - 1
                chkSelect = CType(DtgAsset.Items(i).FindControl("chkSelect"), CheckBox)
                HyApplicationID = CType(DtgAsset.Items(i).FindControl("HyApplicationID"), HyperLink)
                If chkSelect.Checked = True Then
                    strApplicationID &= CStr(IIf(strApplicationID = "", "", ",")) & HyApplicationID.Text
                    ViewState("hal" & CStr(curr_page).Trim) = strApplicationID
                End If
            Next
        End If
    End Sub
#End Region
#Region "Isicheck"
    Sub Isicheck()
        Dim i, j, n As Integer
        Dim lBytIndex, counter As Byte
        Dim chkSelect As New CheckBox
        Dim totalrecord, totalhalaman As Integer
        Dim HyApplicationID As New HyperLink
        Dim strAgreement As String
        Dim curr_page As Integer = CInt(lblPage.Text) - 1
        totalrecord = CInt(lblrecord.Text)
        totalhalaman = CInt(lblTotPage.Text)

        If curr_page <= totalrecord / pageSize - 1 Then
            Dim splithalaman() As String
            splithalaman = Split(CStr(ViewState("hal" & CStr(curr_page).Trim)), ",")
            For i = 0 To pageSize - 1
                chkSelect = CType(DtgAsset.Items(i).FindControl("chkSelect"), CheckBox)
                HyApplicationID = CType(DtgAsset.Items(i).FindControl("HyApplicationID"), HyperLink)
                For n = 0 To UBound(splithalaman)
                    If HyApplicationID.Text.Trim = Trim(splithalaman(n)) Then
                        chkSelect.Checked = True
                    End If
                Next
            Next i
        Else
            Dim splithalaman() As String
            splithalaman = Split(CStr(ViewState("hal" & CStr(curr_page).Trim)), ",")
            For i = 0 To (totalrecord Mod pageSize) - 1
                chkSelect = CType(DtgAsset.Items(i).FindControl("chkSelect"), CheckBox)
                HyApplicationID = CType(DtgAsset.Items(i).FindControl("HyApplicationID"), HyperLink)
                For n = 0 To UBound(splithalaman)
                    If HyApplicationID.Text.Trim = RTrim(splithalaman(n)) Then
                        chkSelect.Checked = True
                    End If
                Next
            Next
        End If
    End Sub
#End Region
#Region "imbSelect"
    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim oCustomClass As New Parameter.LookUpSupplier
        IsiViewstate()
        Dim i As Double
        Dim j As Int32
        Dim counter As Int32
        Dim strwhere As String
        Dim strApplicationID As String
        counter = 0
        Me.TotalApplicationID = ""

        For i = 0 To Me.TotalHalaman
            Dim splitApplicationID() As String
            If Not IsNothing(ViewState("hal" & CStr(i))) Then
                If CStr(ViewState("hal" & CStr(i))) <> "" Then
                    splitApplicationID = Split(CStr(ViewState("hal" & CStr(i))), ",")
                    For j = 0 To UBound(splitApplicationID)
                        If CStr(splitApplicationID(j)) <> "" Then
                            counter = counter + 1
                            Me.TotalApplicationID = Me.TotalApplicationID & CStr(IIf(Me.TotalApplicationID = "", "", ",")) & "'" & LTrim(splitApplicationID(j)) & "'"
                        End If
                    Next
                End If
            End If
        Next

        hdnSupplierID.Value = Me.TotalApplicationID
        BindGridEntity_Confirm(Me.TotalApplicationID, Me.SortBy)

        'Dim script As String
        'script = "<script language=""JavaScript"">" & vbCrLf
        'script &= "alert('Masuk');" & vbCrLf
        'script &= "Select_Click();" & vbCrLf
        'script &= "</script>"
        'Response.Write(script)
    End Sub
#End Region
#Region "BindGrid"

    Sub BindGridEntity(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim ChkSelect As CheckBox
        Dim lblAssetDocRack As Label
        Dim lblRackID As Label
        Dim lblBranchID As Label
        Dim lblDocStatus As Label
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetDocPledgeListMulti"
        End With
        oCustomClass = oController.GetDocPledgePaging(oCustomClass)

        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.listData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        PagingFooter()
        For intloop = 0 To DtgAsset.Items.Count - 1
            ChkSelect = CType(DtgAsset.Items(intloop).Cells(0).FindControl("ChkSelect"), CheckBox)
            lblAssetDocRack = CType(DtgAsset.Items(intloop).Cells(0).FindControl("lblAssetDocRack"), Label)
            lblRackID = CType(DtgAsset.Items(intloop).Cells(0).FindControl("lblRackID"), Label)
            lblBranchID = CType(DtgAsset.Items(intloop).Cells(0).FindControl("lblBranchID"), Label)
            lblDocStatus = CType(DtgAsset.Items(intloop).Cells(0).FindControl("lblDocStatus"), Label)
            If lblDocStatus.Text.Trim = "On Hand" Then
                If lblBranchID.Text.Trim = Me.sesBranchId.Replace("'", "") Then
                    If lblAssetDocRack.Text.Trim = lblRackID.Text.Trim Then
                        ChkSelect.Enabled = True
                        ChkSelect.Checked = True
                    Else
                        ChkSelect.Enabled = False
                    End If
                Else
                    ChkSelect.Enabled = False
                End If
            Else
                ChkSelect.Enabled = False
            End If
            If lblDocStatus.Text.Trim = "Waiting" Then
                ChkSelect.Enabled = True
                ChkSelect.Checked = True
            End If
        Next
        pnlList.Visible = False
        PnlSearchDetail.Visible = True
        pnlDocumentList.Visible = True
        pnlSaveConfirm.Visible = False

    End Sub
    Sub BindGridEntity_Confirm(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim lblApplicationID As HyperLink
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim DocNotPledge As Integer
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        If Me.TotalApplicationID = "" Then
            ShowMessage(lblMessage, "Harap Periksa Pilihan ", True)
            lblMessage.Visible = True
            Exit Sub
        End If
        cmdWhere = " Agr.ApplicationID in (" + Me.TotalApplicationID + ") "

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetDocPledgeListMultiConfirm"
        End With

        oCustomClass = oController.GetDocPledgePaging(oCustomClass)
        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.listData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        DocNotPledge = CInt(lblrecord.Text.Trim) - CInt(recordCount)
        DtgDocPledgeConf.DataSource = DtUserList.DefaultView
        'DtgDocPledgeConf.CurrentPageIndex = 0
        DtgDocPledgeConf.DataBind()
        'PagingFooter()
        '' --------- INPUT ke Array ApplicationID ---------------- ''
        Me.TotalAgreementSelected = DtgDocPledgeConf.Items.Count - 1
        ReDim Me.arrApplicationID(Me.TotalAgreementSelected)
        For intloop = 0 To DtgDocPledgeConf.Items.Count - 1
            lblApplicationID = CType(DtgDocPledgeConf.Items(intloop).Cells(0).FindControl("hyApplicationIdConf"), HyperLink)
            Me.arrApplicationID(intloop) = lblApplicationID.Text.Trim
        Next
        ''------------------------------------------------------- ''
        LblDocPledge.Text = CStr(recordCount)
        LblDocumentWillNotBePledge.Text = CStr(DocNotPledge)
        pnlList.Visible = False
        PnlSearchDetail.Visible = False
        pnlDocumentList.Visible = False
        pnlSaveConfirm.Visible = True

    End Sub
#End Region
#Region "Databound"

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink
        Dim m As Int32
        Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then

            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then
            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)

            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub
    Private Sub DtgDocPledgeConf_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDocPledgeConf.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If

        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink
        Dim m As Int32
        Dim lblApplicationid As Label
        If e.Item.ItemIndex >= 0 Then

            'If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then
            HyappId = CType(e.Item.FindControl("hyApplicationIdConf"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerIdConf"), Label)

            'End If

            lblTemp = CType(e.Item.FindControl("lblCustomerIdConf"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationIdConf"), HyperLink)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerNameConf"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNoConf"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationIdConf"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub

#End Region


    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim IntLoop As Integer
            'For IntLoop = 0 To Me.totalAgreementSelected
            With oCustomClass
                .strConnection = GetConnectionString()
                .ArrApplicationId() = Me.arrApplicationID()
                .BusinessDate = Me.BusinessDate
                .totalAgreementPledge = Me.TotalAgreementSelected

            End With
            Try
                oCustomClass = oController.DocPledgeProcess(oCustomClass)
                If oCustomClass.strError <> "" Then
                    ShowMessage(lblMessage, "Update Dokumen Dijaminkan Gagal ", True)
                    Exit Sub
                Else
                    Response.Redirect("DocPledging.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                lblMessage.Visible = True
                Exit Sub
            End Try
            'Next
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        lblFundingCoy.Text = "Not Specified"
        LblContractNo.Text = "Not Specified"
        LblBatchDate.Text = "Not Specified"
        LblSearchBy.Text = "Not Specified"
        If drdCompany.SelectedItem.Value.Trim <> "All" Then
            Me.SearchBy = "FundingBatch.FundingCoyID='" & drdCompany.SelectedItem.Value.Trim & "'"
            lblFundingCoy.Text = " " & drdCompany.SelectedItem.Value.Trim & " "
        End If
        If tempChild.Value.Trim = "" Or tempChild.Value.Trim = "0" Then
        Else
            Me.SearchBy = Me.SearchBy + " and FundingBatch.FundingContractNO= '" & tempChild.Value.Trim & "'"
            LblContractNo.Text = "" & tempChild.Value.Trim & ""
            If tempChild2.Value.Trim = "" Or tempChild2.Value.Trim = "0" Then
            Else
                Me.SearchBy = Me.SearchBy + " and FundingBatch.FundingBatchNo = '" & tempChild2.Value.Trim & "'"
                LblBatchDate.Text = "" & tempChild2.Value.Trim & ""
            End If
        End If
        If oSearchBy.Text.Trim <> "" Then
            If Me.SearchBy <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
            End If
            LblSearchBy.Text = " " & oSearchBy.Text.Trim & " "
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("DocPledging.aspx")
    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("DocPledging.aspx")
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DocPledging.aspx")
    End Sub

    Private Sub RegisterFirstItem(ByVal ddl As DropDownList)
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "0")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "All")
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtContract As DataTable = getComboContract()
        Dim i As Integer

        For i = 0 To DtContract.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdContract.UniqueID, CStr(DtContract.Rows(i).Item("FundingContractNo")).Trim)
        Next

        RegisterFirstItem(drdContract)


        Dim DtBatchDate As DataTable = getComboBatchDate()
        Dim j As Integer

        For j = 0 To DtBatchDate.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdBatchDate.UniqueID, CStr(DtBatchDate.Rows(j).Item("FundingBatchNo")).Trim)
        Next
        RegisterFirstItem(drdBatchDate)


        Dim DtComboCompany As DataTable = getComboCompany()
        Dim k As Integer

        For k = 0 To DtComboCompany.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdCompany.UniqueID, CStr(DtComboCompany.Rows(k).Item("FundingCoyId")).Trim)
        Next
        RegisterFirstItem(drdCompany)
        MyBase.Render(writer)
    End Sub
End Class