﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocGenerateBarcodeList.aspx.vb"
    Inherits="Maxiloan.Webform.CollateralMgt2.DocGenerateBarcodeList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                GENERATE BARCODE BPKB
            </h3>
        </div>
    </div>
    <div class="form_box">
            <asp:UpdatePanel ID="panelBarcode" runat="server">
                <ContentTemplate>
                <asp:Image ID="BarcodeImage" runat="server" />
                </ContentTemplate>
             </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
