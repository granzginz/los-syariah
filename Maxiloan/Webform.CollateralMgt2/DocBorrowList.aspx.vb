﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocBorrowList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(Viewstate("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(Viewstate("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("TotOH") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(Viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property BranchID2() As String
        Get
            Return (CType(Viewstate("BranchID2"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID2") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController

#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.BranchID2 = Request.QueryString("branchid2")
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))
            Me.FormID = "DOCBORROW"

            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
              
                txtsdate.text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtrdate.Text = DateAdd(DateInterval.Day, 1, Me.BusinessDate).ToString("dd/MM/yyyy")

                DoBind()
                DoBindGrid()

                If Me.TotOH <= 0 Then
                    ShowMessage(lblMessage, "Tidak ada dokumen on hand!", True)
                    btnSave.Visible = False
                End If
            End If
        End If

    End Sub
#End Region
#Region "DoBind"
    Sub DoBind()
        Dim inCustid As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID2
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            inCustid = .CustomerID
            hyCustomerName.Text = .Customername
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustid) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "AssetDocument" & "', '" & Server.UrlEncode(inSupId) & "')"
            lblAssetDesc.Text = .assetDesc
            lblEngineNo.Text = .EngineNo
            Me.Chasis = .ChasisNo
            lblLicPlate.Text = .LicensePlate
            If .Itaxdate = "1" Then
                lblTaxDate.Text = .taxdate.ToString("dd/MM/yyyy")
            Else
                lblTaxDate.Text = "-"
            End If

            lblRack.Text = .RackLoc
            lblFLoc.Text = .FillingLoc
            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            txtChasisNo.Text = .ChasisNo.Trim
            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
        End With
    End Sub
#End Region
#Region "DoBindGrid"
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "' and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()

    End Sub
#End Region
#Region "DataBound"
    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound
        Dim intxtDocNo As New TextBox
        Dim inlblDocNo As New Label
        Dim inlblAssetDocStatus As Label
        Dim inChkSlct As CheckBox


        If e.Item.ItemIndex >= 0 Then
            inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            inChkSlct = CType(e.Item.FindControl("ChkSlct"), CheckBox)
            If inlblAssetDocStatus.Text.Trim = "O" Then
                inChkSlct.Visible = True
                OH = OH + 1
            Else
                inChkSlct.Visible = False
            End If
        End If

        Me.TotOH = OH

        'If e.Item.ItemIndex >= 0 Then
        '    inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
        '    inChkSlct = CType(e.Item.FindControl("ChkSlct"), CheckBox)
        '    If inlblAssetDocStatus.Text.Trim = "O" Then
        '        inChkSlct.Visible = True
        '        OH = OH + 1
        '    Else
        '        inChkSlct.Visible = False
        '    End If
        'End If
        'Me.TotOH = OH
        'If Me.TotOH <= 0 Then
        '    ShowMessage(lblMessage, "Tidak ada Dokumen Onhand", True)
        '    btnSave.Visible = False
        'Else

        '    btnSave.Visible = True
        'End If

    End Sub
#End Region
#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("DocBorrow.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim oPosition As New Parameter.DocRec
            Dim oNewPosition As New Parameter.DocRec

            oPosition.strConnection = GetConnectionString()
            oPosition.BranchId = Me.BranchID2
            oPosition.sesBranchID = Me.BranchID
            oPosition.ApplicationId = Me.ApplicationID
            oNewPosition = oController.GetBPKBPosition(oPosition)
            Dim strBranchName As String
            Dim strBranchID As String
            strBranchID = oNewPosition.BranchId
            strBranchName = oNewPosition.BranchName
            If strBranchID <> Me.BranchID Then
                ShowMessage(lblMessage, "Anda tidak Berhak. Dokumen ada di Cabang " & strBranchName & "!", True)
                Exit Sub
            End If


            If Me.Chasis <> txtChasisNo.Text.Trim Then
                ShowMessage(lblMessage, "No Chassis Salah", True)
                Exit Sub
            End If

            If DateDiff(DateInterval.Day, ConvertDate2(txtsdate.Text), ConvertDate2(txtrdate.Text)) < 0 Then
                ShowMessage(lblMessage, "Tanggal kembali harus >= Tanggal Pinjam", True)
                Exit Sub
            End If
            If DtgDoc.Items.Count > 0 Then
                Dim i As Integer
                Dim j As Integer
                Dim k As Integer
                Dim dtAdd As New DataTable
                Dim dr As DataRow
                Dim isRec As New Label
                Dim inDocname As New Label
                Dim inDocNo As New TextBox
                Dim inReceiveddate As String
                Dim inAssetDocStatus As New Label
                Dim inStatusDate As String
                Dim inChkSlct As New CheckBox
                Dim inisMainDoc As New Label
                Dim inlblassetdocid As New Label
                Dim strGroupID As String

                strGroupID = ""
                dtAdd.Columns.Add("AssetDocStatus", System.Type.GetType("System.String"))
                dtAdd.Columns.Add("IsMainDoc", System.Type.GetType("System.String"))
                dtAdd.Columns.Add("AssetDocID", System.Type.GetType("System.String"))

                j = 0
                k = 0
                For i = 0 To DtgDoc.Items.Count - 1

                    inChkSlct = CType(DtgDoc.Items(i).FindControl("ChkSlct"), CheckBox)
                    If inChkSlct.Checked Then
                        j = j + 1
                        inAssetDocStatus = CType(DtgDoc.Items(i).FindControl("lblAssetDocStatus"), Label)
                        inisMainDoc = CType(DtgDoc.Items(i).FindControl("lblisMainDoc"), Label)
                        inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)
                        dr = dtAdd.NewRow()
                        dr("AssetDocStatus") = inAssetDocStatus.Text.Trim
                        dr("IsMainDoc") = CStr(inisMainDoc.Text.Trim)
                        If inisMainDoc.Text.Trim = "True" Then
                            k = k + 1
                        End If
                        dr("AssetDocID") = inlblassetdocid.Text.Trim
                        strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & inlblassetdocid.Text.Replace("'", "") & "'"
                        dtAdd.Rows.Add(dr)
                    End If
                Next

                If j = 0 Then
                    ShowMessage(lblMessage, "Harap dipilih kotaknya", True)
                    Exit Sub
                End If


                With oCustomClass
                    .strConnection = GetConnectionString()
                    .listData = dtAdd
                    .BranchId = Me.BranchID2
                    .ApplicationId = Me.ApplicationID
                    .BorrowDate = ConvertDate2(txtsdate.Text)
                    .BorrowBy = txtBorrowBy.Text.Trim
                    .ChasisNo = txtChasisNo.Text.Trim
                    .ReturnDate = ConvertDate2(txtrdate.Text)
                    .BusinessDate = Me.BusinessDate
                    .AssetSeqNo = Me.AssetSeqNo
                    .Notes = txtNotes.Text
                    If k > 0 Then
                        .isMainDoc = 1
                    Else
                        .isMainDoc = 0
                    End If
                    .strGroupId = strGroupID.Trim
                End With
                Try
                    oCustomClass = oController.DocBorrowSave(oCustomClass)
                    If oCustomClass.strError <> "" Then
                        ShowMessage(lblMessage, "Update Pinjam Dokumen Gagal", True)
                        Exit Sub
                    Else
                        Response.Redirect("DocBorrow.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                    lblMessage.Visible = True
                    Exit Sub

                End Try
            End If

        End If
    End Sub
#End Region

End Class