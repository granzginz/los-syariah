﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DocPledgingReceiveList
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim oFundingContractBatch As New Parameter.FundingContractBatch
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property FundingBatchID() As String
        Get
            Return CStr(viewstate("FundingBatchID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If Not Me.IsPostBack Then
            drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            Me.FormID = "DOCPLEDGERCV"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            bindComboCompany()
            Response.Write(GenerateScript(getComboContract, getComboBatchDate))
        End If
    End Sub
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNO")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf

        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region
#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContract"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Function
#End Region
#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Function
#End Region
#Region "GetComboCompany"
    Private Function getComboCompany() As DataTable
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData

            Return dtsEntity
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try
    End Function
#End Region
#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            lblMessage.Visible = True
        End Try

    End Sub
#End Region
    Protected Function drdCompanyChange() As String
        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"
    End Function
    Protected Function drdContractChange() As String
        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"
    End Function

    Private Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Save", "MAXILOAN") Then
            If tempChild.Value.Trim = "" Or tempChild.Value.Trim = "0" Then
                ShowMessage(lblMessage, "Harap pilih Funding Kontrak", True)
                lblMessage.Visible = True
                bindComboCompany()
                Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                Exit Sub
            Else
                If tempChild2.Value.Trim = "" Or tempChild2.Value.Trim = "0" Then
                    ShowMessage(lblMessage, "Harap Pilih Funding Batch", True)
                    lblMessage.Visible = True
                    bindComboCompany()
                    Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                    Exit Sub
                End If
            End If
            If txtSearchBy.Text.Trim <> "" Then
                'Me.SearchBy = Me.SearchBy + " Agreement.AgreementNo Like '%" & txtSearchBy.Text.Trim.Replace("%", "") & "%'  and "
                Me.SearchBy = Me.SearchBy + " Agreement.AgreementNo = " & txtSearchBy.Text.Trim & "  and "                
            End If
            Me.CompanyID = drdCompany.SelectedItem.Value.Trim
            Me.FundingContractNo = tempChild.Value.Trim
            Me.FundingBatchID = tempChild2.Value.Trim
            Response.Redirect("DocPledgingReceive.aspx?FundingCoyID=" & Me.CompanyID & "&FundingContractNO=" & Me.FundingContractNo & "&FundingBatchNO=" & Me.FundingBatchID & "&SearchBy=" & Me.SearchBy & "")
        End If
    End Sub

    Private Sub RegisterFirstItem(ByVal ddl As DropDownList)
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "0")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "All")
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = getComboContract()
        Dim DtChild2 As DataTable = getComboBatchDate()
        Dim DtComboCompany As DataTable = getComboCompany()

        Dim i As Integer        

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdContract.UniqueID, CStr(DtChild.Rows(i).Item("FundingContractNo")).Trim)            
        Next
        RegisterFirstItem(drdContract)

        For i = 0 To DtChild2.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdBatchDate.UniqueID, CStr(DtChild2.Rows(i).Item("FundingBatchNO")).Trim)
        Next
        RegisterFirstItem(drdBatchDate)

        For i = 0 To DtComboCompany.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdCompany.UniqueID, CStr(DtComboCompany.Rows(i).Item("FundingCoyId")).Trim)
        Next
        RegisterFirstItem(drdCompany)

        MyBase.Render(writer)
    End Sub
End Class