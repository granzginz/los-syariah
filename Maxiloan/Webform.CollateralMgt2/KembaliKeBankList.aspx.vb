﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class KembaliKeBankList
    Inherits Maxiloan.Webform.WebBased
    Dim OH As Integer = 0


#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property Chasis() As String
        Get
            Return (CType(Viewstate("Chasis"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Chasis") = Value
        End Set
    End Property

    Private Property Rack() As String
        Get
            Return (CType(Viewstate("Rack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Rack") = Value
        End Set
    End Property

    Private Property FLoc() As String
        Get
            Return (CType(Viewstate("FLoc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLoc") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(Viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property DocStatus() As String
        Get
            Return (CType(Viewstate("DocStatus"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("DocStatus") = Value
        End Set
    End Property
    Private Property TotOH() As Integer
        Get
            Return (CType(Viewstate("TotOH"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("TotOH") = Value
        End Set
    End Property
    Private Property FlagRack() As String
        Get
            Return (CType(Viewstate("FlagRack"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FlagRack") = Value
        End Set
    End Property

    Private Property FLocDesc() As String
        Get
            Return (CType(Viewstate("FLocDesc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FLocDesc") = Value
        End Set
    End Property

    Private Property RackDesc() As String
        Get
            Return (CType(Viewstate("RackDesc"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RackDesc") = Value
        End Set
    End Property
    Private Property BranchID2() As String
        Get
            Return (CType(Viewstate("BranchID2"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID2") = Value
        End Set
    End Property
    Private Property RackFillLoc() As IList(Of RackFill)
        Get
            Return (CType(ViewState("RackFillLoc"), IList(Of RackFill)))
        End Get
        Set(ByVal Value As IList(Of RackFill))
            ViewState("RackFillLoc") = Value
        End Set
    End Property

    Private Property IsDocInFunding() As Boolean
        Get
            Return (CType(ViewState("IsDocInFunding"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsDocInFunding") = Value
        End Set
    End Property
#End Region


    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController 


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid").Trim
            Me.BranchID2 = Request.QueryString("branchid2").Trim
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))

            Me.FormID = "PJMDARIBANK"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then



                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


                Dim oReceiveList As New Parameter.DocRec
                oReceiveList.ApplicationId = Me.ApplicationID
                oReceiveList.BranchId = Me.BranchID2
                oReceiveList.strConnection = GetConnectionString
                oReceiveList = oController.GetTaxDate(oReceiveList)

                Dim strTaxDate As String
                If oReceiveList.taxdate = #1/1/1900# Then
                    strTaxDate = ""
                Else
                    strTaxDate = oReceiveList.taxdate.ToString("dd/MM/yyyy")
                End If

                txtTdate.Text = strTaxDate

                DoBind()
                DoBindGrid()


                'lblRack.Visible = False
                'lblFLoc.Visible = False
                'cboRack.Visible = True
                'cboFill.Visible = True
                Me.FlagRack = "2"


                'Me.RackFillLoc = RackFillHelper.GetRacks(GetConnectionString, Me.BranchID, True)

                'With cboRack
                '    .DataTextField = "Text"
                '    .DataValueField = "Value"
                '    .DataSource = Me.RackFillLoc
                '    .DataBind()
                '    .Items.Insert(0, "Select One")
                '    .Items(0).Value = "0"
                'End With
                'RefreshCboFill(New List(Of Parameter.ValueTextObject), cboFill)
                'End If

            End If
        End If

    End Sub

#Region "DoBind"
    Sub DoBind()
        Dim inCustID As String
        Dim inSupId As String
        oCustomClass.BranchId = Me.BranchID2
        oCustomClass.ApplicationId = Me.ApplicationID
        oCustomClass.AssetSeqNo = Me.AssetSeqNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.ListDoc(oCustomClass)
        With oCustomClass
            hyAgreementNo.Text = .AgreementNo
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            hyCustomerName.Text = .Customername
            inCustID = .CustomerID
            hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(inCustID) & "')"
            inSupId = .Supplierid
            lblSupplier.Text = .SupplierName
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & Server.UrlEncode(inSupId) & "', '" & "AssetDocument" & "')"
            lblAssetDesc.Text = .assetDesc
            txtEngineNo.Text = .EngineNo
            Me.Chasis = .ChasisNo
            txtLicPlate.Text = .LicensePlate
            Me.DocStatus = .AssetDocStatus
            Me.Rack = .RackLocID
            Me.FLoc = .FLocID
            Me.FLocDesc = .FillingLoc
            Me.RackDesc = .RackLoc
            IsDocInFunding = .DocInFunding
            serialno1label.Text = .seriallabel1
            serialno2label.Text = .seriallabel2
            txtChasisNo.Text = .ChasisNo.Trim

            'Select Case (.AssetDocStatus)
            '    Case "W"
            '        txtReceiveFrom.Text = .SupplierName.Trim
            '    Case "I"
            '        txtReceiveFrom.Text = getBranchName(.BranchId)
            '    Case "B"
            '    Case "H"
            '        txtReceiveFrom.Text = IIf(IsDocInFunding = False, "CustodianBPKB", "Funding") '.SupplierName.Trim 
            'End Select



            lblFundingCoName.Text = .FundingCoName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
            'lblBorrowerName.Text = .BorrowBy

            Dim strBorrowDate As String
            strBorrowDate = .BorrowDate.ToString("dd/MM/yyyy")

            'If strBorrowDate = "01/01/1900" Then
            '    lblBorrowDate.Text = "-"
            'Else
            '    lblBorrowDate.Text = strBorrowDate
            'End If

        End With
    End Sub
#End Region

    Function getBranchName(branchid As String) As String

        If (IsHoBranch = False) Then branchid = "900"
        Dim m_controller As New DataUserControlController
        Dim dt_branch As New DataTable
        dt_branch = m_controller.GetBranchAll(GetConnectionString)
        For Each row In dt_branch.Rows
            If row("ID").ToString.Trim = branchid Then
                Return row("Name")
            End If
        Next
        Return ""
    End Function

#Region "RackChange"

    'Protected Function RackChange() As String
    '    Return "ParentChange('" & Trim(cboRack.ClientID) & "','" & Trim(cboFill.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    'End Function

#End Region
#Region "DoBindGrid"
    Sub DoBindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = " adc.branchid = '" & Me.BranchID2 & "'  and applicationid = '" & Me.ApplicationID & "'  and adc.assetseqno ='" & Me.AssetSeqNo & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.DocListPaging(oCustomClass)
        DtUserList = oCustomClass.listDoc

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()



    End Sub
#End Region
#Region "DataBound"
    Private Sub DtgDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgDoc.ItemDataBound

        Dim inlblAssetDocStatus As Label
        Dim inChkSlct As CheckBox
        Dim inDocDate As TextBox
        Dim inlblDocDate As New Label
        Dim lblIsNoRequired As New Label
        Dim txtDocNo As New TextBox
        Dim RFVDocNo As New RequiredFieldValidator

        If e.Item.ItemIndex >= 0 Then

            inlblAssetDocStatus = CType(e.Item.FindControl("lblAssetDocStatus"), Label)
            inChkSlct = CType(e.Item.FindControl("ChkSlct"), CheckBox)
            inDocDate = CType(e.Item.FindControl("txtDocDate"), TextBox)
            inlblDocDate = CType(e.Item.FindControl("lblDocDate"), Label)
            lblIsNoRequired = CType(e.Item.FindControl("lblIsNoRequired"), Label)
            txtDocNo = CType(e.Item.FindControl("txtDocNo"), TextBox)
            RFVDocNo = CType(e.Item.FindControl("RFVDocNo"), RequiredFieldValidator)

            inDocDate.Visible = True
            inDocDate.Enabled = True

           
                inDocDate.Visible = False
                inlblDocDate.Visible = True

                If CBool(lblIsNoRequired.Text) Then
                    txtDocNo.Visible = True
                    RFVDocNo.Visible = True
                Else
                    txtDocNo.Visible = False
                    RFVDocNo.Visible = False
                End If


            If inlblAssetDocStatus.Text.Trim = "F" Then
                inChkSlct.Visible = True
                OH = OH + 1
            Else
                inChkSlct.Visible = False
            End If

            Me.TotOH = OH
            If Me.TotOH <= 0 Then
                ShowMessage(lblMessage, "Dokumen Sudah diterima", True)
                btnSave.Visible = False
            Else
                btnSave.Visible = True
            End If

        End If

    End Sub
#End Region
   

#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("KembaliKeBank.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
        If ConvertDate2(txtsdate.Text.Trim) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal Terima tidak boleh lebih besar dari hari ini", True)
            Exit Sub
        End If

        If Me.Chasis <> txtChasisNo.Text.Trim Then
            ShowMessage(lblMessage, "No Rangka Salah", True)
            Exit Sub
        End If

        If DtgDoc.Items.Count > 0 Then
            Dim i As Integer
            Dim j As Integer
            Dim k As Integer
            Dim dtAdd As New DataTable
            Dim dr As DataRow
            Dim isRec As New Label
            Dim inDocname As New Label
            Dim inDocNo As New TextBox
            Dim inlblDocNo As New Label
            Dim inReceiveddate As String
            Dim inAssetDocStatus As New Label
            Dim inStatusDate As String
            Dim inChkSlct As New CheckBox
            Dim inisMainDoc As New Label
            Dim inlblassetdocid As New Label
            Dim lblIsNoRequired As New Label
            Dim inDocDate As TextBox
            Dim strGroupID As String
            Dim AssetDocNO As String

            strGroupID = ""

            dtAdd.Columns.Add("isDocExist", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocName", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocNo", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("DocDate", System.Type.GetType("System.DateTime"))
            dtAdd.Columns.Add("Receiveddate", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("AssetDocStatus", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("StatusDate", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("IsMainDoc", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("AssetDocID", System.Type.GetType("System.String"))

            j = 0
            k = 0
            For i = 0 To DtgDoc.Items.Count - 1

                inChkSlct = CType(DtgDoc.Items(i).FindControl("ChkSlct"), CheckBox)

                If inChkSlct.Checked Then
                    j = j + 1
                    isRec = CType(DtgDoc.Items(i).FindControl("lblrec"), Label)
                    inDocname = CType(DtgDoc.Items(i).FindControl("lblDocname"), Label)
                    inDocNo = CType(DtgDoc.Items(i).FindControl("txtDocNo"), TextBox)
                    inlblDocNo = CType(DtgDoc.Items(i).FindControl("lblDocNo"), Label)
                    inDocDate = CType(DtgDoc.Items(i).FindControl("txtDocDate"), TextBox)
                    inReceiveddate = DtgDoc.Items(i).Cells(3).Text
                    inAssetDocStatus = CType(DtgDoc.Items(i).FindControl("lblAssetDocStatus"), Label)
                    inStatusDate = DtgDoc.Items(i).Cells(7).Text
                    inisMainDoc = CType(DtgDoc.Items(i).FindControl("lblisMainDoc"), Label)
                    inlblassetdocid = CType(DtgDoc.Items(i).FindControl("lblassetdocid"), Label)
                    lblIsNoRequired = CType(DtgDoc.Items(i).FindControl("lblIsNoRequired"), Label)

                    If inAssetDocStatus.Text = "P" Or inAssetDocStatus.Text = "W" Or inAssetDocStatus.Text = "G" Then
                        AssetDocNO = inDocNo.Text.Trim
                    Else
                        AssetDocNO = inlblDocNo.Text.Trim
                    End If


                    If CBool(lblIsNoRequired.Text) And AssetDocNO = "" Then
                        ShowMessage(lblMessage, "Harap isi No Dokumen Asset", True)
                        Exit Sub

                    Else
                        dr = dtAdd.NewRow()
                        dr("isDocExist") = isRec.Text.Trim
                        dr("DocName") = inDocname.Text.Trim
                        dr("DocNo") = AssetDocNO.Trim


                        If inDocDate.Text.Trim = "" Then
                            dr("DocDate") = ConvertDate2("1/1/1900")
                        Else
                            dr("DocDate") = ConvertDate2(inDocDate.Text.Trim)
                        End If


                        dr("Receiveddate") = inReceiveddate

                        Dim intDocStat = inAssetDocStatus.Text.Trim


                        dr("AssetDocStatus") = "G"

                        dr("StatusDate") = inStatusDate
                        dr("IsMainDoc") = CStr(inisMainDoc.Text.Trim)

                        If inisMainDoc.Text.Trim = "True" Then
                            k = k + 1
                        End If

                        dr("AssetDocID") = inlblassetdocid.Text.Trim
                        strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & inlblassetdocid.Text.Replace("'", "") & "'"
                        dtAdd.Rows.Add(dr)
                    End If
                    'End If
                End If
            Next

            If j = 0 Then
                ShowMessage(lblMessage, "harap pilih Kotaknya", True)
                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .listData = dtAdd
                .BranchId = Me.BranchID2
                .ApplicationId = Me.ApplicationID
                .ReceiveDate = ConvertDate2(txtsdate.Text)

                '.TanggalKembali = ConvertDate2(txtRdate.Text)

                .ReceiveFrom = txtReceiveFrom.Text.Trim
                .ChasisNo = txtChasisNo.Text.Trim
                .EngineNo = txtEngineNo.Text.Trim
                .LicensePlate = txtLicPlate.Text.Trim
                If txtTdate.Text.Trim = "" Then
                    .taxdate = #1/1/1900#
                Else
                    .taxdate = ConvertDate2(txtTdate.Text)
                End If

                .RackLoc = 0
                .FillingLoc = 0
                .BusinessDate = Me.BusinessDate
                .AssetSeqNo = Me.AssetSeqNo
                .OwnerAsset = ""
                .OwnerAddress = ""
                .OwnerRT = ""
                .OwnerRW = ""
                .OwnerKelurahan = ""
                .OwnerKecamatan = ""
                .OwnerCity = ""
                .Notes = txtAssetNotes.Text


                If k > 0 Then
                    .isMainDoc = 1
                Else
                    .isMainDoc = 0
                End If
                .strGroupId = strGroupID.Trim
                .DocPinjamDariBank = "FG"
            End With

            Try
                oCustomClass = oController.DocRecSave(oCustomClass)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)

            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                lblMessage.Visible = True
                Exit Sub
            Finally
                Response.Redirect("KembaliKeBank.aspx")
            End Try
        End If

        'End If
    End Sub

#End Region
End Class