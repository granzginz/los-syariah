﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalScreen.aspx.vb"
    Inherits="Maxiloan.Webform.Workflow.ApprovalScreen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Form Persetujuan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinLink(pStrURL) {
            window.open(pStrURL, null, 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }		
    </script>
</head>
<body onload="gridGeneralSize('dtgHistory');" onresize="gridGeneralSize('dtgHistory')">
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:ValidationSummary ID="vsuSummary" runat="server" EnableViewState="False"></asp:ValidationSummary>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    ID Approval
                </label>
                <asp:Label ID="lblApprNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Approval
                </label>
                <asp:Label ID="lblApprType" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <asp:Label ID="lblTrans" runat="server" CssClass="label"></asp:Label>
                <asp:HyperLink ID="lnkTrans" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <asp:Label ID="lblLimit" runat="server" CssClass="label"></asp:Label>
                <asp:Label ID="lblLimitAmount" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <%--<div class="form_box">
        <div class="form_left">
            <table id="tabel_appr1">
                <tbody>
                    <tr>
                        <td class="td_label"  >
                            Agreement No
                        </td>
                        <td class="td_content">
                            <a href="javascript:OpenWinLink('../Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?Style=AccMnt&ApplicationId=801A201206000022')">
                                - </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form_right">
            <table id="tabel_appr2">
                <tbody>
                    <tr>
                        <td class="td_label">
                            Customer Name
                        </td>
                        <td class="td_content">
                            <a href="javascript:OpenWinLink('../Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?Style=AccMnt&ApplicationId=801A201206000022')">
                                BAMBANG KUSMULYADI</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>--%>
    <div class="form_box">
        <div class="form_left">
            <table id="tabel_appr1" runat="server" visible="false">
            </table>
        </div>
        <div class="form_right">
            <table id="tabel_appr2" runat="server" visible="false">
            </table>
        </div>
    </div>
    <%--    <table id="tblApproval" runat="server">
    </table>--%>
    <asp:Panel runat="server" ID="pnlRec" Visible="false" CssClass="form_box_hide">
        <div class="form_title">
            <div class="form_single">
                <asp:Label ID="lblRecomendation" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <%--<asp:Label ID="lblTextRecomendation" runat="server"></asp:Label>--%>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label class="label_req">
                    Approval
                </label>
                <asp:DropDownList ID="cboApproval" runat="server" AutoPostBack="True" DataTextField="Key"
                    DataValueField="Value">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="A">Approved</asp:ListItem>
                    <asp:ListItem Value="J">Reject</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rvlApproval" runat="server" ErrorMessage="Harap dipilih Approval!"
                    ControlToValidate="cboApproval" Display="None" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblSecurityCode" runat="server">Security Code</asp:Label>
                </label>
                <asp:TextBox ID="txtSecurityCode" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                    ID="rvlSecurityCode" runat="server" ErrorMessage="Harap diisi Security Code"
                    ControlToValidate="txtSecurityCode" Display="None" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlDeclineFinal">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Ditolak Final
                </label>
                <asp:RadioButtonList ID="rdoDeclineFinal" runat="server" RepeatDirection="Horizontal"
                    AutoPostBack="True">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlNextPerson">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Diapproved Oleh
                </label>
                <asp:DropDownList ID="cboNextPerson" runat="server" DataTextField="Key" DataValueField="Value">
                </asp:DropDownList>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box">
        <div class="form_single">
            <label>
                Rekomendasi Approval
            </label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox ID="txtNotes" width="50%" Height = "80px" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
        <asp:Button ID="btnCancelSave" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False" />
    </div>
    <table id="tblLink" width="95%" align="center" border="0" runat="server">
    </table>
    <div class="form_box_title">
        <div class="form_single">
            <h5>
                CATATAN SURVEY
            </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:Label ID="lblTextRecomendation" runat="server"></asp:Label>
            <asp:TextBox ID="txtSurveyNotes" width="50%" Height = "80px" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
        </div>
    </div>
    <div class="form_title">
            <div class="form_single">
                <h5>
                    HISTORY APPROVAL</h5>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgHistory" runat="server" CellPadding="3" AutoGenerateColumns="False"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn HeaderText="NO">
                                <HeaderStyle Width="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="UserApprovalName" HeaderText="NAMA" ItemStyle-CssClass="name_col">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalDate" HeaderText="TANGGAL">
                                <HeaderStyle Width="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="WOA" HeaderText="JENIS">
                                <HeaderStyle Width="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Duration" HeaderText="LAMA">
                                <HeaderStyle Width="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SecurityCode" HeaderText="CODE">
                                <HeaderStyle Width="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Argumentasi" HeaderText="ARGUMENTASI"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    <asp:Panel runat="server" ID="pnlApprovalItems" Visible="true">
        <div class="form_box_title">
            <div class="form_single">
                <h5>
                    APPROVAL ITEMS
                </h5>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dgApprovalItems" runat="server" CellPadding="3" AutoGenerateColumns="False"
                    BorderWidth="0" CssClass="grid_general" ShowHeader="false">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="Notes"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlHistory">
        
    </asp:Panel>
    </form>
</body>
</html>
