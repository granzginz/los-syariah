﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalList.aspx.vb"
    Inherits="Maxiloan.Webform.Workflow.ApprovalList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Daftar Approval</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';						
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinLink(pStrURL) {
            window.open(pStrURL, null, 'left=50, top=10, width=900, height=700, menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                APPROVAL</h3>
        </div>
    </div>
    <asp:Panel ID="pnlDataGrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgAppr" runat="server" width="100%" CellPadding="3" AutoGenerateColumns="False"
                        AllowSorting="True" AllowPaging="True" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkApproval" runat="server" CausesValidation="False" Text="APPROVAL"
                                        Visible='<%# IIf(Container.DataItem("ApprovalStatus") = "Final", "False", "True")%>'
                                        CommandName="Approval">APPROVAL</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RE-REQUEST">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReRequest" runat="server" CausesValidation="False" Text="REREQUEST"
                                        Visible='<%# IIf(Container.DataItem("ApprovalStatus") <> "Final" And Trim(Container.DataItem("UserRequest")) = Trim(Session("LoginID")) And Container.DataItem("ApprovalStatus") <> "New", "True", "False")%>'
                                        CommandName="ReRequest">REREQUEST</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:ButtonColumn DataTextField="ApprovalID" SortExpression="ApprovalID" HeaderText="ID APPR"
                                CommandName="View" ItemStyle-CssClass="command_col"></asp:ButtonColumn>
                            <asp:TemplateColumn SortExpression="TransactionNo" ItemStyle-Width="9%" HeaderText="NO APLIKASI">
                                <ItemStyle CssClass="normal_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%# IIf(IsDBNull(Container.DataItem("TransactionNo")), "", Container.DataItem("TransactionNo"))%>'
                                        Visible='<%# IIf(Container.DataItem("AgreementNo") = "", "False", "True")%>'
                                        NavigateUrl='<%#Set_Link(Container.DataItem("TransactionNoLink"), Container.DataItem("TransactionNo"), "", "")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblSchemeID" runat="server" Text='<%# IIf(IsDBNull(Container.DataItem("ApprovalSchemeID")), "", Container.DataItem("ApprovalSchemeID"))%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblApprovalNo" runat="server" Text='<%# Container.DataItem("ApprovalNo")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# IIf(IsDBNull(Container.DataItem("CustomerID")), "", Container.DataItem("CustomerID"))%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblLink1" runat="server" Text='<%# Get_ValueLink(Container.DataItem("OptionalSQL1"), IIf(IsDBNull(Container.DataItem("TransactionNo")), "", Container.DataItem("TransactionNo")), "", "")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblLink2" runat="server" Text='<%# Get_ValueLink(Container.DataItem("OptionalSQL2"), IIf(IsDBNull(Container.DataItem("TransactionNo")), "", Container.DataItem("TransactionNo")), "", "")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" ItemStyle-Width="9%" HeaderText="NO KONTRAK">
                                <ItemStyle CssClass="normal_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%# IIf(IsDBNull(Container.DataItem("AgreementNo")), "", Container.DataItem("AgreementNo"))%>'
                                        Visible='<%# IIf(IsDBNull(Container.DataItem("AgreementNo")), "False", "True")%>'
                                        NavigateUrl='<%#Set_Link(Container.DataItem("TransactionNoLink"), Container.DataItem("TransactionNo"), Container.DataItem("CustomerID"), "")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" ItemStyle-Width="20%" HeaderText="CUSTOMER">
                                <ItemStyle CssClass="medium_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCustomer" runat="server" Text='<%# IIf(IsDBNull(Container.DataItem("CustomerName")), "", Container.DataItem("CustomerName"))%>'
                                        Visible='<%# IIf(IsDBNull(Container.DataItem("AgreementNo")), "False", "True")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ApprovalStatus" HeaderText="STATUS"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalResult" HeaderText="HASIL"></asp:BoundColumn>
                            <asp:BoundColumn DataField="UserApproval" ItemStyle-Width="20%" HeaderText="DARI / KE" ItemStyle-CssClass="normal_col">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RequestDate" ItemStyle-Width="12%" HeaderText="TANGGAL REQUEST" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}"
                                SortExpression="RequestDate"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Duration" ItemStyle-Width="6%" HeaderText="DURATION"></asp:BoundColumn>
                            <asp:BoundColumn DataField="StepOfStep" HeaderText="APPROVAL STEP" ItemStyle-CssClass="normal_col">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                    </asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtpage"
                        Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ErrorMessage="No Halaman salah" ControlToValidate="txtpage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI DATA UNTUK APPROVAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">None</asp:ListItem>
                    <asp:ListItem Value="ApprovalID">ID Approval</asp:ListItem>
                    <asp:ListItem Value="TransactionNo">No Aplikasi </asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="CustomerName">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Approval
                    </label>
                    <asp:Label ID="lblApprovalType" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Status Approval
                    </label>
                    <asp:DropDownList ID="cboApprStatus" runat="server">
                        <asp:ListItem Value="OP">Open</asp:ListItem>
                        <asp:ListItem Value="F">Final</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Diminta Oleh
                    </label>
                    <asp:DropDownList ID="cboRequestFrom" runat="server" DataTextField="FullName" DataValueField="LoginID">
                    </asp:DropDownList>
                </div>
                <div class="form_right">
                    <label>
                        Hasil Approval
                    </label>
                    <asp:DropDownList ID="cboApprResult" runat="server">
                        <asp:ListItem Value="">ALL</asp:ListItem>
                        <asp:ListItem Value="R">Request</asp:ListItem>
                        <asp:ListItem Value="A">Approved</asp:ListItem>
                        <asp:ListItem Value="D">Declined</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Akan di Approved oleh
                </label>
                <asp:DropDownList ID="cboApprBy" runat="server" DataTextField="FullName" DataValueField="LoginID">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlHistory" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    HISTORY APPROVAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgHistory" runat="server" Width="100%" CellPadding="3" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn HeaderText="NO">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="UserApproval" HeaderText="NAMA">
                                <HeaderStyle Width="20%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalDate" HeaderText="TANGGAL">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="WOA" HeaderText="JENIS">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Duration" HeaderText="LAMA">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SecurityCode" HeaderText="SECURITY CODE">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN">
                                <HeaderStyle Width="30%"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
