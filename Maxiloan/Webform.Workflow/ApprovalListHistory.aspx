﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalListHistory.aspx.vb"
    Inherits="Maxiloan.Webform.Workflow.ApprovalListHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ApprovalListHistory</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
		<!--
        function OpenWinLink(pStrURL) {
            window.open(pStrURL, null, 'left=50, top=10, width=900, height=700, menubar=0,scrollbars=1')
        }
		//-->
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server">
        <asp:ValidationSummary ID="vsuSummary" runat="server" EnableViewState="False"></asp:ValidationSummary>
        <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    </asp:Panel>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                HISTORY APPROVAL</h3>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgHistory" runat="server" CellPadding="3" AutoGenerateColumns="False"
                    Width="100%">
                      <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn HeaderText="NO">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="UserRequest" HeaderText="REQUEST OLEH">
                            <HeaderStyle></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="UserApproval" HeaderText="APPROVED OLEH">
                            <HeaderStyle></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ApprovalResult" HeaderText="HASIL">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="RequestDate" HeaderText="TGL REQUEST">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ApprovalDate" HeaderText="TGL APPROVED">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN">
                            <HeaderStyle></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <input   onclick="javascript:history.go(-1)" type="button" value="BACK"
            name="Submit32" class="small button blue" />
        <input class="small button gray" onclick="javascript:window.close()" type="button" value="EXIT"
            name="Submit32" />
    </div>
    <center>
    </center>
    </form>
</body>
</html>
