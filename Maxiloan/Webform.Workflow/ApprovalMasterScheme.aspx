﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalMasterScheme.aspx.vb"
    Inherits="Maxiloan.Webform.Workflow.ApprovalMasterScheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ApprovalMasterScheme</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnApplication" type="hidden" name="hdnApplication" runat="server" />
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR SKEMA MASTER</h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgScheme" runat="server" CssClass="grid_general" AllowSorting="True"
                        DataKeyField="ApprovalSchemeID" AutoGenerateColumns="False" OnSortCommand="sortgrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/IconDelete.gif"
                                        CommandName="DELETE" Visible='<%# iif(container.dataitem("IsSystem") = "1", "False", "True")%>'>
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID SKEMA APPROVAL" SortExpression="ApprovalSchemeID">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="dtgApprovalSchemeID" runat="server" CausesValidation="False"
                                        Text='<%#container.dataitem("ApprovalSchemeID")%>' CommandName="ShowView">
                                    </asp:LinkButton>
                                    <asp:Label ID="dtgApprovalType" runat="server" Visible="False" Text='<%#container.dataitem("ApprovalTypeID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA SKEMA APPROVAL" SortExpression="ApprovalSchemeName">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="dtgApprovalName" runat="server" Text='<%#container.dataitem("ApprovalSchemeName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" CommandName="First"
                            OnCommand="NavigationLink_Click" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" CommandName="Prev"
                            OnCommand="NavigationLink_Click" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" CommandName="Next"
                            OnCommand="NavigationLink_Click" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" CommandName="Last"
                            OnCommand="NavigationLink_Click" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                        </asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" ControlToValidate="txtpage"
                            MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    View Master Type Scheme</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approval Type</label>
                <asp:Label ID="lblApprType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approval ID
                </label>
                <asp:Label ID="lblApprID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approval Name
                </label>
                <asp:Label ID="lblApprNm" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Staging
                </label>
                <asp:Label ID="lblIsStaging" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Limited Needed
                </label>
                <asp:Label ID="lblIsLimited" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Transaction Label
                </label>
                <asp:Label ID="lblLabelTrans" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Transaction Address
                </label>
                <asp:Label ID="lblAddrTrans" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional Link Label 1
                </label>
                <asp:Label ID="lblLabelOptional1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional&nbsp;Address 1
                </label>
                <asp:Label ID="lblAddrOptional1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional SQL Command 1
                </label>
                <asp:Label ID="lblSQLOptional1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional Link Label 2
                </label>
                <asp:Label ID="lblLabelOptional2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional&nbsp;Address 2
                </label>
                <asp:Label ID="lblAddrOptional2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional SQL Command 2
                </label>
                <asp:Label ID="lblSQLOptional2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Note Label
                </label>
                <asp:Label ID="lblLabelNote" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Note SQL Command
                </label>
                <asp:Label ID="lblSQLNote" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Limit Label
                </label>
                <asp:Label ID="lblLabelLimit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 1
                </label>
                <asp:Label ID="lblLabelLink1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 1
                </label>
                <asp:Label ID="lblAddrLink1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 2
                </label>
                <asp:Label ID="lblLabelLink2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 2
                </label>
                <asp:Label ID="lblAddrLink2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 3
                </label>
                <asp:Label ID="lblLabelLink3" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 3
                </label>
                <asp:Label ID="lblAddrLink3" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 4
                </label>
                <asp:Label ID="lblLabelLink4" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 4
                </label>
                <asp:Label ID="lblAddrLink4" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 5
                </label>
                <asp:Label ID="lblLabelLink5" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 5
                </label>
                <asp:Label ID="lblAddrLink5" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancelView" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button  gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlNewEdit" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    New/Edit - Master Type Scheme</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
             <label class ="label_req">
                    Approval Type
                </label>
                <asp:DropDownList ID="cboApprType" runat="server" >
                </asp:DropDownList>
                &nbsp;
                <asp:RequiredFieldValidator ID="rfvApprType" runat="server" ErrorMessage="Harap dipilih Jenis Approval"
                    InitialValue="0" ControlToValidate="cboApprType" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Scheme ID
                </label>
                <asp:TextBox ID="txtApprID" runat="server"  MaxLength="5" Columns="5"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="rfvApprID" runat="server" ErrorMessage="Harap diisi ID Skema"
                    ControlToValidate="txtApprID" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
              <label class ="label_req">
                    Scheme Name
                </label>
                <asp:TextBox ID="txtApprNm" runat="server"  MaxLength="50" Columns="40"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="rfvApprNm" runat="server" ErrorMessage="Harap diisi Nama Skema"
                    ControlToValidate="txtApprNm" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Staging
                </label>
                <asp:CheckBox ID="chkIsStaging" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Limited Needed
                </label>
                <asp:CheckBox ID="chkIsLimited" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Transaction Label
                </label>
                <asp:TextBox ID="txtLabelTrans" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Transaction Address
                </label>
                <asp:TextBox ID="txtAddrTrans" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional Link Label 1
                </label>
                <asp:TextBox ID="txtLabelOptinal1" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional&nbsp;Address 1
                </label>
                <asp:TextBox ID="txtAddrOptional1" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional SQL Command 1
                </label>
                <asp:TextBox ID="txtSQLOptional1" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional Link Label 2
                </label>
                <asp:TextBox ID="txtLabelOptinal2" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional&nbsp;Address 2
                </label>
                <asp:TextBox ID="txtAddrOptional2" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional SQL Command 2
                </label>
                <asp:TextBox ID="txtSQLOptional2" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Note Label
                </label>
                <asp:TextBox ID="txtLabelNote" runat="server"  MaxLength="30" Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Note SQL Command
                </label>
                <asp:TextBox ID="txtSQLNote" runat="server"  MaxLength="100" Columns="40"
                    TextMode="MultiLine" Rows="5"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Limit Label
                </label>
                <asp:TextBox ID="txtLabelLimit" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 1
                </label>
                <asp:TextBox ID="txtLabelLink1" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 1
                </label>
                <asp:TextBox ID="txtAddrLink1" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 2
                </label>
                <asp:TextBox ID="txtLabelLink2" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 2
                </label>
                <asp:TextBox ID="txtAddrLink2" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 3
                </label>
                <asp:TextBox ID="txtLabelLink3" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 3
                </label>
                <asp:TextBox ID="txtAddrLink3" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 4
                </label>
                <asp:TextBox ID="txtLabelLink4" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 4
                </label>
                <asp:TextBox ID="txtAddrLink4" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 5
                </label>
                <asp:TextBox ID="txtLabelLink5" runat="server"  MaxLength="30"
                    Columns="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 5
                </label>
                <asp:TextBox ID="txtAddrLink5" runat="server"  MaxLength="100"
                    Columns="40" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="btnCancelSave" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
