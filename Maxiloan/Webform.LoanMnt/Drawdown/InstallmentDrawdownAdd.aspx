﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallmentDrawdownAdd.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InstallmentDrawdownAdd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../webform.UserController/ucLookUpCustomer.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Installment Drawdown</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       
        function hitungUlang()
        {
            var duration = hitungJangkaWaktu();
            var bunga = document.getElementById('hdnFlatRate').value;
            var pencairan = document.getElementById('ucJumlahPencairan_txtNumber').value;
            var provisi = document.getElementById('hdnProvisiPercent').value;
            var biayaprovisi = (provisi / 100) * pencairan;
            var totalbunga =  (pencairan * ((bunga / 100)) / 360) * duration;

            $('#lblTotalBunga').html(number_format(totalbunga, 0));
            $('#lblBiayaProvisi').html(number_format(biayaprovisi, 0));
        }

        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('ucPeriodeFrom_txtDateCE').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);

            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var _invoiceDueDate = document.getElementById('ucPeriodeTo_txtDateCE').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
    </script>
     <style >
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
<body>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div runat="server" id="jlookupContent" />
 
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        NEW DRAW DOWN
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class=""> Nama Customer</label>
                    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
                    <input id="hdnBranchID" type="hidden" name="hdnBranchID" runat="server" />
                    <input id="hdnInsSeqNo" type="hidden" name="hdnInsSeqNo" runat="server" />
                    <input id="hdnFlatRate" type="hidden" name="hdnFlatRate" runat="server" />
                    <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
                    <input id="hdnProvisiPercent" type="hidden" name="hdnProvisiPercent" runat="server" />
                    <asp:Label ID="lblNamaCustomer" runat="server"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Jenis Pembiayaan
                    </label>
                    <asp:Label ID="lblKegiatanUsaha" runat="server"></asp:Label>
                    <asp:DropDownList ID="cboKegiatanUsaha" runat="server" Visible="false" />
                </div>
                <div class="form_right">
                    <label>
                        Skema Pembiayaan
                    </label>
                    <asp:Label ID="lblJenisPembiyaan" runat="server"></asp:Label>
                    <asp:DropDownList ID="cboJenisPembiyaan" runat="server" Visible="false" />
                </div>    
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Periode Pencairan
                    </label>
                    <asp:Label ID="lblPeriodePencairan" runat="server"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label  class="">Jumlah Plafond</label>
                    <asp:Label ID="lblFasilitasAmount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label  class="">
                        Flat Rate
                    </label>
                    <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Skema Angsuran
                    </label>
                    <asp:Label ID="lblInstallmentScheme" runat="server"></asp:Label>
                    <asp:DropDownList ID="cboInstScheme" runat="server" visible="false" >
                        <asp:ListItem Value="RF">Regular Fixed Installment Scheme</asp:ListItem>
                        <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                        <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                        <asp:ListItem Value="BP">Ballon Payment</asp:ListItem>
                        <asp:ListItem Value="DL">Demand Loan</asp:ListItem>
                        <asp:ListItem Value="RK">PRK</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form_right">
                    <label  class="">
                        Persentase Provisi
                    </label>
                    <asp:Label ID="lblBiayaProvisiPercent" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Type Fasilitas
                    </label>
                    <asp:Label ID="lblFasilitasType" runat="server"></asp:Label>
                    <asp:DropDownList runat="server" ID="listFasilitasType" visible="false" >
                        <asp:ListItem Value="R">Revolving</asp:ListItem>
                        <asp:ListItem Value="N">Non Revolving</asp:ListItem>
                    </asp:DropDownList>
                </div>    
            </div>
           <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Tanggal Pencairan
                    </label>
                    <uc1:ucdatece id="ucPeriodeFrom" runat="server"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_box" id="panelJatuhTempo">
                <div class="form_left">
                    <label class="label_req">
                        Jatuh Tempo
                    </label>
                    <uc1:ucdatece id="ucPeriodeTo" runat="server"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Jumlah Pencairan
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucJumlahPencairan" onclientchange="hitungUlang()" ></uc1:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total Margin
                    </label>
                    <asp:Label ID="lblTotalBunga" runat="server"></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Biaya Provisi
                    </label>
                    <asp:Label ID="lblBiayaProvisi" runat="server"></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Tipe Margin
                    </label>
                    <asp:DropDownList runat="server" ID="cboInterestType">
                        <asp:ListItem Value="A">Advance</asp:ListItem>
                        <asp:ListItem Value="R">Arrear</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box" runat="server" id="panelApproval">
                <div class="form_left">
                    <label class="label_req"> Akan diSetujui Oleh </label>
                    <asp:DropDownList ID="cboApprovedBy" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh" Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general" />-
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button blue" />
                <asp:Button ID="btnSave" runat="server" Text="Proceed" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h5>
                        DATA PENCAIRAN</h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgPaging" runat="server" DataKeyField=""
                            CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn DataField="InsSeqNo" HeaderText="No." ItemStyle-CssClass=""></asp:BoundColumn>
                                <asp:BoundColumn DataField="DrawdownDate" HeaderText="Tanggal Pencairan" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DrawdownAmount" HeaderText="Pencairan" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingPrincipal" HeaderText="OS Pokok" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmountUnused" HeaderText="Unused" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestPeriodFrom" HeaderText="Margin Dari" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestPeriodTo" HeaderText="Margin s/d" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestPeriod" HeaderText="Margin Hari" ItemStyle-CssClass="" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmount" HeaderText="Jumlah Margin" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaidDate" HeaderText="Tgl Bayar" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaidPrincipal" HeaderText="Bayar Pokok" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmountPaid" HeaderText="Bayar Margin" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmountTotal" HeaderText="Jumlah Kewajiban" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaidStatus" SortExpression="Status" HeaderText="Status" ItemStyle-CssClass=""></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>  
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- </asp:Content>--%>
 </form>
</body>
</html> 
