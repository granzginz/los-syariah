﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallmentDrawdownApprovalExec.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallmentDrawdownApprovalExec" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Installment Drawdown Exec Approval</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinViewInstallmentDrawdown(id) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewFacility/ViewInstallmentDrawdown.aspx?id=' + id, 'CustomerLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
    <form id="Form1" method="post" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DRAWDOWN APPROVAL
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Drawdown
            </label>            
            <asp:HyperLink ID="hypDrawdownNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Amount Yang Akan Di Approve
            </label>
            <asp:Label ID="lblAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right">

        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label class="label_req">
                    Approval
                </label>
<%--                <asp:DropDownList ID="cboApproval" runat="server" AutoPostBack="True" DataTextField="Key"
                    DataValueField="Value">--%>
                    <asp:DropDownList ID="cboApproval" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboApproval_SelectedIndexChanged" >                    
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="A">Approved</asp:ListItem>
                    <asp:ListItem Value="J">Reject</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rvlApproval" runat="server" ErrorMessage="Harap dipilih Approval!"
                    ControlToValidate="cboApproval" Display="None" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <asp:Panel ID="pnlSecurityCode" runat="server" >
            <div class="form_right">
                <label>
                    <asp:Label ID="lblSecurityCode" runat="server">Security Code</asp:Label>
                </label>
                <asp:TextBox ID="txtSecurityCode" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                    ID="rvlSecurityCode" runat="server" ErrorMessage="Harap diisi Security Code"
                    ControlToValidate="txtSecurityCode" Display="None" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            </asp:Panel>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlDeclineFinal">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Ditolak Final
                </label>
<%--                <asp:RadioButtonList ID="rdoDeclineFinal" runat="server" RepeatDirection="Horizontal"
                    AutoPostBack="True">--%>
                <asp:RadioButtonList ID="rdoDeclineFinal" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" 
                OnSelectedIndexChanged="rdoDeclineFinal_SelectedIndexChanged" >
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlNextPerson">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Diapproved Oleh
                </label>
                <asp:DropDownList ID="cboNextPerson" runat="server" DataTextField="FullName" DataValueField="LoginID">
                </asp:DropDownList>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box">
        <div class="form_single">
            <label>
                Rekomendasi Approval
            </label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox ID="txtNotes" width="50%" Height = "80px" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
