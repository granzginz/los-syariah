﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region


Public Class InstallmentDrawdownAdd
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucPeriodeFrom As ucDateCE
    Protected WithEvents ucPeriodeTo As ucDateCE
    Protected WithEvents ucJumlahPencairan As ucNumberFormat

    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New ApplicationController
    Private m_controller As New ProductController
    Private m_customer As New CustomerController

    Private ocustomclass As New Parameter.Application
    Private dcustomclass As New Parameter.Drawdown
    Private dataInstallment As DataTable

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property InsSeqNo() As Int16
        Get
            Return CType(ViewState("InsSeqNo"), Int16)
        End Get
        Set(ByVal Value As Int16)
            ViewState("InsSeqNo") = Value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CheckFeature(Me.Loginid, "InstallmentDrawdown", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.BranchID = Request("BranchID")
            Me.InsSeqNo = Request("InsSeqNo")
            btnNext.Visible = True
            btnSave.Visible = False
            panelApproval.Visible = False
            If Me.ApplicationID <> "" Then
                getDataApplication()
                BindEdit()
                BindDataGrid()
            End If
        End If
    End Sub

    Public Sub getDataApplication()
        ' Bind data for edit maintenance
        With ocustomclass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .AppID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        ocustomclass = m_controllerApp.GetApplicationEdit(ocustomclass)
    End Sub

    Public Sub BindDataGrid()
        Dim param As New Parameter.Drawdown
        With param
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
        End With
        param = m_controllerApp.InstallmentDrawDownList(param)
        If param IsNot Nothing Then
            dataInstallment = param.listData
            dtgPaging.DataSource = dataInstallment
            dtgPaging.DataBind()
        End If

    End Sub


#Region "Edit"
    Sub BindEdit()
        Dim cust As New Parameter.Customer
        Dim custType As String
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, sukubunga, plafond, tenor As Decimal
        Dim oRow As DataRow = ocustomclass.ListData.Rows(0)
        Dim cRow As DataRow

        cust.strConnection = GetConnectionString()
        cust.CustomerID = oRow("CustomerID").ToString
        custType = m_customer.GetCustomerType(cust)
        cust.CustomerType = custType
        cust = m_customer.GetViewCustomer(cust)
        cRow = cust.listdata.Rows(0)

        plafond = oRow("NTF")
        NotaryFee = oRow("NotaryFee")
        AdminFee = oRow("AdminFee")
        ProvisionFee = oRow("ProvisiPercent")
        SurveyFee = oRow("SurveyFee")
        sukubunga = oRow("FlatRate")
        tenor = oRow("Tenor")

        lblNamaCustomer.Text = cRow("CustomerName").ToString
        hdnCustomerID.Value = oRow("CustomerID").ToString
        hdnApplicationID.Value = oRow("ApplicationID").ToString
        hdnBranchID.Value = oRow("BranchID").ToString
        hdnFlatRate.Value = oRow("FlatRate")
        hdnInsSeqNo.Value = IIf(IsNothing(Me.InsSeqNo), "", Me.InsSeqNo)

        lblPeriodePencairan.Text = Format(oRow("GoLiveDate"), "dd/MM/yyyy") & " s/d " & Format(oRow("MaturityDate"), "dd/MM/yyyy")
        lblFasilitasAmount.Text = FormatNumber(plafond, 0)
        lblFlatRate.Text = FormatNumber(sukubunga, 0) & " %"
        lblBiayaProvisiPercent.Text = FormatNumber(ProvisionFee, 0) & " %"
        hdnProvisiPercent.Value = ProvisionFee

        LoadingKegiatanUsaha()
        cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
        lblKegiatanUsaha.Text = cboKegiatanUsaha.SelectedItem.ToString
        refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString)
        cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
        lblJenisPembiyaan.Text = cboJenisPembiyaan.SelectedItem.ToString

        cboInstScheme.SelectedIndex = cboInstScheme.Items.IndexOf(cboInstScheme.Items.FindByValue(oRow("InstallmentScheme").ToString))
        lblInstallmentScheme.Text = cboInstScheme.SelectedItem.ToString
        listFasilitasType.SelectedIndex = listFasilitasType.Items.IndexOf(listFasilitasType.Items.FindByValue(oRow("FacilityType").ToString))
        lblFasilitasType.Text = listFasilitasType.SelectedItem.ToString


    End Sub

    Private Sub loadApprovalData()
        Dim oData As New DataTable
        oData = Get_UserApproval("RCA5", Me.sesBranchId.Replace("'", ""), CDec(ucJumlahPencairan.Text))
        cboApprovedBy.DataSource = oData.DefaultView
        cboApprovedBy.DataTextField = "Name"
        cboApprovedBy.DataValueField = "ID"
        cboApprovedBy.DataBind()
        cboApprovedBy.Items.Insert(0, "Select One")
        cboApprovedBy.Items(0).Value = "0"
    End Sub

    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"

        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"
        cboJenisPembiyaan.DataSource = def
        cboJenisPembiyaan.Items.Insert(0, "Select One")
        cboJenisPembiyaan.Items(0).Value = "SelectOne"
        cboJenisPembiyaan.DataBind()
    End Sub

    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"

        If key = String.Empty Then
            cboJenisPembiyaan.DataSource = def
            cboJenisPembiyaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiyaan.DataBind()
    End Sub

#End Region

    Function Validator() As Boolean
        If (CDec(ucJumlahPencairan.Text) < 1) Then
            ShowMessage(lblMessage, "harap isi jumlah pencairan", True)
            Return False
        End If
        Dim startDate As Date = ConvertDate2(ucPeriodeFrom.Text)
        Dim endDate As Date = ConvertDate2(ucPeriodeTo.Text)
        If (startDate >= endDate) Then
            ShowMessage(lblMessage, "periode tanggal tidak benar", True)
            Return False
        End If
        If (startDate < Me.BusinessDate) Then
            ShowMessage(lblMessage, "periode tanggal tidak benar", True)
            Return False
        End If
        Return True
    End Function

#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim err As String = ""

        If Validator() = False Then
            Exit Sub
        End If

        Try
            Dim param As New Drawdown
            Dim startDate As Date = ConvertDate2(ucPeriodeFrom.Text)
            Dim endDate As Date = ConvertDate2(ucPeriodeTo.Text)
            Dim periode As Int16 = DateDiff(DateInterval.Day, startDate, endDate)
            Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, sukubunga, plafond, tenor As Decimal

            getDataApplication()
            Dim oRow As DataRow = ocustomclass.ListData.Rows(0)

            plafond = oRow("NTF")
            NotaryFee = oRow("NotaryFee")
            AdminFee = oRow("AdminFee")
            ProvisionFee = oRow("ProvisiPercent")
            SurveyFee = oRow("SurveyFee")
            sukubunga = oRow("FlatRate")
            tenor = oRow("Tenor")

            param.strConnection = GetConnectionString()
            param.ApplicationID = hdnApplicationID.Value
            param.DrawdownAmount = CDec(ucJumlahPencairan.Text)
            param.BranchId = hdnBranchID.Value
            param.InterestPeriodFrom = IIf(ucPeriodeFrom.Text <> "", ConvertDate(ucPeriodeFrom.Text), "").ToString
            param.InterestPeriodTo = IIf(ucPeriodeTo.Text <> "", ConvertDate(ucPeriodeTo.Text), "").ToString
            param.InterestPeriod = periode
            param.InterestType = cboInterestType.SelectedValue
            param.BiayaProvisi = CDec(ucJumlahPencairan.Text) * (ProvisionFee / 100)
            param.InterestAmount = ((sukubunga / 100) / 360) * CDec(ucJumlahPencairan.Text) * periode
            param.LoginId = Me.UserID
            param.BusinessDate = Me.BusinessDate
            param.DrawdownDate = Me.BusinessDate.ToString("yyyyMMdd")
            param.UserApproval = cboApprovedBy.SelectedValue

            err = m_controllerApp.InstallmentDrawDownSave(param)

            'ShowMessage(lblMessage, "Data saved!", False)
        Catch ex As Exception
            'ShowMessage(lblMessage, ex.Message, True)
            err = ex.Message
        End Try

        If (err = "") Then
            Response.Redirect("InstallmentDrawdown.aspx?message=Proses request drawdown berhasil")
        Else
            Response.Redirect("InstallmentDrawdown.aspx?message1=Proses request drawdown gagal")
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("InstallmentDrawdown.aspx")
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        Dim startDate As Date = ConvertDate2(ucPeriodeFrom.Text)
        Dim endDate As Date = ConvertDate2(ucPeriodeTo.Text)
        Dim periode As Int16 = DateDiff(DateInterval.Day, startDate, endDate)
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, sukubunga, plafond, tenor, BiayaProvisi, InterestAmount As Decimal

        getDataApplication()
        Dim oRow As DataRow = ocustomclass.ListData.Rows(0)


        plafond = oRow("NTF")
        NotaryFee = oRow("NotaryFee")
        AdminFee = oRow("AdminFee")
        ProvisionFee = oRow("ProvisiPercent")
        SurveyFee = oRow("SurveyFee")
        sukubunga = oRow("FlatRate")
        tenor = oRow("Tenor")

        If Validator() = False Then
            Exit Sub
        End If

        BiayaProvisi = CDec(ucJumlahPencairan.Text) * (ProvisionFee / 100)
        InterestAmount = ((sukubunga / 100) / 360) * CDec(ucJumlahPencairan.Text) * periode

        lblBiayaProvisi.Text = FormatNumber(BiayaProvisi, 0)
        lblTotalBunga.Text = FormatNumber(InterestAmount, 0)


        loadApprovalData()

        ucJumlahPencairan.Enabled = True
        ucPeriodeFrom.Enabled = False
        ucPeriodeTo.Enabled = False

        btnSave.Visible = True
        btnNext.Visible = False
        panelApproval.Visible = True

    End Sub

#End Region

End Class