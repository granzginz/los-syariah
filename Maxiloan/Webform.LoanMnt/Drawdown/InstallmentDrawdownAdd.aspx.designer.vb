﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class InstallmentDrawdownAdd
    
    '''<summary>
    '''UpdateProgress1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress1 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''jlookupContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents jlookupContent As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''updatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTitle As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''hdnApplicationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnApplicationID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnBranchID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBranchID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnInsSeqNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnInsSeqNo As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnFlatRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFlatRate As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnCustomerID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCustomerID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnProvisiPercent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnProvisiPercent As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''lblNamaCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNamaCustomer As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblKegiatanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKegiatanUsaha As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboKegiatanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboKegiatanUsaha As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblJenisPembiyaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblJenisPembiyaan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboJenisPembiyaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboJenisPembiyaan As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblPeriodePencairan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPeriodePencairan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblFasilitasAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFasilitasAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblFlatRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFlatRate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblInstallmentScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInstallmentScheme As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboInstScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboInstScheme As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblBiayaProvisiPercent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBiayaProvisiPercent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblFasilitasType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFasilitasType As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''listFasilitasType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents listFasilitasType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblTotalBunga control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalBunga As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblBiayaProvisi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBiayaProvisi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboInterestType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboInterestType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''panelApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panelApproval As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''cboApprovedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboApprovedBy As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''RequiredFieldValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''btnNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNext As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''dtgPaging control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgPaging As Global.System.Web.UI.WebControls.DataGrid
End Class
