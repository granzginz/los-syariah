﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallmentDrawdownApprovalExec
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Private oCustomClass As New Parameter.Drawdown
    Private oController As New InstallmentDrawdownController
#End Region

#Region "Property"

    Property DrawdownNo() As String
        Get
            Return ViewState("DrawdownNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("DrawdownNo") = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            Return CType(ViewState("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmount") = Value
        End Set
    End Property

    Property RequestDate() As String
        Get
            Return ViewState("RequestDate").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("RequestDate") = Value
        End Set
    End Property

    Property isFinal() As Boolean
        Get
            Return ViewState("isFinal").ToString
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isFinal") = Value
        End Set
    End Property

    Property NextPersonApproval() As String
        Get
            Return ViewState("NextPersonApproval").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NextPersonApproval") = Value
        End Set
    End Property

    Property UserApproval() As String
        Get
            Return ViewState("UserApproval").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("UserApproval") = Value
        End Set
    End Property

    Property ApprovalNo() As String
        Get
            Return ViewState("ApprovalNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalNo") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.DrawdownNo = Request.QueryString("DrawdownNo")
            Me.TotalAmount = Request.QueryString("TotalAmount")
            Me.RequestDate = Request.QueryString("RequestDate")
        End If

        Dim oInqPCReimburse As New Parameter.Drawdown
        Dim pInqPCReimburse As New Parameter.Drawdown
        With oCustomClass
            .strConnection = GetConnectionString()
            .DrawdownNo = Me.DrawdownNo
            .ApprovalSchemeID = "RCA5"
            .LoginId = Me.Loginid
            .IsReRequest = 1
        End With

        'jika login yang approved bukan login yang semestinya, maka munculkan security code
        oInqPCReimburse = oController.ApproveIsValidApproval(oCustomClass)
        Me.UserApproval = LTrim(RTrim(oInqPCReimburse.UserApproval))
        Me.ApprovalNo = LTrim(RTrim(oInqPCReimburse.ApprovalNo))
        oCustomClass.ApprovalNo = Me.ApprovalNo

        If Me.UserApproval <> Me.Loginid = True Then
            pnlSecurityCode.Visible = True
        Else
            pnlSecurityCode.Visible = False
        End If

        'Cek userscheme apakah bisa final?
        pInqPCReimburse = oController.ApproveisFinal(oCustomClass)
        Me.NextPersonApproval = LTrim(RTrim(pInqPCReimburse.NextPersonApproval))
        If pInqPCReimburse.IsFinal = False Then
            Me.isFinal = False
        Else
            Me.isFinal = True
        End If

        BindData()
        pnlDeclineFinal.Visible = False
        pnlNextPerson.Visible = False
    End Sub

    Sub BindData()
        Dim hypDrawdownNoa As HyperLink

        hypDrawdownNo.Text = Me.DrawdownNo
        lblAmount.Text = FormatNumber(Me.TotalAmount, 2)
        lblRequestDate.Text = Me.RequestDate

        hypDrawdownNoa = CType((hypDrawdownNo), HyperLink)
        hypDrawdownNoa.NavigateUrl = LinkToViewPCReimburse(hypDrawdownNo.Text.Trim, "ACCMNT", "ALL")
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim pCustomClass As New Parameter.Drawdown
        Dim Result As String

        With pCustomClass
            .strConnection = GetConnectionString()
            .ApprovalSchemeID = "RCA5"
            .DrawdownNo = Me.DrawdownNo
            .ApprovalResult = cboApproval.SelectedValue
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .BusinessDate = Me.BusinessDate
            .ApprovedDate = Me.BusinessDate
            .notes = txtNotes.Text
            .SecurityCode = txtSecurityCode.Text.Trim
            .UserApproval = Me.Loginid
            .UserSecurityCode = If(Me.UserApproval <> Me.Loginid = True, Me.Loginid, "")
            .IsEverRejected = 0
            .LoginId = Me.Loginid
            .NextPersonApproval = Me.NextPersonApproval
        End With

        If cboApproval.SelectedValue = "J" Then
            If rdoDeclineFinal.SelectedValue = 1 Then

                Try
                    Result = oController.ApproveSave(pCustomClass)
                    If Result <> Nothing Then
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message=Proses Approve Berhasil")
                    End If
                Catch ex As Exception

                End Try

            ElseIf rdoDeclineFinal.SelectedValue = 0 Then

                Try
                    Result = oController.ApproveSaveToNextPerson(pCustomClass)
                    If Result <> Nothing Then
                        ShowMessage(lblMessage, Result, True)
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message=Proses Approve Request To Next Person Berhasil")
                    End If
                Catch ex As Exception

                End Try

            End If
        ElseIf cboApproval.SelectedValue = "A" Then
            If Me.isFinal = True Then

                Try
                    Result = oController.ApproveSave(pCustomClass)
                    If Result <> Nothing Then
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message=Proses Approve Berhasil")
                    End If
                Catch ex As Exception

                End Try

            ElseIf Me.isFinal = False Then

                Try
                    Result = oController.ApproveSaveToNextPerson(pCustomClass)
                    If Result <> Nothing Then
                        ShowMessage(lblMessage, Result, True)
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("InstallmentDrawdownApproval.aspx?message=Proses Approve Request To Next Person Berhasil")
                    End If
                Catch ex As Exception

                End Try

            End If
        End If

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("InstallmentDrawdownApproval.aspx")
    End Sub

    Protected Sub cboApproval_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboApproval.SelectedIndexChanged
        Dim WhereUser As String = "BranchID = '" & Me.NextPersonApproval & "'"
        If cboApproval.SelectedValue = "A" Then
            If Me.isFinal = False Then
                pnlNextPerson.Visible = True
                FillCboEmp()
            End If
        ElseIf cboApproval.SelectedValue = "J" Then
            pnlDeclineFinal.Visible = True
        End If

    End Sub

    Protected Sub FillCboEmp()
        Dim oPCReimburse As New Parameter.Drawdown
        Dim oData As New DataTable

        oPCReimburse.strConnection = GetConnectionString()
        oPCReimburse.AppID = "IFINAPPMGR"
        oPCReimburse.LoginId = Me.NextPersonApproval
        oPCReimburse = oController.GetCboUserApproval(oPCReimburse)

        oData = oPCReimburse.listData
        cboNextPerson.DataSource = oData
        cboNextPerson.DataTextField = "FullName"
        cboNextPerson.DataValueField = "LoginID"
        cboNextPerson.DataBind()
        cboNextPerson.Items.Insert(0, "Select One")
        cboNextPerson.Items(0).Value = "Select One"
    End Sub

    Protected Sub rdoDeclineFinal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoDeclineFinal.SelectedIndexChanged
        If rdoDeclineFinal.SelectedValue = 0 Then
            pnlDeclineFinal.Visible = True
            pnlNextPerson.Visible = True
            FillCboEmp()
        ElseIf rdoDeclineFinal.SelectedValue = 1 Then
            pnlDeclineFinal.Visible = True
        End If
    End Sub

#Region "linkTo"
    Function LinkToViewPCReimburse(ByVal strApplicationID As String, ByVal strStyle As String, ByVal strBranch As String) As String
        ' Return "javascript:OpenWinViewPCReimburse('" & strStyle & "','" & strApplicationID & "','" & strBranch & "')"
    End Function
#End Region


End Class