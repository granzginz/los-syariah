﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallmentDrawdown
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New AgreementListController
    Private a_controller As New ApplicationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                txtGoPage.Text = "1"
                If CheckForm(Me.Loginid, "InstallmentDrawdown", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.Sort = "Name ASC"
                BindGrid(Me.CmdWhere)
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), False)
            End If
            If Request.QueryString("message1") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message1"), True)
            End If
        End If
    End Sub
#End Region
#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkCustName As LinkButton
            lnkCustName = CType(e.Item.FindControl("lnkCustName"), LinkButton)
            lnkCustName.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(1).Text & "','accacq');")
        End If
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.Item.ItemIndex >= 0 Then
            Dim Id As String = e.Item.Cells(0).Text.Trim
            Dim branchid As String = e.Item.Cells(2).Text.Trim
            If e.CommandName = "NewApp" Then
                If CheckFeature(Me.Loginid, "InstallmentDrawdown", "Add", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Dim drawdown As New Parameter.Drawdown
                drawdown.strConnection = GetConnectionString()
                drawdown.ApplicationID = Id
                Dim isPending As Boolean = a_controller.InstallmentDrawDownCheckRequest(drawdown)
                If isPending = False Then
                    ShowMessage(lblMessage, "Drawdown request still pending", True)
                Else
                    Response.Redirect("InstallmentDrawdownAdd.aspx?Applicationid=" & Id & "&BranchID=" & branchid)
                End If

            End If
        End If
    End Sub

    Sub InitialDefaultPanel()
        pnlList.Visible = True
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.AgreementList
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize

        oCustomClass.BranchId = Replace(Me.sesBranchId, "'", "")
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.AgreementListDrawdown(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListAgreement
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        InitialDefaultPanel()
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid(Me.CmdWhere)
    End Sub


#End Region

End Class