﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtherDisburseMulti.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.OtherDisburseMulti" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentDetailD" Src="../../Webform.UserController/UcPaymentDetailD.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Other Disburse</title>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <link href="../../Include/jquery-ui.css" rel = "Stylesheet" type="text/css" /> 

    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />

    <script language="javascript" type="text/javascript">
        function OpenTransaction(pTransaction, pDescription, pIsAgreement, pIsPettyCash, pIsHOTransaction, pProcessID, pStyle, pIsPaymentReceive) {
            var AppInfo = window.location.pathname;
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var ServerName = window.location.protocol + '//' + window.location.host + '/';
            window.open(ServerName + App + '/General/LookUpTransaction.aspx?Transaction=' + pTransaction + '&Description=' + pDescription + '&IsAgreeMent=' + pIsAgreement + '&IsPettyCash=' + pIsPettyCash + '&IsHOTransaction=' + pIsHOTransaction + '&ProcessID=' + pProcessID + '&Style=' + pStyle + '&IsPaymentReceive=' + pIsPaymentReceive, 'TransactionLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
        }
    </script>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var hidBankAccount;
        function WOPChange(pCmbOfPayment, pBankAccount, pHidDetail, pAmountReceive, itemArray) {
            hidBankAccount = eval('document.forms[0].' + pHidDetail);
            var a = eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value;
            if (a == 'CP') {
                for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                    eval('document.forms[0].' + pBankAccount).options[i] = null;
                }
                eval('document.forms[0].' + pAmountReceive).disabled = true;
                eval('document.forms[0].' + pAmountReceive).value = 0;
                eval('document.forms[0].' + pBankAccount).disabled = true;
                hidBankAccount.value = '';
            }
            else {
                eval('document.forms[0].' + pAmountReceive).disabled = false;
                eval('document.forms[0].' + pBankAccount).disabled = false;
                var i, j;
                for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                    eval('document.forms[0].' + pBankAccount).options[i] = null
                };
                j = 0;
                eval('document.forms[0].' + pBankAccount).options[j++] = new Option('Select One', '0');
                //		eval('document.forms[0].' + pBankAccount).options[j++].value = '0';
                //                var a;
                if (itemArray != null) {
                    for (i = 0; i < itemArray.length; i++) {
                        eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
                    };
                    eval('document.forms[0].' + pBankAccount).selected = true;
                };
            };
        }

        function setCboDetlVal(l) {
            hidBankAccount.value = l;
        }

        function klikpotong(n, x) {
            var amount = $('#' + x).val();
            var a = amount.replace(/\s*,\s*/g, '');

            if ($('#' + n).is(":checked")) {
                amount = parseFloat(a) * -1;
            } else {
                amount = Math.abs(parseFloat(a));
            }

            $('#' + x).val(number_format(amount));
            total();
        }

        function total(e) {
            var grid = document.getElementById('DtgTransList');
            var rowCount = grid.rows.length - 2;
            var total = 0;     

            for (i = 0; i < rowCount; i++) {
                var Amount = document.getElementById('DtgTransList_txtAmountTrans_' + i).value.replace(/,/gi, "");

                total = total + parseFloat(Amount);
            }

            $('#DtgTransList_txtTotTransAmount').val(number_format(total, 2));
            $('#hdTotal').val(number_format(total, 2));                        
        }

        function selectAllCheckbox(val) {
            $('#DtgTransList input:checkbox').prop('checked', $(val).is(':checked'));
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function DoPostBack() {
            __doPostBack('Jlookup', '');
        }
        function selectAllCheckbox(val) {            
            $('#DtgTransList input:checkbox').prop('checked', $(val).is(':checked'));
        }

       
    </script>

    <script type="text/javascript">
        var submit = 0;
        function CheckDouble() {
            if (++submit > 1) {
                alert('This sometimes takes a few seconds - please be patient.');

                setInterval(function () {
                    submit = 0;
                },3000)
                return false;
            }
        }
 </script>

</head>
<body>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        function OpenLookup() {
            if ($('#cboDepartement').val() == "0") {
                alert("Departement harap diisi")
                return;
            } else {
//                OpenJLookup('<%= ResolveClientUrl("~/Webform.LoanMnt/GeneralLookup/ucLookupTransMnt.aspx?vspay=vsPay&vsjumlah=vsJumlah&vsdesc=vsDesc") %>', 'DAFTAR TRANSAKSI', '<%= jlookupContent.ClientID %>', 'DoPostBack')
                OpenJLookup('<%= ResolveClientUrl("~/Webform.LoanMnt/GeneralLookup/ucLookupTransMnt.aspx?vspay=vsPay&vsjumlah=vsJumlah&vsdesc=vsDesc&isPaymentRequest=1") %>', 'DAFTAR TRANSAKSI', '<%= jlookupContent.ClientID %>', 'DoPostBack')
            }
        }
    </script>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="jlookupContent" runat="server" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PEMBAYARAN LAINNYA
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlawal" runat="server" Visible="True">
                <div class="form_box_uc">
                    <uc1:ucpaymentdetaild id="oPaymentDetail" runat="server"></uc1:ucpaymentdetaild>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlView" runat="server" Visible="False">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PEMBAYARAN LAINNYA
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Penerima</label>
                        <asp:Label ID="lblRecipientName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cara Pembayaran</label>
                        <asp:Label ID="lblWOP" runat="server"></asp:Label>
                    </div>
                </div>
                <asp:Panel runat="server" ID="pnlGT" Visible="false">
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Jenis Transfer
                            </label>
                            <asp:Label runat="server" ID="lblJenisTransfer"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                BENEFICIARY BANK ACCOUNT</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Bank</label>
                            <asp:Label runat="server" ID="lblNamaBank"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Cabank</label>
                            <asp:Label runat="server" ID="lblNamaCabang"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                No. Rekening</label>
                            <asp:Label runat="server" ID="lblNoRekening"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Rekening</label>
                            <asp:Label runat="server" ID="lblNamaRekening"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            TRANSAKSI PEMBAYARAN</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No. Bukti Kas Keluar</label>
                        <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Valuta</label>
                        <asp:Label ID="lblValueDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah DiBayar</label>
                        <asp:Label ID="lblAmountRec" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Bank</label>
                        <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Catatan</label>
                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Departemen</label>
                        <asp:DropDownList ID="cboDepartement" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Visible="true"
                            ErrorMessage="Harap Pilih Departemen" ControlToValidate="cboDepartement" Display="Dynamic"
                            InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Transaksi</label>
                            <asp:HiddenField runat="server" ID="vsPay" />                                                        
                            <asp:HiddenField runat="server" ID="vsDesc" />
                            <asp:HiddenField runat="server" ID="vsJumlah" />       
                            <asp:TextBox runat="server" ID="txtJLookup" CssClass="regular_text" Enabled="false"></asp:TextBox>                     
                            <%--<button class="small buttongo blue" 
                            onclick ="OpenLookup();return false;">...</button>--%>   
                            <button class="small buttongo blue" id="btnLookup" onclick="OpenJLookup('<%=ResolveClientUrl("~/Webform.LoanMnt/GeneralLookup/ucLookupTransMnt.aspx?vspay=vsPay&vsjumlah=vsJumlah&vsdesc=vsDesc&isPaymentRequest=1") %>' ,'DAFTAR DETAIL TRANSAKSI','<%= jlookupContent.ClientID %>', 'DoPostBack');return false;">
                            ...</button>                     
                            <asp:Button runat="server" ID="Jlookup" style="display:none"/>
                            <asp:RequiredFieldValidator ID="RFVtxtJLookup" runat="server"
                            ErrorMessage="Harap Pilih Traksaksi!" ControlToValidate="txtJLookup" Display="Dynamic"
                            InitialValue="Belum dipilih!" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>                
                <%--<div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DETAIL TRANSAKSI</h4>
                    </div>
                </div>--%>
                <%--<div class="form_box_uc">
                    <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
                </div>
                --%>
                <%--<div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>--%>
            </asp:Panel>
            <asp:Panel ID="pnlTrans" runat="server" Visible="False">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR DETAIL TRANSAKSI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgTransList" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowFooter="True" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">                                        
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:selectAllCheckbox(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DEPARTMEN">                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartmentID" runat="server" Visible="False" Text='<%#Container.DataItem("DepartmentID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TRANSAKSI">                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaymentAllocID" runat="server" Visible="False" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblPaymentAllocDesc" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KETERANGAN">                                        
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtKeterangan" CssClass="long_text"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvtxtKeterangan" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                                ControlToValidate="txtKeterangan" CssClass="validator_general"></asp:RequiredFieldValidator>
                                        </ItemTemplate>                                        
                                        <FooterTemplate>
                                            <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH">                                        
                                        <ItemTemplate>                                            
                                            <asp:TextBox runat="server" ID="txtAmountTrans" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);total(this);"
                                                onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                                onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign small_text">0</asp:TextBox>
                                            <asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 999999999999999"
                                                ControlToValidate="txtAmountTrans" MaximumValue="999999999999999" MinimumValue="0"
                                                Type="Currency" CssClass="validator_general"></asp:RangeValidator>
                                            <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                                ControlToValidate="txtAmountTrans" CssClass="validator_general"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="right"></FooterStyle>
                                        <FooterTemplate>                                            
                                            <asp:TextBox runat="server" ID="txtTotTransAmount" CssClass="numberAlign readonly_text small_text" Enabled="false">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Pembiayaan">
                                        <ItemTemplate>
                                             <asp:CheckBox ID="chkIsPotong" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:Button ID="ButtonDelete" runat="server" CausesValidation="False" Text="Delete"
                                    CssClass="small button gray"></asp:Button>
                                <button class="small buttongo blue" style="height:35px;" id="Lookup" onclick="OpenJLookup('<%=ResolveClientUrl("~/Webform.LoanMnt/GeneralLookup/ucLookupTransMnt.aspx?vspay=vsPay&vsjumlah=vsJumlah&vsdesc=vsDesc") %>' ,'DAFTAR DETAIL TRANSAKSI','<%= jlookupContent.ClientID %>', 'DoPostBack');return false;">
                             Add COA</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlButton" Visible="false">
            <div class="form_button">
                   <asp:Button ID="ButtonSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue" OnClientClick="return CheckDouble();">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonimbCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hdTotal" />   
    </form>
</body>
</html>
