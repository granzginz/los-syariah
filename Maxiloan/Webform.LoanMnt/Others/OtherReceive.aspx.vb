﻿#Region "Imports"
Imports System.Threading
Imports System.Math
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web

#End Region

Public Class OtherReceive
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oTrans As ucLookUpTransaction

    Dim temptotalTrans As Double
    Dim intLoopGrid As Integer
    Protected WithEvents oBranch As ucBranchAll
#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property TotTransAmount() As Double
        Get
            Return (CType(ViewState("TotTransAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotTransAmount") = Value
        End Set
    End Property

    Private Property ReceiveFrom() As String
        Get
            Return (CType(ViewState("ReceiveFrom"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReceiveFrom") = Value
        End Set
    End Property

    Private Property ReferenceNo() As String
        Get
            Return (CType(ViewState("ReferenceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReferenceNo") = Value
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return (CType(ViewState("BankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountName") = Value
        End Set
    End Property

    Private Property Notes() As String
        Get
            Return (CType(ViewState("Notes"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Notes") = Value
        End Set
    End Property

    Public Property BranchIDX() As String
        Get
            Return (CType(ViewState("BranchIDX"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDX") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.OtherReceive
    Private oController As New OtherReceiveController
    Private m_Controller As New DataUserControlController
#End Region
#Region "page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Request.QueryString("filekwitansi") <> "" Then
            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            Dim strFileLocation As String

            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"

            Response.Write("<script language = javascript>" & vbCrLf _
            & "var x = screen.width; " & vbCrLf _
            & "var y = screen.height; " & vbCrLf _
            & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
            & "</script>")
        End If

        If Not IsPostBack Then
            Me.FormID = "OTHERRECEIVE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.BranchID = Request.QueryString("branchid")

                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.hpsxml = "1"
                oCustomClass = oController.GetTable(oCustomClass, pstrFile)
                'oPaymentDetail.BindRekeningCashModule("B")
                oPaymentDetail.BindRekeningCashModule("", "ALL")
                oPaymentDetail.IsTitle = True
                oPaymentDetail.ValueDate = CDate(Me.BusinessDate).ToString("dd/MM/yyyy")
                oPaymentDetail.WithOutPrepaid = True
                oPaymentDetail.IsCanNegative = True
                oPaymentDetail.pnlConll = False
                DoBind()
                oTrans.IsCanNeagetive = True
                oTrans.IsPaymentReceive = "1"
                GetComboDepartement()
            End If
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind()
        If Me.IsHoBranch Then
            With oTrans
                .ProcessID = "OTHRCVNA"
                .IsAgreement = "0"
                ' .IsPettyCash = "0"
                .IsHOTransaction = ""
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        Else
            With oTrans
                .ProcessID = "OTHRCVNA"
                .IsAgreement = "0"
                ' .IsPettyCash = "0"
                .IsHOTransaction = "0"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With

        End If
    End Sub
#End Region

#Region "Add"
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Dim lBytEnd As Integer
            Dim lBytCounter As Integer
            Dim lblListTransNo As Label
            Dim lstrTrans As String
            Dim pStrFile As String


            If oTrans.Amount <= 0 Then
                ShowMessage(lblMessage, "Jumlah transaksi tidak boleh nol!", True)
                Exit Sub
            ElseIf oTrans.Description = "" Then

                ShowMessage(lblMessage, "Harap isi Keterangan", True)
                Exit Sub
            ElseIf oTrans.TransactionID = "" Then

                ShowMessage(lblMessage, "Harap Pilih Transaksi", True)
                Exit Sub
            End If

            If DateDiff(DateInterval.Day, ConvertDate2(oPaymentDetail.ValueDate), Me.BusinessDate) < 0 Then
                oPaymentDetail.WayOfPayment = "0"

                ShowMessage(lblMessage, "Tanggal Valuta Harus <= Tanggal hari ini", True)
                Exit Sub

            End If

            With oCustomClass
                pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = .LoginId
                .strConnection = GetConnectionString()
                .ReceivedFrom = oPaymentDetail.ReceivedFrom
                Me.ReceiveFrom = .ReceivedFrom
                .ReferenceNo = oPaymentDetail.ReferenceNo
                Me.ReferenceNo = txtNoJurnalBank.Text.Trim '.ReferenceNo

                'way of payment di set di sp
                '.WOP = oPaymentDetail.WayOfPayment
                '.WOP = "BA"
                'Me.WayOfPayment = .WOP

                .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)

                Me.ValueDate = .ValueDate
                .BankAccountID = oPaymentDetail.BankAccount
                Me.BankAccount = .BankAccountID
                .BankAccountName = oPaymentDetail.BankAccountName
                Me.BankAccountName = .BankAccountName

                '.AmountReceive = oPaymentDetail.AmountReceive
                .AmountReceive = oTrans.Amount
                Me.AmountReceive = .AmountReceive
                '.Notes = oPaymentDetail.Notes
                .Notes = oTrans.Description
                Me.Notes = .Notes
                If pnlList.Visible Then
                    .DepartementID = cboDepartmentID1.SelectedValue
                    .DepartmentName = cboDepartmentID1.SelectedItem.Text
                    cboDepartmentID1.SelectedIndex = 0
                Else
                    .DepartementID = cboDepartement.SelectedValue
                    .DepartmentName = cboDepartement.SelectedItem.Text
                    cboDepartement.SelectedIndex = 0
                End If
                .PaymentAllocationID = oTrans.TransactionID
                .PaymentAllocationdesc = oTrans.Transaction
                .AmountTrans = oTrans.Amount
                .Desc = oTrans.Description
                .flagdelete = "0"

            End With

            oCustomClass = oController.GetTable(oCustomClass, pStrFile)

            If Not oCustomClass.IsValidTrans Then

                ShowMessage(lblMessage, "Transaksi Sudah ada", True)
                Exit Sub
            Else
                DtgTransList.DataSource = oCustomClass.listReceive
                DtgTransList.DataBind()
                DtgTransList.Visible = True
                pnlTrans.Visible = True
                pnlList.Visible = False
                DoBindList()
                pnlLabel.Visible = True
            End If
            oTrans.TransactionID = ""
            oTrans.Transaction = ""
            oTrans.Amount = 0
            oTrans.Description = ""

            oBranch.BranchID = "0"

            oTrans.BindData()
            oPaymentDetail.WayOfPayment = oCustomClass.WOP
        End If
    End Sub
#End Region

#Region "DoBindList"
    Private Sub DoBindList()
        'lblRecFrom.Text = Me.ReceiveFrom
        'lblInRefNo.Text = Me.ReferenceNo

        'If Me.WayOfPayment = "CA" Then
        '    lblinWOP.Text = "Cash"
        'ElseIf Me.WayOfPayment = "BA" Then
        '    lblinWOP.Text = "Bank"
        'End If


        lblinValueDate.Text = Me.ValueDate.ToString("dd/MM/yyyy")
        lblinBankAccount.Text = Me.BankAccountName
        lblJurnalBank.Text = Me.ReferenceNo
        lblInAmountRec.Text = FormatNumber(Me.AmountReceive, 2)
        lblinNotes.Text = Me.Notes
        lblBranchIDFrom.Text = oBranch.BranchName
        Me.BranchIDX = oBranch.BranchID
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        'ButtonCancel.Attributes.Add("Onclick", "return DeleteConfirm()")        
        'Server.Transfer("OtherReceive.aspx")
        Response.Redirect("OtherReceive.aspx")
    End Sub
#End Region
#Region "Departement ID"
    Private Sub GetComboDepartement()
        Dim dtDepartement As New DataTable

        dtDepartement = m_Controller.GetDepartement(GetConnectionString)

        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"
        cboDepartement.Items.Insert(1, "None")
        cboDepartement.Items(1).Value = "-"

        cboDepartmentID1.DataTextField = "Name"
        cboDepartmentID1.DataValueField = "ID"
        cboDepartmentID1.DataSource = dtDepartement
        cboDepartmentID1.DataBind()

        cboDepartmentID1.Items.Insert(0, "Select One")
        cboDepartmentID1.Items(0).Value = "0"
        cboDepartmentID1.Items.Insert(1, "None")
        cboDepartmentID1.Items(1).Value = "-"
    End Sub
#End Region
#Region "Databound"
    Private Sub DtgTransList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgTransList.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim totalTransAmount As New Label
        Dim lTransAmount As New Label
        With oCustomClass

            If e.Item.ItemIndex >= 0 Then
                lTransAmount = CType(e.Item.FindControl("lblAmountTrans"), Label)

                temptotalTrans += CDbl(lTransAmount.Text.Replace(",", ""))
            End If

            If e.Item.ItemType = ListItemType.Footer Then

                totalTransAmount = CType(e.Item.FindControl("lblTotTransAmount"), Label)
                Me.TotTransAmount = temptotalTrans
                totalTransAmount.Text = FormatNumber(temptotalTrans.ToString, 2)
            End If
        End With
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
        DoBindList()
        pnlLabel.Visible = True
    End Sub
#End Region

#Region "ItemCommand"
    Private Sub DtgTransList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgTransList.ItemCommand
        Dim pstrFile As String
        If e.CommandName = "DELETE" Then
            With oCustomClass
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = Me.Session.SessionID + Me.Loginid
                .BranchId = Me.BranchID
                .flagdelete = "1"
                .NumTrans = DtgTransList.Items.Count
                .IndexDelete = CStr(e.Item.ItemIndex())
                .hpsxml = "2"
            End With
            oCustomClass = oController.GetTable(oCustomClass, pstrFile)
            DtgTransList.DataSource = oCustomClass.listReceive
            DtgTransList.DataBind()
            DtgTransList.Visible = True
            pnlTrans.Visible = True
            DoBindList()
            pnlLabel.Visible = True
        End If
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "OTHERRECEIVE"

        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim strIDTrans As String
            Dim strDescTrans As String
            Dim strAmountTrans As String
            Dim lblID As Label
            Dim lblDesc As Label
            Dim lblAmount As Label
            Dim lblDepartmentID As Label
            Dim strDepartmentID As String

            strIDTrans = ""
            strDescTrans = ""
            strAmountTrans = ""
            strDepartmentID = ""

            If DtgTransList.Items.Count > 0 Then
                Dim Ndttable As DataTable
                Dim pStrFile As String
                If Round(Me.TotTransAmount, 2) = Me.AmountReceive Then
                    For intLoopGrid = 0 To DtgTransList.Items.Count - 1

                        lblID = CType(DtgTransList.Items(intLoopGrid).Cells(0).FindControl("lblPaymentAllocID"), Label)
                        lblDesc = CType(DtgTransList.Items(intLoopGrid).Cells(0).FindControl("lblDesc"), Label)
                        lblAmount = CType(DtgTransList.Items(intLoopGrid).Cells(0).FindControl("lblAmountTrans"), Label)
                        lblDepartmentID = CType(DtgTransList.Items(intLoopGrid).Cells(0).FindControl("lblDepartmentID"), Label)

                        strIDTrans &= CStr(IIf(strIDTrans = "", "", ",")) & lblID.Text.Replace("'", "")
                        strDescTrans &= CStr(IIf(strDescTrans = "", "", ",")) & lblDesc.Text.Replace("'", "")
                        strAmountTrans &= CStr(IIf(strAmountTrans = "", "", ",")) & lblAmount.Text.Replace("'", "").Replace(",", "")
                        strDepartmentID &= CStr(IIf(strDepartmentID = "", "", ",")) & lblDepartmentID.Text.Replace("'", "")
                    Next

                    Ndttable = GetStructTransPrint()
                    With oCustomClass
                        

                        pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                        .LoginId = Me.Loginid
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .strConnection = GetConnectionString()
                        .ReceivedFrom = Me.ReceivedFrom
                        .BusinessDate = Me.BusinessDate
                        .ReferenceNo = Me.ReferenceNo
                        .BankAccountID = Me.BankAccount

                        'Override di SP
                        '.WOP = Me.WayOfPayment
                        .WOP = "-"

                        .Notes = Me.Notes
                        .CoyID = "001"
                        .AmountReceive = Me.AmountReceive
                        .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                        .NumTrans = DtgTransList.Items.Count
                        .strId = strIDTrans.Trim
                        .strDesc = strDescTrans.Trim
                        .strAmount = strAmountTrans.Trim
                        .BranchIDX = oBranch.BranchID
                        .DepartementID = strDepartmentID
                    End With

                    Dim customClass As Parameter.OtherReceive = oController.SaveTrans(oCustomClass, pStrFile)


                    If customClass.ErrorMessage <> "" Then
                        ShowMessage(lblMessage, customClass.ErrorMessage, True)
                    Else
                        oCustomClass.hpsxml = "1"
                        Try
                            Me.ReferenceNo = customClass.ReferenceNo
                            Dim row As DataRow = Ndttable.NewRow
                            row("ApplicationID") = ""
                            row("BranchID") = ""
                            row("DueDate") = Me.BusinessDate
                            row("ReceiptNo") = Me.ReferenceNo
                            row("ReferenceNo") = Me.ReferenceNo
                            row("Name") = strDescTrans.Trim
                            row("Address") = ""
                            row("addressonly") = ""
                            row("Phone") = ""
                            row("DebitAmount") = 0
                            row("PembayaranAmount") = strAmountTrans.Trim
                            row("DendaAmount") = 0
                            row("DepositAmount") = 0
                            row("TotalTerbilang") = TERBILANG(CDbl(strAmountTrans))
                            row("InsSeqNo") = ""
                            row("BranchCity") = Me.BranchName
                            row("TotalAmount") = strAmountTrans.Trim
                            Ndttable.Rows.Add(row)
                            BindReport(Ndttable)
                            oCustomClass = oController.GetTable(oCustomClass, pStrFile)
                            Response.Redirect("OtherReceive.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                        Catch ex As Exception
                            ShowMessage(lblMessage, ex.Message, True)
                            Response.Redirect("OtherReceive.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                        End Try
                    End If

                Else

                    ShowMessage(lblMessage, "Total Nilai harus sama dengan Jumlah diterima", True)
                End If
            Else

                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Exit Sub
            End If
        End If
    End Sub

#End Region

#Region "GetStructTrans"
    Public Function GetStructTrans() As DataTable
        Dim lObjDataTable As New DataTable("Receive")
        '  Dim dtTable As New DataTable("PDC")
        With lObjDataTable
            .Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
            .Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
            .Columns.Add("Description", System.Type.GetType("System.String"))
            .Columns.Add("Amount", System.Type.GetType("System.String"))
            .Columns.Add("DepartmentID", System.Type.GetType("System.String"))
            .Columns.Add("DepartmentName", System.Type.GetType("System.String"))
        End With

        Return lObjDataTable
    End Function

    Public Function GetStructTransPrint() As DataTable
        Dim lObjDataTable As New DataTable("Table")
        '  Dim dtTable As New DataTable("PDC")
        With lObjDataTable
            .Columns.Add("ApplicationID", System.Type.GetType("System.String"))
            .Columns.Add("BranchID", System.Type.GetType("System.String"))
            .Columns.Add("DueDate", System.Type.GetType("System.DateTime"))
            .Columns.Add("ReceiptNo", System.Type.GetType("System.String"))
            .Columns.Add("ReferenceNo", System.Type.GetType("System.String"))
            .Columns.Add("Name", System.Type.GetType("System.String"))
            .Columns.Add("Address", System.Type.GetType("System.String"))
            .Columns.Add("addressonly", System.Type.GetType("System.String"))
            .Columns.Add("Phone", System.Type.GetType("System.String"))
            .Columns.Add("DebitAmount", System.Type.GetType("System.Double"))
            .Columns.Add("PembayaranAmount", System.Type.GetType("System.Double"))
            .Columns.Add("DendaAmount", System.Type.GetType("System.Double"))
            .Columns.Add("DepositAmount", System.Type.GetType("System.Double"))
            .Columns.Add("TotalTerbilang", System.Type.GetType("System.String"))
            .Columns.Add("InsSeqNo", System.Type.GetType("System.String"))
            .Columns.Add("BranchCity", System.Type.GetType("System.String"))
            .Columns.Add("TotalAmount", System.Type.GetType("System.Double"))
        End With

        Return lObjDataTable
    End Function
#End Region

    Sub BindReport(dt As DataTable)
        Dim oData As New DataSet
        Dim objReport As RptKwitansiOther = New RptKwitansiOther
        oData.Tables.Add(dt)
        objReport.SetDataSource(oData)

        ''add cashier
        AddParamField(objReport, "CashierName", Me.FullName)
        AddParamField(objReport, "BusinessDate", Me.BusinessDate.ToString("dd") + " " + getMonthID() + " " + Me.BusinessDate.ToString("yyyy"))
        AddParamField(objReport, "BranchName", oData.Tables(0).Rows(0).Item("BranchCity").ToString)

        ''crvKwitansi.ReportSource = objReport 
        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        ''Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = pathReport("otherreceive", True) & Session.SessionID & ".pdf"

        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("OtherReceive.aspx?filekwitansi=" & pathReport("otherreceive", False) & Session.SessionID)

    End Sub

    Private Sub AddParamField(ByVal objReport As RptKwitansiOther, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub

    Private Function getMonthID() As String
        Dim rtn As String = ""

        If Month(Me.BusinessDate) = "1" Then
            rtn = "Januari"
        End If
        If Month(Me.BusinessDate) = "2" Then
            rtn = "Febuari"
        End If
        If Month(Me.BusinessDate) = "3" Then
            rtn = "Maret"
        End If
        If Month(Me.BusinessDate) = "4" Then
            rtn = "April"
        End If
        If Month(Me.BusinessDate) = "5" Then
            rtn = "Mai"
        End If
        If Month(Me.BusinessDate) = "6" Then
            rtn = "Juni"
        End If
        If Month(Me.BusinessDate) = "7" Then
            rtn = "Juli"
        End If
        If Month(Me.BusinessDate) = "8" Then
            rtn = "Augustus"
        End If
        If Month(Me.BusinessDate) = "9" Then
            rtn = "September"
        End If
        If Month(Me.BusinessDate) = "10" Then
            rtn = "Oktober"
        End If
        If Month(Me.BusinessDate) = "11" Then
            rtn = "November"
        End If
        If Month(Me.BusinessDate) = "12" Then
            rtn = "Desember"
        End If

        Return rtn

    End Function

    Private Function pathReport(ByVal group As String, ByVal dirXML As Boolean) As String
        Dim strDirectory As String = ""
        If dirXML = True Then
            strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += Me.sesBranchId.Replace("'", "") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += group & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If
        Else
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "/"
            strDirectory += Me.sesBranchId.Replace("'", "") & "/"
            strDirectory += group & "/"
        End If
        Return strDirectory
    End Function


    Public Function TERBILANG(x As Double) As String
        Dim tampung As Double
        Dim teks As String
        Dim bagian As String
        Dim i As Integer
        Dim tanda As Boolean

        Dim letak(5)
        letak(1) = "RIBU "
        letak(2) = "JUTA "
        letak(3) = "MILYAR "
        letak(4) = "TRILYUN "

        If (x < 0) Then
            TERBILANG = ""
            Exit Function
        End If

        If (x = 0) Then
            TERBILANG = "NOL"
            Exit Function
        End If

        If (x < 2000) Then
            tanda = True
        End If
        teks = ""

        If (x >= 1.0E+15) Then
            TERBILANG = "NILAI TERLALU BESAR"
            Exit Function
        End If

        For i = 4 To 1 Step -1
            tampung = Int(x / (10 ^ (3 * i)))
            If (tampung > 0) Then
                bagian = ratusan(tampung, tanda)
                teks = teks & bagian & letak(i)
            End If
            x = x - tampung * (10 ^ (3 * i))
        Next

        teks = teks & ratusan(x, False)
        TERBILANG = teks & " Rupiah"
    End Function

    Function ratusan(ByVal y As Double, ByVal flag As Boolean) As String
        Dim tmp As Double
        Dim bilang As String
        Dim bag As String
        Dim j As Integer

        Dim angka(9)
        angka(1) = "SE"
        angka(2) = "DUA "
        angka(3) = "TIGA "
        angka(4) = "EMPAT "
        angka(5) = "LIMA "
        angka(6) = "ENAM "
        angka(7) = "TUJUH "
        angka(8) = "DELAPAN "
        angka(9) = "SEMBILAN "

        Dim posisi(2)
        posisi(1) = "PULUH "
        posisi(2) = "RATUS "

        bilang = ""
        For j = 2 To 1 Step -1
            tmp = Int(y / (10 ^ j))
            If (tmp > 0) Then
                bag = angka(tmp)
                If (j = 1 And tmp = 1) Then
                    y = y - tmp * 10 ^ j
                    If (y >= 1) Then
                        posisi(j) = "BELAS "
                    Else
                        angka(y) = "SE"
                    End If
                    bilang = bilang & angka(y) & posisi(j)
                    ratusan = bilang
                    Exit Function
                Else
                    bilang = bilang & bag & posisi(j)
                End If
            End If
            y = y - tmp * 10 ^ j
        Next

        If (flag = False) Then
            angka(1) = "SATU "
        End If
        bilang = bilang & angka(y)
        ratusan = bilang
    End Function
  

End Class