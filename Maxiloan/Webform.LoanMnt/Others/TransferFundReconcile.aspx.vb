﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TransferFundReconcile
    Inherits Maxiloan.Webform.AccMntWebBased    
    Protected WithEvents oSearchby As UcSearchBy
    Protected WithEvents oBankAccount As UcBankAccountID
    Dim tempTotal As Double
    Dim tempTotal2 As Double
    Dim DtList As DataTable
    Dim j As Integer
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.TransferAccount
    Private oController As New TransferFundReconcileController

#End Region

#Region "Property"

    Private Property SelectAll2() As Double
        Get
            Return (CType(Viewstate("SelectAll"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("SelectAll") = Value
        End Set
    End Property

    Private Property inDtTble() As DataTable
        Get
            Return (CType(Viewstate("inDtTble"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("inDtTble") = Value
        End Set
    End Property

    Private Property strId() As String
        Get
            Return (CType(Viewstate("strId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strId") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CType(viewstate("BankID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "TRFACCRECON"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If

                oSearchBy.ListData = "TransferNo, Transfer No.-Description,Description-ReferenceNO,Reference No."
                oSearchBy.BindData()

                Me.SearchBy = ""
                Me.SortBy = ""
                oBankAccount.BankPurpose = ""
                oBankAccount.BankType = ""
                oBankAccount.IsAll = False
                oBankAccount.BindBankAccount()
            End If
        End If
        pnlDatagrid.Visible = False
        ltrBankID.Visible = False
    End Sub

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy

        End With

        oCustomClass = oController.TransferList(oCustomClass)

        DtUserList = oCustomClass.Listdata
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        DtgTrans.DataSource = DvUserList
        DtgTrans.DataBind()
        If DtgTrans.Items.Count > 0 Then
            '    imbNext.Visible = True
        Else
            ' imbNext.Visible = False
        End If

        pnlList.Visible = True
        pnlDatagrid.Visible = True
        pnlHasil.Visible = False

    End Sub

#End Region

    Private Sub Dtgtrans_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgTrans.ItemDataBound
        Dim lTransAmount As Label
        Dim totalTransAmount As Label
        Dim hyTransferNo As HyperLink

        If sessioninvalid() Then
            Exit Sub
        End If

        If e.Item.ItemIndex >= 0 Then
            lTransAmount = CType(e.Item.FindControl("lblAmount"), Label)
            tempTotal += CDbl(lTransAmount.Text)
            hyTransferNo = CType(e.Item.FindControl("hyTransferNo"), HyperLink)
            hyTransferNo.NavigateUrl = LinkToViewTransferFund(hyTransferNo.Text.Trim, "Finance", cboTransferType.SelectedItem.Value.Trim)

        End If


        If e.Item.ItemType = ListItemType.Footer Then
            totalTransAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            totalTransAmount.Text = FormatNumber(tempTotal.ToString, 2)
        End If
    End Sub

    Private Sub DtgHasil_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Dim lTransAmount2 As Label
        Dim totalTransAmount2 As Label

        If sessioninvalid() Then
            Exit Sub
        End If

        If e.Item.ItemIndex >= 0 Then
            lTransAmount2 = CType(e.Item.FindControl("lblAmount2"), Label)
            tempTotal2 += CDbl(lTransAmount2.Text)
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            totalTransAmount2 = CType(e.Item.FindControl("lblTotAmount2"), Label)
            totalTransAmount2.Text = FormatNumber(tempTotal2.ToString, 2)
        End If

    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        'Server.Transfer("TransferFundreconcile.aspx")
        Response.Redirect("TransferFundreconcile.aspx")
    End Sub

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            pnlDatagrid.Visible = True
            Me.SearchBy = ""
            If cboTransferType.SelectedItem.Value = "1" Then
                Me.SearchBy = Me.SearchBy & " tf.branchidfrom = '" & Me.sesBranchId.Replace("'", "") & "'  "
                Me.SearchBy = Me.SearchBy & "   and  tf.TransferType = 'TRFEC'  "
                If oBankAccount.BankAccountID <> "ALL" Then
                    Me.SearchBy = Me.SearchBy & " and  tf.bankaccountidfrom = '" & oBankAccount.BankAccountID & "'"
                End If
            Else
                If cboTransferType.SelectedItem.Value = "2" Then
                    Me.SearchBy = Me.SearchBy & " tf.branchidto = '" & Me.sesBranchId.Replace("'", "") & "'  "
                    Me.SearchBy = Me.SearchBy & "  and  tf.TransferType = 'TRFFD'  "
                    If oBankAccount.BankAccountID <> "ALL" Then
                        Me.SearchBy = Me.SearchBy & " and  tf.bankaccountidto = '" & oBankAccount.BankAccountID & "'"
                    End If
                End If
            End If
            If txtdate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and  tf.postingdate <= '" & ConvertDate(txtdate.Text) & "'  "
            End If
            If oSearchby.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchby.ValueID & " = '" & oSearchby.Text.Trim.Replace("'", "''") & "'"
            End If

            pnlDatagrid.Visible = True
            DtgTrans.Visible = True
            pnlList.Visible = True
            pnlHasil.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region "Select All"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim ChkSelectAll As CheckBox
        'Dim ChkSelect As CheckBox
        'Dim x As Integer
        'Dim chk As New CheckBox

        ''If Me.SelectAll2 = 0 Then
        ''    For x = 0 To DtgTrans.Items.Count - 1
        ''        ChkSelect = CType(DtgTrans.Items(x).FindControl("cbCheck"), CheckBox)
        ''        ChkSelect.Checked = True
        ''    Next
        ''    Me.SelectAll2 = 1
        ''Else
        ''    For x = 0 To DtgTrans.Items.Count - 1
        ''        ChkSelect = CType(DtgTrans.Items(x).FindControl("cbCheck"), CheckBox)
        ''        ChkSelect.Checked = False
        ''    Next
        ''    Me.SelectAll2 = 0
        ''End If
        'For x = 0 To DtgTrans.Items.Count - 1
        '    chk = CType(DtgTrans.Items(x).FindControl("cbCheck"), CheckBox)
        '    If CType(sender, CheckBox).Checked Then
        '        chk.Checked = True
        '        Me.SelectAll2 = 1
        '    Else
        '        chk.Checked = False
        '        Me.SelectAll2 = 0
        '    End If
        'Next
    End Sub
#End Region


    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        'Server.Transfer("TransferFundReconcile.aspx")
        Response.Redirect("TransferFundReconcile.aspx")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .bankAccountFrom = ltrBankID.Text.Trim
                .TotalRecord = 1
                .TransferNO = LblTransferNo.Text
                .Desc = lblDesc.Text
                .AmountTransfer = LblAmountTransfer.Text
                .referenceNo = txtReffNo.Text
                .valueDate = ConvertDate2(lblvalueDate.Text)
                .CoyID = Me.SesCompanyID
                .strID = Me.strId

            End With
            oCustomClass = oController.TransferSave(oCustomClass)
            If oCustomClass.strError <> "" Then
                ShowMessage(lblMessage, oCustomClass.strError, True)                
            Else
                'Response.Redirect("TransferFundReconcile.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                ShowMessage(lblMessage, "Data saved!", False)
                pnlHasil.Visible = False
                pnlDatagrid.Visible = False
                pnlList.Visible = True
            End If
        End If
    End Sub

    Private Sub DtgTrans_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgTrans.ItemCommand
        Dim CmdWhere As String = ""
        Dim NlblTransferNo As New HyperLink
        Dim NBranchID As New Label
        Dim NBankAccountID As New Label
        Me.CmdWhere = ""
        Me.BranchID = ""

        Select Case e.CommandName
            Case "Reconcile"
                NBankAccountID = CType(DtgTrans.Items(e.Item.ItemIndex).FindControl("lblBankAccountID"), Label)
                NBranchID = CType(DtgTrans.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)
                NlblTransferNo = CType(DtgTrans.Items(e.Item.ItemIndex).FindControl("HyTransferNo"), HyperLink)
                CmdWhere = CmdWhere + "TransferFundTransaction.BranchId = '" & NBranchID.Text & "' And  TransferFundTransaction.TransferNo = '" & NlblTransferNo.Text & "'"
                Me.CmdWhere = CmdWhere
                ltrBankID.Text = NBankAccountID.Text
                BindDataLabel()

                pnlHasil.Visible = True
                pnlDatagrid.Visible = False
                pnlList.Visible = False

        End Select
    End Sub

    Sub BindDataLabel()
        Dim oPCReimburse As New Parameter.TransferFundInquiry
        Dim m_controller As New TransferFundInquiryController
        Dim oData As New DataTable
        With oPCReimburse
            .strConnection = getconnectionstring()
            .WhereCond = Me.CmdWhere
        End With
        oPCReimburse = m_controller.GetViewTransferFund(oPCReimburse)
        If Not oPCReimburse Is Nothing Then
            oData = oPCReimburse.ListData
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
    End Sub
    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)
        lblBranch.Text = oRow.Item(0).ToString.Trim
        LblTransferNo.Text = oRow.Item(1).ToString.Trim
        lblvalueDate.Text = oRow.Item(2).ToString.Trim
        LblTransferType.Text = oRow.Item(3).ToString.Trim
        LblPostingDate.Text = oRow.Item(4).ToString.Trim
        lblReffNo.Text = oRow.Item(5).ToString.Trim
        LblAmountTransfer.Text = FormatNumber(oRow.Item(6).ToString.Trim, 2)
        lblDesc.Text = oRow.Item(7).ToString.Trim
        LblBranchTransferFrom.Text = oRow.Item(8).ToString.Trim
        lblBankAccountTransferForm.Text = oRow.Item(9).ToString.Trim
        If oRow.Item(10).ToString.Trim = "" Then lblVoucherNoTransferFrom.Text = "-" Else lblVoucherNoTransferFrom.Text = oRow.Item(10).ToString.Trim
        lblBGNo.Text = oRow.Item(11).ToString.Trim
        lblBGDueDate.Text = oRow.Item(12).ToString.Trim
        lblBranchTransferTo.Text = oRow.Item(13).ToString.Trim
        lblBankAccountTransferTo.Text = oRow.Item(14).ToString.Trim
        lblVoucherNoTransferTo.Text = oRow.Item(15).ToString.Trim
    End Sub
    Function LinkToViewTransferFund(ByVal strTransferNo As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewTransferFund('" & strStyle & "','" & strTransferNo & "','" & strBranch & "')"
    End Function

End Class