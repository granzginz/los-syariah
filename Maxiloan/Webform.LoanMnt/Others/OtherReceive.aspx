﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtherReceive.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.OtherReceive" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentDetail" Src="../../Webform.UserController/UcPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OtherReceive</title>
     <script  type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
   
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
 
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>

    <script type="text/javascript">
        var submit = 0;
        function CheckDouble() {
            if (++submit > 1) {
                alert('This sometimes takes a few seconds - please be patient.');
                return false;
            }
        }
 </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
   <%-- <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updatePanel1" UpdateMode="Always">
        <ContentTemplate>--%>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENERIMAAN LAINNYA
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_uc">
            <uc1:ucpaymentdetail id="oPaymentDetail" runat="server"></uc1:ucpaymentdetail>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Departemen</label>
                <asp:DropDownList ID="cboDepartmentID1" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" InitialValue="0"
                    Display="Dynamic" ControlToValidate="cboDepartmentID1" ErrorMessage="Harap dipilih Departemen"
                    Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jurnal Bank</label>
                <asp:TextBox ID="txtNoJurnalBank" runat="server" Width="300px"></asp:TextBox>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlLabel" runat="server" Visible="false">
        <%--<div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Terima Dari</label>
                    <asp:Label ID="lblRecFrom" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        No Bukti Kas Masuk</label>
                    <asp:Label ID="lblInRefNo" runat="server"></asp:Label>
                </div>
            </div>
        </div>--%>
        <div class="form_box">
            <%--<div class="form_single">--%>
                <%--<div class="form_left">
                    <label>
                        Cara Pembayaran</label>
                    <asp:Label ID="lblinWOP" runat="server"></asp:Label>
                </div>--%>
                <div class="form_single">
                    <label>
                        Tanggal Valuta</label>
                    <asp:Label ID="lblinValueDate" runat="server"></asp:Label>
                </div>
            <%--</div>--%>
        </div>
        <div class="form_box">
            <%--<div class="form_single">--%>
                <div class="form_left">
                    <label>
                        Rekening Bank</label>
                    <asp:Label ID="lblinBankAccount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Terima</label>
                    <asp:Label ID="lblInAmountRec" runat="server"></asp:Label>
                </div>
            <%--</div>--%>
        </div>
        <div class="form_box">
                <div class="form_left">
                    <label>
                        Jurnal Bank</label>
                    <asp:Label ID="lblJurnalBank" runat="server"></asp:Label>
                </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:Label ID="lblinNotes" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <%--<div class="form_single">--%>
                <div class="form_left">
                    <label>
                        Cabang</label>
                    <asp:Label ID="lblBranchIDFrom" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label class="label_req">
                        Departemen</label>
                    <asp:DropDownList ID="cboDepartement" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                        Display="Dynamic" ControlToValidate="cboDepartement" ErrorMessage="Harap dipilih Departemen"
                        Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            <%--</div>--%>
        </div>
    </asp:Panel>

    <div class="form_title">
        <div class="form_single">
            <h4>
                TRANSAKSI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <uc1:uclookuptransaction id="oTrans" runat="server" />
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
            CausesValidation="True"></asp:Button>
    </div>
    <asp:Panel ID="pnlTrans" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgTransList" runat="server" Visible="False" Width="100%" AutoGenerateColumns="False"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="DEPARTMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentID" runat="server" Text='<%#Container.DataItem("DepartmentID")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentAllocID" runat="server" Text='<%#Container.DataItem("PaymentAllocationID")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblPaymentAllocDesc" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountTrans" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotTransAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue" OnClientClick="return CheckDouble();">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
<%--
    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
