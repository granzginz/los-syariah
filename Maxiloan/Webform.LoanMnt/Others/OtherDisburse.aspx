﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtherDisburse.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.OtherDisburse" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentDetailD" Src="../../Webform.UserController/UcPaymentDetailD.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Other Disburse</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function OpenTransaction(pTransaction, pDescription, pIsAgreement, pIsPettyCash, pIsHOTransaction, pProcessID, pStyle, pIsPaymentReceive) {
            var AppInfo = window.location.pathname;
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var ServerName = window.location.protocol + '//' + window.location.host + '/';
            window.open(ServerName + App + '/General/LookUpTransaction.aspx?Transaction=' + pTransaction + '&Description=' + pDescription + '&IsAgreeMent=' + pIsAgreement + '&IsPettyCash=' + pIsPettyCash + '&IsHOTransaction=' + pIsHOTransaction + '&ProcessID=' + pProcessID + '&Style=' + pStyle + '&IsPaymentReceive=' + pIsPaymentReceive, 'TransactionLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
        }
    </script>
    <script language="JavaScript" type="text/javascript">
        var hidBankAccount;
        function WOPChange(pCmbOfPayment, pBankAccount, pHidDetail, pAmountReceive, itemArray) {
            hidBankAccount = eval('document.forms[0].' + pHidDetail);
            var a = eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value;
            if (a == 'CP') {
                for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                    eval('document.forms[0].' + pBankAccount).options[i] = null;
                }
                eval('document.forms[0].' + pAmountReceive).disabled = true;
                eval('document.forms[0].' + pAmountReceive).value = 0;
                eval('document.forms[0].' + pBankAccount).disabled = true;
                hidBankAccount.value = '';
            }
            else {
                eval('document.forms[0].' + pAmountReceive).disabled = false;
                eval('document.forms[0].' + pBankAccount).disabled = false;
                var i, j;
                for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                    eval('document.forms[0].' + pBankAccount).options[i] = null
                };
                j = 0;
                eval('document.forms[0].' + pBankAccount).options[j++] = new Option('Select One', '0');
                //		eval('document.forms[0].' + pBankAccount).options[j++].value = '0';
                //                var a;
                if (itemArray != null) {
                    for (i = 0; i < itemArray.length; i++) {
                        eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
                    };
                    eval('document.forms[0].' + pBankAccount).selected = true;
                };
            };
        }

        function setCboDetlVal(l) {
            hidBankAccount.value = l;
        }
    </script>

     <script type="text/javascript">
         var submit = 0;
         function CheckDouble() {
             if (++submit > 1) {
                 alert('This sometimes takes a few seconds - please be patient.');
                 return false;
             }
         }
 </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PEMBAYARAN LAINNYA
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlawal" runat="server" Visible="True">
                <div class="form_box_uc">
                    <uc1:ucpaymentdetaild id="oPaymentDetail" runat="server"></uc1:ucpaymentdetaild>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlView" runat="server" Visible="False">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PEMBAYARAN LAINNYA
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Penerima</label>
                        <asp:Label ID="lblRecipientName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cara Pembayaran</label>
                        <asp:Label ID="lblWOP" runat="server"></asp:Label>
                    </div>
                </div>
                <asp:Panel runat="server" ID="pnlGT" Visible="false">
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Jenis Transfer
                            </label>
                            <asp:Label runat="server" ID="lblJenisTransfer"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                BENEFICIARY BANK ACCOUNT</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Bank</label>
                            <asp:Label runat="server" ID="lblNamaBank"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Cabank</label>
                            <asp:Label runat="server" ID="lblNamaCabang"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                No. Rekening</label>
                            <asp:Label runat="server" ID="lblNoRekening"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Rekening</label>
                            <asp:Label runat="server" ID="lblNamaRekening"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            TRANSAKSI PEMBAYARAN</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No. Bukti Kas Keluar</label>
                        <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Valuta</label>
                        <asp:Label ID="lblValueDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah DiBayar</label>
                        <asp:Label ID="lblAmountRec" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Bank</label>
                        <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Catatan</label>
                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Departemen</label>
                        <asp:DropDownList ID="cboDepartement" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Visible="true"
                            ErrorMessage="Harap Pilih Departemen" ControlToValidate="cboDepartement" Display="Dynamic"
                            InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DETAIL TRANSAKSI</h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlTrans" runat="server" Visible="False">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DETAIL TRANSAKSI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgTransList" runat="server" Visible="False" Width="100%" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="10%" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                                CommandName="DELETE"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DEPARTMEN">
                                        <ItemStyle Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartmentID" runat="server" Visible="False" Text='<%#Container.DataItem("DepartmentID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TRANSAKSI">
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaymentAllocID" runat="server" Visible="False" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblPaymentAllocDesc" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesc" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmountTrans" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotTransAmount" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue" OnClientClick="return CheckDouble();">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonimbCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
