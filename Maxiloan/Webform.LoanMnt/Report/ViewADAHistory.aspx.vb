
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ViewADAHistory
    Inherits maxiloan.webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CrystalReportViewer1 As CrystalDecisions.Web.CrystalReportViewer
    Protected WithEvents imbBack As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Property"

   
    Private Property DateFrom() As String
        Get
            Return CType(viewstate("DateFrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DateFrom") = Value
        End Set
    End Property
    Private Property DateTo() As String
        Get
            Return CType(viewstate("DateTo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DateTo") = Value
        End Set
    End Property
#End Region

#Region "Deklarasi dan konstanta"
    Private m_controller As New GeneralPagingController
    Private oCustomClass As New Parameter.GeneralPaging
    Private dtAPDate As Date
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("ADAHistory")
        Me.BranchID = cookie.Values("BranchID")
        Me.BranchName = cookie.Values("BranchName")
        Me.DateFrom = cookie.Values("DateFrom")
        Me.DateTo = cookie.Values("DateTo")
    End Sub

    Sub BindReport()
        GetCookies()
        Dim dsAssetDocRep As New DataSet
        Dim report As New rptADAHistory

        Dim strStatusDateFrom As String = ""
        Dim strStatusDateTo As String = ""
        Dim dtStatusDateFrom As Date
        Dim dtStatusDateTo As Date

        With oCustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .DateFrom = ConvertDate2(Me.DateFrom)
            .DateTo = ConvertDate2(Me.DateTo)
            .SpName = "spSelectADA"
        End With
        oCustomClass = m_controller.GetReportWithBranchAndTwoPeriod(oCustomClass)

        dsAssetDocRep = oCustomClass.ListDataReport
        Try
            report.SetDataSource(dsAssetDocRep.Tables(0))
        Catch ex As Exception
            'Response.Write(ex.Message.ToString)
        End Try
        CrystalReportViewer1.ReportSource = report
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.EnableDrillDown = False
        CrystalReportViewer1.DisplayGroupTree = False
        CrystalReportViewer1.DataBind()

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = report.DataDefinition.ParameterFields("PrintedBy")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Loginid
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = report.DataDefinition.ParameterFields("PrgId")
        discrete = New ParameterDiscreteValue
        discrete.Value = "RptADAHistory"
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = report.DataDefinition.ParameterFields("DateFrom")
        discrete = New ParameterDiscreteValue
        discrete.Value = ConvertDate2(Me.DateFrom)
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = report.DataDefinition.ParameterFields("DateTo")
        discrete = New ParameterDiscreteValue
        discrete.Value = ConvertDate2(Me.DateTo)
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = report.DataDefinition.ParameterFields("sesbranch")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BranchName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = report.DataDefinition.ParameterFields("companyname")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.sesCompanyName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnback.Click
        Response.Redirect("ADAHistory.aspx")
    End Sub
End Class
