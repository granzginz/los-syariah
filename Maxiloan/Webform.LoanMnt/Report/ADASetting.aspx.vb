﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class ADASetting
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property cmdWhere() As String
        Get
            Return CStr(ViewState("cmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("cmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

#End Region

#Region "Private Sub"

    Private Sub EditBind()
        'Bind Bucket From
        txtBucket1From.Text = lblBucket1From.Text
        txtBucket2From.Text = lblBucket2From.Text
        txtBucket3From.Text = lblBucket3From.Text
        txtBucket4From.Text = lblBucket4From.Text
        txtBucket5From.Text = lblBucket5From.Text
        txtBucket6From.Text = lblBucket6From.Text

        txtBucket7From.Text = lblBucket7From.Text
        txtBucket8From.Text = lblBucket8From.Text
        txtBucket9From.Text = lblBucket9From.Text
        txtBucket10From.Text = lblBucket10From.Text

        'Bind Bucket To
        txtBucket1To.Text = lblBucket1To.Text
        txtBucket2To.Text = lblBucket2To.Text
        txtBucket3To.Text = lblBucket3To.Text
        txtBucket4To.Text = lblBucket4To.Text
        txtBucket5To.Text = lblBucket5To.Text
        txtBucket6To.Text = lblBucket6To.Text

        txtBucket7To.Text = lblBucket7To.Text
        txtBucket8To.Text = lblBucket8To.Text
        txtBucket9To.Text = lblBucket9To.Text
        txtBucket10To.Text = lblBucket10To.Text

        'Bind Bucket Rate
        txtBucket1Rate.Text = lblBucket1Rate.Text
        txtBucket2Rate.Text = lblBucket2Rate.Text
        txtBucket3Rate.Text = lblBucket3Rate.Text
        txtBucket4Rate.Text = lblBucket4Rate.Text
        txtBucket5Rate.Text = lblBucket5Rate.Text
        txtBucket6Rate.Text = lblBucket6Rate.Text

        txtBucket7Rate.Text = lblBucket7Rate.Text
        txtBucket8Rate.Text = lblBucket8Rate.Text
        txtBucket9Rate.Text = lblBucket9Rate.Text
        txtBucket10Rate.Text = lblBucket10Rate.Text


        txtAida1Rate.Text = lblAida1Rate.Text
        txtAida2Rate.Text = lblAida2Rate.Text
    End Sub

    Private Sub GetData()
        Dim dtsADA As DataTable
        Dim cADA As New GeneralPagingController
        Dim oADA As New Parameter.GeneralPaging

        With oADA
            .strConnection = GetConnectionString()
            .SpName = "spGetADASetting"
        End With

        dtsADA = cADA.GetRecordWithoutParameterAll(oADA)

        If dtsADA.Rows.Count > 0 Then
            With dtsADA.Rows(0)
                'Bind Bucket From
                lblBucket1From.Text = .Item("Bucket1_from").ToString
                lblBucket2From.Text = .Item("Bucket2_from").ToString
                lblBucket3From.Text = .Item("Bucket3_from").ToString
                lblBucket4From.Text = .Item("Bucket4_from").ToString
                lblBucket5From.Text = .Item("Bucket5_from").ToString
                lblBucket6From.Text = .Item("Bucket6_from").ToString

                lblBucket7From.Text = .Item("Bucket7_from").ToString
                lblBucket8From.Text = .Item("Bucket8_from").ToString
                lblBucket9From.Text = .Item("Bucket9_from").ToString
                lblBucket10From.Text = .Item("Bucket10_from").ToString

                'Bind Bucket To
                lblBucket1To.Text = .Item("Bucket1_to").ToString
                lblBucket2To.Text = .Item("Bucket2_to").ToString
                lblBucket3To.Text = .Item("Bucket3_to").ToString
                lblBucket4To.Text = .Item("Bucket4_to").ToString
                lblBucket5To.Text = .Item("Bucket5_to").ToString
                lblBucket6To.Text = .Item("Bucket6_to").ToString

                lblBucket7To.Text = .Item("Bucket7_to").ToString
                lblBucket8To.Text = .Item("Bucket8_to").ToString
                lblBucket9To.Text = .Item("Bucket9_to").ToString
                lblBucket10To.Text = .Item("Bucket10_to").ToString

                'Bind Bucket Rate
                lblBucket1Rate.Text = .Item("Bucket1_Percentage").ToString
                lblBucket2Rate.Text = .Item("Bucket2_Percentage").ToString
                lblBucket3Rate.Text = .Item("Bucket3_Percentage").ToString
                lblBucket4Rate.Text = .Item("Bucket4_Percentage").ToString
                lblBucket5Rate.Text = .Item("Bucket5_Percentage").ToString
                lblBucket6Rate.Text = .Item("Bucket6_Percentage").ToString

                lblBucket7Rate.Text = .Item("Bucket7_Percentage").ToString
                lblBucket8Rate.Text = .Item("Bucket8_Percentage").ToString
                lblBucket9Rate.Text = .Item("Bucket9_Percentage").ToString
                lblBucket10Rate.Text = .Item("Bucket10_Percentage").ToString

                lblAida1Rate.Text = .Item("AydaRate").ToString
                lblAida2Rate.Text = .Item("AYDARate1").ToString
            End With
        End If
       

    End Sub

    Private Sub DefaultPanel()
        PnlTop.Visible = True
        pnlBottom.Visible = False
    End Sub

    Private Sub EditPanel()
        PnlTop.Visible = False
        pnlBottom.Visible = True
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Me.FormID = "ADASETTING"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            GetData()
            DefaultPanel()
            lblMessage.Text = ""
            lblMessage.Visible = False
        End If
    End Sub

#Region " even Handlers "

    Private Sub imgbtnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditBind()
        EditPanel()
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Private Sub imgbtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnCancel.Click
        DefaultPanel()
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Private Sub imgbtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbtnSave.Click
        Dim oCustomClass As New Parameter.ADASetting
        Dim cADA As New ADASettingController
        Dim chkResult As Boolean

        With oCustomClass
            .SPName = "spUpdateADASetting"
            .strConnection = GetConnectionString()
            'BucketFrom
            .Bucket1From = CType(txtBucket1From.Text, Integer)
            .Bucket2From = CType(txtBucket2From.Text, Integer)
            .Bucket3From = CType(txtBucket3From.Text, Integer)
            .Bucket4From = CType(txtBucket4From.Text, Integer)
            .Bucket5From = CType(txtBucket5From.Text, Integer)
            .Bucket6From = CType(txtBucket6From.Text, Integer)

            .Bucket7From = CType(txtBucket7From.Text, Integer)
            .Bucket8From = CType(txtBucket8From.Text, Integer)
            .Bucket9From = CType(txtBucket9From.Text, Integer)
            .Bucket10From = CType(txtBucket10From.Text, Integer)


            'BucketTo
            .Bucket1To = CType(txtBucket1To.Text, Integer)
            .Bucket2To = CType(txtBucket2To.Text, Integer)
            .Bucket3To = CType(txtBucket3To.Text, Integer)
            .Bucket4To = CType(txtBucket4To.Text, Integer)
            .Bucket5To = CType(txtBucket5To.Text, Integer)
            .Bucket6To = CType(txtBucket6To.Text, Integer)

            .Bucket7To = CType(txtBucket7To.Text, Integer)
            .Bucket8To = CType(txtBucket8To.Text, Integer)
            .Bucket9To = CType(txtBucket9To.Text, Integer)
            .Bucket10To = CType(txtBucket10To.Text, Integer)
            'BucketRate
            .Bucket1Rate = CDec(txtBucket1Rate.Text)
            .Bucket2Rate = CDec(txtBucket2Rate.Text)
            .Bucket3Rate = CDec(txtBucket3Rate.Text)
            .Bucket4Rate = CDec(txtBucket4Rate.Text)
            .Bucket5Rate = CDec(txtBucket5Rate.Text)
            .Bucket6Rate = CDec(txtBucket6Rate.Text)

            .Bucket7Rate = CDec(txtBucket7Rate.Text)
            .Bucket8Rate = CDec(txtBucket8Rate.Text)
            .Bucket9Rate = CDec(txtBucket9Rate.Text)
            .Bucket10Rate = CDec(txtBucket10Rate.Text)

            .Ayda1Rate = CDec(txtAida1Rate.Text)
            .Ayda2Rate = CDec(txtAida2Rate.Text)
        End With
        chkResult = cADA.UpdateADASetting(oCustomClass)
        If chkResult = True Then
            ShowMessage(lblMessage, "Update Succeeded", False)
        Else
            ShowMessage(lblMessage, "Update Failed", True)
        End If
        GetData()
        DefaultPanel()
    End Sub

    'Private Sub imbtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbtnPrint.Click
    '    If CheckFeature(Me.Loginid, Me.FormID, "PRINT", "MAXILOAN") Then
    '        Me.SearchBy = ""
    '        Dim cookie As HttpCookie = Request.Cookies("ADASettingRpt")
    '        If Not cookie Is Nothing Then
    '            cookie.Values("ReportType") = "ADASettingRpt"
    '            cookie.Values("cmdwhere") = Me.SearchBy
    '            cookie.Values("LoginID") = Me.Loginid
    '            Response.AppendCookie(cookie)
    '        Else
    '            Dim cookieNew As New HttpCookie("ADASettingRpt")
    '            cookieNew.Values.Add("ReportType", "ADASettingRpt")
    '            cookieNew.Values.Add("cmdwhere", Me.SearchBy)
    '            cookieNew.Values.Add("LoginID", Me.Loginid)
    '            Response.AppendCookie(cookieNew)
    '        End If
    '        Response.Redirect("ADASettingViewer.aspx")
    '    End If
    'End Sub

#End Region




End Class