﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ADASetting.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ADASetting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Incentive Per Agreement</title>
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="PnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    Allowance For Doubful Account Setting</h3>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_quarter">
                <h5>
                    BUCKET
                </h5>
            </div>
            <div class="form_quarter">
                <h5>
                    OVER DUE FROM (DAYS)
                </h5>
            </div>
            <div class="form_quarter">
                <h5>
                    OVER DUE TO (DAYS)
                </h5>
            </div>
            <div class="form_quarter">
                <h5>
                    ALLOWANCE RATE (%)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    1</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket1From" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <label>
                    <asp:Label ID="lblBucket1To" runat="server"></asp:Label>
                </label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket1Rate" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    2</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket2From" runat="server"></asp:Label></div>
            <div class="form_quarter">
                <label>
                    <asp:Label ID="lblBucket2To" runat="server"></asp:Label>
                </label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket2Rate" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    3</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket3From" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <label>
                    <asp:Label ID="lblBucket3To" runat="server"></asp:Label></label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket3Rate" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    4</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket4From" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <label>
                    <asp:Label ID="lblBucket4To" runat="server"></asp:Label></label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket4Rate" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    5</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket5From" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <label>
                    <asp:Label ID="lblBucket5To" runat="server"></asp:Label></label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket5Rate" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    6</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket6From" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <label>
                    <asp:Label ID="lblBucket6To" runat="server"></asp:Label></label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblBucket6Rate" runat="server"></asp:Label>%
            </div>
        </div>


        <div class="form_box">
            <div class="form_quarter">
                <label>7</label>
            </div>
            <div class="form_quarter"><asp:Label ID="lblBucket7From" runat="server"></asp:Label> </div>
            <div class="form_quarter"><label><asp:Label ID="lblBucket7To" runat="server"></asp:Label></label></div>
            <div class="form_quarter"><asp:Label ID="lblBucket7Rate" runat="server"></asp:Label>%</div>
        </div>


         <div class="form_box">
            <div class="form_quarter">
                <label>8</label>
            </div>
            <div class="form_quarter"><asp:Label ID="lblBucket8From" runat="server"></asp:Label> </div>
            <div class="form_quarter"><label><asp:Label ID="lblBucket8To" runat="server"></asp:Label></label></div>
            <div class="form_quarter"><asp:Label ID="lblBucket8Rate" runat="server"></asp:Label>%</div>
        </div>

         <div class="form_box">
            <div class="form_quarter">
                <label>9</label>
            </div>
            <div class="form_quarter"><asp:Label ID="lblBucket9From" runat="server"></asp:Label> </div>
            <div class="form_quarter"><label><asp:Label ID="lblBucket9To" runat="server"></asp:Label></label></div>
            <div class="form_quarter"><asp:Label ID="lblBucket9Rate" runat="server"></asp:Label>%</div>
        </div>

           <div class="form_box">
            <div class="form_quarter">
                <label>10</label>
            </div>
            <div class="form_quarter"><asp:Label ID="lblBucket10From" runat="server"></asp:Label> </div>
            <div class="form_quarter"><label><asp:Label ID="lblBucket10To" runat="server"></asp:Label></label></div>
            <div class="form_quarter"><asp:Label ID="lblBucket10Rate" runat="server"></asp:Label>%</div>
        </div>

          <div class="form_box">
            <div class="form_quarter">
                <label>Ayda Allowance Rate 1</label>
            </div>
            <div class="form_quarter"><asp:Label ID="Label1" runat="server"></asp:Label> </div>
            <div class="form_quarter"><label><asp:Label ID="Label2" runat="server"></asp:Label></label></div>
            <div class="form_quarter"><asp:Label ID="lblAida1Rate" runat="server"></asp:Label>%</div>
        </div>

        <div class="form_box">
            <div class="form_quarter">
                <label>Ayda Allowance Rate 2</label>
            </div>
            <div class="form_quarter"><asp:Label ID="Label3" runat="server"></asp:Label> </div>
            <div class="form_quarter"><label><asp:Label ID="Label4" runat="server"></asp:Label></label></div>
            <div class="form_quarter"><asp:Label ID="lblAida2Rate" runat="server"></asp:Label>%</div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="small button blue" />
           <%-- <asp:Button ID="imbtnPrint" runat="server" Text="Print" CssClass="small button blue" />--%>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlBottom">
        <div class="form_box_title">
            <div class="form_quarter">
                <h5>
                    BUCKET
                </h5>
            </div>
            <div class="form_quarter">
                <h5>
                    OVER DUE FROM (DAYS)
                </h5>
            </div>
            <div class="form_quarter">
                <h5>
                    OVER DUE TO (DAYS)
                </h5>
            </div>
            <div class="form_quarter">
                <h5>
                    ALLOWANCE RATE (%)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    1</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket1From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket1From" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket1From"></asp:RequiredFieldValidator></div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket1To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket1To" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket1To"></asp:RequiredFieldValidator></div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket1Rate" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket1Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket1Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    2</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket2From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket2From" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket2From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket2To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket2To" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket2To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket2Rate" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket2Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket2Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    3</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket3From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket3From" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket3From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket3To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket3To" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket3To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket3Rate" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket3Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket3Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    4</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket4From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket4from" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket4From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket4To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket4To" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket4To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket4Rate" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket4Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket4Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    5</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket5From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket5From" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket5From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket5To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket5To" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket5To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket5Rate" runat="server" ErrorMessage="Please Fill Field"
                    ControlToValidate="txtBucket5Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket5Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>
        
        <div class="form_box">
            <div class="form_quarter">
                <label> 6</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket6From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket6From" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket6From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket6To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket6To" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket6To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket6Rate" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket6Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket6Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>

        <div class="form_box">
            <div class="form_quarter">
                <label> 7</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket7From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket7From" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket7From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket7To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket7To" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket7To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket7Rate" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket7Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket7Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>

        <div class="form_box">
            <div class="form_quarter">
                <label> 8</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket8From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket8From" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket8From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket8To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket8To" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket7To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket8Rate" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket8Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket8Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>

        <div class="form_box">
            <div class="form_quarter">
                <label> 9</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket9From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket9From" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket9From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket9To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="rfvBucket9To" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket9To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="rfvBucket9Rate" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket9Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket9Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>

          <div class="form_box">
            <div class="form_quarter">
                <label> 10</label>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket10From" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket10From"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:TextBox ID="txtBucket10To" runat="server" Width="53px"></asp:TextBox>days
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket10To"></asp:RequiredFieldValidator>
            </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtBucket10Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBucket10Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>


         <div class="form_box">
            <div class="form_quarter">
                <label> Ayda Allowance Rate 1</label>
            </div>
            <div class="form_quarter"><span />  </div>
            <div class="form_quarter"><span />  </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtAida1Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtAida1Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>

         <div class="form_box">
            <div class="form_quarter">
                <label> Ayda Allowance Rate 2</label>
            </div>
            <div class="form_quarter"><span />  </div>
            <div class="form_quarter"><span />  </div>
            <div class="form_quarter">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Fill Field" ControlToValidate="txtAida2Rate"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtAida2Rate" runat="server" Width="79px"></asp:TextBox>%
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgbtnSave" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="true"></asp:Button>
            <asp:Button ID="imgbtnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
