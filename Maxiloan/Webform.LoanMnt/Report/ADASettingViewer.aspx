<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ADASettingViewer.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ADASettingViewer" %>

<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>RptInctvPerAgreement</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
    <div class="form_button">
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button blue" />
    </div>
    <div class="form_single">
        <cr:CrystalReportViewer ID="crvADASetting" runat="server" HasDrillUpButton="false"
            HasToggleGroupTreeButton="false" HasPrintButton="true" HasExportButton="false"
            enableddrilldown="false"   HasSearchButton="false" DisplayToolbar="False" 
            EnableDrillDown="False" EnableParameterPrompt="False" ToolPanelView="None"></cr:CrystalReportViewer>
    </div>
    </form>
</body>
</html>
