
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region
Public Class ViewADATransaction
    Inherits maxiloan.webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imbBack As System.Web.UI.WebControls.ImageButton
    Protected WithEvents CrystalReportViewer1 As CrystalDecisions.Web.CrystalReportViewer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Property"

    Private Property strDate() As String
        Get
            Return CType(viewstate("strDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("strDate") = Value
        End Set
    End Property

#End Region

#Region "Deklarasi dan konstanta"
    Private m_controller As New GeneralPagingController
    Private oCustomClass As New Parameter.GeneralPaging
    Private dtDate As Date
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_ADATRANSACTION_REPORT)
        Me.BranchID = cookie.Values("BranchID")
        Me.BranchName = cookie.Values("BranchName")
        Me.strDate = cookie.Values("strDate")
        dtDate = ConvertDate2(Me.strDate)
    End Sub

    Sub BindReport()
        GetCookies()
        Dim dsAssetDocRep As New DataSet
        Dim PrintAssetDoc As New rptADAMutation
        Dim strStatusDateFrom As String = ""
        Dim strStatusDateTo As String = ""
        Dim dtStatusDateFrom As Date
        Dim dtStatusDateTo As Date
        With oCustomClass
            .strConnection = GetConnectionString
            .strDate = ConvertDateSql(Me.strDate)
            .SpName = "spADAMutationReport"
            .BranchId = Me.BranchID
        End With

        dsAssetDocRep = m_controller.GetReportWithBranchAndstrDate(oCustomClass)
        PrintAssetDoc.SetDataSource(dsAssetDocRep.Tables(0))
        CrystalReportViewer1.ReportSource = PrintAssetDoc
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.EnableDrillDown = False
        CrystalReportViewer1.DisplayGroupTree = False
        CrystalReportViewer1.DataBind()

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("BranchName")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BranchName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("Date")
        discrete = New ParameterDiscreteValue
        discrete.Value = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, dtDate))
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("CompanyName")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.sesCompanyName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("ReportID")
        discrete = New ParameterDiscreteValue
        discrete.Value = "rptADAMutation"
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("PrintedBy")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Loginid
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("param")
        discrete = New ParameterDiscreteValue
        discrete.Value = ""
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect("ADATransaction.aspx")
    End Sub
End Class
