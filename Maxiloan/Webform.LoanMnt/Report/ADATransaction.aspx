<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ADATransaction.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ADATransaction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Account Payable To Supplier Report</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ADA Transaction Report</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Branch&nbsp;<font color="red">*)</font></label>
            <asp:DropDownList ID="cboParent" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                ErrorMessage="Please Select Branch" ControlToValidate="cboParent"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Period&nbsp;<font color="red">*)</font></label>
            <asp:DropDownList ID="cboPeriodMonth" runat="server">
                <asp:ListItem Value="1">January</asp:ListItem>
                <asp:ListItem Value="2">February</asp:ListItem>
                <asp:ListItem Value="3">March</asp:ListItem>
                <asp:ListItem Value="4">April</asp:ListItem>
                <asp:ListItem Value="5">May</asp:ListItem>
                <asp:ListItem Value="6">June</asp:ListItem>
                <asp:ListItem Value="7">July</asp:ListItem>
                <asp:ListItem Value="8">August</asp:ListItem>
                <asp:ListItem Value="9">September</asp:ListItem>
                <asp:ListItem Value="10">October</asp:ListItem>
                <asp:ListItem Value="11">November</asp:ListItem>
                <asp:ListItem Value="12">Desember</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtPeriodYear" runat="server" Width="56px" ></asp:TextBox>
            <asp:Label ID="txtSignYear" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="rfvPeriodYear" runat="server" Display="Dynamic" ErrorMessage="Please Insert Year!"
                ControlToValidate="txtPeriodYear"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                    ID="revPeriodYear" runat="server" Display="Dynamic" ErrorMessage="Period Year is not Valid!"
                    ControlToValidate="txtPeriodYear" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnViewReport" runat="server" CausesValidation="True" Enabled="True"
            Text="View Report" CssClass="small button blue"></asp:Button>
    </div>
    </form>
</body>
</html>
