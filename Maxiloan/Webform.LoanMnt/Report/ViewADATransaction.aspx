<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewADATransaction.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ViewADATransaction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ViewARMutation</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
</head>
<body>
   <form id="Form1" runat="server">
    <div class="form_button">
        <asp:button ID="btnBack" runat="server" text="Back" CssClass="small button grey">
        </asp:button>
    </div>
    <div class="form_single">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server"></CR:CrystalReportViewer>
    </div>
    </form>
</body>
</html>
