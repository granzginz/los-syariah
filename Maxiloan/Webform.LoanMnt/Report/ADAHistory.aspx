﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ADAHistory.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ADAHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ARMutation Report</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }

        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('Select One', '0');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_box">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ADA History Report</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Branch</label>
            <asp:DropDownList ID="cboParent" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                ErrorMessage="Please Select Branch" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
        </div>
    </div>
     <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Period</label>
        
            <asp:DropDownList ID="cboPeriodMonth" runat="server">
                <asp:ListItem Value="1">January</asp:ListItem>
                <asp:ListItem Value="2">February</asp:ListItem>
                <asp:ListItem Value="3">March</asp:ListItem>
                <asp:ListItem Value="4">April</asp:ListItem>
                <asp:ListItem Value="5">May</asp:ListItem>
                <asp:ListItem Value="6">June</asp:ListItem>
                <asp:ListItem Value="7">July</asp:ListItem>
                <asp:ListItem Value="8">August</asp:ListItem>
                <asp:ListItem Value="9">September</asp:ListItem>
                <asp:ListItem Value="10">October</asp:ListItem>
                <asp:ListItem Value="11">November</asp:ListItem>
                <asp:ListItem Value="12">Desember</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtPeriodYear" runat="server"  Width="56px"></asp:TextBox>
            <asp:Label ID="txtSignYear" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="rfvPeriodYear" runat="server" ControlToValidate="txtPeriodYear"
                ErrorMessage="Please Insert Year!" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revPeriodYear" runat="server" ControlToValidate="txtPeriodYear"
                ErrorMessage="Period Year is not Valid!" Display="Dynamic" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>To
            <asp:DropDownList ID="cboPeriodMonthTo" runat="server">
                <asp:ListItem Value="1">January</asp:ListItem>
                <asp:ListItem Value="2">February</asp:ListItem>
                <asp:ListItem Value="3">March</asp:ListItem>
                <asp:ListItem Value="4">April</asp:ListItem>
                <asp:ListItem Value="5">May</asp:ListItem>
                <asp:ListItem Value="6">June</asp:ListItem>
                <asp:ListItem Value="7">July</asp:ListItem>
                <asp:ListItem Value="8">August</asp:ListItem>
                <asp:ListItem Value="9">September</asp:ListItem>
                <asp:ListItem Value="10">October</asp:ListItem>
                <asp:ListItem Value="11">November</asp:ListItem>
                <asp:ListItem Value="12">Desember</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtPeriodYearTo" runat="server"  Width="56px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtPeriodYearTo"
                ErrorMessage="Please Insert Year!" Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                    ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtPeriodYearTo"
                    ErrorMessage="Period Year is not Valid!" Display="Dynamic" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="imbViewReport" runat="server" Text="View Report" CssClass="small button blue">
        </asp:Button>
    </div>
    </form>
</body>
</html>
