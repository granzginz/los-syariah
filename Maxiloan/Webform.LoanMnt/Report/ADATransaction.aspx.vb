
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region
Public Class ADATransaction
    Inherits maxiloan.webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cboParent As System.Web.UI.WebControls.DropDownList
    Protected WithEvents rfvcbobranchid As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents cboPeriodMonth As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtPeriodYear As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSignYear As System.Web.UI.WebControls.Label
    Protected WithEvents rfvPeriodYear As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents revPeriodYear As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents imbViewReport As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Deklarasi dan constanta"
    Private m_controller As New DataUserControlController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FormID = "ADATransReport"

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not Page.IsPostBack Then
                Dim dtBranch As New DataTable
                dtBranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                txtPeriodYear.Text = CType(Year(Me.BusinessDate), String)
                With cboParent
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"

                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End If
                End With
            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

    End Sub

    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_ADATRANSACTION_REPORT)
        Dim strMonth As String = "0" & cboPeriodMonth.SelectedItem.Value
        Dim strARDate As String = "01/" & Right(strMonth, 2) & "/" & txtPeriodYear.Text.Trim
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "print", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("BranchID") = cboParent.SelectedItem.Value
                    cookie.Values("BranchName") = cboParent.SelectedItem.Text
                    cookie.Values("LoginID") = Me.Loginid
                    cookie.Values("ReportType") = "RptAPReport"
                    cookie.Values("strDate") = strARDate
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie(COOKIES_ADATRANSACTION_REPORT)
                    cookieNew.Values.Add("BranchID", cboParent.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboParent.SelectedItem.Text)
                    cookieNew.Values.Add("LoginID", Me.Loginid)
                    cookieNew.Values.Add("ReportType", "RptApReport")
                    cookieNew.Values.Add("strDate", strARDate)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("ViewADATransaction.aspx")
            End If
        End If
    End Sub

End Class
