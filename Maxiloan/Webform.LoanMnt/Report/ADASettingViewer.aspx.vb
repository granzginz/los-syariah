

#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ADASettingViewer
    Inherits maxiloan.webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imbBackReport As System.Web.UI.WebControls.ImageButton
    Protected WithEvents crvADASetting As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Property"
    Private Property ReportType() As String
        Get
            Return CType(viewstate("ReportType"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("ReportType") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BindGrid()
    End Sub

#Region " BindGrid & Get Coolies"
    Private Sub BindGrid()
        GetCookies()
        Dim dtsADASetting As DataSet
        Dim cADASetting As New GeneralPagingController
        Dim oADASetting As New Parameter.GeneralPaging
        Dim objreport As New ReportDocument
        Dim sPName As String = ""

        Select Case ReportType
            Case "ADASettingRpt"
                objreport = New ADASettingRpt
                sPName = "spRptADASetting"
        End Select

        With oADASetting
            .strConnection = GetConnectionString
            .WhereCond = Me.SearchBy
            .SpName = sPName
        End With

        oADASetting = cADASetting.GetReportWithParameterWhereCond(oADASetting)
        dtsADASetting = oADASetting.ListDataReport

        objreport.SetDataSource(dtsADASetting)
        crvADASetting.ReportSource = objreport
        crvADASetting.Visible = True
        crvADASetting.DataBind()

        '==========PARAMETER=========

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("CompanyFullName")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.sesCompanyName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("LoginID")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Loginid
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("ADASettingRpt")
        Me.ReportType = cookie.Values("ReportType")
        Me.SearchBy = cookie.Values("cmdwhere")
        Me.Loginid = cookie.Values("LoginID")
    End Sub

#End Region

    Private Sub imbBackReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Select Case Me.ReportType
            Case "ADASettingRpt"
                Response.Redirect("ADASetting.aspx")
        End Select
    End Sub
End Class
