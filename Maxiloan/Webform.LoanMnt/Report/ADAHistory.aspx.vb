﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ADAHistory
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New DataUserControlController


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FormID = "ADAHistory"

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not Page.IsPostBack Then
                Dim dtBranch As New DataTable
                dtBranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                txtPeriodYear.Text = CType(Year(Me.BusinessDate), String)
                With cboParent
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                        .Items.Insert(1, "ALL")
                        .Items(0).Value = "ALL"
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                        Dim strBranch() As String
                        strBranch = Split(Me.sesBranchId, ",")
                        If UBound(strBranch) > 0 Then
                            .Items.Insert(1, "ALL")
                            .Items(1).Value = "ALL"
                        End If
                    End If
                End With
            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

    End Sub

    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("ADAHistory")
        Dim strMonth As String = "0" & cboPeriodMonth.SelectedItem.Value
        Dim strMonthTo As String = "0" & cboPeriodMonthTo.SelectedItem.Value
        Dim strARDate As String = "01/" & Right(strMonth, 2) & "/" & txtPeriodYear.Text.Trim
        Dim strARDateTo As String = "01/" & Right(strMonthTo, 2) & "/" & txtPeriodYearTo.Text.Trim
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "print", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("BranchID") = cboParent.SelectedItem.Value
                    cookie.Values("BranchName") = cboParent.SelectedItem.Text
                    cookie.Values("LoginID") = Me.Loginid
                    cookie.Values("ReportType") = "RptADAHistory"
                    cookie.Values("DateFrom") = strARDate
                    cookie.Values("DateTo") = strARDateTo
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("ADAHistory")
                    cookieNew.Values.Add("BranchID", cboParent.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboParent.SelectedItem.Text)
                    cookieNew.Values.Add("LoginID", Me.Loginid)
                    cookieNew.Values.Add("ReportType", "RptADAHistory")
                    cookieNew.Values.Add("DateFrom", strARDate)
                    cookieNew.Values.Add("DateTo", strARDateTo)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("ViewADAHistory.aspx")
            End If
        End If
    End Sub

End Class