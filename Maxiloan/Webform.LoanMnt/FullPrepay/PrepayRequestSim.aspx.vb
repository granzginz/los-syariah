﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Microsoft.VisualStudio.TestTools.UnitTesting

#End Region

Public Class PrepayRequestSim
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents cboApprovedBy As System.Web.UI.WebControls.DropDownList
    Protected WithEvents reqApprovedBy As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtNotes As System.Web.UI.WebControls.TextBox
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    Protected WithEvents oPaymentInfoNormal As UcFullPrepayInfoNormal
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Protected WithEvents lblWOInstallment As System.Web.UI.WebControls.Label
    Protected WithEvents txtWOLCInstall, txtWOLCInsurance, txtWOInsuranceCollFee, txtWOInstallCollFee,
                         txtWOPDCBounceFee, txtWOSTNKRenewalFee, txtWOInsuranceClaimExpense,
                         txtWOReposessionFee, txtWOInsurance, txtWOInstallment, txtWOPenalty, txtWOInterest As ucNumberFormat
    Protected WithEvents txtPiRepossessionFee As ucNumberFormat

#Region "Property"
    Private Property ValidPrepayment() As Boolean
        Get
            Return CType(ViewState("ValidPrepayment"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("ValidPrepayment") = Value
        End Set
    End Property
    Private Property WOInsurance() As Double
        Get
            Return CType(ViewState("WOInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInsurance") = Value
        End Set
    End Property

    Private Property WOLCInstallment() As Double
        Get
            Return CType(ViewState("WOLCInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOLCInstallment") = Value
        End Set
    End Property

    Private Property WOLCInsurance() As Double
        Get
            Return CType(ViewState("WOLCInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOLCInsurance") = Value
        End Set
    End Property

    Private Property WOInstallCollFee() As Double
        Get
            Return CType(ViewState("WOInstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInstallCollFee") = Value
        End Set
    End Property

    Private Property WOInsuranceCollFee() As Double
        Get
            Return CType(ViewState("WOInsuranceCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInsuranceCollFee") = Value
        End Set
    End Property

    Private Property WOPDCBounceFee() As Double
        Get
            Return CType(ViewState("WOPDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOPDCBounceFee") = Value
        End Set
    End Property

    Private Property WOSTNKRenewalFee() As Double
        Get
            Return CType(ViewState("WOSTNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOSTNKRenewalFee") = Value
        End Set
    End Property

    Private Property WOTerminationPenaltyFee() As Double
        Get
            Return CType(ViewState("WOTerminationPenaltyFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOTerminationPenaltyFee") = Value
        End Set
    End Property

    Private Property WOReposessionFee() As Double
        Get
            Return CType(ViewState("WOReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOReposessionFee") = Value
        End Set
    End Property

    Private Property WOInstallment() As Double
        Get
            Return CType(ViewState("WOInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInstallment") = Value
        End Set
    End Property

    Private Property TotalDiscount() As Double
        Get
            Return (CType(ViewState("TotalDiscount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalDiscount") = Value
        End Set
    End Property

    Private Property PrepaymentAmount() As Double
        Get
            Return (CType(ViewState("PrepaymentAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrepaymentAmount") = Value
        End Set
    End Property

    Private Property DefaultStatus() As String
        Get
            Return (CType(ViewState("DefaultStatus"), String))
        End Get
        Set(value As String)
            ViewState("DefaultStatus") = value
        End Set
    End Property

    Private Property totalPelunasanDipercepat() As Double
        Get
            Return CType(ViewState("totalPelunasanDipercepat"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("totalPelunasanDipercepat") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New FullPrepayController
    Private oCustomClass As New Parameter.FullPrepay
    Protected WithEvents txtEffectiveDate As ucDateCE
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'lblMessage.Visible = False
        Dim stnkrenewal As Boolean = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "FULLSIMPREPAYLIST"
            txtEffectiveDate.IsRequired = True
            txtEffectiveDate.Text = DateAdd(DateInterval.Day, 1, Me.BusinessDate).ToString("dd/MM/yyyy")
            If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
                InitialValue()
                cboPrepaymentType.SelectedIndex = 1
                Me.TotalDiscount = 0
                Me.ApplicationID = Request.QueryString("applicationid")
                Me.BranchAgreement = Request.QueryString("branchid")
                'oApprovalRequest.ReasonTypeID = "FPPAY"
                'oApprovalRequest.ApprovalScheme = "FPAY"
                bindControllerNumber()
                GetPaymentInfoNormal()
                GetPaymentInfo()

                Dim oSTNKRenewal As New Parameter.GeneralPaging
                Dim m_controller As New GeneralPagingController
                With oSTNKRenewal
                    .strConnection = GetConnectionString()
                    .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and BranchID = '" & Me.BranchAgreement & "' and status in ('REQ','REG','INV')"
                    .CurrentPage = 1
                    .PageSize = 5
                    .SortBy = ""
                    .SpName = "spGetSTNKByRenewalOnly"
                End With
                oSTNKRenewal = m_controller.GetGeneralPaging(oSTNKRenewal)
                Dim recordCount As Long = oSTNKRenewal.TotalRecords
                'If recordCount > 0 Then
                '    lblSTNKMessage.Visible = True
                'Else
                '    lblSTNKMessage.Visible = False
                'End If

            End If
        End If
    End Sub


    Sub bindControllerNumber()
        With txtWOLCInstall
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOLCInsurance
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOPenalty
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999999"
        End With
        With txtWOInsuranceCollFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOPDCBounceFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOSTNKRenewalFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOInsuranceClaimExpense
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOReposessionFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOInsurance
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOInstallment
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOInstallCollFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
    End Sub

    Private Sub bindPaymentInfo(oPaymentInfo As UcFullPrepayInfo)

        With oPaymentInfo
            lblOSPrincipalAmount.Text = FormatNumber(.MaximumOutStandingPrincipal, 2)
            lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
            lblInstallmentDue.Text = FormatNumber(.InstallmentDueAmount, 2)
            lblInsuranceDue.Text = FormatNumber(.MaximumInsurance, 2)
            lblLCInstall.Text = FormatNumber(.MaximumLCInstallFee, 2)
            lblLCInsurance.Text = FormatNumber(.MaximumLCInsuranceFee, 2)
            lblInstallColl.Text = FormatNumber(.MaximumInstallCollFee, 2)
            lblInsuranceColl.Text = FormatNumber(.MaximumInsuranceCollFee, 2)
            lblPDCBounceFee.Text = FormatNumber(.MaximumPDCBounceFee, 2)
            lblSTNKFee.Text = FormatNumber(.MaximumSTNKRenewalFee, 2)
            lblInsuranceClaim.Text = FormatNumber(.MaximumInsuranceClaimFee, 2)
            'lblRepossessionFee.Text = FormatNumber(.MaximumReposessionFee, 2)
            txtPiRepossessionFee.Text = FormatNumber(.MaximumReposessionFee, 2)
            txtWOReposessionFee.RangeValidatorMaximumValue = "999999999"
            txtWOReposessionFee.setErrMsg()


            lblTerminationPenaltyFee.Text = FormatNumber(.MaximumPenaltyRate, 2)
            lblTotalPrepaymentAmount.Text = FormatNumber(.TotalPrepaymentAmount, 2)

            totalPelunasanDipercepat = .TotalPrepaymentAmount - .MaximumReposessionFee
        End With
    End Sub

    Private Sub updateDataset(oData As DataSet)
        oData.Tables(0).Rows(0)("CollectionExpense") = CDbl(txtPiRepossessionFee.Text.Trim)
        oData.Tables(0).AcceptChanges()
    End Sub

    Private Sub InitialValue()
        txtWOInstallment.Text = "0.00"
        txtWOLCInstall.Text = "0.00"
        txtWOInstallCollFee.Text = "0.00"
        txtWOPenalty.Text = "0.00"
        txtWOInsurance.Text = "0.00"
        txtWOLCInsurance.Text = "0.00"
        txtWOInsuranceCollFee.Text = "0.00"
        txtWOPDCBounceFee.Text = "0.00"
        txtWOReposessionFee.Text = "0.00"
        txtWOSTNKRenewalFee.Text = "0.00"
        txtWOInsuranceClaimExpense.Text = "0.00"
        lblTotalDiscount.Text = "0.00"
    End Sub
#End Region

#Region "Calculate"
    Private Sub DiscountCalculate()
        Me.TotalDiscount = CDbl(txtWOInstallCollFee.Text.Trim) + CDbl(txtWOInstallment.Text) + _
                                   CDbl(txtWOLCInsurance.Text.Trim) + CDbl(txtWOLCInstall.Text.Trim) + _
                                   CDbl(txtWOPDCBounceFee.Text.Trim) + CDbl(txtWOSTNKRenewalFee.Text.Trim) + _
                                   CDbl(txtWOPenalty.Text.Trim) + CDbl(txtWOInsuranceCollFee.Text.Trim) + _
                                   CDbl(txtWOReposessionFee.Text.Trim) + CDbl(txtWOInsurance.Text.Trim) + CDbl(txtWOInterest.Text.Trim)

        PrepaymentAmount = totalPelunasanDipercepat + CDbl(txtPiRepossessionFee.Text.Trim)
        lblTotalPrepaymentAmount.Text = FormatNumber(PrepaymentAmount, 2)
        Me.AmountToBePaid = Me.PrepaymentAmount - Me.TotalDiscount
        lblTotalDiscount.Text = FormatNumber(TotalDiscount, 2)
        lblTotalAmountToBePaid.Text = FormatNumber(AmountToBePaid, 2)
    End Sub
    Private Sub GetPaymentInfoNormal()
        With oPaymentInfoNormal
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(txtEffectiveDate.Text)
            .BranchID = Me.BranchAgreement.Trim
            .PaymentInfoNormal()
        End With
    End Sub
    Private Sub GetPaymentInfo()

        With oPaymentInfo
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(txtEffectiveDate.Text)
            .PrepaymentType = cboPrepaymentType.SelectedValue.Trim
            .BranchID = Me.BranchAgreement.Trim
            .PaymentInfo()
            lblAgreementBranch.Text = .BranchAgreement
            Me.AgreementNo = .AgreementNo
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            Me.ValidPrepayment = .IsValidPrepayment
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblPrepaidAmount.Text = FormatNumber(.ContractPrepaidAmount, 2)
            Me.PrepaidBalance = .ContractPrepaidAmount
            Me.PrepaymentAmount = .TotalPrepaymentAmount
            'lblTotalDiscount.Text = FormatNumber(Me.TotalDiscount, 2)
            Try
                lblFundingCoyName.Text = .FundingCoyName
                lblFundingPledgeStatus.Text = .FundingPledgeStatus
                lblResiduValue.Text = FormatNumber(.ResiduValue, 2)

                lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
                lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)

            Catch ex As Exception
                ex.Message.ToString()
            End Try



            If Not IsPostBack Then
                Dim oDataTable As DataTable = New DataTable
                Dim oRow As DataRow

                oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
                oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

                oRow = oDataTable.NewRow()

                oRow("ID") = "T"
                oRow("Description") = "Termination Request"
                oDataTable.Rows.Add(oRow)
                oRow = oDataTable.NewRow()
                'lblInsurancePaidBy.Text = "AT Cost"
                If .InsurancePaidBy = "CU" Then
                    oRow("ID") = "C"
                    oRow("Description") = "Release Clausal Leasing"
                    oDataTable.Rows.Add(oRow)
                    oRow = oDataTable.NewRow()
                    'lblInsurancePaidBy.Text = "Customer"
                End If

                'cboInsruranceRequest.DataValueField = "ID"
                'cboInsruranceRequest.DataTextField = "Description"
                'cboInsruranceRequest.DataSource = oDataTable
                'cboInsruranceRequest.DataBind()
                'cboInsruranceRequest.Items.Insert(0, "Select One")
                'cboInsruranceRequest.Items(0).Value = "0"
            End If
            Select Case .ContractStatus
                Case "INV"
                    lblContractStatus.Text = .ContractStatus
                Case "AKT"
                    lblContractStatus.Text = "Aktive"
                Case "PRP"
                    lblContractStatus.Text = "Prospect"
                Case "OSP"
                    lblContractStatus.Text = "Oustanding Pokok"
                Case "OSD"
                    lblContractStatus.Text = "Oustanding Denda"
                Case "SSD"
                    lblContractStatus.Text = "Siap Serah Terima Dokumen"
                Case "LNS"
                    lblContractStatus.Text = "Lunas"
                Case "INV"
                    lblContractStatus.Text = "Inventory"
                Case "RJC"
                    lblContractStatus.Text = "Contract Reject by Company"
                Case "CAN"
                    lblContractStatus.Text = "Contract Cancel by Customer"
            End Select
            Select Case .DefaultStatus.Trim
                Case "NA"
                    lblDefaultStatus.Text = "Stop Accrued"
                Case "WO"
                    lblDefaultStatus.Text = "Write Off"
                Case "NM"
                    lblDefaultStatus.Text = "Normal"
            End Select
            Me.DefaultStatus = .DefaultStatus
            If .IsCrossDefault Then
                lblCrossDefault.Text = "Yes"
            Else
                lblCrossDefault.Text = "No"
            End If
            lblTotalAmountToBePaid.Text = FormatNumber(Me.PrepaymentAmount - Me.TotalDiscount, 2)

            'txtWOInstallment.RangeValidatorMaximumValue = CStr(.MaximumInstallment)
            'txtWOInstallment.setErrMsg()
            'txtWOInsurance.RangeValidatorMaximumValue = CStr(.MaximumInsurance)
            'txtWOInsurance.setErrMsg()

            txtWOInstallCollFee.RangeValidatorMaximumValue = CStr(.MaximumInstallCollFee)
            txtWOInstallCollFee.setErrMsg()
            txtWOInsuranceCollFee.RangeValidatorMaximumValue = CStr(.MaximumInsuranceCollFee)
            txtWOInsuranceCollFee.setErrMsg()
            txtWOLCInsurance.RangeValidatorMaximumValue = CStr(.MaximumLCInsuranceFee)
            txtWOLCInsurance.setErrMsg()
            txtWOLCInstall.RangeValidatorMaximumValue = CStr(.MaximumLCInstallFee)
            txtWOLCInstall.setErrMsg()
            txtWOPDCBounceFee.RangeValidatorMaximumValue = CStr(.MaximumPDCBounceFee)
            txtWOPDCBounceFee.setErrMsg()
            txtWOSTNKRenewalFee.RangeValidatorMaximumValue = CStr(.MaximumSTNKRenewalFee)
            txtWOSTNKRenewalFee.setErrMsg()
            txtWOReposessionFee.RangeValidatorMaximumValue = CStr(.MaximumReposessionFee)
            txtWOReposessionFee.setErrMsg()
            txtWOPenalty.RangeValidatorMaximumValue = CStr(.MaximumPenaltyRate)
            txtWOPenalty.setErrMsg()
            txtWOInsuranceClaimExpense.RangeValidatorMaximumValue = CStr(.MaximumInsuranceClaimFee)
            txtWOInsuranceClaimExpense.setErrMsg()
        End With

        bindPaymentInfo(oPaymentInfo)

        Select Case cboPrepaymentType.SelectedValue.Trim
            Case "NM"
                lblWOInstallment.Text = "Installment"
                txtWOInstallment.Enabled = False
            Case "DI"
                lblWOInstallment.Text = "Installment"
                txtWOInstallment.Enabled = False
            Case "ED"
                lblWOInstallment.Text = "Interest"
                txtWOInstallment.Enabled = True
            Case "PD"
                lblWOInstallment.Text = "Installment"
                txtWOInstallment.Enabled = True
        End Select
    End Sub
#End Region
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Response.Redirect("PrepaySimRequestList.aspx")
    End Sub

    Private Sub imbCalcDiscount_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCalcDiscount.Click

        'Pengecekan diskon ini dinonaktifkan sementara
        'If ConvertDate2(txtEffectiveDate.Text) >= Me.BusinessDate Then
        'GetPaymentInfoNormal()
        'GetPaymentInfo()
        'DiscountCalculate()
        CalcDiscont()
        'Else

        '    ShowMessage(lblMessage, "Harap isi tanggal efektif > tanggal hari ini", True)
        'End If
    End Sub

    Protected Overridable Sub CalcDiscont()
        GetPaymentInfoNormal()
        GetPaymentInfo()
        DiscountCalculate()
    End Sub

    Private Sub imbCalculate_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCalculate.Click
        Dim effDate As Date = ConvertDate2(txtEffectiveDate.Text)
        lblMessage.Text = ""
        If effDate < Me.BusinessDate Then
            ShowMessage(lblMessage, "tanggal effective harus >= tanggal system", True)
            Exit Sub
        End If
        Try
            InitialValue()
            GetPaymentInfo()
            GetPaymentInfoNormal()
            DiscountCalculate()
        Catch exp As Exception
            ShowMessage(lblMessage, "Tanggal Salah", True)
        End Try
    End Sub

#Region "Report"
    Private Sub imbPrintTrialCalculation_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonPrintTrialCalculation.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim objreport As New ReportDocument
            If rboPrintTrial.SelectedValue = "Internal" Then
                objreport = New FullPrepaymentPrintTrialInternalSim
            Else
                objreport = New FullPrepaymentPrintTrialSim
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchAgreement
                .ApplicationID = Me.ApplicationID
                .BusinessDate = ConvertDate2(txtEffectiveDate.Text)
                .PrepaymentType = cboPrepaymentType.SelectedValue.Trim
                .totalDiscount = CDbl(lblTotalDiscount.Text)
            End With
            Dim oData As New DataSet
            oData = oController.PrintTrial(oCustomClass)

            updateDataset(oData)

            objreport.SetDataSource(oData)
            Dim discrete As ParameterDiscreteValue
            Dim ParamField As ParameterFieldDefinition
            Dim CurrentValue As ParameterValues
            ParamField = objreport.DataDefinition.ParameterFields("BranchName")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.BranchName
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamField = objreport.DataDefinition.ParameterFields("AsOf")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.BusinessDate.ToString("MM/dd/yyyy")
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamField = objreport.DataDefinition.ParameterFields("LoginID")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.Loginid
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamField = objreport.DataDefinition.ParameterFields("EffectiveDate")
            discrete = New ParameterDiscreteValue
            discrete.Value = ConvertDate2(txtEffectiveDate.Text).ToString("MM/dd/yyyy")
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            Dim strFileLocation As String
            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID & Me.Loginid & "PrintTrialCalculation.pdf"
            DiskOpts.DiskFileName = strFileLocation
            objreport.ExportOptions.DestinationOptions = DiskOpts
            objreport.Export()

            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PrintTrialCalculation.pdf"


            'Response.Write("<script language = javascript>" & vbCrLf _
            '            & "var x = screen.width; " & vbCrLf _
            '            & "var y = screen.height; " & vbCrLf _
            '            & "window.open('" & strFileLocation & "','PrepayTrial', 'left=0, top=0, width=600, height=700, menubar=0, scrollbars=yes') " & vbCrLf _
            '            & "</script>")


            Dim strScript As New StringBuilder

            strScript.Append("var x = screen.width; ")
            strScript.Append("var y = screen.height; ")
            strScript.Append("window.open('" & strFileLocation & "','PrepayTrial', 'left=0, top=0, width= ' + x + ',  height=' + y + ', menubar=0, scrollbars=yes');")

            ScriptManager.RegisterStartupScript(Page, Me.GetType, "PrintPrepayTrial", strScript.ToString, True)
        End If
    End Sub
#End Region

End Class