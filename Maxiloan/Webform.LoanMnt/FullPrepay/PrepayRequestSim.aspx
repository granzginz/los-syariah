﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrepayRequestSim.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PrepayRequestSim" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="UcFullPrepayInfoNormal" Src="../../Webform.UserController/UcFullPrepayInfoNormal.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prepay Request</title>   
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-1.9.1.min.js"></script>

     <script type="text/javascript">

         //$(document).ready(function () {
         //    //
         //    $("#txtPiRepossessionFee_txtNumber").change(function(){ 
         //        alert(this.value);

         //        var disc = $("#txtWOReposessionFee_txtNumber").val();
         //        alert(disc);
         //        $("#txtWOReposessionFee_rv").maximumvalue = 9000;
         //    });

         ////    $('#txtPiRepossessionFee_txtNumber').prependTo("#oPaymentInfo_lblRepossessionFee");
         ////    //var repfee = $('#oPaymentInfo_lblRepossessionFee').text();
         ////    //$('#oPaymentInfo_lblRepossessionFee').text('<input name="txtPiRepossessionFee$txtNumber" value="0.00" id="txtPiRepossessionFee_txtNumber" class="numberAlign regular_text" onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);" onfocus="this.value=resetNumber(this.value);" onchange="javascript:void(0)" autocomplete="off" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" type="text">');
         //  }); 
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <progresstemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <contenttemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENGAJUAN PELUNASAN DIPERCEPAT (SIMULASI)
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cabang Kontrak</label>
                    <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">                
                <div class="form_left">
                    <label>
                        No Kontrak</label>
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Nama Customer</label>
                    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
                </div>                
            </div>

             <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> Jenis Pelunasan(Prepayment)</label>
                    <asp:DropDownList ID="cboPrepaymentType" runat="server" CssClass="opt_single">
                        <asp:ListItem Value="0">Select One</asp:ListItem>                        
                        <asp:ListItem Value="DI">Daily Interest</asp:ListItem> 
                        <asp:ListItem Value="ED">Earn Discount</asp:ListItem>
                        <asp:ListItem Value="PD">Principle Discount</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0" Display="Dynamic" ErrorMessage="Harap Pilih Jenis Pelunasan" ControlToValidate="cboPrepaymentType" CssClass="validator_general"></asp:RequiredFieldValidator>                    
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Tanggal Effective</label>
                    <uc1:ucDateCE id="txtEffectiveDate" runat="server"></uc1:ucDateCE>    
                    <asp:Button ID="ButtonCalculate" runat="server" Text="Calculate" CssClass="small buttongo blue"
                        CausesValidation="False"></asp:Button>
                    <div style="text-align:right;vertical-align:middle;display:inline;font-weight:bold">
                        <label id="lblSTNKMessage" runat="server" style="color:#3F5C9A">* STNK Sedang dalam proses *</label>
                    </div>             
                </div>
            </div>
            <asp:Panel ID="pnlPaymentInfoNormal" runat="server">
                <uc2:UcFullPrepayInfoNormal id="oPaymentInfoNormal" runat="server"></uc2:UcFullPrepayInfoNormal>
            </asp:Panel>
            <asp:Panel ID="pnlPaymentInfo" runat="server">
                <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server"  Visible="false"  ></uc1:ucfullprepayinfo>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>TOTAL PELUNASAN DIPERCEPAT</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Sisa Pokok</label>
                            <asp:Label ID="lblOSPrincipalAmount" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Accrued Interest</label>
                            <asp:Label ID="lblAccruedInterest" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Angsuran Jatuh Tempo</label>
                            <asp:Label ID="lblInstallmentDue" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Premi Asuransi Jatuh Tempo</label>
                            <asp:Label ID="lblInsuranceDue" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Denda keterlambatan Angsuran</label>
                            <asp:Label ID="lblLCInstall" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Denda Keterlambatan AsuInransi</label>
                            <asp:Label ID="lblLCInsurance" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Biaya Tagih Angsuran</label>
                            <asp:Label ID="lblInstallColl" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Biaya Tagih Asuransi</label>
                            <asp:Label ID="lblInsuranceColl" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Biaya Tolakan PDC</label>
                            <asp:Label ID="lblPDCBounceFee" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Perpanjangan STNK/BBN
                            </label>
                            <asp:Label ID="lblSTNKFee" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Biaya Klaim Asuransi</label>
                            <asp:Label ID="lblInsuranceClaim" runat="server" CssClass="numberAlign label"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Biaya Tarik</label>
                            <%--<asp:Label ID="lblRepossessionFee" runat="server" CssClass="numberAlign label"></asp:Label>--%>
                            <uc1:ucnumberformat id="txtPiRepossessionFee" runat="server" />
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlTerminationFee" runat="server">
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    Penalty & Biaya Administrasi
                                </label>
                                <asp:Label ID="lblTerminationPenaltyFee" runat="server" CssClass="numberAlign label"></asp:Label>
                            </div>
                            <div class="form_right" style="background-color:#FEFF51;font-weight:bold">
                                <label>
                                    Total Pelunasan Dipercepat
                                </label>
                                <asp:Label ID="lblTotalPrepaymentAmount" runat="server" CssClass="numberAlign label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlPrintTrialCalculation" runat="server">
                <div class="form_button">  </div>
                <div class="form_box">
                <asp:RadioButtonList ID="rboPrintTrial" runat="server" RepeatDirection="Horizontal"
                        CssClass="opt_single" >
                        <asp:ListItem Value="External" >Versi Customer</asp:ListItem>
                        <asp:ListItem Value="Internal" Selected="True">Versi Perusahaan</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </asp:Panel>


            
            <asp:Panel ID="pnlAgreementStatus" runat="server"  visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>STATUS KONTRAK</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>Status Kontrak</label>
                            <asp:Label ID="lblContractStatus" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Status Default</label>
                            <asp:Label ID="lblDefaultStatus" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Jumlah Angsuran</label>
                            <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Jatuh Tempo Angsuran Berikut</label>
                            <asp:Label ID="lblNextInstallmentDate" runat="server"></asp:Label>|Installment
                            <asp:Label ID="lblNextInstallmentNumber" runat="server" Width="3px"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>Funding Bank</label>
                            <asp:Label ID="lblFundingCoyName" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Status Penjaminan</label>
                            <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>Nilai Residu</label>
                            <asp:Label ID="lblResiduValue" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>Cross Default</label>
                            <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDiscount" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4> DISCOUNT </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Denda Keterlambatan Angsuran</label>
                        <uc1:ucnumberformat id="txtWOLCInstall" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="label_req">Denda Keterlambatan Asuransi</label>
                        <uc1:ucnumberformat id="txtWOLCInsurance" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req"> Biaya Tagih Angsuran</label>
                        <uc1:ucnumberformat id="txtWOInstallCollFee" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="label_req"> Biaya Tagih Asuransi</label>
                        <uc1:ucnumberformat id="txtWOInsuranceCollFee" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Biaya Tolakan PDC</label>
                        <uc1:ucnumberformat id="txtWOPDCBounceFee" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="label_req"> Biaya Perpanjangan STNK/BBN</label>
                        <uc1:ucnumberformat id="txtWOSTNKRenewalFee" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req"> Biaya Klaim Asuransi</label>
                        <uc1:ucnumberformat id="txtWOInsuranceClaimExpense" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="label_req"> Biaya Tarik</label>
                        <uc1:ucnumberformat id="txtWOReposessionFee" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req"> Premi Asuransi</label>
                        <uc1:ucnumberformat id="txtWOInsurance" runat="server" />
                    </div>
                    <div class="form_right"> 
                        <asp:Label class="label_req label_general" ID="lblWOInstallment" runat="server">Principal</asp:Label>
                        <uc1:ucnumberformat id="txtWOInstallment" runat="server" />
                    </div>
                </div>
                <div class="form_box" id="InstallmentDiv" runat="server" >
                    <div class="form_left"> </div>
                    <div class="form_right">
                        <asp:Label class="label_req label_general" ID="lblInterest" runat="server">Interest</asp:Label>
                        <uc1:ucnumberformat id="txtWOInterest" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Penalty & Biaya Administrasi</label>
                        <uc1:ucnumberformat id="txtWOPenalty" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Total Discount</label>
                        <asp:Label ID="lblTotalDiscount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                       
                    </div>
                </div>
            </asp:Panel>
             <asp:Panel ID="pnlRequestDetail" runat="server">
                <div class="form_box">
                    <div class="form_left">
                    </div>
                    <div class="form_right" style="background-color:#FEFF51;font-weight:bold">
                        <label>
                            Total yg harus dibayar</label>
                        <asp:Label ID="lblTotalAmountToBePaid" runat="server" CssClass ="numberAlign label"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Prepaid
                        </label>
                        <asp:Label ID="lblPrepaidAmount" runat="server" CssClass ="numberAlign label"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_button" runat="server" id="div_button">
                <asp:Button ID="ButtonCalcDiscount" runat="server" Text="Calc Discount" CausesValidation="false" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonPrintTrialCalculation" runat="server" CausesValidation="False"   Text="Print Trial Calculation" CssClass="small button blue"></asp:Button>                 
                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False"></asp:Button>
            </div>
        </contenttemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
