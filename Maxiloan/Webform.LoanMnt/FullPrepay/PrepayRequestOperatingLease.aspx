﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrepayRequestOperatingLease.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PrepayRequestOperatingLease" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfoOL.ascx" %>
<%@ Register TagPrefix="uc2" TagName="UcFullPrepayInfoNormal" Src="../../Webform.UserController/UcFullPrepayInfoNormal.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prepay Request Operating Lease</title>   
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <progresstemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <contenttemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENGAJUAN PEMUTUSAN KONTRAK OPERATING LEASE
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cabang Kontrak</label>
                    <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">                
                <div class="form_left">
                    <label>
                        No Kontrak</label>
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Nama Customer</label>
                    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
                </div>                
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Tanggal Effective</label>
                    <uc1:ucDateCE id="txtEffectiveDate" runat="server"></uc1:ucDateCE>    
                    <asp:Button ID="ButtonCalculate" runat="server" Text="Calculate" CssClass="small buttongo blue"
                        CausesValidation="False"></asp:Button>
                    <div style="text-align:right;vertical-align:middle;display:inline;font-weight:bold">
                        <label id="lblSTNKMessage" runat="server" style="color:#3F5C9A">* STNK Sedang dalam proses *</label>
                    </div>             
                </div>
            </div>
            <asp:Panel ID="pnlPaymentInfo" runat="server">
                <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server"></uc1:ucfullprepayinfo>
            </asp:Panel>
            <asp:Panel ID="pnlAgreementStatus" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            STATUS KONTRAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Status Kontrak</label>
                            <asp:Label ID="lblContractStatus" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Status Default</label>
                            <asp:Label ID="lblDefaultStatus" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Jumlah Angsuran</label>
                            <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Jatuh Tempo Angsuran Berikut</label>
                            <asp:Label ID="lblNextInstallmentDate" runat="server"></asp:Label>|Installment
                            <asp:Label ID="lblNextInstallmentNumber" runat="server" Width="3px"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Funding Bank</label>
                            <asp:Label ID="lblFundingCoyName" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Status Penjaminan</label>
                            <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Nilai Residu</label>
                            <asp:Label ID="lblResiduValue" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Cross Default</label>
                            <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDiscount" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DISCOUNT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Denda Keterlambatan Angsuran</label>
                        <uc1:ucnumberformat id="txtWOLCInstall" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Biaya Tarik</label>
                        <uc1:ucnumberformat id="txtWOReposessionFee" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tolakan PDC</label>
                        <uc1:ucnumberformat id="txtWOPDCBounceFee" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Penalty & Biaya Administrasi</label>
                        <uc1:ucnumberformat id="txtWOPenalty" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            Total Discount</label>
                        <asp:Label ID="lblTotalDiscount" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlRequestDetail" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DETAIL PENGAJUAN PELUNASAN DIPERCEPAT
                        </h4>
                    </div>
                </div>
                <uc1:ucapprovalrequest id="oApprovalRequest" runat="server"></uc1:ucapprovalrequest>
                <div class="form_box">
                    <div class="form_left">
                    </div>
                    <div class="form_right" style="background-color:#FEFF51;font-weight:bold">
                        <label>
                            Total yg harus dibayar</label>
                        <asp:Label ID="lblTotalAmountToBePaid" runat="server" CssClass ="numberAlign label"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Prepaid
                        </label>
                        <asp:Label ID="lblPrepaidAmount" runat="server" CssClass ="numberAlign label"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_button">
                 <asp:Button ID="ButtonCalcDiscount" runat="server" Text="Calc Discount" CausesValidation="false" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonPrintTrialCalculation" runat="server" CausesValidation="False"
                        Text="Print Trial Calculation" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonRequest" runat="server" Text="Request" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </contenttemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
