﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PrepayExecutionOperatingLease
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PrepaymentNo() As String
        Get
            Return CType(ViewState("PrepaymentNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PrepaymentNo") = Value
        End Set
    End Property
    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property Mode() As ProcessMode
        Get
            Return (CType(ViewState("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property
    'Mode 0 = Cancel
    'Mode 1 = Execute
    'Mode 2 = View
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum

    Private Property AmountToBePaid() As Double
        Get
            Return (CType(ViewState("AmountToBePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountToBePaid") = Value
        End Set
    End Property

    Private Property PrepaidAmount() As Double
        Get
            Return (CType(ViewState("PrepaidAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrepaidAmount") = Value
        End Set
    End Property

    Private Property PrepaymentAmount() As Double
        Get
            Return (CType(ViewState("PrepaymentAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrepaymentAmount") = Value
        End Set
    End Property

    Private Property TotalDiscount() As Double
        Get
            Return (CType(ViewState("TotalDiscount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalDiscount") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private oCustomClassFullPrepay As New Parameter.FullPrepay
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "FULLPREPAYEXECOL"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Select Case Request.QueryString("mode")
                    Case "0"
                        Me.Mode = ProcessMode.Cancel
                    Case "1"
                        Me.Mode = ProcessMode.Execute
                    Case "2"
                        Me.Mode = ProcessMode.View
                End Select
                Me.PrepaymentNo = Request.QueryString("prepaymentno")

                InitializeForm()
                DoBind()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#End Region
    Private Sub LinkTo()
        hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Me.ApplicationId.Trim & "')"
        hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerID.Text.Trim & "')"
        lblPrepaymentNo.NavigateUrl = "javascript:OpenWinViewRequestNo('" & "AccMnt" & "','" & Me.PrepaymentNo & "')"
    End Sub
    Private Sub InitializeForm()
        If Me.Mode = ProcessMode.Cancel Then
            ButtonExecute.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonBack.Visible = False
        ElseIf Me.Mode = ProcessMode.Execute Then
            ButtonExecute.Visible = True
            ButtonCancel.Visible = True
            ButtonSave.Visible = False
            ButtonBack.Visible = False
        ElseIf Me.Mode = ProcessMode.View Then
            ButtonExecute.Visible = False
            ButtonCancel.Visible = False
            ButtonSave.Visible = False
            ButtonBack.Visible = True
        End If
    End Sub

    Private Sub DoBind()
        Dim totaldiscount As Double
        Dim totalprepayment As Double
        Dim oCustomClassFullPrepay As New Parameter.FullPrepay
        Dim oControllerPrepayCancelation As New FullPrepayController
        With oCustomClassFullPrepay
            .strConnection = GetConnectionString()
            .PrepaymentNo = Me.PrepaymentNo
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With
        oCustomClassFullPrepay = oControllerPrepayCancelation.FullPrepayInfo(oCustomClassFullPrepay)
        With oCustomClassFullPrepay
            If Me.Mode = ProcessMode.Cancel Then
                lblTitle.Text = "PREPAYMENT CANCELLATION"
            ElseIf Me.Mode = ProcessMode.Execute Then
                lblTitle.Text = "PREPAYMENT EXECUTION"
            ElseIf Me.Mode = ProcessMode.View Then
                lblTitle.Text = "VIEW PREPAYMENT REQUEST"
            End If

            lblPrepaymentNo.Text = .PrepaymentNo
            lblRequestdate.Text = .BusinessDate.ToString("dd/MM/yyyy")
            hypAgreementNo.Text = .Agreementno
            hypCustomerName.Text = .CustomerName
            Select Case .PrepaymentType.Trim
                Case "DI"
                    lblPrepaymentType.Text = "Daily Interest"
                Case "ED"
                    lblPrepaymentType.Text = "Earn Discount"
                Case "NM"
                    lblPrepaymentType.Text = "Standard"
                Case " PD"
                    lblPrepaymentType.Text = "Principal Discount"
            End Select
            lblEffectiveDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            lblOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
            lblLCInstall.Text = FormatNumber(.LcInstallment, 2)

            lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
            lblTerminationFee.Text = FormatNumber(.TerminationPenalty, 2)
            lblRepoFee.Text = FormatNumber(.RepossessionFee, 2)
            lblTotalPrepaymentAmount.Text = FormatNumber(.PrepaymentAmount, 2)
            'totalprepayment = .OutstandingPrincipal + .AccruedInterest + .InstallmentDue + _
            '                  .InsuranceDue + .LcInstallment + .LcInsurance + _
            '                  .InstallmentCollFee + .InsuranceCollFee + .PDCBounceFee + _
            '                  .STNKRenewalFee + .TerminationPenalty + .RepossessionFee + .InsuranceClaimExpense
            totalprepayment = .PrepaymentAmount
            Me.PrepaymentAmount = totalprepayment
            Select Case .ContractStatus.Trim
                Case "AKT"
                    lblContractStatus.Text = "Aktive"
                Case "PRP"
                    lblContractStatus.Text = "Prospect"
                Case "OSP"
                    lblContractStatus.Text = "Oustanding Pokok"
                Case "OSD"
                    lblContractStatus.Text = "Oustanding Denda"
                Case "SSD"
                    lblContractStatus.Text = "Siap Serah Terima Dokumen"
                Case "LNS"
                    lblContractStatus.Text = "Lunas"
                Case "INV"
                    lblContractStatus.Text = "Expired Cause Invetaried"
                Case "RJC"
                    lblContractStatus.Text = "Contract Reject By Company"
                Case "CAN"
                    lblContractStatus.Text = "Contract Cancel by Customer"
            End Select
            Select Case .DefaultStatus.Trim
                Case "NM"
                    lblDefaultStatus.Text = "Normal"
                Case "NA"
                    lblDefaultStatus.Text = "Stop Accrued"
                Case "WO"
                    lblDefaultStatus.Text = "Write Off"
            End Select

            lblCrossDefault.Text = CStr(IIf(.CrossDefault, "Yes", "No"))

            lblLCInstallWaived.Text = FormatNumber(.LcInstallmentWaived, 2)
            lblPDCBounceWaived.Text = FormatNumber(.PDCBounceFeeWaived, 2)
            lblTerminationWaived.Text = FormatNumber(.TerminationPenaltyWaived, 2)
            lblReposessionWaived.Text = FormatNumber(.RepossessionFeeWaived, 2)
            
            totaldiscount = .LcInstallmentWaived + .LcInsuranceWaived + .InstallmentCollFeeWaived + _
                            .InsuranceCollFeeWaived + .PDCBounceFeeWaived + .STNKRenewalFeeWaived + _
                            .TerminationPenaltyWaived + .RepossessionFeeWaived + .InsuranceDueWaived + _
                            .InstallmentWaived + .InsuranceClaimExpenseWaived
            Me.TotalDiscount = totaldiscount
            'AC = At Cost
            'CU = Customer

            Select Case .InsuranceTerminationFlag.Trim
                Case "T"
                    lblInsuranceRequest.Text = "Termination Request"
                Case "C"
                    lblInsuranceRequest.Text = "Release Clausal Leasing"
            End Select

            lblTotalDiscount.Text = FormatNumber(totaldiscount)

            Select Case .InsurancePaidBy.Trim
                Case "AC"
                    lblInsurancePaidBy.Text = "AT Cost"
                Case "CU"
                    lblInsurancePaidBy.Text = "Customer"
            End Select

            lblReason.Text = .ReasonDescription
            lblApprovedBy.Text = .UserApproval
            lblNotes.Text = .PrepaymentNotes
            Select Case .PrepaymentStatus.Trim
                Case "R"
                    lblStatus.Text = "Request"
                Case "A"
                    lblStatus.Text = "Approve"
                Case "C"
                    lblStatus.Text = "Cancel"
                Case "D"
                    lblStatus.Text = "Reject"
            End Select

            lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
            lblRequestBy.Text = .UserRequest

            lblTotalAmountToBePaid.Text = FormatNumber(totalprepayment - totaldiscount, 2)
            lblPrepaidAmount.Text = FormatNumber(.Prepaid, 2)
            lblCustomerID.Text = .CustomerID

            'Me.PrepaymentAmount = .Prepaid
            lblBalanceAmount.Text = FormatNumber(.Prepaid - (Me.PrepaymentAmount - Me.TotalDiscount), 2)
            Me.AmountToBePaid = .Prepaid - (Me.PrepaymentAmount - Me.TotalDiscount)
            Me.ApplicationId = .ApplicationID


            lblFundingCoyName.Text = .FundingCoyName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
            lblResiduValue.Text = FormatNumber(.ResiduValue, 2)

            lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
            lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
            lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)
        End With
        LinkTo()
    End Sub

#Region "Back Cancel"
    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Server.Transfer("PrepayExecutionListOperatingLease.aspx")
    End Sub

    Private Sub ButtonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Server.Transfer("PrepayExecutionListOperatingLease.aspx")
    End Sub
#End Region

#Region "Execute Process"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        'Untuk Proces Cancel
        If CheckFeature(Me.Loginid, Me.FormID, "CAN", Me.AppId) Then
            Dim oCustomClassFullPrepay As New Parameter.FullPrepay
            Dim oControllerPrepayCancelation As New FullPrepayController
            With oCustomClassFullPrepay
                .strConnection = GetConnectionString()
                .PrepaymentNo = Me.PrepaymentNo
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
            End With
            Try
                oControllerPrepayCancelation.FullPrepayCancel(oCustomClassFullPrepay)
                Server.Transfer("PrepayExecutionListOperatingLease.aspx")
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    Private Sub ButtonExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonExecute.Click
        If CheckFeature(Me.Loginid, Me.FormID, "EXEC", Me.AppId) Then
            If ConvertDate2(lblEffectiveDate.Text.Trim) > Me.BusinessDate Then

                ShowMessage(lblMessage, "Tanggal Efektif harus lebih kecil dari Tgl hari ini", True)
            ElseIf CDbl(lblBalanceAmount.Text.Replace(",", "")) < 0 Then

                ShowMessage(lblMessage, "Jumlah Prepaid tidak cukup untuk alokasi pelunasan dipercepat", True)
            Else
                Dim oControllerPrepayExecute As New FullPrepayController
                Dim oCustomClassFullPrepay As New Parameter.FullPrepay
                With oCustomClassFullPrepay
                    .strConnection = GetConnectionString()
                    .PrepaymentNo = Me.PrepaymentNo
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .BusinessDate = Me.BusinessDate
                    .CoyID = Me.SesCompanyID
                End With
                Try
                    oControllerPrepayExecute.FullPrepayExecutionOL(oCustomClassFullPrepay)
                    Server.Transfer("PrepayExecutionListOperatingLease.aspx")
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
        End If
    End Sub
#End Region

End Class