﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PrepayExecutionList
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Protected WithEvents oSearchBy As UcSearchBy    
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "FULLPREPAYEXEC"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                'ShowMessage(lblMessage, "Transaksi Pelunasan Dipercepat Berhasil", False)
                InitialDefaultPanel()
                oSearchBy.ListData = "PREPAYMENTNO,No Request-AGREEMENTNO,No Kontrak-CUSTOMERNAME,Nama Konsumen"
                oSearchBy.BindData()                
                txtEffectiveDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        lblMessage.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal strSortBy As String)
        Dim intloop As Integer
        Dim hypID As HyperLink
        With oContract
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSortBy
            .SpName = "spPrepaymentExecutionList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        recordCount = oContract.TotalRecords
        dtgPrepayExecutionList.DataSource = oContract.ListData
        Try
            dtgPrepayExecutionList.DataBind()
        Catch
            dtgPrepayExecutionList.CurrentPageIndex = 0
            dtgPrepayExecutionList.DataBind()
        End Try
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Text = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        lblMessage.Text = ""
        Me.SearchBy = " BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        Me.SearchBy &= " And EffectiveDate <= '" & ConvertDate2(txtEffectiveDate.Text).ToString("yyyy/MM/dd") & "'"
        If oSearchBy.Text.Trim <> "" Then
            If Right(oSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " Like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " LIKE '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
            End If
        End If

        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        lblMessage.Text = ""
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub dtgPrepayExecutionList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPrepayExecutionList.ItemDataBound
        Dim imbCancel As HyperLink
        Dim imbExecute As HyperLink
        Dim lblPrepaymentNo As HyperLink
        Dim lblApplicationid As Label
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label


        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            lnkCust = CType(e.Item.FindControl("hypCustomerName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            'Mode 0 = Cancel
            'Mode 1 = Execute
            'Mode 2 = View
            imbCancel = CType(e.Item.FindControl("imbCancel"), HyperLink)
            imbExecute = CType(e.Item.FindControl("imbExecute"), HyperLink)
            lblPrepaymentNo = CType(e.Item.FindControl("lblPrepaymentNo"), HyperLink)
            imbCancel.NavigateUrl = "PrepayExecution.aspx?mode=0&prepaymentno=" & lblPrepaymentNo.Text
            imbExecute.NavigateUrl = "PrepayExecution.aspx?mode=1&prepaymentno=" & lblPrepaymentNo.Text
            lblPrepaymentNo.NavigateUrl = "PrepayExecution.aspx?mode=2&prepaymentno=" & lblPrepaymentNo.Text
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & lblApplicationid.Text.Trim & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub

End Class