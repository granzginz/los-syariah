﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrepayExecution.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PrepayExecution" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrepayExecution</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewRequestNo(pStyle, pRequestNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/FullPrepay/ViewRequestNo.aspx?style=' + pStyle + '&prepaymentno=' + pRequestNo, 'RequestNoLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">    
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblCustomerID" runat="server" Visible="false"></asp:Label>
        <div class="form_title"><div class="title_strip"> </div>
            <div class="form_single">           
                <h3>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h3>
            </div>
        </div>     
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>No Request</label>
                    <asp:HyperLink ID="lblPrepaymentNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Request</label>		
                    <asp:Label ID="lblRequestdate" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Nama Customer</label>		
                    <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Jenis Prepayment</label>
                    <asp:Label runat="server" ID="lblPrepaymentType"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Efektif</label>		
                    <asp:Label runat="server" ID="lblEffectiveDate"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box_hide">
	        <div class="form_single">
                <div class="form_left">
                    <%--<label>Jenis Prepayment</label>
                    <asp:Label runat="server" ID="Label1"></asp:Label>--%>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Pembayaran Terakhir</label>		
                    <asp:Label runat="server" ID="lbllastpaiddate"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    DETAIL INFORMASI PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Sisa Pokok</label>
                    <asp:Label runat="server" ID="lblOutstandingPrincipal"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Accrued Interest</label>	
                    <asp:Label runat="server" ID="lblAccruedInterest"></asp:Label>	
		        </div>
	        </div>
        </div>    
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <%--<label>Angsuran Jatuh Tempo</label>--%>
                    <label>Margin Jatuh Tempo</label>
                    <asp:Label runat="server" ID="lblInstallmentDue"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Premi Asuransi</label>	
                    <asp:Label runat="server" ID="lblInsuranceDue"></asp:Label>		
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Dedan Keterlambatan Angsuran</label>
                    <asp:Label ID="lblLCInstall" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Denda Keterlambatan Asuransi</label>	
                    <asp:Label ID="lblLCInsurance" runat="server"></asp:Label>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tagih Angsuran</label>
                    <asp:Label ID="lblInstallCollFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tagih Asuransi</label>
                    <asp:Label runat="server" ID="lblInsuranceCollFee"></asp:Label>		
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tolakan PDC</label>
                    <asp:Label runat="server" ID="lblPDCBounceFee"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Perpanjangan STNK/BBN</label>
                    <asp:Label runat="server" ID="lblSTNKRenewalFee"></asp:Label>		
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Pelunasan Dipercepat</label>
                    <asp:Label ID="lblTerminationFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tarik</label>		
                    <asp:Label ID="lblRepoFee" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Klaim Asuransi</label>
                    <asp:Label ID="lblInsuranceClaimFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label><strong>Total Pelunasan Dipercepat</strong></label>
                    <asp:Label ID="lblTotalPrepaymentAmount" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    STATUS KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Status Kontrak</label>
                    <asp:Label runat="server" ID="lblContractStatus"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Status Default</label>		
                    <asp:Label runat="server" ID="lblDefaultStatus"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Jumlah Angsuran</label>
                    <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Jatuh Tempo Berikutnya</label>		
                    <asp:Label ID="lblNextInstallmentDate" runat="server"></asp:Label>&nbsp;&nbsp;|&nbsp;Installment&nbsp;
                    <asp:Label ID="lblNextInstallmentNumber" Width="3px" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Funding Bank</label>
                    <asp:Label ID="lblFundingCoyName" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Status Penjaminan</label>	
                    <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Nilai Residu</label>
                    <asp:Label ID="lblResiduValue" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Cross Default</label>	
                    <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>		
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>    
                    DISCOUNT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Keterlambatan Angsuran</label>
                    <asp:Label ID="lblLCInstallWaived" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Denda keterlambatan Asuransi</label>	
                    <asp:Label ID="lblLCInsuranceWaived" runat="server"></asp:Label>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tagih Angsuran</label>
                    <asp:Label ID="lblInstallCollWaived" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tagih Asuransi</label>		
                    <asp:Label ID="lblInsuranceCollWaived" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left"> 
                    <label>Biaya Tolakan PDC</label>
                    <asp:Label ID="lblPDCBounceWaived" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Perpanjang STNK/BBN</label>	
                    <asp:Label ID="lblSTNKRenewalWaived" runat="server"></asp:Label>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Klaim Asuransi</label>
                    <asp:Label ID="lblInsuranceClaimWaived" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tarik</label>		
                    <asp:Label ID="lblReposessionWaived" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Premi Asuransi</label>
                    <asp:Label ID="lblInsuranceWaived" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Angsuran</label>		
                    <asp:Label ID="lblInstallmentWaived" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Pelunasan dipercepat</label>
                    <asp:Label ID="lblTerminationWaived" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label><strong>Total Discount</strong></label>	
                    <strong>
                    <asp:Label ID="lblTotalDiscount" runat="server"></asp:Label></strong>	
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    DETAIL PENGAJUAN PELUNASAN DIPERCEPAT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Permintaan Asuransi</label>
                    <asp:Label ID="lblInsuranceRequest" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Asuransi Dibayar Oleh</label>
                    <asp:Label ID="lblInsurancePaidBy" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Alasan</label>
                    <asp:Label ID="lblReason" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Disetujui Oleh</label>		
                    <asp:Label ID="lblApprovedBy" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Catatan</label>
                <asp:Label runat="server" ID="lblNotes"></asp:Label>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>    
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Status</label>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Status</label>	
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>DiAjukan Oleh</label>
                <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label><strong>Total yg harus dibayar : </strong></label>
                <asp:Label ID="lblTotalAmountToBePaid" runat="server"></asp:Label>
	        </div>
        </div>       
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah Prepaid :</label>
                <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">   
                <label>Saldo :</label>
                <asp:Label ID="lblBalanceAmount" runat="server"></asp:Label>
	        </div>
        </div>       
        <div class="form_button">
            <asp:Button ID="ButtonExecute" runat="server"  Text="Execute" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>       
    </form>
</body>
</html>
