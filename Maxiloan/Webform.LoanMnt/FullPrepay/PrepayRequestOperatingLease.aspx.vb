﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Microsoft.VisualStudio.TestTools.UnitTesting
#End Region

Public Class PrepayRequestOperatingLease
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents cboApprovedBy As System.Web.UI.WebControls.DropDownList
    Protected WithEvents reqApprovedBy As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtNotes As System.Web.UI.WebControls.TextBox
    Protected WithEvents oPaymentInfo As UcFullPrepayInfoOL
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Protected WithEvents lblWOInstallment As System.Web.UI.WebControls.Label

    Protected WithEvents txtWOPenalty, txtWOPDCBounceFee, txtWOReposessionFee, txtWOLCInstall As ucNumberFormat


#Region "Property"
    Private Property ValidPrepayment() As Boolean
        Get
            Return CType(ViewState("ValidPrepayment"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("ValidPrepayment") = Value
        End Set
    End Property
    Private Property WOInsurance() As Double
        Get
            Return CType(ViewState("WOInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInsurance") = Value
        End Set
    End Property

    Private Property WOLCInstallment() As Double
        Get
            Return CType(ViewState("WOLCInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOLCInstallment") = Value
        End Set
    End Property

    Private Property WOLCInsurance() As Double
        Get
            Return CType(ViewState("WOLCInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOLCInsurance") = Value
        End Set
    End Property

    Private Property WOInstallCollFee() As Double
        Get
            Return CType(ViewState("WOInstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInstallCollFee") = Value
        End Set
    End Property

    Private Property WOInsuranceCollFee() As Double
        Get
            Return CType(ViewState("WOInsuranceCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInsuranceCollFee") = Value
        End Set
    End Property

    Private Property WOPDCBounceFee() As Double
        Get
            Return CType(ViewState("WOPDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOPDCBounceFee") = Value
        End Set
    End Property

    Private Property WOSTNKRenewalFee() As Double
        Get
            Return CType(ViewState("WOSTNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOSTNKRenewalFee") = Value
        End Set
    End Property

    Private Property WOTerminationPenaltyFee() As Double
        Get
            Return CType(ViewState("WOTerminationPenaltyFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOTerminationPenaltyFee") = Value
        End Set
    End Property

    Private Property WOReposessionFee() As Double
        Get
            Return CType(ViewState("WOReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOReposessionFee") = Value
        End Set
    End Property

    Private Property WOInstallment() As Double
        Get
            Return CType(ViewState("WOInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("WOInstallment") = Value
        End Set
    End Property

    Private Property TotalDiscount() As Double
        Get
            Return (CType(ViewState("TotalDiscount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalDiscount") = Value
        End Set
    End Property

    Private Property PrepaymentAmount() As Double
        Get
            Return (CType(ViewState("PrepaymentAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrepaymentAmount") = Value
        End Set
    End Property

    Private Property DefaultStatus() As String
        Get
            Return (CType(ViewState("DefaultStatus"), String))
        End Get
        Set(value As String)
            ViewState("DefaultStatus") = value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New FullPrepayController
    Private oCustomClass As New Parameter.FullPrepay
    Protected WithEvents txtEffectiveDate As ucDateCE
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Dim stnkrenewal As Boolean = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "FULLPREPAYLISTOL"
            txtEffectiveDate.IsRequired = True
            txtEffectiveDate.Text = DateAdd(DateInterval.Day, 1, Me.BusinessDate).ToString("dd/MM/yyyy")
            If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
                InitialValue()
                Me.TotalDiscount = 0
                Me.ApplicationID = Request.QueryString("applicationid")
                Me.BranchAgreement = Request.QueryString("branchid")
                oApprovalRequest.ReasonTypeID = "FPPOL"
                oApprovalRequest.ApprovalScheme = "FPOL"
                bindControllerNumber()
                GetPaymentInfo()

                Dim oSTNKRenewal As New Parameter.GeneralPaging
                Dim m_controller As New GeneralPagingController
                With oSTNKRenewal
                    .strConnection = GetConnectionString()
                    .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and BranchID = '" & Me.BranchAgreement & "' and status in ('REQ','REG','INV')"
                    .CurrentPage = 1
                    .PageSize = 5
                    .SortBy = ""
                    .SpName = "spGetSTNKByRenewalOnly"
                End With
                oSTNKRenewal = m_controller.GetGeneralPaging(oSTNKRenewal)
                Dim recordCount As Long = oSTNKRenewal.TotalRecords
                If recordCount > 0 Then
                    lblSTNKMessage.Visible = True
                Else
                    lblSTNKMessage.Visible = False
                End If

            End If
        End If
    End Sub
    Sub bindControllerNumber()
        With txtWOLCInstall
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOPenalty
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999999"
        End With
        With txtWOPDCBounceFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
        With txtWOReposessionFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "-99999999999"
            .RangeValidatorMaximumValue = "9999"
        End With
    End Sub
    Private Sub InitialValue()
        txtWOLCInstall.Text = "0.00"
        txtWOPenalty.Text = "0.00"
        txtWOPDCBounceFee.Text = "0.00"
        txtWOReposessionFee.Text = "0.00"
        lblTotalDiscount.Text = "0.00"
    End Sub
#End Region

#Region "Calculate"
    Private Sub DiscountCalculate()
        Me.TotalDiscount = CDbl(txtWOLCInstall.Text.Trim) + _
                                   CDbl(txtWOPDCBounceFee.Text.Trim) + _
                                   CDbl(txtWOPenalty.Text.Trim) + _
                                   CDbl(txtWOReposessionFee.Text.Trim)
        Me.AmountToBePaid = Me.PrepaymentAmount - Me.TotalDiscount
        lblTotalDiscount.Text = FormatNumber(Me.TotalDiscount, 2)
        lblTotalAmountToBePaid.Text = FormatNumber(Me.AmountToBePaid, 2)
    End Sub
    
    Private Sub GetPaymentInfo()

        With oPaymentInfo
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(txtEffectiveDate.Text)
            .BranchID = Me.BranchAgreement.Trim
            .PaymentInfo()
            lblAgreementBranch.Text = .BranchAgreement
            Me.AgreementNo = .AgreementNo
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            Me.ValidPrepayment = .IsValidPrepayment
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblPrepaidAmount.Text = FormatNumber(.ContractPrepaidAmount, 2)
            Me.PrepaidBalance = .ContractPrepaidAmount
            Me.PrepaymentAmount = .TotalPrepaymentAmount
            'lblTotalDiscount.Text = FormatNumber(Me.TotalDiscount, 2)
            Try
                lblFundingCoyName.Text = .FundingCoyName
                lblFundingPledgeStatus.Text = .FundingPledgeStatus
                lblResiduValue.Text = FormatNumber(.ResiduValue, 2)

                lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
                lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)

            Catch ex As Exception
                ex.Message.ToString()
            End Try



            If Not IsPostBack Then
                Dim oDataTable As DataTable = New DataTable
                Dim oRow As DataRow

                oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
                oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

                oRow = oDataTable.NewRow()

                oRow("ID") = "T"
                oRow("Description") = "Termination Request"
                oDataTable.Rows.Add(oRow)
                oRow = oDataTable.NewRow()
                
            End If
            Select Case .ContractStatus
                Case "INV"
                    lblContractStatus.Text = .ContractStatus
                Case "AKT"
                    lblContractStatus.Text = "Aktive"
                Case "PRP"
                    lblContractStatus.Text = "Prospect"
                Case "OSP"
                    lblContractStatus.Text = "Oustanding Pokok"
                Case "OSD"
                    lblContractStatus.Text = "Oustanding Denda"
                Case "SSD"
                    lblContractStatus.Text = "Siap Serah Terima Dokumen"
                Case "LNS"
                    lblContractStatus.Text = "Lunas"
                Case "INV"
                    lblContractStatus.Text = "Inventory"
                Case "RJC"
                    lblContractStatus.Text = "Contract Reject by Company"
                Case "CAN"
                    lblContractStatus.Text = "Contract Cancel by Customer"
            End Select
            Select Case .DefaultStatus.Trim
                Case "NA"
                    lblDefaultStatus.Text = "Stop Accrued"
                Case "WO"
                    lblDefaultStatus.Text = "Write Off"
                Case "NM"
                    lblDefaultStatus.Text = "Normal"
            End Select
            Me.DefaultStatus = .DefaultStatus
            If .IsCrossDefault Then
                lblCrossDefault.Text = "Yes"
            Else
                lblCrossDefault.Text = "No"
            End If
            lblTotalAmountToBePaid.Text = FormatNumber(Me.PrepaymentAmount - Me.TotalDiscount, 2)

            'txtWOInstallment.RangeValidatorMaximumValue = CStr(.MaximumInstallment)
            'txtWOInstallment.setErrMsg()
            'txtWOInsurance.RangeValidatorMaximumValue = CStr(.MaximumInsurance)
            'txtWOInsurance.setErrMsg()

            txtWOLCInstall.RangeValidatorMaximumValue = CStr(.MaximumLCInstallFee)
            txtWOLCInstall.setErrMsg()
            txtWOPDCBounceFee.RangeValidatorMaximumValue = CStr(.MaximumPDCBounceFee)
            txtWOPDCBounceFee.setErrMsg()
            txtWOReposessionFee.RangeValidatorMaximumValue = CStr(.MaximumReposessionFee)
            txtWOReposessionFee.setErrMsg()
            txtWOPenalty.RangeValidatorMaximumValue = CStr(.MaximumPenaltyRate)
            txtWOPenalty.setErrMsg()
        End With

      
    End Sub
#End Region

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Response.Redirect("PrepayRequestListOperatingLease.aspx")
    End Sub

    Private Sub imbCalcDiscount_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCalcDiscount.Click

        'Pengecekan diskon ini dinonaktifkan sementara
        'If ConvertDate2(txtEffectiveDate.Text) >= Me.BusinessDate Then
        GetPaymentInfo()
        DiscountCalculate()
        'Else

        '    ShowMessage(lblMessage, "Harap isi tanggal efektif > tanggal hari ini", True)
        'End If
    End Sub

    Private Sub imbCalculate_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCalculate.Click
        Dim effDate As Date = ConvertDate2(txtEffectiveDate.Text)
        lblMessage.Text = ""
        If effDate < Me.BusinessDate Then
            ShowMessage(lblMessage, "tanggal effective harus >= tanggal system", True)
            Exit Sub
        End If
        Try
            InitialValue()
            GetPaymentInfo()
            DiscountCalculate()
        Catch exp As Exception
            ShowMessage(lblMessage, "Tanggal Salah", True)
        End Try
    End Sub

#Region "Report"
    Private Sub imbPrintTrialCalculation_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonPrintTrialCalculation.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim objreport As New ReportDocument
            objreport = New FullPrepaymentPrintTrialInternalOL

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchAgreement
                .ApplicationID = Me.ApplicationID
                .BusinessDate = ConvertDate2(txtEffectiveDate.Text)
                .totalDiscount = CDbl(lblTotalDiscount.Text)
            End With
            Dim oData As New DataSet
            oData = oController.PrintTrialOL(oCustomClass)
            objreport.SetDataSource(oData)
            Dim discrete As ParameterDiscreteValue
            Dim ParamField As ParameterFieldDefinition
            Dim CurrentValue As ParameterValues
            ParamField = objreport.DataDefinition.ParameterFields("BranchName")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.BranchName
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamField = objreport.DataDefinition.ParameterFields("AsOf")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.BusinessDate.ToString("MM/dd/yyyy")
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamField = objreport.DataDefinition.ParameterFields("LoginID")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.Loginid
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            ParamField = objreport.DataDefinition.ParameterFields("EffectiveDate")
            discrete = New ParameterDiscreteValue
            discrete.Value = ConvertDate2(txtEffectiveDate.Text).ToString("MM/dd/yyyy")
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            Dim strFileLocation As String
            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID & Me.Loginid & "PrintTrialCalculationOL.pdf"
            DiskOpts.DiskFileName = strFileLocation
            objreport.ExportOptions.DestinationOptions = DiskOpts
            objreport.Export()

            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PrintTrialCalculationOL.pdf"


            'Response.Write("<script language = javascript>" & vbCrLf _
            '            & "var x = screen.width; " & vbCrLf _
            '            & "var y = screen.height; " & vbCrLf _
            '            & "window.open('" & strFileLocation & "','PrepayTrial', 'left=0, top=0, width=600, height=700, menubar=0, scrollbars=yes') " & vbCrLf _
            '            & "</script>")


            Dim strScript As New StringBuilder

            strScript.Append("var x = screen.width; ")
            strScript.Append("var y = screen.height; ")
            strScript.Append("window.open('" & strFileLocation & "','PrepayTrialOL', 'left=0, top=0, width= ' + x + ',  height=' + y + ', menubar=0, scrollbars=yes');")

            ScriptManager.RegisterStartupScript(Page, Me.GetType, "PrintPrepayTrialOL", strScript.ToString, True)
        End If
    End Sub
#End Region

    Private Sub imbRequest_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonRequest.Click
        If Not Me.ValidPrepayment Then ShowMessage(lblMessage, "Pengajuan Pelunasan dipercepat tidak dapat diproses", True)

        'comment dulu
        'If Me.BranchAgreement.Trim <> Me.sesBranchId.Trim.Replace("'", "") Then lblMessage.Text = "Request Prepayment can not be process, your branch not access"            

        'Pengecekan diskon ini dinonaktifkan sementara
        'If ConvertDate2(txtEffectiveDate.Text) >= Me.BusinessDate Then ShowMessage(lblMessage, "Harap isi tanggal efektif > tanggal hari ini", True)

        If (Me.DefaultStatus.Trim = "WO") Then
            ShowMessage(lblMessage, "Default status WO, proses tidak dapat dilanjutkan", True)
            Exit Sub
        End If


        If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            GetPaymentInfo()
            DiscountCalculate()
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchAgreement
                .ApplicationID = Me.ApplicationID
                .ValueDate = ConvertDate2(txtEffectiveDate.Text)
                .BusinessDate = Me.BusinessDate
                .PrepaymentAmount = oPaymentInfo.TotalPrepaymentAmount
                .PrepaymentType = "NM"

                .RepossessionFeeWaived = CDbl(txtWOReposessionFee.Text.Trim)
                .TerminationPenaltyWaived = CDbl(txtWOPenalty.Text.Trim)
                .PDCBounceFeeWaived = CDbl(txtWOPDCBounceFee.Text.Trim)
                .InstallmentCollFeeWaived = 0
                .InsuranceCollFeeWaived = 0
                .LcInstallmentWaived = CDbl(txtWOLCInstall.Text.Trim)
                .LcInsuranceWaived = 0
                .InsuranceDueWaived = 0
                .InstallmentWaived = 0
                .STNKRenewalFeeWaived = 0
                .InsuranceClaimExpenseWaived = 0

                .ReasonID = oApprovalRequest.ReasonID
                .LoginId = Me.Loginid
                .RequestTo = oApprovalRequest.ToBeApprove
                .PrepaymentNotes = oApprovalRequest.Notes
                .InsuranceTerminationFlag = "T"
                .TerminationPenalty = oPaymentInfo.TerminationPenalty

            End With
            Try
                oController.FullPrepayRequestOL(oCustomClass)
                Response.Redirect("PrepayRequestListOperatingLease.aspx")
            Catch exp As Exception
                ShowMessage(lblMessage, "Pengajuan Pelunasan dipercepat Gagal" & exp.Message, True)
            End Try
        End If
    End Sub

End Class