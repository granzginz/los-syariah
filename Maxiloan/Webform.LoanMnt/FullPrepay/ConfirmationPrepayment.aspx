﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ConfirmationPrepayment.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ConfirmationPrepayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ConfirmationPrepayment</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>   
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">           
                <h3>
                    PEMBATALAN KONTRAK
                </h3>
            </div>
        </div>   
        <asp:Panel ID="Panel1" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:Label ID="lblAgreementNo" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btn_Trans_view_back" runat="server" Text="Back" CssClass="pbttn"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="btnExecute" runat="server" Text="Execute" CssClass="pbttn"
                CausesValidation="False"></asp:Button>
	    </div>              
    </asp:Panel>
    </form>
</body>
</html>
