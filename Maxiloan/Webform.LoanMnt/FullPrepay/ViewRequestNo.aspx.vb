﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class ViewRequestNo
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private ReadOnly Property PrepaymentNo() As String
        Get
            Return CType(Request.QueryString("prepaymentno"), String)
        End Get
    End Property
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As ProcessMode
        Get
            Return (CType(viewstate("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            viewstate("Mode") = Value
        End Set
    End Property
    'Mode 0 = Cancel
    'Mode 1 = Execute
    'Mode 2 = View
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum

    Private Property AmountToBePaid() As Double
        Get
            Return (CType(viewstate("AmountToBePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("AmountToBePaid") = Value
        End Set
    End Property

    Private Property PrepaidAmount() As Double
        Get
            Return (CType(viewstate("PrepaidAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("PrepaidAmount") = Value
        End Set
    End Property

    Private Property PrepaymentAmount() As Double
        Get
            Return (CType(viewstate("PrepaymentAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("PrepaymentAmount") = Value
        End Set
    End Property

    Private Property TotalDiscount() As Double
        Get
            Return (CType(viewstate("TotalDiscount"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalDiscount") = Value
        End Set
    End Property
    Private ReadOnly Property CSSStyle() As String
        Get
            Return Request("Style").ToString
        End Get
    End Property
    Property CustID() As String
        Get
            Return viewstate("CustId").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustId") = Value
        End Set
    End Property
    Private Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Private Property JournalNo() As String
        Get
            Return (CType(ViewState("JournalNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("JournalNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private oCustomClassFullPrepay As New Parameter.FullPrepay
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "VIEWPREPAYMENT"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then

                InitializeForm()
                DoBind()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            Me.JournalNo = Request.QueryString("JournalNo")
        End If
        hdtr_nomor.Value = Me.JournalNo
    End Sub
#End Region

    Private Sub InitializeForm()
        lblTitle.Text = "View Prepayment No."
    End Sub

    Private Sub DoBind()
        Dim customerid As Label
        Dim totaldiscount As Double
        Dim totalprepayment As Double
        Dim oCustomClassFullPrepay As New Parameter.FullPrepay
        Dim oControllerPrepayCancelation As New FullPrepayController
        With oCustomClassFullPrepay
            .strConnection = GetConnectionString
            .PrepaymentNo = Me.PrepaymentNo
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With
        oCustomClassFullPrepay = oControllerPrepayCancelation.FullPrepayInfo(oCustomClassFullPrepay)
        With oCustomClassFullPrepay
            'If Me.Mode = ProcessMode.Cancel Then
            '    lblTitle.Text = "PREPAYMENT CANCELLATION"
            'ElseIf Me.Mode = ProcessMode.Execute Then
            '    lblTitle.Text = "PREPAYMENT EXECUTION"
            'ElseIf Me.Mode = ProcessMode.View Then
            '    lblTitle.Text = "VIEW PREPAYMENT REQUEST"
            'End If
            lblPrepaymentNo.Text = .PrepaymentNo
            lblRequestdate.Text = .BusinessDate.ToString("dd/MM/yyyy")
            hypAgreementNo.Text = .Agreementno
            hypCustomerName.Text = .CustomerName
            Me.CustID = .CustomerID
            Select Case .PrepaymentType.Trim
                Case "DI"
                    lblPrepaymentType.Text = "Daily Interest"
                Case "ED"
                    lblPrepaymentType.Text = "Earn Discount"
                Case "NM"
                    lblPrepaymentType.Text = "Standard"
                Case " PD"
                    lblPrepaymentType.Text = "Principal Discount"
            End Select
            lblEffectiveDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            lblOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
            lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
            lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
            lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
            lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
            lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
            lblInsuranceClaimFee.Text = FormatNumber(.InsuranceClaimExpense, 2)

            lblInstallCollFee.Text = FormatNumber(.InstallmentCollFee, 2)
            lblInsuranceCollFee.Text = FormatNumber(.InsuranceCollFee, 2)
            lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
            lblSTNKRenewalFee.Text = FormatNumber(.STNKRenewalFee, 2)
            lblTerminationFee.Text = FormatNumber(.TerminationPenalty, 2)
            lblRepoFee.Text = FormatNumber(.RepossessionFee, 2)
            lblInsuranceClaimFee.Text = FormatNumber(.InsuranceClaimExpense, 2)
            lblTotalPrepaymentAmount.Text = FormatNumber(.PrepaymentAmount, 2)
            totalprepayment = .OutstandingPrincipal + .AccruedInterest + _
                              .InsuranceDue + .LcInstallment + .LcInsurance + _
                              .InstallmentCollFee + .InsuranceCollFee + .PDCBounceFee + _
                              .STNKRenewalFee + .TerminationPenalty + .RepossessionFee + .InsuranceClaimExpense
            Me.PrepaymentAmount = totalprepayment
            Me.AmountToBePaid = FormatNumber(.AmountToBePaid, 2)
            Select Case .ContractStatus.Trim
                Case "AKT"
                    lblContractStatus.Text = "Aktive"
                Case "PRP"
                    lblContractStatus.Text = "Prospect"
                Case "OSP"
                    lblContractStatus.Text = "Oustanding Pokok"
                Case "OSD"
                    lblContractStatus.Text = "Oustanding Denda"
                Case "SSD"
                    lblContractStatus.Text = "Siap Serah Terima Dokumen"
                Case "LNS"
                    lblContractStatus.Text = "Lunas"
                Case "INV"
                    lblContractStatus.Text = "Expired Cause Invetaried"
                Case "RJC"
                    lblContractStatus.Text = "Contract Reject By Company"
                Case "CAN"
                    lblContractStatus.Text = "Contract Cancel by Customer"
            End Select
            Select Case .DefaultStatus.Trim
                Case "NM"
                    lblDefaultStatus.Text = "Normal"
                Case "NA"
                    lblDefaultStatus.Text = "Stop Accrued"
                Case "WO"
                    lblDefaultStatus.Text = "Write Off"
            End Select

            lblCrossDefault.Text = CStr(IIf(.CrossDefault, "Yes", "No"))

            lblLCInstallWaived.Text = FormatNumber(.LcInstallmentWaived, 2)
            lblLCInsuranceWaived.Text = FormatNumber(.LcInsuranceWaived, 2)
            lblInstallCollWaived.Text = FormatNumber(.InstallmentCollFeeWaived, 2)
            lblInsuranceCollWaived.Text = FormatNumber(.InsuranceCollFeeWaived, 2)
            lblPDCBounceWaived.Text = FormatNumber(.PDCBounceFeeWaived, 2)
            lblSTNKRenewalWaived.Text = FormatNumber(.STNKRenewalFeeWaived, 2)
            lblInsuranceClaimWaived.Text = FormatNumber(.InsuranceClaimExpense, 2)
            lblTerminationWaived.Text = FormatNumber(.TerminationPenaltyWaived, 2)
            lblReposessionWaived.Text = FormatNumber(.RepossessionFeeWaived, 2)
            lblInsuranceWaived.Text = FormatNumber(.InsuranceDueWaived, 2)
            lblInstallmentWaived.Text = FormatNumber(.InstallmentWaived, 2)

            totaldiscount = .LcInstallmentWaived + .LcInsuranceWaived + .InstallmentCollFeeWaived + _
                            .InsuranceCollFeeWaived + .PDCBounceFeeWaived + .STNKRenewalFeeWaived + _
                            .TerminationPenaltyWaived + .RepossessionFeeWaived + .InsuranceDueWaived + _
                            .InstallmentWaived + .InsuranceClaimExpenseWaived
            Me.TotalDiscount = totaldiscount
            'AC = At Cost
            'CU = Customer

            Select Case .InsuranceTerminationFlag.Trim
                Case "T"
                    lblInsuranceRequest.Text = "Termination Request"
                Case "C"
                    lblInsuranceRequest.Text = "Release Clausal Leasing"
            End Select

            lblTotalDiscount.Text = FormatNumber(totaldiscount)

            Select Case .InsurancePaidBy.Trim
                Case "AC"
                    lblInsurancePaidBy.Text = "AT Cost"
                Case "CU"
                    lblInsurancePaidBy.Text = "Customer"
            End Select

            lblReason.Text = .ReasonDescription
            lblApprovedBy.Text = .UserApproval
            lblNotes.Text = .PrepaymentNotes
            Select Case .PrepaymentStatus.Trim
                Case "R"
                    lblStatus.Text = "Request"
                Case "A"
                    lblStatus.Text = "Approve"
                Case "C"
                    lblStatus.Text = "Cancel"
                Case "D"
                    lblStatus.Text = "Reject"
            End Select

            lblFundingCoyName.Text = .FundingCoyName
            lblFundingPledgeStatus.Text = .FundingPledgeStatus
            lblResiduValue.Text = FormatNumber(.ResiduValue, 2)

            lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
            lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
            lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)

            lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
            lblRequestBy.Text = .UserRequest

            'lblTotalAmountToBePaid.Text = FormatNumber(totalprepayment - totaldiscount, 2)
            lblTotalAmountToBePaid.Text = FormatNumber(Me.AmountToBePaid, 2)
            lblPrepaidAmount.Text = FormatNumber(.Prepaid, 2)
            Me.PrepaymentAmount = .Prepaid
            'lblBalanceAmount.Text = FormatNumber(.Prepaid - (totalprepayment - totaldiscount), 2)
            lblBalanceAmount.Text = FormatNumber(.Prepaid - (Me.AmountToBePaid - totaldiscount), 2)
            'Me.AmountToBePaid = .Prepaid - (totalprepayment - totaldiscount)
            Me.ApplicationId = .ApplicationID
        End With
        hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & Me.CSSStyle & "','" & Me.ApplicationId.Trim & "')"
        hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & Me.CSSStyle & "', '" & Me.CustID & "')"
    End Sub

End Class