﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class PrepaymentInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            Return CType(Viewstate("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalAmount") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New FullPrepayController
    Private oCustomClass As New Parameter.FullPrepay
    Private m_controller As New DataUserControlController    
    Protected WithEvents oBranch As UcBranchAll
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "PREPAYMENTINQ"
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        If Not IsPostBack Then
            Me.CmdWhere = ""
            Me.SortBy = ""
        End If

    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True



        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        BindGridInqPCPrepayment(Me.CmdWhere, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridInqPCPrepayment(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridInqPCPrepayment(Me.CmdWhere, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGridInqPCPrepayment(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqPrepayment As New Parameter.FullPrepay

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
        End With

        oInqPrepayment = oController.InqPrepayment(oCustomClass)

        With oInqPrepayment
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInqPrepayment.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = sortBy
        DtgPrepaymentInquiry.DataSource = dtvEntity
        Try
            DtgPrepaymentInquiry.DataBind()
        Catch
            DtgPrepaymentInquiry.CurrentPageIndex = 0
            DtgPrepaymentInquiry.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
    Private Sub DtgPrepaymentInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPrepaymentInquiry.ItemDataBound
        Dim hyAgreementNo As HyperLink
        Dim HyCustomerName As HyperLink
        Dim hyRequestNo As HyperLink
        Dim lblCustID As Label
        Dim lblApplicationId As Label
        Dim lblJournalNo As Label

        If e.Item.ItemIndex >= 0 Then
            'Dim oInqInsuranceInvoice As New Parameter.InsInqEntities
            'Dim strconn As String = getConnectionString()
            'Dim dt As New DataTable

            hyAgreementNo = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            HyCustomerName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblJournalNo = CType(e.Item.FindControl("lblJournalNo"), Label)
            If hyAgreementNo.Text.Trim.Length > 0 Then
                hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
            If lblCustID.Text.Trim.Length > 0 Then
                HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
            hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            hyRequestNo.NavigateUrl = LinkToViewRequestNo("ACCMNT", hyRequestNo.Text.Trim, lblJournalNo.Text.Trim)

        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            Dim filterby As String
            Me.SortBy = ""
            Me.CmdWhere = ""
            cmdwhere = ""
            filterby = ""

            If oBranch.BranchID <> "0" Then
                If txtSearch.Text.Trim <> "" Then

                    cmdwhere = String.Format("{0}   {1} like '%{2}%' and ", cmdwhere, cboSearch.SelectedItem.Value.Trim, txtSearch.Text.Trim.Replace("'", "''").Replace("%", ""))
                    filterby = String.Format("{0} = {1} and ", cboSearch.SelectedItem.Value.Trim, txtSearch.Text.Trim)

                    'If cboSearch.SelectedItem.Value.Trim = "1" Then
                    '    If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '        cmdwhere = cmdwhere + "prepayment.PrepaymentNo like '" & txtSearch.Text.Trim & "' and "
                    '    Else
                    '        cmdwhere = cmdwhere + "prepayment.PrepaymentNo LIKE '%" & txtSearch.Text.Trim & "%' and "
                    '    End If
                    '    filterby = "Prepayment No. = " & txtSearch.Text.Trim & " and "
                    'ElseIf cboSearch.SelectedItem.Value.Trim = "2" Then
                    '    If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '        cmdwhere = cmdwhere + "Agreement.AgreementNo like '" & txtSearch.Text.Trim & "' and "
                    '    Else
                    '        cmdwhere = cmdwhere + "Agreement.AgreementNo LIKE '%" & txtSearch.Text.Trim & "%' and "
                    '    End If
                    '    filterby = "Agreement No. = " & txtSearch.Text.Trim & " and "
                    'ElseIf cboSearch.SelectedItem.Value.Trim = "3" Then
                    '    If Not IsNumeric(txtSearch.Text.Trim) Then
                    '        If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '            cmdwhere = cmdwhere + "Customer.Name like '" & txtSearch.Text.Trim & "' and "
                    '        Else
                    '            cmdwhere = cmdwhere + "Customer.Name LIKE '%" & txtSearch.Text.Trim & "%' and "
                    '        End If
                    '        filterby = "Customer Name = " & txtSearch.Text.Trim & " and "
                    '    End If
                    'End If
                End If
            End If

            If txtEffDate.Text.Trim <> "" Then
                cmdwhere = cmdwhere + "prepayment.EfectiveDate <= '" & ConvertDate2(txtEffDate.Text.Trim).ToString("yyyyMMdd") & "' and "
                filterby = filterby + "Effective Date <= " & txtEffDate.Text.Trim & " and "
            End If
            If cbostatus.SelectedItem.Value.Trim <> "0" Then
                cmdwhere = cmdwhere + "prepayment.prepaymentstatus = '" & cbostatus.SelectedItem.Value.Trim & "' and "
                filterby = filterby + "Status = " & cbostatus.SelectedItem.Text.Trim & " and "
            End If
            If oBranch.BranchID.Trim <> "ALL" Then
                cmdwhere = cmdwhere + "prepayment.branchID = '" & oBranch.BranchID.Trim & "'"
            Else
                If cmdwhere.Trim <> "" Then
                    cmdwhere = Left(cmdwhere, Len(cmdwhere.Trim) - 4)
                End If
            End If
            Me.CmdWhere = cmdwhere
            If filterby <> "" Then
                filterby = Left(filterby, Len(filterby.Trim) - 4)
            End If
            Me.FilterBy = filterby
            BindGridInqPCPrepayment(Me.CmdWhere, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If

    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Server.Transfer("PrepaymentInq.aspx")
    End Sub

    Private Sub DtgPrepaymentInquiry_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgPrepaymentInquiry.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInqPCPrepayment(Me.CmdWhere, Me.SortBy)
    End Sub

#Region "linkTo"
    Function LinkToViewRequestNo(ByVal strStyle As String, ByVal strRequestNo As String, ByVal lblJournalNo As String) As String
        Return "javascript:OpenWindowViewRequestNo('" & strStyle & "','" & strRequestNo & "','" & lblJournalNo & "')"
    End Function
#End Region

End Class