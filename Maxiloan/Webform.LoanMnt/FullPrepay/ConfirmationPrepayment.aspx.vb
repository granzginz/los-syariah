﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ConfirmationPrepayment
    Inherits Maxiloan.Webform.WebBased

    Private Sub btnExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spPrepaymentByError"

            objCommand.Parameters.Add("@agreementNo", SqlDbType.Char, 20).Value = lblAgreementNo.Text.Trim
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Value = Me.BranchID
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
            Server.Transfer("AgreementListPrepayment.aspx")
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            lblAgreementNo.Text = Request("AgreementNo")
            Me.BranchID = Replace(sesBranchId, "'", "")
        End If
    End Sub

    Private Sub btn_Trans_view_back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Trans_view_back.Click
        Server.Transfer("AgreementListPrepayment.aspx")
    End Sub

End Class