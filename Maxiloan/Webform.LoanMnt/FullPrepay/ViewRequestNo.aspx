﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewRequestNo.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ViewRequestNo" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewRequestNo</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function fClose() {
            window.close();
            return false;
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
     <link rel="Stylesheet" href="../../Include/default/easyui.css" type="text/css">
    <script type="text/javascript" src="../../js/jquery.easyui.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_single">
    <div id="tt" class="easyui-tabs" border="false" style="height: 630px">
    <div title="View">
    <asp:HiddenField ID="hdtr_nomor" runat="server" /> 
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">           
                <h3>
                    <asp:Literal ID="lblTitle" EnableViewState="False" runat="server"></asp:Literal>
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>No Request</label>
                    <asp:Literal ID="lblPrepaymentNo" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Request</label>		
                    <asp:Literal ID="lblRequestdate" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" EnableViewState="False" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Nama Customer</label>		
                    <asp:HyperLink ID="hypCustomerName" EnableViewState="False" runat="server"></asp:HyperLink>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Jenis Prepayment</label>
                    <asp:Literal ID="lblPrepaymentType" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">			
                    <label>Tanggal Efektif</label>
                    <asp:Literal ID="lblEffectiveDate" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    DETAIL INFORMASI PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Sisa Pokok</label>
                    <asp:Literal ID="lblOutstandingPrincipal" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">
                    <label>Accrued Interest</label>			
                    <asp:Literal ID="lblAccruedInterest" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Margin Jatuh Tempo</label>	
                     <asp:Literal ID="lblInstallmentDue" visible="true" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Premi Asuransi</label>		
                    <asp:Literal ID="lblInsuranceDue" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Keterlambatan Angsuran</label>
                    <asp:Literal ID="lblLCInstall" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">			
                    <label>Denda Keterlambatan Asuransi</label>
                    <asp:Literal ID="lblLCInsurance" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tagih Angsuran</label>
                    <asp:Literal ID="lblInstallCollFee" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tagih Asuransi</label>		
                    <asp:Literal ID="lblInsuranceCollFee" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tolakan PDC</label>
                    <asp:Literal ID="lblPDCBounceFee" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Perpanjang STNK/BBN</label>	
                    <asp:Literal ID="lblSTNKRenewalFee" EnableViewState="False" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Pelunasan dipercepat</label>
                    <asp:Literal ID="lblTerminationFee" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tarik</label>		
                    <asp:Literal ID="lblRepoFee" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Klaim Asuransi</label>
                    <asp:Literal ID="lblInsuranceClaimFee" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">		
                    <label><strong>Total Pelunasan Dipercepat</strong></label>	                  
                    <asp:Literal ID="lblTotalPrepaymentAmount" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    STATUS KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Status Kontrak</label>
                    <asp:Literal ID="lblContractStatus" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Status Default</label>	
                    <asp:Literal ID="lblDefaultStatus" EnableViewState="False" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Jumlah Angsuran</label>
                    <asp:Literal ID="lblInstallmentAmount" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Jatuh Tempo Berikutnya</label>	
                    <asp:Literal ID="lblNextInstallmentDate" runat="server"></asp:Literal>&nbsp;&nbsp;|&nbsp;Installment&nbsp;
                    <asp:Literal ID="lblNextInstallmentNumber" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left"> 
                    <label>Funding Bank</label>
                    <asp:Literal ID="lblFundingCoyName" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">		
                    <label>Status Penjaminan</label>
                    <asp:Literal ID="lblFundingPledgeStatus" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Nilai Residu</label>
                    <asp:Literal ID="lblResiduValue" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Cross Default</label>		
                    <asp:Literal ID="lblCrossDefault" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    DISCOUNT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Keterlambatan Angsuran</label>
                    <asp:Literal ID="lblLCInstallWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Denda Keterlambatan Asuransi</label>		
                    <asp:Literal ID="lblLCInsuranceWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tagih Angsuran</label>
                    <asp:Literal ID="lblInstallCollWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">		
                    <label>Biaya Tagih Asuransi</label>	
                    <asp:Literal ID="lblInsuranceCollWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Tolakan PDC</label>
                    <asp:Literal ID="lblPDCBounceWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Perpanjangan STNK/BBN</label>	
                    <asp:Literal ID="lblSTNKRenewalWaived" EnableViewState="False" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Biaya Klaim Asuransi</label>
                    <asp:Literal ID="lblInsuranceClaimWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tarik</label>	
                    <asp:Literal ID="lblReposessionWaived" EnableViewState="False" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Premi Asuransi</label>
                    <asp:Literal ID="lblInsuranceWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Angsuran</label>		
                    <asp:Literal ID="lblInstallmentWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Denda Pelunasan Dipercepat</label>
                    <asp:Literal ID="lblTerminationWaived" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">		
                    <label>Total Discount</label>	
                    <asp:Literal ID="lblTotalDiscount" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>               
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    DETAIL PENGAJUAN PELUNASAN DIPERCEPAT
                </h4>
            </div>
        </div>
         <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Permintaan Asuransi</label>
                    <asp:Literal ID="lblInsuranceRequest" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Asuransi Dibayar Oleh</label>	
                    <asp:Literal ID="lblInsurancePaidBy" EnableViewState="False" runat="server"></asp:Literal>	
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Alasan</label>
                    <asp:Literal ID="lblReason" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">	
                    <label>Disetujui Oleh</label>		
                    <asp:Literal ID="lblApprovedBy" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Catatan</label>
                <asp:Literal ID="lblNotes" EnableViewState="False" runat="server"></asp:Literal>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Status</label>
                    <asp:Literal ID="lblStatus" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
		        <div class="form_right">			
                    <label>Tanggal Status</label>
                    <asp:Literal ID="lblStatusDate" EnableViewState="False" runat="server"></asp:Literal>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Diajukan Oleh</label>
                <asp:Literal ID="lblRequestBy" EnableViewState="False" runat="server"></asp:Literal>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">                
                <label><strong>Total yg Harus dibayar : </strong></label>
                <strong>
                <asp:Literal ID="lblTotalAmountToBePaid" EnableViewState="False" runat="server"></asp:Literal></strong>		        
	        </div>
        </div>       
        <div class="form_box">
	        <div class="form_single">
                <label> Jumlah Prepaid :</label>
                <asp:Literal ID="lblPrepaidAmount" EnableViewState="False" runat="server"></asp:Literal>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Saldo :</label>
                <asp:Literal ID="lblBalanceAmount" EnableViewState="False" runat="server"></asp:Literal>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">

	        </div>
        </div>        
        <div class="form_button">            
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
        </asp:Button>     
    </div>
       
           
    </div>
          <div title="Jurnal"> 
            <iframe id="IframeJournal" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
            width: 100%; height: 100%"></iframe>
        </div>
    </div>
    </div>
    </form>
</body>
</html> 
 <script type="text/javascript">
        $('#tt').tabs({
            border: false,
            onSelect: function (title) {
                var BranchId = $("#hdBranchId").val();
                var VoucherNo = $("#hdVoucherNo").val();
                var tr_nomor = $("#hdtr_nomor").val(); 
                if (title == "Jurnal") {
                    $('#IframeJournal').attr('src', '../../Webform.GL/JournalInquiryView.aspx?style=ACCMNT&tr_nomor=' + tr_nomor);
                } 
            }
        });
    </script> 