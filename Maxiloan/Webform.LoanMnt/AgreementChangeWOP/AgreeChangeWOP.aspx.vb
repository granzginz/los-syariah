﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AgreeChangeWOP
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents txtAdminFee As ucNumberFormat

#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property



    Private Property WOP() As String
        Get
            Return (CType(Viewstate("WOP"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("WOP") = Value
        End Set
    End Property


#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.AChangeWOP
    Private oController As New AChangeWOPController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private m_controller As New ApplicationController
    Dim oApplication As New Parameter.Application
#End Region

#Region "Declare Variable"

#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "AGREEWOP"
        If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                With txtAdminFee
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("Branchid")
                oPaymentInfo.IsTitle = True
                DoBind()
            End If
        End If
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        CallPaymentInfo()
        GetList()
    End Sub
#End Region

#Region "CallPaymentInfo"
    Sub CallPaymentInfo()
        With oPaymentInfo
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        If CDbl(oPaymentInfo.TotOSOverDue) > 0 Then
            ButtonRequest.Visible = False
            ShowMessage(lblMessage, "Jumlah Tunggakan harus dibayar", True)
            Exit Sub
        End If
    End Sub
#End Region

#Region "GetList"
    Sub GetList()
        Dim inWOP As String
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.GetListWOP(oCustomClass)
        With oCustomClass

            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            lblNextInstDueDate.Text = .NextInstallmentDueDate.ToString("dd/MM/yyyy")
            lblNextInstDueNo.Text = CStr(.NextInstallmentNumber)
            lblWOP.Text = .WOPDesc
            inWOP = .WOP.Trim
            Me.WOP = .WOP.Trim
            lblInstallmentAmount.Text = CStr(FormatNumber(.InstallmentAmount, 2))
            lblbranch.Text = .BranchName.Trim
            txtAdminFee.Text = CStr(.AdminFee)
            lblPrepaidAmount.Text = CStr(FormatNumber(.Prepaid, 2))
        End With
    End Sub
#End Region

#Region "Request"
    Private Sub imbRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRequest.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            With oCustomClass
                If cboWOP.SelectedItem.Value = Me.WOP Then
                    ShowMessage(lblMessage, "Cara pembayaran harus berbeda dengan sekarang", True)
                    Exit Sub
                ElseIf CDbl(txtAdminFee.Text) > CDbl(lblPrepaidAmount.Text) Then
                    ShowMessage(lblMessage, "Biaya Administrasi harus <= Jumlah Prepaid", True)
                    Exit Sub
                End If

                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .WOP = Me.WOP
                .WOPToBe = cboWOP.SelectedItem.Value
                .BusinessDate = Me.BusinessDate
                .Agreementno = lblAgreementNo.Text.Trim
                .LoginId = Me.Loginid
                .Notes = txtNotes.Text.Trim
                .AdminFee = CDbl(txtAdminFee.Text.Trim)
                .NextInstallmentNumber = CInt(lblNextInstDueNo.Text.Trim)
                .CoyID = Me.SesCompanyID

                Try
                    oController.WOPChange(oCustomClass)
                    Server.Transfer("AgreeChangeWOPList.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                Catch exp As Exception
                    ShowMessage(lblMessage, "Ganti Cara Bayar Gagal", True)
                End Try

            End With
        End If

    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("AgreeChangeWOPList.aspx")
    End Sub
#End Region

End Class