﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgreeChangeWOP.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.AgreeChangeWOP" %>

<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agreement Change WOP</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                GANTI CARA PEMBAYARAN
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Cabang</label>
                <asp:Label ID="lblbranch" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer</label>
                <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ANGSURAN JATUH TEMPO PER -&nbsp;
                <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <uc1:ucpaymentinfo id="oPaymentInfo" runat="server">
                </uc1:ucpaymentinfo>
    <div class="form_title">
        <div class="form_single">
            <h4>
                INFORMASI CARA PEMBAYARAN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Jatuh Tempo Angsuran Berikut</label>
                <asp:Label ID="lblNextInstDueDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Angsuran Berikut</label>
                <asp:Label ID="lblNextInstDueNo" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Jumlah Angsuran</label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Cara Pembayaran</label>
                <asp:Label ID="lblWOP" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                GANTI CARA PEMBAYARAN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cara Pembayaran Baru</label>
                <asp:DropDownList ID="cboWOP" runat="server">
                <asp:ListItem Value="CA" Selected="True">Cash</asp:ListItem>
                <asp:ListItem Value="TF">Transfer</asp:ListItem>
                <asp:ListItem Value="PD">PDC</asp:ListItem>
                <asp:ListItem Value="PM">Pickup Monthly</asp:ListItem>
                <asp:ListItem Value="PG">Potong Gaji</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Administrasi</label>
            <uc1:ucnumberformat id="txtAdminFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Catatan</label>
            <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jumlah Prepaid</label>
            <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonRequest" runat="server" Text="Request" CssClass="small button blue"
            CausesValidation="true"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
