﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class StopAccruedList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            Me.FormID = "STOPACCRUEDLIST"
            oSearchBy.ListData = "Agreementno, No Kontrak-Name, Nama Konsumen-Address, Alamat-InstallmentAmount, Angsuran"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                Initialize()
            End If
        End If
    End Sub

    Private Sub Initialize()
        pnlDatagrid.Visible = False
        DtgAgree.Visible = False
    End Sub

    'Private Sub DoBind()
    '    ThreadBind()
    '    Dim ThreadBinding As New Thread(AddressOf ThreadBind)
    '    ThreadBinding.Priority = ThreadPriority.Highest
    '    ThreadBinding.Start()
    'End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        'With oCustomClass
        '    .strConnection = GetConnectionString
        '    .WhereCond = strSearch
        '    .CurrentPage = currentPage
        '    .PageSize = pageSize
        '    .SortBy = strSort
        'End With

        'oCustomClass = oController.AgreementList(oCustomClass)
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAgreementListPaging2"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        recordCount = oPaging.TotalRecords
        DtgAgree.DataSource = oPaging.ListData

        Try
            DtgAgree.DataBind()
        Catch exp As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()

            ShowMessage(lblMessage, exp.Message, True)
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        'Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            'hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            'hypReceive.NavigateUrl = "InstallRcv.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & oBranch.BranchID.Trim

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Dim strSearch As New StringBuilder

        strSearch.Append(" branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
        strSearch.Append(" and contractstatus ='AKT'  and prepaidholdstatus = 'NM' ")

        Me.SearchBy = strSearch.ToString
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder

        strSearch.Append(" branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
        strSearch.Append(" and contractstatus IN ('AKT','ICP','OSP')  and prepaidholdstatus = 'NM' ")
        'Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' "

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
            'Me.SearchBy &= " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub


    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "SA"
                Dim lblApplicationid As HyperLink
                lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                oCustomClass.ApplicationID = lblApplicationid.Text
                oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                HttpContext.Current.Items("Receive") = oCustomClass
                Server.Transfer("StopAccruedRequest.aspx")
        End Select
    End Sub
End Class