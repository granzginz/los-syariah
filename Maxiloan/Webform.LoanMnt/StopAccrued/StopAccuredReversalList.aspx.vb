﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class StopAccuredReversalList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private STR_FORM_ID As String = "StopAccReversal"
    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New Controller.GeneralPagingController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If


        If Not IsPostBack Then
            Me.FormID = STR_FORM_ID
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                oSearchBy.ListData = "Agreementno, No Kontrak-Name, Nama Konsumen-Address, Alamat-InstallmentAmount, Angsuran"
                oSearchBy.BindData()

            End If

        End If
        ' pnlDatagrid.Visible = False
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        ' Dim hypReverse As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            ' hypReverse = CType(e.Item.FindControl("hypReverse"), HyperLink)
            ' hypReverse.NavigateUrl = "StopAccuredReversal.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & Replace(Me.sesBranchId, "'", "")

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
    End Sub

#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            lblMessage.Text = ""
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spStopAccruedReversalPaging"
            End With

            oCustomClass = oController.GetGeneralPaging(oCustomClass)

            If oCustomClass Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            'DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            'pnlList.Visible = True
            pnlDatagrid.Visible = True

            'BuildMasterDetailCombo()


        Catch ex As Exception

        End Try
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & Replace(Me.sesBranchId, "'", "") & "' ")

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        '        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Reverse"
                Dim lblApplicationid As HyperLink
                lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)

                Response.Redirect("StopAccruedReversal.aspx?ApplicationID=" & lblApplicationid.Text.Trim & "&BranchID=" & Replace(Me.sesBranchId, "'", ""))

        End Select
    End Sub

End Class