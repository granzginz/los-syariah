﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StopAccruedView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.StopAccruedView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>StopAccruedView</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function fClose() {
            window.close();
            return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">  <div class="title_strip"> </div>
        <div class="form_single">
            <h4>
                VIEW - STOP HITUNG MARGIN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Request
            </label>
            <asp:Label ID="lblRequestNaNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:Label ID="lblRequestdate" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="hypAgreementNo" runat="server" EnableViewState="False"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Konsumen
            </label>
            <asp:HyperLink ID="hypCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tgl Pembayaran Terakhir
            </label>
            <asp:Label ID="lblLastPaymentDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Hari Telat
            </label>
            <asp:Label ID="lblPastDueDays" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis NA
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblNaType"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL - STOP HITUNG MARGIN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Sisa Pokok
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblOutstandingPrincipal"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Sisa Margin
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblOutstandingInterest"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Angsuran Jatuh Tempo
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblInstallmentDue"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Asuransi Jatuh Tempo
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblInsuranceDue"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Denda Keterlambatan Angsuran
            </label>
            <asp:Label ID="lblLCInstall" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan Asuransi
            </label>
            <asp:Label ID="lblLCInsurance" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Tagih Angsuran
            </label>
            <asp:Label ID="lblInstallCollFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Asuransi
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblInsuranceCollFee"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblPDCBounceFee"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Perpanjangan STNK/BBN
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblSTNKRenewalFee"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Klaim Asuransi
            </label>
            <asp:Label ID="lblInsuranceClaimFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblRepoFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Accrued Interest
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblAccruedInterest"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Stop Hitung Margin
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblNAAmount"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                STATUS KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Status Kontrak
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblContractStatus"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Status Default
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblDefaultStatus"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Rekening Lain Dimiliki
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblCrossDefault"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Alasan
            </label>
            <asp:Label ID="lblReason" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Disetujui Oleh
            </label>
            <asp:Label ID="lblApprovedBy" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan
            </label>
            <asp:Label runat="server" EnableViewState="False" ID="lblNotes"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                STATUS
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Status
            </label>
            <asp:Label ID="lblStatus" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Status
            </label>
            <asp:Label ID="lblStatusDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Diajukan Oleh
            </label>
            <asp:Label ID="lblRequestBy" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
   <div class="form_button"> 
        <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
