﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StopAccruedReversal.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.StopAccruedReversal" %>

<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>StopAccruedReversal</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
         <div class="form_single">
              <h4>STOP HITUNG MARGIN REVERSAL</h4>
         </div>
    </div>
    <div class="form_box">
		<div class="form_left">
		    <label>No Kontrak</label>
			<asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
		</div>
		<div class="form_right">
			<label>Nama Customer</label>
			    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
	</div>
                    
    <div class="form_box">
		<div class="form_left">
			<label> Tgl Pembayaran Terakhir </label>
			    <asp:Label ID="lblLastPaymentDate" runat="server"></asp:Label>
		</div>
		<div class="form_right">
			<label>    Jumlah Hari Telat </label>
			<asp:Label ID="lblPastDueDays" runat="server"></asp:Label>
		</div>
	</div>
   
    <asp:Panel ID="pnlPaymentInfo" runat="server">

    <div class="form_box">
					<div class="form_left">
					<label>  Sisa Pokok </label>
					 <asp:Label ID="lblOutstanding" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>  Accured Interest </label>
					  <asp:Label ID="lblAccured" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Angsuran Jatuh Tempo </label>
					 <asp:Label ID="lblInstallmentDue" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>  Premi Asuransi Jatuh Tempo </label>
					<asp:Label ID="lblInsuranceDue" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Denda Keterlambatan Angsuran </label>
					<asp:Label ID="lblLCInstall" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label> Denda Keterlambatan Asuransi </label>
					<asp:Label ID="lblLCInsurance" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Biaya Tagih Angsuran </label>
					 <asp:Label ID="lblInstallColl" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label> Biaya Tagih Asuransi </label>
					<asp:Label ID="lblInsuranceColl" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label>  Biaya Tolakan PDC </label>
					 <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>  Biaya Perpanjang STNK/BBN </label>
					<asp:Label ID="lblSTNKFee" runat="server"></asp:Label>
					</div>
				</div>
                <div class="form_box">
					<div class="form_left">
					<label>  Biaya Klaim Asuransi </label>
					 <asp:Label ID="lblInsuranceClaim" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label> Biaya Tarik </label>
					 <asp:Label ID="lblRepossessionFee" runat="server"></asp:Label>
					</div>
				</div>
         
    </asp:Panel>
    <asp:Panel ID="pnlNA" runat="server">
    <div class="form_box">
					<div class="form_left">
					<label> Tanggal Stop Hitung Margin </label>
					 <asp:Label ID="lblNADate" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label> Jumlah </label>
					 <asp:Label ID="lblNAAmount" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Catatan </label>
					 <asp:TextBox ID="tarNotes" runat="server"  Width="263px" TextMode="MultiLine"></asp:TextBox>
					</div>
					<div class="form_right">
					 
					</div>
				</div>
        
    </asp:Panel>
     <div class="form_button">
			   <br/>
                <asp:Button ID="ImbStopAccSave" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
			 </div>
     
    </form>
</body>
</html>
