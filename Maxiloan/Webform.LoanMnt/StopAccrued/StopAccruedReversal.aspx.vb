﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class StopAccruedReversal
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oValueDate As ValidDate

#Region "Constanta"
    Private oCustomClass As New Parameter.StopAccrued
    Private oController As New StopAccruedController



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "StopAccuredReversal"
            '  If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")

            With oCustomClass
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .BusinessDate = Me.BusinessDate
            End With
            oCustomClass = oController.StopAccruedGetPastDueDays(oCustomClass)
            lblLastPaymentDate.Text = oCustomClass.LastPayment.ToString("dd/MM/yyyy")
            lblPastDueDays.Text = CStr(oCustomClass.PastDueDays)

            With oCustomClass
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
            End With
            oCustomClass = oController.StopAccruedReversalList(oCustomClass)
            With oCustomClass
                Me.AgreementNo = .Agreementno
                Me.CustomerID = .CustomerID

                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                lblAgreementNo.Text = .Agreementno
                lblCustomerName.Text = .CustomerName
                lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"

                lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)
                lblNADate.Text = .NADate.ToString("dd/MM/yyyy")



                lblNAAmount.Text = FormatNumber(.NAAmount, 2)
                lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
                lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                lblOutstanding.Text = FormatNumber(.OutstandingPrincipal, 2)
                lblAccured.Text = FormatNumber(.AccruedInterest, 2)
            End With

        End If
        ' End If
    End Sub

    Private Sub ImbStopAccSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ImbStopAccSave.Click

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            '.ValueDate = ConvertDate2(oValueDate.dateValue)
            .ValueDate = Me.BusinessDate
            .BusinessDate = Me.BusinessDate
            .Notes = tarNotes.Text.Trim
        End With
        Try
            oController.StopAccruedReversal(oCustomClass)
            Response.Redirect("StopAccuredReversalList.aspx")
        Catch exp As Exception
            ShowMessage(lblMessage, "Stop Hitung Bunga Reversal Gagal" & exp.Message, True)
        End Try
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("StopAccuredReversalList.aspx")
    End Sub

End Class