﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class StopAccruedView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private ReadOnly Property RequestNaNo() As String
        Get
            Return CType(Request.QueryString("RequestNaNo"), String)
        End Get
    End Property
    Private ReadOnly Property CSSStyle() As String
        Get
            Return Request("Style").ToString
        End Get
    End Property
    Private ReadOnly Property RequestNoNa() As String
        Get
            Return Request.QueryString("RequestNoNa")
        End Get
    End Property
#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.StopAccrued
    Private oController As New StopAccruedController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Me.FormID = "STOPACCRUEDLIST"
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                With oCustomClass
                    .strConnection = GetConnectionString
                    .RequestNoNa = Me.RequestNaNo
                End With
                oCustomClass = oController.StopAccruedView(oCustomClass)
                With oCustomClass
                    lblRequestNaNo.Text = .RequestNoNa
                    hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & Me.CSSStyle & "','" & .ApplicationID.Trim & "')"
                    hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & Me.CSSStyle & "', '" & .CustomerID & "')"

                    hypAgreementNo.Text = .Agreementno
                    hypCustomerName.Text = .CustomerName
                    Select Case .ContractStatus.Trim
                        Case "AKT"
                            lblContractStatus.Text = "Aktive"
                        Case "PRP"
                            lblContractStatus.Text = "Prospect"
                        Case "OSP"
                            lblContractStatus.Text = "Oustanding Pokok"
                        Case "OSD"
                            lblContractStatus.Text = "Oustanding Denda"
                        Case "SSD"
                            lblContractStatus.Text = "Siap Serah Terima Dokumen"
                        Case "LNS"
                            lblContractStatus.Text = "Lunas"
                        Case "INV"
                            lblContractStatus.Text = "Expired Cause Invetaried"
                        Case "RJC"
                            lblContractStatus.Text = "Contract Reject By Company"
                        Case "CAN"
                            lblContractStatus.Text = "Contract Cancel by Customer"
                    End Select
                    Select Case .DefaultStatus.Trim
                        Case "NM"
                            lblDefaultStatus.Text = "Normal"
                        Case "NA"
                            lblDefaultStatus.Text = "Stop Accrued"
                        Case "WO"
                            lblDefaultStatus.Text = "Write Off"
                    End Select
                    lblCrossDefault.Text = CStr(IIf(.CrossDefault, "Yes", "No"))
                    lblRequestdate.Text = .ValueDate.ToString("dd/MM/yyyy")
                    lblNaType.Text = CStr(IIf(.NAType = "M", "Manual", "Automatic"))

                    lblNAAmount.Text = FormatNumber(.NAAmount, 2)

                    lblOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
                    lblOutstandingInterest.Text = FormatNumber(.OutStandingInterest, 2)

                    lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                    lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                    lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                    lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                    lblInstallCollFee.Text = FormatNumber(.InstallmentCollFee, 2)
                    lblInsuranceCollFee.Text = FormatNumber(.InsuranceCollFee, 2)
                    lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                    lblSTNKRenewalFee.Text = FormatNumber(.STNKRenewalFee, 2)
                    lblRepoFee.Text = FormatNumber(.RepossessionFee, 2)
                    lblInsuranceClaimFee.Text = FormatNumber(.InsuranceClaimExpense, 2)

                    lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)


                    lblNotes.Text = .Notes

                    lblReason.Text = .ReasonDescription
                    lblApprovedBy.Text = .UserApproval
                    lblRequestBy.Text = .UserRequest
                    Select Case .ApprovalStatus
                        Case "A"
                            lblStatus.Text = "Approved"
                        Case "J"
                            lblStatus.Text = "Reject"
                        Case "R"
                            lblStatus.Text = "Request"
                    End Select


                    lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
                    lblPastDueDays.Text = CStr(.PastDueDays)
                    lblLastPaymentDate.Text = .LastPayment.ToString("dd/MM/yyyy")
                End With
            End If

        End If
    End Sub

End Class