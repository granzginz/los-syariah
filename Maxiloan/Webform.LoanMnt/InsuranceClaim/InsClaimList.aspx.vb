﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsClaimList
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(viewstate("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("AmountReceive") = Value
        End Set
    End Property

    Private Property ClaimType() As String
        Get
            Return CType(viewstate("ClaimType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ClaimType") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(viewstate("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(viewstate("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("ValidationData") = Value
        End Set
    End Property
    Private Property ClaimAmount() As Double
        Get
            Return CDbl(viewstate("ClaimAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("ClaimAmount") = Value
        End Set
    End Property

    Private Property ReferenceNo() As String
        Get
            Return (CType(viewstate("ReferenceNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ReferenceNo") = Value
        End Set
    End Property
    Private Property WOP() As String
        Get
            Return (CType(viewstate("WOP"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WOP") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return (CType(viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property
    Private Property Notes() As String
        Get
            Return (CType(viewstate("Notes"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property
    'Private Property ReceivedFrom() As String
    '    Get
    '        Return (CType(viewstate("ReceivedFrom"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("ReceivedFrom") = Value
    '    End Set
    'End Property
    Public Property AssetSeqNo() As String
        Get
            Return (CType(viewstate("AssetSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As String
        Get
            Return (CType(viewstate("InsSequenceNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property

    Public Property InsClaimSeqNo() As String
        Get
            Return (CType(viewstate("InsClaimSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsClaimSeqNo") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.InsClaimReceive
    Private oController As New InsClaimController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                'Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.InsClaim).ApplicationID
                'Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.InsClaim).BranchId

                Me.ApplicationID = Request.QueryString("applicationid")
                Me.BranchID = Request.QueryString("branchid")
                Me.InsSequenceNo = Request.QueryString("InsSequenceNo")
                Me.AssetSeqNo = Request.QueryString("AssetSeqNo")
                Me.InsClaimSeqNo = Request.QueryString("InsClaimSeqNo")
                oPaymentDetail.WithOutPrepaid = False
                oPaymentDetail.ValueDate = Me.BusinessDate.ToString("dd/MM/yyyy")
                oPaymentDetail.DoFormCashModule()
           
            pnlConfirmation.Visible = False
            DoBind()

            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message.ToString + " (Page_Load)", False)
            End Try

        End If
    End Sub
    Private Sub DoBind()
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
            .AssetSeqNo = Me.AssetSeqNo
            .InsSequenceNo = Me.InsSequenceNo
            .InsClaimSeqNo = Me.InsClaimSeqNo

        End With
        Try
            oCustomClass = oController.InsClaiminfo(oCustomClass)
            With oCustomClass
                Me.AgreementNo = .Agreementno
                Me.CustomerID = .CustomerID
                lblAgreementBranch.Text = .BranchAgreement
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                lblAgreementNo.Text = .Agreementno
                lblCustomerName.Text = .CustomerName
                lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
                lblInsuranceCo.Text = .InsuranceCoy
                If Trim(.ClaimType) = "TMN" Then
                    lblClaimType.Text = "Termination"
                ElseIf Trim(.ClaimType) = "TLO" Then
                    lblClaimType.Text = "TLO Claim"
                End If
                lblAssetDesc.Text = .Description
                If .InsurancePaidBy = "CU" Then
                    lblInsuredPaidBy.Text = "Paid by Customer"
                ElseIf .InsurancePaidBy = "AC" Then
                    lblInsuredPaidBy.Text = "At Cost"
                End If

                lblClaimNo.Text = .ClaimDocNo
                lblClaimDate.Text = .ClaimDate.ToString("dd/MM/yyyy")
                lblEventDate.Text = .EventDate
                lblClaimAmount.Text = CType(FormatNumber(.ClaimAmount, 2), String)
                Me.ClaimAmount = .ClaimAmount
                lblReportBy.Text = .ReportedBy
                lblReportDate.Text = .ReportDate
                lblNotes.Text = .Notes
            End With
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message.ToString + " (DoBind)", False)
        End Try
    End Sub

    Private Sub InitialDefaultPanel()
        pnlPaymentDetail.Visible = True
    End Sub

    Private Sub BtnSaveInsClaimReceive1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSaveInsClaimReceive1.Click
        If oPaymentDetail.AmountReceive <> Me.ClaimAmount Then
            'pnlConfirmation.Visible = True
            'pnlPaymentDetail.Visible = False
            'PnlSave.Visible = False
            ShowMessage(lblMessage, "Harap Periksa Jumlah Terima", False)
            Return
        End If
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CompanyID = Me.SesCompanyID
                .BranchAgreement = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .AmountReceive = oPaymentDetail.AmountReceive
                .ReceivedFrom = oPaymentDetail.ReceivedFrom
                .ReferenceNo = oPaymentDetail.ReferenceNo
                .WOP = oPaymentDetail.WayOfPayment
                .BankAccountID = oPaymentDetail.BankAccount
                .Notes = oPaymentDetail.Notes
                .InsClaimSeqNo = Me.InsClaimSeqNo
            End With
            oCustomClass = oController.SaveInsClaimReceive(oCustomClass)
            Response.Redirect("InsClaim.aspx?Message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
     
    End Sub

    Private Sub BtnCancel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel1.Click
        Response.Redirect("InsClaim.aspx")
    End Sub
    Private Sub BtnCancel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btncancel2.Click
        Response.Redirect("InsClaim.aspx")
    End Sub

    Private Sub BtnSaveInsClaimReceive2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnsaveInsClaimReceive2.Click
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CompanyID = Me.SesCompanyID
                .BranchAgreement = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .AmountReceive = oPaymentDetail.AmountReceive
                .ReceivedFrom = oPaymentDetail.ReceivedFrom
                .ReferenceNo = oPaymentDetail.ReferenceNo
                .WOP = oPaymentDetail.WayOfPayment
                .BankAccountID = oPaymentDetail.BankAccount
                .Notes = oPaymentDetail.Notes
            End With
            oCustomClass = oController.SaveInsClaimReceive(oCustomClass)
            Response.Redirect("InsClaim.aspx?Message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

End Class