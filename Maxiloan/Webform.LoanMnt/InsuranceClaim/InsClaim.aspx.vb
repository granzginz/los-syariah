﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsClaim
    Inherits Maxiloan.Webform.AccMntWebBased
#Region "Properties"
    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property
    Private Property LastBranchComboIndex() As String
        Get
            Return (CType(ViewState("LastBranchComboIndex"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("LastBranchComboIndex") = Value
        End Set
    End Property
#End Region

#Region "Constants"
    Private STR_FORM_ID As String = "InsClaim"
#End Region
#Region "NavigationVars"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region "InquiryVars"
    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New Controller.GeneralPagingController
#End Region
#Region "MemberVars"
    Private m_controller As New DataUserControlController
    Private oControllerChild As New DataUserControlController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
    Dim m_dtBankAccount As New DataTable
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = STR_FORM_ID
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                With txtClaimDate
                    .Enabled = True
                End With
                Me.SearchBy = ""
                Me.SortBy = ""
                BuildMasterDetailCombo()
            End If
            pnlDatagrid.Visible = False
        End If

    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            lblMessage.Text = ""
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spInsuranceClaimPaging"
            End With

            oCustomClass = oController.GetGeneralPaging(oCustomClass)

            If oCustomClass Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            'DvUserList.Sort = Me.SortBy
            DtgInsurance.DataSource = DvUserList

            Try
                DtgInsurance.DataBind()
            Catch
                DtgInsurance.CurrentPageIndex = 0
                DtgInsurance.DataBind()
            End Try
            PagingFooter()
            'pnlList.Visible = True
            pnlDatagrid.Visible = True
            DtgInsurance.Visible = True

            'BuildMasterDetailCombo()


        Catch ex As Exception

        End Try
    End Sub
    Private Sub BuildMasterDetailCombo()
        FillComboWithBranches(cboBranch, m_controller)
    End Sub
    Private Sub FillComboWithBranches(ByVal cbo As WebControls.DropDownList, ByVal duccController As DataUserControlController)
        Try
            Dim dtbranch As New DataTable
            dtbranch = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cbo
                If Me.IsHoBranch Then
                    .DataSource = duccController.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                Else
                    .DataSource = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With
            'Me.LastBranchComboIndex = 0
        Catch ex As Exception
            'DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        Dim bNoAND As Boolean
        par = ""

        Dim strFilterBy As String = ""
        txtPage.Text = "1"

        Me.SearchBy = ""
        bNoAND = True
        If (cboBranch.SelectedItem.Value <> "0" And cboBranch.SelectedItem.Value <> "ALL") Then
            If cboType.SelectedValue.Trim <> "All" Then
                Me.SearchBy = "ag.branchid = " & "'" & cboBranch.SelectedValue.Trim & "'" & " And ic.ClaimType = " & "'" & cboType.SelectedValue.Trim & "'"
            Else
                Me.SearchBy = "ag.branchid = " & "'" & cboBranch.SelectedValue.Trim & "'"
            End If
            bNoAND = False
        End If

        '*** Rarely to change this routine
        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & CType(IIf(bNoAND, "", " AND "), String) & " (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"
                bNoAND = False
                If strFilterBy.Trim.Length > 0 Then
                    strFilterBy = strFilterBy & ", "
                End If
                strFilterBy = strFilterBy & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
            End If
        End If

        If txtClaimDate.Text.Trim.Length > 0 Then
            Me.SearchBy = Me.SearchBy & CType(IIf(bNoAND, "", " AND "), String) & " ic.ClaimDate = '" & ConvertDate2(txtClaimDate.Text) & "'"

            If strFilterBy.Trim.Length > 0 Then
                strFilterBy = strFilterBy & ", "
            End If
            strFilterBy = strFilterBy & ", ClaimDate = " & ConvertDate2(txtClaimDate.Text)
        End If

        With oCustomClass
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            Me.ParamReport = strFilterBy
        End With

        pnlDatagrid.Visible = True
        DtgInsurance.Visible = True
        '        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgInsurance_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgInsurance.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("InsClaim.aspx")
    End Sub

    Private Sub DtgInsurance_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgInsurance.ItemCommand
        Select Case e.CommandName
            Case "Receive"
                Dim lblApplicationid As Label
                Dim lblAssetSeqNo As Label
                Dim lblInsSequenceNo As Label
                Dim lblInsClaimSeqNo As Label
                Try
                    lblApplicationid = CType(DtgInsurance.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), Label)
                    lblAssetSeqNo = CType(DtgInsurance.Items(e.Item.ItemIndex).FindControl("lblAssetSeqNo"), Label)
                    lblInsSequenceNo = CType(DtgInsurance.Items(e.Item.ItemIndex).FindControl("lblInsSequenceNo"), Label)
                    lblInsClaimSeqNo = CType(DtgInsurance.Items(e.Item.ItemIndex).FindControl("lblInsClaimSeqNo"), Label)
                    'oCustomClass.ApplicationID = lblApplicationid.Text
                    'oCustomClass.BranchId = cboBranch.SelectedItem.Value
                    'HttpContext.Current.Items("Receive") = oCustomClass
                    Response.Redirect("InsClaimList.aspx?ApplicationID=" & lblApplicationid.Text & "&BranchID=" & cboBranch.SelectedItem.Value & "&AssetSeqNo=" & lblAssetSeqNo.Text & "&InsSequenceNo=" & lblInsSequenceNo.Text & "&Insclaimseqno=" & lblInsClaimSeqNo.Text)
                Catch ex As Exception
                    Response.Write(ex.Message.ToString)
                End Try


        End Select
    End Sub

    Private Sub DtgInsurance_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgInsurance.ItemDataBound
        Dim hypAgreementNo As HyperLink
        Dim hypCustomerName As HyperLink
        Dim lblCustomerID As Label

        If sessioninvalid() Then
            Exit Sub
        End If
        If e.Item.ItemIndex >= 0 Then
            hypAgreementNo = CType(e.Item.FindControl("lblDescription"), HyperLink)
            hypCustomerName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblID"), Label)

            hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hypAgreementNo.Text.Trim) & "')"
            hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
        End If

    End Sub

 
End Class