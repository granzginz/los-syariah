﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsClaimList.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InsClaimList" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocationDetail" Src="../../Webform.UserController/UcPaymentAllocationDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsClaimList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h3>
                    KLAIM ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:Label ID="lblInsuranceCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Klaim
                </label>
                <asp:Label ID="lblClaimType" runat="server" Width="3px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAssetDesc" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asuransi dibayar Oleh
                </label>
                <asp:Label ID="lblInsuredPaidBy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Klaim
                </label>
                <asp:Label ID="lblClaimNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Klaim
                </label>
                <asp:Label ID="lblClaimDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Kejadian
                </label>
                <asp:Label ID="lblEventDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Klaim
                </label>
                <asp:Label ID="lblClaimAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dilaporkan Oleh
                </label>
                <asp:Label ID="lblReportBy" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Lapor
                </label>
                <asp:Label ID="lblReportDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblNotes" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlPaymentDetail" runat="server">
             
                <div class="form_box_uc">
                    <uc1:ucpaymentdetail id="oPaymentDetail" runat="server"></uc1:ucpaymentdetail>
                </div>
             
        </asp:Panel>
        <asp:Panel ID="PnlSave" runat="server">
            <div class="form_button">
                <asp:Button ID="BtnSaveInsClaimReceive1" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="BtnCancel1" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirmation" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        KONFIRMASI KLAIM ASURANSI
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Harap Periksa Jumlah Terima
                    </label>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="BtnsaveInsClaimReceive2" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="Btncancel2" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
