﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvList_Collection_Temp.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRcvList_Collection_Temp" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRcvList_Collection</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PEMBAYARAN CUSTOMER (via COLLECTION) OTOR 
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Cari Berdasarkan Angsuran</label>
                <uc4:ucNumberFormat ID="txtAngsuran1" runat="server" CssClass="small_text" /> s/d 
                <uc4:ucNumberFormat ID="txtAngsuran2" runat="server" CssClass="small_text" />
            </div>
            
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>            
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                            AllowSorting="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <%--<asp:LinkButton ID="HypReceive" runat="server" Text="RECEIVE" CommandName="Receive"></asp:LinkButton>--%>
                                        <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server"
                                            CommandName="Receive"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'> </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="TYPE CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NamaSuamiIstri" HeaderText="NAMA PASANGAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNamaSuamiIstri" runat="server" Text='<%#Container.DataItem("NamaSuamiIstri")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="LicensePlate" HeaderText="NO. POLISI" HeaderStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="MerekType" HeaderText="MERK/TYPE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMerekType" runat="server" Text='<%#Container.DataItem("MerekType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%-- <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>--%>
                                <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%> &nbsp;'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="NextInstallmentNumber" DataFormatString="{0:N0}" HeaderText="KE"
                                    SortExpression="NextInstallmentNumber"></asp:BoundColumn>
                                <asp:BoundColumn DataField="NextInstallmentDate" DataFormatString="{0:dd/MM/yyyy}"
                                    HeaderText="JATUH TEMPO" SortExpression="NextInstallmentDate"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Tenor" HeaderText="TNR" SortExpression="Tenor"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
