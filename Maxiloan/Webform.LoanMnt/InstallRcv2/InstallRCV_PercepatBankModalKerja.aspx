﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRCV_PercepatBankModalKerja.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRCV_PercepatBankModalKerja" %>

<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocationDetail" Src="../../Webform.UserController/UcPaymentAllocationDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRCV_PercepatBankModalKerja</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <style type="text/css">
    /* Paste this css to your style sheet file or under head tag */
    /* This only works with JavaScript, 
    if it's not present, don't show loader */
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
	    position: fixed;
	    left: 0px;
	    top: 0px;
	    width: 100%;
	    height: 100%;
	    z-index: 9999;
	    background: url(../../images/pic-loader.gif) center no-repeat #fff;
    }
    </style>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
</head>
    <script language="javascript" type="text/javascript">
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
            hitungTotal()
        });

        function hitungTotal() {
            
            var sisapokok = $('#lblTotalPembiayaan').html().replace(/\s*,\s*/g, '');
            var sisabunga = $('#lblOutstandingInterest').html().replace(/\s*,\s*/g, '');
            $('#principalPaid_txtNumber').val(number_format(sisapokok, 0));
            $('#interestPaid_txtNumber').val(number_format(sisabunga, 0));

            var pokok = $('#principalPaid_txtNumber').val().replace(/\s*,\s*/g, '');
            var bunga = $('#interestPaid_txtNumber').val().replace(/\s*,\s*/g, '');
            var denda = $('#dendapaid_txtNumber').val().replace(/\s*,\s*/g, '');

            var total = parseFloat(pokok) + parseFloat(bunga) + parseFloat(denda)

            $('#lbltotalbayar').html(number_format(total, 0));

        }

        function number_format(number, decimals, dec_point, thousands_sep) {
            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
<body>
    <script language="javascript" type="text/javascript">
    var AppInfo = '<%= Request.ServerVariables("PATH_INFO")%>';
    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
    var ServerName = 'http://<%=Request.ServerVariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    PELUNASAN DIPERCEPAT CUSTOMER MODAL KERJA (via BANK) OTOR
                </h4>
            </div>
        </div>
        <div class="se-pre-con"></div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <uc1:ucpaymentdetail id="oPaymentDetail" runat="server" isdatemandatory="true" />
            <div class="form_right">
                <label>Tanggal Bayar</label>
                <uc1:ucdatece id="oValueDate" runat="server" />
            </div>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label>Tanggal Invoice</label>
                <uc1:ucdatece ID="oInvoiceDate" runat="server" />
            </div>
            <div class="form_right">
                <label>Tanggal JT Invoice</label>
                 <uc1:ucdatece ID="oInvoiceDueDate" runat="server"/>
            </div>
        </div>
        
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">No Referensi</label>
                <asp:TextBox ID="NoRefrensi" runat="server" CssClass="" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNoRefrensi" runat="server" ControlToValidate="NoRefrensi" 
                    CssClass="validator_general" Display="Dynamic" ErrorMessage="Input No Referensi"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>No Invoice</label>
                <asp:Label ID="lblInvoiceNo" runat="server" CssClass="" ></asp:Label> 
            </div>
        </div>
        <div class="form_box">
             <div class="form_left">
                <label>Bayar Pokok</label>
                <uc1:UcNumberFormat id="principalPaid" runat="server" OnChange="hitungTotal()" />
            </div>
            <div class="form_right">
                <label>Total Invoice</label>
                <asp:Label id ="lblInvoiceAmount" runat="server" ></asp:Label>  
            </div>
        </div>
         <div class="form_box">
            <div class="form_left">
                <label>Bayar Bunga</label>
                <uc1:UcNumberFormat id="interestPaid" runat="server" OnChange="hitungTotal()" />
            </div>
             <div class="form_right">
                 <b>
                 <label>Sisa Pokok</label>
                 <asp:Label id="lblTotalPembiayaan" runat="server" ></asp:Label>  
                 </b>
             </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Bayar Denda</label>
                <uc1:UcNumberFormat id="dendapaid" runat="server" OnChange="hitungTotal()" />
            </div>
            <div class="form_right">
                <div class="form_box_hide">
                    <label>Bunga Invoice</label>
                    <asp:Label ID="lblInterestAmount" runat="server" ></asp:Label>  
                </div>
                <label>Total Bunga</label>
                <asp:Label ID="lblTotalInterest" runat="server" ></asp:Label> 
            </div>
        </div>
        <div class="form_box" style="display:none">
            <div class="form_left">
                <label>Bayar Retensi</label>
                <uc1:UcNumberFormat id="RetensiPaid" runat="server" />
            </div>
             <div class="form_right">
                <label>Retensi Invoice</label>
                <asp:Label ID="lblRetensiAmount" runat="server" ></asp:Label>  
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Total Bayar</label>
                <asp:Label ID="lbltotalbayar" runat="server" ></asp:Label>
            </div>
            <div class="form_right"> 
                <b><label>Bunga Per Tgl Bayar</label>
                <asp:Label ID="lblOutstandingInterest" runat="server" ></asp:Label></b>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">

            </div>
            <div class="form_right">
                <label>Periode</label>
                <asp:Label ID="lblInsseqno" runat="server" />
            </div>
        </div>





        <asp:Panel runat="server" ID="pnlBiaya">
            <div class="form_box">
                 <div class="form_left">
                    <h4>Biaya-biaya</h4>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Admin</label>
                    <asp:Label runat="server" ID="lblBiayaAdmin"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Provisi / Annual</label>
                    <asp:Label runat="server" ID="lblBiayaProvisi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Handling</label>
                    <asp:Label runat="server" ID="lblBiayaHandling"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Asuransi Kredit</label>
                    <asp:Label runat="server" ID="lblAsuransiKredit"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Polis</label>
                    <asp:Label runat="server" ID="lblBiayaPolis"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Survey</label>
                    <asp:Label runat="server" ID="lblBiayaSurvey"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Notaris</label>
                    <asp:Label runat="server" ID="lblBiayaNotaris"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Discount charges</label>
                    <asp:Label runat="server" ID="lblDiscountCharges"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Biaya</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblTotalBiaya"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlListInvoice">
            <div class="form_box_header">
                <div class="form_single">
                    <h5>
                        TABEL BUNGA YANG JATUH TEMPO
                    </h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InsSeqNo" HeaderText="Periode"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="DueDate" HeaderText="Jatuh Tempo" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PrincipalAmount" HeaderText="Pokok" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Bunga" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestPaidAmount" HeaderText="Bunga Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="LateChargeAmount" HeaderText="Denda"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtnGroupPaymentAllocation" runat="server">
            <div class="form_button">
                <asp:Button ID="imbSaveProcess" runat="server" Text="Save" CssClass="small button blue" />
                &nbsp;
                <asp:Button ID="imbCancel2Process" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray" />
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
