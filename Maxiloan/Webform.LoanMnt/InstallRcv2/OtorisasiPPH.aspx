﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtorisasiPPH.aspx.vb" 
    Inherits="Maxiloan.Webform.LoanMnt.OtorisasiPPH" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>OtorisasiPPH</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>     
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.ServerVariables("SERVER_NAME")%>/';

        //function OpenWinMain(SuspendNo, branchid) {
        function OpenWinMain(PPHNo, branchid) {

            var x = screen.width; var y = screen.height - 100;
            //window.open(ServerName + App + '/Webform.LoanMnt/Suspend/SuspendInqView.aspx?SuspendNo=' + SuspendNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
            window.open(ServerName + App + '/Webform.LoanMnt/Suspend/SuspendInqView.aspx?SuspendNo=' + SuspendNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        //function click() {
        //    if (event.button == 2) {
        //        alert('Anda tidak Berhak');
        //    }
        //}
        document.onmousedown = click
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    OTORISASI PPH
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Rekening Bank
                </label>
                <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Referensi
                </label>
                <asp:TextBox ID="txtRefNo" runat="server"  MaxLength="20"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Tanggal Terima
                </label>
                <asp:TextBox ID="txtTglJatuhTempo" runat="server" />
                <aspajax:CalendarExtender ID="calExTglJatuhTempo" runat="server" TargetControlID="txtTglJatuhTempo"
                    Format="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="rfvTglJatuhTempo" runat="server" ControlToValidate="txtTglJatuhTempo"
                    ErrorMessage="Harap isi dengan tanggal jatuh tempo" Enabled="false" Display="Dynamic" />
            </div>
            <%--<div class="form_right">
                <label>
                    Keterangan
                </label>
                <asp:TextBox ID="txtdesc" runat="server" ></asp:TextBox>
            </div>--%>
        </div>
        <%--<div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Terima
                </label>               
                <uc1:ucnumberformat id="txtAmountFrom0" runat="server" /></uc1:ucNumberFormat>S/D
                <uc1:ucnumberformat id="txtamountto0" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>--%>
        <%--<div class="form_box_header">
            <div class="form_single">
                <h5>Pencarian suspend menggunakan jumlah terima</h5>
            </div>
        </div>--%>
        <%--<div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Terima
                </label>               
                <uc1:ucnumberformat id="txtAmountFrom" runat="server" /></uc1:ucNumberFormat>S/D
                <uc1:ucnumberformat id="txtamountto" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>--%>
        <div class="form_button">
            <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR OTORISASI PPH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" DataKeyField="PPHNo"
                        AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" >
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                        <HeaderStyle Width="3%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" AutoPostBack="True" runat="server" OnCheckedChanged="SelectAll">
                                            </asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Applicationid" HeaderText="Applicationid" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="No Kontrak" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="branchid" HeaderText="branchid" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PPHNo" HeaderText="NO PPH">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPPHNo" runat="server" Text='<%#Container.DataItem("PPHNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO BUKTI REFERENSI">
                                <ItemTemplate>
                                    <asp:Label ID="lblTransferRefNO" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--<asp:TemplateColumn SortExpression="DESCRIPTION" HeaderText="KETERANGAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCRIPTION" runat="server" Text='<%#Container.DataItem("DESCRIPTION")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn SortExpression="Bayar" HeaderText="JUMLAH BAYAR">
                                <ItemTemplate>
                                    <asp:Label ID="lblAMOUNT" runat="server" Text='<%#FormatNumber(Container.DataItem("Bayar"), 2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PostingDate" SortExpression="PostingDate" HeaderText="TGL SUSPEND"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="BANKACCOUNTID" HeaderText="ID REKENING BANK"
                                Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBANKACCOUNTID" runat="server" Text='<%#Container.DataItem("BANKACCOUNTID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="bankaccountname" HeaderText="REKENING BANK">
                                <ItemTemplate>
                                    <asp:Label ID="LBLBANKACCOUNTDESC" runat="server" Text='<%#Container.DataItem("bankaccountname")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                 
                </div>
            </div>
        </div>
           <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnReject" runat="server" Text="Reject" CssClass="small button red"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
