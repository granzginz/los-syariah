﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRCV_PPH.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRCV_PPH" %>

<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocationDetail" Src="../../Webform.UserController/UcPaymentAllocationDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRCV_PPH</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
</head>
<script type="text/javascript">
    //function validateJenisBayar() {
    //    var pokokPaids = $('#principalPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var bungaPaids = $('#interestPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var retensiPaids = $('#RetensiPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var dendaPaids = $('#DendaPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var jatuhTempos = $('#oInvoiceDueDate_txtDateCE').val().replace(/\s*,\s*/g, '');
    //    var tglBayars = $('#oValueDate_txtDateCE').val().replace(/\s*,\s*/g, '');

    //    var pokoks = $('#lblTotalPembiayaan').html().replace(/\s*,\s*/g, '');
    //    var bungas = $('#lblInterestAmount').html().replace(/\s*,\s*/g, '');
    //    var retensis = $('#lblRetensiAmount').html().replace(/\s*,\s*/g, '');
    //    var dendas = $('#lblDendaAmount').html().replace(/\s*,\s*/g, '');

    //    var pokokPaid  = parseFloat(pokokPaids)
    //    var bungaPaid  = parseFloat(bungaPaids)
    //    var retensiPaid = parseFloat(retensiPaids)
    //    var dendaPaid  = parseFloat(dendaPaids)   
    //    var jatuhTempo = parseFloat(jatuhTempos)   
    //    var tglBayar = parseFloat(tglBayars)  

    //    var pokok = parseFloat(pokoks)
    //    var bunga = parseFloat(bungas)
    //    var retensi = parseFloat(retensis)
    //    var denda = parseFloat(dendas)

    //    console.log('pokokPaid',pokokPaid,'bungaPaid',bungaPaid,'retensiPaid',retensiPaid,'jatuhTempo',jatuhTempo,'tglBayar',tglBayar,'pokok',pokok,'bunga',bunga,'retensi',retensi,)

    //    if ((tglBayar < jatuhTempo) && (pokokPaid == pokok) && (bungaPaid == bunga) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PELUNASAN DIPERCEPAT TANPA BAYAR RETENSI")
    //    } else if ((tglBayar < jatuhTempo) && (pokokPaid == pokok) && (bungaPaid == bunga) && (retensiPaid == retensi)) {
    //        $('#lblJenisBayar').html("PELUNASAN DIPERCEPAT DENGAN BAYAR RETENSI")
    //    } else if ((tglBayar >= jatuhTempo) && (pokokPaid == pokok) && (bungaPaid == bunga) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PELUNASAN NORMAL TANPA BAYAR RETENSI")
    //    } else if ((tglBayar >= jatuhTempo) && (pokokPaid == pokok) && (bungaPaid == bunga) && (retensiPaid == retensi)) {
    //        $('#lblJenisBayar').html("PELUNASAN NORMAL DENGAN BAYAR RETENSI")
    //    } else if ((tglBayar < jatuhTempo) && (pokokPaid > 0) && (pokokPaid < pokok) && (bungaPaid == 0) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PEMBAYARAN POKOK SEBAGIAN TANPA BUNGA")
    //    } else if ((tglBayar < jatuhTempo) && (pokokPaid > 0) && (pokokPaid < pokok) && (bungaPaid <= bunga) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PEMBAYARAN POKOK SEBAGIAN DENGAN BUNGA")
    //    } else if ((tglBayar < jatuhTempo) && (pokokPaid > 0) && (pokokPaid < pokok) && (bungaPaid <= bunga) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PEMBAYARAN POKOK SEBAGIAN DENGAN BUNGA")
    //    } else if ((tglBayar < jatuhTempo) && (pokokPaid == 0) && (bungaPaid < bunga) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PEMBAYARAN BUNGA SEBAGIAN")
    //    } else if ((tglBayar < jatuhTempo) && (pokokPaid == 0) && (bungaPaid == bunga) && (bungaPaid > 0) && (retensiPaid == 0)) {
    //        $('#lblJenisBayar').html("PEMBAYARAN BUNGA PENUH")
    //    } else if ($('#lbltotalbayar').html() == 0 ) {
    //        $('#lblJenisBayar').html("-")
    //    } else {
    //        $('#lblJenisBayar').html("PEMBAYARAN LAINNYA")
    //    }


    //}
    //function hitungTotal() {
    //    var pokok = $('#principalPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var bunga = $('#interestPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var retensi = $('#RetensiPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    var denda = $('#DendaPaid_txtNumber').val().replace(/\s*,\s*/g, '');
    //    validateJenisBayar()
    //    var total = parseFloat(pokok) + parseFloat(bunga) + parseFloat(retensi) + parseFloat(denda);
    //    $('#lbltotalbayar').html(number_format(total, 2));

    //}
    function allocatePayment(a) {
        //modify Nofi 22112019
        var pph = $('#lblPPHAmount').html().replace(/\s*,\s*/g, '');
        var sisapph = $('#lblSisaPPHAmount').html().replace(/\s*,\s*/g, ''); 
         
        if (parseFloat(a) > parseFloat(sisapph)){
            var selisih =parseFloat(a) - parseFloat(sisapph);  
            var nf = Intl.NumberFormat(); 
            alert("Masih ada kelebihan bayar sebesar Rp. " + nf.format(selisih));
            //alert("Masih ada kelebihan bayar sebesar Rp. " .number_format(selisih,2));
        } 
    }
    //function number_format(number, decimals, dec_point, thousands_sep) {

    //    number = (number + '')
    //    .replace(/[^0-9+\-Ee.]/g, '');
    //    var n = !isFinite(+number) ? 0 : +number,
    //    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    //    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    //    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    //    s = '',
    //    toFixedFix = function (n, prec) {
    //        var k = Math.pow(10, prec);
    //        return '' + (Math.round(n * k) / k)
    //        .toFixed(prec);
    //    };
    //    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    //    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    //    .split('.');
    //    if (s[0].length > 3) {
    //        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    //    }
    //    if ((s[1] || '')
    //    .length < prec) {
    //        s[1] = s[1] || '';
    //        s[1] += new Array(prec - s[1].length + 1)
    //      .join('0');
    //    }
    //    return s.join(dec);
    //}
</script>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    PEMBAYARAN PENYELESAIN PERJANJIAN HUTANG (PPH)
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <uc1:ucpaymentdetail id="oPaymentDetail" runat="server" isdatemandatory="true" />
            <div class="form_right">
                <label>Tanggal Bayar</label>
                <uc1:ucdatece id="oValueDate" runat="server" />
            </div>
        </div>
         <div class="form_box">
            <div class="form_left" >
                <label>Tanggal Terima</label>
                <uc1:ucdatece ID="oInvoiceDate" runat="server" /> 
            </div>
            <%--<div class="form_right">
                <label>Tanggal JT Invoice</label>
                 <uc1:ucdatece ID="oInvoiceDueDate" runat="server"/>
            </div>--%>
             <div class="form_right">
                <label>No PJJ</label>
                <asp:Label ID="lblNoPJJ" runat="server"  ></asp:Label> 
            </div>
        </div>        
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">No Referensi</label>
                <asp:TextBox ID="NoRefrensi" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNoRefrensi" runat="server" ControlToValidate="NoRefrensi" 
                    CssClass="validator_general" Display="Dynamic" ErrorMessage="Input No Referensi"></asp:RequiredFieldValidator>
            </div> 
            <div class="form_right">
                <label>Nilai PPH</label>
                <asp:Label id ="lblPPHAmount" runat="server" CssClass="numberAlign3" ></asp:Label>  
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Bayar PPH</label>
                <uc1:UcNumberFormat id="jumlahBayar" runat="server" OnChange="allocatePayment(this.value)" /> <%--OnChange="CekNilaiAngsuran(this.value);calculateTotal();"/>--%>
            </div>
          <div class="form_right">
                <label>Sisa Nilai PPH</label>
                <asp:Label id ="lblSisaPPHAmount" runat="server" CssClass="numberAlign3" ></asp:Label>  
            </div>
        </div> 
        <div class="form_box"> 
            <div class="form_right">
            </div>
        </div> 

        <asp:Panel runat="server" ID="pnlBiaya">
        <div style="display:none">
            <div class="form_box">
                 <div class="form_left">
                    <h4>Biaya-biaya</h4>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Admin</label>
                    <asp:Label runat="server" ID="lblBiayaAdmin"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Provisi / Annual</label>
                    <asp:Label runat="server" ID="lblBiayaProvisi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Handling</label>
                    <asp:Label runat="server" ID="lblBiayaHandling"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Asuransi Pembiayaan</label>
                    <asp:Label runat="server" ID="lblAsuransiKredit"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Polis</label>
                    <asp:Label runat="server" ID="lblBiayaPolis"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Survey</label>
                    <asp:Label runat="server" ID="lblBiayaSurvey"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Notaris</label>
                    <asp:Label runat="server" ID="lblBiayaNotaris"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Discount charges</label>
                    <asp:Label runat="server" ID="lblDiscountCharges"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Biaya</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblTotalBiaya"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div>
        </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlListInvoice">
            <div class="form_box_header">
                <div class="form_single">
                    <h5>
                        TABEL INVOICE
                    </h5>
                </div>
            </div>
            <div class="form_box_header_hide">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" DataKeyField="InvoiceSeqNo" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="No" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="Invoice No" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="Inv Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="Inv Due Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice Amount"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Pokok"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Margin"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <%--<asp:BoundColumn Visible="True" DataField="JangkaWaktu" HeaderText="Periode"  DataFormatString="{0:0 days}" ></asp:BoundColumn>--%>
                                
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtnGroupPaymentAllocation" runat="server">
            <div class="form_button">
                <asp:Button ID="imbSaveProcess" runat="server" Text="Save" CssClass="small button blue" />
                &nbsp;
                <asp:Button ID="imbCancel2Process" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray" />
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
