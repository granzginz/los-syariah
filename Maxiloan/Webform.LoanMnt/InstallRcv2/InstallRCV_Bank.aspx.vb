﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

#End Region

Public Class InstallRCV_Bank
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oViewPaymentDetail As UCViewPaymentDetail
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule
    Protected WithEvents oValueDate As ucDateCE

    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String


#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId
            Me.FormID = "INSTALLRCV_BA"
            Me.Mode = "Normal"
            oCashier.CashierID = ""
            oCashier.HeadCashier = True
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'If Not CheckCashier(Me.Loginid) Then

                '    ShowMessage(lblMessage, "Kasir Belum Buka", True)
                '    Dim ThreadInitialPanel As New Thread(AddressOf InitialDefaultPanel)

                '    ThreadInitialPanel.Priority = ThreadPriority.AboveNormal
                '    pnlBtnGroupPaymentReceive.Visible = False
                '    pnlBtnGroupPaymentAllocation.Visible = False

                '    Dim ThreadBindData As New Thread(AddressOf DoBind)
                '    ThreadBindData.Priority = ThreadPriority.AboveNormal
                '    ThreadInitialPanel.Start()
                '    ThreadBindData.Start()

                '    Exit Sub
                'End If

                InitialDefaultPanel()
                oValueDate.AutoPostBack = True
                oValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind()
                LoadNextStep()
                BlokirBayarStep()

                'load grid angsuran
                oInstallmentSchedule.ApplicationId = Me.ApplicationID
                oInstallmentSchedule.ValueDate = ConvertDate2(oValueDate.Text)
                oInstallmentSchedule.DoBind_Angsuran(String.Empty)

                If Not IsNothing(Session("BankSelected")) Then
                    oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
                    oPaymentDetail.cmbBankAccountEnabled = False
                End If
                If Not IsNothing(Session("tglValuta")) Then
                    oValueDate.Text = Session("tglValuta")

                End If
            End If

        Else
            'If Not CheckCashier(Me.Loginid) Then
            'ShowMessage(lblMessage, "Kasir Belum Buka", True)
            'pnlPaymentDetail.Visible = True
            'pnlViewPaymentDetail.Visible = False
            'pnlPaymentAllocation.Visible = False
            'pnlBtnGroupPaymentReceive.Visible = False
            'pnlBtnGroupPaymentAllocation.Visible = False
            'pnlPaymentInfo.Visible = False

            'Exit Sub
            'End If


        End If




    End Sub

    Private Sub LoadNextStep()
        NextProcess()

        If Me.WayOfPayment <> "CP" Then
            If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                Else
                    If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                        oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                        oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                    Else
                        oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                    End If
                End If
            Else
                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
            End If

            IsiKolomTagihan()


        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlPaymentDetail.Visible = True
        pnlViewPaymentDetail.Visible = False
        pnlPaymentAllocation.Visible = True
        pnlBtnGroupPaymentReceive.Visible = False
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlPaymentInfo.Visible = False
        pnlHeadCashierPassword.Visible = False
        pnlConfirmation.Visible = False


    End Sub

    Private Sub DoBind()
        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID

            'value date dari tanggal bayar
            '.ValueDate = Me.BusinessDate
            .ValueDate = ConvertDate2(oValueDate.Text)

            .BranchId = Me.BranchID
        End With


        oCustomClass = oController.GetPaymentInfo(oCustomClass)


        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            'lblAgreementBranch.Text = .BranchAgreement
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            'lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
            'lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
            Me.NextInstallmentDate = .NextInstallmentDate
            'lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)



            'lblFundingCoyName.Text = .FundingCoyName
            'lblFundingPledgeStatus.Text = .FundingPledgeStatus
            'lblResiduValue.Text = FormatNumber(.ResiduValue, 2)
            lblPrepaid.Text = FormatNumber(.ContractPrepaidAmount, 0)
            Me.PrepaidBalance = .ContractPrepaidAmount
            Me.AmountToBePaid = .AmountToBePaid
            lblAmountToBePaid.Text = FormatNumber(Me.AmountToBePaid, 0)

            oPaymentAllocationDetail.ToleransiBayarKurang = .ToleransiBayarKurang
            oPaymentAllocationDetail.ToleransiBayarLebih = .ToleransiBayarLebih

            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "BA"
            oPaymentDetail.DoFormCashModule()

            If .ContractStatus <> "OSD" Then
                oPaymentDetail.AmountReceive = .InstallmentAmount
            Else
                oPaymentDetail.AmountReceive = .InstallmentDue
            End If
            lblTitipanAngsuran.Text = FormatNumber(.TitipanAngsuran)
            oPaymentDetail.ShowTglBayar = False
            oPaymentDetail.ValueDate = oValueDate.Text

            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub

#Region "BtnGroup Payment Receive"
    Private Sub imbNextProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbNextProcess.Click
        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            lblMessage.Text = ""
            If oPaymentDetail.WayOfPayment = "CP" Then
                If Me.BranchID <> Me.sesBranchId.Trim.Replace("'", "") Then

                    ShowMessage(lblMessage, "Cara Pembayaran Tidak bisa untuk Transaksi Antar Cabang", True)
                    Exit Sub
                End If
            End If
            If Me.IsValidLastPayment(Me.ApplicationID, ConvertDate2(oValueDate.Text)) Then
                If Me.Mode = "Normal" Then
                    NextProcess()
                    'Dim ThreadNextProcess As New Thread(AddressOf NextProcess)
                    'ThreadNextProcess.Priority = ThreadPriority.BelowNormal

                    'If Not CheckCashier(Me.Loginid) Then

                    ShowMessage(lblMessage, "Kasir belum Buka", True)
                    Exit Sub
                    'End If
                    If Me.WayOfPayment = "CP" Then
                        If Me.PrepaidBalance = 0 Then

                            ShowMessage(lblMessage, "Tidak bisa Alokasi Saldo Prepaid, Karena Saldo Kurang", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    Else
                        If oPaymentDetail.AmountReceive = 0 Then

                            ShowMessage(lblMessage, "Harap diisi dengan Jumlah diterima", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                        Session.Add("BankSelected", oPaymentDetail.BankAccount)
                    End If
                    If IsMaxBacDated(ConvertDate2(oValueDate.Text)) Then
                        oPaymentDetail.WayOfPayment = "0"
                        Me.Mode = "NotNormal"
                        pnlHeadCashierPassword.Visible = True
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        oCashier.CashierID = ""
                        oCashier.HeadCashier = True

                        Exit Sub
                    ElseIf DateDiff(DateInterval.Day, ConvertDate2(oValueDate.Text), Me.BusinessDate) < 0 Then
                        oPaymentDetail.WayOfPayment = "0"

                        ShowMessage(lblMessage, "Tanggal Valuta tidak boleh lebih besar dari tgl hari ini", True)
                        Exit Sub
                    End If


                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = True
                    pnlPaymentAllocation.Visible = True
                    pnlPaymentInfo.Visible = True
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = True
                    If Me.WayOfPayment <> "CP" Then
                        If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                            If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                            Else
                                If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                                Else
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                                End If
                            End If
                        Else
                            oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                        End If

                        IsiKolomTagihan() 'scl

                    End If
                    'ThreadNextProcess.Start()
                ElseIf Me.Mode = "NotNormal" Then
                    Dim objDecent As New Decrypt.am_futility
                    Dim oLogin As New LoginController
                    Dim oLoginEntities As New Parameter.Login
                    Dim strError As String
                    oLoginEntities.strConnection = GetConnectionString()
                    oLoginEntities.LoginId = oCashier.CashierID.Trim
                    Dim lstrPassword As String
                    'oLogin.GetEmployee(oLoginEntities) retreive password dari user
                    lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
                    If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = True
                        pnlPaymentAllocation.Visible = True
                        pnlPaymentInfo.Visible = True
                        pnlHeadCashierPassword.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = True
                        Me.Mode = "Normal"
                    Else

                        ShowMessage(lblMessage, "Password Salah", True)
                        Exit Sub
                    End If
                End If
            Else

                ShowMessage(lblMessage, "Tanggal Valuta Lebih kecil dari tanggal pembayaran terakhir", True)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub NextProcess()

        With oPaymentInfo
            'oPaymentDetail.ValueDate = Format(Me.BusinessDate, "dd/MM/yyyy")
            oPaymentDetail.ValueDate = oValueDate.Text
            .ValueDate = ConvertDate2(oValueDate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()

            Me.ValidationData = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
        End With

        Me.AmountReceive = oPaymentDetail.AmountReceive
        'With oViewPaymentDetail
        '    Me.WayOfPayment = oPaymentDetail.WayOfPayment
        '    .WayOfPayment = oPaymentDetail.WayOfPaymentName
        '    Me.BankAccount = oPaymentDetail.BankAccount
        '    .BankAccount = oPaymentDetail.BankAccountName
        '    If Me.WayOfPayment = "CP" Then
        '        Me.AmountReceive = 0
        '        .AmountReceive = 0
        '    Else
        '        Me.AmountReceive = oPaymentDetail.AmountReceive
        '        .AmountReceive = oPaymentDetail.AmountReceive
        '    End If

        '    .ReceivedFrom = oPaymentDetail.ReceivedFrom
        '    .ReferenceNo = oPaymentDetail.ReferenceNo

        '    .ValueDate = oPaymentDetail.ValueDate
        '    .Notes = oPaymentDetail.Notes
        '    .BayarDI = oPaymentDetail.BayarDi
        '    .CollectorName = oPaymentDetail.CollectorName
        'End With
        With oPaymentAllocationDetail
            '.MaximumInstallment = oPaymentInfo.MaximumInstallment + getValueGeneralLimit("PREPAIDINCOME", "AM")
            '=================================
            .MaximumLCInstallFee = 999999999999
            .MaximumInstallCollFee = 999999999999
            .MaximumLCInsuranceFee = 999999999999
            .MaximumInsuranceCollFee = 999999999999

            .MaximumPDCBounceFee = 999999999999
            .MaximumSTNKRenewalFee = 999999999999
            .MaximumInsuranceClaimFee = 999999999999
            .MaximumReposessionFee = 999999999999


            '.MaximumLCInstallFee = oPaymentInfo.MaximumLCInstallFee
            '.MaximumInstallCollFee = oPaymentInfo.MaximumInstallCollFee

            .MaximumInsurance = oPaymentInfo.MaximumInsurance
            '.MaximumLCInsuranceFee = oPaymentInfo.MaximumLCInsuranceFee
            '.MaximumInsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee

            '.MaximumPDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
            '.MaximumSTNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee
            '.MaximumInsuranceClaimFee = oPaymentInfo.MaximumInsuranceClaimFee
            '.MaximumReposessionFee = oPaymentInfo.MaximumReposessionFee

            If oPaymentInfo.PrepaidHoldStatus.Trim <> "NM" Then
                .DisabledAll = True
            End If

            If (oPaymentInfo.ContractStatus.Trim = "PRP" Or _
                oPaymentInfo.ContractStatus.Trim = "INV" Or _
                oPaymentInfo.ContractStatus.Trim = "LNS") Then
                .DisabledAll = True
            End If

            .PaymentAllocationBind()
            'Dim ThreadBindPaymentAllocationBind As New Thread(AddressOf .PaymentAllocationBind)
            'ThreadBindPaymentAllocationBind.Priority = ThreadPriority.AboveNormal
            'ThreadBindPaymentAllocationBind.Start()
        End With
    End Sub

    Private Sub imbCancelProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelProcess.Click
        Response.Redirect("InstallRcvList_Bank.aspx")
    End Sub
#End Region

#Region "Button Group Payment Allocation"


    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvList_Bank.aspx")
    End Sub
#End Region

    Sub BindReport()
        'Dim oData As New DataSet
        'Dim objReport As RptPrintKwitansi = New RptPrintKwitansi

        'oCustomClass.WhereCond = String.Empty
        'oCustomClass.MultiAgreementNo = Me.AgreementNo
        'oCustomClass.MultiApplicationID = Me.ApplicationID
        'oCustomClass.BranchId = Me.BranchID
        'oCustomClass.strConnection = GetConnectionString
        'oData = oController.ReportKwitansiInstallment(oCustomClass)

        'objReport.SetDataSource(oData)

        'Dim discrete As ParameterDiscreteValue
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues

        'ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.BusinessDate
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        '' crvKwitansi.ReportSource = objReport

        'Dim doctoprint As New System.Drawing.Printing.PrintDocument
        'Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        'For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
        '    Dim rawKind As Integer
        '    If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
        '        rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
        '        objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
        '        Exit For
        '    End If
        'Next

        'Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        'objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        'objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        'Dim strFileLocation As String

        'strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        'strFileLocation += Me.Session.SessionID + Me.Loginid + "ReprintKwitansi.pdf"
        'DiskOpts.DiskFileName = strFileLocation
        'objReport.ExportOptions.DestinationOptions = DiskOpts
        'objReport.Export()

        'objReport.PrintToPrinter(1, False, 1, 1)
        'objReport.Close()

        'Response.Redirect("installrcvlist.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "installrcv")
        'Response.Redirect("installrcvlist.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
        Response.Redirect("installrcvlist_Bank.aspx")
    End Sub

    Function getLastAr() As Double
        Dim LastAR As Double
        With oPaymentInfo
            .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
            LastAR = .Prepaid + _
                        .MaximumInstallment + _
                        .MaximumLCInstallFee + _
                        .MaximumInstallCollFee + _
                        .MaximumInsurance + _
                        .MaximumLCInsuranceFee + _
                        .MaximumInsuranceCollFee + _
                        .MaximumPDCBounceFee + _
                        .MaximumSTNKRenewalFee + _
                        .MaximumInsuranceClaimFee + _
                        .MaximumReposessionFee
        End With
        Return LastAR
    End Function
    Sub bindParamsClass(oCustomClass As Parameter.InstallRcv)
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Replace("'", "")
            .CoyID = Me.SesCompanyID
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
            .ApplicationID = Me.ApplicationID

            '-----------Installment
            .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
            .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

            .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
            .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

            .LcInstallment = oPaymentAllocationDetail.LCInstallment
            .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

            .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
            .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
            '------------------------

            '----------Insurance
            .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
            .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

            .LcInsurance = oPaymentAllocationDetail.LCInsurance
            .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

            .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
            .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
            '-----------------------------

            .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
            .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

            .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
            .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

            .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
            .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

            .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
            .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

            .Prepaid = oPaymentAllocationDetail.Prepaid
            .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim

            .BayarDi = oPaymentDetail.BayarDi
            .CollectorID = oPaymentDetail.CollectorID

            .PLL = oPaymentAllocationDetail.PLL
            .PLLDesc = oPaymentAllocationDetail.PLLDesc

            .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
            .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc
        End With
    End Sub
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then
            Dim TotalAllocation As Double

            Dim nilaikuran As Double = getValueGeneralLimit("PREPAIDEXPEND", "AM")
            Dim nilaiLebih As Double = getValueGeneralLimit("PREPAIDINCOME", "AM")
            If oPaymentAllocationDetail.InstallmentDue <> 0 Then
                If oPaymentAllocationDetail.InstallmentDue < oPaymentAllocationDetail.InstallmentDue0 Then
                    If (oPaymentAllocationDetail.InstallmentDue0 - nilaikuran) > oPaymentAllocationDetail.InstallmentDue Then
                        ShowMessage(lblMessage, "Nilai Angsuran di bawah limit minimum", True)
                        Exit Sub
                    End If
                End If
            End If

            If oPaymentAllocationDetail.InstallmentDue > oPaymentAllocationDetail.InstallmentDue0 Then
                If (oPaymentAllocationDetail.InstallmentDue0 + nilaiLebih) < oPaymentAllocationDetail.InstallmentDue Then
                    ShowMessage(lblMessage, "Nilai Angsuran di Atas limit maximum", True)
                    Exit Sub
                End If
            End If

            If ConvertDate2(oValueDate.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari tanggal hari ini", True)
                Exit Sub
            End If

            'If getValueGeneralLimit(ByVal GSID As String, ByVal ModuleID As String)
            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                'If Not CheckCashier(Me.Loginid) Then
                '    ShowMessage(lblMessage, "Kasir belum Buka", True)
                '    Exit Sub
                'End If 
                If Me.ValidationData <> getLastAr() Then
                    ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)
                ElseIf oPaymentAllocationDetail.InstallmentDue > 0 And oPaymentAllocationDetail.InstallmentDue < Me.AmountReceive Then
                    ShowMessage(lblMessage, "Angsuran hanya boleh diisi dengan 0 atau " & FormatNumber(Me.AmountReceive, 0), True)
                Else
                    With oPaymentAllocationDetail
                        TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                                        .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                                        .RepossessionFee + .Prepaid + .PLL

                    End With

                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .BranchId = Me.BranchID
                        .ApplicationID = Me.ApplicationID
                        .AmountReceive = oPaymentAllocationDetail.InstallmentDue
                    End With
                    If oController.IsLastPayment(oCustomClass) Then
                        pnlConfirmation.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        pnlHeadCashierPassword.Visible = False
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = False
                        Exit Sub
                    End If
                    With oCustomClass
                        .ValueDate = ConvertDate2(oValueDate.Text)
                        .ReceivedFrom = oPaymentDetail.ReceivedFrom
                        .ReferenceNo = oPaymentDetail.ReferenceNo
                        .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim  
                        .BankAccountID = oPaymentDetail.BankAccount
                        .Notes = oPaymentDetail.Notes
                        .AmountReceive = TotalAllocation


                        .BayarDi = oPaymentDetail.BayarDi
                        .CollectorID = oPaymentDetail.CollectorID
                    End With
                    bindParamsClass(oCustomClass)
                    Session.Add("BankSelected", oPaymentDetail.BankAccount)

                    Dim strError As String

                    Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        SavingController.InstallmentReceive()
                        BindReport()

                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        End If
    End Sub
    Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim TotalAllocation As Double

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                'If Not CheckCashier(Me.Loginid) Then
                '    ShowMessage(lblMessage, "Kasir belum Buka", True)
                '    Exit Sub
                'End If

                If Me.ValidationData <> getLastAr() Then
                    ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
                ElseIf oPaymentAllocationDetail.InstallmentDue > 0 And oPaymentAllocationDetail.InstallmentDue < Me.AmountReceive Then
                    ShowMessage(lblMessage, "Angsuran hanya boleh diisi dengan 0 atau " & Me.AmountReceive, True)
                Else
                    With oPaymentAllocationDetail
                        TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                                        .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                                        .RepossessionFee + .Prepaid

                    End With

                    If Me.WayOfPayment = "CP" Then
                        If TotalAllocation > Me.PrepaidBalance Then

                            ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
                            Exit Sub
                        End If
                        If oPaymentAllocationDetail.Prepaid > 0 Then

                            ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
                            Exit Sub
                        End If
                    Else
                        If Me.AmountReceive <> TotalAllocation Then

                            ShowMessage(lblMessage, "Total Alokasi Pembayaran harus sama dengan Jumlah diterima", True)
                            Exit Sub
                        End If
                    End If
                    With oCustomClass
                        .ValueDate = ConvertDate2(oViewPaymentDetail.ValueDate)
                        .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
                        .ReferenceNo = oViewPaymentDetail.ReferenceNo
                        .WOP = Me.WayOfPayment.Trim
                        .BankAccountID = Me.BankAccount
                        .Notes = oViewPaymentDetail.Notes
                        .AmountReceive = TotalAllocation
                    End With
                    bindParamsClass(oCustomClass)

                    Dim strError As String

                    Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        SavingController.InstallmentReceive()

                        BindReport()

                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        End If
    End Sub


    'If Me.Page.IsValid Then
    '    'Dim LastAR As Double
    '    Dim TotalAllocation As Double

    '    If ConvertDate2(oValueDate.Text) > Me.BusinessDate Then
    '        ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari tanggal hari ini", True)
    '        Exit Sub
    '    End If

    '    If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then 
    '        'If Not CheckCashier(Me.Loginid) Then

    '        '    ShowMessage(lblMessage, "Kasir belum Buka", True)
    '        '    Exit Sub
    '        'End If
    '        'With oPaymentInfo
    '        '    .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
    '        '    .ApplicationID = Me.ApplicationID
    '        '    .PaymentInfo()
    '        '    'Dim ThreadPaymentInfo As New Thread(AddressOf .PaymentInfo)
    '        '    'ThreadPaymentInfo.Priority = ThreadPriority.BelowNormal
    '        '    'ThreadPaymentInfo.Start()
    '        '    LastAR = .Prepaid + _
    '        '                .MaximumInstallment + _
    '        '                .MaximumLCInstallFee + _
    '        '                .MaximumInstallCollFee + _
    '        '                .MaximumInsurance + _
    '        '                .MaximumLCInsuranceFee + _
    '        '                .MaximumInsuranceCollFee + _
    '        '                .MaximumPDCBounceFee + _
    '        '                .MaximumSTNKRenewalFee + _
    '        '                .MaximumInsuranceClaimFee + _
    '        '                .MaximumReposessionFee
    '        'End With
    '        If Me.ValidationData <> getLastAr() Then 
    '            ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)
    '        Else 
    '            With oPaymentAllocationDetail
    '                TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
    '                                .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
    '                                .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
    '                                .RepossessionFee + .Prepaid + .PLL

    '            End With

    '            'If Me.AmountReceive <> TotalAllocation Then
    '            'If oPaymentDetail.AmountReceive <> TotalAllocation Then
    '            'If oPaymentAllocationDetail.totalTagihan <> TotalAllocation Then
    '            '    ShowMessage(lblMessage, "Total Alokasi Pembayaran harus sama dengan Jumlah diterima", True)
    '            '    Exit Sub
    '            'End If

    '            With oCustomClass
    '                .strConnection = GetConnectionString()
    '                .BranchId = Me.BranchID
    '                .ApplicationID = Me.ApplicationID
    '                .AmountReceive = oPaymentAllocationDetail.InstallmentDue
    '            End With
    '            If oController.IsLastPayment(oCustomClass) Then
    '                pnlConfirmation.Visible = True
    '                pnlBtnGroupPaymentAllocation.Visible = False
    '                pnlHeadCashierPassword.Visible = False
    '                pnlPaymentDetail.Visible = False
    '                pnlViewPaymentDetail.Visible = False
    '                pnlPaymentAllocation.Visible = False
    '                pnlPaymentInfo.Visible = False
    '                pnlBtnGroupPaymentReceive.Visible = False
    '                pnlBtnGroupPaymentAllocation.Visible = False
    '                Exit Sub
    '            End If
    '            With oCustomClass
    '                .ValueDate = ConvertDate2(oValueDate.Text)
    '                .ReceivedFrom = oPaymentDetail.ReceivedFrom
    '                .ReferenceNo = oPaymentDetail.ReferenceNo
    '                .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim 
    '                '.BankAccountID = Me.BankAccount
    '                .BankAccountID = oPaymentDetail.BankAccount
    '                .Notes = oPaymentDetail.Notes
    '                .AmountReceive = TotalAllocation


    '                .BayarDi = oPaymentDetail.BayarDi
    '                .CollectorID = oPaymentDetail.CollectorID


    '                '.strConnection = GetConnectionString()
    '                '.ValueDate = ConvertDate2(oValueDate.Text)
    '                '.BranchId = Me.sesBranchId.Replace("'", "")
    '                '.CoyID = Me.SesCompanyID
    '                '.BusinessDate = Me.BusinessDate
    '                '.ReceivedFrom = oPaymentDetail.ReceivedFrom
    '                '.ReferenceNo = oPaymentDetail.ReferenceNo
    '                '.WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
    '                '.LoginId = Me.Loginid
    '                ''.BankAccountID = Me.BankAccount
    '                '.BankAccountID = oPaymentDetail.BankAccount
    '                '.Notes = oPaymentDetail.Notes
    '                '.ApplicationID = Me.ApplicationID

    '                ''-----------Installment
    '                '.InstallmentDue = oPaymentAllocationDetail.InstallmentDue
    '                '.InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

    '                '.TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
    '                '.TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

    '                '.LcInstallment = oPaymentAllocationDetail.LCInstallment
    '                '.LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

    '                '.InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
    '                '.InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
    '                ''------------------------

    '                ''----------Insurance
    '                '.InsuranceDue = oPaymentAllocationDetail.InsuranceDue
    '                '.InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

    '                '.LcInsurance = oPaymentAllocationDetail.LCInsurance
    '                '.LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

    '                '.InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
    '                '.InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
    '                ''-----------------------------

    '                '.PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
    '                '.PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

    '                '.STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
    '                '.STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

    '                '.InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
    '                '.InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

    '                '.RepossessionFee = oPaymentAllocationDetail.RepossessionFee
    '                '.RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

    '                '.Prepaid = oPaymentAllocationDetail.Prepaid
    '                '.PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim


    '                '.AmountReceive = TotalAllocation


    '                '.PLL = oPaymentAllocationDetail.PLL
    '                '.PLLDesc = oPaymentAllocationDetail.PLLDesc
    '            End With

    '            Session.Add("BankSelected", oPaymentDetail.BankAccount)

    '            Dim strError As String

    '            Try
    '                Dim SavingController As New InstallRcvController(oCustomClass)
    '                SavingController.InstallmentReceive()
    '                'Dim thread1 As New Thread(AddressOf SavingController.InstallmentReceive)
    '                'thread1.Priority = ThreadPriority.AboveNormal
    '                'thread1.Start()
    '                BindReport()

    '            Catch exp As Exception

    '                ShowMessage(lblMessage, exp.Message, True)
    '            End Try
    '        End If
    '    End If
    'End If
    'End Sub

    'Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveLastPayment.Click
    '    If Me.Page.IsValid Then
    '        'Dim LastAR As Double
    '        Dim TotalAllocation As Double

    '        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
    '            'If Not CheckCashier(Me.Loginid) Then

    '            '    ShowMessage(lblMessage, "Kasir belum Buka", True)
    '            '    Exit Sub
    '            'End If
    '            'With oPaymentInfo
    '            '    .ValueDate = ConvertDate2(oValueDate.Text)
    '            '    .ApplicationID = Me.ApplicationID
    '            '    .PaymentInfo()
    '            '    'Dim ThreadPaymentInfo As New Thread(AddressOf .PaymentInfo)
    '            '    'ThreadPaymentInfo.Priority = ThreadPriority.BelowNormal
    '            '    'ThreadPaymentInfo.Start()
    '            '    LastAR = .Prepaid + _
    '            '                .MaximumInstallment + _
    '            '                .MaximumLCInstallFee + _
    '            '                .MaximumInstallCollFee + _
    '            '                .MaximumInsurance + _
    '            '                .MaximumLCInsuranceFee + _
    '            '                .MaximumInsuranceCollFee + _
    '            '                .MaximumPDCBounceFee + _
    '            '                .MaximumSTNKRenewalFee + _
    '            '                .MaximumInsuranceClaimFee + _
    '            '                .MaximumReposessionFee
    '            'End With
    '            If Me.ValidationData <> getLastAr() Then 
    '                ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
    '            Else
    '                With oPaymentAllocationDetail
    '                    TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
    '                                    .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
    '                                    .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
    '                                    .RepossessionFee + .Prepaid

    '                End With

    '                If Me.WayOfPayment = "CP" Then
    '                    If TotalAllocation > Me.PrepaidBalance Then

    '                        ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
    '                        Exit Sub
    '                    End If
    '                    If oPaymentAllocationDetail.Prepaid > 0 Then

    '                        ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
    '                        Exit Sub
    '                    End If
    '                Else
    '                    If Me.AmountReceive <> TotalAllocation Then

    '                        ShowMessage(lblMessage, "Total Alokasi Pembayaran harus sama dengan Jumlah diterima", True)
    '                        Exit Sub
    '                    End If
    '                End If
    '                With oCustomClass
    '                    .strConnection = GetConnectionString()
    '                    .ValueDate = ConvertDate2(oViewPaymentDetail.ValueDate)
    '                    .BranchId = Me.sesBranchId.Replace("'", "")
    '                    .CoyID = Me.SesCompanyID
    '                    .BusinessDate = Me.BusinessDate
    '                    .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
    '                    .ReferenceNo = oViewPaymentDetail.ReferenceNo
    '                    .WOP = Me.WayOfPayment.Trim
    '                    .LoginId = Me.Loginid
    '                    .BankAccountID = Me.BankAccount
    '                    .Notes = oViewPaymentDetail.Notes
    '                    .ApplicationID = Me.ApplicationID

    '                    '-----------Installment
    '                    .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
    '                    .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

    '                    .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
    '                    .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

    '                    .LcInstallment = oPaymentAllocationDetail.LCInstallment
    '                    .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

    '                    .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
    '                    .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
    '                    '------------------------

    '                    '----------Insurance
    '                    .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
    '                    .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim



    '                    .LcInsurance = oPaymentAllocationDetail.LCInsurance
    '                    .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

    '                    .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
    '                    .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
    '                    '-----------------------------

    '                    .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
    '                    .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

    '                    .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
    '                    .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

    '                    .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
    '                    .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

    '                    .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
    '                    .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

    '                    .Prepaid = oPaymentAllocationDetail.Prepaid
    '                    .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
    '                    .AmountReceive = TotalAllocation

    '                    .PLL = oPaymentAllocationDetail.PLL
    '                    .PLLDesc = oPaymentAllocationDetail.PLLDesc
    '                End With
    '                Dim strError As String

    '                Try
    '                    Dim SavingController As New InstallRcvController(oCustomClass)
    '                    SavingController.InstallmentReceive()
    '                    'Dim thread1 As New Thread(AddressOf SavingController.InstallmentReceive)
    '                    'thread1.Priority = ThreadPriority.AboveNormal
    '                    'thread1.Start()
    '                    BindReport()

    '                Catch exp As Exception

    '                    ShowMessage(lblMessage, exp.Message, True)
    '                End Try
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub imbCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelLastPayment.Click
        Response.Redirect("InstallRcvList_Bank.aspx")
    End Sub

    Private Sub IsiKolomTagihan()
        With oPaymentAllocationDetail
            .InstallmentDue0 = Me.AmountReceive ' oPaymentInfo.InstallmentDue
            .LCInstallment0 = oPaymentInfo.MaximumLCInstallFee
            .InstallmentCollFee0 = oPaymentInfo.MaximumInstallCollFee
            .InsuranceDue0 = oPaymentInfo.MaximumInsurance
            .LCInsurance0 = oPaymentInfo.MaximumLCInsuranceFee
            .InsuranceCollFee0 = oPaymentInfo.MaximumInsuranceCollFee
            .PDCBounceFee0 = oPaymentInfo.MaximumPDCBounceFee
            .STNKRenewalFee0 = oPaymentInfo.MaximumSTNKRenewalFee
            .InsuranceClaimExpense0 = oPaymentInfo.MaximumInsuranceClaimFee
            .RepossessionFee0 = oPaymentInfo.MaximumReposessionFee
            .Prepaid0 = oPaymentInfo.Prepaid
            .PLL0 = 0

            .TitipAngsuran0 = 0
            .TitipanAngsuran = 0

            .totalTagihan = .InstallmentDue0 + .LCInstallment0 + .InstallmentCollFee0 + .InsuranceDue0 + _
                   .LCInsurance0 + .InsuranceCollFee0 + .PDCBounceFee0 + .STNKRenewalFee0 + .InsuranceClaimExpense0 + _
                   .RepossessionFee0 + .Prepaid0 + .PLL0

            .PrioritasPembayaran = Me.PrioritasPembayaran

            .InstallmentDue = Me.AmountReceive ' oPaymentInfo.InstallmentDue  
            .LCInstallment = oPaymentInfo.MaximumLCInstallFee
            .InstallmentCollFee = "0" ' oPaymentInfo.MaximumInstallCollFee
            .InsuranceDue = "0" ' oPaymentInfo.MaximumInsurance
            .LCInsurance = "0" ' oPaymentInfo.MaximumLCInsuranceFee
            .InsuranceCollFee = "0" ' oPaymentInfo.MaximumInsuranceCollFee
            .PDCBounceFee = "0" ' oPaymentInfo.MaximumPDCBounceFee
            .STNKRenewalFee = "0" ' oPaymentInfo.MaximumSTNKRenewalFee
            .InsuranceClaimExpense = "0" ' oPaymentInfo.MaximumInsuranceClaimFee
            .RepossessionFee = "0" ' oPaymentInfo.MaximumReposessionFee
            .Prepaid = oPaymentInfo.Prepaid

            .totalBayar = .InstallmentDue + .LCInstallment + .InstallmentCollFee + .InsuranceDue + _
                   .LCInsurance + .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                   .RepossessionFee + .Prepaid + .PLL0

            .MaximumInstallment = Me.AmountReceive + getValueGeneralLimit("PREPAIDINCOME", "AM")
            .PaymentAllocationBind()
        End With
    End Sub

    Private Sub BlokirBayarStep()
        If BlokirBayar Then
            imbSaveProcess.Visible = False
            ShowMessage(lblMessage, "Pembayaran ini diblokir", True)
        Else
            imbSaveProcess.Visible = True
        End If
    End Sub

    Private Sub oValueDate_TextChanged(sender As Object, e As System.EventArgs) Handles oValueDate.TextChanged
        DoBind()
        LoadNextStep()
        BlokirBayarStep()
        oInstallmentSchedule.ApplicationId = Me.ApplicationID
        oInstallmentSchedule.ValueDate = ConvertDate2(oValueDate.Text)
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)
        If Not IsNothing(Session("BankSelected")) Then
            oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
            oPaymentDetail.cmbBankAccountEnabled = False
        End If
    End Sub

    Public Function getValueGeneralLimit(ByVal GSID As String, ByVal ModuleID As String) As Double
        Dim nilai As Double = 0
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "spGetGeneralSetting"
        objCommand.Parameters.Add("@GSID", SqlDbType.VarChar, 50).Value = GSID.Trim
        objCommand.Parameters.Add("@ModuleID", SqlDbType.Char, 3).Value = ModuleID.Trim
        objCommand.Connection = objConnection
        objReader = objCommand.ExecuteReader()
        If objReader.Read Then
            nilai = CDbl(objReader.Item("GSValue").ToString.Trim)
        End If

        Return (nilai)
    End Function
End Class