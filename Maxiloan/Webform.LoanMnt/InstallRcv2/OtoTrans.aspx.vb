﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class OtoTrans
    Inherits Maxiloan.Webform.WebBased
    Private oDataUserCtlr As New DataUserControlController
    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New InstallRcvController
    Protected WithEvents txtPeriodPosting1 As ucDateCE
    Protected WithEvents txtPeriodPosting2 As ucDateCE
    Protected WithEvents txtPeriodValuta1 As ucDateCE
    Protected WithEvents txtPeriodValuta2 As ucDateCE

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 15
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


    Private oCustom As New Parameter.InstallRcv

#Region "Property"
    Private Property TempAPP() As DataTable
        Get
            Return CType(ViewState("TempAPP"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempAPP") = Value
        End Set
    End Property
    Private Property VoucherNo() As String
        Get
            Return CType(ViewState("VoucherNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("VoucherNo") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property Keterangan() As String
        Get
            Return CType(ViewState("Keterangan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Keterangan") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property VoucherNoTemp() As String
        Get
            Return CType(ViewState("VoucherNoTemp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("VoucherNoTemp") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("Status") = "Button" And Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("Message"), False)
            End If
            If Request.QueryString("Status") = "Err" And Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("Message"), True)
            End If

            Me.FormID = "OTOTRANS"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                If Request.QueryString("SearchBy") <> "" Then
                    Me.SearchBy = Request.QueryString("SearchBy")
                    Me.SortBy = ""
                    cmbJenisTransaksi.SelectedValue = Request.QueryString("JenisTransaksiID").ToString.Trim
                    BindBankAccount()
                    cmbBankAccount.SelectedValue = Request.QueryString("BankAccountID").ToString.Trim

                    pnlDatagrid.Visible = True
                    pnlList.Visible = True
                    ClearData()
                    DoBind(Me.SortBy, Me.SearchBy)
                    BindData()
                    pnlDatagrid.Visible = True
                    pnlList.Visible = False
                Else
                    Me.SearchBy = ""
                    Me.SortBy = ""
                    With cmbBankAccount
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = ""
                    End With
                End If

            End If

            If Request("thingstodo") = "1" Then
                Dim strSearch As New StringBuilder
                strSearch.Append(" CashBankTransactions.branchid = '" & Me.sesBranchId.Replace("'", "") & "'")
                Me.SearchBy = strSearch.ToString
                pnlDatagrid.Visible = True
                pnlList.Visible = True
                ClearData()
                DoBind(Me.SortBy, Me.SearchBy)
                BindData()
                pnlDatagrid.Visible = True
                pnlList.Visible = False
            End If

        End If

    End Sub
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With

        oCustomClass = oController.TransaksiPending(oCustomClass)
        recordCount = oCustomClass.TotalRecords
        DtgAgree.DataSource = oCustomClass.ListData

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = False
        pnlDatagrid.Visible = True
        lblMessage.Visible = False
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Response.Redirect("OtoTrans.aspx?Status=Reset")
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        If cmbBankAccount.SelectedValue.Trim <> "" Then
            Dim strSearch As New StringBuilder
            strSearch.Append(" CashBankTransactions.branchid = '" & sesBranchId.Replace("'", "") & "'")

            If cmbBankAccount.SelectedValue <> "" Then
                strSearch.Append(" and CashBankTransactions.BankAccountID = '" & cmbBankAccount.SelectedValue & "' ")
            End If
            If cmbJenisTransaksi.SelectedValue <> "0" Then
                strSearch.Append(" and CashBankTransactions.VoucherNo in (select VoucherNo from TransBeforePosting WHERE FormID =  '" & cmbJenisTransaksi.SelectedValue & "') ")
            End If
            If txtPeriodPosting1.Text.Trim <> "" And txtPeriodPosting2.Text.Trim <> "" Then
                strSearch.Append(" and CashBankTransactions.PostingDate >= '" & ConvertDate2(txtPeriodPosting1.Text.Trim) & "'" & " and CashBankTransactions.PostingDate <= '" & ConvertDate2(txtPeriodPosting2.Text.Trim) & "'")
            End If
            If txtPeriodValuta1.Text.Trim <> "" And txtPeriodValuta2.Text.Trim <> "" Then
                strSearch.Append(" and CashBankTransactions.ValueDate >= '" & ConvertDate2(txtPeriodValuta1.Text.Trim) & "'" & " and CashBankTransactions.ValueDate <= '" & ConvertDate2(txtPeriodValuta2.Text.Trim) & "'")
            End If

            Me.SearchBy = strSearch.ToString
            pnlDatagrid.Visible = True
            pnlList.Visible = True
            ClearData()
            DoBind(Me.SortBy, Me.SearchBy)
            BindData()
            pnlDatagrid.Visible = True
            pnlList.Visible = False
        Else
            pnlDatagrid.Visible = False
            pnlList.Visible = True
        End If

    End Sub
    Private Sub DtgAgree_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Otorisasi"
                Dim lblVoucherNo As Label
                Dim lblAgreementNo As HyperLink
                Dim lblBankAccountName As Label
                Dim lblDescription As Label
                Dim lblCustomerName As Label
                Dim lblAmount As Label
                Dim lblCashierId As Label


                lblVoucherNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblVoucherNo"), Label)
                lblAgreementNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblAgreementNo"), HyperLink)
                lblBankAccountName = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblBankAccountName"), Label)
                lblDescription = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblDescription"), Label)
                lblCustomerName = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblCustomerName"), Label)
                lblAmount = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblAmount"), Label)
                lblCashierId = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblCashierId"), Label)

                Dim cookieNew As New HttpCookie("otorisasi")
                cookieNew.Values.Add("VoucherNo", lblVoucherNo.Text)
                cookieNew.Values.Add("AgreementNo", lblAgreementNo.Text)
                cookieNew.Values.Add("BankAccountName", lblBankAccountName.Text)
                cookieNew.Values.Add("Description", lblDescription.Text)
                cookieNew.Values.Add("CustomerName", lblCustomerName.Text)
                cookieNew.Values.Add("Amount", lblAmount.Text)
                cookieNew.Values.Add("CashierId", lblCashierId.Text)

                cookieNew.Values.Add("PostingDate", e.Item.Cells.Item(5).Text)
                cookieNew.Values.Add("ValutaDate", e.Item.Cells.Item(6).Text)


                Response.AppendCookie(cookieNew)
                Response.Redirect("OtoTransDetail.aspx")
        End Select
    End Sub
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound        
        Dim lblApplicationid As HyperLink
        Dim lblAgreementNo As HyperLink
        Dim lblAmount As Label
        Dim lblAmountDetail As Label
        Dim lblAngske As Label
        Dim lblCashierId As Label        
        Dim indexC As Integer

        If e.Item.ItemIndex >= 0 Then           
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            lblAmountDetail = CType(e.Item.FindControl("lblAmountDetail"), Label)
            lblAngske = CType(e.Item.FindControl("lblAngske"), Label)
            lblCashierId = CType(e.Item.FindControl("lblCashierId"), Label)

            If CDec(lblAmount.Text) = 0 Then
                lblAmount.Text = ""
                lblAngske.Text = ""
                lblCashierId.Text = ""
            Else
                lblAmount.Text = FormatNumber(lblAmount.Text)
                For indexC = 0 To e.Item.Cells.Count - 1
                    e.Item.Cells(indexC).Attributes.CssStyle.Add("background-color", "#cccccc")
                Next

            End If

            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            If CDec(lblAmountDetail.Text) = 0 Then
                lblAmountDetail.Text = ""                
            End If

        End If
    End Sub
    Sub ClearData()
        TempAPP = New DataTable
        With TempAPP
            .Columns.Add(New DataColumn("VoucherNo", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
        End With
    End Sub
    Sub BindData()
        Dim DataList As New List(Of Parameter.InstallRcv)
        Dim custom As New Parameter.InstallRcv
        Dim lblApplicationid As HyperLink
        Dim lblVoucherNo As Label
        Dim lblDescription As Label
        Dim oRow As DataRow

        If TempAPP Is Nothing Then
            TempAPP = New DataTable
            With TempAPP
                .Columns.Add(New DataColumn("VoucherNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))                
            End With
        Else
            For index = 0 To TempAPP.Rows.Count - 1
                custom = New Parameter.InstallRcv

                custom.VoucherNO = TempAPP.Rows(index).Item("VoucherNo").ToString.Trim
                custom.ApplicationID = TempAPP.Rows(index).Item("ApplicationID").ToString.Trim                

                DataList.Add(custom)
            Next
        End If

        For intLoopGrid = 0 To DtgAgree.Items.Count - 1
            Dim chkItem As CheckBox
            chkItem = CType(DtgAgree.Items(intLoopGrid).FindControl("chkItem"), CheckBox)
            lblVoucherNo = CType(DtgAgree.Items(intLoopGrid).FindControl("lblVoucherNo"), Label)
            lblApplicationid = CType(DtgAgree.Items(intLoopGrid).FindControl("lblApplicationid"), HyperLink)
            lblDescription = CType(DtgAgree.Items(intLoopGrid).FindControl("lblDescription"), Label)

            Me.VoucherNo = lblVoucherNo.Text.Trim
            Me.ApplicationID = lblApplicationid.Text.Trim
            Me.Keterangan = lblDescription.Text.Trim

            Dim query As New Parameter.InstallRcv
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If query Is Nothing Then
                If Me.Keterangan.Trim = "" Then
                    chkItem.Visible = True
                Else
                    chkItem.Visible = False
                End If

                oRow = TempAPP.NewRow()
                oRow("VoucherNo") = Me.VoucherNo
                oRow("ApplicationID") = Me.ApplicationID
                TempAPP.Rows.Add(oRow)

                custom = New Parameter.InstallRcv
                custom.VoucherNO = Me.VoucherNo
                custom.ApplicationID = Me.ApplicationID

                DataList.Add(custom)
            Else
                chkItem.Visible = False
            End If
        Next
    End Sub
    Public Function PredicateFunction(ByVal custom As Parameter.InstallRcv) As Boolean
        Return Me.VoucherNo = custom.VoucherNO And Me.ApplicationID = custom.ApplicationID
    End Function
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        ClearData()
        DoBind(Me.SortBy, Me.SearchBy)
        BindData()        
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                ClearData()
                DoBind(Me.SortBy, Me.SearchBy)
                BindData()                
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
            BindData()            
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
            BindData()            
        End If

    End Sub

#End Region
    Private Sub imgSave_Click(sender As Object, e As System.EventArgs) Handles imgSave.Click

        Dim lblApplicationID As Label
            Dim lblVoucherNo As Label
            Dim chkDtList As CheckBox
            PopulateData()

            Try
                'For i = 0 To DtgAgree.Items.Count - 1
                '    chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
                '    If Not IsNothing(chkDtList) Then
                '        If chkDtList.Visible Then
                '            If chkDtList.Checked Then
                '                lblVoucherNo = CType(DtgAgree.Items(i).FindControl("lblVoucherNo"), Label)
                '                With oCustom
                '                    .strConnection = GetConnectionString()
                '                    .VoucherNO = lblVoucherNo.Text
                '                End With
                '                oController.SaveOtorisasi(oCustom)
                '            End If
                '        End If
                '    End If
                'Next

                If TempDataTable.Rows.Count > 0 Then
                    With oCustom
                        .strConnection = GetConnectionString()
                        .TableVoucherNO = TempDataTable
                    End With
                    oController.SaveOtorisasi(oCustom)
                    Response.Redirect("OtoTrans.aspx?Status=Button&Message=Proses Approved Berhasil&SearchBy=" & Me.SearchBy & "&BankAccountID=" & cmbBankAccount.SelectedValue & "&JenisTransaksiID=" & cmbJenisTransaksi.SelectedValue & "")
                Else
                    ShowMessage(lblMessage, "Data Belum dipilih!", True)
                End If
            Catch ex As Exception
                'Response.Redirect("OtoTrans.aspx?Status=Err&Message=" & ex.Message & "")
                ShowMessage(lblMessage, ex.Message, True)
            End Try

    End Sub
    Private Sub imbReject_Click(sender As Object, e As System.EventArgs) Handles imbReject.Click
        Dim lblApplicationID As Label
        Dim lblVoucherNo As Label
        Dim chkDtList As CheckBox

        Try
            For i = 0 To DtgAgree.Items.Count - 1
                chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Visible Then
                        If chkDtList.Checked Then
                            lblVoucherNo = CType(DtgAgree.Items(i).FindControl("lblVoucherNo"), Label)
                            With oCustom
                                .strConnection = GetConnectionString()
                                .VoucherNO = lblVoucherNo.Text
                            End With
                            oController.RejectOtorisasi(oCustom)
                        End If
                    End If                    
                End If
            Next
            Response.Redirect("OtoTrans.aspx?Status=Button&Message=Proses Reject Berhasil&SearchBy=" & Me.SearchBy & "&BankAccountID=" & cmbBankAccount.SelectedValue & "&JenisTransaksiID=" & cmbJenisTransaksi.SelectedValue & "")
        Catch ex As Exception
            'Response.Redirect("OtoTrans.aspx?Status=Err&Message=" & ex.Message & "")
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        
    End Sub
    Private Sub cmbJenisTransaksi_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbJenisTransaksi.SelectedIndexChanged
        BindBankAccount()
    End Sub
    Sub BindBankAccount()
        Dim oInstal As New Parameter.InstallRcv
        Dim data As New DataTable
        Dim whereCond As String = ""

        If cmbJenisTransaksi.SelectedValue.Trim = "INSTALLRCV_BA" Then
            whereCond = " BranchId = '" + sesBranchId.Replace("'", "").Trim + "' and BankAccountType='B'"
        End If
        If cmbJenisTransaksi.SelectedValue.Trim = "INSTALLRCV_COL" Then
            whereCond = " BranchId = '" + sesBranchId.Replace("'", "").Trim + "' and BankAccountType='C' and BankPurpose='EC'"
        End If
        If Not cmbJenisTransaksi.SelectedValue.Trim = "INSTALLRCV_BA" And Not cmbJenisTransaksi.SelectedValue.Trim = "INSTALLRCV_COL" Then
            whereCond = " BranchId = '" + sesBranchId.Replace("'", "").Trim + "' and BankAccountType in ('B','C') and BankPurpose not in ('FD','PC')"
        End If

        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetBankAccountOtor"
        oInstal.WhereCond = whereCond
        data = oController.GetSP(oInstal).ListData()

        With cmbBankAccount
            .DataSource = data
            .DataTextField = "name"
            .DataValueField = "id"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub imgEdit_Click(sender As Object, e As System.EventArgs) Handles imgEdit.Click
        Dim lblApplicationID As Label
        Dim lblVoucherNo As Label
        Dim chkDtList As CheckBox

        Try
            For i = 0 To DtgAgree.Items.Count - 1
                chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Visible Then
                        If chkDtList.Checked Then
                            lblVoucherNo = CType(DtgAgree.Items(i).FindControl("lblVoucherNo"), Label)
                            With oCustom
                                .strConnection = GetConnectionString()
                                .VoucherNO = lblVoucherNo.Text
                            End With
                            oController.EditOtorisasi(oCustom)
                        End If
                    End If                    
                End If
            Next
            Response.Redirect("OtoTrans.aspx?Status=Button&Message=Proses Edit Berhasil&SearchBy=" & Me.SearchBy & "&BankAccountID=" & cmbBankAccount.SelectedValue & "&JenisTransaksiID=" & cmbJenisTransaksi.SelectedValue & "")
        Catch ex As Exception
            'Response.Redirect("OtoTrans.aspx?Status=Err&Message=" & ex.Message & "")
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        
    End Sub
    Private Sub imbBack_Click(sender As Object, e As System.EventArgs) Handles imbBack.Click
        Response.Redirect("OtoTrans.aspx?Status=Reset")
    End Sub
    Sub PopulateData()
        TempDataTable = New DataTable
        Dim DataList As New List(Of String)        

        With TempDataTable
            .Columns.Add(New DataColumn("VoucherNo", GetType(String)))
        End With

        For index = 0 To DtgAgree.Items.Count - 1
            Dim VoucherNo As String
            Dim chek As New CheckBox
            chek = CType(DtgAgree.Items(index).FindControl("chkItem"), CheckBox)

            If chek.Checked Then
                Dim oRow As DataRow
                Dim tempStr As String = CType(DtgAgree.Items(index).FindControl("lblVoucherNo"), Label).Text.Trim()
                Dim query As String

                Me.VoucherNoTemp = tempStr
                If DataList.Count > 0 Then
                    query = DataList.Find(AddressOf PredicateFunction)
                Else
                    query = ""
                End If

                If query = "" Then
                    oRow = TempDataTable.NewRow()
                    oRow("VoucherNo") = Me.VoucherNoTemp
                    TempDataTable.Rows.Add(oRow)
                End If
            End If

            For i = 0 To TempDataTable.Rows.Count - 1
                VoucherNo = ""

                VoucherNo = TempDataTable.Rows(i).Item("VoucherNo").ToString.Trim

                DataList.Add(VoucherNo)
            Next
        Next

    End Sub
    Public Function PredicateFunction(ByVal custom As String) As String
        Return custom = Me.VoucherNoTemp
    End Function
End Class