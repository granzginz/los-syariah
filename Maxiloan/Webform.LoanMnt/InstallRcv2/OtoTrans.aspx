﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtoTrans.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.OtoTrans" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Otorisasi Transaksi</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>    
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>    
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                OTORISASI TRANSAKSI
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Transaksi
                </label>
                <asp:DropDownList ID="cmbJenisTransaksi" runat="server" AutoPostBack="true" Width="280px">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <%--<asp:ListItem Value="InstallRcvList">Penerimaan Angsuran Tunai</asp:ListItem>--%>
                    <asp:ListItem Value="INSTALLRCV_BA">Penerimaan Angsuran (Bank)</asp:ListItem>
                    <asp:ListItem Value="INSTALLRCV_BA_FACT">Penerimaan Angsuran (Factoring)</asp:ListItem>
					<asp:ListItem Value="INSTALLRCV_BA_MDKJ">Penerimaan Angsuran (Modalkerja)</asp:ListItem>
                    <asp:ListItem Value="INSTALLRCV_PCBA_MDKJ">Pelunasan Percepat Angsuran (Modalkerja)</asp:ListItem>
<%--                    <asp:ListItem Value="ADVINS">Advance Installment</asp:ListItem>
                    <asp:ListItem Value="SUSPENDALLOCATION">Alokasi Suspend</asp:ListItem>--%>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                    InitialValue="0" ControlToValidate="cmbJenisTransaksi" ErrorMessage="Harap pilih Jenis Transaksi"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class="label_split">
                    Sumber Dokumen
                </label>
                <asp:DropDownList ID="cmbBankAccount" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="cmbBankAccount" ErrorMessage="Harap pilih Sumber Dokumen"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Posting</label>
                <uc1:ucdatece id="txtPeriodPosting1" runat="server" />
                &nbsp; S/D
                <uc1:ucdatece id="txtPeriodPosting2" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Valuta</label>
                <uc1:ucdatece id="txtPeriodValuta1" runat="server" />
                &nbsp; S/D
                <uc1:ucdatece id="txtPeriodValuta2" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                        OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                        AllowSorting="True">
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PILIH" Visible="false">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconReceived.gif" runat="server"
                                        CommandName="Otorisasi"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOUCHER">
                                <ItemTemplate>
                                    <asp:Label ID="lblVoucherNo" runat="server" Text='<%#Container.DataItem("VoucherNo")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="BANK">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'> </asp:HyperLink>
                                    <asp:HyperLink ID="lblApplicationid" runat="server" Visible="false" Text='<%#Container.DataItem("Applicationid")%>'> </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="KONSUMEN">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PostingDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL POSTING"
                                SortExpression="PostingDate"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ValueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL VALUTA"
                                SortExpression="ValueDate"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),0)%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="ALOKASI">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AmountDetail" HeaderText="JUMLAH DETAIL">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountDetail" runat="server" Text='<%#formatnumber(Container.DataItem("AmountDetail"),0)%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Angske" HeaderText="ANGSKE">
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAngske" runat="server" Text='<%#Container.DataItem("Angske")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CashierId" HeaderText="USER ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCashierId" runat="server" Text='<%#Container.DataItem("CashierId")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgSave" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"></asp:Button>            
            <div class="form_box_hide">
                <asp:Button ID="imgEdit" runat="server" Text="Edit" CssClass="small button orange"
                CausesValidation="False"></asp:Button>
            </div>
            <asp:Button ID="imbReject" runat="server" Text="Reject" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="imbBack" runat="server" Text="Back" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>    
    </form>
</body>
</html>
