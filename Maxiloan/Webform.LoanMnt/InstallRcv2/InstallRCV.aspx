﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRCV.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InstallRCV" %>

<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocationDetail" Src="../../Webform.UserController/UcPaymentAllocationDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Installment Receive</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
    
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PEMBAYARAN TUNAI
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <%--<div class="form_box">
            <div class="form_left">
                <label>
                    Cabang</label>
                <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer</label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <%-- <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Angsuran</label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Angsuran Jatuh Tempo
                </label>
                <asp:Label ID="lblNextInstallmentDate" runat="server"></asp:Label>
                &nbsp;&nbsp;|&nbsp;Angsuran Ke-&nbsp;
                <asp:Label ID="lblNextInstallmentNumber" runat="server" Width="3px"></asp:Label>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Saldo Angsuran
                </label>
                <asp:Label ID="lblAmountToBePaid" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Saldo Prepaid
                </label>
                <asp:Label ID="lblPrepaid" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Titipan Angsuran</label>
                <asp:Label ID="lblTitipanAngsuran" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>Prepaid Status</label>
                <asp:Label id="lblPrepaidStatus" runat="server"></asp:Label>
            </div>
        </div>
        <%-- <div class="form_box">
            <div class="form_left">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Status Penjaminan</label>
                <asp:Label ID="lblFundingPledgeStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nilai Residu</label>
                <asp:Label ID="lblResiduValue" runat="server"></asp:Label>
            </div>
        </div>--%>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    TABEL ANGSURAN
                </h5>
            </div>
        </div>
        <uc1:ucinstallmentschedule id="oInstallmentSchedule" runat="server" />
        <asp:Panel ID="pnlPaymentDetail" runat="server" Visible="false">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        RINCIAN PEMBAYARAN
                    </h4>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucpaymentdetail id="oPaymentDetail" runat="server" />
            </div>
            <div class="form_box">
                <div class="form_left"></div>
                <div class="form_right">
                    <label>Tanggal Bayar</label>
                    <uc1:ucdatece id="oValueDate" runat="server" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlHeadCashierPassword" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        KONFIRMASI KEPALA KASIR
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Kepala Kasir
                    </label>
                    <uc1:uccashier id="oCashier" runat="server" />
                </div>
                <div class="form_right">
                    <label class="label_req">
                        Password Kasir
                    </label>
                    <asp:TextBox ID="txtCashierPassword" runat="server"  MaxLength="20"
                        TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtCashierPassword"
                        Display="Dynamic" ErrorMessage="Harap isi Password" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlViewPaymentDetail" runat="server">
            <div class="form_box_uc">
                <uc1:ucviewpaymentdetail id="oViewPaymentDetail" runat="server"></uc1:ucviewpaymentdetail>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlPaymentAllocation" runat="server">
            <div class="form_box_uc">
                <uc1:ucpaymentallocationdetail id="oPaymentAllocationDetail" runat="server" showpanelcashier="true"></uc1:ucpaymentallocationdetail>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtnGroupPaymentReceive" runat="server">
            <div class="form_button">
                <asp:Button ID="ButtonNextProcess" runat="server" CssClass="small button blue" Text="Next" />
                <asp:Button ID="ButtonCancelProcess" runat="server" CausesValidation="False" CssClass="small button gray"
                    Text="Cancel" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirmation" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        KONFIRMASI KEPALA KASIR
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    Ini Adalah Pembayaran Terakhir
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSaveLastPayment" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/>
                <asp:Button ID="ButtonCancelLastPayment" runat="server" CausesValidation="False"
                    Text="Cancel" CssClass="small button gray" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtnGroupPaymentAllocation" runat="server">
            <div class="form_button">
                <asp:Button ID="ButtonSaveProcess" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/>
                <asp:Button ID="ButtonCancel2Process" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray" />
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <div class="form_box_uc">
            <uc1:ucpaymentinfo id="oPaymentInfo" runat="server" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
<script type = "text/javascript">
    function preventMultipleSubmissions() {
        $('#<%=ButtonSaveLastPayment.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        $('#<%=ButtonSaveProcess.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
    }
    window.onbeforeunload = preventMultipleSubmissions;
    </script>