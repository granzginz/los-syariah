﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtoTransDetail.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.OtoTransDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Detail Transaksi</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
   
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
      <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                OTORISASI TRANSAKSI
            </h4>
        </div>
      </div>
      <div class="form_box">
          <div class="form_left">
                <label>NO Voucher</label>
                <asp:Label ID="lblVoucherNO" runat="server"></asp:Label>
          </div>
          <div class="form_right">
                <label>Nama Konsumen</label>
                <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
          </div>
      </div>
      <div class="form_box">
          <div class="form_left">
                <label>Bank</label>
                <asp:Label ID="lblBankAccountName" runat="server"></asp:Label>
          </div>
          <div class="form_right">
                <label>Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
          </div>
      </div>
      <div class="form_box">
          <div class="form_left">
                <label>Tgl Posting</label>
                <asp:Label ID="lblPostingDate" runat="server"></asp:Label>
          </div>
          <div class="form_right">
                <label>Jumlah</label>
                <asp:Label ID="lblAmount" runat="server"></asp:Label>
          </div>
      </div>
      <div class="form_box">
          <div class="form_left">
                <label>Tgl Valuta</label>
                <asp:Label ID="lblValueDate" runat="server"></asp:Label>
          </div>
          <div class="form_right">
                <label>User ID</label>
                <asp:Label ID="lblCashierId" runat="server"></asp:Label>
          </div>
      </div>
      <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DETAIL TRANSAKSI
            </h4>
        </div>
      </div>
      <div class="form_box_header">
          <div class="form_single">
                <asp:DataGrid ID="DtgDtl" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                    BorderStyle="None" CssClass="grid_general" >
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="SeqNo" HeaderText="NO.URUT" SortExpression="SeqNo"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Description" HeaderText="KETERANGAN" SortExpression="Description"></asp:BoundColumn>
                        <asp:BoundColumn DataField="DebitAmount" HeaderText="Debit" SortExpression="DebitAmount" DataFormatString="{0:N}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CreditAmount" HeaderText="Kredit" SortExpression="CreditAmount" DataFormatString="{0:N}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="InsSeqNo" HeaderText="Angs Ke" SortExpression="InsSeqNo"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
          </div>
      </div>
      <div class="form_button">
            <asp:Button ID="imgSave" runat="server" EnableViewState="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="imbCancel" runat="server" EnableViewState="False" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="imbReject" runat="server" EnableViewState="False" Text="Reject" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </form>
</body>
</html>
