﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports System.IO


#End Region

Public Class InstallRCV_Collection
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oViewPaymentDetail As UCViewPaymentDetail
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule

    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String
    Private Property HistorySequenceNo As Integer
    Protected WithEvents oValueDate As ucDateCE

#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId

            Me.FormID = "INSTALLRCV_COL"
            Me.Mode = "Normal"
            oCashier.CashierID = ""
            oCashier.HeadCashier = True
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Not CheckCashier(Me.Loginid) Then

                    ShowMessage(lblMessage, "Kasir Belum Buka", True)
                    Dim ThreadInitialPanel As New Thread(AddressOf InitialDefaultPanel)
                    ThreadInitialPanel.Priority = ThreadPriority.AboveNormal
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = False
                    Dim ThreadBindData As New Thread(AddressOf DoBind)
                    ThreadBindData.Priority = ThreadPriority.AboveNormal
                    ThreadInitialPanel.Start()
                    ThreadBindData.Start()
                    Exit Sub
                End If
                'oPaymentDetail.TglBayarTxt.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                oValueDate.AutoPostBack = True
                oValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                InitialDefaultPanel()
                DoBind()

                LoadNextStep()
                BlokirBayarStep()

                'load grid angsuran
                oInstallmentSchedule.ApplicationId = Me.ApplicationID
                oInstallmentSchedule.ValueDate = Me.BusinessDate
                oInstallmentSchedule.DoBind_Angsuran(String.Empty)

                ModConnection.ApplicationID = Me.ApplicationID
                ModConnection.StrConn = GetConnectionString()
            End If
        Else
            If Not CheckCashier(Me.Loginid) Then
                ShowMessage(lblMessage, "Kasir Belum Buka", True)
                pnlPaymentDetail.Visible = True
                pnlViewPaymentDetail.Visible = False
                pnlPaymentAllocation.Visible = False
                pnlBtnGroupPaymentReceive.Visible = False
                pnlBtnGroupPaymentAllocation.Visible = False
                pnlPaymentInfo.Visible = False
                Exit Sub
            End If

        End If


    End Sub
    'Private Sub tglBayar_TextChanged(sender As Object, e As System.EventArgs) Handles oPaymentDetail.TextChanged
    '    InitialDefaultPanel()
    '    DoBind()

    '    LoadNextStep()
    '    BlokirBayarStep()

    '    'load grid angsuran
    '    oInstallmentSchedule.ApplicationId = Me.ApplicationID
    '    oInstallmentSchedule.ValueDate = Me.BusinessDate
    '    oInstallmentSchedule.DoBind_Angsuran(String.Empty)

    'End Sub

  
    Private Sub LoadNextStep()
        'scl
        NextProcess()
        If Me.WayOfPayment <> "CP" Then
            If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                Else
                    If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                        oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                        oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                    Else
                        oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                    End If
                End If
            Else
                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
            End If

            IsiKolomTagihan()
        End If
        '/scl
    End Sub
    Private Sub InitialDefaultPanel()
        pnlPaymentDetail.Visible = True
        pnlViewPaymentDetail.Visible = False
        pnlPaymentAllocation.Visible = True
        pnlBtnGroupPaymentReceive.Visible = False
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlPaymentInfo.Visible = False
        pnlHeadCashierPassword.Visible = False
        pnlConfirmation.Visible = False


    End Sub

    Private Sub DoBind()
        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            '.ValueDate = Me.BusinessDate
            .ValueDate = ConvertDate2(oValueDate.Text)
            .BranchId = Me.BranchID
        End With
        oCustomClass = oController.GetPaymentInfo(oCustomClass)
        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            'lblAgreementBranch.Text = .BranchAgreement
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            'lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
            'lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
            Me.NextInstallmentDate = .NextInstallmentDate
            'lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)

            If .ContractStatus <> "OSD" Then
                oPaymentDetail.AmountReceive = .InstallmentAmount
            Else
                oPaymentDetail.AmountReceive = .InstallmentDue
            End If

            'lblFundingCoyName.Text = .FundingCoyName
            'lblFundingPledgeStatus.Text = .FundingPledgeStatus
            'lblResiduValue.Text = FormatNumber(.ResiduValue, 2)
            lblPrepaid.Text = FormatNumber(.ContractPrepaidAmount, 2)
            Me.PrepaidBalance = .ContractPrepaidAmount
            Me.AmountToBePaid = .AmountToBePaid
            lblAmountToBePaid.Text = FormatNumber(Me.AmountToBePaid, 2)
            lblTitipanAngsuran.Text = FormatNumber(.TitipanAngsuran)

            lblPrepaidStatus.Text = .PrepaidHoldStatusDesc
            lblPrepaidStatus.Font.Bold = True

            oPaymentAllocationDetail.ToleransiBayarKurang = 0 '.ToleransiBayarKurang
            oPaymentAllocationDetail.ToleransiBayarLebih = 0 '.ToleransiBayarLebih
            'scl
            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "CP"
            oPaymentDetail.DoFormCashModule()
            'oPaymentDetail.AutoPostBack = True

            oPaymentDetail.ApplicationID = Me.ApplicationID

            'If .CollectorID <> "-" Then
            '    oPaymentDetail.CollectorID = .CollectorID
            '    oPaymentDetail.BindKwitansiCollection(.CollectorID)

            '    oPaymentDetail.ReceiptNo = .ReceiptNo
            '    oPaymentDetail.EnableCollector()
            'End If
            

            Me.BankAccount = oPaymentDetail.BankAccount

            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar


        End With
    End Sub

#Region "BtnGroup Payment Receive"
    Private Sub imbNextProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbNextProcess.Click
        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            lblMessage.Text = ""
            If oPaymentDetail.WayOfPayment = "CP" Then
                If Me.BranchID <> Me.sesBranchId.Trim.Replace("'", "") Then

                    ShowMessage(lblMessage, "Cara Pembayaran Tidak bisa untuk Transaksi Antar Cabang", True)
                    Exit Sub
                End If
            End If
            If Me.IsValidLastPayment(Me.ApplicationID, ConvertDate2(oPaymentDetail.ValueDate)) Then
                If Me.Mode = "Normal" Then
                    NextProcess()
                    'Dim ThreadNextProcess As New Thread(AddressOf NextProcess)
                    'ThreadNextProcess.Priority = ThreadPriority.BelowNormal

                    If Not CheckCashier(Me.Loginid) Then

                        ShowMessage(lblMessage, "Kasir belum Buka", True)
                        Exit Sub
                    End If
                    If Me.WayOfPayment = "CP" Then
                        If Me.PrepaidBalance = 0 Then

                            ShowMessage(lblMessage, "Tidak bisa Alokasi Saldo Prepaid, Karena Saldo Kurang", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    Else
                        If oPaymentDetail.AmountReceive = 0 Then

                            ShowMessage(lblMessage, "Harap diisi dengan Jumlah diterima", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    End If
                    'If IsMaxBacDated(ConvertDate2(oPaymentDetail.TglBayarTxt.Text)) Then
                    If IsMaxBacDated(ConvertDate2(oPaymentDetail.ValueDate)) Then
                        oPaymentDetail.WayOfPayment = "0"
                        Me.Mode = "NotNormal"
                        pnlHeadCashierPassword.Visible = True
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        oCashier.CashierID = ""
                        oCashier.HeadCashier = True

                        Exit Sub
                        'ElseIf DateDiff(DateInterval.Day, ConvertDate2(oPaymentDetail.TglBayarTxt.Text), Me.BusinessDate) < 0 Then
                    ElseIf DateDiff(DateInterval.Day, ConvertDate2(oPaymentDetail.ValueDate), Me.BusinessDate) < 0 Then
                        oPaymentDetail.WayOfPayment = "0"

                        ShowMessage(lblMessage, "Tanggal Valuta tidak boleh lebih besar dari tgl hari ini", True)
                        Exit Sub
                    End If


                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = True
                    pnlPaymentAllocation.Visible = True
                    pnlPaymentInfo.Visible = True
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = True
                    If Me.WayOfPayment <> "CP" Then
                        If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                            If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                            Else
                                If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                                Else
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                                End If
                            End If
                        Else
                            oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                        End If

                        IsiKolomTagihan() 'scl

                    End If
                    'ThreadNextProcess.Start()
                ElseIf Me.Mode = "NotNormal" Then
                    Dim objDecent As New Decrypt.am_futility
                    Dim oLogin As New LoginController
                    Dim oLoginEntities As New Parameter.Login
                    Dim strError As String
                    oLoginEntities.strConnection = GetConnectionString()
                    oLoginEntities.LoginId = oCashier.CashierID.Trim
                    Dim lstrPassword As String
                    'oLogin.GetEmployee(oLoginEntities) retreive password dari user
                    lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
                    If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = True
                        pnlPaymentAllocation.Visible = True
                        pnlPaymentInfo.Visible = True
                        pnlHeadCashierPassword.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = True
                        Me.Mode = "Normal"
                    Else

                        ShowMessage(lblMessage, "Password Salah", True)
                        Exit Sub
                    End If
                End If
            Else

                ShowMessage(lblMessage, "Tanggal Valuta Lebih kecil dari tanggal pembayaran terakhir", True)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub NextProcess()

        With oPaymentInfo
            'oPaymentDetail.ValueDate = Format(Me.BusinessDate, "dd/MM/yyyy")
            oPaymentDetail.ValueDate = oValueDate.Text
            '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
            .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()

            Me.ValidationData = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
        End With

        Me.AmountReceive = oPaymentDetail.AmountReceive
        'With oViewPaymentDetail
        '    Me.WayOfPayment = oPaymentDetail.WayOfPayment
        '    .WayOfPayment = oPaymentDetail.WayOfPaymentName
        '    Me.BankAccount = oPaymentDetail.BankAccount
        '    .BankAccount = oPaymentDetail.BankAccountName
        '    If Me.WayOfPayment = "CP" Then
        '        Me.AmountReceive = 0
        '        .AmountReceive = 0
        '    Else
        '        Me.AmountReceive = oPaymentDetail.AmountReceive
        '        .AmountReceive = oPaymentDetail.AmountReceive
        '    End If

        '    .ReceivedFrom = oPaymentDetail.ReceivedFrom
        '    .ReferenceNo = oPaymentDetail.ReferenceNo

        '    .ValueDate = oPaymentDetail.TglBayarTxt.Text
        '    .Notes = oPaymentDetail.Notes
        '    .BayarDI = oPaymentDetail.BayarDi
        '    .CollectorName = oPaymentDetail.CollectorName
        'End With
        With oPaymentAllocationDetail
            .MaximumInstallment = oPaymentInfo.MaximumInstallment
            '=================================
            .MaximumLCInstallFee = 999999999999
            .MaximumInstallCollFee = 999999999999
            .MaximumLCInsuranceFee = 999999999999
            .MaximumInsuranceCollFee = 999999999999

            .MaximumPDCBounceFee = 999999999999
            .MaximumSTNKRenewalFee = 999999999999
            .MaximumInsuranceClaimFee = 999999999999
            .MaximumReposessionFee = 999999999999


            '.MaximumLCInstallFee = oPaymentInfo.MaximumLCInstallFee
            '.MaximumInstallCollFee = oPaymentInfo.MaximumInstallCollFee

            .MaximumInsurance = oPaymentInfo.MaximumInsurance
            '.MaximumLCInsuranceFee = oPaymentInfo.MaximumLCInsuranceFee
            '.MaximumInsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee

            '.MaximumPDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
            '.MaximumSTNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee
            '.MaximumInsuranceClaimFee = oPaymentInfo.MaximumInsuranceClaimFee
            '.MaximumReposessionFee = oPaymentInfo.MaximumReposessionFee

            If oPaymentInfo.PrepaidHoldStatus.Trim <> "NM" Then
                .DisabledAll = True
            End If

            If oPaymentInfo.PrepaidHoldStatus.Trim = "DC_" Then
                imbSaveProcess.Visible = False
                .DisabledAll = True
                ShowMessage(lblMessage, "Applikasi ini sedang ada pengajuan Grace Periode (Belum di Approve / Eksekusi)", True)
            End If

            If oPaymentInfo.PrepaidHoldStatus.Trim = "DC" Then
                .DisableForGracePeriod = True
            End If

            If (oPaymentInfo.ContractStatus.Trim = "PRP" Or _
                oPaymentInfo.ContractStatus.Trim = "INV" Or _
                oPaymentInfo.ContractStatus.Trim = "LNS") Then
                .DisabledAll = True
            End If

            .PaymentAllocationBind()
            'Dim ThreadBindPaymentAllocationBind As New Thread(AddressOf .PaymentAllocationBind)
            'ThreadBindPaymentAllocationBind.Priority = ThreadPriority.AboveNormal
            'ThreadBindPaymentAllocationBind.Start()
        End With
    End Sub

    Private Sub imbCancelProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelProcess.Click
        Response.Redirect("InstallRcvList_Collection.aspx")
    End Sub
#End Region

#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                If Not CheckCashier(Me.Loginid) Then

                    ShowMessage(lblMessage, "Kasir belum Buka", True)
                    Exit Sub
                End If

                'If ConvertDate2(oPaymentDetail.TglBayarTxt.Text) > Me.BusinessDate Then
                If ConvertDate2(oPaymentDetail.ValueDate) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari hari ini", True)
                Exit Sub
            End If

            With oPaymentInfo
                    '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                    .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                    .ApplicationID = Me.ApplicationID
                    .PaymentInfo()
                    'Dim ThreadPaymentInfo As New Thread(AddressOf .PaymentInfo)
                    'ThreadPaymentInfo.Priority = ThreadPriority.BelowNormal
                    'ThreadPaymentInfo.Start()
                    LastAR = .Prepaid + _
                            .MaximumInstallment + _
                            .MaximumLCInstallFee + _
                            .MaximumInstallCollFee + _
                            .MaximumInsurance + _
                            .MaximumLCInsuranceFee + _
                            .MaximumInsuranceCollFee + _
                            .MaximumPDCBounceFee + _
                            .MaximumSTNKRenewalFee + _
                            .MaximumInsuranceClaimFee + _
                            .MaximumReposessionFee
            End With
            If Me.ValidationData <> LastAR Then

                ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)
            ElseIf oPaymentAllocationDetail.InstallmentDue > 0 And oPaymentAllocationDetail.InstallmentDue < Me.AmountReceive Then
                ShowMessage(lblMessage, "Angsuran hanya boleh diisi dengan 0 atau " & FormatNumber(Me.AmountReceive, 0), True)
            Else

                With oPaymentAllocationDetail
                        TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee +
                                    .InsuranceDue + .LCInsurance + .InsuranceCollFee +
                                    .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee +
                                    .RepossessionFee + .Prepaid + .PLL

                    End With

                ' If Me.AmountReceive <> TotalAllocation Then
                'If oPaymentAllocationDetail.totalTagihan <> TotalAllocation Then


                '    ShowMessage(lblMessage, "Total Alokasi Pembayaran harus sama dengan Jumlah diterima", True)
                '    Exit Sub
                'End If

                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .AmountReceive = oPaymentAllocationDetail.InstallmentDue
                End With
                If oController.IsLastPayment(oCustomClass) Then
                    pnlConfirmation.Visible = True
                    pnlBtnGroupPaymentAllocation.Visible = False
                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = False
                    pnlPaymentAllocation.Visible = False
                    pnlPaymentInfo.Visible = False
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = False
                    Exit Sub
                End If
                With oCustomClass
                    .strConnection = GetConnectionString()
                        .NoKwitansi = oPaymentDetail.NoTTS  'oPaymentDetail.NoKwitansiTxt.SelectedValue
                        '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                        .ValueDate = ConvertDate2(oValueDate.Text)
                        .BranchId = Me.sesBranchId.Replace("'", "")
                    .CoyID = Me.SesCompanyID
                    .BusinessDate = Me.BusinessDate
                    .ReceivedFrom = oPaymentDetail.ReceivedFrom
                    .ReferenceNo = oPaymentDetail.ReferenceNo
                    .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
                    .LoginId = Me.Loginid
                    .BankAccountID = Me.BankAccount
                    .Notes = oPaymentDetail.Notes
                    .ApplicationID = Me.ApplicationID

                    '-----------Installment
                    .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                    .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                    .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                    .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                    .LcInstallment = oPaymentAllocationDetail.LCInstallment
                    .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                    .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                    .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                    '------------------------

                    '----------Insurance
                    .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                    .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                    .LcInsurance = oPaymentAllocationDetail.LCInsurance
                    .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                    .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                    .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                    '-----------------------------

                    .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                    .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                    .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                    .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                    .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                    .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                    .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                    .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                    .Prepaid = oPaymentAllocationDetail.Prepaid
                    .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                    .AmountReceive = TotalAllocation
                    .BayarDi = oPaymentDetail.BayarDi
                        .CollectorId = oPaymentDetail.CollectorID
                        .PLL = oPaymentAllocationDetail.PLL
                        .PLLDesc = oPaymentAllocationDetail.PLLDesc.Trim

                    End With
                Dim strError As String

                Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        Me.HistorySequenceNo = SavingController.InstallmentReceive()
                        'Dim thread1 As New Thread(AddressOf SavingController.InstallmentReceive)
                        'thread1.Priority = ThreadPriority.AboveNormal
                        'thread1.Start()
                        BindReport()

                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
        End If
        End If
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvList_Collection.aspx")
    End Sub
#End Region


    Sub BindReport()
        
        Dim oData As New DataSet
        Dim objReport As RptKwitansi2 = New RptKwitansi2
        Dim VoucherNo As String


        oController.PrintTTU(GetConnectionString, Me.ApplicationID, Me.BranchID, Me.BusinessDate, Me.Loginid)
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.HistorySeqNo = Me.HistorySequenceNo
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.strConnection = GetConnectionString()
        oData = oController.ReportKwitansiInstallment2(oCustomClass)

        objReport.SetDataSource(oData)

        AddParamField(objReport, "CashierName", Me.FullName)
        AddParamField(objReport, "BusinessDate", ConvertDate2(oValueDate.Text).ToString("dd") + " " + getMonthID() + " " + ConvertDate2(oValueDate.Text).ToString("yyyy"))
        AddParamField(objReport, "BranchName", oData.Tables(0).Rows(0).Item("BranchCity").ToString)

        VoucherNo = oData.Tables(1).Rows(0).Item("ReferenceNo").ToString

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        'strFileLocation = pathReport("Kwitansi", True) & Me.ApplicationID.Replace("/", "").Trim & ".pdf"
        strFileLocation = pathReport("Kwitansi", True) & VoucherNo.Trim & ".pdf"

        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        'Response.Redirect("installrcvlist.aspx?filekwitansi=" & pathReport("Kwitansi", False) & Me.ApplicationID.Replace("/", "").Trim)
        Response.Redirect("installrcvlist_Collection.aspx?filekwitansi=" & pathReport("Kwitansi", False) & VoucherNo.Trim)

    End Sub

    Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                If Not CheckCashier(Me.Loginid) Then

                    ShowMessage(lblMessage, "Kasir belum Buka", True)
                    Exit Sub
                End If
                With oPaymentInfo
                    '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                    .ValueDate = ConvertDate2(oValueDate.Text)
                    .ApplicationID = Me.ApplicationID
                    .PaymentInfo()
                    'Dim ThreadPaymentInfo As New Thread(AddressOf .PaymentInfo)
                    'ThreadPaymentInfo.Priority = ThreadPriority.BelowNormal
                    'ThreadPaymentInfo.Start()
                    LastAR = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
                End With
                If Me.ValidationData <> LastAR Then

                    ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
                Else
                    With oPaymentAllocationDetail
                        TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .LCInsurance + .InsuranceCollFee +
                                        .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee +
                                        .RepossessionFee + .Prepaid

                    End With
                    If Me.WayOfPayment = "CP" Then
                        If TotalAllocation > Me.PrepaidBalance Then

                            ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
                            Exit Sub
                        End If
                        If oPaymentAllocationDetail.Prepaid > 0 Then

                            ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
                            Exit Sub
                        End If
                    Else
                        If Me.AmountReceive <> TotalAllocation Then

                            ShowMessage(lblMessage, "Total Alokasi Pembayaran harus sama dengan Jumlah diterima", True)
                            Exit Sub
                        End If
                    End If
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text) 'ConvertDate2(oViewPaymentDetail.ValueDate)
                        .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .CoyID = Me.SesCompanyID
                        .BusinessDate = Me.BusinessDate
                        .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
                        .ReferenceNo = oViewPaymentDetail.ReferenceNo
                        .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
                        .LoginId = Me.Loginid
                        .BankAccountID = Me.BankAccount
                        .Notes = oViewPaymentDetail.Notes
                        .ApplicationID = Me.ApplicationID

                        '-----------Installment
                        .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                        .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim
                        .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                        .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                        .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                        .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                        .LcInstallment = oPaymentAllocationDetail.LCInstallment
                        .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                        .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                        .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                        '------------------------

                        '----------Insurance
                        .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                        .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                        .LcInsurance = oPaymentAllocationDetail.LCInsurance
                        .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                        .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                        .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                        '-----------------------------

                        .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                        .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                        .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                        .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                        .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                        .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                        .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                        .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                        .Prepaid = oPaymentAllocationDetail.Prepaid
                        .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                        .AmountReceive = TotalAllocation

                        .PLL = oPaymentAllocationDetail.PLL
                        .PLLDesc = oPaymentAllocationDetail.PLLDesc.Trim

                    End With
                    Dim strError As String

                    Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        SavingController.InstallmentReceive()
                        'Dim thread1 As New Thread(AddressOf SavingController.InstallmentReceive)
                        'thread1.Priority = ThreadPriority.AboveNormal
                        'thread1.Start()
                        BindReport()

                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub imbCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelLastPayment.Click
        Response.Redirect("InstallRcvList_Collection.aspx")
    End Sub

    Private Sub IsiKolomTagihan()
        With oPaymentAllocationDetail
            .InstallmentDue0 = Me.AmountReceive ' oPaymentInfo.InstallmentDue
            .LCInstallment0 = oPaymentInfo.MaximumLCInstallFee
            .InstallmentCollFee0 = oPaymentInfo.MaximumInstallCollFee
            .InsuranceDue0 = oPaymentInfo.MaximumInsurance
            .LCInsurance0 = oPaymentInfo.MaximumLCInsuranceFee
            .InsuranceCollFee0 = oPaymentInfo.MaximumInsuranceCollFee
            .PDCBounceFee0 = oPaymentInfo.MaximumPDCBounceFee
            .STNKRenewalFee0 = oPaymentInfo.MaximumSTNKRenewalFee
            .InsuranceClaimExpense0 = oPaymentInfo.MaximumInsuranceClaimFee
            .RepossessionFee0 = oPaymentInfo.MaximumReposessionFee
            .Prepaid0 = oPaymentInfo.Prepaid

            .TitipAngsuran0 = 0
            .TitipanAngsuran = 0

            .totalTagihan = .InstallmentDue0 + .LCInstallment0 + .InstallmentCollFee0 + .InsuranceDue0 +
                   .LCInsurance0 + .InsuranceCollFee0 + .PDCBounceFee0 + .STNKRenewalFee0 + .InsuranceClaimExpense0 +
                   .RepossessionFee0 + .Prepaid0

            .PrioritasPembayaran = Me.PrioritasPembayaran

            .InstallmentDue = Me.AmountReceive ' oPaymentInfo.InstallmentDue  
            .LCInstallment = 0 ' oPaymentInfo.MaximumLCInstallFee
            .InstallmentCollFee = 0 'oPaymentInfo.MaximumInstallCollFee
            .InsuranceDue = 0 'oPaymentInfo.MaximumInsurance
            .LCInsurance = 0 'oPaymentInfo.MaximumLCInsuranceFee
            .InsuranceCollFee = 0 'oPaymentInfo.MaximumInsuranceCollFee
            .PDCBounceFee = 0 'oPaymentInfo.MaximumPDCBounceFee
            .STNKRenewalFee = 0 'oPaymentInfo.MaximumSTNKRenewalFee
            .InsuranceClaimExpense = 0 'oPaymentInfo.MaximumInsuranceClaimFee
            .RepossessionFee = 0 'oPaymentInfo.MaximumReposessionFee
            .Prepaid = 0 'oPaymentInfo.Prepaid
            .PLL = 0

            .totalBayar = .InstallmentDue + .LCInstallment + .InstallmentCollFee + .InsuranceDue +
                   .LCInsurance + .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                   .RepossessionFee + .Prepaid

            .MaximumInstallment = Me.AmountReceive
            .PaymentAllocationBind()
        End With
    End Sub

    Private Sub BlokirBayarStep()
        If BlokirBayar Then
            imbSaveProcess.Visible = False
            ShowMessage(lblMessage, "Pembayaran ini diblokir", True)
        Else
            imbSaveProcess.Visible = True
        End If
    End Sub
    Public Shared Function Get_Combo_ReceiptByCollector(ByVal customClass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oReturnValue As New Parameter.InstallRcv
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CollectorId", SqlDbType.VarChar, 20) With {.Value = customClass.CollectorId})
        params.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = customClass.ApplicationID})


        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCbReceiptByCollector", params.ToArray()).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try

    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function BindKwitansiCollection(ByVal collid As String) As String
        Dim builder As New StringWriter()
        Dim dt As New DataTable

        Dim customClassInstallRcv As New Parameter.InstallRcv
        customClassInstallRcv.CollectorId = collid 'ddlCollectorID.SelectedValue
        customClassInstallRcv.ApplicationID = ModConnection.ApplicationID
        customClassInstallRcv.strConnection = ModConnection.StrConn

        dt = Get_Combo_ReceiptByCollector(customClassInstallRcv).ListData

        builder.WriteLine("[")
        If dt.Rows.Count > 0 Then
            'builder.WriteLine("{""optionDisplay"":""Select State"",")
            'builder.WriteLine("""optionValue"":""0""},")
            For i As Integer = 0 To dt.Rows.Count - 1
                builder.WriteLine("{""optionDisplay"":""" & Convert.ToString(dt.Rows(i)("Descrn")) & """,")
                builder.WriteLine("""optionValue"":""" & Convert.ToString(dt.Rows(i)("Code")) & """},")
            Next
        Else
            builder.WriteLine("{""optionDisplay"":""None"",")
            builder.WriteLine("""optionValue"":""""},")
        End If
        Dim returnjson As String = builder.ToString().Substring(0, builder.ToString().Length - 3)
        returnjson = returnjson & "]"
        Return returnjson.Replace(vbCr, "").Replace(vbLf, "")
    End Function

    Private Sub oValueDate_TextChanged(sender As Object, e As System.EventArgs) Handles oValueDate.TextChanged
        DoBind()
        LoadNextStep()
        BlokirBayarStep()
        oInstallmentSchedule.ApplicationId = Me.ApplicationID
        oInstallmentSchedule.ValueDate = ConvertDate2(oValueDate.Text)
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)
    End Sub
    Private Function getMonthID() As String
        Dim rtn As String = ""

        If Month(ConvertDate2(oValueDate.Text)) = "1" Then
            rtn = "Januari"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "2" Then
            rtn = "Febuari"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "3" Then
            rtn = "Maret"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "4" Then
            rtn = "April"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "5" Then
            rtn = "Mai"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "6" Then
            rtn = "Juni"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "7" Then
            rtn = "Juli"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "8" Then
            rtn = "Augustus"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "9" Then
            rtn = "September"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "10" Then
            rtn = "Oktober"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "11" Then
            rtn = "November"
        End If
        If Month(ConvertDate2(oValueDate.Text)) = "12" Then
            rtn = "Desember"
        End If

        Return rtn

    End Function

    Private Function pathReport(ByVal group As String, ByVal dirXML As Boolean) As String
        Dim strDirectory As String = ""
        If dirXML = True Then
            strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"

            strDirectory += Me.sesBranchId.Replace("'", "") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += group & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If




        Else
            strDirectory += Me.sesBranchId.Replace("'", "") & "/"
            strDirectory += group & "/"
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "/"


        End If
        Return strDirectory
    End Function
    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
End Class