﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web


Public Class ReprintKwitansi
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
    Private m_controller As New DataUserControlController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "REPRINTKWT"
            bindSearchBranch()
            If Request.QueryString("filekwitansi") <> "" Then
                Dim strFileLocation As String

                strFileLocation = "../../XML/" & Request.QueryString("filekwitansi") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                               & "var x = screen.width;" & vbCrLf _
                               & "var y = screen.height;" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If

            If Request.QueryString("applicationid") <> "" Then
                pnlsearch.Visible = False
                Me.SearchBy = "  MailTransaction.MailTypeID='KWT' and MailTransaction.BranchID='" & Request.QueryString("BranchID") & _
                "' and KwitansiInstallmentPrint.applicationid = '" & Request.QueryString("applicationid") & "' and KwitansiInstallmentPrint.HistorySequenceNo = " & Request.QueryString("HistorySequence") & ""

                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "  Agreement.BranchID='" & cboBranch.SelectedValue.Trim & "'"

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
        End If

        Me.SortBy = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        DtUserList = oController.ListKwitansiInstallment(oCustomClass).Tables(0)
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgKwitansi.DataSource = DvUserList

        Try
            dtgKwitansi.DataBind()
        Catch
            dtgKwitansi.CurrentPageIndex = 0
            dtgKwitansi.DataBind()
        End Try

        pnlDtGrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = " BranchID='" & cboBranch.SelectedValue.Trim & "'"
        Me.SortBy = ""

        DoBind(Me.SearchBy, Me.SortBy)
        txtSearchBy.Text = ""
        cboSearchBy.ClearSelection()
    End Sub



    Sub DownloadKwitansi(index As Integer)
        'Dim VoucherNo = CType(dtgKwitansi.Items(index).FindControl("lblRefNo"), Label).Text.Trim
        'Dim strHTTPServer As String
        'Dim StrHTTPApp As String
        'Dim strNameServer As String
        'Dim strFileLocation As String

        'strHTTPServer = Request.ServerVariables("PATH_INFO")
        'strNameServer = Request.ServerVariables("SERVER_NAME")
        'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & pathReport("Kwitansi", False) & VoucherNo & ".pdf"

        'Response.Write("<script language = javascript>" & vbCrLf _
        '& "var x = screen.width; " & vbCrLf _
        '& "var y = screen.height; " & vbCrLf _
        '& "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
        '& "</script>")

        Dim oData As New DataSet
        Dim objReport As RptKwitansi2 = New RptKwitansi2
        Dim VoucherNo As String = CType(dtgKwitansi.Items(index).FindControl("lblRefNo"), Label).Text.Trim
        Dim tglBayar As String = CType(dtgKwitansi.Items(index).FindControl("lbltglbayar"), Label).Text.Trim
        Dim UsrUpd As String = CType(dtgKwitansi.Items(index).FindControl("lblUsrUpd"), Label).Text.Trim
        Dim BranchID As String = CType(dtgKwitansi.Items(index).FindControl("lblBranchID"), Label).Text.Trim

        Dim tglByr As String = CType(dtgKwitansi.Items(index).FindControl("lbltglByr"), Label).Text.Trim
        Dim blnByr As String = CType(dtgKwitansi.Items(index).FindControl("lblblnByr"), Label).Text.Trim
        Dim thnByr As String = CType(dtgKwitansi.Items(index).FindControl("lblthnByr"), Label).Text.Trim

        Dim ApplicationID As String = CType(dtgKwitansi.Items(index).FindControl("lblApplicationID"), Label).Text.Trim
        Dim HistorySequenceNo As String = CType(dtgKwitansi.Items(index).FindControl("lblHistorySequenceNo"), Label).Text.Trim

        oCustomClass.ApplicationID = ApplicationID
        oCustomClass.HistorySeqNo = HistorySequenceNo
        oCustomClass.BranchId = BranchID
        oCustomClass.strConnection = GetConnectionString()
        oData = oController.ReportKwitansiInstallment2(oCustomClass)

        objReport.SetDataSource(oData)

        AddParamField(objReport, "CashierName", UsrUpd)
        AddParamField(objReport, "BusinessDate", tglByr + " " + getMonthID(blnByr) + " " + thnByr)
        AddParamField(objReport, "BranchName", BranchID)


        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        'strFileLocation = pathReport("Kwitansi", True) & Me.ApplicationID.Replace("/", "").Trim & ".pdf"
        strFileLocation = pathReport("Kwitansi", True) & VoucherNo.Trim & "_RePrint.pdf"

        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        'Response.Redirect("installrcvlist.aspx?filekwitansi=" & pathReport("Kwitansi", False) & Me.ApplicationID.Replace("/", "").Trim)
        Response.Redirect("ReprintKwitansi.aspx?filekwitansi=" & pathReport("Kwitansi", False) & VoucherNo.Trim & "_RePrint")

    End Sub
    Private Function getMonthID(ByVal tglBayar As String) As String
        Dim rtn As String = ""

        If tglBayar = "1" Then
            rtn = "Januari"
        End If
        If tglBayar = "2" Then
            rtn = "Febuari"
        End If
        If tglBayar = "3" Then
            rtn = "Maret"
        End If
        If tglBayar = "4" Then
            rtn = "April"
        End If
        If tglBayar = "5" Then
            rtn = "Mai"
        End If
        If tglBayar = "6" Then
            rtn = "Juni"
        End If
        If tglBayar = "7" Then
            rtn = "Juli"
        End If
        If tglBayar = "8" Then
            rtn = "Augustus"
        End If
        If tglBayar = "9" Then
            rtn = "September"
        End If
        If tglBayar = "10" Then
            rtn = "Oktober"
        End If
        If tglBayar = "11" Then
            rtn = "November"
        End If
        If tglBayar = "12" Then
            rtn = "Desember"
        End If

        Return rtn

    End Function
    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
    Private Function pathReport(ByVal group As String, ByVal dirXML As Boolean) As String
        Dim strDirectory As String = ""
        If dirXML = True Then
            strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"

            strDirectory += Me.sesBranchId.Replace("'", "") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += group & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If




        Else
            strDirectory += Me.sesBranchId.Replace("'", "") & "/"
            strDirectory += group & "/"
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "/"


        End If
        Return strDirectory
    End Function
    Private Sub dtgKwitansi_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgKwitansi.ItemCommand

        Select Case e.CommandName
            Case "Print"
                DownloadKwitansi(e.Item.ItemIndex)
        End Select
    End Sub
    Private Sub bindSearchBranch()
        With cboBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(1).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End If

        End With
    End Sub
End Class