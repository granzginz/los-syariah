﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region

Public Class InstallRCV_BankModalKerja
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    'Protected WithEvents txtValueDate As ucDateCE
    Protected WithEvents dendapaid As ucNumberFormat
    Protected WithEvents oInvoiceDate As ucDateCE
    Protected WithEvents oInvoiceDueDate As ucDateCE
    Protected WithEvents principalPaid As ucNumberFormat
    Protected WithEvents interestPaid As ucNumberFormat
    Protected WithEvents RetensiPaid As ucNumberFormat

    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String


#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property

    Private Property InsSeqNo() As String
        Get
            Return (CType(ViewState("InsSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InsSeqNo") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property

    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
    Private oModalKerjaInvoice As New Parameter.ModalKerjaInvoice
    Private m_controller As New ModalKerjaInvoiceController
    Private m_ControllerApp As New ApplicationController
    Private oControllerInsurance As New NewAppInsuranceByCompanyController
    'Private oControllerFacility As New CustomerFacilityController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            lblMessage.Visible = False
            Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId
            Me.InvoiceNo = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).InvoiceNo
            Me.FormID = "INSTALLRCV_BA_MDKJ"
            Me.Mode = "Normal"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                InitialDefaultPanel()
                txtValueDate.AutoPostBack = True
                txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind()

                If Not IsNothing(Session("BankSelected")) Then
                    oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
                    oPaymentDetail.cmbBankAccountEnabled = False
                End If
                If Not IsNothing(Session("tglValuta")) Then
                    txtValueDate.Text = Session("tglValuta")

                End If
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlBiaya.Visible = False
        pnlListInvoice.Visible = False
    End Sub

    Private Sub DoBind()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        Dim oclass As New Parameter.ModalKerjaInvoice

        With oclass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With

        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(txtValueDate.Text)
            .BranchId = Me.BranchID
        End With

        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        Dim oRow As DataRow = oData.Rows(0)
        Me.FacilityNo = oRow("NoFasilitas").ToString

        'If (oRow("ContractStatus").ToString = "PRP") Then
        If (oRow("ContractStatus").ToString = "AKT") Then
            oModalKerjaInvoice.strConnection = GetConnectionString()
            oModalKerjaInvoice.BranchId = Me.BranchID
            oModalKerjaInvoice.ApplicationID = Me.ApplicationID
            ''Modify by Wira 20180830
            'Add valuedate
            oModalKerjaInvoice.ValueDate = ConvertDate2(txtValueDate.Text)
            oModalKerjaInvoice = m_controller.GetJatuhTempoList(oModalKerjaInvoice)

            ItemData = oModalKerjaInvoice.Listdata
            dtgInvoice.DataSource = ItemData.DefaultView
            dtgInvoice.DataBind()
            pnlListInvoice.Visible = True
        Else
            fillPanelBiaya(oRow)
            pnlBiaya.Visible = True
        End If

        oclass = m_controller.GetDataByLastPaymentDate(oclass)
        With oclass
            lblInvoiceNo.Text = .InvoiceNo
            oInvoiceDate.Text = .InvoiceDate.ToString("dd/MM/yyyy")
            oInvoiceDueDate.Text = .InvoiceDueDate.ToString("dd/MM/yyyy")
            lblInvoiceAmount.Text = FormatNumber(.InvoiceAmount, 0)
            lblTotalPembiayaan.Text = FormatNumber(.OutstandingPrincipal, 0)
            lblInterestAmount.Text = FormatNumber(.InterestAmount, 0)
            lblRetensiAmount.Text = FormatNumber(.InvoiceAmount - .TotalPembiayaan, 0)
            lblInsseqno.Text = .InsSeqno
            oModalKerjaInvoice.RateDenda = .RateDenda
        End With
        oInvoiceDate.Enabled = False
        oInvoiceDueDate.Enabled = False

        oCustomClass = oController.GetPaymentInfo(oCustomClass)

        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            Me.NextInstallmentDate = .NextInstallmentDate
            Me.PrepaidBalance = .ContractPrepaidAmount
            Me.AmountToBePaid = .AmountToBePaid

            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "BA"
            oPaymentDetail.DoFormCashModule()

            If .ContractStatus <> "OSD" Then
                oPaymentDetail.AmountReceive = .InstallmentAmount
            Else
                oPaymentDetail.AmountReceive = .InstallmentDue
            End If

            oPaymentDetail.ShowTglBayar = False
            oPaymentDetail.ValueDate = txtValueDate.Text

            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub

    Private Sub fillPanelBiaya(oRow As DataRow)
        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
        Dim oCustomClass As New Parameter.CustomerFacility
        Dim oData, fData As New DataTable
        Dim oApp = New Parameter.ModalKerjaInvoice
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, FactoringDiscountCharges As Decimal
        Dim totalbiaya, asuransi, handlingfee, biayapolis, handlingfeeamount As Decimal

        oModalKerjaInvoice.strConnection = GetConnectionString()
        oModalKerjaInvoice.BranchId = Me.BranchID
        oModalKerjaInvoice.ApplicationID = Me.ApplicationID

        oModalKerjaInvoice = m_controller.GetInvoiceList(oModalKerjaInvoice)

        ItemData = oModalKerjaInvoice.Listdata

        Dim totalPembiayaan As Object = ItemData.Compute("SUM(TotalPembiayaan)", String.Empty)
        Dim totalInterest As Object = ItemData.Compute("SUM(InterestAmount)", String.Empty)
        Dim totalPlafond As Decimal = oCustomClass.FasilitasAmount

        NotaryFee = IIf(IsDBNull(oRow("NotaryFee")), 0, oRow("NotaryFee"))
        AdminFee = IIf(IsDBNull(oRow("AdminFee")), 0, oRow("AdminFee"))

        ProvisionFee = IIf(IsDBNull(oRow("ProvisionFee")), 0, oRow("ProvisionFee"))
        handlingfee = IIf(IsDBNull(oRow("HandlingFee")), 0, oRow("HandlingFee"))
        SurveyFee = IIf(IsDBNull(oRow("SurveyFee")), 0, oRow("SurveyFee"))
        FactoringDiscountCharges = IIf(IsDBNull(oRow("FactoringDiscountCharges")), 0, oRow("FactoringDiscountCharges"))

        lblBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
        handlingfeeamount = (handlingfee / 100) * totalPembiayaan
        lblBiayaHandling.Text = FormatNumber(handlingfeeamount, 0)

        With oNewAppInsuranceByCompany
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With
        oNewAppInsuranceByCompany = oControllerInsurance.GetInsuranceEntryFactoring(oNewAppInsuranceByCompany)

        asuransi = Convert.ToDecimal((oNewAppInsuranceByCompany.Rate / 100) * totalPembiayaan)
        lblAsuransiKredit.Text = FormatNumber(asuransi, 0)

        biayapolis = Convert.ToDecimal(oRow("BiayaPolis"))
        lblBiayaPolis.Text = FormatNumber(biayapolis, 0)

        lblBiayaAdmin.Text = FormatNumber(AdminFee, 0)
        lblBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
        lblBiayaNotaris.Text = FormatNumber(NotaryFee, 0)
        lblBiayaSurvey.Text = FormatNumber(SurveyFee, 0)
        lblDiscountCharges.Text = FormatNumber(FactoringDiscountCharges, 0)

        totalbiaya = NotaryFee + AdminFee + ProvisionFee + SurveyFee + asuransi + handlingfeeamount + biayapolis - FactoringDiscountCharges
        lblTotalBiaya.Text = FormatNumber(totalbiaya, 0)

    End Sub


#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then

            If ConvertDate2(txtValueDate.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari tanggal hari ini", True)
                Exit Sub
            End If

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .ValueDate = ConvertDate2(txtValueDate.Text)
                    .CoyID = Me.SesCompanyID
                    .BusinessDate = Me.BusinessDate
                    .PaidPrincipal = CDbl(principalPaid.Text)
                    .PaidInterest = CDbl(interestPaid.Text)
                    .PaidRetensi = CDbl(RetensiPaid.Text)
                    .DendaPaid = CDbl(DendaPaid.Text)
                    .FormID = Me.FormID
                    .Status = "CREATE"
                    .BankAccountID = oPaymentDetail.BankAccount
                    .ReceivedFrom = oPaymentDetail.ReceivedFrom
                    .ReferenceNo = NoRefrensi.Text
                    .WOP = oPaymentDetail.WayOfPayment
                    .LoginId = Me.Loginid
                    .InvoiceNo = Me.InvoiceNo
                End With

                Session.Add("BankSelected", oPaymentDetail.BankAccount)

                Try
                    If CDbl(principalPaid.Text) + CDbl(interestPaid.Text) + CDbl(RetensiPaid.Text) + CDbl(dendapaid.Text) = 0 Then
                        ShowMessage(lblMessage, "Tidak ada nilai yang akan di alokasi! cek kembali", True)
                        Exit Sub
                    End If
                    Dim SavingController As New InstallRcvController(oCustomClass)
                    SavingController.InstallRCVBAModalKerja()
                    Response.Redirect("installrcvlist_BankModalKerja.aspx")
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
        End If
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvList_BankModalKerja.aspx")
    End Sub
#End Region

    Private Sub txtValueDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtValueDate.TextChanged
        DoBind()
        If Not IsNothing(Session("BankSelected")) Then
            oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
            oPaymentDetail.cmbBankAccountEnabled = False
        End If

        'logic javascript

        Dim a As Double = CDbl(DateDiff(DateInterval.Day, ConvertDate2(oInvoiceDueDate.Text), ConvertDate2(txtValueDate.Text)))
        Dim b = CDbl(lblTotalPembiayaan.Text) + CDbl(lblInterestAmount.Text)
        lblDendaAmount.Text = CDbl((oModalKerjaInvoice.RateDenda / 1000) * b * a).ToString("#,##0")
    End Sub
End Class