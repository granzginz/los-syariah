﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvList_Collection_TempKoreksi.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRcvList_Collection_TempKoreksi" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRcvList_Collection</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PEMBAYARAN CUSTOMER KOREKSI (via COLLECTION) OTOR 
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Cari Berdasarkan Angsuran</label>
                <uc4:ucNumberFormat ID="txtAngsuran1" runat="server" CssClass="small_text" /> s/d 
                <uc4:ucNumberFormat ID="txtAngsuran2" runat="server" CssClass="small_text" />
            </div>
            
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>            
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                            AllowSorting="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.png"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOURCHER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblVoucherNo" runat="server" Text='<%#Container.DataItem("VoucherNo")%>'> </asp:HyperLink>
                                        <asp:Label ID="lblApplicationid" runat="server" Visible="False" Text='<%#Container.DataItem("Applicationid")%>'> </asp:Label>
                                        <asp:Label ID="lblInsSeqNo" runat="server" Visible="False" Text='<%#Container.DataItem("InsSeqNo")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="BANK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'> </asp:Label>
                                        <asp:Label ID="lblBankAccountID" runat="server" Visible="False" Text='<%#Container.DataItem("BankAccountID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="PostingDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL POST"
                                    SortExpression="PostingDate"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ValueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL VALUE"
                                    SortExpression="ValueDate"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Description" HeaderText="KETERANGAN"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReceivedFrom" HeaderText="CUSTOMER"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DebitAmount" DataFormatString="{0:N2}" HeaderText="JUMLAH">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="CashierName" HeaderText="USER"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
