﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region

Public Class InstallRCV_PPH
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oValueDate As ucDateCE
    Protected WithEvents oInvoiceDate As ucDateCE
    'Protected WithEvents oInvoiceDueDate As ucDateCE
    Protected WithEvents JumlahBayar As ucNumberFormat
    Protected WithEvents principalPaid As ucNumberFormat
    Protected WithEvents interestPaid As ucNumberFormat
    Protected WithEvents RetensiPaid As ucNumberFormat
    Protected WithEvents DendaPaid As ucNumberFormat
    'Protected WithEvents ucInvoiceAmount As ucNumberFormat
    'Protected WithEvents ucTotalPembiayaan As ucNumberFormat
    'Protected WithEvents ucInterestAmount As ucNumberFormat


    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String


#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property

    Private Property InsSeqNo() As String
        Get
            Return (CType(ViewState("InsSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InsSeqNo") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property

    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property

    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
    Private oFactoringInvoice As New Parameter.FactoringInvoice
    Private m_controller As New FactoringInvoiceController
    Private m_ControllerApp As New ApplicationController
    Private oControllerInsurance As New NewAppInsuranceByCompanyController
    'Private oControllerFacility As New CustomerFacilityController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId
            Me.InvoiceNo = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).InvoiceNo
            Me.InvoiceSeqNo = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).InvoiceSeqNo

            Me.FormID = "INSTALLRCV_BA_FACT"
            Me.Mode = "Normal"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                InitialDefaultPanel()
                oValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                oInvoiceDate.Text= Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind()

                If Not IsNothing(Session("BankSelected")) Then
                    oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
                    oPaymentDetail.cmbBankAccountEnabled = False
                End If
                If Not IsNothing(Session("tglValuta")) Then
                    oValueDate.Text = Session("tglValuta")

                End If
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlBiaya.Visible = False
        pnlListInvoice.Visible = False
    End Sub

    Private Sub DoBind()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(oValueDate.Text)
            .BranchId = Me.BranchID
        End With

        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        'oApplication = m_ControllerApp.GetApplicationEdit(oApplication)
        oApplication = m_ControllerApp.GetApplicationEditPPH(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        Dim oRow As DataRow = oData.Rows(0)
        'Me.FacilityNo = oRow("NoFasilitas").ToString

        If (oRow("ContractStatus").ToString = "AKT") Then
            oFactoringInvoice.strConnection = GetConnectionString()
            oFactoringInvoice.BranchId = Me.BranchID
            oFactoringInvoice.ApplicationID = Me.ApplicationID
            oFactoringInvoice.valuedate = ConvertDate2(oValueDate.Text)
            oFactoringInvoice.InvoiceSeqNo = Me.InvoiceSeqNo

            'oFactoringInvoice = m_controller.GetInvoiceList(oFactoringInvoice)
            'Modify by WIra 20180727
            'Untuk mendapatkan detail invoice
            'oFactoringInvoice = m_controller.GetInvoiceDetail2(oFactoringInvoice)
            oFactoringInvoice = m_controller.GetPPH(oFactoringInvoice)

            With oFactoringInvoice.Listdata
                lblNoPJJ.Text = .Rows(0).Item("PerjanjianNo").ToString
                'oInvoiceDate.Text = .Rows(0).Item("T").ToString
                'oInvoiceDueDate.Text = .Rows(0).Item("InvoiceDueDate").ToString
                lblPPHAmount.Text = FormatNumber(.Rows(0).Item("PPhAmount").ToString, 2)
                lblSisaPPHAmount.Text = FormatNumber(.Rows(0).Item("SisaPPHAmount").ToString, 2)
                'lblTotalPembiayaan.Text = FormatNumber(.Rows(0).Item("OutstandingPrincipal").ToString, 2)
                'lblInterestAmount.Text = FormatNumber(.Rows(0).Item("InterestAmount").ToString, 2)
                'lblRetensiAmount.Text = FormatNumber(.Rows(0).Item("InvoiceAmount").ToString - .Rows(0).Item("TotalPembiayaan").ToString, 2)
                'lblDendaAmount.Text = FormatNumber(.Rows(0).Item("AmountDenda").ToString, 2)

                'principalPaid.RangeValidatorEnable = True
                'principalPaid.RangeValidatorMinimumValue = "0"
                'principalPaid.RangeValidatorMaximumValue = .InvoiceAmount

                'interestPaid.RangeValidatorEnable = True
                'interestPaid.RangeValidatorMinimumValue = "0"
                'interestPaid.RangeValidatorMaximumValue = .InterestAmount

                'RetensiPaid.RangeValidatorEnable = True
                'RetensiPaid.RangeValidatorMinimumValue = "0"
                'RetensiPaid.RangeValidatorMaximumValue = .InvoiceAmount - .TotalPembiayaan

                'DendaPaid.RangeValidatorEnable = True
                'DendaPaid.RangeValidatorMinimumValue = "0"
                'DendaPaid.RangeValidatorMaximumValue = .AmountDenda.ToString
                'lbltotalTagihan.Text = FormatNumber(.Rows(0).Item("OutstandingPrincipal") + .Rows(0).Item("InterestAmount") + (.Rows(0).Item("InterestAmount") - .Rows(0).Item("TotalPembiayaan")) + .Rows(0).Item("AmountDenda"), 0)

                '*2019-02-014 total tagihan harusnya terdiri dari OutstandingPrincipal+InterestAmount+TotalPembiayaan+AmountDenda *'
                'lbltotalTagihan.Text = FormatNumber(.Rows(0).Item("OutstandingPrincipal") + .Rows(0).Item("InterestAmount") + .Rows(0).Item("AmountDenda"), 2)
            End With

            oInvoiceDate.Enabled = False
            'oInvoiceDueDate.Enabled = False

            'ItemData = oFactoringInvoice.Listdata 
            'dtgInvoice.DataSource = ItemData.DefaultView
            'dtgInvoice.DataBind()
            'pnlListInvoice.Visible = True
        Else
            fillPanelBiaya(oRow)
            pnlBiaya.Visible = True
        End If

        oCustomClass = oController.GetPaymentInfo(oCustomClass)

        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            Me.NextInstallmentDate = .NextInstallmentDate
            Me.PrepaidBalance = .ContractPrepaidAmount
            Me.AmountToBePaid = .AmountToBePaid

            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "BA"
            oPaymentDetail.DoFormCashModule()

            If .ContractStatus <> "OSD" Then
                oPaymentDetail.AmountReceive = .installmentamount
            Else
                oPaymentDetail.AmountReceive = .InstallmentDue
            End If

            oPaymentDetail.ShowTglBayar = False
            oPaymentDetail.ValueDate = oValueDate.Text

            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub

    Private Sub fillPanelBiaya(oRow As DataRow)
        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
        Dim oCustomClass As New Parameter.CustomerFacility
        Dim oData, fData As New DataTable
        Dim oApp = New Parameter.FactoringInvoice
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, FactoringDiscountCharges As Decimal
        Dim totalbiaya, asuransi, handlingfee, biayapolis, handlingfeeamount As Decimal

        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID

        oFactoringInvoice = m_controller.GetInvoiceList(oFactoringInvoice)

        ItemData = oFactoringInvoice.Listdata

        Dim totalPembiayaan As Object = ItemData.Compute("SUM(TotalPembiayaan)", String.Empty)
        Dim totalInterest As Object = ItemData.Compute("SUM(InterestAmount)", String.Empty)
        Dim totalPlafond As Decimal = oCustomClass.FasilitasAmount

        'NotaryFee = IIf(IsDBNull(oRow("NotaryFee")), 0, oRow("NotaryFee"))
        'AdminFee = IIf(IsDBNull(oRow("AdminFee")), 0, oRow("AdminFee"))

        'ProvisionFee = IIf(IsDBNull(oRow("ProvisionFee")), 0, oRow("ProvisionFee"))
        'handlingfee = IIf(IsDBNull(oRow("HandlingFee")), 0, oRow("HandlingFee"))
        'SurveyFee = IIf(IsDBNull(oRow("SurveyFee")), 0, oRow("SurveyFee"))
        'FactoringDiscountCharges = IIf(IsDBNull(oRow("FactoringDiscountCharges")), 0, oRow("FactoringDiscountCharges"))

        'lblBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
        'handlingfeeamount = (handlingfee / 100) * totalPembiayaan
        'lblBiayaHandling.Text = FormatNumber(handlingfeeamount, 0)

        With oNewAppInsuranceByCompany
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With
        oNewAppInsuranceByCompany = oControllerInsurance.GetInsuranceEntryFactoring(oNewAppInsuranceByCompany)

        'asuransi = Convert.ToDecimal((oNewAppInsuranceByCompany.Rate / 100) * totalPembiayaan)
        'lblAsuransiKredit.Text = FormatNumber(asuransi, 0)

        'biayapolis = Convert.ToDecimal(oRow("BiayaPolis"))
        'lblBiayaPolis.Text = FormatNumber(biayapolis, 0)

        'lblBiayaAdmin.Text = FormatNumber(AdminFee, 0)
        'lblBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
        'lblBiayaNotaris.Text = FormatNumber(NotaryFee, 0)
        'lblBiayaSurvey.Text = FormatNumber(SurveyFee, 0)
        'lblDiscountCharges.Text = FormatNumber(FactoringDiscountCharges, 0)

        'totalbiaya = NotaryFee + AdminFee + ProvisionFee + SurveyFee + asuransi + handlingfeeamount + biayapolis - FactoringDiscountCharges
        'lblTotalBiaya.Text = FormatNumber(totalbiaya, 0)

    End Sub


#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then

            If CDbl(JumlahBayar.Text.Replace(",", "")) > CDbl(lblPPHAmount.Text.Replace(",", "")) Then
                ShowMessage(lblMessage, "Masih ada kelebihan bayar", True)
                Exit Sub
            End If

            If ConvertDate2(oValueDate.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari tanggal hari ini", True)
                Exit Sub
            End If

            ' If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .ApplicationID = Me.ApplicationID
                .ValueDate = ConvertDate2(oValueDate.Text)
                .BusinessDate = ConvertDate2(oInvoiceDate.Text)
                .LoginId = Me.Loginid
                .ReferenceNo = NoRefrensi.Text
                .BankAccountID = oPaymentDetail.BankAccount
                .BayarPPh = CDbl(JumlahBayar.Text)
                .InsSeqNo = Me.InvoiceSeqNo

                '.InsSeqNo = Me.InvoiceSeqNo
            End With

            Session.Add("BankSelected", oPaymentDetail.BankAccount)

            Try
                Dim SavingController As New InstallRcvController(oCustomClass)
                'SavingController.InstallRCVBAFactoring()
                SavingController.InstallRCVPPH()
                Response.Redirect("installrcvpph.aspx")
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
            ' End If
        End If
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvPPH.aspx")
    End Sub
#End Region

    'Private Sub oValueDate_TextChanged(sender As Object, e As System.EventArgs) Handles oValueDate.TextChanged
    '    DoBind()
    '    If Not IsNothing(Session("BankSelected")) Then
    '        oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
    '        oPaymentDetail.cmbBankAccountEnabled = False
    '    End If
    '    jumlahBayar.Text = "0"
    '    principalPaid.Text = "0"
    '    interestPaid.Text = "0"
    '    RetensiPaid.Text = "0"
    '    DendaPaid.Text = "0"

    'End Sub
End Class