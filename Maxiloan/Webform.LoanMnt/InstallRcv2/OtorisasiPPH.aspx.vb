﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

#End Region

Public Class OtorisasiPPH
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtAmountFrom0 As ucNumberFormat
    Protected WithEvents txtamountto0 As ucNumberFormat
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendReversalController

#End Region

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property SuspendNo() As String
        Get
            Return (CType(ViewState("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SuspendNo") = Value
        End Set
    End Property
    Private Property PPHNo() As String
        Get
            Return (CType(ViewState("PPHNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PPHNo") = Value
        End Set
    End Property


#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_UPDATE_SUCCESS, False, True))
            End If
            Me.FormID = "OTOPPH"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                oBankAccount.BankPurpose = ""
                oBankAccount.BankType = ""
                oBankAccount.IsAll = True
                oBankAccount.BindBankAccount()



                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spPPHOtorPaging"
        objCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 2000).Value = cmdWhere
        objCommand.Parameters.Add("@SortBy", SqlDbType.VarChar, 100).Value = SortBy
        objread = objCommand.ExecuteReader()
        DtgAgree.DataSource = objread

        Try
            DtgAgree.DataBind()
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub




    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        'Dim Nlblsuspendno As HyperLink
        Dim NlblPPHNo As HyperLink
        Dim Nbranchid As Label

        If e.Item.ItemIndex >= 0 Then
            'Nlblsuspendno = CType(e.Item.FindControl("lblsuspendno"), HyperLink)
            NlblPPHNo = CType(e.Item.FindControl("lblPPHNo"), HyperLink)
            Nbranchid = CType(e.Item.FindControl("lblbranchid"), Label)

            'Nlblsuspendno.NavigateUrl = "javascript:OpenWinMain('" & Nlblsuspendno.Text.Trim & "','" & Nbranchid.Text.Trim & "')"
            NlblPPHNo.NavigateUrl = "javascript:OpenWinMain('" & NlblPPHNo.Text.Trim & "','" & Nbranchid.Text.Trim & "')"


        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Server.Transfer("OtorisasiPPH.aspx")
    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
        lblMessage.Visible = False
        Me.SearchBy = " and agr.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
        Me.SearchBy = Me.SearchBy & ""


        If oBankAccount.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " pphd.bankaccountid = '" & obankaccount.BankAccountID.Trim & "' "
        End If

        If txtTglJatuhTempo.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " AND pphd.postingdate <= '" & ConvertDate2(txtTglJatuhTempo.Text) & "'  "
        End If

        'If txtdesc.Text.Trim <> "" Then
        '    Me.SearchBy = Me.SearchBy & " AND st.description Like '%" & txtdesc.Text.Trim & "%'  "
        'End If

        If txtRefNo.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " AND pphd.ReferenceNo LIKE '%" & txtRefNo.Text.Trim & "%'"
        End If

        'If IsNumeric(txtAmountFrom0.Text.Trim) And IsNumeric(txtamountto0.Text.Trim) Then
        '    If CDbl(txtAmountFrom0.Text.Trim) >= 0 And CDbl(txtamountto0.Text.Trim) > 0 Then
        '        Me.SearchBy = Me.SearchBy & "AND (st.amount BETWEEN " & CDbl(txtAmountFrom0.Text.Trim) & " AND " & CDbl(txtamountto0.Text.Trim) & ")"
        '    End If
        'End If

        'If IsNumeric(txtAmountFrom.Text.Trim) And IsNumeric(txtamountto.Text.Trim) Then
        '    If CDbl(txtAmountFrom.Text.Trim) >= 0 And CDbl(txtamountto.Text.Trim) > 0 Then
        '        Me.SearchBy = Me.SearchBy & " OR ( st.branchid = '" & Me.sesBranchId.Replace("'", "") &
        '        "' AND st.suspendstatus = 'S' and  isnull(st.Otorisasi,'')= 'N' AND st.amount BETWEEN " & CDbl(txtAmountFrom.Text.Trim) & " AND " & CDbl(txtamountto.Text.Trim) & ")"
        '    End If
        'End If

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True

        DoBind(Me.SearchBy, Me.SortBy)
        'End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

        Try
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            'Dim oSuspendReceiveController As New SuspendReceiveController
            Dim oInstallRcvController As New InstallRcvController
            'Dim oEntities As New Parameter.SuspendReceive
            Dim oEntities As New Parameter.InstallRcv
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To DtgAgree.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(DtgAgree.Items(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    'Dim lblSuspendNo As HyperLink = CType(DtgAgree.Items(n).FindControl("lblSuspendNo"), HyperLink)
                    Dim lblPPHNo As HyperLink = CType(DtgAgree.Items(n).FindControl("lblPPHNo"), HyperLink)
                    'oEntities.SuspendNo = lblSuspendNo.Text.Trim
                    oEntities.PPHNo = lblPPHNo.Text.Trim
                    oEntities.LoginId = Me.Loginid
                    'oSuspendReceiveController.SaveSuspendOtorisasi(oEntities)
                    oInstallRcvController.SaveOtorisasiPPH(oEntities)
                    'Response.Redirect("OtorisasiPPH.aspx")
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di simpan", False)
            Response.Redirect("OtorisasiPPH.aspx")
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region
#Region "Reject"
    Private Sub BtnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReject.Click

        Try
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Dim oInstallRcvController As New InstallRcvController
            Dim oEntities As New Parameter.InstallRcv
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To DtgAgree.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(DtgAgree.Items(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim lblPPHNo As HyperLink = CType(DtgAgree.Items(n).FindControl("lblPPHNo"), HyperLink)
                    oEntities.PPHNo = lblPPHNo.Text.Trim
                    oEntities.LoginId = Me.Loginid
                    oInstallRcvController.RejectOtorisasiPPH(oEntities)
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di Reject", False)
            Response.Redirect("OtorisasiPPH.aspx")
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

End Class