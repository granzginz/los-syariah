﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class InstallRcvPPH
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCari As UcSearchBy
    Protected WithEvents oBranch1 As ucBranchAll
    Protected WithEvents txtAngsuran3 As ucNumberFormat
    Protected WithEvents txtAngsuran4 As ucNumberFormat


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 6
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
    Private oDataUserCtlr As New DataUserControlController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("filekwitansi") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"


                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If

            If Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage1, Request.QueryString("Message"), True)
            End If

            Me.FormID = "RCVPPH"
            'oCari.ListData = "InvoiceNo, No Faktur-Name, Nama-Agreementno, No. Kontrak-Address, Alamat"
            oCari.ListData = "PerjanjianNo, No Perjanjian-Name, Nama-Agreementno, No. Kontrak-Address, Alamat"
            oCari.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            With cmbBankAccount1
                .DataSource = oDataUserCtlr.GetBankAccount(GetConnectionString, Me.sesBranchId, "B", "EC")
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataBind()
                If Not IsNothing(Session("BankSelected")) Then .SelectedValue = Session("BankSelected").ToString.Trim
            End With

            If Not IsNothing(Session("tglValuta")) Then
                txtTglValuta1.Text = Session("tglValuta")
            Else
                txtTglValuta1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            End If
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With

        'oCustomClass = oController.AgreementListFactoring(oCustomClass)
        oCustomClass = oController.AgreementListRCVPPH(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        DtgAgree1.DataSource = oCustomClass.ListAgreement

        Try
            DtgAgree1.DataBind()
        Catch
            DtgAgree1.CurrentPageIndex = 0
            DtgAgree1.DataBind()
        End Try
        lblMessage1.Text = ""
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage1.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage1, "Data tidak ditemukan .....", True)
            lblTotPage1.Text = "1"
            rgvGo1.MaximumValue = "1"
        Else
            lblTotPage1.Text = CType(totalPages, String)
            rgvGo1.MaximumValue = CType(totalPages, String)
        End If
        lblrecord1.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage1.Enabled = False
            imbFirstPage1.Enabled = False
            If totalPages > 1 Then
                imbNextPage1.Enabled = True
                imbLastPage1.Enabled = True
            Else
                imbPrevPage1.Enabled = False
                imbNextPage1.Enabled = False
                imbLastPage1.Enabled = False
                imbFirstPage1.Enabled = False
            End If
        Else
            imbPrevPage1.Enabled = True
            imbFirstPage1.Enabled = True
            If currentPage = totalPages Then
                imbNextPage1.Enabled = False
                imbLastPage1.Enabled = False
            Else
                imbLastPage1.Enabled = True
                imbNextPage1.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage1.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage1.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage1.Text) - 1

        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb1.Click
        If IsNumeric(txtPage1.Text) Then
            If CType(lblTotPage1.Text, Integer) > 1 And CType(txtPage1.Text, Integer) <= CType(lblTotPage1.Text, Integer) Then
                currentPage = CType(txtPage1.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree1.ItemDataBound
        'Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            'hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            'hypReceive.NavigateUrl = "InstallRcv.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & oBranch1.BranchID.Trim

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree1.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset1.Click
        'oCari.ListData = "InvoiceNo, No Faktur-Name, Nama-Agreementno, No. Kontrak-Address, Alamat"
        oCari.ListData = "PerjanjianNo, No Perjanjian-Name, Nama-Agreementno, No. Kontrak-Address, Alamat"
        oCari.BindData()
        oCari.Text = ""
        txtAngsuran3.Text = "0"
        txtAngsuran4.Text = "0"
        Me.SortBy = ""
        Me.SearchBy = " branchid = '" & oBranch1.BranchID.Trim & "'"
        DoBind(Me.SortBy, Me.SearchBy)
        pnlDatagrid.Visible = False
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch1.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch1.BranchID.Trim & "'")

        If oCari.Text.Trim <> "" Then
            strSearch.Append(" and " & oCari.ValueID & " like  '%" & oCari.Text.Trim.Replace("'", "''") & "%'")
        End If
        If CDbl(IIf(txtAngsuran3.Text.Trim = "", "0", txtAngsuran3.Text.Trim)) > 0 Then
            If CDbl(IIf(txtAngsuran4.Text.Trim = "", "0", txtAngsuran4.Text.Trim)) > 0 Then
                'strSearch.Append(" and InstallmentAmount between '" & CDbl(txtAngsuran3.Text) & "' and '" & CDbl(txtAngsuran4.Text) & "' ")
                strSearch.Append(" and PPhAmount between '" & CDbl(txtAngsuran3.Text) & "' and '" & CDbl(txtAngsuran4.Text) & "' ")
            Else
                'strSearch.Append(" and InstallmentAmount = '" & CDbl(txtAngsuran3.Text) & "' ")
                strSearch.Append(" and PPhAmount = '" & CDbl(txtAngsuran3.Text) & "' ")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub


    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree1.ItemCommand
        Select Case e.CommandName
            Case "Receive"
                If ConvertDate2(txtTglValuta1.Text) > Me.BusinessDate Then
                    ShowMessage(lblMessage1, "Tanggal Valuta Tidak Boleh lebih dari tanggal " + Me.BusinessDate.ToString("dd/MM/yyyy"), True)
                    Exit Sub
                End If

                Dim lblApplicationid, lblInvoiceNo As HyperLink
                Dim lblInvoiceSeqNo As Label

                lblApplicationid = CType(DtgAgree1.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                'lblInvoiceNo = CType(DtgAgree1.Items(e.Item.ItemIndex).FindControl("lblInvoiceNo"), HyperLink)
                lblInvoiceSeqNo = CType(DtgAgree1.Items(e.Item.ItemIndex).FindControl("lblInvoiceSeqNo"), Label)

                oCustomClass.ApplicationID = lblApplicationid.Text
                oCustomClass.BranchId = oBranch1.BranchID.Trim
                'oCustomClass.InvoiceNo = lblInvoiceNo.Text
                oCustomClass.InvoiceSeqNo = lblInvoiceSeqNo.Text

                Session.Add("BankSelected", cmbBankAccount1.SelectedValue.Trim)
                Session.Add("tglValuta", txtTglValuta1.Text.Trim)
                HttpContext.Current.Items("Receive") = oCustomClass
                Server.Transfer("InstallRcv_PPH.aspx")
        End Select
    End Sub


End Class