﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvPPH.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRcvPPH" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRcvPPH</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
   
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage1" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PEMBAYARAN PENYELESAIN PERJANJIAN HUTANG (PPH)
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch1" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
                <label class="label_split">
                    Sumber Dokumen
                </label>
                <asp:DropDownList ID="cmbBankAccount1" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Cari Berdasarkan Angsuran</label>
                    <uc4:ucNumberFormat ID="txtAngsuran3" runat="server" CssClass="small_text" /> s/d 
                    <uc4:ucNumberFormat ID="txtAngsuran4" runat="server" CssClass="small_text" />
                </div>
                <div class="form_right">
                    <label>Tanggal Valuta</label>
                    <asp:TextBox runat="server" ID="txtTglValuta1" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceDate1" TargetControlID="txtTglValuta1" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RegularExpressionValidator ID="reTglValuta1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtTglValuta1"
    SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rftglvaluta" runat="server" ErrorMessage="Tgl Valuta harus diisi" ControlToValidate="txtTglValuta1" SetFocusOnError="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>              
            </div>
        </div>
        <div class="form_box_uc">
            <div>
                <uc1:ucsearchby id="oCari" runat="server"></uc1:ucsearchby>        
            </div>
        </div>

         
        <div class="form_button">
            <asp:Button ID="imgsearch1" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="imbReset1" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            <%--<uc2:ucIdentifikasiPemb id="oIdentifikasiPemb" runat="server"></uc2:ucIdentifikasiPemb>--%>
            <div style="display:none">
            <a href="javascript:OpenPemb();">
                <asp:Button ID="btnIdentPemb" runat="server" Text="Identifikasi Rekening Koran" CssClass="small button gray"
                    CausesValidation="False" UseSubmitBehavior="false"></asp:Button>
            </a>
            </div>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree1" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                            AllowSorting="True">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Receive"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO FAKTUR" HeaderStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>--%>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'> </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:BoundColumn DataField="NoPJJ" HeaderText="NO PJJ"></asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="PerjanjianNo" HeaderText="NO Perjanjian"></asp:BoundColumn>
                                <%--<asp:BoundColumn DataField="InvoiceDueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="JATUH TEMPO FAKTUR"></asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="PPhAmount" DataFormatString="{0:N0}" HeaderText="PPH AMOUNT"></asp:BoundColumn>
                                <asp:BoundColumn DataField="SisaPPhAmount" DataFormatString="{0:N0}" HeaderText="SISA PPH"></asp:BoundColumn>
                                <%--<asp:BoundColumn DataField="InvoiceSeqNo" DataFormatString="{0:N0}" HeaderText="SEQUENCE NO"></asp:BoundColumn>--%>
                                <%--<asp:BoundColumn DataField="InterestAmount" DataFormatString="{0:N0}" HeaderText="BUNGA BERJALAN"></asp:BoundColumn>--%>
                                <%--<asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>--%>
                                <asp:TemplateColumn SortExpression="InvoiceSeqNo" HeaderText="SEQ NO" HeaderStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceSeqNo" runat="server" Text='<%#Container.DataItem("InvoiceSeqNo")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage1" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage1" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage1" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage1" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage1" runat="server">1</asp:TextBox>
                            <asp:Button ID="imgbtnPageNumb1" runat="server" CssClass="small buttongo blue" Text="Go"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo1" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage1"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage1" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage1" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage1" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord1" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
