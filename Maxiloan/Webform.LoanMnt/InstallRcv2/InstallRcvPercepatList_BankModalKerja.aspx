﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvPercepatList_BankModalKerja.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRcvPercepatList_BankModalKerja" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRcvPercepatList_BankModalKerja</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
   
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PELUNASAN DIPERCEPAT CUSTOMER MODAL KERJA (via BANK) OTOR
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
                <label class="label_split">
                    Sumber Dokumen
                </label>
                <asp:DropDownList ID="cmbBankAccount" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Cari Berdasarkan Angsuran</label>
                    <uc4:ucNumberFormat ID="txtAngsuran1" runat="server" CssClass="small_text" /> s/d 
                    <uc4:ucNumberFormat ID="txtAngsuran2" runat="server" CssClass="small_text" />
                </div>
                <div class="form_right">
                    <label>Tanggal Valuta</label>
                    <asp:TextBox runat="server" ID="txtTglValuta" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtTglValuta" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RegularExpressionValidator ID="reTglValuta" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtTglValuta"
    SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rftglvaluta" runat="server" ErrorMessage="Tgl Valuta harus diisi" ControlToValidate="txtTglValuta" SetFocusOnError="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>              
            </div>
        </div>
        <div class="form_box_uc">
            <div>
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>        
            </div>
        </div>

         
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            <%--<uc2:ucIdentifikasiPemb id="oIdentifikasiPemb" runat="server"></uc2:ucIdentifikasiPemb>--%>
            <div style="display:none">
            <a href="javascript:OpenPemb();">
                <asp:Button ID="btnIdentPemb" runat="server" Text="Identifikasi Rekening Koran" CssClass="small button gray"
                    CausesValidation="False" UseSubmitBehavior="false"></asp:Button>
            </a>
            </div>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                            AllowSorting="True">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Receive"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO FAKTUR" HeaderStyle-Width="300">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNO")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'> </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="tglcair" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL CAIR"></asp:BoundColumn>
                                <asp:BoundColumn DataField="NextPaymentDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL BAYAR BERIKUT"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingPrincipal" DataFormatString="{0:N0}" HeaderText="OS POKOK"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmount" DataFormatString="{0:N0}" HeaderText="BUNGA"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Insseqno" DataFormatString="{0:N0}" HeaderText="KE"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
