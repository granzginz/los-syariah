﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentHistoryDetailModalKerja
	Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
	Private Property HistorySequenceNo() As String
		Get
			Return CType(ViewState("HistorySequenceNo"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("HistorySequenceNo") = Value
		End Set
	End Property

#End Region
#Region "Constanta"
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1
	Private totalDebitAmount As Double
	Private totalCreditAmount As Double
	Private oCustomClass As New Parameter.PaymentHistory
	Private oController As New PaymentHistoryController
#End Region

#Region "Page Load"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.FormID = "INSTALLRCVLIST"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				ImbExit.Attributes.Add("onclick", "javascript:fclose();")
				'imbBack.Attributes.Add("onclick", "return fback()")
				Me.AgreementNo = Request.QueryString("AgreementNo")
				Me.ApplicationID = Request.QueryString("ApplicationID")
				'Me.BranchID = Request.QueryString("BranchID")
				Me.HistorySequenceNo = Request.QueryString("HistorySequenceNo")
				If Request.QueryString("Message") <> "" Then

					ShowMessage(lblMessage, Request.QueryString("Message"), True)
				End If
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)
			End If
		End If
	End Sub
#End Region

#Region "Bind Data"
	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView
		Dim intloop As Integer
		Dim hypID As HyperLink

		With oCustomClass
			.ApplicationID = Me.ApplicationID
			'.BranchId = Me.BranchID
			.HistorySequenceNo = CInt(Me.HistorySequenceNo)
			.strConnection = GetConnectionString()
			.CurrentPage = currentPage
			.PageSize = pageSize
			.SortBy = SortBy
		End With

		oCustomClass = oController.ListPaymentHistoryDetailModalKerja(oCustomClass)
		With oCustomClass
			lblAgreementNo.Text = .Agreementno
			lblCustomerName.Text = .CustomerName
			lblTransactionType.Text = .TransactionType
			lblHistorySequenceNo.Text = Me.HistorySequenceNo
			lblPostingDate.Text = .BusinessDate.ToString("dd/MM/yyyy")
			lblValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
			lblReceiveFrom.Text = .ReceivedFrom
			lblReferenceNo.Text = .ReferenceNo
			lblStatus.Text = .Status
			lblStatusDate.Text = .StatusDate
			lblBankAccountID.Text = .BankAccountID
			lblWOP.Text = .PaymentTypeID
			lblReceiptNo.Text = .ReceiptNo
			lblPrintBy.Text = .PrintBy
			lblNumOfPrint.Text = CStr(.NumOfPrint)
			lblLastPrintDate.Text = .LastPrintDate
			DtUserList = .ListPaymentHistoryDetail
			lblBankName.Text = .BankName
            lblGiroNo.Text = .GiroNo
        End With

        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgPaymentHistory.DataSource = DvUserList
        Try
            dtgPaymentHistory.DataBind()
        Catch
            dtgPaymentHistory.CurrentPageIndex = 0
            dtgPaymentHistory.DataBind()
        End Try
    End Sub
#End Region
#Region "DataGrid Command"
    Private Sub dtgPaymentHistory_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaymentHistory.ItemDataBound
        Dim lblTotalDebitAmount As Label
        Dim lblTotalCreditAmount As Label
        Dim lblDebitAmount As Label
        Dim lblCreditAmount As Label
        If e.Item.ItemIndex >= 0 Then
            lblDebitAmount = CType(e.Item.FindControl("lblDebitAmount"), Label)
            lblCreditAmount = CType(e.Item.FindControl("lblCreditAmount"), Label)
            totalDebitAmount += CDbl(lblDebitAmount.Text)
            totalCreditAmount += CDbl(lblCreditAmount.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotalDebitAmount = CType(e.Item.FindControl("lblTotalDebitAmount"), Label)
            lblTotalDebitAmount.Text = FormatNumber(totalDebitAmount, 2)
            lblTotalCreditAmount = CType(e.Item.FindControl("lblTotalCreditAmount"), Label)
            lblTotalCreditAmount.Text = FormatNumber(totalCreditAmount, 2)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgPaymentHistory.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbBack.Click
        Response.Redirect("PaymentHistory.aspx?ApplicationID=" & Me.ApplicationID & "&AgreementNo=" & Me.AgreementNo & "&Style=" & Request.QueryString("style"))
    End Sub

End Class