﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class LCWaivedView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property WaiveNo() As String
        Get
            Return CType(viewstate("WaiveNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WaiveNo") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.FullPrepay
    Private oController As New LCWaivedController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.WaiveNo = ""
            Me.BranchID = ""
            Me.ApplicationID = ""

            If Request.QueryString("WaiveNo") <> "" Then Me.WaiveNo = Request.QueryString("WaiveNo")
            If Request.QueryString("BranchID") <> "" Then Me.BranchID = Request.QueryString("BranchID")
            If Request.QueryString("ApplicationID") <> "" Then Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.FormID = "WAIVETRANSACTIONVIEW"
            imbClose.Attributes.Add("onclick", "javascript:Close_Window()")
            'If Me.CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            With oCustomClass
                .WaiveNo = Me.WaiveNo
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
            End With
            oCustomClass = oController.ListLCWaived(oCustomClass)
            With oCustomClass
                lblCustomerName.Text = .CustomerName
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
                lblAgreementNo.Text = .Agreementno
                lblCustomerName.Text = .CustomerName
                lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID.Trim) & "')"

                lblLCInstall.Text = FormatNumber(.LcInstallmentWaived, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsuranceWaived, 2)
                lblInstallCollFee.Text = FormatNumber(.InstallmentCollFeeWaived, 2)
                lblInsuranceCollFee.Text = FormatNumber(.InsuranceCollFeeWaived, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFeeWaived, 2)
                lblRequest.Text = .UserApproval
                txtNotes.Text = .Notes
                lblReasonID.Text = .ReasonID
                txtValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            End With
            'End If
        End If
    End Sub


End Class