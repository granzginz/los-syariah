﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Amortization.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.Amortization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Amortization</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList2" runat="server" Visible="False">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    AMORTISASI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="lblCustomer" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreement" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Angsuran Jatuh Tempo
                </label>
                <asp:Label ID="lblOSInstallmentDue" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asuransi jatuh Tempo
                </label>
                <asp:Label ID="lblOSInsuranceDue" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Denda keterlambatan Angsuran
                </label>
                <asp:Label ID="lblLCInstallment" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan Asuransi
                </label>
                <asp:Label ID="lblLCInsurance" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Prepaid
                </label>
                <asp:Label ID="lblContractPrepaid" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total yg Harus dibayar
                </label>
                <asp:Label ID="lblTotAmountpaid" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgDisplay" runat="server" Text="Display" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAmor" runat="server" Visible="True" CssClass="grid_general"
                        DataKeyField="GiroNo" AutoGenerateColumns="False" OnSortCommand="SortGrid2" AllowSorting="True"
                        AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle Font-Bold="True" Height="25px" CssClass="tdjudul"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn SortExpression="Insseqno" HeaderText="NO">
                                <ItemTemplate>
                                    <asp:Label ID="lblInsseqnoA" runat="server" Text='<%#(Container.DataItem("Insseqno"))%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DUEDATE" SortExpression="DUEDATE" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="JUMLAH">
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmountA" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblITotInstallmentAmountA" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PAidAmount" HeaderText="JUMLAH DIBAYAR">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaidAmountA" runat="server" Text='<%#formatnumber(Container.DataItem("PAidAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPaidAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="WaivedAmount" HeaderText="JUMLAH DIBEBASKAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblWaivedAmountA" runat="server" Text='<%#formatnumber(Container.DataItem("WaivedAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotWaivedAmountA" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PaidDate" SortExpression="PaidDate" HeaderText="TGL BAYAR"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="lateCharges" HeaderText="DENDA">
                                <ItemTemplate>
                                    <asp:Label ID="lbllateChargesA" runat="server" Text='<%#formatnumber(Container.DataItem("lateCharges"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotlateChargesA" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CollectionFee" HeaderText="BIAYA TAGIH">
                                <ItemTemplate>
                                    <asp:Label ID="lblCollectionFeeA" runat="server" Text='<%#formatnumber(Container.DataItem("CollectionFee"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotCollectionFeeA" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CollectionFeeWaived" HeaderText="BIAYA TAGIH DIBEBASKAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblCollFeeWaivedA" runat="server" Text='<%#formatnumber(Container.DataItem("CollectionFeeWaived"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotCollFeeWaivedA" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="OSInstallment" HeaderText="SISA ANGSURAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblOSInstallmentA" runat="server" Text='<%#formatnumber(Container.DataItem("OSInstallment"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSInstallmentA" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ImagePrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="imgBack" runat="server" Text="Back" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="imbExit" runat="server" Text="Exit" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
