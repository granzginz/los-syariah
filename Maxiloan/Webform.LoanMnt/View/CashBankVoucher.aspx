﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CashBankVoucher.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.CashBankVoucher" %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CashBankVoucher</title>
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" /> 
    <link rel="Stylesheet" href="../../Include/default/easyui.css" type="text/css">
     <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.easyui.min.js"></script>
    <script type="text/javascript" language="JavaScript">
        function fClose() {
            window.close();
            return false;
        }			
	</script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_single">
    <div id="tt" class="easyui-tabs" border="false" style="height: 630px">
    <div title="View"> 
    <asp:HiddenField ID="hdBranchId" runat="server" />
    <asp:HiddenField ID="hdVoucherNo" runat="server" />
    <asp:HiddenField ID="hdtr_nomor" runat="server" /> 
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        <asp:Label ID="lblJudul" runat="server" EnableViewState="False"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        No Voucher
                    </label>
                    <asp:Label ID="lblVoucherNo" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Posting
                    </label>
                    <asp:Label ID="lblPostingDate" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Rekening Bank
                    </label>
                    <asp:Label ID="lblBankAccount" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Valuta
                    </label>
                    <asp:Label ID="lblValueDate" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Keterangan
                    </label>
                    <asp:Label ID="lblDescription" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        No Bukti Kas
                    </label>
                    <asp:Label ID="lblReferenceNo" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        No Bilyet Giro
                    </label>
                    <asp:Label ID="lblBGNo" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tgl jatuh Tempo BG
                    </label>
                    <asp:Label ID="lblBGDueDate" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Kasir
                    </label>
                    <asp:Label ID="lblCashier" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah
                    </label>
                    <asp:Label ID="lblAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        No Urut
                    </label>
                    <asp:Label ID="lblSequence" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        ID Proses
                    </label>
                    <asp:Label ID="lblProcessID" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Catatan
                    </label>
                    <asp:TextBox ID="txtNotes" runat="server" EnableViewState="False" Width="70%" 
                        TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                </div>
                <div class="form_right">
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        VOUCHER DETAIL
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" EnableViewState="False" CssClass="grid_general"
                            DataKeyField="DescriptionF" AutoGenerateColumns="False" AllowSorting="False"
                            ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle Height="25px" CssClass="tdjudul"></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn SortExpression="E.COA" HeaderText="COA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCOA" runat="server" EnableViewState="False" Text='<%# DataBinder.Eval(Container, "DataItem.COA") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="F.DescriptionF" HeaderText="ALOKASI PEMBAYARAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaymentAllocation" runat="server" EnableViewState="False" Text='<%# DataBinder.Eval(Container, "DataItem.DescriptionF") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="A.DescriptionA" HeaderText="KETERANGAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lnkDescription" runat="server" EnableViewState="False" Text='<%# DataBinder.Eval(Container, "DataItem.DescriptionA") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        TOTAL
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="E.DebitAmount" HeaderText="+/+ JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmountDebit" runat="server" EnableViewState="False" Text='<%# DataBinder.Eval(Container, "DataItem.DebitAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSumDebit" runat="server" EnableViewState="False"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="E.CreditAmount" HeaderText="-/- JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmountCredit" runat="server" EnableViewState="False" Text='<%# DataBinder.Eval(Container, "DataItem.CreditAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSumCredit" runat="server" EnableViewState="False"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
       

           <div class="form_button">            
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
            </asp:Button>     
        </div>   


        </div>
        <div title="Jurnal"> 
            <iframe id="IframeJournal" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
            width: 100%; height: 100%"></iframe>
        </div>
  </div>
</div>
      

    </form>
</body>
</html>
 <script type="text/javascript">
        $('#tt').tabs({
            border: false,
            onSelect: function (title) {
                var BranchId = $("#hdBranchId").val();
                var VoucherNo = $("#hdVoucherNo").val();
                var tr_nomor = $("#hdtr_nomor").val(); 
                if (title == "Jurnal") {
                    $('#IframeJournal').attr('src', '../../Webform.GL/JournalInquiryView.aspx?style=ACCMNT&tr_nomor=' + tr_nomor);
                }
     
              
            }
        });
    </script> 

