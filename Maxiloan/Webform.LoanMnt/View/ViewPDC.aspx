﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPDC.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ViewPDC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewPDC</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, 'PDC', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }	
			
		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:LinkButton ID="lblCustomerName" runat="server"></asp:LinkButton>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPDC" runat="server" CssClass="grid_general" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO PDC">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNoV" runat="server" Text='<%#  DataBinder.Eval(Container, "DataItem.PDCNo") %>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblPDCReceiptNo" runat="server" Visible="False" Text='<%#  DataBinder.Eval(Container, "DataItem.PDCReceiptNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblPDCStatus" runat="server" Visible="False" Text='<%#  DataBinder.Eval(Container, "DataItem.PDCstatus") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBranchID" runat="server" Visible="False" Text='<%#  DataBinder.Eval(Container, "DataItem.BranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BankName" HeaderText="BANK PDC"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TotalPDCamount" HeaderText="AMOUNT" DataFormatString="{0:###,###,###}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PDCduedate" HeaderText="TGL JT"></asp:BoundColumn>
                            <asp:BoundColumn DataField="bankaccountName" HeaderText="NAMA REKENING"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PDCstatus" HeaderText="STATUS"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="CETAK">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrint" runat="server" CausesValidation="False" ImageUrl="../../Images/IconPrinter.gif"
                                        CommandName="PRINT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <%--<div class="form_button">
            <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>--%>
    </div>
    </form>
</body>
</html>
