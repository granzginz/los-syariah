﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CashBankVoucher
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.CashBankVoucher
    Private oController As New CashBankVoucherController
    Dim TotalCredit, TotalDebit As Double
#End Region
#Region "Properti"
    Private Property VoucherNo() As String
        Get
            Return (CType(viewstate("VoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("VoucherNo") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CType(viewstate("Style"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
    Private Property tr_nomor() As String
        Get
            Return (CType(ViewState("tr_nomor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("tr_nomor") = Value
        End Set
    End Property
#End Region
#Region "Datagrid Command"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        GetTheStuff(Me.SortBy)
    End Sub

    Public Sub dtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmountCredit As New Label
        Dim lblAmountDebit As New Label
        Dim lblSumCredit As New Label
        Dim lblSumDebit As New Label

        If e.Item.ItemIndex >= 0 Then
            lblAmountDebit = CType(e.Item.FindControl("lblAmountDebit"), Label)
            lblAmountCredit = CType(e.Item.FindControl("lblAmountCredit"), Label)

            TotalDebit += CDbl(lblAmountDebit.Text)
            lblAmountDebit.Text = FormatNumber(CDbl(lblAmountDebit.Text), 2)

            TotalCredit += CDbl(lblAmountCredit.Text)
            lblAmountCredit.Text = FormatNumber(CDbl(lblAmountCredit.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSumDebit = CType(e.Item.FindControl("lblSumDebit"), Label)
            lblSumCredit = CType(e.Item.FindControl("lblSumCredit"), Label)

            lblSumDebit.Text = FormatNumber(TotalDebit.ToString, 2)
            lblSumCredit.Text = FormatNumber(TotalCredit.ToString, 2)
            'lblJudul.Text = TotalCredit.ToString
        End If
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'If Request.QueryString("VoucherNo") = "-" Then
            '    Response.Redirect("../../Finance/AccPay/BGMaintanance.aspx")
            'End If
            If Request.QueryString("BranchID") = "" Or Request.QueryString("VoucherNo") = "" Then
                With lblMessage
                    .Text = "Parameter Salah, harap Periksa Parameter"
                    .Visible = True
                End With
            Else
                Me.BranchID = Request.QueryString("BranchID")
                Me.VoucherNo = Request.QueryString("VoucherNo")
                Me.tr_nomor = Request.QueryString("tr_nomor")
                Me.Style = Request.QueryString("Style")
                lblJudul.Text = "VIEW - VOUCHER CASH BANK"
                GetTheStuff("")
            End If
            'Me.BranchID = Me.sesBranchId.Replace("'", "")
            'Me.VoucherNo = "90020"

        End If
        hdBranchId.Value = Me.BranchID
        hdVoucherNo.Value = Me.VoucherNo
        hdtr_nomor.Value = Me.tr_nomor
        lblMessage.Visible = False
    End Sub

    Private Sub GetTheStuff(ByVal SortBy As String)
        With oCustomClass
            .strConnection = GetConnectionString
            .VoucherNo = Me.VoucherNo
            .BranchId = Me.BranchID
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListCashBankVoucher(oCustomClass)
        Dim dtCashBank As DataTable
        dtCashBank = oCustomClass.ListCashBankVoucher
        If dtCashBank.Rows.Count > 0 Then
            lblVoucherNo.Text = Me.VoucherNo

            'A.VoucherNo, A.PostingDate, B.BankAccountName, A.ValueDate,
            'A.Description as descriptionA, A.ReferenceNo, C.EmployeeName, A.Amount,
            'A.BGNo, A.BGDueDate, A.OpeningSequence, D.Description, 
            'A.Notes, F.Description as descriptionF, E.Description as descriptionE, E.DebitAmount, E.CreditAmount, 
            '(case when A.ReceiveDisburseFlag='R' then 'Receive'
            '	when A.ReceiveDisburseFlag='D' then 'Disburse' end) as ReceiveDisburseFlag, 
            '(case when B.BankAccountType='C' then 'Cash'
            '	when B.BankAccountType='B' then 'Bank' end) as BankAccountType

            lblBankAccount.Text = CStr(dtCashBank.Rows(0)("BankAccountName"))
            lblDescription.Text = CStr(dtCashBank.Rows(0)("DescriptionA"))
            lblBGNo.Text = CStr(dtCashBank.Rows(0)("BGNo"))
            lblCashier.Text = CStr(dtCashBank.Rows(0)("EmployeeName"))
            lblSequence.Text = CStr(dtCashBank.Rows(0)("OpeningSequence"))
            txtNotes.Text = CStr(dtCashBank.Rows(0)("Notes"))
            lblPostingDate.Text = CStr(dtCashBank.Rows(0)("PostingDate"))
            lblValueDate.Text = CStr(dtCashBank.Rows(0)("ValueDate"))
            lblReferenceNo.Text = CStr(dtCashBank.Rows(0)("ReferenceNo"))
            lblBGDueDate.Text = CStr(dtCashBank.Rows(0)("BGDueDate"))
            If CStr(dtCashBank.Rows(0)("BGDueDate")).Trim = "1/1/1990" Then
                lblBGDueDate.Text = "-"
            End If

            'lblAmount.Text = CStr(dtCashBank.Rows(0)("Amount"))
            'FormatNumber(.SumInsured, 2, , , )
            lblAmount.Text = FormatNumber(dtCashBank.Rows(0)("Amount"), 2, , , )
            lblProcessID.Text = CStr(dtCashBank.Rows(0)("DescriptionD"))
            With dtgEntity
                .DataSource = dtCashBank
                .DataBind()
            End With

            Dim CashOrBank, ReOrDis As String
            CashOrBank = CStr(dtCashBank.Rows(0)("BankAccountType"))
            ReOrDis = CStr(dtCashBank.Rows(0)("ReceiveDisburseFlag"))
            lblJudul.Text = "VIEW " & CashOrBank.Trim & " " & ReOrDis & " VOUCHER"
        End If

    End Sub


End Class