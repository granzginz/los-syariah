﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LCWaivedView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.LCWaivedView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LCWaivedView</title>
     <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br/>
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False" ></asp:Label>
    <asp:Panel runat="server" EnableViewState="False" ID="pnlWaivedValueDate">

    <div class="form_title">  <div class="title_strip"> </div>
                <div class="form_single">
                     
                    <h4>
                         VIEW - TRANSAKSI PEMBEBASAN BIAYA/DENDA </h4>
                </div>
            </div>

      <div class="form_box">
				<div class="form_left">
				<label>    No Kontrak </label>
				<asp:HyperLink ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:HyperLink>
				</div>
				<div class="form_right">
				<label>  Nama Customer </label>
				 <asp:HyperLink ID="lblCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
				</div>
			</div>
        
        <div class="form_title">
                <div class="form_single">
                     
                    <h4>
                         DETAIL PENGAJUAN PEMBEBASAN BBIAYA/DENDA </h4>
                </div>
            </div>
        
        <div class="form_box">
				<div class="form_left">
				<label>  Tanggal Valuta </label>
				<asp:Label ID="txtValueDate" runat="server" EnableViewState="False"></asp:Label>
				</div>
				<div class="form_right">
				 
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label>  Denda Keterlmbatan Angsuran </label>
				<asp:Label ID="lblLCInstall" runat="server" EnableViewState="False"></asp:Label>
				</div>
				<div class="form_right">
				<label> Denda Keterlmabatan Asuransi </label>
				 <asp:Label ID="lblLCInsurance" runat="server" EnableViewState="False"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label>  Biaya Tagih Angsuran </label>
				 <asp:Label ID="lblInstallCollFee" runat="server" EnableViewState="False"></asp:Label>
				</div>
				<div class="form_right">
				<label>  Biaya Tagih Asuransi </label>
				<asp:Label ID="lblInsuranceCollFee" runat="server" EnableViewState="False"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label> Biaya Tolakan PDC </label>
				<asp:Label ID="lblPDCBounceFee" runat="server" EnableViewState="False" Width="100%"></asp:Label>
				</div>
				<div class="form_right">
				<label> <b>Total Pembebasan Biaya/Denda</b> </label>
				<b>
                        <asp:Label ID="lblTotalWaivedAmount" runat="server" EnableViewState="False" Width="100%"></asp:Label></b>
				</div>
			</div>
      
         <div class="form_box">
				<div class="form_single">
				 &nbsp;
				</div>
			</div>


            <div class="form_box">
				<div class="form_left">
				<label> Alasan </label>
				  <asp:Label ID="lblReasonID" runat="server" EnableViewState="False"></asp:Label>
				</div>
				<div class="form_right">
				<label>  DiSetujui Oleh </label>
				 <asp:Label ID="lblRequest" runat="server" EnableViewState="False"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label> Catatan </label>
				 <asp:TextBox ID="txtNotes" runat="server" EnableViewState="False" ReadOnly="True"
                        Width="100%" TextMode="MultiLine" ></asp:TextBox>
				</div>
				<div class="form_right">
				 
				</div>
			</div>
        

         <div class="form_button">
			  <asp:Button ID="imbClose" runat="server" EnableViewState="False" 
                        CausesValidation="False" Height="20" Text="Close" CssClass="small button red">
                    </asp:Button>
			 </div>
        
    </asp:Panel>
    </form>
</body>
</html>
