﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.General
Imports Maxiloan.cbse
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
#End Region

Public Class ViewPDC
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PDCReceiptNo() As String
        Get
            Return (CType(Viewstate("PDCReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    'Private oCustomClass As New Parameter.PDCReceive

#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "ViewPDC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.ApplicationID = Request("ApplicationID").ToString
                Me.CustomerName = Request("CustomerName").ToString
                Me.AgreementNo = Request("AgreementNo").ToString
                lblAgreementNo.Text = Request("AgreementNo").ToString
                lblCustomerName.Text = Me.CustomerName
                Me.Style = Request("Style").ToString
                Me.CustomerID = Request("CustomerID").ToString.Trim
                Bindgrid()

                'Dim lb As New LinkButton
                'lb = CType(Me.FindControl("lblCustomerName"), LinkButton)
                lblCustomerName.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
            End If
        End If
    End Sub
#End Region
#Region "BindGrid"
    Sub Bindgrid()
        'Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReaderGrid As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Connection = objConnection
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewPDC"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim

            objReaderGrid = objCommand.ExecuteReader
            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtgPDC.DataSource = objReaderGrid
            dtgPDC.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objReaderGrid.Close()
            objCommand.Dispose()

            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()

        End Try
    End Sub
#End Region

#Region "ItemDataBound"
    Private Sub dtgPDC_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPDC.ItemDataBound
        Dim hypGiroNo As HyperLink
        Dim flagfile As New Label
        Dim lblPDCReceiptNo As Label
        'If e.Item.ItemIndex >= 0 Then

        'End If

        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            hypGiroNo = CType(e.Item.FindControl("lblGiroNoV"), HyperLink)
            lblPDCReceiptNo = CType(e.Item.FindControl("lblPDCReceiptNo"), Label)

            ' hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & oCustomClass.BranchId & "&girono=" & hypGiroNo.Text.Trim & "&flagfile=" & flagfile.Text.Trim
            hypGiroNo.NavigateUrl = "../../Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?PDCReceiptNo=" & lblPDCReceiptNo.Text.Trim & "&branchid=" & Me.sesBranchId.Replace("'", "") & "&girono=" & hypGiroNo.Text.Trim & "&flagfile=1"
        End If

    End Sub
#End Region
    Private Sub dtgPDC_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPDC.ItemCommand
        Select Case e.CommandName
            'TODO Printing is disabled
            Case "PRINT"
                'Dim lblPDCReceiptNo As Label
                'Dim hypGiroNo As HyperLink
                'Dim lblPDCStatus As Label

                'Dim lblBranchID As Label
                'lblPDCReceiptNo = CType(dtgPDC.Items(e.Item.ItemIndex).FindControl("lblPDCReceiptNo"), Label)
                'hypGiroNo = CType(dtgPDC.Items(e.Item.ItemIndex).FindControl("lblGiroNoV"), HyperLink)
                'lblPDCStatus = CType(dtgPDC.Items(e.Item.ItemIndex).FindControl("lblPDCStatus"), Label)
                'lblBranchID = CType(dtgPDC.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)
                'Dim strFileLocation As String

                'If lblPDCStatus.Text <> "Bounce" Then
                '    Dim oData As New DataSet
                '    Dim objReport As RptPrintReceiptNo = New RptPrintReceiptNo
                '    Dim oCustomClass As New Parameter.PDCReceive
                '    Dim oController As New PDCReceiveController
                '    oData = oController.PrintReceiptNo(GetConnectionString, lblPDCReceiptNo.Text, lblBranchID.Text.Trim)
                '    objReport.SetDataSource(oData)
                '    '========================================================
                '    Dim discrete As ParameterDiscreteValue
                '    Dim ParamField As ParameterFieldDefinition
                '    Dim CurrentValue As ParameterValues
                '    ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
                '    discrete = New ParameterDiscreteValue
                '    discrete.Value = Me.BusinessDate
                '    CurrentValue = New ParameterValues
                '    CurrentValue = ParamField.DefaultValues
                '    CurrentValue.Add(discrete)
                '    ParamField.ApplyCurrentValues(CurrentValue)

                '    Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

                '    objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
                '    objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

                '    strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
                '    strFileLocation += Me.Session.SessionID & Me.Loginid & "ViewPDC.pdf"
                '    DiskOpts.DiskFileName = strFileLocation
                '    objReport.ExportOptions.DestinationOptions = DiskOpts
                '    objReport.Export()

                'Else
                '    Dim oCustomClass As New Parameter.PDCBounce
                '    Dim oController As New PDCBounceReconcileController
                '    Dim oData As New DataSet
                '    Dim objReport As RptPrintPDCBounceRecipt = New RptPrintPDCBounceRecipt
                '    With oCustomClass
                '        .strConnection = GetConnectionString()
                '        .BranchId = lblBranchID.Text.Trim
                '        .GiroNo = hypGiroNo.Text
                '        .PDCReceiptNo = lblPDCReceiptNo.Text
                '        .LoginId = Me.Loginid
                '        .BusinessDate = Me.BusinessDate
                '    End With
                '    oData = oController.PrintPDCBounce(oCustomClass)
                '    'oData.WriteXmlSchema("C:\InetPub\wwwroot\eStartunas\AccMnt\PDC\ReceiptPrint\PDCBounce.xmd")

                '    objReport.SetDataSource(oData)


                '    Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

                '    objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
                '    objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



                '    strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
                '    strFileLocation += Me.Session.SessionID & Me.Loginid & "ViewPDC.pdf"
                '    DiskOpts.DiskFileName = strFileLocation
                '    objReport.ExportOptions.DestinationOptions = DiskOpts
                '    objReport.Export()
                'End If
                'Dim strHTTPServer As String
                'Dim StrHTTPApp As String
                'Dim strNameServer As String
                'strHTTPServer = Request.ServerVariables("PATH_INFO")
                'strNameServer = Request.ServerVariables("SERVER_NAME")
                'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "ViewPDC.pdf"
                ''& "history.back(-1) " & vbCrLf _
                ''Call file PDF 
                'Response.Write("<script language = javascript>" & vbCrLf _
                '& "window.open('" & strFileLocation & "','ViewPDC', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                '& "</script>")
        End Select
    End Sub
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("../../Webform.LoanOrg/Credit/viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID)
    'End Sub
End Class