﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentHistoryDetailModalKerja.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentHistoryDetailModalKerja" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentHistoryDetailModalKerja</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                VIEW - HISTORY PEMBAYARAN MODAL KERJA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="lblCustomerName" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Kontrak
            </label>
            <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Transaksi
            </label>
            <asp:Label ID="lblTransactionType" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Urut History
            </label>
            <asp:Label ID="lblHistorySequenceNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Posting
            </label>
            <asp:Label ID="lblPostingDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Valuta
            </label>
            <asp:Label ID="lblValueDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Terima Dari
            </label>
            <asp:Label ID="lblReceiveFrom" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Bukti Kas
            </label>
            <asp:Label ID="lblReferenceNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cara Pembayaran
            </label>
            <asp:Label ID="lblWOP" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Rekening Bank
            </label>
            <asp:Label ID="lblBankAccountID" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Bank
            </label>
            <asp:Label ID="lblBankName" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Bilyet Giro
            </label>
            <asp:Label ID="lblGiroNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                STATUS
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kwitansi
            </label>
            <asp:Label ID="lblReceiptNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Cetak Ke
            </label>
            <asp:Label ID="lblNumOfPrint" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Di Cetak Oleh
            </label>
            <asp:Label ID="lblPrintBy" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Terakhir Cetak
            </label>
            <asp:Label ID="lblLastPrintDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Status
            </label>
            <asp:Label ID="lblStatus" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Status
            </label>
            <asp:Label ID="lblStatusDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                HISTORY PEMBAYARAN DETAIL
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaymentHistory" runat="server" EnableViewState="False" AllowSorting="True"
                    AutoGenerateColumns="False" OnSortCommand="SortGrid" CssClass="grid_general"
                    ShowFooter="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TRANSAKSI">
                            <ItemTemplate>
                                <asp:Label ID="lblTransaction" runat="server" EnableViewState="False" Text='<%#Container.DataItem("TransactionType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO URUT">
                            <ItemTemplate>
                                <asp:Label ID="lblSeqNo" runat="server" EnableViewState="False" Text='<%#Container.DataItem("InsSeqNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" EnableViewState="False" Text='<%#Container.DataItem("Description")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total per Halaman
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH DEBET" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right" FooterStyle-CssClass="item_grid_right">
                            <ItemTemplate>
                                <asp:Label ID="lblDebitAmount" runat="server" EnableViewState="False" Text='<%#FormatNumber(Container.DataItem("DebitAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalDebitAmount" runat="server" EnableViewState="False"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH KREDIT" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right" FooterStyle-CssClass="item_grid_right">
                            <ItemTemplate>
                                <asp:Label ID="lblCreditAmount" runat="server" EnableViewState="False" Text='<%#FormatNumber(Container.DataItem("CreditAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalCreditAmount" runat="server" EnableViewState="False"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="HARI TELAT" ></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="JUMLAH DENDA" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right" FooterStyle-CssClass="item_grid_right"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH POKOK" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right" FooterStyle-CssClass="item_grid_right">
                            <ItemTemplate>
                                <asp:Label ID="lblPrincipalAmount" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PrincipalAmount")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH BUNGA" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right" FooterStyle-CssClass="item_grid_right">
                            <ItemTemplate>
                                <asp:Label ID="lblInterestAmount" runat="server" EnableViewState="False" Text='<%#Container.DataItem("InterestAmount")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="imbBack" runat="server" EnableViewState="False" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
        <asp:Button ID="ImbExit" runat="server" EnableViewState="False" Text="Exit" CssClass="small button red"
            CausesValidation="False" Visible="false"></asp:Button>
    </div>
    </form>
</body>
</html>
