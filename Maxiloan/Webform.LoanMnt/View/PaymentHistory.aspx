﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentHistory.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentHistory</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/AccMnt.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                VIEW - HISTORY PEMBAYARAN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            <asp:Label ID="lblCustomerId" runat="server" Visible="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Kontrak
            </label>
            <asp:Label ID="lblAgreementNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:Repeater runat="server" ID="testRepeater">
                <HeaderTemplate>
                </HeaderTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaymentHistory" runat="server" AutoGenerateColumns="False" OnSortCommand="SortGrid"
                    CssClass="grid_general" ShowFooter="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid"></FooterStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblHistorySequenceNo" runat="server" Text='<%#Container.DataItem("HistorySequenceNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TRANSAKSI">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblTransactionType" runat="server" Text='<%#Container.DataItem("Description")%>'
                                    OnCommand="VIEW">
                                </asp:HyperLink>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total Amount
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL VALUTA">
                            <ItemTemplate>
                                <asp:Label ID="lblValueDate" runat="server" Text='<%#Container.DataItem("ValueDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL POSTING">
                            <ItemTemplate>
                                <asp:Label ID="lblPostingDate" runat="server" Text='<%#Container.DataItem("PostingDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CARA BAYAR">
                            <ItemTemplate>
                                <asp:Label ID="lblWayOfPayment" runat="server" Text='<%#Container.DataItem("PaymentTypeID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>  
                                <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("Amount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID BANK" ItemStyle-HorizontalAlign="Right" >
                            <ItemTemplate>
                                <asp:Label ID="lblBankID" runat="server" Text='<%#Container.DataItem("BankID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO BG" ItemStyle-HorizontalAlign="Right" >
                            <ItemTemplate>
                                <asp:Label ID="lblGiroNo" runat="server" Text='<%#Container.DataItem("GiroNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ANGSURAN" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("Installment"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalAmountInstallment" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TA'WIDH TELAT" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblLCAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("LCAmount"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalLCAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BIAYA TAGIH" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblCollFee" runat="server" Text='<%#FormatNumber(Container.DataItem("CollFee"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalCollFee" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KOREKSI">
                            <ItemTemplate>
                                <asp:Label ID="lblCorrectionHistSequence" runat="server" Text='<%#iif(Container.DataItem("CorrectionHistSequence")="0","",Container.DataItem("CorrectionHistSequence"))%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:TemplateColumn HeaderText="CETAK">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbPrint" runat="server" CausesValidation="False" Visible='<%# iif(Container.DataItem("CorrectionHistSequence") = "0" and (Container.DataItem("PaymentTypeID") <> "PDC" And Container.DataItem("PaymentTypeID") <> "Prepaid"), True, False) %>'
                                    ImageUrl="../../Images/IconPrinter.gif" CommandName="PRINT"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Saldo Prepaid
            </label>
            <asp:Label ID="lblPrepaidBalance" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Total diterima
            </label>
            <asp:Label ID="lblTotalAmountReceive" runat="server"></asp:Label>
        </div>
    </div>
   <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
