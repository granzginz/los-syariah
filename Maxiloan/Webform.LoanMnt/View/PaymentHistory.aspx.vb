﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentHistory
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private totalAmount As Double
    Private TotalAmountInstallment As Double
    Private TotalLCAmount As Double
    Private TotalCollFee As Double

    Private oCustomClass As New Parameter.PaymentHistory
    Private oController As New PaymentHistoryController
#End Region

#Region "Properties"
    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return (CType(Viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property Style() As String
        Get
            Return (CType(Viewstate("Style"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'Me.FormID = "INSTALLRCVLIST"
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

            'imbBack.Attributes.Add("onclick", "return fback()")
            If Request.QueryString("Message") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("Message"), True)
            End If
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.Style = Request.QueryString("Style")
            Me.BranchID = Me.Request.QueryString("BranchID")
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            'End If
        End If
    End Sub
#End Region

#Region "Bind Data"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .ApplicationID = Me.ApplicationID
            '.BranchId = Me.BranchID
            .strConnection = GetConnectionString
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListPaymentHistory(oCustomClass)
        With oCustomClass
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerId.Text = .CustomerID
            lblPrepaidBalance.Text = FormatNumber(.Prepaid, 0)
            lblTotalAmountReceive.Text = FormatNumber(.AmountReceive, 0)
        End With
        DtUserList = oCustomClass.ListPaymentHistoryHeader
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgPaymentHistory.DataSource = DvUserList

        Try
            dtgPaymentHistory.DataBind()
        Catch
            dtgPaymentHistory.CurrentPageIndex = 0
            dtgPaymentHistory.DataBind()
        End Try
        lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerId.Text & "')"

    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub dtgPaymentHistory_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaymentHistory.ItemDataBound
        Dim lblHistorySequenceNo As Label
        Dim lblTransactionType As HyperLink
        Dim lblTotalAmount As Label
        Dim lblAmount As Label
        Dim lblTotalAmountInstallment As Label
        Dim lblTotalLCAmount As Label
        Dim lblTotalCollFee As Label

        Dim lblInstallment As Label
        Dim lblLCAmount As Label
        Dim lblCollFee As Label

        If e.Item.ItemIndex >= 0 Then
            lblTransactionType = CType(e.Item.FindControl("lblTransactionType"), HyperLink)
            lblHistorySequenceNo = CType(e.Item.FindControl("lblHistorySequenceNo"), Label)

            lblInstallment = CType(e.Item.FindControl("lblInstallment"), Label)
            lblLCAmount = CType(e.Item.FindControl("lblLCAmount"), Label)
            lblCollFee = CType(e.Item.FindControl("lblCollFee"), Label)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            'lblTransactionType.NavigateUrl = "PaymentHistoryDetail.aspx?BranchID=" & Me.BranchID & "&ApplicationID=" & Me.ApplicationID & "&HistorySequenceNo=" & lblHistorySequenceNo.Text.Trim & "&style=" & Request.QueryString("style") & "&AgreementNo=" & Me.AgreementNo
            lblTransactionType.NavigateUrl = "PaymentHistoryDetail.aspx?ApplicationID=" & Me.ApplicationID & "&HistorySequenceNo=" & lblHistorySequenceNo.Text.Trim & "&style=" & Request.QueryString("style") & "&AgreementNo=" & Me.AgreementNo
            totalAmount += CDbl(lblAmount.Text)
            TotalAmountInstallment += CDbl(lblInstallment.Text)
            TotalLCAmount += CDbl(lblLCAmount.Text)
            TotalCollFee += CDbl(lblCollFee.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotalAmount = CType(e.Item.FindControl("lblTotalAmount"), Label)
            lblTotalAmount.Text = FormatNumber(totalAmount, 0)

            lblTotalAmountInstallment = CType(e.Item.FindControl("lblTotalAmountInstallment"), Label)
            lblTotalAmountInstallment.Text = FormatNumber(TotalAmountInstallment, 0)
            lblTotalLCAmount = CType(e.Item.FindControl("lblTotalLCAmount"), Label)
            lblTotalLCAmount.Text = FormatNumber(TotalLCAmount, 0)
            lblTotalCollFee = CType(e.Item.FindControl("lblTotalCollFee"), Label)
            lblTotalCollFee.Text = FormatNumber(TotalCollFee, 0)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgPaymentHistory.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region


    Private Sub dtgPaymentHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaymentHistory.ItemCommand
        Select Case e.CommandName
            'TODO Printing is disabled
            Case "PRINT"
                'Dim lblHistorySequenceNo As Label
                'lblHistorySequenceNo = CType(dtgPaymentHistory.Items(e.Item.ItemIndex).FindControl("lblHistorySequenceNo"), Label)

                'Dim oData As New DataSet
                'Dim oController As New InstallRcvController
                'Dim objReport As RptPrintKwitansi = New RptPrintKwitansi

                'oData = oController.PrintTTU(GetConnectionString, Me.ApplicationID, Me.BranchID, Me.BusinessDate, CInt(lblHistorySequenceNo.Text), Me.Loginid)
                'oData.Dispose()
                'Response.Redirect("../InstallRcv/ReprintKwitansi.aspx?Applicationid=" & Me.ApplicationID & "&HistorySequence=" & lblHistorySequenceNo.Text & "&BranchID=" & Me.BranchID & "&agreementNo='" & Me.AgreementNo & "'")
                ''objReport.SetDataSource(oData)

                ' ''========================================================
                ''Dim discrete As ParameterDiscreteValue
                ''Dim ParamField As ParameterFieldDefinition
                ''Dim CurrentValue As ParameterValues

                ''ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
                ''discrete = New ParameterDiscreteValue
                ''discrete.Value = Me.BusinessDate
                ''CurrentValue = New ParameterValues
                ''CurrentValue = ParamField.DefaultValues
                ''CurrentValue.Add(discrete)
                ''ParamField.ApplyCurrentValues(CurrentValue)

                ''Dim strHTTPServer As String
                ''Dim StrHTTPApp As String
                ''Dim strNameServer As String
                ''Dim strFileLocation As String

                ''Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

                ''objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
                ''objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

                ''strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
                ''strFileLocation += Me.Session.SessionID & Me.Loginid & "PaymentReceive.pdf"
                ''DiskOpts.DiskFileName = strFileLocation
                ''objReport.ExportOptions.DestinationOptions = DiskOpts
                ''objReport.Export()

                ''strHTTPServer = Request.ServerVariables("PATH_INFO")
                ''strNameServer = Request.ServerVariables("SERVER_NAME")
                ''StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                ''strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PaymentReceive.pdf"
                ''Response.Write("<script language = javascript>" & vbCrLf _
                ''& "window.open('" & strFileLocation & "','ViewPaymentHistory', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                ''& "</script>")

        End Select
    End Sub
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click        
    '    Response.Redirect("../../Webform.LoanOrg/Credit/viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID)
    'End Sub
End Class