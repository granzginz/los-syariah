﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class Amortization
    Inherits Maxiloan.Webform.WebBased

#Region "Property"


    Private Property Applicationid() As String
        Get
            Return (CType(Viewstate("Applicationid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Applicationid") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AccMntBase
    Private oCustomClass2 As New Parameter.AccMntBase
    Private oController As New AmortizationController
    Private oControllerH As New UCPaymentInfoController
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblApplicationid As New Label
            Dim lblnewStat As New Label

            lblApplicationid.Text = Request.QueryString("applicationid")
            Me.Applicationid = lblApplicationid.Text
            Me.BranchID = Request.QueryString("branchid")
            Me.FormID = "VIEWAMORTIZATION"

            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                DoBind()
                DoBindDetail(Me.SortBy)

            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        oCustomClass.ApplicationID = Me.Applicationid
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.BusinessDate = Me.BusinessDate
        oCustomClass = oControllerH.GetPaymentInfo(oCustomClass)
        oCustomClass2 = oController.ListAmortization(oCustomClass)
        With oCustomClass
            lblOSInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
            lblOSInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
            lblLCInstallment.Text = FormatNumber(.LcInstallment, 2)
            lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
            lblContractPrepaid.Text = FormatNumber(.Prepaid, 2)
            lblTotAmountpaid.Text = FormatNumber(.InstallmentDue + .InsuranceDue + .LcInstallment + .LcInsurance - .Prepaid, 2)
        End With

        With oCustomClass2
            lblCustomer.Text = .CustomerName
            lblAgreement.Text = .Agreementno
        End With
    End Sub

    Sub DoBindDetail(ByVal sortby As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        oCustomClass.ApplicationID = Me.Applicationid
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.BusinessDate = Me.BusinessDate
        oCustomClass = oController.ListAmortization(oCustomClass)

        DtUserList = oCustomClass.listdata
        DvUserList = DtUserList.DefaultView
        ' recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAmor.DataSource = DvUserList

        Try
            DtgAmor.DataBind()
        Catch
            'DtgPDC.CurrentPageIndex = 0
            DtgAmor.DataBind()
        End Try
    End Sub
#End Region

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBindDetail(Me.SortBy)
    End Sub
End Class