﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentRequestInquiryView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property RequestNo() As String
        Get
            Return (CType(Viewstate("RequestNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RequestNo") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As Parameter.PaymentRequest
    Private oController As PaymentRequestController
    Private m_TotalTransferAmount As Double
    Private m_TotalRequestAmount As Double
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PAYREQNOVIEW"

            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                m_TotalRequestAmount = 0
                m_TotalTransferAmount = 0

                lblMessage.Visible = False

                Me.BranchID = Request.QueryString("BranchId")
                Me.RequestNo = Request.QueryString("RequestNo")

                DoBind()
            End If
        End If
    End Sub

#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub

    Private Sub DoBind()
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            lblMessage.Text = ""
            With oCustomClass
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .RequestNo = Me.RequestNo
                .strConnection = GetConnectionString
            End With

            oCustomClass = oController.GetPaymentRequestHeaderAndDetail(oCustomClass)

            If oCustomClass Is Nothing Then
                DisplayError("No record found. Search conditions: " & Me.SearchBy)
                Exit Sub
            End If

            With oCustomClass
                '*** HEADER
                lblRequestNo.Text = .RequestNo
                lblBranchRequest.Text = .BranchName
                lblBankAccount.Text = .BankAccount
                lblNamaRek.Text = .NamaRekening
                lblNoRek.Text = .NoRekening
                lblDescription.Text = .Description
                lblDepartement.Text = .Departement
                lblDate.Text = .RequestDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.TotalAmount, 2)
                lblMemo.Text = .NoMemo
                '*** DETAIL
                FillGrid(.ListData, DtgAgree)

                '*** FOOTER
                lblTransferRefVoucherNo.Text = .TransferRefVoucherNo
                lblTransferRefNo.Text = .TransferReferenceNo
                lblTransferDate.Text = .TransferDate.ToString("dd/MM/yyyy")
                lblTransferAmount.Text = FormatNumber(.TransferAmount, 2)
                lblStatus.Text = .Status
                lblRequestBy.Text = .RequestBy
                lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")

                '*** DETAIL HISTORY REJECT
                If .Num <> 0 Then
                    panelHistory.Visible = True
                    FillGridHistory(.ListDataHistory)
                Else
                    panelHistory.Visible = False
                End If


                '*** DETAIL HISTORY APPROVE
                If .NumApprove <> 0 Then
                    panelHistory.Visible = True
                    FillGridHistoryApprove(.ListDataHistoryApproval)
                Else
                    panelHistoryApprove.Visible = False
                End If

            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region
    Sub FillGridHistory(ByRef oTable As DataTable)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgreeHistory.DataSource = DvUserList
            Try
                DtgAgreeHistory.DataBind()
            Catch
                DtgAgreeHistory.CurrentPageIndex = 0
                DtgAgreeHistory.DataBind()
            End Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.StackTrace, True)
        End Try
    End Sub

    Sub FillGridHistoryApprove(ByRef oTable As DataTable)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgreeHistoryApprove.DataSource = DvUserList
            Try
                DtgAgreeHistoryApprove.DataBind()
            Catch
                DtgAgreeHistoryApprove.CurrentPageIndex = 0
                DtgAgreeHistoryApprove.DataBind()
            End Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.StackTrace, True)
        End Try
    End Sub
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Public Sub New()
        oCustomClass = New Parameter.PaymentRequest
        oController = New Controller.PaymentRequestController
    End Sub

    Private Sub DtgAgree_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DtgAgree.SelectedIndexChanged

    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lbltemp As Label
        Dim lblTotal As Label

        If e.Item.ItemIndex >= 0 Then
            lbltemp = CType(e.Item.FindControl("lblTransferAmountDetail"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalTransferAmount += CType(lbltemp.Text, Double)
            End If
            lbltemp = CType(e.Item.FindControl("lblRequestAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalRequestAmount += CType(lbltemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotal = CType(e.Item.FindControl("lblTotalTransferAmount"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalTransferAmount.ToString, 2)
            End If
            lblTotal = CType(e.Item.FindControl("lblTotalRequestAmount"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalRequestAmount.ToString, 2)
            End If
        End If
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim cookie As HttpCookie = Request.Cookies("PaymentRequestInquiry")

        If Not cookie Is Nothing Then
            cookie.Values("RequestNo") = Me.RequestNo
            cookie.Values("BranchID") = Me.BranchID
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("PaymentRequestInquiry")
            cookieNew.Values.Add("RequestNo", Me.RequestNo)
            cookieNew.Values.Add("BranchID", Me.BranchID)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("rptPaymentRequestViewer.aspx?RedirectFiles=PaymentRequestMulti&RequestNo=" & lblRequestNo.Text & "&BranchID=" & Me.sesBranchId.Replace("'", "") & "")
        'Response.Redirect("rptPaymentRequestViewer.aspx")
    End Sub

End Class