﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExecutePaymentRequest.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ExecutePaymentRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountNoCondition" Src="../../Webform.UserController/ucBankAccountNoCondition.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ExecutePaymentRequest</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        EKSEKUSI PERMINTAAN PEMBAYARAN
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Rekening Bank</label>
                    <uc1:ucbankaccountnocondition id="oBankAccount" runat="server">
                </uc1:ucbankaccountnocondition>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <%--<label class="label_req">--%>
                    <label>
                        Tanggal Request</label>
                    <asp:TextBox ID="txtRequestDate" runat="server"></asp:TextBox>
                    <asp:CalendarExtender ID="txtRequestDate_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtRequestDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtRequestDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari Berdasarkan</label>
                    <uc1:ucsearchbywithnotable id="oSearchBy" runat="server">
                </uc1:ucsearchbywithnotable>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jenis Request</label>
                    <asp:DropDownList ID="cboRequestType" runat="server">
                        <asp:ListItem Value="0" Selected="True">Semua</asp:ListItem>
                        <asp:ListItem Value="INS">Asuransi</asp:ListItem>
                        <asp:ListItem Value="COL">Collection</asp:ListItem>
                        <asp:ListItem Value="PYR">Umum</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
            <asp:Panel ID="PnlGrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR EKSEKUSI PERMINTAAN PEMBAYARAN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgExecute" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="RequestNo" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                OnSortCommand="SortGrid">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbReconcile" ImageUrl="../../Images/iconedit.gif" runat="server"
                                                CommandName="Execute"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                        <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="DESCRIPTION" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblDescription" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="REQUEST">
                                        <ItemStyle HorizontalAlign="Right" Width="17%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.TotalAmount"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AmountTransfer" HeaderText="TRANSFER">
                                        <ItemStyle HorizontalAlign="Right" Width="17%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label22" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.AmountTransfer"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AmountUsed" HeaderText="TERPAKAI">
                                        <ItemStyle HorizontalAlign="Right" Width="17%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.AmountUsed"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestDate" HeaderText="TGL REQUEST">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RequestDate", "{0:dd/MM/yyyy}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="StatusDate" HeaderText="TGL TRANSFER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransferDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StatusDate", "{0:dd/MM/yyyy}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestType" HeaderText="JENIS">
                                        <ItemTemplate>
                                            <asp:Label ID="LblrequestType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RequestType") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="BankAccountID">
                                        <ItemTemplate>
                                            <asp:Label ID="LblBankAccountID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="BankAccountName">
                                        <ItemTemplate>
                                            <asp:Label ID="LblBankAccountName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountName") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="DepartementName">
                                        <ItemTemplate>
                                            <asp:Label ID="LblDepartementName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DepartementName") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonNext" runat="server" Text="Next" CssClass="small button blue"
                        Visible="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
