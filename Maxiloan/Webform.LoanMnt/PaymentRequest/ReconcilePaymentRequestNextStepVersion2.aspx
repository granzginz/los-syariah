﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReconcilePaymentRequestNextStepVersion2.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ReconcilePaymentRequestNextStepVersion2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReconcilePaymentRequestNextStepVersion2</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonReconcile.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;

        var submit = 0;
        function CheckDouble() {
            if (++submit > 1) {
                alert('This sometimes takes a few seconds - please be patient.');
                return false;
            }
        }

    </script>

    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                RECONCILE PERMINTAAN PEMBAYARAN
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Cabang Request</label>
                <asp:Label ID="LblBranchRequestID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Request</label>
                <asp:Label ID="LblRequestType" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Request</label>
                <asp:Label ID="LblRequestNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Request</label>
                <asp:Label ID="LblRequestDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Rekening Bank</label>
                <asp:Label ID="LblBankAccount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Transfer</label>
                <asp:Label ID="LblTransferDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                Keterangan</label>
                <asp:Label ID="LblDescription" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label">
                    No Bukti Kas Masuk *)</label>
                <asp:TextBox ID="txtReferenceNo" runat="server"  Width="136px"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL TRANSAKSI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgReconcile" runat="server" Width="100%" CssClass="grid_general"
                    BorderStyle="None" BorderWidth="0" DataKeyField="RequestNo" AutoGenerateColumns="False"
                    AllowSorting="True" HorizontalAlign="Center">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblSeqNO" runat="server" Text='<%#Container.DataItem("SequenceNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DEPARTEMENT">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDepartement" runat="server" Text='<%#Container.DataItem("DepartementName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TRANSAKSI">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblRequestNo" runat="server" Text='<%#Container.DataItem("TRANSACTIONDESC")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="REQUEST">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTTotalAmount" runat="server" Text='<%# FormatNumber(Container.DataItem("RequestAmount"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TRANSFER">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAmountTransfer" runat="server" Text='<%# FormatNumber(Container.DataItem("AmountTransfer"),2 )%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblTransferNotes" runat="server" Text='<%# Container.DataItem("TransferNotes") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonReconcile" runat="server" Text="Reconcile" CssClass="small button blue" OnClientClick="return CheckDouble();">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
