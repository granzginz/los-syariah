﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestView.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PaymentRequestView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PaymentRequestInquiryView</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        //        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        //        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        //        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function fClose() {
            window.close();
            return false;
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - PAYMENT REQUEST
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server" Width="100%">
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Cabang Request</label>
                    <asp:Label ID="lblBranchRequest" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Request</label>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Request</label>
                    <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Rekening Bank</label>
                    <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Request</label>
                    <asp:Label ID="lblAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Departemen</label>
                <asp:Label ID="lblDepartement" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("TransactionName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH REQUEST">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRequestAmount" runat="server" Text='<%#formatnumber(Container.DataItem("RequestAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalRequestAmount" runat="server">0.0</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TRANSFER">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransferAmountDetail" runat="server" Text='<%#formatnumber(Container.DataItem("TransferAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalTransferAmount" runat="server">0.0</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <HeaderStyle Width="25%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Note")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    INFORMASI TRANSFER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Voucher</label>
                    <asp:Label ID="lblTransferRefVoucherNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Transfer</label>
                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Bukti Kas</label>
                    <asp:Label ID="lblTransferRefNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Transfer</label>
                    <asp:Label ID="lblTransferAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Status</label>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Status</label>
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Di Ajukan Oleh</label>
                    <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <asp:Panel ID="pnlbutton" runat="server" Width="100%">
            <div class="form_button">
                <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false"
                    Text="Close" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
