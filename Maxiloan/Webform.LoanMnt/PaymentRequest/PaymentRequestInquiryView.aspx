﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestInquiryView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentRequestInquiryView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentRequestInquiryView</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
//        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
//        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
//        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function fClose() {
            window.close();
            return false;
        }				
    </script>
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.easyui.min.js"></script>
    <link rel="Stylesheet" href="../../Include/default/easyui.css" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div id="tt" class="easyui-tabs" border="false" style="height: 550px">
        <div title="View Permintaan Dana" style="padding: 10px">
    <div class="form_title">
        <div class="title_strip">
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server" Width="100%">
        <div class="form_box">
                <div class="form_left">
                    <label> Cabang Request</label>
                    <asp:Label ID="lblBranchRequest" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Departemen</label>
                    <asp:Label ID="lblDepartement" runat="server"></asp:Label>
                </div>
            </div>
        <div class="form_box">
                <div class="form_left">
                    <label> No Request</label>
                    <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Tanggal Request</label>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </div>
            </div>
        <div class="form_box">
                <div class="form_left">
                <label runat="server" id="lblBankAccountID">
                    Transfer Rekening</label>
                    <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Jumlah Request</label>
                    <asp:Label ID="lblAmount" runat="server"></asp:Label>
                </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                   Nama Rekening</label>
                <asp:Label ID="lblNamaRek" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label> Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label>
                   Nomor Rekening</label>
                <asp:Label ID="lblNoRek" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label> Memo</label>
                <asp:Label ID="lblMemo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("TransactionName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH REQUEST">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRequestAmount" runat="server" Text='<%#formatnumber(Container.DataItem("RequestAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalRequestAmount" runat="server">0.0</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TRANSFER">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransferAmountDetail" runat="server" Text='<%#formatnumber(Container.DataItem("TransferAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalTransferAmount" runat="server">0.0</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <HeaderStyle Width="25%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Note")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    INFORMASI TRANSFER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Voucher</label>
                    <asp:Label ID="lblTransferRefVoucherNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Transfer</label>
                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Bukti Kas</label>
                    <asp:Label ID="lblTransferRefNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Transfer</label>
                    <asp:Label ID="lblTransferAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Status</label>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Status</label>
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Di Ajukan Oleh</label>
                    <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
       <asp:panel id="panelHistory" runat="server">
        <div class="form_title">
                <div class="form_single">
                <h4> HISTORY REJECT TRANSAKSI </h4>
            </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgreeHistory" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblSeqNo" runat="server" Text='<%#Container.DataItem("SeqNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RejectNote")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TANGGAL">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTanggal"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tanggal") %>'  DataFormatString="{0:dd/MM/yyyy}"> </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="User">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        </div>
        </asp:panel>
        <asp:panel id="panelHistoryApprove" runat="server">
        <div class="form_title">
                <div class="form_single">
                <h4> HISTORY APPROVE TRANSAKSI </h4>
            </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgreeHistoryApprove" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblSeqNo" runat="server" Text='<%#Container.DataItem("Seq")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TANGGAL">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTanggal"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tanggal") %>'  DataFormatString="{0:dd/MM/yyyy}"> </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="User">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        </div>
        </asp:panel>
    </asp:Panel>
    
        </div>
       <div title="JOURNAL">
            <iframe id="IframeJournal" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                width: 100%; height: 100%"></iframe>
        </div>
    </div>

    <asp:Panel ID="pnlbutton" runat="server" Width="100%">
            <div class="form_button">
                <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false"
                    Text="Close" CssClass="small button gray"></asp:Button>
                <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    <script type="text/javascript">
        $('#tt').tabs({
            border: false,
            onSelect: function (title) {
                var _RequestNo = $("#lblRequestNo").html();
                var AppInfo = window.location.pathname;
                var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
                var ServerName = window.location.protocol + '//' + window.location.host + '/';
                if (title == "JOURNAL") {
                    $('#IframeJournal').attr('src', ServerName + App + '/Webform.GL/ViewJournalTransaction.aspx?TransactionType=PYR&TransactionNo=' + _RequestNo);
                }
            }
        });
    </script>
    </form>
</body>
</html>
