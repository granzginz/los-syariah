﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentRequest
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oTrans As ucLookUpTransaction
    Protected WithEvents oBankAcc As ucBankAccountNoCondition
    Protected WithEvents txtAmount As ucNumberFormat

#Region "Private Constanta"
    Private oController As New DataUserControlController
    Private m_controller As New PaymentRequestController
    Dim oCustomClass As New Parameter.PaymentRequest
    Dim tempTotalPRAmount As Double
    Private Const SCHEME_ID As String = "PYR"
#End Region

#Region "InitialDefaultValue"
    Sub InitialDefaultValue()
        oTrans.TransactionID = ""
        oTrans.Transaction = ""
        oTrans.Amount = 0
        oTrans.Description = ""
    End Sub
#End Region

#Region "Property"
    Private Property TotPRAmount() As Double
        Get
            Return (CType(Viewstate("TotPRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("TotPRAmount") = Value
        End Set
    End Property

    Private Property PRAmount() As Double
        Get
            Return (CType(Viewstate("PRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("PRAmount") = Value
        End Set
    End Property

    Private Property BankAccount() As String
        Get
            Return (CType(Viewstate("BankAccount"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccount") = Value
        End Set
    End Property

    Private Property Departement() As String
        Get
            Return (CType(Viewstate("Departement"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Departement") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return (CType(Viewstate("Description"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Description") = Value
        End Set
    End Property

    Private Property PRDataTable() As DataTable
        Get
            Return (CType(Viewstate("PRDataTable"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("PRDataTable") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        'Response.Write(" Page-Load ....Me.TotPRAmount = " + CStr(Me.TotPRAmount) + " ME.PRAmount = " + CStr(Me.PRAmount))

        Me.FormID = "PAYREQ"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
                GetCombo()

                oTrans.BindData()

                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.hpsxml = "1"
                oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)

                oBankAcc.Type = "B"
                oBankAcc.BindBankAccountNoCondition()
                pnlGrid.Visible = False
                Bind()

                txtAmount.RequiredFieldValidatorEnable = True
                txtAmount.RangeValidatorEnable = True
                txtAmount.TextCssClass = "numberAlign regular_text"
            End If
        End If
    End Sub
#End Region

#Region "Bind"
    Private Sub Bind()
        If Me.IsHoBranch Then
            With oTrans
                .ProcessID = "PAYREQ"
                .IsAgreement = "0"
                .IsPettyCash = "0"
                .IsHOTransaction = "2"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        Else
            With oTrans
                .ProcessID = "PAYREQ"
                .IsAgreement = "0"
                .IsPettyCash = "0"
                .IsHOTransaction = "0"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        End If
    End Sub
#End Region

#Region "GetCombo"
    Private Sub GetCombo()
        '------------------
        'CBO DEPARTEMEN
        '------------------
        Dim dtDepartement As New DataTable

        dtDepartement = oController.GetDepartement(GetConnectionString)

        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"

        cboDepartement.Items.Insert(1, "None")
        cboDepartement.Items(1).Value = "-"
    End Sub
#End Region

#Region "ButtonAdd1"
    Private Sub ButtonAdd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd1.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            lblMessage.Text = ""
            oBankAcc.Visible = False
            cboDepartement.Visible = False
            txtDescription.Visible = False
            txtAmount.Visible = False

            lblBankAccount.Visible = True
            lblDepartement.Visible = True
            lblDescription.Visible = True
            lblAmount.Visible = True
            lblBankAccount.Text = oBankAcc.BankAccountName
            lblDepartement.Text = cboDepartement.SelectedItem.Text
            lblDescription.Text = txtDescription.Text
            lblAmount.Text = FormatNumber(txtAmount.Text, 2)

            Me.BankAccount = lblBankAccount.Text
            Me.Departement = lblDepartement.Text
            Me.Description = lblDescription.Text
            Me.PRAmount = CDbl(lblAmount.Text.Trim)

            'Response.Write(" ButtonAdd1_Click....Me.TotPRAmount = " + CStr(Me.TotPRAmount) + " ME.PRAmount = " + CStr(Me.PRAmount))

            'If oTrans.TransactionID = "" Then
            '    lblMessage.Text = "Please Fill Transaction"
            '    Exit Sub
            'ElseIf oTrans.Description = "" Then
            '    lblMessage.Text = "Please Fill Description"
            '    Exit Sub
            'ElseIf oTrans.Amount <= 0 Then
            '    lblMessage.Text = "Transaction Amount Must be > 0 "
            '    Exit Sub
            'End If

            CallTransaction()
            pnlGrid.Visible = True
            InitialDefaultValue()
            ButtonAdd1.Visible = False
            ButtonAdd2.Visible = True
            ButtonCancel1.Visible = False

        End If
    End Sub
#End Region

#Region "CallTransaction"
    Sub CallTransaction()
        Dim oPayReq As New Parameter.PaymentRequest
        Dim pStrFile As String

        With oPayReq
            pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
            .strConnection = GetConnectionString
            .PaymentAllocationID = oTrans.TransactionID
            .PaymentAllDescription = oTrans.Transaction
            .Description = oTrans.Description
            .Amount = oTrans.Amount
        End With

        Dim i As Integer
        Dim lblTransactionID As New Label
        For i = 0 To DtgPRList.Items.Count - 1
            lblTransactionID = CType(DtgPRList.Items(i).FindControl("lblTransactionID"), Label)
            If oTrans.TransactionID = lblTransactionID.Text.Trim Then

                ShowMessage(lblMessage, "Transaksi Sudah Ada", True)
                pnlGrid.Visible = True
                PnlHeader.Visible = True
                InitialDefaultValue()
                Exit Sub
            End If
        Next
        oPayReq = m_controller.GetTableTransaction(oPayReq, pStrFile)

        DtgPRList.DataSource = oPayReq.ListData
        Me.PRDataTable = oPayReq.ListData
        DtgPRList.DataBind()
        DtgPRList.Visible = True
        pnlGrid.Visible = True

    End Sub
#End Region

#Region "ButtonAdd2"
    Private Sub ButtonAdd2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd2.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            CallTransaction()
            InitialDefaultValue()
            oTrans.BindData()
            'Response.Write(" ButtonAdd2_Click....Me.TotPRAmount = " + CStr(Me.TotPRAmount) + " ME.PRAmount = " + CStr(Me.PRAmount))
        End If
    End Sub
#End Region

#Region "DtgPRList_ItemDataBound"
    Private Sub DtgPRList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPRList.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim lblTotPRAmount As New Label
        Dim lblPRAmount As New Label
        With oCustomClass
            If e.Item.ItemIndex >= 0 Then
                lblPRAmount = CType(e.Item.FindControl("lblPRAmount"), Label)
                tempTotalPRAmount += CDbl(lblPRAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then
                lblTotPRAmount = CType(e.Item.FindControl("lblTotPRAmount"), Label)
                lblTotPRAmount.Text = FormatNumber(tempTotalPRAmount.ToString, 2)
                Me.TotPRAmount = CDbl(lblTotPRAmount.Text.Trim)
            End If

        End With
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

#Region "DtgPRList_ItemCommand"
    Private Sub DtgPRList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPRList.ItemCommand
        Dim pstrFile As String
        If e.CommandName = "DELETE" Then

            'Response.Write(" DtgPRList_ItemCommand ....e.CommandName = DELETE  ...Me.TotPRAmount = " + CStr(Me.TotPRAmount) + " ME.PRAmount = " + CStr(Me.PRAmount))

            With oCustomClass
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = Me.Session.SessionID + Me.Loginid
                .BranchId = Me.BranchID
                .flagdelete = "1"
                .IndexDelete = CStr(e.Item.ItemIndex())
                .hpsxml = "2"
                'Response.Write(" IndexDelete = " + .IndexDelete)
            End With
            oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)
            DtgPRList.DataSource = oCustomClass.ListData
            Me.PRDataTable = oCustomClass.ListData ' ini ditambahkan sejak kamis 24 juni jam 17:03
            DtgPRList.DataBind()
            'Response.Write(" DtgPRList_ItemCommand .... DtgPRList.DataBind() = DELETE  ...Me.TotPRAmount = " + CStr(Me.TotPRAmount) + " ME.PRAmount = " + CStr(Me.PRAmount))
            DtgPRList.Visible = True
            pnlGrid.Visible = True
        End If
    End Sub
#End Region

#Region "imbCancel"
    'button cancel berikut di comment karena berikutnya adalah screen approval
    'Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbCancel.Click
    '    Dim pstrFile As String
    '    pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
    '    oCustomClass.hpsxml = "1"
    '    oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)

    '    pnlGrid.Visible = False
    '    PnlHeader.Visible = True
    '    cboDepartement.Visible = True
    '    cboDepartement.SelectedIndex = 0
    '    oBankAcc.BindBankAccountNoCondition()
    '    oBankAcc.Visible = True
    '    txtDescription.Visible = True
    '    txtAmount.Visible = True
    '    txtAmount.Text = "0"
    '    txtDescription.Text = ""

    '    lblBankAccount.Visible = False
    '    lblDepartement.Visible = False
    '    lblDescription.Visible = False
    '    lblAmount.Visible = False
    '    lblMessage.Text = ""
    'End Sub

    Private Sub ButtonCancel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel1.Click
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.hpsxml = "1"
        oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)

        pnlGrid.Visible = False
        PnlHeader.Visible = True
        cboDepartement.Visible = True
        cboDepartement.SelectedIndex = 0
        oBankAcc.Type = "B"
        oBankAcc.BindBankAccountNoCondition()
        oBankAcc.Visible = True
        txtDescription.Visible = True
        txtAmount.Visible = True
        txtAmount.Text = "0"
        txtDescription.Text = ""

        lblBankAccount.Visible = False
        lblDepartement.Visible = False
        lblDescription.Visible = False
        lblAmount.Visible = False
        lblMessage.Text = ""
        InitialDefaultValue()
        oTrans.BindData()
    End Sub
#End Region

    
    Private Sub imgSaveApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveApproval.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If DtgPRList.Items.Count > 0 Then
                Dim pStrFile As String
                If Me.TotPRAmount <> Me.PRAmount Then

                    ShowMessage(lblMessage, "Total Nilai harus sama dengan Total Permintaan pembayaran", True)
                    pnlGrid.Visible = True
                    PnlHeader.Visible = True
                    Exit Sub
                ElseIf Me.TotPRAmount = Me.PRAmount Then
                    Dim strRequestNo As String
                    oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                    oCustomClass.BusinessDate = Me.BusinessDate
                    oCustomClass.ID = "PAYREQ"
                    oCustomClass.strConnection = GetConnectionString()
                    strRequestNo = m_controller.Get_RequestNo(oCustomClass)

                    With oCustomClass
                        .LoginId = Me.Loginid
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .RequestNo = strRequestNo
                        .BusinessDate = Me.BusinessDate
                        .strConnection = GetConnectionString()
                        .BankAccount = oBankAcc.BankAccountID
                        .Description = Me.Description
                        .Amount = Me.PRAmount
                        .PRDataTable = Me.PRDataTable
                        .NumPR = DtgPRList.Items.Count
                        .Departement = cboDepartement.SelectedValue
                    End With
                    
                    Dim ErrMsg As String
                    ErrMsg = m_controller.SavePR(oCustomClass)
                    If ErrMsg = "" Then

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                        pnlGrid.Visible = False
                        PnlHeader.Visible = True
                        lblDepartement.Visible = False
                        cboDepartement.Visible = True
                        cboDepartement.SelectedIndex = 0
                        lblBankAccount.Visible = False
                        oBankAcc.Visible = True
                        oBankAcc.Type = "B"
                        oBankAcc.BindBankAccountNoCondition()
                        txtDescription.Visible = True
                        txtDescription.Text = ""
                        lblDescription.Visible = False
                        txtAmount.Visible = True
                        txtAmount.Text = "0"
                        lblAmount.Visible = False
                    Else

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                        pnlGrid.Visible = True
                        PnlHeader.Visible = True
                        Exit Sub
                    End If

                    pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                    oCustomClass.hpsxml = "1"
                    oCustomClass = m_controller.GetTableTransaction(oCustomClass, pStrFile)

                    ButtonAdd2.Visible = False
                    ButtonAdd1.Visible = True
                    ButtonCancel1.Visible = True
                End If
            Else

                ShowMessage(lblMessage, "Tidak ada Data", True)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub imbNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNext.Click

        
        PnlHeader.Visible = False
        pnlGrid.Visible = False

    End Sub

End Class