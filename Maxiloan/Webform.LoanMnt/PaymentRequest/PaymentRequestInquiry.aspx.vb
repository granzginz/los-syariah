﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PaymentRequestInquiry
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents osearchby As UcSearchBy
    'Protected WithEvents oBranch As UcBranch
    Protected WithEvents hyNo As System.Web.UI.WebControls.HyperLink    
#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property

#End Region


#Region "Constanta"
    Private Const STR_FORM_ID As String = "PAYREQINQ"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.PaymentRequest
    Private oController As New Controller.PaymentRequestController
    Private m_TotalAmount As Double
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub

        End If

        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")


            End If
            If Request.QueryString("message") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = STR_FORM_ID
            Dim dtbranch As New DataTable
            dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cboParent
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "ALL")
                    .Items(0).Value = "ALL"
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "ALL")
                    .Items(0).Value = "ALL"

                    cboParent.SelectedValue = Me.sesBranchId.Replace("'", "")
                End If
            End With
            txtDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            oCustomClass.ListData = oController.GetPaymentRequestPaging(oCustomClass).ListData

            If oCustomClass Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True



        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub



#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        'oBranch.DataBind()
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        cboStatus.SelectedIndex = 0
        'Server.Transfer("PaymentRequestInquiry.aspx")

        pnlDatagrid.Visible = False
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String = ""
        txtPage.Text = "1"
        Me.SearchBy = "1=1"

        If (cboParent.SelectedItem.Value <> "ALL") Then
            Me.SearchBy = Me.SearchBy & "AND  PR.branchid = " & "'" & cboParent.SelectedItem.Value.Trim & "'"
        End If

        If cboStatus.SelectedIndex > 0 Then
            If cboStatus.SelectedValue.ToUpper = "REQ" Or cboStatus.SelectedValue.ToUpper = "TRF" Or cboStatus.SelectedValue.ToUpper = "CAN" Or cboStatus.SelectedValue.ToUpper = "APR" Or cboStatus.SelectedValue.ToUpper = "HQ" Or cboStatus.SelectedValue.ToUpper = "CAB" Or cboStatus.SelectedValue.ToUpper = "ACC" Then
                Me.SearchBy = Me.SearchBy & " AND PR.IsReconcile <> 1 AND PR.Status = '" & cboStatus.SelectedItem.Value & "' "
            End If
            If cboStatus.SelectedValue.ToUpper = "RECONCILE" Then
                Me.SearchBy = Me.SearchBy & " AND PR.IsReconcile = 1 And Pr.Status = 'TRF' "
            End If

            If cboStatus.SelectedValue.ToUpper = "EXE" Then
                Me.SearchBy = Me.SearchBy & " AND PR.IsReconcile = 1 and Pr.Status = 'EXE' "
            End If
            strFilterBy = strFilterBy & "Status = " & cboStatus.SelectedItem.Value
        End If

        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '%" & txtSearchBy.Text.Trim & "%')"
                strFilterBy = strFilterBy & ", " & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
            End If
        End If

        If txtDate.Text.Trim.Length > 0 Then

            If txtaging.Text.Trim <> "" Then
                Dim agingdate As Date
                agingdate = ConvertDate2(txtDate.Text.Trim).AddDays(CDbl(txtaging.Text) * +1)
                Me.SearchBy = Me.SearchBy & " AND RequestDate between '" & ConvertDate2(txtDate.Text.Trim) & "' and '" & ConvertDate2(agingdate.ToString("dd/MM/yyyy")) & "'"
            Else
                Me.SearchBy = Me.SearchBy & " AND RequestDate <= '" & ConvertDate2(txtDate.Text.Trim) & "'"
            End If

            strFilterBy = strFilterBy & ", RequestDate <= " & txtDate.Text.Trim
        End If

        With oCustomClass
            .WhereCond = Me.SearchBy
            Me.SortBy = " RequestDate Desc "
            Me.ParamReport = strFilterBy
        End With


        With oCustomClass
            .WhereCond = par
            Me.ParamReport = strFilterBy
        End With


        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)

    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label

        If e.Item.ItemIndex >= 0 Then
            Dim hypPrint As HyperLink
            Dim cookie As HttpCookie = Request.Cookies("PaymentRequestInquiry")
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                cookie.Values("FilterBy") = Me.ParamReport
                cookie.Values("BranchId") = cboParent.SelectedItem.Value.Trim
                cookie.Values("PrintedDate") = BusinessDate
                cookie.Values("PrintedBy") = Me.Loginid
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PaymentRequestInquiry")
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                cookieNew.Values.Add("FilterBy", Me.ParamReport)
                cookieNew.Values.Add("BranchId", cboParent.SelectedItem.Value.Trim)
                cookieNew.Values.Add("PrintedDate", BusinessDate)
                cookieNew.Values.Add("PrintedBy", Me.Loginid)

                Response.AppendCookie(cookieNew)
            End If

            Dim hyNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            hyNo.NavigateUrl = "javascript:OpenWinMain('" & cboParent.SelectedItem.Value.Trim & "','" & hyNo.Text.Trim & "')"

            hypPrint = CType(e.Item.FindControl("hypPrint"), HyperLink)
            hypPrint.NavigateUrl = "../../Webform.Reports.RDLC/LoanMnt/PayReq/PayReqPrint.aspx?RequestNo=" & hyNo.Text.Trim


            lblTemp = CType(e.Item.FindControl("lblAmount"), Label)
            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblTotalAmount"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If
    End Sub

End Class