﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ExecutePaymentRequestNextStep
    Inherits Maxiloan.Webform.WebBased
    Private m_TotalAmount As Double
    Private m_TotalTransfer As Double
#Region "Constanta"

#End Region
#Region "Property"
    Private Property RequestNo() As String
        Get
            Return (CType(Viewstate("RequestNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RequestNo") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return (CType(Viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccountID") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            ButtonExecute.Enabled = True
            ButtonExecute.Visible = True
            Dim cookie As HttpCookie = Request.Cookies("ExecutePaymentRequest")
            Dim BranchRequest As String = Replace(cookie.Values("BranchRequest"), "'", "")
            Me.RequestNo = cookie.Values("RequestNo")
            Dim TransferDate As String = cookie.Values("TransferDate")
            Dim RequestDate As String = cookie.Values("RequestDate")
            Dim RequestType As String = cookie.Values("RequestType")
            Dim Description As String = cookie.Values("Description")
            Me.BankAccountID = cookie.Values("BankAccountID")
            Dim BankAccountName As String = cookie.Values("BankAccountName")
            Dim DepartementName As String = cookie.Values("DepartementName")

            cookie.Values.Remove("ExecutePaymentRequest")

            LblBranchRequestID.Text = BranchRequest
            LblRequestNo.Text = RequestNo
            LblDescription.Text = Description
            LblRequestType.Text = RequestType
            LblRequestDate.Text = RequestDate
            LblTransferDate.Text = TransferDate
            LblBankAccount.Text = "(" + BankAccountID + ") " + BankAccountName
            Me.SearchBy = " prdtl.BranchID = '" & Replace(BranchRequest, "'", "") & "' "
            Me.SearchBy = Me.SearchBy & " And prdtl.RequestNo = '" & Replace(RequestNo, "'", "") & "' "
            BindGrid(Me.SearchBy, Me.SortBy)
        End If

    End Sub

#End Region
#Region "BindGrid"

    Public Sub BindGrid(ByVal cmdWhere As String, ByVal cmdsortby As String)
        Dim oEntities As New Parameter.GeneralPaging
        Dim oController As New GeneralPagingController

        If Me.SortBy = "" Then
            Me.SortBy = " RequestNo "
        End If

        With oEntities
            .SpName = "spPaymentRequestNextStepVersion2"
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)
        Dim dt As DataSet
        If Not oEntities Is Nothing Then
            dt = oEntities.ListDataReport
        End If
        DtgExecute.DataSource = dt
        DtgExecute.DataBind()
    End Sub
#End Region
#Region "SortGrid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Execute"


    Private Sub ButtonExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonExecute.Click
        Dim txtAmountTrans As New TextBox
        Dim oReconcile As New Parameter.PettyCashReconcile
        Dim oSaveContoller As New PettyCashController
        Dim chk As New CheckBox
        Dim lblRequire As New Label
        Dim lblAmountTransfer As New Label
        Dim seqno As New Label
        Dim tempDels As String = ""
        Dim stat As Boolean = True
        Dim totalPenggunaan As Double
        Dim dtl As New DataTable
        Dim oRow As DataRow

        With dtl
            .Columns.Add(New DataColumn("RequestNo", GetType(String)))
            .Columns.Add(New DataColumn("SequenceNo", GetType(Integer)))
            .Columns.Add(New DataColumn("AmountUsed", GetType(Double)))
        End With

        For intLoopGrid = 0 To DtgExecute.Items.Count - 1
            chk = CType(DtgExecute.Items(intLoopGrid).FindControl("chk"), CheckBox)
            seqno = CType(DtgExecute.Items(intLoopGrid).FindControl("lblSeqNO"), Label)
            lblRequire = CType(DtgExecute.Items(intLoopGrid).FindControl("lblRequire"), Label)
            txtAmountTrans = CType(DtgExecute.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            lblAmountTransfer = CType(DtgExecute.Items(intLoopGrid).FindControl("lblAmountTransfer"), Label)
            lblRequire.Visible = False

            If chk.Checked Then
                If txtAmountTrans.Text = "" Then
                    lblRequire.Text = "*"
                    lblRequire.Visible = True
                    stat = False
                Else
                    If CDbl(txtAmountTrans.Text) > CDbl(lblAmountTransfer.Text) Then
                        lblRequire.Text = "harus lebih kecil dari " & lblAmountTransfer.Text
                        lblRequire.Visible = True
                        stat = False
                    Else
                        oRow = dtl.NewRow
                        oRow("RequestNo") = CType(Me.RequestNo, String)
                        oRow("SequenceNo") = CType(seqno.Text, Integer)
                        oRow("AmountUsed") = CType(txtAmountTrans.Text, Double)
                        dtl.Rows.Add(oRow)

                        totalPenggunaan += CDbl(txtAmountTrans.Text)
                    End If
                End If

            Else
                lblRequire.Visible = False
            End If

        Next

        If stat = False Then
            Exit Sub
        End If

        If totalPenggunaan = 0 Then
            ShowMessage(lblMessage, "totalPenggunaan tidak boleh nol !!!", True)
            Exit Sub
        End If

        If dtl.Rows.Count > 0 Then
            With oReconcile
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .RequestNo = Me.RequestNo
                .BankAccountID = Me.BankAccountID
                .LoginId = Me.Loginid
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                .ReconcileMode = "EPR"
                .strDesc = txtReferenceNo.Text.Trim  'RefNo
                .TransactionName = "spExecutePaymentRequest" 'NamaSpnya
                .strConnection = GetConnectionString()
                .Amount = totalPenggunaan
                .NDtTable = dtl
            End With

            Try
                oReconcile = oSaveContoller.PCReimburseReconcile(oReconcile)
                Server.Transfer("ExecutePaymentRequest.aspx")
            Catch ex As MaxiloanExceptions

                Response.Write(ex.Message + ex.StackTrace)
                'ex.WriteLog(ex.Message, "LogName", "LogModulue")
                'ExceptionManager.Publish(ex)
                ButtonExecute.Enabled = False
                ButtonExecute.Visible = False



            End Try
        Else
            ShowMessage(lblMessage, "Tidak ada transaksi yang di pilih", True)
        End If
        

    End Sub
#End Region

#Region "Cancel"

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click


    End Sub
#End Region

    Private Sub DtgExecute_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgExecute.ItemDataBound
        Dim txtAmountTrans As New TextBox
        Dim txtAmountUsed As Label
        Dim lblsumTTotalAmount As Label
        Dim lblAmountTransfer As Label
        Dim lblTTotalAmount As Label
        Dim lblsumTransfer As Label

        Dim chk As New CheckBox
        'If e.Item.ItemType = ListItemType.Item Then
        If e.Item.ItemIndex >= 0 Then
            chk = CType(e.Item.FindControl("chk"), CheckBox)
            txtAmountTrans = CType(e.Item.FindControl("txtAmountTrans"), TextBox)
            txtAmountUsed = CType(e.Item.FindControl("txtAmountUsed"), Label)
            lblTTotalAmount = CType(e.Item.FindControl("lblTTotalAmount"), Label)
            lblAmountTransfer = CType(e.Item.FindControl("lblAmountTransfer"), Label)


            m_TotalAmount += CType(lblTTotalAmount.Text, Double)
            m_TotalTransfer += CType(lblAmountTransfer.Text, Double)

            If CDbl(txtAmountUsed.Text) > 0 Then
                txtAmountTrans.Enabled = False
                chk.Enabled = False
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblsumTTotalAmount = CType(e.Item.FindControl("lblsumTTotalAmount"), Label)
            lblsumTransfer = CType(e.Item.FindControl("lblsumTransfer"), Label)

            lblsumTTotalAmount.Text = FormatNumber(m_TotalAmount.ToString, 2)
            lblsumTransfer.Text = FormatNumber(m_TotalTransfer.ToString, 2)
        End If

    End Sub
End Class