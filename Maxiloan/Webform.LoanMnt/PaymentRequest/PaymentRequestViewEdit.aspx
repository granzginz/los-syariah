﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestViewEdit.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PaymentRequestViewEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PettyCashInquiryView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }


        function fclose() {
            window.close();
        }
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div runat="server" id="jlookupContent" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h3>
                    VIEW - TRANSAKSI PAYMENT REQUEST
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Cabang Request</label>
                    <asp:Label ID="lblBranchRequest" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
        <div class="form_left">
                <label>
                    Nama Karyawan
                </label>
                <asp:Label ID="lblEmployeeName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal
                </label>
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Request</label>
                    <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                <label>
                        Rekening Bank</label>
                    <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Jumlah Request</label>
                    <asp:Label ID="lblAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label> Departemen</label>
                <asp:Label ID="lblDepartement" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single"><div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                  CssClass="grid_general" DataKeyField="PaymentAllocationID" 
                    ShowFooter="True">
                 <HeaderStyle CssClass="th" />
                  <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                    <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PAYMENT ALLOCATION ID">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentAllocationID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PaymentAllocationID") %>'>
                                    </asp:Label>
                        </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TRANSAKSI">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPettyCashNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SEQ NO">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblSequenceNo" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SequenceNo") %>'>
                                    </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="COA">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCOA" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.COA") %>'>
                                    </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TANGGAL">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbltgl"  runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Tanggal") %>'  DataFormatString="{0:dd/MM/yyyy}"> 
                                    </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="Label1" runat="server">Total</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPCDAmount" runat="server" Text='<%#formatnumber(Container.DataItem("RequestAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="lblTotal" runat="server">Label</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN KOREKSI">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNote" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Note") %>'>
                                    </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div></div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    INFORMASI TRANSFER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Voucher</label>
                    <asp:Label ID="lblTransferRefVoucherNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Transfer</label>
                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Bukti Kas</label>
                    <asp:Label ID="lblTransferRefNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Transfer</label>
                    <asp:Label ID="lblTransferAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Status</label>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Status</label>
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        Di Ajukan Oleh</label>
                    <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                <label>
                    Departement
                </label>
                <asp:Label ID="lblDep" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
      <div class="form_button">
      <asp:Button ID="btnSave" runat="server" CausesValidation="false" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
      </asp:Panel>
      <asp:Panel ID="Panel1" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KOREKSI TRANSFER</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                Diajukan<label>
                </label>
                <asp:Label ID="lblKasir" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblTanggal" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblJum" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            <label>
                Keterangan</label>
                <asp:Label ID="lblKet" runat="server"></asp:Label>
        </div>
      </div>
      <div class="form_box">
      <div class="form_left">
                <label>
                    No Request
                </label>
                <asp:Label ID="lblRequestNo1" runat="server"></asp:Label>
            </div>
            <div class="form_right">
        </div>
      </div>
        <div class="form_box_uc">
            <uc1:ucLookUpTransaction id="oTrans" runat="server"></uc1:ucLookUpTransaction>
        </div>
        <div class="form_button">
        <asp:Button ID="BtnUpdate" runat="server" CausesValidation="false" Text="Update" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
