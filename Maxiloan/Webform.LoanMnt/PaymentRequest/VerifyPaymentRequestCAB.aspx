﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VerifyPaymentRequestCAB.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.VerifyPaymentRequestCAB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../Webform.UserController/ucBranchHO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VerifyPaymentRequestCAB</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
   
    function OpenWinViewPRInquiryEdit(pStyle, pPaymentRequestNo) {
        var x = screen.width; var y = screen.height - 100;
        window.open(ServerName + App + '/Webform.LoanMnt/PaymentRequest/PaymentRequestViewEdit.aspx?style=' + pStyle + '&RequestNo=' + pPaymentRequestNo, 'userlookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
    }
     			
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"  ToolTip="Click to close" onclick="hideMessage();"  />
            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3> VERIFIKASI - PERMINTAAAN DANA CABANG </h3>
                </div>
            </div>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_box">
                        <div class="form_left">
                            <label class="label_req"> Cabang</label>
                            <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
                        </div>
                        <div class="form_right">
                            <label class="label"> Tanggal Request</label>
                            <asp:TextBox ID="txtDate" runat="server" Width="80px"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDate" Format="dd/MM/yyyy" />
                        </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                            <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                            <asp:ListItem Value="ReferenceNo">No Memo</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"  Width="240px"></asp:TextBox>
                    </div>
                </div>
                 
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>

             <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4> DAFTAR PERMINTAAN PEMBAYARAN </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemStyle CssClass="command_col" Width="7%" />
                                        <ItemTemplate>
                                             <asp:LinkButton ID="hypPrint" runat="server" Text="Verifikasi"  CommandName="Verifikasi"/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestBy" HeaderText="DIAJUKAN OLEH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestBy" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Container.DataItem("Description") %>' />
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="JUMLAH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalAmount"),0)%>' />
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAmount" runat="server" Visible="True" Text='0.0' />
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL REQUEST" DataFormatString="{0:dd/MM/yyyy}">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Note" HeaderText="NOTE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNote" runat="server" Text='<%#Container.DataItem("Note")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO MEMO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoMemo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                              <uc2:ucGridNav id="GridNavigator" runat="server"/>
                        </div>
                    </div>
                </div> 
            </asp:Panel>



    <asp:panel id="pnlVerify" runat="server"  Visible="false">
                
        <div class="form_box">
                <div class="form_left">
                    <label> Cabang Request</label>
                    <asp:Label ID="lblBranchRequest" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Departemen</label>
                    <asp:Label ID="lblDepartement" runat="server"></asp:Label>
                </div>
            </div>
        <div class="form_box">
                <div class="form_left">
                    <label> No Request</label>
                    <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Tanggal Request</label>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </div>
            </div>
        <div class="form_box">
                <div class="form_left">
                <label runat="server" id="lblBankAccountID">
                    Transfer Rekening</label>
                    <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Jumlah Request</label>
                    <asp:Label ID="lblAmount" runat="server"></asp:Label>
                </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                   Nama Rekening</label>
                <asp:Label ID="lblNamaRek" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label> Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label>
                   Nomor Rekening</label>
                <asp:Label ID="lblNoRek" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label> Memo</label>
                <asp:Label ID="lblMemo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4> DETAIL TRANSAKSI </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgreeVerify" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                        <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST" Visible="false">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PAYMENT ALLOCATION ID" Visible="false">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentAllocation" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PaymentAllocationID") %>'>
                                    </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SEQ NO" Visible="false">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblSequenceNo" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SequenceNo") %>'>
                                    </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <ItemTemplate> <asp:Label ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("TransactionName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COA" Visible="true">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCOA" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.COA") %>'>
                                    </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <ItemTemplate> <asp:Label ID="Label2" runat="server" Text='<%#Container.DataItem("Description")%>' />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TANGGAL" Visible="false" >
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbltgl"  runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Tanggal") %>'  DataFormatString="{0:dd/MM/yyyy}"> 
                                    </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH REQUEST">
                                <ItemTemplate> <asp:Label ID="lblRequestAmount" runat="server" Text='<%#formatnumber(Container.DataItem("RequestAmount"),2)%>' />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalRequestAmount" runat="server">0.0</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TRANSFER">
                                <ItemTemplate>
                                    <asp:Label ID="lblTransferAmountDetail" runat="server" Text='<%#formatnumber(Container.DataItem("TransferAmount"),2)%>' />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalTransferAmount" runat="server">0.0</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Note")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4> INFORMASI TRANSFER </h4>
            </div>
        </div>
        <div class="form_box">
                <div class="form_left">
                    <label> No Voucher</label>
                    <asp:Label ID="lblTransferRefVoucherNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Tanggal Transfer</label>
                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
                </div>
        </div>
        <div class="form_box">
                <div class="form_left">
                    <label> No Bukti Kas</label>
                    <asp:Label ID="lblTransferRefNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Jumlah Transfer</label>
                    <asp:Label ID="lblTransferAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
                <div class="form_left">
                    <label> Status</label>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Tanggal Status</label>
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
                </div>
        </div>

        <div class="form_box">
                <div class="form_left">
                    <label> Di Ajukan Oleh</label>
                    <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <div class="form_left"> </div>
                <div class="form_right"> </div>
            </div>
        </div>

        <asp:Panel ID="pnlbutton" runat="server" Width="100%">
            <div class="form_button">
            <asp:Button ID="btnVerifikasi" runat="server" Text="Verify" CssClass="small button blue" CausesValidation="False" OnClientClick="if(this.value === 'Saving...') { return false; } else { this.value = 'Saving...'; }"/>
            <asp:Button ID="ButtonClose"  runat="server" CausesValidation="false" Text="Close" CssClass="small button gray" />
            </div>
        </asp:Panel>

    </asp:panel>
    <asp:Panel ID="PanelKoreksi" runat="server" Visible="false" >
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KOREKSI TRANSFER</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Request
                </label>
                <asp:Label ID="lblRequestNo1" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblTglStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblJum" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            <label>
                Keterangan</label>
                <asp:Label ID="lblKet" runat="server"></asp:Label>
        </div>
      </div>
        <div class="form_box_uc">
            <uc1:ucLookUpTransaction id="oTrans" runat="server"></uc1:ucLookUpTransaction>
        </div>
        <div class="form_button">
        <asp:Button ID="BtnUpdate" runat="server" CausesValidation="false" Text="Save" CssClass="small button blue" OnClientClick="if(this.value === 'Saving...') { return false; } else { this.value = 'Saving...'; }">
            </asp:Button>
            <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
