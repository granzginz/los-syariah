﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequest.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentRequest" %>

<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountNoCondition" Src="../../Webform.UserController/ucBankAccountNoCondition.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Request</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }

        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah Yakin mau hapus data ini ? ")) {
                return true;
            }
            else {
                return false;
            }
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak Berhak');
            }
        }
        document.onmousedown = click			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PERMINTAAN PEMBAYARAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="PnlHeader" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Transfer Ke Rekening Bank</label>
                <asp:Label ID="lblBankAccount" runat="server" Visible="False"></asp:Label>
                <uc1:ucbankaccountnocondition id="oBankAcc" runat="server"></uc1:ucbankaccountnocondition>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jumlah</label>
                <uc1:ucnumberformat runat="server" id="txtAmount"></uc1:ucnumberformat>                
                <asp:Label ID="lblAmount" runat="server" Visible="False"></asp:Label>                
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Departemen</label>
                <asp:DropDownList ID="cboDepartement" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ControlToValidate="cboDepartement" ErrorMessage="Harap pilih Departemen" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Label ID="lblDepartement" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Keterangan</label>
                <asp:TextBox ID="txtDescription" runat="server" Width="448px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    ControlToValidate="txtDescription" ErrorMessage="Harap diisi Keterangan" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Label ID="lblDescription" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd1" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel1" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Button ID="ButtonAdd2" runat="server" Visible="False" CausesValidation="True"
                Text="Add" CssClass="small button blue"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPRList" runat="server" Visible="true" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionID" runat="server" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionName" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Container.DataItem("Desc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPRAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPRAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonNext" runat="server" CausesValidation="False" Text="Next" CssClass="small button blue">
            </asp:Button>
        </div>
        <asp:Button ID="ButtonSaveApproval" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
    </asp:Panel>
    </form>
</body>
</html>
