﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ReconcilePaymentRequestNextStepVersion2
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

#End Region
#Region "Property"
    Private Property RequestNo() As String
        Get
            Return (CType(Viewstate("RequestNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RequestNo") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return (CType(Viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccountID") = Value
        End Set
    End Property
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Dim cookie As HttpCookie = Request.Cookies("ReconcilePaymentRequest")
            Dim BranchRequest As String = Replace(cookie.Values("BranchRequest"), "'", "")
            Me.RequestNo = cookie.Values("RequestNo")
            Dim TransferDate As String = cookie.Values("TransferDate")
            Dim RequestDate As String = cookie.Values("RequestDate")
            Dim RequestType As String = cookie.Values("RequestType")
            Dim Description As String = cookie.Values("Description")
            Me.BankAccountID = cookie.Values("BankAccountID")
            Dim BankAccountName As String = cookie.Values("BankAccountName")

            LblBranchRequestID.Text = BranchRequest
            LblRequestNo.Text = RequestNo
            LblDescription.Text = Description
            LblRequestType.Text = RequestType
            LblRequestDate.Text = RequestDate
            LblTransferDate.Text = TransferDate
            LblBankAccount.Text = "(" + BankAccountID + ") " + BankAccountName
            Me.SearchBy = " prdtl.BranchID = '" & Replace(BranchRequest, "'", "") & "' "
            Me.SearchBy = Me.SearchBy & " And prdtl.RequestNo = '" & Replace(RequestNo, "'", "") & "' "
            BindGrid(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "BindGrid"

    Public Sub BindGrid(ByVal cmdWhere As String, ByVal cmdsortby As String)
        Dim oEntities As New Parameter.GeneralPaging
        Dim oController As New GeneralPagingController

        If Me.SortBy = "" Then
            Me.SortBy = " RequestNo "
        End If

        With oEntities
            .SpName = "spPaymentRequestNextStepVersion2"
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)
        Dim dt As DataSet
        If Not oEntities Is Nothing Then
            dt = oEntities.ListDataReport
        End If
        DtgReconcile.DataSource = dt
        DtgReconcile.DataBind()
    End Sub
#End Region
#Region "SortGrid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Ketika tombol Reconcile di click!"
    Private Sub imgReconcile_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReconcile.Click
        Dim oReconcile As New Parameter.PettyCashReconcile
        Dim oSaveContoller As New PettyCashController
        With oReconcile
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .RequestNo = Me.RequestNo
            .BankAccountID = Me.BankAccountID
            .LoginId = Me.Loginid
            .CoyID = Me.SesCompanyID
            .BusinessDate = Me.BusinessDate
            .ReconcileMode = "PR"
            .strDesc = txtReferenceNo.Text.Trim  'RefNo
            .TransactionName = "spReconcilePaymentRequest" 'NamaSpnya
            .strConnection = GetConnectionString()
        End With

        Try
            oReconcile = oSaveContoller.PCReimburseReconcile(oReconcile)
            Server.Transfer("ReconcilePaymentRequest.aspx")
        Catch ex As MaxiloanExceptions

            Response.Write(ex.Message + ex.StackTrace)
            'ex.WriteLog(ex.Message, "LogName", "LogModulue")
            'ExceptionManager.Publish(ex)

        End Try

    End Sub
#End Region
#Region "Ketika tombol cancel diclick!"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Server.Transfer("ReconcilePaymentRequest.aspx")
    End Sub
#End Region


End Class