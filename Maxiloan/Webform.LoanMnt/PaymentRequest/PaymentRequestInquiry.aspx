﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestInquiry.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentRequestInquiry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../Webform.UserController/ucBranchHO.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentRequestInquiry</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        //var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        //var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        //var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        var ServerName = window.location.protocol + '//' + window.location.host + '/';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function OpenWinMain(BranchId, RequestNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PaymentRequest/PaymentRequestInquiryView.aspx?BranchId=' + BranchId + '&RequestNo=' + RequestNo + '&style=AccMnt', null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }					
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1" >
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"  onclick="hideMessage()"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        INQUIRY - PERMINTAAN PEMBAYARAN
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                        <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                        <div class="form_right">
                            <label class="label">
                                Tanggal Request</label>
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                            <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                            <asp:ListItem Value="ReferenceNo">No Memo</asp:ListItem>
                            <asp:ListItem Value="TotalAmount">Jumlah</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"  Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                        <div class="form_single">
                            <label>
                                Aging &nbsp;&gt;=
                            </label>
                            <asp:TextBox ID="txtaging" runat="server" CssClass="inptype" Width="80px"></asp:TextBox>(day)
                        </div>
                    </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="cboStatus" runat="server">
                            <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                            <asp:ListItem Value="REQ">New</asp:ListItem>
                            <asp:ListItem Value="CAB">BM CABANG</asp:ListItem>
                            <asp:ListItem Value="HQ">HRD / GA</asp:ListItem>
                            <asp:ListItem Value="ACC">Accounting</asp:ListItem>
                            <asp:ListItem Value="TRF">Transfered</asp:ListItem>
                            <asp:ListItem Value="CAN">Cancelled</asp:ListItem>
                            <asp:ListItem Value="RECONCILE">Reconciled</asp:ListItem>
                            <asp:ListItem Value="EXE">Executed</asp:ListItem>
                            <asp:ListItem Value="APR">Approval</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PERMINTAAN PEMBAYARAN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH" Visible="false">
                                        <ItemStyle CssClass="command_col" Width="7%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypPrint" runat="server" Text="CETAK"  Target="_blank">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                            </asp:HyperLink>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestBy" HeaderText="DIAJUKAN OLEH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestBy" runat="server" Text='<%#Container.DataItem("RequestBy")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="No. MEMO">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" width="35%"> </ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Container.DataItem("Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalAmount"),0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAmount" runat="server" Visible="True" Text='0.0'></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL REQUEST"
                                        DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                     
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
