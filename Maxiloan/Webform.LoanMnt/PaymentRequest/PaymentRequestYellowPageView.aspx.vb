﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class PaymentRequestYellowPageView
    Inherits Maxiloan.Webform.WebBased
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents crvKwitansi As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region 
    Private oController As New PaymentRequestController
    Private Property PettyCashNo() As String
        Get
            Return CType(ViewState("PettyCashNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PettyCashNo") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If 
        bindReport()
    End Sub

    Private Sub bindReport()
        Dim oDataSet As New DataSet
        Dim oReport = New YellowReimbursePage


        Dim data = CType(Session(COOKIES_PETTY_CASH_VOUCHER), IList(Of String))
        oDataSet = oController.GetPaymentRequestFormKuning(data, GetConnectionString)

        oReport.SetDataSource(oDataSet.Tables(0))
        crvKwitansi.ReportSource = oReport
        crvKwitansi.DataBind()


        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        oReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String = String.Format("{0}{1}{2}", Me.Session.SessionID, Me.Loginid, "PaymentRequestFormKuning")

        DiskOpts.DiskFileName = String.Format("{0}XML\{1}.pdf", Request.ServerVariables("APPL_PHYSICAL_PATH"), strFileLocation)
        oReport.ExportOptions.DestinationOptions = DiskOpts
        oReport.Export()
        oReport.Close()
        oReport.Dispose()
        Session("COOKIES_PETTY_CASH_VOUCHER") = Nothing
        Response.Redirect("PaymentRequestYellowPage.aspx?strFileLocation=" & strFileLocation)

    End Sub

End Class