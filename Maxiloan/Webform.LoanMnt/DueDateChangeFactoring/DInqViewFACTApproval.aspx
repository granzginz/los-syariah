﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DInqViewFACTApproval.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.DInqViewFACTApproval" %>

<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DInqView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
     <input id="lblNextDueDate" type="hidden" name="lblNextDueDate" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                GANTI TANGGAL JATUH TEMPO FACTORING
                <asp:Label ID="lblJudulHeader" runat="server" visible ="false"></asp:Label>
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            <label>
                Tgl JT Awal
            </label>
                <asp:Label ID="lblDueDateAwal" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            <label> Tgl JT Perpanjangan</label>
            <asp:Label ID="lblNexDueDate" runat="server"></asp:Label>
            </div>
        </div> 
       <%-- <div class="form_left">
            <label>
                Tanggal Efektif
            </label>
            <asp:Label ID="lblEffDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Pengajuan
            </label>
            <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
        </div>--%>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            <label> Tgl Pengajuan </label>
                 <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label> Rate Margin </label>
                  <asp:Label ID="lblEffRate" runat="server"></asp:Label>%
            </div>
        </div>
    </div>
      <asp:Panel ID="pnlPaymentInfo" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h4>
                OS dan Denda
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Sisa Pokok (O/S Principal)
                </label>
                <asp:Label ID="lblOsPrinciple" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <%--Sisa Bunga (O/S Interest)--%>
                    Nilai Margin (Interest Amount)
                </label>
                <asp:Label ID="lblOsInterest" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Denda Keterlambatan Angsuran
                </label>
                <asp:Label ID="lbLCInstallx" runat="server" CssClass="numberAlign label">0.00</asp:Label>
            </div>
            <div class="form_right">
                <div class="form_box_hide">
                    <label>
                        Sisa Margin (O/S Interest) Baru
                    </label>
                    <asp:Label ID="lblOsInterestNew" runat="server" CssClass="numberAlign label"></asp:Label>
                </div>
            </div>
        </div>
    </div>
        <div class="form_title">
            <div class="form_single">
                <asp:HiddenField runat="server" ID="hdfApplicationID" />
                <h4>VIEW AGREEMENT FACTORING INVOICE</h4>
            </div>
        </div>
        <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="SeqNo"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="InvoiceNo"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="TGL Invoice" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Margin" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="RetensiAmount" HeaderText="Retensi" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Total Pokok"  DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="JangkaWaktu" HeaderText="Jangka Waktu" DataFormatString="{0:0,0}"></asp:BoundColumn>                     
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
    </asp:Panel>
        <asp:Panel ID="pnlAmor" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW AGREEMENT FACTORING INVOICE BARU
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                        CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="SeqNo"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="InvoiceNo"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="TGL Invoice" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Margin" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="RetensiAmount" HeaderText="Retensi" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Total Pokok"  DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="JangkaWaktu" HeaderText="Jangka Waktu" DataFormatString="{0:0,0}"></asp:BoundColumn>     
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4> DETAIL GANTI TANGGAL JATUH TEMPO FACTORING </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Administrasi
            </label>
            <asp:Label ID="lblAdminFee" runat="server" ></asp:Label> 
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Alasan
            </label>
            <asp:Label ID="lblReason" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Disetujui Oleh
            </label>
            <asp:Label ID="lblApproved" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan</label>
            <asp:Label ID="lblNotes" runat="server"></asp:Label>
        </div>
    </div>
    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" align="center"
        border="0">
        <tr>
            <td class="tdgenap" align="right">
                Total yg Harus Dibayar:
                <asp:Label ID="totAmountpaid" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" align="right">
                Jumlah Prepaid:
                <asp:Label ID="lblPaidAmount" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" align="right">
                Selisih:
                <asp:Label ID="lblBal" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
    </table>
   <%-- <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Total Yang harus dibayar</label>
            <asp:Label ID="totAmountpaid" runat="server" CssClass="numberAlign regular_text"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Jumlah Prepaid</label>
            <asp:Label ID="lblPrepaidAmount" runat="server" CssClass="numberAlign regular_text"></asp:Label>
        </div>
    </div>--%>
 <%--   <uc1:ucapprovalrequest id="oReq" runat="server">
                </uc1:ucapprovalrequest>--%>
    <div class="form_button"> 
      <%--  <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
        </asp:Button>--%>
        <%--<asp:Button ID="BtnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>--%>
    </div>
         
    </form>
</body>
</html>
