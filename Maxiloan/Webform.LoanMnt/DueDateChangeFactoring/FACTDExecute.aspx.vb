﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FACTDExecute
	Inherits Maxiloan.Webform.WebBased
	Protected WithEvents oSearchBy As UcSearchBy

#Region "Constanta"
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1

	Private oCustomClass As New Parameter.AgreementList
	Private oController As New AgreementListController
#End Region

#Region "Page Load"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			If Request.QueryString("message") <> "" Then

				ShowMessage(lblMessage, Request.QueryString("message"), True)
			End If
			Me.FormID = "FACTDUEDATEEXEC"
			oSearchBy.ListData = "Agreementno, Agreement No-Name, Name"
			oSearchBy.BindData()


			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				Me.SearchBy = ""
				Me.SortBy = ""
			End If
		End If
	End Sub

#End Region

#Region "DoBind"
	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView
		Dim intloop As Integer
		Dim hypID As HyperLink
		Dim oPaging As New Parameter.GeneralPaging
		Dim m_controller As New GeneralPagingController

		With oPaging
			.strConnection = GetConnectionString()
			.WhereCond = cmdWhere
			.CurrentPage = currentPage
			.PageSize = pageSize
			.SortBy = SortBy
			.SpName = "spFACTChangeDueDatePaging"
		End With

		oPaging = m_controller.GetGeneralPaging(oPaging)
		If Not oPaging Is Nothing Then
			DtUserList = oPaging.ListData
			recordCount = oPaging.TotalRecords
		Else
			recordCount = 0
		End If
		DtgAgree.DataSource = DtUserList.DefaultView
		DtgAgree.CurrentPageIndex = 0
		DtgAgree.DataBind()
		PagingFooter()
		pnlList.Visible = True
		pnlDatagrid.Visible = True
		lblMessage.Text = ""
	End Sub

#End Region

#Region "Navigation"
	Private Sub PagingFooter()
		lblPage.Text = currentPage.ToString()
		totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
		If totalPages = 0 Then

			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			lblTotPage.Text = "1"
			rgvGo.MaximumValue = "1"
		Else
			lblTotPage.Text = CType(totalPages, String)
			rgvGo.MaximumValue = CType(totalPages, String)
		End If
		lblrecord.Text = CType(recordCount, String)

		If currentPage = 1 Then
			imbPrevPage.Enabled = False
			imbFirstPage.Enabled = False
			If totalPages > 1 Then
				imbNextPage.Enabled = True
				imbLastPage.Enabled = True
			Else
				imbPrevPage.Enabled = False
				imbNextPage.Enabled = False
				imbLastPage.Enabled = False
				imbFirstPage.Enabled = False
			End If
		Else
			imbPrevPage.Enabled = True
			imbFirstPage.Enabled = True
			If currentPage = totalPages Then
				imbNextPage.Enabled = False
				imbLastPage.Enabled = False
			Else
				imbLastPage.Enabled = True
				imbNextPage.Enabled = True
			End If
		End If
	End Sub

	Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
		Select Case e.CommandName
			Case "First" : currentPage = 1
			Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
			Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
			Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
		End Select
		If Me.SortBy Is Nothing Then
			Me.SortBy = ""
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub

	Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		If IsNumeric(txtPage.Text) Then
			If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
				currentPage = CType(txtPage.Text, Int16)
				If Me.SortBy Is Nothing Then
					Me.SortBy = ""
				End If
				DoBind(Me.SearchBy, Me.SortBy)
			End If
		End If
	End Sub

#End Region

#Region "databound"
	Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
		Dim lblTemp As Label
        Dim lbbranch As Label
        Dim lblInvoiceNo As Label
        Dim hyTemp As HyperLink

		If SessionInvalid() Then
			Exit Sub
		End If
		Dim m As Int32
		Dim hypCancel As HyperLink
		Dim HyExecute As HyperLink
		Dim hyApplicationid As HyperLink
		Dim lblInStatus As Label
		Dim inlblSeqNo As New Label
		Dim invSeqNo As New Label
		Dim flagdel As String = "2"
		Dim inflagdel As String = "1"

		If e.Item.ItemIndex >= 0 Then
			lblInStatus = CType(e.Item.FindControl("lblStatus"), Label)
			lbbranch = CType(e.Item.FindControl("lbranchid"), Label)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            lblInvoiceNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
			hyApplicationid = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
			hypCancel = CType(e.Item.FindControl("HYCAN"), HyperLink)
			HyExecute = CType(e.Item.FindControl("HYEXEC"), HyperLink)
			inlblSeqNo = CType(e.Item.FindControl("lblSeqNO"), Label)
			invSeqNo = CType(e.Item.FindControl("lblInvoiceSeqNo"), Label)

            hypCancel.NavigateUrl = "FACTDExecProc.aspx?Applicationid=" & hyTemp.Text.Trim & "&branchid=" & lbbranch.Text.Trim & "&SeqNo=" & inlblSeqNo.Text.Trim & "&FlagDel=" & inflagdel.Trim & "&InvoiceSeqNo=" & invSeqNo.Text.Trim & "&InvoiceNo=" & lblInvoiceNo.Text.Trim

            If lblInStatus.Text.Trim = "A" Then
				HyExecute.Enabled = True
                HyExecute.NavigateUrl = "FACTDExecProc.aspx?Applicationid=" & hyTemp.Text.Trim & "&branchid=" & lbbranch.Text.Trim & "&SeqNo=" & inlblSeqNo.Text.Trim & "&FlagDel=" & flagdel.Trim & "&InvoiceSeqNo=" & invSeqNo.Text.Trim & "&InvoiceNo=" & lblInvoiceNo.Text.Trim
            Else
				HyExecute.Enabled = False
			End If



			'*** Customer Link
			'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
			hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
			hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
			'*** Agreement No link
			hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
			hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyApplicationid.Text.Trim) & "')"
			'*** ApplicationId link
			hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
			hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

		End If
	End Sub
#End Region

#Region "Reset"

	Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
		Server.Transfer("DExecute.aspx")
	End Sub
#End Region

#Region "Search"
	Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
		If SessionInvalid() Then
			Exit Sub
		End If
		If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

			Me.SearchBy = ""
			Me.SearchBy = Me.SearchBy & "  ad.branchid = '" & Me.sesBranchId.Replace("'", "") & "'"
			Me.SearchBy = Me.SearchBy & "   and   ad.Status in ('A','R') "
			If oSearchBy.Text.Trim <> "" Then
				If Right(oSearchBy.Text.Trim, 1) = "%" Then
					Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
				Else
					Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
				End If

			End If
			pnlDatagrid.Visible = True
			DtgAgree.Visible = True
			pnlList.Visible = True
			DoBind(Me.SearchBy, Me.SortBy)

		End If
	End Sub
#End Region

#Region "Sort"
	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
#End Region

End Class