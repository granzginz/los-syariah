﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FACTDRequest
	Inherits Maxiloan.Webform.AccMntWebBased
	Dim tempInstallmentAmount As Double
	Protected WithEvents oReq As ucApprovalRequest
	Protected WithEvents txtAdminFee As ucNumberFormat
	Protected WithEvents txtInterestPaid As ucNumberFormat
	Protected WithEvents ucEfRate As ucNumberFormat

#Region "Property"
	Private Property strCustomerid() As String
		Get
			Return (CType(ViewState("strCustomerid"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("strCustomerid") = Value
		End Set
	End Property

	Private Property PaymentFrequency() As Integer
		Get
			Return (CType(ViewState("PaymentFrequency"), Integer))
		End Get
		Set(ByVal Value As Integer)
			ViewState("PaymentFrequency") = Value
		End Set
	End Property

	Private Property OSPrincipal() As Double
		Get
			Return (CType(ViewState("OSPrincipal"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("OSPrincipal") = Value
		End Set
	End Property

	Private Property EffRate() As Decimal
		Get
			Return (CType(ViewState("EffRate"), Decimal))
		End Get
		Set(ByVal Value As Decimal)
			ViewState("EffRate") = Value
		End Set
	End Property

	Private Property DaysDiff() As Integer
		Get
			Return (CType(ViewState("DaysDiff"), Integer))
		End Get
		Set(ByVal Value As Integer)
			ViewState("DaysDiff") = Value
		End Set
	End Property

	Private Property TotInterest() As Double
		Get
			Return (CType(ViewState("TotInterest"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("TotInterest") = Value
		End Set
	End Property

	Private Property TotAmountTobePaid() As Double
		Get
			Return (CType(ViewState("TotAmountTobePaid"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("TotAmountTobePaid") = Value
		End Set
	End Property

	Private Property TotInfo() As Double
		Get
			Return (CType(ViewState("TotInfo"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("TotInfo") = Value
		End Set
	End Property

	Private Property Bev() As String
		Get
			Return (CType(ViewState("Bev"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("Bev") = Value
		End Set
	End Property

	Private Property VTempDate() As Date
		Get
			Return (CType(ViewState("VTempDate"), Date))
		End Get
		Set(ByVal Value As Date)
			ViewState("VTempDate") = Value
		End Set
	End Property

	Property FacilityNo() As String
		Get
			Return ViewState("FacilityNo").ToString
		End Get
		Set(ByVal Value As String)
			ViewState("FacilityNo") = Value
		End Set
	End Property

	Property ItemData() As DataTable
		Get
			Return ViewState("ItemData")
		End Get
		Set(ByVal Value As DataTable)
			ViewState("ItemData") = Value
		End Set
	End Property

    Private Property InsSeqNo() As Integer
        Get
            Return (CType(ViewState("InsSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("InsSeqNo") = Value
        End Set
    End Property
    Private Property InvoiceNo() As String
        Get
            Return (CType(ViewState("InvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property FacilityId() As String
        Get
            Return ViewState("FacilityId").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityId") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
	Private oController As New DChangeController
	Private oControllerInfo As New UCPaymentInfoController
	Private oCustomClassInfo As New Parameter.AccMntBase
	Private m_ControllerApp As New ApplicationController
	Private oFactoringInvoice As New Parameter.FactoringInvoice
	Private m_controller As New FactoringInvoiceController
#End Region

#Region "Declare Variable"
	Dim tempPrincipalAmount As Double
	Dim tempInterestAmount As Double
	Dim tempOSPrincipalAmount As Double
	Dim tempOSInterestAmount As Double
	Dim tempdate As Date
	Dim dateBefore As Date
	Dim i As Integer
	Dim j As Integer = 0
	Dim TotInstallmentAmount As Label
	Dim TotPrincipalAmount As Label
	Dim TotInterestAmount As Label
	Dim TotOSPrincipalAmount As Label
	Dim TotOSInterestAmount As Label
	Dim intcounter As Integer = 1
#End Region

#Region "PageLoad"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		If SessionInvalid() Then
			Exit Sub
		End If
		Me.FormID = "DUEDATEREQFACT"
		If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
			If Not IsPostBack Then
				With txtAdminFee
					.RequiredFieldValidatorEnable = True
					.RangeValidatorEnable = True
					.TextCssClass = "numberAlign regular_text"
				End With

				Me.InsSeqNo = Request.QueryString("InvoiceSeqNo")

				With txtInterestPaid
					.RequiredFieldValidatorEnable = True
					.TextCssClass = "numberAlign regular_text"
				End With

                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                Me.BranchID = Me.sesBranchId.Replace("'", "")
				txtEfdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

				oCustomClass.ApplicationID = Me.ApplicationID
				oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                oCustomClass.InsSeqNo = Me.InsSeqNo
                oCustomClass.InvoiceNo = Me.InvoiceNo
                oCustomClass.strConnection = GetConnectionString()
				oCustomClass = oController.GetListFactoring(oCustomClass)
				With oCustomClass
					txtsdate.Text = .NextInstallmentDueDate.ToString("dd/MM/yyyy")
					txtDueDateAwal.Text = .NextInstallmentDueDate.ToString("dd/MM/yyyy")
					lblOsPrinciple.Text = FormatNumber(.OutstandingPrincipal, 2)
					lblOsInterest.Text = FormatNumber(.OutStandingInterest, 2)
					ucEfRate.Text = FormatNumber(.EffectiveRate, 2)
				End With

				GetList()
				oReq.ApprovalScheme = "FCDD"
				oReq.ReasonTypeID = "AGCDD"
				pnlAmor.Visible = False
				BtnRequest.Visible = False
				BtnCalculate2.Visible = False
				DoBind()
			End If
		End If
	End Sub
#End Region

#Region "Dobind"
	Sub DoBind()
		' lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
		oCustomClassInfo.ApplicationID = Me.ApplicationID
		oCustomClassInfo.strConnection = GetConnectionString()
		oCustomClassInfo.ValueDate = Me.BusinessDate
		oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
		With oCustomClassInfo
			lblAgreementNo.Text = .Agreementno
			Me.strCustomerid = .CustomerID
			lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
			lblCustName.Text = .CustomerName
			lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
		End With

		Dim oController As New UCPaymentInfoController
		Dim oCustomClass As New Parameter.AccMntBase
		Dim oclass As New Parameter.FactoringInvoice
		With oclass
			.strConnection = GetConnectionString()
			.ApplicationID = Me.ApplicationID
			.BranchId = Me.BranchID
		End With

		With oCustomClass
			.strConnection = GetConnectionString()
			.ApplicationID = Me.ApplicationID
			.ValueDate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
			.BranchId = Me.BranchID
		End With

		Dim oApplication As New Parameter.Application
		Dim oData As New DataTable

		oApplication.strConnection = GetConnectionString()
		oApplication.BranchId = Me.BranchID
		oApplication.AppID = Me.ApplicationID
		oApplication.ApplicationID = Me.ApplicationID
		oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

		If Not oApplication Is Nothing Then
			oData = oApplication.ListData
		End If
		Dim oRow As DataRow = oData.Rows(0)
        Me.FacilityNo = oRow("NoFasilitas").ToString
        Me.FacilityId = oRow("FacilityId").ToString


        'If (oRow("ContractStatus").ToString = "PRP") Then
        If (oRow("ContractStatus").ToString = "AKT") Then
			oFactoringInvoice.strConnection = GetConnectionString()
			oFactoringInvoice.BranchId = Me.BranchID
			oFactoringInvoice.ApplicationID = Me.ApplicationID
			''Modify by Wira 20180830
			'Add valuedate
			'oFactoringInvoice.valuedate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
			'oFactoringInvoice = m_controller.GetJatuhTempoList(oFactoringInvoice)

			'ItemData = oFactoringInvoice.Listdata
			'dtgInvoice.DataSource = ItemData.DefaultView
			'dtgInvoice.DataBind()
		End If

		'*/ GetListInvoiceFactoring /*'
		Dim oControllerD As New DChangeController
		Dim oCustomClassD As New Parameter.DChange

		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView

		With oCustomClassD
			.ApplicationID = Me.ApplicationID
			.InsSeqNo = Me.InsSeqNo
			.strConnection = GetConnectionString()
		End With
		oCustomClassD = oControllerD.GetListInvoiceFactoring(oCustomClassD)
		DtUserList = oCustomClassD.ListData
		dtgInvoice.DataSource = DtUserList.DefaultView
		dtgInvoice.DataBind()

		'' Jika InvoiceStatus Sudah Lunas Maka Tidak Bisa Dilanjut ''
		If DtUserList.Rows(0)("InvoiceStatus").ToString() = "LNS" Then
			lblMessage.Visible = True
			ShowMessage(lblMessage, "Invoice Sudah LUNAS", True)

			BtnCalculate.Enabled = False
			BtnCalculate2.Enabled = False
			BtnRequest.Enabled = False
		End If



	End Sub
#End Region

#Region "Calculate"
	Private Sub BtnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCalculate.Click
		lblMessage.Text = ""
		lblMessage.Visible = False

		'If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtEfdate.Text)) < 0 Then
		'	lblMessage.Visible = True
		'	ShowMessage(lblMessage, "Tanggal Efektif harus >= Tgl hari ini", True)
		'	Exit Sub
		'End If

		If ConvertDate2(txtEfdate.Text) > ConvertDate2(txtDueDateAwal.Text) Then
			lblMessage.Visible = True
			ShowMessage(lblMessage, "Tanggal Pengajuan Tidak Boleh Lebih Besar Dari Tanggal Jatuh Tempo Awal", True)
			Exit Sub
		End If

		pnlAmor.Visible = True
		Dim StrBehv As String
		GetList()
		If lblMessage.Text.Trim <> "" Then
			Exit Sub
		End If
		txtAdminFee.Text = CStr(oCustomClass.ChangeDueDateFee)
		Select Case Me.Bev
			Case "L"
				txtAdminFee.Enabled = False
				txtAdminFee.RangeValidatorMinimumValue = "0"
				txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
			Case "N"
				txtAdminFee.RangeValidatorMinimumValue = CStr(oCustomClass.ChangeDueDateFee)
				txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
			Case "X"
				txtAdminFee.RangeValidatorMaximumValue = CStr(oCustomClass.ChangeDueDateFee)
				txtAdminFee.RangeValidatorMinimumValue = "0"
			Case "D"
				txtAdminFee.RangeValidatorMinimumValue = "0"
				txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
		End Select

		' lbljudul.Text = ConvertDate2(txtEfdate.Text).ToString("dd/MM/yyyy")
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView
		With oCustomClass
			.ApplicationID = Me.ApplicationID
			.InsSeqNo = Me.InsSeqNo
			.strConnection = GetConnectionString()
			.RequestDate = ConvertDate2(txtsdate.Text)      '*/Tanggal Jatuh Tempo Baru /*'
			.EffectiveRate = ucEfRate.Text                  '*/Effective Rate Baru /*'
		End With

		'*/ GetListAmorFactoring untuk mendapatkan LIst Amortisasi Baru /*'
		oCustomClass = oController.GetListAmorFactoring(oCustomClass)
		DtUserList = oCustomClass.ListData
		dtg.DataSource = DtUserList.DefaultView
		dtg.DataBind()

		CallPaymentInfo()
		CalAmountToBePaid()
		txtInterestPaid.Text = FormatNumber(Me.TotInterest, 0)
		totAmountpaid.Text = 0
		lblPrepaidAmount.Text = 0
		BtnRequest.Visible = False
		BtnCalculate2.Visible = True
		oReq.ApprovalScheme = "FCDD"
		oReq.ReasonTypeID = "AGCDD"
	End Sub
#End Region

#Region "Calculate2"
	Private Sub BtnCalculate2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCalculate2.Click
		CalAmountToBePaid()
		totAmountpaid.Text = CStr(FormatNumber(Me.TotAmountTobePaid, 2))
		lblPrepaidAmount.Text = CStr(FormatNumber(Me.PrepaidBalance, 2))
		If totAmountpaid.Text = "" Then
			BtnRequest.Visible = False
		Else
			BtnRequest.Visible = True
		End If
	End Sub
#End Region

#Region "GetList"
	Sub GetList()
		oCustomClass.ApplicationID = Me.ApplicationID
		oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
		oCustomClass.InsSeqNo = Me.InsSeqNo
		oCustomClass.strConnection = GetConnectionString()
		oCustomClass = oController.GetListFactoring(oCustomClass)
		With oCustomClass
			Me.PaymentFrequency = CInt(.PaymentFrequency)
			Me.EffRate = .EffectiveRate
			Me.Bev = .ChangeDueDateBehaviour
			Me.PrepaidBalance = .Prepaid
			lblPrepaidAmount.Text = FormatNumber(Me.PrepaidBalance)
			If ConvertDate2(txtsdate.Text) = .NextInstallmentDueDate Then

				ShowMessage(lblMessage, "Harap isi jatuh tempo berikutnya yang baru!", True)
				Exit Sub
			ElseIf DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtsdate.Text)) < 0 Then

				ShowMessage(lblMessage, "Jatuh tempo berikutnya harus > tanggal hari ini!", True)
				Exit Sub
			End If
		End With
	End Sub
#End Region

#Region "CallPaymentInfo"
	Sub CallPaymentInfo()
		Dim tempdate As Date
		Dim totDiff As Integer
		Dim temp As Double
		Dim temp1 As Double

		'With oPaymentInfo
		'    .ValueDate = ConvertDate2(txtEfdate.Text)
		'    .ApplicationID = Me.ApplicationID
		'    .PaymentInfo()
		'End With

		oCustomClass.ApplicationID = Me.ApplicationID
		oCustomClass.strConnection = GetConnectionString()
		oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
		oCustomClass.EffectiveDate = Me.BusinessDate
		oCustomClass = oController.GetMaxDate(oCustomClass)
		tempdate = oCustomClass.DueDateMax
		totDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.DueDateMax, ConvertDate2(txtsdate.Text)))


		'temp = (oPaymentInfo.MaximumOutStandingPrincipal * (Me.EffRate / 36000) * totDiff)
		If temp > 0 Then
			temp1 = (Round((temp + 100) / 100)) * 100
			Me.TotInterest = temp1
		End If
		If totDiff <= 0 Then
			Me.TotInterest = 0
		End If
	End Sub
#End Region

#Region "CalAmountToBePaid"
	Sub CalAmountToBePaid()
		Me.TotAmountTobePaid = FormatNumber(CDbl(txtInterestPaid.Text), 0) + FormatNumber(CDbl(lbLCInstallx.Text), 0) + FormatNumber(CDbl(txtAdminFee.Text), 0)
		totAmountpaid.Text = FormatNumber(Me.TotAmountTobePaid, 2)
	End Sub
#End Region

#Region "Cancel"
	Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
		Server.Transfer("FACTDueDateRequest.aspx")
	End Sub
#End Region

#Region "Request"
	Private Sub BtnRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRequest.Click
		If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
			lblMessage.Text = ""
			GetList()
			CallPaymentInfo()
			If lblMessage.Text.Trim <> "" Then
				Exit Sub
			End If
			CalAmountToBePaid()
			With oCustomClass
                '==================Approval==================
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .InvoiceNo = Me.InvoiceNo
                .FacilityId = Me.FacilityId
                .BranchId = Me.sesBranchId.Replace("'", "")
				.BusinessDate = Me.BusinessDate
				.LoginId = Me.Loginid
				.RequestTo = oReq.ToBeApprove
				.ChangeNotes = oReq.Notes
				'==========================================
				'====================Due date================
				.EffectiveDate = ConvertDate2(txtEfdate.Text)
				.NextInstallmentDueDate = ConvertDate2(txtsdate.Text)
				.InterestAmount = CDbl(txtInterestPaid.Text)
				.AdminFee = CDbl(txtAdminFee.Text)
				.ReasonID = oReq.ReasonID
				.ReasonTypeID = oReq.ReasonTypeID

                '.Prepaid = Me.PrepaidBalance
                .TotAmountToBePaid = Me.TotAmountTobePaid
                .OutstandingPrincipal = lblOsPrinciple.Text
                '.OutstandingPrincipal = oPaymentInfo.MaximumOutStandingPrincipal
                '.OutStandingInterest = oPaymentInfo.MaximumOutStandingInterest
                '.InstallmentDue = oPaymentInfo.MaximumInstallmentDue
                '.InsuranceDue = oPaymentInfo.MaximumInsurance
                '.LcInstallment = oPaymentInfo.MaximumLCInstallFee
                '.LcInsurance = oPaymentInfo.MaximumLCInsuranceFee
                '.InsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee
                '.InstallmentCollFee = oPaymentInfo.MaximumInstallCollFee
                '.PDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
                '.STNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee
                '.InsuranceClaimExpense = oPaymentInfo.MaximumInsuranceClaimFee
                '===========================================

                '===========|Proses Save InstallmentSchedule Modal Kerja Perpanjangan|=============
                .RequestDate = ConvertDate2(txtsdate.Text)      '*/Tanggal Jatuh Tempo Baru /*'
                .EffectiveRate = ucEfRate.Text                  '*/Effective Rate Baru /*'
				.LoginId = Me.Loginid
                .InsSeqNo = Me.InsSeqNo
                .DueDateAwal = ConvertDate2(txtDueDateAwal.Text)
                '==================================================================================
                Try
					oController.CreateAmorFactoring(oCustomClass)
					oController.DueDateChangeRequestFactoring(oCustomClass)
					Server.Transfer("FACTDueDateRequest.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
				Catch exp As Exception

					ShowMessage(lblMessage, exp.Message, True)
				End Try
			End With
		End If
	End Sub
#End Region

End Class