﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FACTDExecProc.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.FACTDExecProc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DExecProc</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                GANTI TANGGAL JATUH TEMPO FACTORING -
                <asp:Label ID="lblJdl" runat="server"></asp:Label>
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Efektif
            </label>
            <asp:Label ID="lblEffDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Pengajuan
            </label>
            <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                TGL JT Berikutnya
            </label>
            <asp:Label ID="lblNextDueDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ANGSURAN JATUH TEMPO PER TANGGAL
                <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Angsuran Jatuh Tempo
            </label>
            <asp:Label ID="lblInstallmentDue" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Asuransi Jatuh Tempo
            </label>
            <asp:Label ID="lblInsuranceDue" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Denda Keterlambatan Angsuran
            </label>
            <asp:Label ID="lblLCInstall" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Denda keterlmabatan Asuransi
            </label>
            <asp:Label ID="lblLCInsurance" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Tagih Angsuran
            </label>
            <asp:Label ID="lblInstallColl" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Asuransi
            </label>
            <asp:Label ID="lblInsuranceColl" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Perpanjangan STNK/BBN
            </label>
            <asp:Label ID="lblSTNKFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Klaim Asuransi
            </label>
            <asp:Label ID="lblInsuranceClaim" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblRepossessionFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Jatuh Tempo
            </label>
            <asp:Label ID="lblTotalOSOverDue" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <asp:Panel ID="pnlAmor" Visible="True" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    AGREEEMENT INVOICE FACTORING BARU
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
<%--                    <asp:DataGrid ID="dtg" runat="server" Visible="true" ShowFooter="true" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSeqNo"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DUEDATE" HeaderText="DUE DATE">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ANGSURAN">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("InstallmentAmount"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotInstallmentAmount" runat="server" align="center"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("PrincipalAmount"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BUNGA">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%#FormatNumber(Container.DataItem("INTERESTAMOUNT"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOSPrincipal" runat="server" Text='<%#FormatNumber(Container.DataItem("OutstandingPrincipal"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA BUNGA">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOSInterest" runat="server" Text='<%#FormatNumber(Container.DataItem("OutstandingInterest"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSInterestAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>--%>
                   <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                        CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="SeqNo"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="InvoiceNo"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="TGL Invoice" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Margin" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="RetensiAmount" HeaderText="Retensi" DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Total Pokok"  DataFormatString="{0:0,0}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="JangkaWaktu" HeaderText="Jangka Waktu" DataFormatString="{0:0,0}"></asp:BoundColumn>   
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL GANTI TANGGAL JATUH TEMPO
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Margin yang harus dibayar
            </label>
            <asp:Label ID="lblInterestpaid" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Administrasi
            </label>
            <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Alasan
            </label>
            <asp:Label ID="lblReason" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Disetujui Oleh
            </label>
            <asp:Label ID="lblApproved" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <%--<div class="form_single">--%>
        <div class="form_left">
            <label>
                Catatan</label>
            <asp:Label ID="lblNotes" runat="server"></asp:Label>
        </div>
    </div>
    <%--<table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" align="center"
        border="0">
        <tr>
            <td class="tdgenap" align="right">
                Total yg harus dibayar:
                <asp:Label ID="totAmountpaid" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" align="right">
                Jumlah Prepaid:
                <asp:Label ID="lblPaidAmount" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" align="right">
                Selisih:
                <asp:Label ID="lblBal" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
    </table>--%>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>                
                Total yg harus dibayar:                
            </label>
            <asp:Label ID="totAmountpaid" runat="server" Font-Bold="True"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>                
                Jumlah Prepaid:                
            </label>
            <asp:Label ID="lblPaidAmount" runat="server" Font-Bold="True"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>                
                Selisih:                
            </label>
            <asp:Label ID="lblBal" runat="server" Font-Bold="True"></asp:Label>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnExec" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
