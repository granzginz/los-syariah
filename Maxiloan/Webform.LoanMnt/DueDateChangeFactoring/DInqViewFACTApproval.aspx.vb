﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DInqViewFACTApproval
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property


    Private Property TotAmountTobePaid() As Double
        Get
            Return (CType(ViewState("TotAmountTobePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotAmountTobePaid") = Value
        End Set
    End Property

    Private Property SeqNo() As Integer
        Get
            Return (CType(ViewState("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property

    Private Property InSeqNo() As String
        Get
            Return (CType(ViewState("InSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InSeqNo") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property

    Private Property TotInterest() As Double
        Get
            Return (CType(ViewState("TotInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInterest") = Value
        End Set
    End Property


    Private Property OSPrincipal() As Double
        Get
            Return (CType(ViewState("OSPrincipal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("OSPrincipal") = Value
        End Set
    End Property

    Private Property EffDate() As Date
        Get
            Return (CType(ViewState("EffDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("EffDate") = Value
        End Set
    End Property

    Private Property flagdel() As String
        Get
            Return (CType(ViewState("flagdel"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("flagdel") = Value
        End Set
    End Property
    Private Property InsSeqNo() As Integer
        Get
            Return (CType(ViewState("InsSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("InsSeqNo") = Value
        End Set
    End Property

    Private Property InvoiceNo() As String
        Get
            Return (CType(ViewState("InvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New DChangeController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private m_ControllerApp As New ApplicationController
#End Region

#Region "Declare Variable"
    Dim tempPrincipalAmount As Double
    Dim tempInterestAmount As Double
    Dim tempOSPrincipalAmount As Double
    Dim tempOSInterestAmount As Double
    Dim tempInstallmentAmount As Double
    Dim tempdate As Date
    Dim i As Integer
    Dim j As Integer = 0
    Dim TotInstallmentAmount As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipalAmount As Label
    Dim TotOSInterestAmount As Label
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "DINQVIEW"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                'Me.ApplicationID = Request.QueryString("ApplicationId")
                'Me.ApplicationID = IIf(Request.QueryString("ApplicationId") Is Nothing, "", Request.QueryString("ApplicationId"))
                'Me.BranchID = Request.QueryString("Branchid")
                'Me.SeqNo = CInt(Request.QueryString("SeqNo"))
                Me.InsSeqNo = Request.QueryString("InvoiceSeqNo")
                Me.InvoiceNo = Request.QueryString("InvoiceNo")

                oCustomClass.ApplicationID = Me.ApplicationID
                oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                'oCustomClass.InsSeqNo = Me.InsSeqNo
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass.InvoiceNo = Me.InvoiceNo
                oCustomClass.InvoiceSeqNo = Me.InsSeqNo
                oCustomClass.SeqNo = Me.SeqNo

                oCustomClass = oController.GetListFactoringViewApproval(oCustomClass)
                With oCustomClass
                    lblNexDueDate.Text = .NextDueDate.ToString("dd/MM/yyyy")
                    lblRequestDate.Text = .RequestDate.ToString("dd/MM/yyyy")
                    lblDueDateAwal.Text = .NextInstallmentDueDate.ToString("dd/MM/yyyy")
                    lblOsPrinciple.Text = FormatNumber(.OutstandingPrincipal, 2)
                    lblOsInterest.Text = FormatNumber(.OutStandingInterest, 2)
                    lblEffRate.Text = FormatNumber(.EffectiveRate, 2)
                    Me.ApplicationID = .ApplicationID.Trim
                    Me.InsSeqNo = .InvoiceSeqNo
                    Me.BranchID = .BranchId
                    Me.SeqNo = .SeqNo

                End With
                pnlAmor.Visible = True

                DoBind()
                CallPaymentInfo()
            End If
        End If
    End Sub
#End Region


#Region "CallPaymentInfo"
    Sub CallPaymentInfo()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .ApplicationID = Me.ApplicationID
            .InsSeqNo = Me.InsSeqNo
            .strConnection = GetConnectionString()
            .RequestDate = ConvertDate2(lblNexDueDate.Text)
            .EffectiveRate = lblEffRate.Text
        End With
        oCustomClass = oController.GetListAmorFactoring(oCustomClass)
        DtUserList = oCustomClass.ListData
        dtg.DataSource = DtUserList.DefaultView
        dtg.DataBind()

        'Dim tempdate As Date
        'Dim totDiff As Integer
        'Dim temp As Double
        'Dim temp1 As Double

        ''With oPaymentInfo
        ''    .ValueDate = ConvertDate2(txtEfdate.Text)
        ''    .ApplicationID = Me.ApplicationID
        ''    .PaymentInfo()
        ''End With

        'oCustomClass.ApplicationID = Me.ApplicationID
        'oCustomClass.strConnection = GetConnectionString()
        'oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        'oCustomClass.EffectiveDate = Me.BusinessDate
        'oCustomClass = oController.GetMaxDate(oCustomClass)
        'tempdate = oCustomClass.DueDateMax
        'totDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.DueDateMax, ConvertDate2(lblNexDueDate.Text)))

        'If temp > 0 Then
        '    temp1 = (Round((temp + 100) / 100)) * 100
        '    Me.TotInterest = temp1
        'End If
        'If totDiff <= 0 Then
        '    Me.TotInterest = 0
        'End If
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString()
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        'Dim oclass As New Parameter.FactoringInvoice
        'With oclass
        '    .strConnection = GetConnectionString()
        '    .ApplicationID = Me.ApplicationID
        '    .BranchId = Me.BranchID
        'End With

        'With oCustomClass
        '    .strConnection = GetConnectionString()
        '    .ApplicationID = Me.ApplicationID
        '    .ValueDate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
        '    .BranchId = Me.BranchID
        'End With

        'Dim oApplication As New Parameter.Application
        'Dim oData As New DataTable

        'oApplication.strConnection = GetConnectionString()
        'oApplication.BranchId = Me.BranchID
        'oApplication.AppID = Me.ApplicationID
        'oApplication.ApplicationID = Me.ApplicationID
        'oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        'If Not oApplication Is Nothing Then
        '    oData = oApplication.ListData
        'End If
        'Dim oRow As DataRow = oData.Rows(0)
        'Me.FacilityNo = oRow("NoFasilitas").ToString

        'If (oRow("ContractStatus").ToString = "PRP") Then
        'If (oRow("ContractStatus").ToString = "AKT") Then
        '    oFactoringInvoice.strConnection = GetConnectionString()
        '    oFactoringInvoice.BranchId = Me.BranchID
        '    oFactoringInvoice.ApplicationID = Me.ApplicationID
        'End If

        Dim oControllerD As New DChangeController
        Dim oCustomClassD As New Parameter.DChange

        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClassD
            .ApplicationID = Me.ApplicationID
            .InsSeqNo = Me.InsSeqNo
            .strConnection = GetConnectionString()
        End With
        oCustomClassD = oControllerD.GetListInvoiceFactoring(oCustomClassD)
        DtUserList = oCustomClassD.ListData
        dtgInvoice.DataSource = DtUserList.DefaultView
        dtgInvoice.DataBind()

        If DtUserList.Rows(0)("InvoiceStatus").ToString() = "LNS" Then
            lblMessage.Visible = True
            ShowMessage(lblMessage, "Invoice Sudah LUNAS", True)
        End If

        '===========================================================================================================================================
        oCustomClassD.ApplicationID = Me.ApplicationID
        oCustomClassD.strConnection = GetConnectionString()
        oCustomClassD.BranchId = Me.BranchID
        oCustomClassD.SeqNo = SeqNo
        oCustomClassD.InvoiceNo = Me.InvoiceNo
        oCustomClassD = oControllerD.GetListExecFactoringApproval(oCustomClassD)
        If Not oCustomClassD.CustomerID Is Nothing Then

            With oCustomClassD
                lblAgreementNo.Text = .Agreementno
                Me.strCustomerid = .CustomerID
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                lblCustName.Text = .CustomerName
                lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
                'lblEffDate.Text = .EffectiveDate.ToString("dd/MM/yyyy")
                Me.EffDate = .EffectiveDate
                lblRequestDate.Text = .RequestDate.ToString("dd/MM/yyyy")
                'lblInterestpaid.Text = FormatNumber(.InterestAmount, 2)
                lblReason.Text = .ReasonDescription
                lblAdminFee.Text = FormatNumber(.AdminFee, 2)
                lblNotes.Text = .ChangeNotes
                lblApproved.Text = .ApprovedBy
                totAmountpaid.Text = FormatNumber(.TotAmountToBePaid, 2)
                lblPaidAmount.Text = FormatNumber(.Prepaid, 2)
                lblBal.Text = FormatNumber((.TotAmountToBePaid - .Prepaid), 2)
                'lbljudul.Text = .EffectiveDate.ToString("dd/MM/yyyy")
                Me.PaymentFrequency = CInt(oCustomClassD.PaymentFrequency)
                lblJudulHeader.Text = .statusdesc.ToUpper.Trim
                lblNextDueDate.Value = .NextInstallmentDueDate.ToString("dd/MM/yyyy")

                '================Payment Info===================================
                'lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                'lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                'lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                'lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                'lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)
                'lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)
                'lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                'lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
                'lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
                'lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                'lblTotalOSOverDue.Text = FormatNumber(.InstallmentDue + .LcInstallment + .InstallmentCollFee +
                '                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                '                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                '                        .RepossessionFee, 2)
                '===============================================================


            End With

            '    Dim DtUserList As New DataTable
            '    Dim DvUserList As New DataView
            '    With oCustomClass
            '        .ApplicationID = Me.ApplicationID
            '        .strConnection = GetConnectionString()
            '        .BusinessDate = oCustomClass.EffectiveDate
            '    End With
            '    oCustomClass = oController.GetListAmorFACT(oCustomClass)
            '    DtUserList = oCustomClass.ListData
            '    dtg.DataSource = DtUserList.DefaultView
            '    dtg.DataBind()

        End If
    End Sub
#End Region
    Dim dateBefore As Date
    '#Region "Databound"
    '    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
    '        Dim lb As New Label
    '        If e.Item.ItemIndex >= 0 Then
    '            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
    '            lb = CType(e.Item.FindControl("lblInstallmentAmount"), Label)
    '            tempInstallmentAmount = tempInstallmentAmount + CDbl(lb.Text)

    '            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
    '            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

    '            lb = CType(e.Item.FindControl("lblINTERESTAMOUNT"), Label)
    '            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)
    '            Me.TotInterest = tempInterestAmount

    '            lb = CType(e.Item.FindControl("lblOSPrincipal"), Label)
    '            tempOSPrincipalAmount = tempOSPrincipalAmount + CDbl(lb.Text)
    '            Me.OSPrincipal = tempOSPrincipalAmount

    '            lb = CType(e.Item.FindControl("lblOSInterest"), Label)
    '            tempOSInterestAmount = tempOSInterestAmount + CDbl(lb.Text)

    '            'If DateDiff(DateInterval.Day, oCustomClass.EffectiveDate, ConvertDate2(e.Item.Cells(2).Text)) >= 0 Then
    '            '    j = j + 1
    '            '    If j = 1 Then
    '            '        tempdate = oCustomClass.EffectiveDate
    '            '        Me.DaysDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.EffectiveDate, ConvertDate2(e.Item.Cells(2).Text)))
    '            '        Me.InSeqNo = e.Item.Cells(1).Text
    '            '    Else
    '            '        tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), tempdate)
    '            '    End If
    '            '    e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
    '            '    e.Item.Cells(2).Font.Bold = True
    '            'Else
    '            '    e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
    '            'End If

    '            If DateDiff(DateInterval.Day, oCustomClass.EffectiveDate, ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
    '                j = j + 1
    '                If j = 1 Then
    '                    tempdate = oCustomClass.NextInstallmentDueDate
    '                    Me.DaysDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.NextInstallmentDueDate, ConvertDate2(e.Item.Cells(2).Text)))
    '                    If Me.DaysDiff < 0 Then
    '                        Me.DaysDiff = 0
    '                    End If
    '                Else
    '                    'ngakalin bulan februari ditanggal 28 dan 29
    '                    tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), tempdate)
    '                    If Month(tempdate) = 3 And Day(dateBefore) > 28 Then
    '                        tempdate = DateAdd(DateInterval.Day, CDbl(Day(dateBefore) - Day(tempdate)), tempdate)
    '                    End If
    '                    If Month(tempdate) = 1 Then ' get original date
    '                        dateBefore = tempdate
    '                    End If
    '                End If
    '                e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
    '                e.Item.Cells(2).Font.Bold = True
    '            Else
    '                e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
    '            End If


    '        End If
    '        If e.Item.ItemType = ListItemType.Footer Then
    '            TotInstallmentAmount = CType(e.Item.FindControl("lblTotInstallmentAmount"), Label)
    '            TotInstallmentAmount.Text = FormatNumber(tempInstallmentAmount, 2).ToString

    '            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
    '            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2).ToString

    '            TotInterestAmount = CType(e.Item.FindControl("lblTotINTERESTAMOUNT"), Label)
    '            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2).ToString

    '            TotOSPrincipalAmount = CType(e.Item.FindControl("lblTotOSPrincipalAmount"), Label)
    '            TotOSPrincipalAmount.Text = FormatNumber(tempOSPrincipalAmount, 2).ToString

    '            TotOSInterestAmount = CType(e.Item.FindControl("lblTotOSInterestAmount"), Label)
    '            TotOSInterestAmount.Text = FormatNumber(tempOSInterestAmount, 2).ToString

    '        End If
    '    End Sub
    '#End Region



    'Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnClose.Click
    '    Response.Redirect("ApprovalList.aspx")
    'End Sub

End Class