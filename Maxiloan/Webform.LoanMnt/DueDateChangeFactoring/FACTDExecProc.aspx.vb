﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FACTDExecProc
	Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
	Private Property strCustomerid() As String
		Get
			Return (CType(ViewState("strCustomerid"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("strCustomerid") = Value
		End Set
	End Property


	Private Property TotAmountTobePaid() As Double
		Get
			Return (CType(ViewState("TotAmountTobePaid"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("TotAmountTobePaid") = Value
		End Set
	End Property

	Private Property SeqNo() As Integer
		Get
			Return (CType(ViewState("SeqNo"), Integer))
		End Get
		Set(ByVal Value As Integer)
			ViewState("SeqNo") = Value
		End Set
	End Property

	Private Property InvoiceSeqNo() As Integer
		Get
			Return (CType(ViewState("InvoiceSeqNo"), Integer))
		End Get
		Set(ByVal Value As Integer)
			ViewState("InvoiceSeqNo") = Value
		End Set
	End Property

	Private Property InSeqNo() As String
		Get
			Return (CType(ViewState("InSeqNo"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("InSeqNo") = Value
		End Set
	End Property
	Private Property PaymentFrequency() As Integer
		Get
			Return (CType(ViewState("PaymentFrequency"), Integer))
		End Get
		Set(ByVal Value As Integer)
			ViewState("PaymentFrequency") = Value
		End Set
	End Property
	Private Property DaysDiff() As Integer
		Get
			Return (CType(ViewState("DaysDiff"), Integer))
		End Get
		Set(ByVal Value As Integer)
			ViewState("DaysDiff") = Value
		End Set
	End Property

	Private Property TotInterest() As Double
		Get
			Return (CType(ViewState("TotInterest"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("TotInterest") = Value
		End Set
	End Property


	Private Property OSPrincipal() As Double
		Get
			Return (CType(ViewState("OSPrincipal"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("OSPrincipal") = Value
		End Set
	End Property

	Private Property EffDate() As Date
		Get
			Return (CType(ViewState("EffDate"), Date))
		End Get
		Set(ByVal Value As Date)
			ViewState("EffDate") = Value
		End Set
	End Property

	Private Property flagdel() As String
		Get
			Return (CType(ViewState("flagdel"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("flagdel") = Value
		End Set
	End Property

    Private Property VTempDate() As Date
        Get
            Return (CType(ViewState("VTempDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("VTempDate") = Value
        End Set
    End Property

    Private Property InvoiceNo() As String
        Get
            Return (CType(ViewState("InvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
	Private oController As New DChangeController
	Private oControllerInfo As New UCPaymentInfoController
	Private oCustomClassInfo As New Parameter.AccMntBase
#End Region

#Region "Declare Variable"
	Dim tempPrincipalAmount As Double
	Dim tempInterestAmount As Double
	Dim tempOSPrincipalAmount As Double
	Dim tempOSInterestAmount As Double
	Dim tempInstallmentAmount As Double
	Dim tempdate As Date
	Dim dateBefore As Date
	Dim i As Integer
	Dim j As Integer = 0
	Dim TotInstallmentAmount As Label
	Dim TotPrincipalAmount As Label
	Dim TotInterestAmount As Label
	Dim TotOSPrincipalAmount As Label
	Dim TotOSInterestAmount As Label
	Dim intcounter As Integer = 1
#End Region

#Region "PageLoad"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		If SessionInvalid() Then
			Exit Sub
		End If
		Me.FormID = "FACTDUEDATEEXEC"
		If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
			If Not IsPostBack Then
				Me.ApplicationID = Request.QueryString("ApplicationId")
				Me.BranchID = Request.QueryString("Branchid")
				Me.SeqNo = CInt(Request.QueryString("SeqNo"))
				Me.flagdel = Request.QueryString("FlagDel")
                Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                DoBind()
			End If
		End If
	End Sub
#End Region

#Region "Dobind"
	Sub DoBind()
		oCustomClass.ApplicationID = Me.ApplicationID
		oCustomClass.strConnection = GetConnectionString()
		oCustomClass.BranchId = Me.BranchID
		oCustomClass.SeqNo = Me.SeqNo
        oCustomClass.InsSeqNo = Me.InvoiceSeqNo
        oCustomClass.InvoiceNo = Me.InvoiceNo
        oCustomClass = oController.GetListExecFactoring(oCustomClass)
		With oCustomClass
			lblAgreementNo.Text = .Agreementno
			Me.strCustomerid = .CustomerID
			lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
			lblCustName.Text = .CustomerName
			lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
			lblEffDate.Text = .EffectiveDate.ToString("dd/MM/yyyy")
			Me.EffDate = .EffectiveDate
			lblRequestDate.Text = .RequestDate.ToString("dd/MM/yyyy")
			lblInterestpaid.Text = FormatNumber(.InterestAmount, 2)
			lblReason.Text = .ReasonDescription
			lblAdminFee.Text = FormatNumber(.AdminFee, 2)
			lblNotes.Text = .ChangeNotes
			lblApproved.Text = .ApprovedBy
			totAmountpaid.Text = FormatNumber(.TotAmountToBePaid, 2)
			lblPaidAmount.Text = FormatNumber(.Prepaid, 2)
			lblBal.Text = FormatNumber((.TotAmountToBePaid - .Prepaid), 2)
			lbljudul.Text = .EffectiveDate.ToString("dd/MM/yyyy")
			Me.PaymentFrequency = CInt(oCustomClass.PaymentFrequency)
			lblNextDueDate.Text = .NextInstallmentDueDate.ToString("dd/MM/yyyy")

			If Me.flagdel = "2" Then
				If .Prepaid < .TotAmountToBePaid Then
					lblJdl.Text = "EXECUTION"
					ShowMessage(lblMessage, "Jumlah Prepaid harus >= Total yang harus dibayar", True)
					BtnExec.Visible = False
					Exit Sub
				End If
			End If

			'================Payment Info===================================
			lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
			lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
			lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
			lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
			lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)
			lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)
			lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
			lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
			lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
			lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
			lblTotalOSOverDue.Text = FormatNumber(.InstallmentDue + .LcInstallment + .InstallmentCollFee +
									.InsuranceDue + .InsuranceCollFee + .LcInsurance +
									.PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
									.RepossessionFee, 2)
			'===============================================================

			If Me.flagdel = "1" Then
				lblJdl.Text = "CANCELLATION"
			Else
				lblJdl.Text = "EXECUTION"
			End If
		End With

		'TO GET NEW FACTORING FROM TABLE INSTALLMENTSCHEDULEFACTORINGDUEDATECHANGE
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView
		With oCustomClass
			.ApplicationID = Me.ApplicationID
			.strConnection = GetConnectionString()
			.SeqNo = Me.SeqNo
            .InsSeqNo = Me.InvoiceSeqNo
            .InvoiceNo = Me.InvoiceNo
        End With
		oCustomClass = oController.GetListAmorFactoringFromTable(oCustomClass)
		DtUserList = oCustomClass.ListData
		dtg.DataSource = DtUserList.DefaultView
		dtg.DataBind()
		If DtUserList Is Nothing Then
			ShowMessage(lblMessage, "Agreement Factoring Invoice Baru Tidak Terbentuk", True)
			BtnExec.Enabled = False
			Exit Sub
		End If
	End Sub
#End Region

#Region "Cancel"
	Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
		Server.Transfer("FACTDExecute.aspx")
	End Sub
#End Region

#Region "Execute"
	Private Sub BtnExec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExec.Click
		If CheckFeature(Me.Loginid, Me.FormID, "EXEC", Me.AppId) Then

			With oCustomClass
				.strConnection = GetConnectionString()
				.BranchId = Me.BranchID
				.ApplicationID = Me.ApplicationID
				.BusinessDate = Me.BusinessDate
				.EffectiveDate = ConvertDate2(lblEffDate.Text)
				.NextInstallmentDueDate = ConvertDate2(lblNextDueDate.Text)
				.SeqNo = Me.SeqNo
				.CoyID = Me.SesCompanyID
				.flagDel = Me.flagdel
                .InsSeqNo = Me.InvoiceSeqNo
                .InvoiceNo = Me.InvoiceNo
            End With
			Try
				oController.DueDateChangeExecFactoring(oCustomClass)
				Server.Transfer("FACTDExecute.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
			Catch exp As Exception

				ShowMessage(lblMessage, exp.Message, True)
			End Try
		End If

		'End If
	End Sub

#End Region

End Class