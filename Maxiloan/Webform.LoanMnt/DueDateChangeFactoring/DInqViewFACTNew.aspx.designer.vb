﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class DInqViewFACTNew
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAgreementNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAgreementNo As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblCustName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustName As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblDueDateAwal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDueDateAwal As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblsdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsdate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblEfdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEfdate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtInterestPaid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInterestPaid As Global.System.Web.UI.UserControl
    
    '''<summary>
    '''pnlPaymentInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPaymentInfo As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblOsPrinciple control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOsPrinciple As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOsInterest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOsInterest As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbLCInstallx control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbLCInstallx As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOsInterestNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOsInterestNew As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''hdfApplicationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdfApplicationID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''dtgInvoice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgInvoice As Global.System.Web.UI.WebControls.DataGrid
    
    '''<summary>
    '''pnlAmor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAmor As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''dtg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtg As Global.System.Web.UI.WebControls.DataGrid
    
    '''<summary>
    '''totAmountpaid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents totAmountpaid As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPrepaidAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrepaidAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReason As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblApproved control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblApproved As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNotes As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPaidAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPaidAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblBal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBal As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''BtnCalculate2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnCalculate2 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''BtnRequest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnRequest As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''BtnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnCancel As Global.System.Web.UI.WebControls.Button
End Class
