﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports System.Web.Script.Serialization
#End Region

Public Class ucLookupTransMnt
    Inherits Maxiloan.Webform.WebBased
    Private oController As New LookUpZipCodeController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private ZipCodeDS As DataTable

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Public Property vsPay() As String
        Get
            Return CType(ViewState("vsPay"), String)
        End Get
        Set(ByVal vsPay As String)
            ViewState("vsPay") = vsPay
        End Set
    End Property
    Public Property vsDesc() As String
        Get
            Return CType(ViewState("vsDesc"), String)
        End Get
        Set(ByVal vsDesc As String)
            ViewState("vsDesc") = vsDesc
        End Set
    End Property
    Public Property vsCoa() As String
        Get
            Return CType(ViewState("vsCoa"), String)
        End Get
        Set(ByVal vsCoa As String)
            ViewState("vsCoa") = vsCoa
        End Set
    End Property
    Public Property isPettyCash As String
        Get
            Return CType(ViewState("isPettyCash"), String)
        End Get
        Set(value As String)
            ViewState("isPettyCash") = isPettyCash
        End Set
    End Property
    Public Property vsJumlah() As String
        Get
            Return CType(ViewState("vsJumlah"), String)
        End Get
        Set(ByVal vsJumlah As String)
            ViewState("vsJumlah") = vsJumlah
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property PaymentAllocationID() As String
        Get
            Return CType(ViewState("PaymentAllocationID"), String)
        End Get
        Set(ByVal PaymentAllocationID As String)
            ViewState("PaymentAllocationID") = PaymentAllocationID
        End Set
    End Property
    Private Property COA() As String
        Get
            Return CType(ViewState("COA"), String)
        End Get
        Set(ByVal COA As String)
            ViewState("COA") = COA
        End Set
    End Property

    Public Property isPaymentRequest() As String
        Get
            Return CType(ViewState("isPaymentRequest"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("isPaymentRequest") = Value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtGoPage.Text = "1"
        If Not Page.IsPostBack Then

            Me.vsPay = Request("vspay")            
            Me.vsJumlah = Request("vsjumlah")
            Me.vsDesc = Request("vsdesc")
            Me.isPettyCash = Request("isPettyCash")
            Me.isPaymentRequest = Request("isPaymentRequest")

            Me.SortBy = " PaymentAllocationID ASC"
            'Me.CmdWhere = " IsAgreement = '0' AND isSystem = '0'  " ' And IsHOTransaction = '0' And IsPaymentReceive = '0' "
            'If isPettyCash = "1" Then
            '    Me.CmdWhere = Me.CmdWhere & " AND isPettyCash = '1' "
            'Else
            '    Me.CmdWhere = Me.CmdWhere & " AND isPaymentRequest = '1' "
            'End If
            If isPaymentRequest = "1" Then
                Me.CmdWhere = " IsAgreement = '0' AND isSystem = '0' AND isPaymentRequest = '1' "
            Else
                Me.CmdWhere = " IsAgreement = '0' AND isSystem = '0'  "
            End If
            BindGridEntity(Me.CmdWhere)

        End If
    End Sub
    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If


        BindGridEntity(Me.CmdWhere)
    End Sub
    Protected Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustom As New Parameter.LookUpTransaction
        Dim oCtr As New Controller.LookUpTransactionController


        Dim DataList As New List(Of Parameter.MasterTransaction)
        Dim custom As New Parameter.MasterTransaction
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.MasterTransaction

                custom.PaymentAllocationID = TempDataTable.Rows(index).Item("PaymentAllocationID").ToString.Trim
                custom.COA = TempDataTable.Rows(index).Item("COA").ToString.Trim
                custom.Jumlah = TempDataTable.Rows(index).Item("Jumlah").ToString.Trim
                custom.Description = TempDataTable.Rows(index).Item("Description").ToString.Trim

                DataList.Add(custom)
            Next
        End If

        oCustom.strConnection = GetConnectionString()
        oCustom = oCtr.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustom Is Nothing Then
            dtEntity = oCustom.ListData
            recordCount = oCustom.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

        If dtEntity.Rows.Count > 0 Then            
            For intLoopGrid = 0 To dtgPaging.Items.Count - 1
                Dim chek As CheckBox
                Dim Jumlah As New TextBox
                chek = CType(dtgPaging.Items(intLoopGrid).FindControl("chkSelect"), CheckBox)
                Dim PaymentAllocationID As String = dtgPaging.Items(intLoopGrid).Cells(1).Text.Trim
                Dim COA As String = dtgPaging.Items(intLoopGrid).Cells(3).Text.Trim
                Jumlah = CType(dtgPaging.Items(intLoopGrid).FindControl("txtJumlah"), TextBox)

                Me.PaymentAllocationID = PaymentAllocationID
                Me.COA = COA

                Dim query As New Parameter.MasterTransaction
                If DataList.Count > 0 Then
                    query = DataList.Find(AddressOf PredicateFunction)
                Else
                    query = Nothing
                End If

                If Not query Is Nothing Then
                    chek.Checked = True
                    Jumlah.Text = query.Jumlah
                End If
            Next
        End If

    End Sub
    Public Function PredicateFunction(ByVal custom As Parameter.MasterTransaction) As Boolean
        Return custom.PaymentAllocationID.Trim = Me.PaymentAllocationID.Trim And custom.COA.Trim = Me.COA.Trim
    End Function
    Sub BindData()
        Dim DataList As New List(Of Parameter.MasterTransaction)
        Dim custom As New Parameter.MasterTransaction

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
                .Columns.Add(New DataColumn("COA", GetType(String)))
                .Columns.Add(New DataColumn("Jumlah", GetType(String)))
                .Columns.Add(New DataColumn("Description", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.MasterTransaction

                custom.PaymentAllocationID = TempDataTable.Rows(index).Item("PaymentAllocationID").ToString.Trim
                custom.COA = TempDataTable.Rows(index).Item("COA").ToString.Trim
                custom.Jumlah = TempDataTable.Rows(index).Item("Jumlah").ToString.Trim
                custom.Description = TempDataTable.Rows(index).Item("Description").ToString.Trim

                DataList.Add(custom)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgPaging.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPaging.Items(intLoopGrid).FindControl("chkSelect"), CheckBox)
            Dim PaymentAllocationID As String = dtgPaging.Items(intLoopGrid).Cells(1).Text.Trim
            Dim COA As String = dtgPaging.Items(intLoopGrid).Cells(3).Text.Trim
            Dim Description As String = dtgPaging.Items(intLoopGrid).Cells(2).Text.Trim
            Dim Jumlah As String = CType(dtgPaging.Items(intLoopGrid).FindControl("txtJumlah"), TextBox).Text.Trim


            Me.PaymentAllocationID = PaymentAllocationID
            Me.COA = COA

            Dim query As New Parameter.MasterTransaction
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("PaymentAllocationID") = PaymentAllocationID
                oRow("COA") = COA
                oRow("Jumlah") = Jumlah
                oRow("Description") = Description
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClear()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("COA", GetType(String)))
            .Columns.Add(New DataColumn("Jumlah", GetType(String)))
            .Columns.Add(New DataColumn("Description", GetType(String)))
        End With
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindData()
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindData()
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then

            Dim chkSelect As CheckBox
            chkSelect = CType(e.Item.FindControl("chkSelect"), CheckBox)

        End If
    End Sub
    Private Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        BindData()

        If TempDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap periksa item", True)
            Exit Sub
        End If

        Dim arr As New List(Of ArrayOut)
        Dim resultPay As String = ""
        Dim resultJumlah As String = ""
        Dim resultDesc As String = ""

        For index = 0 To TempDataTable.Rows.Count - 1
            If resultPay.Trim = "" Then
                resultPay = resultPay + "'" + TempDataTable.Rows(index).Item("PaymentAllocationID").ToString.Trim + "'"
            Else
                resultPay = resultPay + ",'" + TempDataTable.Rows(index).Item("PaymentAllocationID").ToString.Trim + "'"
            End If

            If resultJumlah.Trim = "" Then
                resultJumlah = resultJumlah + "'" + IIf(TempDataTable.Rows(index).Item("Jumlah").ToString.Trim = "", "0", TempDataTable.Rows(index).Item("Jumlah").ToString.Trim) + "'"
            Else
                resultJumlah = resultJumlah + ",'" + IIf(TempDataTable.Rows(index).Item("Jumlah").ToString.Trim = "", "0", TempDataTable.Rows(index).Item("Jumlah").ToString.Trim) + "'"
            End If

            If resultDesc.Trim = "" Then
                resultDesc = resultDesc + "'" + TempDataTable.Rows(index).Item("Description").ToString.Trim + "'"
            Else
                resultDesc = resultDesc + ",'" + TempDataTable.Rows(index).Item("Description").ToString.Trim + "'"
            End If
        Next

        Dim ar As New ArrayOut
        ar.name = Me.vsPay
        ar.value = resultPay
        arr.Add(ar)
        ar = New ArrayOut
        ar.name = Me.vsJumlah
        ar.value = resultJumlah
        arr.Add(ar)
        ar = New ArrayOut
        ar.name = Me.vsDesc
        ar.value = resultDesc
        arr.Add(ar)

        Dim serializer As New JavaScriptSerializer()
        Dim serializedResult = serializer.Serialize(arr)

        ScriptManager.RegisterStartupScript(Page, GetType(Page), Page.ClientID, " window.parent.BindFromJLookup(" + serializedResult + "); window.parent.CloseJLookup();", True)
    End Sub    

    <Serializable()> _
    Public Class ArrayOut
        Public Property name As String
        Public Property value As String
    End Class

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        BindClear()
        Me.CmdWhere = " IsAgreement = '0' and isSystem = '0'   " 'And IsHOTransaction = '0' And IsPaymentReceive = '0' "
        'If isPettyCash = "1" Then
        '    Me.CmdWhere = Me.CmdWhere & " AND isPettyCash = '1' "
        'Else
        '    Me.CmdWhere = Me.CmdWhere & " AND isPaymentRequest = '1' "
        'End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click        
        BindData()
        Me.CmdWhere = " IsAgreement = '0' AND isSystem = '0'   " ' And IsHOTransaction = '0' And IsPaymentReceive = '0' "
        'If isPettyCash = "1" Then
        '    Me.CmdWhere = Me.CmdWhere & " AND isPettyCash = '1' "
        'Else
        '    Me.CmdWhere = Me.CmdWhere & " AND isPaymentRequest = '1' "
        'End If
        If txtSearchBy.Text <> "" Then
            Me.CmdWhere = Me.CmdWhere & " And " & cboSearchBy.SelectedItem.Value + " like '%" + txtSearchBy.Text.Replace("%,", "").Trim + "%'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
End Class