﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ucLookupTransMnt.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ucLookupTransMnt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">

//        function SelectRehabList() {
//            var _vspay, _vsjumlah;

//            _vspay = '';
//            _vsjumlah = 0;

//            $('#dtgPaging').find('tbody>tr')
//                .each(function (index) {
//                    if (index > 0) {
//                        var chk = $(this).find("td").find('input[type="checkbox"]');
//                        var jumlah = $(this).parents('tr').find('.jumlah').html();

//                        if (chk.is(":checked") && !chk.is(":disabled")) {
//                            _vspay += $(this).find("td").eq(1).html().trim() + ';';
//                            _vsdesc += $(this).find("td").eq(2).html().trim() + ';';
//                            _vscoa += $(this).find("td").eq(3).html().trim() + ';';
//                            _vsjumlah += jumlah + ';';
//                        }
//                    }
//                });

//            if (_vscoa != '') {
//                _vspay = _vspay.substr(0, _vspay.length - 1)
//                _vsjumlah = _vsjumlah.substr(0, _vsjumlah.length - 1)

//                var dataArg_Temp = [
//                    { name: '<%= vsPay %>', value: _vspay },
//                    { name: '<%= vsJumlah %>', value: _vsjumlah }
//                    ];

//                var dataArg = new Array();

//                $.each(dataArg_Temp, function (index, value) {
//                    dataArg.push({ name: value.name, value: value.value });
//                });

//                window.parent.BindFromJLookup(dataArg);
//                window.parent.CloseJLookup();
//            }
//        }

        function selectAllCheckbox(val) {
            $('#<%= dtgPaging.ClientID %> input:checkbox').prop('checked', $(val).is(':checked'));
        }     
    </script>
</head>
<body>
    <form id="form1" runat="server" class="ui-lookup">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_box_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                        BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                        DataKeyField="PaymentAllocationID" AutoGenerateColumns="False" AllowSorting="True"
                        Width="100%" Visible="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="SELECT">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:selectAllCheckbox(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkSelect" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PaymentAllocationID" HeaderText="PAYMENT ALLOCATION ID"
                                SortExpression="PaymentAllocationID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="DESCRIPTION" SortExpression="Description">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="COA" HeaderText="COA" SortExpression="COA">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="BARIS">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtJumlah" CssClass="jumlah smaller_text" MaxLength="2"
                                        onblur="this.value=blankToZero(this.value);" onkeypress="return CustomAngka(event);"
                                        onfocus="this.value=resetNumber(this.value);">1</asp:TextBox>
                                    <asp:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtJumlah"
                                        Width="50" RefValues="" TargetButtonDownID="" TargetButtonUpID="" Maximum="100"
                                        Minimum="1">
                                    </asp:NumericUpDownExtender>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                    </asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSelect" runat="server" CausesValidation="False" Text="Select"
                CssClass="small button blue"></asp:Button>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_auto">
                    Find By</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Description">Description</asp:ListItem>
                    <asp:ListItem Value="COA">COA</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Search"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
