﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RefundView.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.RefundView" %>

<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RefundView</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">           
                <h3>
                    VIEW - REFUND KONSUMEN
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        
                <div class="form_left">
                    <label>No Request</label>
                    <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Tanggal Request</label>	
                    <asp:Label ID="lblRequestDate" runat="server"></asp:Label>		
		        </div>
	        
        </div>
        <div class="form_box">
	     
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="HyAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">		
                    <label>Nama Customer</label>
                    <asp:HyperLink ID="HyCustomerName" runat="server"></asp:HyperLink>	
		        </div>
	   
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        
                <div class="form_left">
                    <label>Angsuran Jatuh Tempo</label>
                    <asp:Label ID="lblInstallmentDue" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Premi Asuransi Jatuh Tempo</label>
                    <asp:Label ID="lblInsuranceDue" runat="server"></asp:Label>	
		        </div>
	      
        </div>
        <div class="form_box">
	        
                <div class="form_left">
                    <label>Denda Keterlambatan Angsuran</label>
                    <asp:Label ID="lblLCInstall" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Denda keterlambatan Asuransi</label>
                    <asp:Label ID="lblLCInsurance" runat="server"></asp:Label>
		        </div>
	    
        </div>        
        <div class="form_box">
	       
                <div class="form_left">
                    <label>Biaya Tagih Angsuran</label>
                    <asp:Label ID="lblInstallColl" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Biaya Tagih Asuransi</label>
                    <asp:Label ID="lblInsuranceColl" runat="server"></asp:Label>
		        </div>
	      
        </div> 
        <div class="form_box">
	        
                <div class="form_left">
                    <label>Biaya Tolakan PDC</label>
                    <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Biaya Perpanjangan STNK/BBN</label>
                    <asp:Label ID="lblSTNKFee" runat="server"></asp:Label>
		        </div>
	     
        </div> 
        <div class="form_box">
	    
                <div class="form_left">
                    <label>Biaya Klaim Asuransi</label>
                    <asp:Label ID="lblInsuranceClaim" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Biaya Tarik</label>	
                    <asp:Label ID="lblRepossessionFee" runat="server"></asp:Label>
		        </div>
	    
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Total Jatuh tempo</label>
                <asp:Label ID="lblTotalOSOverDue" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    DETAIL REFUND KE CUSTOMER
                </h4>
            </div>
        </div>
        <div class="form_box">
	   
                <div class="form_left">
                    <label>Prepaid yg Dapat DiRefund</label>
                    <asp:Label ID="lblprepaidRefund" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Refund</label>	
                    <asp:Label ID="lblRefundAmount" runat="server"></asp:Label>
		        </div>
	      
        </div>     
        <div class="form_box">
	       
                <div class="form_left">
                    <label>Nama Bank</label>
                    <asp:Label ID="lblBankName" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Cabang Bank</label>	
                    <asp:Label ID="lblBankBranch" runat="server"></asp:Label>
		        </div>
	      
        </div>  
        <div class="form_box">
	      
                <div class="form_left">
                    <label>Nama Rekening</label>
                    <asp:Label ID="lblAccountName" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>No Rekening</label>		
                    <asp:Label ID="lblAccountNomor" runat="server"></asp:Label>
		        </div>
	     
        </div>  
        <div class="form_box">
	   
                <div class="form_left">
                    <label>Alasan</label>
                    <asp:Label ID="lblReason" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Disetujui Oleh</label>	
                    <asp:Label ID="lblToBeApproved" runat="server"></asp:Label>	
		        </div>
	      
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Approval Send Via</label>
                <asp:Label ID="Label2" runat="server"></asp:Label>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Catatan</label>
                <asp:Label ID="lblNotes" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_title">
            <div class="form_single">              
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
	        
                <div class="form_left">
                    <label>Status</label>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Tanggal Status</label>	
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
		        </div>
	   
        </div>
        <div class="form_box">
	        
                <div class="form_left">
                    <label>Diajukan Oleh</label>
                    <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>
	    
        </div>                       
        <asp:Panel ID="pnlbutton" runat="server">
        <div class="form_button">            
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
        </asp:Button>
        </div>         
        </asp:Panel>
    </form>
</body>
</html>
