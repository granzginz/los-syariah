﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RefundList.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.RefundList" %>

<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../Webform.UserController/UCReason.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccount" Src="../../Webform.UserController/ucBankAccount.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Refund List</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
		<!--
        function flyto(strParam, strOpt, strType) {
            switch (strOpt) {
                case "customer":
                    {
                        if (strType == 'C')
                            var a = window.open("../../../../SmartSearch/viewsearchcompany.aspx?custid=" + strParam, "customer", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        else
                            var a = window.open("../../../../SmartSearch/viewsearch.aspx?custid=" + strParam, "customer", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        a.focus();
                        break;
                    }
                case "agreement":
                    {
                        var a = window.open("../../../../SmartSearch/peragreement.aspx?applicationid=" + strParam, "agreement", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        a.focus();
                        break;
                    }
            }
        }
        function click() {
            if (event.button == 2) {
                alert('Anda Tidak Berhak');
            }
        }
        document.onmousedown = click
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(SuspendNo, branchid) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Suspend/SuspendInqView.aspx?SuspendNo=' + SuspendNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
		//-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sc1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                REFUND KONSUMEN
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak</label>
            <asp:HyperLink ID="HyAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer</label>
            <asp:HyperLink ID="HyCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <uc1:ucpaymentinfo id="oPaymentInfo" runat="server"></uc1:ucpaymentinfo>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL REFUND KONSUMEN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Prepaid yg Dapat direfund</label>
            <asp:Label ID="lblPrepaidRefund" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Jumlah Refund</label>
            <uc1:ucnumberformat id="txtRefundAmount" runat="server" />
        </div>
    </div>
    <%--<div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Nama Bank</label>
            <asp:TextBox ID="txtBankName" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Harap isi Nama Bank"
                ControlToValidate="txtBankName" Visible="true" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label class="label_req">
                Cabang Bank</label>
            <asp:TextBox ID="txtBankBranch" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ErrorMessage="Harap diisi Cabang Bank"
                ControlToValidate="txtBankBranch" Visible="true" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Nama Rekening</label>
            <asp:TextBox ID="txtAccName" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAcc" runat="server" ErrorMessage="Harap diisi Nama Rekening"
                ControlToValidate="txtAccName" Visible="true" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label class="label_req">
                No Rekening</label>
            <asp:TextBox ID="txtAccNo" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAccName" runat="server" ErrorMessage="Harap diisi No Rekening"
                ControlToValidate="txtAccNo" Visible="true" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>--%>
    <div class="form_box_uc">
        <uc1:ucbankaccount runat="server" id="oBankAcc"></uc1:ucbankaccount>
    </div>
    <uc1:ucapprovalrequest id="oReq" runat="server">
                </uc1:ucapprovalrequest>
    <div class="form_button">
        <asp:Button ID="ButtonRequest" runat="server" Text="Request" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
