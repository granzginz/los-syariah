﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RefundView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Refund
    Private oController As New RefundInqController
#End Region

#Region "Property"

    Private Property RefundNo() As String
        Get
            Return (CType(Viewstate("RefundNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RefundNo") = Value
        End Set
    End Property



#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblgiro As New Label
            Dim lblnewStat As New Label
            Me.RefundNo = ""
            Me.BranchID = ""
            Me.ApplicationID = ""
            If Request.QueryString("RefundNo") <> "" Then Me.RefundNo = Request.QueryString("RefundNo")
            If Request.QueryString("branchid") <> "" Then Me.BranchID = Request.QueryString("branchid")
            If Request.QueryString("Applicationid") <> "" Then Me.ApplicationID = Request.QueryString("Applicationid")

            Me.FormID = "REFUNDVIEW"
            DoBind()
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        lblTitle.Text = "OS AMOUNT OVER DUE AS OF " & Me.BusinessDate.ToString("dd/MM/yyyy")
        'HyAgreementNo.Text = Me.AgreementNo.trim
        'HyCustomerName.Text = Me.CustomerName

        oCustomClass.RefundNo = Me.RefundNo.Trim
        oCustomClass.BranchId = Me.BranchID.Trim
        oCustomClass.ApplicationID = Me.ApplicationID.Trim
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.ViewRefund(oCustomClass)

        With oCustomClass
            HyAgreementNo.Text = .Agreementno
            HyCustomerName.Text = .CustomerName
            lblRequestNo.Text = .RefundNo
            lblRequestDate.Text = .RequestDate.ToString("dd/MM/yyyy")
            lblRefundAmount.Text = FormatNumber(.RefundAmount, 2)
            lblBankName.Text = .bankName
            lblBankBranch.Text = .BankBranchID
            lblAccountName.Text = .BankAccountName
            lblAccountNomor.Text = .bankAccountNo
            lblReason.Text = .ReasonDesc
            lblToBeApproved.Text = .ApprovedBy
            lblStatus.Text = .StatusRefund
            lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
            lblRequestBy.Text = .Requestby
            lblprepaidRefund.Text = FormatNumber(.PrepaidRefundable, 2)

            lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
            lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
            lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
            lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
            lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)
            lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)
            lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
            lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
            lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
            lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)

            lblTotalOSOverDue.Text = FormatNumber(.InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                    .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                    .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                    .RepossessionFee, 2)


        End With

        '    HyPDCReceiptNo.NavigateUrl = "javascript:OpenWinMain('" & Trim(HyPDCReceiptNo.Text) & "','" & Trim(oCustomClass.BranchId) & "','" & Trim(flagfile) & "')"

    End Sub
#End Region

End Class