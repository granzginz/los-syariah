﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class RefundInq
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchby As UcSearchBy    
    Protected WithEvents oBranch As UcBranch

#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property



#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Refund
    Private oController As New Maxiloan.Controller.RefundInqController

#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub

        End If

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "REFUNDINQUIRY"

            osearchby.ListData = "RefundCustomerNo, No Request-AgreementNo,No Kontrak-Name, Nama Konsumen"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink


        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spRefundInqPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgRefund.DataSource = DtUserList.DefaultView
        DtgRefund.CurrentPageIndex = 0
        DtgRefund.DataBind()


        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True


    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan ....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataBound"
    Private Sub DtgRefund_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgRefund.ItemDataBound
        Dim hypRefundNo As HyperLink
        Dim lblNewBranch As Label
        Dim lblNewApplicationId As Label
        Dim HyCust As HyperLink
        Dim HyNewName As HyperLink
        Dim HyAgree As HyperLink
        If e.Item.ItemIndex >= 0 Then

            hypRefundNo = CType(e.Item.FindControl("HyRefundCustomerNo"), HyperLink)
            lblNewBranch = CType(e.Item.FindControl("lblbranchid"), Label)
            lblNewApplicationId = CType(e.Item.FindControl("lblApplicationid"), Label)
            HyCust = CType(e.Item.FindControl("HyCustomerID"), HyperLink)
            HyNewName = CType(e.Item.FindControl("lblName"), HyperLink)
            HyAgree = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)

            ' hypPDCRNo.NavigateUrl = "PDCRInquiryView.aspx?PDCReceiptNo=" & hypPDCRNo.Text.Trim & "&branchid=" & oBranch.BranchID.Trim
            hypRefundNo.NavigateUrl = "javascript:OpenWinMain('" & hypRefundNo.Text.Trim & "','" & lblNewBranch.Text.Trim & "','" & lblNewApplicationId.Text.Trim & "')"
            '*** Customer Link
            HyNewName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(HyCust.Text.Trim) & "')"
            '*** Agreement No link
            HyAgree.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblNewApplicationId.Text.Trim) & "')"

        End If
    End Sub
#End Region

#Region "reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Server.Transfer("RefundInq.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim par As String
        par = ""

        Me.SearchBy = "  REF.branchid = '" & oBranch.BranchID.Trim & "' "
        par = par & "Branch : " & oBranch.BranchName.Trim & " "

        If cbostatus.SelectedItem.Value <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " and REF.RefundStatus = '" & cbostatus.SelectedItem.Value & "'"
            If par <> "" Then
                par = par & " , Status : " & cbostatus.SelectedItem.Text & " "
            Else
                par = par & "Status : " & cbostatus.SelectedItem.Text & " "
            End If
        Else
            Me.SearchBy = Me.SearchBy & " and REF.RefundStatus in ('A','R', 'J')"

        End If


        If txtdate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and REF.RefundRequestdate = '" & ConvertDate2(txtdate.Text) & "'"
            If par <> "" Then
                par = par & " , Request Date : " & ConvertDate2(txtdate.Text).ToString("dd/MM/yyyy") & " "
            Else
                par = par & "Request Date : " & ConvertDate2(txtdate.Text).ToString("dd/MM/yyyy") & " "
            End If
        End If

        If osearchby.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            If par <> "" Then
                par = par & " , " & osearchby.ValueID & " : '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            Else
                par = par & "" & osearchby.ValueID & " : '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            End If
        End If

        oCustomClass.paramReport = par
        Me.ParamReport = oCustomClass.paramReport

        pnlDatagrid.Visible = True
        DtgRefund.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


End Class