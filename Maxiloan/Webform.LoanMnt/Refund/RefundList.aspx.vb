﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RefundList
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentinfo As UcPaymentInfo
    Protected WithEvents oReq As ucApprovalRequest
    Protected WithEvents txtRefundAmount As ucNumberFormat
    Protected WithEvents oBankAcc As UcBankAccount


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Refund
    Private oController As New RefundReqController
#End Region

#Region "Property"

    'Private Property CustomerName() As String
    '    Get
    '        Return (CType(Viewstate("CustomerName"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("CustomerName") = Value
    '    End Set
    'End Property



#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "REFUNDCUST"

            With txtRefundAmount
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign regular_text"
            End With


            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                Dim lblgiro As New Label
                Dim lblnewStat As New Label


                Me.ApplicationID = Request.QueryString("applicationid")
                Me.BranchID = Request.QueryString("branchid")
                oPaymentinfo.IsTitle = True


                With oBankAcc
                    .ValidatorTrue()
                    .Style = "AccMnt"
                    .BindBankAccount()
                End With


                DoBind()


            End If
        End If
    End Sub

#End Region

#Region "DoBind"
    Sub DoBind()
        Dim dblRefundable As Double
        lblTitle.Text = "OS AMOUNT OVER DUE AS OF " & Me.BusinessDate.ToString("dd/MM/yyyy")

        oCustomClass.BranchId = Me.BranchID
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.RefundReqList(oCustomClass)
        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerName = .CustomerName
        End With

        HyAgreementNo.Text = Me.AgreementNo.Trim
        HyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        HyCustomerName.Text = Me.CustomerName
        HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        With oPaymentinfo
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With

        oReq.ReasonTypeID = "RFUND"
        oReq.ApprovalScheme = "RFND"

        oCustomClass.BranchId = Me.BranchID
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.RefundReqListCust(oCustomClass)

        'With oCustomClass
        '    txtBankName.Text = .bankName
        '    txtBankBranch.Text = .BankBranch
        '    txtAccNo.Text = .bankAccountNo
        '    txtAccName.Text = .BankAccountName
        'End With
        
        With oBankAcc
            .BankBranchId = oCustomClass.BankBranchID.ToString
            .BindBankAccount()
            .BankID = oCustomClass.BankID.Trim
            .AccountNo = oCustomClass.bankAccountNo
            .AccountName = oCustomClass.BankAccountName            
        End With        

        'Modify by Wira, agar AYDA bisa di refund customer jika ada terlanjur bayar
        'If CDbl(oPaymentinfo.TotOSOverDue) > 0 Then
        If CDbl(oPaymentinfo.TotOSOverDue) > 0 And oPaymentinfo.ContractStatus <> "INV" Then
            'dblRefundable = oPaymentinfo.Prepaid - oPaymentinfo.TotOSOverDue
            'lblPrepaidRefund.Text = FormatNumber(oPaymentinfo.Prepaid - oPaymentinfo.TotOSOverDue, 2)
            dblRefundable = oPaymentinfo.Prepaid
            lblPrepaidRefund.Text = FormatNumber(oPaymentinfo.Prepaid, 2)
            If dblRefundable > 0 Then
                txtRefundAmount.RangeValidatorMinimumValue = "0"
                txtRefundAmount.RangeValidatorMaximumValue = CType(dblRefundable + 1, String)
            Else
                txtRefundAmount.RangeValidatorMinimumValue = "0"
                txtRefundAmount.RangeValidatorMaximumValue = CType(oPaymentinfo.Prepaid + 1, String)
            End If

        Else
            lblPrepaidRefund.Text = FormatNumber(oPaymentinfo.Prepaid, 2)
            txtRefundAmount.RangeValidatorMinimumValue = "0"
            txtRefundAmount.RangeValidatorMaximumValue = CType(oPaymentinfo.Prepaid + 1, String)
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Server.Transfer("Refund.aspx")
    End Sub
#End Region

#Region "Request"
    Private Sub imbRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRequest.Click
        If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            If CDbl(txtRefundAmount.Text) <= CDbl(lblPrepaidRefund.Text) Then
                If CDbl(txtRefundAmount.Text) = 0 Then

                    ShowMessage(lblMessage, "Jumlah Refund harus > 0", True)
                    Exit Sub
                End If
                With oCustomClass
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    '.bankName = txtBankName.Text.Trim
                    '.BankBranch = txtBankBranch.Text.Trim
                    '.BankAccountName = txtAccName.Text.Trim
                    '.bankAccountNo = txtAccNo.Text.Trim

                    .BankID = oBankAcc.BankID
                    .BankBranchID = CInt(IIf(oBankAcc.BankBranchId.Trim = "", "0", oBankAcc.BankBranchId.Trim).ToString)
                    .BankAccountName = oBankAcc.AccountName
                    .bankAccountNo = oBankAcc.AccountNo
                    .bankName = oBankAcc.BankName
                    .BankBranchName = oBankAcc.BankBranchName
                    .ReasonID = oReq.ReasonID
                    .ReasonDesc = oReq.ReasonName
                    .ReasonTypeID = "RFUND"
                    .ApprovedBy = oReq.ToBeApprove
                    .Requestby = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .ApplicationID = Me.ApplicationID



                    'If chkSMS.Checked Then
                    '    .ApprovedSMS = "1"
                    'Else
                    '    .ApprovedSMS = "0"
                    'End If
                    'If chkEmail.Checked Then
                    '    .ApprovedEmail = "1"
                    'Else
                    '    .ApprovedEmail = "0"
                    'End If
                    'If chkFaxNo.Checked Then
                    '    .ApprovedFaxNo = "1"
                    '    .ApprovedDesc = txtApproveVia.Text.Trim
                    'Else
                    '    .ApprovedFaxNo = "0"
                    '    .ApprovedDesc = ""
                    'End If
                    .Notes = oReq.Notes
                    .RefundAmount = CDbl(txtRefundAmount.Text)
                End With

                oCustomClass.strConnection = GetConnectionString()
                Try
                    oCustomClass = oController.SaveRefundReqH(oCustomClass)
                    Response.Redirect("Refund.aspx?msg=success")                    
                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try


            Else

                ShowMessage(lblMessage, "Jumlah Refund harus <= Prepaid yang dapat direfund", True)
                Exit Sub
            End If

        End If
    End Sub

#End Region

End Class