﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RefundInq.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.RefundInq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RefundInq</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
     
        function OpenWinMain(RefundNo, branchid, Applicationid) { 
            window.open(ServerName + App + '/Webform.LoanMnt/Refund/RefundView.aspx?RefundNo=' + RefundNo + '&branchid=' + branchid + '&Applicationid=' + Applicationid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        INQUIRY REFUND KONSUMEN
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                     <div class="form_left">
                            <label class="label_req">
                                Cabang</label>
                            <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
                      </div>
                      <div class="form_right">
                            <label>
                                Tanggal Request</label>
                            <asp:TextBox ID="txtdate" runat="server" CssClass ="small_text" ></asp:TextBox>
                            <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtdate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtdate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                      </div>
                </div>
                <div class="form_box">
                 
                        <div class="form_left">
                            <label>
                                Status</label>
                            <asp:DropDownList ID="cbostatus" runat="server">
                                <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                                <asp:ListItem Value="R">Request</asp:ListItem>
                                <asp:ListItem Value="A">Approved</asp:ListItem>
                                <asp:ListItem Value="J">Reject</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                                      
                </div>
                <div class="form_box_uc">
                        <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                    </div>            
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR REFUND KONSUMEN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgRefund" runat="server" Width="100%" Visible="False" AllowSorting="True"
                                OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="RefundCustomerNo"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn Visible="False" SortExpression="BranchID" HeaderText="BRANCH ID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RefundCustomerNo" HeaderText="NO REQUEST">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyRefundCustomerNo" runat="server" Text='<%#Container.DataItem("RefundCustomerNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="APPLICATION ID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                         
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomeriD" HeaderText="CUSTOMER ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyCustomerID" runat="server" Text='<%#Container.DataItem("CustomeriD")%>'>
                                            </asp:HyperLink>
                                
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                            </asp:HyperLink>
                                   
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RefundAmount" HeaderText="JUMLAH REFUND">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="item_grid_right" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" CssClass="item_grid_right"  Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRefundAmount" runat="server" Text='<%#formatnumber(Container.DataItem("RefundAmount"),0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="RefundRequestDate" SortExpression="RefundRequestDate"
                                        HeaderText="REQUEST DATE" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="RefundStatusdesc" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("RefundStatusdesc")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
