﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCClearing.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PDCClearing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCClearing</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">  <div class="title_strip"> </div>
        <div class="form_single">
            <h3>
                KLIRING PDC
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jumlah PDC dialokasi
            </label>
            <asp:Label ID="lblTotalPDC" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Pembayaran dialokasi
            </label>
            <asp:Label ID="lblTotalAPDisburse" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnExecuteBatch" runat="server" Text="Eksekusi" CssClass="small button blue">
        </asp:Button>
    </div>
    </form>
</body>
</html>
