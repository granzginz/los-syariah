﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController


Public Class CashBankEndingBalance
    Inherits Maxiloan.Webform.WebBased
    Private oCustomClass As New Parameter.CashierTransaction
    Private oController As New CashierTransactionController
    Private m_controller As New DataUserControlController
    Dim beginBalance As Double
    Dim debit As Double
    Dim credit As Double


#Region " Properties"
    Private Property cmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property BranchIDHistory() As String
        Get
            Return CType(viewstate("BranchIDHistory"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchIDHistory") = Value
        End Set
    End Property
    Private Property BankAccountHistory() As String
        Get
            Return CType(viewstate("BankAccountHistory"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountHistory") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            initPanels()
            initControlValue()
            bindSearchBranch()
        End If
    End Sub

    Sub initControlValue()
        txtsdate.Text = ""
        txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        lblMessage.Text = ""
        Me.cmdWhere = ""
        Me.FormID = "CASHBANKBAL"
        Me.SortBy = "BranchID"
    End Sub

    Private Sub initPanels()
        pnlSearch.Visible = True
        pnlDtGrid.Visible = False
    End Sub

    Private Sub bindSearchBranch()
        With cboBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(1).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End If

        End With
    End Sub

    Private Sub doBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim oDataTable As New DataTable
        Dim oDataView As New DataView

        If cmdWhere.Trim = "" Then cmdWhere = ""

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.getCashBankBalance(oCustomClass)

        oDataTable = oCustomClass.DataSet.Tables(0)
        oDataView = oDataTable.DefaultView
        oDataView.Sort = Me.SortBy
        Datagrid1.DataSource = oDataView

        Try
            Datagrid1.DataBind()
        Catch
            Datagrid1.CurrentPageIndex = 0
            Datagrid1.DataBind()
        End Try

        pnlDtGrid.Visible = True
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim strSQLBranch As String = IIf(cboBranch.SelectedValue = "ALL", "and branchID IN (select branchID from branch)", " and BranchID = " & cboBranch.SelectedValue).ToString
        Me.SearchBy = strSQLBranch
        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub Datagrid1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid1.ItemCommand
        Select Case e.CommandName
            Case "history"
                lblBranchFullNameRef.Text = CType(e.Item.Cells(2).FindControl("lblBranchFullName"), Label).Text.Trim
                lblBankAccountNameRef.Text = CType(e.Item.Cells(3).FindControl("lblBankAccountName"), Label).Text.Trim
                Me.BranchIDHistory = CType(e.Item.Cells(1).FindControl("lblBranchID"), Label).Text.Trim
                Me.BankAccountHistory = CType(e.Item.Cells(1).FindControl("lblBankAccountID"), Label).Text.Trim
                pnlSearch.Visible = False
                pnlDtGrid.Visible = False
                pnlHistory.Visible = True
        End Select
    End Sub

    Private Sub BtnFindHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindHistory.Click
        bindCashBankEndingBalance()
        pnlHistory.Visible = True
        pnlHistoryGrid.Visible = True
        pnlDtGrid.Visible = False
        pnlSearch.Visible = False
    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        pnlHistory.Visible = False
        pnlHistoryGrid.Visible = False
        pnlDtGrid.Visible = True
        pnlSearch.Visible = True
    End Sub

    Private Sub Datagrid1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Datagrid1.SelectedIndexChanged

    End Sub

    Private Sub bindCashBankEndingBalance()
        Dim oDataTable As New DataTable
        Dim oDataView As New DataView
        Dim oCustomClass1 As New Parameter.CashierTransaction
        With oCustomClass1
            .strConnection = getConnectionString
            .WhereCond = "bacc.branchID = '" & Me.BranchIDHistory & "' and bacc.bankAccountID = '" & Me.BankAccountHistory & "'"
            .BusinessDate = ConvertDate2(txtsdate.Text)
            .JournalCode = txtVocuherNo.Text
        End With
        oCustomClass1 = oController.getCashBankBalanceHistory(oCustomClass1)
        oDataTable = oCustomClass1.DataSet.Tables(0)
        oDataView = oDataTable.DefaultView
        beginBalance = CDbl(oDataView.Item(0).Item("beginbalance"))
        Datagrid2.DataSource = oDataView
        Try
            Datagrid2.DataBind()
        Catch
            Datagrid2.CurrentPageIndex = 0
            Datagrid2.DataBind()
        End Try
    End Sub

    Private Sub Datagrid2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Datagrid2.ItemDataBound

        Select Case e.Item.ItemType




            Case ListItemType.Header
                'CType(e.Item.Cells(8).FindControl("lblBeginBalanceRef"), Label).Text = "Begin Balance " & FormatNumber(beginBalance, 0)
                CType(e.Item.Cells(8).FindControl("lblBeginBalanceRef"), Label).Text = "Begin Balance " & FormatNumber(beginBalance, 2)
            Case ListItemType.Item, ListItemType.AlternatingItem

                Dim lblEndingBalanceHistory As Label
                beginBalance += CDbl(CType(e.Item.Cells(0).FindControl("lblDebitAmountHistory"), Label).Text) - CDbl(CType(e.Item.Cells(0).FindControl("lblCreditAmountHistory"), Label).Text)
                'CType(e.Item.Cells(0).FindControl("lblEndingBalanceHistory"), Label).Text = FormatNumber(beginBalance, 0)
                CType(e.Item.Cells(0).FindControl("lblEndingBalanceHistory"), Label).Text = FormatNumber(beginBalance, 2)



                debit += CDbl(CType(e.Item.Cells(0).FindControl("lblDebitAmountHistory"), Label).Text)
                credit += CDbl(CType(e.Item.Cells(0).FindControl("lblCreditAmountHistory"), Label).Text)
            Case ListItemType.Footer
                'CType(e.Item.Cells(6).FindControl("lblTotaoDebit"), Label).Text = FormatNumber(debit, 0)
                'CType(e.Item.Cells(6).FindControl("lblTotalCredit"), Label).Text = FormatNumber(credit, 0)
                CType(e.Item.Cells(6).FindControl("lblTotaoDebit"), Label).Text = FormatNumber(debit, 2)
                CType(e.Item.Cells(6).FindControl("lblTotalCredit"), Label).Text = FormatNumber(credit, 2)

        End Select


    End Sub

End Class