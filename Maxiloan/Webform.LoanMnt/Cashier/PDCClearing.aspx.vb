﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports Decrypt
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCClearing
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.CashierTransaction
    Private oController As New CashierTransactionController

    Protected WithEvents oCashier As UcCashier
#End Region

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property CashierID() As String
        Get
            Return (CType(Viewstate("CashierID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CashierID") = Value
        End Set
    End Property

    Private Property CashierName() As String
        Get
            Return (CType(Viewstate("CashierName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CashierName") = Value
        End Set
    End Property

    Private Property OpeningSequence() As Integer
        Get
            Return (CType(Viewstate("OpeningSequence"), Integer))
        End Get
        Set(ByVal Value As Integer)
            viewstate("OpeningSequence") = Value
        End Set
    End Property

    Private Property EndingBalance() As Double
        Get
            Return (CType(Viewstate("EndingBalance"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("EndingBalance") = Value
        End Set
    End Property

    Private Property IsHeadCashier() As Boolean
        Get
            Return (CType(Viewstate("IsHeadCashier"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsHeadCashier") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Me.FormID = "OPENCLOSECASHIER"
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId
                .strConnection = GetConnectionString
            End With
            Me.IsHeadCashier = True
            If Not oController.CheckHeadCashier(oCustomClass) Then
                
                ShowMessage(lblMessage, "Anda Bukan Kepala Kasir", True)
                BtnExecuteBatch.Visible = False
                Me.IsHeadCashier = False
            Else

                With oCustomClass
                    .strConnection = GetConnectionString
                    .BranchId = Me.sesBranchId
                    .BusinessDate = Me.BusinessDate
                End With
                oCustomClass = oController.CHCloseList(oCustomClass)
                lblTotalAPDisburse.Text = CStr(oCustomClass.TotalAPToDisburse)
                lblTotalPDC.Text = CStr(oCustomClass.TotalPDCToClear)
                If oCustomClass.TotalAPToDisburse > 0 Or oCustomClass.TotalPDCToClear > 0 Then
                    BtnExecuteBatch.Visible = True
                Else
                    BtnExecuteBatch.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub BtnExecuteBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExecuteBatch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "BATCH", "MAXILOAN") Then
            If Me.IsHeadCashier Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .CoyID = Me.SesCompanyID
                    .BusinessDate = Me.BusinessDate
                    .LoginId = Me.Loginid
                End With
                If CInt(lblTotalPDC.Text) > 0 Or CInt(lblTotalAPDisburse.Text) > 0 Then
                    Try
                        oController.HeadCashierBatchProcess(oCustomClass)
                        BtnExecuteBatch.Visible = False
                    Catch exp As Exception
                        Dim dtHeadCashier As New DataTable
                        lblMessage.Text = ""
                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .BranchId = Me.sesBranchId
                            .CoyID = Me.SesCompanyID
                            .BusinessDate = Me.BusinessDate
                        End With
                        oCustomClass = oController.CHCloseList(oCustomClass)
                        lblTotalAPDisburse.Text = CStr(oCustomClass.TotalAPToDisburse)
                        lblTotalPDC.Text = CStr(oCustomClass.TotalPDCToClear)
                        If oCustomClass.TotalAPToDisburse > 0 Or oCustomClass.TotalPDCToClear > 0 Then
                            BtnExecuteBatch.Visible = True
                        End If

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        Else

            ShowMessage(lblMessage, "Anda bukan Kepala Kasir", True)
        End If
    End Sub

End Class