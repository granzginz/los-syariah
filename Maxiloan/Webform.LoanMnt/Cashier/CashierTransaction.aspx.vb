﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports Decrypt
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Webform.UserController.UcCashier

#End Region

Public Class CashierTransaction
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property CashierID() As String
        Get
            Return (CType(ViewState("CashierID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CashierID") = Value
        End Set
    End Property

    Private Property CashierName() As String
        Get
            Return (CType(ViewState("CashierName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CashierName") = Value
        End Set
    End Property

    Private Property OpeningSequence() As Integer
        Get
            Return (CType(ViewState("OpeningSequence"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("OpeningSequence") = Value
        End Set
    End Property

    Private Property EndingBalance() As Double
        Get
            Return (CType(ViewState("EndingBalance"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("EndingBalance") = Value
        End Set
    End Property

    Private Property IsHeadCashier() As Boolean
        Get
            Return (CType(ViewState("IsHeadCashier"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsHeadCashier") = Value
        End Set
    End Property

    Private Property TotOutstanding() As Integer
        Get
            Return (CType(ViewState("TotOutstanding"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotOutstanding") = Value
        End Set
    End Property
    Private Property CashierEndingBalance() As Double
        Get
            Return (CType(ViewState("CashierEndingBalance"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("CashierEndingBalance") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.CashierTransaction
    Private oController As New CashierTransactionController
    Dim Total As Double
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents txtOpeningAmount As ucNumberFormat
    Protected WithEvents txtClosingAmount As ucNumberFormat
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "OPENCLOSECASHIER"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                lblMessage.Text = ""
                Me.IsHeadCashier = True
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                pnlOutstanding.Visible = False

                With txtOpeningAmount
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
                With txtClosingAmount
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")

        If UBound(strBranch) > 0 Then

            ShowMessage(lblMessage, "Anda tidak dapat melakukan transaksi Kasir<BR>", True)
            BtnOpen.Enabled = False
            BtnHeadCashierClose.Enabled = False
            Me.IsHeadCashier = False
        Else
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId
                .strConnection = GetConnectionString()
            End With
            If Not oController.CheckHeadCashier(oCustomClass) Then

                ShowMessage(lblMessage, "Anda bukan Kepala Kasir", True)
                BtnOpen.Enabled = False
                BtnHeadCashierClose.Enabled = False
                Me.IsHeadCashier = False
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtCashierHistory As New DataTable
        Dim DvCashierHistory As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim strBranch() As String

        strBranch = Split(Me.sesBranchId, ",")

        If UBound(strBranch) = 0 Then
            With oCustomClass
                .BranchId = Me.sesBranchId
                .BusinessDate = Me.BusinessDate
                .strConnection = GetConnectionString()
            End With

            oCashier.CashierID = ""
            oCashier.HeadCashier = False
            txtOpeningAmount.Text = ""
            txtCashierPassword.Text = ""
            oCustomClass = oController.ListCashierHistory(oCustomClass)

            DtCashierHistory = oCustomClass.ListCashierHistory
            DvCashierHistory = DtCashierHistory.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvCashierHistory.Sort = Me.SortBy
            dtgCashierHistory.DataSource = DvCashierHistory
            lblHeadCashierBalance.Text = FormatNumber(CStr(oCustomClass.HeadCashierBalance), 2)
            txtOpeningAmount.RangeValidatorEnable = True
            txtOpeningAmount.RangeValidatorMaximumValue = CStr(oCustomClass.HeadCashierBalance)


            If oCustomClass.IsHeadCashierClose Then
                BtnOpen.Enabled = False
                BtnHeadCashierClose.Enabled = False

                ShowMessage(lblMessage, "Kepala Kasir Sudah Tutup", True)
            End If

            Try
                dtgCashierHistory.DataBind()
            Catch
                dtgCashierHistory.CurrentPageIndex = 0
                dtgCashierHistory.DataBind()
            End Try
            pnlList.Visible = True
            pnlCashierClose.Visible = False
            pnlHeadClose.Visible = False
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub dtgCashierHistory_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCashierHistory.ItemCommand
        Select Case e.CommandName
            Case "CLOSE"
                lblMessage.Text = ""
                If CheckFeature(Me.Loginid, Me.FormID, "CLOSE", "MAXILOAN") Then
                    Dim lblEmployeeID As Label
                    lblEmployeeID = CType(dtgCashierHistory.Items(e.Item.ItemIndex).FindControl("lblEmployeeID"), Label)

                    Dim lblSequenceNo As Label
                    lblSequenceNo = CType(dtgCashierHistory.Items(e.Item.ItemIndex).FindControl("lblOpeningSequence"), Label)
                    Dim lblEndingBalance As Label
                    lblEndingBalance = CType(dtgCashierHistory.Items(e.Item.ItemIndex).FindControl("lblEndingBalance"), Label)
                    Dim lblCashName As Label
                    lblCashName = CType(dtgCashierHistory.Items(e.Item.ItemIndex).FindControl("lblEmployeeName"), Label)
                    'txtClosingAmount.Text = ""
                    txtClosingAmount.Text = lblEndingBalance.Text
                    txtClosingAmount.RangeValidatorMinimumValue = lblEndingBalance.Text.Replace(",", "")
                    txtClosingAmount.RangeValidatorMaximumValue = lblEndingBalance.Text.Replace(",", "")

                    Me.CashierID = lblEmployeeID.Text.Trim
                    Me.OpeningSequence = CInt(lblSequenceNo.Text)
                    Me.CashierName = lblCashName.Text

                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .LoginId = Me.CashierID.Trim
                        .BusinessDate = Me.BusinessDate
                        .OpeningSequence = Me.OpeningSequence
                        .BranchId = Me.sesBranchId
                    End With
                    oCustomClass = oController.CashierCloseList(oCustomClass)
                    With oCustomClass
                        dtgListCashierClose.DataSource = .ListCashierClose
                        dtgListCashierClose.DataBind()
                        lblCashierName.Text = Me.CashierName.Trim
                        lblOpeningDate.Text = .BusinessDate.ToString("dd/MM/yyyy HH:mm:ss")
                        lblOnHandAmount.Text = FormatNumber(.ClosingAmount, 2)
                        lblOpeningAmount.Text = FormatNumber(.OpeningAmount, 2)
                        lblNoOfPDC.Text = CStr(.NumberOfPDC)
                        lblPDCAmount.Text = FormatNumber(.PDCAmount, 2)
                    End With
                    pnlCashierClose.Visible = True
                    pnlList.Visible = False
                End If
        End Select
    End Sub

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub dtgCashierHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCashierHistory.ItemDataBound
        Dim m As Int32
        Dim imbClose As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbClose = CType(e.Item.FindControl("imbClose"), ImageButton)
            If Not Me.IsHeadCashier Then
                imbClose.Enabled = False
            End If
            'imbClose.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
#End Region

#Region "Cashier Open"
    Private Sub BtnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOpen.Click
        lblMessage.Text = ""
        Dim oLogin As New LoginController
        Dim oLoginEntities As New Parameter.Login
        Dim objDecent As New Decrypt.am_futility
        Dim strError As String
        oLoginEntities.strConnection = GetConnectionString()
        oLoginEntities.LoginId = oCashier.CashierID.Trim
        Dim lstrPassword As String

        If CDbl(txtOpeningAmount.Text) <> Me.CashierEndingBalance Then
            ShowMessage(lblMessage, "Saldo tidak sesuai dengan saldo terakhir yang ada di kasir", True)
            Exit Sub
        End If
        'oLogin.GetEmployee(oLoginEntities) retreive password dari user
        lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
        If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BusinessDate = CType(CStr(Me.BusinessDate) & " " & Now.ToLongTimeString, DateTime)
                .LoginId = oCashier.CashierID
                .OpeningAmount = CDbl(txtOpeningAmount.Text)
                .BranchId = Me.sesBranchId
            End With
            Try
                oController.CashierOpen(oCustomClass)

                ShowMessage(lblMessage, "Kasir Sudah Buka", True)
                DoBind(Me.SearchBy, Me.SortBy)
            Catch exp As Exception

                ShowMessage(lblMessage, TranslateMessage(exp.Message), True)
            End Try
        Else

            ShowMessage(lblMessage, "Password Salah", True)
        End If
    End Sub

    Protected Function TranslateMessage(ByVal msg As String) As String
        Dim rtn As String = ""
        Dim msgcode As String = ""
        If msg.Length > 4 Then
            msgcode = msg.Substring(0, 4)
            Select Case msgcode
                Case "0001"
                    rtn = "Cashier cannot be open, because Cash Balance not sufficient"
                Case "0002"
                    rtn = "Cashier cannot be open, because It is already open"
                Case "0003"
                    rtn = "Failed On Opening Cashier"
                Case Else
                    rtn = msg
            End Select
        End If
        Return rtn
    End Function
#End Region

#Region "imbHeadCashierClose_Click"
    Private Sub BtnHeadCashierClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnHeadCashierClose.Click

        Dim dtHeadCashier As New DataTable
        lblMessage.Text = ""
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .BusinessDate = Me.BusinessDate
        End With
        If oController.CheckAllCashierClose(oCustomClass) Then
            oCustomClass = oController.CHCloseList(oCustomClass)
            dtHeadCashier = oCustomClass.ListHeadCashierHistory
            dtgHeadCashier.DataSource = dtHeadCashier
            dtgHeadCashier.DataBind()
            lblTotalAPDisburse.Text = CStr(oCustomClass.TotalAPToDisburse)
            lblTotalPDC.Text = CStr(oCustomClass.TotalPDCToClear)
            pnlList.Visible = False
            pnlHeadClose.Visible = True
            If oCustomClass.TotalAPToDisburse > 0 Or oCustomClass.TotalPDCToClear > 0 Then
                BtnHCClose.Visible = False
            Else
                BtnExecuteBatch.Visible = False
            End If
            DoBind2()
            If Me.TotOutstanding > 0 Then
                BtnHCClose.Visible = False

                ShowMessage(lblMessage, "Kasir tidak bisa Tutup", True)
                Exit Sub
            End If
        Else

            ShowMessage(lblMessage, "Harap Tutup Semua Kasir Terlebih Dahulu", True)
            Exit Sub
        End If
    End Sub
#End Region

#Region "Cashier Close"
    Private Sub CloseCashier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCloseCashier.Click
        lblMessage.Text = ""
        Dim oLogin As New LoginController
        Dim oLoginEntities As New Parameter.Login
        Dim objDecent As New Decrypt.am_futility
        Dim strError As String
        Dim lstrPassword As String

        If Not IsNumeric(txtClosingAmount.Text.Trim) Then
            ShowMessage(lblMessage, "Saldo kasir tidak valid", True)
            Exit Sub
        End If
        If CDbl(txtClosingAmount.Text) <> CDbl(lblOnHandAmount.Text) Then
            ShowMessage(lblMessage, "Saldo tidak sesuai dengan saldo terakhir yang ada di kasir", True)
            Exit Sub
        End If
        oLoginEntities.strConnection = GetConnectionString()
        oLoginEntities.LoginId = Me.CashierID.Trim

        lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
        If lstrPassword.Trim = txtPasswordClose.Text.Trim Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .OpeningSequence = Me.OpeningSequence
                .BusinessDate = CType(CStr(Me.BusinessDate) & " " & Now.ToLongTimeString, DateTime)
                .LoginId = Me.CashierID
                .ClosingAmount = CDbl(txtClosingAmount.Text.Trim)
                .BranchId = Me.sesBranchId.Replace("'", "")
            End With
            Try
                oController.CashierClose(oCustomClass)

                ShowMessage(lblMessage, "Kasir Sudah Tutup", True)
                pnlList.Visible = True
                pnlCashierClose.Visible = False
                DoBind(Me.SearchBy, Me.SortBy)
                pnlCashierClose.Visible = False
                pnlList.Visible = True
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        Else

            ShowMessage(lblMessage, "Password Salah", True)
        End If
    End Sub
#End Region

#Region "imbCloseCancel_Click"
    Private Sub BtnCloseCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCloseCancel.Click
        pnlCashierClose.Visible = False
        pnlList.Visible = True
    End Sub
#End Region

#Region "imbHCCancel_Click"
    Private Sub BtnHCCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnHCCancel.Click
        pnlList.Visible = True
        pnlHeadClose.Visible = False
        pnlOutstanding.Visible = False
        lblMessage.Text = ""
    End Sub
#End Region

#Region "imbHCClose_Click"
    Private Sub BtnHCClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnHCClose.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
        End With
        Dim strError As String
        Try
            oController.HeadCashierClose(oCustomClass)

            ShowMessage(lblMessage, "Tutup Kepala Kasir - Berhasil", True)
            pnlList.Visible = True
            pnlHeadClose.Visible = False
            BtnOpen.Enabled = False
            BtnHeadCashierClose.Enabled = False
            pnlOutstanding.Visible = False
            GenerateTextFile()
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

#Region "ExecuteBatch"
    Private Sub BtnExecuteBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExecuteBatch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "BATCH", "MAXILOAN") Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With
            If CInt(lblTotalPDC.Text) > 0 Or CInt(lblTotalAPDisburse.Text) > 0 Then
                Try
                    oController.HeadCashierBatchProcess(oCustomClass)
                    BtnHCClose.Visible = True
                    BtnExecuteBatch.Visible = False
                Catch exp As Exception
                    Dim dtHeadCashier As New DataTable
                    lblMessage.Text = ""
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .BranchId = Me.sesBranchId
                        .CoyID = Me.SesCompanyID
                        .BusinessDate = Me.BusinessDate
                    End With
                    If oController.CheckAllCashierClose(oCustomClass) Then
                        oCustomClass = oController.CHCloseList(oCustomClass)
                        dtHeadCashier = oCustomClass.ListHeadCashierHistory
                        dtgHeadCashier.DataSource = dtHeadCashier
                        dtgHeadCashier.DataBind()
                        lblTotalAPDisburse.Text = CStr(oCustomClass.TotalAPToDisburse)
                        lblTotalPDC.Text = CStr(oCustomClass.TotalPDCToClear)
                        pnlList.Visible = False
                        pnlHeadClose.Visible = True
                        If oCustomClass.TotalAPToDisburse > 0 Or oCustomClass.TotalPDCToClear > 0 Then
                            BtnHCClose.Visible = False
                        End If
                    Else

                        ShowMessage(lblMessage, "Harap tutup semua kasir terlebih dahulu", True)
                        Exit Sub
                    End If

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
        End If
    End Sub
#End Region

#Region "Generate Text Untuk GL Interface"
    Private Sub GenerateTextFile()
        Dim DTJournalH As New DataTable
        Dim DTJournalD As New DataTable
        Dim PathFile As String
        Dim counterHeader As Integer
        Dim counterDetail As Integer
        Dim objStreamWriter As StreamWriter
        Dim strFileName As String
        Dim currentDate As Date
        Dim currentTransactionID As String

        'PathFile = oController.FileAllocationGLInterface(GetConnectionString)
        'If Not IO.Directory.Exists(PathFile) Then
        '    IO.Directory.CreateDirectory(PathFile)
        'End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Replace("'", "")
            .BusinessDate = Me.BusinessDate
        End With
        DTJournalH = oController.GLInterfaceHeader(oCustomClass)
        'For counterHeader = 0 To DTJournalH.Rows.Count - 1
        '    strFileName = Me.sesBranchId.Replace("'", "") + CStr(DTJournalH.Rows(counterHeader).Item("TransactionID")).Trim + CDate(DTJournalH.Rows(counterHeader).Item("Tr_date")).ToString("ddMMyyyy")
        '    If Not File.Exists(PathFile & strFileName) Then
        '        objStreamWriter = New StreamWriter(PathFile & strFileName & ".txt", True, _
        '            Encoding.Unicode)
        '        objStreamWriter.Write("""1"",")
        '        objStreamWriter.Write("""000001"",")
        '        objStreamWriter.Write("""" & CStr(DTJournalH.Rows(counterHeader).Item("Nomor")) & """,")
        '        objStreamWriter.Write("""GL"",")
        '        objStreamWriter.Write("""HR"",")
        '        objStreamWriter.WriteLine("""" & CStr(DTJournalH.Rows(counterHeader).Item("Tr_description")) & """")
        '        With oCustomClass
        '            .strConnection = GetConnectionString
        '            .JournalCode = CStr(DTJournalH.Rows(counterHeader).Item("Tr_nomor"))
        '            .BranchId = Me.sesBranchId
        '        End With
        '        DTJournalD = oController.GLInterfaceDetail(oCustomClass)
        '        For counterDetail = 0 To DTJournalD.Rows.Count - 1
        '            objStreamWriter.Write("""2"",")
        '            objStreamWriter.Write("""000001"",")
        '            objStreamWriter.Write("""" & CStr(DTJournalD.Rows(counterDetail).Item("Nomor")) & """,")
        '            objStreamWriter.Write("""" & CStr(DTJournalD.Rows(counterDetail).Item("SequenceNo")) & """,")
        '            objStreamWriter.Write("""" & CStr(DTJournalD.Rows(counterDetail).Item("CoaID")) & """,")
        '            objStreamWriter.WriteLine("""" & CStr(DTJournalD.Rows(counterDetail).Item("Tr_description")) & """")
        '        Next
        '        objStreamWriter.Close()
        '    End If
        'Next
    End Sub
#End Region

#Region "DoBind2"
    Sub DoBind2()
        pnlOutstanding.Visible = True
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .strConnection = GetConnectionString()
        End With
        oCustomClass = oController.GetListOutProc(oCustomClass)

        DtUserList = oCustomClass.listdata
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgOutstanding.DataSource = DvUserList
        DtgOutstanding.DataBind()
    End Sub
#End Region

#Region "DtgOutstanding_ItemDataBound"
    Private Sub DtgOutstanding_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgOutstanding.ItemDataBound
        Dim inlblSQLCmd As Label
        Dim inlblTotal As Label
        Dim inlblSum As Label
        If e.Item.ItemIndex >= 0 Then
            inlblSQLCmd = CType(e.Item.FindControl("lblSqlCmd"), Label)
            inlblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            If inlblSQLCmd.Text.Trim <> "" Then
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass.WhereCond = Replace(Replace(inlblSQLCmd.Text.Trim, "$BranchID", Me.sesBranchId.Replace("'", "")), "$EffectiveDate", Me.BusinessDate.ToString("yyyyMMdd"))
                oCustomClass = oController.GetOutProc(oCustomClass)
                inlblTotal.Text = CStr(oCustomClass.TotalRecord)
                Total += CDbl(inlblTotal.Text)
            End If

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            inlblSum = CType(e.Item.FindControl("lblSum"), Label)
            inlblSum.Text = FormatNumber(Total, 0)
            Me.TotOutstanding = CInt(inlblSum.Text)
        End If
    End Sub
#End Region

    Protected Sub GetMySelection(sender As Object, e As SelectedIndexChangedEventArgs)
        lblMessage.Text = ""
        txtOpeningAmount.Text = FormatNumber(e.SelectedItem, 2)
        Me.CashierEndingBalance = e.SelectedItem
    End Sub
End Class