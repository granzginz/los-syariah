﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Web.Services
Imports System.Data.SqlClient

#End Region
Public Class AdvanceReturn
    Inherits AccMntWebBased

#Region "Private&Const"
    Private STR_FORM_ID As String = "ADVRTN"
    Private QUERY_STR_MSG As String = "Message"

    Private m_oCustomClass As Parameter.AdvanceRequest
    Private m_oController As Controller.AdvanceRequestController

    '*** Paging vars
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

    Private Property hyAdvanceNo As HyperLink

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If SessionInvalid() Then
                Exit Sub
            End If

            If Not IsPostBack Then
                If Request.QueryString(QUERY_STR_MSG) <> "" Then
                    lblMessage.Text = Request.QueryString(QUERY_STR_MSG)
                End If
                Me.FormID = STR_FORM_ID

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If
            End If



        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub
    '**** STEP 6. Add DisplayError procedure
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub
    '*** STEP 7. Add DoBind procedure
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            With m_oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .BusinessDate = Me.BusinessDate
            End With

            m_oCustomClass.PettyCashAdvanceList = m_oController.GetPettyCashAdvanceList(m_oCustomClass).PettyCashAdvanceList

            DtUserList = m_oCustomClass.PettyCashAdvanceList
            DvUserList = DtUserList.DefaultView
            recordCount = m_oCustomClass.TotalRecord
            'DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True

            lblMessage.Text = ""

        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try

    End Sub

    '**** STEP 4.1 Initialize class vars
    Public Sub New()
        m_oCustomClass = New Parameter.AdvanceRequest
        m_oController = New Controller.AdvanceRequestController
    End Sub

    '**** STEP 8. Add navigation routines
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data Not Found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    '*** STEP 9. Add imbReset handler
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnReset.Click
        Me.SearchBy = ""
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        txtAging.Text = ""

        pnlDatagrid.Visible = False
    End Sub

    '**** STEP 10. Add imgSearch handler
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""

        txtPage.Text = "1"
        Me.SearchBy = " StatusAdvance = 'AD' AND AdvanceTransaction.BranchId = '" & Me.sesBranchId.Trim.Replace("'", "") & "'"

        '*** Search By
        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"
            End If
        End If

        If txtAging.Text.Trim() <> "" Then
            'Me.SearchBy = Me.SearchBy & " and Aging >= " & txtAging.Text.Trim
            Dim dtAdvanceDateCriteria As Date

            dtAdvanceDateCriteria = Me.BusinessDate.AddDays(CDbl(txtAging.Text.Trim) * -1)

            Me.SearchBy = Me.SearchBy & " and AdvanceDate <= '" & dtAdvanceDateCriteria.Month.ToString() & "/" & dtAdvanceDateCriteria.Day.ToString() & "/" & dtAdvanceDateCriteria.Year.ToString() & "'"
        End If

        With m_oCustomClass
            .ParamReport = par
            .WhereCond = par
        End With

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    '**** STEP 11. Add datagrid sort command handler
    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    '*** STEP 12. Add datagrid item hyperlink on click handler
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim hyTemp As HyperLink
        'Dim hyTempDetail As HyperLink

        If e.Item.ItemIndex >= 0 Then
            'If CheckFeature(Me.Loginid, Me.FormID, "ADVNO", Me.AppId) Then
            hyAdvanceNo = CType(e.Item.FindControl("hyAdvanceNo"), HyperLink)
            hyAdvanceNo.NavigateUrl = "javascript:OpenWinMain('" & Me.sesBranchId.Trim.Replace("'", "") & "','" & hyAdvanceNo.Text.Trim & "')"
            'End If

            'If CheckFeature(Me.Loginid, Me.FormID, "RETRN", Me.AppId) Then
            hyTemp = CType(e.Item.FindControl("hyAction"), HyperLink)
            hyTemp.NavigateUrl = "AdvanceReturnView.aspx?BranchId=" + Server.UrlEncode(Me.sesBranchId.Trim.Replace("'", "")) + "&AdvanceNo=" + Server.UrlEncode(hyAdvanceNo.Text.Trim)

            'hyTempDetail = CType(e.Item.FindControl("hyActionDetail"), HyperLink)
            'hyTempDetail.NavigateUrl = "AdvanceTransactionDetail.aspx?BranchId=" + Server.UrlEncode(Me.sesBranchId.Trim.Replace("'", "")) + "&AdvanceNo=" + Server.UrlEncode(hyAdvanceNo.Text.Trim)
            'End If
        End If
    End Sub

End Class