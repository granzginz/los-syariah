﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class AdvanceInquiryView
    Inherits WebBased
#Region "Property"
    Private Property AdvanceNo() As String
        Get
            Return (CType(Viewstate("AdvanceNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AdvanceNo") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AdvanceRequest
    Private oController As New AdvanceRequestController

#End Region



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ADVNOVIEW"

            'If CheckFeature(Me.Loginid, Request.QueryString("ParentFormId"), "ADVNO", Me.AppId) Then

            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                lblMessage.Visible = False
                Me.BranchID = Request.QueryString("BranchId")
                Me.AdvanceNo = Request.QueryString("AdvanceNo")
                DoBind()
            End If
            'End If            
        End If
    End Sub

#Region "DoBind"
    Private Sub DoBind()
        Try
            With oCustomClass
                '*** Get advance transaction info
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .AdvanceNo = Me.AdvanceNo.Trim
                .strConnection = GetConnectionString

                oCustomClass = oController.GetAnAdvanceTransactionRecord(oCustomClass)

                '*** PETTY CASH ADVANCE INFO
                lblAdvanceNo.Text = .AdvanceNo
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .VoucherNoAdvance
                lblDate.Text = .AdvanceDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.AmountString, 2)

                '*** RETURN INFORMATION
                lblReturnToAccount.Text = .ReturnToAccount
                lblCashier.Text = .CashierReturn
                lblRefVoucherNoReturn.Text = .VoucherNoReturn

                '*** STATUS
                lblStatus.Text = .AdvanceStatus
                lblCashierStatus.Text = .CashierAdvance
                lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")

                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

End Class