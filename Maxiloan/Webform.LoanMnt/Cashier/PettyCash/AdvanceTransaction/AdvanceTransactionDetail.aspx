﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceTransactionDetail.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.AdvanceTransactionDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../../../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../../../Webform.UserController/ucLookUpTransaction.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Petty Cash Advance</title>
       <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>

    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
     <link href="../../../../Include/jquery-ui.css" rel = "Stylesheet" type="text/css" /> 
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>

    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
               Petty Cash Advance Return
            </h3>
        </div>
    </div>

      <div class="form_box">
            <div>
                <div class ="form_left">
                    <label>Advance No.</label>
                   <asp:label id="lblAdvanceNo" runat="server"></asp:label>
                </div>
                <div class ="form_right">
                    <label >Ref. Voucher No.</label>
                   <asp:label id="lblRefVoucherNo" runat="server"></asp:label>
                </div>
            </div>
        </div>

         <div class="form_box">
            <div>
                <div class ="form_left">
                    <label>Employee Name</label>
               <asp:label id="lblEmployeeName" runat="server"></asp:label>
                </div>
                <div class ="form_right">
                    <label >Date</label>
                   <asp:label id="lblDate" runat="server" Width="136px"></asp:label>
                </div>
            </div>
        </div>


           <div class="form_box">
            <div>
                <div class ="form_left">
                    <label>Bank Account</label>
              <asp:label id="lblBankAccount" runat="server" ></asp:label>
                    <asp:HiddenField ID="hdBankAccId" runat="server" />
                </div>
                <div class ="form_right">
                    <label >Amount</label>
                   <asp:label id="lblAmount" runat="server" >0</asp:label>
                </div>
            </div>
        </div>

            <div class="form_box">
            <div>
                <div class ="form_left">
                    <label>Description</label>
              <asp:label id="lblDescription" runat="server" ></asp:label>
                </div>
                <div class ="form_right">
                    <label >Departemen</label>
                 <asp:label id="lbldepartement" runat="server" ></asp:label>
                    <asp:HiddenField ID="hdDepID" runat="server" />
                </div>
            </div>
        </div>

            <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>DETAIL TRANSAKSI</strong></label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
        <asp:Panel ID="pnlGrid" runat="server" Visible="False">
            <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>

        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                  <asp:GridView ID="dtgCSV" runat="server" CssClass="grid_general" BorderStyle="None"
                BorderWidth="0px" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="Id" HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid" 
                        GridLines="Horizontal">
                      <AlternatingRowStyle HorizontalAlign="Center" VerticalAlign="Top" />
                <Columns>
                       <asp:TemplateField HeaderText="Action">
                           <ItemTemplate>
                              <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../../Images/IconDelete.gif"
                                        CommandName="Delete"  CommandArgument='<%# eval("Id") %>' OnClientClick="return confirm('Are you sure you want to delete?');"></asp:ImageButton>
                           </ItemTemplate>
                           <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="80px" />
                           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="No">
                        <ItemTemplate>
               
             
                                <asp:Label ID="lblSeq" runat="server" Text='<%# eval("Seq") %>'>
                                    </asp:Label>
                                 </ItemTemplate>
                           <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="50px" />
                           <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="COA">
                           <ItemTemplate>
                                <asp:Label ID="lblTransactionName" runat="server" Text='<%#eval("PaymentAllocationName")%>'>
                                    </asp:Label>
                                     <asp:Label ID="lblTransactionId" runat="server" Text='<%#eval("PaymentAllocationId")%>' Visible="false">
                                    </asp:Label>
                           </ItemTemplate>
                           <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                           <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="400px" />
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Jumlah">
                           <ItemTemplate>
                               <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(eval("Amount"),2)%>'>
                                    </asp:Label>
                           </ItemTemplate>
                           <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="150px" />
                           <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Keterangan">
                           <ItemTemplate>
                               <asp:Label ID="lblket" runat="server" Text='<%# eval("Description")%>'>
                                    </asp:Label>
                           </ItemTemplate>
                           <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="300px" />
                           <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                       </asp:TemplateField>
                      </Columns>

<HeaderStyle CssClass="th"></HeaderStyle>

<RowStyle CssClass="item_grid" HorizontalAlign="Center" VerticalAlign="Middle"></RowStyle>
            </asp:GridView>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="false" Text="Save" CssClass="small button blue">
            </asp:Button>
        </div>
       </asp:Panel>
    </form>
</body>
</html>
