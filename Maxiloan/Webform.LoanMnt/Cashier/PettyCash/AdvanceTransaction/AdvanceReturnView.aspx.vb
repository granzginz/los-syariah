
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class AdvanceReturnView
    Inherits Maxiloan.Webform.AccMntWebBased
    'Protected WithEvents oTrans As ucLookUpTransaction
    Protected WithEvents txtAmount As ucNumberFormat
    Dim LimitPettyCashTrans As Decimal = 0
    '#Region " Web Form Designer Generated Code "

    '    'This call is required by the Web Form Designer.
    '    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    '    End Sub
    '    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblAdvanceNo As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblRefVoucherNo As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblEmployeeName As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblDate As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblBankAccount As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblAmount As System.Web.UI.WebControls.Label
    '    Protected WithEvents lblDescription As System.Web.UI.WebControls.Label
    '    Protected WithEvents pnlbutton As System.Web.UI.WebControls.Panel
    '    Protected WithEvents pnlList As System.Web.UI.WebControls.Panel
    '    Protected WithEvents cmbBankAccount As System.Web.UI.WebControls.DropDownList
    '    Protected WithEvents imgSave As System.Web.UI.WebControls.ImageButton
    '    Protected WithEvents cboBankAccountType As System.Web.UI.WebControls.DropDownList
    '    Protected WithEvents imbCancel As System.Web.UI.WebControls.ImageButton
    '    Protected WithEvents rfvReturnAccount As System.Web.UI.WebControls.RequiredFieldValidator

    '    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    '    'Do not delete or move it.
    '    Private designerPlaceholderDeclaration As System.Object

    '    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    '        'CODEGEN: This method call is required by the Web Form Designer
    '        'Do not modify it using the code editor.
    '        InitializeComponent()
    '    End Sub

    '#End Region

#Region "PrivateConst"
    Private Const STR_FRM_ID As String = "AdvanceRequest"
    Private Const STR_PARENT_FORM_ID As String = "ADVRTN"

    Private Const STR_BANK_ACC_TYPE As String = "BankAccountType"
    Private Const STR_EMPLOYEE_ID As String = "EmployeeId"
    Private Const STR_EMPLOYEE_NAME As String = "EmployeeName"
    Private Const STR_CURR_DATE As String = "CurrentDate"
    Private Const STR_AMOUNT As String = "Amount"
    Private Const STR_DESC As String = "Description"
    Private Const STR_BANK_ACC_NAME As String = "BankAccountName"
    Private Const STR_BANK_ACC_ID As String = "BankAccountId"
    Private Const STR_BG_STATUS_NORMAL As String = "N"

    Private Const STR_FILE_ARBS As String = "AdvanceRequestBeforeSave.aspx"
    Private Const STR_PETTY_CASH_PURPOSE As String = "PC"
    Private Const STR_CACHE_BANK_ACC As String = "BankAccountCache"

    Private m_EmployeeId As String
    Private m_BankAccountID As String
    Private m_BankAccountName As String
    Private m_CTAdvReq As Controller.AdvanceRequestController
    Private m_ETAdvReq As Parameter.AdvanceRequest
    Private oBankController As New DataUserControlController

#End Region

#Region "Property"
    Private Property AdvanceNo() As String
        Get
            Return (CType(Viewstate("AdvanceNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AdvanceNo") = Value
        End Set
    End Property
    Private Property ParentFormId() As String
        Get
            Return (CType(Viewstate("ParentFormId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParentFormId") = Value
        End Set
    End Property
    Private Property EmployeeId() As String
        Get
            Return (CType(Viewstate("EmployeeId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("EmployeeId") = Value
        End Set
    End Property
    Public Property ARAmount() As Double
        Get
            Return CDbl(ViewState("ARAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("ARAmount") = Value
        End Set
    End Property
    Public Property IsPaymentReceive() As String
        Get
            Return CType(ViewState("IsPaymentReceive"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsPaymentReceive") = Value
        End Set
    End Property
    Public Property IsPettyCash() As String
        Get
            Return CType(ViewState("IsPettyCash"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsPettyCash") = Value
        End Set
    End Property
    Public Property IsAgreement() As String
        Get
            Return CType(ViewState("IsAgreement"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsAgreement") = Value
        End Set
    End Property
    Public Property IsHOTransaction() As String
        Get
            Return CType(ViewState("IsHOTransaction"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsHOTransaction") = Value
        End Set
    End Property
    Public Property ProcessID() As String
        Get
            Return CType(ViewState("ProcessID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProcessID") = Value
        End Set
    End Property
    Public Property Amount() As Double
        Get
            Return CType(txtAmount.Text.Trim, Double)
        End Get
        Set(ByVal Value As Double)
            txtAmount.Text = FormatNumber(CStr(Value), 2)
        End Set
    End Property
    Public Property Description() As String
        Get
            Return CType(txtDescription.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtDescription.Text = Value
        End Set
    End Property
    'Public Property TransactionID() As String
    '    Get
    '        Return CType(hdnTransactionID.Value.Trim, String)
    '    End Get
    '    Set(ByVal Value As String)
    '        hdnTransactionID.Value = Value
    '    End Set
    'End Property
    'Public Property Transaction() As String
    '    Get
    '        Return CType(Transaction.Text.Trim, String)
    '    End Get
    '    Set(ByVal Value As String)
    '        txtTransaction.Text = Value
    '    End Set
    'End Property
    Public Property TransactionID() As String
        Get
            Return CType(ViewState("TransactionID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TransactionID") = Value
        End Set
    End Property
    Public Property Transaction() As String
        Get
            Return CType(ViewState("Transaction"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Transaction") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AdvanceRequest
    Private oController As New Controller.AdvanceRequestController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "PCTRS"
        Session.Add("ConString", GetConnectionString())
        Session.Add("BranchID", Me.sesBranchId.Replace("'", ""))

        If Not IsPostBack Then
            'Me.FormID = "ADVRTNVIEW"


            'If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If CheckFeature(Me.Loginid, STR_PARENT_FORM_ID, "RETRN", Me.AppId) Then
                lblMessage.Visible = False
                Me.BranchID = Request.QueryString("BranchId")
                Me.AdvanceNo = Request.QueryString("AdvanceNo")

                BindBankAccount()
                DoBind()
                BtnCancel.Attributes.Add("onclick", "GoBack();")
                InitialDefaultPanel()
            End If
            'End If
        End If
    End Sub

#Region "DoBind"
    Private Sub BindBankAccount()
        Try
            Dim strCacheName As String
            strCacheName = STR_CACHE_BANK_ACC & Me.sesBranchId.Replace("'", "") & STR_PETTY_CASH_PURPOSE
            Dim dtBankAccount As New DataTable
            dtBankAccount = CType(Me.Cache.Item(strCacheName), DataTable)
            If dtBankAccount Is Nothing Then
                Dim dtBankAccountCache As New DataTable
                dtBankAccountCache = oBankController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), _
                                    "", STR_PETTY_CASH_PURPOSE)
                Me.Cache.Insert(strCacheName, dtBankAccountCache, Nothing, DateTime.Now.AddHours(CommonCacheHelper.DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                dtBankAccount = CType(Me.Cache.Item(strCacheName), DataTable)
            End If
            cmbBankAccount.DataTextField = "Name"
            cmbBankAccount.DataValueField = "ID"
            cmbBankAccount.DataSource = dtBankAccount
            cmbBankAccount.DataBind()

            cmbBankAccount.Items.Insert(0, "Select One")
            cmbBankAccount.Items(0).Value = "0"

            With cboBankAccountType
                .DataValueField = "ID"
                .DataTextField = "Type"
                .DataSource = cmbBankAccount.DataSource
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With

            cboBankAccountType.SelectedIndex = cmbBankAccount.SelectedIndex
        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub

    Private Sub DoBind()
        Try
            With oCustomClass
                '*** Get advance transaction info
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .AdvanceNo = Me.AdvanceNo.Trim
                .strConnection = GetConnectionString

                oCustomClass = oController.GetAnAdvanceTransactionRecord(oCustomClass)

                '*** PETTY CASH ADVANCE INFO
                lblAdvanceNo.Text = .AdvanceNo
                Me.EmployeeId = .EmployeeId
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .VoucherNoAdvance
                lblDate.Text = .AdvanceDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.AmountString, 2)
                'txtAmount.Text = "0"
                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region
    Private Sub DisplayError(ByVal strErrMsg As String)
        ShowMessage(lblMessage, strErrMsg, True)
        'With lblMessage
        '    .Text = strErrMsg
        '    .Visible = True
        'End With
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try
            If Not CheckFeature(Me.Loginid, STR_PARENT_FORM_ID, "SAVE", Me.AppId) Then
                Exit Sub
            End If
            If Not CheckCashier(Me.Loginid) Then
                DisplayError("Cashier Not Open")
                Exit Sub
            End If
            'If m_ETAdvReq Is Nothing Then m_ETAdvReq = New Parameter.AdvanceRequest
            'With m_ETAdvReq
            '    .strConnection = GetConnectionString
            '    .BranchId = Me.sesBranchId.Trim.Replace("'", "")
            '    .AdvanceNo = lblAdvanceNo.Text.Trim
            'End With
            'm_ETAdvReq = m_CTAdvReq.GetAnAdvanceTransactionRecord(m_ETAdvReq)
            'If m_ETAdvReq Is Nothing Then
            '    DisplayError("Can not process advance return. Advance data not found.")
            '    Exit Sub
            'End If
            'If m_ETAdvReq.AdvanceStatus.Trim.ToUpper = "RETURN" Then
            '    DisplayError("Can not process advance return. Advance data already returned.")
            '    Exit Sub
            'End If
            If DtgPCList.Items.Count > 0 Then
                Dim Ndttable As DataTable
                Dim pStrFile As String
                Dim strIDTrans As String
                Dim strDescTrans As String
                Dim strAmountTrans As String
                Dim lblID As Label
                Dim lblDesc As Label
                Dim lblAmountG As Label

                Dim AmountTotal As Double

                strIDTrans = ""
                strDescTrans = ""
                strAmountTrans = ""

                For intLoopGrid = 0 To DtgPCList.Items.Count - 1

                    lblID = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblTransactionID2"), Label)
                    lblDesc = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblDESCRIPTION"), Label)
                    lblAmountG = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblPCAmount"), Label)

                    strIDTrans &= CStr(IIf(strIDTrans = "", "", ",")) & lblID.Text.Replace("'", "")
                    strDescTrans &= CStr(IIf(strDescTrans = "", "", ",")) & lblDesc.Text.Replace("'", "")
                    strAmountTrans &= CStr(IIf(strAmountTrans = "", "", ",")) & lblAmountG.Text.Replace("'", "").Replace(",", "")
                    AmountTotal += FormatNumber(CDbl(lblAmountG.Text), 2)
                Next

                If lblAmount.Text <> FormatNumber(AmountTotal, 2) Then
                    ShowMessage(lblMessage, "Total Cash Advance Request Tidak sesuai", True)
                    Exit Sub
                End If

                With m_ETAdvReq
                    .strConnection = GetConnectionString()
                    .BranchId = Me.sesBranchId.Trim.Replace("'", "")
                    .BankAccountID = cmbBankAccount.SelectedValue
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .AmountString = lblAmount.Text
                    .CoyID = Me.SesCompanyID
                    .AppID = ""
                    .EmployeeId = Me.EmployeeId
                    .EmployeeName = lblEmployeeName.Text.Trim
                    .AdvanceNo = lblAdvanceNo.Text.Trim
                    .BankAccountType = cmbBankAccount.SelectedValue
                    .Description = lblDescription.Text
                    .TransactionID = Me.TransactionID
                    .TransactionName = Me.Transaction
                    .Amount = Me.Amount
                    .Keterangan = Me.Description
                    '.TransactionID = oTrans.TransactionID
                    '.TransactionName = oTrans.Transaction
                    '.Amount = oTrans.Amount
                    '.Keterangan = oTrans.Description
                End With

                m_CTAdvReq.SaveAdvanceReturnTransaction(m_ETAdvReq)

                Server.Transfer("AdvanceReturn.aspx?Message=" & Server.UrlEncode("Record saved"))
            Else

                ShowMessage(lblMessage, "Tidak ada data", True)
                Exit Sub

            End If

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Sub New()
        m_ETAdvReq = New Parameter.AdvanceRequest
        m_CTAdvReq = New Controller.AdvanceRequestController
    End Sub

    Private Sub cmbBankAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBankAccount.SelectedIndexChanged
        Try
            cboBankAccountType.SelectedIndex = cmbBankAccount.SelectedIndex
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        pnlGrid.Visible = False
        pnlList.Visible = True
        BtnCancel.Visible = True
        txtAmount.Text = "0"
        Try
            Server.Transfer("AdvanceReturn.aspx")
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub


    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then

            If Not CheckCashier(Me.Loginid) Then
                ShowMessage(lblMessage, "Kasir Belum buka", True)
                btnSave.Visible = False
            Else
                lblMessage.Text = ""
                'If CDbl(txtAmount.Text) <= 0 Then

                '    ShowMessage(lblMessage, "Jumlah Harus  > 0", True)
                '    Exit Sub
                'End If
                'If CDbl(txtAmount.Text) > getLimitPettycashTrans() Then
                '    ShowMessage(lblMessage, "Jumlah Harus  <= " & getLimitPettycashTrans(), True)
                '    Exit Sub
                'End If

                'lblEmployeeName.Text = txtEmployeeName.Text
                'lblDate.Text = lblBussinessDate.Text.Trim
                'lblBankAccount.Text = cboBank.SelectedItem.Text.Trim
                'lblDepartement.Text = cboDepartement.SelectedItem.Text.Trim
                'lblDescription.Text = txtDescription.Text
                'lblAmount.Text = FormatNumber(txtAmount.Text, 2)


                'Dim BController As New TransferAccountController
                'Dim BCustomClass As New TransferAccount

                'BCustomClass.strConnection = GetConnectionString()
                'BCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                'BCustomClass.bankaccountid = cboBankAccountType.SelectedIndex
                'BCustomClass = BController.GetBankType(BCustomClass)

                'With BCustomClass
                '    Me.BankType = .bankAccountType
                '    If .bankAccountType = "B" Then
                '        GetComboBilyet()
                '        pnlBG.Visible = True
                '    Else
                '        pnlBG.Visible = False
                '    End If
                'End With

                'Me.ARAmount = CDbl(lblAmount.Text)

                'GetTransactionData()
                'Me.TransactionID = ""
                'Me.Description = lblDescription.Text
                'Me.Amount = CDbl(lblAmount.Text)
                'Me.Transaction = ""
                'BindDataTransaction()
                'With oTrans
                '    .TransactionID = ""
                '    .Description = ""
                '    .Amount = 0
                '    .BindData()
                'End With

                'With oTrans
                '    .TransactionID = ""
                '    .Description = lblDescription.Text
                '    .Amount = CDbl(lblAmount.Text)
                '    .BindData()
                'End With

                pnlList.Visible = True
                pnlGrid.Visible = False
                PnlHeader.Visible = True
            End If
        End If
    End Sub
    'Public Sub BindDataTransaction()
    '    txtTransaction.Text = Me.Transaction
    '    txtDescription.Text = Me.Description
    'End Sub
    Private Sub GetTransactionData()
        Me.ProcessID = "PCTRS"
        Me.IsAgreement = "0"
        Me.IsPettyCash = "0"
        Me.IsHOTransaction = "0"
        Me.IsPaymentReceive = "0"
        'BindDataTransaction()

        'If Me.IsHoBranch Then
        '    With oTrans
        '        .ProcessID = "PCTRS"
        '        .IsAgreement = "0"
        '        .IsPettyCash = "1"
        '        .IsHOTransaction = "1"
        '        .IsPaymentReceive = "0"
        '        .Style = "AccMnt"
        '        .BindData()
        '    End With
        'Else
        '    With oTrans
        '        .ProcessID = "PCTRS"
        '        .IsAgreement = "0"
        '        .IsPettyCash = "0"
        '        .IsHOTransaction = "0"
        '        .IsPaymentReceive = "0"
        '        .Style = "AccMnt"
        '        .BindData()
        '    End With
        'End If
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("AdvanceReturn.aspx")
    End Sub
    Private Sub btnCancelNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        Server.Transfer("AdvanceReturn.aspx")
    End Sub
    Private Sub InitialDefaultPanel()
        PnlHeader.Visible = True
        pnlList.Visible = False
        pnlGrid.Visible = False
    End Sub
    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then

            Dim pStrFile As String
            Me.FormID = "PCTRANS"
            Dim oCustomClass As New PettyCash
            Dim dt As New DataTable
            lblMessage.Text = ""
            If Me.TransactionID = "" Then

                ShowMessage(lblMessage, "Harap pilih Transaksi", True)
                Exit Sub
            ElseIf Me.Amount <= 0 Then

                ShowMessage(lblMessage, "Nilai Transaksi harus > 0 ", True)
                Exit Sub
            ElseIf Me.Description = "" Then

                ShowMessage(lblMessage, "Harap isi Keterangan", True)
                Exit Sub

            End If
            With oCustomClass
                pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = .LoginId
                .strConnection = GetConnectionString()
                .TransactionID = Me.TransactionID
                .TransactionName = Me.Transaction
                .Amount = Me.Amount
                .Description = Me.Description
                .FlagDelete = "0"
                .NumPC = DtgPCList.Items.Count
            End With
            'oCustomClass = oPCController.GetTablePC(oCustomClass, pStrFile)
            'If Not oCustomClass.isValidPC Then

            '    ShowMessage(lblMessage, "Transaksi sudah ada", True)
            '    Exit Sub
            'Else
            '    DtgPCList.DataSource = oCustomClass.ListPC
            '    DtgPCList.DataBind()
            '    DtgPCList.Visible = True
            '    pnlList.Visible = False
            '    pnlList.Visible = True
            '    pnlGrid.Visible = True
            'End If
            BtnCancel.Visible = False
            Me.TransactionID = ""
            Me.Transaction = ""
            Me.Amount = 0
            Me.Description = ""
            'BindDataTransaction()
        End If
    End Sub
    Private Function getLimitPettycashTrans() As Decimal
        If LimitPettyCashTrans = 0 Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "LimitPettycash"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return LimitPettyCashTrans
        End If
    End Function
End Class
