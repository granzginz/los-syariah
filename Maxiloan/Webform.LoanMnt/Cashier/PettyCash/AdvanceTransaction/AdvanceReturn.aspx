﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceReturn.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.AdvanceReturn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../../../Webform.UserController/ucBranchHO.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PettyCashReturn</title>


    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinMain(BranchId, AdvanceNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/AdvanceTransaction/AdvanceInquiryView.aspx?BranchId=' + BranchId + '&AdvanceNo=' + AdvanceNo, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>

</head>

<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlList" runat="server">
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>


            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>ADVANCE RETURN
                    </h3>
                </div>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Find By</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="AdvanceNo">Advance No.</asp:ListItem>
                            <asp:ListItem Value="EmployeeName">Employee Name</asp:ListItem>
                            <asp:ListItem Value="Description">Description</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server" CssClass="inptype" Width="88px"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>
                            Aging
                        </label>
                        <asp:TextBox ID="txtAging" runat="server" CssClass="inptype"></asp:TextBox>
                        <asp:Label ID="Label4" runat="server">day(s)</asp:Label>
                        <asp:RegularExpressionValidator ID="revAging" runat="server" Display="Dynamic" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                            ControlToValidate="txtAging" ErrorMessage="You have to fill numeric value in Aging"></asp:RegularExpressionValidator>
                    </div>
                </div>

            </div>
            <div class="form_button">
                <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue"></asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">

            <div class="form_title">

                <div class="form_single">
                    <h3>List Of Petty Cash Advance
                    </h3>
                </div>
            </div>

            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" Visible="False" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PERFORM">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyAction" runat="server" Text='RETURN'></asp:HyperLink>
                                        <%--<ASP:HYPERLINK id="hyActionDetail" runat="server" text='ADD DETAIL'></ASP:HYPERLINK>--%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AdvanceNo" HeaderText="ADVANCE NO.">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyAdvanceNo" runat="server" Text='<%#Container.DataItem("AdvanceNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="EMPLOYEE NAME">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Container.DataItem("EmployeeName")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Description" HeaderText="DESCRIPTION">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Container.DataItem("Description") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Amount" HeaderText="AMOUNT">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("AmountReceive"),2)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Aging" HeaderText="AGING">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAging" runat="server" Text='<%#Container.DataItem("Aging")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="AdvanceDate" SortExpression="AdvanceDate" HeaderText="ADVANCE DATE" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                </asp:BoundColumn>

                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="First" ImageUrl="../../../../Images/grid_navbutton01.png"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="Prev" ImageUrl="../../../../Images/grid_navbutton02.png"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="Next" ImageUrl="../../../../Images/grid_navbutton03.png"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="Last" ImageUrl="../../../../Images/grid_navbutton04.png"></asp:ImageButton>
                            Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                                Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ErrorMessage="No Halaman Salah"
                                ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
