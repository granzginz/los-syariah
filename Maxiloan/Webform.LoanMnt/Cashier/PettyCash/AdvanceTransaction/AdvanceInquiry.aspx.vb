﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region
Public Class AdvanceInquiry
    Inherits AccMntWebBased
    Protected WithEvents hyPettyCashNo As System.Web.UI.WebControls.HyperLink

#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property
    Public ReadOnly Property CustomClassWhereCond() As String
        Get
            Return Me.SearchBy
        End Get
    End Property
    Public ReadOnly Property CustomClassSortBy() As String
        Get
            Return Me.SortBy
        End Get
    End Property
    Public ReadOnly Property CustomClassBusinessDate() As Date
        Get
            Return Me.BusinessDate
        End Get
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AdvanceRequest
    Private oController As New Controller.AdvanceRequestController
    Private m_BranchController As New DataUserControlController
#End Region

    Private Property hyAdvanceNo As HyperLink

    Private Sub FillComboWithBranches(ByVal cbo As WebControls.DropDownList, ByVal duccController As DataUserControlController)
        Try
            Dim dtbranch As New DataTable
            dtbranch = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cbo
                If Me.IsHoBranch Then
                    .DataSource = duccController.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                Else
                    .DataSource = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                lblMessage.Text = Request.QueryString("message")
            End If
            Me.FormID = "ADVINQ"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                FillComboWithBranches(cboParent, m_BranchController)
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .BusinessDate = Me.BusinessDate
            End With

            'DtUserList = New DataTable
            'DvUserList = New DataView

            oCustomClass.PettyCashAdvanceList = oController.GetPettyCashAdvanceList(oCustomClass).PettyCashAdvanceList

            If oCustomClass Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            DtUserList = oCustomClass.PettyCashAdvanceList
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            'DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True

            lblMessage.Text = ""

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub



#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data Not Found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnReset.Click
        Me.SearchBy = ""
        FillComboWithBranches(cboParent, m_BranchController)
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        txtAging.Text = ""
        cboStatus.SelectedIndex = 0
        DoBind(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = False
    End Sub

    'Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
    '    Me.SearchBy = ""
    '    cboSearchBy.SelectedIndex = 0
    '    txtSearchBy.Text = ""
    '    cboStatus.SelectedIndex = 0
    '    txtDate.Text = ""
    '    FillComboWithBranches(cboParent, m_BranchController)
    '    pnlDatagrid.Visible = False
    '    'DoBind(Me.SearchBy, Me.SortBy)        
    'End Sub



    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String

        strFilterBy = ""
        txtPage.Text = "1"
        Me.SearchBy = ""
        If (cboParent.SelectedItem.Value <> "0" And cboParent.SelectedItem.Value <> "ALL") Then
            Me.SearchBy = "  AdvanceTransaction.branchid = " & "'" & cboParent.SelectedValue.Trim & "'"
        End If

        If cboStatus.SelectedIndex > 0 Then
            Me.SearchBy = Me.SearchBy & " and StatusAdvance = '" & cboStatus.SelectedItem.Value & "' "
            strFilterBy = strFilterBy & ", Status = " & cboStatus.SelectedItem.Value
        End If

        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"
                strFilterBy = strFilterBy & ", " & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
            End If
        End If

        If txtAging.Text.Trim() <> "" Then
            'Me.SearchBy = Me.SearchBy & " and Aging >= " & txtAging.Text.Trim
            Dim dtAdvanceDateCriteria As Date

            dtAdvanceDateCriteria = Me.BusinessDate.AddDays(CDbl(txtAging.Text.Trim) * -1)

            Me.SearchBy = Me.SearchBy & " and (AdvanceDate <= '" & dtAdvanceDateCriteria.ToString("MM/dd/yyyy") & "')"
            strFilterBy = strFilterBy & ", Aging = " & txtAging.Text.Trim
        End If

        With oCustomClass
            .ParamReport = par
            .WhereCond = par
            Me.ParamReport = strFilterBy
        End With

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label

        If e.Item.ItemIndex >= 0 Then
            'If CheckFeature(Me.Loginid, Me.FormID, "ADVNO", Me.AppId) Then
            hyAdvanceNo = CType(e.Item.FindControl("hyAdvanceNo"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblBranchId"), Label)
            If Not lblTemp Is Nothing Then
                hyAdvanceNo.NavigateUrl = "javascript:OpenWinMain('" & lblTemp.Text.Trim & "','" & hyAdvanceNo.Text.Trim & "')"
            End If
            'End If
        End If
    End Sub
End Class

