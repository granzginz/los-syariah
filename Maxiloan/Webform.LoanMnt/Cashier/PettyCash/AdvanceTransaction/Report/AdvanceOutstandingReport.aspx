<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceOutstandingReport.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.AdvanceOutstandingReport" %>

<%@ Register TagPrefix="uc1" TagName="ucdatece" Src="../../../../../Webform.UserController/ucdatece.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCRInquiry</title>
    <script src="../../../../../Maxiloan.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">    
        function OpenWinMain(BranchId, AdvanceNo) {
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/AdvanceTransaction/AdvanceInquiryView.aspx?BranchId=' + BranchId + '&AdvanceNo=' + AdvanceNo, null, 'left=50, top=50, width=900, height=300, menubar=0,scrollbars=1')
        }
    </script>
    <link href="../../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="Form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>


        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>Oustanding Advance Petty Cash Advance Report
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Branch</label>
                <asp:DropDownList ID="cboParent" runat="server"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Please Select Branch"
                    ControlToValidate="cboParent" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Aging Date</label>
                <uc1:ucdatece ID="oAgingDate" runat="server"></uc1:ucdatece>
                <asp:Label ID="lblSign" runat="server"></asp:Label>
                <asp:Label ID="lblMsgAgingDate" runat="server" Visible="False" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button runat="server" ID="btnViewReport" Text="View" CssClass="small button blue" />
        </div>
    </form>
</body>
</html>
