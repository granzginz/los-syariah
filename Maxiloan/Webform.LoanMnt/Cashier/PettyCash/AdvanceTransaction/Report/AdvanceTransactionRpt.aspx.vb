
#Region "Imports"
Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Partial Class AdvanceTransactionRpt
    Inherits AccMntWebBased

    Protected WithEvents sdate As ucDateCE
    Protected WithEvents edate As ucDateCE

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

#End Region
#Region "constanta"
    Private m_controller As New DataUserControlController
    Private oControllerChild As New DataUserControlController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "RPTADVTRANS"

        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                sdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                edate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


                Dim dtbranch As New DataTable
                Dim dtEmp As New DataTable
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                With oEntitesChild
                    .strConnection = GetConnectionString()
                    .BranchId = Me.sesBranchId

                End With
                dtEmp = oControllerChild.ViewEmployee(GetConnectionString)
                Response.Write(GenerateScript(dtEmp))
            End If
        End If
    End Sub
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("EmployeeName")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("EmployeeName")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
            Dim cmdwhere As String
            Dim filterBy As String
            filterBy = ""

            cmdwhere = "AdvanceTransaction.branchID='" & cboParent.SelectedItem.Value.Trim & "' And "

            If hdnChildValue.Value.Trim = "" Or hdnChildValue.Value.Trim = "0" Then
            Else
                cmdwhere = cmdwhere + "AdvanceTransaction.EmployeeID = '" & hdnChildValue.Value.Trim & "' and "
                filterBy = filterBy + "Employee Name = " & hdnChildName.Value.Trim & " and "
            End If
            If cboStatus.SelectedItem.Value.Trim <> "0" Then
                cmdwhere = cmdwhere + "AdvanceTransaction.statusAdvance = '" & cboStatus.SelectedItem.Value.Trim & "' and "
                filterBy = filterBy + "Status = " & cboStatus.SelectedItem.Text.Trim & " and "
            End If
            filterBy = filterBy + "Branch : " & cboParent.SelectedItem.Text.Trim & ""
            cmdwhere = cmdwhere + " (AdvanceTransaction.AdvanceDate  between '" & ConvertDate2(sdate.Text.Trim) & "'  and " &
                        "  '" & ConvertDate2(edate.Text.Trim) & "')"

            Me.CmdWhere = cmdwhere
            Me.FilterBy = filterBy

            Dim cookie As HttpCookie = Request.Cookies("AdvanceTrans")
            If Not cookie Is Nothing Then
                cookie.Values("sdate") = "Period : " & ConvertDate2(sdate.Text).ToString("dd/MM/yyyy") & " To " & ConvertDate2(edate.Text).ToString("dd/MM/yyyy")
                cookie.Values("cmdwhere") = Me.CmdWhere
                cookie.Values("LoginID") = Me.Loginid
                cookie.Values("FilterBy") = Me.FilterBy
                cookie.Values("ReportType") = "RPTADVANCETRANSACTION"
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("AdvanceTrans")
                cookieNew.Values.Add("sdate", "Period : " & ConvertDate2(sdate.Text).ToString("dd/MM/yyyy") & " To " & ConvertDate2(edate.Text).ToString("dd/MM/yyyy"))
                cookieNew.Values.Add("cmdwhere", Me.CmdWhere)
                cookieNew.Values.Add("LoginID", Me.Loginid)
                cookieNew.Values.Add("FilterBy", Me.FilterBy)
                cookieNew.Values.Add("ReportType", "RPTADVANCETRANSACTION")
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("AdvanceTransactionViewer.aspx")
        End If
    End Sub

End Class
