<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OutstandingAdvanceTransactionRpt.aspx.vb" Inherits="Maxiloan.webform.LoanMnt.OutstandingAdvanceTransactionRpt" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="ucdatece" Src="../../../../../Webform.UserController/ucdatece.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OutstandingAdvanceTransactionRpt</title>
    <script language="javascript" type="text/javascript">

        var hdnDetail;
        var hdndetailvalue;
        function ParentChange(pBranch, pEmployee, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pEmployee).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pEmployee).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 1;
            };
            eval('document.forms[0].' + pEmployee).options[0] = new Option('ALL', '0');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pEmployee).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pEmployee).selected = true;
            }
        };

        function cboChildonChange(l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
        }

    </script>
    <link href="../../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>


        <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
        <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>REPORT OUTSTANDING ADVANCE TRANSACTION
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Branch</label>
                <asp:DropDownList ID="cboParent" onchange="<%#BranchIDChange()%>" runat="server"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" ControlToValidate="cboParent"
                    ErrorMessage="Please Select Branch" InitialValue="0"></asp:RequiredFieldValidator>

            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Employee</label>
                <asp:DropDownList ID="cboChild" onchange="cboChildonChange(this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);"
                    runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Aging Date</label>
                <uc1:ucdatece ID="AgingDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_button">
            <asp:Button runat="server" ID="btnViewReport" Text="View" CssClass="small button blue" />
        </div>
    </form>
</body>
</html>
