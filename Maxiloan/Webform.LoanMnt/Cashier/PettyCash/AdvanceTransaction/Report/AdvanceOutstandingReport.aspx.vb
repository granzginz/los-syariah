#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Partial Class AdvanceOutstandingReport
    Inherits AccMntWebBased
    Protected WithEvents oAgingDate As ucDateCE

#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property
    Public ReadOnly Property CustomClassWhereCond() As String
        Get
            Return Me.SearchBy
        End Get
    End Property
    Public ReadOnly Property CustomClassSortBy() As String
        Get
            Return Me.SortBy
        End Get
    End Property
    Public ReadOnly Property CustomClassBusinessDate() As Date
        Get
            Return Me.BusinessDate
        End Get
    End Property
#End Region


#Region "Constanta"
    Private m_BranchController As New DataUserControlController
#End Region

#Region "MemberVars"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New Controller.GeneralPagingController
#End Region
    Private Sub FillComboWithBranches(ByVal cbo As WebControls.DropDownList, ByVal duccController As DataUserControlController)
        Try
            Dim dtbranch As New DataTable
            dtbranch = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cbo
                If Me.IsHoBranch Then
                    .DataSource = duccController.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                Else
                    .DataSource = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub Setucdatece(ByVal oDate As ucDateCE, ByVal bRequired As Boolean)
        With oDate
            .IsRequired = bRequired
            '.ValidationErrMessage = "Date is not valid"
            '.Enable()
            '.isCalendarPostBack = False
        End With
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                lblMessage.Text = Request.QueryString("message")
            End If
            Me.FormID = "RPTADVAGING"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Setucdatece(oAgingDate, False)
                oAgingDate.Text = DateAdd(DateInterval.Day, -1, Me.BusinessDate).ToString("dd/MM/yyyy")

                FillComboWithBranches(cboParent, m_BranchController)
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Private Sub imbViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim dtStart As Date
        Dim dtEnd As Date
        Dim strSearchBy As String
        Dim strReportFilter As String

        strSearchBy = ""
        strReportFilter = ""
        If CheckFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
            If (oAgingDate.Text.Trim = "") Then
                lblMsgAgingDate.Visible = True
                lblMsgAgingDate.Text = "Aging Date is required!"
                Exit Sub
            End If
            strSearchBy = "AgingDate = '" & ConvertDate2(oAgingDate.Text) & "'"
            strReportFilter += strSearchBy

            Me.SearchBy = ""
            If (cboParent.SelectedItem.Value <> "0" And cboParent.SelectedItem.Value <> "ALL") Then
                Dim strOperator As String

                strOperator = String.Empty
                If strSearchBy.Trim.Length > 0 Then
                    strOperator = " AND "
                End If
                strSearchBy += strOperator + "  AdvanceTransaction.branchid = " & "'" & cboParent.SelectedValue.Trim & "'"
            End If

            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = strSearchBy
                cookie.Values("FilterBy") = strReportFilter
                cookie.Values("AgingDate") = oAgingDate.Text.Trim
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", strSearchBy)
                cookieNew.Values.Add("FilterBy", strReportFilter)
                cookieNew.Values.Add("AgingDate", oAgingDate.Text.Trim)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("rptAdvanceOutstandingViewer.aspx")
        End If
    End Sub
End Class
