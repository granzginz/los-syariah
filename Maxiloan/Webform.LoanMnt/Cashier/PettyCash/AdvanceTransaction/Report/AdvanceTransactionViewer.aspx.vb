
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Partial Class AdvanceTransactionViewer
    Inherits AccMntWebBased

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property sdate() As String
        Get
            Return CType(ViewState("sdate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("sdate") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return CType(ViewState("BankAccountName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountName") = Value
        End Set
    End Property
    Private Property ReportType() As String
        Get
            Return CType(ViewState("ReportType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ReportType") = Value
        End Set
    End Property


#End Region
    Private ocustomclass As New Parameter.GeneralPaging
    Private ocontroller As New GeneralPagingController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub BindReport()
        GetCookies()
        Dim ds As New DataSet
        Dim oDCReport As New Parameter.GeneralPaging
        Dim strconn As String = GetConnectionString()
        Dim reportID As String
        Dim sptype As String
        Dim objreport As ReportDocument

        Select Case Me.ReportType
            Case "RPTADVANCETRANSACTION"
                objreport = New RptAdvTrans
                sptype = "spAdvanceTransactionRpt"
                reportID = "RptAdvanceTransaction"
            Case "RPTOSADVANCETRANSACTION"
                objreport = New RptOSAdvTrans
                sptype = "spOutstandingAdvanceTransactionRpt"
                reportID = "RptOutstandingAdvanceTransaction"
        End Select
        With ocustomclass
            .strConnection = GetConnectionString()
            .WhereCond = Me.cmdwhere
            .SpName = sptype
        End With


        oDCReport = ocontroller.GetReportWithParameterWhereCond(ocustomclass)
        ds = oDCReport.ListDataReport

        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        objreport.SetDataSource(ds)


        CRVAdvanceTransaction.ReportSource = objreport
        CRVAdvanceTransaction.Visible = True
        CRVAdvanceTransaction.DataBind()

        '========================================================
        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues


        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("PrgId")
        discrete = New ParameterDiscreteValue
        discrete.Value = reportID
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("PrintedBy")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Loginid
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("Param")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.FilterBy
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("companyname")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.sesCompanyName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("sesbranch")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BranchName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("sdate")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.sdate
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("AdvanceTrans")
        Me.sdate = cookie.Values("sdate")
        Me.cmdwhere = cookie.Values("cmdwhere")
        Me.Loginid = cookie.Values("LoginID")
        Me.FilterBy = cookie.Values("FilterBy")
        Me.ReportType = cookie.Values("ReportType")
    End Sub

    Private Sub imbBackReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim Cookie As HttpCookie = Request.Cookies("AdvanceTrans")
        Dim dt As DateTime = CDate("01/01/1950 00:00:00 AM")
        Dim ts As New TimeSpan(-2, 0, 0, 0)
        If Not Cookie Is Nothing Then
            Cookie.Expires = dt.Add(ts)
            Response.AppendCookie(Cookie)
        End If
        If Me.ReportType = "RPTADVANCETRANSACTION" Then
            Response.Redirect("AdvanceTransactionRpt.aspx")
        ElseIf Me.ReportType = "RPTOSADVANCETRANSACTION" Then
            Response.Redirect("OutstandingAdvanceTransactionRpt.aspx")
        End If
    End Sub

End Class
