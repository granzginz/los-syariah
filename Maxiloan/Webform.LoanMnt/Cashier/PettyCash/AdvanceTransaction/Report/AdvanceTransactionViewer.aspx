<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692FBEA5521E1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceTransactionViewer.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.AdvanceTransactionViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AdvanceTransactionViewer</title>
    
    <link href="../../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="Form1" runat="server">
        <div class="form_button">
            <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="small button gray" />
        </div>
        <CR:CrystalReportViewer ID="CRVAdvanceTransaction" runat="server"></CR:CrystalReportViewer>
    </form>
</body>
</html>
