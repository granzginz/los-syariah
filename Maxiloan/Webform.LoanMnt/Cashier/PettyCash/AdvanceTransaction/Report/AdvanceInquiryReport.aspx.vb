#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Partial Class AdvanceInquiryReport
    Inherits AccMntWebBased


    Protected Property FilterBy() As String
        Get
            Return (CType(ViewState("FilterBy"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

    Private m_oCustomClass As Parameter.AdvanceRequest
    Private m_oController As Controller.AdvanceRequestController

    Private m_Rpt As rptAdvanceInquiry
    Private m_DatasetRpt As DataSet
    '*** Parameter field
    Private m_ParamFieldDef As ParameterFieldDefinition
    Private m_ParamDiscreteValue As ParameterDiscreteValue
    Private m_ParamValues As ParameterValues

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("AdvanceInquiry")
        Me.SearchBy = cookie.Values("where")
        Me.SortBy = cookie.Values("sortby")
        Me.FilterBy = cookie.Values("FilterBy")
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        Try
            lblMessage.Visible = False
            GetCookies()
            BindReport()
        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub

    Private Sub SetParamFieldValue(ByVal objReport As ReportClass, ByVal strParamFldName As String, ByVal oValue As Object)
        m_ParamFieldDef = objReport.DataDefinition.ParameterFields(strParamFldName)
        m_ParamDiscreteValue.Value = oValue
        m_ParamValues = m_ParamFieldDef.DefaultValues
        m_ParamValues.Add(m_ParamDiscreteValue)
        m_ParamFieldDef.ApplyCurrentValues(m_ParamValues)
    End Sub

    Private Sub BindReport()
        Try
            With m_oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .SortBy = Me.SortBy
                .BusinessDate = Me.BusinessDate
            End With
            m_oCustomClass = m_oController.GetPettyCashAdvanceRpt(m_oCustomClass)

            If m_oCustomClass Is Nothing Then
                Throw New Exception("Unable to get list for report")
            End If
            m_DatasetRpt = m_oCustomClass.PettyCashAdvanceDataSet

            m_Rpt.SetDataSource(m_DatasetRpt)
            With crAdvanceInquiry
                .ReportSource = m_Rpt
                .DataBind()
                .Visible = True
            End With
            '*** Set param fields value
            SetParamFieldValue(m_Rpt, "CompanyName", Me.sesCompanyName)
            SetParamFieldValue(m_Rpt, "BranchName", Me.BranchName.Trim)
            SetParamFieldValue(m_Rpt, "ReportId", "rptAdvanceInquiry")
            SetParamFieldValue(m_Rpt, "FilterBy", Me.FilterBy)
            'SetParamFieldValue(m_Rpt, "PrintedDate", Me.BusinessDate)
            SetParamFieldValue(m_Rpt, "PrintedBy", Me.Loginid)

        Catch ex As Exception
            DisplayError(ex.ToString())
        End Try
    End Sub

    Public Sub New()
        m_Rpt = New rptAdvanceInquiry
        m_ParamDiscreteValue = New ParameterDiscreteValue
        m_oCustomClass = New Parameter.AdvanceRequest
        m_oController = New Controller.AdvanceRequestController
    End Sub

    Private Sub imbBackReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../AdvanceInquiry.aspx")
    End Sub
End Class
