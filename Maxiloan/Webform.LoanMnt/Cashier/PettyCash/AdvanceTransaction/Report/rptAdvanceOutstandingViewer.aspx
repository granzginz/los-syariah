<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptAdvanceOutstandingViewer.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.rptAdvanceOutstandingViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692FBEA5521E1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Advance Inquiry Report</title>
    <script language="javascript" type="text/javascript">
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
    <link href="../../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="Form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_button">
            <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="small button gray" />
        </div>
        <CR:CrystalReportViewer ID="crAdvanceInquiry" runat="server"></CR:CrystalReportViewer>
    </form>
</body>
</html>
