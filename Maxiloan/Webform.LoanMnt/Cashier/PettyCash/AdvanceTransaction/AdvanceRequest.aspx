﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceRequest.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.AdvanceRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcLookupEmployee" Src="../../../../Webform.UserController/UcLookupEmployee.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>

    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <link href="../../../../Include/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtEmployeeName").autocomplete({
                source: function (request, response) {
                    $("#txtEmployeeID").val('');
                    $.ajax({
                        url: '../../../../Webform.Utility/autocomplate/GetEmployee.asmx/GetData',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#txtEmployeeID").val(i.item.val);
                },
                minLength: 1
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>

        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>Petty Cash Advance Request
                </h3>
            </div>
        </div>

        <asp:Panel ID="pnlMain" runat="server">
            <div class="form_box">


                <div class="form_single">
                    <label class="label_req">Karyawan</label>

                    <asp:TextBox ID="txtEmployeeName" runat="server" Style="width: 250px" placeholder="Masukan nama karyawan"></asp:TextBox>
                    <asp:TextBox ID="txtEmployeeID" runat="server" placeholder="ID Karyawan" />

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                        Display="Dynamic" ControlToValidate="txtEmployeeName" ErrorMessage="Harap pilih Karyawan"
                        Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Tanggal</label>
                        <asp:Label ID="lblBusinessDate" runat="server">DD/MM/YYYY</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Rekening Bank
                        </label>
                        <asp:DropDownList ID="cboBank" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="cbank" runat="server" InitialValue="0" Display="Dynamic"
                            ControlToValidate="cboBank" ErrorMessage="Harap pilih Rekening Bank" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Jumlah
                        </label>
                        <uc1:ucNumberFormat ID="txtAmount" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Departemen
                    </label>
                    <asp:DropDownList ID="cboDepartement" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="0"
                        Display="Dynamic" ControlToValidate="cboDepartement" ErrorMessage="Harap pilih Departemen"
                        Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>

            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Keterangan </label>
                        <asp:TextBox ID="txtDesc" runat="server" Width="222px"
                            CssClass="inptype" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic" ControlToValidate="txtdesc" ErrorMessage="Harus diisi"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="BtnNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button green"></asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlReview" runat="server" Height="168px">
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Employee Name</label>
                        <asp:Label ID="lblEmployeeName" runat="server" Width="192px">EmployeeName</asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Date</label>
                        <asp:Label ID="lblDate" runat="server" Width="208px">DD/MM/YYYY</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Bank Account</label>
                        <asp:Label ID="lblBankAccount" runat="server" Width="248px">BankAccount</asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Jumlah</label>
                        <asp:Label ID="lblAmount" runat="server">Amount</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Departemen</label>
                    <asp:Label ID="lblDepartement" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Keterangan</label>
                        <asp:Label ID="lblDesc" runat="server">Desc</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="imgSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" />
            </div>
        </asp:Panel>       
    </form>
</body>
</html>
