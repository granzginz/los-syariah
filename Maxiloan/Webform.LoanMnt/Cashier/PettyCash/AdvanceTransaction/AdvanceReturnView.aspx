<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceReturnView.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.AdvanceReturnView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../../../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../../../Webform.UserController/ucLookUpTransaction.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>VIEW PETTY CASH ADVANCE</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
     <script type="text/javascript"> 
</script>

</head>
<body>
    <script type = "text/javascript">
        function TransaksiClick(a, b) {
            alert(a);
            var IsPaymentReceive = "0";
            //var transaction = " ";//$('#txtTransaction').val();
            var IsPettyCash = "0";
            var IsAgreement = "0";
            var IsHOTransaction = "0";
            var url = a + IsPettyCash + "&IsAgreement=" + IsAgreement + "&IsHOTransaction=" + IsHOTransaction
            alert(url);
            OpenJLookup(url, 'Daftar Transaction', b); return false;
        }
        // function NamaIndustriBuClick(e,h) { 
        //    var cboselect = $('#cboIndrustriHeader').val();
        //    if (cboselect == 'Select One') return;
        //    var url = e + cboselect
        //    OpenJLookup(url, 'Daftar Industri', h); return false;
        //    <%--<button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookUpTransaction.aspx?transactionID=" & hdnTransactionID.ClientID & 
        //"&IsPaymentReceive=" & IsPaymentReceive  & "&transaction=" & txtTransaction.ClientID & "&IsPettyCash=" & IsPettyCash & "&IsAgreement=" & IsAgreement & "&IsHOTransaction=" & IsHOTransaction)%>',
        'Daftar Transaction','<%= jlookupContent.ClientID %>');return false;">...</button>  --%>
        //}

     <%--   function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;--%>
    </script>
    <form id="form1" runat="server">
<asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <div runat="server" id="jlookupContent" />
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <asp:Panel ID="PnlHeader" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>Petty Cash Advance Return
                </h3>
            </div>
        </div>

        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Advance No.</label>
                    <asp:Label ID="lblAdvanceNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Ref. Voucher No.</label>
                    <asp:Label ID="lblRefVoucherNo" runat="server"></asp:Label>
                </div>
            </div>
        </div>

        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Employee Name</label>
                    <asp:Label ID="lblEmployeeName" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Date</label>
                    <asp:Label ID="lblDate" runat="server" Width="136px"></asp:Label>
                </div>
            </div>
        </div>


        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Bank Account</label>
                    <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Amount</label>
                    <asp:Label ID="lblAmount" runat="server">0</asp:Label>
                </div>
            </div>
        </div>

        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Description</label>
                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Return To Account</label>
                    <asp:DropDownList ID="cmbBankAccount" runat="server" CssClass="inptype"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvReturnAccount" runat="server" ControlToValidate="cmbBankAccount" ErrorMessage="Please Select Bank Account"
                        CssClass="validator_general"
                        InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
         <div class="form_button">
                <asp:Button ID="BtnNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button green">
                </asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
            
       <%-- <div class="form_button">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
            <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="small button gray" CausesValidation="False"/>

        </div>    --%>    
        <asp:dropdownlist id="cboBankAccountType" runat="server" Visible="False"></asp:dropdownlist>
            </asp:Panel>
    </form>


      <asp:Panel ID="pnlList" runat="server">
         <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>DETAIL TRANSAKSI</strong></label>
            </div>
        </div>

          <div class="form_box">
                <div class="form_single">
                    <label class=""> Alokasi COA / Transaksi</label> 
                    <asp:HiddenField runat="server" ID="hdnTransactionID" /> 
                    <asp:TextBox runat="server" ID="txtTransaction" Enabled="false" CssClass="medium_text" text="-"></asp:TextBox>
                    <div style="margin-top:2px; float:left">
                         <asp:Panel ID="pnlLookupTransaction" runat="server">
                             <button class="small buttongo blue" onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookUpTransaction.aspx?transactionID=" & hdnTransactionID.ClientID & " &IsPaymentReceive= '0' &transaction= 'txtTransaction' &IsPettyCash='0' &IsAgreement= '0' & IsHOTransaction = '0'") %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>
                          </asp:Panel>
                    </div>
                    <asp:RequiredFieldValidator ID="rfvtxtTransaction" runat="server" ErrorMessage="*" Display="Dynamic"  CssClass="validator_general" ControlToValidate="txtTransaction" />
                </div>
            </div>
          <div class="form_box">
              <div class="form_single">
                  <label class="">Jumlah</label> 
                  <uc1:ucNumberFormat ID="txtAmount" runat="server" /> 
                 </div>
          </div>
          <div class="form_box">
              <div class="form_single">
                  <label class ="label_general"> Keterangan</label>
                  <asp:TextBox ID="txtDescription"  runat="server" CssClass="multiline_textbox_uc" MaxLength="35" TextMode="MultiLine" ></asp:TextBox>
                  <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtDescription" ErrorMessage="Harap isi Keterangan" Visible="True" Enabled="True" CssClass="validator_general"></asp:RequiredFieldValidator>
              </div>
          </div>
 
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlGrid" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPCList" runat="server" Visible="False" ShowFooter="True" CssClass="grid_general"
                        AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSACTION" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionID2" runat="server" Text='<%#Container.DataItem("TransactionID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionID" runat="server" Text='<%#Container.DataItem("Transaction")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCRIPTION" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="Amount"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="false" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
</body>
</html>
