﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class AdvanceTransactionDetail
    Inherits AccMntWebBased

    Protected WithEvents oTrans As ucLookUpTransaction
    Protected WithEvents txtAmount As ucNumberFormat

    Private Const STR_PARENT_FORM_ID As String = "ADVRTN"

#Region "Property"
    Private Property AdvanceNo() As String
        Get
            Return (CType(Viewstate("AdvanceNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AdvanceNo") = Value
        End Set
    End Property
    Private Property ParentFormId() As String
        Get
            Return (CType(Viewstate("ParentFormId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParentFormId") = Value
        End Set
    End Property
    Private Property EmployeeId() As String
        Get
            Return (CType(Viewstate("EmployeeId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("EmployeeId") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AdvanceRequest
    Private oController As New Controller.AdvanceRequestController

#End Region

    Protected Property AdvanceDetails() As IList(Of Parameter.AdvanceDetail)
        Get
            Return CType(ViewState("AdvanceDetails"), IList(Of Parameter.AdvanceDetail))
        End Get
        Set(ByVal Value As IList(Of Parameter.AdvanceDetail))
            ViewState("AdvanceDetails") = Value
        End Set
    End Property

 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ADVRTNVIEWDETAIL"

            'If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If CheckFeature(Me.Loginid, STR_PARENT_FORM_ID, "RETRN", Me.AppId) Then
                lblMessage.Visible = False
                Me.AdvanceDetails = New List(Of Parameter.AdvanceDetail)
                Me.BranchID = Request.QueryString("BranchId")
                Me.AdvanceNo = Request.QueryString("AdvanceNo")


                DoBind()

            End If
            'End If
        End If
    End Sub
    Private Sub DoBind()
        Try
            With oCustomClass
                '*** Get advance transaction info
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .AdvanceNo = Me.AdvanceNo.Trim
                .strConnection = GetConnectionString

                oCustomClass = oController.GetAnAdvanceTransactionRecord(oCustomClass)

                '*** PETTY CASH ADVANCE INFO
                lblAdvanceNo.Text = .AdvanceNo
                Me.EmployeeId = .EmployeeId
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .VoucherNoAdvance
                lblDate.Text = .AdvanceDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.AmountString, 2)
                lbldepartement.Text = .DepartmentName
                hdBankAccId.Value = .BankAccountID
                hdDepID.Value = .DepartementID
                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Sub reBindGrid()
        DoBind()
        Dim i = 1
        For Each item In AdvanceDetails
            item.Seq = i.ToString()
            i += 1
        Next
        Dim sm = AdvanceDetails.Sum(Function(x) x.Amount)
        Dim nilaiSisa = CDec(lblAmount.Text.Replace(",", "")) - sm
        lblAmount.Text = FormatNumber(nilaiSisa, 2)
        If nilaiSisa < 0 Then
            lblAmount.ForeColor = Drawing.Color.Red
        Else
            lblAmount.ForeColor = Drawing.Color.Black
        End If
        dtgCSV.DataSource = AdvanceDetails
        dtgCSV.DataBind()
        If AdvanceDetails.Count > 0 Then
            pnlGrid.Visible = True
        Else
            pnlGrid.Visible = False
        End If
    End Sub
    Shared Function guidString() As String

        Dim gb As Byte() = Guid.NewGuid().ToByteArray()
        Return BitConverter.ToInt64(gb, 0).ToString()
    End Function
    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim ad = New Parameter.AdvanceDetail() With
                  {
                      .Id = guidString(),
                      .BranchId = sesBranchId.Replace("'", ""),
                      .AdvanceNo = Trim(lblAdvanceNo.Text),
                      .PaymentAllocationId = oTrans.TransactionID,
                      .PaymentAllocationName = oTrans.Transaction,
                      .Description = oTrans.Description,
                      .Amount = oTrans.Amount
                  }


        AdvanceDetails.Add(ad)

        reBindGrid()
        cleardata()
    End Sub

    Private Sub cleardata()
        oTrans.TransactionID = ""
        oTrans.Transaction = ""
        oTrans.Description = ""
        oTrans.Amount = 0
    End Sub

  
    Sub remove(key As String)
        Dim k = AdvanceDetails.Where(Function(x) x.Id = key).SingleOrDefault()
        If Not k Is Nothing Then
            AdvanceDetails.Remove(k)
        End If
        reBindGrid()
    End Sub
 
  
    Protected Sub dtgCSV_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dtgCSV.RowDeleting

        Dim key = dtgCSV.DataKeys(e.RowIndex).Values("Id").ToString()
        'dtgCSV.DataKeys(dtgCSV.EditIndex).Value.ToString()
        remove(key)

    End Sub

    Protected Sub btnCancelNew_Click(sender As Object, e As EventArgs) Handles btnCancelNew.Click
        cleardata()
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click

        Try
            Dim Err As String = oController.CreateAdvanceTransactionDetail(GetConnectionString(), lblAdvanceNo.Text.Trim, AdvanceDetails)
            If Err = "" Then
                ' Response.Redirect("CustomerCompanyEC.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabEmergencyCt")

                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.sesBranchId.Trim.Replace("'", "")
                    .BankAccountID = hdBankAccId.Value
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .AmountString = lblAmount.Text
                    .CoyID = Me.SesCompanyID
                    .AppID = ""
                    .EmployeeId = Me.EmployeeId
                    .EmployeeName = lblEmployeeName.Text.Trim
                    .AdvanceNo = lblAdvanceNo.Text.Trim
                    .BankAccountType = hdBankAccId.Value
                    .Description = lblDescription.Text
                End With

                oController.SaveAdvanceReturnTransaction(oCustomClass)
                Server.Transfer("AdvanceReturn.aspx?Message=" & Server.UrlEncode("Record saved"))
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try


        'For Each item In AdvanceDetails
        '    oCustomClass.strConnection = GetConnectionString()
        '    oCustomClass.BranchId = item.BranchId
        '    oCustomClass.AdvanceNo = item.AdvanceNo
        '    oCustomClass.SequenceNo = item.Seq
        '    oCustomClass.PaymentAllocationID = item.PaymentAllocationId
        '    oCustomClass.Description = item.Description
        '    oCustomClass.AmountDetail = item.Amount
        '    oController.SaveAdvanceTransactionDetail(oCustomClass)
        'Next





    End Sub
End Class