﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvanceInquiryView.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.AdvanceInquiryView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VIEW PETTY CASH ADVANCE</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(BranchId, AdvanceNo) {
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/AdvanceTransaction/AdvanceInquiryView.aspx?BranchId=' + BranchId + '&AdvanceNo=' + AdvanceNo, null, 'left=50, top=50, width=900, height=300, menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlList" runat="server">
            <asp:Label ID="lblMessage" Visible="false" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>Petty Cash Advance - View
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Advance No.
                        </label>
                        <asp:Label ID="lblAdvanceNo" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Ref. Voucher No.
                        </label>
                        <asp:Label ID="lblRefVoucherNo" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Employee Name
                        </label>
                        <asp:Label ID="lblEmployeeName" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Date
                        </label>
                        <asp:Label ID="lblDate" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Bank Account
                        </label>
                        <asp:Label ID="lblBankAccount" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Amount
                        </label>
                        <asp:Label ID="lblAmount" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Description
                        </label>
                        <asp:Label ID="lblDescription" EnableViewState="False" runat="server"></asp:Label></font>
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div>
                    <div class="form_single">
                        <h4>RETURN INFORMATION</h4>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Return To Account
                        </label>
                        <asp:Label ID="lblReturnToAccount" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Ref. Voucher No.
                        </label>
                        <asp:Label ID="lblRefVoucherNoReturn" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Cashier
                        </label>
                        <asp:Label ID="lblCashier" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div>
                    <div class="form_single">
                        <h3>STATUS</h3>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Status
                        </label>
                        <asp:Label ID="lblStatus" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Status Date
                        </label>
                        <asp:Label ID="lblStatusDate" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Cashier
                        </label>
                        <asp:Label ID="lblCashierStatus" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
