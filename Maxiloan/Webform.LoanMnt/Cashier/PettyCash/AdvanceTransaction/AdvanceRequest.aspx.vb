﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports System.Web.Services
Imports System.Data.SqlClient

#End Region


Public Class AdvanceRequest
    Inherits AccMntWebBased
    Protected WithEvents oBranch As UcBranch
    Protected WithEvents txtAmount As ucNumberFormat

#Region "PrivateConst"
    Private Const STR_FRM_ID As String = "ADVREQ"

    Private Const STR_BANK_ACC_TYPE As String = "BankAccountType"
    Private Const STR_EMPLOYEE_ID As String = "EmployeeId"
    Private Const STR_EMPLOYEE_NAME As String = "EmployeeName"
    Private Const STR_CURR_DATE As String = "CurrentDate"
    Private Const STR_AMOUNT As String = "Amount"
    Private Const STR_DESC As String = "Description"
    Private Const STR_BANK_ACC_NAME As String = "BankAccountName"
    Private Const STR_BANK_ACC_ID As String = "BankAccountId"
    Private Const STR_BG_STATUS_NORMAL As String = "N"

    Private Const STR_FILE_ARBS As String = "AdvanceRequestBeforeSave.aspx"
    Private Const STR_PETTY_CASH_PURPOSE As String = "PC"
    Private Const STR_CACHE_BANK_ACC As String = "BankAccountCache"

    Private m_EmployeeId As String
    Private m_BankAccountID As String
    Private m_BankAccountName As String
    Private m_CTAdvReq As Controller.AdvanceRequestController
    Private m_ETAdvReq As Parameter.AdvanceRequest
    Private oController As New DataUserControlController
#End Region

#Region "Properties"
    Public Property EmployeeId() As String
        Get
            Return (CType(Viewstate("EmployeeId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("EmployeeId") = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return (CType(Viewstate("EmployeeName"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("EmployeeName") = Value
        End Set
    End Property
    Public Property CurrentDate() As String
        Get
            Return (CType(Viewstate("CurrentDate"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("CurrentDate") = Value
        End Set
    End Property
    Public Property Amount() As String
        Get
            Return (CType(Viewstate("Amount"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Amount") = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return (CType(Viewstate("Description"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Description") = Value
        End Set
    End Property
    Public Property BankAccountType() As String
        Get
            Return (CType(Viewstate("BankAccountType"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccountType") = Value
        End Set
    End Property
    Public Property BankAccountName() As String
        Get
            Return (CType(Viewstate("BankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccountName") = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return (CType(Viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccountID") = Value
        End Set
    End Property
#End Region
    Private Sub GetComboDepartement()



        '------------------
        'CBO DEPARTEMEN
        '------------------
        Dim dtDepartement As New DataTable

        dtDepartement = oController.GetDepartement(GetConnectionString)

        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"
    End Sub

    Private Sub BindBankAccount()
        Dim dtBankAccount As New DataTable

        dtBankAccount = oController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), _
                      "", "PC")

        cboBank.DataTextField = "Name"
        cboBank.DataValueField = "ID"
        cboBank.DataSource = dtBankAccount
        cboBank.DataBind()

        cboBank.Items.Insert(0, "Select One")
        cboBank.Items(0).Value = "0"

        cboBank.SelectedIndex = "1"
    End Sub
    Public Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        Try
            If Not Me.IsPostBack Then

                Me.FormID = STR_FRM_ID
                HideAllPanels()
                pnlMain.Visible = True
                Session.Add("ConString", GetConnectionString())
                Session.Add("BranchID", Me.sesBranchId.Replace("'", ""))
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    lblMessage.Text = Request.QueryString("Message")
                    lblMessage.Visible = True
                    lblBusinessDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                    'txtAmount.Text = "0" 
                    With txtAmount
                        .Text = ""
                        .RequiredFieldValidatorEnable = True
                        .RangeValidatorMinimumValue = "1"
                        .RangeValidatorMaximumValue = "999999999999999"
                        .RangeValidatorErrorMessage = "Input Salah"
                        .RangeValidatorEnable = True
                    End With
                    BindBankAccount()
                    GetComboDepartement()
                End If
            End If

        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub
    Private Sub HideAllPanels()
        Try
            pnlMain.Visible = False
            pnlReview.Visible = False
            lblMessage.Text = ""

        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub

    Private Sub cboBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBank.SelectedIndexChanged
        Try
            cboBank.SelectedIndex = cboBank.SelectedIndex
        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub


    Private Sub BtnNext_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        Try
            lblMessage.Visible = False

            If SessionInvalid() Then
                Exit Sub
            End If
            If Not CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
                Exit Sub
            End If


            HideAllPanels()
            '*** Fill labels with entered data
            Me.EmployeeId = txtEmployeeID.Text
            Me.BankAccountID = cboBank.SelectedItem.Value
            Me.BankAccountType = cboBank.SelectedItem.Text

            lblEmployeeName.Text = txtEmployeeName.Text
            lblBankAccount.Text = cboBank.SelectedItem.Text
            lblDepartement.Text = cboDepartement.SelectedItem.Text.Trim
            lblDesc.Text = txtDesc.Text
            lblDate.Text = lblBusinessDate.Text
            lblAmount.Text = FormatNumber(txtAmount.Text, 2)

            Dim dtBilyet As New DataTable
            Dim strWhere As String
            strWhere = "BranchID='" & Me.sesBranchId.Replace("'", "") & "' and BankAccountID='" & cboBank.SelectedItem.Value & "' and Status='N'"

            pnlReview.Visible = True
        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        ShowMessage(lblMessage, strErrMsg, True)
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgSave.Click

        Try
            If Not CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
                Exit Sub
            End If
            If Not CheckCashier(Me.Loginid) Then
                DisplayError("Cashier Not Open")
                Exit Sub
            End If

            With m_ETAdvReq
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Trim.Replace("'", "")
                .BankAccountID = Me.BankAccountID
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .AmountString = lblAmount.Text.Trim
                .CoyID = Me.SesCompanyID.Trim
                .AppID = ""
                .DepartementID = cboDepartement.SelectedValue
                .Description = lblDesc.Text
                .BankAccountName = Me.BankAccountName
                .BankAccountType = Me.BankAccountType
                .BilyetGiroNo = ""
                .BilyetGiroDueDate = Me.BusinessDate
                .EmployeeId = Me.EmployeeId
                .LoginId = Me.Loginid
            End With

            m_CTAdvReq.SaveAdvanceTransaction(m_ETAdvReq)
            ResetMainDisplay()
            HideAllPanels()
            pnlMain.Visible = True
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch exp As Exception
            DisplayError(exp.Message)
        End Try
    End Sub
    Public Sub New()
        Try
            m_CTAdvReq = New Controller.AdvanceRequestController
            m_ETAdvReq = New Parameter.AdvanceRequest
        Catch ex As Exception
            DisplayError(ex.ToString)
        End Try
    End Sub

    Private Sub ResetMainDisplay()
        txtEmployeeID.Text = ""
        txtEmployeeName.Text = ""
        cboBank.SelectedIndex = 0
        txtDesc.Text = ""
        lblDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtAmount.Text = ""
    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        ResetMainDisplay()
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imbCancel.Click
        HideAllPanels()
        pnlMain.Visible = True
        ResetMainDisplay()
    End Sub

End Class