﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PCReimburseReconcile.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PCReimburseReconcile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../../../Webform.UserController/ucBankAccountReconcile.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../../../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PCReimburseReconcile</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinPetty(pBranch, pPetty) {
            var x = screen.width; var y = screen.height - 100;
            window.open('../../Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?BranchID=' + pBranch + '&PettyCashNo=' + pPetty + '&Style=Finance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinRequest(pBranch, pRequest) {
            var x = screen.width; var y = screen.height - 100;
            window.open('ViewPettyCashReimburse.aspx?Branch=' + pBranch + '&RequestNo=' + pRequest + '&Style=Finance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                TERIMA PETTY CASH
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Rekening Bank
            </label>
            <uc1:ucbankaccountid id="oBankAccount" runat="server">
                </uc1:ucbankaccountid>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:TextBox runat="server" ID="txtRequestDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtRequestDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cari Berdasarkan
            </label>
            <uc1:ucsearchbywithnotable id="oSearchBy" runat="server">
                </uc1:ucsearchbywithnotable>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PENGAJUAN PETTY CASH REIMBURSE
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgReimburse" runat="server" AllowPaging="True" AllowSorting="True"
                        OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="RequestNo"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <HeaderTemplate>
                                    <br />
                                    <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                        AutoPostBack="True"></asp:CheckBox>
                                    <p>
                                    </p>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblRequestNo" runat="server" Visible="False" Text='<%#  container.dataitem("RequestNo") %>'>
                                    </asp:Label>
                                    <asp:CheckBox ID="CheckBoxReimburse" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                <HeaderStyle Width="18%"></HeaderStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenWinRequest('<%#Container.dataItem("BranchID")%>','<%#Container.dataItem("RequestNo")%>');">
                                        <asp:Label ID="lblGridRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DESCRIPTION" HeaderText="KETERANGAN">
                                <HeaderStyle Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="JUMLAH">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.TotalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AmountTransfer" HeaderText="JUMLAH TRANSFER">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.AmountTransfer"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL REQUEST"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="StatusDate" SortExpression="StatusDate" HeaderText="TGL TRANSFER"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" Text="Next" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
