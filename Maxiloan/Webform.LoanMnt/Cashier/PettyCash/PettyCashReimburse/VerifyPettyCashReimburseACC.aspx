﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VerifyPettyCashReimburseACC.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.VerifyPettyCashReimburseACC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %> 
<%@ Register Src="../../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Verifikasi PettyCash Reimburse PC ACC</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinViewPCReimburse(pStyle, pRequestNo, pBranch) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashReimburse/ViewPettyCashReimburse.aspx?style=' + pStyle + '&RequestNo=' + pRequestNo + '&Branch=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function OpenWinViewPCInquiryEdit(pStyle, pPettyCashNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryViewEdit.aspx?style=' + pStyle + '&PettyCashNo=' + pPettyCashNo, 'userlookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
 <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1" /> 
     <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />

      <div class="form_title">
            <div class="title_strip" ></div>
            <div class="form_single">
                <h3> VERIFIKASI PETTY CASH REIMBURSE ACC </h3>
            </div>
        </div>
         
    <asp:Panel ID="pnlSearch" runat="server"> 
        <div class="form_box">
             <div class="form_single">
               <label class="label_req"> Cabang </label>
                <asp:DropDownList ID="cboParent" runat="server" />
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" />
              </div>
         </div>
        <div class="form_box">
             <div class="form_single">
                <label> Tanggal </label>
                <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDate" Format="dd/MM/yyyy"> </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
             <div class="form_single">
                <label> Cari Berdasarkan </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                    <asp:ListItem Value="ReferenceNo">No Memo</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="250px" ></asp:TextBox>
            </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
 
     <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PETTY CASH REIMBURSE HQ
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgReimburseInquiry" runat="server" ShowFooter="True" Visible="true" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" /> <ItemStyle CssClass="item_grid" /> <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle HorizontalAlign="Center"   CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="hypPrint" runat="server" Text="Verifikasi"  CommandName="Edit"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="BranchId" HeaderText="CABANG">
                                <ItemTemplate>
                                    <asp:label ID="BranchFullName" runat="server" Text='<%#Container.DataItem("BranchFullName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                <ItemStyle Width="15%" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestBy" HeaderText="DIAJUKAN OLEH">
                                <ItemStyle Width="20%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblRequestBy" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <ItemStyle Width="30%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>' />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="20%"></FooterStyle>
                                <FooterTemplate> Total </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO MEMO">
                                <ItemStyle Width="12%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                <ItemStyle HorizontalAlign="Right" Width="8%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%" />
                                <FooterTemplate>
                                    <asp:Label ID="lblTotAmount" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL PENGAJUAN" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="10%" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                <ItemStyle Width="12%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="BranchId" HeaderText="BranchId" Visible="false">
                                <ItemStyle Width="12%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchId")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"   Mode="NumericPages" />
                    </asp:DataGrid>
                     <uc2:ucGridNav id="GridNavigator" runat="server"/>
                     
                </div>
            </div>
        </div> 
    </asp:Panel>




    <asp:panel id="pnlVerify" runat="server"  Visible="false">
     
    <div class="form_box">
        <div class="form_left">
            <label> Cabang Request </label>
            <asp:Label ID="lblBranchRequest" runat="server" />
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label> No Request </label>
            <asp:Label ID="LblRequestNo" runat="server" />
        </div>
        <div class="form_right">
            <label> Tanggal Request </label>
            <asp:Label ID="lblRequestDate" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label> Rekening Bank Cabang </label>
            <asp:Label ID="LblBranchBankAccount" runat="server" />
        </div>
        <div class="form_right">
            <label> Total Request </label>
            <asp:Label ID="LblRequestAmount" runat="server" align="right" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>Keterangan</label>
            <asp:Label ID="lblDescription" runat="server" />
        </div>
        <div class="form_right">
            <label> No Memo </label>
            <asp:Label ID="lblNoMemo" runat="server" />
            <asp:Label ID="lblNum" runat="server" Width="2px" Visible="false"/>
        </div>
    </div>
        <div class="form_button">
            <asp:Button ID="btnhistrans" runat="server" Text="History Transaksi" CssClass="small button blue" />
        </div>
    <asp:panel id="panelHistory" runat="server">
        <div class="form_title">
                <div class="form_single">
                <h4> HISTORY REJECT TRANSAKSI </h4>
            </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgreeHistory" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblSeqNo" runat="server" Text='<%#Container.DataItem("SeqNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RejectNote")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TANGGAL">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTanggal"  runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Tanggal") %>'  DataFormatString="{0:dd/MM/yyyy}"> </asp:Label>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="User">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        </div>
        </asp:panel>
    <div class="form_title">
        <div class="form_single">
            <h4> DAFTAR TRANSAKSI PETTY CASH </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtdTransactionList" runat="server" AutoGenerateColumns="False" CssClass="grid_general" ShowFooter="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn SortExpression="PETTYCASHNO" HeaderText="NO PETTY CASH">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblPettyCashNo" runat="server" Text='<%#Container.DataItem("PETTYCASHNO")%>' />
                            </ItemTemplate>
                            <FooterTemplate> Total </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDescription" runat="server" Text='<%#Container.DataItem("Description")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PCAMOUNT" HeaderText="JUMLAH">
                            <ItemTemplate>
                                <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNT"),2)%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotPCAmount" runat="server" Text="TOTAL" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PCDATE" HeaderText="TGL PC">
                            <ItemTemplate>
                                <asp:Label ID="lblPCDate" runat="server" Text='<%#Container.DataItem("PETTYCASHDATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AMOUNTTRANSFER" HeaderText="JUMLAH TRANSFER">
                            <ItemTemplate>
                                <asp:Label ID="lblGridAmountTransfer" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNTTRANSFER"),2)%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotAmountTransfer" runat="server" Text="TOTAL" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="NOTES" HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("FINANCERNOTE")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_box">
    <div class="form_left">
        <label>
            Pilih Status
        </label>
        <asp:DropDownList ID="cboStatus" AutoPostBack="true" runat="server">
            <asp:ListItem Text="Verifikasi" Value="VR" />
            <asp:ListItem Text="Reject" Value="RJ"/>
            </asp:DropDownList>
    </div>
    </div>
    </asp:panel>
     <asp:panel id="Panel1" runat="server"  Visible="false">
     <div class="form_box">
     <div class="form_left">
                <label>
                    Keterangan
                </label>
                <asp:TextBox ID="txtnotes" runat="server" Width="365px" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue" OnClientClick="if(this.value === 'Saving...') { return false; } else { this.value = 'Saving...'; }"/>
            <asp:Button ID="BtnCancel" runat="server"  Text="Cancel" CssClass="small button gray" />
            </div>
     </div>
     </asp:panel> 
    <asp:Panel id="Panel2" runat="server"  Visible="false">
    <div class="form_box_header">
        <div class="form_single">
                <label class="label_req"> Akan diSetujui Oleh </label>
                <asp:DropDownList ID="cboApprovedBy" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh" Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general" />-
         </div>
      </div>
     
      <div class="form_button">
        <asp:Button ID="btnVerifikasi" runat="server" Text="Verifikasi" CssClass="small button blue" OnClientClick="if(this.value === 'Saving...') { return false; } else { this.value = 'Saving...'; }"/>
        <asp:Button ID="btnClose" runat="server"  CausesValidation="False" Text="Close" CssClass="small button gray" />
    </div>
    </asp:Panel> 
    </form>

</body>
</html>
