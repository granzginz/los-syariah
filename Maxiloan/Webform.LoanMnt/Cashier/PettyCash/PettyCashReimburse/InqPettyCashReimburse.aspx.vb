﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.BusinessProcess
'Imports Maxiloan.Parameter.CollZipCode
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class InqPettyCashReimburse
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            Return CType(Viewstate("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalAmount") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
    Private m_controller As New DataUserControlController

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "PCREIMBURSEINQ"
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        If Not IsPostBack Then
            Dim dtbranch As New DataTable
            dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cboParent
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(0).Value = "ALL"
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With
            Me.CmdWhere = ""
            Me.SortBy = ""
        End If

    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        BindGridInqPCReimburse(Me.CmdWhere, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridInqPCReimburse(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridInqPCReimburse(Me.CmdWhere, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGridInqPCReimburse(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqPCReimburse As New PettyCash

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .sortBy = sortBy
        End With

        oInqPCReimburse = oController.InqPCReimburse(oCustomClass)

        With oInqPCReimburse
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInqPCReimburse.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = sortBy
        DtgReimburseInquiry.DataSource = dtvEntity
        Try
            DtgReimburseInquiry.DataBind()
        Catch
            DtgReimburseInquiry.CurrentPageIndex = 0
            DtgReimburseInquiry.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
    Private Sub DtgReimburseInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgReimburseInquiry.ItemDataBound
        Dim lblAmount As Label
        Dim lblTotAmount As Label
        Dim hyRequestNo As HyperLink
        'Dim hypPrint As HyperLink

        If e.Item.ItemIndex >= 0 Then

            hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            hyRequestNo.NavigateUrl = LinkToViewPCReimburse(hyRequestNo.Text.Trim, "ACCMNT", cboParent.SelectedItem.Value.Trim)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            Me.TotalAmount += CDbl(lblAmount.Text)

            'hypPrint = CType(e.Item.FindControl("hypPrint"), HyperLink)
            'hypPrint.NavigateUrl = "rptPettyCashReimburseViewer01.aspx?RequestNo=" & hyRequestNo.Text.Trim & "&BranchID=" & cboParent.SelectedItem.Value.Trim & ""
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblTotAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            lblTotAmount.Text = FormatNumber(Me.TotalAmount, 2)
        End If
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            Dim filterby As String
            Me.SortBy = ""
            Me.CmdWhere = ""
            cmdwhere = ""
            filterby = ""
            Me.TotalAmount = 0.0

            If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
                If txtSearchBy.Text.Trim <> "" Then
                    If cboSearchBy.SelectedItem.Value.Trim = "RequestNo" Then
                        If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                            cmdwhere = cmdwhere + "PettyCashReimburse.RequestNo like '" & txtSearchBy.Text.Trim & "' and "
                        Else
                            cmdwhere = cmdwhere + "PettyCashReimburse.RequestNo = '" & txtSearchBy.Text.Trim & "' and "
                        End If
                        filterby = "Request NO. = " & txtSearchBy.Text.Trim & " and "
                    ElseIf cboSearchBy.SelectedItem.Value.Trim = "Description" Then
                        If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                            cmdwhere = cmdwhere + "PettyCashReimburse.Description like '" & txtSearchBy.Text.Trim & "' and "
                        Else
                            cmdwhere = cmdwhere + "PettyCashReimburse.Description = '" & txtSearchBy.Text.Trim & "' and "
                        End If
                        filterby = "Description = " & txtSearchBy.Text.Trim & " and "
                    End If
                End If
            End If
            If txtDate.Text.Trim <> "" Then
                cmdwhere = cmdwhere + "PettyCashReimburse.RequestDate = '" & ConvertDate2(txtDate.Text.Trim).ToString("yyyyMMdd") & "' and "
                filterby = filterby + "Request Date = " & txtDate.Text.Trim & "  and "
            End If
            If cboStatus.SelectedItem.Value.Trim <> "ALL" Then
                If cboStatus.SelectedItem.Value.Trim = "REC" Then
                    cmdwhere = cmdwhere + "PettyCashReimburse.IsReconcile = '1' and "
                Else
                    cmdwhere = cmdwhere + "PettyCashReimburse.status = '" & cboStatus.SelectedItem.Value.Trim & "' and "
                End If
                filterby = filterby + "Status = " & cboStatus.SelectedItem.Text.Trim & " and "
            End If
            If cboParent.SelectedItem.Value.Trim <> "ALL" Then
                cmdwhere = cmdwhere + "PettyCashReimburse.branchID = '" & cboParent.SelectedItem.Value.Trim & "'"
            Else
                If cmdwhere.Trim <> "" Then
                    cmdwhere = Left(cmdwhere, Len(cmdwhere.Trim) - 4)
                End If
            End If
            Me.CmdWhere = cmdwhere
            If filterby <> "" Then
                filterby = Left(filterby, Len(filterby.Trim) - 4)
            End If
            Me.FilterBy = filterby
            BindGridInqPCReimburse(Me.CmdWhere, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If

    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("InqPettyCashReimburse.aspx")
    End Sub

    'Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
    '    Dim cookie As HttpCookie = Request.Cookies(COOKIES_SALES_REPORT)
    '    If Not cookie Is Nothing Then
    '        cookie.Values("cmdwhere") = Me.CmdWhere
    '        cookie.Values("LoginID") = Me.Loginid
    '        cookie.Values("FilterBy") = Me.FilterBy
    '        cookie.Values("BranchFullName") = cboParent.SelectedItem.Text.Trim
    '        cookie.Values("ReportType") = "RptPettyCashReimburse"
    '        'this is dynamis cookie,it's value return the name of crystal report file we want to open 
    '        Response.AppendCookie(cookie)
    '    Else
    '        Dim cookieNew As New HttpCookie(COOKIES_SALES_REPORT)
    '        cookieNew.Values.Add("cmdwhere", Me.CmdWhere)
    '        cookieNew.Values.Add("LoginID", Me.Loginid)
    '        cookieNew.Values.Add("FilterBy", Me.FilterBy)
    '        cookieNew.Values.Add("BranchFullName", cboParent.SelectedItem.Text.Trim)
    '        cookieNew.Values.Add("ReportType", "RptPettyCashReimburse")
    '        Response.AppendCookie(cookieNew)
    '    End If
    '    Response.Redirect("RptPettyCashReimburseViewer.aspx")
    'End Sub

    Private Sub DtgReimburseInquiry_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgReimburseInquiry.SortCommand
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInqPCReimburse(Me.CmdWhere, Me.SortBy)
    End Sub
#Region "linkTo"
    Function LinkToViewPCReimburse(ByVal strRequestNo As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewPCReimburse('" & strStyle & "','" & strRequestNo & "','" & strBranch & "')"
    End Function
#End Region

    
End Class