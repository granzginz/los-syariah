﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPettyCashReimburse.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ViewPettyCashReimburse" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewPettyCashReimburse</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinViewPCInquiry(pStyle, pPettyCashNo, pBranch) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?style=' + pStyle + '&PettyCashNo=' + pPettyCashNo + '&BranchId=' + pBranch, 'userlookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }

        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - PETTY CASH REIMBURSE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang Request
            </label>
            <asp:Label ID="lblBranchRequest" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Request
            </label>
            <asp:Label ID="LblRequestNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Rekening Bank Cabang
            </label>
            <asp:Label ID="LblBranchBankAccount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Total Request
            </label>
            <asp:Label ID="LblRequestAmount" runat="server" align="right"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Keterangan</label>
            <asp:Label ID="lblDescription" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DAFTAR TRANSAKSI PETTY CASH
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtdTransactionList" runat="server" AutoGenerateColumns="False"
                    CssClass="grid_general" ShowFooter="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn SortExpression="PETTYCASHNO" HeaderText="NO PETTY CASH">
                            <ItemStyle Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblPettyCashNo" runat="server" Text='<%#Container.DataItem("PETTYCASHNO")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                            <ItemStyle Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblGridDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" Width="20%"></FooterStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PCAMOUNT" HeaderText="JUMLAH">
                            <%--<ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>--%>
                            <ItemStyle HorizontalAlign="right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNT"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotPCAmount" runat="server" Text="TOTAL"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PCDATE" HeaderText="TGL PC">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPCDate" runat="server" Text='<%#Container.DataItem("PETTYCASHDATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AMOUNTTRANSFER" HeaderText="JUMLAH TRANSFER">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblGridAmountTransfer" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNTTRANSFER"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotAmountTransfer" runat="server" Text="TOTAL"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AMOUNTAPROVEACC" HeaderText="APR by ACC">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblGridAmountAprbyACC" runat="server" Text='<%#formatnumber(Container.DataItem("AprAmountbyACC"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotAmountAprbyACC" runat="server" Text="TOTAL"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="STATUSACC" HeaderText="STATUS ACC">
                            <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblStatusACC" runat="server" Text='<%#Container.DataItem("STATUSACC")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="NOTES" HeaderText="CATATAN">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("FINANCERNOTE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                INFORMASI TRANSFER
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Voucher
            </label>
            <asp:Label ID="lblRefVoucherNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Transfer
            </label>
            <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Bukti
            </label>
            <asp:Label ID="lblRefNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Transfer
            </label>
            <asp:Label ID="lblAmountTransfer" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                <strong>STATUS</strong></label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Status
            </label>
            <asp:Label ID="lblStatus" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Status
            </label>
            <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Diajukan Oleh
            </label>
            <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_button">
<%--        <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
        </asp:Button>--%>
        <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
