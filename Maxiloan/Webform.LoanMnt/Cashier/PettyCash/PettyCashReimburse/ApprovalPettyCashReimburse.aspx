﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalPettyCashReimburse.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ApprovalPettyCashReimburse" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>InqPettyCashReimburse</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinViewPCReimburse(pStyle, pRequestNo, pBranch) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashReimburse/ViewPettyCashReimburse.aspx?style=' + pStyle + '&RequestNo=' + pRequestNo + '&Branch=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    APPROVAL PETTY CASH REIMBURSE
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Hasil Approval
                </label>
                <asp:DropDownList ID="cboHasilApproval" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">Semua</asp:ListItem>
                    <asp:ListItem Value="R">Request</asp:ListItem>
                    <asp:ListItem Value="A">Approved</asp:ListItem>
                    <asp:ListItem Value="D">Declined</asp:ListItem>
                </asp:DropDownList>             
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="RequestNo">No Request</asp:ListItem>                    
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="350px" ></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Status Approval
                </label>
                <asp:DropDownList ID="cboStatus" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">Semua</asp:ListItem>
                    <asp:ListItem Value="0">Open</asp:ListItem>
                    <asp:ListItem Value="1">Final</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Diminta oleh
                </label>
                <asp:DropDownList ID="cboRequestFrom" runat="server">                    
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Akan di Approved Oleh
                </label>
                <asp:DropDownList ID="cboApprBy" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DATA PETTY CASH REIMBURSE
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgReimburseInquiry" runat="server" ShowFooter="True" Visible="true"
                        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="PILIH" HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <a href='ApprovalPettyCashReimburseExec.aspx?RequestNo=<%# DataBinder.eval(Container,"DataItem.RequestNo") %>&TotalPCAmount=<%#formatnumber(Container.DataItem("Amount"),2)%>&RequestDate=<%# DataBinder.eval(Container,"DataItem.RequestDate") %>'>
                                        <asp:Image ID="hyApproval" ImageUrl="../../../../images/iconDrawDown.gif" runat="server"></asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApprovalStatus" HeaderText="STATUS">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Container.DataItem("ApprovalStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApprovalResult" HeaderText="Hasil">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApprovalResult" runat="server" Text='<%#Container.DataItem("ApprovalResult")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="From" HeaderText="DARI / KE">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFrom" runat="server" Text='<%#Container.DataItem("UserApproval")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
<%--                            <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL PENGAJUAN"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundColumn>--%>
                            <asp:TemplateColumn SortExpression="Tgl Pengajuan" HeaderText="TGL PENGAJUAN">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRequestDate" runat="server" Text='<%#Container.DataItem("RequestDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Duration" HeaderText="DURATION">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%#Container.DataItem("Duration")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Cabang" HeaderText="CABANG">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranch" runat="server" Text='<%#Container.DataItem("BranchRequestFullName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="Amount">
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotAmount" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" Display="Dynamic"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGoPage"
                            ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>    
    <br />
    </form>
</body>
</html>
