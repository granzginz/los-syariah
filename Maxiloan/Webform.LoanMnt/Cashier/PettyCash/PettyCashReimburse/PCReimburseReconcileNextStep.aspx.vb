﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PCReimburseReconcileNextStep
    Inherits Maxiloan.Webform.WebBased

#Region "Property"


    Private Property RequestDate() As String
        Get
            Return (CType(viewstate("RequestDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("RequestDate") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return (CType(viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property
    Private Property BankAccountName() As String
        Get
            Return (CType(viewstate("BankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountName") = Value
        End Set
    End Property


#End Region


#Region "Constanta"
    Private Const FILE_REDIRECT As String = "PCReimburseReconcile.aspx"
    Private oController As New GeneralPagingController
    Dim oEntites As New Parameter.PettyCashReconcile
    Dim oSaveContoller As New PettyCashController
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'If IsSingleBranch And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 Then
            GetCookies()
            LblBankAccount.Text = Me.BankAccountName
            LblRequestDate.Text = Me.RequestDate
            BindGrid(Me.SearchBy)
            'Else
            'KickOutForMultipleBranch()
            'End If
        End If


    End Sub
#End Region

#Region "BindGrid"
    Public Sub BindGrid(ByVal CmdWhere As String)

        Dim oEntities As New Parameter.GeneralPaging
        With oEntities
            .SpName = "spPCReimburseSelect"
            .WhereCond = CmdWhere
            .SortBy = ""
            .strConnection = GetConnectionString
        End With

        oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)

        Dim dt As DataSet
        If Not oEntities Is Nothing Then
            dt = oEntities.ListDataReport
        End If
        DtgReconcile.DataSource = dt
        DtgReconcile.DataBind()

    End Sub

#End Region
#Region "Cancel"
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect(FILE_REDIRECT)
    End Sub
#End Region
#Region "Ketika Tombol Reconcile Dipijit"
    Private Sub BtnReconcile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReconcile.Click
        'lakukan proses reconcile satu demi satu 
        Dim strBranch As String = Replace(Me.sesBranchId, "'", "")
        Dim oLoop As Int32
        For oLoop = 0 To DtgReconcile.Items.Count - 1
            Dim strRequestNo As New Label
            strRequestNo = CType(DtgReconcile.Items(oLoop).FindControl("LblRequestNo"), Label)
            Dim oReconcile As New Parameter.PettyCashReconcile
            With oReconcile
                .BranchId = strBranch
                .RequestNo = strRequestNo.Text
                .BankAccountID = Me.BankAccountID
                .LoginId = Me.Loginid
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                .ReconcileMode = "PC"
                .strConnection = GetConnectionString()
            End With

            'Try
            oReconcile = oSaveContoller.PCReimburseReconcile(oReconcile)
            'Catch ex As eloanExceptions

            'ex.WriteLog(ex.Message, "LogName", "LogModulue")
            'ExceptionManager.Publish(ex)
            'End Try

        Next

        Response.Redirect(FILE_REDIRECT)


    End Sub
#End Region
#Region "Get Cookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_PETTY_CASH_REIMBURSE)
        Me.SearchBy = cookie.Values("SearchBy")
        Me.BankAccountID = cookie.Values("BankAccountID")
        Me.BankAccountName = cookie.Values("BankAccountName")
        Me.RequestDate = cookie.Values("RequestDate")
    End Sub
#End Region

End Class