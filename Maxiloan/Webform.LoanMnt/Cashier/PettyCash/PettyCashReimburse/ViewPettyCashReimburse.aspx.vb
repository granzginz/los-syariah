﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewPettyCashReimburse
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
#End Region

#Region "Property"

    Property RequestNo() As String
        Get
            Return viewstate("RequestNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("RequestNo") = Value
        End Set
    End Property
    Property CSSStyle() As String
        Get
            Return viewstate("CSSStyle").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CSSStyle") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Public Property TotalPCAmount() As Double
        Get
            Return CType(Viewstate("TotalPCAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalPCAmount") = Value
        End Set
    End Property
    Public Property TotalAmountTransfer() As Double
        Get
            Return CType(Viewstate("TotalAmountTransfer"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalAmountTransfer") = Value
        End Set
    End Property

    Public Property TotalAmountAprbyACC() As Double
        Get
            Return CType(ViewState("TotalAmountAprbyACC"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmountAprbyACC") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "VIEWPETTYCASHREIMBURSE"
            'If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Dim CmdWhere As String
            Me.CmdWhere = ""
            Me.SortBy = ""
            Me.RequestNo = ""
            Me.CSSStyle = ""
            Me.BranchID = ""
            Me.TotalAmountTransfer = 0.0
            Me.TotalPCAmount = 0.0

            Me.RequestNo = Request("RequestNo").ToString
            Me.CSSStyle = Request("Style").ToString
            Me.BranchID = Request("Branch").ToString

            If Me.BranchID <> "ALL" Then
                CmdWhere = CmdWhere + "PettyCashReimburse.BranchId = '" & Me.BranchID & "' and "
            End If
            CmdWhere = "PettyCashReimburse.requestNo = '" & Me.RequestNo & "'"

            Me.CmdWhere = CmdWhere

            BindDataLabel()
            BindDataGrid()
            'End If
        End If

    End Sub

    Sub BindDataLabel()
        Dim oPCReimburse As New Parameter.PettyCash
        Dim oSupplier As New Parameter.Supplier
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable

        With oPCReimburse
            .strConnection = getconnectionstring()
            .WhereCond = Me.CmdWhere
        End With

        oPCReimburse = oController.getViewPCReimburseLabel(oPCReimburse)

        If Not oPCReimburse Is Nothing Then
            oData = oPCReimburse.ListData
        End If

        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
    End Sub

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow

        oRow = oData.Rows(0)
        lblBranchRequest.Text = oRow.Item(0).ToString.Trim
        LblRequestNo.Text = oRow.Item(1).ToString.Trim
        lblRequestDate.Text = oRow.Item(2).ToString.Trim
        LblBranchBankAccount.Text = oRow.Item(3).ToString.Trim
        LblRequestAmount.Text = FormatNumber(oRow.Item(4).ToString.Trim, 2)
        lblDescription.Text = oRow.Item(5).ToString.Trim
        lblRefVoucherNo.Text = oRow.Item(6).ToString.Trim
        lblRefNo.Text = oRow.Item(7).ToString.Trim
        lblTransferDate.Text = oRow.Item(8).ToString.Trim
        lblAmountTransfer.Text = oRow.Item(9).ToString.Trim
        lblRequestBy.Text = oRow.Item(11).ToString.Trim
        lblStatusDate.Text = oRow.Item(12).ToString.Trim
        lblStatus.Text = oRow.Item(13).ToString.Trim
    End Sub
    Sub BindDataGrid()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oViewPCReimburse As New PettyCash

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .SortBy = Me.SortBy
        End With

        oViewPCReimburse = oController.GetViewPCReimburseGrid(oCustomClass)

        dtsEntity = oViewPCReimburse.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = Me.SortBy
        DtdTransactionList.DataSource = dtvEntity
        Try
            DtdTransactionList.DataBind()
        Catch
            DtdTransactionList.CurrentPageIndex = 0
            DtdTransactionList.DataBind()
        End Try
    End Sub

    Private Sub DtdTransactionList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtdTransactionList.ItemDataBound
        Dim lblPCAmount As Label
        Dim lblAmountTransfer As Label
        Dim lblTotPCAmount As Label
        Dim lblTotAmountTransfer As Label
        Dim lblTotAmountAprbyACC As Label
        Dim lblGridAmountAprbyACC As Label
        Dim hyPettyCashNo As HyperLink
        If e.Item.ItemIndex >= 0 Then
            hyPettyCashNo = CType(e.Item.FindControl("lblPettyCashNo"), HyperLink)
            hyPettyCashNo.NavigateUrl = LinkToViewPCInquiry(hyPettyCashNo.Text.Trim, "ACCMNT", Me.BranchID)
            lblPCAmount = CType(e.Item.FindControl("lblPCAmount"), Label)
            Me.TotalPCAmount += CDbl(lblPCAmount.Text)
            lblAmountTransfer = CType(e.Item.FindControl("lblGridAmountTransfer"), Label)
            Me.TotalAmountTransfer += CDbl(lblAmountTransfer.Text)
            lblGridAmountAprbyACC = CType(e.Item.FindControl("lblGridAmountAprbyACC"), Label)
            Me.TotalAmountAprbyACC += CDbl(lblGridAmountAprbyACC.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotPCAmount = CType(e.Item.FindControl("lblTotPCAmount"), Label)
            lblTotPCAmount.Text = FormatNumber(Me.TotalPCAmount, 2)
            lblTotAmountTransfer = CType(e.Item.FindControl("lblTotAmountTransfer"), Label)
            lblTotAmountTransfer.Text = FormatNumber(Me.TotalAmountTransfer, 2)
            lblTotAmountAprbyACC = CType(e.Item.FindControl("lblTotAmountAprbyACC"), Label)
            lblTotAmountAprbyACC.Text = FormatNumber(Me.TotalAmountAprbyACC, 2)
        End If
    End Sub

    Private Sub DtdTransactionList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtdTransactionList.SortCommand
        Me.TotalAmountTransfer = 0.0
        Me.TotalPCAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindDataGrid()
    End Sub
#Region "linkTo"
    Function LinkToViewPCInquiry(ByVal strRequestNo As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewPCInquiry('" & strStyle & "','" & strRequestNo & "','" & strBranch & "')"
    End Function
#End Region

    'Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If

    '    Dim cookie As HttpCookie = Request.Cookies("PettyCashReimburse01")

    '    If Not cookie Is Nothing Then
    '        cookie.Values("RequestNo") = Me.RequestNo
    '        cookie.Values("BranchID") = Me.BranchID
    '        Response.AppendCookie(cookie)
    '    Else
    '        Dim cookieNew As New HttpCookie("PettyCashReimburse01")

    '        cookieNew.Values.Add("RequestNo", Me.RequestNo)
    '        cookieNew.Values.Add("BranchID", Me.BranchID)
    '        Response.AppendCookie(cookieNew)
    '    End If

    '    Response.Redirect("rptPettyCashReimburseViewer01.aspx")
    'End Sub

End Class