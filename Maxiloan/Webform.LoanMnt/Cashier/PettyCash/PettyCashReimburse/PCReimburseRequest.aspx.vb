﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PCReimburseRequest
    Inherits Maxiloan.Webform.AccMntWebBased
    Dim j As Integer
    Dim temptotalPC As Double
    Dim temptotalPC2 As Double
#Region "Property"
    Public Property TotPCAmount() As Double
        Get
            Return CDbl(viewstate("TotPCAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotPCAmount") = Value
        End Set
    End Property


    Public Property TotPCAmount2() As Double
        Get
            Return CDbl(viewstate("TotPCAmount2"))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotPCAmount2") = Value
        End Set
    End Property

    Public Property isCheckAll() As Boolean
        Get
            Return CBool(viewstate("isCheckAll"))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isCheckAll") = Value
        End Set
    End Property

#End Region

    Dim dtChoosen As DataTable

    Private oDUController As New DataUserControlController
    Private oController As New Controller.PettyCashController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If sessioninvalid() Then
                Exit Sub
            End If
            Me.FormID = "PCREIREQ"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If
                With txtValidDate
                    .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                    ';.DataBind()
                End With

                pnlList.Visible = True
                pnlGrid.Visible = False
                pnlListChoosen.Visible = False
                GetCombo()
                Me.TotPCAmount = 0
            End If
        End If
    End Sub

    Private Sub GetCombo()
        '------------------
        'CBO BANK ACCOUNT
        '------------------
        Dim dtBankAccount As New DataTable

        dtBankAccount = oDUController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), "B", "EC")

        cboBank.DataTextField = "Name"
        cboBank.DataValueField = "ID"
        cboBank.DataSource = dtBankAccount
        cboBank.DataBind()

        cboBank.Items.Insert(0, "Select One")
        cboBank.Items(0).Value = "0"

        If Me.sesBranchId.Replace("'", "") = "014" Or Me.sesBranchId.Replace("'", "") = "099" Then
            dtBankAccount = oDUController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), "B", "")

            cboBank.DataTextField = "Name"
            cboBank.DataValueField = "ID"
            cboBank.DataSource = dtBankAccount
            cboBank.DataBind()

            cboBank.Items.Insert(0, "Select One")
            cboBank.Items(0).Value = "0"
        End If



        dtBankAccount.Clear()
        dtBankAccount = oDUController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), "", "PC")

        cbobankfind.DataTextField = "Name"
        cbobankfind.DataValueField = "ID"
        cbobankfind.DataSource = dtBankAccount
        cbobankfind.DataBind()

        cbobankfind.Items.Insert(0, "Select One")
        cbobankfind.Items(0).Value = "0"

        cbobankfind.SelectedIndex = "1"

    End Sub
#Region "Process Reset And Cancel"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'Me.SearchBy = "PCT.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and isReimburse=0"
        Me.SearchBy = "PCT.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and isReimburse=0 and STATUS='-' and STATUSACC='-' "
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlGrid.Visible = True
        With txtValidDate
            .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        End With
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        DoBind(Me.SearchBy, Me.SortBy)
        pnlGrid.Visible = True
        pnlList.Visible = True
        pnlListChoosen.Visible = False
    End Sub
#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Dim oCustomClass As New PettyCash
        Dim PCList As New PettyCash

        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = Me.SearchBy
                .SortBy = Me.SortBy
                .BusinessDate = Me.BusinessDate
            End With


            PCList = oController.GetPagingTable(oCustomClass)

            DtUserList = PCList.PagingTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgPC.DataSource = DvUserList

            'If DtUserList.Rows.Count <= 0 Then
            '    imbNext.Visible = False
            'End If

            Try
                DtgPC.DataBind()
            Catch ex As Exception

            End Try

            If DtgPC.Items.Count <= 0 Then
                BtnNext.Visible = False
            Else
                BtnNext.Visible = True
            End If
            pnlList.Visible = True
            pnlGrid.Visible = True
            lblMessage.Text = ""
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgPC.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            lblMessage.Text = ""
            'Me.SearchBy = "BranchID='" & Me.sesBranchId.Replace("'", "") & "' and isReimburse=0 and Entities.PettyCashdate <= '" & ConvertDate2(oValiddate.dateValue).ToString("yyyyMMdd") & "'"
            'Me.SearchBy = "PCT.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and PCT.isReimburse=0 and PCT.pettyCashStatus ='PC' and PettyCashdate <= '" & ConvertDate2(txtValidDate.Text).ToString("yyyyMMdd") & "' and BankAccountID = '" & cbobankfind.SelectedValue.ToString & "'"
            Me.SearchBy = "PCT.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and PCT.isReimburse=0 and STATUS='-' and STATUSACC='-' and PCT.pettyCashStatus ='PC' and PettyCashdate <= '" & ConvertDate2(txtValidDate.Text).ToString("yyyyMMdd") & "' and BankAccountID = '" & cbobankfind.SelectedValue.ToString & "'"
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            pnlGrid.Visible = True
            With txtValidDate
                .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                ';.DataBind()
            End With
        End If
    End Sub

    Private Sub ChoosePC()

        Dim i As Integer
        Dim cb As New CheckBox
        Dim dr As DataRow
        dtChoosen = New DataTable
        dtChoosen.Clear()
        For i = 0 To DtgPC.Items.Count - 1

            cb = CType(DtgPC.Items(i).FindControl("cbCheck"), CheckBox)

            If cb.Checked = True Then
                dr("PettyCashNo") = DtgPC.Items(i).Cells(1).Text
                dr("Departement") = DtgPC.Items(i).Cells(2).Text
                dr("Employee") = DtgPC.Items(i).Cells(3).Text
                dr("Description") = DtgPC.Items(i).Cells(4).Text
                dr("Amount") = DtgPC.Items(i).Cells(5).Text
                dr("PCDate") = DtgPC.Items(i).Cells(6).Text
                dtChoosen.Rows.Add(dr)
            End If
        Next

    End Sub

    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        'ChoosePC()
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            lblMessage.Text = ""
            Dim i As Integer
            Dim cb As New CheckBox
            Dim dr As DataRow
            j = 0
            dtChoosen = New DataTable
            dtChoosen.Columns.Add("PettyCashNo", System.Type.GetType("System.String"))
            dtChoosen.Columns.Add("Departement", System.Type.GetType("System.String"))
            dtChoosen.Columns.Add("Employee", System.Type.GetType("System.String"))
            dtChoosen.Columns.Add("Description", System.Type.GetType("System.String"))
            dtChoosen.Columns.Add("Amount", System.Type.GetType("System.String"))
            dtChoosen.Columns.Add("PCDate", System.Type.GetType("System.String"))

            dtChoosen.Clear()

            Dim lblPCNo As New HyperLink
            Dim lblAmt As New Label

            For i = 0 To DtgPC.Items.Count - 1

                cb = CType(DtgPC.Items(i).FindControl("cbCheck"), CheckBox)

                If cb.Checked = True Then
                    j = j + 1
                    dr = dtChoosen.NewRow()

                    lblPCNo = CType(DtgPC.Items(i).FindControl("lblPettyCash"), HyperLink)
                    lblAmt = CType(DtgPC.Items(i).FindControl("lblPCAmount"), Label)

                    dr.Item("PettyCashNo") = lblPCNo.Text
                    dr.Item("Departement") = DtgPC.Items(i).Cells(2).Text
                    dr.Item("Employee") = DtgPC.Items(i).Cells(3).Text
                    dr.Item("Description") = DtgPC.Items(i).Cells(4).Text
                    dr.Item("Amount") = lblAmt.Text
                    dr.Item("PCDate") = DtgPC.Items(i).Cells(6).Text
                    dtChoosen.Rows.Add(dr)
                End If
            Next
            If j <= 0 Then

                ShowMessage(lblMessage, "Harap Check Item", True)
                Exit Sub
            End If
            pnlList.Visible = False
            pnlGrid.Visible = False

            lblDate.Text = txtValidDate.Text
            txtDescription.Text = ""

            dtgChoosen.DataSource = dtChoosen
            dtgChoosen.DataBind()
            pnlListChoosen.Visible = True
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim oET As New PettyCash
        Dim i As Integer

        Dim dtTable As New DataTable
        Dim lObjDataRow As DataRow
        Dim hyPCNo As HyperLink
        Dim strID As String
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            dtTable.Columns.Add("PettyCashNo", System.Type.GetType("System.String"))
            strID = ""

            For i = 0 To dtgChoosen.Items.Count - 1
                lObjDataRow = dtTable.NewRow()
                hyPCNo = CType(dtgChoosen.Items(i).FindControl("hyPettyCashNo"), HyperLink)
                lObjDataRow("PettyCashNo") = hyPCNo.Text.Trim
                dtTable.Rows.Add(lObjDataRow)
                strID &= CStr(IIf(strID = "", "", ",")) & hyPCNo.Text.Replace("'", "")
            Next

            With oET
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .BankAccountID = cboBank.SelectedItem.Value.Trim
                .Description = txtDescription.Text.Trim
                .TotalAmount = Me.TotPCAmount2
                .NDtTable = dtTable
                .strID = strID.Trim
            End With

            Try
                oController.PCReimburseSave(oET)
                If oET.strDesc <> "" Then

                    ShowMessage(lblMessage, "Petty Cash Sudah reimburse oleh user Lain", True)
                    Exit Sub
                Else
                    Server.Transfer("PCReimburseRequest.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                End If
            Catch ex As Exception

                ShowMessage(lblMessage, "Proses Petty Cash Reimburse Gagal", True)
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub dtgPC_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPC.ItemDataBound
        Dim totalPCAmount As New Label
        Dim lPCAmount As New Label
        Dim NPettyCashNo As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            lPCAmount = CType(e.Item.FindControl("lblPCAmount"), Label)
            temptotalPC += CDbl(lPCAmount.Text)
            NPettyCashNo = CType(e.Item.FindControl("lblPettyCash"), HyperLink)
            NPettyCashNo.NavigateUrl = "javascript:OpenWinMain('" & Trim(NPettyCashNo.Text) & "','" & Trim(Me.sesBranchId.Replace("'", "")) & "')"
        End If
        Me.TotPCAmount = temptotalPC
        If e.Item.ItemType = ListItemType.Footer Then
            totalPCAmount = CType(e.Item.FindControl("lblTotPCAmount"), Label)
            totalPCAmount.Text = FormatNumber(temptotalPC.ToString, 0)
        End If
    End Sub

#Region "CheckAllCheckBox"
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim Chk As CheckBox

        For loopitem = 0 To CType(DtgPC.Items.Count - 1, Int16)
            Chk = New CheckBox
            Chk = CType(DtgPC.Items(loopitem).FindControl("cbCheck"), CheckBox)

            If Chk.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Chk.Checked = True
                    'Session("IsCheckAll") = "Yes"
                    Me.isCheckAll = True
                Else
                    Chk.Checked = False
                    'Session("IsCheckAll") = "No"
                    Me.isCheckAll = False
                End If
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
#End Region

    Private Sub dtgChoosen_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgChoosen.ItemDataBound
        Dim totalPCAmount2 As New Label
        Dim lPCAmount2 As New Label
        Dim NPettyCashNo2 As New HyperLink

        If e.Item.ItemIndex >= 0 Then
            lPCAmount2 = CType(e.Item.FindControl("lblPCAmountChoosen2"), Label)
            temptotalPC2 += CDbl(lPCAmount2.Text)
        End If
        Me.TotPCAmount2 = temptotalPC2
        If e.Item.ItemType = ListItemType.Footer Then
            totalPCAmount2 = CType(e.Item.FindControl("lblTotPCAmount2"), Label)
            totalPCAmount2.Text = FormatNumber(temptotalPC2.ToString, 2)
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "PCNO", Me.AppId) Then

            If e.Item.ItemIndex >= 0 Then
                NPettyCashNo2 = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink)
                NPettyCashNo2.NavigateUrl = "javascript:OpenWinMain('" & Trim(NPettyCashNo2.Text) & "','" & Trim(Me.sesBranchId.Replace("'", "")) & "')"
            End If
        End If
    End Sub

End Class