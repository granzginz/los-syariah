﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController


Public Class VerifyPettyCashReimburse
    Inherits Maxiloan.Webform.AccMntWebBased

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set

    End Property

    Private Property PettyCashNo() As String
        Get
            Return (CType(Viewstate("PettyCashNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PettyCashNo") = Value
        End Set
    End Property

    Private Property SequenceNo() As String
        Get
            Return (CType(ViewState("SequenceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SequenceNo") = Value
        End Set
    End Property
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_TotalPCAmount As Double
    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
    Private m_controller As New DataUserControlController
    Protected WithEvents GridNavigator As ucGridNav
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "VERPCREIMBURSECAB"

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            Dim dtbranch As New DataTable
            dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            If Not (dtbranch Is Nothing) Then
                lblCabang.Text = dtbranch.Rows(0)(1)
            End If 
            Me.CmdWhere = ""
            Me.SortBy = ""
            pnlViewPettyCash.Visible = False

        End If 
    End Sub
     
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("VerifyPettyCashReimburse.aspx")
    End Sub
    Dim TotalAmount As Double
    Private Sub DtgReimburseInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgReimburseInquiry.ItemDataBound
         

        If e.Item.ItemIndex >= 0 Then
             
            Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink) 
            hyRequestNo.NavigateUrl = String.Format("javascript:OpenWinViewPCReimburse('ACCMNT','{0}',{1})", hyRequestNo.Text.Trim, Me.sesBranchId)

            TotalAmount += CDbl(CType(e.Item.FindControl("lblAmount"), Label).Text)
        
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            lblTotAmount.Text = FormatNumber(TotalAmount, 2)
        End If
    End Sub
   
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim cmdwhere As String
        Dim filterby As String
        Me.SortBy = ""
        Me.CmdWhere = ""
        cmdwhere = ""
        filterby = ""


        If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
            If txtSearchBy.Text.Trim <> "" Then
                cmdwhere = String.Format("PettyCashReimburse.{0} like '%{1}%' and ", cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim.Replace("%", ""))
                filterby = String.Format("{0} = {1}  and ", IIf(cboSearchBy.SelectedItem.Value.Trim = "RequestNo", "Request NO.", cboSearchBy.SelectedItem.Value.Trim), txtSearchBy.Text.Trim)
            End If
        End If

        If txtDate.Text.Trim <> "" Then
            cmdwhere = String.Format("{0} PettyCashReimburse.RequestDate = '{1}' and ", cmdwhere, ConvertDate2(txtDate.Text.Trim).ToString("yyyyMMdd"))
            filterby = String.Format("{0} Request Date = {1} and ", filterby, txtDate.Text.Trim)
        End If

        Me.CmdWhere = String.Format("{0} PettyCashReimburse.status = 'REQ' and PettyCashReimburse.branchID = {1}", cmdwhere, Me.sesBranchId)
        Me.FilterBy = filterby & ", RequestDate = " & txtDate.Text.Trim

        BindGridInqPCReimburse()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        'End If

    End Sub
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridInqPCReimburse(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGridInqPCReimburse(Optional isFrNav As Boolean = False)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqPCReimburse As New PettyCash

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
        End With

        oInqPCReimburse = oController.InqPCReimburse(oCustomClass)

        recordCount = oInqPCReimburse.TotalRecord

        dtsEntity = oInqPCReimburse.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = SortBy
        DtgReimburseInquiry.DataSource = dtvEntity

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        Try
            DtgReimburseInquiry.DataBind()
        Catch
            DtgReimburseInquiry.CurrentPageIndex = 0
            DtgReimburseInquiry.DataBind()
        End Try

    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgReimburseInquiry.ItemCommand
        Try
            If e.CommandName.Trim = "Edit" Then 
                Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink).Text.Trim
                pnlVerify.Visible = True
                pnlSearch.Visible = False
                pnlDatagrid.Visible = False
                pnlViewPettyCash.Visible = False

                SetValueToLabel(hyRequestNo) 
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try



    End Sub

    Private Sub DtdTransactionList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtdTransactionList.ItemCommand
        Try
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim imbClaim As ImageButton
            Dim intloop As Integer
            If e.CommandName.Trim = "Verifikasi" Then
                Dim lblPettyCashNomor = CType(e.Item.FindControl("lblPettyCashNo"), HyperLink).Text.Trim
                pnlVerify.Visible = False
                pnlSearch.Visible = False
                pnlDatagrid.Visible = False
                pnlViewPettyCash.Visible = True
                DtgAgree.Visible = True

                'SetValueToLabel(hyRequestNo)

                m_TotalPCAmount = 0
                lblMessage.Visible = False

                'Me.BranchID = Request.QueryString("BranchId")
                'Me.PettyCashNo = Request.QueryString("PettyCashNo")
                Me.PettyCashNo = CType(e.Item.FindControl("lblPettyCashNo"), HyperLink).Text.Trim
                Me.SequenceNo = CType(e.Item.FindControl("lblSequenceNo"), HyperLink).Text.Trim
                DoBind()


            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)

        End Try

    End Sub
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub
#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub
    Private Sub DoBind()
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            With oCustomClass
                '.BranchId = Me.BranchID.Trim.Replace("'", "")
                '.PettyCashNo = Me.PettyCashNo.Trim
                .BranchId = Me.sesBranchId.Trim.Replace("'", "")
                .PettyCashNo = Me.PettyCashNo
                .strConnection = GetConnectionString()
                .SequenceNo = Me.SequenceNo
            End With
            oCustomClass = CType(oController.GetARecordAndDetailTableWithSeq(oCustomClass), Parameter.PettyCash)
            With oCustomClass
                '*** HEADER
                lblPettyCashNo.Text = .PettyCashNo
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDepartement.Text = .DepartementName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .PCVoucherNo
                lblDate.Text = .PCDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.PCAmount, 2)
                FillGrid(oCustomClass.PagingTable, DtgAgree)
                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub

#End Region
    Sub SetValueToLabel(ByVal reqNo As String)
        Dim oPCReimburse As New Parameter.PettyCash With {
            .strConnection = GetConnectionString(),
            .WhereCond = String.Format("PettyCashReimburse.BranchId = {0} and  PettyCashReimburse.requestNo = '{1}' ", Me.sesBranchId, reqNo),
            .SortBy = ""
        }
        '.WhereCond = String.Format("PettyCashReimburse.BranchId = {0} and  PettyCashReimburse.requestNo = '{1}' and PettyCashTransactionD.Status = '-' ", Me.sesBranchId, reqNo),
        oPCReimburse = oController.getViewPCReimburseLabel(oPCReimburse) 
        If Not oPCReimburse Is Nothing Then 
            If oPCReimburse.ListData.Rows.Count > 0 Then 
                Dim oData = oPCReimburse.ListData 
                Dim oRow As DataRow 
                oRow = oData.Rows(0)
                lblBranchRequest.Text = oRow.Item(0).ToString.Trim
                LblRequestNo.Text = oRow.Item(1).ToString.Trim
                lblRequestDate.Text = oRow.Item(2).ToString.Trim
                LblBranchBankAccount.Text = oRow.Item(3).ToString.Trim
                LblRequestAmount.Text = FormatNumber(oRow.Item(4).ToString.Trim, 2)
                lblDescription.Text = oRow.Item(5).ToString.Trim 
            End If
        End If

        oPCReimburse = oController.GetViewPCReimburseGrid(oPCReimburse) 
        DtdTransactionList.DataSource = oPCReimburse.ListData.DefaultView
        Try
            DtdTransactionList.DataBind()
        Catch
            DtdTransactionList.CurrentPageIndex = 0
            DtdTransactionList.DataBind()
        End Try 
    End Sub

    Dim TotalPCAmount
    Dim TotalAmountTransfer
    Private Sub DtdTransactionList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtdTransactionList.ItemDataBound
        

        If e.Item.ItemIndex >= 0 Then

            Dim hyPettyCashNo = CType(e.Item.FindControl("lblPettyCashNo"), HyperLink)
            hyPettyCashNo.NavigateUrl = String.Format("javascript:OpenWinViewPCInquiry('ACCMNT','{0}',{1})", hyPettyCashNo.Text.Trim, Me.sesBranchId)

            TotalPCAmount += CDbl(CType(e.Item.FindControl("lblPCAmount"), Label).Text) 
            TotalAmountTransfer += CDbl(CType(e.Item.FindControl("lblGridAmountTransfer"), Label).Text)
        
        End If
        If e.Item.ItemType = ListItemType.Footer Then 
            CType(e.Item.FindControl("lblTotPCAmount"), Label).Text = FormatNumber(TotalPCAmount, 2)

            CType(e.Item.FindControl("lblTotAmountTransfer"), Label).Text = FormatNumber(TotalAmountTransfer, 2)
        End If
    End Sub

    'Private Sub BtnVerifikasiPC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnVerifikasiPC.Click
    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If
    '    pnlVerify.Visible = False
    '    pnlViewPettyCash.Visible = True
    '    pnlSearch.Visible = False
    '    pnlDatagrid.Visible = False
    '    Btnsearch_Click(Nothing, Nothing)
    'End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lbltemp As Label
        Dim lblTotal As Label
        Dim SequenceNo As Label

        If e.Item.ItemIndex >= 0 Then
            lbltemp = CType(e.Item.FindControl("lblPCDAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalPCAmount = m_TotalPCAmount + CType(lbltemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalPCAmount.ToString, 2)
            End If
        End If
        'Dim SequenceNo = CType(e.Item.FindControl("hySequenceNo"), Label)

    End Sub


    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Try
            'If e.CommandName.Trim = "Edit" Then
            Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink).Text.Trim
            pnlVerify.Visible = True
            pnlSearch.Visible = False
            pnlDatagrid.Visible = False
            pnlViewPettyCash.Visible = True

            'SetValueToLabel(hyRequestNo)
            'End If
            Me.SequenceNo = CType(e.Item.FindControl("hySequenceNo"), HyperLink).Text.Trim
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlVerify.Visible = False
        pnlViewPettyCash.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        Btnsearch_Click(Nothing, Nothing)
    End Sub


    Private Sub BtnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerifikasi.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim oPCReimburse As New Parameter.PettyCash With {
           .strConnection = GetConnectionString(),
           .BranchId = Me.sesBranchId,
           .PettyCashNo = LblRequestNo.Text.Trim,
           .PCStatus = "CAB"
       }
        Try
            Dim result = oController.PCreimburseStatusUpdate(oPCReimburse)
            If result <> "OK" Then
                ShowMessage(lblMessage, result, True)
                Exit Sub
            Else
                pnlVerify.Visible = False
                pnlViewPettyCash.Visible = False
                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                Btnsearch_Click(Nothing, Nothing)
                ShowMessage(lblMessage, "Proses Verifikasi Petty Cash Berhasil", False)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, "Proses Verifikasi Petty Cash  Gagal", True)
            Exit Sub
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chk As CheckBox
        Dim lblSequenceNo As Label
        Dim hasil As Integer
        Dim cmdwhere As String
        Dim isCheck As Boolean
        isCheck = False
        For intloop = 0 To DtgAgree.Items.Count - 1
            chk = CType(DtgAgree.Items(intloop).FindControl("chk"), CheckBox)
            If chk.Checked Then
                isCheck = True
            End If
        Next
        If isCheck = False Then
            ShowMessage(lblMessage, "Harap dipilih Item petty cash", True)
            Exit Sub
        End If

        For intloop = 0 To DtgAgree.Items.Count - 1
            chk = CType(DtgAgree.Items(intloop).Cells(0).FindControl("chk"), CheckBox)
            lblSequenceNo = CType(DtgAgree.Items(intloop).Cells(1).FindControl("hySequenceNo"), Label)
            If chk.Checked Then
                Dim oPCReimburse As New Parameter.PettyCash With {
           .strConnection = GetConnectionString(),
           .BranchId = Me.sesBranchId,
           .PettyCashNo = LblRequestNo.Text.Trim,
           .PCStatus = "CAB",
           .PCDStatus = "A",
           .SequenceNo = lblSequenceNo.Text.Trim,
           .PettyCashNoPDC = Me.PettyCashNo
       }

                Try
                    Dim result = oController.PCreimburseStatusUpdate(oPCReimburse)
                    If result <> "OK" Then
                        ShowMessage(lblMessage, result, True)
                        Exit Sub
                    Else
                        'pnlVerify.Visible = False
                        SetValueToLabel(LblRequestNo.Text.Trim)
                        pnlViewPettyCash.Visible = False
                        pnlSearch.Visible = False
                        pnlDatagrid.Visible = False
                        pnlViewPettyCash.Visible = False                        
                        pnlVerify.Visible = True
                        pnlBack.Visible = True                        
                        'Btnsearch_Click(Nothing, Nothing)
                        ShowMessage(lblMessage, "Proses Approval Petty Cash Berhasil", False)
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, "Proses Approval Petty Cash  Gagal", True)
                    Exit Sub
                End Try
            End If
        Next

    End Sub

    Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chk As CheckBox
        Dim lblSequenceNo As Label
        Dim hasil As Integer
        Dim cmdwhere As String
        Dim isCheck As Boolean

        isCheck = False
        For intloop = 0 To DtgAgree.Items.Count - 1
            chk = CType(DtgAgree.Items(intloop).FindControl("chk"), CheckBox)
            If chk.Checked Then
                isCheck = True
            End If
        Next
        If isCheck = False Then
            ShowMessage(lblMessage, "Harap dipilih Item petty cash", True)
            Exit Sub
        End If

        For intloop = 0 To DtgAgree.Items.Count - 1
            chk = CType(DtgAgree.Items(intloop).Cells(0).FindControl("chk"), CheckBox)
            lblSequenceNo = CType(DtgAgree.Items(intloop).Cells(1).FindControl("hySequenceNo"), Label)

            If chk.Checked Then
                Dim oPCReimburse As New Parameter.PettyCash With {
           .strConnection = GetConnectionString(),
           .BranchId = Me.sesBranchId,
           .PettyCashNo = LblRequestNo.Text.Trim,
           .PCStatus = "CAB",
           .PCDStatus = "J",
           .SequenceNo = lblSequenceNo.Text.Trim,
          .PettyCashNoPDC = Me.PettyCashNo
       }

                Try
                    Dim result = oController.PCreimburseStatusUpdate(oPCReimburse)
                    If result <> "OK" Then
                        ShowMessage(lblMessage, result, True)
                        Exit Sub
                    Else
                        'pnlVerify.Visible = False
                        SetValueToLabel(LblRequestNo.Text.Trim)
                        pnlVerify.Visible = True
                        pnlViewPettyCash.Visible = False
                        pnlSearch.Visible = False
                        pnlDatagrid.Visible = False
                        pnlViewPettyCash.Visible = False
                        pnlVerify.Visible = True
                        pnlBack.Visible = True
                        'Btnsearch_Click(Nothing, Nothing)
                        ShowMessage(lblMessage, "Proses Reject Petty Cash  Berhasil", False)
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, "Proses Reject Petty Cash  Gagal", True)
                    Exit Sub
                End Try
            End If
        Next
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        pnlVerify.Visible = False
        pnlViewPettyCash.Visible = False
        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlViewPettyCash.Visible = False
        Btnsearch_Click(Nothing, Nothing)
    End Sub
End Class