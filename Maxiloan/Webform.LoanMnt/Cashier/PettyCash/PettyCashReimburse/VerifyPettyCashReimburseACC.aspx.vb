﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper

Public Class VerifyPettyCashReimburseACC
    Inherits Maxiloan.Webform.AccMntWebBased

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

    Private Property FilterBranch() As String
        Get
            Return CType(ViewState("FilterBranch"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBranch") = Value
        End Set
    End Property

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
    Private m_controller As New DataUserControlController
    Protected WithEvents GridNavigator As ucGridNav
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "VERPCREIMBURSEACC"
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            With cboParent
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With
            Me.CmdWhere = ""
            Me.SortBy = ""

            If Request("thingstodo") = "1" Then
                Dim cmdwhere As String
                Dim filterby As String
                cmdwhere = ""
                filterby = ""
                FilterBranch = ""
                Me.CmdWhere = String.Format("{0} PettyCashReimburse.status = 'REQ'", cmdwhere)
                Me.FilterBy = filterby + "Status = REQ "
                FilterBranch = cboParent.SelectedItem.Value.Trim
                BindGridInqPCReimburse()
                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
            End If

        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("VerifyPettyCashReimburseACC.aspx")
    End Sub
    Dim TotalAmount As Double
    Private Sub DtgReimburseInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgReimburseInquiry.ItemDataBound 
        If e.Item.ItemIndex >= 0 Then 
            Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            hyRequestNo.NavigateUrl = String.Format("javascript:OpenWinViewPCReimburse('ACCMNT','{0}',{1})", hyRequestNo.Text.Trim, FilterBranch) 
            TotalAmount += CDbl(CType(e.Item.FindControl("lblAmount"), Label).Text)
       
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            lblTotAmount.Text = FormatNumber(TotalAmount, 2)
        End If
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
        Dim cmdwhere As String
        Dim filterby As String
        Me.SortBy = ""
        Me.CmdWhere = ""
        cmdwhere = ""
        filterby = ""
        FilterBranch = ""

        If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
            If txtSearchBy.Text.Trim <> "" Then
                cmdwhere = String.Format("PettyCashReimburse.{0} like '%{1}%' and ", cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim.Replace("%", ""))
                filterby = String.Format("{0} = {1}  and ", IIf(cboSearchBy.SelectedItem.Value.Trim = "RequestNo", "Request NO.", cboSearchBy.SelectedItem.Value.Trim), txtSearchBy.Text.Trim)
            End If
        End If

        If txtDate.Text.Trim <> "" Then
            cmdwhere = String.Format("{0} PettyCashReimburse.RequestDate = '{1}' and ", cmdwhere, ConvertDate2(txtDate.Text.Trim).ToString("yyyyMMdd"))
            filterby = String.Format("{0} Request Date = {1} and ", filterby, txtDate.Text.Trim)
        End If

        If cboParent.SelectedValue.Trim = "ALL" Then
            Me.CmdWhere = String.Format("{0} PettyCashReimburse.status = 'CAB'", cmdwhere)
        Else
            Me.CmdWhere = String.Format("{0} PettyCashReimburse.status = 'CAB' and PettyCashReimburse.branchID LIKE '{1}' ", cmdwhere, cboParent.SelectedItem.Value.Trim.Replace("%", ""))
        End If
        Me.FilterBy = filterby + "Status = CAB "

        '' Cache Branchid dari combo karena posisi kita di HO 
        '' dan branchid tergantung dari combo 
        FilterBranch = cboParent.SelectedItem.Value.Trim
        BindGridInqPCReimburse()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        'End If

    End Sub


    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridInqPCReimburse(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGridInqPCReimburse(Optional isFrNav As Boolean = False)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqPCReimburse As New PettyCash

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
        End With

        oInqPCReimburse = oController.InqPCReimburse(oCustomClass)

        recordCount = oInqPCReimburse.TotalRecord

        dtsEntity = oInqPCReimburse.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = SortBy
        DtgReimburseInquiry.DataSource = dtvEntity

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        Try
            DtgReimburseInquiry.DataBind()
        Catch
            DtgReimburseInquiry.CurrentPageIndex = 0
            DtgReimburseInquiry.DataBind()
        End Try

    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgReimburseInquiry.ItemCommand
        Try
            If e.CommandName.Trim = "Edit" Then
                Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink).Text.Trim
                Me.BranchID = CType(e.Item.FindControl("lblBranchId"), Label).Text

                pnlVerify.Visible = True
                Panel2.Visible = True
                Panel1.Visible = False
                pnlSearch.Visible = False
                pnlDatagrid.Visible = False
                SetValueToLabel(hyRequestNo)

                Dim oData As New DataTable
                oData = Get_UserApproval("PCR", Me.sesBranchId.Replace("'", ""), CDec(LblRequestAmount.Text))
                cboApprovedBy.DataSource = oData.DefaultView
                cboApprovedBy.DataTextField = "Name"
                cboApprovedBy.DataValueField = "ID"
                cboApprovedBy.DataBind()
                cboApprovedBy.Items.Insert(0, "Select One")
                cboApprovedBy.Items(0).Value = "0"

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Sub SetValueToLabel(ByVal reqNo As String)
        Dim oPCReimburse As New Parameter.PettyCash With {
            .strConnection = GetConnectionString(),
            .WhereCond = String.Format("PettyCashReimburse.requestNo = '{0}'", reqNo),
            .SortBy = ""
        }
        oPCReimburse = oController.getViewPCReimburseLabel(oPCReimburse)
        If Not oPCReimburse Is Nothing Then
            If oPCReimburse.ListData.Rows.Count > 0 Then
                Dim oData = oPCReimburse.ListData
                Dim oRow As DataRow
                oRow = oData.Rows(0)
                lblBranchRequest.Text = oRow.Item(0).ToString.Trim
                LblRequestNo.Text = oRow.Item(1).ToString.Trim
                lblRequestDate.Text = oRow.Item(2).ToString.Trim
                LblBranchBankAccount.Text = oRow.Item(3).ToString.Trim
                LblRequestAmount.Text = FormatNumber(oRow.Item(4).ToString.Trim, 2)
                lblDescription.Text = oRow.Item(5).ToString.Trim
                lblNoMemo.Text = oRow.Item(14).ToString.Trim
                lblNum.Text = oRow.Item(15).ToString.Trim
            End If
        End If

        oPCReimburse = oController.GetViewPCReimburseGrid(oPCReimburse)
        DtdTransactionList.DataSource = oPCReimburse.ListData.DefaultView
        Try
            DtdTransactionList.DataBind()
        Catch
            DtdTransactionList.CurrentPageIndex = 0
            DtdTransactionList.DataBind()
        End Try

        If lblNum.Text <> 0 Then
            oPCReimburse = oController.GetViewPCReimburseGridHistoryReject(oPCReimburse)
            DtgAgreeHistory.DataSource = oPCReimburse.ListData.DefaultView
            Try
                DtgAgreeHistory.DataBind()
            Catch
                DtgAgreeHistory.CurrentPageIndex = 0
                DtgAgreeHistory.DataBind()
            End Try
            panelHistory.Visible = True
        Else
            panelHistory.Visible = False
        End If
    End Sub

    Dim TotalPCAmount
    Dim TotalAmountTransfer
    Private Sub DtdTransactionList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtdTransactionList.ItemDataBound
         
        If e.Item.ItemIndex >= 0 Then

            Dim hyPettyCashNo = CType(e.Item.FindControl("lblPettyCashNo"), HyperLink)
            hyPettyCashNo.NavigateUrl = String.Format("javascript:OpenWinViewPCInquiryEdit('ACCMNT','{0}')", hyPettyCashNo.Text.Trim)

            TotalPCAmount += CDbl(CType(e.Item.FindControl("lblPCAmount"), Label).Text)
            TotalAmountTransfer += CDbl(CType(e.Item.FindControl("lblGridAmountTransfer"), Label).Text)
        
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("lblTotPCAmount"), Label).Text = FormatNumber(TotalPCAmount, 2)

            CType(e.Item.FindControl("lblTotAmountTransfer"), Label).Text = FormatNumber(TotalAmountTransfer, 2)
        End If
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlVerify.Visible = False
        Panel1.Visible = False
        Panel2.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        Btnsearch_Click(Nothing, Nothing)
    End Sub


    Private Sub BtnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerifikasi.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim oPCReimburse As New Parameter.PettyCash With {
           .strConnection = GetConnectionString(),
           .BranchId = FilterBranch,
           .PettyCashNo = LblRequestNo.Text.Trim,
           .PCStatus = "ACC",
           .ApprovalFrom = Me.Loginid,
           .ApprovalBy = cboApprovedBy.SelectedValue,
           .strAmount = LblRequestAmount.Text,
           .BusinessDate = Me.BusinessDate,
           .notes = "-"
       }
        Try
            Dim result = oController.PCreimburseStatusUpdate(oPCReimburse)
            If result <> "OK" Then
                ShowMessage(lblMessage, result, True)
                Exit Sub
            Else

                pnlVerify.Visible = False
                Panel1.Visible = False
                Panel2.Visible = False
                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                Btnsearch_Click(Nothing, Nothing)
                ShowMessage(lblMessage, "Proses Verifikasi Petty Cash Berhasil", False)

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, "Proses Verifikasi Petty Cash  Gagal", True)
            Exit Sub
        End Try
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlVerify.Visible = False
        Panel1.Visible = False
        Panel2.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        Btnsearch_Click(Nothing, Nothing)
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim oPCReimburse As New Parameter.PettyCash With {
           .strConnection = GetConnectionString(),
           .PettyCashNo = LblRequestNo.Text.Trim,
           .PCStatus = "REJ",
           .IsReimburse = 0,
           .PettyCashStatus = "RJ",
           .notes = ("Reject Accounting - " + txtnotes.Text.Trim),
           .BranchId = Me.sesBranchId.Trim.Replace("'", ""),
           .RejectBy = Me.Loginid,
           .RejectDate = Me.BusinessDate
       }
        Try
            Dim result = oController.PCreimburseStatusReject(oPCReimburse)
            If result <> "OK" Then
                ShowMessage(lblMessage, result, True)
                Exit Sub
            Else
                pnlVerify.Visible = False
                Panel1.Visible = False
                Panel2.Visible = False
                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                Panel2.Visible = False
                Panel1.Visible = False
                Btnsearch_Click(Nothing, Nothing)
                ShowMessage(lblMessage, "Proses Reject Petty Cash Berhasil", False)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, "Proses Reject Petty Cash  Gagal", True)
            Exit Sub
        End Try
    End Sub
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        If cboStatus.SelectedItem.Value = "RJ" Then
            Panel1.Visible = True
            Panel2.Visible = False
        ElseIf cboStatus.SelectedItem.Value = "VR" Then
            Panel1.Visible = False
            Panel2.Visible = True
        End If
    End Sub
    Private Sub btnhistrans_Click(sender As Object, e As EventArgs) Handles btnhistrans.Click
        Dim oClass As New Parameter.ControlsRS
        oClass.SelectedBranch = Me.BranchID
        oClass.AmountTransfer = LblRequestAmount.Text.Replace(",", "")
        oClass.AreaID = "ALL"
        oClass.Memo = lblNoMemo.Text.Trim
        oClass.Notes = lblDescription.Text.Trim

        Response.AppendCookie(Webform.UserController.RSConfig.setCookies(Request.Cookies("RSCOOKIES"), oClass))

        Dim strReportFile As String = "../../../../Webform.Reports/ucRSViewerPopup.aspx?formid=CEKTRANS&rsname=/BataviaReport/CashMgt/hisTransaksiPembayaran"

        Response.Write("<script language = javascript>" & vbCrLf _
                        & "var x = screen.width; " & vbCrLf _
                        & "var y = screen.height; " & vbCrLf _
                           & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                           & "</script>")
    End Sub
End Class