﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PCReimburseReconcileNextStep.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PCReimburseReconcileNextStep" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PCReimburseReconcileNextStep</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                TERIMA PETTY CASH
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Rekening Bank
            </label>
            <asp:Label ID="LblBankAccount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:Label ID="LblRequestDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgReconcile" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyField="RequestNo" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO REQUEST">
                            <HeaderStyle Width="12%"></HeaderStyle>
                            <ItemStyle Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH REQUEST">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" Width="17%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTTotalAmount" runat="server" Text='<%# FormatNumber(Container.DataItem("TotalAmount"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH TRANSFER">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" Width="17%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAmountTransfer" runat="server" Text='<%# FormatNumber(Container.DataItem("AmountTransfer"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="RequestDate" HeaderText="TGL REQUEST" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="StatusDate" HeaderText="TGL TRANSFER" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnReconcile" runat="server" Text="Reconcile" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
