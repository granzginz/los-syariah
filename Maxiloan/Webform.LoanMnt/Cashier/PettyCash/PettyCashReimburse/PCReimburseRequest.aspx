﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PCReimburseRequest.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PCReimburseRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PCReimburseRequest</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinMain(pPettyCashNo, pBranchId) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?BranchId=' + pBranchId + '&PettyCashNo=' + pPettyCashNo + '&style=AccMnt', null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlListChoosen" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PETTY CASH REIMBURSE
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal &lt;=
                </label>
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Rekening Bank
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Rekening Bank"
                    Display="Dynamic" ControlToValidate="cboBank" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req"> Keterangan</label>
                <asp:TextBox ID="txtDescription" runat="server" MaxLength="500"  TextMode="MultiLine"  style="height: 31px; width: 400px;"  ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap isi Keterangan"
                    Display="Dynamic" ControlToValidate="txtDescription" InitialValue='""' CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>                        
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI PETTY CASH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgChoosen" runat="server" ShowFooter="True" CssClass="grid_general"
                        AutoGenerateColumns="False" PageSize="1000">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="PettyCashNo"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="NO PETTY CASH">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("PettyCashNo")%>'
                                        Visible="True">
                                    </asp:HyperLink>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot2" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DEPARTEMEN">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartement" runat="server" Text='<%#Container.DataItem("Departement")%>'
                                        Visible="True">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA KARYAWAN">
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%# Container.DataItem("Employee") %>'
                                        Visible="True">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'
                                        Visible="True">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPCAmountChoosen2" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'
                                        Visible="True">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPCAmount2" runat="server" Visible="true"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PCDate" HeaderText="TGL PC" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PETTY CASH REIMBURSE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal &lt;=</label>
                <asp:TextBox runat="server" ID="txtValidDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValidDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                    <label class="label_req">
                        Rekening Bank
                    </label>
                    <asp:DropDownList ID="cbobankfind" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rqcbobankfind" runat="server" InitialValue="0" Display="Dynamic"
                        ControlToValidate="cbobankfind" ErrorMessage="Harap pilih Rekening Bank" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server" Visible="true">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI PETTY CASH
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPC" runat="server" ShowFooter="True"  CssClass="grid_general"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll"
                                        AutoPostBack="True"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCheck" Visible="True" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PettyCashNo" HeaderText="NO PETTY CASH">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPettyCash" runat="server" Text='<%#Container.DataItem("PettyCashNo")%>'
                                        Visible="True">
                                    </asp:HyperLink>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DEPARTEMENTNAME" SortExpression="DEPARTMENTNAME" HeaderText="DEPARTMEN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="EMPLOYEENAME" SortExpression="EMPLOYEENAME" HeaderText="NAMA KARYAWAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DESCRIPTION" SortExpression="DESCRIPTION" HeaderText="KETERANGAN">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'
                                        Visible="True">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPCAmount" runat="server" Font-Bold="True" Visible="true"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PettyCashDate" SortExpression="PettyCashDate" HeaderText="TGL PC"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Visible="True" Text="Next"
                CssClass="small button green"></asp:Button>
        </div>
    </asp:Panel>
    <br />
    </form>
</body>
</html>
