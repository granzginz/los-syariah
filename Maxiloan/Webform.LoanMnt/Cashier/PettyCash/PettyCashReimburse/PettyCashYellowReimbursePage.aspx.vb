﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper


Public Class PettyCashYellowReimbursePage
    Inherits Maxiloan.Webform.AccMntWebBased

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

    Private Property FilterBranch() As String
        Get
            Return CType(ViewState("FilterBranch"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBranch") = Value
        End Set
    End Property

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
    Private m_controller As New DataUserControlController
    Protected WithEvents GridNavigator As ucGridNav
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "PRNPCREIMBURSE"
    
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then

            If Request.QueryString("strFileLocation") <> "" Then 
                Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
                Dim strFileLocation = String.Format("http://{0}/{1}/XML/{2}.pdf", Request.ServerVariables("SERVER_NAME"), strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1), Request.QueryString("strFileLocation"))
                Response.Write("<script language = javascript>" & vbCrLf & "var x = screen.width; " & vbCrLf & "var y = screen.heigth; " & vbCrLf & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf & "</script>")
            End If


            With cboParent
                .DataValueField = "ID"
                .DataTextField = "Name"
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString) 
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                Else
                    Dim dtbranch As New DataTable
                    dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0" 
                    If Not (dtbranch Is Nothing) Then
                        .Items.Insert(1, dtbranch.Rows(0)(1))
                        .Items(1).Value = Me.sesBranchId 
                    End If
                End If
            End With
            Me.CmdWhere = ""
            Me.SortBy = ""
        End If
    End Sub


    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("VerifyPettyCashReimburseACC.aspx")
    End Sub

    Dim TotalAmount As Double
    Private Sub DtgReimburseInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgReimburseInquiry.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            hyRequestNo.NavigateUrl = String.Format("javascript:OpenWinViewPCReimburse('ACCMNT','{0}',{1})", hyRequestNo.Text.Trim, FilterBranch)
            TotalAmount += CDbl(CType(e.Item.FindControl("lblAmount"), Label).Text)

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            lblTotAmount.Text = FormatNumber(TotalAmount, 2)
        End If
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
        Dim cmdwhere As String
        Dim filterby As String
        Me.SortBy = ""
        Me.CmdWhere = ""
        cmdwhere = ""
        filterby = ""
        FilterBranch = ""

        If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
            If txtSearchBy.Text.Trim <> "" Then
                cmdwhere = String.Format("PettyCashReimburse.{0} like '%{1}%' and ", cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim.Replace("%", ""))
                filterby = String.Format("{0} = {1}  and ", IIf(cboSearchBy.SelectedItem.Value.Trim = "RequestNo", "Request NO.", cboSearchBy.SelectedItem.Value.Trim), txtSearchBy.Text.Trim)
            End If
        End If

        If txtDate.Text.Trim <> "" Then
            cmdwhere = String.Format("{0} PettyCashReimburse.RequestDate = '{1}' and ", cmdwhere, ConvertDate2(txtDate.Text.Trim).ToString("yyyyMMdd"))
            filterby = String.Format("{0} Request Date = {1} and ", filterby, txtDate.Text.Trim)
        End If

        Me.CmdWhere = String.Format("{0}   PettyCashReimburse.branchID = {1}", cmdwhere, cboParent.SelectedItem.Value.Trim)
        Me.FilterBy = filterby

        '' Cache Branchid dari combo karena posisi kita di HO 
        '' dan branchid tergantung dari combo 
        FilterBranch = cboParent.SelectedItem.Value.Trim
        BindGridInqPCReimburse()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        'End If

    End Sub


    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridInqPCReimburse(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGridInqPCReimburse(Optional isFrNav As Boolean = False)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqPCReimburse As New PettyCash

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
        End With

        oInqPCReimburse = oController.InqPCReimburse(oCustomClass)

        recordCount = oInqPCReimburse.TotalRecord

        dtsEntity = oInqPCReimburse.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = SortBy
        DtgReimburseInquiry.DataSource = dtvEntity

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        Try
            DtgReimburseInquiry.DataBind()
        Catch
            DtgReimburseInquiry.CurrentPageIndex = 0
            DtgReimburseInquiry.DataBind()
        End Try

    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgReimburseInquiry.ItemCommand
        Try
            If e.CommandName.Trim = "Edit" Then
                Dim hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink).Text.Trim
                pnlVerify.Visible = True
                pnlSearch.Visible = False
                pnlDatagrid.Visible = False
                ' SetValueToLabel(hyRequestNo)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs) 
        Dim loopitem As Int16 
        For loopitem = 0 To CType(DtgReimburseInquiry.Items.Count - 1, Int16) 
            Dim Chk = CType(DtgReimburseInquiry.Items(loopitem).FindControl("cbCheck"), CheckBox) 
            If Chk.Enabled Then 
                Chk.Checked = CType(sender, CheckBox).Checked 
            Else
                Chk.Checked = False
            End If
        Next 
    End Sub
    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        Dim j = 0
        Dim dtChoosen As IList(Of String) = New List(Of String)
        For i = 0 To DtgReimburseInquiry.Items.Count - 1 
            Dim cb = CType(DtgReimburseInquiry.Items(i).FindControl("cbCheck"), CheckBox) 
            If cb.Checked = True Then
                j += 1  
                dtChoosen.Add(CType(DtgReimburseInquiry.Items(i).FindControl("hyRequestNo"), HyperLink).Text.Trim)
            End If
        Next
        If j <= 0 Then 
            ShowMessage(lblMessage, "Harap Check Item", True)
            Exit Sub
        End If
        Session(COOKIES_PETTY_CASH_VOUCHER) = dtChoosen

        'Dim cookie As HttpCookie = Request.Cookies(COOKIES_PETTY_CASH_VOUCHER)
        'If Not cookie Is Nothing Then
        '    cookie.Values("PettyCashNo") = dtChoosen
        '    Response.AppendCookie(cookie)
        'Else
        '    Dim cookieNew As New HttpCookie(COOKIES_PETTY_CASH_VOUCHER)
        '    cookieNew.Values.Add("PettyCashNo", dtChoosen)
        '    Response.AppendCookie(cookieNew)
        'End If
        Response.Redirect("PettyCashYellowReimbursePageView.aspx")


        'ChoosePC()
        'If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
        '    lblMessage.Text = ""
        '    Dim i As Integer
        '    Dim cb As New CheckBox
        '    Dim dr As DataRow
        '    j = 0
        '    dtChoosen = New DataTable
        '    dtChoosen.Columns.Add("PettyCashNo", System.Type.GetType("System.String"))
        '    dtChoosen.Columns.Add("Departement", System.Type.GetType("System.String"))
        '    dtChoosen.Columns.Add("Employee", System.Type.GetType("System.String"))
        '    dtChoosen.Columns.Add("Description", System.Type.GetType("System.String"))
        '    dtChoosen.Columns.Add("Amount", System.Type.GetType("System.String"))
        '    dtChoosen.Columns.Add("PCDate", System.Type.GetType("System.String"))

        '    dtChoosen.Clear()

        '    Dim lblPCNo As New HyperLink
        '    Dim lblAmt As New Label

        '    For i = 0 To DtgPC.Items.Count - 1

        '        cb = CType(DtgPC.Items(i).FindControl("cbCheck"), CheckBox)

        '        If cb.Checked = True Then
        '            j = j + 1
        '            dr = dtChoosen.NewRow()

        '            lblPCNo = CType(DtgPC.Items(i).FindControl("lblPettyCash"), HyperLink)
        '            lblAmt = CType(DtgPC.Items(i).FindControl("lblPCAmount"), Label)

        '            dr.Item("PettyCashNo") = lblPCNo.Text
        '            dr.Item("Departement") = DtgPC.Items(i).Cells(2).Text
        '            dr.Item("Employee") = DtgPC.Items(i).Cells(3).Text
        '            dr.Item("Description") = DtgPC.Items(i).Cells(4).Text
        '            dr.Item("Amount") = lblAmt.Text
        '            dr.Item("PCDate") = DtgPC.Items(i).Cells(6).Text
        '            dtChoosen.Rows.Add(dr)
        '        End If
        '    Next
        '    If j <= 0 Then

        '        ShowMessage(lblMessage, "Harap Check Item", True)
        '        Exit Sub
        '    End If
        '    pnlList.Visible = False
        '    pnlGrid.Visible = False

        '    lblDate.Text = txtValidDate.Text
        '    txtDescription.Text = ""

        '    dtgChoosen.DataSource = dtChoosen
        '    dtgChoosen.DataBind()
        '    pnlListChoosen.Visible = True
        'End If
    End Sub

    'select Description , accountName as dibayarke, bm.BankName , accountno, totalamount
    ' from  PettyCashreimburse  pc
    'left join bankaccount  ba on pc.branchid=  ba.branchid and pc.BankAccountId=ba.BankAccountId
    'left join BankMaster bm on ba.bankid=bm.bankid  where RequestNo='121PCR201503000003'
End Class