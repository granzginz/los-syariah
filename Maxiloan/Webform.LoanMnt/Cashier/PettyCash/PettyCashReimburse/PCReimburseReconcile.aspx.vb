﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.BusinessProcess
'Imports Maxiloan.Parameter.CollZipCode
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PCReimburseReconcile
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Protected WithEvents oBankAccount As ucBankAccountReconcile
    Private Const FILE_NAME_NEXT As String = "PCReimburseReconcileNextStep.Aspx"
    Dim oController As New GeneralPagingController
    Private Const SP_NAME_PAGING As String = "spPCReimburseSelect"


#End Region
#Region "Property"

    Private Property BankAccountID() As String
        Get
            Return (CType(viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

    Private Property StartSelectionDate() As String
        Get
            Return (CType(viewstate("StartSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartSelectionDate") = Value
        End Set
    End Property

    Private Property BulkofRequest() As String
        Get
            Return (CType(viewstate("BulkofRequest"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BulkofRequest") = Value
        End Set
    End Property

#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
       
        If Not IsPostBack Then
            'If IsSingleBranch And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 Then
            'txtRequestDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            oSearchBy.ListData = "DBO.pettycashreimburse.RequestNo,Request No-dbo.pettycashreimburse.Description, Description"
            oSearchBy.BindData()
            InitialDefaultPanel()

            'Else
            'KickOutForMultipleBranch()
            'End If
        End If

        Me.StartSelectionDate = txtRequestDate.Text
    End Sub
#End Region
#Region "InitialDefaultPanel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub
#End Region
#Region "Ketika Tombol Next Dipijit "
    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click

        Dim LoopNext As Int32
        Dim TempRequestNo As String

        For LoopNext = 0 To DtgReimburse.Items.Count - 1
            Dim CurrentCheckBox As New CheckBox
            Dim strRequestNo As New Label
            Dim koma As String
            'CurrentCheckBox = CType(DtgReimburse.Items(LoopNext).FindControl("CheckBoxReimburse"), CheckBox)
            'strRequestNo = CType(DtgReimburse.Items(LoopNext).FindControl("LblRequestNo"), Label)
            CurrentCheckBox = CType(DtgReimburse.Items(LoopNext).Cells(0).FindControl("CheckBoxReimburse"), CheckBox)
            strRequestNo = CType(DtgReimburse.Items(LoopNext).Cells(1).FindControl("LblRequestNo"), Label)

            'If CurrentCheckBox.Checked = True Then
            If CurrentCheckBox.Checked Then
                If TempRequestNo = "" Then koma = "" Else koma = ","
                TempRequestNo = TempRequestNo & koma & strRequestNo.Text.Trim
            End If
        Next

        If TempRequestNo <> "" Then
            NextProcess(TempRequestNo)
        Else
            Exit Sub
        End If

    End Sub
#End Region

#Region "Proses ke langkah selanjutnya"

    Public Sub NextProcess(ByVal BulkRequest As String)
        Dim RequestThatChecked As String
        RequestThatChecked = Replace(BulkRequest, ",", "','").Trim
        RequestThatChecked = " And RequestNo in ('" & RequestThatChecked & "')".Trim

        Me.BulkofRequest = RequestThatChecked
        SendCookies()
        Dim paramBankAccout As String
        Dim paramRequestDate As String

        Server.Transfer(FILE_NAME_NEXT)

    End Sub

#End Region
#Region "Ketika Tombol Search Dipijit"
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        PnlGrid.Visible = True

        Me.SearchBy = " And BranchID in( '" & Replace(Me.sesBranchId, "'", "") & "') "

        If oSearchBy.Text.Trim <> "" Then
            If Right(oSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy = " And " & oSearchBy.ValueID.Replace("'", "''") & " Like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            Else
                Me.SearchBy = " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            End If
        End If
        If txtRequestDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " And RequestDate = '" & ConvertDate2(Me.StartSelectionDate).ToString("yyyyMMdd") & "'"
        End If

        Me.BankAccountID = oBankAccount.BankAccountID
        txtRequestDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "BindGrid"
    Public Sub BindGrid(ByVal cmdWhere As String, ByVal cmdsortby As String)
        Dim oEntities As New Parameter.GeneralPaging

        If Me.SortBy = "" Then
            Me.SortBy = " RequestNo "
        End If

        With oEntities
            .SpName = SP_NAME_PAGING
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)
        Dim dt As DataSet
        If Not oEntities Is Nothing Then
            dt = oEntities.ListDataReport
        End If
        DtgReimburse.DataSource = dt
        DtgReimburse.DataBind()
    End Sub
#End Region
#Region "SortGrid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Ketika Tombol Reset Dipijit"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = ""
        txtRequestDate.Text = ""
        oBankAccount.BankAccountID = ""
        oSearchBy.Text = ""
        'BindGrid(Me.SearchBy)
        PnlGrid.Visible = False

    End Sub
#End Region
#Region "CheckStatus"

    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusReimburse As CheckBox


        For loopitem = 0 To CType(DtgReimburse.Items.Count - 1, Int16)
            ChkStatusReimburse = New CheckBox
            ChkStatusReimburse = CType(DtgReimburse.Items(loopitem).FindControl("CheckBoxReimburse"), CheckBox)

            If ChkStatusReimburse.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    ChkStatusReimburse.Checked = True
                Else
                    ChkStatusReimburse.Checked = False
                End If
            Else
                ChkStatusReimburse.Checked = False
            End If

        Next

    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_PETTY_CASH_REIMBURSE)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = Me.SearchBy & Me.BulkofRequest
            cookie.Values("BankAccountID") = Me.BankAccountID
            cookie.Values("BankAccountName") = oBankAccount.BankAccountName
            cookie.Values("RequestDate") = txtRequestDate.Text
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_PETTY_CASH_REIMBURSE)
            cookieNew.Values("SearchBy") = Me.SearchBy
            cookieNew.Values("BankAccountID") = Me.BankAccountID
            cookieNew.Values("BankAccountName") = oBankAccount.BankAccountName
            cookieNew.Values("RequestDate") = txtRequestDate.Text
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region

End Class