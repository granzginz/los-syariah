﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PettyCashYellowReimbursePage.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PettyCashYellowReimbursePage" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %> 
<%@ Register Src="../../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Cetak Form Kuning Reimburse</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinViewPCReimburse(pStyle, pRequestNo, pBranch) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashReimburse/ViewPettyCashReimburse.aspx?style=' + pStyle + '&RequestNo=' + pRequestNo + '&Branch=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function OpenWinViewPCInquiry(pStyle, pPettyCashNo, pBranch) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?style=' + pStyle + '&PettyCashNo=' + pPettyCashNo + '&BranchId=' + pBranch, 'userlookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1" /> 
     <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />

      <div class="form_title">
            <div class="title_strip" ></div>
            <div class="form_single">
                <h3>PETTY CASH REIMBURSE</h3>
            </div>
        </div>
         
    <asp:Panel ID="pnlSearch" runat="server"> 
        <div class="form_box">
             <div class="form_single">
               <label class="label_req"> Cabang </label>
                <asp:DropDownList ID="cboParent" runat="server" />
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" />
              </div>
         </div>
        <div class="form_box">
             <div class="form_single">
                <label> Tanggal </label>
                <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDate" Format="dd/MM/yyyy"> </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
             <div class="form_single">
                <label> Cari Berdasarkan </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="88px" ></asp:TextBox>
            </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
 
     <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PETTY CASH REIMBURSE 
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgReimburseInquiry" runat="server" ShowFooter="True" Visible="true" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" /> <ItemStyle CssClass="item_grid" /> <FooterStyle CssClass="item_grid" />
                        <Columns>

                         <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>


                           <%-- <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle HorizontalAlign="Center"   CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="hypPrint" runat="server" Text="Verifikasi"  CommandName="Edit"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                <ItemStyle Width="15%" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestBy" HeaderText="DIAJUKAN OLEH">
                                <ItemStyle Width="20%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblRequestBy" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <ItemStyle Width="30%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>' />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="20%"></FooterStyle>
                                <FooterTemplate> Total </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                <ItemStyle HorizontalAlign="Right" Width="8%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%" />
                                <FooterTemplate>
                                    <asp:Label ID="lblTotAmount" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL PENGAJUAN" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="10%" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                <ItemStyle Width="12%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"   Mode="NumericPages" />
                    </asp:DataGrid>
                     <uc2:ucGridNav id="GridNavigator" runat="server"/>
                     
                </div>
            </div>
        </div> 

         <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Visible="True" Text="Next" CssClass="small button green"></asp:Button>
        </div>

    </asp:Panel>




    <asp:panel id="pnlVerify" runat="server"  Visible="false">
     
    <div class="form_box">
        <div class="form_left">
            <label> Cabang Request </label>
            <asp:Label ID="lblBranchRequest" runat="server" />
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label> No Request </label>
            <asp:Label ID="LblRequestNo" runat="server" />
        </div>
        <div class="form_right">
            <label> Tanggal Request </label>
            <asp:Label ID="lblRequestDate" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label> Rekening Bank Cabang </label>
            <asp:Label ID="LblBranchBankAccount" runat="server" />
        </div>
        <div class="form_right">
            <label> Total Request </label>
            <asp:Label ID="LblRequestAmount" runat="server" align="right" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>Keterangan</label>
            <asp:Label ID="lblDescription" runat="server" />
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4> DAFTAR TRANSAKSI PETTY CASH </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtdTransactionList" runat="server" AutoGenerateColumns="False" CssClass="grid_general" ShowFooter="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn SortExpression="PETTYCASHNO" HeaderText="NO PETTY CASH">
                            <ItemStyle Width="15%" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lblPettyCashNo" runat="server" Text='<%#Container.DataItem("PETTYCASHNO")%>' />
                            </ItemTemplate>
                            <FooterTemplate> Total </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                            <ItemStyle Width="30%" />
                            <ItemTemplate>
                                <asp:Label ID="lblGridDescription" runat="server" Text='<%#Container.DataItem("Description")%>' />
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" Width="20%" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PCAMOUNT" HeaderText="JUMLAH">
                            <ItemStyle HorizontalAlign="right" Width="20%" />
                            <ItemTemplate>
                                <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNT"),2)%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotPCAmount" runat="server" Text="TOTAL" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PCDATE" HeaderText="TGL PC">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPCDate" runat="server" Text='<%#Container.DataItem("PETTYCASHDATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AMOUNTTRANSFER" HeaderText="JUMLAH TRANSFER">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" />
                            <ItemStyle HorizontalAlign="right" Width="12%" />
                            <ItemTemplate>
                                <asp:Label ID="lblGridAmountTransfer" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNTTRANSFER"),2)%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotAmountTransfer" runat="server" Text="TOTAL" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="NOTES" HeaderText="CATATAN">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" />
                            <ItemStyle HorizontalAlign="Center" Width="12%" />
                            <ItemTemplate>
                                <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("FINANCERNOTE")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
   

      <div class="form_button">
        <asp:Button ID="btnVerifikasi" runat="server" CausesValidation="False" Text="Verifikasi" CssClass="small button blue" />
        <asp:Button ID="btnClose" runat="server"  Text="Close" CssClass="small button gray" />
    </div>
    </asp:panel>
    
    </form>

</body>
</html>
