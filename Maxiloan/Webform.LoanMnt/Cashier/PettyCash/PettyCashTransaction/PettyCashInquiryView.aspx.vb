﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PettyCashInquiryView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property PettyCashNo() As String
        Get
            Return (CType(Viewstate("PettyCashNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PettyCashNo") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As Parameter.PettyCash
    Private oController As PettyCashController
    Private m_TotalPCAmount As Double
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PCNOVIEW"

            'If CheckFeature(Me.Loginid, Request.QueryString("ParentFormId"), "PCNO", Me.AppId) Then
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                m_TotalPCAmount = 0
                lblMessage.Visible = False

                Me.BranchID = Request.QueryString("BranchId")
                Me.PettyCashNo = Request.QueryString("PettyCashNo")

                DoBind()
            End If
            'End If
        End If
    End Sub

#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub
    Private Sub DoBind()
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            With oCustomClass
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .PettyCashNo = Me.PettyCashNo.Trim
                .strConnection = GetConnectionString
            End With
            oCustomClass = CType(oController.GetARecordAndDetailTable(oCustomClass), Parameter.PettyCash)
            With oCustomClass
                '*** HEADER
                lblPettyCashNo.Text = .PettyCashNo
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDepartement.Text = .DepartementName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .PCVoucherNo
                lblDate.Text = .PCDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.PCAmount, 2)

                lblCashierReversal.Text = .CashierNameReversal

                lblRefVoucherNoReversal.Text = .PCReversalVoucherNo

                '*** DETAIL
                FillGrid(oCustomClass.PagingTable, DtgAgree)

                '*** FOOTER
                lblStatus.Text = .PCStatus
                lblCashierStatus.Text = .CashierNameTransaction
                lblStatusDate.Text = .PCStatusDate.ToString("dd/MM/yyyy")

                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Public Sub New()
        oCustomClass = New Parameter.PettyCash
        oController = New Controller.PettyCashController
    End Sub

    Private Sub DtgAgree_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DtgAgree.SelectedIndexChanged

    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lbltemp As Label
        Dim lblTotal As Label

        If e.Item.ItemIndex >= 0 Then
            lbltemp = CType(e.Item.FindControl("lblPCDAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalPCAmount = m_TotalPCAmount + CType(lbltemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalPCAmount.ToString, 2)
            End If
        End If
    End Sub


End Class