﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Web.Services
Imports System.Data.SqlClient

#End Region

Public Class PettyCashTransaction
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oTrans As ucLookUpTransaction
    Protected WithEvents txtAmount As ucNumberFormat

    Dim intLoopGrid As Integer
    Dim temptotalPC As Double
    Private oController As New DataUserControlController
    Private oCustomClass As New PettyCash
    Private oPCController As New Controller.PettyCashController
    Dim LimitPettyCashTrans As Decimal = 0

#Region "Properties"
    

    Public Property EmployeeName() As String
        Get
            Return CStr(ViewState("EmployeeName"))
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeName") = Value
        End Set
    End Property


    Public Property Departement() As String
        Get
            Return CStr(ViewState("Departement"))
        End Get
        Set(ByVal Value As String)
            ViewState("Departement") = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return CStr(ViewState("Description"))
        End Get
        Set(ByVal Value As String)
            ViewState("Description") = Value
        End Set
    End Property

    Public Property Amount() As String
        Get
            Return CStr(ViewState("Amount"))
        End Get
        Set(ByVal Value As String)
            ViewState("Amount") = Value
        End Set
    End Property

    Public Property PCAmount() As Double
        Get
            Return CDbl(ViewState("PCAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PCAmount") = Value
        End Set
    End Property

    Public Property TotPCAmount() As Double
        Get
            Return CDbl(ViewState("TotPCAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotPCAmount") = Value
        End Set
    End Property

    Public Property BankType() As String
        Get
            Return CStr(ViewState("BankType"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property

    Public Property PettyCashNo() As String
        Get
            Return CStr(ViewState("PettyCashNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("PettyCashNo") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return txtEmployeeID.Text
        End Get
        Set(value As String)
            txtEmployeeID.Text = value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "PCTRS"

        Session.Add("ConString", GetConnectionString())
        Session.Add("BranchID", Me.sesBranchId.Replace("'", ""))

        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                With txtAmount
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With

                If Request.QueryString("strFileLocation") <> "" Then
                    'Dim strFileLocation As String

                    'strFileLocation = "../../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                    'Response.Write("<script language = javascript>" & vbCrLf _
                    '& "window.open('" & strFileLocation & "','accacq', 'menubar=0, scrollbars=yes, resizable=1,left=0, top=0, width='+ x + ', height = ' + y +') " & vbCrLf _
                    '& "</script>")


                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.heigth; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                    & "</script>")

                End If

                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If
                InitialDefaultPanel()

               

                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.HpsXml = "1"
                oCustomClass = oPCController.GetTablePC(oCustomClass, pstrFile)

                lblBussinessDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                GetCombo()
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        PnlHeader.Visible = True
        pnlList.Visible = False
        pnlGrid.Visible = False

    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'cboBank.SelectedIndex = 0
        'cboDepartement.SelectedIndex = 0
        'txtDescription.Text = ""
        'txtAmount.Text = "0"
        Server.Transfer("PettyCashTransaction.aspx")
    End Sub

    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then

            txtEmployeeID.Text = EmployeeID.ToString

            If Not CheckCashier(Me.Loginid) Then
                ShowMessage(lblMessage, "Kasir Belum buka", True)
                btnAddNew.Visible = False
            Else
                lblMessage.Text = ""
                If CDbl(txtAmount.Text) <= 0 Then

                    ShowMessage(lblMessage, "Jumlah Harus  > 0", True)
                    Exit Sub
                End If
                If CDbl(txtAmount.Text) > getLimitPettycashTrans() Then
                    ShowMessage(lblMessage, "Jumlah Harus  <= " & getLimitPettycashTrans(), True)
                    Exit Sub
                End If

                lblEmployeeName.Text = txtEmployeeName.Text
                lblDate.Text = lblBussinessDate.Text.Trim
                lblbankAccount.Text = cboBank.SelectedItem.Text.Trim
                lblDepartement.Text = cboDepartement.SelectedItem.Text.Trim
                lblDescription.Text = txtDescription.Text
                lblAmount.Text = FormatNumber(txtAmount.Text, 2)


                Dim BController As New TransferAccountController
                Dim BCustomClass As New TransferAccount

                BCustomClass.strConnection = GetConnectionString()
                BCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                BCustomClass.bankaccountid = cboBank.SelectedItem.Value
                BCustomClass = BController.GetBankType(BCustomClass)

                With BCustomClass
                    Me.BankType = .bankAccountType
                    If .bankAccountType = "B" Then
                        GetComboBilyet()
                        pnlBG.Visible = True
                    Else
                        pnlBG.Visible = False
                    End If
                End With

                Me.PCAmount = CDbl(txtAmount.Text)

                GetTransactionData()

                With oTrans
                    .TransactionID = ""
                    .Description = ""
                    .Amount = 0
                    .BindData()
                End With

                With oTrans
                    '.EnableAmount = False
                    '.EnableDescription = False
                    .TransactionID = ""
                    .Description = lblDescription.Text
                    .Amount = CDbl(lblAmount.Text)
                    .BindData()
                End With

                pnlList.Visible = True
                pnlGrid.Visible = False
                PnlHeader.Visible = False
            End If
        End If
    End Sub

    Private Sub GetCombo()


        '------------------
        'CBO DEPARTEMEN
        '------------------
        Dim dtDepartement As New DataTable

        dtDepartement = oController.GetDepartement(GetConnectionString)

        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"


        '------------------
        'CBO BANK ACCOUNT
        '------------------
        Dim dtBankAccount As New DataTable

        dtBankAccount = oController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), _
                      "", "PC")

        cboBank.DataTextField = "Name"
        cboBank.DataValueField = "ID"
        cboBank.DataSource = dtBankAccount
        cboBank.DataBind()

        cboBank.Items.Insert(0, "Select One")
        cboBank.Items(0).Value = "0"

        cboBank.SelectedIndex = "1"

    End Sub

    Private Sub GetComboBilyet()
        '------------------
        'CBO BILYET GIRO
        '------------------
        Dim dtBilyet As New DataTable
        Dim strWhere As String
        strWhere = "BranchID='" & Me.sesBranchId.Replace("'", "") & "' and BankAccountID='" & cboBank.SelectedItem.Value & "' and Status='N'"

        dtBilyet = oController.GetBGNumber(GetConnectionString, strWhere)

        cboBilyet.DataTextField = "Name"
        cboBilyet.DataValueField = "ID"
        cboBilyet.DataSource = dtBilyet
        cboBilyet.DataBind()

        cboBilyet.Items.Insert(0, "Select One")
        cboBilyet.Items(0).Value = "0"
    End Sub

    Private Sub GetTransactionData()
        If Me.IsHoBranch Then
            With oTrans
                .ProcessID = "PCTRS"
                .IsAgreement = "0"
                .IsPettyCash = "1"
                .IsHOTransaction = "2"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        Else
            With oTrans
                .ProcessID = "PCTRS"
                .IsAgreement = "0"
                .IsPettyCash = "1"
                .IsHOTransaction = "0"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        End If
    End Sub

    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then

            Dim lBytEnd As Integer
            Dim lBytCounter As Integer
            Dim lblListPDCNo As Label
            Dim lstrPDC As String
            Dim pStrFile As String
            Me.FormID = "PCTRANS"
            Dim oCustomClass As New PettyCash
            Dim dt As New DataTable
            lblMessage.Text = ""
            If oTrans.TransactionID = "" Then

                ShowMessage(lblMessage, "Harap pilih Transaksi", True)
                Exit Sub
            ElseIf oTrans.Amount <= 0 Then

                ShowMessage(lblMessage, "Nilai Transaksi harus > 0 ", True)
                Exit Sub
            ElseIf oTrans.Description = "" Then

                ShowMessage(lblMessage, "Harap isi Keterangan", True)
                Exit Sub

            End If
            With oCustomClass
                pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = .LoginId
                .strConnection = GetConnectionString()
                .TransactionID = oTrans.TransactionID
                .TransactionName = oTrans.Transaction
                .Amount = oTrans.Amount
                .ValueDate = ConvertDate2(oTrans.ValutaDate)
                .Description = oTrans.Description
                .FlagDelete = "0"
                .NumPC = DtgPCList.Items.Count
            End With
            oCustomClass = oPCController.GetTablePC(oCustomClass, pStrFile)
            If Not oCustomClass.isValidPC Then

                ShowMessage(lblMessage, "Transaksi sudah ada", True)
                Exit Sub
            Else
                DtgPCList.DataSource = oCustomClass.ListPC
                DtgPCList.DataBind()
                DtgPCList.Visible = True
                pnlList.Visible = False
                pnlList.Visible = True
                pnlGrid.Visible = True
            End If
            BtnCancel.Visible = False
            oTrans.TransactionID = ""
            oTrans.Transaction = ""
            oTrans.Amount = 0
            oTrans.Description = ""
            oTrans.BindData()
        End If
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        pnlGrid.Visible = False
        pnlList.Visible = True
        PnlHeader.Visible = False
        txtDescription.Text = ""
        txtAmount.Text = "0"
        BtnCancel.Visible = True
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oPCController.GetTablePC(oCustomClass, pstrFile)
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If DtgPCList.Items.Count > 0 Then
                Dim Ndttable As DataTable
                Dim pStrFile As String
                Dim strIDTrans As String
                Dim strDescTrans As String
                Dim strAmountTrans As String
                Dim strValueDateTrans As String
                Dim lblID As Label
                Dim lblDesc As Label
                Dim lblAmountG As Label
                Dim lblValueDate As Label

                'Dim AmountTotal As Integer = 0
                Dim AmountTotal As Double

                strIDTrans = ""
                strDescTrans = ""
                strAmountTrans = ""
                strValueDateTrans = ""
                'If Me.TotPCAmount = Me.PCAmount Then
                For intLoopGrid = 0 To DtgPCList.Items.Count - 1

                    lblID = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblTransactionID2"), Label)
                    lblDesc = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblDESCRIPTION"), Label)
                    lblAmountG = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblPCAmount"), Label)
                    lblValueDate = CType(DtgPCList.Items(intLoopGrid).Cells(0).FindControl("lblValueDate"), Label)

                    strIDTrans &= CStr(IIf(strIDTrans = "", "", ",")) & lblID.Text.Replace("'", "")
                    strDescTrans &= CStr(IIf(strDescTrans = "", "", ",")) & lblDesc.Text.Replace("'", "")
                    strAmountTrans &= CStr(IIf(strAmountTrans = "", "", ",")) & lblAmountG.Text.Replace("'", "").Replace(",", "") 

                    'AmountTotal += Int(lblAmountG.Text)
                    'Modify by Wira 20161129, diganti karena jika ada nilai pecahan, tidak bisa dieksekusi
                    AmountTotal += FormatNumber(CDbl(lblAmountG.Text), 2)
                Next

                If lblAmount.Text <> FormatNumber(AmountTotal, 2) Then
                    ShowMessage(lblMessage, "Total PettyCash Tidak sesuai", True)
                    Exit Sub
                End If

                Ndttable = GetStructPC()
                With oCustomClass

                    '       @BranchID char(3),
                    '@BusinessDate datetime,
                    '@LoginID varchar(20),
                    '@BankAccountID varchar(10),
                    '@DepartementID varchar(10),
                    '@EmployeeID char(10),
                    '@TotalAmount numeric(17,2),
                    '@BGNo char(20),
                    '@BGDueDate datetime,
                    '@desc varchar(500),
                    '@amount numeric(17,2),
                    '@NumOfDetail int, 
                    '@strID as varchar(8000),
                    '@strDesc as varchar(8000),
                    '@strAmount as varchar(7000),

                    pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                    .LoginId = Me.Loginid
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .strConnection = GetConnectionString()
                    .BusinessDate = Me.BusinessDate
                    .DepartementId = cboDepartement.SelectedItem.Value
                    .EmployeeId = txtEmployeeID.Text
                    .BankAccountID = cboBank.SelectedItem.Value
                    .Description = txtDescription.Text
                    .TotalAmount = Me.TotPCAmount
                    .strID = strIDTrans.Trim
                    .strDesc = strDescTrans.Trim
                    .strAmount = strAmountTrans.Trim
                    If .BankAccountType = "B" Then
                        .BGNo = cboBilyet.SelectedItem.Value
                        .BGDueDate = ConvertDate2(txtDueDate.Text)
                    Else
                        .BGNo = ""
                        .BGDueDate = Me.BusinessDate
                    End If

                    .NumPC = DtgPCList.Items.Count
                    .BankAccountType = Me.BankType
                End With


                Try


                    oPCController.PCTransSave(oCustomClass, pStrFile)

                    Me.PettyCashNo = oCustomClass.PettyCashNo
                oCustomClass.HpsXml = "1"
                pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass = oPCController.GetTablePC(oCustomClass, pStrFile)


                Dim cookie As HttpCookie = Request.Cookies(COOKIES_PETTY_CASH_VOUCHER)
                If Not cookie Is Nothing Then
                    cookie.Values("PettyCashNo") = Me.PettyCashNo.Trim
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie(COOKIES_PETTY_CASH_VOUCHER)
                    cookieNew.Values.Add("PettyCashNo", Me.PettyCashNo.Trim)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("PettyCashVoucherViewer.aspx") 

                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            Else

                ShowMessage(lblMessage, "Tidak ada data", True)
                Exit Sub

            End If
        End If
    End Sub


    Public Function GetStructPC() As DataTable
        Dim lObjDataTable As New DataTable("PC")

        With lObjDataTable
            .Columns.Add("TransactionID", System.Type.GetType("System.String"))
            .Columns.Add("Transaction", System.Type.GetType("System.String"))
            .Columns.Add("Description", System.Type.GetType("System.String"))
            .Columns.Add("Amount", System.Type.GetType("System.String"))
            .Columns.Add("ValueDate", System.Type.GetType("System.String"))
        End With

        Return lObjDataTable
    End Function


    Private Sub DtgPCList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPCList.ItemCommand
        Dim pstrFile As String
        If e.CommandName = "DELETE" Then
            Dim oCustomClass As New PettyCash
            With oCustomClass
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = Me.Session.SessionID + Me.Loginid
                .BranchId = Me.BranchID
                .FlagDelete = "1"
                '.CDtGrid = DtgPCList.Items.Count
                .IndexDelete = CStr(e.Item.ItemIndex())
                .HpsXml = "2"
            End With
            oCustomClass = oPCController.GetTablePC(oCustomClass, pstrFile)
            DtgPCList.DataSource = oCustomClass.ListPC
            DtgPCList.DataBind()
            DtgPCList.Visible = True
            pnlGrid.Visible = True
        End If
    End Sub


    Private Sub DtgPCList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPCList.ItemDataBound
        Dim totalPCAmount As New Label
        Dim lPCAmount As New Label
        Dim NPettyCashNo2 As New HyperLink

        With oCustomClass
            If e.Item.ItemIndex >= 0 Then
                lPCAmount = CType(e.Item.FindControl("lblPCAmount"), Label)
                temptotalPC += CDbl(lPCAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then

                totalPCAmount = CType(e.Item.FindControl("lblTotPCAmount"), Label)
                Me.TotPCAmount = temptotalPC
                totalPCAmount.Text = FormatNumber(temptotalPC.ToString, 2)
            End If
        End With
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If



    End Sub

    Private Sub btnCancelNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        'pnlGrid.Visible = False
        'pnlList.Visible = False
        'PnlHeader.Visible = True
        oCustomClass.HpsXml = "1"
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass = oPCController.GetTablePC(oCustomClass, pstrFile)
        Server.Transfer("PettyCashTransaction.aspx")
    End Sub

    'Wira 20161012
    Private Function getLimitPettycashTrans() As Decimal
        If LimitPettyCashTrans = 0 Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "LimitPettycash"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return LimitPettyCashTrans
        End If
    End Function
End Class