﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PettyCashEdit.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PettyCashEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../../../Webform.UserController/ucBranchHO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Petty Cash Edit</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />

     <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OpenWinMain(BranchId, PettyCashNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?BranchId=' + BranchId + '&PettyCashNo=' + PettyCashNo + '&style=AccMnt', null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function OpenActionWindow(BranchId, PettyCashNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashReversalView.aspx?BranchId=' + BranchId + '&PettyCashNo=' + PettyCashNo, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }					
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PETTY CASH EDIT
                </h3>
            </div>
        </div>

         <div class="form_box">
             <div class="form_single">
               <label class="label_req"> Cabang </label>
                <asp:DropDownList ID="cboParent" runat="server" />
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" />
              </div>
         </div>

        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="PettyCashNo">No Petty Cash</asp:ListItem>
                    <asp:ListItem Value="DepartementName">Departmen</asp:ListItem>
                    <asp:ListItem Value="EmployeeName">Naka Karyawan</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"  Width="88px"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Tanggal
                </label>
                <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PETTY CASH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                        AllowSorting="True" AllowPaging="false" Visible="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                      
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAction" runat="server" Text='EDIT'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PettyCashNo" HeaderText="NO PETTY CASH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("PettyCashNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DepartementName" HeaderText="DEPARTEMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartementName" runat="server" Text='<%#Container.DataItem("DepartementName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="NAMA KARYAWAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%# Container.DataItem("EmployeeName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBranchid" runat="server" Visible="False" Text='<%#DataBinder.Eval(Container, "DataItem.BranchId") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PettyCashDate" SortExpression="PettyCashDate" HeaderText="TGL PC"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns> 
                    </asp:DataGrid>
                    <uc2:ucGridNav id="GridNavigator" runat="server"/>
                </div>
            </div>
        </div>
        <%--<div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>--%>
    </asp:Panel>
    </form>
</body>
</html>
