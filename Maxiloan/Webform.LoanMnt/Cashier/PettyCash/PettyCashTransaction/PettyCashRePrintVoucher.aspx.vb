﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PettyCashRePrintVoucher
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents hyPettyCashNo As System.Web.UI.WebControls.HyperLink
#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property
    Public ReadOnly Property CustomClassWhereCond() As String
        Get
            Return Me.SearchBy
        End Get
    End Property
    Public ReadOnly Property CustomClassSortBy() As String
        Get
            Return Me.SortBy
        End Get
    End Property
    Public ReadOnly Property CustomClassBusinessDate() As Date
        Get
            Return Me.BusinessDate
        End Get
    End Property

    Public Property PettyCashNo() As String
        Get
            Return CStr(ViewState("PettyCashNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("PettyCashNo") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private m_BranchController As New DataUserControlController
#End Region

#Region "MemberVars"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
#End Region
    Private Sub FillComboWithBranches(ByVal cbo As WebControls.DropDownList, ByVal duccController As DataUserControlController)
        Try
            Dim dtbranch As New DataTable
            dtbranch = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cbo
                If Me.IsHoBranch Then
                    .DataSource = duccController.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                Else
                    .DataSource = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "PCTRS"
            If Request.QueryString("strFileLocation") <> "" Then
                'Dim strFileLocation As String

                'strFileLocation = "../../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                'Response.Write("<script language = javascript>" & vbCrLf _
                '& "window.open('" & strFileLocation & "','accacq', 'menubar=0, scrollbars=yes, resizable=1,left=0, top=0, width='+ x + ', height = ' + y +') " & vbCrLf _
                '& "</script>")


                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.heigth; " & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            With txtDate
                .Enabled = True

            End With

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                FillComboWithBranches(cboParent, m_BranchController)
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .BusinessDate = Me.BusinessDate
            End With


            oCustomClass = CType(oController.GetPagingTable(oCustomClass), PettyCash)

            DtUserList = oCustomClass.PagingTable
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True

            lblMessage.Text = ""

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = ""
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        cboStatus.SelectedIndex = 0
        txtDate.Text = ""
        FillComboWithBranches(cboParent, m_BranchController)
        pnlDatagrid.Visible = False
        'DoBind(Me.SearchBy, Me.SortBy)        
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        'If InStr(Me.SortBy, "DESC") > 0 Then
        '    Me.SortBy = e.SortExpression + " ASC"
        'Else
        '    Me.SortBy = e.SortExpression + " DESC"
        'End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub



    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String = ""
        txtPage.Text = "1"

        Me.SearchBy = ""
        If (cboParent.SelectedItem.Value <> "0" And cboParent.SelectedItem.Value <> "ALL") Then
            Me.SearchBy = "  PCT.branchid = " & "'" & cboParent.SelectedValue.Trim & "'"
        End If

        If cboStatus.SelectedIndex > 0 Then
            Me.SearchBy = Me.SearchBy & " and PettyCashStatus = '" & cboStatus.SelectedItem.Value & "' "

            If strFilterBy.Trim.Length > 0 Then
                strFilterBy = strFilterBy & ", "
            End If
            strFilterBy = strFilterBy & "Status = " & cboStatus.SelectedItem.Value
        End If

        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '%" & txtSearchBy.Text.Trim & "%')"

                If strFilterBy.Trim.Length > 0 Then
                    strFilterBy = strFilterBy & ", "
                End If
                strFilterBy = strFilterBy & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
            End If
        End If

        If txtDate.Text.Trim.Length > 0 Then
            Me.SearchBy = Me.SearchBy & " AND PettyCashDate = '" & ConvertDate2(txtDate.Text) & "'"

            If strFilterBy.Trim.Length > 0 Then
                strFilterBy = strFilterBy & ", "
            End If
            strFilterBy = strFilterBy & " PC Date = " & ConvertDate2(txtDate.Text)
        End If

        With oCustomClass
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            Me.ParamReport = strFilterBy
        End With

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label

        If e.Item.ItemIndex >= 0 Then
            'If CheckFeature(Me.Loginid, Me.FormID, "PCNO", Me.AppId) Then
            lblTemp = CType(e.Item.FindControl("lblBranchId"), Label)
            If Not lblTemp Is Nothing Then
                hyPettyCashNo = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink)
                hyPettyCashNo.NavigateUrl = "javascript:OpenWinMain('" & lblTemp.Text.Trim & "','" & hyPettyCashNo.Text.Trim & "')"
            End If
            'End If
        End If
    End Sub

#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chkPrint As CheckBox
        Dim lblPettyCashNo As HyperLink
        Dim hasil As Integer
        Dim cmdwhere As String

        With oDataTable
            .Columns.Add(New DataColumn("PettyCashNo", GetType(String)))
        End With
        For intloop = 0 To DtgAgree.Items.Count - 1
            chkPrint = CType(DtgAgree.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
            lblPettyCashNo = CType(DtgAgree.Items(intloop).Cells(1).FindControl("hyPettyCashNo"), HyperLink)            
            If chkPrint.Checked Then
                Me.PettyCashNo = CType(lblPettyCashNo.Text.Trim, String)

                Try

                    Dim cookie As HttpCookie = Request.Cookies(COOKIES_PETTY_CASH_VOUCHER)
                    If Not cookie Is Nothing Then
                        cookie.Values("PettyCashNo") = Me.PettyCashNo.Trim
                        Response.AppendCookie(cookie)
                    Else
                        Dim cookieNew As New HttpCookie(COOKIES_PETTY_CASH_VOUCHER)
                        cookieNew.Values.Add("PettyCashNo", Me.PettyCashNo.Trim)
                        Response.AppendCookie(cookieNew)
                    End If
                    Response.Redirect("PettyCashRePrintVoucherViewer.aspx")

                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try

            End If
        Next
    End Sub
#End Region

End Class