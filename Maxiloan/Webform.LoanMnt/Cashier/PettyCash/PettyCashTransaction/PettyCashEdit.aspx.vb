﻿Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Globalization

Public Class PettyCashEdit
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents GridNavigator As ucGridNav
    Private currentPage As Integer = 1
    Private pageSize As Integer = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Integer = 1


    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        Try
            If Not IsPostBack Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                Me.FormID = "PCREDT"
                 
                If Me.IsHoBranch = False Then
                    NotAuthorized()
                    Exit Sub
                End If

                If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Exit Sub
                End If

                With txtDate
                    .Enabled = True
                End With
                Me.SearchBy = ""
                Me.SortBy = ""
 

                With cboParent
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
 
                    .Items.Insert(0, "ALL")
                    .Items(0).Value = "ALL"
                End With


            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub

    Public Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Sub DoBind(Optional isFrNav As Boolean = False) 'ByVal cmdWhere As String, ByVal SortBy As String)
        'Dim DtUserList As DataTable
        'Dim DvUserList As DataView

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .BusinessDate = Me.BusinessDate
            End With


            oCustomClass = oController.GetPagingTable(oCustomClass)

            'DtUserList = oCustomClass.PagingTable
            'DvUserList = oCustomClass.PagingTable.DefaultView
            recordCount = oCustomClass.TotalRecord
            oCustomClass.PagingTable.DefaultView.Sort = Me.SortBy
            DtgAgree.DataSource = oCustomClass.PagingTable.DefaultView

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try

            pnlList.Visible = True
            pnlDatagrid.Visible = True

            lblMessage.Text = ""
            If (isFrNav = False) Then
                GridNavigator.Initialize(recordCount, pageSize)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Me.SearchBy = ""
            cboSearchBy.SelectedIndex = 0
            txtSearchBy.Text = ""
            txtDate.Text = ""
            pnlDatagrid.Visible = False

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""

        Try
            Dim strFilterBy As String = ""

            Me.SearchBy = String.Format("  PCT.PettyCashStatus = 'PC' AND PCT.IsReimburse = 0 {0} ", IIf(cboSearchBy.SelectedItem.Value.Trim <> "0", " AND PCT.branchid = '" & cboParent.SelectedItem.Value.Trim & "'", ""))

            If cboSearchBy.SelectedItem.Value <> "0" Then
                If txtSearchBy.Text.Trim.Length > 0 Then
                    Dim strOperator As String

                    If Me.SearchBy.IndexOf("%") <> -1 Then
                        strOperator = " = "
                    Else
                        strOperator = " LIKE "
                    End If
                    Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"

                    If strFilterBy.Trim.Length > 0 Then
                        strFilterBy = strFilterBy & ", "
                    End If
                    strFilterBy = strFilterBy & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
                End If
            End If

            If txtDate.Text.Trim.Length > 0 Then
                Me.SearchBy = Me.SearchBy & " AND PettyCashDate = '" & ConvertDate2(txtDate.Text) & "'"

                If strFilterBy.Trim.Length > 0 Then
                    strFilterBy = strFilterBy & ", "
                End If
                strFilterBy = strFilterBy & " PC Date = " & ConvertDate2(txtDate.Text)
            End If

            With oCustomClass
                .WhereCond = Me.SearchBy
                .SortBy = Me.SortBy
                Me.ParamReport = strFilterBy
            End With

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Try
            If InStr(Me.SortBy, "DESC") > 0 Then
                Me.SortBy = e.SortExpression + " ASC"
            Else
                Me.SortBy = e.SortExpression + " DESC"
            End If
            DoBind()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Try 
            If e.Item.ItemIndex >= 0 Then 
                Dim hyPettyCashNo = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink)
                Dim branchId = CType(e.Item.FindControl("lblBranchid"), Label).Text.Trim
                hyPettyCashNo.NavigateUrl = "javascript:OpenWinMain('" & branchId & "','" & Server.UrlEncode(hyPettyCashNo.Text.Trim) & "')" 
                Dim hyTemp = CType(e.Item.FindControl("hyAction"), HyperLink)
                hyTemp.NavigateUrl = "PettyCashReversalView.aspx?BranchId=" & Server.UrlEncode(branchId) & "&PettyCashNo=" & Server.UrlEncode(hyPettyCashNo.Text.Trim) & "&IsEditCOA=1&style=" & Server.UrlEncode("AccMnt")
            End If 
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

End Class