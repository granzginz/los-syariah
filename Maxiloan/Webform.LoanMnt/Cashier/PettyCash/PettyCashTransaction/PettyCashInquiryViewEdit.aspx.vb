﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PettyCashInquiryViewEdit
    Inherits Maxiloan.Webform.AccMntWebBased
    Private oController As New PettyCashController
    Protected WithEvents oTrans As ucLookupCOATransaction

#Region "Property"
    Private Property PettyCashNo() As String
        Get
            Return (CType(ViewState("PettyCashNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PettyCashNo") = Value
        End Set
    End Property
    Private Property PaymentAllocationID() As String
        Get
            Return (CType(ViewState("PaymentAllocationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentAllocationID") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property NoIndex() As Integer
        Get
            Return CType(ViewState("NoIndex"), Integer)
        End Get
        Set(value As Integer)
            ViewState("NoIndex") = value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 100
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PettyCash
    Private m_TotalPCAmount As Double
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PCTNOVIEW"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                m_TotalPCAmount = 0
                lblMessage.Visible = False

                Me.PettyCashNo = Request.QueryString("PettyCashNo")

                DoBind()
                InitialDefaultPanel()

                oTrans.IsPettyCash = "1"
                oTrans.BindData()
            End If
            'End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        Panel1.Visible = False
    End Sub
#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub
    Private Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        Try
            With oCustomClass
                .PettyCashNo = Me.PettyCashNo.Trim
                .strConnection = GetConnectionString()
            End With
            oCustomClass = CType(oController.GetARecordAndDetailTable(oCustomClass), Parameter.PettyCash)
            With oCustomClass
                '*** HEADER
                lblPettyCashNo.Text = .PettyCashNo
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDepartement.Text = .DepartementName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .PCVoucherNo
                lblDate.Text = .PCDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.PCAmount, 2)

                lblCashierReversal.Text = .CashierNameReversal

                lblRefVoucherNoReversal.Text = .PCReversalVoucherNo

                '*** DETAIL
                FillGrid(oCustomClass.PagingTable, DtgAgree)

                '*** FOOTER
                lblStatus.Text = .PCStatus
                lblCashierStatus.Text = .CashierNameTransaction
                lblStatusDate.Text = .PCStatusDate.ToString("dd/MM/yyyy")

            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region


    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Public Sub New()
        oCustomClass = New Parameter.PettyCash
        oController = New Controller.PettyCashController
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Dim err As String
        Dim lbljumlahgrid As Label
        Dim lblPaymentAllocId As Label
        Dim lblTransName As Label
        Dim lblKetNotes As Label

        If e.CommandName = "Edit" Then
            Me.AddEdit = "EDIT"
            lblPettyCash.Text = lblPettyCashNo.Text
            lblKasir.Text = lblEmployeeName.Text
            lblDep.Text = lblDepartement.Text
            lblTanggal.Text = lblDate.Text
            lblKet.Text = lblDescription.Text
            lbljumlahgrid = CType(e.Item.FindControl("lblPCDAmount"), Label)
            lblPaymentAllocId = CType(e.Item.FindControl("lblPaymentAllocationID"), Label)
            lblTransName = CType(e.Item.FindControl("lblPettyCashNo"), Label)
            lblKetNotes = CType(e.Item.FindControl("lblNote"), Label)
            lblJum.Text = lbljumlahgrid.Text
            oTrans.Description = lblKetNotes.Text
            oTrans.Transaction = lblTransName.Text
            oTrans.TransactionID = lblPaymentAllocId.Text
            Me.NoIndex = e.Item.ItemIndex
        End If
        pnlList.Visible = False
        Panel1.Visible = True
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lbltemp As Label
        Dim lblTotal As Label

        If e.Item.ItemIndex >= 0 Then
            lbltemp = CType(e.Item.FindControl("lblPCDAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalPCAmount = m_TotalPCAmount + CType(lbltemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalPCAmount.ToString, 2)
            End If
        End If
    End Sub
    Private Sub BtnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnUpdate.Click

        CType(DtgAgree.Items(Me.NoIndex).FindControl("lblPettyCashNo"), Label).Text = oTrans.Transaction
        CType(DtgAgree.Items(Me.NoIndex).FindControl("lblPaymentAllocationID"), Label).Text = oTrans.TransactionID
        CType(DtgAgree.Items(Me.NoIndex).FindControl("lblNote"), Label).Text = oTrans.Description
        CType(DtgAgree.Items(Me.NoIndex).FindControl("lblCOA"), Label).Text = oTrans.COA
        pnlList.Visible = True
        Panel1.Visible = False

    End Sub
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim Ndttable As New DataTable

        Ndttable = GetStructPC()
        If DtgAgree.Items.Count > 0 Then

            Dim strIDTrans As String
            Dim strNote As String
            Dim strSeqNo As String



            For intLoopGrid = 0 To DtgAgree.Items.Count - 1
                strIDTrans = CType(DtgAgree.Items(intLoopGrid).FindControl("lblPaymentAllocationID"), Label).Text
                strNote = CType(DtgAgree.Items(intLoopGrid).FindControl("lblNote"), Label).Text
                strSeqNo = CType(DtgAgree.Items(intLoopGrid).FindControl("lblSequenceNo"), Label).Text

                Ndttable.Rows.Add(Me.PettyCashNo, strIDTrans, strNote, strSeqNo)
            Next


            With oCustomClass
                .PettyCashNo = Me.PettyCashNo
                .ListData = Ndttable
                .strConnection = GetConnectionString()
            End With
            Try
                Dim result = oController.PCreimburseCOAUpdate(oCustomClass)
                If result <> "OK" Then
                    ShowMessage(lblMessage, result, True)
                    Exit Sub
                Else
                    ShowMessage(lblMessage, "Proses Update Petty Cash Berhasil", False)
                End If
            Catch es As Exception
                ShowMessage(lblMessage, "Proses Update Petty Cash  Gagal", True)
                Exit Sub
            End Try

            Me.PettyCashNo = oCustomClass.PettyCashNo
        Else
            ShowMessage(lblMessage, "Tidak ada data", True)
            Exit Sub

        End If

    End Sub
    Private Function GetStructPC() As DataTable
        Dim lObjDataTable As New DataTable

        lObjDataTable.Columns.Add("PettyCashNo")
        lObjDataTable.Columns.Add("TransactionID")
        lObjDataTable.Columns.Add("Note")
        lObjDataTable.Columns.Add("SequenceNo")

        Return lObjDataTable
    End Function
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        InitialDefaultPanel()
        DoBind()

    End Sub
End Class