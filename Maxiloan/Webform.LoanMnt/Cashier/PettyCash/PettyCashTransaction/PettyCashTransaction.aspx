﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PettyCashTransaction.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PettyCashTransaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../../../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../../../Webform.UserController/UcBankName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcLookupEmployee" Src="../../../../Webform.UserController/UcLookupEmployee.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Petty Cash Transaction</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>

    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $("#txtEmployeeName").autocomplete({
                 source: function (request, response) {
                     $("#txtEmployeeID").val('');
                     $.ajax({
                         url: '../../../../Webform.Utility/autocomplate/GetEmployee.asmx/GetData',
                         data: "{ 'prefix': '" + request.term + "'}",
                         dataType: "json",
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         success: function (data) {
                             response($.map(data.d, function (item) {
                                 return {
                                     label: item.split('-')[0],
                                     val: item.split('-')[1]
                                 }
                             }))
                         },
                         error: function (response) {
                             alert(response.responseText);
                         },
                         failure: function (response) {
                             alert(response.responseText);
                         }
                     });
                 },
                 select: function (e, i) {
                     $("#txtEmployeeID").val(i.item.val);
                 },
                 minLength: 1
             });
         });
</script>
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>

       <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="PnlHeader" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TRANSAKSI PETTY CASH
                    </h3>
                </div>
            </div>
            <div class="form_box">
            

                <div class="form_single">
                <label class="label_req">Karyawan</label>

                <asp:TextBox id="txtEmployeeName" runat="server" style="width:250px" placeholder="Masukan nama karyawan"></asp:TextBox>
                <asp:TextBox ID="txtEmployeeID" runat="server"  placeholder="ID Karyawan" />

                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                        Display="Dynamic" ControlToValidate="txtEmployeeName" ErrorMessage="Harap pilih Karyawan"
                        Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Rekening Bank
                    </label>
                    <asp:DropDownList ID="cboBank" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="cbank" runat="server" InitialValue="0" Display="Dynamic"
                        ControlToValidate="cboBank" ErrorMessage="Harap pilih Rekening Bank" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal
                    </label>
                    <asp:Label ID="lblBussinessDate" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Departemen
                    </label>
                    <asp:DropDownList ID="cboDepartement" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                        Display="Dynamic" ControlToValidate="cboDepartement" ErrorMessage="Harap pilih Departemen"
                        Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label class="label_req">
                        Jumlah
                    </label>                
                    <uc1:ucNumberFormat ID="txtAmount" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Keterangan</label>
                    <asp:TextBox ID="txtDescription" runat="server" CssClass ="long_text" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                        ControlToValidate="txtDescription" ErrorMessage="Harap isi Keterangan" Visible="true"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            
            <div class="form_button">
                <asp:Button ID="BtnNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button green">
                </asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
            
        </asp:Panel>
     

    
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TRANSAKSI PETTY CASH
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Karyawan
                </label>
                <asp:Label ID="lblEmployeeName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal
                </label>
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Rekening
                </label>
                <asp:Label ID="lblbankAccount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Departemen</label>
                <asp:Label ID="lblDepartement" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                <asp:Label ID="lblDescription" runat="server" ></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlBG" Visible="False" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        <strong>BILYET GIRO</strong></label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        No Bilyet Giro
                    </label>
                    <asp:DropDownList ID="cboBilyet" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Jatuh Tempo
                    </label>
                    <asp:TextBox runat="server" ID="txtDueDate"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDueDate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
        </asp:Panel>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>DETAIL TRANSAKSI</strong></label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPCList" runat="server" Visible="False" ShowFooter="True" CssClass="grid_general"
                        AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSACTION" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionID2" runat="server" Text='<%#Container.DataItem("TransactionID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionID" runat="server" Text='<%#Container.DataItem("Transaction")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TANGGAL VALUTA">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblValueDate" runat="server" Text='<%#Container.DataItem("ValueDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCRIPTION" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="Amount"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="false" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
     
    </form>
</body>
</html>
