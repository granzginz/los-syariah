﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PettyCashReversalView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property PettyCashNo() As String
        Get
            Return (CType(Viewstate("PettyCashNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PettyCashNo") = Value
        End Set
    End Property
    Private Property ParentFormId() As String
        Get
            Return (CType(Viewstate("ParentFormId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParentFormId") = Value
        End Set
    End Property
    Private Property TotalAmount() As Double
        Get
            Return (CType(viewstate("TotalAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalAmount") = Value
        End Set
    End Property


    Private Property IsEditCOA() As Boolean
        Get
            Return (CType(ViewState("ISEDITCOA"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("ISEDITCOA") = Value
        End Set
    End Property

    Private Property MementoPettyCasts As IList(Of MementoPettyCast)
        Get
            Return (CType(ViewState("MEMENTOPETTYCASTS"), IList(Of MementoPettyCast)))
        End Get
        Set(ByVal Value As IList(Of MementoPettyCast))
            ViewState("MEMENTOPETTYCASTS") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private Const STR_PARENT_FORM_ID As String = "PCRVS"

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As Parameter.PettyCash
    Private oController As PettyCashController
    Private m_TotalPCAmount As Double
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PCRVIEW"
            txtTransaction.Attributes.Add("readonly", "true")
            'If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If CheckFeature(Me.Loginid, STR_PARENT_FORM_ID, "RVRS", Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If
                m_TotalPCAmount = 0
                lblMessage.Visible = False

                Me.BranchID = Request.QueryString("BranchId")
                Me.PettyCashNo = Request.QueryString("PettyCashNo")

                IsEditCOA = Request.QueryString("IsEditCOA") <> ""
                If (IsEditCOA) Then 
                    BtnSave.Visible = False
                    lblCaption.Text = "PETTY CASH EDIT"
                End If

                 DoBind()
            End If
            'End If
        End If
    End Sub

#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            oGrid.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try 
            oGrid.Columns(0).Visible = (IsEditCOA = True) 
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub

    Sub saveToTemporal(dt As DataTable)
        Dim val = dt.AsEnumerable().Select(Function(r) New MementoPettyCast(
         r.Field(Of String)("TransactionName"), r.Field(Of String)("PCDDescription"), r.Field(Of Decimal)("PCDAmount"), r.Field(Of Int16)("SequenceNo"), r.Field(Of String)("PaymentAllocationId"))).ToList()

        MementoPettyCasts = val
    End Sub

    Private Sub DoBind()
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            With oCustomClass
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .PettyCashNo = Me.PettyCashNo.Trim
                .strConnection = GetConnectionString
            End With
            oCustomClass = CType(oController.GetARecordAndDetailTable(oCustomClass), Parameter.PettyCash)
            With oCustomClass
                '*** HEADER
                lblPettyCashNo.Text = .PettyCashNo
                lblEmployeeName.Text = .EmployeeName
                lblBankAccount.Text = .BankAccountName
                lblDepartement.Text = .DepartementName
                lblDescription.Text = .Description
                lblRefVoucherNo.Text = .PCVoucherNo
                lblDate.Text = .PCDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.PCAmount, 2)
                Me.TotalAmount = .PCAmount
                '*** DETAIL
                FillGrid(oCustomClass.PagingTable, DtgAgree)
                If IsEditCOA Then
                    saveToTemporal(oCustomClass.PagingTable)
                End If
                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Public Sub New()
        oCustomClass = New Parameter.PettyCash
        oController = New Controller.PettyCashController
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound



        If e.Item.ItemIndex >= 0 Then
            Dim lbltemp = CType(e.Item.FindControl("lblPCDAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalPCAmount = m_TotalPCAmount + CType(lbltemp.Text, Double)
            End If

            'Dim seq = CType(e.Item.FindControl("lblSeq"), Label).Text.Trim
            'Dim trName = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink).Text.Trim
             
            'CType(e.Item.FindControl("hyAction"), HyperLink).Attributes.Add("onclick", String.Format("ShowEditor('{0}','{1}');", seq, trName))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalPCAmount.ToString, 2)
            End If
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If CheckFeature(Me.Loginid, STR_PARENT_FORM_ID, "SAVE", Me.AppId) Then
            Dim oET As Parameter.PettyCash

            Try 
                If Me.TotalAmount <= 0 Then
                    DisplayError("Tidak dapat proses petty cash reversal, Jumlah harus > 0")
                    Exit Sub
                End If

                oET = New Parameter.PettyCash


                If IsEditCOA Then

                    With oET
                        .strConnection = GetConnectionString()
                        .BranchId = Me.BranchID
                        .PettyCashNo = Me.PettyCashNo
                        .ListData = ToPettyTable()
                    End With

                    oController.EditCOAPettyCash(oET)
                    Server.Transfer("PettyCashEdit.aspx?Message=" & Server.UrlEncode("Record saved"))
                    Return
                End If



                With oET
                    .strConnection = GetConnectionString()
                    .Amount = Me.TotalAmount
                    .BranchId = Me.sesBranchId.Trim.Replace("'", "")
                    .PettyCashNo = Me.PettyCashNo
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .CoyID = Me.SesCompanyID
                    .PCDate = ConvertDate2(lblDate.Text)
                    .Description = lblDescription.Text
                    .PCVoucherNo = lblRefVoucherNo.Text
                End With
                oController.SavePettyCashReversal(oET)
                Server.Transfer("PettyCashReversal.aspx?Message=" & Server.UrlEncode("Record saved"))


            Catch ex As Exception
                DisplayError(ex.Message)
            End Try
        End If
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Server.Transfer(IIf(IsEditCOA, "PettyCashEdit.aspx", "PettyCashReversal.aspx"))
    End Sub


    Function ToPettyTable() As DataTable
        Dim dt = New DataTable()

        Dim strArray As String() = New String() {"id", "value1", "value2"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next

        For Each v In MementoPettyCasts
            Dim row = dt.NewRow()
            row("id") = v.SequenceNo
            row("value1") = v.PaymentAllocationId
            row("value2") = ""
            dt.Rows.Add(row)
        Next
        Return dt
    End Function


    Private Sub btnCancelUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUpdate.Click
        pnlbutton.Visible = True
        pnlEditPetty.Visible = False 
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select e.CommandName
            Case "Edit"
                hidSeqNo.Value = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblSeq"), Label).Text.Trim 
                lblTrName.Text = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("hyPettyCashNo"), HyperLink).Text.Trim

                hdnTransactionID.Value = ""
                txtTransaction.Text = ""

                pnlbutton.Visible = False
                pnlEditPetty.Visible = True

        End Select
    End Sub
    Private Sub btnUpdateCoa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCoa.Click
        pnlbutton.Visible = True
        pnlEditPetty.Visible = False

        
        Dim key = CType(hidSeqNo.Value, Short)
        Dim pettycash = MementoPettyCasts.Where(Function(x) x.SequenceNo = key).SingleOrDefault()
        If (Not IsNothing(pettycash)) Then

            Dim newCoa = hdnTransactionID.Value.Trim
            Dim newTrans = txtTransaction.Text.Trim

            pettycash.PaymentAllocationId = newCoa
            pettycash.TransactionName = newTrans

            DtgAgree.DataSource = MementoPettyCasts
            DtgAgree.DataBind()


            BtnSave.Text = "UPDATE"
            BtnSave.Visible = True
        End If
         
    End Sub
     
End Class


<Serializable()>
Public Class MementoPettyCast
    Public Sub New()
        mTransactionName = ""
        mPCDDescription = ""
        mPCDAmount = 0
        mSequenceNo = 0
        mPaymentAllocationId = ""
    End Sub

    Public Sub New(_TransactionName As String, _PCDDescription As String, _PCDAmount As Decimal, _SequenceNo As Int16, _PaymentAllocationId As String)

        mTransactionName = _TransactionName
        mPCDDescription = _PCDDescription
        mPCDAmount = _PCDAmount
        mSequenceNo = _SequenceNo
        mPaymentAllocationId = _PaymentAllocationId

    End Sub
    Private mTransactionName As String
    Private mPCDDescription As String
    Private mPCDAmount As Decimal
    Private mSequenceNo As Int16
    Private mPaymentAllocationId As String

    Public Property TransactionName As String
        Get
            Return mTransactionName
        End Get
        Set(value As String)
            mTransactionName = value
        End Set
    End Property
    Public Property PCDDescription As String
        Get
            Return mPCDDescription
        End Get
        Set(value As String)
            mPCDDescription = value
        End Set
    End Property
    Public Property PCDAmount As Decimal
        Get
            Return mPCDAmount
        End Get
        Set(value As Decimal)
            mPCDAmount = value
        End Set
    End Property
    Public Property SequenceNo As Int16
        Get
            Return mSequenceNo
        End Get
        Set(value As Int16)
            mSequenceNo = value
        End Set
    End Property
    Public Property PaymentAllocationId As String
        Get
            Return mPaymentAllocationId
        End Get
        Set(value As String)
            mPaymentAllocationId = value
        End Set
    End Property
End Class