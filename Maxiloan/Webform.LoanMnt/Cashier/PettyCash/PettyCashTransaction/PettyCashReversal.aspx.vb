﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PettyCashReversal
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents hyPettyCashNo As System.Web.UI.WebControls.HyperLink

#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property
    Public ReadOnly Property CustomClassWhereCond() As String
        Get
            Return Me.SearchBy
        End Get
    End Property
    Public ReadOnly Property CustomClassSortBy() As String
        Get
            Return Me.SortBy
        End Get
    End Property
    Public ReadOnly Property CustomClassBusinessDate() As Date
        Get
            Return Me.BusinessDate
        End Get
    End Property
#End Region

#Region "Constanta"

#End Region

#Region "MemberVars"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PettyCash
    Private oController As New PettyCashController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If sessioninvalid() Then
                Exit Sub
            End If

            If Not IsPostBack Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                Me.FormID = "PCRVS"

                With txtDate
                    .Enabled = True

                End With

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If
            End If

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView       

        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .BusinessDate = Me.BusinessDate
            End With


            oCustomClass = oController.GetPagingTable(oCustomClass)

            DtUserList = oCustomClass.PagingTable
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True

            lblMessage.Text = ""

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try

    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Me.SearchBy = ""
            cboSearchBy.SelectedIndex = 0
            txtSearchBy.Text = ""
            txtDate.Text = ""

            pnlDatagrid.Visible = False
            'DoBind(Me.SearchBy, Me.SortBy)        
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        'If InStr(Me.SortBy, "DESC") > 0 Then
        '    Me.SortBy = e.SortExpression + " ASC"
        'Else
        '    Me.SortBy = e.SortExpression + " DESC"
        'End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Try
            If SessionInvalid() Then
                Exit Sub
            End If
            Dim cookie As HttpCookie = Request.Cookies("PettyCashInquiry")
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                cookie.Values("FilterBy") = Me.ParamReport
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PettyCashInquiry")
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                cookieNew.Values.Add("FilterBy", Me.ParamReport)
                Response.AppendCookie(cookieNew)
            End If
            Server.Transfer("Report/ViewerRptPettyCashInquiry.aspx")

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""

        Try
            Dim strFilterBy As String = ""
            txtPage.Text = "1"
            'Me.SearchBy = "  PCT.PettyCashStatus = 'PC' AND PCT.IsReimburse = 0 AND PCT.branchid = " & "'" & Me.sesBranchId.Trim.Replace("'", "") & "'"
            Me.SearchBy = "  PCT.PettyCashStatus in ('PC', 'RV')   AND PCT.branchid = " & "'" & Me.sesBranchId.Trim.Replace("'", "") & " ' and PettyCashNo not in (select PettycashNo from PettyCashReimburseDetail pct
											inner join PettyCashReimburse d on d.RequestNo = pct.RequestNo where d.status in ('APR', 'OTO', 'RJC') ) "
            'strFilterBy = strFilterBy & " Branch = " & oBranch.BranchID.Trim


            If cboSearchBy.SelectedItem.Value <> "0" Then
                If txtSearchBy.Text.Trim.Length > 0 Then
                    Dim strOperator As String

                    If Me.SearchBy.IndexOf("%") <> -1 Then
                        strOperator = " = "
                    Else
                        strOperator = " LIKE "
                    End If
                    Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"

                    If strFilterBy.Trim.Length > 0 Then
                        strFilterBy = strFilterBy & ", "
                    End If
                    strFilterBy = strFilterBy & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
                End If
            End If

            If txtDate.Text.Trim.Length > 0 Then
                'Me.SearchBy = Me.SearchBy & " AND PettyCashDate = '" & ConvertDate2(txtDate.Text) & "'"
                Me.SearchBy = Me.SearchBy & " AND PettyCashDate <= '" & ConvertDate2(txtDate.Text) & "'"

                If strFilterBy.Trim.Length > 0 Then
                    strFilterBy = strFilterBy & ", "
                End If
                strFilterBy = strFilterBy & " PC Date = " & ConvertDate2(txtDate.Text)
            End If

            With oCustomClass
                .WhereCond = Me.SearchBy
                .SortBy = Me.SortBy
                Me.ParamReport = strFilterBy
            End With

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Try
            If InStr(Me.SortBy, "DESC") > 0 Then
                Me.SortBy = e.SortExpression + " ASC"
            Else
                Me.SortBy = e.SortExpression + " DESC"
            End If
            DoBind(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Try
            Dim hyTemp As HyperLink
            If e.Item.ItemIndex >= 0 Then
                'If CheckFeature(Me.Loginid, Me.FormID, "PCNO", Me.AppId) Then
                hyPettyCashNo = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink)
                hyPettyCashNo.NavigateUrl = "javascript:OpenWinMain('" & Me.sesBranchId.Trim.Replace("'", "") & "','" & Server.UrlEncode(hyPettyCashNo.Text.Trim) & "')"
                'End If
                hyTemp = CType(e.Item.FindControl("hyAction"), HyperLink)
                hyTemp.NavigateUrl = "PettyCashReversalView.aspx?BranchId=" & Server.UrlEncode(Me.sesBranchId.Trim.Replace("'", "")) & "&PettyCashNo=" & Server.UrlEncode(hyPettyCashNo.Text.Trim) & "&style=" & Server.UrlEncode("AccMnt")
            End If

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub


End Class