﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class PettyCashRePrintVoucherViewer
    Inherits Maxiloan.Webform.WebBased

    Private oController As New PettyCashController
    Private oParameter As New Parameter.PettyCash

    Private Property PettyCashNo() As String
        Get
            Return CType(ViewState("PettyCashNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PettyCashNo") = Value
        End Set
    End Property
    Private Property ReturnPage As String
        Get
            Return CType(ViewState("ReturnPage"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ReturnPage") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        ReturnPage = ""
        bindReport()
    End Sub

    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oReport As PettyCashVoucherPrint = New PettyCashVoucherPrint

        With oParameter
            .strConnection = GetConnectionString()
            .PettyCashNo = Me.PettyCashNo
        End With

        oDataSet = oController.GetPettyCashVoucher(oParameter)

        oReport.SetDataSource(oDataSet)
        CrystalReportViewer.ReportSource = oReport
        CrystalReportViewer.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "PettyCashVoucher" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "PettyCashVoucher.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        If Not (String.IsNullOrEmpty(ReturnPage)) Then
            Response.Redirect(String.Format("{0}.aspx?strFileLocation={1}{2}PettyCashVoucher", ReturnPage, Me.Session.SessionID, Me.Loginid))
        Else
            Response.Redirect("PettyCashRePrintVoucher.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "PettyCashVoucher")
        End If
    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_PETTY_CASH_VOUCHER)
        Me.PettyCashNo = cookie.Values("PettyCashNo")
        ReturnPage = cookie.Values("ReturnPage")
    End Sub
End Class