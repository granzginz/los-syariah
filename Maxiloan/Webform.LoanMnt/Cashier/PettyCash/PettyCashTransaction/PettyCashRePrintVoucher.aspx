﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PettyCashRePrintVoucher.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PettyCashRePrintVoucher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../../../Webform.UserController/ucBranchHO.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PettyCashRePrintVoucher</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinMain(BranchId, PettyCashNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?BranchId=' + BranchId + '&PettyCashNo=' + PettyCashNo + '&style=AccMnt', null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    REPRINT PETTY CASH VOUCHER
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    ControlToValidate="cboParent" ErrorMessage="Harap pilih Cabang" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Status
                </label>
                <asp:DropDownList ID="cboStatus" runat="server">
                    <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                    <asp:ListItem Value="PC">New</asp:ListItem>
                    <asp:ListItem Value="RV">Reversed</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="PettyCashNo">No Petty Cash</asp:ListItem>
                    <asp:ListItem Value="DepartementName">Departmen</asp:ListItem>
                    <asp:ListItem Value="EmployeeName">Nama Karyawan</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="88px" ></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Tanggal
                </label>
                <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PETTY CASH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Visible="False" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PettyCashNo" HeaderText="NO PETTY CASH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("PettyCashNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DepartementName" HeaderText="DEPARTEMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartementName" runat="server" Text='<%#Container.DataItem("DepartementName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="NAMA KARYAWAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%# Container.DataItem("EmployeeName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="35%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PettyCashDate" SortExpression="PettyCashDate" HeaderText="TGL PC"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="PettyCashStatus" HeaderText="STATUS">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPettyCashStatus" runat="server" Text='<%#Container.DataItem("PettyCashStatus")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BRANCH ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchId")%>'>Label</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                            Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>        
    </asp:Panel>
    </form>
</body>
</html>
