﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PettyCashInquiryView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PettyCashInquiryView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PettyCashInquiryView</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
        function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }

        function fclose() {
            window.close();
        }
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h3>
                    VIEW - TRANSAKSI PETTY CASH
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Petty Cash
                </label>
                <asp:Label ID="lblPettyCashNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Voucher
                </label>
                <asp:Label ID="lblRefVoucherNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Karyawan
                </label>
                <asp:Label ID="lblEmployeeName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal
                </label>
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Rekening Bank
                </label>
                <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblAmount" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Departemen
                </label>
                <asp:Label ID="lblDepartement" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Keterangan
                </label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single"><div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                  CssClass="grid_general"
                    ShowFooter="True">
                 <HeaderStyle CssClass="th" />
  <ItemStyle CssClass="item_grid" />
    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TRANSAKSI">
                            <HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("TransactionName")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN">
                            <HeaderStyle HorizontalAlign="Center" Width="45%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%#Container.DataItem("PCDDescription")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="Label1" runat="server">Total</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH">
                            <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPCDAmount" runat="server" Text='<%#formatnumber(Container.DataItem("PCDAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="lblTotal" runat="server">Label</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div></div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    INFORMASI KOREKSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Kasir
                </label>
                <asp:Label ID="lblCashierReversal" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Voucher
                </label>
                <asp:Label ID="lblRefVoucherNoReversal" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>STATUS</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status
                </label>
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Kasir
                </label>
                <asp:Label ID="lblCashierStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
