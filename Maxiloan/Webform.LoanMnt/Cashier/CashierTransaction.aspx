﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CashierTransaction.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.CashierTransaction" %>

<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CashierTransaction</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    BUKA / TUTUP KASIR
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Saldo Kepala Kasir
                </label>
                <asp:Label ID="lblHeadCashierBalance" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Saldo Awal
                </label>
                <uc1:ucnumberformat id="txtOpeningAmount" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Kasir
                </label>
                <%--<uc1:uccashier id="oCashier" runat="server"></uc1:uccashier>--%>
                <uc1:uccashier id="oCashier" runat="server" onselectedindexchanged="GetMySelection"></uc1:uccashier>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Password Kasir
                </label>
                <asp:TextBox ID="txtCashierPassword" runat="server"  MaxLength="20"
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Harap isi Password"
                    ControlToValidate="txtCashierPassword" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnOpen" runat="server" Text="Open" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KASIR
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCashierHistory" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="LoginId" HeaderText="KASIR">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Container.DataItem("LoginID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OpeningDate" SortExpression="openingdate" HeaderText="TGL AWAL"
                                DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="BeginingBalance" HeaderText="SALDO AWAL">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBeginingBalance" runat="server" Text='<%#FormatNumber(Container.DataItem("BeginingBalance"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EndingBalance" HeaderText="SALDO AKHIR">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblEndingBalance" runat="server" Text='<%#FormatNumber(Container.DataItem("EndingBalance"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CashierStatus" HeaderText="STATUS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Iif(container.dataitem("CashierStatus")="O", "Open", "Closed")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="closingdate" SortExpression="closingdate" HeaderText="TGL CLOSING"
                                DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="TUTUP">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbClose" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="CLOSE" Visible='<%#iif(container.dataitem("CashierStatus")="O", "True", "False")%>'>
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="OpeningSequence" HeaderText="Opening Sequence">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblOpeningSequence" runat="server" Text='<%#container.dataitem("OpeningSequence")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="EmployeeID" HeaderText="Employee ID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeID" runat="server" Text='<%#container.dataitem("EmployeeID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnHeadCashierClose" runat="server" CausesValidation="False" Text="Head Kasir Close"
                CssClass="small button blue"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCashierClose" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TUTUP KASIR
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Kasir
                </label>
                <asp:Label ID="lblCashierName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Awal
                </label>
                <asp:Label ID="lblOpeningDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Saldo Awal
                </label>
                <asp:Label ID="lblOpeningAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Saldo Akhir
                </label>
                <asp:Label ID="lblOnHandAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Saldo Kasir
                </label>
                <uc1:ucnumberformat id="txtClosingAmount" runat="server" />
            </div>
            <div class="form_right">
                <label class="label_req">
                    Password Kasir
                </label>
                <td class="tdganjil" width="30%">
                    <asp:TextBox ID="txtPasswordClose" runat="server"  MaxLength="20"
                        TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Enabled="True"
                        Visible="True" ErrorMessage="Harap isi Password" ControlToValidate="txtPasswordClose"
                        Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnCloseCashier" runat="server" Text="Close" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCloseCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TRANSAKSI KASIR
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah PDC
                </label>
                <asp:Label ID="lblNoOfPDC" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total Nilai PDC
                </label>
                <asp:Label ID="lblPDCAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgListCashierClose" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="REKENING BANK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankName" HeaderText="NAMA BANK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" runat="server" Text='<%#Container.DataItem("BankName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankPurpose" HeaderText="PENGGUNAAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankPurpose" runat="server" Text='<%#Container.DataItem("BankPurpose")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountType" HeaderText="JENIS">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountType" runat="server" Text='<%#Container.DataItem("BankAccountType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DebitAmount" HeaderText="TOTAL DEBET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDebitAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("DebitAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CreditAmount" HeaderText="TOTAL KREDIT">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreditAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("CreditAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlHeadClose" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KEPALA KASIR TUTUP
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgHeadCashier" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                        AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="REKENING BANK">
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblHCBankAccount" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankName" HeaderText="NAMA BANK">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblHCBankName" runat="server" Text='<%#Container.DataItem("BankName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankPurpose" HeaderText="PENGGUNAAN">
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblHCBankPurpose" runat="server" Text='<%#Container.DataItem("BankPurpose")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountType" HeaderText="JENIS">
                                <ItemStyle Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblHCBankAccountType" runat="server" Text='<%#Container.DataItem("BankAccountType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DebitAmount" HeaderText="SALDO AKHIR">
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl" runat="server" Text='<%#FormatNumber(Container.DataItem("DebitAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnHCClose" runat="server" Text="Head Kasir Close" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnHCCancel" runat="server" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah PDC dialokasi
                </label>
                <asp:Label ID="lblTotalPDC" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Pembayaran dialokasi
                </label>
                <asp:Label ID="lblTotalAPDisburse" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnExecuteBatch" runat="server" Text="Eksekusi" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlOutstanding" Visible="false" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PROSES BELUM SELESAI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgOutstanding" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="ID" HeaderText="ID" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblId" runat="server" Text='<%#Container.DataItem("ID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Container.DataItem("DESCRIPTION")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalDesc" runat="server" Text="Total" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsActive" HeaderText="Active" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" Text='<%#Iif(Container.DataItem("Isactive") = True,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SqlCommand" HeaderText="SqlCmd" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblSqlCmd" runat="server" Text='<%#Container.DataItem("SqlCommand")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TOTAL">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblSum" runat="server" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
