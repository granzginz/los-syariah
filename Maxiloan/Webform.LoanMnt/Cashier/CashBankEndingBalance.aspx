﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CashBankEndingBalance.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.CashBankEndingBalance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CashBankEndingBalance</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY SALDO AKHIR KAS-BANK
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR SALDO AKHIR KAS - BANK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="Datagrid1" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkHistory" CommandName="history">HISTORY</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="branchID" HeaderText="ID CABANG">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBRanchID" runat="server" Text='<%#Container.dataitem("branchID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankAccountID" runat="server" Visible="False" Text='<%#Container.dataitem("BankAccountID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="branchFullName" HeaderText="NAMA CABANG">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchFullName" runat="server" Text='<%#Container.dataitem("BranchInitialName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="NAMA REKENING">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.dataitem("BankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AccountNo" HeaderText="NO REKENING">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%#Container.dataitem("AccountNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                              <asp:TemplateColumn SortExpression="BeginBalance" HeaderText="SALDO AWAL"  HeaderStyle-HorizontalAlign="Right">                                
                                <ItemTemplate >
                                    <%--<asp:Label ID="lblBeginBalance" runat="server" CssClass="numberAlign2" Text='<%#formatnumber(Container.dataitem("BeginBalance"),0)%>'>--%>
                                    <asp:Label ID="lblBeginBalance" runat="server" CssClass="numberAlign2" Text='<%#formatnumber(Container.dataitem("BeginBalance"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EndingBalance" HeaderText="SALDO AKHIR" HeaderStyle-CssClass="numberAlign2">                                
                                <ItemTemplate >
                                    <%--<asp:Label ID="lblEndingBalance" runat="server" CssClass="numberAlign2" Text='<%#formatnumber(Container.dataitem("EndingBalance"),0)%>'>--%>
                                    <asp:Label ID="lblEndingBalance" runat="server" CssClass="numberAlign2" Text='<%#formatnumber(Container.dataitem("EndingBalance"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlHistory" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    HISTORY TRANSAKSI KAS - BANK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblBranchFullNameRef" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rekening Bank</label>
                <asp:Label ID="lblBankAccountNameRef" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Per Tanggal</label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>

         <div class="form_box">
            <div class="form_single">
                <label>
                    No Voucher</label>
                <asp:TextBox runat="server" ID="txtVocuherNo"></asp:TextBox> 
            </div>
        </div>

        <div class="form_button">
            <asp:Button ID="btnFindHistory" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlHistoryGrid" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    HISTORY TRANSAKSI KAS - BANK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="Datagrid2" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general" ShowFooter="True" GridLines="None">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="BranchID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchIDHistory" runat="server" Text='<%#Container.dataitem("BranchID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblbankaccountidHistory" runat="server" Text='<%#Container.dataitem("bankaccountid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO VOUCHER">
                                <ItemTemplate>
                                    <asp:Label ID="lblVoucherNoHistory" runat="server" Text='<%#Container.dataitem("VoucherNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA REKENING">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountNameHistory" runat="server" Text='<%#Container.dataitem("BankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COA">
                                <ItemTemplate>
                                    <asp:Label ID="lblCOAHistory" runat="server" Text='<%#Container.dataitem("COA")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescriptionHistory" runat="server" Text='<%#Container.dataitem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO BUKTI KAS">
                                <FooterTemplate>
                                    &nbsp;<asp:Label ID="Label1" runat="server" style="font-weight: 700" Text="Jumlah"></asp:Label>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblreferencenoHistory" runat="server" Text='<%#Container.dataitem("referenceno")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DEBET">                                
                                <FooterTemplate>
                                    &nbsp;&nbsp;<asp:Label ID="lblTotaoDebit" runat="server" Text="Label" 
                                        style="font-weight: 700"></asp:Label>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblDebitAmountHistory" runat="server" Text='<%#formatnumber(Container.dataitem("Debitamt"),0)%>'>--%>
                                    <asp:Label ID="lblDebitAmountHistory" runat="server" Text='<%#formatnumber(Container.dataitem("Debitamt"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH KREDIT">                                
                                <FooterTemplate>
                                    &nbsp;&nbsp;
                                    <asp:Label ID="lblTotalCredit" runat="server" Text="Label" 
                                        style="font-weight: 700"></asp:Label>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblCreditAmountHistory" runat="server" Text='<%#formatnumber(Container.dataitem("CreditAmt"),0)%>'>--%>
                                    <asp:Label ID="lblCreditAmountHistory" runat="server" Text='<%#formatnumber(Container.dataitem("CreditAmt"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="beginbalance">                                
                                <HeaderTemplate>
                                    <asp:Label ID="lblBeginBalanceRef" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndingBalanceHistory" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>


                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnBack" runat="server" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
