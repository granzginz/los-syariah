﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ATRequestView
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oAgree As UcAgreeExposure
    Protected WithEvents oCust As UcCustExposure
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property


#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "ATREQ"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack() Then
                Me.ApplicationID = Request.QueryString("ApplicationId")

                DoBind()
            End If

        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind()
        Dim oControllerInfo As New UCPaymentInfoController
        Dim oCustomClassInfo As New Parameter.AccMntBase
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With

        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        With oPaymentInfo
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .IsTitle = True
            .PaymentInfo()
        End With

        With oAgree
            .ApplicationID = Me.ApplicationID
            .AgreementExposure()
        End With

        With oCust
            .CustomerID = Me.strCustomerid
            .CustomerExposure()
        End With
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Server.Transfer("ATRequest.aspx")
    End Sub
#End Region

#Region "Next"
    Private Sub imbNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            'Server.Transfer("ATReq.aspx?Applicationid=" & Me.ApplicationID)
            Response.Redirect("ATReq.aspx?Applicationid=" & Me.ApplicationID & "&CustomerID=" & Me.strCustomerid.Trim)
        End If
    End Sub
#End Region

End Class