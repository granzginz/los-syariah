﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ATInqView.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ATInqView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ATInqView</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function fClose() {
            window.close();
            return false;
        }			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - OVER KONTRAK&nbsp;<asp:Label ID="lblJudulHeader" runat="server"></asp:Label>
            </h3>
        </div>
    </div>
    <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer Lama</label>
                <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
            </div>
    </div>
    <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Penjamin Lama</label>
                <asp:HyperLink ID="HyGuarantor" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Rekening lain Dimiliki</label>
                <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
            </div>
     </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Efektif</label>
            <asp:Label ID="lblEffDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4><label>
                A/R PER TANGGAL</label>
            <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div> 
    <div class="form_box">
        <div class="form_left">
            <label>
                Angsuran Jatuh Tempo</label>
            <asp:Label ID="lblInstallmentDue" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Asuransi Jatuh Tempo</label>
            <asp:Label ID="lblInsuranceDue" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
         <div class="form_left">
                <label>
                    Denda Keterlambatan Angsuran</label>
                <asp:Label ID="lblLCInstall" runat="server"></asp:Label>
         </div>
         <div class="form_right">
                <label>
                    Denda Keterlambatan Angsuransi</label>
                <asp:Label ID="lblLCInsurance" runat="server"></asp:Label>
         </div>
    </div>
    <div class="form_box">
         <div class="form_left">
                <label>
                    Biaya Tagih Angsuran</label>
                <asp:Label ID="lblInstallColl" runat="server"></asp:Label>
          </div>
          <div class="form_right">
                <label>
                    Biaya Tagih Asuransi</label>
                <asp:Label ID="lblInsuranceColl" runat="server"></asp:Label>
            </div>
    </div>
    <div class="form_box">
       
            <div class="form_left">
                <label>
                    Biaya Tolakan PDC</label>
                <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Perpanjangan STNK/BBN</label>
                <asp:Label ID="lblSTNKFee" runat="server"></asp:Label>
            </div>
       
    </div>
    <div class="form_box">
      
            <div class="form_left">
                <label>
                    Biaya Klaim Asuransi</label>
                <asp:Label ID="lblInsuranceClaim" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Tarik</label>
                <asp:Label ID="lblRepossessionFee" runat="server"></asp:Label>
            </div>
    
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Jatuh Tempo</label>
            <asp:Label ID="lblTotalOSOverDue" runat="server"></asp:Label>
        </div>
    </div>

    <div class="form_title">
        <div class="form_single">
            <h4>
                DAFTAR DOKUMEN
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgDoc" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="grid_general" BorderStyle="None" BorderWidth="0" ShowFooter="false"
                    Visible="true">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOCUMENT ID" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOCUMENT">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="35%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%#Container.DataItem("documentno")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="docreceiveddate" HeaderText="TGL TERIMA PERTAMA" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ASSETSTATUS" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatus")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ISMAINDOC" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblisMainDoc" runat="server" Text='<%#Container.DataItem("isMainDoc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL OVER KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Over Kontrak Kepada</label>
            <asp:HyperLink ID="lblAgreementTransferTo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
                <label>
                    Penjamin</label>
                <asp:HyperLink ID="lblGuarantor" runat="server"></asp:HyperLink>
            </div>
    </div>
    <div class="form_box">
          <div class="form_left">
                <label>
                    Biaya Administrasi</label>
                <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
            </div>
            <div class="form_right">

            </div>
    </div>
    <div class="form_box">
          <div class="form_left">
                <label>
                    Alasan</label>
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh</label>
                <asp:Label ID="lblApproved" runat="server"></asp:Label>
            </div>
   </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan</label>
            <asp:Label ID="lblNotes" runat="server"></asp:Label>
        </div>

        <div class="form_right">
            <label>
                Jumlah Prepaid:</label>
            <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC" runat="server" DataKeyField="TCName" Width="100%" CssClass="grid_general"
                    BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" Visible="true">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                            <HeaderStyle Width="25%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA" Visible="true">
                            <HeaderStyle Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCChecked" runat="server" Checked='<%#iif(Container.Dataitem("Checked")="1", True, False)%>'
                                    Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <HeaderStyle Width="30%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes" Text='<%#Container.DataItem("Notes")%>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC2" runat="server" DataKeyField="TCName" Width="100%" CssClass="grid_general"
                    BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" Visible="true"
                    ShowHeader="true">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO" visible="false" >
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo2" visible="false"  runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN" visible="false">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST" visible="false" >
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA" visible="false" >
                            <HeaderStyle Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCCheck2" visible="false" runat="server" Checked='<%#iif(Container.Dataitem("Checked")="1", True, False)%>'
                                    Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN" visible="false" >
                            <HeaderStyle Width="30%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes2" visible="false" runat="server" Text='<%# Container.DataItem("Notes") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonClose" OnClientClick="Close();" runat="server" CausesValidation="false"
            Text="Close" CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
