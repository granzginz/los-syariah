﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ATReq
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents ucLookUpCustomer As ucLookUpCustomer
    Protected WithEvents ucGuarantor As ucGuarantor
    Protected WithEvents oReq As ucApprovalRequest
    Protected WithEvents txtAdminFee As ucNumberFormat
    Protected WithEvents UcCompanyAddress1 As UcCompanyAddress


#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property

    Private Property EffRate() As Decimal
        Get
            Return (CType(ViewState("EffRate"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffRate") = Value
        End Set
    End Property

    Private Property TotInterest() As Double
        Get
            Return (CType(ViewState("TotInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInterest") = Value
        End Set
    End Property

    Private Property TotAmountTobePaid() As Double
        Get
            Return (CType(ViewState("TotAmountTobePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotAmountTobePaid") = Value
        End Set
    End Property

    Private Property TotInfo() As Double
        Get
            Return (CType(ViewState("TotInfo"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInfo") = Value
        End Set
    End Property

    Private Property Bev() As String
        Get
            Return (CType(ViewState("Bev"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Bev") = Value
        End Set
    End Property

    Private Property Product() As String
        Get
            Return (CType(ViewState("Product"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Product") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private m_controller As New ApplicationController
    Dim oApplication As New Parameter.Application
#End Region

#Region "Declare Variable"
    Dim tempPrincipalAmount As Double
    Dim tempInterestAmount As Double
    Dim tempOSPrincipalAmount As Double
    Dim tempOSInterestAmount As Double
    Dim tempdate As Date
    Dim i As Integer
    Dim j As Integer = 0
    Dim TotInstallmentAmount As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipalAmount As Label
    Dim TotOSInterestAmount As Label
    Dim style As String = "ACCMNT"
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "ATREQ"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                With txtAdminFee
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = True
                    .TextCssClass = "numberAlign regular_text"
                End With
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.strCustomerid = Request.QueryString("CustomerID")
                oPaymentInfo.IsTitle = True
                oReq.ApprovalScheme = "AGTF"
                oReq.ReasonTypeID = "AGTRF"
                'ButtonRequest.Visible = False
                'ButtonCancel.Visible = False
                pnlTC.Visible = False
                lblAgreementTransferTo.Visible = False
                DoBind()
                bindUC()
            End If
        End If
    End Sub
#End Region

#Region "BIND UC"
    Sub bindUC()
        ucGuarantor.ApplicationID = Me.ApplicationID
        ucGuarantor.bindData("REQ")


        ucLookUpCustomer.BindLookup(Me.strCustomerid.Trim)
        'txtoATTo.Text = ucLookUpCustomer.Name
        txtoATTo.Text = ""
        'Me.CustomerID = ucLookUpCustomer.CustomerID
        'Me.CustomerType = ucLookUpCustomer.CustomerType
    End Sub
#End Region



#Region "Dobind"
    Sub DoBind()
        GetList()
        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString()
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With

        CallPaymentInfo()
        If CDbl(oPaymentInfo.TotOSOverDue) > 0 Then
            ShowMessage(lblMessage, "Total Tunggak harus dibayar", True)
            ButtonTC.Visible = False
            Exit Sub
        End If
        '==========BInd Grid Document======================
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = " adc.branchid = '" & Me.sesBranchId.Replace("'", "") & "'  and applicationid = '" & Me.ApplicationID & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.AssetDocPaging(oCustomClass)
        DtUserList = oCustomClass.ListData

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()

        '=========================================

    End Sub
#End Region

#Region "CallPaymentInfo"
    Sub CallPaymentInfo()
        Dim tempdate As Date
        Dim totDiff As Integer
        Dim temp As Double
        Dim temp1 As Double

        With oPaymentInfo
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With

        'temp = (oPaymentInfo.MaximumOutStandingPrincipal * (Me.EffRate / 36000) * totDiff)
        'temp1 = (Round((temp + 50) / 100)) * 100
        'Me.TotInterest = temp1

    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("ATRequest.aspx")
    End Sub
#End Region

#Region "GetList"
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = DController.GetList(oCustomClass)
        With oCustomClass
            Me.EffRate = .EffectiveRate
            Me.Bev = .AgreementTransferBehav
            Me.PrepaidBalance = .Prepaid
            Me.Product = .ProductId
            HyGuarantor.Text = .GuarantorName.Trim
            If HyGuarantor.Text <> "-" Then
                HyGuarantor.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.GuarantorID.Trim) & "')"
            End If

            lblPrepaidAmount.Text = CStr(FormatNumber(Me.PrepaidBalance, 2))
            lblCrossDefault.Text = .IsCrossDefault
            If .IsCrossDefault = "Yes" Then
                ShowMessage(lblMessage, "Kontrak ini tidak dapat di over Kontrak", True)
                PnlDetail.Visible = False
                ButtonTC.Visible = False
                Exit Sub
            Else
                PnlDetail.Visible = True
            End If

            txtAdminFee.Text = CStr(oCustomClass.AgreementTransferFee)
            Select Case Me.Bev
                Case "L"
                    txtAdminFee.Enabled = False
                    txtAdminFee.RangeValidatorMinimumValue = "0"
                    txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
                Case "N"
                    txtAdminFee.RangeValidatorMinimumValue = CStr(oCustomClass.AgreementTransferFee)
                    txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
                Case "X"
                    txtAdminFee.RangeValidatorMaximumValue = CStr(oCustomClass.AgreementTransferFee)
                    txtAdminFee.RangeValidatorMinimumValue = "0"

                Case "D"
                    txtAdminFee.RangeValidatorMinimumValue = "0"
                    txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
            End Select
        End With
    End Sub
#End Region


#Region "GetCustType"
    Sub GetCustType()
        oCustomClass.CustomerID = txtoATTo.Text
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetCust(oCustomClass)
        Me.CustomerType = oCustomClass.CustomerType
    End Sub
#End Region

#Region "TC"
    Private Sub ButtonTC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTC.Click
        Dim validCheck As New Label
        Dim Mandatory As String

        If txtoATTo.Text = "" Then
            ShowMessage(lblMessage, "Harap isi Di Over Kontrak Kepada Siapa", True)
            Exit Sub
        ElseIf hdnoATTo.Value.Trim = Me.CustomerID.Trim Then
            ShowMessage(lblMessage, "Over Kontrak tidak boleh kepada Customer yang sama", True)
            Exit Sub
        Else
            'GetCustType()
            '=============Bind Grid TC================
            Dim oData As New DataTable
            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Me.sesBranchId
            oApplication.ProductID = Me.Product
            oApplication.Type = Me.CustomerType
            oApplication = m_controller.GetTC(oApplication)
            If Not oApplication Is Nothing Then
                oData = oApplication.ListData
            End If
            dtgTC.DataSource = oData.DefaultView
            dtgTC.DataKeyField = "TCName"
            dtgTC.DataBind()

            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Me.sesBranchId
            oApplication.ProductID = Me.Product
            oApplication.Type = Me.CustomerType
            oApplication = m_controller.GetTC2(oApplication)
            If Not oApplication Is Nothing Then
                oData = oApplication.ListData
            End If
            dtgTC2.DataSource = oData.DefaultView
            dtgTC2.DataKeyField = "TCName"
            dtgTC2.DataBind()

            If txtoATTo.Text = "" Then
                pnlTC.Visible = False
            Else
                pnlTC.Visible = True
            End If

            If dtgTC.Items.Count > 0 Then
                For j = 0 To dtgTC.Items.Count - 1
                    validCheck = CType(dtgTC.Items(j).FindControl("lblVTCChecked"), Label)
                    Mandatory = dtgTC.Items(j).Cells(3).Text.Trim
                    If Mandatory = "v" Then
                        validCheck.Visible = True
                    Else
                        validCheck.Visible = False
                    End If
                Next
            End If

            If dtgTC2.Items.Count > 0 Then
                For j = 0 To dtgTC2.Items.Count - 1
                    validCheck = CType(dtgTC2.Items(j).FindControl("lblVTC2Checked"), Label)
                    Mandatory = dtgTC2.Items(j).Cells(4).Text.Trim
                    If Mandatory = "v" Then
                        validCheck.Visible = True
                    Else
                        validCheck.Visible = False

                    End If

                Next
            End If

            'oATTo.Visible = False
            lblAgreementTransferTo.Text = txtoATTo.Text
            lblAgreementTransferTo.Visible = True
            ButtonTC.Visible = False
            'imbCalculate.Visible = True
            ButtonRequest.Visible = True
            ButtonCancel.Visible = True
            ' =========================================
        End If
    End Sub
#End Region

#Region "DataBOund"
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
#End Region

#Region "Request"
    Private Sub imbRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRequest.Click
        Dim oData1 As New DataTable
        Dim oData2 As New DataTable
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim validCheck As New Label
        Dim i As Integer
        Dim inlblseqno As String
        Dim MasterTCID, MasterTCID2, AGTCCLSequenceNo, Notes, AgreementNo As String

        If ucGuarantor.Name = txtoATTo.Text Then

            ShowMessage(lblMessage, "Penjamin tidak boleh sama dengan Customer yang mau di Over Kontrak", True)
            Exit Sub

        Else
            CallPaymentInfo()


            oData1.Columns.Add("MasterTCID", GetType(String))
            oData1.Columns.Add("IsChecked", GetType(String))
            oData1.Columns.Add("IsMandatory", GetType(String))
            oData1.Columns.Add("Notes", GetType(String))
            oData1.Columns.Add("SeqNo", GetType(Integer))

            oData2.Columns.Add("MasterTCID", GetType(String))
            oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
            oData2.Columns.Add("IsChecked", GetType(String))
            oData2.Columns.Add("Notes", GetType(String))
            oData2.Columns.Add("SeqNo", GetType(Integer))

            If dtgTC.Items.Count > 0 Then
                For i = 0 To dtgTC.Items.Count - 1
                    MasterTCID = dtgTC.Items(i).Cells(5).Text
                    Checked = CType(dtgTC.Items(i).FindControl("chkTCChecked"), CheckBox).Checked
                    Mandatory = dtgTC.Items(i).Cells(3).Text.Trim
                    Notes = CType(dtgTC.Items(i).Cells(4).FindControl("txtTCNotes"), TextBox).Text
                    inlblseqno = CType(dtgTC.Items(i).FindControl("lblTCNo"), Label).Text
                    If Mandatory = "v" And Checked = False Then
                        ShowMessage(lblMessage, "Harap periksa untuk yang mandatory", True)
                        Exit Sub
                    End If

                    dr = oData1.NewRow()
                    dr("MasterTCID") = MasterTCID.Trim
                    dr("IsChecked") = IIf(Checked = True, "1", "0")
                    dr("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                    dr("Notes") = Notes
                    dr("SeqNo") = CInt(inlblseqno)
                    oData1.Rows.Add(dr)
                Next

            End If

            If dtgTC2.Items.Count > 0 Then
                For i = 0 To dtgTC2.Items.Count - 1
                    MasterTCID = dtgTC2.Items(i).Cells(6).Text
                    AGTCCLSequenceNo = dtgTC2.Items(i).Cells(7).Text
                    Checked = CType(dtgTC2.Items(i).FindControl("chkTCCheck2"), CheckBox).Checked
                    Notes = CType(dtgTC2.Items(i).Cells(4).FindControl("txtTCNotes2"), TextBox).Text
                    Mandatory = dtgTC2.Items(i).Cells(4).Text.Trim
                    If Mandatory = "v" And Checked = False Then
                        ShowMessage(lblMessage, "harap periksa untuk yang mandatory", True)
                        Exit Sub
                    End If

                    dr2 = oData2.NewRow()
                    dr2("MasterTCID") = MasterTCID.Trim
                    dr2("AGTCCLSequenceNo") = AGTCCLSequenceNo.Trim
                    dr2("IsChecked") = IIf(Checked = True, "1", "0")
                    dr2("Notes") = Notes
                    dr2("SeqNo") = CInt(inlblseqno)
                    oData2.Rows.Add(dr2)
                Next

            End If

            With oCustomClass

                '==================Approval==================
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BusinessDate = Me.BusinessDate
                .TotAmountToBePaid = CDbl(txtAdminFee.Text)
                .LoginId = Me.Loginid
                .RequestTo = oReq.ToBeApprove
                .ChangeNotes = oReq.Notes
                .Data1 = oData1
                .Data2 = oData2

                .ApplicationID = ucGuarantor.ApplicationID
                .GuarantorName = ucGuarantor.Name
                .GuarantorJobTitle = ucGuarantor.Jabatan
                .GuarantorAddress = ucGuarantor.Address
                .GuarantorRT = ucGuarantor.RT
                .GuarantorRW = ucGuarantor.RW
                .GuarantorKelurahan = ucGuarantor.Kelurahan
                .GuarantorKecamatan = ucGuarantor.Kecamatan
                .GuarantorCity = ucGuarantor.Kota
                .GuarantorZipCode = ucGuarantor.KodePos
                .GuarantorAreaPhone1 = ucGuarantor.Area1
                .GuarantorPhone1 = ucGuarantor.Phone1
                .GuarantorAreaPhone2 = ucGuarantor.Area2
                .GuarantorPhone2 = ucGuarantor.Phone2
                .GuarantorAreaFax = ucGuarantor.AreaFax
                .GuarantorFax = ucGuarantor.Fax
                .GuarantorMobilePhone = ucGuarantor.NoHP
                .GuarantorEmail = ucGuarantor.Email
                .GuarantorNotes = ucGuarantor.Catatan
                .GuarantorPenghasilan = ucGuarantor.Penghasilan
                '==========================================

                '====================AT================

                '@OldCustomerID	varchar(20),
                '@CustomerID  varchar(20),
                '@GuarantorID varchar(20),
                '@Requestdate datetime,
                '@EffectiveDate datetime,
                '@AdministrationFee dec(17,2),
                '@ContractPrepaidAmount dec(17,2),
                '@OutstandingPrincipal dec(17,2),
                '@OutstandingInterest dec(17,2),
                '@OSInstallmentDue  dec (17,2),
                '@OSInsuranceDue dec(17,2),
                '@OSLCInstallment dec(17,2),
                '@OSLCInsurance dec(17,2),
                '@OSInstallCollectionFee dec(17,2),
                '@OSInsuranceCollectionFee dec(17,2),
                '@OSPDCBounceFee dec(17,2),
                '@STNKFee dec(17,2),
                '@InsuranceClaim  dec(17,2),
                '@ReposessFee dec(17,2),
                '@ApprovalNo varchar(50),
                '@ReasonTypeID varchar(5),
                '@ReasonID varchar(10),
                '@Notes varchar(50),

                '.CustomerID = hdnoATTo.Value
                .CustomerID = Me.CustomerID.Trim

                .OldCustId = Me.strCustomerid.Trim


                'Guarantor sudah tidak pakai ID lagi 
                'If ucGuarantor.CustomerID.Trim <> "" Then
                '    .GuarantorID = ucGuarantor.CustomerID.Trim
                'Else
                '    .GuarantorID = ""
                'End If

                .EffectiveDate = Me.BusinessDate
                .BusinessDate = Me.BusinessDate
                .AdminFee = CDbl(txtAdminFee.Text)

                .Prepaid = Me.PrepaidBalance
                .OutstandingPrincipal = oPaymentInfo.MaximumOutStandingPrincipal
                .OutStandingInterest = oPaymentInfo.MaximumOutStandingInterest
                .InstallmentDue = oPaymentInfo.MaximumInstallmentDue
                .InsuranceDue = oPaymentInfo.MaximumInsurance
                .LcInstallment = oPaymentInfo.MaximumLCInstallFee
                .LcInsurance = oPaymentInfo.MaximumLCInsuranceFee
                .InsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee
                .InstallmentCollFee = oPaymentInfo.MaximumInstallCollFee
                .PDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
                .STNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee
                .InsuranceClaimExpense = oPaymentInfo.MaximumInsuranceClaimFee
                .ReasonID = oReq.ReasonID
                .ReasonTypeID = oReq.ReasonTypeID

                '===========================================
                Try
                    oController.AgreementTransferReq(oCustomClass)
                    Response.Redirect("ATRequest.aspx")
                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End With

        End If
    End Sub
#End Region

#Region "LookupCustomer"
    Protected Sub btnLookupCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupCustomer.Click
        ucLookUpCustomer.CmdWhere = "All"
        ucLookUpCustomer.Sort = "CustomerID ASC"
        ucLookUpCustomer.Popup()
    End Sub
    Public Sub CatSelectedCustomer(ByVal CustomerID As String,
                                           ByVal Name As String,
                                           ByVal Address As String,
                                           ByVal BadType As String,
                                           ByVal IDType As String,
                                     ByVal IDTypeDescription As String,
                                     ByVal IDNumber As String,
                                     ByVal BirthPlace As String,
                                     ByVal BirthDate As DateTime,
                                     ByVal CustomerType As String)
        txtoATTo.Text = Name
        hdnoATTo.Value = CustomerID
        Me.CustomerID = CustomerID
        Me.CustomerType = CustomerType
    End Sub
#End Region
End Class