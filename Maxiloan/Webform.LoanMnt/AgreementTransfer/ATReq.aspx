﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ATReq.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ATReq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../Webform.UserController/ucLookUpCustomer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucGuarantor" Src="../../Webform.UserController/ucGuarantor.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../Webform.UserController/UCReason.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ATReq</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnoATTo" type="hidden" name="hdnoATTo" runat="server" />
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENGAJUAN OVER KONTRAK
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div class="form_left">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div class="form_left">
                        <label>
                            Penjamin</label>
                        <asp:HyperLink ID="HyGuarantor" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Kontrak Lain dimiliki</label>
                        <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        A/R PER TANGGAL -
                        <asp:Label ID="lbljudul" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <uc1:ucpaymentinfo id="oPaymentInfo" runat="server">
                </uc1:ucpaymentinfo>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR DOKUMEN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgDoc" runat="server" AutoGenerateColumns="False" Width="100%"
                            CssClass="grid_general" BorderStyle="None" BorderWidth="0" Visible="true">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="TERIMA DIDEPAN">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblrec" runat="server" Text='<%#Iif(Container.DataItem("isdocexist"),"Yes","No")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DOCUMENT ID" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblassetdocid" runat="server" Text='<%#Container.DataItem("assetdocid")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NAMA DOKUMEN">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDocname" runat="server" Text='<%#Container.DataItem("assetdocname")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NO DOKUMEN">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="center" Width="35%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDocNo" runat="server" Text='<%#Container.DataItem("documentno")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="docreceiveddate" HeaderText="TGL TERIMA PERTAMA" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="ASSETSTATUS" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetDocStatus" runat="server" Text='<%#Container.DataItem("assetdocstatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="STATUS">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("assetdocstatusdesc")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="statusdate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="ISMAINDOC" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblisMainDoc" runat="server" Text='<%#Container.DataItem("isMainDoc")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:Panel ID="PnlDetail" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DETAIL OVER KONTRAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Over Kontrak Kepada</label>
                        <asp:TextBox ID="txtoATTo" Enabled="false" runat="server"></asp:TextBox>
                        <asp:Button ID="btnLookupCustomer" runat="server" CssClass="small buttongo blue"
                            Text="..." CausesValidation="False" />
                        <uc1:uclookupcustomer id="ucLookUpCustomer" runat="server" oncatselected="CatSelectedCustomer" />
                        </uc1:ucLookUpCustomer>
                        <asp:Label ID="lblAgreementTransferTo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucguarantor id="ucGuarantor" runat="server"></uc1:ucguarantor>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Biaya Administrasi</label>
                        <uc1:ucnumberformat id="txtAdminFee" runat="server" />
                    </div>
                </div>
                <uc1:ucapprovalrequest id="oReq" runat="server"></uc1:ucapprovalrequest>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Prepaid</label>
                        <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonTC" runat="server" CausesValidation="False" Text="Next" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlTC" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            SYARAT DAN KONDISI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgTC" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" Width="100%" DataKeyField="TCName">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                        <HeaderStyle Width="25%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PERIKSA">
                                        <HeaderStyle Width="8%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                                runat="server"></asp:CheckBox>
                                            <asp:Label ID="lblVTCChecked" runat="server" ForeColor="Red">Must be checked!</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                        <HeaderStyle Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <HeaderStyle Width="30%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                runat="server" Width="95%">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            SYARAT DAN KONDISI CHECK LIST
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgTC2" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" Width="100%" DataKeyField="TCName">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                        <HeaderStyle Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                                        <HeaderStyle Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PERIKSA">
                                        <HeaderStyle Width="8%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                                runat="server"></asp:CheckBox>
                                            <asp:Label ID="lblVTC2Checked" runat="server" ForeColor="Red">Must be Checked!</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                                        <HeaderStyle Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <HeaderStyle Width="30%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                Width="95%">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_button">
                <asp:Button ID="ButtonRequest" runat="server" Text="Request" CssClass="small button blue"
                    CausesValidation="true"></asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
