﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ATInqView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property

    Private Property SeqNo() As Integer
        Get
            Return (CType(Viewstate("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("SeqNo") = Value
        End Set
    End Property

    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(Viewstate("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("PaymentFrequency") = Value
        End Set
    End Property

    Private Property OSPrincipal() As Double
        Get
            Return (CType(Viewstate("OSPrincipal"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("OSPrincipal") = Value
        End Set
    End Property

    Private Property flagdel() As String
        Get
            Return (CType(Viewstate("flagdel"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("flagdel") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
#End Region

#Region "Declare Variable"
    Dim i As Integer
    Dim j As Integer = 0
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "ATINQVIEW"
        If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                Me.ApplicationID = ""
                Me.BranchID = ""
                Me.SeqNo = 0
                If Request.QueryString("ApplicationID") <> "" Then Me.ApplicationID = Request.QueryString("ApplicationID")
                If Request.QueryString("BranchID") <> "" Then Me.BranchID = Request.QueryString("BranchID")
                If Request.QueryString("SeqNo") <> "" Then Me.SeqNo = CInt(Request.QueryString("SeqNo"))
                DoBind()
            End If
        End If
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        GetHeaderList()
        GetDetailList()
        '==========BInd Grid Document======================
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = " adc.branchid = '" & Me.sesBranchId.Replace("'", "") & "'  and applicationid = '" & Me.ApplicationID & "'"
            .SortBy = "  adc.applicationid"
        End With

        oCustomClass = oController.AssetDocPaging(oCustomClass)
        DtUserList = oCustomClass.listdata

        DtgDoc.DataSource = DtUserList.DefaultView
        DtgDoc.CurrentPageIndex = 0
        DtgDoc.DataBind()

        '=========================================
        BindTC()

    End Sub
#End Region

#Region "GetHeaderList"
    Sub GetHeaderList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = DController.GetList(oCustomClass)
        With oCustomClass
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"

            HyGuarantor.Text = .GuarantorName.Trim
            If HyGuarantor.Text <> "-" Then
                HyGuarantor.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.GuarantorID.Trim) & "')"
            End If
            lblCrossDefault.Text = .IsCrossDefault
        End With
    End Sub
#End Region

#Region "GetDetailList"
    Sub GetDetailList()
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.SeqNo = Me.SeqNo
        oCustomClass = oController.GetListExecAT(oCustomClass)
        With oCustomClass
            '================Payment Info===================================
            lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
            lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
            lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
            lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
            lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)
            lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)
            lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
            lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
            lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
            lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
            lblTotalOSOverDue.Text = FormatNumber(.InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                    .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                    .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                    .RepossessionFee, 2)
            '===============================================================

            '================Detail==========================
            lblAgreementTransferTo.Text = .ATName.Trim
            lblAgreementTransferTo.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.ATTo) & "')"
            lblGuarantor.Text = .GuarantorName.Trim
            lblGuarantor.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.GuarantorID) & "')"
            lblReason.Text = .ReasonDescription
            lblAdminFee.Text = FormatNumber(.AdminFee, 2)
            lblNotes.Text = .ChangeNotes
            lblApproved.Text = .ApprovedBy
            lblPrepaidAmount.Text = CStr(FormatNumber(.AdminFee, 2))

            lblEffDate.Text = .EffectiveDate.ToString("dd/MM/yyyy")
            lbljudul.Text = .EffectiveDate.ToString("dd/MM/yyyy")
            lblJudulHeader.Text = .statusdesc.ToUpper.Trim
        End With
        '================================================
    End Sub
#End Region

#Region "BindTC"
    Sub BindTC()

        Dim DtUserList As New DataTable
        Dim DtTC2 As New DataTable

        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.SeqNo = Me.SeqNo
        oCustomClass = oController.GetATTC(oCustomClass)
        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.listdata
        End If
        dtgTC.DataSource = DtUserList.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()
        dtgTC.Visible = True

        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.SeqNo = Me.SeqNo
        oCustomClass = oController.GetATTC2(oCustomClass)
        If Not oCustomClass Is Nothing Then
            DtTC2 = oCustomClass.Data1
        End If
        dtgTC2.DataSource = DtTC2.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()
    End Sub

#End Region

#Region "DataBound"
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound

        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

#End Region

End Class