﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ATInq.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ATInq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ATInq</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        INQUIRY OVER KONTRAK
                    </h3>
                </div>
            </div>
            <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang</label>
                        <uc1:ucbranch id="oBranch" runat="server">
                    </uc1:ucbranch>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Tanggal Efektif</label>
                        <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtdate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtdate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
            </div>
            <div class="form_box">
                
                    <div class="form_left">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="cboStatus" runat="server">
                            <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                            <asp:ListItem Value="R">Request</asp:ListItem>
                            <asp:ListItem Value="A">Approve</asp:ListItem>
                            <asp:ListItem Value="C">Cancel</asp:ListItem>
                            <asp:ListItem Value="E">Execute</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                            Status Cetak</label>
                        <asp:DropDownList ID="ddlPrint" runat="server">
                            <asp:ListItem Value="0">No</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                        </asp:DropDownList>
                    </div>
             
            </div>
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server">
                </uc1:ucsearchby>
            </div>
            <div class="form_button">
                <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h3>
                            DAFTAR OVER KONTRAK
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" AllowSorting="True" OnSortCommand="SortGrid"
                                AutoGenerateColumns="False" DataKeyField="Applicationid" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <HeaderStyle HorizontalAlign="Center" Width="6%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="6%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyView" runat="server" Text='VIEW'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CETAK" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="6%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="6%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyPrint" runat="server" Text='PRINT'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Branchid" HeaderText="BRANCH ID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SeqNO" HeaderText="SeqNO" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSeqNO" runat="server" Text='<%#Container.DataItem("SeqNO")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                                        <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                        <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="Status" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="administrationFee" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmountToBepaid" runat="server" Text='<%#formatnumber(Container.DataItem("administrationFee"),0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="EFFECTIVEDATE" HeaderText="TGL EFEKTIF" SortExpression="AGT.EFFECTIVEDATE"
                                        DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Statusdesc" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatusdesc" runat="server" Text='<%#Container.DataItem("Statusdesc")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">
                                        <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="25%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
