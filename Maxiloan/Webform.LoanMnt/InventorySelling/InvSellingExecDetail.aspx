﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InvSellingExecDetail.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InvSellingExecDetail" %>

<%@ Register TagPrefix="uc1" TagName="UCSearchBY" Src="../../Webform.UserController/UCSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>InvSellingReceiveDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    EKSEKUSI HASIL PENJUALAN ASSET
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Asset
                </label>
                <asp:Label ID="LblAssetType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Rangka
                </label>
                <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Mesin
                </label>
                <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Jual
                </label>
                <asp:Label ID="lblSellingDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Harga Jual
                </label>
                <asp:Label ID="lblSellingAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pembeli
                </label>
                <asp:Label ID="lblBuyer" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Titipan
                </label>
                <asp:Label ID="lblTitipanPembeli" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal STNK
                </label>
                <asp:Label ID="LblTaxDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polis
                </label>
                <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan
                </label>
                <asp:Label ID="LblNotes" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSaveLastPayment" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancelLastPayment" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
