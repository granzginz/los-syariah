﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter 
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InvSellingReceiveDetail
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
#Region "Properties"
    Private Property Mode() As String
        Get
            Return CType(viewstate("Mode"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(viewstate("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property SellingAmount() As Double
        Get
            Return CDbl(viewstate("SellingAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("SellingAmount") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InvSelling
    Private m_controller As New InventorySellingController
    Private oController As New InstallRcvController
    Private oRCV As New Parameter.InstallRcv
    Private Property HistorySequenceNo As Integer
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
           

            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("BranchID")

            With oPaymentDetail
                .WithOutPrepaid = True
                .ValueDate = Me.BusinessDate.ToString("dd/MM/yyyy")
                .BankPurpose = "HB"
                .DoFormCashModule()
            End With
            
            Me.FormID = "INVSELLING"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then 
                DoBind()
            End If 
        End If
    End Sub
    Private Sub DoBind()
        Dim oController As New InventorySellingController
        Dim oCustomClass As New Parameter.InvSelling

        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        oCustomClass = oController.InvSellingReceiveView(oCustomClass)

        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType

            lblAgreementBranch.Text = .BranchAgreement
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblAssetDescription.Text = .AssetDescription
            LblAssetType.Text = .AssetType
            lblChassisNo.Text = .ChassisNo
            lblEngineNo.Text = .EngineNo
            lblLicensePlate.Text = .LicensePlate
            LblTaxDate.Text = .TaxDate.ToString("dd/MM/yyyy")
            lblSellingDate.Text = .SellingDate.ToString("dd/MM/yyyy")
            lblSellingAmount.Text = FormatNumber(.SellingAmount, 2)
            Me.SellingAmount = .SellingAmount
            lblBuyer.Text = .Buyer
            LblNotes.Text = .SellingNotes
            lblTitipanPembeli.Text = FormatNumber(.TitiapnPembeli, 2)
            lblRekening.Text = .BankAccountName
        End With
    End Sub
    Private Sub BtnSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSaveLastPayment.Click
        Dim VoucherNo As String

        If Me.Page.IsValid Then
            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                If oPaymentDetail.WayOfPayment = "CA" Then
                    If Not CheckCashier(Me.Loginid) Then
                        ShowMessage(lblMessage, "Kasir Belum Buka", True)
                        Exit Sub
                    End If
                End If 

                If ConvertDate2(oPaymentDetail.ValueDate) > Me.BusinessDate Then
                    ShowMessage(lblMessage, "Tanggal Valuta Harus <= Tanggal hari ini", True)
                    Exit Sub
                End If

				If oPaymentDetail.AmountReceive = 0 Then
					ShowMessage(lblMessage, "Amount harus > 0", True)
					Exit Sub
				End If

				'''' CEGAT AMOUNT LEBIH
				If CDbl(oPaymentDetail.AmountReceive + CDbl(lblTitipanPembeli.Text)) > CDbl(lblSellingAmount.Text) Then
					ShowMessage(lblMessage, "Amount Melebihi Selling Amount", True)
					Exit Sub
				End If

				With oCustomClass
						.strConnection = GetConnectionString()
						.LoginId = Me.Loginid
						.BusinessDate = Me.BusinessDate
						.BranchId = Me.sesBranchId.Replace("'", "")
						.BranchAgreement = Me.BranchID
						.ReceivedFrom = oPaymentDetail.ReceivedFrom
						.ReferenceNo = oPaymentDetail.ReferenceNo
						.WOP = oPaymentDetail.WayOfPayment
						.ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
						.BankAccountID = oPaymentDetail.BankAccount
						.AmountReceive = oPaymentDetail.AmountReceive
						.Notes = oPaymentDetail.Notes
						.ApplicationID = Me.ApplicationID

						.CoyID = Me.SesCompanyID
					End With


					Try
						Dim SavingController As New InventorySellingController
						VoucherNo = SavingController.SavingInventorySellingReceive(oCustomClass)
						'If oPaymentDetail.WayOfPayment = "CA" Then
						'    BindReport(VoucherNo)
						'Else
						Response.Redirect("InventorySellingReceive.aspx?msg=1")
						'End If


					Catch exp As Exception
						ShowMessage(lblMessage, exp.Message, True)
					End Try
				End If
			End If
    End Sub
    Sub BindReport(ByVal VoucherNo As String)

        Dim oData As New DataSet
        Dim objReport As RptKwitansi2 = New RptKwitansi2

        oRCV.strConnection = GetConnectionString()
        oRCV.VoucherNO = VoucherNo
        oData = oController.ReportKwitansiInstallmentWithVoucherNo(oRCV)

        objReport.Subreports(0).SetDataSource(oData)
        objReport.SetDataSource(oData)


        AddParamField(objReport, "CashierName", Me.FullName)
        AddParamField(objReport, "BusinessDate", Me.BusinessDate.ToString("dd") + " " + getMonthID() + " " + Me.BusinessDate.ToString("yyyy"))
        AddParamField(objReport, "BranchName", oData.Tables(0).Rows(0).Item("BranchCity").ToString)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String
        strFileLocation = pathReport("InventorySellingReceive", True) & Me.ApplicationID.Replace("/", "").Trim & ".pdf"

        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        Response.Redirect("InventorySellingReceive.aspx?filekwitansi=" & pathReport("InventorySellingReceive", False) & Me.ApplicationID.Replace("/", "").Trim)

    End Sub
    Private Function getMonthID() As String
        Dim rtn As String() = New String() {"Januari", "Febuari", "Maret", "April", "Mai", "Juni", "Juli", "Augustus", "Oktober", "November", "Desember"}

        Return rtn(Month(Me.BusinessDate) - 1)
     

    End Function
    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
    Private Function pathReport(ByVal group As String, ByVal dirXML As Boolean) As String
        Dim strDirectory As String = ""
        If dirXML = True Then
            strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += Me.sesBranchId.Replace("'", "") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += group & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If
        Else
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "/"
            strDirectory += Me.sesBranchId.Replace("'", "") & "/"
            strDirectory += group & "/"
        End If
        Return strDirectory
    End Function
    Private Sub BtnCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelLastPayment.Click
        Response.Redirect("InventorySellingReceive.aspx")
    End Sub

End Class