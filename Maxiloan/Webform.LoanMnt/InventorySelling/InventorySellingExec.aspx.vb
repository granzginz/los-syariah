﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class InventorySellingExec
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
#Region "Constanta"

    Private m_controller As New InventorySellingController
    Private m_uscontroller As New DataUserControlController
    Private oInvSelling As New Parameter.InvSelling

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            txtPage.Text = "1"
            Me.FormID = "EXESELLING"
            oSearchBy.ListData = "Buyer,Buyer-AM.Description, Asset Description -AT.Description,Asset Type-AA.LicensePlate,License Plate"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then

                Me.SearchBy = ""
                Me.SortBy = ""
                InitialDefaultPanel()
                Dim dtBranch As New DataTable
                dtBranch = m_uscontroller.GetBranchName(GetConnectionString, Me.sesBranchId)

                With cboParent
                    .DataSource = m_uscontroller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(sesBranchId.Replace("'", "")))
            End If
        End If
    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        PnlSearch.Visible = True
        lblMessage.Text = ""
    End Sub
#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()

        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.searchby, Me.sortby)
    End Sub

#End Region
#Region "Reseh..eh Reset !"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("InventorySellingExec.aspx")
    End Sub
#End Region
    Sub BindGridEntity(ByVal searchby As String, ByVal sortby As String)
        With oInvSelling
            .WhereCond = searchby
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortby
            .strConnection = GetConnectionString
        End With
        oInvSelling = m_controller.GetInvSellingReceive(oInvSelling)
        recordCount = oInvSelling.TotalRecords
        DtgPaging.DataSource = oInvSelling.listdata
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()
        PnlSearch.Visible = True
        PnlGrid.Visible = True
        PagingFooter()
    End Sub

    Private Sub DtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPaging.ItemCommand
        Select Case e.CommandName
            Case "Exec"
                Dim lblApplicationid As HyperLink
                lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
                Response.Redirect("InvSellingExecDetail.aspx?ApplicationID=" & lblApplicationid.Text & "&BranchID=" & cboParent.SelectedValue)
        End Select
    End Sub


    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        PnlSearch.Visible = True
        PnlGrid.Visible = True
        Dim strSearch As New StringBuilder
        Dim StrBranch As String

        If Me.IsSingleBranch Then
            StrBranch = " Agreement.BranchID = '" & cboParent.SelectedValue & "' and Agreement.TitipanPembeli >= ARO.SellingAmount "
        Else

            ShowMessage(lblMessage, "Harap Login di Cabang", True)
            Exit Sub
        End If
        strSearch.Append(StrBranch.Trim)

        If oSearchBy.Text <> "" Then
            strSearch.Append(" and ")
            If Right(oSearchBy.Text, 1) = "%" Then
                strSearch.Append(oSearchBy.ValueID)
                strSearch.Append(" like '")
                strSearch.Append(oSearchBy.Text)
                strSearch.Append("'")
            Else
                strSearch.Append(oSearchBy.ValueID)
                strSearch.Append(" = '")
                strSearch.Append(oSearchBy.Text)
                strSearch.Append("'")
            End If
        End If

        If txtSellingDate.Text <> "" Then
            strSearch.Append(" and ")
            strSearch.Append("SellingDate")
            strSearch.Append(" = '")
            strSearch.Append(ConvertDate2(txtSellingDate.Text))
            strSearch.Append("'")
        End If



        Me.SearchBy = strSearch.ToString
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

End Class