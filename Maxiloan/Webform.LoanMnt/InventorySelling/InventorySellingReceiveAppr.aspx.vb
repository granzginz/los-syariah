﻿
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController

Public Class InventorySellingReceiveAppr
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Private m_controller As New InventorySellingController
    Private m_uscontroller As New DataUserControlController
    Private oInvSelling As New Parameter.InvSelling

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        lblMessage.Text = ""
        If Not Me.IsPostBack Then

            Me.FormID = "INVSELLINGAPR"
            oSearchBy.ListData = "Buyer,Buyer-AM.Description, Asset Description -AT.Description,Asset Type-AA.LicensePlate,License Plate"
            'If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then

            If Request("msg") = "1" Then
                ShowMessage(lblMessage, "Sukses", False)
            End If

            Me.SearchBy = ""
            Me.SortBy = ""
            InitialDefaultPanel()
            Dim dtBranch As New DataTable
            dtBranch = m_uscontroller.GetBranchName(GetConnectionString, Me.sesBranchId)

            With cboParent
                .DataSource = m_uscontroller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(sesBranchId.Replace("'", "")))
            'End If
        End If

    End Sub

    Protected Overloads Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Private Sub InitialDefaultPanel()
        'PnlGrid.Visible = False
        PnlSearch.Visible = True 
    End Sub

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) 
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "desc")) 
        BindGridEntity()
    End Sub
    Private Sub DtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPaging.ItemCommand
        Select Case e.CommandName
            Case "Receive"
                Dim lblApplicationid As HyperLink
                lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
                Dim repoid = e.Item.Cells(0).Text.Trim()
                Response.Redirect("InvSellingReceiveDetailAppr.aspx?ApplicationID=" & lblApplicationid.Text & "&BranchID=" & cboParent.SelectedValue & "&RepoId=" & repoid)
        End Select
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("InventorySellingReceiveAppr.aspx") 
    End Sub
    Sub BindGridEntity(Optional isFrNav As Boolean = False)
        With oInvSelling
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .strConnection = GetConnectionString()
        End With
        oInvSelling = m_controller.GetInvSellingReceiveApproval(oInvSelling)
        recordCount = oInvSelling.TotalRecords
        DtgPaging.DataSource = oInvSelling.ListData
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()
        PnlSearch.Visible = True
        PnlGrid.Visible = True
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub


    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        PnlSearch.Visible = True
        PnlGrid.Visible = True
        Dim strSearch As New StringBuilder
        Dim StrBranch As String

        If Me.IsSingleBranch Then
            StrBranch = " Agreement.BranchID = '" & cboParent.SelectedValue & "' "
        Else

            ShowMessage(lblMessage, "Harap Login di Cabang", True)
            Exit Sub

        End If
        strSearch.Append(StrBranch.Trim)

        If oSearchBy.Text <> "" Then
            strSearch.Append(" and ")
            If Right(oSearchBy.Text, 1) = "%" Then
                strSearch.Append(oSearchBy.ValueID)
                strSearch.Append(" like '")
                strSearch.Append(oSearchBy.Text)
                strSearch.Append("'")
            Else
                strSearch.Append(oSearchBy.ValueID)
                strSearch.Append(" = '")
                strSearch.Append(oSearchBy.Text)
                strSearch.Append("'")
            End If
        End If

        If txtSellingDate.Text <> "" Then
            strSearch.Append(" and ")
            strSearch.Append("SellingDate")
            strSearch.Append(" = '")
            strSearch.Append(ConvertDate2(txtSellingDate.Text))
            strSearch.Append("'")
        End If

        Me.SearchBy = strSearch.ToString
        BindGridEntity()
    End Sub


End Class