﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class InvSellingExecDetail
    Inherits Maxiloan.Webform.AccMntWebBased
#Region "Properties"
    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property SellingAmount() As Double
        Get
            Return CDbl(ViewState("SellingAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("SellingAmount") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InvSelling
    Private m_controller As New InventorySellingController
    Private oController As New InstallRcvController
    Private oRCV As New Parameter.InstallRcv
    Private Property HistorySequenceNo As Integer
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("BranchID")

            Me.FormID = "EXESELLING"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'If Not CheckCashier(Me.Loginid) Then

                '    ShowMessage(lblMessage, "Kasir Belum Buka", True)
                '    Exit Sub
                'End If
                DoBind()
            End If
        Else
            'If Not CheckCashier(Me.Loginid) Then
            '    ShowMessage(lblMessage, "Kasir Belum Buka", True)
            '    Exit Sub
            'End If
        End If
    End Sub
    Private Sub DoBind()
        Dim oController As New InventorySellingController
        Dim oCustomClass As New Parameter.InvSelling

        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        oCustomClass = oController.InvSellingReceiveView(oCustomClass)

        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType

            lblAgreementBranch.Text = .BranchAgreement
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblAssetDescription.Text = .AssetDescription
            LblAssetType.Text = .AssetType
            lblChassisNo.Text = .ChassisNo
            lblEngineNo.Text = .EngineNo
            lblLicensePlate.Text = .LicensePlate
            LblTaxDate.Text = .TaxDate.ToString("dd/MM/yyyy")
            lblSellingDate.Text = .SellingDate.ToString("dd/MM/yyyy")
            lblSellingAmount.Text = FormatNumber(.SellingAmount, 2)
            Me.SellingAmount = .SellingAmount
            lblBuyer.Text = .Buyer
            lblTitipanPembeli.Text = FormatNumber(.TitiapnPembeli, 2)
        End With
    End Sub
    Private Sub BtnSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSaveLastPayment.Click
        If Me.Page.IsValid Then
            If CheckFeature(Me.Loginid, Me.FormID, "EXE", Me.AppId) Then
                'If Not CheckCashier(Me.Loginid) Then
                '    ShowMessage(lblMessage, "Kasir Belum Buka", True)
                '    Exit Sub
                'End If
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .BranchAgreement = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .AmountReceive = CDbl(lblTitipanPembeli.Text)
                    .CoyID = Me.SesCompanyID
                End With


                Try
                    Dim SavingController As New InventorySellingController
                    SavingController.SavingInventorySellingExec(oCustomClass)
                    Response.Redirect("InventorySellingExec.aspx")
                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
        End If
    End Sub
   
    
    Private Sub BtnCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelLastPayment.Click
        Response.Redirect("InventorySellingExec.aspx")
    End Sub
End Class