﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventorySellingReceive.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InventorySellingReceive" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InventorySellingReceive</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="PnlSearch" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENERIMAAN PEMBAYARAN DARI PENJUALAN ASSET
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Cabang
                    </label>
                    <asp:DropDownList ID="cboParent" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                        ErrorMessage="Harap pilih Cabang" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Tanggal Jual
                    </label>
                    <asp:TextBox runat="server" ID="txtSellingDate"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtSellingDate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
           
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
             
            <div class="form_button">
                <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlGrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PENERIMAAN PEMBAYARAN DARI PENJUALAN ASSET
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgPaging" runat="server" CssClass="grid_general" OnSortCommand="Sorting"
                            DataKeyField="Buyer" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle CssClass="command_col" Width="7%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="HypReceive" runat="server" Text="TERIMA" CommandName="Receive" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                    <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px" Width="12%">
                                    </HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Buyer" HeaderText="PEMBELI">
                                    <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Buyer" runat="server" Text='<%#Container.DataItem("Buyer") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="KONTRAK">
                                    <HeaderStyle HorizontalAlign="Left" Width="15%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle HorizontalAlign="Left" Width="20%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblName" runat="server" Text='<%#Container.DataItem("Name") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AM.Description" HeaderText="NAMA ASSET">
                                    <HeaderStyle HorizontalAlign="Center" Width="22%"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetDescription" runat="server" Text='<%#Container.DataItem("AssetDescription") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AT.Description" HeaderText="JENIS ASSET">
                                    <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetType" runat="server" Text='<%#Container.DataItem("AssetType") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AA.LicensePlate" HeaderText="NO POLISI">
                                    <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblLicence" runat="server" Text='<%#Container.DataItem("LicensePlate") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SellingDate" HeaderText="TGL JUAL">
                                    <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSellingDate" runat="server" Text='<%#Container.DataItem("SellingDate") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SellingAmount" HeaderText="HARGA JUAL">
                                    <HeaderStyle HorizontalAlign="center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="right" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSellingAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("SellingAmount"), 2) %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="TitipanPembeli" HeaderText="TOTAL TITIPAN">
                                    <HeaderStyle HorizontalAlign="center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="right" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTitipanPembali" runat="server" Text='<%#FormatNumber(Container.DataItem("TitipanPembeli"), 2) %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="TitipanBelumMasuk" HeaderText="TITIPAN MASUK">
                                    <HeaderStyle HorizontalAlign="center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="right" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTitipanBelumMasuk" runat="server" Text='<%#FormatNumber(Container.DataItem("AmountReceive"), 2) %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                               <asp:TemplateColumn SortExpression="ApprovedStatus" HeaderText="APPROVAL STATUS" Visible="false">
                                    <HeaderStyle HorizontalAlign="center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="right" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsApproved" runat="server" Text='<%#Container.DataItem("Status") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left"   Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                          <uc2:ucGridNav id="GridNavigator" runat="server"/>   
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
