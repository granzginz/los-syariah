﻿Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter 
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class InvSellingReceiveDetailAppr
    Inherits Maxiloan.Webform.AccMntWebBased
    Private Property RepoId() As Long
        Get
            Return CType(ViewState("RepoId"), Long)
        End Get
        Set(ByVal Value As Long)
            ViewState("RepoId") = Value
        End Set
    End Property

    Private Property DataRepo() As DataTable
        Get
            Return CType(ViewState("DataRepo"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataRepo") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
             
            ApplicationID = Request.QueryString("ApplicationID")
            BranchID = Request.QueryString("BranchID")
            RepoId = CType(Request.QueryString("RepoId"), Long)
             
            Me.FormID = "INVSELLINGAPPR"
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            DoBind()
            'End If
        End If
    End Sub

    Private Sub DoBind()
        Dim oController As New InventorySellingController
        Dim oCustomClass As New Parameter.InvSelling

        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
            .RepoId = RepoId
        End With
        oCustomClass = oController.InvSellingReceiveViewRepo(oCustomClass)

        If (oCustomClass.ListData.Rows.Count <= 0) Then
            ShowMessage(lblMessage, "Data Tidak Ditemukan", True)
            Exit Sub
        End If

        DataRepo = oCustomClass.ListData

        With oCustomClass
            Me.AgreementNo = .Agreementno


            Me.CustomerID = IIf(IsDBNull(DataRepo.Rows(0)("CustomerID")), "", CType(DataRepo.Rows(0)("CustomerID"), String))
            Me.CustomerType = .CustomerType

            lblAgreementBranch.Text = IIf(IsDBNull(DataRepo.Rows(0)("BranchFullName")), "", CType(DataRepo.Rows(0)("BranchFullName"), String))
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = IIf(IsDBNull(DataRepo.Rows(0)("AgreementNo")), "", CType(DataRepo.Rows(0)("AgreementNo"), String))
            lblCustomerName.Text = IIf(IsDBNull(DataRepo.Rows(0)("Name")), "", CType(DataRepo.Rows(0)("Name"), String))
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblAssetDescription.Text = IIf(IsDBNull(DataRepo.Rows(0)("AssetDescription")), "", CType(DataRepo.Rows(0)("AssetDescription"), String))
            LblAssetType.Text = IIf(IsDBNull(DataRepo.Rows(0)("AssetType")), "", CType(DataRepo.Rows(0)("AssetType"), String))
            lblChassisNo.Text = IIf(IsDBNull(DataRepo.Rows(0)("chassisNo")), "", CType(DataRepo.Rows(0)("chassisNo"), String))
            lblEngineNo.Text = IIf(IsDBNull(DataRepo.Rows(0)("EngineNo")), "", CType(DataRepo.Rows(0)("EngineNo"), String))
            lblLicensePlate.Text = IIf(IsDBNull(DataRepo.Rows(0)("LicensePlate")), "", CType(DataRepo.Rows(0)("LicensePlate"), String))
            LblTaxDate.Text = IIf(IsDBNull(DataRepo.Rows(0)("TaxDate")), "", CType(DataRepo.Rows(0)("TaxDate"), DateTime).ToString("dd/MM/yyyy"))
            lblSellingDate.Text = IIf(IsDBNull(DataRepo.Rows(0)("SellingDate")), "", CType(DataRepo.Rows(0)("SellingDate"), DateTime).ToString("dd/MM/yyyy"))
            lblSellingAmount.Text = IIf(IsDBNull(DataRepo.Rows(0)("SellingAmount")), "0", FormatNumber(CDbl(DataRepo.Rows(0)("SellingAmount")), 2))
            lblBuyer.Text = IIf(IsDBNull(DataRepo.Rows(0)("Buyer")), "", CType(DataRepo.Rows(0)("Buyer"), String))
            LblNotes.Text = IIf(IsDBNull(DataRepo.Rows(0)("SellingNotes")), "", CType(DataRepo.Rows(0)("SellingNotes"), String))
            lblTitipanPembeli.Text = IIf(IsDBNull(DataRepo.Rows(0)("TitipanPembeli")), "0", FormatNumber(CDbl(DataRepo.Rows(0)("TitipanPembeli")), 2))

            lblCaraPembayaran.Text = IIf(IsDBNull(DataRepo.Rows(0)("WOP")), "-", IIf(CType(DataRepo.Rows(0)("WOP"), String) = "BA", "BANK", "CASH"))
            lblJumlahTerima.Text = IIf(IsDBNull(DataRepo.Rows(0)("amountReceive")), "0", FormatNumber(CDbl(DataRepo.Rows(0)("amountReceive")), 2))
            lblRekening.Text = IIf(IsDBNull(DataRepo.Rows(0)("BankAccountName")), "", CType(DataRepo.Rows(0)("BankAccountName"), String))
            lblTanggalBayar.Text = IIf(IsDBNull(DataRepo.Rows(0)("ValueDate")), "", CType(DataRepo.Rows(0)("ValueDate"), DateTime).ToString("dd/MM/yyyy"))
            lblKeterangan.Text = IIf(IsDBNull(DataRepo.Rows(0)("notes")), "", CType(DataRepo.Rows(0)("notes"), String))
        End With
    End Sub
    Private Sub b_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelLastPayment.Click
        Response.Redirect("InventorySellingReceiveAppr.aspx")
    End Sub
     
    Private Sub BtnSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim oCustomClass As New Parameter.InvSelling
            With oCustomClass
                .strConnection = GetConnectionString()
                .LoginId = Me.Loginid
                .BusinessDate = CType(DataRepo.Rows(0)("PostingDate"), DateTime)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BranchAgreement = CType(DataRepo.Rows(0)("branchAgreement"), String)
                .ReceivedFrom = CType(DataRepo.Rows(0)("ReceivedFrom"), String)
                .ReferenceNo = CType(DataRepo.Rows(0)("ReferenceNo"), String)
                .WOP = CType(DataRepo.Rows(0)("WOP"), String)
                .ValueDate = CType(DataRepo.Rows(0)("ValueDate"), DateTime)
                .BankAccountID = CType(DataRepo.Rows(0)("bankAccountId"), String)
                .AmountReceive = CDbl(DataRepo.Rows(0)("amountReceive"))
                .Notes = CType(DataRepo.Rows(0)("notes"), String)
                .ApplicationID = Me.ApplicationID 
                .CoyID = Me.SesCompanyID
                .RepoId = RepoId
                .Approval = cboappr.SelectedValue
            End With


            Try
                Dim SavingController As New InventorySellingController
                Dim VoucherNo = SavingController.SavingInventorySellingReceiveApproval(oCustomClass)
                
                Response.Redirect("InventorySellingReceiveAppr.aspx?msg=1")
            

            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
End Class