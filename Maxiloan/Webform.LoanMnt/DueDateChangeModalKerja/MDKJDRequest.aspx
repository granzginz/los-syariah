﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MDKJDRequest.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.MDKJDRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../Webform.UserController/UCReason.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DRequest</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
   <script language="javascript" type="text/javascript">
        $(document).ready(function () {
           
        });
        //function hitungUlang()
        //{
        //    var OSBungaString = document.getElementById("lblOsInterest").innerHTML;
        //    var bungaPercent = document.getElementById('ucEfRate_txtNumber').value;
        //    var AmountString = document.getElementById("lblOsPrinciple").innerHTML;

        //    var OSBunga = parseInt(OSBungaString.replace(/\s*,\s*/g, ''));
        //    var Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
        //    var jk = hitungJangkaWaktu();
            
        //    var dibiayai = Amount;
        //    var bunga = (dibiayai * bungaPercent / 100) * jk / 360;
        //    var bungaBaru = OSBunga + bunga

        //    $('#txtInterestPaid_txtNumber').val(number_format(bunga, 2));
        //    $('#lblOsInterestNew').html(number_format(bungaBaru, 2));
        //}

       function amountToBePaid() {
           let lcAmountStr = document.getElementById('lbLCInstallx').innerHTML;
           let lcAmount = parseInt(lcAmountStr.replace(/\s*,\s*/g, ''));
           console.log('Debug Total Yang Harus Dibayar : ',lcAmountStr)

           let adminFeeStr = document.getElementById('txtAdminFee_txtNumber').value;
           let adminFee = parseInt(adminFeeStr.replace(/\s*,\s*/g, ''));
          
           //let prepaidAmountStr = document.getElementById('lblPrepaidAmount').innerHTML;
           //let prepaidAmount = parseInt(prepaidAmountStr.replace(/\s*,\s*/g, ''));

            let amountToBepaid = (lcAmount + adminFee) //- prepaidAmount
            $('#totAmountpaid').html(number_format(amountToBepaid, 0));
          
       }

        function hitungJangkaWaktu() {
            var _tglCair = document.getElementById('txtDueDateAwal').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(tglCair)

            var _invoiceDueDate = document.getElementById('txtsdate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(invoiceDueDate)

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN PERPANJANGAN TGL JT MODAL KERJA
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            <label>
                Tgl JT Awal
            </label>
               <%-- <asp:Label ID="lblDueDateAwal" runat="server" CssClass="numberAlign regular_text"></asp:Label>--%>
                <asp:TextBox runat="server" ID="txtDueDateAwal" CssClass="small_text" ></asp:TextBox>
                 <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtsdate"
                     Format="dd/MM/yyyy">
                 </asp:CalendarExtender>
            </div>
            <div class="form_right">
            <label>
                Tgl JT Perpanjangan</label>
            <asp:TextBox runat="server" ID="txtsdate" CssClass="small_text" OnClientChange="hitungUlang()"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtsdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            <label>
                Tgl Pengajuan
            </label>
                <asp:TextBox runat="server" ID="txtEfdate" CssClass="small_text"  ></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtEfdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label>
                    Rate Margin
                </label>
                <uc1:ucnumberformat runat="server" id="ucEfRate" width="70" OnClientChange="hitungUlang()"></uc1:ucnumberformat> %
            </div>
        </div>
    </div>
    <div class="form_box_hide">
        <div>
            <div class="form_left">

            </div>
            <div class="form_right">
                <label>
                    Margin yg harus dibayar
                </label>
                <uc1:ucnumberformat id="txtInterestPaid" runat="server" />
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnCalculate" runat="server" CausesValidation="false" Text="Calculate"
            CssClass="small button blue"></asp:Button>
    </div>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h4>
                OS dan Denda
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Sisa Pokok (O/S Principal)
                </label>
                <asp:Label ID="lblOsPrinciple" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <%--Sisa Bunga (O/S Interest)--%>
                    Nilai Margin (Interest Amount)
                </label>
                <asp:Label ID="lblOsInterest" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Denda Keterlambatan Angsuran
                </label>
                <asp:Label ID="lbLCInstallx" runat="server" CssClass="numberAlign label">0.00</asp:Label>
            </div>
            <div class="form_right">
                <div class="form_box_hide">
                    <label>
                        Sisa Margin (O/S Interest) Baru
                    </label>
                    <asp:Label ID="lblOsInterestNew" runat="server" CssClass="numberAlign label"></asp:Label>
                </div>
            </div>
        </div>
    </div>
            <div class="form_title">
                <div class="form_single">
                    <asp:HiddenField runat="server" ID="hdfApplicationID" />
                    <h4>
                        VIEW AMORTISASI DATA AWAL</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InsSeqNo" HeaderText="Periode"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="DueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PrincipalAmount" HeaderText="Pokok" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Margin" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestRate" HeaderText="Eff Rate (%)" DataFormatString="{0:0,0} %"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="LateChargeAmount" HeaderText="Denda"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InstallmentPaidDate" HeaderText="Tgl Bayar" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PrincipalPaidAmount" HeaderText="Pokok Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestPaidAmount" HeaderText="Margin Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="LateChargePaidAmount" HeaderText="Denda Paid"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlAmor" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TABEL AMORTISASI BARU
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <%--<asp:DataGrid ID="dtg" runat="server" Visible="true" ShowFooter="true" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSeqNo"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DUEDATE" HeaderText="JATUH TEMPO">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ANGSURAN">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%# formatnumber(container.dataitem("InstallmentAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotInstallmentAmount" runat="server" align="center"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# formatnumber(container.dataitem("PrincipalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BUNGA">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%# formatnumber(container.dataitem("INTERESTAMOUNT"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblOSPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingPrincipal"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA BUNGA">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblOSInterest" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingInterest"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSInterestAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>--%>
                    <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                        CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="True" DataField="InsSeqNo" HeaderText="Periode"></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="DueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="PrincipalAmount" HeaderText="Pokok" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Margin" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="InterestRate" HeaderText="Eff Rate (%)" DataFormatString="{0:0,0} %"  ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="LateChargeAmount" HeaderText="Denda"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="InstallmentPaidDate" HeaderText="Tgl Bayar" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="PrincipalPaidAmount" HeaderText="Pokok Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="InterestPaidAmount" HeaderText="Margin Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="LateChargePaidAmount" HeaderText="Denda Paid"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Administrasi
            </label>
            <uc1:ucnumberformat id="txtAdminFee" runat="server" OnClientChange="amountToBePaid()" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Total Yang harus dibayar</label>
            <asp:Label ID="totAmountpaid" runat="server" CssClass="numberAlign regular_text"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Jumlah Prepaid</label>
            <asp:Label ID="lblPrepaidAmount" runat="server" CssClass="numberAlign regular_text"></asp:Label>
        </div>
    </div>
    <uc1:ucapprovalrequest id="oReq" runat="server">
                </uc1:ucapprovalrequest>
    <div class="form_button">
        <asp:Button ID="BtnCalculate2" runat="server" Text="Next" CssClass="small button green">
        </asp:Button>
        <asp:Button ID="BtnRequest" runat="server" Text="Request" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
