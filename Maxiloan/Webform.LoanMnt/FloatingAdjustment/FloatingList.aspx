﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FloatingList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FloatingList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FloatingList</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        Floating Adjustment - Review
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Review Date &lt;=</label>
                        <asp:TextBox ID="txtEffDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtEffDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtEffDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtEffDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            List of Agreement
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgreeRescheduling" runat="server" Width="100%" Visible="False"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PERFORM">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkview" runat="server">ADJUST</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="AGREEMENT NO.">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="LblBranchID" runat="server" Visible="False" Text='<%#Container.DataItem("BranchID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="LblApplicationId" runat="server" Visible="False" Text='<%#Container.DataItem("ApplicationId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAME">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblCustID" runat="Server" Text='<%#Container.DataItem("CustomerID")%>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Address" HeaderText="ADDRESS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbladdress" runat="Server" Text='<%#Container.DataItem("Address")%>'>&nbsp;&nbsp;
                                            </asp:Label>
                                            <asp:Label ID="lblcity" runat="Server" Text='<%#Container.DataItem("City")%>'>&nbsp;&nbsp;
                                            </asp:Label>
                                            <asp:Label ID="lblZipcode" runat="Server" Text='<%#Container.DataItem("ZipCode")%>'>&nbsp;&nbsp;
                                            </asp:Label>
                                            <asp:Label ID="lblkelurahan" runat="Server" Text='<%#Container.DataItem("Kelurahan")%>'>&nbsp;&nbsp;
                                            </asp:Label>
                                            <asp:Label ID="lblkecamatan" runat="Server" Text='<%#Container.DataItem("Kecamatan")%>'>&nbsp;&nbsp;
                                            </asp:Label>
                                            <asp:Label ID="lblrt" runat="Server" Text='<%#Container.DataItem("RT")%>'>&nbsp;&nbsp;
                                            </asp:Label>
                                            <asp:Label ID="lblrw" runat="Server" Text='<%#Container.DataItem("RW")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="FloatingNextReviewDate" SortExpression="Agreement.FloatingNextReviewDate"
                                        HeaderText="REVIEW DATE" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="INSTALLMENT">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
