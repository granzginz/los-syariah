﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FloatingAdjustReview.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FloatingAdjustReview" %>

<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FloatingAdjustReview</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN FLOATING ADJUSTMENT
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer</label>
                <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DATA SAAT INI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Periode Floating</label>
                <asp:Label ID="lblFloatingPeriod" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Dapat di Review Sampai</label>
                <asp:Label ID="lblCanbe" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Margin Effective</label>
                <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Margin Flat</label>
                <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Pola Pembayaran</label>
                <asp:Label ID="lblPaymentFreq" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Lama Angsuran</label>
                <asp:Label ID="lblNumofIns" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jangka Waktu</label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                A/R PER TANGGAL
                <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server">
                </uc1:ucfullprepayinfo>
    </div>
    <div class="form_box">
        <div class="form_single">
            <b>Total Sisa A/R
                <asp:Label ID="lblStopAccruedAmount" runat="server"></asp:Label></b>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                Adjustment Terakhir
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtg" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                    BorderWidth="0" AutoGenerateColumns="False" Visible="false">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="FloatingStatusDate" SortExpression="FloatingStatusDate"
                            HeaderText="TGL ADJUSTMENT" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="EffectiveDateFrom" SortExpression="EffectiveDateFrom"
                            HeaderText="JT DARI" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="EffectiveDateTo" SortExpression="EffectiveDateTo" HeaderText="JT SAMPAI"
                            DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="MARGIN EFFECTIVE">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblERate" runat="server" Text='<%# formatnumber(container.dataitem("EffectiveRate"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ANGSURAN">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%# formatnumber(container.dataitem("InstallmentAmount"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ADJUSTED OLEH">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblUsrUpd" runat="server" Text='<%#Container.dataitem("UsrUpd")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                FLOATING ADJUSTMENT MENJADI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Angsuran di Adjusted</label>
                <asp:Label ID="lblInstNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label class="label_req">
                    No Angsuran</label>                
                <uc1:ucNumberFormat ID="txtInstNo" runat="server" />                
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label class="label_req">
                    Periode Floating</label>
                <asp:DropDownList ID="cboFloatingPeriod" runat="server">
                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                    <asp:ListItem Value="Q">Quarterly</asp:ListItem>
                    <asp:ListItem Value="S">Semi Annualy </asp:ListItem>
                    <asp:ListItem Value="A">Annualy </asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server" Display="Dynamic"
                    ErrorMessage="Harap Pilih Periode Floating" ControlToValidate="cboFloatingPeriod"
                    InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label class="label_req">
                    Margin Effective Rate (%) Baru</label>                
                <uc1:ucNumberFormat ID="txtEffRate" runat="server" />               
            </div>
            <div class="form_right">
                <label>
                    Margin Flat / Year</label>
                <asp:Label ID="lblFRate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Jumlah Angsuran</label>
            <uc1:ucNumberFormat ID="txtInstallmentAmt" runat="server" />              
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonCalAmor" runat="server" CausesValidation="false" Text="Cal Amor"
            CssClass="small button blue"></asp:Button>&nbsp;
        <asp:Button ID="ButtonCalRate" runat="server" CausesValidation="false" Text="Cal Rate"
            CssClass="small button blue"></asp:Button>&nbsp;
        <asp:Button ID="ButtonPrint" runat="server" CausesValidation="false" Text="Print"
            CssClass="small button blue"></asp:Button>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
