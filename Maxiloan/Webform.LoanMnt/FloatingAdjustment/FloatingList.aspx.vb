﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class FloatingList
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("filterby"))
        End Get
        Set(ByVal Value As String)
            viewstate("filterby") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property SeqNo() As Integer
        Get
            Return CType(Viewstate("SeqNo"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("SeqNo") = Value
        End Set
    End Property
    Property AssetSeqno() As Integer
        Get
            Return CType(Viewstate("AssetSeqno"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqno") = Value
        End Set
    End Property
#End Region

#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New GeneralPagingController
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New DataUserControlController    
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "FLOATINGREV"
            pnlSearch.Visible = True
            pnlDatagrid.Visible = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                oSearchBy.ListData = "Agreement.AGREEMENTNO,Agreement No-Agreement.ApplicationID,Application ID-Customer.NAME,Customer Name"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGrid(ByVal SearchBy As String, ByVal sortBy As String)
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
            .SpName = "spFloatingReviewPaging"
        End With

        oCustomClass = oController.GetGeneralPaging(oCustomClass)

        recordCount = oCustomClass.TotalRecords
        DtgAgreeRescheduling.DataSource = oCustomClass.ListData
        Try
            DtgAgreeRescheduling.DataBind()
        Catch
            DtgAgreeRescheduling.CurrentPageIndex = 0
            DtgAgreeRescheduling.DataBind()
        End Try
        DtgAgreeRescheduling.Visible = True
        pnlDatagrid.Visible = True
        PagingFooter()
    End Sub
#End Region

#Region "linkTo"
    Function LinkToViewRequestNo(ByVal strStyle As String, ByVal strRequestNo As String) As String
        Return "javascript:OpenWinViewRequestNaNo('" & strStyle & "','" & strRequestNo & "')"
    End Function
#End Region



    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim strSearchBy As New StringBuilder
            Dim strfilterby As New StringBuilder
            pnlDatagrid.Visible = True            
            If oSearchBy.Text.Trim <> "" Then
                If Right(oSearchBy.Text.Trim, 1) = "%" Then
                    Me.SearchBy = oSearchBy.ValueID.Replace("'", "''") & " Like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
                Else
                    Me.SearchBy = oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
                End If
            End If
            If txtEffDate.Text.Trim <> "" And oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " And Agreement.FloatingNextReviewDate <= '" & ConvertDate2(txtEffDate.Text.Trim).ToString("yyyy/MM/dd") & "'"
            ElseIf txtEffDate.Text.Trim <> "" Then
                Me.SearchBy = " Agreement.FloatingNextReviewDate <= '" & ConvertDate2(txtEffDate.Text.Trim).ToString("yyyy/MM/dd") & "'"
            End If

            BindGrid(Me.SearchBy, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("FloatingList.aspx")
    End Sub

    Private Sub DtgAgreeRescheduling_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreeRescheduling.ItemDataBound
        Dim hyAssetSetReQno As HyperLink
        Dim HyAgreementNo As HyperLink
        Dim HyCustomerName As HyperLink
        Dim hyRequestNo As HyperLink
        Dim lblCustID As Label
        Dim lblApplicationId As Label
        Dim LblBranchID As Label
        Dim lblSeqNo As Label
        Dim lblView As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblView = CType(e.Item.FindControl("lnkview"), HyperLink)
            LblBranchID = CType(e.Item.FindControl("LblBranchID"), Label)
            HyCustomerName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)
            HyAgreementNo = CType(e.Item.FindControl("HyAgreementNo"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)

            Me.BranchID = LblBranchID.Text.Trim
            Me.ApplicationID = lblApplicationId.Text.Trim
            If lblView.Text.Trim.Length > 0 Then
                lblView.NavigateUrl = "FloatingAdjustReview.aspx?Branchid=" & LblBranchID.Text.Trim & "&applicationid=" & lblApplicationId.Text.Trim
            End If
            If HyAgreementNo.Text.Trim.Length > 0 Then
                HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
            If lblCustID.Text.Trim.Length > 0 Then
                HyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
        End If
    End Sub

    Private Sub DtgAgreeRescheduling_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgreeRescheduling.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub

End Class