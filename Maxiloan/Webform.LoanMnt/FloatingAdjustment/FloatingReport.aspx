﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FloatingReport.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FloatingReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FloatingReport</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server"/>
        <input id="hdnChildName" type="hidden" name="hdnSP" runat="server"/>    
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">           
                <h3>
                    LAPORAN FLOATING ADJUSTMENT
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        <div class="form_single">
                <label class="label_req">Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                ErrorMessage="Harap pilih Cabang" ControlToValidate="cboBranch" CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class="label_req">Period Floating</label>                
                <asp:TextBox ID="txtPODateFrom" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtPODateFrom_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtPODateFrom" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                ControlToValidate="txtPODateFrom" CssClass="validator_general"  Display="Dynamic"></asp:RequiredFieldValidator>

                &nbsp;S/D&nbsp;                
                <asp:TextBox ID="txtPODateTo" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtPODateTo_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtPODateTo" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="txtPODateTo" CssClass="validator_general"  Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class="label_req">Produk</label>
                <asp:DropDownList ID="CboProduct" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCboProduct" runat="server" InitialValue="0" Display="Dynamic"
                ErrorMessage="Harap Pilih Produk" ControlToValidate="CboProduct" CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>       
        <div class="form_button">
            <asp:Button ID="ButtonViewReport" runat="server"  Text="Print Preview" CssClass ="small button blue"
            Enabled="True"></asp:Button>
	    </div>      
    </ContentTemplate>
    </asp:UpdatePanel>          
    </form>
</body>
</html>
