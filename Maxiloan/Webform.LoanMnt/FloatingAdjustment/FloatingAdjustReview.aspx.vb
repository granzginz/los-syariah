﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class FloatingAdjustReview
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Protected WithEvents txtInstNo As ucNumberFormat
    Protected WithEvents txtEffRate As ucNumberFormat
    Protected WithEvents txtInstallmentAmt As ucNumberFormat

#Region "Property"
    Property FloatingNextReviewDate() As Date
        Get
            Return (CType(Viewstate("FloatingNextReviewDate"), Date))
        End Get
        Set(ByVal Value As Date)
            viewstate("FloatingNextReviewDate") = Value
        End Set
    End Property
    Property FloatingDueDate() As Date
        Get
            Return (CType(Viewstate("FloatingDueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            viewstate("FloatingDueDate") = Value
        End Set
    End Property
    Private Property newNo() As Integer
        Get
            Return (CType(Viewstate("newNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("newNo") = Value
        End Set
    End Property
    Private Property NewNum() As Integer
        Get
            Return (CType(Viewstate("NewNum"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewNum") = Value
        End Set
    End Property
    Property OldOutstandingPrincipal() As Double
        Get
            Return CDbl(viewstate("OldOutstandingPrincipal"))
        End Get
        Set(ByVal Value As Double)
            viewstate("OldOutstandingPrincipal") = Value
        End Set
    End Property
    Property EffectiveRateBehaviour() As String
        Get
            Return viewstate("EffectiveRateBehaviour").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("EffectiveRateBehaviour") = Value
        End Set
    End Property
    Private Property EffRate() As Integer
        Get
            Return (CType(Viewstate("EffRate"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("EffRate") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(Viewstate("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("SeqNo") = Value
        End Set
    End Property
    Private Property MaxSeqNo() As Integer
        Get
            Return (CType(Viewstate("MaxSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("MaxSeqNo") = Value
        End Set
    End Property
    Private Property NewNumInst() As Integer
        Get
            Return (CType(Viewstate("NewNumInst"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewNumInst") = Value
        End Set
    End Property
    Property Status() As Boolean
        Get
            Return CType(viewstate("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("Status") = Value
        End Set
    End Property
    Property FRate() As Double
        Get
            Return CDbl(viewstate("FRate"))
        End Get
        Set(ByVal Value As Double)
            viewstate("FRate") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(viewstate("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            viewstate("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(viewstate("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(viewstate("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            viewstate("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(viewstate("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            viewstate("RejectMinimumIncome") = Value
        End Set
    End Property
    Property ReschedulingFeeBehaviour() As String
        Get
            Return viewstate("ReschedulingFeeBehaviour").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ReschedulingFeeBehaviour") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return viewstate("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property

    Property CustName() As String
        Get
            Return viewstate("CustName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return viewstate("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Private Property AccruedAmount() As Double
        Get
            Return (CType(ViewState("AccruedAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AccruedAmount") = Value
        End Set
    End Property
    Private Property NewPrinciple() As Double
        Get
            Return (CType(ViewState("NewPrinciple"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NewPrinciple") = Value
        End Set
    End Property
    Private Property ReschedulingFee() As Double
        Get
            Return (CType(Viewstate("ReschedulingFee"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("ReschedulingFee") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(Viewstate("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(Viewstate("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property

    Private Property sdate() As String
        Get
            Return (CType(Viewstate("sdate"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(Viewstate("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(Viewstate("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(Viewstate("Product"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Product") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.FinancialData
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private oControllerResc As New DChangeController
    Private m_controller As New FinancialDataController
    Private m_controllerFinancial As New FinancialDataController
    Private m_controllerFloat As New FloatingController
    Private Entities As New Parameter.FinancialData
    Private j As Integer = 0
    Private RunRate As Double
    Private FlateRate As Double
    Private objrow As DataRow
    Private intLoop As Integer
    Private myDataTable As DataTable
    Private myDataColumn As DataColumn
    Private myDataRow As DataRow
    Private myDataSet As New DataSet
    Private dblInterestTotal As Double
    Private i As Integer
    Private PokokHutang() As Double
    Private Hutang() As Double
    Private PokokBunga() As Double
    Private Bunga() As Double
    Private HitungUlangSisa As Boolean = False
    Private HitungUlangKurang As Boolean = False
    Private SisaPrincipal As Double
    Private KurangPrincipal As Double
#End Region

#Region " Sub & Function "
    Private Sub CalculateRate()
        Dim Eff As Double
        Dim Supp As Double
        If txtInstNo.Text <> "" And txtEffRate.Text <> "" And txtInstallmentAmt.Text <> "" Then
            If CInt(txtInstNo.Text) > 1 And CInt(txtInstNo.Text) >= Me.SeqNo And CInt(txtInstNo.Text) <= Me.NewNumInst Then
                If CInt(txtInstallmentAmt.Text) <= 0 Then

                    ShowMessage(lblMessage, "Jumlah Angsuran harus > 0", True)
                    Exit Sub
                End If
                Try
                    GetFloatingPrincipal()
                    Eff = CountRate(Me.OldOutstandingPrincipal)
                    Context.Trace.Write("Eff = " & Eff)
                    txtEffRate.Text = Eff.ToString.Trim
                Catch ex As Exception

                    ShowMessage(lblMessage, "Jumlah Angsuran Salah", True)
                End Try
            Else

                ShowMessage(lblMessage, " No Angsuran > 1 dan >= " & Me.SeqNo & " dan <= " & Me.NewNumInst, True)
            End If
        End If
    End Sub
    Private Sub CalculateAmor()
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double
        If txtInstNo.Text <> "" And txtEffRate.Text <> "" Then
            If CInt(txtInstNo.Text) > 1 And CInt(txtInstNo.Text) >= Me.SeqNo And CInt(txtInstNo.Text) <= Me.NewNumInst Then
                If CInt(txtEffRate.Text) < 1 Then

                    ShowMessage(lblMessage, "Bunga Efective harus > 0", True)
                    Exit Sub
                End If
                If Me.InstallmentScheme = "RF" Then
                    Inst = InstAmt(CDbl(txtEffRate.Text))
                    txtInstallmentAmt.Text = Math.Round(Inst, 0).ToString.Trim
                    FlatRate()
                    GetFloatingPrincipal()
                    Try
                        With oEntities
                            .NTF = Me.OldOutstandingPrincipal
                            .NumOfInstallment = CInt(Me.MaxSeqNo)
                            .GracePeriod = 0
                        End With
                        oEntities = m_controllerFinancial.GetPrincipleAmount(oEntities)
                        Me.PrincipleAmount = oEntities.Principal
                    Catch ex As Exception

                        ShowMessage(lblMessage, "Jumlah Angsuran Salah", True)
                    End Try
                End If
            Else

                ShowMessage(lblMessage, " No Angsuran > 1 dan  " & Me.SeqNo & " dan <= " & Me.NewNumInst, True)
            End If
        End If
    End Sub
    Function CountRate(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer
        ' If Me.InstallmentScheme = "ST" And Me.StepUpDownType = "RL" Then
        'intJmlInst = CInt(txtCummulative.Text)
        'Else
        intJmlInst = CInt(Me.MaxSeqNo)
        'End If
        ReturnVal = m_controllerFinancial.GetEffectiveRateArr(intJmlInst, CDbl(txtInstallmentAmt.Text), NTF, Me.PaymentFrequency)
        Return ReturnVal
    End Function
    Private Sub FlatRate()
        GetFloatingPrincipal()
        FlateRate = 100 * ((CDbl(txtInstallmentAmt.Text) * CDbl(lblTenor.Text)) - Me.OldOutstandingPrincipal) / Me.OldOutstandingPrincipal
        lblFRate.Text = FormatNumber(FlateRate, 2) & "% per " & lblTenor.Text & " month "
        lblFRate.Visible = True
        Me.FRate = FlateRate
    End Sub
    Function InstAmt(ByVal Rate As Double) As Double
        Dim ReturnVal As Double
        GetFloatingPrincipal()
        RunRate = (Rate / (m_controllerFinancial.GetTerm(Me.PaymentFrequency) * 100))
        ReturnVal = Math.Round(m_controllerFinancial.GetInstAmtArr(RunRate, Me.PaymentFrequency, _
        CInt(Me.MaxSeqNo), Me.OldOutstandingPrincipal), 0)
        'ReturnVal = Math.Round(m_controllerFinancial.GetInstAmtArr(RunRate, Me.PaymentFrequency, _
        'CInt(Me.NewNumInst), Me.OldOutstandingPrincipal), 0)

        Return ReturnVal
    End Function
    Sub DoBind()
        Dim totalPrepayment As Double
        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        With oPaymentInfo
            '.IsTitle = True
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            .IsPenaltyTerminationShow = False
            .PaymentInfo()

            Me.AgreementNo = .AgreementNo
            Me.strCustomerid = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
            lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
            Me.AccruedAmount = totalPrepayment
        End With
        GetList()
    End Sub
    Sub GetList()
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controllerFloat.ReschedulingProduct(oCustomClass)
        With oCustomClass
            lblFloatingPeriod.Text = .txtSearch
            lblCanbe.Text = .FloatingNextReviewDate.ToString("dd/MM/yyyy")
            lblEffectiveRate.Text = FormatNumber(.EffectiveRate, 2) & "%"
            lblFlatRate.Text = FormatNumber(.FlatRate, 2) & "%"
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
            End Select
            lblNumofIns.Text = CStr(.NumOfInstallment)
            lblTenor.Text = CStr(.Tenor)
            lblInstNo.Text = CStr(.SeqNo) & " up to " & lblNumofIns.Text
            Me.SeqNo = .SeqNo
            Me.EffRate = .EffRate
            Me.EffectiveRateBehaviour = .EffectiveRateBehaviour
            txtEffRate.Text = FormatNumber(Me.EffRate, 0)
            Me.NewNumInst = .NumOfInstallment
            Me.InstallmentScheme = .InstallmentScheme
            Me.FloatingNextReviewDate = .FloatingNextReviewDate
            Me.PaymentFrequency = CInt(.PaymentFrequency)
        End With
    End Sub
    Sub GetFloatingPrincipal()
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.SeqNo = CInt(txtInstNo.Text)
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controllerFloat.GetFloatingData(oCustomClass)
        With oCustomClass
            Me.OldOutstandingPrincipal = .OutstandingPrincipalOld
            Me.MaxSeqNo = .NewNumInst
            Me.FloatingDueDate = .EffectiveDate
        End With
    End Sub
    Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
        Return (dblInterestTotal * 1200) / (dblNTF * CDbl(lblTenor.Text))
    End Function
    Public Sub BuatTable()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "DueDate"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Dim tempdate As Date
        Dim j As Integer = 0
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        Me.newNo = CInt(txtInstNo.Text) - 2
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = Me.newNo + i
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)

            'If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(Me.sdate)) >= 0 Then
            j = j + 1
            If j = 1 Then
                tempdate = Me.FloatingDueDate
            Else
                tempdate = DateAdd(DateInterval.Month, Me.PaymentFrequency, tempdate)
            End If
            myDataRow("DueDate") = tempdate.ToString("yyyyMMdd")
            'End If
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region

#Region "Event Handlers "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "FLOATINGREV"
        If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                With txtInstNo
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
                With txtEffRate
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = True
                    .TextCssClass = "numberAlign regular_text"
                    .RangeValidatorMinimumValue = "0"
                    .RangeValidatorMaximumValue = "100"
                End With
                With txtInstallmentAmt
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With

                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("BranchID")
                DoBind()
                Dim DtUserList As New DataTable
                Dim DvUserList As New DataView
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                End With
                oCustomClass = m_controllerFloat.LastAdjustment(oCustomClass)
                DtUserList = oCustomClass.ListData
                dtg.DataSource = DtUserList.DefaultView
                dtg.DataBind()
                If Me.EffectiveRateBehaviour = "L" Then txtEffRate.Enabled = False
            End If
        End If
    End Sub
    Private Sub imbCalAmor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCalAmor.Click
        If Me.EffectiveRateBehaviour = "X" Then
            If CDbl(txtEffRate.Text) <= Me.EffRate Then
                Call CalculateAmor()
            Else

                ShowMessage(lblMessage, "Bunga Effective harus <=" & Me.EffRate, True)
            End If
        ElseIf Me.EffectiveRateBehaviour = "N" Then
            If CDbl(txtEffRate.Text) >= Me.EffRate Then
                Call CalculateAmor()
            Else

                ShowMessage(lblMessage, "Bunga Effective harus >=" & Me.EffRate, True)
            End If
        Else
            Call CalculateAmor()
        End If

    End Sub
    Private Sub imbCalRate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCalRate.Click
        If Me.EffectiveRateBehaviour = "X" Then
            If CDbl(txtEffRate.Text) <= Me.EffRate Then
                Call CalculateRate()
            Else

                ShowMessage(lblMessage, "Bunga Effective harus <=" & Me.EffRate, True)
            End If
        ElseIf Me.EffectiveRateBehaviour = "N" Then
            If CDbl(txtEffRate.Text) >= Me.EffRate Then
                Call CalculateRate()
            Else

                ShowMessage(lblMessage, "Bunga Effective harus >=" & Me.EffRate, True)
            End If
        Else
            Call CalculateRate()
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim FlatRate As Double
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        BuatTable()
        GetFloatingPrincipal()
        'Me.NewNum = Me.NewNumInst - (CInt(txtInstNo.Text) - 1)
        If Me.InstallmentScheme = "RF" Then
            oFinancialData.MydataSet = MakeAmortTable2(CInt(Me.MaxSeqNo), CDbl(txtInstallmentAmt.Text), Me.OldOutstandingPrincipal, CDbl(txtEffRate.Text), Me.PaymentFrequency)
        End If
        If Me.InstallmentScheme = "RF" Then  'untuk regular  
            If HitungUlangSisa Then
                dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
            If HitungUlangKurang Then
                dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
        End If
        If Me.InstallmentScheme = "RF" Then  'untuk regular    
            FlatRate = Math.Round(GetFlatRate(dblInterestTotal, Me.OldOutstandingPrincipal, Me.PaymentFrequency, CInt(lblTenor.Text)), 5)
            oFinancialData.FlatRate = FlatRate
            oFinancialData.InstallmentAmount = CDbl(txtInstallmentAmt.Text)
            oFinancialData.NumOfInstallment = CInt(txtInstallmentAmt.Text)
        End If
        Try
            With oFinancialData
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .BusinessDate = Me.BusinessDate
                .ApplicationID = Me.ApplicationID
                .EffectiveRate = CDbl(txtEffRate.Text)
                .FlatRate = Me.FRate / (Me.NewNumInst / 12)
                .InstallmentAmount = CDbl(txtInstallmentAmt.Text)
                .FloatingPeriod = cboFloatingPeriod.SelectedItem.Value
                .NumOfInstallment = Me.NewNumInst
                .SeqNo = CInt(txtInstNo.Text)
            End With
            If Me.InstallmentScheme = "RF" Then
                oFinancialData = m_controllerFloat.SaveFloatingData(oFinancialData)
            End If
            Response.Redirect("FloatingList.aspx")
        Catch ex As Exception
        End Try
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Server.Transfer("FloatingList.aspx")
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        'Print is disabled temporary
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            'If txtInstallmentAmt.Text <> "" And txtEffRate.Text <> "" And txtInstNo.Text <> "" Then
            '    Dim objreport As rptFloatingPrintTrial = New rptFloatingPrintTrial


            '    With Entities
            '        .strConnection = GetConnectionString
            '        .BranchId = Me.BranchID
            '        .ApplicationID = Me.ApplicationID
            '        .BusinessDate = Me.BusinessDate
            '        .NumOfInstallment = Me.NewNumInst
            '        .FloatingPeriod = cboFloatingPeriod.SelectedItem.Text
            '        .EffectiveRate = CDbl(txtEffRate.Text)
            '        .FlatRate = Me.FRate
            '        .InstallmentAmount = CDbl(txtInstallmentAmt.Text)
            '        .SeqNo = CInt(txtInstNo.Text)
            '    End With

            '    Dim oData As New DataSet
            '    oData = m_controllerFloat.FloatingPrintTrial(Entities)

            '    objreport.SetDataSource(oData)

            '    Dim discrete As ParameterDiscreteValue
            '    Dim ParamField As ParameterFieldDefinition
            '    Dim CurrentValue As ParameterValues

            '    ParamField = objreport.DataDefinition.ParameterFields("LoginID")
            '    discrete = New ParameterDiscreteValue
            '    discrete.Value = Me.Loginid
            '    CurrentValue = New ParameterValues
            '    CurrentValue = ParamField.DefaultValues
            '    CurrentValue.Add(discrete)
            '    ParamField.ApplyCurrentValues(CurrentValue)

            '    ParamField = objreport.DataDefinition.ParameterFields("BusinessDate1")
            '    discrete = New ParameterDiscreteValue
            '    discrete.Value = Me.BusinessDate
            '    CurrentValue = New ParameterValues
            '    CurrentValue = ParamField.DefaultValues
            '    CurrentValue.Add(discrete)
            '    ParamField.ApplyCurrentValues(CurrentValue)

            '    Dim strHTTPServer As String
            '    Dim StrHTTPApp As String
            '    Dim strNameServer As String
            '    Dim strFileLocation As String
            '    Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            '    objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            '    objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            '    strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            '    strFileLocation += Me.Session.SessionID & Me.Loginid & "PrintTrialCalculation.pdf"
            '    DiskOpts.DiskFileName = strFileLocation
            '    objreport.ExportOptions.DestinationOptions = DiskOpts
            '    objreport.Export()

            '    strHTTPServer = Request.ServerVariables("PATH_INFO")
            '    strNameServer = Request.ServerVariables("SERVER_NAME")
            '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            '    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PrintTrialCalculation.pdf"
            '    Response.Write("<script language = javascript>" & vbCrLf _
            '    & "window.open('" & strFileLocation & "','PrintTrialCalculation', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
            '    & "</script>")
            'End If
        End If
    End Sub
#End Region

End Class