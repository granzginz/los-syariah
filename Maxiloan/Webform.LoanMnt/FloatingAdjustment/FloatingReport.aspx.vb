﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class FloatingReport
    Inherits Maxiloan.Webform.WebBased

#Region "Deklarasi dan constanta"    
    Private m_controller As New DataUserControlController
    Private ocontroller As New ReschedulingController
    Private oCustomclass As New Parameter.FinancialData
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "FLOATINGREPORT"

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not Page.IsPostBack Then
                Dim dtbranch As New DataTable
                Dim dtAssetType As New DataTable

                txtPODateFrom.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtPODateTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                dtAssetType = m_controller.GetAssetType(GetConnectionString)
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End If
                End With
                Dim dtAgency As New DataTable
                Dim row As DataRow
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .BranchId = Replace(Me.sesBranchId, "'", "")

                End With
                dtAgency = ocontroller.ReschedulingProduct(oCustomclass.strConnection, Replace(Me.sesBranchId, "'", ""))
                With CboProduct
                    .DataSource = dtAgency.DefaultView
                    .DataValueField = "ProductID"
                    .DataTextField = "Description"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub

    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("COOKIES_REPORT")
        Me.SearchBy = " FloatingHistory.BranchID = '" & cboBranch.SelectedItem.Value & "' And FloatingHistory.FloatingNextReviewDate between  '" & ConvertDate2(txtPODateFrom.Text.Trim).ToString("yyyyMMdd") & "' And  '" & ConvertDate2(txtPODateTo.Text.Trim).ToString("yyyyMMdd") & "'"
        Me.SearchBy = Me.SearchBy & " And ProductBranch.ProductID = '" & CboProduct.SelectedItem.Value & "'"
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("PODateFrom") = ConvertDate2(txtPODateFrom.Text.Trim).ToString("dd/MM/yyyy")
                    cookie.Values("PODateTo") = ConvertDate2(txtPODateTo.Text.Trim).ToString("dd/MM/yyyy")
                    cookie.Values("BranchID") = cboBranch.SelectedItem.Value
                    cookie.Values("BranchName") = cboBranch.SelectedItem.Text
                    cookie.Values("Product") = CboProduct.SelectedItem.Value
                    cookie.Values("SearchBy") = Me.SearchBy
                    'this is dynamis cookie,it's value return the name of crystal report file we want to open 
                    cookie.Values("ReportType") = "rptFloatingReportrpt"
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("COOKIES_REPORT")
                    cookieNew.Values.Add("PODateFrom", txtPODateFrom.Text.Trim)
                    cookieNew.Values.Add("PODateTo", txtPODateTo.Text.Trim)
                    cookieNew.Values.Add("BranchID", cboBranch.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboBranch.SelectedItem.Text)
                    cookieNew.Values.Add("Product", CboProduct.SelectedItem.Text)
                    cookieNew.Values.Add("SearchBy", Me.SearchBy)
                    cookieNew.Values.Add("ReportType", "rptFloatingReportrpt")
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("FloatingReportViewer.aspx")
            End If
        End If
    End Sub

End Class