﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class FloatingInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("filterby"))
        End Get
        Set(ByVal Value As String)
            viewstate("filterby") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property SeqNo() As Integer
        Get
            Return CType(Viewstate("SeqNo"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("SeqNo") = Value
        End Set
    End Property
    Property AssetSeqno() As Integer
        Get
            Return CType(Viewstate("AssetSeqno"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqno") = Value
        End Set
    End Property
#End Region

#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New GeneralPagingController
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New DataUserControlController    
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "FLOATINGINQ"
            pnlSearch.Visible = True
            pnlDatagrid.Visible = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGrid(ByVal SearchBy As String, ByVal sortBy As String)
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
            .SpName = "spFloatinglInquiryPaging"
        End With

        oCustomClass = oController.GetGeneralPaging(oCustomClass)

        recordCount = oCustomClass.TotalRecords
        DtgAgreeRescheduling.DataSource = oCustomClass.ListData
        Try
            DtgAgreeRescheduling.DataBind()
        Catch
            DtgAgreeRescheduling.CurrentPageIndex = 0
            DtgAgreeRescheduling.DataBind()
        End Try
        DtgAgreeRescheduling.Visible = True
        pnlDatagrid.Visible = True
        PagingFooter()
    End Sub
#End Region




    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        Dim cookie As HttpCookie = Request.Cookies("COOKIES_FLOATING")
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("SearchBy") = Me.SearchBy
                    cookie.Values("LoginID") = Me.Loginid
                    cookie.Values("FilterBy") = Me.FilterBy
                    cookie.Values("BranchName") = cboParent.SelectedItem.Text.Trim
                    cookie.Values("ReportType") = "rptFloatingInquiry"
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("COOKIES_FLOATING")
                    cookieNew.Values.Add("SearchBy", Me.SearchBy)
                    cookieNew.Values.Add("LoginID", Me.Loginid)
                    cookieNew.Values.Add("FilterBy", Me.FilterBy)
                    cookieNew.Values.Add("BranchName", cboParent.SelectedItem.Text.Trim)
                    cookieNew.Values.Add("ReportType", "rptFloatingInquiry")
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("FloatingInqViewer.aspx")
            End If
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim strSearchBy As New StringBuilder
            Dim strfilterby As New StringBuilder
            pnlDatagrid.Visible = True
            If cboSearch.SelectedItem.Value.Trim <> "0" Then
                If txtSearch.Text.Trim <> "" Then
                    If cboSearch.SelectedItem.Value.Trim = "1" Then
                        If Right(txtSearch.Text.Trim, 1) = "%" Then
                            strSearchBy.Append("Agreement.AgreementNo like '" & txtSearch.Text.Trim & "' and ")
                        Else
                            strSearchBy.Append("Agreement.AgreementNo = '" & txtSearch.Text.Trim & "' and ")
                        End If
                        strfilterby.Append("Agreement No. = " & txtSearch.Text.Trim & " and ")
                    ElseIf cboSearch.SelectedItem.Value.Trim = "2" Then
                        If Not IsNumeric(txtSearch.Text.Trim) Then
                            If Right(txtSearch.Text.Trim, 1) = "%" Then
                                strSearchBy.Append("Customer.Name like '" & txtSearch.Text.Trim & "' and ")
                            Else
                                strSearchBy.Append("Customer.Name = '" & txtSearch.Text.Trim & "' and ")
                            End If
                            strfilterby.Append("Customer Name = " & txtSearch.Text.Trim & " and ")
                        End If
                    End If
                End If
            End If
            If txtEffDate.Text.Trim <> "" Then
                strSearchBy.Append("FloatingHistory.FloatingNextReviewDate <= '" & ConvertDate2(txtEffDate.Text.Trim).ToString("yyyyMMdd") & "' and ")
                strfilterby.Append("Effective Date = " & txtEffDate.Text.Trim & " and ")
            End If
            If CboStatus.SelectedItem.Value.Trim <> "0" Then
                strSearchBy.Append("FloatingHistory.FloatingStatus = '" & CboStatus.SelectedItem.Value.Trim & "' and ")
                strfilterby.Append("Status = " & CboStatus.SelectedItem.Text.Trim & " and ")
            End If
            If cboParent.SelectedItem.Value.Trim <> "ALL" Then
                strSearchBy.Append("FloatingHistory.branchID = '" & cboParent.SelectedItem.Value.Trim & "'")
                'Else
                '    If strSearchBy.ToString.Trim <> "" Then
                '        strSearchBy = Left(SearchBy, Len(SearchBy.Trim) - 4)
                '    End If
            End If
            Me.SearchBy = strSearchBy.ToString
            'If strfilterby.ToString <> "" Then
            '    filterby = Left(filterby, Len(filterby.Trim) - 4)
            'End If
            Me.FilterBy = strSearchBy.ToString
            BindGrid(Me.SearchBy, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If

    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("FloatingInq.aspx")
    End Sub

    Private Sub DtgAgreeRescheduling_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreeRescheduling.ItemDataBound
        Dim hyAssetSetReQno As HyperLink
        Dim HyAgreementNo As HyperLink
        Dim HyCustomerName As HyperLink
        Dim hyRequestNo As HyperLink
        Dim lblCustID As Label
        Dim lblApplicationId As Label
        Dim LblBranchID As Label
        Dim lblSeqNo As Label
        Dim lblView As HyperLink
        If e.Item.ItemIndex >= 0 Then

            LblBranchID = CType(e.Item.FindControl("LblBranchID"), Label)
            HyCustomerName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)
            HyAgreementNo = CType(e.Item.FindControl("HyAgreementNo"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)


            Me.BranchID = LblBranchID.Text.Trim
            Me.ApplicationID = lblApplicationId.Text.Trim


            If HyAgreementNo.Text.Trim.Length > 0 Then
                HyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
            If lblCustID.Text.Trim.Length > 0 Then
                HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub

    Private Sub DtgAgreeRescheduling_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgreeRescheduling.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub

End Class