﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SuspendAllocationGeneral
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccountID As UcBankAccountID
    Protected WithEvents ucLookUpCOA As ucLookUpCOA
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtJumlah As ucNumberFormat

    Public Property jumlah As Decimal
        Set(ByVal value As Decimal)
            ViewState("jumlah") = value
        End Set
        Get
            Return ViewState("jumlah")
        End Get
    End Property

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendAllocationGeneral
    Private oController As New SuspendAllocationGeneralController

#End Region



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            With txtAmountFrom
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With
            With txtamountto
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With
            With txtJumlah
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With

            Me.FormID = "SUSPENDALLGEN"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                UcBankAccountID.BankPurpose = ""
                UcBankAccountID.BankType = ""
                UcBankAccountID.IsAll = True
                UcBankAccountID.BindBankAccount()
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("SuspendNo") <> "" Then
                    bindPanelEdit()
                End If
                BindComboCabang()
                BindComboDepartemen()
            End If
        End If

    End Sub

    Sub bindPanelEdit()
        ucLookUpCOA.Visible = False
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        pnlDatagrid.Visible = False
        hdnSuspendNo.Value = Request("SuspendNo")
        oCustomClass.SuspendNo = Request("SuspendNo")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetSuspendAllocationGeneralList(oCustomClass)
        imbCancel.CausesValidation = False
        Me.jumlah = oCustomClass.Amount
        txtJumlah.Text = FormatNumber(oCustomClass.Amount.ToString)
        txtJumlah.Enabled = False
        'txtJumlah.Text = oCustomClass.Departemen
        txtTransaction.Attributes.Add("readonly", "true")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        oCustomClass = New Parameter.SuspendAllocationGeneral

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.GetSuspendAllocationGeneral(oCustomClass)

        DtUserList = oCustomClass.Listdata
        recordCount = oCustomClass.TotalRecord
        DtgAgree.DataSource = DtUserList.DefaultView
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
    End Sub

    Sub BindComboCabang()
        oCustomClass = New Parameter.SuspendAllocationGeneral
        Dim tbl As New DataTable
        oCustomClass.strConnection = GetConnectionString()
        tbl = oController.GetComboCabang(oCustomClass)

        cboCabang.DataTextField = "BranchFullName"
        cboCabang.DataValueField = "BranchID"
        cboCabang.DataSource = tbl
        cboCabang.DataBind()

        cboCabang.SelectedIndex = cboCabang.Items.IndexOf(cboCabang.Items.FindByValue(Me.sesBranchId.Replace("'", "")))
    End Sub

    Sub BindComboDepartemen()
        oCustomClass = New Parameter.SuspendAllocationGeneral
        Dim tbl As New DataTable
        oCustomClass.strConnection = GetConnectionString()
        tbl = oController.GetComboDepartemen(oCustomClass)

        cboDepartemen.DataTextField = "DepartementName"
        cboDepartemen.DataValueField = "DepartementId"
        cboDepartemen.DataSource = tbl
        cboDepartemen.DataBind()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            lblMessage.Visible = False
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim Nlblsuspendno As HyperLink
        Dim HyReverse As HyperLink
        If e.Item.ItemIndex >= 0 Then
            Nlblsuspendno = CType(e.Item.FindControl("lblsuspendno"), HyperLink)
            HyReverse = CType(e.Item.FindControl("HyReverse"), HyperLink)

            HyReverse.NavigateUrl = "SuspendAllocationGeneral.aspx?SuspendNo=" & Nlblsuspendno.Text.Trim

        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        pnlAddEdit.Visible = False
        pnlDatagrid.Visible = False
        pnlList.Visible = True
        lblMessage.Visible = False
    End Sub
    Sub bindGrid()
        Me.SearchBy = " BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and "
        If UcBankAccountID.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " BankAccountID = '" & UcBankAccountID.BankAccountID.Trim & "' and "
        End If

        If txtDate.Text <> "" Then
            'Me.SearchBy = Me.SearchBy & " ValueDate ='" & ConvertDate2(txtDate.Text) & "' and "
            Me.SearchBy = Me.SearchBy & " ValueDate <='" & ConvertDate2(txtDate.Text) & "' and "
        End If

        If txtdesc.Text.Trim <> "" Then
            If Right(txtdesc.Text.Trim, 1) = "%" Then
                Me.SearchBy = Me.SearchBy & " Description Like '" & txtdesc.Text.Trim & "'  and "
            Else
                Me.SearchBy = Me.SearchBy & " Description ='" & txtdesc.Text.Trim & "' and "
            End If

        End If
        If txtRefNo.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " ReferenceNo ='" & txtRefNo.Text.Trim & "' and "
        End If

        If txtAmountFrom.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " Amount >= '" & CDbl(txtAmountFrom.Text.Trim) & "' and "
        End If

        If txtamountto.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " Amount <= '" & CDbl(txtamountto.Text.Trim) & "' and "
        End If

        If Me.SearchBy.Trim <> "" Then
            Me.SearchBy = Left(Me.SearchBy, Len(Me.SearchBy) - 4)
        Else
            Me.SearchBy = ""
        End If
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        Me.SortBy = " SuspendNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            lblMessage.Visible = False
            bindGrid()
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub    
    Protected Sub btnLookupTransaction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupTransaction.Click
        ucLookUpCOA.Visible = True
        ucLookUpCOA.CmdWhere = "All"
        ucLookUpCOA.Sort = "PaymentAllocationID ASC"
        ucLookUpCOA.Popup()
    End Sub
    Public Sub CatSelectedTransaction(ByVal PaymentAllocationID As String,
                                           ByVal Description As String,
                                           ByVal COA As String)
        txtTransaction.Text = Description
        hdnTransaction.Value = PaymentAllocationID
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        oCustomClass = New Parameter.SuspendAllocationGeneral
        Dim ErrMessage As String = ""

        With oCustomClass
            .SuspendNo = hdnSuspendNo.Value.Trim
            .PaymentAllocationID = hdnTransaction.Value.Trim
            .BranchId = cboCabang.SelectedItem.Value
            .Departemen = cboDepartemen.SelectedItem.Value
            .Amount = Me.jumlah
            .Description = txtKeteranganAlokasi.Text.Trim
            .strConnection = GetConnectionString()
            .BusinessDate = Me.BusinessDate
        End With

        oController.SuspendAllocationGeneralSaveEdit(oCustomClass)

        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        bindGrid()
        pnlAddEdit.Visible = False
        pnlList.Visible = True
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("SuspendAllocationGeneral.aspx")
    End Sub
End Class