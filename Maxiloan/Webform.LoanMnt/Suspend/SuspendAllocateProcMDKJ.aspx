﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendAllocateProcMDKJ.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendAllocateProcMDKJ" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucpaymentallocationdetail" Src="../../Webform.UserController/ucpaymentallocationdetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SuspendAllocateProc</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		<!--
        function flyto(strParam, strOpt, strType) {
            switch (strOpt) {
                case "customer":
                    {
                        if (strType == 'C')
                            var a = window.open("../../../../SmartSearch/viewsearchcompany.aspx?custid=" + strParam, "customer", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        else
                            var a = window.open("../../../../SmartSearch/viewsearch.aspx?custid=" + strParam, "customer", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        a.focus();
                        break;
                    }
                case "agreement":
                    {
                        var a = window.open("../../../../SmartSearch/peragreement.aspx?applicationid=" + strParam, "agreement", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        a.focus();
                        break;
                    }
            }
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak Berhak');
            }
        }
        document.onmousedown = click
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(SuspendNo, branchid) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Suspend/SuspendInqView.aspx?SuspendNo=' + SuspendNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
		//-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
            onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    ALOKASI SUSPEND MDKJ
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Suspend
                </label>
                <asp:HyperLink ID="lblSuspendNO" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Posting
                </label>
                <asp:Label ID="lblPostingdate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Saldo Angsuran
                </label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Saldo Prepaid
                </label>
                <asp:Label ID="lblPrepaid" runat="server"></asp:Label>
            </div>
        </div>        
       <%--<div class="form_box_header">
            <div class="form_single">
                <h5>
                    TABEL ANGSURAN
                </h5>
            </div>
        </div>
        <uc1:ucinstallmentschedule id="oInstallmentSchedule" runat="server" />--%>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">              
                <label>
                    Cara Pembayaran
                </label>
                <asp:Label ID="lblWOP" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas
                </label>
                <asp:Label ID="lblRefNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Rekening Bank
                </label>
                <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Valuta
                </label>               
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Diterima
                </label>
                <asp:Label ID="lblAmountRec" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Keterangan
                </label>
                <asp:TextBox ID="txtnotes" runat="server" Width="365px" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>Tanggal Bayar</label>
                <uc1:ucdatece id="oValueDate" runat="server" /> 
            </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Tanggal Invoice</label>
                    <uc1:ucdatece ID="oInvoiceDate" runat="server" Enabled="false"/>               
                </div>
                <div class="form_right">    
                <label>Tanggal JT Invoice</label>
                 <uc1:ucdatece ID="oInvoiceDueDate" runat="server" Enabled="false"/>
            </div>
        </div>        
        <asp:Panel ID="pnlPaymentDetail" runat="server">
            <%--<div class="form_box_uc">
                <uc1:ucpaymentallocationdetail id="oPaymentAllocationDetail" runat="server"></uc1:ucpaymentallocationdetail>
            </div>--%>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">No Referensi</label>
                    <asp:TextBox ID="NoRefrensi" runat="server" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNoRefrensi" runat="server" ControlToValidate="NoRefrensi" 
                        CssClass="validator_general" Display="Dynamic" ErrorMessage="Input No Referensi"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label>No Invoice</label>
                    <asp:Label ID="lblInvoiceNo" runat="server"  ></asp:Label> 
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Total Bayar</label>
                    <uc1:UcNumberFormat id="jumlahBayar" runat="server" OnChange="allocatePayment(this.value)" /> <%--OnChange="CekNilaiAngsuran(this.value);calculateTotal();"/>--%>
                </div>
                <div class="form_right">
                    <label>Nilai Invoice</label>
                    <asp:Label id ="lblInvoiceAmount" runat="server" CssClass="numberAlign3" ></asp:Label> 
                    <%--<uc1:UcNumberFormat id="ucInvoiceAmount" runat="server" />--%>
                </div>
            </div>
            <div class="form_box" >
                <div class="form_left">
                    <label>Bayar Pokok</label>
                    <uc1:UcNumberFormat id="principalPaid" runat="server" OnChange="hitungTotal()" />
                </div>
                <div class="form_right"  style="background-color:#ffd800">
                    <label>Pokok Invoice</label>
                    <asp:Label id="lblTotalPembiayaan" runat="server" CssClass="numberAlign3" ></asp:Label> 
                    <%--<uc1:UcNumberFormat id="ucTotalPembiayaan" runat="server" />--%>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Bayar Margin</label>
                    <uc1:UcNumberFormat id="interestPaid" runat="server" OnChange="hitungTotal()"  />
                </div>
                <div class="form_right" style="background-color:#ffd800">
                    <label>Margin Invoice</label>
                    <asp:Label ID="lblInterestAmount" runat="server"  CssClass="numberAlign3"></asp:Label> 
                    <%--<uc1:UcNumberFormat id="ucInterestAmount" runat="server" />--%>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Bayar Denda</label>
                    <uc1:ucNumberFormat ID="DendaPaid" runat="server" OnChange="hitungTotal()" /> 
                </div>
                <div class="form_right" style="background-color:#ffd800">
                    <label>Denda</label>
                    <asp:label ID="lblDendaAmount" runat="server"  CssClass="numberAlign3" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Bayar Retensi</label>
                    <uc1:UcNumberFormat id="RetensiPaid" runat="server" OnChange="hitungTotal()" />
                </div>
                <div class="form_right" style="background-color:#ffd800">
                    <label>Retensi Invoice</label>
                    <asp:Label ID="lblRetensiAmount" runat="server"  CssClass="numberAlign3"></asp:Label>  
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Total pembayaran</label>
                    <asp:Label ID="lbltotalbayar" runat="server" ></asp:Label>
                </div>
                <div class="form_right">
                    <label>Total Tagihan</label>
                    <asp:Label ID="lbltotalTagihan" runat="server"  CssClass="numberAlign3"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtn" runat="server">
            <div class="form_button">
                <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentInfo" Visible="false" runat="server">
        <div class="form_box_uc">
            <uc1:ucpaymentinfo id="oPaymentInfo" runat="server"></uc1:ucpaymentinfo>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
