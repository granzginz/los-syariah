﻿#Region "Import"
Imports System.IO
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports CrystalDecisions.Web
#End Region

Public Class PrepaidAllocateProc
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oPaymentinfo As UcPaymentInfo
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule
    Protected WithEvents txtValueDate As ucDateCE

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive


    Private oCustomClass2 As New Parameter.InstallRcv
    Private oController2 As New AgreementListController
    Private oController3 As New InstallRcvController
#End Region

#Region "Property"


    Private Property AmountReceive() As Double
        Get
            Return (CType(Viewstate("AmountReceive"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("AmountReceive") = Value
        End Set
    End Property

    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property

    Private Property InstallmentAmount() As Double
        Get
            Return (CType(ViewState("InstallmentAmount"), Double))
        End Get
        Set(value As Double)
            ViewState("InstallmentAmount") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "PREPAIDALLOCATION"
        If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                Dim lblgiro As New Label
                Dim lblnewStat As New Label
                Dim inFlagfile As New Label

                Me.BranchID = Request.QueryString("branchid")
                Me.AgreementNo = Request.QueryString("agreementno")
                Me.ApplicationID = Request.QueryString("applicationid")

                DoBind()

                IsiKolomTagihan()

                txtValueDate.IsRequired = True
                'load grid angsuran
                oInstallmentSchedule.ApplicationId = Me.ApplicationID
                oInstallmentSchedule.ValueDate = Me.BusinessDate
                oInstallmentSchedule.DoBind_Angsuran(String.Empty)
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        Dim oControllerInfo As New UCPaymentInfoController
        Dim oCustomClassInfo As New Parameter.AccMntBase

        'Diganti karena pusat dibuka semua
        oCustomClass.BranchId = Replace(Me.sesBranchId, "'", "")

        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)

        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            lblPrepaid.Text = FormatNumber(.Prepaid, 0)
            lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 0)
            Me.InstallmentAmount = .InstallmentAmount
            Me.NextInstallmentDate = .NextInstallmentDate
            txtValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            Me.ValueDate = ConvertDate2(txtValueDate.Text)
            lblWOP.Text = "Prepaid"
            lblTitipanAngsuran.Text = FormatNumber(.TitipanAngsuran, 0)
            lblPrepaidStatus.Text = .PrepaidHoldStatusDesc
            Me.AmountReceive = FormatNumber(.Prepaid, 0)
            oPaymentAllocationDetail.ToleransiBayarKurang = .ToleransiBayarKurang
            oPaymentAllocationDetail.ToleransiBayarLebih = .ToleransiBayarLebih
        End With

        With oPaymentInfo
            .ValueDate = ConvertDate2(txtValueDate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        With oPaymentAllocationDetail
            .ShowPanelCashier = False
            .totalBayar = Me.AmountReceive
            .MaximumInstallment = oPaymentinfo.MaximumInstallment
            .MaximumLCInstallFee = oPaymentinfo.MaximumLCInstallFee
            .MaximumInstallCollFee = oPaymentinfo.MaximumInstallCollFee
            .MaximumInsurance = oPaymentinfo.MaximumInsurance
            .MaximumLCInsuranceFee = oPaymentinfo.MaximumLCInsuranceFee
            .MaximumInsuranceCollFee = oPaymentinfo.MaximumInsuranceCollFee

            .MaximumPDCBounceFee = oPaymentinfo.MaximumPDCBounceFee
            .MaximumSTNKRenewalFee = oPaymentinfo.MaximumSTNKRenewalFee
            .MaximumInsuranceClaimFee = oPaymentinfo.MaximumInsuranceClaimFee
            .MaximumReposessionFee = oPaymentinfo.MaximumReposessionFee

            .DisableForPrepaidAllocation = True
            .PaymentAllocationBind()
        End With
    End Sub
#End Region

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbSave.Click


        Dim TotalAllocation As Double

            With oPaymentAllocationDetail
                TotalAllocation = .InstallmentDue + .LCInstallment + .InstallmentCollFee + _
                                  .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                                  .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                                  .RepossessionFee + .Prepaid + .PLL + .TitipanAngsuran

            End With
        If TotalAllocation > CDbl(lblPrepaid.Text) Then

            ShowMessage(lblMessage, "Total Alokasi tidak boleh lebih besar dari Saldo Prepaid", True)
            Exit Sub
        End If

            With oCustomClass2
                .strConnection = GetConnectionString()
                .ValueDate = Me.ValueDate
                .CoyID = Me.SesCompanyID

                'diganti karena branch pusat dibuka semua
                .BranchId = Me.sesBranchId.Replace("'", "")

                .BusinessDate = Me.BusinessDate
                .ReceivedFrom = "-"
            .WOP = "PP"
            .LoginId = Me.Loginid
                .BankAccountID = Me.BankAccount
                .Notes = txtnotes.Text.Trim
                .ApplicationID = Me.ApplicationID
                '-----------Installment
                .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                .LcInstallment = oPaymentAllocationDetail.LCInstallment
                .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                '------------------------

                '----------Insurance
                .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                .LcInsurance = oPaymentAllocationDetail.LCInsurance
                .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                '-----------------------------

                .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                .Prepaid = oPaymentAllocationDetail.Prepaid
                .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim

                .PLL = oPaymentAllocationDetail.PLL
                .PLLDesc = oPaymentAllocationDetail.PLLDesc

                .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc

            .AmountReceive = TotalAllocation
            .BankAccountID = ""
            .ReferenceNo = ""
            End With
            Try
                oController2.PrepaidAllocation(oCustomClass2)
                Server.Transfer("PrepaidAllocate.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try

    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Server.Transfer("PrepaidAllocate.aspx")
    End Sub
    Private Sub IsiKolomTagihan()
        With oPaymentAllocationDetail
            .InstallmentDue0 = oPaymentinfo.InstallmentDue
            .LCInstallment0 = oPaymentInfo.MaximumLCInstallFee
            .InstallmentCollFee0 = oPaymentInfo.MaximumInstallCollFee
            .InsuranceDue0 = oPaymentInfo.MaximumInsurance
            .LCInsurance0 = oPaymentInfo.MaximumLCInsuranceFee
            .InsuranceCollFee0 = oPaymentInfo.MaximumInsuranceCollFee
            .PDCBounceFee0 = oPaymentInfo.MaximumPDCBounceFee
            .STNKRenewalFee0 = oPaymentInfo.MaximumSTNKRenewalFee
            .InsuranceClaimExpense0 = oPaymentInfo.MaximumInsuranceClaimFee
            .RepossessionFee0 = oPaymentInfo.MaximumReposessionFee
            .Prepaid0 = oPaymentInfo.Prepaid
            .PLL0 = 0

            .TitipAngsuran0 = 0
            .TitipanAngsuran = 0

            .totalTagihan = .InstallmentDue0 + .LCInstallment0 + .InstallmentCollFee0 + .InsuranceDue0 + _
                   .LCInsurance0 + .InsuranceCollFee0 + .PDCBounceFee0 + .STNKRenewalFee0 + .InsuranceClaimExpense0 + _
                   .RepossessionFee0 + .Prepaid0 + .PLL0


            .InstallmentDue = "0" 'Me.AmountReceive ' oPaymentInfo.InstallmentDue  
            .LCInstallment = oPaymentInfo.MaximumLCInstallFee
            .InstallmentCollFee = "0" ' oPaymentInfo.MaximumInstallCollFee
            .InsuranceDue = "0" ' oPaymentInfo.MaximumInsurance
            .LCInsurance = "0" ' oPaymentInfo.MaximumLCInsuranceFee
            .InsuranceCollFee = "0" ' oPaymentInfo.MaximumInsuranceCollFee
            .PDCBounceFee = "0" ' oPaymentInfo.MaximumPDCBounceFee
            .STNKRenewalFee = "0" ' oPaymentInfo.MaximumSTNKRenewalFee
            .InsuranceClaimExpense = "0" ' oPaymentInfo.MaximumInsuranceClaimFee
            .RepossessionFee = "0" ' oPaymentInfo.MaximumReposessionFee
            .Prepaid = "0" ' oPaymentInfo.Prepaid

            .totalBayar = .InstallmentDue + .LCInstallment + .InstallmentCollFee + .InsuranceDue + _
                   .LCInsurance + .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                   .RepossessionFee + .Prepaid + .PLL0

            .PaymentAllocationBind()
        End With
    End Sub
End Class