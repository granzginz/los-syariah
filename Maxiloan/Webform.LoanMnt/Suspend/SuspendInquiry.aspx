﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendInquiry.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendInquiry" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SuspendInquiry</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(SuspendNo, branchid) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Suspend/SuspendInqView.aspx?SuspendNo=' + SuspendNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak berhak');
            }
        }
        document.onmousedown = click
    </script>
    <script language="JavaScript" type="text/javascript">
<!--
        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 1;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };

        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        }
        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
        }
        
        

    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
            &nbsp;
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label><input id="hdnChildValue" type="hidden" name="hdnSP"
                    runat="server">
            <input id="hdnChildName" type="hidden" name="hdnSP" runat="server">
            <input id="hdnBankAccount" type="hidden" name="hdSP" runat="server">
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            INQUIRY SUSPEND
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang&nbsp;<font color="#ff0033">*)</font>
                        </label>
                        <asp:DropDownList ID="cboParent" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                            ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Rekening Bank&nbsp;<font color="#ff0033">*)</font>
                        </label>
                        <asp:DropDownList ID="cboChild" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);"
                            runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                            ErrorMessage="Harap pilih Rekening Bank" ControlToValidate="cboChild" Display="Dynamic"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            No Bukti Kas
                        </label>
                        <asp:TextBox ID="txtRefNo" runat="server" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Suspend
                        </label>
                        <asp:TextBox ID="txtTglSuspend" runat="server" />
                        <aspajax:CalendarExtender ID="calExTglSuspend" runat="server" TargetControlID="txtTglSuspend"
                            Format="dd/MM/yyyy" />
                    </div>
                    <div class="form_right">
                        <label>
                            Status
                        </label>
                        <asp:DropDownList ID="cbostatus" runat="server">
                            <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                            <asp:ListItem Value="S">Suspend</asp:ListItem>
                            <asp:ListItem Value="A">Allocate</asp:ListItem>
                            <asp:ListItem Value="C">Reverse</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Keterangan
                        </label>
                        <asp:TextBox ID="txtdesc" runat="server" Width="477px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            <%--Jumlah Terima--%>
                            Jumlah Terima&nbsp;<font color="#ff0033">*)</font>
                        </label>
                        <uc1:ucnumberformat id="txtAmountFrom" runat="server" />
                        &nbsp;S/D&nbsp;
                        <uc1:ucnumberformat id="txtamountto" runat="server" />
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSAKSI SUSPEND
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" AllowPaging="True" AllowSorting="True"
                                OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="SuspendNo"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn SortExpression="branchid" HeaderText="branchid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SuspendNo" HeaderText="NO SUSPEND">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblSuspendNo" runat="server" Text='<%#Container.DataItem("SuspendNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TransferRefNO" HeaderText="NO BUKTI KAS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransferRefNO" runat="server" Text='<%#Container.DataItem("TransferRefNO")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="DESCRIPTION" HeaderText="KETERANGAN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDESCRIPTION" runat="server" Text='<%#Container.DataItem("DESCRIPTION")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AMOUNT" HeaderText="JUMLAH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNT"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="VALUEDATE" SortExpression="VALUEDATE" HeaderText="TGL SUSPEND"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="BANKACCOUNTID" HeaderText="BANK ACCOUNTID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBANKACCOUNTID" runat="server" Text='<%#Container.DataItem("BANKACCOUNTID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="bankaccountname" HeaderText="REKENING BANK">
                                        <ItemTemplate>
                                            <asp:Label ID="LBLBANKACCOUNTDESC" runat="server" Text='<%#Container.DataItem("bankaccountname")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SuspendStatusdesc" HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("SuspendStatusdesc")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
               
            </asp:Panel>

            <div id="divInnerHTML" runat="server">
            </div>

    </form>
</body>
</html>
