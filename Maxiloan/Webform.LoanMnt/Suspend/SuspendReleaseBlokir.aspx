﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendReleaseBlokir.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendReleaseBlokir" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ReleaseBlokirSuspend</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinMain(SuspendNo, branchid) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/Suspend/SuspendInqView.aspx?SuspendNo=' + SuspendNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">    
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>  
        <input id="hdnSuspendNo" type="hidden" name="hdnSuspendNo" runat="server" />
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                       
                <h3>                                        
                    RELEASE BLOKIR SUSPEND
                </h3>
            </div>
        </div>        
        <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DAFTAR TRANSAKSI SUSPEND DIBLOKIR
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyField="SuspendNo" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>                                                                
                                    <asp:TemplateColumn HeaderText="PILIH">                                    
                                    <ItemTemplate>                                                                                
                                        <asp:LinkButton ID="idRelease" runat="server" CausesValidation="False" Text="RELEASE"
                                        CommandName="Edit"></asp:LinkButton>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SuspendNo" HeaderText="NO SUSPEND">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblSuspendNo" runat="server" Text='<%#Container.DataItem("SuspendNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SuspendNo" SortExpression="SuspendNo" HeaderText="NO SUSPEND">                                   
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ReferenceNo" SortExpression="ReferenceNo" HeaderText="NO BUKTI KAS MASUK">                                   
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="KETERANGAN">                                   
                                    </asp:BoundColumn>                                    
                                    <asp:BoundColumn DataField="Amount" SortExpression="Amount" HeaderText="JUMLAH">                                   
                                    <HeaderStyle cssClass="th_right"/> 
                                    <ItemStyle cssClass="item_grid_right"/>
                                    </asp:BoundColumn>                                    
                                    <asp:BoundColumn DataField="ValueDate" SortExpression="ValueDate" HeaderText="TGL SUSPEND"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankAccount" SortExpression="BankAccount" HeaderText="REKENING BANK">                                   
                                    </asp:BoundColumn>                                    
                                    <asp:BoundColumn DataField="branchid" SortExpression="branchid" HeaderText="BRANCH ID">                                   
                                    </asp:BoundColumn>                                    
                                    <asp:TemplateColumn SortExpression="branchid" HeaderText="branchid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                            </Columns>
                        </asp:DataGrid>                                       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
        </div>
        </div>
        </div>  
        </asp:Panel>       
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    CARI SUSPEND DIBLOKIR
                </h4>
            </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="SuspendNo">Suspend No</asp:ListItem>
                </asp:DropDownList>                        
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
	        </div>
        </div>             
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass ="small button blue" Text="Search">
            </asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass ="small button gray">
            </asp:Button>
        </div>                                        
        </asp:Panel>
        <asp:Panel ID="pnlRelease" runat="server">        
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    PROSES REALEASE SUSPEND
                </h4>
            </div>
        </div>                        
        <div class="form_box">
	        <div class="form_single">
                <label>No Suspend</label>                
                <asp:Label ID="lblNoSuspend" runat="server"></asp:Label>                
	        </div>
        </div>                             
        <div class="form_box">
	        <div class="form_single">
                <label>No Bukti Kas</label>                
                <asp:Label ID="lblBuktiKas" runat="server"></asp:Label>         
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Keterangan</label>                
                <asp:Label ID="lblKeterangan" runat="server"></asp:Label>         
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah</label>                
                <asp:Label ID="lblJumlah" runat="server"></asp:Label>      
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Suspend</label>                
                <asp:Label ID="lblTglSuspend" runat="server"></asp:Label>    
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Rekening Bank</label>                
                <asp:Label ID="lblRekeningBank" runat="server"></asp:Label>         
	        </div>
        </div>                           
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass ="small button gray">
            </asp:Button>
        </div>                  
        </asp:Panel>       
    </form>
</body>
</html>

