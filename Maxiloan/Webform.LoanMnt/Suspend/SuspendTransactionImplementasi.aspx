﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendTransactionImplementasi.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendTransactionImplementasi" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Suspend Receive</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak Berhak');
            }
        }
        document.onmousedown = click
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    PENERIMAAN SUSPEND (IMPLEMENTASI)
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Rekening Bank
                </label>
                <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
            </div>
            <div class="form_right">
                <label class="label_req">
                    No Bukti Kas Masuk
                </label>
                <asp:TextBox ID="txtRefNo" runat="server"  MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtrefno"
                    ErrorMessage="Harap isi No Bukti Kas Masuk" Display="Dynamic" Enabled="False"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Tanggal Valuta
                </label>
                <asp:TextBox ID="txtTglValuta" runat="server" />
                <aspajax:CalendarExtender ID="calExTglValuta" runat="server" TargetControlID="txtTglValuta"
                    Format="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="rfvTglValuta" runat="server" ControlToValidate="txtTglValuta"
                    ErrorMessage="Harap isi dengan tanggal valuta" Display="Dynamic" />
            </div>
            <div class="form_right">
                <label class="label_req">
                    Jumlah Terima
                </label>
               <%-- <asp:TextBox ID="txtAmount" runat="server"  MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAmount" runat="server" Display="Dynamic" ErrorMessage="Harap isi Jumlah Terima"
                    ControlToValidate="txtamount" Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rvfAmount" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                    ControlToValidate="txtamount" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                    CssClass="validator_general"></asp:RegularExpressionValidator>--%>
                     <uc1:ucnumberformat id="txtAmount" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Keterangan
                </label>
                <asp:TextBox ID="txtdesc" runat="server"  Width="344px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdesc"
                    ErrorMessage="Harap isi Keterangan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>     
        </div>
    </asp:Panel>
    </form>
</body>
</html>
