﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
#End Region

Public Class SplitTransactionTitipanAngsuran
    Inherits Maxiloan.Webform.WebBased

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendAllocationController


    Private Property ReferenceNo() As String
        Get
            Return (CType(ViewState("ReferenceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReferenceNo") = Value
        End Set
    End Property
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(ViewState("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim rCusomClass As New Parameter.SuspendReceive
            Me.ReferenceNo = Request.QueryString("ReferenceNo")
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.ReferenceNo = Me.ReferenceNo
            oCustomClass.ApplicationID = Me.ApplicationID

            rCusomClass = oController.TitipanDetail(oCustomClass)
            With rCusomClass
                lblReferenceNo.Text = Me.ReferenceNo
                lblJumlah.Text = FormatNumber(.AmountRec, 2)
                Me.Amount = .AmountRec
                lblPostingdate.Text = .postingdate.ToString("dd/MM/yyyy")
                lblValutaDate.Text = .ValueDate.ToString("dd/MM/yyyy")
                lblBankAccountName.Text = .BankAccountName.ToString
                lblAgreementNo.Text = Me.AgreementNo
            End With

            GenerateGrid()

            ModVar.StrConn = GetConnectionString()
            ModVar.ReferenceNo = Me.ReferenceNo
            ModVar.BussinessDate = Me.BusinessDate
            ModVar.BranchID = sesBranchId.Replace("'", "")
            ModVar.ApplicationID = Me.ApplicationID
        End If
    End Sub

    Private Sub GenerateGrid()
        Dim tmpTable As New DataTable
        Dim oRow As DataRow

        With tmpTable
            .Columns.Add(New DataColumn("lblNomor", GetType(String)))
            .Columns.Add(New DataColumn("txtAmount", GetType(String)))
            .Columns.Add(New DataColumn("txtReferenceNo", GetType(String)))
        End With

        oRow = tmpTable.NewRow()
        oRow("lblNomor") = 1
        oRow("txtAmount") = 0
        oRow("txtReferenceNo") = ""
        tmpTable.Rows.Add(oRow)


        dtgSplit.DataSource = tmpTable
        dtgSplit.DataBind()
    End Sub

    'Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
    '    Dim txtAmount As TextBox
    '    Dim totSplit As Double = 0
    '    For i As Integer = 0 To dtgSplit.Items.Count - 1
    '        txtAmount = CType(dtgSplit.Items(i).FindControl("txtAmount"), TextBox)
    '        totSplit += txtAmount.Text
    '    Next
    '    If totSplit <> Me.Amount Then
    '        ShowMessage(lblMessage, "Total Split harus sama dengan " & FormatNumber(Amount, 0), True)
    '        Exit Sub
    '    End If
    'End Sub

    <WebMethod()>
    Public Shared Sub SimpanData(Amount As String, ReferenceNo As String, txtReferenceNo As String, Nomor As Integer)
        Dim oClass As New Parameter.SuspendReceive
        Dim m_suspend As New TitipanKoreksiAngsuranController

        oClass.strConnection = ModVar.StrConn
        oClass.ReferenceNo = ModVar.ReferenceNo
        oClass.AmountRec = CDbl(Amount)
        oClass.Nomor = Nomor
        oClass.txtReferenceNo = txtReferenceNo
        oClass.BranchId = ModVar.BranchID
        oClass.BusinessDate = ModVar.BussinessDate
        oClass.ApplicationID = ModVar.ApplicationID

        m_suspend.TitipanSplit(oClass)

    End Sub
End Class