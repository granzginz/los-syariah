﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SplitTransactionTitipanAngsuran.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.SplitTransactionTitipanAngsuran" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Split Titipan Process</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script type='text/javascript' language='javascript'>
        function AddNewRecord() {
            var grd = document.getElementById("dtgSplit");
            var tbod = grd.rows[0].parentNode;

            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);
            var len = grd.rows.length - 1;
            newRow.cells[0].innerHTML = len + 1;


            var inp1 = newRow.cells[1].getElementsByTagName('input')[0];
            inp1.id += len;
            inp1.value = '0';

            var inp2 = newRow.cells[2].getElementsByTagName('input')[0];
            inp2.id += len;
            inp2.value = '';

            $('#dtgSplit').append(newRow);

            return false;
        }
        function Save() {
            var grid = document.getElementById('dtgSplit');
            var rowCount = grid.rows.length - 1;
            var total = 0;
            var id = '';
            var lblJumlah = $('#lblJumlah').html();
            for (i = 0; i < rowCount; i++) {
                id = id + i;
                var Amount = $('#dtgSplit_txtAmount_' + id).val();
                total = total + parseInt(Amount.replace(/\s*,\s*/g, ''));
            }
            if (total != parseInt(lblJumlah.replace(/\s*,\s*/g, ''))) {
                alert("Total Split harus sama dengan " + lblJumlah)
            } else {
                simpandata();
            }
        }
        function simpandata() { 
            var grd = document.getElementById('dtgSplit');
            var row = grd.rows.length - 1; 
            
            $('#pesan').html('Mohon tunggu, Sedang dalam proses!');
            $('#dtgSplit').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');

                    var Amount = $tds[1].getElementsByTagName('input')[0];
                    var txtReferenceNo = $tds[2].getElementsByTagName('input')[0];
                    var valueNOMOR = i;
                    //alert(valueNOMOR);
                    var idAmout = Amount.id;
                    var idtxtReferenceNo = txtReferenceNo.id; 
                    var z = $('#lblReferenceNo').html();
                    
                    var paramArg = JSON.stringify({
                        //ReferenceNo: hyperlinkReferenceNo.value,
                        ReferenceNo:z,
                        Amount: Amount.value,
                        Nomor: valueNOMOR,
                        txtReferenceNo: txtReferenceNo.value 
                    }); 
                    //if(Amount.value > 0){
                    $.ajax({
                        url: "SplitTransactionTitipanAngsuran.aspx/SimpanData",
                        data: paramArg,
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        async: false,
                        success: function (response) {
                            //if (response.d == "D") {
                            window.location = "SplitTitipanAngsuran.aspx"
                            //}
                        },
                        error: function (xhr, status, error) {
                            var err = eval("(" + xhr.responseText + ")");
                            $('#pesan').html('Proses Error : ' + err.Message);
                            $('#pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%;color:red');
                        }
                    });
                    // }
                }
            });

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:updatePanel ID="upData" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
            onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    SPLIT TITIPAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
            <div class="form_left">
                <label>
                    No Reference
                </label>
                <asp:HyperLink ID="lblReferenceNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Rekening Bank
                </label>
                <asp:Label ID="lblBankAccountName" runat="server"></asp:Label>
            </div>
            </div>
        </div>
        <div class="form_box">
            <div>
            <div class="form_left">
                <label>
                    Tanggal Posting
                </label>
                <asp:Label ID="lblPostingdate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Valuta
                </label>
                <asp:Label ID="lblValutaDate" runat="server"></asp:Label>
            </div>
            </div>
        </div>
        <div class="form_box">
            <div>
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblJumlah" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server"></asp:Label>
            </div>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper">
                    <asp:DataGrid ID="dtgSplit" runat="server" CellPadding="0" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                        AutoGenerateColumns="False" AllowSorting="True"
                        Width="100%" Visible="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns> 
                            <asp:TemplateColumn HeaderText="NO" ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblNomor">1</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AMOUNT" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtAmount" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);"
                                    CssClass="numberAlign regular_text">0</asp:TextBox>
                                    </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO REFERENCE">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtReferenceNo" CssClass="long_text"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <label style="width:30px;height:13px" ID="btnAdd" class="small button blue" OnClick="AddNewRecord()">ADD</label>
            <label style="width:30px;height:13px" ID="btnSave" class="small button blue" OnClick="Save()">SAVE</label>
            <%--<asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="SAVE"
                CssClass="small button blue"></asp:Button>--%>

            <div id="pesan"></div>
        </div>
    </ContentTemplate>
    </asp:Updatepanel>
    </form>
</body>
</html>
