﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendInqView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendInqView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SuspendInqView</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <asp:Panel ID="pnlList" runat="server" EnableViewState="False">
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h4>
                    VIEW - SUSPEND
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Suspend
                </label>
                <asp:Label ID="lblSuspendNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Posting
                </label>
                <asp:Label ID="lblPostingdate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Rekening Bank
                </label>
                <asp:Label ID="lblBankaccount" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas
                </label>
                <asp:Label ID="lblRefno" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Valuta
                </label>
                <asp:Label ID="lblValueDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Terima
                </label>
                <asp:Label ID="lblAmountRec" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Voucher
                </label>
                <asp:Label ID="lblVoucherNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Terima
                </label>
                <asp:Label ID="lblReceiveDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Keterangan
                </label>
                <asp:Label ID="lblDesc" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    ALOKASI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="lblCustname" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status
                </label>
                <asp:Label ID="lblstatus" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <img id="imbexit" style="cursor: hand" onfocus="Close();" onclick="Close();" alt=""
                    src="../../images/buttonExit.gif">
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
