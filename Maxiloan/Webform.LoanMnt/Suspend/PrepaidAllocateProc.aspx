﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrepaidAllocateProc.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PrepaidAllocateProc" %>


<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucpaymentallocationdetail" Src="../../Webform.UserController/ucpaymentallocationdetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PrepaidAllocateProc</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		
        function flyto(strParam, strOpt, strType) {
            switch (strOpt) {
                case "customer":
                    {
                        if (strType == 'C')
                            var a = window.open("../../../../SmartSearch/viewsearchcompany.aspx?custid=" + strParam, "customer", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        else
                            var a = window.open("../../../../SmartSearch/viewsearch.aspx?custid=" + strParam, "customer", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        a.focus();
                        break;
                    }
                case "agreement":
                    {
                        var a = window.open("../../../../SmartSearch/peragreement.aspx?applicationid=" + strParam, "agreement", "status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=yes");
                        a.focus();
                        break;
                    }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
            onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    ALOKASI PREPAID
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box" style="background-color:#FCFCCC">
            <div class="form_left">
                <label>
                    Saldo Angsuran
                </label><b>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label></b>
            </div>
            <div class="form_right">
                <label>
                    Saldo Prepaid
                </label><b>
                <asp:Label ID="lblPrepaid" runat="server"></asp:Label></b>
            </div>
        </div> 
        <div class="form_box" style="background-color:#FCFCCC">
            <div class="form_left">
                <label>Titipan Angsuran</label><b>
                <asp:Label ID="lblTitipanAngsuran" runat="server"></asp:Label></b>
            </div>
            <div class="form_right">
                <label>Prepaid Status</label><b>
                <asp:Label id="lblPrepaidStatus" runat="server"></asp:Label></b>
            </div>
        </div>       
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    TABEL ANGSURAN
                </h5>
            </div>
        </div>
        <uc1:ucinstallmentschedule id="oInstallmentSchedule" runat="server" />
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box" style="background-color:#FCFCCC">
            <div class="form_left">              
                <label>
                    Cara Pembayaran
                </label><b>
                <asp:Label ID="lblWOP" runat="server"></asp:Label></b> 
            </div>
            <div class="form_right">
                <label>
                    Tanggal Valuta
                </label><b>               
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece></b> 
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Keterangan
                </label>
                <asp:TextBox ID="txtnotes" runat="server" Width="365px" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="form_right">
            </div>
        </div>        
        <asp:Panel ID="pnlPaymentDetail" runat="server">
            <div class="form_box_uc">
                <uc1:ucpaymentallocationdetail id="oPaymentAllocationDetail" runat="server"></uc1:ucpaymentallocationdetail>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtn" runat="server">
            <div class="form_button">
                <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false">
                </asp:Button>&nbsp;
                <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentInfo" Visible="false" runat="server">
        <div class="form_box_uc">
            <uc1:ucpaymentinfo id="oPaymentInfo" runat="server"></uc1:ucpaymentinfo>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
