﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendReversalList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendReversalList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SuspendReversalList</title>
     <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak Berhak');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server"  ></asp:Label>
    <div class="form_title">  <div class="title_strip"> </div>
                <div class="form_single">
                     
                    <h4>
                         SUSPEND REVERSAL </h4>
                </div>
            </div>
    
    <div class="form_box">
					<div class="form_left">
					<label>  No Suspend </label>
					 <asp:Label ID="lblSuspendNo" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>   Tanggal Posting </label>
					 <asp:Label ID="lblPostingDate" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Rekening Bank </label>
					<asp:Label ID="lblbankAccount" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>  No Bukti Kas </label>
					 <asp:Label ID="lblRefNO" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Tanggal Suspend </label>
					<asp:Label ID="lblSuspendDate" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>  Jumlah Terima </label>
					<asp:Label ID="lblAmount" runat="server"></asp:Label>
					</div>
				</div>
    
    <div class="form_box">
					<div class="form_left">
					<label> Keterangan </label>
					 <asp:Label ID="lbldesc" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					 
					</div>
				</div>
     <div class="form_box">
					<div class="form_left">
					<label> No Bukti Kas Reversal </label>
					 <asp:TextBox ID="txtReversalReffNo" runat="server"  MaxLength="20"
                    Width="184px"></asp:TextBox>
					</div>
					<div class="form_right">
					 
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Catatan </label>
					  <asp:TextBox ID="txtCorrection" runat="server"  Width="70%" TextMode="MultiLine"></asp:TextBox>
					</div>
					<div class="form_right">
					 
					</div>
				</div>
      <div class="form_button">
			  <asp:Button ID="btnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray">
                </asp:Button>
			 </div>
    
    </form>
</body>
</html>
