﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SuspendReversalList
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendReversalController
    Dim strbankaccountid As String
#End Region

#Region "Property"


    Private Property SuspendNo() As String
        Get
            Return (CType(Viewstate("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("SuspendNo") = Value
        End Set
    End Property

    Private Property BankAccountid() As String
        Get
            Return (CType(Viewstate("BankAccountid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankAccountid") = Value
        End Set
    End Property


#End Region



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "SUSPENDREVERSAL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SuspendNo = Request.QueryString("SuspendNo")
                Me.BranchID = Request.QueryString("branchid")

                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind()
            End If

        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        oCustomClass.SuspendNo = Me.SuspendNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass = oController.SuspendReverse(oCustomClass)
        With oCustomClass
            lblSuspendNo.Text = Me.SuspendNo
            lblPostingDate.Text = .postingdate.ToString("dd/MM/yyyy")
            lblbankAccount.Text = .bankAccountName
            lblRefNO.Text = .ReferenceNo
            'txtReversalReffNo.Text = .ReferenceNo
            lblSuspendDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            lblAmount.Text = FormatNumber(.AmountRec, 2)
            lbldesc.Text = .description
            strbankaccountid = .BankAccountID
            Me.BankAccountid = strbankaccountid
        End With

    End Sub
#End Region

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            btnSave.Attributes.Add("Onclick", "return checksubmit()")
            '       @BranchID varchar(3),
            '@bankaccountid varchar(10),
            '@loginid varchar(12),
            '@BusinessDate datetime,
            '@Amount decimal(17,2),
            '@ValueDate datetime,
            '@referenceNo char(20),
            '@Notes varchar(50),
            '@CoyID varchar(3)= '001',
            '@strerror varchar(100) output
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.BranchID
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .SuspendNo = Me.SuspendNo
                .description = txtCorrection.Text
                .BankAccountID = Me.BankAccountid
                .AmountRec = CDbl(lblAmount.Text)
                .ValueDate = ConvertDate2(lblSuspendDate.Text)
                .ReferenceNo = txtReversalReffNo.Text.Trim
                .CoyID = Me.SesCompanyID
            End With
            Try
                oController.SaveSuspendReverse(oCustomClass)
                Server.Transfer("SuspendReversal.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Server.Transfer("SuspendReversal.aspx")
    End Sub

End Class