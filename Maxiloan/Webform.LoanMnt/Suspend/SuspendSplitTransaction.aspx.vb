﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
#End Region

Public Class SuspendSplitTransaction
    Inherits Maxiloan.Webform.WebBased

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendAllocationController


    Private Property SuspendNo() As String
        Get
            Return (CType(ViewState("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SuspendNo") = Value
        End Set
    End Property
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim rCusomClass As New Parameter.SuspendReceive
            Me.SuspendNo = Request.QueryString("SuspendNo")
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.SuspendNo = Me.SuspendNo

            rCusomClass = oController.SuspendDetail(oCustomClass)
            With rCusomClass
                lblSuspendNO.Text = Me.SuspendNo
                lblJumlah.Text = FormatNumber(.AmountRec, 2)
                Me.Amount = .AmountRec
                lblPostingdate.Text = .postingdate.ToString("dd/MM/yyyy")
                lblValutaDate.Text = .ValueDate.ToString("dd/MM/yyyy")
                lblBankAccountName.Text = .BankAccountName.ToString
                lblDesc.Text = .description.ToString
            End With
            
            GenerateGrid()

            ModVar.StrConn = GetConnectionString()
            ModVar.SuspendNO = Me.SuspendNo
            ModVar.BussinessDate = Me.BusinessDate
            ModVar.BranchID = sesBranchId.Replace("'", "")
        End If
    End Sub
   
    Private Sub GenerateGrid()
        Dim tmpTable As New DataTable
        Dim oRow As DataRow

        With tmpTable
            .Columns.Add(New DataColumn("lblNomor", GetType(String)))
            .Columns.Add(New DataColumn("txtAmount", GetType(String)))
            .Columns.Add(New DataColumn("txtDescription", GetType(String)))
        End With

        oRow = tmpTable.NewRow()
        oRow("lblNomor") = 1
        oRow("txtAmount") = 0
        oRow("txtDescription") = ""
        tmpTable.Rows.Add(oRow)


        dtgSplit.DataSource = tmpTable
        dtgSplit.DataBind()
    End Sub

    'Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
    '    Dim txtAmount As TextBox
    '    Dim totSplit As Double = 0
    '    For i As Integer = 0 To dtgSplit.Items.Count - 1
    '        txtAmount = CType(dtgSplit.Items(i).FindControl("txtAmount"), TextBox)
    '        totSplit += txtAmount.Text
    '    Next
    '    If totSplit <> Me.Amount Then
    '        ShowMessage(lblMessage, "Total Split harus sama dengan " & FormatNumber(Amount, 0), True)
    '        Exit Sub
    '    End If
    'End Sub

    <WebMethod()> _
    Public Shared Sub SimpanData(Amount As String, Desc As String, SuspenNo As String)
        Dim oClass As New Parameter.SuspendReceive
        Dim m_suspend As New SuspendAllocationController

        oClass.strConnection = ModVar.StrConn
        oClass.SuspendNo = ModVar.SuspendNO
        oClass.AmountRec = CDbl(Amount)
        oClass.description = Desc
        oClass.BranchId = ModVar.BranchID
        oClass.BusinessDate = ModVar.BussinessDate

        m_suspend.SuspendSplit(oClass)

    End Sub
End Class