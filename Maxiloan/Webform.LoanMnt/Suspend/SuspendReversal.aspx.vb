﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SuspendReversal
    Inherits Maxiloan.Webform.WebBased
    'Protected WithEvents sdate As ValidDate
    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendReversalController

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "SUSPENDREVERSAL"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                oBankAccount.BankPurpose = ""
                oBankAccount.BankType = ""
                oBankAccount.IsAll = True
                oBankAccount.BindBankAccount()

                'sdate.dateValue = CStr(Day(Me.BusinessDate)) + "/" + CStr(Month(Me.BusinessDate)) + "/" + CStr(Year(Me.BusinessDate))
                'sdate.isCalendarPostBack = False
                'sdate.FillRequired = False


                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If

    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.SuspendReversalList(oCustomClass)

        DtUserList = oCustomClass.listdata
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim NlblSuspendNo As HyperLink
        Dim Nbranchid As Label
        Dim NhyReverse As HyperLink

        'HyReverse()
        If e.Item.ItemIndex >= 0 Then
            NlblSuspendNo = CType(e.Item.FindControl("lblSuspendNo"), HyperLink)
            Nbranchid = CType(e.Item.FindControl("lblbranchid"), Label)
            ' If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            NhyReverse = CType(e.Item.FindControl("HyReverse"), HyperLink)
            NhyReverse.NavigateUrl = "SuspendReversalList.aspx?SuspendNo=" & NlblSuspendNo.Text.Trim & "&branchid=" & Nbranchid.Text.Trim
            NlblSuspendNo.NavigateUrl = "javascript:OpenWinMain('" & NlblSuspendNo.Text.Trim & "','" & Nbranchid.Text.Trim & "')"
            'End If

        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Server.Transfer("SuspendReversal.aspx")
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Me.SearchBy = " st.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "

            Me.SearchBy = Me.SearchBy & " st.suspendstatus = 'S' and isnull(st.Otorisasi,'')= 'A' and "

            If obankaccount.BankAccountID <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " st.bankaccountid = '" & obankaccount.BankAccountID.Trim & "' and "
            End If

            If txtTglSuspend.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " st.valuedate ='" & ConvertDate2(txtTglSuspend.Text) & "' and "
            End If

            If txtdesc.Text.Trim <> "" Then
                If Right(txtdesc.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " st.description Like '" & txtdesc.Text.Trim & "'  and "
                Else
                    Me.SearchBy = Me.SearchBy & " st.description ='" & txtdesc.Text.Trim & "' and "
                End If

            End If
            If txtRefNo.Text.Trim <> "" Then

                If Right(txtdesc.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " st.TransferRefNo Like '" & txtRefNo.Text.Trim & "'  and "
                Else
                    Me.SearchBy = Me.SearchBy & " st.TransferRefNo ='" & txtRefNo.Text.Trim & "' and "
                End If
            End If

            If txtAmountFrom.Text.Trim > "0" Then
                Me.SearchBy = Me.SearchBy & " st.amount >= '" & CDbl(txtAmountFrom.Text.Trim) & "' and "
            End If

            If txtamountto.Text.Trim > "0" Then
                Me.SearchBy = Me.SearchBy & " st.amount <= '" & CDbl(txtamountto.Text.Trim) & "' and "
            End If

            If Me.SearchBy.Trim <> "" Then
                Me.SearchBy = Left(Me.SearchBy, Len(Me.SearchBy) - 4)
            Else
                Me.SearchBy = ""
            End If
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


End Class