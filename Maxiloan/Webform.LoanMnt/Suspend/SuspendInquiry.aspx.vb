﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SuspendInquiry
    Inherits Maxiloan.Webform.WebBased
    ' Protected WithEvents sdate As ValidDate
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendInquiryController
    Private m_controller As New DataUserControlController
    Private oEntitesChild As New Parameter.GeneralPaging
    Private oControllerChild As New GeneralPagingController

#End Region
#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "SUSPENDINQUIRY"
     
        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'If Request.QueryString("message") <> "" Then

                '    ShowMessage(lblMessage, Request.QueryString("message"), True)
                'End If
                'sdate.isCalendarPostBack = False
                'sdate.FillRequired = False
                Dim dtbranch As New DataTable
                Me.sesBranchId = Replace(Me.sesBranchId, "'", "")
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Me.sesBranchId))
                getchildcombo()
                Me.SearchBy = ""
                Me.SortBy = ""

                InitiateUCnumberFormat(txtAmountFrom, True, True)
                InitiateUCnumberFormat(txtamountto, True, True)
            End If

        End If

    End Sub
    Protected Sub getchildcombo()
        Dim dtInsCo As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = cboParent.SelectedValue.ToString
        End With

        dtInsCo = oController.GetBankAccountAll(oCustomClass)

        cboChild.DataValueField = "ID"
        cboChild.DataTextField = "Name"
        cboChild.DataSource = dtInsCo.DefaultView
        cboChild.DataBind()

        'dtInsCo = oController.GetBankAccountAll(oCustomClass)
        'Response.Write(GenerateScript(dtInsCo))
    End Sub
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("Id")), "null", DataRow(i)("Id"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("Id")), "null", DataRow(i)("Id"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.SuspendInqList(oCustomClass)

        DtUserList = oCustomClass.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        getchildcombo()
        Dim strScript As String
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
        strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
        strScript &= "</script>"
        divInnerHTML.InnerHtml = strScript
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages <> 0 Then

          
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim NlblSuspendNo As HyperLink
        Dim Nbranchid As Label
        Dim NhyReverse As HyperLink

        'HyReverse()
        If e.Item.ItemIndex >= 0 Then
            NlblSuspendNo = CType(e.Item.FindControl("lblSuspendNo"), HyperLink)
            Nbranchid = CType(e.Item.FindControl("lblbranchid"), Label)
            ' If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then
            NlblSuspendNo.NavigateUrl = "javascript:OpenWinMain('" & NlblSuspendNo.Text.Trim & "','" & Nbranchid.Text.Trim & "')"

            'End If

        End If
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Server.Transfer("SuspendInquiry.aspx")
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim par As String
        par = ""
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            'Me.SearchBy = " st.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
            Me.SearchBy = " st.branchid = '" & cboParent.SelectedItem.Value.Trim & "' and "
            par = par & " Branch : " & cboParent.SelectedItem.Text.Trim & " "
            If cboChild.SelectedValue <> "" Then

                Me.SearchBy = Me.SearchBy & " st.bankaccountid = '" & cboChild.SelectedValue & "' and "
                If par <> "" Then
                    par = par & " , Bank Account : " & cboChild.SelectedValue & " "
                Else
                    par = par & "Bank Account : " & cboChild.SelectedValue & " "
                End If
            End If
            'If obankaccount.BankAccountID <> "ALL" Then
            '    Me.SearchBy = Me.SearchBy & " st.bankaccountid = '" & obankaccount.BankAccountID.Trim & "' and "
            '    If par <> "" Then
            '        par = par & " , Bank Account : " & obankaccount.BankAccountName & " "
            '    Else
            '        par = par & "Bank Account : " & obankaccount.BankAccountName & " "
            '    End If
            'End If

            If txtRefNo.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " st.TransferRefNo ='" & txtRefNo.Text.Trim & "' and "
            End If

            If txtTglSuspend.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " st.valuedate ='" & ConvertDate2(txtTglSuspend.Text) & "' and "
                If par <> "" Then
                    par = par & " , Suspend Date : " & ConvertDate2(txtTglSuspend.Text).ToString("dd/MM/yyyy") & " "
                Else
                    par = par & "Suspend Date : " & ConvertDate2(txtTglSuspend.Text).ToString("dd/MM/yyyy") & " "
                End If
            End If

            If cbostatus.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " st.suspendstatus = '" & cbostatus.SelectedItem.Value & "' and "
                If par <> "" Then
                    par = par & " , Status : " & cbostatus.SelectedItem.Value & " "
                Else
                    par = par & "Status : " & cbostatus.SelectedItem.Value & " "
                End If
            End If


            If txtdesc.Text.Trim <> "" Then
                If Right(txtdesc.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " st.description Like '" & txtdesc.Text.Trim & "'  and "
                Else
                    Me.SearchBy = Me.SearchBy & " st.description ='" & txtdesc.Text.Trim & "' and "
                End If
                If par <> "" Then
                    par = par & " , Description : " & txtdesc.Text.Trim & " "
                Else
                    par = par & "Description : " & txtdesc.Text.Trim & " "
                End If
            End If


            If txtAmountFrom.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " st.amount >= '" & CDbl(txtAmountFrom.Text.Trim) & "' and "
                If par <> "" Then
                    par = par & " , Amount From : " & CDbl(txtAmountFrom.Text.Trim) & " "
                Else
                    par = par & "Amount From : " & CDbl(txtAmountFrom.Text.Trim) & " "
                End If
            End If

            If txtamountto.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " st.amount <= '" & CDbl(txtamountto.Text.Trim) & "' and "
                If par <> "" Then
                    par = par & " , Amount To : " & CDbl(txtamountto.Text.Trim) & " "
                Else
                    par = par & "Amount To : " & CDbl(txtamountto.Text.Trim) & " "
                End If
            End If

            If Me.SearchBy.Trim <> "" Then
                Me.SearchBy = Left(Me.SearchBy, Len(Me.SearchBy) - 4)
            Else
                Me.SearchBy = ""
            End If

            If par.Trim <> "" Then
                Me.ParamReport = par
            Else
                Me.ParamReport = ""
            End If

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign regular_text"
        End With
    End Sub
End Class