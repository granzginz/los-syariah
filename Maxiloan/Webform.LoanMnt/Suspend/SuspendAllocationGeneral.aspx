﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuspendAllocationGeneral.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.SuspendAllocationGeneral" %>


<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCOA" Src="../../Webform.UserController/ucLookUpCOA.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Suspend Allocation General</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak Berhak');
            }
        }
        document.onmousedown = click
    </script>
    <style type="text/css">
        .inptype
        {
            margin-left: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        SUSPEND ALLOCATION GENERAL
                    </h4>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Rekening Bank
                        </label>
                        <uc1:ucbankaccountid id="UcBankAccountID" runat="server">
                        </uc1:ucbankaccountid>
                    </div>
                    <div class="form_right">
                        <label>
                            No Reference
                        </label>
                        <asp:TextBox ID="txtRefNo" runat="server" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Suspend
                        </label>
                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                    <div class="form_right">
                        <label>
                            Keterangan
                        </label>
                        <asp:TextBox ID="txtdesc" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Terima
                        </label>                        
                        <uc1:ucnumberformat id="txtAmountFrom" runat="server" />
                        </uc1:ucNumberFormat>
                         &nbsp;S/D&nbsp;                        
                        <uc1:ucnumberformat id="txtamountto" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imgSearch" runat="server" CssClass="button small blue" Text="Search">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbReset" runat="server" CssClass="button small gray" Text="Reset"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSAKSI SUSPEND
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" DataKeyField="SuspendNo"
                                AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" AllowPaging="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle  ></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyReverse" runat="server" Text='ALOKASI'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SuspendNo" HeaderText="NO SUSPEND">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblSuspendNo" runat="server" Text='<%#Container.DataItem("SuspendNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO BUKTI KAS MASUK">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ValueDate" SortExpression="ValueDate" HeaderText="TGL SUSPEND"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="BankAccount" HeaderText="REKENING BANK">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccount" runat="server" Text='<%#Container.DataItem("BankAccount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left"  
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALOKASI PEMBAYARAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:DropDownList ID="cboCabang" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Departemen
                        </label>
                        <asp:DropDownList ID="cboDepartemen" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Transaksi
                        </label>
                        <input id="hdnTransaction" type="hidden" name="hdnTransaction" runat="server" />
                        <asp:TextBox runat="server" ID="txtTransaction" CssClass="medium_text"></asp:TextBox>
                        <asp:Button ID="btnLookupTransaction" runat="server" CausesValidation="False" Text="..."
                            CssClass="small buttongo blue" />
                        <uc1:uclookupcoa id="ucLookUpCOA" runat="server" oncatselected="CatSelectedTransaction" />
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Input ini harus diisi!"
                                ControlToValidate="txtTransaction" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah
                        </label>                        
                        <uc1:ucNumberFormat ID="txtJumlah" runat="server"  />                        
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_general">
                            Keterangan
                        </label>
                        <asp:TextBox ID="txtKeteranganAlokasi" runat="server" CssClass="multiline_textbox_uc"
                            TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="imbCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <input id="hdnSuspendNo" type="hidden" name="hdnSuspendNo" runat="server" />
    </form>
</body>
</html>
