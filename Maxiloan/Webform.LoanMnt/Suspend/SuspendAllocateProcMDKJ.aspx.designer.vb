﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SuspendAllocateProcMDKJ
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''pnlPaymentReceive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPaymentReceive As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblSuspendNO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSuspendNO As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblPostingdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPostingdate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAgreementNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAgreementNo As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomerName As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblInstallmentAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInstallmentAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPrepaid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrepaid As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblWOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOP As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRefNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblBankAccount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBankAccount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAmountRec control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAmountRec As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtnotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtnotes As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPaymentDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPaymentDetail As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''NoRefrensi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NoRefrensi As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rfvNoRefrensi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvNoRefrensi As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''lblInvoiceNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInvoiceNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''jumlahBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents jumlahBayar As Global.System.Web.UI.UserControl
    
    '''<summary>
    '''lblInvoiceAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInvoiceAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTotalPembiayaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalPembiayaan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblInterestAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInterestAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblDendaAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDendaAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRetensiAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRetensiAmount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbltotalbayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltotalbayar As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbltotalTagihan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltotalTagihan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlBtn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBtn As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''imbSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''imbCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlPaymentInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPaymentInfo As Global.System.Web.UI.WebControls.Panel
End Class
