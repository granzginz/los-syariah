﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SuspendAllocateProcFACT
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    'Protected WithEvents oPaymentinfo As UcPaymentInfo
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule
    Protected WithEvents txtValueDate As ucDateCE
    Protected WithEvents oValueDate As ucDateCE
    Protected WithEvents oInvoiceDate As ucDateCE
    Protected WithEvents oInvoiceDueDate As ucDateCE
    Protected WithEvents principalPaid As ucNumberFormat
    Protected WithEvents interestPaid As ucNumberFormat
    Protected WithEvents RetensiPaid As ucNumberFormat
    Protected WithEvents DendaPaid As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendReversalController
    Private oFactoringInvoice As New Parameter.FactoringInvoice
    Private oCustomClass2 As New Parameter.InstallRcv
    Private oController2 As New SuspendAllocationController
    Private m_controller As New FactoringInvoiceController

#End Region

#Region "Property"


    Private Property SuspendNo() As String
        Get
            Return (CType(ViewState("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SuspendNo") = Value
        End Set
    End Property

    Private Property AmountReceive() As Double
        Get
            Return (CType(ViewState("AmountReceive"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property

    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo")
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "SUSPENDALLOCATION"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                Dim lblgiro As New Label
                Dim lblnewStat As New Label
                Dim inFlagfile As New Label

                Me.SuspendNo = Request.QueryString("SuspendNo")
                Me.BranchID = Request.QueryString("branchid")
                Me.AgreementNo = Request.QueryString("agreementno")
                Me.ApplicationID = Request.QueryString("applicationid")
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")
                oValueDate.AutoPostBack = True
                oValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind()
                txtValueDate.IsRequired = True
                'load grid angsuran
                'oInstallmentSchedule.ApplicationId = Me.ApplicationID
                'oInstallmentSchedule.ValueDate = Me.BusinessDate
                'oInstallmentSchedule.DoBind_Angsuran(String.Empty)
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        Dim oControllerInfo As New UCPaymentInfoController
        Dim oCustomClassInfo As New Parameter.AccMntBase
        oCustomClass.SuspendNo = Me.SuspendNo
        oCustomClass.strConnection = GetConnectionString()

        'Diganti karena pusat dibuka semua
        'oCustomClass.BranchId = Me.BranchID
        oCustomClass.BranchId = Replace(Me.sesBranchId, "'", "")

        oCustomClass = oController.SuspendReverse(oCustomClass)

        With oCustomClass
            lblSuspendNO.Text = .SuspendNo
            lblSuspendNO.NavigateUrl = "javascript:OpenWinMain('" & lblSuspendNO.Text.Trim & "','" & Me.BranchID.Trim & "')"
            lblPostingdate.Text = .postingdate.ToString("dd/MM/yyyy")
            lblBankAccount.Text = .BankAccountName
            Me.BankAccount = .BankAccountID
            lblRefNo.Text = .ReferenceNo
            txtValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            Me.ValueDate = ConvertDate2(txtValueDate.Text)
            lblAmountRec.Text = FormatNumber(.AmountRec, 0)
            Me.AmountReceive = .AmountRec
            lblWOP.Text = "Suspend"
            'lblReceiveFrom.Text = "-"

        End With

        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString()
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)

        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            'lblAmountToBePaid.Text = FormatNumber(.AmountToBePaid, 2)
            lblPrepaid.Text = FormatNumber(.Prepaid, 0)
            lblInstallmentAmount.Text = FormatNumber(.installmentamount, 0)

            'lblNextInstallmentDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
            Me.NextInstallmentDate = .NextInstallmentDate
            'lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)


            'lblFundingCoyName.Text = .FundingCoyName
            'lblFundingPledgeStatus.Text = .FundingPledgeStatus
            'lblResiduValue.Text = FormatNumber(.ResiduValue, 2)

        End With

        'With oPaymentInfo
        '    .ValueDate = ConvertDate2(txtValueDate.Text)
        '    .ApplicationID = Me.ApplicationID
        '    .PaymentInfo()
        'End With
        'With oPaymentAllocationDetail
        '    .PaymentAllocationBind()

        '    .ShowPanelCashier = False
        '    .InstallmentDue = Me.AmountReceive
        '    .totalBayar = Me.AmountReceive
        '.MaximumInstallment = oPaymentInfo.MaximumInstallment
        '.MaximumLCInstallFee = oPaymentInfo.MaximumLCInstallFee
        '.MaximumInstallCollFee = oPaymentInfo.MaximumInstallCollFee
        '.MaximumInsurance = oPaymentInfo.MaximumInsurance
        '.MaximumLCInsuranceFee = oPaymentInfo.MaximumLCInsuranceFee
        '.MaximumInsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee

        '.MaximumPDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
        '.MaximumSTNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee
        '.MaximumInsuranceClaimFee = oPaymentInfo.MaximumInsuranceClaimFee
        '.MaximumReposessionFee = oPaymentInfo.MaximumReposessionFee
        'If oPaymentinfo.PrepaidHoldStatus.Trim <> "NM" Then
        '    .DisabledAll = True
        'End If
        'If oPaymentinfo.PrepaidHoldStatus.Trim <> "NM" Then
        '    .DisabledAll = True
        'End If

        'If (oPaymentInfo.ContractStatus.Trim = "PRP" Or
        '    oPaymentInfo.ContractStatus.Trim = "INV" Or
        '    oPaymentInfo.ContractStatus.Trim = "LNS") Then
        '    .DisabledAll = True
        'End If
        'End With
        Dim oCustomClass1 As New Parameter.AccMntBase
        With oCustomClass1
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            '.ValueDate = ConvertDate2(oValueDate.Text)
            .BranchId = Me.BranchID
        End With

        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID
        oFactoringInvoice.valuedate = ConvertDate2(oValueDate.Text)
        oFactoringInvoice.InvoiceSeqNo = Me.InvoiceSeqNo
        oFactoringInvoice = m_controller.GetInvoiceDetail2(oFactoringInvoice)

        With oFactoringInvoice.Listdata
            'lblInvoiceNo.Text = .Rows(0).Item("InvoiceNo").ToString
            lblInvoiceNo.Text = Me.InvoiceNo.Trim
            oInvoiceDate.Text = .Rows(0).Item("InvoiceDate").ToString
            oInvoiceDueDate.Text = .Rows(0).Item("InvoiceDueDate").ToString
            lblInvoiceAmount.Text = FormatNumber(.Rows(0).Item("InvoiceAmount").ToString, 2)
            lblTotalPembiayaan.Text = FormatNumber(.Rows(0).Item("OutstandingPrincipal").ToString, 2)
            lblInterestAmount.Text = FormatNumber(.Rows(0).Item("InterestAmount").ToString, 2)
            lblRetensiAmount.Text = FormatNumber(.Rows(0).Item("InvoiceAmount").ToString - .Rows(0).Item("TotalPembiayaan").ToString, 2)
            lblDendaAmount.Text = FormatNumber(.Rows(0).Item("AmountDenda").ToString, 2)
            lbltotalTagihan.Text = FormatNumber(.Rows(0).Item("OutstandingPrincipal") + .Rows(0).Item("InterestAmount") + .Rows(0).Item("AmountDenda"), 2)
        End With
    End Sub
#End Region

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbSave.Click


        Dim TotalAllocation As Double
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then

            'With oPaymentAllocationDetail
            '    TotalAllocation = .InstallmentDue + .LCInstallment + .InstallmentCollFee +
            '                      .InsuranceDue + .LCInsurance + .InsuranceCollFee +
            '                      .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee +
            '                      .RepossessionFee + .Prepaid + .PLL
            '    If .InstallmentDue > .MaximumInstallment Then
            '        ShowMessage(lblMessage, "Total pembayaran harus <= total outstanding installment", True)
            '        Exit Sub
            '    End If
            '    If .LCInstallment > .MaximumLCInstallFee Then
            '        ShowMessage(lblMessage, "Total bayar denda harus <= total outstanding denda", True)
            '        Exit Sub
            '    End If

            'End With

            If Me.WayOfPayment = "CP" Then
                If TotalAllocation > Me.PrepaidBalance Then

                    ShowMessage(lblMessage, "Total Alokasi Harus <= Saldo Prepaid", True)
                    Exit Sub
                End If
            Else
                If (CDbl(principalPaid.Text) + CDbl(interestPaid.Text) + CDbl(DendaPaid.Text) + CDbl(RetensiPaid.Text)) > CDbl(lblAmountRec.Text) Then

                    ShowMessage(lblMessage, "Total Alokasi harus = Jumlah Terima", True)
                    Exit Sub
                End If
            End If
            'With oCustomClass2
            '    .strConnection = GetConnectionString()
            '.ValueDate = Me.ValueDate
            '.CoyID = Me.SesCompanyID

            'diganti karena branch pusat dibuka semua
            '.BranchId = Me.sesBranchId.Replace("'", "")
            '.BranchId = Me.BranchID.Replace("'", "")

            '.BusinessDate = Me.BusinessDate
            '.ReceivedFrom = "-"
            '.ReferenceNo = lblRefNo.Text
            '.WOP = "SS"
            '.LoginId = Me.Loginid
            '.BankAccountID = Me.BankAccount
            '.Notes = txtnotes.Text.Trim
            '.ApplicationID = Me.ApplicationID
            '.SuspendNo = Me.SuspendNo
            '-----------Installment
            '.InstallmentDue = oPaymentAllocationDetail.InstallmentDue
            '.InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

            '.LcInstallment = oPaymentAllocationDetail.LCInstallment
            '.LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

            '.InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
            '.InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
            ''------------------------

            ''----------Insurance
            '.InsuranceDue = oPaymentAllocationDetail.InsuranceDue
            '.InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

            '.LcInsurance = oPaymentAllocationDetail.LCInsurance
            '.LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

            '.InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
            '.InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
            ''-----------------------------

            '.PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
            '.PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

            '.STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
            '.STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

            '.InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
            '.InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

            '.RepossessionFee = oPaymentAllocationDetail.RepossessionFee
            '.RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

            '.Prepaid = oPaymentAllocationDetail.Prepaid
            '.PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim

            '.PLL = oPaymentAllocationDetail.PLL
            '.PLLDesc = oPaymentAllocationDetail.PLLDesc 

            ' If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            With oCustomClass2
                    .strConnection = GetConnectionString()
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .ApplicationID = Me.ApplicationID
                    .ValueDate = ConvertDate2(oValueDate.Text)
                    .CoyID = Me.SesCompanyID
                    .BusinessDate = Me.BusinessDate
                    '.ToAmountAllocate = lblTotalBiaya.Text
                    .AmountReceive = CDbl(principalPaid.Text) + CDbl(interestPaid.Text) + CDbl(RetensiPaid.Text) + CDbl(DendaPaid.Text)
                    .Notes = txtnotes.Text.Trim
                    .PaidPrincipal = CDbl(principalPaid.Text)
                    .PaidInterest = CDbl(interestPaid.Text)
                    .PaidRetensi = CDbl(RetensiPaid.Text)
                    .DendaPaid = CDbl(DendaPaid.Text)
                '.FormID = Me.FormID
                '.Status = "CREATE"
                .BankAccountID = Me.BankAccount
                    .ReceivedFrom = "-"
                    '.ReferenceNo = oPaymentDetail.ReferenceNo
                    .ReferenceNo = lblRefNo.Text
                    .WOP = "SS"
                    .LoginId = Me.Loginid
                '.InvoiceNo = lblInvoiceNo.Text
                '.InsSeqNo = Me.InvoiceSeqNo
                .SuspendNo = Me.SuspendNo
                '.AmountReceive = TotalAllocation
            End With

            'Session.Add("BankSelected", oPaymentDetail.BankAccount)


            Try
                oController2.SuspendAllocationFACT(oCustomClass2)
                Server.Transfer("SuspendAllocation.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    'Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
    '    Server.Transfer("suspendAllocation.aspx")
    'End Sub

End Class