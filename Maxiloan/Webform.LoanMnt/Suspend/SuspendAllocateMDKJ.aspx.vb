﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class SuspendAllocateMDKJ
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchby As UcSearchBy
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
    Private m_controller As New DataUserControlController
    Private d_Controller As New SuspendAllocationGeneralController
    Private dCustomClass As New Parameter.SuspendAllocationGeneral
#End Region

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property SuspendNo() As String
        Get
            Return (CType(ViewState("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SuspendNo") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "SUSPENDALLOCATION"

        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_INSERT_SUCCESS, False, True))
                End If

                Dim dtbranch As New DataTable

                'If Me.IsHoBranch Then
                '    dtbranch = m_controller.GetBranchName(GetConnectionString, "All")
                'Else
                '    dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                'End If
                dtbranch = m_controller.GetBranchName(GetConnectionString, "All")

                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                lblbranch.Text = Me.BranchName

                'tidak terpakai karena branch dibuka semua di kantor pusat
                Me.BranchID = Request.QueryString("branchid")
                Me.SuspendNo = Request.QueryString("SuspendNo")

                oSearchBy.ListData = "Name, Name-Agreementno, No Kontrak-Address, Alamat-InstallmentAmount, Angsuran"
                oSearchBy.BindData()

                cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Me.BranchID))

                Me.SearchBy = " branchid = '" & Me.BranchID.Replace("'", "").Trim & "'"
                Me.SortBy = ""

                '--------------------------------------------------------------------------------------------------
                'Modify by Wira 20161003, jika suspend transaction lebih dari 6 bulan, harus confirm ke accounting
                '--------------------------------------------------------------------------------------------------
                Dim IsBlock As Boolean
                With dCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID.Trim
                    .SuspendNo = Me.SuspendNo.Trim
                End With
                dCustomClass = d_Controller.GetIsBlock(dCustomClass)
                With dCustomClass
                    If .IsBlock = True Then
                        ShowMessage(lblMessage, "Nomor Suspend ini lebih dari 6 bulan, mohon confirm ke Accounting !!!", True)
                        Exit Sub
                    End If
                End With
                '--------------------------------------------------------------------------------------------------
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.AgreementListMDKJ(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        DtgAgree.DataSource = oCustomClass.ListAgreement

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As Label
        Dim lblTemp As Label
        Dim hyTemp As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)

            'diganti karena branch pusat bisa alokasi
            'hypReceive.NavigateUrl = "SuspendAllocateProc.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & Me.BranchID & "&SuspendNo=" & Me.SuspendNo

            hypReceive.NavigateUrl = "SuspendAllocateProcMDKJ.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & cboParent.SelectedValue.Trim & "&SuspendNo=" & Me.SuspendNo

            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblcustomerid"), Label)
            hyTemp = CType(e.Item.FindControl("lblName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"

            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
        End If
    End Sub
#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        'Me.SearchBy = ""
        'oSearchBy.Text = ""
        'oSearchBy.BindData()
        'DoBind(Me.SearchBy, Me.SortBy)
        If SessionInvalid() Then
            Exit Sub
        End If
        Server.Transfer("SuspendAllocate.aspx")
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            'Buka semua cabang
            'Me.SearchBy = " branchid = '" & Me.sesBranchId.Replace("'", "").Trim & "'"

            Me.SearchBy = " branchid = '" & cboParent.SelectedValue.Trim & "'"
            Me.SearchBy = Me.SearchBy & " and contractstatus in ('PRP','AKT','ICP', 'ICL', 'OSP', 'OSD','SSD','LNS')"

            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
            End If

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True

            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


End Class