﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SuspendInqView
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendReversalController

#End Region

#Region "Property"


    Private Property SuspendNo() As String
        Get
            Return (CType(Viewstate("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("SuspendNo") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "SUSPENDINQVIEW"

        If Not IsPostBack Then
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim lblgiro As New Label
                Dim lblnewStat As New Label
                Dim inFlagfile As New Label
                Me.FormID = "SUSPENDINQVIEW"

                Me.SuspendNo = Request.QueryString("SuspendNo")
                Me.BranchID = Request.QueryString("branchid")
                DoBind()
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        oCustomClass.SuspendNo = Me.SuspendNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID

        oCustomClass = oController.SuspendReverse(oCustomClass)
        With oCustomClass
            lblSuspendNo.Text = .SuspendNo
            lblPostingdate.Text = .postingdate.ToString("dd/MM/yyyy")
            lblBankaccount.Text = .bankAccountName
            lblRefno.Text = .ReferenceNo
            lblValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
            lblAmountRec.Text = FormatNumber(.AmountRec, 2)
            lblVoucherNo.Text = .voucherno
            lblDesc.Text = .description
            lblAgreement.Text = .Agreementno
            lblCustname.Text = .CustomerName
            lblstatus.Text = .suspendstatusdesc
            lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
            lblReceiveDate.Text = .postingdate.ToString("dd/MM/yyyy")
        End With

    End Sub
#End Region

End Class