﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class AssetReplacementExecuteList
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents UcVAddress As UcViewAddress

#Region "Constanta"
    Dim style As String = "ACCMNT"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True
    Dim oCustomClass As New Parameter.AssetReplacement
    Dim m_controller As New AssetDataController
    Dim m_controllerAR As New AssetReplacementController
#End Region
#Region "Property"
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum
    Private Property Mode() As ProcessMode
        Get
            Return (CType(viewstate("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            viewstate("Mode") = Value
        End Set
    End Property
    Property AssetRepSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetRepSeqNo"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetRepSeqNo") = Value
        End Set
    End Property
    Property AssetSeqno() As Integer
        Get
            Return CType(Viewstate("AssetSeqno"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqno") = Value
        End Set
    End Property
    Property Attr() As DataTable
        Get
            Return CType(Viewstate("Attr"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("Attr") = Value
        End Set
    End Property
    Property ListDoc() As DataTable
        Get
            Return CType(Viewstate("ListDoc"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("ListDoc") = Value
        End Set
    End Property
    Property RepSeqNo() As String
        Get
            Return viewstate("RepSeqNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("RepSeqNo") = Value
        End Set
    End Property
    Property SeqNo() As String
        Get
            Return viewstate("SeqNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SeqNo") = Value
        End Set
    End Property
    Property Key() As String
        Get
            Return viewstate("Key").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Key") = Value
        End Set
    End Property
    Property Address() As String
        Get
            Return viewstate("Address").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Address") = Value
        End Set
    End Property
    Property Kelurahan() As String
        Get
            Return viewstate("Kelurahan").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Kelurahan") = Value
        End Set
    End Property
    Property Kecamatan() As String
        Get
            Return viewstate("Kecamatan").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Kecamatan") = Value
        End Set
    End Property
    Property City() As String
        Get
            Return viewstate("City").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("City") = Value
        End Set
    End Property
    Property ZipCode() As String
        Get
            Return viewstate("ZipCode").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ZipCode") = Value
        End Set
    End Property
    Property ReasonName() As String
        Get
            Return viewstate("ReasonName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ReasonName") = Value
        End Set
    End Property
    Property ToBeApprov() As String
        Get
            Return viewstate("ToBeApprov").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ToBeApprov") = Value
        End Set
    End Property
    Property UcNotes() As String
        Get
            Return viewstate("UcNotes").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("UcNotes") = Value
        End Set
    End Property
    Property Name() As String
        Get
            Return viewstate("Name").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property

    Property TaxDate() As Date
        Get
            Return CType(Viewstate("TaxDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("TaxDate") = Value
        End Set
    End Property
    Property Notes() As String
        Get
            Return viewstate("Notes").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property

    Property OTR() As Decimal
        Get
            Return CType(Viewstate("OTR"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("OTR") = Value
        End Set
    End Property
    Property DP() As Decimal
        Get
            Return CType(Viewstate("DP"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("DP") = Value
        End Set
    End Property
    Property UsedNew() As String
        Get
            Return viewstate("UsedNew").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("UsedNew") = Value
        End Set
    End Property
    Property Usage() As String
        Get
            Return viewstate("Usage").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Usage") = Value
        End Set
    End Property
    Property AssetCodeID() As String
        Get
            Return viewstate("AssetCodeID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetCodeID") = Value
        End Set
    End Property
    Property AssetCode() As String
        Get
            Return viewstate("AssetCode").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetCode") = Value
        End Set
    End Property
    Property Bulan1() As Integer
        Get
            Return CType(Viewstate("Bulan1"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Bulan1") = Value
        End Set
    End Property
    Property Bulan() As String
        Get
            Return viewstate("Bulan").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Bulan") = Value
        End Set
    End Property
    Property Tahun() As Integer
        Get
            Return CType(Viewstate("Tahun"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Tahun") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return viewstate("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Property Serial1() As String
        Get
            Return viewstate("Serial1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Serial1") = Value
        End Set
    End Property
    Property Serial2() As String
        Get
            Return viewstate("Serial2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Serial2") = Value
        End Set
    End Property

    Property AssetTypeID() As String
        Get
            Return viewstate("AssetTypeID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property

    Property Desc() As String
        Get
            Return viewstate("Desc").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Desc") = Value
        End Set
    End Property
    Property lks() As String
        Get
            Return viewstate("lks").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("lks") = Value
        End Set
    End Property
    Property license() As String
        Get
            Return viewstate("license").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("license") = Value
        End Set
    End Property
    Property claimdate() As String
        Get
            Return viewstate("claimdate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("claimdate") = Value
        End Set
    End Property
    Property amount() As Double
        Get
            Return CType(Viewstate("amount"), Double)

        End Get
        Set(ByVal Value As Double)
            viewstate("amount") = Value
        End Set
    End Property
    Property AdmFee() As Double
        Get
            Return CType(Viewstate("AdmFee"), Double)

        End Get
        Set(ByVal Value As Double)
            viewstate("AdmFee") = Value
        End Set
    End Property

    Property insurance() As String
        Get
            Return viewstate("insurance").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("insurance") = Value
        End Set
    End Property


    Property AOID() As String
        Get
            Return viewstate("AOID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return viewstate("NU").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("NU") = Value
        End Set
    End Property
    Property App() As String
        Get
            Return viewstate("App").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("App") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property

    Property Asset() As String
        Get
            Return viewstate("Asset").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Asset") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ASSETREPEXEC"
            '  If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            Me.ApplicationID = Request.QueryString("Applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.AssetRepSeqNo = CType(Request.QueryString("assetrepseqno"), Integer)
            Me.AssetSeqno = CType(Request.QueryString("assetseqno"), Integer)
            Select Case Request.QueryString("mode")
                Case "0"
                    Me.Mode = ProcessMode.Cancel
                Case "1"
                    Me.Mode = ProcessMode.Execute
                Case "2"
                    Me.Mode = ProcessMode.View
            End Select
            Me.Key = Request.QueryString("assetrepno")

            InitializeForm()
            With oCustomClass
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .AssetReqNo = Me.AssetRepSeqNo
                .assetseqNo = Me.AssetSeqno
            End With
            oCustomClass = m_controllerAR.ViewAssetReplInquiry(oCustomClass)
            With oCustomClass
                lblRequestNo.Text = Me.AssetRepSeqNo.ToString
                lblReqDate.Text = .requestdate.ToString("dd/MM/yyyy")
                lblAgreementNo.Text = .Agreementno
                lblInsuranceCoy.Text = .insurancecoy
                lblDesc.Text = .txtSearch
                lblLKS.Text = .LKS
                lblCustName.Text = .CustomerName
                lblLicense.Text = .License
                lblClaimdate.Text = .ClaimDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.ClaimAmount, 2)
                lblSupplier.Text = .SupplierName
                lblOTR.Text = FormatNumber(.OTR, 2)
                lblDP.Text = FormatNumber(.DP, 2)
                lblAssetDesc.Text = .Description
                lblSerialNo1.Text = .Serial1
                lblSerialNo2.Text = .Serial2
                Me.CustomerID = .CustomerID
                Me.SupplierID = .SupplierID
                If .UsedNew = "N" Then lblNewUsed.Text = "New"
                Select Case .Usage
                    Case "C"
                        lblUsage.Text = "Commercial"
                    Case "N"
                        lblUsage.Text = "Non Commercial"
                    Case "S"
                        lblUsage.Text = "Semi Commercial"
                End Select

                lblYear.Text = .ManufacturingYear.ToString
                Select Case .ManufacturingMonth
                    Case 1
                        lblBulan.Text = "Januari"
                    Case 2
                        lblBulan.Text = "Februari"
                    Case 3
                        lblBulan.Text = "Maret"
                    Case 4
                        lblBulan.Text = "April"
                    Case 5
                        lblBulan.Text = "Mei"
                    Case 6
                        lblBulan.Text = "Juni"
                    Case 7
                        lblBulan.Text = "Juli"
                    Case 8
                        lblBulan.Text = "Agustus"
                    Case 9
                        lblBulan.Text = "September"
                    Case 10
                        lblBulan.Text = "Oktober"
                    Case 11
                        lblBulan.Text = "November"
                    Case 12
                        lblBulan.Text = "Desember"
                End Select
                dtgAttribute.DataSource = .DataAttribute
                dtgAttribute.DataBind()

                Label1.Text = .OwnerAsset
                UcVAddress.Address = .Address
                UcVAddress.RT = .Rt
                UcVAddress.RW = .Rw
                UcVAddress.AreaPhone1 = .AreaPhone1
                UcVAddress.Phone1 = .Phone1
                UcVAddress.AreaPhone2 = .AreaPhone2
                UcVAddress.Phone2 = .Phone2
                UcVAddress.AreaFax = .AreaFax
                UcVAddress.Fax = .Fax
                UcVAddress.Kelurahan = .Kelurahan
                UcVAddress.Kecamatan = .Kecamatan
                UcVAddress.City = .City
                UcVAddress.ZipCode = .ZipCode
                lblTaxDate.Text = .TaxDate.ToString("dd/MM/yyyy")
                lblUcNotes.Text = .ucNotes
                dtgAssetDoc.DataSource = .DataAssetdoc
                dtgAssetDoc.DataBind()
                lblFee.Text = FormatNumber(.AssetReplacementFee, 2)
                lblReason.Text = .ReasonID
                lblToBe.Text = .approvalby
                lblNotes.Text = .Notes
            End With

            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(Me.SupplierID.Trim) & "')"
        End If
    End Sub

    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub InitializeForm()
        If Me.Mode = ProcessMode.Cancel Then
            ButtonExecute.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonBack.Visible = False
        ElseIf Me.Mode = ProcessMode.Execute Then
            ButtonExecute.Visible = True
            ButtonCancel.Visible = True
            ButtonSave.Visible = False
            ButtonBack.Visible = False
        ElseIf Me.Mode = ProcessMode.View Then
            ButtonExecute.Visible = False
            ButtonCancel.Visible = False
            ButtonSave.Visible = False
            ButtonBack.Visible = True
        End If
    End Sub

#Region "Back Cancel"
    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Server.Transfer("AssetReplacementExecute.aspx")
    End Sub

    Private Sub ButtonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Server.Transfer("AssetReplacementExecute.aspx")
    End Sub
#End Region

#Region "Execute Process"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        'Untuk Proces Cancel
        ' If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
        Dim CustomClass As New Parameter.AssetReplacement
        Dim m_Controller As New AssetReplacementController
        With CustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetReqNo = Me.AssetRepSeqNo
            .assetseqNo = Me.AssetSeqno
            .BusinessDate = Me.BusinessDate
        End With
        Try
            m_Controller.SaveAssetReplacementCancel(CustomClass)
            Server.Transfer("AssetReplacementExecute.aspx")
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
        'End If
    End Sub

    Private Sub ButtonExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonExecute.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "EXEC", Me.AppId) Then
        Dim CustomClass As New Parameter.AssetReplacement
        Dim m_Controller As New AssetReplacementController
        With CustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetReqNo = Me.AssetRepSeqNo
            .assetseqNo = Me.AssetSeqno
            .BusinessDate = Me.BusinessDate
        End With
        Try
            m_Controller.SaveAssetReplacementExecute(CustomClass)
            Server.Transfer("AssetReplacementExecute.aspx")
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
        'End If
    End Sub
#End Region
End Class