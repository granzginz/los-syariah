﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetReplacementList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.AssetReplacementList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCAO" Src="../../Webform.UserController/UCAO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookupAssetWithAssetType" Src="../../Webform.UserController/ucLookupAssetWithAssetType.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpSupplier" Src="../../Webform.UserController/ucLookUpSupplier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetReplacementList</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewRequestNaNo(pStyle, pRequestNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/StopAccrued/StopAccruedView.aspx?style=' + pStyle + '&RequestNaNo=' + pRequestNo, 'RequestNoLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
    <script language="javascript" type="text/javascript">
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value;
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENGAJUAN PENGGANTIAN ASSET
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div class="form_left">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div class="form_left">
                        <label>
                            Perusahaan Asuransi</label>
                        <asp:Label ID="lblInsuranceCoy" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Polisi</label>
                        <asp:Label ID="lblLicense" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div class="form_left">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            tanggal Klaim</label>
                        <asp:Label ID="lblClaimdate" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div class="form_left">
                        <label>
                            No LKS</label>
                        <asp:Label ID="lblLKS" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Klaim</label>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ENTRY DATA ASSET
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Supplier</label>
                    <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
                    <uc1:uclookupsupplier id="UCSupplier" runat="server"></uc1:uclookupsupplier>
                    <asp:Label ID="lblreqPO" runat="server" ForeColor="Red">*)</asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Harga OTR (On The Road)</label>
                    <uc1:ucNumberFormat ID="txtOTR" runat="server" />  
                    <asp:Label ID="lblOTR" runat="server" Visible="False"></asp:Label>                  
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Uang Muka (DP)</label>
                    <uc1:ucNumberFormat ID="txtDP" runat="server" />    
                    <asp:Label ID="lblDP" runat="server" Visible="False"></asp:Label>                
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                </div>
            </div>
            <asp:Panel ID="pnl1" runat="server" Width="100%">
                <div class="form_button">
                    <asp:Button ID="ButtonOK" runat="server" CausesValidation="true" Text="Ok" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnl2" runat="server" Width="100%">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DATA ASSET
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <uc1:uclookupassetwithassettype id="UCAsset" runat="server"></uc1:uclookupassetwithassettype>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <asp:Label ID="lblSerial1" class="label_req" runat="server"></asp:Label>
                            <asp:TextBox ID="txtSerial1" runat="server" MaxLength="50"  Width="192px" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvChasisNo" runat="server" Display="Dynamic" ControlToValidate="txtSerial1"
                                ErrorMessage="Harap isi No Rangka" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Asset : Baru / Bekas</label>
                            <asp:Label ID="lblNewUsed" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <asp:Label ID="lblSerial2" class="label_req" runat="server"></asp:Label>
                            <asp:TextBox ID="txtSerial2" runat="server" MaxLength="50"  Width="192px" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEngineNo" runat="server" ControlToValidate="txtSerial2"
                                ErrorMessage="Harap isi No Mesin" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label class="label_req">
                                Penggunaan</label>
                            <asp:DropDownList ID="cboUsage" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="cboUsage" ErrorMessage="Harap Pilih Penggunaan" InitialValue="Select One"
                                CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tahun Produksi</label>
                        <asp:DropDownList ID="cboBulan" runat="server">
                            <asp:ListItem Value="0">SelectOne</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                        <font color="#000066">/</font>
                        <asp:TextBox ID="txtYear" runat="server" Columns="7" MaxLength="4" ></asp:TextBox>
                        <label  class="label_req"></label>
                        <asp:RequiredFieldValidator ID="RFVYear" runat="server" Display="Dynamic" ControlToValidate="txtYear"
                            ErrorMessage="Harap isi Tahun Prosuksi" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" Display="Dynamic" ControlToValidate="txtYear"
                            ErrorMessage="Tahun harus <= tahun ini" MinimumValue="0" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="txtYear" ErrorMessage="Tahun Salah" ValidationExpression="\d{4}"
                            CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgAttribute" runat="server" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" Width="100%" AutoGenerateColumns="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" Text='<%#Container.DataItem("Name")%>' runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemStyle HorizontalAlign="Left" Width="75%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAttribute" runat="server" Width="75%"  MaxLength='<%# DataBinder.eval(Container,"DataItem.AttributeLength") %>'>
                                            <label  class="label_req"></label>                                            
                                            </asp:TextBox>
                                            <asp:Label ID="lblVAttribute" runat="server" ForeColor="Red">Attribute already exists!</asp:Label>
                                            <asp:RegularExpressionValidator ID="RVAttribute" Enabled='<%# DataBinder.eval(Container,"DataItem.AttributeType") %>'
                                                runat="server" Display="Dynamic" ControlToValidate="txtAttribute" ErrorMessage="Harap isi dengan Angka"
                                                ValidationExpression="\d*" CssClass="validator_general">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="AttributeID"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            REGISTRASI ASSET
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama</label>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50"  Width="90%"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccompanyadress id="UCAddress" runat="server"></uc1:uccompanyadress>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tangal STNK</label>
                        <asp:TextBox ID="txtTax" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtTax_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtTax" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTax" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Catatan</label>
                        <asp:TextBox ID="txtAssetNotes" runat="server"  Width="305px" TextMode="MultiLine"
                            Height="54px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DOKUMEN ASSET
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgAssetDoc" runat="server" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                                <ItemStyle CssClass="tdganjil"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle Width="7%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AssetDocName" HeaderText="DOKUMEN">
                                        <HeaderStyle Width="20%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="NO DOKUMEN">
                                        <HeaderStyle Width="20%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNumber" runat="server" Width="95%" ></asp:TextBox>
                                            <label class="label_req"></label>
                                            <asp:Label ID="lblVNumber" runat="server" ForeColor="Red">Number already exists!</asp:Label>
                                            <asp:Label ID="lblVNumber2" runat="server" Visible='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                                ForeColor="Red">Number must be filled!</asp:Label>
                                            <asp:RequiredFieldValidator ID="RFVNumber" Enabled='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                                runat="server" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtNumber"
                                                Display="Dynamic" CssClass="validator_general">
                                            </asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PERIKSA">
                                        <HeaderStyle Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                            <asp:Label ID="lblVChk" runat="server" ForeColor="Red">Must be checked!</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <HeaderStyle Width="36%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNotes" runat="server" Width="95%" ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="MandatoryForNewAsset"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="MandatoryForUsedAsset"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="AssetDocID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="IsMainDoc"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="IsValueNeeded"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Biaya Administrasi</label>
                        <uc1:ucNumberFormat ID="txtAdmFee" runat="server" />                        
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucapprovalrequest id="UcAppReq" runat="server"></uc1:ucapprovalrequest>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
