﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class AssetReplacementView
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents UCAppReq As ucApprovalRequest
    Protected WithEvents UcVAddress As UcViewAddress

#Region "Constanta"
    Dim style As String = "ACCMNT"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True
    Dim oCustomClass As New Parameter.AssetReplacement
    Dim m_controller As New AssetDataController
    Dim m_controllerAR As New AssetReplacementController
#End Region
#Region "Property"
    Property Attr() As DataTable
        Get
            Return CType(Viewstate("Attr"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("Attr") = Value
        End Set
    End Property
    Property ListDoc() As DataTable
        Get
            Return CType(Viewstate("ListDoc"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("ListDoc") = Value
        End Set
    End Property
    Property Address() As String
        Get
            Return viewstate("Address").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Address") = Value
        End Set
    End Property

    Property RT() As String
        Get
            Return viewstate("RT").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("RT") = Value
        End Set
    End Property
    Property RW() As String
        Get
            Return viewstate("RW").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("RW") = Value
        End Set
    End Property
    Property AreaPhone1() As String
        Get
            Return viewstate("AreaPhone1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AreaPhone1") = Value
        End Set
    End Property
    Property Phone1() As String
        Get
            Return viewstate("Phone1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Phone1") = Value
        End Set
    End Property
    Property AreaPhone2() As String
        Get
            Return viewstate("AreaPhone2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AreaPhone2") = Value
        End Set
    End Property
    Property Phone2() As String
        Get
            Return viewstate("Phone2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Phone2") = Value
        End Set
    End Property
    Property AreaFax() As String
        Get
            Return viewstate("AreaFax").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AreaFax") = Value
        End Set
    End Property
    Property Fax() As String
        Get
            Return viewstate("Fax").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Fax") = Value
        End Set
    End Property
    Property Kelurahan() As String
        Get
            Return viewstate("Kelurahan").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Kelurahan") = Value
        End Set
    End Property
    Property Kecamatan() As String
        Get
            Return viewstate("Kecamatan").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Kecamatan") = Value
        End Set
    End Property
    Property City() As String
        Get
            Return viewstate("City").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("City") = Value
        End Set
    End Property
    Property ZipCode() As String
        Get
            Return viewstate("ZipCode").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ZipCode") = Value
        End Set
    End Property
    Property ReasonName() As String
        Get
            Return viewstate("ReasonName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ReasonName") = Value
        End Set
    End Property
    Property ReasonID() As String
        Get
            Return viewstate("ReasonID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ReasonID") = Value
        End Set
    End Property
    Property ToBeApprov() As String
        Get
            Return viewstate("ToBeApprov").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ToBeApprov") = Value
        End Set
    End Property
    Property UcNotes() As String
        Get
            Return viewstate("UcNotes").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("UcNotes") = Value
        End Set
    End Property
    Property Name() As String
        Get
            Return viewstate("Name").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property

    Property TaxDate() As Date
        Get
            Return CType(Viewstate("TaxDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("TaxDate") = Value
        End Set
    End Property
    Property Notes() As String
        Get
            Return viewstate("Notes").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property

    Property OTR() As Decimal
        Get
            Return CType(Viewstate("OTR"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("OTR") = Value
        End Set
    End Property
    Property DP() As Decimal
        Get
            Return CType(Viewstate("DP"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("DP") = Value
        End Set
    End Property
    Property UsedNew() As String
        Get
            Return viewstate("UsedNew").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("UsedNew") = Value
        End Set
    End Property
    Property Usage() As String
        Get
            Return viewstate("Usage").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Usage") = Value
        End Set
    End Property
    Property AssetCodeID() As String
        Get
            Return viewstate("AssetCodeID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetCodeID") = Value
        End Set
    End Property
    Property AssetCode() As String
        Get
            Return viewstate("AssetCode").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetCode") = Value
        End Set
    End Property
    Property Bulan1() As Integer
        Get
            Return CType(Viewstate("Bulan1"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Bulan1") = Value
        End Set
    End Property
    Property Bulan() As String
        Get
            Return viewstate("Bulan").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Bulan") = Value
        End Set
    End Property
    Property Tahun() As Integer
        Get
            Return CType(Viewstate("Tahun"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Tahun") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return viewstate("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Property SerialNo1() As String
        Get
            Return viewstate("SerialNo1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SerialNo1") = Value
        End Set
    End Property
    Property SerialNo2() As String
        Get
            Return viewstate("SerialNo2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SerialNo2") = Value
        End Set
    End Property
    Property Serial1() As String
        Get
            Return viewstate("Serial1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Serial1") = Value
        End Set
    End Property
    Property Serial2() As String
        Get
            Return viewstate("Serial2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Serial2") = Value
        End Set
    End Property

    Property AssetTypeID() As String
        Get
            Return viewstate("AssetTypeID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property

    Property Desc() As String
        Get
            Return viewstate("Desc").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Desc") = Value
        End Set
    End Property
    Property lks() As String
        Get
            Return viewstate("lks").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("lks") = Value
        End Set
    End Property
    Property license() As String
        Get
            Return viewstate("license").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("license") = Value
        End Set
    End Property
    Property claimdate() As String
        Get
            Return viewstate("claimdate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("claimdate") = Value
        End Set
    End Property
    Property amount() As Double
        Get
            Return CType(Viewstate("amount"), Double)

        End Get
        Set(ByVal Value As Double)
            viewstate("amount") = Value
        End Set
    End Property
    Property AdmFee() As Double
        Get
            Return CType(Viewstate("AdmFee"), Double)

        End Get
        Set(ByVal Value As Double)
            viewstate("AdmFee") = Value
        End Set
    End Property

    Property insurance() As String
        Get
            Return viewstate("insurance").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("insurance") = Value
        End Set
    End Property


    Property AOID() As String
        Get
            Return viewstate("AOID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return viewstate("NU").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("NU") = Value
        End Set
    End Property
    Property App() As String
        Get
            Return viewstate("App").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("App") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property

    Property Asset() As String
        Get
            Return viewstate("Asset").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Asset") = Value
        End Set
    End Property
    Property ApprovalStatus() As String
        Get
            Return viewstate("ApprovalStatus").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalStatus") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        oCustomClass = CType(HttpContext.Current.Items("Replacement"), Parameter.AssetReplacement)
        Me.ApprovalStatus = ""
        If Not Page.IsPostBack Then

            With oCustomClass
                Me.AgreementNo = .Agreementno
                Me.insurance = .insurancecoy
                Me.Desc = .Description
                Me.lks = .LKS
                Me.CustomerName = .CustomerName
                Me.license = .License
                Me.claimdate = .ClaimDate.ToString("yyyyMMdd")
                Me.amount = .ClaimAmount
                Me.SupplierID = .SupplierID
                Me.Supplier = .SupplierName
                Me.OTR = .OTR
                Me.DP = .DP
                Me.Serial1 = .Serial1
                Me.Serial2 = .Serial2
                Me.Bulan = .Bulan
                Me.Tahun = .Year
                Me.AssetCode = .AssetCode
                Me.UsedNew = .UsedNew
                Me.Usage = .Usage
                Me.Name = .Name
                Me.CustomerID = .CustomerID
                UcVAddress.Address = .Address
                UcVAddress.Kelurahan = .Kelurahan
                UcVAddress.Kecamatan = .Kecamatan
                UcVAddress.City = .City
                UcVAddress.ZipCode = .ZipCode
                UcVAddress.RT = .Rt
                UcVAddress.RW = .Rw
                UcVAddress.AreaPhone1 = .AreaPhone1
                UcVAddress.Phone1 = .Phone1
                UcVAddress.AreaPhone2 = .AreaPhone2
                UcVAddress.Phone2 = .Phone2
                UcVAddress.AreaFax = .AreaFax
                UcVAddress.Fax = .Fax
                Me.TaxDate = .TaxDate
                Me.Notes = .Notes
                Me.Attr = .ListAttribute
                Me.AdmFee = .AdmFee
                Me.ReasonName = .ReasonDescription
                Me.ReasonID = .ReasonID
                Me.ToBeApprov = .approvalby
                Me.UcNotes = .ucNotes
                Me.ListDoc = .listdata
                Me.ApplicationID = .ApplicationID
                Me.Bulan1 = CType(.txtSearch, Integer)
                Me.Address = .Address
                Me.RT = .rt
                Me.RW = .Rw
                Me.AreaPhone1 = .AreaPhone1
                Me.Phone1 = .Phone1
                Me.AreaPhone2 = .AreaPhone2
                Me.Phone2 = .Phone2
                Me.AreaFax = .AreaFax
                Me.Fax = .Fax
                Me.Kelurahan = .Kelurahan
                Me.Kecamatan = .Kecamatan
                Me.City = .City
                Me.ZipCode = .ZipCode
                Me.AssetCodeID = .AssetCodeID
                Me.AssetTypeID = .AssetTypeID
                Me.SerialNo1 = .SerialNo1
                Me.SerialNo2 = .serialNo2
            End With

        End If
        lblSerial1.Text = Me.SerialNo1
        lblSerial2.Text = Me.SerialNo2
        lblAgreementNo.Text = Me.AgreementNo
        lblInsuranceCoy.Text = Me.insurance
        lblDesc.Text = Me.Desc
        lblLKS.Text = Me.lks
        lblCustName.Text = Me.CustomerName
        lblLicense.Text = Me.license
        lblClaimdate.Text = Me.claimdate
        lblAmount.Text = FormatNumber(Me.amount, 2)
        lblSupplier.Text = Me.Supplier
        lblOTR.Text = CType(Me.OTR, String)
        lblDP.Text = CType(Me.DP, String)
        lblSerialNo1.Text = Me.Serial1
        lblSerialNo2.Text = Me.Serial2
        lblAssetDesc.Text = Me.AssetCode
        lblBulan.Text = Me.Bulan
        lblYear.Text = CType(Me.Tahun, String)
        lblNewUsed.Text = Me.UsedNew
        lblUsage.Text = Me.Usage
        lblTaxDate.Text = CType(Me.TaxDate, String)
        lblNotes.Text = Me.Notes
        lblName.Text = Me.Name
        lblFee.Text = FormatNumber(Me.AdmFee, 2)
        lblReason.Text = Me.ReasonName
        lblToBe.Text = Me.ToBeApprov
        lblUcNotes.Text = Me.UcNotes

        dtgAttribute.DataSource = Me.Attr
        dtgAttribute.DataBind()

        dtgAssetDoc.DataSource = Me.ListDoc
        dtgAssetDoc.DataBind()
        lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(Me.SupplierID.Trim) & "')"
        CheckAssetReplacementApprovalStatus()
        If Me.ApprovalStatus = "R" Then
            
            ShowMessage(lblMessage, "Kontrak sudah diajukan untuk penggantian Asset, Proses tidak dapat dilakukan", True)
            ButtonSave.Visible = False
        Else
            ButtonSave.Visible = True
        End If

    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click


        Try

            Dim oCustomClass As New Parameter.AssetReplacement
            Dim oAddress As New Parameter.Address
            Dim oData1 As New DataTable
            Dim oData2 As New DataTable
            Dim i, j As Integer


            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .ApplicationID = Me.ApplicationID
                .LoginId = Me.Loginid
                .assetseqNo = 1
                .requestdate = Me.BusinessDate
                .Notes = Me.Notes
                .ReasonTypeID = "ASRPL"
                .ReasonID = Me.ReasonID
                .AssetCode = Me.AssetCodeID
                .Serial1 = Me.Serial1
                .Serial2 = Me.Serial2
                .AssetUsage = Me.Usage
                .UsedNew = Me.UsedNew
                .OwnerAsset = Me.Name
                .AssetReplacementFee = Me.AdmFee
                .AssetReplacementFeePaid = 1
                .IsSendByEmail = 1
                .IsSendByFax = 1
                .issendbysms = 1
                .approvalorderid = Me.ToBeApprov
                .approvaldate = Me.BusinessDate
                .approvalstatus = "R"
                .voucherNo = "-"
                .TaxDate = Me.TaxDate
                .approvalby = Me.ToBeApprov
                .InsuranceStatus = "-"
                .approvalrecomendation = "-"
                .ManufacturingMonth = Me.Bulan1
                .ManufacturingYear = Me.Tahun
                oData1 = Me.Attr
                oData2 = Me.ListDoc
                oAddress.Address = Me.Address
                oAddress.Kelurahan = Me.Kelurahan
                oAddress.Kecamatan = Me.Kecamatan
                oAddress.City = Me.City
                oAddress.ZipCode = Me.ZipCode
                oAddress.RT = Me.RT
                oAddress.RW = Me.RW
                oAddress.AreaPhone1 = Me.AreaPhone1
                oAddress.Phone1 = Me.Phone1
                oAddress.AreaPhone2 = Me.AreaPhone2
                oAddress.Phone2 = Me.Phone2
                oAddress.AreaFax = Me.AreaFax
                oAddress.Fax = Me.Fax
                .SupplierID = Me.SupplierID
                .AssetTypeID = Me.AssetTypeID
                .OTR = Me.OTR
                .DP = Me.DP
            End With
            m_controllerAR.AssetReplacementSaveAdd(oCustomClass, oAddress, oData1, oData2)
            Response.Redirect("AssetReplacementRequest.aspx")
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("AssetReplacementList.aspx.vb", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)

            ShowMessage(lblMessage, "Data Asset sudah ada " & ex.StackTrace, True)
        End Try

    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("AssetReplacementRequest.aspx")
    End Sub
#Region "CheckAssetReplacementApprovalStatus"
    Public Sub CheckAssetReplacementApprovalStatus()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spAssetReplacementCheckApprovalStatus"

            objCommand.Parameters.Add("@AplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@ApprovalStatus", SqlDbType.Char, 1).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.ApprovalStatus = CStr(IIf(IsDBNull(objCommand.Parameters("@ApprovalStatus").Value), "", objCommand.Parameters("@ApprovalStatus").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

#End Region

End Class