﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetReplacementExecute.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.AssetReplacementExecute" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetReplacementExecute</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        EKSEKUSI PENGGANTIAN ASSET
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Efektif</label>
                        <asp:TextBox ID="txtEffectiveDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtEffectiveDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtEffectiveDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtEffectiveDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
                <asp:Panel ID="pnlDtGrid" runat="server">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                DAFTAR KONTRAK UNTUK PENGGANTIAN ASSET
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgAssetReplacementExecution" runat="server" Width="100%" OnSortCommand="SortGrid"
                                    CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                    AllowSorting="True">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="PILIH">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="imbExecute" runat="server" Text="EKSEKUSI"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PILIH">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="imbCancel" runat="server" Text="BATAL"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="AssetRepSeqno" HeaderText="NO REQUEST">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lblPrepaymentNo" runat="server" Text='<%#Container.DataItem("AssetRepSeqNo")%>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                                </asp:HyperLink>
                                                <asp:Label ID="lblCustomerID" runat="server" Text='<%#container.dataitem("CustomerID")%>'
                                                    Visible="False">
                                                </asp:Label>
                                                <asp:Label ID="lblBranchID" runat="server" Text='<%#container.dataitem("BranchID")%>'
                                                    Visible="False">
                                                </asp:Label>
                                                <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#container.dataitem("AssetSeqno")%>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="AssetReplacementFee" HeaderText="JUMLAH">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountToBePaid" runat="server" Text='<%#Formatnumber(Container.DataItem("AssetReplacementFee"),2)%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="EffectiveDate" HeaderText="TGL EFEKTIF">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEffectiveDate" runat="server" Text='<%#container.dataitem("EffectiveDate")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="Application ID" Visible="False">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
