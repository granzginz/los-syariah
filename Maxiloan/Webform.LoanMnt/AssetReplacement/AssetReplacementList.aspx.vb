﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class AssetReplacementList
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents uscTax As ValidDate
    Protected WithEvents UCAddress As UcCompanyAddress
    Protected WithEvents UCAppReq As ucApprovalRequest
    Protected WithEvents UCSupplier As ucLookUpSupplier
    Protected WithEvents UCAsset As ucLookupAssetWithAssetType
    Protected WithEvents txtOTR As ucNumberFormat
    Protected WithEvents txtDP As ucNumberFormat
    Protected WithEvents txtAdmFee As ucNumberFormat

#Region "Constanta"
    Dim style As String = "ACCMNT"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True
    Dim m_controller As New AssetDataController
    Dim m_controllerAR As New AssetReplacementController
#End Region
#Region "Property"
    Property SupplierID() As String
        Get
            Return viewstate("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Property Serial1() As String
        Get
            Return viewstate("Serial1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Serial1") = Value
        End Set
    End Property
    Property Serial2() As String
        Get
            Return viewstate("Serial2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Serial2") = Value
        End Set
    End Property

    Property AssetTypeID() As String
        Get
            Return viewstate("AssetTypeID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property
    Property Desc() As String
        Get
            Return viewstate("Desc").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Desc") = Value
        End Set
    End Property
    Property Lks() As String
        Get
            Return viewstate("lks").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("lks") = Value
        End Set
    End Property
    Property License() As String
        Get
            Return viewstate("license").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("license") = Value
        End Set
    End Property
    Property Claimdate() As String
        Get
            Return viewstate("claimdate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("claimdate") = Value
        End Set
    End Property
    Property Amount() As Double
        Get
            Return CType(Viewstate("amount"), Double)

        End Get
        Set(ByVal Value As Double)
            viewstate("amount") = Value
        End Set
    End Property
    Property Insurance() As String
        Get
            Return viewstate("insurance").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("insurance") = Value
        End Set
    End Property

    Property Bulan1() As String
        Get
            Return viewstate("Bulan1").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Bulan1") = Value
        End Set
    End Property

    Property AOID() As String
        Get
            Return viewstate("AOID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return viewstate("NU").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("NU") = Value
        End Set
    End Property

    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property

    Property Asset() As String
        Get
            Return viewstate("Asset").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Asset") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        'Me.Supplier = UCSupplier.SupplierID
        If Not Page.IsPostBack Then
            With txtOTR
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With
            With txtDP
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With
            With txtAdmFee
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With

            GetCookies()
            GetAssetType()
            lblMessage.Text = ""
            InitialLabelValid()
            If Request("Application") <> "" Then
                Dim script As String
                script = "<script language=""JavaScript"">" & vbCrLf
                script &= "alert('Your Application Form No. is " & Request("Application").ToString.Trim & "');" & vbCrLf
                script &= "</script>"
                Response.Write(script)
            End If
            lblSerial1.Text = "Chasis No."
            lblSerial2.Text = "Engine No."
            UCSupplier.ApplicationId = Me.ApplicationID
            'UCSupplier.Style = style
            UCSupplier.bindData()
            UCAsset.ApplicationId = Me.ApplicationID
            UCAsset.BranchID = Replace(Me.sesBranchId, "'", "")
            UCAsset.AssetTypeId = Me.AssetTypeID
            UCAsset.Style = style
            UCAsset.BindData()
            UCAddress.Style = style
            UCAddress.BindAddress()
            UCAddress.ValidatorFalse()
            lblAgreementNo.Text = Me.AgreementNo
            lblInsuranceCoy.Text = Me.Insurance
            lblLicense.Text = Me.License
            lblLKS.Text = Me.Lks
            lblDesc.Text = Me.Desc
            lblClaimdate.Text = Me.Claimdate
            lblAmount.Text = FormatNumber(Me.Amount, 2).ToString
            lblCustName.Text = Me.CustomerName
            uscTax.isCalendarPostBack = False
            uscTax.FieldRequiredMessage = "Harap isi Tanggal STNK"
            uscTax.ValidationErrMessage = "Harap isi Tanggal STNK dengan format dd/MM/yyyy"
            uscTax.Display = "Dynamic"
            If Request("page") = "Edit" Then
                Panel2()
            Else
                InitialPanel()
            End If
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            UCAppReq.ReasonTypeID = "ASRPL"
            UCAppReq.ApprovalScheme = "ASRP"
        End If
    End Sub
#Region "Initial Label Validator"
    Sub InitialLabelValid()
        Dim count As Integer
        count = CInt(IIf(dtgAssetDoc.Items.Count > dtgAttribute.Items.Count, dtgAssetDoc.Items.Count, dtgAttribute.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label).Visible = False
            End If
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label).Visible = False
            End If
        Next
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("AssetReplacement")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.CustomerID = cookie.Values("Custid")
        Me.CustomerName = cookie.Values("name")
        Me.Asset = cookie.Values("AssetID")
        Me.agreementNo = cookie.Values("AgreementNo")
        Me.Insurance = cookie.Values("InsuranceCoy")
        Me.Desc = cookie.Values("Desc")
        Me.Lks = cookie.Values("LKS")
        Me.License = cookie.Values("License")
        Me.Claimdate = cookie.Values("Claimdate")
        Me.Amount = CType(cookie.Values("Amount"), Double)

    End Sub
#End Region
#Region "Panel"
    Sub InitialPanel()
        pnl1.Visible = True
        pnl2.Visible = False
        lblSupplier.Visible = False
        UCSupplier.Visible = True
        lblreqPO.Visible = True
        'lblreqPO2.Visible = True
    End Sub
    Sub Panel2()
        pnl1.Visible = False
        pnl2.Visible = True
        lblSupplier.Visible = True
        UCSupplier.Visible = False
        lblreqPO.Visible = False
        'lblreqPO2.Visible = False
    End Sub
#End Region
#Region "FillCbo"
    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
#End Region
#Region "BindAsset, BindAttribute, GetAO, & GetSerial"
    Sub BindAsset()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AssetID = Me.AssetTypeID
        oAssetData = m_controller.GetAssetDoc(oAssetData)
        oData = oAssetData.ListData
        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()
    End Sub
    Sub BindAttribute()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AssetID = Me.AssetTypeID
        oAssetData = m_controller.GetAttribute(oAssetData)
        oData = oAssetData.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub
    Sub GetSerial()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AssetID = Me.AssetTypeID
        oAssetData = m_controller.GetSerial(oAssetData)
        oData = oAssetData.ListData
        lblSerial1.Text = oData.Rows(0).Item(0).ToString
        lblSerial2.Text = oData.Rows(0).Item(1).ToString
        '  Me.NU = oData.Rows(0).Item(2).ToString
        oAssetData.strConnection = GetConnectionString
        oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        oAssetData.AppID = Me.ApplicationID
        oAssetData = m_controller.GetUsedNew(oAssetData)
        oData2 = oAssetData.ListData
        Me.NU = oData2.Rows(0).Item(0).ToString
        lblNewUsed.Text = IIf(Me.NU.ToUpper.Trim = "N", "New", "Used").ToString

    End Sub
    Sub GetAssetType()
        Dim oAssetRep As New Parameter.AssetReplacement
        oAssetRep.strConnection = GetConnectionString
        oAssetRep.AssetID = Me.Asset
        oAssetRep.ApplicationID = Me.ApplicationID
        oAssetRep = m_controllerAR.GetAssetRepSerial(oAssetRep)
        With oAssetRep
            Me.AssetTypeID = .AssetTypeID
            Me.BranchID = .BranchId
            Me.ApplicationID = .ApplicationID
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.SupplierID = .SupplierID
            Me.Serial1 = .Serial1
            Me.Serial2 = .Serial2
        End With

    End Sub
    Sub GetAO()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        oAssetData.SupplierID = Me.Supplier
        oAssetData = m_controller.GetAO(oAssetData)
        oData = oAssetData.ListData
        If oData.Rows.Count > 0 Then
            'lblAO.EmployeeName = oData.Rows(0).Item(0).ToString.Trim
            'lblAO.EmployeeID = oData.Rows(0).Item(1).ToString.Trim
            Me.AOID = oData.Rows(0).Item(1).ToString.Trim
        End If
    End Sub
#End Region
#Region "ImbOK, ImbCancel click"
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        'lblSupplier.Text = UCSupplier.SupplierName
        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Dim WhereSup As String = "SupplierID = '" & Me.Supplier & "'"
        'Me.Supplier = UCSupplier.SupplierID
        UCSupplier.Visible = False
        lblSupplier.Visible = True
        lblreqPO.Visible = False
        RangeValidator3.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        lblOTR.Text = txtOTR.Text
        lblDP.Text = txtDP.Text
        txtOTR.Visible = False
        txtDP.Visible = False
        lblOTR.Visible = True
        lblDP.Visible = True
        Panel2()
        '----
        'GetAssetType()
        BindAsset()
        BindAttribute()
        GetSerial()
        GetAO()
        FillCbo(cboUsage, "TblAssetUsage")
        InitialLabelValid()
        lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(Me.Supplier.Trim) & "')"
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("AssetReplacementRequest.aspx")
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
#End Region
#Region "Validator & save"
    Sub Validator(ByVal oData2 As DataTable, ByVal oData3 As DataTable)
        Dim ErrS1 As Boolean = False
        Dim ErrS2 As Boolean = False

        ' If cboPaidBy.SelectedValue = "Select One" And cboInsuredBy.SelectedValue <> "CU" Then
        'lblVPaidBy.Visible = True
        'status = False
        'End If
        If CDec(txtOTR.Text) < CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text.Trim)) Then
            status = False
            ShowMessage(lblMessage, "Harga OTR harus > Uang Muka", True)
            Exit Sub
        End If

        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        If txtSerial1.Text.Trim <> "" Or txtSerial2.Text.Trim <> "" Then
            oAssetData.strConnection = GetConnectionString
            oAssetData.Serial1 = txtSerial1.Text
            oAssetData.Serial2 = txtSerial2.Text
            oAssetData.AssetID = Me.Asset
            oAssetData.AppID = Me.ApplicationID
            oAssetData = m_controller.CheckSerial(oAssetData)
            oData = oAssetData.ListData
            If oData.Rows.Count <> 0 Then
                oData = oAssetData.ListData
                Dim intLoop As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If txtSerial1.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(0).ToString.Trim = txtSerial1.Text.Trim Then
                            ErrS1 = True
                        End If
                    End If
                    If txtSerial2.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(1).ToString.Trim = txtSerial2.Text.Trim Then
                            ErrS2 = True
                        End If
                    End If
                Next
                If ErrS1 = True And ErrS2 = True Then

                    ShowMessage(lblMessage, "No Rangka dan No Mesin Sudah Ada", True)
                    status = False
                Else
                    If ErrS1 = True Then

                        ShowMessage(lblMessage, "No rangka Sudah ada", True)
                        status = False
                    ElseIf ErrS2 = True Then

                        ShowMessage(lblMessage, "No Mesin sudah ada", True)
                        status = False
                    End If
                End If
            End If
        End If
        Dim AttributeId As String
        Dim txtAttribute As TextBox
        Dim lblVAttribute, lblVNumber, lblVNumber2 As Label
        Dim count As Integer
        Dim lblvChk As Label
        Dim chk As CheckBox
        Dim MandatoryNew, MandatoryUsed As String
        Dim txtnumber As TextBox
        Dim isValueNeeded, value As String
        count = CInt(IIf(dtgAttribute.Items.Count > dtgAssetDoc.Items.Count, dtgAttribute.Items.Count, dtgAssetDoc.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                AttributeId = dtgAttribute.Items(intLoop).Cells(2).Text.Trim
                txtAttribute = CType(dtgAttribute.Items(intLoop).FindControl("txtAttribute"), TextBox)
                lblVAttribute = CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label)
                If AttributeId = "LICPLATE" And txtAttribute.Text.Trim <> "" Then
                    oAssetData = New Parameter.AssetData
                    oData = New DataTable
                    oAssetData.strConnection = GetConnectionString
                    oAssetData.Input = txtAttribute.Text
                    oAssetData.AssetID = Me.Asset
                    oAssetData = m_controller.CheckAttribute(oAssetData)
                    oData = oAssetData.ListData
                    If oData.Rows.Count > 0 Then
                        lblVAttribute.Visible = True
                        If status <> False Then
                            status = False
                        End If
                    End If
                End If
                objrow = oData2.NewRow
                objrow("AttributeID") = AttributeId
                objrow("AttributeContent") = txtAttribute.Text.Trim
                oData2.Rows.Add(objrow)
            End If
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                chk = CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox)
                lblvChk = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label)
                lblVNumber = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label)
                lblVNumber2 = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label)
                value = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                isValueNeeded = dtgAssetDoc.Items(intLoop).Cells(9).Text.Trim
                MandatoryNew = dtgAssetDoc.Items(intLoop).Cells(5).Text.Trim
                MandatoryUsed = dtgAssetDoc.Items(intLoop).Cells(6).Text.Trim
                If Me.NU = "N" And MandatoryNew = "1" Then
                    If chk.Checked = False Then
                        If MandatoryNew = "1" Then
                            lblvChk.Visible = True
                        Else
                            lblvChk.Visible = False
                        End If
                        If status <> False Then
                            status = False
                        End If
                    Else
                        If isValueNeeded = "True" And value = "" Then
                            lblVNumber2.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        End If
                    End If
                ElseIf Me.NU = "U" And MandatoryUsed = "1" Then
                    If chk.Checked = False Then
                        If MandatoryUsed = "1" Then
                            lblvChk.Visible = True
                        Else
                            lblvChk.Visible = False
                        End If
                        If status <> False Then
                            status = False
                        End If
                    Else
                        If isValueNeeded = "True" And value = "" Then
                            lblVNumber2.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        End If
                    End If
                End If
                txtnumber = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox)
                If txtnumber.Text.Trim <> "" Then
                    oAssetData = New Parameter.AssetData
                    oData = New DataTable
                    oAssetData.strConnection = GetConnectionString
                    oAssetData.Input = txtnumber.Text.Trim
                    oAssetData.AssetDocID = dtgAssetDoc.Items(intLoop).Cells(7).Text.Trim
                    oAssetData.AssetID = Me.Asset
                    oAssetData = m_controller.CheckAssetDoc(oAssetData)
                    oData = oAssetData.ListData
                    If oData.Rows.Count > 0 Then
                        lblVNumber.Visible = True
                        If status <> False Then
                            status = False
                        End If
                    End If
                End If
                objrow = oData3.NewRow
                objrow("AssetDocID") = dtgAssetDoc.Items(intLoop).Cells(7).Text.Trim
                objrow("DocumentNo") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                objrow("IsMainDoc") = dtgAssetDoc.Items(intLoop).Cells(8).Text.Trim
                objrow("IsDocExist") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox).Checked = True, "1", "0").ToString
                objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                oData3.Rows.Add(objrow)
            End If
        Next
        If status = False Then
            Exit Sub
        End If
    End Sub
#End Region

    Private Sub imbNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        Dim AttributeId As String
        Dim lblAttrName As Label
        Dim txtAttribute As Label
        Dim ocustomclass As New Parameter.AssetReplacement
        Dim oAssetdata As New Parameter.AssetData
        Dim oAddress As New Parameter.Address
        Dim i, j As Integer
        Dim dtAttribute As New DataTable
        Dim dtAssetDoc As New DataTable
        Dim drAttr, drAssetDoc As DataRow
        With dtAttribute
            .Columns.Add("AttributeID", System.Type.GetType("System.String"))
            .Columns.Add("AttributeName", System.Type.GetType("System.String"))
            .Columns.Add("Attribute", System.Type.GetType("System.String"))
        End With
        With dtAssetDoc
            .Columns.Add("No", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocName", System.Type.GetType("System.String"))
            .Columns.Add("Number", System.Type.GetType("System.String"))
            .Columns.Add("chk", System.Type.GetType("System.String"))
            .Columns.Add("Notes", System.Type.GetType("System.String"))
            .Columns.Add("MandatoryForNewAsset", System.Type.GetType("System.String"))
            .Columns.Add("MandatoryForUsedAsset", System.Type.GetType("System.String"))
            .Columns.Add("AssetDocID", System.Type.GetType("System.String"))
            .Columns.Add("IsMainDoc", System.Type.GetType("System.String"))
            .Columns.Add("IsValueNeeded", System.Type.GetType("System.String"))
        End With

        For i = 0 To dtgAttribute.Items.Count - 1
            drAttr = dtAttribute.NewRow
            If Not dtAttribute Is Nothing Then
                drAttr("AttributeId") = dtgAttribute.Items(i).Cells(2).Text.Trim
                drAttr("AttributeName") = CType(dtgAttribute.Items(i).FindControl("lblName"), Label).Text
                drAttr("Attribute") = CType(dtgAttribute.Items(i).FindControl("txtAttribute"), TextBox).Text
                dtAttribute.Rows.Add(drAttr)
            End If
        Next

        For j = 0 To dtgAssetDoc.Items.Count - 1
            drAssetDoc = dtAssetDoc.NewRow
            If Not dtAssetDoc Is Nothing Then
                drAssetDoc("No") = CType(dtgAssetDoc.Items(j).FindControl("lblNo"), Label).Text   'dtgAssetDoc.Items(j).Cells(0).Text.Trim
                drAssetDoc("AssetDocName") = dtgAssetDoc.Items(j).Cells(1).Text.Trim
                drAssetDoc("chk") = CType(dtgAssetDoc.Items(j).FindControl("chk"), CheckBox).Checked
                drAssetDoc("Number") = CType(dtgAssetDoc.Items(j).FindControl("txtNumber"), TextBox).Text
                drAssetDoc("Notes") = CType(dtgAssetDoc.Items(j).FindControl("txtNotes"), TextBox).Text
                drAssetDoc("MandatoryForNewAsset") = dtgAssetDoc.Items(j).Cells(5).Text.Trim
                drAssetDoc("MandatoryForUsedAsset") = dtgAssetDoc.Items(j).Cells(6).Text.Trim
                drAssetDoc("AssetDocID") = dtgAssetDoc.Items(j).Cells(7).Text.Trim
                drAssetDoc("IsMainDoc") = dtgAssetDoc.Items(j).Cells(8).Text.Trim
                drAssetDoc("IsValueNeeded") = dtgAssetDoc.Items(j).Cells(9).Text.Trim
                dtAssetDoc.Rows.Add(drAssetDoc)
            End If
        Next

        With ocustomclass
            .Agreementno = lblAgreementNo.Text
            .insurancecoy = lblInsuranceCoy.Text
            .Description = lblDesc.Text
            .LKS = lblLKS.Text
            .CustomerID = Me.CustomerID
            .CustomerName = lblCustName.Text
            .License = lblLicense.Text
            .ClaimDate = ConvertDate2(lblClaimdate.Text)
            .ClaimAmount = CType(lblAmount.Text, Double)
            .SupplierName = lblSupplier.Text
            .OTR = CType(txtOTR.Text, Decimal)
            .DP = CType(txtDP.Text, Decimal)
            .Serial1 = txtSerial1.Text
            .Serial2 = txtSerial2.Text
            .UsedNew = lblNewUsed.Text
            .Usage = cboUsage.SelectedItem.Text
            .Bulan = cboBulan.SelectedItem.Text
            .txtSearch = cboBulan.SelectedItem.Value
            .Year = CType(txtYear.Text, Integer)
            .AssetCode = UCAsset.Description
            .AssetCodeID = UCAsset.AssetCode
            .AssetTypeID = UCAsset.AssetTypeId
            .Name = txtName.Text
            .Address = UCAddress.Address
            .Kelurahan = UCAddress.Kelurahan
            .Kecamatan = UCAddress.Kecamatan
            .City = UCAddress.City
            .ZipCode = UCAddress.ZipCode
            .TaxDate = ConvertDate2(uscTax.dateValue)
            .Notes = txtAssetNotes.Text
            .Rt = UCAddress.RT
            .Rw = UCAddress.RW
            .AreaPhone1 = UCAddress.AreaPhone1
            .Phone1 = UCAddress.Phone1
            .AreaPhone2 = UCAddress.AreaPhone2
            .Phone2 = UCAddress.Phone2
            .AreaFax = UCAddress.AreaFax
            .Fax = UCAddress.Fax
            .ListAttribute = dtAttribute
            .AdmFee = CType(txtAdmFee.Text, Double)
            .ReasonID = UCAppReq.ReasonID
            .ReasonDescription = UCAppReq.ReasonName
            .approvalby = UCAppReq.ToBeApprove
            .ucNotes = UCAppReq.Notes
            .ListData = dtAssetDoc
            .ApplicationID = Me.ApplicationID
            .SupplierID = Me.SupplierID
            .SerialNo1 = lblSerial1.Text
            .SerialNo2 = lblSerial2.Text
        End With
        HttpContext.Current.Items("Replacement") = ocustomclass
        Server.Transfer("AssetReplacementView.aspx")
    End Sub

End Class