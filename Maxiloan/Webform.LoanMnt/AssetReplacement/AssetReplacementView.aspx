﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetReplacementView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.AssetReplacementView" %>

<%@ Register TagPrefix="uc1" TagName="UcViewAddress" Src="../../Webform.UserController/UcViewAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/UcCompanyAdress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCAO" Src="../../Webform.UserController/UCAO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpAsset" Src="../../Webform.UserController/ucLookUpAsset.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpSupplier" Src="../../Webform.UserController/ucLookUpSupplier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetReplacementView</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewRequestNaNo(pStyle, pRequestNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/StopAccrued/StopAccruedView.aspx?style=' + pStyle + '&RequestNaNo=' + pRequestNo, 'RequestNoLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
    <script language="javascript" type="text/javascript">
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value;
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN PENGGANTIAN ASSET
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer</label>
                <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Perusahaan Asuransi</label>
                <asp:Label ID="lblInsuranceCoy" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polisi</label>
                <asp:Label ID="lblLicense" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Nama Asset</label>
                <asp:Label ID="lblDesc" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Klaim</label>
                <asp:Label ID="lblClaimdate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No LKS</label>
                <asp:Label ID="lblLKS" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Klaim</label>
                <asp:Label ID="lblAmount" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ENTRY DATA ASSET
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Supplier</label>
            <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Harga OTR (On The Road)</label>
            <asp:Label ID="lblOTR" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Uang Muka (DP)</label>
            <asp:Label ID="lblDP" runat="server"></asp:Label>
        </div>
    </div>
    <asp:Panel ID="pnl2" runat="server" Width="100%">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DATA ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:Label ID="lblAssetDesc" runat="server" ForeColor="#404040"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <asp:Label ID="lblSerial1" runat="server"></asp:Label>
                    <asp:Label ID="lblSerialNo1" runat="server" ForeColor="#404040"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Asset : Baru / Bekas</label>
                    <asp:Label ID="lblNewUsed" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        <asp:Label ID="lblSerial2" runat="server"></asp:Label></label>
                    <asp:Label ID="lblSerialNo2" runat="server" ForeColor="#404040"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Penggunaan</label>
                    <asp:Label ID="lblUsage" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tahun Produksi</label>
                <asp:Label ID="lblBulan" runat="server"></asp:Label>&nbsp; /
                <asp:Label ID="lblYear" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgAttribute" runat="server" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" Text='<%#Container.DataItem("AttributeName")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemStyle HorizontalAlign="Left" Width="75%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAttribute" runat="server" Text='<%#Container.DataItem("Attribute")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="AttributeID"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    REGISTRASI ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama</label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucviewaddress id="UcVAddress" runat="server"></uc1:ucviewaddress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal STNK</label>
                <asp:Label ID="lblTaxDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:Label ID="lblNotes" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgAssetDoc" runat="server" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%#Container.DataItem("No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DOKUMEN">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDocName" runat="server" Text='<%#Container.DataItem("AssetDocName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO DOKUMEN">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" runat="server" Text='<%#Container.DataItem("Number")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PERIKSA">
                                <HeaderStyle Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk" runat="server" Checked='<%#Container.DataItem("chk")%>'></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <HeaderStyle Width="36%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="MandatoryForNewAsset"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="MandatoryForUsedAsset"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AssetDocID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsMainDoc"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsValueNeeded"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Biaya Administrasi</label>
                <asp:Label ID="lblFee" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan</label>
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Disetujui Oleh</label>
                <asp:Label ID="lblToBe" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:Label ID="lblUcNotes" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
