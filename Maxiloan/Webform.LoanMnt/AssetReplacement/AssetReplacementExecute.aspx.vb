﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class AssetReplacementExecute
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property AssetRepSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetRepSeqNo"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetRepSeqNo") = Value
        End Set
    End Property
    Property AssetSeqno() As Integer
        Get
            Return CType(Viewstate("AssetSeqno"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqno") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Dim oCustomClass As New Parameter.AssetReplacement
    Dim m_controller As New AssetDataController
    Dim m_controllerAR As New AssetReplacementController
    Protected WithEvents oSearchBy As UcSearchBy    
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ASSETREPEXEC"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                ShowMessage(lblMessage, "Simpan Transaksi pelunasan Berhasil", False)
                InitialDefaultPanel()
                oSearchBy.ListData = "AssetRepSeqNo,Request No-AGREEMENTNO,Agreement No-CUSTOMERNAME,Customer Name"
                oSearchBy.BindData()
                'oEffectiveDate.Display = ""
                'oEffectiveDate.FillRequired = True
                'oEffectiveDate.isCalendarPostBack = False
                'oEffectiveDate.FieldRequiredMessage = "Please Fill Effective Date"
                'oEffectiveDate.ValidationErrMessage = "Please Fill dd/MM/yyyy"
                'txtEffectiveDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlDtGrid.Visible = False        
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal strSortBy As String)
        Dim intloop As Integer
        Dim hypID As HyperLink
        With oContract
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSortBy
            .SpName = "spAssetReplacementExecutionPaging"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        recordCount = oContract.TotalRecords
        dtgAssetReplacementExecution.DataSource = oContract.ListData
        Try
            dtgAssetReplacementExecution.DataBind()
        Catch
            dtgAssetReplacementExecution.CurrentPageIndex = 0
            dtgAssetReplacementExecution.DataBind()
        End Try
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)        
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = " BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        If txtEffectiveDate.Text <> "" Then
            Me.SearchBy &= " And EffectiveDate = '" & ConvertDate2(txtEffectiveDate.Text).ToString("yyyy/MM/dd") & "'"
        End If
        If oSearchBy.ValueID.Trim = "AssetRepSeqNo" Then
            If Not IsNumeric(oSearchBy.ValueID.Trim = "AssetRepSeqNo") Then
                Exit Sub
            End If
            If Len(oSearchBy.Text.Trim) >= 5 Then Exit Sub
        End If
        If oSearchBy.Text.Trim <> "" Then
            If Right(oSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " Like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            End If
        End If

        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub dtgAssetReplacementExecution_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetReplacementExecution.ItemDataBound
        Dim imbCancel As HyperLink
        Dim imbExecute As HyperLink
        Dim lblPrepaymentNo As HyperLink
        Dim lblApplicationid As Label
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblBranchID As Label
        Dim lblAssetSeqNo As Label

        If e.Item.ItemIndex >= 0 Then

            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            lnkAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            lnkCust = CType(e.Item.FindControl("hypCustomerName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationID"), Label)

            imbCancel = CType(e.Item.FindControl("imbCancel"), HyperLink)
            imbExecute = CType(e.Item.FindControl("imbExecute"), HyperLink)
            lblPrepaymentNo = CType(e.Item.FindControl("lblPrepaymentNo"), HyperLink)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)

            With oCustomClass
                Me.BranchID = lblBranchID.Text.Trim
                Me.ApplicationID = lblApplicationid.Text.Trim
                Me.AssetRepSeqNo = CInt(lblPrepaymentNo.Text)
                Me.AssetSeqno = CInt(lblAssetSeqNo.Text)
            End With

            'Mode 0 = Cancel
            'Mode 1 = Execute
            'Mode 2 = View
            imbCancel.NavigateUrl = "AssetReplacementExecuteList.aspx?mode=0&assetrepseqno=" & CStr(Me.AssetRepSeqNo) & "&applicationid=" & Server.UrlEncode(Me.ApplicationID) & "&branchid=" & lblBranchID.Text.Trim & "&assetseqno=" & Server.UrlEncode(CStr(Me.AssetSeqno))
            imbExecute.NavigateUrl = "AssetReplacementExecuteList.aspx?mode=1&assetrepseqno=" & CStr(Me.AssetRepSeqNo) & "&applicationid=" & Server.UrlEncode(Me.ApplicationID) & "&branchid=" & lblBranchID.Text.Trim & "&assetseqno=" & Server.UrlEncode(CStr(Me.AssetSeqno))
            lblPrepaymentNo.NavigateUrl = "AssetReplacementExecuteList.aspx?mode=2&assetrepseqno=" & CStr(Me.AssetRepSeqNo) & "&applicationid=" & Server.UrlEncode(Me.ApplicationID) & "&branchid=" & lblBranchID.Text.Trim & "&assetseqno=" & Server.UrlEncode(CStr(Me.AssetSeqno))
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & lblApplicationid.Text.Trim & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub

End Class