﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AssetReplacementRequest
    Inherits Maxiloan.Webform.WebBased    

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AssetReplacement
    Private oController As New AssetReplacementController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            Me.FormID = "AssetRepRequest"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                Initialize()
            End If
        End If
    End Sub
    Private Sub Initialize()
        pnlDatagrid.Visible = False
        DtgAssetReplacement.Visible = False
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAssetReplacement_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAssetReplacement.ItemDataBound
        Dim hypRequest As HyperLink
        Dim lblApplicationid As Label
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            hypRequest = CType(e.Item.FindControl("HypRequest"), HyperLink)
            'hypRequest.NavigateUrl = "InstallRcv.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & oBranch.BranchID.Trim

            lblAgreementNo = CType(e.Item.FindControl("HyAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            'If lblApplicationid.Text.Trim.Length > 0 Then
            '    lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            'End If
            lblName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAssetReplacement.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.AssetReplacement
        Dim m_controller As New AssetReplacementController
        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAssetReplacementPaging"
        End With

        oPaging = m_controller.AssetReplacementPaging(oPaging)
        recordCount = oPaging.TotalRecords
        DtgAssetReplacement.DataSource = oPaging.listdata

        Try
            DtgAssetReplacement.DataBind()
        Catch exp As Exception
            DtgAssetReplacement.CurrentPageIndex = 0
            DtgAssetReplacement.DataBind()

            ShowMessage(lblMessage, exp.Message, True)
            lblMessage.Visible = True
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DtgAssetReplacement.Visible = True
        pnlDatagrid.Visible = True
        DtgAssetReplacement.Visible = True
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim cmdwhere As String
        Dim filterby As String
        Me.SortBy = ""
        Me.SearchBy = ""
        cmdwhere = ""
        filterby = ""
        pnlDatagrid.Visible = True

        If txtSearch.Text <> "" And cboSearch.SelectedItem.Value <> "0" And txtClaimDate.Text.Trim <> "" Then
            If txtSearch.Text.IndexOf("%") > 0 Then
                cmdwhere = cmdwhere + cboSearch.SelectedItem.Value & " like '" & _
                                    txtSearch.Text & "'" & " And ic.ClaimDate  =  '" & ConvertDate2(txtClaimDate.Text.Trim).ToString("yyyyMMdd") & "'"
            Else
                cmdwhere = cmdwhere + cboSearch.SelectedItem.Value & " = '" & _
                               txtSearch.Text & "'" & "And ic.ClaimDate  =  '" & ConvertDate2(txtClaimDate.Text.Trim).ToString("yyyyMMdd") & "'"
            End If

        ElseIf txtSearch.Text <> "" And cboSearch.SelectedItem.Value <> "0" Then
            If txtSearch.Text.IndexOf("%") > 0 Then
                cmdwhere = cmdwhere + cboSearch.SelectedItem.Value & " like '" & _
                                    txtSearch.Text & "'"
            Else
                cmdwhere = cmdwhere + cboSearch.SelectedItem.Value & " = '" & _
                               txtSearch.Text & "'"
            End If
        ElseIf txtClaimDate.Text.Trim <> "" Then
            cmdwhere = cmdwhere + "ic.ClaimDate  =  '" & ConvertDate2(txtClaimDate.Text.Trim).ToString("yyyyMMdd") & "'"
        End If
        If cmdwhere <> "" Then
            cmdwhere = cmdwhere + " And ic.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Else
            cmdwhere = cmdwhere + " ic.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        End If
        Me.SearchBy = cmdwhere

        Me.SortBy = "ic.Applicationid"
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub DtgAssetReplacement_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAssetReplacement.ItemCommand
        If e.CommandName = "Request" Then
            Dim oCustomer As Parameter.Customer
            Dim lblApplicationid As Label
            Dim lblAssetSeqNo As Label
            Dim lblCustID As Label
            Dim lblCustName As HyperLink
            Dim lblSupNama As Label
            Dim lblAgreementNo As HyperLink
            Dim lblInsuranceCoy As Label
            Dim lblDesc As Label
            Dim lblLKS As Label
            Dim lblLicense As Label
            Dim lblclaimdate As String
            Dim lblAmount As Label

            lblAgreementNo = CType(e.Item.FindControl("HyAgreementNo"), HyperLink)
            lblInsuranceCoy = CType(e.Item.FindControl("lblInsuranceCoy"), Label)
            lblDesc = CType(e.Item.FindControl("lblDesc"), Label)
            lblLKS = CType(e.Item.FindControl("lblClaimNo"), Label)
            lblLicense = CType(e.Item.FindControl("lblLicensePlate"), Label)
            lblclaimdate = e.Item.Cells(5).Text.Trim
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)

            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblSupNama = CType(e.Item.FindControl("lblSupNama"), Label)
            lblCustName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)


            Dim cookie As HttpCookie = Request.Cookies("AssetReplacement")
            If Not cookie Is Nothing Then
                cookie.Values("id") = lblApplicationid.Text
                cookie.Values("Custid") = lblCustID.Text
                cookie.Values("name") = lblCustName.Text
                cookie.Values("AssetID") = lblAssetSeqNo.Text
                cookie.Values("ApplicationID") = lblApplicationid.Text

                cookie.Values("AgreementNo") = lblAgreementNo.Text
                cookie.Values("InsuranceCoy") = lblInsuranceCoy.Text
                cookie.Values("Desc") = lblDesc.Text
                cookie.Values("LKS") = lblLKS.Text
                cookie.Values("License") = lblLicense.Text
                cookie.Values("Claimdate") = lblclaimdate
                cookie.Values("Amount") = lblAmount.Text

                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("AssetReplacement")
                cookieNew.Values.Add("id", lblApplicationid.Text)
                cookieNew.Values.Add("Custid", lblCustID.Text)
                cookieNew.Values.Add("name", lblCustName.Text)
                cookieNew.Values.Add("AssetID", lblAssetSeqNo.Text)
                cookieNew.Values.Add("ApplicationID", lblApplicationid.Text)

                cookieNew.Values.Add("AgreementNo", lblAgreementNo.Text)
                cookieNew.Values.Add("InsuranceCoy", lblInsuranceCoy.Text)
                cookieNew.Values.Add("Desc", lblDesc.Text)
                cookieNew.Values.Add("LKS", lblLKS.Text)
                cookieNew.Values.Add("License", lblLicense.Text)
                cookieNew.Values.Add("Claimdate", lblclaimdate)
                cookieNew.Values.Add("Amount", lblAmount.Text)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("AssetReplacementList.aspx")
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("AssetReplacementRequest.aspx")
    End Sub

End Class