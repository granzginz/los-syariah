﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewLCWaived.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ViewLCWaived" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LCWaived</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:Panel runat="server" ID="pnlWaivedValueDate">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PENGAJUAN PEMBEBASAN BIAYA/DENDA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <asp:Panel ID="pnlValueDate" runat="server">
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Tanggal Valuta
                    </label>
                    <asp:Label runat="server" ID="lblValidDate" CssClass="small_text"></asp:Label>
                    <%--<asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValidDate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>--%>
                </div>
                <div class="form_right">
                </div>
            </div>
            <%--<div class="form_button">
                <asp:Button ID="BtnNext" runat="server" Text="Next" CssClass="small button green">
                </asp:Button>
                <asp:Button ID="BtnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button>
            </div>--%>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <uc1:ucpaymentinfo id="oPaymentInfo" runat="server"></uc1:ucpaymentinfo>
    
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI PEMBEBASAN BIAYA/DENDA
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Denda Keterlambatan Angsuran
                </label>
                <asp:Label id="lblLCInstall" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan Asuransi
                </label>
                <asp:Label id="lblLCInsurance" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tagih Angsuran
                </label>
                <asp:Label id="lblInstallCollFee" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Biaya Tagih Asuransi
                </label>
                <asp:Label id="lblInsuranceCollFee" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tolakan PDC
                </label>
                <asp:Label id="lblPDCBounceFee" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Total Nilai
                </label>
                <asp:Label ID="lblTotalWaivedAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Alasan
                </label>
                <asp:Label id="lblAlasan" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="lblSetuju" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label id="lblCatatan" runat="server" />
            </div>
            <div class="form_right">
                <%--<label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="Label2" runat="server"></asp:Label>--%>
            </div>
        </div>
    
    </asp:Panel>
    <asp:Panel ID="pnlWaivedAllocation" runat="server">
        <%--<div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI PEMBEBASAN BIAYA/DENDA
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Denda Keterlambatan Angsuran
                </label>
                <asp:Label id="lblLCInstall" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan Asuransi
                </label>
                <asp:Label id="lblLCInsurance" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tagih Angsuran
                </label>
                <asp:Label id="lblInstallCollFee" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Biaya Tagih Asuransi
                </label>
                <asp:Label id="lblInsuranceCollFee" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tolakan PDC
                </label>
                <asp:Label id="lblPDCBounceFee" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Total Nilai
                </label>
                <asp:Label ID="lblTotalWaivedAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Alasan
                </label>
                <asp:Label id="lblAlasan" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="lblSetuju" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label id="lblCatatan" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="Label2" runat="server"></asp:Label>
            </div>
        </div>--%>
        <%--<div class="form_box">
            <div class="form_single">
                <asp:Button ID="BtnCalculate" CausesValidation="False" runat="server" Text="Calculate" CssClass="small buttongo blue">
                </asp:Button>
            </div>
        </div>--%>
        <uc1:ucapprovalrequest id="oApprovalRequest" runat="server"></uc1:ucapprovalrequest>
        <%--<div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>--%>
    </asp:Panel>
    
    </form>
</body>
</html>
