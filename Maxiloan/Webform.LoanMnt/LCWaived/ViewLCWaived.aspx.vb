﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ViewLCWaived
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents txtLCInstall As ucNumberFormat
    Protected WithEvents txtLCInsurance As ucNumberFormat
    Protected WithEvents txtInstallCollFee As ucNumberFormat
    Protected WithEvents txtInsuranceCollFee As ucNumberFormat
    Protected WithEvents txtPDCBounceFee As ucNumberFormat

#Region "Constanta"
    Private oCustomClass As New Parameter.FullPrepay
    Private oController As New LCWaivedController
#End Region
    Property WaivedNo() As String
        Get
            Return ViewState("WaivedNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("WaivedNo") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "WAIVEDTRANSACTIONLIST"
            If Me.CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
                Me.ApplicationID = Request.QueryString("applicationid")
                Me.BranchID = Request.QueryString("branchid")
                pnlWaivedAllocation.Visible = False
                pnlWaivedValueDate.Visible = True
                pnlPaymentInfo.Visible = True
                oApprovalRequest.ReasonTypeID = "WATRS"
                oApprovalRequest.ApprovalScheme = "WA__"
                lblValidDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                If Me.ApplicationID <> "" Then
                    ' Bind data for edit maintenance
                    With oCustomClass
                        .BranchId = Me.BranchID
                        .strConnection = GetConnectionString()
                        .ApplicationID = Me.ApplicationID
                    End With
                    PaymentInfo()
                    bindControllerNumber()
                    BindEdit(oCustomClass)
                End If
            End If
        End If
    End Sub
    Sub bindControllerNumber()
        'With txtLCInstall
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = True
        '    .TextCssClass = "numberAlign regular_text"
        'End With
        'With txtLCInsurance
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = True
        '    .TextCssClass = "numberAlign regular_text"

        'End With
        'With txtInstallCollFee
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = True
        '    .TextCssClass = "numberAlign regular_text"

        'End With
        'With txtInsuranceCollFee
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = True
        '    .TextCssClass = "numberAlign regular_text"

        'End With
        'With txtPDCBounceFee
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = True
        '    .TextCssClass = "numberAlign regular_text"

        'End With
        With oPaymentInfo
            lblLCInstall.Text = CStr(.MaximumLCInstallFee)
            lblLCInsurance.Text = CStr(.MaximumLCInsuranceFee)
            lblInstallCollFee.Text = CStr(.MaximumInstallCollFee)
            lblInsuranceCollFee.Text = CStr(.MaximumInsuranceCollFee)
            lblPDCBounceFee.Text = CStr(.MaximumPDCBounceFee)
        End With
    End Sub
    Private Sub PaymentInfo()
        Dim oControllerPaymentInfo As New UCPaymentInfoController
        Dim oCustomClassPaymentInfo As New Parameter.AccMntBase
        With oCustomClassPaymentInfo
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
            .BranchId = Me.BranchID
        End With
        oCustomClassPaymentInfo = oControllerPaymentInfo.GetPaymentInfo(oCustomClassPaymentInfo)
        With oCustomClassPaymentInfo
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType

            lblAgreementBranch.Text = .BranchAgreement
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        End With
        With oPaymentInfo
            .ValueDate = ConvertDate2(lblValidDate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        lblLCInstall.Text = "0.00"
        lblLCInsurance.Text = "0.00"
        lblInstallCollFee.Text = "0.00"
        lblInsuranceCollFee.Text = "0.00"
        lblPDCBounceFee.Text = "0.00"
        'lblTotalWaivedAmount.Text = "0.00"
        'pnlWaivedAllocation.Visible = True
        'lblAlasan.Text = "0.00"
        'lblSetuju.Text = "0.00"
        'lblCatatan.Text = "0.00"
    End Sub

    'Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
    '    Server.Transfer("LCWaivedList.aspx")
    'End Sub

    'Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
    '    Server.Transfer("LCWaivedList.aspx")
    'End Sub

    'Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
    '    pnlWaivedAllocation.Visible = True
    '    pnlWaivedValueDate.Visible = True
    '    pnlPaymentInfo.Visible = True
    '    pnlValueDate.Visible = False

    '    With oPaymentInfo
    '        .ValueDate = ConvertDate2(txtValidDate.Text)
    '        .ApplicationID = Me.ApplicationID
    '        .PaymentInfo()
    '    End With
    '    txtLCInstall.Text = "0.00"
    '    txtLCInsurance.Text = "0.00"
    '    txtInstallCollFee.Text = "0.00"
    '    txtInsuranceCollFee.Text = "0.00"
    '    txtPDCBounceFee.Text = "0.00"
    '    lblTotalWaivedAmount.Text = "0.00"



    'End Sub

    Sub BindEdit(NoWaived)

        If Me.ApplicationID <> "" Then
            With oCustomClass
                .BranchId = Me.BranchID
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
            End With
            oCustomClass = oController.ViewLCWaived(oCustomClass)
            'Dim oCustomerFacility As New Parameter.CustomerFacility

            lblLCInstall.Text = oCustomClass.LcInstallmentWaived
            lblLCInsurance.Text = oCustomClass.LcInsuranceWaived
            lblInstallCollFee.Text = oCustomClass.InstallmentCollFeeWaived
            lblInsuranceCollFee.Text = oCustomClass.InsuranceCollFeeWaived
            lblPDCBounceFee.Text = oCustomClass.PDCBounceFeeWaived
            lblTotalWaivedAmount.Text = oCustomClass.TotalNilai
            lblCatatan.Text = oCustomClass.Notes
            lblAlasan.Text = oCustomClass.ReasonID
            lblSetuju.Text = oCustomClass.UserApproval
        End If
    End Sub



End Class