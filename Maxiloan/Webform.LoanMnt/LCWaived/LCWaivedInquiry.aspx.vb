﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class LCWaivedInquiry
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oSearchBy As UcSearchBy
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Dim err As String = Request.QueryString("message")
            If err <> String.Empty Then
                ShowMessage(lblMessage, err, True)
            End If
           
            Me.FormID = "WAIVETRANSACTIONINQUIRY"
            oBranch.IsAll = True
            oSearchBy.ListData = "WaiveNo, No Request-AgreementNo,No Kontrak-CustomerName, Nama Konsumen"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView        

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spWaivedTransactionInquiry"
        End With
        oCustomClass = oController.GetGeneralPaging(oCustomClass)
        DtUserList = oCustomClass.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecords
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Visible = False
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim HypWaiveNo As HyperLink
        Dim flagfile As New Label
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim lblApplicationid As Label

        flagfile.Text = "1"
        If e.Item.ItemIndex >= 0 Then

            HypWaiveNo = CType(e.Item.FindControl("HypWaiveNo"), HyperLink)
            HypWaiveNo.NavigateUrl = "javascript:OpenWinWaiveInquiry('ACCMNT', '" & HypWaiveNo.Text.Trim & "','" & oBranch.BranchID.Trim & "','" & flagfile.Text.Trim & "')"

            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblcustomerid"), Label)
            hyTemp = CType(e.Item.FindControl("lblName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("LCWaivedInquiry.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""
        Me.SearchBy = ""
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            If oBranch.BranchID <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " Branchid = '" & oBranch.BranchID.Trim & "'"
                If par <> "" Then
                    par = par & " , Agreement Branch : " & oBranch.BranchName.Trim & " "
                Else
                    par = par & "Agreement Branch : " & oBranch.BranchName.Trim & " "
                End If
            End If
            If txtEffectiveDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and EffectiveDate = '" & ConvertDate2(txtEffectiveDate.Text) & "'"
                If par <> "" Then
                    par = par & " , Effective Date : " & txtEffectiveDate.Text & " "
                Else
                    par = par & "Effective Date : " & txtEffectiveDate.Text & " "
                End If
            End If

            If oSearchBy.Text.Trim <> "" Then               
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " LIKE '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"

                If par <> "" Then
                    par = par & " , " & oSearchBy.Text & " : '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
                Else
                    par = par & "" & oSearchBy.Text & " : '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
                End If
            End If
            If cmbStatus.SelectedValue <> "ALL" Then

                Me.SearchBy = Me.SearchBy & " and Status = '" & cmbStatus.SelectedItem.Text.Trim.Replace("'", "''") & "'"
                If par <> "" Then
                    par = par & " , Status : '" & cmbStatus.SelectedItem.Text & "'"
                Else
                    par = par & " Status : '" & cmbStatus.SelectedItem.Text & "'"
                End If
            End If
            'oCustomClass.paramReport = par
            'Me.ParamReport = oCustomClass.paramReport

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

End Class