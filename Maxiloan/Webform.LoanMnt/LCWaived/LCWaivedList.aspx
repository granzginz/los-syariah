﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LCWaivedList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.LCWaivedList" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LCWaivedList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h3>
                    PENGAJUAN PEMBEBASAN BIAYA/DENDA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranchAll" runat="server"></uc1:ucbranchall>
            </div>
        </div>
       
            <div class="form_box_uc" >
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
         
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single"><div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="Applicationid"   OnSortCommand="SortGrid"
                         CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle  Width="2%"></HeaderStyle>
                                <ItemStyle  CssClass="command_col" Width="2%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypReceive" runat="server" Text='WAIVED'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                <HeaderStyle  Width="9%">
                                </HeaderStyle>
                                <ItemStyle   Width="9%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle  Width="7%"></HeaderStyle>
                                <ItemStyle Width="7%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--  <asp:TemplateColumn HeaderText="SEQ">
                                <ItemStyle Width="1%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSeqCust" runat="server" Text='<%#Container.DataItem("SeqCust")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                <HeaderStyle   Width="20%">
                                </HeaderStyle>
                                <ItemStyle  Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="TYPE CUSTOMER">
                                <HeaderStyle   Width="5%">
                                </HeaderStyle>
                                <ItemStyle   Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                <HeaderStyle  Width="60%">
                                </HeaderStyle>
                                <ItemStyle  Width="60%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationStep" HeaderText="STEP">
                                <HeaderStyle   Width="5%">
                                </HeaderStyle>
                                <ItemStyle  Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                <HeaderStyle   CssClass="item_grid_right" Width="10%"></HeaderStyle>
                                <ItemStyle  CssClass="item_grid_right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),0)%> &nbsp;'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STS">
                                <HeaderStyle   Width="3%">
                                </HeaderStyle>
                                <ItemStyle   Width="3%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"   CssClass="validator_general" 
                         ControlToValidate="txtPage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                            MaximumValue="999999999" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"   ControlToValidate="txtPage" ErrorMessage="No Halaman Salah"
                            Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div></div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
