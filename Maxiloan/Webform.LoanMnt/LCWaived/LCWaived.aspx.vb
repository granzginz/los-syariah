﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class LCWaived
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents txtLCInstall As ucNumberFormat
    Protected WithEvents txtLCInsurance As ucNumberFormat
    Protected WithEvents txtInstallCollFee As ucNumberFormat
    Protected WithEvents txtInsuranceCollFee As ucNumberFormat
    Protected WithEvents txtPDCBounceFee As ucNumberFormat

#Region "Constanta"
    Private oCustomClass As New Parameter.FullPrepay
    Private oController As New LCWaivedController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "WAIVEDTRANSACTIONLIST"
            If Me.CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
                Me.ApplicationID = Request.QueryString("applicationid")
                Me.BranchID = Request.QueryString("branchid")
                pnlWaivedAllocation.Visible = False
                pnlWaivedValueDate.Visible = True
                pnlPaymentInfo.Visible = True
                oApprovalRequest.ReasonTypeID = "WATRS"
                oApprovalRequest.ApprovalScheme = "WA__"
                txtValidDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                PaymentInfo()
                bindControllerNumber()
            End If
        End If
    End Sub
    Sub bindControllerNumber()
        With txtLCInstall
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtLCInsurance
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"

        End With
        With txtInstallCollFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"

        End With
        With txtInsuranceCollFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"

        End With
        With txtPDCBounceFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"

        End With
        With oPaymentInfo
            txtLCInstall.RangeValidatorMaximumValue = CStr(.MaximumLCInstallFee)
            txtLCInsurance.RangeValidatorMaximumValue = CStr(.MaximumLCInsuranceFee)
            txtInstallCollFee.RangeValidatorMaximumValue = CStr(.MaximumInstallCollFee)
            txtInsuranceCollFee.RangeValidatorMaximumValue = CStr(.MaximumInsuranceCollFee)
            txtPDCBounceFee.RangeValidatorMaximumValue = CStr(.MaximumPDCBounceFee)
        End With
    End Sub
    Private Sub PaymentInfo()
        Dim oControllerPaymentInfo As New UCPaymentInfoController
        Dim oCustomClassPaymentInfo As New Parameter.AccMntBase
        With oCustomClassPaymentInfo
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
            .BranchId = Me.BranchID
        End With
        oCustomClassPaymentInfo = oControllerPaymentInfo.GetPaymentInfo(oCustomClassPaymentInfo)
        With oCustomClassPaymentInfo
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType

            lblAgreementBranch.Text = .BranchAgreement
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        End With
        With oPaymentInfo
            .ValueDate = ConvertDate2(txtValidDate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Server.Transfer("LCWaivedList.aspx")
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Server.Transfer("LCWaivedList.aspx")
    End Sub

    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        pnlWaivedAllocation.Visible = True
        pnlWaivedValueDate.Visible = True
        pnlPaymentInfo.Visible = True
        pnlValueDate.Visible = False

        With oPaymentInfo
            .ValueDate = ConvertDate2(txtValidDate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        txtLCInstall.Text = "0.00"
        txtLCInsurance.Text = "0.00"
        txtInstallCollFee.Text = "0.00"
        txtInsuranceCollFee.Text = "0.00"
        txtPDCBounceFee.Text = "0.00"
        lblTotalWaivedAmount.Text = "0.00"

        

    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        With oPaymentInfo
            .ValueDate = ConvertDate2(txtValidDate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Replace("'", "")
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .ValueDate = ConvertDate2(txtValidDate.Text)
            .LcInstallmentWaived = CDbl(txtLCInstall.Text.Trim)
            .LcInsuranceWaived = CDbl(txtLCInsurance.Text.Trim)
            .InstallmentCollFeeWaived = CDbl(txtInstallCollFee.Text.Trim)
            .InsuranceCollFeeWaived = CDbl(txtInsuranceCollFee.Text.Trim)
            .PDCBounceFeeWaived = CDbl(txtPDCBounceFee.Text.Trim)
            .ReasonDescription = oApprovalRequest.Notes
            .ReasonID = oApprovalRequest.ReasonID
            .ReasonTypeID = oApprovalRequest.ReasonTypeID
            .RequestTo = oApprovalRequest.ToBeApprove
            .LoginId = Me.Loginid
        End With

        Try
            With oCustomClass
                If .LcInstallmentWaived + .LcInsuranceWaived + .InstallmentCollFeeWaived + _
                    +.InsuranceCollFeeWaived + .PDCBounceFee > 0 Then
                    oController.SaveLCWaived(oCustomClass)
                    Response.Redirect("LCWaivedList.aspx?Message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                Else
                    ShowMessage(lblMessage, "Nilai Pembebasan tidak boleh 0", True)
                End If
            End With
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

    Private Sub BtnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCalculate.Click
        lblTotalWaivedAmount.Text = FormatNumber(CDbl(txtInstallCollFee.Text) + CDbl(txtInsuranceCollFee.Text) + CDbl(txtLCInstall.Text) + CDbl(txtLCInsurance.Text) + _
                                    CDbl(txtPDCBounceFee.Text), 2)
    End Sub

End Class