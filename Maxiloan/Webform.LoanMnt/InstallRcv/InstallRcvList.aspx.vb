﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallRcvList
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 6
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1
    Protected oCustomClass As New Parameter.AgreementList
    Protected oController As New AgreementListController
    Protected oDataUserCtlr As New DataUserControlController

    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents txtAngsuran1 As ucNumberFormat
    Protected WithEvents txtAngsuran2 As ucNumberFormat

    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oViewPaymentAllocate As ucPaymentAllocate

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not CheckCashier(Me.Loginid) Then
            initSearchBy()
            ShowMessage(lblMessage, "Kasir Belum Buka", True)
            Buttonsearch.Visible = False
            ButtonReset.Visible = False
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        AddHandler oViewPaymentAllocate.PaymentAllocateEvent, AddressOf PaymentAllocateChanged
        If Not IsPostBack Then
            initSearchBy()
            If Request.QueryString("filekwitansi") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If


        End If
    End Sub
      
#End Region


    Protected Sub initSearchBy()
        Me.FormID = "INSTALLRCVLIST"
        oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-LicensePlate, No Polisi"
        oSearchBy.BindData()
        pnlAlokasi.Visible = False
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            Me.SearchBy = ""
            Me.SortBy = ""
        End If
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SortBy, Me.SearchBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Protected Sub DoBind(ByVal strSort As String, ByVal strSearch As String, Optional isFrNav As Boolean = False)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With

        oCustomClass = oController.AgreementList(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        DtgAgree.DataSource = oCustomClass.ListAgreement

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub



    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            Dim lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)

            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            Dim lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            Dim lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind(e.SortExpression, Me.SearchBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")

        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()

        txtAngsuran1.Text = ""
        txtAngsuran2.Text = ""

        Me.SearchBy = strSearch.ToString
        DoBind("", Me.SearchBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")
     
        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'") 
        End If
        If txtAngsuran1.Text > 0 Then
            If txtAngsuran2.Text > 0 Then
                strSearch.Append(" and InstallmentAmount between '" & CDbl(txtAngsuran1.Text) & "' and '" & CDbl(txtAngsuran2.Text) & "' ")
            Else
                strSearch.Append(" and InstallmentAmount = '" & CDbl(txtAngsuran1.Text) & "' ")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If sessioninvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            ' If Me.checkCashier() Then

            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/AgreementListReport.aspx")
            'End If
        End If
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Receive"
                'Dim lblApplicationid As HyperLink
                'lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                'oCustomClass.ApplicationID = lblApplicationid.Text
                'oCustomClass.BranchId = oBranch.BranchID.Trim
                'HttpContext.Current.Items("Receive") = oCustomClass

                'Server.Transfer("InstallRCV.aspx")



                Dim oController As New UCPaymentInfoController
                Dim oClass As New Parameter.AccMntBase With {
                    .ApplicationID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink).Text,
                    .BranchId = oBranch.BranchID.Trim,
                    .InsSeqNo = DtgAgree.Items(e.Item.ItemIndex).Cells(9).Text.Trim,
                     .ValueDate = Me.BusinessDate,
                    .strConnection = GetConnectionString()
                    }
                oController.GetPaymentInfo(oClass)

                oViewPaymentAllocate.PaymentInfo(oClass)
                pnlAlokasi.Visible = True
                pnlList.Visible = False
                 
        End Select
    End Sub

    Sub PaymentAllocateChanged(ByVal sender As Object, ByVal e As PayAllocateEventArgs)
        If (Not e.Ok) Then
            pnlAlokasi.Visible = False
            pnlList.Visible = True
            Return
        End If
        Session.Add("oCustomClass", e.OClass)
        Server.Transfer("InstallRCV.aspx")
    End Sub
End Class