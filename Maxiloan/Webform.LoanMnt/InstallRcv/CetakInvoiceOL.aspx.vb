﻿#Region "Imports"
Imports System.IO
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class CetakInvoiceOL
    Inherits Maxiloan.Webform.AccMntWebBased

    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 6
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oValueDate1 As ucDateCE
    Protected WithEvents oValueDate2 As ucDateCE
    Protected WithEvents txtVATPercent As ucNumberFormat
    Protected WithEvents txtWHTPercent As ucNumberFormat


    Protected oCustomClass As New Parameter.InvoiceOL
    Protected oController As New InvoiceOLController

    Public Property oClass() As Parameter.InvoiceOL
        Get
            Return CType(ViewState("InvoiceOLClass"), Parameter.InvoiceOL)
        End Get
        Set(ByVal Value As Parameter.InvoiceOL)
            ViewState("InvoiceOLClass") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If SessionInvalid() Then
                Exit Sub
            End If
            Me.FormID = "CETAKINVOICEOL"
            If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Exit Sub
            End If

            If Not IsPostBack Then
                initSearchBy()
                If Request.QueryString("fileinvoiceol") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("fileinvoiceol") & ".pdf"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.height; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','InvoiceOL', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                    & "</script>")
                End If
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub initSearchBy()
        Me.FormID = "CETAKINVOICEOL"
        oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-LicensePlate, No Polisi"
        oSearchBy.BindData()
        pnlView.Visible = False
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            Me.SearchBy = ""
            Me.SortBy = ""
        End If
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SortBy, Me.SearchBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Protected Sub DoBind(ByVal strSort As String, ByVal strSearch As String, Optional isFrNav As Boolean = False)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With

        oCustomClass = oController.CetakInvoiceOLPaging(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        DtgAgree.DataSource = oCustomClass.ListData
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            Dim lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)

            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            Dim lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            Dim lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind(e.SortExpression, Me.SearchBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")

        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()

        oValueDate1.Text = ""
        oValueDate2.Text = ""

        Me.SearchBy = strSearch.ToString
        DoBind("", Me.SearchBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
        End If
        If oValueDate1.Text <> "" Then
            If oValueDate2.Text <> "" Then
                strSearch.Append(" and DueDate between '" & ConvertDate2(oValueDate1.Text) & "' and '" & ConvertDate2(oValueDate2.Text) & "' ")
            Else
                strSearch.Append(" and DueDate = '" & CDbl(oValueDate1.Text) & "' ")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Print"
                Dim tClass As New Parameter.InvoiceOL
                tClass.strConnection = GetConnectionString()
                tClass.ApplicationID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink).Text
                Dim dt As DataTable = oController.CetakInvoiceOLDetail(tClass)

                If dt.Rows.Count > 0 Then
                    oClass = tClass
                    oClass.InvoiceNo = ""
                    oClass.InvoiceDate = dt.Rows(0).Item("InvoiceDate")
                    oClass.InvoiceDueDate = dt.Rows(0).Item("InvoiceDueDate")
                    oClass.Attn = dt.Rows(0).Item("Attn")
                    oClass.SeqNo = dt.Rows(0).Item("InsSeqNo")
                    oClass.Description = dt.Rows(0).Item("MerekType").ToString
                    oClass.PaidAmount = 0
                    oClass.PaidStatus = "R"
                    oClass.BranchId = dt.Rows(0).Item("BranchID")
                    oClass.LoginId = Me.Loginid

                    Dim gs As New GeneralSettingController
                    Dim pgs As New Parameter.GeneralSetting
                    pgs.strConnection = GetConnectionString()

                    lblNoKontrak.Text = dt.Rows(0).Item("AgreementNo").ToString
                    lblDueDate.Text = Format(dt.Rows(0).Item("InvoiceDueDate"), "dd/MM/yyyy")
                    lblAsset.Text = dt.Rows(0).Item("MerekType").ToString
                    lblPeriodLease.Text = Format(dt.Rows(0).Item("GoLiveDate"), "dd/MM/yyyy") & " - " & Format(dt.Rows(0).Item("MaturityDate"), "dd/MM/yyyy")
                    lblCustomer.Text = dt.Rows(0).Item("Name").ToString
                    lblAddress.Text = dt.Rows(0).Item("Alamat").ToString
                    lblBasicLease.Text = FormatNumber(dt.Rows(0).Item("InstallmentAmount").ToString, 0)
                    lblDenda.Text = FormatNumber(dt.Rows(0).Item("Denda").ToString, 0)
                    pgs.GSID = "VATOPERATINGLEASE"
                    pgs = gs.GetGeneralSettingByID(pgs)
                    txtVATPercent.Text = pgs.ListData.Rows(0).Item("GSValue").ToString
                    txtVATAmount.Text = FormatNumber((CDbl(pgs.ListData.Rows(0).Item("GSValue").ToString) / 100) * CDbl(dt.Rows(0).Item("InstallmentAmount").ToString), 0)

                    pgs.strConnection = GetConnectionString()
                    pgs.GSID = "WHTOPERATINGLEASE"
                    pgs = gs.GetGeneralSettingByID(pgs)
                    txtWHTPercent.Text = pgs.ListData.Rows(0).Item("GSValue").ToString
                    txtWHTAmount.Text = FormatNumber((CDbl(pgs.ListData.Rows(0).Item("GSValue").ToString) / 100) * CDbl(dt.Rows(0).Item("InstallmentAmount").ToString), 0)

                    lblTotalAmount.Text = FormatNumber(CDbl(lblBasicLease.Text) - CDbl(txtWHTAmount.Text) + CDbl(txtVATAmount.Text), 0)
                    pnlView.Visible = True
                    pnlList.Visible = False
                Else
                    ShowMessage(lblMessage, "Data not found", True)
                    pnlView.Visible = False
                    pnlList.Visible = True
                End If
                
        End Select
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
        ' disini perlu di save dulu
        Dim tClass As New Parameter.InvoiceOL
        tClass.BranchId = oClass.BranchId
        tClass.strConnection = GetConnectionString()
        tClass.ApplicationID = oClass.ApplicationID
        tClass.InvoiceNo = oClass.InvoiceNo
        tClass.InvoiceDate = oClass.InvoiceDate
        tClass.InvoiceDueDate = oClass.InvoiceDueDate
        tClass.BusinessDate = Me.BusinessDate
        tClass.Attn = oClass.Attn
        tClass.SeqNo = oClass.SeqNo
        tClass.Description = oClass.Description
        tClass.BasicLease = CDbl(lblBasicLease.Text)
        tClass.VATPercentage = CDec(txtVATPercent.Text)
        tClass.VATAmount = (CDbl(txtVATPercent.Text) / 100) * tClass.BasicLease
        tClass.WHTPercentage = CDec(txtWHTPercent.Text)
        tClass.WHTAmount = (CDbl(txtWHTPercent.Text) / 100) * tClass.BasicLease
        tClass.BillAmount = tClass.BasicLease + tClass.VATAmount - tClass.WHTAmount
        tClass.PaidAmount = 0
        tClass.PaidStatus = "R"
        tClass.LoginId = Me.Loginid
        Try
            oClass.InvoiceNo = oController.CetakInvoiceOLSave(tClass)
            BindReport()
        Catch ex As Exception
            ShowMessage(lblMessage, "Error - " & ex.Message, True)
        End Try

    End Sub

    Private Sub BindReport()
        Dim oData As New DataTable
        Dim objReport As RptKwitansiOL = New RptKwitansiOL

        'oController.PrintTTU(GetConnectionString, oClass.ApplicationID, oClass.BranchId, Me.BusinessDate, Me.Loginid)

        oCustomClass.ApplicationID = oClass.ApplicationID
        oCustomClass.InvoiceNo = oClass.InvoiceNo
        oCustomClass.BranchId = oClass.BranchId
        oCustomClass.strConnection = GetConnectionString()
        oData = oController.ReportKwitansi(oCustomClass)

        If (oData.Rows.Count <= 0) Then
            Exit Sub
        End If

        objReport.SetDataSource(oData)

        'crvKwitansi.ReportSource = objReport 
        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Invoice" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = pathReport("invoiceol", True) & oClass.ApplicationID.Replace("/", "").Trim & ".pdf"

        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("cetakinvoiceol.aspx?fileinvoiceol=" & pathReport("invoiceol", False) & oClass.ApplicationID.Replace("/", "").Trim)
    End Sub

    Private Function pathReport(ByVal group As String, ByVal dirXML As Boolean) As String
        Dim strDirectory As String = ""
        If dirXML = True Then
            strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += Me.sesBranchId.Replace("'", "") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += group & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If
        Else
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "/"
            strDirectory += Me.sesBranchId.Replace("'", "") & "/"
            strDirectory += group & "/"
        End If
        Return strDirectory
    End Function
    Private Function getMonthID() As String
        Dim rtn As String = ""

        If Month(Me.BusinessDate) = "1" Then
            rtn = "Januari"
        End If
        If Month(Me.BusinessDate) = "2" Then
            rtn = "Febuari"
        End If
        If Month(Me.BusinessDate) = "3" Then
            rtn = "Maret"
        End If
        If Month(Me.BusinessDate) = "4" Then
            rtn = "April"
        End If
        If Month(Me.BusinessDate) = "5" Then
            rtn = "Mai"
        End If
        If Month(Me.BusinessDate) = "6" Then
            rtn = "Juni"
        End If
        If Month(Me.BusinessDate) = "7" Then
            rtn = "Juli"
        End If
        If Month(Me.BusinessDate) = "8" Then
            rtn = "Augustus"
        End If
        If Month(Me.BusinessDate) = "9" Then
            rtn = "September"
        End If
        If Month(Me.BusinessDate) = "10" Then
            rtn = "Oktober"
        End If
        If Month(Me.BusinessDate) = "11" Then
            rtn = "November"
        End If
        If Month(Me.BusinessDate) = "12" Then
            rtn = "Desember"
        End If

        Return rtn

    End Function

    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnlView.Visible = False
        pnlList.Visible = True
    End Sub

End Class