﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallRCV_Collection
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oViewPaymentDetail As UCViewPaymentDetail
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule

    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String
#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Public Property oClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property

    Private Property ValutaDateCollector() As Date
        Get
            Return CType(ViewState("ValutaDateCollector"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("ValutaDateCollector") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Private Property InsSeqno() As String
        Get
            Return CType(ViewState("InsSeqno"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsSeqno") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            'Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            'Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId

            Me.FormID = "INSTALLRCV_COL"
            Me.Mode = "Normal"
            Me.ValutaDateCollector = Me.BusinessDate

            oClass = Session("oCustomClass")

            oCashier.CashierID = ""
            oCashier.HeadCashier = True

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'If Not CheckCashier(Me.Loginid) Then

                '    ShowMessage(lblMessage, "Kasir Belum Buka", True)
                '    Dim ThreadInitialPanel As New Thread(AddressOf InitialDefaultPanel)
                '    ThreadInitialPanel.Priority = ThreadPriority.AboveNormal
                '    pnlBtnGroupPaymentReceive.Visible = False
                '    pnlBtnGroupPaymentAllocation.Visible = False
                '    Dim ThreadBindData As New Thread(AddressOf DoBind)
                '    ThreadBindData.Priority = ThreadPriority.AboveNormal
                '    ThreadInitialPanel.Start()
                '    ThreadBindData.Start()
                '    Exit Sub
                'End If
                oPaymentDetail.TglBayarTxt.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                InitialDefaultPanel()
                DoBind()
                LoadNextStep()
                BlokirBayarStep()

                'load grid angsuran
                oInstallmentSchedule.ApplicationId = oClass.ApplicationID
                oInstallmentSchedule.ValueDate = Me.BusinessDate
                oInstallmentSchedule.DoBind_Angsuran(String.Empty)

                Me.ApplicationID = oClass.ApplicationID
                Me.BranchID = oClass.BranchId.Trim
                Me.InsSeqno = oClass.InsSeqNo
            End If
        End If

        If Not CheckCashier(Me.Loginid) Then
            ShowMessage(lblMessage, "Kasir Belum Buka", True)
            pnlPaymentDetail.Visible = True
            pnlViewPaymentDetail.Visible = False
            pnlPaymentAllocation.Visible = False
            pnlBtnGroupPaymentReceive.Visible = False
            pnlBtnGroupPaymentAllocation.Visible = False
            pnlPaymentInfo.Visible = False

            pnlBtnGroupPaymentAllocation.Visible = True
            imbSaveProcess.Visible = False
            Exit Sub
        End If
         
    End Sub
    Private Sub tglBayar_TextChanged(sender As Object, e As System.EventArgs) Handles oPaymentDetail.TextChanged
        InitialDefaultPanel()        
        DoBind()        
        Me.ValutaDateCollector = Date.ParseExact(oPaymentDetail.TglBayarTxt.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        'load grid angsuran
        oInstallmentSchedule.ApplicationId = oClass.ApplicationID
        'oInstallmentSchedule.ValueDate = Me.BusinessDate
        oInstallmentSchedule.ValueDate = Me.ValutaDateCollector
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)

        GetPaymentInfoUlang()
        LoadNextStep()
        BlokirBayarStep()
    End Sub

    Private Sub DoBind() 
        Dim oCustomClass = oClass
        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType

            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID.Trim) & "')"
            lblPrepaid.Text = FormatNumber(.Prepaid, 0)
            lblAmountToBePaid.Text = FormatNumber(.AmountToBePaid, 0)
            lblTitipanAngsuran.Text = FormatNumber(.TitipanAngsuran, 0)

            oPaymentAllocationDetail.ToleransiBayarKurang = .ToleransiBayarKurang
            oPaymentAllocationDetail.ToleransiBayarLebih = .ToleransiBayarLebih

            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "CP"
            oPaymentDetail.DoFormCashModule()
            oPaymentDetail.AutoPostBack = True
            oPaymentDetail.AmountReceive = .InstallmentAmount
            oPaymentDetail.ShowTglBayar = False
            oPaymentDetail.ResultcmbBankAccount = .BankAccountID
            oPaymentDetail.cmbBankAccountEnabled = False
            oPaymentDetail.ValueDate = .ValueDate.ToString("dd/MM/yyyy")
             

            Me.NextInstallmentDate = .NextInstallmentDate   
            Me.PrepaidBalance = .Prepaid
            Me.AmountToBePaid = .AmountToBePaid 
            Me.BankAccount = oPaymentDetail.BankAccount 
            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub
    Private Sub LoadNextStep()
        'scl
        NextProcess()
        If Me.WayOfPayment <> "CP" Then
            If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                Else
                    If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                        oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                        oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                    Else
                        oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                    End If
                End If
            Else
                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
            End If
            oInstallmentSchedule.ApplicationId = oClass.ApplicationID
            'oInstallmentSchedule.ValueDate = BusinessDate
            oInstallmentSchedule.ValueDate = Me.ValutaDateCollector
            oPaymentAllocationDetail.IsiKolomTagihan()
            'oPaymentAllocationDetail.BindPrioritasPembayaran(Me.BusinessDate, True, oInstallmentSchedule.GetAllAngsuranUnPaid)
            oPaymentAllocationDetail.BindPrioritasPembayaran(Me.ValutaDateCollector, True, oInstallmentSchedule.GetAllAngsuranUnPaid)
        End If

    End Sub

    Private Sub NextProcess()

        With oPaymentInfo
             .PaymentInfo(oClass) 
            Me.ValidationData = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
        End With
        Me.AmountReceive = oPaymentDetail.AmountReceive
        oPaymentAllocationDetail.PaymentAllocationBind(oClass) 
    End Sub
    Private Sub InitialDefaultPanel()
        pnlPaymentDetail.Visible = True
        pnlViewPaymentDetail.Visible = False
        pnlPaymentAllocation.Visible = True
        pnlBtnGroupPaymentReceive.Visible = False
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlPaymentInfo.Visible = False
        pnlHeadCashierPassword.Visible = False
        pnlConfirmation.Visible = False 
    End Sub

    

#Region "BtnGroup Payment Receive"
    Private Sub imbNextProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbNextProcess.Click
        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            lblMessage.Text = ""
            If oPaymentDetail.WayOfPayment = "CP" Then
                If oClass.BranchId <> Me.sesBranchId.Trim.Replace("'", "") Then
                    ShowMessage(lblMessage, "Cara Pembayaran Tidak bisa untuk Transaksi Antar Cabang", True)
                    Exit Sub
                End If
            End If

            If Me.IsValidLastPayment(oClass.ApplicationID, ConvertDate2(oPaymentDetail.TglBayarTxt.Text)) Then
                If Me.Mode = "Normal" Then
                    NextProcess()
                    If Not CheckCashier(Me.Loginid) Then 
                        ShowMessage(lblMessage, "Kasir belum Buka", True)
                        Exit Sub
                    End If
                    If Me.WayOfPayment = "CP" Then
                        If Me.PrepaidBalance = 0 Then 
                            ShowMessage(lblMessage, "Tidak bisa Alokasi Saldo Prepaid, Karena Saldo Kurang", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    Else
                        If oPaymentDetail.AmountReceive = 0 Then 
                            ShowMessage(lblMessage, "Harap diisi dengan Jumlah diterima", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    End If
                    If IsMaxBacDated(ConvertDate2(oPaymentDetail.TglBayarTxt.Text)) Then
                        oPaymentDetail.WayOfPayment = "0"
                        Me.Mode = "NotNormal"
                        pnlHeadCashierPassword.Visible = True
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        oCashier.CashierID = ""
                        oCashier.HeadCashier = True

                        Exit Sub
                    ElseIf DateDiff(DateInterval.Day, ConvertDate2(oPaymentDetail.TglBayarTxt.Text), Me.BusinessDate) < 0 Then
                        oPaymentDetail.WayOfPayment = "0"
                        ShowMessage(lblMessage, "Tanggal Valuta tidak boleh lebih besar dari tgl hari ini", True)
                        Exit Sub
                    End If


                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = True
                    pnlPaymentAllocation.Visible = True
                    pnlPaymentInfo.Visible = True
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = True

                    If Me.WayOfPayment <> "CP" Then
                        If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                            If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                            Else
                                If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                                Else
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                                End If
                            End If
                        Else
                            oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                        End If 
                        oPaymentAllocationDetail.IsiKolomTagihan() 
                    End If

                ElseIf Me.Mode = "NotNormal" Then
                    Dim objDecent As New Decrypt.am_futility
                    Dim oLogin As New LoginController
                    Dim oLoginEntities As New Parameter.Login

                    oLoginEntities.strConnection = GetConnectionString()
                    oLoginEntities.LoginId = oCashier.CashierID.Trim
                    Dim lstrPassword As String

                    lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
                    If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = True
                        pnlPaymentAllocation.Visible = True
                        pnlPaymentInfo.Visible = True
                        pnlHeadCashierPassword.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = True
                        Me.Mode = "Normal"
                    Else 
                        ShowMessage(lblMessage, "Password Salah", True)
                        Exit Sub
                    End If
                End If
            Else 
                ShowMessage(lblMessage, "Tanggal Valuta Lebih kecil dari tanggal pembayaran terakhir", True)
                Exit Sub
            End If
        End If
    End Sub


    Private Sub imbCancelProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelProcess.Click
        Response.Redirect("InstallRcvList_Collection.aspx")
    End Sub
#End Region

#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If Not CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                Exit Sub
            End If

            If Not CheckCashier(Me.Loginid) Then
                ShowMessage(lblMessage, "Kasir belum Buka", True)
                Exit Sub
            End If

            If ConvertDate2(oPaymentDetail.TglBayarTxt.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari hari ini", True)
                Exit Sub
            End If

            With oPaymentInfo
                '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                '.ApplicationID = oClass.ApplicationID
                '.PaymentInfo()
                LastAR = .Prepaid + _
                            .MaximumInstallment + _
                            .MaximumLCInstallFee + _
                            .MaximumInstallCollFee + _
                            .MaximumInsurance + _
                            .MaximumLCInsuranceFee + _
                            .MaximumInsuranceCollFee + _
                            .MaximumPDCBounceFee + _
                            .MaximumSTNKRenewalFee + _
                            .MaximumInsuranceClaimFee + _
                            .MaximumReposessionFee
            End With
            If Me.ValidationData <> LastAR Then
                ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)
                Exit Sub
            End If

            '
            ' Need to remove paid amount validation for payment from collection
            '
            'If oPaymentAllocationDetail.InstallmentDue > 0 And oPaymentAllocationDetail.InstallmentDue < Me.AmountReceive Then
            '    ShowMessage(lblMessage, "Angsuran hanya boleh diisi dengan 0 atau " & FormatNumber(Me.AmountReceive, 0), True)
            '    Exit Sub
            'End If
            TotalAllocation = oPaymentAllocationDetail.CountTotalBayar()
            'With oPaymentAllocationDetail
            '    TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
            '                    .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
            '                    .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
            '                    .RepossessionFee + .Prepaid

            'End With
            If TotalAllocation <> oClass.ToAmountAllocate Then
                ShowMessage(lblMessage, String.Format("Total Alokasi Alokasi harus = {0:N0} ", oClass.ToAmountAllocate), True)
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = oClass.BranchId
                .ApplicationID = oClass.ApplicationID
                .AmountReceive = oPaymentAllocationDetail.InstallmentDue
            End With

            If oController.IsLastPayment(oCustomClass) Then
                pnlConfirmation.Visible = True
                pnlBtnGroupPaymentAllocation.Visible = False
                pnlHeadCashierPassword.Visible = False
                pnlPaymentDetail.Visible = False
                pnlViewPaymentDetail.Visible = False
                pnlPaymentAllocation.Visible = False
                pnlPaymentInfo.Visible = False
                pnlBtnGroupPaymentReceive.Visible = False
                pnlBtnGroupPaymentAllocation.Visible = False
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .NoKwitansi = oPaymentDetail.NoKwitansiTxt.Text.Trim
                .ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                '.BusinessDate = Me.ValutaDateCollector
                .ReceivedFrom = oPaymentDetail.ReceivedFrom
                .ReferenceNo = oPaymentDetail.ReferenceNo
                .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
                .LoginId = Me.Loginid
                .BankAccountID = Me.BankAccount
                .Notes = oPaymentDetail.Notes
                .ApplicationID = oClass.ApplicationID

                '-----------Installment
                .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                .LcInstallment = oPaymentAllocationDetail.LCInstallment
                .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                '------------------------

                '----------Insurance
                .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                .LcInsurance = oPaymentAllocationDetail.LCInsurance
                .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                '-----------------------------

                .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                .Prepaid = oPaymentAllocationDetail.Prepaid
                .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                .AmountReceive = TotalAllocation
                .BayarDi = "Collection"
                .CollectorID = oPaymentDetail.CollectorID
            End With 

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Try
                Dim SavingController As New InstallRcvController(oCustomClass)
                SavingController.InstallmentReceive() 
                BindReport()

            Catch exp As Exception 
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If 
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvList_Collection.aspx")
    End Sub
#End Region


    Sub BindReport() 
        Response.Redirect("installrcvlist_Collection.aspx")
    End Sub

    Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If Not CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                Exit Sub
            End If

            If Not CheckCashier(Me.Loginid) Then
                ShowMessage(lblMessage, "Kasir belum Buka", True)
                Exit Sub
            End If

            With oPaymentInfo
                '.ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                '.ApplicationID = Me.ApplicationID
                '.PaymentInfo()
                LastAR = .Prepaid + _
                            .MaximumInstallment + _
                            .MaximumLCInstallFee + _
                            .MaximumInstallCollFee + _
                            .MaximumInsurance + _
                            .MaximumLCInsuranceFee + _
                            .MaximumInsuranceCollFee + _
                            .MaximumPDCBounceFee + _
                            .MaximumSTNKRenewalFee + _
                            .MaximumInsuranceClaimFee + _
                            .MaximumReposessionFee
            End With
            If Me.ValidationData <> LastAR Then 
                ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
                Exit Sub
            End If

            With oPaymentAllocationDetail
                TotalAllocation = oPaymentAllocationDetail.CountTotalBayar()
                'TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
                '                .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                '                .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                '                .RepossessionFee + .Prepaid

            End With
            If Me.WayOfPayment = "CP" Then
                If TotalAllocation > Me.PrepaidBalance Then
                    ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
                    Exit Sub
                End If
                If oPaymentAllocationDetail.Prepaid > 0 Then
                    ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
                    Exit Sub
                End If
            Else
                If TotalAllocation - oPaymentAllocationDetail.PLL > LastAR Then
                    ShowMessage(lblMessage, "Alokasi pembayaran harus lebih kecil atau sama dengan sisa angsuran", True)
                    Exit Sub
                End If
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .ValueDate = ConvertDate2(oPaymentDetail.TglBayarTxt.Text)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                '.BusinessDate = Me.ValutaDateCollector
                .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
                .ReferenceNo = oViewPaymentDetail.ReferenceNo
                .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
                .LoginId = Me.Loginid
                .BankAccountID = Me.BankAccount
                .Notes = oViewPaymentDetail.Notes
                .ApplicationID = oClass.ApplicationID

                '-----------Installment
                .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim
                .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                .LcInstallment = oPaymentAllocationDetail.LCInstallment
                .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                '------------------------

                '----------Insurance
                .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                .LcInsurance = oPaymentAllocationDetail.LCInsurance
                .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                '-----------------------------

                .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                .Prepaid = oPaymentAllocationDetail.Prepaid
                .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                .PLL = oPaymentAllocationDetail.PLL
                .PLLDesc = oPaymentAllocationDetail.PLLDesc

                .AmountReceive = TotalAllocation
            End With

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Try
                Dim SavingController As New InstallRcvController(oCustomClass)
                SavingController.InstallmentReceive()
                BindReport()
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
       
    End Sub

    Private Sub imbCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelLastPayment.Click
        Response.Redirect("InstallRcvList_Collection.aspx")
    End Sub
     
    Private Sub BlokirBayarStep()
        If BlokirBayar Then
            imbSaveProcess.Visible = False
            ShowMessage(lblMessage, "Pembayaran ini diblokir", True)
        Else
            imbSaveProcess.Visible = True
        End If
    End Sub

    Private Sub GetPaymentInfoUlang()
        Dim oController As New UCPaymentInfoController
        Dim ToAmountAllocate As Double = oClass.ToAmountAllocate
        Dim ToAmountInstFee As Double = oClass.ToAmountInstFee
        Dim ToAmountOther As Double = oClass.ToAmountOther

        Dim oClass1 As New Parameter.AccMntBase With {
            .ApplicationID = Me.ApplicationID.Trim,
            .BranchId = Me.BranchID,
            .InsSeqNo = Me.InsSeqno,
            .ValueDate = Me.ValutaDateCollector,
            .strConnection = GetConnectionString()
            }
        oClass = oController.GetPaymentInfo(oClass1)
        oClass.ToAmountAllocate = ToAmountAllocate
        oClass.ToAmountInstFee = ToAmountInstFee
        oClass.ToAmountOther = ToAmountOther
    End Sub
End Class