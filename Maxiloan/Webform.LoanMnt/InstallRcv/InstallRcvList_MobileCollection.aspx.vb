﻿#Region "Imports"
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallRcvList_MobileCollection
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents txtAngsuran1 As ucNumberFormat
    Protected WithEvents txtAngsuran2 As ucNumberFormat
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oViewPaymentAllocate As ucPaymentAllocate
    Public Event PaymentAllocateEvent As PayAllocateChangedHandler

#Region "Property"
    Public Property strAll() As String
        Get
            Return CType(ViewState("strAll"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("strAll") = Value
        End Set
    End Property

    Private Property SearchBy2() As String
        Get
            Return CType(ViewState("SearchBy2CL"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy2CL") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 6
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass2 As New Parameter.CLActivity
    Private oController As New CLActivityController
    Private oController1 As New CLActivityController
    Private m_CollZipCode As New CollZipCodeController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        'AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        AddHandler oViewPaymentAllocate.PaymentAllocateEvent, AddressOf PaymentAllocateChanged
        If Not IsPostBack Then

            Dim dtParent As New DataTable
            Dim Coll As New Parameter.RptCollAct
            Dim ControllerCG As New RptCollActController

            If Me.strAll = "1" Then
                With Coll
                    .strConnection = GetConnectionString()
                    .CGID = Me.GroubDbID
                    .strKey = "CG_ALL"
                    .CollectorType = ""
                End With
            Else
                With Coll
                    .strConnection = GetConnectionString()
                    .CGID = Me.GroubDbID
                    .strKey = "CG"
                    .CollectorType = ""
                End With
            End If
            Coll = ControllerCG.ViewDataCollector(Coll)
            dtParent = Coll.ListCollector

            cboParent.DataTextField = "CGName"
            cboParent.DataValueField = "CGID"
            cboParent.DataSource = dtParent
            cboParent.DataBind()
            cboParent.Items.Insert(0, "Select One")
            cboParent.Items(0).Value = "0"
            BindCollector()

            If Request.QueryString("filekwitansi") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            If Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("Message"), True)
            End If

            Me.FormID = "INSTALLRCV_COL"
            pnlAlokasi.Visible = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub



    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, ByVal where2 As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        With oCustomClass2
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .WhereCond2 = where2
        End With

        oCustomClass2 = oController.CLActivityListMobCollector(oCustomClass2)

        DtUserList = oCustomClass2.ListCLActivity
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass2.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgCLActivity.DataSource = DvUserList

        Try
            dtgCLActivity.DataBind()
        Catch
            dtgCLActivity.CurrentPageIndex = 0
            dtgCLActivity.DataBind()
        End Try



        PagingFooter()
        pnlDtGrid.Visible = True
        'pnlResult.Visible = False
        'pnlsearch.Visible = True

    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
            End If
        End If
    End Sub
#End Region
        
    Private Sub dtgCLActivity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCLActivity.ItemCommand
        Select Case e.CommandName
            Case "Receive"
                Dim oController As New UCPaymentInfoController
                Dim oClass As New Parameter.AccMntBase With {
                    .ApplicationID = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text.Trim,
                    .BranchId = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label).Text.Trim,
                    .InsSeqNo = dtgCLActivity.Items(e.Item.ItemIndex).Cells(11).Text.Trim,
                    .ValueDate = Me.BusinessDate,
                    .strConnection = GetConnectionString(),
                    .ToAmountAllocate = CDbl(CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblAmount"), Label).Text.Trim),
                    .ToAmountInstFee = CDbl(CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblAmount"), Label).Text.Trim),
                    .ToAmountOther = CDbl(0),
                    .ReceivedFrom = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblReceivedFrom"), Label).Text.Trim,
                    .ReferenceNo = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblReceiptNo"), Label).Text.Trim,
                    .CollectorID = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblCollectorID"), Label).Text.Trim
                    }
                oController.GetPaymentInfo(oClass)

                'oViewPaymentAllocate.PaymentInfo(oClass)
                'pnlAlokasi.Visible = True
                'pnlList.Visible = False

                Session.Add("oCustomClass", oClass)
                Server.Transfer("InstallRCV_MobileCollection.aspx")
        End Select
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SortBy = ""
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "'"
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Me.SearchBy = "Collector.CGID='" & cboParent.SelectedItem.Value.Trim & "' "
        Me.SearchBy = Me.SearchBy & " and Collector.COllectorID='" & cboChild.SelectedValue.Trim & "'"

        'Me.SearchBy2 = "DCR.CGID='" & cboParent.SelectedItem.Value.Trim & "' " & " and DCR.COllectorID='" & cboChild.SelectedValue.Trim & "'"

        pnlDtGrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub

    Function getColor(ByVal LegalPhoneYN As String) As System.Drawing.Color
        If LegalPhoneYN = "Y" Then
            Return System.Drawing.Color.Blue
        Else
            Return System.Drawing.Color.Black
        End If
    End Function

    Sub PaymentAllocateChanged(ByVal sender As Object, ByVal e As PayAllocateEventArgs)
        If (Not e.Ok) Then
            pnlAlokasi.Visible = False
            pnlList.Visible = True
            Return
        End If
        Session.Add("oCustomClass", e.OClass)
        Server.Transfer("InstallRCV_MobileCollection.aspx")
    End Sub

#Region "GenerateScriptreplaceUserController"
    Protected Function CollectionGroupIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" CGID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CGID")).Trim Then
                        strType = CStr(DataRow(i)("CGID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

    Private Sub BindCollector()

        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        With oCollZipCode
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = "CL"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)

        cboChild.DataSource = dt
        cboChild.DataTextField = "CollectorName"
        cboChild.DataValueField = "CollectorID"
        cboChild.DataBind()
        cboChild.Items.Insert(0, "Select One")
        cboChild.Items(0).Value = "0"

    End Sub
#End Region


End Class
