﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvList_MobileCollection.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRcvList_MobileCollection" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>

<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocate" Src="../../Webform.UserController/UcPaymentAllocate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRcvList_MobileCollection</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
     <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 2;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            eval('document.forms[0].' + pBankAccount).options[1] = new Option('ALL', 'ALL');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };
        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        }
        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
        }
    </script>
</head>
<body>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PEMBAYARAN CUSTOMER (via MOBILE COLLECTION)
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Collection Group </label>
                <asp:DropDownList ID="cboParent" runat="server" onchange="<%#CollectionGroupIDChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboCollectionGroup" runat="server" ControlToValidate="cboParent" CssClass="validator_general"
                    ErrorMessage="Harap pilih Collection Group" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
             <label class ="label_req">Collector </label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCollector" runat="server" ControlToValidate="cboChild" CssClass="validator_general"
                    ErrorMessage="Harap pilih Collector" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Tgl Angsuran</label>
                <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
<%--                    <asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>--%>
            </div>            
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>    
        </div>                
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR KONTRAK YANG DIBAYAR VIA MOBILE COLLECTION
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCLActivity" runat="server" Width="100%" AllowSorting="True"
                                AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate> 
                                            <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Receive"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CollectorName" HeaderText="NAMA COLLECTOR">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCollectorName" runat="server" Text='<%# Container.DataItem("CollectorName") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ValueDate" SortExpression="ValueDate" HeaderText="TGL VALUTA"
                                        DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle HorizontalAlign="Center" Width="9%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="9%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblAgreementNo" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="Application Id" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerName" runat="server" Text='<%# Container.DataItem("CustomerName") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReceiptNo" HeaderText="NO RECEIPT">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceiptNo" runat="server" Text='<%# Container.DataItem("ReceiptNo") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReceivedFrom" HeaderText="DITERIMA DARI">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceivedFrom" runat="server" Text='<%# Container.DataItem("ReceivedFrom") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="35%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BranchID" HeaderText="BranchID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="35%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="NextInstallmentNumber" DataFormatString="{0:N0}" HeaderText="KE" SortExpression="NextInstallmentNumber" />
                                    <asp:BoundColumn DataField="NextInstallmentDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="JATUH TEMPO" SortExpression="NextInstallmentDate" />
                                    <asp:TemplateColumn SortExpression="CollectorID" HeaderText="COLLECTORID">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCollectorID" runat="server" Text='<%# Container.DataItem("CollectorID") %>'>
                                            </asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
<%--        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button blue">
            </asp:Button>
        </div>--%>
    </asp:Panel>
     <asp:Panel ID="pnlAlokasi" runat="server">
            <div class="form_box_uc">
                <uc1:UcPaymentAllocate id="oViewPaymentAllocate" runat="server" /> 
            </div>
     </asp:Panel>
    </form>
</body>
</html>
