﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web


Public Class ReprintKwitansi
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As ucBranchAll
#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "REPRINTKWT"

            If Request.QueryString("strFileLocation") <> "" Then              
                Dim strFileLocation As String

                strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                               & "var x = screen.width;" & vbCrLf _
                               & "var y = screen.height;" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If

            If Request.QueryString("applicationid") <> "" Then
                pnlsearch.Visible = False
                Me.SearchBy = "  MailTransaction.MailTypeID='KWT' and MailTransaction.BranchID='" & Request.QueryString("BranchID") & _
                "' and KwitansiInstallmentPrint.applicationid = '" & Request.QueryString("applicationid") & "' and KwitansiInstallmentPrint.HistorySequenceNo = " & Request.QueryString("HistorySequence") & ""

                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "  MailTransaction.MailTypeID='KWT' and MailTransaction.BranchID='" & oBranch.BranchID.Trim & "'"

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " ='" & txtSearchBy.Text.Trim & "'"
        End If

        Me.SortBy = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        DtUserList = oController.ListKwitansiInstallment(oCustomClass).Tables(0)
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgKwitansi.DataSource = DvUserList

        Try
            dtgKwitansi.DataBind()
        Catch
            dtgKwitansi.CurrentPageIndex = 0
            dtgKwitansi.DataBind()
        End Try

        pnlDtGrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = " MailTransaction.MailTypeID='KWT' and MailTransaction.BranchID='" & oBranch.BranchID.Trim & "'"
        Me.SortBy = ""

        DoBind(Me.SearchBy, Me.SortBy)
        txtSearchBy.Text = ""
        cboSearchBy.ClearSelection()
    End Sub

#Region "Print"
    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As New Label
            Dim hasil As Integer
            Dim cmdwhere As String
            Dim strMultiAgreementNo As String = ""
            Dim strMultiApplicationID As String = ""

            With oDataTable
                .Columns.Add(New DataColumn("ReferenceNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
                .Columns.Add(New DataColumn("HistorySequenceNo", GetType(String)))
                .Columns.Add(New DataColumn("InsSeqNo", GetType(String)))
            End With

            For intloop = 0 To dtgKwitansi.Items.Count - 1
                chkPrint = CType(dtgKwitansi.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo.Text = dtgKwitansi.Items(intloop).Cells(3).Text

                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("ReferenceNo") = lblAgreementNo.Text.Trim
                    oRow("ApplicationID") = CType(dtgKwitansi.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                    oRow("HistorySequenceNo") = dtgKwitansi.Items(intloop).Cells(5).Text
                    oRow("InsSeqNo") = dtgKwitansi.Items(intloop).Cells(6).Text
                    oDataTable.Rows.Add(oRow)

                    If cmdwhere = "" Then
                        cmdwhere = "(ApplicationID ='" & CType(oRow("ApplicationID"), String) & "' AND HistorySequenceNo = " & CType(oRow("HistorySequenceNo"), String) & " AND InsSeqNo = " & CType(oRow("InsSeqNo"), String) & ")"
                    Else
                        cmdwhere = cmdwhere & " or (ApplicationID='" & CType(oRow("ApplicationID"), String) & "' AND HistorySequenceNo = " & CType(oRow("HistorySequenceNo"), String) & " AND InsSeqNo = " & CType(oRow("InsSeqNo"), String) & ")"
                    End If

                    strMultiAgreementNo = strMultiAgreementNo + "'" + lblAgreementNo.Text.Trim + "',"
                    strMultiApplicationID = strMultiApplicationID + "'" + CType(oRow("ApplicationID"), String).Trim + "',"
                End If
            Next

            strMultiAgreementNo = Left(strMultiAgreementNo, (Len(strMultiAgreementNo) - 1))
            strMultiApplicationID = Left(strMultiApplicationID, (Len(strMultiApplicationID) - 1))

            If oDataTable.Rows.Count = 0 Then

                ShowMessage(lblMessage, "Harap Periksa Item", True)
                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .ListData = oDataTable
                .BranchId = oBranch.BranchID.Trim
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With

            oCustomClass = oController.SavePrintKwitansiInstallment(oCustomClass)
            hasil = oCustomClass.Hasil

            If hasil = 0 Then

                ShowMessage(lblMessage, "Gagal!", True)
                Exit Sub
            Else
                cmdwhere = "(" & cmdwhere & ") and BranchID='" & oBranch.BranchID.Trim & "'"
                Dim cookie As HttpCookie = Request.Cookies("ReprintKwitansiInstallment")

                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = cmdwhere
                    cookie.Values("MultiAgreementNo") = strMultiAgreementNo.Trim
                    cookie.Values("MultiApplicationID") = strMultiApplicationID.Trim
                    cookie.Values("BranchID") = Me.sesBranchId.Replace("'", "")
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("ReprintKwitansiInstallment")
                    cookieNew.Values.Add("cmdwhere", cmdwhere)
                    cookieNew.Values.Add("MultiAgreementNo", strMultiAgreementNo.Trim)
                    cookieNew.Values.Add("MultiApplicationID", strMultiApplicationID.Trim)
                    cookieNew.Values.Add("BranchID", Me.sesBranchId.Replace("'", ""))
                    Response.AppendCookie(cookieNew)
                End If

                Response.Redirect("ReprintKwitansiViewer.aspx?applicationid=" & Request.QueryString("ApplicationID") & "&branchid=" & Request.QueryString("BranchID") & "&HistorySequence=" & Request.QueryString("HistorySequence") & "&AgreementNo=" & Request.QueryString("AgreementNo"))
            End If
        End If
    End Sub
#End Region
    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
    Sub BindReport(ByVal index As Integer)
        Dim oData As New DataSet
        Dim objReport As RptKwitansi2 = New RptKwitansi2
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim hasil As Integer
        With oDataTable
            .Columns.Add(New DataColumn("ReferenceNo", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("HistorySequenceNo", GetType(String)))
            .Columns.Add(New DataColumn("InsSeqNo", GetType(String)))
        End With

        oRow = oDataTable.NewRow
        oRow("ReferenceNo") = CType(dtgKwitansi.Items(index).FindControl("hyAgreementNo"), HyperLink).Text.Trim
        oRow("ApplicationID") = CType(dtgKwitansi.Items(index).FindControl("lblApplicationID"), Label).Text.Trim
        oRow("HistorySequenceNo") = dtgKwitansi.Items(index).Cells(5).Text
        oRow("InsSeqNo") = dtgKwitansi.Items(index).Cells(6).Text
        oDataTable.Rows.Add(oRow)

        With oCustomClass
            .strConnection = GetConnectionString()
            .ListData = oDataTable
            .BranchId = oBranch.BranchID.Trim
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
        End With

        oCustomClass = oController.SavePrintKwitansiInstallment(oCustomClass)
        hasil = oCustomClass.Hasil

        If hasil = 0 Then
            ShowMessage(lblMessage, "Gagal!", True)
            Exit Sub
        Else
            oCustomClass.ApplicationID = CType(dtgKwitansi.Items(index).FindControl("lblApplicationID"), Label).Text.Trim
            oCustomClass.HistorySeqNo = dtgKwitansi.Items(index).Cells(5).Text
            oCustomClass.BranchId = oBranch.BranchID.Trim
            oCustomClass.strConnection = GetConnectionString()
            oData = oController.ReportKwitansiInstallment2(oCustomClass)

            '{ get;set;}
            objReport.SetDataSource(oData)

            'add cashier
            AddParamField(objReport, "CashierName", Me.FullName)
            AddParamField(objReport, "BusinessDate", Me.BusinessDate.ToString("dd MMMM yy"))
            AddParamField(objReport, "BranchName", oData.Tables(0).Rows(0).Item("BranchCity").ToString)

            Dim doctoprint As New System.Drawing.Printing.PrintDocument
            Dim i As Integer

            For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
                Dim rawKind As Integer
                If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                    rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                    objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                    Exit For
                End If
            Next

            'Export ke PDF
            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "ReprintKwitansi.pdf"
            DiskOpts.DiskFileName = strFileLocation
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()

            Response.Redirect("ReprintKwitansi.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "ReprintKwitansi")
        End If

    End Sub
    Private Sub dtgKwitansi_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgKwitansi.ItemCommand

        Select Case e.CommandName
            Case "Print"
                BindReport(e.Item.ItemIndex)
        End Select
    End Sub
End Class