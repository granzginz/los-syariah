﻿#Region "Imports"
Imports System.IO
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class InstallRCVOL
    Inherits Maxiloan.Webform.AccMntWebBased

    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 6
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oValueDate1 As ucDateCE
    Protected WithEvents oValueDate2 As ucDateCE
    Protected WithEvents txtVATPercent As ucNumberFormat
    Protected WithEvents txtWHTPercent As ucNumberFormat
    Protected WithEvents txtBayarDenda As ucNumberFormat

    Protected oCustomClass As New Parameter.InvoiceOL
    Protected oController As New InvoiceOLController

    Public Property oClass() As Parameter.InvoiceOL
        Get
            Return CType(ViewState("InvoiceOLClass"), Parameter.InvoiceOL)
        End Get
        Set(ByVal Value As Parameter.InvoiceOL)
            ViewState("InvoiceOLClass") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If SessionInvalid() Then
                Exit Sub
            End If
            Me.FormID = "INSTALLRCVOL"
            If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Exit Sub
            End If

            If Not IsPostBack Then
                initSearchBy()
            End If

            If Not IsPostBack Then
                initSearchBy()
                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.height; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','InstallRCVPrint', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                    & "</script>")
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub initSearchBy()
        Me.FormID = "INSTALLRCVOL"
        oSearchBy.ListData = "Agreementno, No. Kontrak-InvoiceNo, Nomor Invoice"
        oSearchBy.BindData()
        pnlView.Visible = False
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            Me.SearchBy = ""
            Me.SortBy = ""
        End If
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SortBy, Me.SearchBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Protected Sub DoBind(ByVal strSort As String, ByVal strSearch As String, Optional isFrNav As Boolean = False)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With

        oCustomClass = oController.InvoiceOLPaging(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        DtgAgree.DataSource = oCustomClass.ListData
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            Dim lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)

            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            Dim lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            Dim lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind(e.SortExpression, Me.SearchBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")

        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()

        oValueDate1.Text = ""
        oValueDate2.Text = ""

        Me.SearchBy = strSearch.ToString
        DoBind("", Me.SearchBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
        End If
        If oValueDate1.Text <> "" Then
            If oValueDate2.Text <> "" Then
                strSearch.Append(" and InvoiceDueDate between '" & ConvertDate2(oValueDate1.Text) & "' and '" & ConvertDate2(oValueDate2.Text) & "' ")
            Else
                strSearch.Append(" and InvoiceDueDate = '" & CDbl(oValueDate1.Text) & "' ")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Terima"
                Dim tClass As New Parameter.InvoiceOL
                tClass.strConnection = GetConnectionString()
                tClass.ApplicationID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink).Text
                tClass.InvoiceNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblInvoiceNo"), Label).Text
                Dim dt As DataTable = oController.InvoiceOLDetail(tClass)

                If dt.Rows.Count > 0 Then
                    oClass = tClass
                    oClass.InvoiceNo = dt.Rows(0).Item("InvoiceNo")
                    oClass.InvoiceDate = dt.Rows(0).Item("InvoiceDate")
                    oClass.InvoiceDueDate = dt.Rows(0).Item("InvoiceDueDate")
                    oClass.Attn = dt.Rows(0).Item("Attn")
                    oClass.SeqNo = dt.Rows(0).Item("SeqNo")
                    oClass.Description = dt.Rows(0).Item("MerekType").ToString
                    oClass.PaidAmount = dt.Rows(0).Item("BillAmount").ToString
                    oClass.PaidStatus = dt.Rows(0).Item("PaidStatus").ToString

                    oPaymentDetail.BindRekeningCashModule("", "ALL")
                    oPaymentDetail.BindRekeningCashModule("", "ALL")
                    oPaymentDetail.IsTitle = True
                    oPaymentDetail.ValueDate = CDate(Me.BusinessDate).ToString("dd/MM/yyyy")
                    oPaymentDetail.WithOutPrepaid = True
                    oPaymentDetail.IsCanNegative = True
                    oPaymentDetail.pnlConll = False

                    Dim gs As New GeneralSettingController
                    Dim pgs As New Parameter.GeneralSetting
                    Dim persenString As String = " % "
                    pgs.strConnection = GetConnectionString()

                    lblNoKontrak.Text = dt.Rows(0).Item("AgreementNo").ToString
                    lblDueDate.Text = Format(dt.Rows(0).Item("InvoiceDueDate"), "dd/MM/yyyy")
                    lblAsset.Text = dt.Rows(0).Item("MerekType").ToString
                    lblPeriodLease.Text = Format(dt.Rows(0).Item("GoLiveDate"), "dd/MM/yyyy") & " - " & Format(dt.Rows(0).Item("MaturityDate"), "dd/MM/yyyy")
                    lblCustomer.Text = dt.Rows(0).Item("Name").ToString
                    lblAddress.Text = dt.Rows(0).Item("Alamat").ToString
                    lblBasicLease.Text = FormatNumber(dt.Rows(0).Item("InstallmentAmount").ToString, 0)
                    lblVATPercent.Text = FormatNumber(dt.Rows(0).Item("VATPercentage").ToString, 0) & persenString
                    lblWHTPercent.Text = FormatNumber(dt.Rows(0).Item("WHTPercentage").ToString, 0) & persenString
                    lblVAT.Text = FormatNumber(dt.Rows(0).Item("VATAmount").ToString, 0)
                    lblWHT.Text = FormatNumber(dt.Rows(0).Item("WHTAmount").ToString, 0)
                    lblTotalKewajiban.Text = FormatNumber(dt.Rows(0).Item("BillAmount").ToString, 0)
                    lblTotalBayar.Text = FormatNumber(dt.Rows(0).Item("BillAmount").ToString, 0)
                    lblTotalAmount.Text = FormatNumber(dt.Rows(0).Item("BillAmount") + dt.Rows(0).Item("Denda"), 0)
                    lblDenda.Text = FormatNumber(dt.Rows(0).Item("Denda").ToString, 0)
                    txtBayarDenda.Text = "0"

                    pnlView.Visible = True
                    pnlList.Visible = False
                Else
                    ShowMessage(lblMessage, "Data not found", True)
                    pnlView.Visible = False
                    pnlList.Visible = True
                End If

        End Select
    End Sub

    Private Sub btnTerima_Click(sender As Object, e As System.EventArgs) Handles btnTerima.Click
        ' disini perlu di save dulu
        Dim tClass As New Parameter.InvoiceOL
        tClass.strConnection = GetConnectionString()
        tClass.ApplicationID = oClass.ApplicationID
        tClass.InvoiceNo = oClass.InvoiceNo
        tClass.BankAccountID = oPaymentDetail.BankAccount
        tClass.PaymentDate = ConvertDate2(oPaymentDetail.ValueDate)
        tClass.PaidAmount = CDbl(txtBayarDenda.Text) + CDbl(lblTotalKewajiban.Text)
        tClass.BayarDenda = CDbl(txtBayarDenda.Text)
        tClass.LoginId = Me.Loginid
        Try
            oController.InvoiceOLExecute(tClass)
            '---------------------------------------------------------------------------
            'Wira 20160624
            'Tambah cetakan
            '---------------------------------------------------------------------------
        
            Dim cookie As HttpCookie = Request.Cookies("InstallRCVOLPrint")
            If Not cookie Is Nothing Then
                cookie.Values("ApplicationID") = oClass.ApplicationID
                cookie.Values("InvoiceNo") = oClass.InvoiceNo
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("InstallRCVOLPrint")
                cookieNew.Values.Add("ApplicationID", oClass.ApplicationID)
                cookieNew.Values.Add("InvoiceNo", oClass.InvoiceNo)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("InstallRCVOLViewer.aspx")
            '---------------------------------------------------------------------------
            ShowMessage(lblMessage, "Success - Payment sudah diterima", False)
            pnlView.Visible = False
            pnlList.Visible = True
            Response.Redirect("installrcvol.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, "Error - " & ex.Message, True)
        End Try

    End Sub


    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnlView.Visible = False
        pnlList.Visible = True
    End Sub

End Class