﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallRcvList_Collection
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents txtAngsuran1 As ucNumberFormat
    Protected WithEvents txtAngsuran2 As ucNumberFormat
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oViewPaymentAllocate As ucPaymentAllocate
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 6
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        AddHandler oViewPaymentAllocate.PaymentAllocateEvent, AddressOf PaymentAllocateChanged
        If Not IsPostBack Then
            If Request.QueryString("filekwitansi") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            If Request.QueryString("Message") <> "" Then 
                ShowMessage(lblMessage, Request.QueryString("Message"), True)
            End If
            Me.FormID = "INSTALLRCV_COL"
            oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-LicensePlate, No Polisi"
            oSearchBy.BindData()
            pnlAlokasi.Visible = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

     

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String, Optional isFrNav As Boolean = False)
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With

        oCustomClass = oController.AgreementList(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        DtgAgree.DataSource = oCustomClass.ListAgreement

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#End Region

#Region "Navigation" 

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
      
        If e.Item.ItemIndex >= 0 Then
            Dim lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            
            Dim lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            Dim lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            Dim lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind(e.SortExpression, Me.SearchBy)
    End Sub
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SortBy, Me.SearchBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click        
        oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-LicensePlate, No Polisi"
        oSearchBy.BindData()
        oSearchBy.Text = ""
        txtAngsuran1.Text = "0"
        txtAngsuran2.Text = "0"
        Me.SortBy = ""
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "'"
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "'")        

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")            
        End If
        If CDbl(IIf(txtAngsuran1.Text.Trim = "", "0", txtAngsuran1.Text.Trim)) > 0 Then
            If CDbl(IIf(txtAngsuran2.Text.Trim = "", "0", txtAngsuran2.Text.Trim)) > 0 Then
                strSearch.Append(" and InstallmentAmount between '" & CDbl(txtAngsuran1.Text) & "' and '" & CDbl(txtAngsuran2.Text) & "' ")
            Else
                strSearch.Append(" and InstallmentAmount = '" & CDbl(txtAngsuran1.Text) & "' ")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If sessioninvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
           
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/AgreementListReport.aspx")
            'End If
        End If
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Receive"
                'Dim lblApplicationid As HyperLink
                'lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                'oCustomClass.ApplicationID = lblApplicationid.Text
                'oCustomClass.BranchId = oBranch.BranchID.Trim
                'HttpContext.Current.Items("Receive") = oCustomClass
                'Server.Transfer("InstallRCV_Collection.aspx")                


                Dim oController As New UCPaymentInfoController
                Dim oClass As New Parameter.AccMntBase With {
                    .ApplicationID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink).Text,
                    .BranchId = oBranch.BranchID.Trim,
                    .InsSeqNo = DtgAgree.Items(e.Item.ItemIndex).Cells(10).Text.Trim,
                    .ValueDate = Me.BusinessDate,
                    .strConnection = GetConnectionString()
                    }
                oController.GetPaymentInfo(oClass)

                oViewPaymentAllocate.PaymentInfo(oClass)
                pnlAlokasi.Visible = True
                pnlList.Visible = False
        End Select
    End Sub

    Sub PaymentAllocateChanged(ByVal sender As Object, ByVal e As PayAllocateEventArgs)
        If (Not e.Ok) Then
            pnlAlokasi.Visible = False
            pnlList.Visible = True
            Return
        End If
        Session.Add("oCustomClass", e.OClass)
        Server.Transfer("InstallRCV_Collection.aspx")
    End Sub


End Class