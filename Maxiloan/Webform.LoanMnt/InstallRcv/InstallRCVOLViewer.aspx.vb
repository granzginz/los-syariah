﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class InstallRCVOLViewer
    Inherits Maxiloan.Webform.WebBased

    Private oController As New InvoiceOLController
    Private oParameter As New Parameter.InvoiceOL

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property ReturnPage As String
        Get
            Return CType(ViewState("ReturnPage"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ReturnPage") = Value
        End Set
    End Property

    Public Property oClass() As Parameter.InvoiceOL
        Get
            Return CType(ViewState("InvoiceOLClass"), Parameter.InvoiceOL)
        End Get
        Set(ByVal Value As Parameter.InvoiceOL)
            ViewState("InvoiceOLClass") = Value
        End Set
    End Property

    Private Property InvoiceNo() As String
        Get
            Return CType(ViewState("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        ReturnPage = ""
        bindReport()
    End Sub

    Private Sub bindReport()
        GetCookies()

        Dim oData As New DataTable
        Dim oReport As RptBuktiPembayaranOL = New RptBuktiPembayaranOL

        With oParameter
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .InvoiceNo = Me.InvoiceNo
        End With
        oData = oController.GetInstallRCVPrint(oParameter)

        If (oData.Rows.Count <= 0) Then
            Exit Sub
        End If

        oReport.SetDataSource(oData)
        oData = oController.GetInstallRCVPrint(oParameter)

        oReport.SetDataSource(oData)
        CrystalReportViewer.ReportSource = oReport
        CrystalReportViewer.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer        

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "ApplicationID" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "InstallRCVPrint.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        If Not (String.IsNullOrEmpty(ReturnPage)) Then
            Response.Redirect(String.Format("{0}.aspx?strFileLocation={1}{2}ApplicationID", ReturnPage, Me.Session.SessionID, Me.Loginid))
        Else
            Response.Redirect("InstallRCVOL.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "InstallRCVPrint")
        End If
    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("InstallRCVOLPrint")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.InvoiceNo = cookie.Values("InvoiceNo")
        ReturnPage = cookie.Values("ReturnPage")
    End Sub
End Class