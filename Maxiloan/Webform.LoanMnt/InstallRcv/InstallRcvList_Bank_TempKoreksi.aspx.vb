﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class InstallRcvList_Bank_TempKoreksi
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll    

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
    Private oDataUserCtlr As New DataUserControlController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler oViewPaymentAllocate.PaymentAllocateEvent, AddressOf PaymentAllocateChanged
        If Not IsPostBack Then
            If Request.QueryString("filekwitansi") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If

            If Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("Message"), True)
            End If
            pnlAlokasi.Visible = False
            Me.FormID = "INSTALLRCV_BA"
            oSearchBy.ListData = "ReceivedFrom, Nama-Agreement.AgreementNo, No. Kontrak"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = "CBT.VoucherNo ASC"
                With cmbBankAccount
                    .DataSource = oDataUserCtlr.GetBankAccount(GetConnectionString, Me.sesBranchId, "B", "EC")
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = ""
                End With
            End If
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)        
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
            .SpName = "spCashBankKoreksiPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound                
        'Dim lblCustID As Label
        'Dim lblName As HyperLink        

        If e.Item.ItemIndex >= 0 Then
            
            'lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            'lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            'If lblCustID.Text.Trim.Length > 0 Then
            '    lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            'End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SearchBy = " CBT.branchid = '" & oBranch.BranchID.Trim & "'"
        Me.SortBy = "CBT.VoucherNo ASC"        
        oSearchBy.Text = ""
        oSearchBy.ListData = "ReceivedFrom, Nama-Agreement.AgreementNo, No. Kontrak"
        oSearchBy.BindData()
        DoBind(Me.SortBy, Me.SearchBy)
        pnlDatagrid.Visible = False
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" CBT.branchid = '" & oBranch.BranchID.Trim & "'")
        
        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")            
        End If
        If cmbBankAccount.SelectedValue.Trim <> "" Then
            strSearch.Append(" and BankAccount.BankAccountID='" & cmbBankAccount.SelectedValue & "'")
        End If

        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            ' If Me.checkCashier() Then

            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/AgreementListReport.aspx")
            'End If
        End If
    End Sub
    Protected WithEvents oViewPaymentAllocate As ucPaymentAllocate
    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                'Dim lblVoucherNo As HyperLink
                ''Dim lblApplicationid As Label
                'Dim lblInsSeqNo As Label
                'Dim lblBankAccountID As Label

                'lblVoucherNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblVoucherNo"), HyperLink)
                '' lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), Label)
                'lblInsSeqNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblInsSeqNo"), Label)
                'lblBankAccountID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblBankAccountID"), Label)

                ''oCustomClass.ApplicationID = lblApplicationid.Text.Trim
                '' oCustomClass.BranchId = oBranch.BranchID.Trim
                'oCustomClass.InsSeqNo = lblInsSeqNo.Text.Trim
                'oCustomClass.VoucherNo = lblVoucherNo.Text.Trim
                'oCustomClass.BankAccountID = lblBankAccountID.Text.Trim
                'oCustomClass.PostingDate = ConvertDate2(IIf(DtgAgree.Items(e.Item.ItemIndex).Cells(3).Text.Trim = "", "01/01/1900", DtgAgree.Items(e.Item.ItemIndex).Cells(3).Text.Trim))
                'oCustomClass.ValueDate = ConvertDate2(IIf(DtgAgree.Items(e.Item.ItemIndex).Cells(4).Text.Trim = "", "01/01/1900", DtgAgree.Items(e.Item.ItemIndex).Cells(4).Text.Trim))
                'HttpContext.Current.Items("ReceiveBankTempKoreksi") = oCustomClass
                'Server.Transfer("InstallRCV_Bank_TempKoreksi.aspx")


                Dim oController As New UCPaymentInfoController
                Dim oClass As New Parameter.AccMntBase With {
                    .ApplicationID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), Label).Text,
                    .BranchId = oBranch.BranchID.Trim,
                    .InsSeqNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblInsSeqNo"), Label).Text,
                    .BankAccountID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblBankAccountID"), Label).Text,
                    .ValueDate = ConvertDate2(IIf(DtgAgree.Items(e.Item.ItemIndex).Cells(4).Text.Trim = "", "01/01/1900", DtgAgree.Items(e.Item.ItemIndex).Cells(4).Text.Trim)),
                .strConnection = GetConnectionString(),
                    .ReferenceNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblVoucherNo"), HyperLink).Text
                    }
                oController.GetPaymentInfo(oClass)

                oViewPaymentAllocate.PaymentInfo(oClass)
                pnlAlokasi.Visible = True
                pnlList.Visible = False
        End Select
    End Sub

    Sub PaymentAllocateChanged(ByVal sender As Object, ByVal e As PayAllocateEventArgs)
        If (Not e.Ok) Then
            pnlAlokasi.Visible = False
            pnlList.Visible = True
            Return
        End If
        Session.Add("oCustomClass", e.OClass)
        Server.Transfer("InstallRCV_Bank_TempKoreksi.aspx")
    End Sub
End Class