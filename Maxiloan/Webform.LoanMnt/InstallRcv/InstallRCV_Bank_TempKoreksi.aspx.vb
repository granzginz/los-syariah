﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InstallRCV_Bank_TempKoreksi
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oViewPaymentDetail As UCViewPaymentDetail
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule
    Protected WithEvents oValueDate As ucDateCE

    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String


#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property

    Private Property InsSeqNo() As String
        Get
            Return (CType(ViewState("InsSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InsSeqNo") = Value
        End Set
    End Property
    Private Property VoucherNo() As String
        Get
            Return (CType(ViewState("VoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("VoucherNo") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return (CType(ViewState("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property
    Private Property PostingDate() As Date
        Get
            Return (CType(ViewState("PostingDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("PostingDate") = Value
        End Set
    End Property
    Private Property _ValueDate() As Date
        Get
            Return (CType(ViewState("_ValueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("_ValueDate") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Public Property oClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'Me.ApplicationID = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).ApplicationID
            'Me.BranchID = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).BranchId
            'Me.InsSeqNo = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).InsSeqNo
            'Me.VoucherNo = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).VoucherNo
            'Me.BankAccountID = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).BankAccountID
            'Me.PostingDate = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).PostingDate
            'Me._ValueDate = CType(HttpContext.Current.Items("ReceiveBankTempKoreksi"), Parameter.AgreementList).ValueDate


            oClass = Session("oCustomClass")
            Me.FormID = "INSTALLRCV_BA"
            Me.Mode = "Normal"
            Me.VoucherNo = oClass.ReferenceNo
            oCashier.CashierID = ""
            oCashier.HeadCashier = True
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then 
                InitialDefaultPanel()

                'Dim oController As New UCPaymentInfoController
                'Dim oCustomClass As New Parameter.AccMntBase
                'With oCustomClass
                '    .strConnection = GetConnectionString()
                '    .ApplicationID = Me.ApplicationID
                '    .ValueDate = ConvertDate2(Me._ValueDate.ToString("dd/MM/yyyy"))
                '    .BranchId = Me.BranchID
                'End With 
                'oClass = oController.GetPaymentInfo(oCustomClass) 
                BindData()                
            End If
        End If
    End Sub

    Sub BindData()
        oValueDate.AutoPostBack = True
        oValueDate.Text = oClass.ValueDate.ToString("dd/MM/yyyy")
        DoBind()
        
        'load grid angsuran
        oInstallmentSchedule.ApplicationId = oClass.ApplicationID
        oInstallmentSchedule.ValueDate = oClass.ValueDate
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)

        LoadNextStep()
        BlokirBayarStep()

        If Not IsNothing(oClass.BankAccountID) Then
            oPaymentDetail.ResultcmbBankAccount = oClass.BankAccountID.Trim
            oPaymentDetail.cmbBankAccountEnabled = True
        End If
    End Sub

    Private Sub LoadNextStep()
        NextProcess()

        If Me.WayOfPayment <> "CP" Then
            If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                Else
                    If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                        oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                        oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                    Else
                        oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                    End If
                End If
            Else
                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
            End If
        End If
        oPaymentAllocationDetail.IsiKolomTagihan() 
        oPaymentAllocationDetail.BindPrioritasPembayaran(Me.BusinessDate, True, oInstallmentSchedule.GetAllAngsuranUnPaid)
    End Sub
    Private Sub NextProcess()

        With oPaymentInfo
            .PaymentInfo(oClass)

            Me.ValidationData = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
        End With
        Me.AmountReceive = oPaymentDetail.AmountReceive
        oPaymentAllocationDetail.PaymentAllocationBind(oClass)
    End Sub
    Private Sub InitialDefaultPanel()
        pnlPaymentDetail.Visible = True
        pnlViewPaymentDetail.Visible = False
        pnlPaymentAllocation.Visible = True
        pnlBtnGroupPaymentReceive.Visible = False
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlPaymentInfo.Visible = False
        pnlHeadCashierPassword.Visible = False
        pnlConfirmation.Visible = False 
    End Sub

    Private Sub DoBind()  
        Dim oCustomClass = oClass 
        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType 
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID.Trim) & "')"
            Me.NextInstallmentDate = .NextInstallmentDate 
            lblPrepaid.Text = FormatNumber(.Prepaid, 0)
            Me.PrepaidBalance = .Prepaid
            Me.AmountToBePaid = .AmountToBePaid
            lblAmountToBePaid.Text = FormatNumber(Me.AmountToBePaid, 0)
             
            oPaymentAllocationDetail.ToleransiBayarKurang = .ToleransiBayarKurang
            oPaymentAllocationDetail.ToleransiBayarLebih = .ToleransiBayarLebih

            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "BA"
            oPaymentDetail.DoFormCashModule()
            oPaymentDetail.AmountReceive = .AmountReceiveInsurance
            oPaymentDetail.ShowTglBayar = False
            oPaymentDetail.ResultcmbBankAccount = .BankAccountID
            oPaymentDetail.cmbBankAccountEnabled = False
            oPaymentDetail.ValueDate = .ValueDate.ToString("dd/MM/yyyy")
             
            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub

#Region "BtnGroup Payment Receive"
    Private Sub imbNextProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbNextProcess.Click
        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            lblMessage.Text = ""
            If oPaymentDetail.WayOfPayment = "CP" Then
                If Me.BranchID <> Me.sesBranchId.Trim.Replace("'", "") Then

                    ShowMessage(lblMessage, "Cara Pembayaran Tidak bisa untuk Transaksi Antar Cabang", True)
                    Exit Sub
                End If
            End If
            If Me.IsValidLastPayment(oClass.ApplicationID, ConvertDate2(oValueDate.Text)) Then
                If Me.Mode = "Normal" Then
                    NextProcess()
                    ShowMessage(lblMessage, "Kasir belum Buka", True)
                    Exit Sub
                    If Me.WayOfPayment = "CP" Then
                        If Me.PrepaidBalance = 0 Then
                            ShowMessage(lblMessage, "Tidak bisa Alokasi Saldo Prepaid, Karena Saldo Kurang", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    Else
                        If oPaymentDetail.AmountReceive = 0 Then
                            ShowMessage(lblMessage, "Harap diisi dengan Jumlah diterima", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                        Session.Add("BankSelected", oPaymentDetail.BankAccount)
                    End If
                    If IsMaxBacDated(ConvertDate2(oValueDate.Text)) Then
                        oPaymentDetail.WayOfPayment = "0"
                        Me.Mode = "NotNormal"
                        pnlHeadCashierPassword.Visible = True
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        oCashier.CashierID = ""
                        oCashier.HeadCashier = True

                        Exit Sub
                    ElseIf DateDiff(DateInterval.Day, ConvertDate2(oValueDate.Text), Me.BusinessDate) < 0 Then
                        oPaymentDetail.WayOfPayment = "0"

                        ShowMessage(lblMessage, "Tanggal Valuta tidak boleh lebih besar dari tgl hari ini", True)
                        Exit Sub
                    End If


                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = True
                    pnlPaymentAllocation.Visible = True
                    pnlPaymentInfo.Visible = True
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = True
                    If Me.WayOfPayment <> "CP" Then
                        If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                            If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                            Else
                                If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                                Else
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                                End If
                            End If
                        Else
                            oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                        End If

                        oPaymentAllocationDetail.IsiKolomTagihan() 'scl

                    End If
                    'ThreadNextProcess.Start()
                ElseIf Me.Mode = "NotNormal" Then
                    Dim objDecent As New Decrypt.am_futility
                    Dim oLogin As New LoginController
                    Dim oLoginEntities As New Parameter.Login
                    Dim strError As String
                    oLoginEntities.strConnection = GetConnectionString()
                    oLoginEntities.LoginId = oCashier.CashierID.Trim
                    Dim lstrPassword As String
                    'oLogin.GetEmployee(oLoginEntities) retreive password dari user
                    lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
                    If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = True
                        pnlPaymentAllocation.Visible = True
                        pnlPaymentInfo.Visible = True
                        pnlHeadCashierPassword.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = True
                        Me.Mode = "Normal"
                    Else

                        ShowMessage(lblMessage, "Password Salah", True)
                        Exit Sub
                    End If
                End If
            Else

                ShowMessage(lblMessage, "Tanggal Valuta Lebih kecil dari tanggal pembayaran terakhir", True)
                Exit Sub
            End If
        End If
    End Sub

   

    Private Sub imbCancelProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelProcess.Click
        Response.Redirect("InstallRcvList_Bank_TempKoreksi.aspx")
    End Sub
#End Region

#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If ConvertDate2(oValueDate.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Bayar tidak boleh lebih besar dari tanggal hari ini", True)
                Exit Sub
            End If

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then 
                With oPaymentInfo
                    .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                    .ApplicationID = oClass.ApplicationID
                    .PaymentInfo() 
                    LastAR = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
                End With
                If Me.ValidationData <> LastAR Then

                    ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)
                Else
                    TotalAllocation = oPaymentAllocationDetail.CountTotalBayar()
                    'With oPaymentAllocationDetail
                    '    TotalAllocation = .InstallmentDue + .LCInstallment + .InstallmentCollFee + _
                    '                    .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                    '                    .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                    '                    .RepossessionFee + .Prepaid + .PLL

                    'End With

                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .BranchId = oClass.BranchId
                        .ApplicationID = oClass.ApplicationID
                        .AmountReceive = oPaymentAllocationDetail.InstallmentDue
                    End With
                    If oController.IsLastPayment(oCustomClass) Then
                        pnlConfirmation.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        pnlHeadCashierPassword.Visible = False
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = False
                        Exit Sub
                    End If
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .ValueDate = ConvertDate2(oValueDate.Text)
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .CoyID = Me.SesCompanyID
                        .BusinessDate = Me.BusinessDate
                        .ReceivedFrom = oPaymentDetail.ReceivedFrom
                        .ReferenceNo = oPaymentDetail.ReferenceNo
                        .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
                        .LoginId = Me.Loginid
                        '.BankAccountID = Me.BankAccount
                        .BankAccountID = oPaymentDetail.BankAccount
                        .Notes = oPaymentDetail.Notes
                        .ApplicationID = oClass.ApplicationID

                        '-----------Installment
                        .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                        .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                        .LcInstallment = oPaymentAllocationDetail.LCInstallment
                        .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                        .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                        .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                        '------------------------

                        '----------Insurance
                        .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                        .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                        .LcInsurance = oPaymentAllocationDetail.LCInsurance
                        .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                        .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                        .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                        '-----------------------------

                        .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                        .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                        .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                        .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                        .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                        .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                        .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                        .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                        .Prepaid = oPaymentAllocationDetail.Prepaid
                        .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                        .AmountReceive = TotalAllocation
                        .BayarDi = oPaymentDetail.BayarDi
                        .CollectorID = oPaymentDetail.CollectorID

                        .PLL = oPaymentAllocationDetail.PLL
                        .PLLDesc = oPaymentAllocationDetail.PLLDesc
                        .InsSeqNo = CInt(oClass.InsSeqNo)
                        .FormID = Me.FormID
                        .Status = "UPDATE"
                        .VoucherNO = Me.VoucherNo.Trim
                    End With

                    Session.Add("BankSelected", oPaymentDetail.BankAccount)

                    Dim strError As String

                    Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        SavingController.InstallRCVBATemp()
                        BindReport()

                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvList_Bank_TempKoreksi.aspx")
    End Sub
#End Region

    Sub BindReport() 
        Response.Redirect("InstallRcvList_Bank_TempKoreksi.aspx")
    End Sub

    Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then 
                With oPaymentInfo
                    .ValueDate = ConvertDate2(oValueDate.Text)
                    .ApplicationID = Me.ApplicationID
                    .PaymentInfo() 
                    LastAR = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
                End With
                If Me.ValidationData <> LastAR Then

                    ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
                Else
                    TotalAllocation = oPaymentAllocationDetail.CountTotalBayar()
                    'With oPaymentAllocationDetail
                    '    TotalAllocation = .InstallmentDue + .LCInstallment + .InstallmentCollFee + _
                    '                    .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                    '                    .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                    '                    .RepossessionFee + .Prepaid

                    'End With
                    If Me.WayOfPayment = "CP" Then
                        If TotalAllocation > Me.PrepaidBalance Then

                            ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
                            Exit Sub
                        End If
                        If oPaymentAllocationDetail.Prepaid > 0 Then

                            ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
                            Exit Sub
                        End If
                    Else
                        If TotalAllocation - oPaymentAllocationDetail.PLL > LastAR Then
                            ShowMessage(lblMessage, "Alokasi pembayaran harus lebih kecil atau sama dengan sisa angsuran", True)
                            Exit Sub
                        End If
                    End If
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .ValueDate = ConvertDate2(oViewPaymentDetail.ValueDate)
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .CoyID = Me.SesCompanyID
                        .BusinessDate = Me.BusinessDate
                        .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
                        .ReferenceNo = oViewPaymentDetail.ReferenceNo
                        .WOP = Me.WayOfPayment.Trim
                        .LoginId = Me.Loginid
                        .BankAccountID = oPaymentDetail.BankAccount ' Me.BankAccount
                        .Notes = oViewPaymentDetail.Notes
                        .ApplicationID = oClass.ApplicationID

                        '-----------Installment
                        .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                        .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                        .LcInstallment = oPaymentAllocationDetail.LCInstallment
                        .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                        .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                        .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                        '------------------------

                        '----------Insurance
                        .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                        .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                        .LcInsurance = oPaymentAllocationDetail.LCInsurance
                        .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                        .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                        .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                        '-----------------------------

                        .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                        .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                        .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                        .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                        .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                        .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                        .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                        .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                        .Prepaid = oPaymentAllocationDetail.Prepaid
                        .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                        .AmountReceive = TotalAllocation

                        .PLL = oPaymentAllocationDetail.PLL
                        .PLLDesc = oPaymentAllocationDetail.PLLDesc
                    End With
                    Dim strError As String

                    Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        SavingController.InstallmentReceive()
                        BindReport()

                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub imbCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelLastPayment.Click
        Response.Redirect("InstallRcvList_Bank_TempKoreksi.aspx")
    End Sub

    'Private Sub IsiKolomTagihan()
    '    Dim oCustom As New Parameter.RefundInsentif
    '    Dim m_Insentif As New RefundInsentifController

    '    With oCustom
    '        .strConnection = GetConnectionString()
    '        .WhereCond = " VoucherNo = '" & Me.VoucherNo.Trim & "'"
    '        .SPName = "spGetTransBeforePosting"
    '    End With

    '    oCustom = m_Insentif.GetSPBy(oCustom)

    '    If oCustom.ListDataTable.Rows.Count > 0 Then
    '        With oPaymentAllocationDetail
    '            .InstallmentDue0 = CDbl(oCustom.ListDataTable.Rows(0).Item("InstallmentDuePaid").ToString)
    '            .LCInstallment0 = CDbl(oCustom.ListDataTable.Rows(0).Item("LCInstallmentPaid").ToString)
    '            .InstallmentCollFee0 = CDbl(oCustom.ListDataTable.Rows(0).Item("InstallmentCollPaid").ToString)
    '            .InsuranceDue0 = CDbl(oCustom.ListDataTable.Rows(0).Item("InsuranceDuePaid").ToString)
    '            .LCInsurance0 = CDbl(oCustom.ListDataTable.Rows(0).Item("LCInsurancePaid").ToString)
    '            .InsuranceCollFee0 = CDbl(oCustom.ListDataTable.Rows(0).Item("InsuranceCollPaid").ToString)
    '            .PDCBounceFee0 = CDbl(oCustom.ListDataTable.Rows(0).Item("PDCBounceFeePaid").ToString)
    '            .STNKRenewalFee0 = CDbl(oCustom.ListDataTable.Rows(0).Item("STNKRenewalPaid").ToString)
    '            .InsuranceClaimExpense0 = CDbl(oCustom.ListDataTable.Rows(0).Item("InsuranceClaimExpensePaid").ToString)
    '            .RepossessionFee0 = CDbl(oCustom.ListDataTable.Rows(0).Item("RepossessionFeePaid").ToString)
    '            .Prepaid0 = CDbl(oCustom.ListDataTable.Rows(0).Item("PrepaidPaid").ToString)
    '            .PLL0 = CDbl(oCustom.ListDataTable.Rows(0).Item("PLL").ToString)

    '            .totalTagihan = .InstallmentDue0 + .LCInstallment0 + .InstallmentCollFee0 + .InsuranceDue0 + _
    '                   .LCInsurance0 + .InsuranceCollFee0 + .PDCBounceFee0 + .STNKRenewalFee0 + .InsuranceClaimExpense0 + _
    '                   .RepossessionFee0 + .Prepaid0 + .PLL0

    '            .PrioritasPembayaran = Me.PrioritasPembayaran

    '            .InstallmentDue = .InstallmentDue0
    '            .LCInstallment = .LCInstallment0
    '            .InstallmentCollFee = .InstallmentCollFee0
    '            .InsuranceDue = .InsuranceDue0
    '            .LCInsurance = .LCInsurance0
    '            .InsuranceCollFee = .InsuranceCollFee0
    '            .PDCBounceFee = .PDCBounceFee0
    '            .STNKRenewalFee = .STNKRenewalFee0
    '            .InsuranceClaimExpense = .InsuranceClaimExpense0
    '            .RepossessionFee = .RepossessionFee0
    '            .Prepaid = .Prepaid0
    '            .PLL = .PLL0

    '            .totalBayar = .InstallmentDue + .LCInstallment + .InstallmentCollFee + .InsuranceDue + _
    '                   .LCInsurance + .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
    '                   .RepossessionFee + .Prepaid + .PLL
    '        End With
    '    End If 
    'End Sub

    Private Sub BlokirBayarStep()
        If BlokirBayar Then
            imbSaveProcess.Visible = False
            ShowMessage(lblMessage, "Pembayaran ini diblokir", True)
        Else
            imbSaveProcess.Visible = True
        End If
    End Sub

    Private Sub oValueDate_TextChanged(sender As Object, e As System.EventArgs) Handles oValueDate.TextChanged
        DoBind()
        LoadNextStep()
        BlokirBayarStep()
        oInstallmentSchedule.ApplicationId = oClass.ApplicationID
        oInstallmentSchedule.ValueDate = ConvertDate2(oValueDate.Text)
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)
        If Not IsNothing(Session("BankSelected")) Then
            oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
            oPaymentDetail.cmbBankAccountEnabled = False
        End If
    End Sub
    Private Sub Jlookup_Click(sender As Object, e As System.EventArgs) Handles Jlookup.Click
        oClass.ApplicationID = hdnAppID.Value.Trim
        oClass.CustomerID = hdnCustID.Value.Trim
        oClass.InsSeqNo = hdnInsSeqNo.Value.Trim
        BindData()
    End Sub

End Class