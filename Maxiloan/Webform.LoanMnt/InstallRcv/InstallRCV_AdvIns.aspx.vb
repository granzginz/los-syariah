﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController

#End Region

Public Class InstallRCV_AdvIns
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oViewPaymentDetail As UCViewPaymentDetail
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule
    Protected WithEvents oValueDate As ucDateCE

    Private Const SPTYPE_2 As String = "SP2"
    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String


#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property

    Private Property ReferenceNo() As String
        Get
            Return (CType(ViewState("ReferenceNo"), String))
        End Get
        Set(ByVal ReferenceNo As String)
            ViewState("ReferenceNo") = ReferenceNo
        End Set
    End Property
#End Region
    Public Property oClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property
#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            'Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId

            oClass = Session("oCustomClass")

            Me.FormID = "ADVINS"
            Me.Mode = "Normal"
            oCashier.CashierID = ""
            oCashier.HeadCashier = True
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
 
                InitialDefaultPanel()
                oValueDate.AutoPostBack = True
                oValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy") 

                DoBind()

                'load grid angsuran
                oInstallmentSchedule.ApplicationId = oClass.ApplicationID
                oInstallmentSchedule.ValueDate = ConvertDate2(oValueDate.Text)
                oInstallmentSchedule.DoBind_Angsuran(String.Empty)

                LoadNextStep()
                BlokirBayarStep()

                If Not IsNothing(Session("BankSelected")) Then
                    oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
                    oPaymentDetail.cmbBankAccountEnabled = False
                End If
            End If 
        End If 
    End Sub
    Private Sub DoBind()
        'Dim oController As New UCPaymentInfoController
        'Dim oCustomClass As New Parameter.AccMntBase
        'With oCustomClass
        '    .strConnection = GetConnectionString()
        '    .ApplicationID = Me.ApplicationID 
        '    .ValueDate = ConvertDate2(oValueDate.Text) 
        '    .BranchId = Me.BranchID
        'End With

        'oClass = oController.GetPaymentInfo(oCustomClass)
        Dim oCustomClass = oClass
         
        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType 
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID.Trim) & "')"
            lblPrepaid.Text = FormatNumber(.Prepaid, 0)
            lblAmountToBePaid.Text = FormatNumber(.AmountToBePaid, 0) 
            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "BA"
            oPaymentDetail.DoFormCashModule()
            oPaymentDetail.BindRekeningCashModule("", "AdvIns")
            oPaymentDetail.AmountReceive = .AmountReceiveInsurance
            oPaymentDetail.ShowTglBayar = False 
            oPaymentDetail.ValueDate = .ValueDate.ToString("dd/MM/yyyy")

            Me.NextInstallmentDate = .NextInstallmentDate  
            Me.PrepaidBalance = .Prepaid
            Me.AmountToBePaid = .AmountToBePaid 
            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub
    Private Sub LoadNextStep()
        NextProcess()

        If Me.WayOfPayment <> "CP" Then
            If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                Else
                    If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                        oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                        oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                    Else
                        oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                    End If
                End If
            Else
                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
            End If 
            oPaymentAllocationDetail.IsiKolomTagihan()
            oPaymentAllocationDetail.BindPrioritasPembayaran(Me.BusinessDate, , oInstallmentSchedule.GetAllAngsuranUnPaid)
        End If
    End Sub

    Private Sub NextProcess() 
        With oPaymentInfo
            .PaymentInfo(oClass) 
            Me.ValidationData = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
        End With
        Me.AmountReceive = oPaymentDetail.AmountReceive
        oPaymentAllocationDetail.PaymentAllocationBind(oClass)
    End Sub 
    Private Sub InitialDefaultPanel()
        pnlPaymentDetail.Visible = True
        pnlViewPaymentDetail.Visible = False
        pnlPaymentAllocation.Visible = True
        pnlBtnGroupPaymentReceive.Visible = False
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlPaymentInfo.Visible = False
        pnlHeadCashierPassword.Visible = False
        pnlConfirmation.Visible = False 
    End Sub
     
#Region "BtnGroup Payment Receive"
    Private Sub imbNextProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbNextProcess.Click
        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            lblMessage.Text = ""
            If oPaymentDetail.WayOfPayment = "CP" Then
                If oClass.BranchId <> Me.sesBranchId.Trim.Replace("'", "") Then
                    ShowMessage(lblMessage, "Cara Pembayaran Tidak bisa untuk Transaksi Antar Cabang", True)
                    Exit Sub
                End If
            End If
            If Me.IsValidLastPayment(oClass.ApplicationID, ConvertDate2(oPaymentDetail.ValueDate)) Then
                If Me.Mode = "Normal" Then
                    NextProcess()
                    ShowMessage(lblMessage, "Kasir belum Buka", True)
                    Exit Sub
                    'End If
                    If Me.WayOfPayment = "CP" Then
                        If Me.PrepaidBalance = 0 Then
                            ShowMessage(lblMessage, "Tidak bisa Alokasi Saldo Prepaid, Karena Saldo Kurang", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    Else
                        If oPaymentDetail.AmountReceive = 0 Then
                            ShowMessage(lblMessage, "Harap diisi dengan Jumlah diterima", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                        Session.Add("BankSelected", oPaymentDetail.BankAccount)
                    End If
                    If IsMaxBacDated(ConvertDate2(oPaymentDetail.ValueDate)) Then
                        oPaymentDetail.WayOfPayment = "0"
                        Me.Mode = "NotNormal"
                        pnlHeadCashierPassword.Visible = True
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        oCashier.CashierID = ""
                        oCashier.HeadCashier = True
                        Exit Sub
                    ElseIf DateDiff(DateInterval.Day, ConvertDate2(oPaymentDetail.ValueDate), Me.BusinessDate) < 0 Then
                        oPaymentDetail.WayOfPayment = "0"
                        ShowMessage(lblMessage, "Tanggal Valuta tidak boleh lebih besar dari tgl hari ini", True)
                        Exit Sub
                    End If


                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = True
                    pnlPaymentAllocation.Visible = True
                    pnlPaymentInfo.Visible = True
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = True
                    If Me.WayOfPayment <> "CP" Then
                        If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                            If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                            Else
                                If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                                Else
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                                End If
                            End If
                        Else
                            oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                        End If

                        oPaymentAllocationDetail.IsiKolomTagihan() 'scl

                    End If
                    'ThreadNextProcess.Start()
                ElseIf Me.Mode = "NotNormal" Then
                    Dim objDecent As New Decrypt.am_futility
                    Dim oLogin As New LoginController
                    Dim oLoginEntities As New Parameter.Login
                    Dim strError As String
                    oLoginEntities.strConnection = GetConnectionString()
                    oLoginEntities.LoginId = oCashier.CashierID.Trim
                    Dim lstrPassword As String
                    'oLogin.GetEmployee(oLoginEntities) retreive password dari user
                    lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
                    If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = True
                        pnlPaymentAllocation.Visible = True
                        pnlPaymentInfo.Visible = True
                        pnlHeadCashierPassword.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = True
                        Me.Mode = "Normal"
                    Else

                        ShowMessage(lblMessage, "Password Salah", True)
                        Exit Sub
                    End If
                End If
            Else

                ShowMessage(lblMessage, "Tanggal Valuta Lebih kecil dari tanggal pembayaran terakhir", True)
                Exit Sub
            End If
        End If
    End Sub



    Private Sub imbCancelProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelProcess.Click
        Response.Redirect("InstallRcvList_AdvIns.aspx?Message=cancel")
    End Sub
#End Region

#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveProcess.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If Not CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                Exit Sub
            End If

            With oPaymentInfo
                LastAR = .Prepaid + _
                            .MaximumInstallment + _
                            .MaximumLCInstallFee + _
                            .MaximumInstallCollFee + _
                            .MaximumInsurance + _
                            .MaximumLCInsuranceFee + _
                            .MaximumInsuranceCollFee + _
                            .MaximumPDCBounceFee + _
                            .MaximumSTNKRenewalFee + _
                            .MaximumInsuranceClaimFee + _
                            .MaximumReposessionFee
            End With
            If Me.ValidationData <> LastAR Then
                ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)
                Exit Sub
            End If
            If oPaymentAllocationDetail.InstallmentDue > 0 And oPaymentAllocationDetail.InstallmentDue < Me.AmountReceive Then
                ShowMessage(lblMessage, "Angsuran hanya boleh diisi dengan 0 atau " & FormatNumber(Me.AmountReceive, 0), True)
                Exit Sub
            End If

            TotalAllocation = oPaymentAllocationDetail.CountTotalBayar()
            'With oPaymentAllocationDetail
            '    TotalAllocation = .InstallmentDue + .LCInstallment + .InstallmentCollFee + _
            '                    .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
            '                    .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
            '                    .RepossessionFee + .Prepaid

            'End With

            'If TotalAllocation <> oClass.ToAmountAllocate Then
            '    ShowMessage(lblMessage, String.Format("Total Alokasi Alokasi harus = {0:N0} ", oClass.ToAmountAllocate), True)
            '    Exit Sub
            'End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = oClass.BranchId
                .ApplicationID = oClass.ApplicationID
                .AmountReceive = oPaymentAllocationDetail.InstallmentDue
            End With

            If oController.IsLastPayment(oCustomClass) Then
                pnlConfirmation.Visible = True
                pnlBtnGroupPaymentAllocation.Visible = False
                pnlHeadCashierPassword.Visible = False
                pnlPaymentDetail.Visible = False
                pnlViewPaymentDetail.Visible = False
                pnlPaymentAllocation.Visible = False
                pnlPaymentInfo.Visible = False
                pnlBtnGroupPaymentReceive.Visible = False
                pnlBtnGroupPaymentAllocation.Visible = False
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                .ReceivedFrom = oPaymentDetail.ReceivedFrom
                .ReferenceNo = oPaymentDetail.ReferenceNo
                .WOP = oPaymentDetail.WayOfPayment
                .LoginId = Me.Loginid
                .BankAccountID = oPaymentDetail.BankAccount
                .Notes = oPaymentDetail.Notes
                .ApplicationID = oClass.ApplicationID

                '-----------Installment
                .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                .LcInstallment = oPaymentAllocationDetail.LCInstallment
                .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                '------------------------

                '----------Insurance
                .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                .LcInsurance = oPaymentAllocationDetail.LCInsurance
                .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                '-----------------------------

                .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                .Prepaid = oPaymentAllocationDetail.Prepaid
                .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                .AmountReceive = TotalAllocation
                .BayarDi = oPaymentDetail.BayarDi
                .CollectorID = oPaymentDetail.CollectorID
            End With

            Session.Add("BankSelected", oPaymentDetail.BankAccount)

            Dim strError As String

            Try
                Dim SavingController As New InstallRcvController(oCustomClass)
                SavingController.InstallmentReceiveAdvIns()
                Me.ReferenceNo = oCustomClass.ReferenceNo1
                BindReport()
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
      
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel2Process.Click
        Response.Redirect("InstallRcvList_AdvIns.aspx?Message=cancel")
    End Sub
#End Region

    Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If Not CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                Exit Sub
            End If

            With oPaymentInfo
                '.ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                '.ApplicationID = Me.ApplicationID
                '.PaymentInfo() 
                LastAR = .Prepaid + _
                            .MaximumInstallment + _
                            .MaximumLCInstallFee + _
                            .MaximumInstallCollFee + _
                            .MaximumInsurance + _
                            .MaximumLCInsuranceFee + _
                            .MaximumInsuranceCollFee + _
                            .MaximumPDCBounceFee + _
                            .MaximumSTNKRenewalFee + _
                            .MaximumInsuranceClaimFee + _
                            .MaximumReposessionFee
            End With
            If Me.ValidationData <> LastAR Then 
                ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
                Exit Sub
            End If

            With oPaymentAllocationDetail
                TotalAllocation = .InstallmentDue + .LCInstallment + .InstallmentCollFee + _
                                .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                                .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                                .RepossessionFee + .Prepaid

            End With

            If Me.WayOfPayment = "CP" Then
                If TotalAllocation > Me.PrepaidBalance Then
                    ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
                    Exit Sub
                End If
                If oPaymentAllocationDetail.Prepaid > 0 Then 
                    ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
                    Exit Sub
                End If
            End If

            If Me.AmountReceive <> TotalAllocation Then
                ShowMessage(lblMessage, "Total Alokasi Pembayaran harus sama dengan Jumlah diterima", True)
                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .ValueDate = ConvertDate2(oViewPaymentDetail.ValueDate)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
                .ReferenceNo = oViewPaymentDetail.ReferenceNo
                .WOP = Me.WayOfPayment.Trim
                .LoginId = Me.Loginid
                .BankAccountID = Me.BankAccount
                .Notes = oViewPaymentDetail.Notes
                .ApplicationID = oClass.ApplicationID

                '-----------Installment
                .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                .LcInstallment = oPaymentAllocationDetail.LCInstallment
                .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                '------------------------

                '----------Insurance
                .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                .LcInsurance = oPaymentAllocationDetail.LCInsurance
                .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                '-----------------------------

                .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                .Prepaid = oPaymentAllocationDetail.Prepaid
                .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim
                .AmountReceive = TotalAllocation
            End With
            Dim strError As String

            Try
                Dim SavingController As New InstallRcvController(oCustomClass)
                SavingController.InstallmentReceive() 
                BindReport() 
            Catch exp As Exception 
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If 
    End Sub

    Private Sub imbCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelLastPayment.Click
        Response.Redirect("InstallRcvList_AdvIns.aspx?Message=cancel")
    End Sub
  
    Private Sub BlokirBayarStep()
        If BlokirBayar Then
            imbSaveProcess.Visible = False
            ShowMessage(lblMessage, "Pembayaran ini diblokir", True)
        Else
            imbSaveProcess.Visible = True
        End If
    End Sub

    Private Sub oValueDate_TextChanged(sender As Object, e As System.EventArgs) Handles oValueDate.TextChanged
        DoBind()
        LoadNextStep()
        BlokirBayarStep()
        oInstallmentSchedule.ApplicationId = Me.ApplicationID
        oInstallmentSchedule.ValueDate = ConvertDate2(oValueDate.Text)
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)
        If Not IsNothing(Session("BankSelected")) Then
            oPaymentDetail.ResultcmbBankAccount = Session("BankSelected").ToString.Trim
            oPaymentDetail.cmbBankAccountEnabled = False
        End If
    End Sub

    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub

    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As RptKwitansi2 = New RptKwitansi2
        Dim oDataTable As New DataTable
        Dim oRow As DataRow

        oCustomClass.ApplicationID = oClass.ApplicationID
        oCustomClass.ReferenceNo = Me.ReferenceNo
        oCustomClass.BranchId = oClass.BranchId
        oCustomClass.strConnection = GetConnectionString()
        oData = oController.ReportKwitansiInstallmentAdvIns(oCustomClass)

        objReport.Subreports(0).SetDataSource(oData)
        objReport.SetDataSource(oData)

        'add cashier
        AddParamField(objReport, "CashierName", Me.FullName)
        AddParamField(objReport, "BusinessDate", Me.BusinessDate.ToString("dd MMMM yy"))
        AddParamField(objReport, "BranchName", oData.Tables(0).Rows(0).Item("BranchCity").ToString)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "InstallRCV_AdvIns.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()

        Response.Redirect("InstallRcvList_AdvIns.aspx?Message=sucess&strFileLocation=" & Me.Session.SessionID & Me.Loginid & "InstallRCV_AdvIns")

    End Sub
End Class