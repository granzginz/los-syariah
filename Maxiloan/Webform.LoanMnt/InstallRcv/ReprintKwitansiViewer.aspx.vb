#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ReprintKwitansiViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_coll As New InstallRcvController
    Private oCustomClass As New Parameter.InstallRcv
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents crvKwitansi As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiAgreementNo() As String
        Get
            Return CType(viewstate("MultiAgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MultiAgreementNo") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(viewstate("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MultiApplicationID") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(viewstate("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        GetCookies()
        BindReport()
    End Sub

#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As RptPrintKwitansi = New RptPrintKwitansi

        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.MultiAgreementNo = Me.MultiAgreementNo
        oCustomClass.MultiApplicationID = Me.MultiApplicationID
        oCustomClass.BranchId = Me.Branch_ID
        oCustomClass.strConnection = GetConnectionString
        oData = m_coll.ReportKwitansiInstallment(oCustomClass)

        objReport.SetDataSource(oData)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BusinessDate
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        crvKwitansi.ReportSource = objReport

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "ReprintKwitansi.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        'Dim vInfo As CrystalDecisions.Web.ViewInfo = crvKwitansi.ViewInfo

        'objReport.PrintToPrinter(1, False, 1, vInfo.LastPageNumber)
        'objReport.Close()

        Response.Redirect("ReprintKwitansi.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "ReprintKwitansi" & "&ApplicationID=" & Request.QueryString("applicationid") & "&AgreementNo=" & Request.QueryString("AgreementNo") & "&BranchID=" & Request.QueryString("BranchID") & "&HistorySequence=" & Request.QueryString("HistorySequence"))
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("ReprintKwitansiInstallment")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiAgreementNo = cookie.Values("MultiAgreementNo")
        Me.MultiApplicationID = cookie.Values("MultiApplicationID")
        Me.Branch_ID = cookie.Values("BranchID")
    End Sub
#End Region

End Class
