﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRCVOL.aspx.vb" 
Inherits="Maxiloan.Webform.LoanMnt.InstallRCVOL" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucGridNav" Src="../../webform.UserController/ucGridNav.ascx" %>  
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentDetail" Src="../../Webform.UserController/UcPaymentDetail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Penerimaan Pembayaran Operating Lease</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }


        function hitungtxt(txtidA, txtidB, txtidS) {
            var status = true;
            var A = $('#' + txtidA).val();
            var B = $('#' + txtidB).val();

            A = A.replace(/\s*,\s*/g, '');
            B = B.replace(/\s*,\s*/g, '');

            var totalBill = parseFloat(A) + parseFloat(B);

            $('#' + txtidS).val(number_format(totalBill, 0));

        }


    </script>      
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                Penerimaan Pembayaran Operating Lease
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Cari Berdasarkan Jatuh Tempo</label>
                <uc1:ucdatece id="oValueDate1" runat="server" />s/d 
                <uc1:ucdatece id="oValueDate2" runat="server" />
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR INVOICE
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                            AllowSorting="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate> 
                                        <asp:ImageButton ID="ImbTerima" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Terima"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'> </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="INVOICE NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceNo" runat="server" Visible="True" Text='<%#Container.DataItem("InvoiceNo")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="NextInstallmentNumber" DataFormatString="{0:N0}" HeaderText="KE" SortExpression="NextInstallmentNumber" />
                                <asp:BoundColumn DataField="InvoiceDueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="JATUH TEMPO" SortExpression="InvoiceDueDate" />
                                <asp:TemplateColumn SortExpression="BillAmount" HeaderText="BILL AMOUNT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBillAmount" runat="server" Text='<%#formatnumber(Container.DataItem("BillAmount"))%> &nbsp;'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                       <uc1:ucGridNav id="GridNavigator" runat="server"/>
                    </div>
                </div>
            </div>
        </asp:Panel>
     </asp:Panel>
     <asp:Panel ID="pnlView" runat="server">
        <div class="form_box">
            <uc1:ucpaymentdetail id="oPaymentDetail" runat="server"></uc1:ucpaymentdetail>
        </div>
        <div class="form_title">
            <div class="title_strip">
        </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    No Kontrak
                </label>
                <asp:Label ID="lblNoKontrak" runat="server" CssClass="" />
            </div>
            <div class="form_right">
                <label class="label_general">
                    Invoice Due Date
                </label>
                <asp:Label ID="lblDueDate" runat="server" CssClass="" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Asset Desc
                </label>
                <asp:Label ID="lblAsset" runat="server" CssClass="" />
            </div>
            <div class="form_right">
                <label class="label_general">
                    Periode Lease
                </label>
                <asp:Label ID="lblPeriodLease" runat="server" CssClass="" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Customer
                </label>
                <asp:Label ID="lblCustomer" runat="server" CssClass="" />
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Address
                </label>
                <asp:Literal ID="lblAddress" runat="server" EnableViewState="False"></asp:Literal>
            </div>
            <div class="form_right" >
                
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Basic Lease
                </label>
                <asp:Label ID="lblBasicLease" runat="server" CssClass="numberAlign2" />
            </div>
            <div class="form_right">
                
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    VAT
                </label>
                <asp:Label ID="lblVAT" runat="server" CssClass="numberAlign2" style="display:inline-block; width:200px;"  />
                <asp:Label ID="lblVATPercent" runat="server" CssClass="numberAlign2" style="display:inline-block; width:100px;" />
            </div>
            <div class="form_right">
                
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    WHT
                </label>
                <label class="label_calc2">
                    -
                </label>
                <asp:Label ID="lblWHT" runat="server" CssClass="numberAlign2" style="display:inline-block; width:200px;" />
                <asp:Label ID="lblWHTPercent" runat="server" CssClass="numberAlign2" style="display:inline-block; width:100px;" />
            </div>
             <div class="form_right" >
                <label class="label_general">
                    Total kewajiban
                </label>
                <asp:TextBox ID="lblTotalKewajiban" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Denda
                </label>
                <label class="label_calc2">
                    +
                </label>
                <asp:TextBox ID="lblDenda" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Bayar Denda
                </label>
                <uc1:UcNumberFormat name="txtBayarDenda" id="txtBayarDenda" TextCssClass="numberAlign2"  runat="server" width="200" OnClientChange="hitungtxt(this.id,'lblTotalKewajiban','lblTotalBayar',false);" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">Total Amount</label>
                <label class="label_calc2">
                    +
                </label>
                <asp:Label ID="lblTotalAmount" runat="server" CssClass="numberAlign2" />
            </div>
            <div class="form_right">
                <label class="label_general">
                    Total Pembayaran 
                </label>
                <asp:TextBox ID="lblTotalBayar" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true" style="font-weight:bold"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnTerima" runat="server" EnableViewState="False" Text="Execute" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="btnCancel" runat="server" EnableViewState="False" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
     </asp:Panel>
    </form>
</body>
</html>
