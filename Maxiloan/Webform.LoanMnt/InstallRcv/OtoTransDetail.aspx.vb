﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class OtoTransDetail
    Inherits Maxiloan.Webform.WebBased

    Private oCustom As New Parameter.InstallRcv
    Private oController As New InstallRcvController
    Property VoucherNo As String
        Get
            Return CType(ViewState("VoucherNo"), String)
        End Get
        Set(value As String)
            ViewState("VoucherNo") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim cookie As HttpCookie = Request.Cookies("otorisasi")
            Dim tmp As New DataTable

            Me.VoucherNo = cookie.Values("voucherno")
            lblVoucherNO.Text = Me.VoucherNo
            lblBankAccountName.Text = cookie.Values("BankAccountName")
            lblCashierId.Text = cookie.Values("CashierId")
            lblCustomerName.Text = cookie.Values("CustomerName")
            lblDescription.Text = cookie.Values("Description")
            lblPostingDate.Text = cookie.Values("PostingDate")
            lblValueDate.Text = cookie.Values("ValutaDate")
            lblAmount.Text = FormatNumber(cookie.Values("Amount"))

            oCustom.strConnection = GetConnectionString()
            oCustom.VoucherNO = Me.VoucherNo

            tmp = oController.DetailTransaction(oCustom)
            DtgDtl.DataSource = tmp
            DtgDtl.DataBind()
        End If


    End Sub

    Private Sub imgSave_Click(sender As Object, e As System.EventArgs) Handles imgSave.Click
        With oCustom
            .strConnection = GetConnectionString()
            .VoucherNO = Me.VoucherNo
        End With
        oController.SaveOtorisasi(oCustom)
        Response.Redirect("OtoTrans.aspx")
    End Sub

    Private Sub imbCancel_Click(sender As Object, e As System.EventArgs) Handles imbCancel.Click
        Response.Redirect("OtoTrans.aspx")
    End Sub

    Private Sub imbReject_Click(sender As Object, e As System.EventArgs) Handles imbReject.Click
        With oCustom
            .strConnection = GetConnectionString()
            .VoucherNO = Me.VoucherNo
        End With
        oController.RejectOtorisasi(oCustom)
        Response.Redirect("OtoTrans.aspx")
    End Sub
End Class