﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvList_Bank_Temp.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.InstallRcvList_Bank_Temp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocate" Src="../../Webform.UserController/UcPaymentAllocate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRcvList_Bank_Temp</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
  
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
    <div class="form_title">
        <div class="title_strip"></div>
        <div class="form_single">
            <h4> PEMBAYARAN CUSTOMER (via BANK) OTOR </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label> Cabang </label>
                <uc1:ucbranchall id="oBranch" runat="server" />
            </div>
            <div class="form_right">
                <label class="label_split"> Sumber Dokumen </label>
                <asp:DropDownList ID="cmbBankAccount" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Cari Berdasarkan Angsuran</label>
                    <uc4:ucNumberFormat ID="txtAngsuran1" runat="server" CssClass="small_text" /> s/d 
                    <uc4:ucNumberFormat ID="txtAngsuran2" runat="server" CssClass="small_text" />
                </div>
                <div class="form_right">
                    <label>Tanggal Valuta</label>
                    <asp:TextBox runat="server" ID="txtTglValuta" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtTglValuta" Format="dd/MM/yyyy" />
                    <asp:RegularExpressionValidator ID="reTglValuta" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtTglValuta"
                    SetFocusOnError="true" Display="Dynamic" CssClass="validator_general" />
                    <asp:RequiredFieldValidator ID="rftglvaluta" runat="server" ErrorMessage="Tgl Valuta harus diisi" ControlToValidate="txtTglValuta" SetFocusOnError="true" CssClass="validator_general" />
                </div>              
            </div>
        </div>
        <div class="form_box_uc">
            <div>
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>        
            </div>
        </div>

         
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue" />
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray" CausesValidation="False" /> 
            <div style="display:none">
            <a href="javascript:OpenPemb();">
                <asp:Button ID="btnIdentPemb" runat="server" Text="Identifikasi Rekening Koran" CssClass="small button gray" CausesValidation="False" UseSubmitBehavior="false" />
            </a>
            </div>
        </div>

        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid" AllowSorting="True">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col" />
                                    <ItemTemplate> 
                                        <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Receive" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>' />
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="TYPE CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NamaSuamiIstri" HeaderText="NAMA PASANGAN">
                                    <ItemTemplate> 
                                        <asp:Label ID="lblNamaSuamiIstri" runat="server" Text='<%#Container.DataItem("NamaSuamiIstri")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="LicensePlate" HeaderText="NO. POLISI" HeaderStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="MerekType" HeaderText="MERK/TYPE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMerekType" runat="server" Text='<%#Container.DataItem("MerekType")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%> &nbsp;' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="NextInstallmentNumber" DataFormatString="{0:N0}" HeaderText="KE" SortExpression="NextInstallmentNumber" />
                                <asp:BoundColumn DataField="NextInstallmentDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="JATUH TEMPO" SortExpression="NextInstallmentDate" />
                                <asp:BoundColumn DataField="Tenor" HeaderText="TNR" SortExpression="Tenor" />
                                <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="CONTRACT STATUS" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                         <uc2:ucGridNav id="GridNavigator" runat="server"/>  
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAlokasi" runat="server">
            <div class="form_box_uc">
                <uc1:UcPaymentAllocate id="oViewPaymentAllocate" runat="server" /> 
            </div>
     </asp:Panel> 
    </form>
</body>
</html>
