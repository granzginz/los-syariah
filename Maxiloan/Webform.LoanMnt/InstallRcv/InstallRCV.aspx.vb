﻿#Region "Import"
Imports System.IO
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports CrystalDecisions.Web
#End Region

Public Class InstallRCV
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentDetail As UCPaymentDetail
    Protected WithEvents oViewPaymentDetail As UCViewPaymentDetail
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oPaymentAllocationDetail As UcPaymentAllocationDetail
    Protected WithEvents oCashier As UcCashier
    Private Const SPTYPE_2 As String = "SP2" 
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule

#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property

    Private Property PrioritasPembayaran As String
    Private Property BlokirBayar As Boolean
    Private Property SPType As String
    Private Property HistorySequenceNo As Integer
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region
    Public Property oClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If SessionInvalid() Then
                Exit Sub
            End If
            Me.FormID = "INSTALLRCV"
            If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Exit Sub
            End If
             
            If Not IsPostBack Then 
                oClass = Session("oCustomClass")

                Me.Mode = "Normal"
                oCashier.CashierID = ""
                oCashier.HeadCashier = True

                InitialDefaultPanel()
                DoBind()

                'load grid angsuran
                oInstallmentSchedule.ApplicationId = oClass.ApplicationID
                oInstallmentSchedule.ValueDate = Me.BusinessDate
                oInstallmentSchedule.DoBind_Angsuran(String.Empty)

                LoadNextStep()
                BlokirBayarStep()

            End If

            If Not CheckCashier(Me.Loginid) Then 
                ShowMessage(lblMessage, "Kasir Belum Buka", True)
                pnlViewPaymentDetail.Visible = False
                pnlPaymentAllocation.Visible = False
                pnlBtnGroupPaymentReceive.Visible = False
                pnlBtnGroupPaymentAllocation.Visible = False
                pnlPaymentInfo.Visible = False

                pnlBtnGroupPaymentAllocation.Visible = True
                ButtonSaveProcess.Visible = False
                Exit Sub
            End If
           
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
       


    End Sub
    Private Sub DoBind() 
        Dim oCustomClass As Parameter.AccMntBase = oClass
        With oCustomClass
            Me.AgreementNo = .Agreementno
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType 
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .Agreementno
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID.Trim) & "')"

            Me.NextInstallmentDate = .NextInstallmentDate
            oPaymentDetail.AmountReceive = .AmountReceiveInsurance 
            lblPrepaid.Text = FormatNumber(.Prepaid, 0)
            Me.PrepaidBalance = .Prepaid
            Me.AmountToBePaid = .AmountToBePaid
            lblAmountToBePaid.Text = FormatNumber(.AmountToBePaid, 0)
            lblTitipanAngsuran.Text = FormatNumber(.TitipanAngsuran)

            oPaymentAllocationDetail.ToleransiBayarKurang = .ToleransiBayarKurang
            oPaymentAllocationDetail.ToleransiBayarLebih = .ToleransiBayarLebih
            oPaymentAllocationDetail.JumlahUangDiterima = .ToAmountAllocate

            oPaymentDetail.ReceivedFrom = .CustomerName
            oPaymentDetail.JenisFormPembayaran = "CA"
            oPaymentDetail.DoFormCashModule()

            oPaymentDetail.ValueDate = .ValueDate.ToString("dd/MM/yyyy")

            Me.BankAccount = oPaymentDetail.BankAccount 
            Me.PrioritasPembayaran = .PrioritasPembayaran
            Me.SPType = .SPType
            Me.BlokirBayar = .BlokirBayar
        End With
    End Sub

    Private Sub LoadNextStep()

        NextProcess()
        If Me.WayOfPayment <> "CP" Then
            If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                Else
                    If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                        oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                        oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                    Else
                        oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                    End If
                End If
            Else
                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
            End If

            oPaymentAllocationDetail.IsiKolomTagihan()
            oPaymentAllocationDetail.BindPrioritasPembayaran(Me.BusinessDate, True, oInstallmentSchedule.GetAllAngsuranUnPaid)
        End If

    End Sub
    Private Sub NextProcess()

        With oPaymentInfo
            .PaymentInfo(oClass) 
            Me.ValidationData = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
        End With

        Me.AmountReceive = oPaymentDetail.AmountReceive
        oPaymentAllocationDetail.PaymentAllocationBind(oClass)
    End Sub
     
    Private Sub InitialDefaultPanel() 
        pnlViewPaymentDetail.Visible = False
        pnlPaymentAllocation.Visible = True
        pnlBtnGroupPaymentReceive.Visible = False
        pnlBtnGroupPaymentAllocation.Visible = True
        pnlPaymentInfo.Visible = False
        pnlHeadCashierPassword.Visible = False
        pnlConfirmation.Visible = False
    End Sub


#Region "BtnGroup Payment Receive"
    Private Sub imbNextProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonNextProcess.Click
        If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
            lblMessage.Text = ""
            If oPaymentDetail.WayOfPayment = "CP" Then
                If oClass.BranchId <> Me.sesBranchId.Trim.Replace("'", "") Then
                    ShowMessage(lblMessage, "Cara Pembayaran Tidak bisa untuk Transaksi Antar Cabang", True)
                    Exit Sub
                End If
            End If

            If Me.IsValidLastPayment(Me.ApplicationID, ConvertDate2(oPaymentDetail.ValueDate)) Then
                If Me.Mode = "Normal" Then
                    NextProcess()
                    If Not CheckCashier(Me.Loginid) Then
                        ShowMessage(lblMessage, "Kasir belum Buka", True)
                        Exit Sub
                    End If


                    If Me.WayOfPayment = "CP" Then
                        If Me.PrepaidBalance = 0 Then 
                            ShowMessage(lblMessage, "Tidak bisa Alokasi Saldo Prepaid, Karena Saldo Kurang", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    Else
                        If oPaymentDetail.AmountReceive = 0 Then 
                            ShowMessage(lblMessage, "Harap diisi dengan Jumlah diterima", True)
                            oPaymentDetail.AmountReceive = 0
                            Exit Sub
                        End If
                    End If

                    If IsMaxBacDated(ConvertDate2(oPaymentDetail.ValueDate)) Then
                        oPaymentDetail.WayOfPayment = "0"
                        Me.Mode = "NotNormal"
                        pnlHeadCashierPassword.Visible = True
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = False
                        pnlPaymentAllocation.Visible = False
                        pnlPaymentInfo.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = True
                        pnlBtnGroupPaymentAllocation.Visible = False
                        oCashier.CashierID = ""
                        oCashier.HeadCashier = True 
                        Exit Sub
                    ElseIf DateDiff(DateInterval.Day, ConvertDate2(oPaymentDetail.ValueDate), Me.BusinessDate) < 0 Then
                        oPaymentDetail.WayOfPayment = "0" 
                        ShowMessage(lblMessage, "Tanggal Valuta tidak boleh lebih besar dari tgl hari ini", True) 
                        Exit Sub
                    End If


                    pnlHeadCashierPassword.Visible = False
                    pnlPaymentDetail.Visible = False
                    pnlViewPaymentDetail.Visible = True
                    pnlPaymentAllocation.Visible = True
                    pnlPaymentInfo.Visible = True
                    pnlBtnGroupPaymentReceive.Visible = False
                    pnlBtnGroupPaymentAllocation.Visible = True
                    If Me.WayOfPayment <> "CP" Then
                        If oPaymentInfo.PrepaidHoldStatus.Trim = "NM" Then
                            If oPaymentInfo.ContractStatus.Trim = "PRP" Or oPaymentInfo.ContractStatus.Trim = "INV" Or oPaymentInfo.ContractStatus.Trim = "LNS" Then
                                oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                            Else
                                If oPaymentDetail.AmountReceive > oPaymentInfo.MaximumInstallment Then
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentInfo.MaximumInstallment
                                    oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive - oPaymentInfo.MaximumInstallment
                                Else
                                    oPaymentAllocationDetail.InstallmentDue = oPaymentDetail.AmountReceive
                                End If
                            End If
                        Else
                            oPaymentAllocationDetail.Prepaid = oPaymentDetail.AmountReceive
                        End If
                        oPaymentAllocationDetail.IsiKolomTagihan()
                        oPaymentAllocationDetail.BindPrioritasPembayaran(Me.BusinessDate, True, oInstallmentSchedule.GetAllAngsuranUnPaid)
                    End If

                ElseIf Me.Mode = "NotNormal" Then
                    Dim objDecent As New Decrypt.am_futility
                    Dim oLogin As New LoginController
                    Dim oLoginEntities As New Parameter.Login

                    oLoginEntities.strConnection = GetConnectionString()
                    oLoginEntities.LoginId = oCashier.CashierID.Trim
                    Dim lstrPassword As String

                    lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
                    If lstrPassword.Trim = txtCashierPassword.Text.Trim Then
                        pnlPaymentDetail.Visible = False
                        pnlViewPaymentDetail.Visible = True
                        pnlPaymentAllocation.Visible = True
                        pnlPaymentInfo.Visible = True
                        pnlHeadCashierPassword.Visible = False
                        pnlBtnGroupPaymentReceive.Visible = False
                        pnlBtnGroupPaymentAllocation.Visible = True
                        Me.Mode = "Normal"
                    Else 
                        ShowMessage(lblMessage, "Password Salah", True)
                        Exit Sub
                    End If
                End If
            Else 
                ShowMessage(lblMessage, "Tanggal Valuta Lebih kecil dari tanggal pembayaran terakhir", True)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub imbCancelProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancelProcess.Click
        Response.Redirect("InstallRcvList.aspx")
    End Sub
#End Region

#Region "Button Group Payment Allocation"
    Private Sub imbSaveProcess_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSaveProcess.Click

        'BindReport()
        'Exit Sub
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If Not CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then

                Exit Sub
            End If

            If Not CheckCashier(Me.Loginid) Then
                ShowMessage(lblMessage, "Kasir belum Buka", True)

                Exit Sub
            End If

            With oPaymentInfo
                LastAR = .Prepaid + _
                            .MaximumInstallment + _
                            .MaximumLCInstallFee + _
                            .MaximumInstallCollFee + _
                            .MaximumInsurance + _
                            .MaximumLCInsuranceFee + _
                            .MaximumInsuranceCollFee + _
                            .MaximumPDCBounceFee + _
                            .MaximumSTNKRenewalFee + _
                            .MaximumInsuranceClaimFee + _
                            .MaximumReposessionFee
            End With

            If Me.ValidationData <> LastAR Then
                ShowMessage(lblMessage, "User lain Sudah Update Kontrak ini, Harap cek lagi .....", True)

                Exit Sub
            End If

            'If oPaymentAllocationDetail.InstallmentDue > 0 And oPaymentAllocationDetail.InstallmentDue < Me.AmountReceive Then
            '    ShowMessage(lblMessage, "Angsuran hanya boleh diisi dengan 0 atau " & FormatNumber(Me.AmountReceive, 0), True)
            '    Exit Sub
            'End If

            With oPaymentAllocationDetail
                TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
                                .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                                .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                                .RepossessionFee + .Prepaid + .PLL
            End With

            If TotalAllocation <> oClass.ToAmountAllocate Then
                ShowMessage(lblMessage, String.Format("Total Alokasi Alokasi harus = {0:N0} ", oClass.ToAmountAllocate), True)

                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = oClass.BranchId
                .ApplicationID = oClass.ApplicationID
                .AmountReceive = oPaymentAllocationDetail.InstallmentDue
            End With

            If oController.IsLastPayment(oCustomClass) Then
                pnlConfirmation.Visible = True
                pnlBtnGroupPaymentAllocation.Visible = False
                pnlHeadCashierPassword.Visible = False
                pnlPaymentDetail.Visible = False
                pnlViewPaymentDetail.Visible = False
                pnlPaymentAllocation.Visible = False
                pnlPaymentInfo.Visible = False
                pnlBtnGroupPaymentReceive.Visible = False
                pnlBtnGroupPaymentAllocation.Visible = False
                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .CoyID = Me.SesCompanyID
                .BusinessDate = Me.BusinessDate
                .ReceivedFrom = oPaymentDetail.ReceivedFrom
                .ReferenceNo = oPaymentDetail.ReferenceNo
                .WOP = oPaymentDetail.WayOfPayment ' Me.WayOfPayment.Trim
                .LoginId = Me.Loginid
                .BankAccountID = Me.BankAccount
                .Notes = oPaymentDetail.Notes
                .ApplicationID = oClass.ApplicationID

                '-----------Installment
                .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                .LcInstallment = oPaymentAllocationDetail.LCInstallment
                .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                '------------------------

                '----------Insurance
                .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                .LcInsurance = oPaymentAllocationDetail.LCInsurance
                .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                '-----------------------------

                .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                .Prepaid = oPaymentAllocationDetail.Prepaid
                .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim

                .PLL = oPaymentAllocationDetail.PLL
                .PLLDesc = oPaymentAllocationDetail.PLLDesc

                .AmountReceive = TotalAllocation
                .BayarDi = "Cabang"  'oPaymentDetail.BayarDi
                .CollectorID = oPaymentDetail.CollectorID
                .Penyetor = oPaymentAllocationDetail.Penyetor
            End With

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Try
                Dim SavingController As New InstallRcvController(oCustomClass)
                Me.HistorySequenceNo = SavingController.InstallmentReceive()

                If HistorySequenceNo <= 0 Then
                    ShowMessage(lblMessage, oCustomClass.strError, True)
                    Exit Sub
                End If
                BindReport()
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)

            End Try
        End If
    End Sub

    Private Sub imbCancel2Process_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel2Process.Click
        Response.Redirect("InstallRcvList.aspx")
    End Sub
#End Region


    Sub BindReport()
        Dim oData As New DataSet 
        Dim objReport As RptKwitansi2 = New RptKwitansi2
        'oClass.ApplicationID = "030A201505000018"
        'HistorySequenceNo = 1
        'oClass.BranchId = "030"
        'execute routine to insert to table KwitansiInstallmentPrint
        oController.PrintTTU(GetConnectionString, oClass.ApplicationID, oClass.BranchId, Me.BusinessDate, Me.Loginid)
         
        oCustomClass.ApplicationID = oClass.ApplicationID
        oCustomClass.HistorySeqNo = Me.HistorySequenceNo
        oCustomClass.BranchId = oClass.BranchId
        oCustomClass.strConnection = GetConnectionString() 
        oData = oController.ReportKwitansiInstallment2(oCustomClass)

        If (oData.Tables(0).Rows.Count <= 0) Then
            Exit Sub
        End If

        objReport.SetDataSource(oData)
         
        'add cashier
        AddParamField(objReport, "CashierName", Me.FullName)
        AddParamField(objReport, "BusinessDate", Me.BusinessDate.ToString("dd") + " " + getMonthID() + " " + Me.BusinessDate.ToString("yyyy")) 
        AddParamField(objReport, "BranchName", oData.Tables(0).Rows(0).Item("BranchCity").ToString)

        'crvKwitansi.ReportSource = objReport 
        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer 
        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Kwitansi" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String 
        strFileLocation = pathReport("installrcv", True) & oClass.ApplicationID.Replace("/", "").Trim & ".pdf"

        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("installrcvlist.aspx?filekwitansi=" & pathReport("installrcv", False) & oClass.ApplicationID.Replace("/", "").Trim)

    End Sub
    Private Function pathReport(ByVal group As String, ByVal dirXML As Boolean) As String
        Dim strDirectory As String = ""
        If dirXML = True Then
            strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += Me.sesBranchId.Replace("'", "") & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If

            strDirectory += group & "\"
            If Not System.IO.Directory.Exists(strDirectory) Then
                System.IO.Directory.CreateDirectory(strDirectory)
            End If
        Else
            strDirectory += Me.BusinessDate.ToString("ddMMyyy") & "/"
            strDirectory += Me.sesBranchId.Replace("'", "") & "/"
            strDirectory += group & "/"
        End If
        Return strDirectory
    End Function
    Private Function getMonthID() As String
        Dim rtn As String = ""

        If Month(Me.BusinessDate) = "1" Then
            rtn = "Januari"
        End If
        If Month(Me.BusinessDate) = "2" Then
            rtn = "Febuari"
        End If
        If Month(Me.BusinessDate) = "3" Then
            rtn = "Maret"
        End If
        If Month(Me.BusinessDate) = "4" Then
            rtn = "April"
        End If
        If Month(Me.BusinessDate) = "5" Then
            rtn = "Mai"
        End If
        If Month(Me.BusinessDate) = "6" Then
            rtn = "Juni"
        End If
        If Month(Me.BusinessDate) = "7" Then
            rtn = "Juli"
        End If
        If Month(Me.BusinessDate) = "8" Then
            rtn = "Augustus"
        End If
        If Month(Me.BusinessDate) = "9" Then
            rtn = "September"
        End If
        If Month(Me.BusinessDate) = "10" Then
            rtn = "Oktober"
        End If
        If Month(Me.BusinessDate) = "11" Then
            rtn = "November"
        End If
        If Month(Me.BusinessDate) = "12" Then
            rtn = "Desember"
        End If

        Return rtn

    End Function
    Private Sub AddParamField(ByVal objReport As RptKwitansi2, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub

    Private Sub imbSaveLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSaveLastPayment.Click
        If Me.Page.IsValid Then
            Dim LastAR As Double
            Dim TotalAllocation As Double

            If CheckFeature(Me.Loginid, Me.FormID, "RCV", Me.AppId) Then
                If Not CheckCashier(Me.Loginid) Then

                    ShowMessage(lblMessage, "Kasir belum Buka", True)
                    Exit Sub
                End If
                With oPaymentInfo
                    '.ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                    '.ApplicationID = Me.ApplicationID
                    '.PaymentInfo() 
                    LastAR = .Prepaid + _
                                .MaximumInstallment + _
                                .MaximumLCInstallFee + _
                                .MaximumInstallCollFee + _
                                .MaximumInsurance + _
                                .MaximumLCInsuranceFee + _
                                .MaximumInsuranceCollFee + _
                                .MaximumPDCBounceFee + _
                                .MaximumSTNKRenewalFee + _
                                .MaximumInsuranceClaimFee + _
                                .MaximumReposessionFee
                End With
                If Me.ValidationData <> LastAR Then

                    ShowMessage(lblMessage, "User lain sudah Update Kontrak ini, harap cek lagi .....", True)
                Else
                    With oPaymentAllocationDetail
                        TotalAllocation = .InstallmentDue + .TitipanAngsuran + .LCInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .LCInsurance + .InsuranceCollFee + _
                                        .InsuranceClaimExpense + .STNKRenewalFee + .PDCBounceFee + _
                                        .RepossessionFee + .Prepaid + .PLL

                    End With
                    If Me.WayOfPayment = "CP" Then
                        If TotalAllocation > Me.PrepaidBalance Then

                            ShowMessage(lblMessage, "Total Alokasi Alokasi harus <= Saldo Prepaid", True)
                            Exit Sub
                        End If
                        If oPaymentAllocationDetail.Prepaid > 0 Then

                            ShowMessage(lblMessage, "Alokasi Pembayaran tidak bisa untuk Prepaid", True)
                            Exit Sub
                        End If
                    Else
                        If TotalAllocation - oPaymentAllocationDetail.PLL > LastAR Then
                            ShowMessage(lblMessage, "Alokasi pembayaran harus lebih kecil atau sama dengan sisa angsuran", True)
                            Exit Sub
                        End If
                    End If
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .CoyID = Me.SesCompanyID
                        .BusinessDate = Me.BusinessDate
                        .ReceivedFrom = oViewPaymentDetail.ReceivedFrom
                        .ReferenceNo = oViewPaymentDetail.ReferenceNo
                        .WOP = oPaymentDetail.WayOfPayment 'Me.WayOfPayment.Trim
                        .LoginId = Me.Loginid
                        .BankAccountID = Me.BankAccount
                        .Notes = oViewPaymentDetail.Notes
                        .ApplicationID = oClass.ApplicationID

                        '-----------Installment
                        .InstallmentDue = oPaymentAllocationDetail.InstallmentDue
                        .InstallmentDueDesc = oPaymentAllocationDetail.InstallmentDueDesc.Trim

                        .TitipanAngsuran = oPaymentAllocationDetail.TitipanAngsuran
                        .TitipanAngsuranDesc = oPaymentAllocationDetail.TitipanAngsuranDesc.Trim

                        .LcInstallment = oPaymentAllocationDetail.LCInstallment
                        .LcInstallmentDesc = oPaymentAllocationDetail.LCInstallmentDesc.Trim

                        .InstallmentCollFee = oPaymentAllocationDetail.InstallmentCollFee
                        .InstallmentCollFeeDesc = oPaymentAllocationDetail.InstallmentCollFeeDesc.Trim
                        '------------------------

                        '----------Insurance
                        .InsuranceDue = oPaymentAllocationDetail.InsuranceDue
                        .InsuranceDueDesc = oPaymentAllocationDetail.InsuranceDueDesc.Trim

                        .LcInsurance = oPaymentAllocationDetail.LCInsurance
                        .LcInsuranceDesc = oPaymentAllocationDetail.LCInsuranceDesc.Trim

                        .InsuranceCollFee = oPaymentAllocationDetail.InsuranceCollFee
                        .InsuranceCollFeeDesc = oPaymentAllocationDetail.InsuranceCollFeeDesc.Trim
                        '-----------------------------

                        .PDCBounceFee = oPaymentAllocationDetail.PDCBounceFee
                        .PDCBounceFeeDesc = oPaymentAllocationDetail.PDCBounceFeeDesc.Trim

                        .STNKRenewalFee = oPaymentAllocationDetail.STNKRenewalFee
                        .STNKRenewalFeeDesc = oPaymentAllocationDetail.STNKRenewalDesc.Trim

                        .InsuranceClaimExpense = oPaymentAllocationDetail.InsuranceClaimExpense
                        .InsuranceClaimExpenseDesc = oPaymentAllocationDetail.InsuranceClaimExpenseDesc.Trim

                        .RepossessionFee = oPaymentAllocationDetail.RepossessionFee
                        .RepossessionFeeDesc = oPaymentAllocationDetail.RepossessionFeeDesc.Trim

                        .Prepaid = oPaymentAllocationDetail.Prepaid
                        .PrepaidDesc = oPaymentAllocationDetail.PrepaidDesc.Trim

                        .PLL = oPaymentAllocationDetail.PLL
                        .PLLDesc = oPaymentAllocationDetail.PLLDesc

                        .AmountReceive = TotalAllocation
                        .BayarDi = "Cabang"
                        .CollectorID = oPaymentDetail.CollectorID
                        .Penyetor = oPaymentAllocationDetail.Penyetor
                    End With

                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

                    Try
                        Dim SavingController As New InstallRcvController(oCustomClass)
                        Me.HistorySequenceNo = SavingController.InstallmentReceive()
                        If HistorySequenceNo <= 0 Then
                            ShowMessage(lblMessage, oCustomClass.strError, True)
                            Exit Sub
                        End If
                        BindReport()
                    Catch exp As Exception
                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub imbCancelLastPayment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancelLastPayment.Click
        Response.Redirect("InstallRcvList.aspx")
    End Sub
     
    Private Sub BlokirBayarStep()
        If BlokirBayar Then
            ButtonSaveProcess.Visible = False
            ShowMessage(lblMessage, "Pembayaran ini diblokir", True)
        Else
            ButtonSaveProcess.Visible = True
        End If
    End Sub

End Class