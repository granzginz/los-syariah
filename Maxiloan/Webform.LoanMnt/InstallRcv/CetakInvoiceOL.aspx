﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CetakInvoiceOL.aspx.vb" 
Inherits="Maxiloan.Webform.LoanMnt.CetakInvoiceOL" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucGridNav" Src="../../webform.UserController/ucGridNav.ascx" %>  
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cetak Invoice Operating Lease</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }


        function hitungtxt(txtidA, txtidB, txtidS, toPersen) {
            var status = true;
            var A = $('#' + txtidA).val();
            var S;
            var N;
            var M;
            if ($('#' + txtidS).html() == "") {
                S = $('#' + txtidS).val();
            } else {
                S = $('#' + txtidS).html();
            }

            A = A.replace(/\s*,\s*/g, '');
            S = S.replace(/\s*,\s*/g, '');

            if (status == true) {
                if (toPersen == true) {
                    N = parseInt(A) / parseInt(S) * 100;
                    $('#' + txtidB).val(number_format(parseFloat((N * 10) / 10), 2));
                } else {
                    N = parseFloat(S) * (A / 100);
                    //$('#' + txtidA).val(number_format(A, 2));
                    $('#' + txtidB).val(number_format(N, 0));
                }
            } else {
                $('#' + txtidA).val(number_format(0, 2));
                $('#' + txtidB).val(number_format(0, 2));
            }

            var basiclease = parseFloat($('#lblBasicLease').val().replace(/\s*,\s*/g, ''));
            var vat = parseFloat($('#txtVATAmount').val().replace(/\s*,\s*/g, ''));
            var wht = parseFloat($('#txtWHTAmount').val().replace(/\s*,\s*/g, ''));
            var totalBill = basiclease + vat - wht;

            $('#lblTotalAmount').val(number_format(totalBill, 2));


        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                CETAK INVOICE OPERATING LEASE
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Cari Berdasarkan Jatuh Tempo</label>
                <uc1:ucdatece id="oValueDate1" runat="server" />s/d 
                <uc1:ucdatece id="oValueDate2" runat="server" />
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" EnableViewState="False" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" EnableViewState="False" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK OPERATING LEASE
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="Applicationid"
                            AllowSorting="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate> 
                                        <asp:ImageButton ID="ImbCetak" ImageUrl="../../Images/IconPrinter.gif" runat="server" CommandName="Print"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'> </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'> </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="NextInstallmentNumber" DataFormatString="{0:N0}" HeaderText="KE" SortExpression="NextInstallmentNumber" />
                                <asp:BoundColumn DataField="DueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="JATUH TEMPO" SortExpression="NextInstallmentDate" />
                                <asp:BoundColumn DataField="Tenor" HeaderText="TNR" SortExpression="Tenor" />
                                <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%> &nbsp;'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="MerekType" HeaderText="MERK/TYPE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMerekType" runat="server" Text='<%#Container.DataItem("MerekType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="LicensePlate" HeaderText="NO. POLISI" HeaderStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                       <uc1:ucGridNav id="GridNavigator" runat="server"/>
                    </div>
                </div>
            </div>
        </asp:Panel>
     </asp:Panel>
     <asp:Panel ID="pnlView" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    No Kontrak
                </label>
                <asp:Label ID="lblNoKontrak" runat="server" CssClass="" />
            </div>
            <div class="form_right">
                <label class="label_general">
                    Invoice Due Date
                </label>
                <asp:Label ID="lblDueDate" runat="server" CssClass="" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Asset Desc
                </label>
                <asp:Label ID="lblAsset" runat="server" CssClass="" />
            </div>
            <div class="form_right">
                <label class="label_general">
                    Periode Lease
                </label>
                <asp:Label ID="lblPeriodLease" runat="server" CssClass="" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Customer
                </label>
                <asp:Label ID="lblCustomer" runat="server" CssClass="" />
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Address
                </label>
                <asp:Literal ID="lblAddress" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Basic Lease
                </label>
                <asp:TextBox ID="lblBasicLease" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Denda
                </label>
                <asp:TextBox ID="lblDenda" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    VAT
                </label>
                <asp:TextBox ID="txtVATAmount" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
                <uc1:UcNumberFormat name="txtVATPercent" id="txtVATPercent" runat="server" OnClientChange="hitungtxt(this.id,'txtVATAmount','lblBasicLease',false);" width="50" />
                <label class="label_auto "> % </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    WHT
                </label>
                <label class="label_calc2">
                    -
                </label>
                <asp:TextBox ID="txtWHTAmount" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
                <uc1:UcNumberFormat name="txtWHTPercent" id="txtWHTPercent" runat="server" OnClientChange="hitungtxt(this.id,'txtWHTAmount','lblBasicLease',false);" width="50"  />
                <label class="label_auto "> % </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">Total Amount</label>
                <label class="label_calc2">
                    +
                </label>
                <asp:TextBox ID="lblTotalAmount" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint" runat="server" EnableViewState="False" Text="Print" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="btnCancel" runat="server" EnableViewState="False" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
     </asp:Panel>
    </form>
</body>
</html>
