﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRCV_Bank_Temp.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InstallRCV_Bank_Temp" %>

<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocationDetail" Src="../../Webform.UserController/UcPaymentAllocationDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRCV_Bank_Temp</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <div class="form_title">
            <div class="title_strip" ></div>
            <div class="form_single">
                <h4> PEMBAYARAN CUSTOMER (via BANK) OTOR </h4>
            </div>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label> No Kontrak </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server" />
            </div>
            <div class="form_right">
                <label> Nama Customer </label>
                <asp:HyperLink ID="lblCustomerName" runat="server" />
            </div>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label> Saldo Angsuran </label>
                <asp:Label ID="lblAmountToBePaid" runat="server" />
            </div>
            <div class="form_right">
                <label>Saldo Prepaid</label>
                <asp:Label ID="lblPrepaid" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Titipan Angsuran</label>
                <asp:Label ID="lblTitipanAngsuran" runat="server" />
            </div>
        </div>
      
        <div class="form_box_header">
            <div class="form_single">
                <h5>TABEL ANGSURAN</h5>
            </div>
        </div>
        <uc1:ucinstallmentschedule id="oInstallmentSchedule" runat="server" />
        <asp:Panel ID="pnlPaymentDetail" runat="server">
            <div class="form_box">
                <uc1:ucpaymentdetail id="oPaymentDetail" runat="server" isdatemandatory="true" />
                <div class="form_right">
                    <label>Tanggal Bayar</label>
                    <uc1:ucdatece id="oValueDate" runat="server" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlHeadCashierPassword" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>KONFIRMASI KEPALA KASIR</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Kepala Kasir</label>
                    <uc1:uccashier id="oCashier" runat="server" />
                </div>
                <div class="form_right">
                    <label class="label_req">Password Kasir</label>
                    <asp:TextBox ID="txtCashierPassword" runat="server" MaxLength="20" TextMode="Password" />
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtCashierPassword" Display="Dynamic" ErrorMessage="Harap isi Password" CssClass="validator_general" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlViewPaymentDetail" runat="server">
            <div class="form_box_uc">
                <uc1:ucviewpaymentdetail id="oViewPaymentDetail" runat="server" /> 
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlPaymentAllocation" runat="server">
            <div class="form_box_uc">
                <uc1:ucpaymentallocationdetail id="oPaymentAllocationDetail" runat="server" showpanelcashier="false" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtnGroupPaymentReceive" runat="server">
            <div class="form_button">
                <asp:Button ID="imbNextProcess" runat="server" Text="Next" CssClass="small button green" />
                &nbsp;
                <asp:Button ID="imbCancelProcess" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirmation" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4> KONFIRMASI KEPALA KASIR </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single"> Ini Adalah Pembayaran Terakhir </div>
            </div>
            <div class="form_button">
                <asp:Button ID="imbSaveLastPayment" runat="server" Text="Save" CssClass="small button blue" />
                &nbsp;
                <asp:Button ID="imbCancelLastPayment" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlBtnGroupPaymentAllocation" runat="server">
            <div class="form_button">
                <asp:Button ID="imbSaveProcess" runat="server" Text="Save" CssClass="small button blue" />
                &nbsp;
                <asp:Button ID="imbCancel2Process" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <div class="form_box_uc">
            <uc1:ucpaymentinfo id="oPaymentInfo" runat="server" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
