﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DRequest.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.DRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../Webform.UserController/UCReason.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DRequest</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN GANTI TANGGAL JATUH TEMPO
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Efektif
            </label>
            <asp:TextBox runat="server" ID="txtEfdate" CssClass="small_text"  ></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtEfdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal JT Berikutnya</label>
            <asp:TextBox runat="server" ID="txtsdate" CssClass="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtsdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnCalculate" runat="server" CausesValidation="false" Text="Calculate"
            CssClass="small button blue"></asp:Button>
    </div>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <uc1:ucpaymentinfo id="oPaymentInfo" runat="server"></uc1:ucpaymentinfo>
    </asp:Panel>
    
    <asp:Panel ID="pnlAmor" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TABEL AMORTISASI BARU
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" Visible="true" ShowFooter="true" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSeqNo"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DUEDATE" HeaderText="JATUH TEMPO">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ANGSURAN">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%# formatnumber(container.dataitem("InstallmentAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotInstallmentAmount" runat="server" align="center"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# formatnumber(container.dataitem("PrincipalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MARGIN">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%# formatnumber(container.dataitem("INTERESTAMOUNT"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblOSPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingPrincipal"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <HeaderStyle Width="15%" CssClass="th_right"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <FooterStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblOSInterest" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingInterest"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSInterestAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL GANTI TANGGAL JATUH TEMPO
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Margin yg harus dibayar
            </label>
            <%--<asp:TextBox ID="lblInterestpaid" runat="server" ></asp:TextBox>--%>
            <uc1:ucnumberformat id="txtInterestpaid" runat="server" />
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Administrasi
            </label>
            <uc1:ucnumberformat id="txtAdminFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Total Yang harus dibayar</label>
            <asp:Label ID="totAmountpaid" runat="server" CssClass="numberAlign regular_text"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Jumlah Prepaid</label>
            <asp:Label ID="lblPrepaidAmount" runat="server" CssClass="numberAlign regular_text"></asp:Label>
        </div>
    </div>
    <uc1:ucapprovalrequest id="oReq" runat="server">
                </uc1:ucapprovalrequest>
    <div class="form_button">
        <asp:Button ID="BtnCalculate2" runat="server" Text="Calculate" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnRequest" runat="server" Text="Request" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
