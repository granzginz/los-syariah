﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DInqView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property


    Private Property TotAmountTobePaid() As Double
        Get
            Return (CType(Viewstate("TotAmountTobePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("TotAmountTobePaid") = Value
        End Set
    End Property

    Private Property SeqNo() As Integer
        Get
            Return (CType(Viewstate("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("SeqNo") = Value
        End Set
    End Property

    Private Property InSeqNo() As String
        Get
            Return (CType(Viewstate("InSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("InSeqNo") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(Viewstate("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(Viewstate("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("DaysDiff") = Value
        End Set
    End Property

    Private Property TotInterest() As Double
        Get
            Return (CType(Viewstate("TotInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("TotInterest") = Value
        End Set
    End Property


    Private Property OSPrincipal() As Double
        Get
            Return (CType(Viewstate("OSPrincipal"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("OSPrincipal") = Value
        End Set
    End Property

    Private Property EffDate() As Date
        Get
            Return (CType(Viewstate("EffDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("EffDate") = Value
        End Set
    End Property

    Private Property flagdel() As String
        Get
            Return (CType(Viewstate("flagdel"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("flagdel") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New DChangeController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
#End Region

#Region "Declare Variable"
    Dim tempPrincipalAmount As Double
    Dim tempInterestAmount As Double
    Dim tempOSPrincipalAmount As Double
    Dim tempOSInterestAmount As Double
    Dim tempInstallmentAmount As Double
    Dim tempdate As Date
    Dim i As Integer
    Dim j As Integer = 0
    Dim TotInstallmentAmount As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipalAmount As Label
    Dim TotOSInterestAmount As Label
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "DINQVIEW"
        If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("Branchid")
                Me.SeqNo = CInt(Request.QueryString("SeqNo"))
                DoBind()
            End If
        End If
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.SeqNo = SeqNo
        oCustomClass = oController.GetListExec(oCustomClass)
        If Not oCustomClass.CustomerID Is Nothing Then

            With oCustomClass
                lblAgreementNo.Text = .Agreementno
                Me.strCustomerid = .CustomerID
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                lblCustName.Text = .CustomerName
                lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
                lblEffDate.Text = .EffectiveDate.ToString("dd/MM/yyyy")
                Me.EffDate = .EffectiveDate
                lblRequestDate.Text = .RequestDate.ToString("dd/MM/yyyy")
                lblInterestpaid.Text = FormatNumber(.InterestAmount, 2)
                lblReason.Text = .ReasonDescription
                lblAdminFee.Text = FormatNumber(.AdminFee, 2)
                lblNotes.Text = .ChangeNotes
                lblApproved.Text = .ApprovedBy
                totAmountpaid.Text = FormatNumber(.TotAmountToBePaid, 2)
                lblPaidAmount.Text = FormatNumber(.Prepaid, 2)
                lblBal.Text = FormatNumber((.TotAmountToBePaid - .Prepaid), 2)
                lbljudul.Text = .EffectiveDate.ToString("dd/MM/yyyy")
                Me.PaymentFrequency = CInt(oCustomClass.PaymentFrequency)
                lblJudulHeader.Text = .statusdesc.ToUpper.Trim
                lblNextDueDate.Value = .NextInstallmentDueDate.ToString("dd/MM/yyyy")

                '================Payment Info===================================
                lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)
                lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
                lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                lblTotalOSOverDue.Text = FormatNumber(.InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                        .RepossessionFee, 2)
                '===============================================================


            End With

            Dim DtUserList As New DataTable
            Dim DvUserList As New DataView
            With oCustomClass
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString
                .BusinessDate = oCustomClass.EffectiveDate
            End With
            oCustomClass = oController.GetListAmor(oCustomClass)
            DtUserList = oCustomClass.listdata
            dtg.DataSource = DtUserList.DefaultView
            dtg.DataBind()

        End If
    End Sub
#End Region
    Dim dateBefore As Date
#Region "Databound"
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lb As New Label
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            lb = CType(e.Item.FindControl("lblInstallmentAmount"), Label)
            tempInstallmentAmount = tempInstallmentAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblINTERESTAMOUNT"), Label)
            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)
            Me.TotInterest = tempInterestAmount

            lb = CType(e.Item.FindControl("lblOSPrincipal"), Label)
            tempOSPrincipalAmount = tempOSPrincipalAmount + CDbl(lb.Text)
            Me.OSPrincipal = tempOSPrincipalAmount

            lb = CType(e.Item.FindControl("lblOSInterest"), Label)
            tempOSInterestAmount = tempOSInterestAmount + CDbl(lb.Text)

            'If DateDiff(DateInterval.Day, oCustomClass.EffectiveDate, ConvertDate2(e.Item.Cells(2).Text)) >= 0 Then
            '    j = j + 1
            '    If j = 1 Then
            '        tempdate = oCustomClass.EffectiveDate
            '        Me.DaysDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.EffectiveDate, ConvertDate2(e.Item.Cells(2).Text)))
            '        Me.InSeqNo = e.Item.Cells(1).Text
            '    Else
            '        tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), tempdate)
            '    End If
            '    e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
            '    e.Item.Cells(2).Font.Bold = True
            'Else
            '    e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            'End If

            If DateDiff(DateInterval.Day, oCustomClass.EffectiveDate, ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
                j = j + 1
                If j = 1 Then
                    tempdate = oCustomClass.NextInstallmentDueDate
                    Me.DaysDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.NextInstallmentDueDate, ConvertDate2(e.Item.Cells(2).Text)))
                    If Me.DaysDiff < 0 Then
                        Me.DaysDiff = 0
                    End If
                Else
                    'ngakalin bulan februari ditanggal 28 dan 29
                    tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), tempdate)
                    If Month(tempdate) = 3 And Day(dateBefore) > 28 Then
                        tempdate = DateAdd(DateInterval.Day, CDbl(Day(dateBefore) - Day(tempdate)), tempdate)
                    End If
                    If Month(tempdate) = 1 Then ' get original date
                        dateBefore = tempdate
                    End If
                End If
                e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
                e.Item.Cells(2).Font.Bold = True
            Else
                e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            End If


        End If
        If e.Item.ItemType = ListItemType.Footer Then
            TotInstallmentAmount = CType(e.Item.FindControl("lblTotInstallmentAmount"), Label)
            TotInstallmentAmount.Text = FormatNumber(tempInstallmentAmount, 2).ToString

            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2).ToString

            TotInterestAmount = CType(e.Item.FindControl("lblTotINTERESTAMOUNT"), Label)
            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2).ToString

            TotOSPrincipalAmount = CType(e.Item.FindControl("lblTotOSPrincipalAmount"), Label)
            TotOSPrincipalAmount.Text = FormatNumber(tempOSPrincipalAmount, 2).ToString

            TotOSInterestAmount = CType(e.Item.FindControl("lblTotOSInterestAmount"), Label)
            TotOSInterestAmount.Text = FormatNumber(tempOSInterestAmount, 2).ToString

        End If
    End Sub
#End Region

End Class