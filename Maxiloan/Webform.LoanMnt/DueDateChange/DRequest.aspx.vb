﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DRequest
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Dim tempInstallmentAmount As Double    
    Protected WithEvents oReq As ucApprovalRequest
    Protected WithEvents txtAdminFee As ucNumberFormat
    Protected WithEvents txtInterestPaid As ucNumberFormat

#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property

    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property

    Private Property OSPrincipal() As Double
        Get
            Return (CType(ViewState("OSPrincipal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("OSPrincipal") = Value
        End Set
    End Property

    Private Property EffRate() As Decimal
        Get
            Return (CType(ViewState("EffRate"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffRate") = Value
        End Set
    End Property

    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property

    Private Property TotInterest() As Double
        Get
            Return (CType(ViewState("TotInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInterest") = Value
        End Set
    End Property

    Private Property TotAmountTobePaid() As Double
        Get
            Return (CType(ViewState("TotAmountTobePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotAmountTobePaid") = Value
        End Set
    End Property

    Private Property TotInfo() As Double
        Get
            Return (CType(ViewState("TotInfo"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInfo") = Value
        End Set
    End Property

    Private Property Bev() As String
        Get
            Return (CType(ViewState("Bev"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Bev") = Value
        End Set
    End Property

    Private Property VTempDate() As Date
        Get
            Return (CType(ViewState("VTempDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("VTempDate") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New DChangeController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
#End Region

#Region "Declare Variable"
    Dim tempPrincipalAmount As Double
    Dim tempInterestAmount As Double
    Dim tempOSPrincipalAmount As Double
    Dim tempOSInterestAmount As Double
    Dim tempdate As Date
    Dim dateBefore As Date
    Dim i As Integer
    Dim j As Integer = 0
    Dim TotInstallmentAmount As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipalAmount As Label
    Dim TotOSInterestAmount As Label
    Dim intcounter As Integer = 1
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "DUEDATEREQ"
        If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                With txtAdminFee
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = True
                    .TextCssClass = "numberAlign regular_text"
                End With

                With txtInterestPaid
                    .RequiredFieldValidatorEnable = True                    
                    .TextCssClass = "numberAlign regular_text"
                End With

                Me.ApplicationID = Request.QueryString("ApplicationId")
                oPaymentInfo.IsTitle = True
                txtEfdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                oCustomClass.ApplicationID = Me.ApplicationID
                oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass = oController.GetList(oCustomClass)
                With oCustomClass
                    txtsdate.Text = .NextInstallmentDueDate.ToString("dd/MM/yyyy")

                End With
                oReq.ApprovalScheme = "ACDD"
                oReq.ReasonTypeID = "AGCDD"
                pnlAmor.Visible = False
                BtnRequest.Visible = False
                BtnCalculate2.Visible = False
                DoBind()
            End If
        End If
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        ' lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With
        With oPaymentInfo
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        If CDbl(oPaymentInfo.TotOSOverDue) > 0 Then
            ShowMessage(lblMessage, "Total Jumlah Tunggakan harus dibayar", True)
            BtnRequest.Visible = False
            Exit Sub
        End If

    End Sub
#End Region

#Region "Databound"
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lb As New Label

        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            lb = CType(e.Item.FindControl("lblInstallmentAmount"), Label)
            tempInstallmentAmount = tempInstallmentAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblINTERESTAMOUNT"), Label)
            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblOSPrincipal"), Label)
            tempOSPrincipalAmount = tempOSPrincipalAmount + CDbl(lb.Text)
            Me.OSPrincipal = tempOSPrincipalAmount

            lb = CType(e.Item.FindControl("lblOSInterest"), Label)
            tempOSInterestAmount = tempOSInterestAmount + CDbl(lb.Text)

            If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
                j = j + 1
                If j = 1 Then
                    tempdate = ConvertDate2(txtsdate.Text)
                    Me.DaysDiff = CInt(DateDiff(DateInterval.Day, ConvertDate2(txtsdate.Text), ConvertDate2(e.Item.Cells(2).Text)))
                    If Me.DaysDiff < 0 Then
                        Me.DaysDiff = 0
                    End If
                Else
                    'ngakalin bulan februari ditanggal 28 dan 29
                    tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), tempdate)
                    If Month(tempdate) = 3 And Day(dateBefore) > 28 Then
                        tempdate = DateAdd(DateInterval.Day, CDbl(Day(dateBefore) - Day(tempdate)), tempdate)
                    End If
                    If Month(tempdate) = 1 Then ' get original date
                        dateBefore = tempdate
                    End If
                End If
                e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
                e.Item.Cells(2).Font.Bold = True
            Else
                e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            End If

            '===============================================================================================
            '' If DateDiff(DateInterval.Day, ConvertDate2(e.Item.Cells(2).Text), ConvertDate2(sdate.dateValue)) >= 0 Then
            'If j = 0 Then
            '    If DateDiff(DateInterval.Year, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
            '        If DateDiff(DateInterval.Month, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
            '            If CInt(DatePart(DateInterval.Day, ConvertDate2(sdate.dateValue))) < CInt(DatePart(DateInterval.Day, ConvertDate2(e.Item.Cells(2).Text))) Then
            '                tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), ConvertDate2(sdate.dateValue))
            '                Me.VTempDate = tempdate
            '                j = j + 1
            '            Else
            '                tempdate = ConvertDate2(sdate.dateValue)
            '                Me.VTempDate = tempdate
            '                j = j + 1
            '            End If
            '            Me.DaysDiff = CInt(DateDiff(DateInterval.Day, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)))
            '        End If
            '    ElseIf DateDiff(DateInterval.Year, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)) = 0 Then
            '        If DateDiff(DateInterval.Month, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)) = 0 Then
            '            If CInt(DatePart(DateInterval.Day, ConvertDate2(sdate.dateValue))) >= CInt(DatePart(DateInterval.Day, ConvertDate2(e.Item.Cells(2).Text))) Then
            '                ' tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), ConvertDate2(sdate.dateValue))
            '                tempdate = ConvertDate2(sdate.dateValue)
            '                Me.VTempDate = tempdate
            '                j = j + 1
            '                Me.DaysDiff = CInt(DateDiff(DateInterval.Day, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)))
            '            Else
            '                e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            '                j = 0
            '            End If
            '        ElseIf DateDiff(DateInterval.Month, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
            '            If CInt(DatePart(DateInterval.Day, ConvertDate2(sdate.dateValue))) < CInt(DatePart(DateInterval.Day, ConvertDate2(e.Item.Cells(2).Text))) Then
            '                tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), ConvertDate2(sdate.dateValue))
            '                Me.VTempDate = tempdate
            '                j = j + 1
            '                Me.DaysDiff = CInt(DateDiff(DateInterval.Day, ConvertDate2(sdate.dateValue), ConvertDate2(e.Item.Cells(2).Text)))
            '            Else
            '                e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            '                j = 0
            '            End If
            '        Else
            '            e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            '            j = 0
            '        End If
            '    End If

            'ElseIf j > 0 Then
            '    tempdate = DateAdd(DateInterval.Month, CDbl((Me.PaymentFrequency * intcounter)), Me.VTempDate)
            '    intcounter = intcounter + 1
            'End If
            'If j >= 1 Then
            '    e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
            '    e.Item.Cells(2).Font.Bold = True
            'End If
            ''Else
            ''    e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            ''End If
            '=================================================================================================

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            TotInstallmentAmount = CType(e.Item.FindControl("lblTotInstallmentAmount"), Label)
            TotInstallmentAmount.Text = FormatNumber(tempInstallmentAmount, 2).ToString

            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2).ToString

            TotInterestAmount = CType(e.Item.FindControl("lblTotINTERESTAMOUNT"), Label)
            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2).ToString

            TotOSPrincipalAmount = CType(e.Item.FindControl("lblTotOSPrincipalAmount"), Label)
            TotOSPrincipalAmount.Text = FormatNumber(tempOSPrincipalAmount, 2).ToString

            TotOSInterestAmount = CType(e.Item.FindControl("lblTotOSInterestAmount"), Label)
            TotOSInterestAmount.Text = FormatNumber(tempOSInterestAmount, 2).ToString

        End If
    End Sub
#End Region

#Region "Calculate"
    Private Sub BtnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCalculate.Click
        lblMessage.Text = ""
        If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtEfdate.Text)) < 0 Then
            ShowMessage(lblMessage, "Tanggal Efektif harus >= Tgl hari ini", True)
            Exit Sub
        End If

        pnlAmor.Visible = True
        Dim StrBehv As String
        GetList()
        If lblMessage.Text.Trim <> "" Then
            Exit Sub
        End If
        txtAdminFee.Text = CStr(oCustomClass.ChangeDueDateFee)
        Select Case Me.Bev
            Case "L"
                txtAdminFee.Enabled = False
                txtAdminFee.RangeValidatorMinimumValue = "0"
                txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
            Case "N"
                txtAdminFee.RangeValidatorMinimumValue = CStr(oCustomClass.ChangeDueDateFee)
                txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
            Case "X"
                txtAdminFee.RangeValidatorMaximumValue = CStr(oCustomClass.ChangeDueDateFee)
                txtAdminFee.RangeValidatorMinimumValue = "0"
            Case "D"
                txtAdminFee.RangeValidatorMinimumValue = "0"
                txtAdminFee.RangeValidatorMaximumValue = "999999999999999999"
        End Select

        ' lbljudul.Text = ConvertDate2(txtEfdate.Text).ToString("dd/MM/yyyy")
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .BusinessDate = ConvertDate2(txtsdate.Text)
        End With
        oCustomClass = oController.GetListAmor(oCustomClass)
        DtUserList = oCustomClass.ListData
        dtg.DataSource = DtUserList.DefaultView
        dtg.DataBind()

        CallPaymentInfo()
        txtInterestPaid.Text = FormatNumber(Me.TotInterest, 0)
        BtnRequest.Visible = False
        BtnCalculate2.Visible = True
        oReq.ApprovalScheme = "ACDD"
        oReq.ReasonTypeID = "AGCDD"
    End Sub
#End Region

#Region "Calculate2"
    Private Sub BtnCalculate2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCalculate2.Click
        CalAmountToBePaid()
        totAmountpaid.Text = CStr(FormatNumber(Me.TotAmountTobePaid, 2))
        lblPrepaidAmount.Text = CStr(FormatNumber(Me.PrepaidBalance, 2))
        If totAmountpaid.Text = "" Then
            BtnRequest.Visible = False
        Else
            BtnRequest.Visible = True
        End If
    End Sub
#End Region

#Region "GetList"
    Sub GetList()
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.GetList(oCustomClass)
        With oCustomClass
            Me.PaymentFrequency = CInt(.PaymentFrequency)
            Me.EffRate = .EffectiveRate
            Me.Bev = .ChangeDueDateBehaviour
            Me.PrepaidBalance = .Prepaid
            If ConvertDate2(txtsdate.Text) = .NextInstallmentDueDate Then

                ShowMessage(lblMessage, "Harap isi jatuh tempo berikutnya yang baru!", True)
                Exit Sub
            ElseIf DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtsdate.Text)) <= 0 Then

                ShowMessage(lblMessage, "Jatuh tempo berikutnya harus > tanggal hari ini!", True)
                Exit Sub
            End If
        End With
    End Sub
#End Region

#Region "CallPaymentInfo"
    Sub CallPaymentInfo()
        Dim tempdate As Date
        Dim totDiff As Integer
        Dim temp As Double
        Dim temp1 As Double

        With oPaymentInfo
            .ValueDate = ConvertDate2(txtEfdate.Text)
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With

        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.EffectiveDate = Me.BusinessDate
        oCustomClass = oController.GetMaxDate(oCustomClass)
        tempdate = oCustomClass.DueDateMax
        totDiff = CInt(DateDiff(DateInterval.Day, oCustomClass.DueDateMax, ConvertDate2(txtsdate.Text)))


        temp = (oPaymentInfo.MaximumOutStandingPrincipal * (Me.EffRate / 36000) * totDiff)
        If temp > 0 Then
            temp1 = (Round((temp + 100) / 100)) * 100
            Me.TotInterest = temp1
        End If
        If totDiff <= 0 Then
            Me.TotInterest = 0
        End If
    End Sub
#End Region

#Region "CalAmountToBePaid"
    Sub CalAmountToBePaid()
        Dim totInfo As Double
        With oPaymentInfo
            'totInfo = CDbl(.MaximumInstallmentDue) + CDbl(.MaximumLCInstallFee) + CDbl(.MaximumInstallCollFee) + CDbl(.MaximumInsurance)
            'totInfo = totInfo + CDbl(.MaximumLCInsuranceFee) + CDbl(.MaximumInsuranceCollFee) + CDbl(.MaximumPDCBounceFee) + CDbl(.MaximumSTNKRenewalFee)
            'totInfo = totInfo + CDbl(.MaximumInsuranceClaimFee) + CDbl(.MaximumReposessionFee)

            totInfo = CDbl(.TotOSOverDue)
            'MaximumInstallmentDue + MaximumLCInstallFee + MaximumInstallCollFee+
            'MaximumInsurance + MaximumInsuranceCollFee+ MaximumLCInsuranceFee+
            'MaximumPDCBounceFee + MaximumSTNKRenewalFee + MaximumInsuranceClaimFee+
            '            MaximumReposessionFee()
            Me.TotInfo = totInfo
            ' Me.TotAmountTobePaid = CDbl(txtInterestPaid.Text) + CDbl(txtAdminFee.Text) + Me.TotInfo
            Me.TotAmountTobePaid = CDbl(txtInterestPaid.Text) + CDbl(txtAdminFee.Text)


        End With
    End Sub
#End Region

#Region "Cancel"
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Server.Transfer("DueDateRequest.aspx")
    End Sub
#End Region

#Region "Request"
    Private Sub BtnRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRequest.Click
        If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            lblMessage.Text = ""
            GetList()
            CallPaymentInfo()
            If lblMessage.Text.Trim <> "" Then
                Exit Sub
            End If
            CalAmountToBePaid()
            With oCustomClass
                '==================Approval==================
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .RequestTo = oReq.ToBeApprove
                .ChangeNotes = oReq.Notes
                '==========================================
                '====================Due date================
                .EffectiveDate = ConvertDate2(txtEfdate.Text)
                .NextInstallmentDueDate = ConvertDate2(txtsdate.Text)
                .InterestAmount = CDbl(txtInterestpaid.Text)
                .AdminFee = CDbl(txtAdminFee.Text)
                .ReasonID = oReq.ReasonID
                .ReasonTypeID = oReq.ReasonTypeID

                '.Prepaid = Me.PrepaidBalance
                .TotAmountToBePaid = Me.TotAmountTobePaid
                '.TotAmountToBePaid = Me.TotAmountTobePaid
                '.OutstandingPrincipal = oPaymentInfo.MaximumOutStandingPrincipal
                '.OutStandingInterest = oPaymentInfo.MaximumOutStandingInterest
                '.InstallmentDue = oPaymentInfo.MaximumInstallmentDue
                '.InsuranceDue = oPaymentInfo.MaximumInsurance
                '.LcInstallment = oPaymentInfo.MaximumLCInstallFee
                '.LcInsurance = oPaymentInfo.MaximumLCInsuranceFee
                '.InsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee
                '.InstallmentCollFee = oPaymentInfo.MaximumInstallCollFee
                '.PDCBounceFee = oPaymentInfo.MaximumPDCBounceFee

                '.STNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee
                '.InsuranceClaimExpense = oPaymentInfo.MaximumInsuranceClaimFee
                '===========================================
                Try
                    oController.DueDateChangeRequest(oCustomClass)
                    Server.Transfer("DueDateRequest.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End With
        End If
    End Sub
#End Region

End Class