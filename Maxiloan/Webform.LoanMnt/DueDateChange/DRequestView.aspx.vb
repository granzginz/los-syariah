﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DRequestView
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    Protected WithEvents oAgree As UcAgreeExposure    
    Protected WithEvents oCust As UcCustExposure
#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "DUEDATEREQ"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack() Then
                Me.ApplicationID = Request.QueryString("ApplicationId")
                DoBind()
            End If

        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind()
        Dim oControllerInfo As New UCPaymentInfoController
        Dim oCustomClassInfo As New Parameter.AccMntBase
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With

        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        With oPaymentInfo
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .IsTitle = True
            .PaymentInfo()
        End With

        With oAgree
            .ApplicationID = Me.ApplicationID
            .AgreementExposure()
        End With

        With oCust
            .CustomerID = Me.strCustomerid
            .CustomerExposure()
        End With
    End Sub
#End Region

#Region "Cancel"
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Server.Transfer("DueDateRequest.aspx")
    End Sub
#End Region

#Region "Next"
    Private Sub BtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Server.Transfer("DRequest.aspx?Applicationid=" & Me.ApplicationID)
        End If
    End Sub
#End Region

End Class