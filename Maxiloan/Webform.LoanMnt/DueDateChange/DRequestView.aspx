﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DRequestView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.DRequestView" %>

<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcAgreeExposure" Src="../../Webform.UserController/UcAgreeExposure.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCustExposure" Src="../../Webform.UserController/UcCustExposure.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Due Date Change Request View</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN GANTI TANGGAL JATUH TEMPO
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server" EnableViewState="False"></asp:HyperLink>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ANGSURAN JATUH TEMPO PER TANGGAL
                <asp:Label ID="lbljudul" runat="server" EnableViewState="False"></asp:Label>
            </h4>
        </div>
    </div>
    <uc1:ucpaymentinfo id="oPaymentInfo" runat="server" enableviewstate="False"></uc1:ucpaymentinfo>
    <uc1:ucagreeexposure id="oAgree" runat="server" enableviewstate="False"></uc1:ucagreeexposure>
    <uc1:uccustexposure id="oCust" runat="server" enableviewstate="False"></uc1:uccustexposure>
    <div class="form_button">
        <asp:Button ID="BtnNext" runat="server" EnableViewState="False" CausesValidation="true"
            Text="Next" CssClass="small button green"></asp:Button>
        <asp:Button ID="BtnCancel" runat="server" EnableViewState="False" CausesValidation="False"
            Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
