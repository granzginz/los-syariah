﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BankMaster
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.BankMaster
    Private oController As New BankController
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            Me.FormID = "SETBANKMST"
            oSearchBy.ListData = "BANKID,Bank ID-BANKNAME,Bank Name"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.BankMasterList(oCustomClass)

        DtUserList = oCustomClass.ListBankMaster
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgBankMaster.DataSource = DvUserList
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        Try
            DtgBankMaster.DataBind()
        Catch
            DtgBankMaster.CurrentPageIndex = 0
            DtgBankMaster.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"

            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgBankMaster_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgBankMaster.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "ADD", "MAXILOAN") Then
                    Me.Mode = "EDIT"
                    lblMessage.Text = ""
                    Dim hypID As Label
                    hypID = CType(DtgBankMaster.Items(e.Item.ItemIndex).FindControl("lblBankID"), Label)
                    Dim hypName As Label
                    hypName = CType(DtgBankMaster.Items(e.Item.ItemIndex).FindControl("lblBankName"), Label)
                    Dim lblStatus As Label
                    lblStatus = CType(DtgBankMaster.Items(e.Item.ItemIndex).FindControl("lblStatus"), Label)
                    Dim lblShortName As Label
                    lblShortName = CType(DtgBankMaster.Items(e.Item.ItemIndex).FindControl("lblShortName"), Label)
                    Dim lblSandiKliring As Label
                    lblSandiKliring = CType(DtgBankMaster.Items(e.Item.ItemIndex).FindControl("lblSandiKliring"), Label)
                    pnlList.Visible = False
                    pnlDatagrid.Visible = False
                    pnlAdd.Visible = True

                    txtBankID.Text = hypID.Text.Trim
                    txtBankName.Text = hypName.Text.Trim
                    txtShortName.Text = lblShortName.Text.Trim
                    txtSandiKliring.Text = lblSandiKliring.Text.Trim

                    If lblStatus.Text = "Active" Then
                        chkIsActive.Checked = True
                    Else
                        chkIsActive.Checked = False
                    End If
                    txtBankID.Enabled = False

                    lblTitle.Text = "EDIT - BANK"
                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", "MAXILOAN") Then
                    Dim hypID As Label
                    hypID = CType(DtgBankMaster.Items(e.Item.ItemIndex).FindControl("lblBankID"), Label)
                    oCustomClass.strConnection = GetConnectionString
                    oCustomClass.BankID = hypID.Text.Trim
                    Dim strError As String
                    Try
                        strError = oController.BankMasterDelete(oCustomClass)
                        If strError = "" Then

                            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        End If
                        If Not (Me.Cache.Item(CACHE_BANK_MASTER) Is Nothing) Then
                            Me.Cache.Remove(CACHE_BANK_MASTER)
                        End If
                        DoBind(Me.SearchBy, Me.SortBy)
                    Catch Exp As Exception

                        ShowMessage(lblMessage, strError, True)
                    End Try
                End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgBankMaster_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgBankMaster.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
#End Region

#Region "Add New"
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            pnlDatagrid.Visible = False
            Me.Mode = "ADD"
            chkIsActive.Checked = True
            lblTitle.Text = "TAMBAH - BANK"
            txtBankID.Text = ""
            txtBankName.Text = ""
            lblMessage.Text = ""
            txtShortName.Text = ""
            txtSandiKliring.Text = ""
            txtBankID.Enabled = True
            chkIsActive.Checked = False
            Requiredfieldvalidator3.Enabled = True
        End If
    End Sub
#End Region

#Region "Process Save"
    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSaveAdd.Click

        Dim strError As String
        If Page.IsValid Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BankID = txtBankID.Text.Trim
                .BankName = txtBankName.Text.Trim
                .Status = chkIsActive.Checked
                .ShortName = txtShortName.Text
                .LoginId = Me.Loginid.Trim
                .SandiKliring = txtSandiKliring.Text
            End With
            Select Case Mode
                Case "ADD"
                    strError = oController.BankMasterAdd(oCustomClass)
                    If strError <> "" Then
                        ShowMessage(lblMessage, strError, True)
                    Else
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                        pnlList.Visible = True
                        pnlDatagrid.Visible = False
                        pnlAdd.Visible = False
                        DoBind(Me.SearchBy, Me.SortBy)
                        If Not (Me.Cache.Item(CACHE_BANK_MASTER) Is Nothing) Then
                            Me.Cache.Remove(CACHE_BANK_MASTER)
                        End If
                    End If

                Case "EDIT"
                    strError = oController.BankMasterUpdate(oCustomClass)
                    If strError <> "" Then

                        ShowMessage(lblMessage, strError, True)
                    Else

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                        pnlList.Visible = True
                        pnlDatagrid.Visible = False
                        pnlAdd.Visible = False
                        DoBind(Me.SearchBy, Me.SortBy)
                        If Not (Me.Cache.Item(CACHE_BANK_MASTER) Is Nothing) Then
                            Me.Cache.Remove(CACHE_BANK_MASTER)
                        End If
                    End If
            End Select
        End If
    End Sub
#End Region

#Region "Process Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim & "%' "
        Else
            Me.SearchBy = ""
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reset Process"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel Process"
    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnCancelAdd.Click
        pnlAdd.Visible = False
        lblMessage.Text = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

End Class