﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class Cashier
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oEmployee As UcLookupEmployee
    Protected WithEvents oViewEmployee As UcViewEmployee
    Protected WithEvents oBranch As UcBranch
    Protected WithEvents oSearchBy As UcSearchBy
#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property BankPurpose() As String
        Get
            Return (CType(viewstate("BankPurpose"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankPurpose") = Value
        End Set
    End Property

    Private Property BankType() As String
        Get
            Return (CType(viewstate("BankType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankType") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Cashier
    Private oController As New CashierController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        oEmployee.Style = "AccMnt"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.BranchID = Me.sesBranchId
            Me.SearchBy = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            Me.FormID = "SETCASHIER"
            oSearchBy.ListData = "EMPLOYEEID,ID Karyawan-EMPLOYEENAME,Nama Karyawan"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then

                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        lblMessage.Text = ""
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListCashier(oCustomClass)

        DtUserList = oCustomClass.ListCashier
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgCashier.DataSource = DvUserList
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlAdd.Visible = False

        Try
            DtgCashier.DataBind()
        Catch
            DtgCashier.CurrentPageIndex = 0
            DtgCashier.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgCashier_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgCashier.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                    Me.Mode = "EDIT"
                    lblMessage.Text = ""
                    Me.BranchId = oBranch.BranchID
                    lblTitle.Text = "EDIT - KASIR"
                    'btnCancelAdd.Attributes.Add("onclick", "return fback()")
                    pnlList.Visible = False
                    lblBranchID.Text = oBranch.BranchName
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = True
                    pnlLookup.Visible = False
                    pnlEdit.Visible = True
                    Dim hypID As Label
                    Dim hypName As Label
                    Dim hypTitle As Label
                    Dim hypStatus As Label
                    hypID = CType(DtgCashier.Items(e.Item.ItemIndex).FindControl("lblEmployeeId"), Label)
                    hypName = CType(DtgCashier.Items(e.Item.ItemIndex).FindControl("lblEmployeeName"), Label)
                    hypStatus = CType(DtgCashier.Items(e.Item.ItemIndex).FindControl("lblStatus"), Label)
                    hypTitle = CType(DtgCashier.Items(e.Item.ItemIndex).FindControl("lblCashierTitle"), Label)

                    lblBranchID.Text = oBranch.BranchName
                    oViewEmployee.EmployeeID = hypID.Text
                    oViewEmployee.EmployeeName = hypName.Text

                    cmbTitle.SelectedIndex = cmbTitle.Items.IndexOf(cmbTitle.Items.FindByValue(Left(hypTitle.Text.Trim, 1)))
                    If hypStatus.Text.Trim.ToUpper = "ACTIVE" Then
                        chkIsActive.Checked = True
                    Else
                        chkIsActive.Checked = False
                    End If
                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim hypID As Label
                    Dim strError As String
                    lblMessage.Text = ""
                    hypID = CType(DtgCashier.Items(e.Item.ItemIndex).FindControl("lblEmployeeId"), Label)
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .CashierID = hypID.Text
                        .BranchId = oBranch.BranchID
                    End With
                    oCustomClass.BranchId = oBranch.BranchID
                    Try
                        oController.CashierDelete(oCustomClass)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        If Not (Me.Cache.Item("CASHIER" & Me.BranchID.Replace("'", "''"))) Is Nothing Then
                            Me.Cache.Remove("CASHIER" & Me.BranchID.Replace("'", "''"))
                            Me.Cache.Remove("HEADCASHIER" & Me.BranchID.Replace("'", "''"))
                        End If
                        DoBind(Me.SearchBy, Me.SortBy)
                    Catch ex As Exception

                        ShowMessage(lblMessage, ex.Message, True)
                    End Try
                End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        lblMessage.Text = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgCashier_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgCashier.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

#End Region

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            lblMessage.Text = ""
            pnlAdd.Visible = True
            pnlDtGrid.Visible = False
            pnlList.Visible = False
            pnlEdit.Visible = False
            pnlLookup.Visible = True
            oEmployee.Style = "AccMnt"
            oEmployee.EmployeeID = ""
            oEmployee.EmployeeName = ""
            oEmployee.BranchID = oBranch.BranchID
            oEmployee.BindData()

            Me.Mode = "ADD"
            Me.BranchID = oBranch.BranchID
            lblBranchID.Text = oBranch.BranchName

            chkIsActive.Checked = True
            lblTitle.Text = "TAMBAH - KASIR"
            chkIsActive.Checked = False
            lblMessage.Visible = False
            'btnCancelAdd.Attributes.Add("onclick", "return fback()")
        End If
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSaveAdd.Click
        lblMessage.Text = ""
        Dim strError As String = ""
        If Page.IsValid Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                If Me.Mode = "ADD" Then
                    .CashierID = oEmployee.EmployeeID
                Else
                    .CashierID = oViewEmployee.EmployeeID
                End If
                .CashierStatus = chkIsActive.Checked
                .CashierTitle = cmbTitle.SelectedValue.Trim
            End With
            Select Case Me.Mode
                Case "ADD"
                    Try
                        oController.CashierAdd(oCustomClass)
                        If strError.Trim = "" Then

                            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                            If Not (Me.Cache.Item(CACHE_CASHIER_LIST & Me.BranchID.Replace("'", "''"))) Is Nothing Then
                                Me.Cache.Remove(CACHE_CASHIER_LIST & Me.BranchID.Replace("'", "''"))
                                Me.Cache.Remove(CACHE_HEAD_CASHIER & Me.BranchID.Replace("'", "''"))
                            End If
                            DoBind(Me.SearchBy, Me.SortBy)
                        Else

                            ShowMessage(lblMessage, strError, True)
                            Exit Sub
                        End If
                    Catch ex As Exception

                        ShowMessage(lblMessage, ex.Message, True)
                    End Try

                Case "EDIT"
                    Try
                        oController.CashierEdit(oCustomClass)
                        If strError.Trim = "" Then

                            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                            If Not (Me.Cache.Item(CACHE_CASHIER_LIST & Me.BranchID.Replace("'", "''"))) Is Nothing Then
                                Me.Cache.Remove(CACHE_CASHIER_LIST & Me.BranchID.Replace("'", "''"))
                                Me.Cache.Remove(CACHE_HEAD_CASHIER & Me.BranchID.Replace("'", "''"))
                            End If
                            DoBind(Me.SearchBy, Me.SortBy)
                        Else

                            ShowMessage(lblMessage, strError, True)
                            Exit Sub
                        End If
                    Catch ex As Exception

                        ShowMessage(lblMessage, ex.Message, True)
                    End Try
            End Select
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        lblMessage.Text = ""
        Me.SearchBy = ""
        Me.BranchID = oBranch.BranchID
        Me.SearchBy = " BranchID = '" & Me.BranchID.Replace("'", "''") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        lblMessage.Text = ""
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnCancelAdd.Click
        lblMessage.Text = ""
        InitialDefaultPanel()
        Me.BranchID = Me.sesBranchId
        Me.SearchBy = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class