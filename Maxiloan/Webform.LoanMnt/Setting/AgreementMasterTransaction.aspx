﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgreementMasterTransaction.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.AgreementMasterTransaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookupPaymentAllocation" Src="../../Webform.UserController/ucLookupPaymentAllocation.ascx" %>

<%@ Register TagPrefix="uc2" TagName="UcNumberFormat" Src="../../Webform.UserController/UcNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DAFTAR TRANSAKSI</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSAKSI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="TransID" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="TransID" SortExpression="TransID" HeaderText="TRANS ID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TransDesc" SortExpression="TransDesc" HeaderText="NAMA TRANSAKSI">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NoUrutTransaksi" SortExpression="NoUrutTransaksi" HeaderText="No. Urut Transaksi">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BungaNett" SortExpression="BungaNett" HeaderText="MARGIN NET">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Isactive" SortExpression="Isactive" HeaderText="STATUS">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PaymentAllocationIDSupplier" Visible="false"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button  blue"
                        CausesValidation="False"></asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI TRANSAKSI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="TransID">TRANS ID</asp:ListItem>
                            <asp:ListItem Value="TransDesc">NAMA LENGKAP</asp:ListItem>
                            <%--<asp:ListItem Value="Isactive">STATUS</asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"  MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="imbReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                    <asp:Button ID="imbBackMenu" runat="server" CausesValidation="False" Visible="false"
                        Text="Back" CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            MENAMBAHKAN MASTER TRANSAKSI -&nbsp;
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            ID TRANS
                        </label>
                        <asp:TextBox MaxLength="3" ID="txtTransID" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTransID" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Nama Transaksi
                        </label>
                        <asp:TextBox ID="txtTransDesc" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTransDesc" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Aktif
                        </label>
                        <asp:CheckBox ID="chekAktif" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label  class="label_req">
                            Payment Allocation Supplier
                        </label>
                        <asp:TextBox runat="server" ID="txtPaymentAllocationSupplier" Enabled="false" Width="342px"></asp:TextBox>
                        <input id="hdnPaymentAllocationSupplier" type="hidden" name="hdnPaymentAllocationSupplier"
                            runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPaymentAllocationSupplier" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:Button ID="btnLookupPaymentAllocationSupplier" CssClass="buttongo blue small"
                            Text="Find" runat="server" CausesValidation="False" />
                        <uc1:uclookuppaymentallocation id="ucLookupPaymentAllocationSupplier" runat="server"
                            oncatselected="CatSelectedPaymentAllocationSupplier" />
                        </uc1:ucLookupPaymentAllocation>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No. Urut Transaksi
                        </label>
                        <uc2:ucnumberformat id="txtNoUrutTrans" runat="server"></uc2:ucnumberformat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            MARGIN NETT
                        </label>
                        <asp:DropDownList runat="server" ID="cboBungaNet">
                            <asp:ListItem Enabled="true" Text="None" Selected="True"></asp:ListItem>
                            <asp:ListItem Enabled="true" Text="+" Value="+"></asp:ListItem>
                            <asp:ListItem Enabled="TRUE" Text="-" Value="-"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Distribusi ke Supplier
                        </label>
                        <asp:CheckBox runat="server" ID="isDistributable" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Potong Pencairan Supplier
                        </label>
                        <asp:DropDownList runat="server" ID="cboFlagSupplier">
                            <asp:ListItem Value="n">None</asp:ListItem>
                            <asp:ListItem Value="+" Text="(+) Plus"></asp:ListItem>
                            <asp:ListItem Value="-" Text="(-) Minus"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Read Only
                        </label>
                        <asp:CheckBox runat="server" ID="chkReadOnly" />
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label>
                            Group Transaksi
                        </label>
                        <asp:TextBox MaxLength="3" ID="txtGroupTransaksi" runat="server"></asp:TextBox>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label>
                            Employee Position
                        </label>
                        <asp:DropDownList runat="server" id="ddlEmployeeSeupplier"></asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button red">
                    </asp:Button>
                </div>
            </asp:Panel>
            <input id="hdnTransID" type="hidden" name="hdnTransID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
