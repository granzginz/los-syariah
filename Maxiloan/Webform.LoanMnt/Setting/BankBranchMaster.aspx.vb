﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BankBranchMaster
    Inherits Maxiloan.Webform.WebBased
    Private m_Bank As New DataUserControlController
    Private m_controller As New BankBranchMasterController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return CType(ViewState("SupplierGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierGroupID") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CType(ViewState("BankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""            
            Me.FormID = "BNKBRANCHMST"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtgoPage.Text = "1"
                Me.Sort = "BankBranchid ASC"
                Me.CmdWhere = "ALL"
                BindGridEntity(Me.CmdWhere)                
            End If
            BindcboNamaBank()
            BindcboNamaLengkapBank()
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oBankBranchMaster As New Parameter.BankBranchMaster
        InitialDefaultPanel()
        lblMessage.Text = ""
        oBankBranchMaster.strConnection = GetConnectionString()
        oBankBranchMaster.WhereCond = cmdWhere
        oBankBranchMaster.CurrentPage = currentPage
        oBankBranchMaster.PageSize = pageSize
        oBankBranchMaster.SortBy = Me.Sort
        oBankBranchMaster = m_controller.GetBankBranchMaster(oBankBranchMaster)

        If Not oBankBranchMaster Is Nothing Then
            dtEntity = oBankBranchMaster.Listdata
            recordCount = oBankBranchMaster.Totalrecords
        Else
            recordCount = 0
        End If
        
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oBankBranchMaster As New Parameter.BankBranchMaster
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            imbBack.Visible = False
            imbCancel.Visible = True
            imbSave.Visible = True
            imbClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oBankBranchMaster.BankBranchid = CInt(dtgPaging.DataKeys.Item(e.Item.ItemIndex))
            oBankBranchMaster.strConnection = GetConnectionString()
            oBankBranchMaster = m_controller.GetBankBranchMasterList(oBankBranchMaster)
            imbCancel.CausesValidation = False

            hdnBankBranchID.Value = oBankBranchMaster.BankBranchid.ToString
            cboNamaLengkapBank.SelectedIndex = cboNamaLengkapBank.Items.IndexOf(cboNamaLengkapBank.Items.FindByValue(oBankBranchMaster.BankID.Trim))
            txtAlamatCabang.Text = oBankBranchMaster.Address.Trim
            txtCity.Text = oBankBranchMaster.City.Trim
            txtNamaCabang.Text = oBankBranchMaster.BankBranchName.Trim            
            txtSandiBank.Text = oBankBranchMaster.BankCode.Trim
            checkAktif.Checked = oBankBranchMaster.IsActive

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.BankBranchMaster
            With customClass
                .BankBranchid = CInt(dtgPaging.DataKeys.Item(e.Item.ItemIndex))
                .strConnection = GetConnectionString()
            End With
            err = m_controller.BankBranchMasterDelete(customClass)
            If err <> "" Then

                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        Dim customClass As New Parameter.BankBranchMaster
        Dim ErrMessage As String = ""

        With customClass
            .Address = txtAlamatCabang.Text
            .BankBranchName = txtNamaCabang.Text
            .BankCode = txtSandiBank.Text
            .BankID = cboNamaLengkapBank.SelectedItem.Value
            .IsActive = checkAktif.Checked
            .City = txtCity.Text
            .Address = txtAlamatCabang.Text
            .LoginId = Me.Loginid.Trim
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.BankBranchMasterSaveAdd(customClass)
            If ErrMessage <> "" Then

                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.BankBranchid = CInt(hdnBankBranchID.Value)
            m_controller.BankBranchMasterSaveEdit(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        imbBack.Visible = False
        imbCancel.Visible = True
        imbSave.Visible = True
        imbClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        imbCancel.CausesValidation = False

        cboNamaBank.SelectedIndex = 0
        txtAlamatCabang.Text = Nothing
        txtCity.Text = Nothing
        txtNamaCabang.Text = Nothing
        txtSandiBank.Text = Nothing
        checkAktif.Checked = False

    End Sub
   
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("BankBranchMaster")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("BankBranchMaster")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbSearch.Click
        Dim optionSearch As String = ""
        Me.CmdWhere = "ALL"

        If cboNamaBank.SelectedIndex > 0 Then
            Me.CmdWhere = "BankID = '" + cboNamaBank.SelectedItem.Value + "'"
        End If
        If txtSearch.Text.Trim <> "" Then
            optionSearch = cboSearch.SelectedItem.Value + " like '%" + txtSearch.Text.Trim + "%'"
            If Me.CmdWhere = "ALL" Then
                Me.CmdWhere = optionSearch
            Else
                Me.CmdWhere = Me.CmdWhere + " and " + optionSearch
            End If
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbBackMenu_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbBackMenu.Click
        Response.Redirect("BankBranchMaster.aspx")
    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbBack.Click
        Response.Redirect("BankBranchMaster.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("BankBranchMaster.aspx")
    End Sub
    Public Sub BindcboNamaBank()
        Dim DtBankName As DataTable

        cboNamaBank.Items.Clear()
        DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)

        If DtBankName Is Nothing Then
            Dim dtBankNameCache As New DataTable

            dtBankNameCache = m_Bank.GetBankName(GetConnectionString)
            Me.Cache.Insert(CACHE_BANK_MASTER, dtBankNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)
        End If

        cboNamaBank.DataValueField = "ID"
        cboNamaBank.DataTextField = "Name"
        cboNamaBank.DataSource = DtBankName
        cboNamaBank.DataBind()
        cboNamaBank.Items.Insert(0, "Select One")
        cboNamaBank.Items(0).Value = "0"
        cboNamaBank.SelectedIndex = 0
    End Sub
    Public Sub BindcboNamaLengkapBank()
        Dim DtBankName As DataTable

        cboNamaLengkapBank.Items.Clear()
        DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)

        If DtBankName Is Nothing Then
            Dim dtBankNameCache As New DataTable

            dtBankNameCache = m_Bank.GetBankName(GetConnectionString)
            Me.Cache.Insert(CACHE_BANK_MASTER, dtBankNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)
        End If

        cboNamaLengkapBank.DataValueField = "ID"
        cboNamaLengkapBank.DataTextField = "Name"
        cboNamaLengkapBank.DataSource = DtBankName
        cboNamaLengkapBank.DataBind()
        cboNamaLengkapBank.Items.Insert(0, "Select One")
        cboNamaLengkapBank.Items(0).Value = "0"
        cboNamaLengkapBank.SelectedIndex = 0
    End Sub
End Class