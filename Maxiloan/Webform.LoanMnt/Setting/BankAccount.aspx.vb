﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BankAccount
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
    Private Property BankPurpose() As String
        Get
            Return (CType(viewstate("BankPurpose"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankPurpose") = Value
        End Set
    End Property
    Private Property BankType() As String
        Get
            Return (CType(viewstate("BankType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankType") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property cmdWhere() As String
        Get
            Return (CType(viewstate("cmdWhere"), String))
        End Get
        Set(ByVal cmdWhere As String)
            ViewState("cmdWhere") = cmdWhere
        End Set
    End Property
    Private Property SortBy() As String
        Get
            Return (CType(ViewState("SortBy"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.BankAccount
    Private oController As New BankController
    Private oClassAddress As New Parameter.Address
    Private oClassPersonal As New Parameter.Personal
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oContactPerson As UcContactPerson
    Protected WithEvents oCompanyAddress As UcCompanyAddress
    Protected WithEvents oBankAccount As UcBankAccount
    Protected WithEvents oBranch As UcBranch
    Protected WithEvents oViewAddress As UcViewAddress    
    Protected WithEvents oViewBankAccount As ucViewBankAccount
    Protected WithEvents oViewContactPerson As UcViewContactPerson
    Protected WithEvents txtTarifSKN As ucNumberFormat
    Protected WithEvents txtTarifRTGS As ucNumberFormat
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()

            Me.cmdWhere = " BranchID = '" & Me.sesBranchId.Replace("'", "") & "'"
            oBranch.BranchID = Me.sesBranchId.Replace("'", "").Trim
            Me.SortBy = ""

            DoBind()

            Me.FormID = "SETBANKACCOUNT"

            oSearchBy.ListData = "BANKACCOUNTID,ID Bank-BANKNAME,Nama Bank-BANKACCOUNTNAME,Nama Rekening"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Me.cmdWhere = ""
                Me.SortBy = ""

                oBankAccount.RequiredBankName()
                oBankAccount.Style = "Marketing"
                oBankAccount.BindBankAccount()

            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        With txtTarifSKN
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtTarifRTGS
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With

        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlEditDetail.Visible = False
        pnlView.Visible = False
        lblMessage.Text = ""
    End Sub

    Sub BindTempDataTable()
        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetBankAccountList"
        oInstal.WhereCond = " BranchID = '" + sesBranchId.Replace("'", "") + "'  "

        TempDataTable = oConInst.GetSP(oInstal).ListData
    End Sub

    Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.BankAccountList(oCustomClass)

        DtUserList = oCustomClass.ListBankAccount
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgBankAccount.DataSource = DvUserList

        Try
            DtgBankAccount.DataBind()
        Catch
            DtgBankAccount.CurrentPageIndex = 0
            DtgBankAccount.DataBind()
        End Try

        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlEditDetail.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data Tidak ditemukan  .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Text = ""

        DoBind()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind()
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgBankAccount_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgBankAccount.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                    Me.Mode = "EDIT"
                    lblMessage.Text = ""
                    lblTitle.Text = "EDIT - REKENING BANK"

                    BindBankPurpose()
                    BindBankType()

                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = True
                    Requiredfieldvalidator3.Enabled = False
                    txtBankAccountID.Visible = False
                    lblBankAccountID.Visible = True

                    Dim hypID As LinkButton
                    Dim dtView As New DataTable

                    hypID = CType(DtgBankAccount.Items(e.Item.ItemIndex).FindControl("lnkBankAccountID"), LinkButton)
                    lblBankAccountID.Text = hypID.Text
                    lblBranchID.Text = oBranch.BranchName

                    dtView = oController.BankAccountInformasi(GetConnectionString, hypID.Text.Trim, oBranch.BranchID.Trim)
                    Me.BranchID = oBranch.BranchID.Trim
                    BindEditPanel(dtView)
                    dtView.Dispose()
                    cmbPurpose.SelectedIndex = cmbPurpose.Items.IndexOf(cmbPurpose.Items.FindByValue(Me.BankPurpose))
                    cmbAccountType.SelectedIndex = cmbAccountType.Items.IndexOf(cmbAccountType.Items.FindByValue(Me.BankType))
                End If

            Case "DELETE"
                lblMessage.Text = ""
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim hypID As LinkButton
                    Dim strError As String

                    hypID = CType(DtgBankAccount.Items(e.Item.ItemIndex).FindControl("lnkBankAccountID"), LinkButton)
                    oCustomClass.strConnection = GetConnectionString()
                    oCustomClass.BankAccountID = hypID.Text
                    oCustomClass.BranchId = oBranch.BranchID
                    Try
                        strError = oController.BankAccountDelete(oCustomClass)
                        If strError = "" Then

                            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                            RemoveCache()
                            DoBind()
                        Else

                            ShowMessage(lblMessage, strError, True)
                        End If

                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            Case "ShowView"
                If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                    lblMessage.Text = ""
                    Dim hypID As LinkButton
                    hypID = CType(DtgBankAccount.Items(e.Item.ItemIndex).FindControl("lnkBankAccountID"), LinkButton)

                    Dim dtView As New DataTable
                    dtView = oController.BankAccountInformasi(GetConnectionString, hypID.Text.Trim, oBranch.BranchID.Trim)
                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlView.Visible = True

                    lblViewBranch.Text = oBranch.BranchName
                    lblViewBankAccountID.Text = hypID.Text.Trim
                    Me.BranchID = oBranch.BranchID.Trim
                    BindViewPanel(dtView)
                    dtView.Dispose()
                    'imbBack.Attributes.Add("onclick", "return fback()")
                End If

            Case "EDITDETAIL"
                If CheckFeature(Me.Loginid, Me.FormID, "DTL", Me.AppId) Then
                    Me.Mode = "EDITDETAIL"

                    lblMessage.Text = ""
                    'imbCancelEditDetail.Attributes.Add("onclick", "return fback()")

                    BindBankPurpose()
                    BindBankType()
                    oBankAccount.RequiredBankName()

                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = False
                    pnlEditDetail.Visible = True
                    Requiredfieldvalidator3.Enabled = False
                    txtBankAccountID.Visible = False
                    lblBankAccountID.Visible = True

                    Dim hypID As LinkButton
                    Dim dtView As New DataTable

                    hypID = CType(DtgBankAccount.Items(e.Item.ItemIndex).FindControl("lnkBankAccountID"), LinkButton)
                    lblEditBankAccountID.Text = hypID.Text
                    lblEditBranch.Text = oBranch.BranchName

                    Me.BranchID = oBranch.BranchID.Trim

                    dtView = oController.BankAccountInformasi(GetConnectionString, hypID.Text.Trim, oBranch.BranchID.Trim)

                    BindEditDetailPanel(dtView)
                    dtView.Dispose()
                End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind()
    End Sub

    Private Sub DtgBankAccount_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgBankAccount.ItemDataBound
        Dim imbDelete As ImageButton
        Dim chekDefault As New CheckBox
        Dim chekValue As New Label

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm();")

            chekValue = CType(e.Item.FindControl("hdncheckDefault"), Label)
            chekDefault = CType(e.Item.FindControl("checkDefault"), CheckBox)
            If CBool(chekValue.Text) Then
                chekDefault.Checked = True
            Else
                chekDefault.Checked = False
            End If

        End If
    End Sub
#End Region

#Region "Bind Panel"
    Private Sub BindEditDetailPanel(ByVal DtInfo As DataTable)
        Try
            If DtInfo.Rows.Count > 0 Then
                lblEditBankAccountName.Text = CStr(DtInfo.Rows(0).Item("BankAccountName")).Trim
                oCompanyAddress.ValidatorFalse()
                lblEditCoa.Text = CStr(DtInfo.Rows(0).Item("Coa")).Trim
                Select Case CStr(DtInfo.Rows(0).Item("BankPurpose")).Trim
                    Case "EC"
                        lblEditAccountPurpose.Text = "Escrow"
                    Case "PC"
                        lblEditAccountPurpose.Text = "Petty Cash"
                    Case "FD"
                        lblEditAccountPurpose.Text = "Funding"
                End Select

                Select Case CStr(DtInfo.Rows(0).Item("BankAccountType")).Trim
                    Case "C"
                        lblEditAccountType.Text = "Cash"
                    Case "B"
                        lblEditAccountType.Text = "Bank"
                End Select

                cmbAccountType.SelectedIndex = cmbAccountType.Items.IndexOf(cmbAccountType.Items.FindByValue(CStr(DtInfo.Rows(0).Item("BankAccountType")).Trim))

                lblEditActive.Text = CType(IIf(CBool(DtInfo.Rows(0).Item("IsActive")), "Active", "Not Active"), String)
                lblEditIsGenerate.Text = CType(IIf(CBool(DtInfo.Rows(0).Item("IsGenerateTextAvailable")), "Yes", "No"), String)
                With oCompanyAddress
                    .Address = CStr(DtInfo.Rows(0).Item("BankAddress")).Trim
                    .RT = CStr(DtInfo.Rows(0).Item("BankRT")).Trim
                    .RW = CStr(DtInfo.Rows(0).Item("BankRW")).Trim
                    .Kelurahan = CStr(DtInfo.Rows(0).Item("BankKelurahan")).Trim
                    .Kecamatan = CStr(DtInfo.Rows(0).Item("BankKecamatan")).Trim
                    .City = CStr(DtInfo.Rows(0).Item("BankCity")).Trim
                    .ZipCode = CStr(DtInfo.Rows(0).Item("BankZipCode")).Trim
                    .AreaPhone1 = CStr(DtInfo.Rows(0).Item("BankAreaPhone1")).Trim
                    .Phone1 = CStr(DtInfo.Rows(0).Item("BankPhone1")).Trim
                    .AreaPhone2 = CStr(DtInfo.Rows(0).Item("BankAreaPhone2")).Trim
                    .Phone2 = CStr(DtInfo.Rows(0).Item("BankPhone2")).Trim
                    .AreaFax = CStr(DtInfo.Rows(0).Item("BankAreaFax")).Trim
                    .Fax = CStr(DtInfo.Rows(0).Item("BankFax")).Trim
                    .Style = "AccMnt"
                    .BindAddress()
                End With

                With oContactPerson
                    .ContactPerson = CStr(DtInfo.Rows(0).Item("ContactPersonName")).Trim
                    .ContactPersonTitle = CStr(DtInfo.Rows(0).Item("ContactPersonTitle")).Trim
                    .Email = CStr(DtInfo.Rows(0).Item("Email")).Trim
                    .MobilePhone = CStr(DtInfo.Rows(0).Item("MobilePhone")).Trim
                    .BindContacPerson()
                End With

                With oBankAccount
                    .BankBranchId = CStr(DtInfo.Rows(0).Item("BankBranchId")).Trim
                    .BindBankAccount()
                    .RequiredBankName()
                    .AccountName = CStr(DtInfo.Rows(0).Item("AccountName")).Trim
                    .BankBranch = CStr(DtInfo.Rows(0).Item("BankBranch")).Trim
                    .AccountNo = CStr(DtInfo.Rows(0).Item("AccountNO")).Trim
                    .BankID = CStr(DtInfo.Rows(0).Item("BankID")).Trim
                    'TODO Bank Account
                    '.setUrlLookup()
                    '.setValidatorLookup()
                End With
                txtTarifRTGS.Text = CInt(CStr(DtInfo.Rows(0).Item("TarifRTGS")).Trim).ToString
                txtTarifSKN.Text = CInt(CStr(DtInfo.Rows(0).Item("TarifSKN")).Trim).ToString
                lblEditDefault.Text = IIf(CBool(DtInfo.Rows(0).Item("BankAccountDefault")), "Yes", "No")
            End If
        Catch exp As Exception

            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

    Private Sub BindEditPanel(ByVal DtInfo As DataTable)
        If DtInfo.Rows.Count > 0 Then
            txtBankAccountName.Text = CStr(DtInfo.Rows(0).Item("BankAccountName")).Trim
            txtCoa.Text = CStr(DtInfo.Rows(0).Item("Coa")).Trim
            Me.BankPurpose = CStr(DtInfo.Rows(0).Item("BankPurpose")).Trim
            Me.BankType = CStr(DtInfo.Rows(0).Item("BankAccountType")).Trim
            chkIsActive.Checked = CBool(DtInfo.Rows(0).Item("IsActive"))
            chkIsGenerate.Checked = CBool(DtInfo.Rows(0).Item("IsGenerateTextAvailable"))
            chkBankAccountDefault.Checked = CBool(DtInfo.Rows(0).Item("BankAccountDefault"))
            chkSweepOtomatis.Checked = CBool(DtInfo.Rows(0).Item("IsSweepOtomatis"))
        End If

    End Sub

    Private Sub BindViewPanel(ByVal dtinfo As DataTable)
        If dtinfo.Rows.Count > 0 Then
            With (dtinfo.Rows(0))
                lblViewBankAccountName.Text = CStr(dtinfo.Rows(0).Item("BankAccountName")).Trim
                lblCoa.Text = CStr(dtinfo.Rows(0).Item("Coa")).Trim
                lblAccountPurpose.Text = CStr(dtinfo.Rows(0).Item("BankPurposeDesc")).Trim
                lblBankAccountType.Text = CStr(dtinfo.Rows(0).Item("BankAccountTypeDesc")).Trim
                cmbAccountType.SelectedIndex = cmbAccountType.Items.IndexOf(cmbAccountType.Items.FindByValue(lblBankAccountType.Text.Trim))

                If CBool(dtinfo.Rows(0).Item("IsActive")) Then
                    lblIsActive.Text = "Active"
                Else
                    lblIsActive.Text = "Not Active"
                End If
                If CBool(dtinfo.Rows(0).Item("IsGenerateTextAvailable")) Then
                    lblIsGenerateText.Text = "Yes"
                Else
                    lblIsGenerateText.Text = "No"
                End If
                With oViewAddress
                    .Address = CStr(dtinfo.Rows(0).Item("BankAddress")).Trim
                    .RT = CStr(dtinfo.Rows(0).Item("BankRT")).Trim
                    .RW = CStr(dtinfo.Rows(0).Item("BankRW")).Trim
                    .Kelurahan = CStr(dtinfo.Rows(0).Item("BankKelurahan")).Trim
                    .Kecamatan = CStr(dtinfo.Rows(0).Item("BankKecamatan")).Trim
                    .City = CStr(dtinfo.Rows(0).Item("BankCity")).Trim
                    .ZipCode = CStr(dtinfo.Rows(0).Item("BankZipCode")).Trim
                    .AreaPhone1 = CStr(dtinfo.Rows(0).Item("BankAreaPhone1")).Trim
                    .Phone1 = CStr(dtinfo.Rows(0).Item("BankPhone1")).Trim
                    .AreaPhone2 = CStr(dtinfo.Rows(0).Item("BankAreaPhone2")).Trim
                    .Phone2 = CStr(dtinfo.Rows(0).Item("BankPhone2")).Trim
                    .AreaFax = CStr(dtinfo.Rows(0).Item("BankAreaFax")).Trim
                    .Fax = CStr(dtinfo.Rows(0).Item("BankFax")).Trim
                End With

                With oViewContactPerson
                    .ContactPerson = CStr(dtinfo.Rows(0).Item("ContactPersonName")).Trim
                    .ContactPersonTitle = CStr(dtinfo.Rows(0).Item("ContactPersonTitle")).Trim
                    .Email = CStr(dtinfo.Rows(0).Item("Email")).Trim
                    .MobilePhone = CStr(dtinfo.Rows(0).Item("MobilePhone")).Trim
                End With

                With oViewBankAccount
                    .AccountName = CStr(dtinfo.Rows(0).Item("AccountName")).Trim
                    .BankBranch = CStr(dtinfo.Rows(0).Item("BankBranch")).Trim
                    .AccountNo = CStr(dtinfo.Rows(0).Item("AccountNO")).Trim
                    .BankName = CStr(dtinfo.Rows(0).Item("BankID")).Trim
                End With
                lblTarifRTGS.Text = CStr(dtinfo.Rows(0).Item("TarifRTGS")).Trim
                lblTarifSKN.Text = CStr(dtinfo.Rows(0).Item("TarifSKN")).Trim
                lblDefault.Text = IIf(CBool(dtinfo.Rows(0).Item("BankAccountDefault")), "Yes", "No")
            End With
        End If
    End Sub
#End Region

#Region "Bind Bank Purpose and Bank Type"
    Private Sub BindBankPurpose()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim i As Integer
        Dim splitListData() As String
        Dim splitRow() As String
        Dim strList As String
        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))
        strList = "EC,Escrow Account-FD,Funding Account-PC,Petty Cash"
        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        splitListData = Split(strList, "-")
        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbPurpose.DataValueField = "ID"
        cmbPurpose.DataTextField = "Description"
        cmbPurpose.DataSource = oDataTable
        cmbPurpose.DataBind()
        oDataTable.Dispose()
        cmbPurpose.Items.Insert(0, "Select One")
        cmbPurpose.Items(0).Value = "0"
        cmbPurpose.SelectedIndex = 0
    End Sub

    Private Sub BindBankType()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim i As Integer
        Dim splitListData() As String
        Dim splitRow() As String
        Dim strList As String
        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))
        strList = "B,BANK-C,CASH"
        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        splitListData = Split(strList, "-")
        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbAccountType.DataValueField = "ID"
        cmbAccountType.DataTextField = "Description"
        cmbAccountType.DataSource = oDataTable
        cmbAccountType.DataBind()
        cmbAccountType.Dispose()
        cmbAccountType.Items.Insert(0, "Select One")
        cmbAccountType.Items(0).Value = "0"
        cmbAccountType.SelectedIndex = 0
    End Sub
#End Region

#Region "Saving Process"
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAdd.Visible = True
            pnlDtGrid.Visible = False
            pnlList.Visible = False
            lblMessage.Text = ""
            Me.Mode = "ADD"
            Requiredfieldvalidator3.Enabled = True
            txtBankAccountID.Visible = True
            lblBankAccountID.Visible = False
            Me.BranchID = oBranch.BranchID
            lblBranchID.Text = oBranch.BranchName
            chkIsActive.Checked = True
            lblTitle.Text = "TAMBAH - REKENING BANK"
            chkIsActive.Checked = False
            Requiredfieldvalidator3.Enabled = True
            BindBankPurpose()
            BindBankType()
            oBankAccount.RequiredBankName()
            txtBankAccountID.Text = ""
            txtBankAccountName.Text = ""
            txtCoa.Text = ""

            chkIsActive.Checked = True
            chkIsGenerate.Checked = False
            chkBankAccountDefault.Checked = False
            chkSweepOtomatis.Checked = False
        End If
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSaveAdd.Click
        Dim strError As String
        If Page.IsValid Then
            lblMessage.Text = ""

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")

                If Me.Mode = "ADD" Then
                    .BankAccountID = txtBankAccountID.Text.Trim
                Else
                    .BankAccountID = lblBankAccountID.Text.Trim
                End If

                .BankAccountName = txtBankAccountName.Text.Trim
                .Coa = txtCoa.Text.Trim
                .BankAccountType = cmbAccountType.SelectedItem.Value.Trim
                .BankPurpose = cmbPurpose.SelectedItem.Value.Trim

                .Status = chkIsActive.Checked
                .IsGenerate = chkIsGenerate.Checked
                .BankAccountDefault = chkBankAccountDefault.Checked
                .SweepOtomatis = chkSweepOtomatis.Checked
            End With

            BindTempDataTable()
            Me.cmdWhere = " BranchID = '" & Me.sesBranchId.Replace("'", "") & "'"
            Me.SortBy = ""
            For index = 0 To TempDataTable.Rows.Count - 1
                If CBool(TempDataTable.Rows(index).Item("BankAccountDefault").ToString) And chkBankAccountDefault.Checked And _
                   TempDataTable.Rows(index).Item("Bankaccountid").ToString.Trim <> oCustomClass.BankAccountID.Trim Then
                    ShowMessage(lblMessage, "Default tiap Type hanya satu", True)
                    Exit Sub
                End If
            Next


            If Me.Mode <> "EDITDETAIL" Then
                With oCustomClass
                    .AccountName = ""
                    .BankID = ""
                    .BankBranch = ""
                    .AccountNo = ""
                End With
                With oClassAddress
                    .Address = ""
                    .RT = ""
                    .RW = ""
                    .Kelurahan = ""
                    .Kecamatan = ""
                    .City = ""
                    .ZipCode = ""
                    .AreaPhone1 = ""
                    .Phone1 = ""
                    .AreaPhone2 = ""
                    .Phone2 = ""
                    .AreaFax = ""
                    .Fax = ""
                End With

                With oClassPersonal
                    .PersonName = ""
                    .PersonTitle = ""
                    .Email = ""
                    .MobilePhone = ""
                End With
            End If

            Select Case Me.Mode
                Case "ADD"
                    Try
                        strError = oController.BankAccountAdd(oCustomClass, oClassAddress, oClassPersonal)
                        If strError <> "" Then

                            ShowMessage(lblMessage, strError, True)
                            Exit Sub
                        Else
                            pnlList.Visible = True
                            pnlDtGrid.Visible = False
                            pnlAdd.Visible = False
                            pnlEditDetail.Visible = False

                            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                            RemoveCache()
                            DoBind()
                        End If
                    Catch exp As Exception

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    End Try
                Case "EDIT"
                    Try
                        oController.BankAccountUpdateH(oCustomClass)
                        pnlList.Visible = True
                        pnlDtGrid.Visible = False
                        pnlAdd.Visible = False
                        pnlEditDetail.Visible = False

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                        RemoveCache()
                        Me.BranchID = oBranch.BranchID
                        Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
                        DoBind()
                    Catch exp As Exception

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                    End Try
            End Select
        End If
    End Sub

    Private Sub RemoveCache()
        Dim strCache As String
        Dim i, j As Int32
        Dim strArr() As String
        Dim strType As String
        Dim strPurpose As String
        strCache = CACHE_BANKACCOUNT & oBranch.BranchID
        For i = 1 To 3
            If i = 1 Then
                strType = "C"
            ElseIf i = 2 Then
                strType = "B"
            ElseIf i = 3 Then
                strType = ""
            End If
            For j = 1 To 4
                If j = 1 Then
                    strPurpose = "EC"
                ElseIf j = 2 Then
                    strPurpose = "PC"
                ElseIf j = 3 Then
                    strPurpose = "FD"
                ElseIf j = 4 Then
                    strPurpose = ""
                End If
                If Not (Me.Cache.Item(strCache & strType & strPurpose)) Is Nothing Then
                    Me.Cache.Remove(strCache & strType & strPurpose)
                End If
            Next
        Next
    End Sub

    Private Sub imbSaveEditDetail_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSaveEditDetail.Click
        Dim strError As String

        lblMessage.Text = ""

        If Page.IsValid Then
            If Me.Mode = "EDITDETAIL" Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .BankAccountID = lblEditBankAccountID.Text.Trim
                    .BankAccountName = lblEditBankAccountName.Text.Trim
                    .Coa = txtCoa.Text.Trim

                    Select Case lblEditAccountPurpose.Text.Trim
                        Case "Escrow"
                            .BankPurpose = "EC"
                        Case "Funding"
                            .BankPurpose = "FD"
                        Case "Petty Cash"
                            .BankPurpose = "PC"
                    End Select

                    Select Case lblEditAccountType.Text.Trim
                        Case "Cash"
                            .BankAccountType = "C"
                        Case "Bank"
                            .BankAccountType = "B"
                    End Select

                    .Status = CBool(IIf(lblEditActive.Text = "Active", True, False))
                    .IsGenerate = CBool(IIf(lblEditIsGenerate.Text = "Yes", True, False))
                End With

                cmbAccountType.SelectedIndex = cmbAccountType.Items.IndexOf(cmbAccountType.Items.FindByValue(oCustomClass.BankAccountType.Trim))

                With oCustomClass
                    .BankAccountDefault = chkBankAccountDefault.Checked
                    .SweepOtomatis = chkSweepOtomatis.Checked
                    .AccountName = oBankAccount.AccountName
                    .BankID = oBankAccount.BankID
                    .BankBranch = oBankAccount.BankBranch
                    .AccountNo = oBankAccount.AccountNo
                    .BankBranchId = oBankAccount.BankBranchId
                    .TarifSKN = CDec(txtTarifSKN.Text)
                    .TarifRTGS = CDec(txtTarifRTGS.Text)
                End With


                With oClassAddress
                    .Address = oCompanyAddress.Address
                    .RT = oCompanyAddress.RT
                    .RW = oCompanyAddress.RW
                    .Kelurahan = oCompanyAddress.Kelurahan
                    .Kecamatan = oCompanyAddress.Kecamatan
                    .City = oCompanyAddress.City
                    .ZipCode = oCompanyAddress.ZipCode
                    .AreaPhone1 = oCompanyAddress.AreaPhone1
                    .Phone1 = oCompanyAddress.Phone1
                    .AreaPhone2 = oCompanyAddress.AreaPhone2
                    .Phone2 = oCompanyAddress.Phone2
                    .AreaFax = oCompanyAddress.AreaFax
                    .Fax = oCompanyAddress.Fax
                End With

                With oClassPersonal
                    .PersonName = oContactPerson.ContactPerson
                    .PersonTitle = oContactPerson.ContactPersonTitle
                    .Email = oContactPerson.Email
                    .MobilePhone = oContactPerson.MobilePhone
                End With

                BindTempDataTable()

                For index = 0 To TempDataTable.Rows.Count - 1
                    If lblEditBankAccountID.Text.Trim <> TempDataTable.Rows(index).Item("Bankaccountid").ToString.Trim And CBool(TempDataTable.Rows(index).Item("BankAccountDefault").ToString) And chkBankAccountDefault.Checked Then
                        ShowMessage(lblMessage, "Default tiap Type hanya satu", True)
                        Exit Sub
                    End If
                Next

                strError = oController.BankAccountUpdate(oCustomClass, oClassAddress, oClassPersonal)

                If strError <> "" Then

                    ShowMessage(lblMessage, strError, True)
                    Exit Sub
                Else
                    pnlList.Visible = True
                    pnlDtGrid.Visible = False
                    pnlEditDetail.Visible = False
                    pnlAdd.Visible = False
                    RemoveCache()

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    Me.BranchID = oBranch.BranchID
                    Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
                    DoBind()
                End If
            End If
        End If
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        lblMessage.Text = ""
        Me.BranchID = oBranch.BranchID
        Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.cmdWhere = Me.cmdWhere & " And " & oSearchBy.ValueID & " Like '%" & oSearchBy.Text.Trim & "%' "
        End If
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind()
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        lblMessage.Text = ""
        Me.BranchID = oBranch.BranchID
        Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "        
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind()
    End Sub
#End Region


#Region "Not Use"

    'With oCompanyAddress
    '    .Address = ""
    '    .RT = ""
    '    .RW = ""
    '    .Kelurahan = ""
    '    .Kecamatan = ""
    '    .City = ""
    '    .ZipCode = ""
    '    .AreaPhone1 = ""
    '    .Phone1 = ""
    '    .AreaPhone2 = ""
    '    .Phone2 = ""
    '    .AreaFax = ""
    '    .Fax = ""
    '    .Style = "AccMnt"
    '    .BindAddress()
    'End With

    'With oContactPerson
    '    .ContactPerson = ""
    '    .ContactPersonTitle = ""
    '    .Email = ""
    '    .MobilePhone = ""
    '    .BindContacPerson()
    'End With

    'With oBankAccount
    '    .AccountName = ""
    '    .BankBranch = ""
    '    .AccountNo = ""
    '    .BindBankAccount()
    'End With
#End Region

#Region "Cancel Process"
    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnCancelAdd.Click
        lblMessage.Text = ""
        Me.BranchID = oBranch.BranchID
        Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlEditDetail.Visible = False
        pnlAdd.Visible = False
        DoBind()
    End Sub

    Private Sub imbCancelEditDetail_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancelEditDetail.Click
        lblMessage.Text = ""
        Me.BranchID = oBranch.BranchID
        Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlEditDetail.Visible = False
        pnlAdd.Visible = False
        DoBind()
    End Sub

#End Region
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbBack.Click
        lblMessage.Text = ""
        Me.BranchID = oBranch.BranchID
        Me.cmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlEditDetail.Visible = False
        pnlAdd.Visible = False
        pnlView.Visible = False        
        DoBind()
    End Sub    
End Class