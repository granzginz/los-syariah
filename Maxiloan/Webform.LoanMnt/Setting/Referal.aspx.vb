﻿
#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class Referal
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccount As UcBankAccount
    Private m_controller As New ReferalController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Dim strStyle As String = "Marketing"
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            Me.BranchID = Me.sesBranchId.Replace("'", "") & "' "
            If CheckForm(Me.Loginid, "Referal", "MAXILOAN") Then
                txtgoPage.Text = "1"
                Me.Sort = "NPP ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"

                End If
                With UcBankAccount
                    .ValidatorTrue()
                    .Style = strStyle
                    .BindBankAccount()
                End With
                BindGridEntity(Me.CmdWhere)

                If Request("cmd") = "dtl" Then
                    If CheckFeature(Me.Loginid, "Referal", "View", "MAXILOAN") Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If
                    BindDetail(Request("id"), Request("desc"))
                End If
                BtnClose.Attributes.Add("OnClick", "return fClose()")
            End If
        End If

    End Sub

    Sub BindAdd()
        UcBankAccount.BindBankAccount()
        UcBankAccount.BankID = ""
        UcBankAccount.BankBranch = ""
        UcBankAccount.AccountNo = ""
        UcBankAccount.AccountName = ""
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oReferal As New Parameter.Referal
        InitialDefaultPanel()
        oReferal.strConnection = GetConnectionString()
        oReferal.WhereCond = cmdWhere
        oReferal.CurrentPage = currentPage
        oReferal.PageSize = pageSize
        oReferal.SortBy = Me.Sort
        oReferal = m_controller.GetReferal(oReferal)

        If Not oReferal Is Nothing Then
            dtEntity = oReferal.ListData
            recordCount = oReferal.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            btnPrint.Enabled = False
        Else
            btnPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal Desc As String)
        Dim oReferal As New Parameter.Referal
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd2") = "close" Then
            BtnBack.Visible = False
            BtnClose.Visible = True
        Else
            BtnBack.Visible = True
            BtnClose.Visible = False
        End If
        BtnCancel.Visible = False
        BtnSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oReferal.UnitBisnisID = ID
        oReferal.strConnection = GetConnectionString()
        oReferal = m_controller.GetReferalEdit(oReferal)

        'lblID.Visible = True
        txtID.Visible = False
        'lblNPP.Visible = True
        txtNPP.Visible = False


        'lblID.Text = ID
        'lblNPP.Text = Desc
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Protected Sub BtnGoPage_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oReferal As New Parameter.Referal
        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, "Referal", "EDIT", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            BtnBack.Visible = False
            BtnCancel.Visible = True
            BtnSave.Visible = True
            BtnClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oReferal.NPP = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oReferal.strConnection = GetConnectionString()
            oReferal = m_controller.GetReferalEdit(oReferal)
            'lblID.Visible = True
            txtID.Visible = True
            'lblNPP.Visible = False
            txtNPP.Visible = True

            'lblID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            txtID.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(4).Text
            txtNPP.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(3).Text
            txtLokasi.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(5).Text
            txtNama.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(7).Text



        ElseIf e.CommandName = "DEL" Then
            If CheckFeature(Me.Loginid, "Referal", "DEL", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.Referal
            With customClass
                '.NPP = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ce.ToString
                .NPP = dtgPaging.Items(e.Item.ItemIndex).Cells(3).Text.Trim
                'dtgPaging.Items(e.Item.ItemIndex).Cells(6).Text.Trim
                .strConnection = GetConnectionString()
            End With
            err = m_controller.ReferalDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
            Response.Redirect("Referal.aspx")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.Referal
        Dim ErrMessage As String = ""
        With customClass
            .BranchID = Me.BranchID
            .UnitBisnisID = txtID.Text
            .NPP = txtNPP.Text
            .NPWP = txtNPWP.Text
            .Lokasi = txtLokasi.Text
            .Nama = txtNama.Text
            .BankID = UcBankAccount.BankID
            .BankBranch = UcBankAccount.BankBranch
            .AccountNo = UcBankAccount.AccountNo
            .AccountName = UcBankAccount.AccountName
            .BankBranchId = UcBankAccount.BankBranchId
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.ReferalAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            m_controller.ReferalUpdate(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, "Referal", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        pnlAddEdit.Visible = True
        pnlList.Visible = False
        BtnBack.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True
        BtnClose.Visible = False

        BindAdd()

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        txtID.Visible = True
        txtID.Text = ""
        txtNPP.Visible = True
        txtNPP.Text = ""
        txtLokasi.Text = ""
        txtNama.Text = ""
        txtNPWP.Text = ""

    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If CheckFeature(Me.Loginid, "Referal", "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/ReferalReport.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("Referal")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Referal")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("Referal.aspx")
    End Sub


    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnClose.Click
        Response.Redirect("Referal.aspx")
    End Sub

    Protected Sub BtnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnBack.Click
        Response.Redirect("Referal.aspx")
    End Sub




End Class