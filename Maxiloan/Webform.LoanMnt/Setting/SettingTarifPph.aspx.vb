﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class SettingTarifPph
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.RatePphMaster
    Private oController As New RatePphController
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            Me.FormID = "SETRATEPPH"
            oSearchBy.ListData = "Level,Level"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'Dim intloop As Integer
        'Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.RatePphMasterList(oCustomClass)

        DtUserList = oCustomClass.ListRatePphMaster
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgTarifPph.DataSource = DvUserList
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        Try
            DtgTarifPph.DataBind()
        Catch
            DtgTarifPph.CurrentPageIndex = 0
            DtgTarifPph.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"

            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgTarifPph_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgTarifPph.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "ADD", "MAXILOAN") Then
                    Me.Mode = "EDIT"
                    lblMessage.Text = ""
                    Dim lblLevel As Label
                    lblLevel = CType(DtgTarifPph.Items(e.Item.ItemIndex).FindControl("lblLevel"), Label)
                    Dim lblFrom As Label
                    lblFrom = CType(DtgTarifPph.Items(e.Item.ItemIndex).FindControl("lblFrom"), Label)
                    Dim lblTo As Label
                    lblTo = CType(DtgTarifPph.Items(e.Item.ItemIndex).FindControl("lblTo"), Label)
                    Dim lblRateNpwp As Label
                    lblRateNpwp = CType(DtgTarifPph.Items(e.Item.ItemIndex).FindControl("lblRate_npwp"), Label)
                    Dim lblRateNonNpwp As Label
                    lblRateNonNpwp = CType(DtgTarifPph.Items(e.Item.ItemIndex).FindControl("lblRate_non_npwp"), Label)
                    pnlList.Visible = False
                    pnlDatagrid.Visible = False
                    pnlAdd.Visible = True

                    txtLevel.Text = lblLevel.Text.Trim
                    txtFrom.Text = lblFrom.Text.Trim
                    txtTo.Text = lblRateNpwp.Text.Trim
                    txtRate_npwp.Text = lblRateNpwp.Text.Trim
                    txtRate_non_npwp.Text = lblRateNonNpwp.Text.Trim



                    lblTitle.Text = "EDIT - RATE PPH"
                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", "MAXILOAN") Then
                    Dim lblLevel As Label
                    lblLevel = CType(DtgTarifPph.Items(e.Item.ItemIndex).FindControl("lblLevel"), Label)
                    oCustomClass.strConnection = GetConnectionString()
                    oCustomClass.Level = lblLevel.Text.Trim
                    Dim strError As String = ""
                    Try
                        strError = oController.RatePphMasterDelete(oCustomClass)
                        If strError = "" Then

                            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        End If

                        DoBind(Me.SearchBy, Me.SortBy)
                    Catch Exp As Exception

                        ShowMessage(lblMessage, strError, True)
                    End Try
                End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgTarifPph_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgTarifPph.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
#End Region

#Region "Add New"
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            pnlDatagrid.Visible = False
            Me.Mode = "ADD"
            lblTitle.Text = "TAMBAH - RATE PPH"
            txtLevel.Text = ""
            txtFrom.Text = ""
            lblMessage.Text = ""
            txtTo.Text = ""
            txtRate_npwp.Text = ""
            txtRate_non_npwp.Text = ""
            Requiredfieldvalidator3.Enabled = True
        End If
    End Sub
#End Region

#Region "Process Save"
    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSaveAdd.Click

        Dim strError As String
        If Page.IsValid Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .Level = txtLevel.Text.Trim
                .AmntFrom = txtFrom.Text.Trim
                .AmntTo = txtTo.Text.Trim
                .RateNpwp = txtRate_npwp.Text
                .RateNonNpwp = txtRate_non_npwp.Text
            End With
            Select Case Mode
                Case "ADD"
                    strError = oController.RatePphMasterAdd(oCustomClass)
                    If strError <> "" Then
                        ShowMessage(lblMessage, strError, True)
                    Else
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                        pnlList.Visible = True
                        pnlDatagrid.Visible = False
                        pnlAdd.Visible = False
                        DoBind(Me.SearchBy, Me.SortBy)
                        'If Not (Me.Cache.Item(CACHE_BANK_MASTER) Is Nothing) Then
                        '    Me.Cache.Remove(CACHE_BANK_MASTER)
                        'End If
                    End If

                Case "EDIT"
                    strError = oController.RatePphMasterUpdate(oCustomClass)
                    If strError <> "" Then

                        ShowMessage(lblMessage, strError, True)
                    Else

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                        pnlList.Visible = True
                        pnlDatagrid.Visible = False
                        pnlAdd.Visible = False
                        DoBind(Me.SearchBy, Me.SortBy)
                        'If Not (Me.Cache.Item(CACHE_BANK_MASTER) Is Nothing) Then
                        '    Me.Cache.Remove(CACHE_BANK_MASTER)
                        'End If
                    End If
            End Select
        End If
    End Sub
#End Region

#Region "Process Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim & "%' "
        Else
            Me.SearchBy = ""
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reset Process"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel Process"
    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnCancelAdd.Click
        pnlAdd.Visible = False
        lblMessage.Text = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

End Class