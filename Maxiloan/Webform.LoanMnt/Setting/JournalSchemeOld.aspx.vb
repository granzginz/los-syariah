﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class JournalSchemeOld
    Inherits Maxiloan.Webform.WebBased

#Region "Private Const"
    Private m_controller As New JournalSchemeController
    Dim oJournalScheme As New Parameter.JournalScheme
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            Me.FormID = "SETJRNLSCHEME"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                txtgoPage.Text = "1"
                Me.Sort = "JournalSchemeID ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.CmdWhere = "ALL"
                BindGridEntity(Me.CmdWhere)
                If Request("cmd") = "dtl" Then
                    BindDetail(Request("id"))
                End If
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        lblMessage.Text = ""
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable

        InitialDefaultPanel()

        With oJournalScheme
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oJournalScheme = m_controller.GetJournalScheme(oJournalScheme)
        If Not oJournalScheme Is Nothing Then
            dtEntity = oJournalScheme.ListData
            recordCount = oJournalScheme.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.DataBind()
        PagingFooter()
        pnlSearch.Visible = True
        pnlAddEdit.Visible = False
        pnlList.Visible = True
    End Sub

    Sub BindDetail(ByVal id As String)
        Dim oData As New DataTable
        Dim intloopDtg As Integer
        pnlSearch.Visible = False
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        pnlCopyAdd.Visible = False

        lblActive.Visible = True
        chkActive.Visible = False
        lblJournalSchemeID.Visible = True
        txtJournalSchemeID.Visible = False
        lblDescription.Visible = True
        txtDescription.Visible = False

        lblTitleAddEdit.Text = "VIEW"
        imbSave.Visible = False
        imbCancel.Visible = False
        imbBack.Visible = True

        oJournalScheme.Id = id
        oJournalScheme.strConnection = GetConnectionString()

        oJournalScheme = m_controller.GetJournalSchemeEditHdr(oJournalScheme)
        txtJournalSchemeID.Text = oJournalScheme.Id.Trim
        lblJournalSchemeID.Text = oJournalScheme.Id.Trim
        lblDescription.Text = oJournalScheme.Description.Trim
        txtDescription.Text = oJournalScheme.Description.Trim
        If oJournalScheme.Status.Trim = "True" Then
            chkActive.Checked = True
            lblActive.Text = "Yes"
        Else
            chkActive.Checked = False
            lblActive.Text = "No"
        End If

        oJournalScheme = m_controller.GetJournalSchemeEditDtl(oJournalScheme)
        oData = oJournalScheme.ListData
        dtgAddEdit.DataSource = oData
        dtgAddEdit.DataBind()

        For intloopDtg = 0 To dtgAddEdit.Items.Count - 1
            CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("lblCOA"), Label).Visible = True
            CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("txtCOA"), TextBox).Visible = False
        Next
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        rgvGo.Enabled = True
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        lblMessage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                lblMessage.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm();")
        End If
    End Sub

    Sub DtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oData As New DataTable
        Dim intloopDtg As Integer
        lblMessage.Text = ""
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlSearch.Visible = False
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                pnlCopyAdd.Visible = False
                lblTitleAddEdit.Text = "EDIT - SKEMA JURNAL"

                lblJournalSchemeID.Visible = True
                txtJournalSchemeID.Visible = False
                imbBack.Visible = False
                imbSave.Visible = True
                imbCancel.Visible = True
                lblActive.Visible = False
                chkActive.Visible = True
                lblDescription.Visible = False
                txtDescription.Visible = True

                oJournalScheme.strConnection = GetConnectionString()
                oJournalScheme.Id = e.Item.Cells(2).Text

                oJournalScheme = m_controller.GetJournalSchemeEditHdr(oJournalScheme)
                txtJournalSchemeID.Text = oJournalScheme.Id.Trim
                lblJournalSchemeID.Text = oJournalScheme.Id.Trim
                txtDescription.Text = oJournalScheme.Description.Trim
                lblDescription.Text = oJournalScheme.Description.Trim
                If oJournalScheme.Status.Trim = "True" Then
                    chkActive.Checked = True
                Else
                    chkActive.Checked = False
                End If

                oJournalScheme = m_controller.GetJournalSchemeEditDtl(oJournalScheme)
                oData = oJournalScheme.ListData
                dtgAddEdit.DataSource = oData
                dtgAddEdit.DataBind()

                For intloopDtg = 0 To dtgAddEdit.Items.Count - 1
                    CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("lblCOA"), Label).Visible = False
                    CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("txtCOA"), TextBox).Visible = True
                Next
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                With oJournalScheme
                    .Id = e.Item.Cells(2).Text
                End With
                oJournalScheme.strConnection = GetConnectionString()
                m_controller.JournalSchemeDelete(oJournalScheme)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
                txtgoPage.Text = "1"
            End If
        End If
    End Sub

    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Dim oData As New DataTable
            Dim oDataCopyFrom As New DataTable
            Dim intloopDtg As Integer
            lblMessage.Text = ""
            Me.AddEdit = "ADD"
            pnlSearch.Visible = False
            pnlList.Visible = False
            pnlCopyAdd.Visible = True
            pnlAddEdit.Visible = True
            lblTitleAddEdit.Text = "TAMBAH - SKEMA JURNAL"

            lblActive.Visible = False
            lblJournalSchemeID.Visible = False
            txtJournalSchemeID.Visible = True
            imbBack.Visible = False
            imbSave.Visible = True
            imbCancel.Visible = True
            chkActive.Visible = True
            lblDescription.Visible = False
            txtDescription.Visible = True

            txtJournalSchemeID.Text = ""
            lblJournalSchemeID.Text = ""
            lblDescription.Text = ""
            txtDescription.Text = ""
            chkActive.Checked = True

            oJournalScheme.strConnection = GetConnectionString()
            oJournalScheme = m_controller.GetJournalSchemeCopyFrom(oJournalScheme)
            oDataCopyFrom = oJournalScheme.ListData
            cboCopy.DataSource = oDataCopyFrom
            cboCopy.DataTextField = "Description"
            cboCopy.DataValueField = "JournalSchemeID"
            cboCopy.DataBind()
            cboCopy.Items.Insert(0, "Select One")

            oJournalScheme.strConnection = GetConnectionString()
            oJournalScheme = m_controller.GetJournalSchemeAddDtl(oJournalScheme)
            oData = oJournalScheme.ListData
            dtgAddEdit.DataSource = oData
            dtgAddEdit.DataBind()

            For intloopDtg = 0 To dtgAddEdit.Items.Count - 1
                CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("lblCOA"), Label).Visible = False
                CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("txtCOA"), TextBox).Visible = True
            Next
        End If
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        lblMessage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        lblMessage.Text = ""
        If InStr(Me.Sort.ToString, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        Dim intLoop As Integer
        Dim COA As String
        Dim PaymentID As String
        Dim ErrMessage As String = ""
        lblMessage.Text = ""
        With oJournalScheme
            .Id = txtJournalSchemeID.Text
            .Description = txtDescription.Text
            If chkActive.Checked = True Then
                .Status = "1"
            Else
                .Status = "0"
            End If
            .strConnection = GetConnectionString()
        End With

        For intLoop = 0 To dtgAddEdit.Items.Count - 1
            COA = COA & CType(dtgAddEdit.Items(intLoop).Cells(2).FindControl("txtCOA"), TextBox).Text & ","
            PaymentID = PaymentID & dtgAddEdit.Items(intLoop).Cells(1).Text & ","
        Next
        With oJournalScheme
            .strConnection = GetConnectionString()
            .COA = COA
            .PaymentAllocationID = PaymentID
        End With

        If Me.AddEdit = "ADD" Then
            Try
                ErrMessage = m_controller.JournalSchemeSaveAdd(oJournalScheme)
                If ErrMessage <> "" Then

                    ShowMessage(lblMessage, ErrMessage, True)
                    Exit Sub
                Else

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                End If
                BindGridEntity(Me.CmdWhere)
            Catch exp As Exception

                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                Exit Sub
            End Try
        ElseIf Me.AddEdit = "EDIT" Then
            Try
                m_controller.JournalSchemeSaveEdit(oJournalScheme)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            Catch exp As Exception

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        cboSearch.SelectedIndex = 0
        lblMessage.Text = ""
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbCopy_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCopy.Click
        Dim oData As New DataTable
        Dim intloopDtg As Integer

        If cboCopy.SelectedIndex <> 0 Then
            lblMessage.Text = ""
            oJournalScheme.Id = cboCopy.SelectedItem.Value
            oJournalScheme.strConnection = GetConnectionString()
            oJournalScheme = m_controller.GetJournalSchemeEditDtl(oJournalScheme)
            oData = oJournalScheme.ListData
            dtgAddEdit.DataSource = oData
            dtgAddEdit.DataBind()

            For intloopDtg = 0 To dtgAddEdit.Items.Count - 1
                CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("lblCOA"), Label).Visible = False
                CType(dtgAddEdit.Items(intloopDtg).Cells(2).FindControl("txtCOA"), TextBox).Visible = True
            Next
        Else

            ShowMessage(lblMessage, "Please Select Other Journal Scheme", True)
        End If

    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        pnlSearch.Visible = True
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbBack.Click
        pnlSearch.Visible = True
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Text = ""
        BindGridEntity(Me.CmdWhere)
        BindGridEntity(Me.CmdWhere)
    End Sub

End Class