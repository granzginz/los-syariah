﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BankAccount.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.BankAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewContactPerson" Src="../../Webform.UserController/UcViewContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucViewBankAccount" Src="../../Webform.UserController/ucViewBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewAddress" Src="../../Webform.UserController/UcViewAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BankAccount</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            &nbsp;
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlDtGrid" runat="server" Visible="False">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR REKENING BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgBankAccount" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyField="BankAccountId" CssClass="grid_general"
                                OnSortCommand="SortGrid" CellPadding="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle  ></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                                CommandName="EDIT"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                                CommandName="DELETE"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DETAIL">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEditDetail" runat="server" CausesValidation="False" ImageUrl="../../Images/icondetail.gif" Visible='<%#Iif(container.dataitem("BankAccountType")="C", "False", "True")%>'
                                                CommandName="EDITDETAIL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BankAccountID" HeaderText="ID BANK">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBankAccountID" runat="server" Text='<%#Container.DataItem("BankAccountID")%>'
                                                CommandName="ShowView">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BankName" HeaderText="NAMA BANK">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankName" runat="server" Text='<%#Container.DataItem("BankName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="NAMA REKENING BANK">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DEFAULT" SortExpression="BankAccountDefault">                                
                                        <ItemTemplate>                                    
                                            <asp:Label id="hdncheckDefault" visible="false" name="hdncheckDefault" runat="server" Text ='<%# Container.DataItem("BankAccountDefault") %>'/>
                                            <asp:CheckBox ID="checkDefault" Enabled="false" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BankPurpose" HeaderText="PENGGUNAAN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankPurpose" runat="server" Text='<%#Container.DataItem("BankPurpose")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Iif(container.dataitem("Status")="True", "Active", "Not Active")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left"  
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            CARI REKENING BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
                <div class="form_button">
                    <asp:Button ID="imgsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:Label ID="lblBranchID" runat="server"></asp:Label>
                    </div>                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            ID Rekening Bank
                        </label>
                        <asp:TextBox ID="txtBankAccountID" runat="server"  MaxLength="10"></asp:TextBox>
                        <asp:Label ID="lblBankAccountID" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtBankAccountID"
                            ErrorMessage="Harap diisi ID Bank" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Nama Rekening Bank
                        </label>
                        <asp:TextBox ID="txtBankAccountName" runat="server" Width="284px" 
                            MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtBankAccountName"
                            ErrorMessage="Harap diisi Nama Rekening Bank" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Chart Of Account
                        </label>
                        <asp:TextBox ID="txtCoa" runat="server" Width="284px"  MaxLength="25"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtCoa"
                            ErrorMessage="Harap diisi Chart of Account" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jenis Rekening
                        </label>
                        <asp:DropDownList ID="cmbAccountType" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="cmbAccountType"
                            ErrorMessage="Harap dipilih Jenis Rekening" Display="Dynamic" InitialValue="0"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Penggunaan Rekening
                        </label>
                        <asp:DropDownList ID="cmbPurpose" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="cmbPurpose"
                            ErrorMessage="Harap dipilih Penggunaan Rekening" Display="Dynamic" InitialValue="0"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Bisa Generate Text
                        </label>
                        <asp:CheckBox ID="chkIsGenerate" runat="server"></asp:CheckBox>Yes
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Aktif
                        </label>
                        <asp:CheckBox ID="chkIsActive" runat="server"></asp:CheckBox>Yes
                    </div>                    
                </div>
                <div class="form_box">
	            <div class="form_left">
                    <label>Default</label>
                    <asp:CheckBox ID="chkBankAccountDefault" runat="server" />                    
	            </div>
                <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
	            <div class="form_left">
                    <label>Sweep Otomatis</label>
                    <asp:CheckBox ID="chkSweepOtomatis" runat="server" />                    
	            </div>
                <div class="form_right">
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>&nbsp;
                    <asp:Button ID="btnCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlView" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            VIEW - REKENING BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:Label ID="lblViewBranch" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            ID Rekening Bank
                        </label>
                        <asp:Label ID="lblViewBankAccountID" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama Rekening Bank
                        </label>
                        <asp:Label ID="lblViewBankAccountName" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Chart Of Account
                        </label>
                        <asp:Label ID="lblCoa" runat="server"></asp:Label>
                    </div>                   
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jenis Rekening
                        </label>
                        <asp:Label ID="lblBankAccountType" runat="server"></asp:Label>
                    </div>                   
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Penggunaan Rekening
                        </label>
                        <asp:Label ID="lblAccountPurpose" runat="server"></asp:Label>
                    </div>                   
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            REGISTRASI BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucviewbankaccount id="oViewBankAccount" runat="server"></uc1:ucviewbankaccount>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tarif SKN
                        </label>
                        <asp:Label ID="lblTarifSKN" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tarif RTGS
                        </label>
                        <asp:Label ID="lblTarifRTGS" runat="server"></asp:Label>
                    </div>                   
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALAMAT BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucviewaddress id="oViewAddress" runat="server"></uc1:ucviewaddress>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            KONTAK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucviewcontactperson id="oViewContactPerson" runat="server"></uc1:ucviewcontactperson>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            STATUS
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Bisa Generate Text
                        </label>
                        <asp:Label ID="lblIsGenerateText" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Aktif
                        </label>
                        <asp:Label ID="lblIsActive" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Default
                        </label>
                        <asp:Label ID="lblDefault" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_button">
                    <asp:Button ID="imbBack" runat="server" Text="Back" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlEditDetail" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            EDIT - REKENING BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:Label ID="lblEditBranch" runat="server"></asp:Label>
                    </div>                   
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            ID Rekening Bank
                        </label>
                        <asp:Label ID="lblEditBankAccountID" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama Rekening Bank
                        </label>
                        <asp:Label ID="lblEditBankAccountName" runat="server"></asp:Label>
                    </div>                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Chart Of Account
                        </label>
                        <asp:Label ID="lblEditCoa" runat="server"></asp:Label>
                    </div>                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jenis Rekening
                        </label>
                        <asp:Label ID="lblEditAccountType" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Penggunaan Rekening
                        </label>
                        <asp:Label ID="lblEditAccountPurpose" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            REGISTRASI BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucbankaccount id="oBankAccount" runat="server"></uc1:ucbankaccount>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Tarif SKN
                        </label>
                        <uc1:ucNumberFormat ID="txtTarifSKN" runat="server" />
                    </div>                   
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Tarif RTGS
                        </label>
                        <uc1:ucNumberFormat ID="txtTarifRTGS" runat="server" />                        
                    </div>                   
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALAMAT BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccompanyaddress id="oCompanyAddress" runat="server"></uc1:uccompanyaddress>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            KONTAK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccontactperson id="oContactPerson" runat="server"></uc1:uccontactperson>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            STATUS
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Bisa Generate Text
                        </label>
                        <asp:Label ID="lblEditIsGenerate" runat="server"></asp:Label>
                    </div>                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Aktif
                        </label>
                        <asp:Label ID="lblEditActive" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Default
                        </label>
                        <asp:Label ID="lblEditDefault" runat="server"></asp:Label>
                    </div>                    
                </div>
                <div class="form_button">
                    <asp:Button ID="imbSaveEditDetail" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>&nbsp;
                    <asp:Button ID="imbCancelEditDetail" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
