﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JournalSchemeOld.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.JournalSchemeOld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>JournalScheme</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <!--<SCRIPT src="../../Maxiloan.js"></SCRIPT>-->
    <script language="JavaScript" type="text/javascript">
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        function fback() {
            history.go(-1);
            return false;

        }	
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL LIST DATA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR SKEMA JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" CssClass="grid_general" OnSortCommand="Sorting"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                                     ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="JournalSchemeID" HeaderText="ID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="JournalSchemeID" HeaderText="ID">
                                <ItemTemplate>
                                    <a href='JournalScheme.aspx?cmd=dtl&id=<%# DataBinder.Eval(Container,"DataItem.JournalSchemeID")%>'>
                                        <asp:Label ID="lblJournalID" Text='<%# DataBinder.Eval(Container,"DataItem.JournalSchemeID")%>'
                                            runat="server">
                                        </asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA SKEMA JURNAL">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsActive" SortExpression="IsActive" HeaderText="STATUS">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                       
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
         <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button blue"
                            CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL SEARCH>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI SKEMA JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="JournalSchemeID">ID Skema Jurnal</asp:ListItem>
                    <asp:ListItem Value="Description">Nama Skema Jurnal</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtSearch" runat="server"  MaxLength="100"></asp:TextBox>
            </div>
             
        </div>
        <div class="form_button">
            <asp:Button ID="imbSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL ADDEDIT DATA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
        <div class="title_strip"></div>
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    ID Skema Jurnal
                </label>
                <asp:Label ID="lblJournalSchemeID" runat="server"></asp:Label>
                <asp:TextBox ID="txtJournalSchemeID" runat="server"  MaxLength="10"
                    Columns="13"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap Pilih ID Skema Jurnal"
                    ControlToValidate="txtJournalSchemeID" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Skema Jurnal
                </label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                <asp:TextBox ID="txtDescription" runat="server"    MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap diisi Nama Skema Jurnal"
                    ControlToValidate="txtDescription" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
             
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Aktif
                </label>
                <asp:CheckBox ID="chkActive" runat="server" Text="Yes"></asp:CheckBox>
                <asp:Label ID="lblActive" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                DETAIL SKEMA JURNAL
            </div>
        </div>
        <asp:Panel ID="pnlCopyAdd" runat="server">
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Copy Dari Skema Lain
                    </label>
                    <asp:DropDownList ID="cboCopy" runat="server" >
                    </asp:DropDownList>
                    <asp:Button ID="imbCopy" runat="server" Text="Copy" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                </div>
                <div class="form_right">
                </div>
            </div>
        </asp:Panel>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgAddEdit" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                        AllowSorting="false">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="NO" HeaderText="NO"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PaymentAllocationID" Visible="False" HeaderText="PAYMENT ALLOCATION">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="ALOKASI PEMBAYARAN"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="CHART OF ACCOUNT">
                                <ItemTemplate>
                                    <asp:Label ID="lblCOA" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.COA") %>'>
                                    </asp:Label>
                                    <asp:TextBox ID="txtCOA" runat="server"  MaxLength="25" Columns="27"
                                        Text='<%# DataBinder.eval(Container,"DataItem.COA") %>'>
                                    </asp:TextBox>&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap diisi Chart of Account"
                                        ControlToValidate="txtCOA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="false"></asp:Button>&nbsp;
            <asp:Button ID="imbBack" runat="server" Text="Back" CssClass="small button gray"
                CausesValidation="false"></asp:Button>
        </div>
    </asp:Panel>
    <input id="htmlHiddenSortExpression" type="hidden" name="htmlHiddenSortExpression"
        runat="server" />
    </form>
</body>
</html>
