﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class AgreementMasterTransaction
    Inherits Maxiloan.Webform.WebBased
    Private m_Bank As New DataUserControlController
    Private m_controller As New AgreementMasterTransactionController
    Protected WithEvents ucLookupPaymentAllocationSupplier As ucLookupPaymentAllocation
    Protected WithEvents ucLookupPaymentAllocationCustomer As ucLookupPaymentAllocation
    Protected WithEvents txtNoUrutTrans As ucNumberFormat

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return CType(ViewState("SupplierGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierGroupID") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CType(ViewState("BankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            Me.FormID = "AGRMASTRANS"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtGoPage.Text = "1"
                Me.Sort = "TransID ASC"
                Me.CmdWhere = "All"
                BindGridEntity(Me.CmdWhere)
                lblMessage.Visible = False
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oAgreementMasterTransaction As New Parameter.AgreementMasterTransaction
        InitialDefaultPanel()
        oAgreementMasterTransaction.strConnection = GetConnectionString()
        oAgreementMasterTransaction.WhereCond = cmdWhere
        oAgreementMasterTransaction.CurrentPage = currentPage
        oAgreementMasterTransaction.PageSize = pageSize
        oAgreementMasterTransaction.SortBy = Me.Sort
        oAgreementMasterTransaction = m_controller.GetAgreementMasterTransaction(oAgreementMasterTransaction)

        If Not oAgreementMasterTransaction Is Nothing Then
            dtEntity = oAgreementMasterTransaction.Listdata
            recordCount = oAgreementMasterTransaction.Totalrecords
        Else
            recordCount = 0
        End If
        
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

            If e.Item.Cells(4).Text.ToUpper.Trim = "TRUE" Then
                e.Item.Cells(4).Text = "AKTIF"
            ElseIf e.Item.Cells(4).Text.ToUpper.Trim = "FALSE" Then
                e.Item.Cells(4).Text = "NON AKTIF"
            End If
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oAgreementMasterTransaction As New Parameter.AgreementMasterTransaction
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            imbBack.Visible = False
            imbCancel.Visible = True
            imbSave.Visible = True
            imbClose.Visible = False
            lblMessage.Visible = False
            FillCbo()

            lblTitleAddEdit.Text = Me.AddEdit
            oAgreementMasterTransaction.TransID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oAgreementMasterTransaction.strConnection = GetConnectionString()
            oAgreementMasterTransaction = m_controller.GetAgreementMasterTransactionList(oAgreementMasterTransaction)
            imbCancel.CausesValidation = False

            hdnTransID.Value = oAgreementMasterTransaction.TransID.Trim
            txtTransID.Text = oAgreementMasterTransaction.TransID.Trim
            txtTransDesc.Text = oAgreementMasterTransaction.TransDesc
            chekAktif.Checked = oAgreementMasterTransaction.Isactive
            txtNoUrutTrans.Text = oAgreementMasterTransaction.NoUrutTrans
            isDistributable.Checked = oAgreementMasterTransaction.isDistribuTable

            cboBungaNet.SelectedValue = IIf(oAgreementMasterTransaction.BungaNet = "", "None", oAgreementMasterTransaction.BungaNet).ToString

            ucLookupPaymentAllocationSupplier.BindLookup(oAgreementMasterTransaction.PaymentAllocationIDSupplier.Trim)
            hdnPaymentAllocationSupplier.Value = oAgreementMasterTransaction.PaymentAllocationIDSupplier.Trim
            txtPaymentAllocationSupplier.Text = ucLookupPaymentAllocationSupplier.PaymentAllocationDescription

            cboFlagSupplier.SelectedIndex = cboFlagSupplier.Items.IndexOf(cboFlagSupplier.Items.FindByValue(oAgreementMasterTransaction.defaultValueTagihanKeSupplier.ToString))
            chkReadOnly.Checked = oAgreementMasterTransaction.isReadOnly

            txtGroupTransaksi.Text = oAgreementMasterTransaction.GroupTransID.Trim
            ddlEmployeeSeupplier.SelectedIndex = ddlEmployeeSeupplier.Items.IndexOf(ddlEmployeeSeupplier.Items.FindByValue(oAgreementMasterTransaction.SupplierEmployeePosition.Trim))
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.AgreementMasterTransaction
            With customClass
                .TransID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.AgreementMasterTransactionDelete(customClass)
            If err <> "" Then

                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        Dim customClass As New Parameter.AgreementMasterTransaction
        Dim ErrMessage As String = ""

        With customClass
            .BungaNet = cboBungaNet.SelectedValue.Trim
            .NoUrutTrans = txtNoUrutTrans.Text.Trim
            .TransIDEdit = txtTransID.Text
            .TransDesc = txtTransDesc.Text
            .Isactive = chekAktif.Checked
            .PaymentAllocationIDSupplier = hdnPaymentAllocationSupplier.Value
            .isDistribuTable = IIf(isDistributable.Checked = True, True, False)
            .defaultValueTagihanKeSupplier = cboFlagSupplier.SelectedValue.ToString.Trim
            .isReadOnly = chkReadOnly.Checked
            .GroupTransID = txtGroupTransaksi.Text.Trim
            .SupplierEmployeePosition = ddlEmployeeSeupplier.SelectedValue.Trim
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.AgreementMasterTransactionSaveAdd(customClass)
            If ErrMessage <> "" Then

                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.TransID = hdnTransID.Value
            m_controller.AgreementMasterTransactionSaveEdit(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        imbBack.Visible = False
        imbCancel.Visible = True
        imbSave.Visible = True
        imbClose.Visible = False
        lblMessage.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        imbCancel.CausesValidation = False

        hdnTransID.Value = Nothing
        txtTransID.Text = Nothing
        txtTransDesc.Text = Nothing
        chekAktif.Checked = False
        isDistributable.Checked = False
        txtPaymentAllocationSupplier.Text = Nothing
        txtNoUrutTrans.Text = 0
        FillCbo()
    End Sub
    
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("AgreementMasterTransaction")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AgreementMasterTransaction")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "All"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbSearch.Click
        Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + txtSearch.Text.Replace("%", "").Trim + "%'"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbBackMenu_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbBackMenu.Click
        Response.Redirect("AgreementMasterTransaction.aspx")
    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbBack.Click
        Response.Redirect("AgreementMasterTransaction.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("AgreementMasterTransaction.aspx")
    End Sub

    Protected Sub btnLookupPaymentAllocationSupplier_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupPaymentAllocationSupplier.Click
        ucLookupPaymentAllocationSupplier.CmdWhere = "All"
        ucLookupPaymentAllocationSupplier.Sort = "PaymentAllocationID ASC"
        ucLookupPaymentAllocationSupplier.Popup()
    End Sub
    Public Sub CatSelectedPaymentAllocationSupplier(ByVal PaymentAllocationID As String,
                                           ByVal PaymentAllocationDescription As String,
                                           ByVal COA As String)
        txtPaymentAllocationSupplier.Text = PaymentAllocationDescription
        hdnPaymentAllocationSupplier.Value = PaymentAllocationID
    End Sub
    Sub FillCbo()
        Dim dtEntity As New DataTable        
        Dim m_Insentif As New RefundInsentifController
        Dim oCustom As New Parameter.RefundInsentif
        Dim tarif As String = ""

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .SPName = "spGetTblEmployeeSupplier"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        dtEntity = oCustom.ListDataTable

        ddlEmployeeSeupplier.DataSource = dtEntity.DefaultView
        ddlEmployeeSeupplier.DataTextField = "Description"
        ddlEmployeeSeupplier.DataValueField = "ID"
        ddlEmployeeSeupplier.DataBind()
        ddlEmployeeSeupplier.Items.Insert(0, "Select One")
        ddlEmployeeSeupplier.Items(0).Value = "Select One"
    End Sub
End Class