﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentAllocation.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentAllocation" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register Src="../../webform.UserController/ucGLMasterAcc.ascx" TagName="ucGLMasterAcc"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentAllocation</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;

        }
        function click() {
            if (event.button == 2) {
                alert('Anda Tidak Berhak');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
     <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server" HorizontalAlign="Center">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR ALOKASI PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgEntity" runat="server" HorizontalAlign="Center" CssClass="grid_general"
                        AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="DELETE" Visible='<%#IsVisible(container.dataitem("IsSystem"))%>'>
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaymentAllocationID" HeaderText="ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="hypPaymentAllocationId" runat="server" Text='<%#Container.DataItem("PaymentAllocationId")%>'
                                        CommandName="SHOWVIEW">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA ALOKASI PEMBAYARAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsSystem" SortExpression="IsSystem" HeaderText="SISTEM">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="COA" SortExpression="COA" HeaderText="CHART OF ACCOUNT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsActive" SortExpression="IsActive" HeaderText="STATUS">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsScheme" HeaderText="IsScheme"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsAgreement" HeaderText="IsAgreement">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsPettyCash" HeaderText="IsPettyCash">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsHOTransaction" HeaderText="IsHO"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsPaymentReceive" HeaderText="IsPaymentReceive">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="TRUE" DataField="IsPaymentRequest" HeaderText="PAYMENT REQUEST">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI ALOKASI PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server" />
        </div>
        <div class="form_button">
            <asp:Button ID="imbSearch" runat="server" CausesValidation="False" Text="Search"
                CssClass="small button blue" />
            &nbsp;
            <asp:Button ID="imbReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <!-- ********Panel Add/Edit ******** -->
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    ALOKASI PEMBAYARAN -&nbsp;
                    <asp:Label ID="lblAddEdt" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    ID Alokasi Pembayaran
                </label>
                <asp:Label ID="lblPaymentAllocationId" runat="server"></asp:Label>
                <asp:TextBox ID="txtPaymentAllocationId" runat="server"  MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPaymentAllocationId" runat="server" ErrorMessage="Harap diisi ID Alokasi Pembayaran"
                    ControlToValidate="txtPaymentAllocationId" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Alokasi Pembayaran
                </label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                <asp:TextBox ID="txtDescription" runat="server"  Width="330px"
                    MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ErrorMessage="Harap diisi Nama Alokasi Pembayaran"
                    ControlToValidate="txtDescription" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate> 
                <label class="label_req">
                    Chart of Account
                </label>
                <asp:Label ID="lblCOA" runat="server"></asp:Label>
                <asp:TextBox ID="txtCOA" Enabled="false" runat="server"  MaxLength="25"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCOA" runat="server" ErrorMessage="Harap diisi No Chart Of Account"
                    ControlToValidate="txtCOA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Button ID="btnGLMasterAcc" runat="server" CausesValidation="False" Text="..."
                            CssClass="small buttongo blue" />
                <uc2:ucGLMasterAcc id="ucGLMasterAcc1" runat="server" oncatselected="CatSelectedGLMasterAcc"></uc2:ucGLMasterAcc> 
             </ContentTemplate>
                </asp:UpdatePanel>
                         </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di SISTEM
                </label>
                <asp:CheckBox ID="chkSystem" runat="server" Enabled="False"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di SKEMA JURNAL
                </label>
                <asp:CheckBox ID="chkScheme" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di KONTRAK
                </label>
                <asp:CheckBox ID="chkAgreement" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di PEMBAYARAN
                </label>
                <asp:CheckBox ID="chkPaymentReceive" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di PETTY CASH
                </label>
                <asp:CheckBox ID="chkPettyCash" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di Payment Request
                </label>
                <asp:CheckBox ID="chkPaymentRequest" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dipakai di KANTOR PUSAT
                </label>
                <asp:CheckBox ID="chkHeadOffice" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Aktif
                </label>
                <asp:CheckBox ID="chkStatus" runat="server"></asp:CheckBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imgBack" runat="server" Text="Back" CssClass="small button gray"
                CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="imgCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <input id="htmlHiddenSortExpression" type="hidden" name="htmlHiddenSortExpression"
            runat="server" />
    </asp:Panel>
    </form>
</body>
</html>
