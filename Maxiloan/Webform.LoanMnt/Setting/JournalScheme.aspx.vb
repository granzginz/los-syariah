﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions

Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Web.Script.Services

#End Region

Public Class JournalScheme
    Inherits Maxiloan.Webform.WebBased


#Region "Private Const"
    Private m_controller As New JournalSchemeController
    Dim oJournalScheme As New Parameter.JournalScheme
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            Me.FormID = "SETJRNLSCHEME"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                txtGoPage.Text = "1"
                Me.Sort = "JournalSchemeID ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.CmdWhere = "ALL"
                BindGridEntity(Me.CmdWhere)
                If Request("cmd") = "dtl" Then
                    BindDetail(Request("id"))
                End If
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        lblMessage.Text = ""
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable

        InitialDefaultPanel()

        With oJournalScheme
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oJournalScheme = m_controller.GetJournalScheme(oJournalScheme)
        If Not oJournalScheme Is Nothing Then
            dtEntity = oJournalScheme.ListData
            recordCount = oJournalScheme.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.DataBind()
        PagingFooter()
        pnlSearch.Visible = True
        pnlAddEdit.Visible = False
        pnlList.Visible = True
    End Sub

    Sub BindDetail(ByVal id As String)
        Dim oData As New DataTable
        Dim intloopDtg As Integer
        pnlSearch.Visible = False
        pnlList.Visible = False
        pnlAddEdit.Visible = True


        lblTitleAddEdit.Text = "VIEW"

        oJournalScheme.Id = id
        oJournalScheme.strConnection = GetConnectionString()

        oJournalScheme = m_controller.GetJournalSchemeEditHdr(oJournalScheme)





    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        rgvGo.Enabled = True
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        lblMessage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                lblMessage.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm();")
        End If
    End Sub

    Sub DtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oData As New DataTable
        Dim intloopDtg As Integer
        lblMessage.Text = ""
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlSearch.Visible = False
                pnlList.Visible = False
                pnlAddEdit.Visible = True

                lblTitleAddEdit.Text = "EDIT - SKEMA JURNAL"



                oJournalScheme.strConnection = GetConnectionString()
                oJournalScheme.Id = e.Item.Cells(2).Text

                oJournalScheme = m_controller.GetJournalSchemeEditHdr(oJournalScheme)


            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                With oJournalScheme
                    .Id = e.Item.Cells(2).Text
                End With
                oJournalScheme.strConnection = GetConnectionString()
                m_controller.JournalSchemeDelete(oJournalScheme)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
                txtGoPage.Text = "1"
            End If
        End If
    End Sub

    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Dim oData As New DataTable
            Dim oDataCopyFrom As New DataTable
            Dim intloopDtg As Integer
            lblMessage.Text = ""
            Me.AddEdit = "ADD"
            pnlSearch.Visible = False
            pnlList.Visible = False

            pnlAddEdit.Visible = True
            lblTitleAddEdit.Text = "TAMBAH - SKEMA JURNAL"
            'BindGlMasterSequenceDropdown(ddlJenisTransaksi, GetConnectionString, sesBranchId.Replace("'", ""))





        End If
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        lblMessage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        lblMessage.Text = ""
        If InStr(Me.Sort.ToString, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        cboSearch.SelectedIndex = 0
        lblMessage.Text = ""
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub


End Class