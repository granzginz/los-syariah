﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JournalScheme.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.JournalScheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>JournalScheme</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <!--<SCRIPT src="../../Maxiloan.js"></SCRIPT>-->
    <script language="JavaScript" type="text/javascript">
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        function fback() {
            history.go(-1);
            return false;

        }	
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL LIST DATA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR SKEMA JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" CssClass="grid_general" OnSortCommand="Sorting"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                                     ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="JournalSchemeID" HeaderText="ID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="JournalSchemeID" HeaderText="ID">
                                <ItemTemplate>
                                    <a href='JournalScheme.aspx?cmd=dtl&id=<%# DataBinder.Eval(Container,"DataItem.JournalSchemeID")%>'>
                                        <asp:Label ID="lblJournalID" Text='<%# DataBinder.Eval(Container, "DataItem.JournalSchemeID")%>'
                                            runat="server">
                                        </asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA SKEMA JURNAL">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsActive" SortExpression="IsActive" HeaderText="STATUS">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                       
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
         <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button blue"
                            CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL SEARCH>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI SKEMA JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="JournalSchemeID">ID Skema Jurnal</asp:ListItem>
                    <asp:ListItem Value="Description">Nama Skema Jurnal</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtSearch" runat="server"  MaxLength="100"></asp:TextBox>
            </div>
             
        </div>
        <div class="form_button">
            <asp:Button ID="imbSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL ADDEDIT DATA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
        <div class="title_strip"></div>
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
                <div class="form_left">
                    <label> Transaksi</label>
                    <asp:DropDownList ID="ddlJenisTransaksi" runat="server"></asp:DropDownList>
                    
                </div>
                <div class="form_right">
                    <label class="label_req">No. Reference</label>
                    <asp:TextBox runat="server" ID="txtNoReference"></asp:TextBox>
                </div>
            </div>
         <div class="form_box">
                        <div class="form_left">
                            <label>COA</label>
                            <asp:TextBox ID="txtNoAccountx" runat="server" class="clsNoAccountx" autocomplete="off" CssClass="medium_text" onchange="show()" onkeypress="javascript:return false;"></asp:TextBox>
                            <button class="small buttongo blue" 
                            onclick ="OpenLookup();return false;">...</button >                        
                            <asp:Button runat="server" ID="Jlookup" style="display:none" />
                        </div>
                        <div class="form_right">
                            <label>Nama COA</label>
                            <asp:TextBox ID="txtAccountName" runat="server" class="clsAccountNam" autocomplete="off" CssClass="medium_text" onchange="show()" onkeypress="javascript:return false;"></asp:TextBox>
                        </div>
                    </div>
  
        <div class="form_box">
                        <div class="form_left">
                            <label>Jenis Transaksi</label>
                        <asp:DropDownList  ID="rblDK1" runat="server" style='width: 131px;' >
                            <asp:ListItem Text="Debit" Value="D" Selected="True" />
                            <asp:ListItem Text="Kredit" Value="C" />
                        </asp:DropDownList>
                        </div>
                        <div class="form_right">
                        </div>
                  </div>
     <div class="form_title">
                <div class="form_single">
                    <h3> DETAIL TRANSAKSI </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False"
                            DataKeyField="ItemKey" CssClass="grid_general" ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnPers" />
                                        <asp:HiddenField runat="server" ID="hdnCab" />
                                        <asp:HiddenField runat="server" ID="hdnCoA" />
                                        <asp:HiddenField runat = "server" ID="hdnSequence" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../Images/iconedit.gif" CommandName="Update_" OnClientClick="return confirmUpdate_()" 
                                            OnCommand = "CommandGrid_Click" CausesValidation="False" CommandArgument='<%#Eval("ItemKey") %>'/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../Images/icondelete.gif" CommandName="Delete"  OnClientClick="return confirmDelete()"
                                        OnCommand = "CommandGrid_Click" CausesValidation="False" CommandArgument='<%#Eval("ItemKey") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Copy">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnCopy" runat="server" ImageUrl="../Images/IconDocument.gif" CommandName="Copy" OnClientClick="return confirmCopy()" onchange="show()"
                                            OnCommand = "CommandGrid_Click" CausesValidation="true" CommandArgument='<%#Eval("ItemKey") %>'/>
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="COA">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPerCabCoA" Text='<%#Eval("CoaId") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>                     
                                        <asp:TextBox id="txtCOA" runat="server" Width="50px" class="clsNoCOA" autocomplete="off" CssClass="medium_text" text='<%# DataBinder.Eval(Container.DataItem, "CoaId") %>' />
                                        <button class="small buttongo blue" 
                                        onclick ="OpenLookup_(i);return false;">...</button >                        
                                        <asp:Button runat="server" ID="btnlookup" style="display:none" />
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Nama COA">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPerCabCoAName" Text='<%#Eval("CoaName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox id="txtNameCoa" runat="server" Width="150px" class="clsNoCOA" autocomplete="off" CssClass="medium_text" text='<%# DataBinder.Eval(Container.DataItem, "CoaName") %>' />
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KETERANGAN">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblKeterangan" Text='<%#Eval("TransactionDesc") %>' />
                                    </ItemTemplate>
                                     <EditItemTemplate>
                                         <asp:TextBox id="txtTransactionDesc" runat="server"   CssClass="multiline_textbox" TextMode="MultiLine" text='<%# DataBinder.Eval(Container.DataItem, "TransactionDesc") %>' />
                                    </EditItemTemplate>
                                     <FooterTemplate>
                                        Selisih : &nbsp; <asp:Label ID="lblSelisih" runat="server"/>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D/C">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblDC" Text='<%#Eval("Post") %>'/>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                         <asp:DropDownList  ID="DropDownList1" runat="server" onchange='DC();' style='width: 45px;' text='<%# DataBinder.Eval(Container.DataItem, "Post") %>'>
                                            <asp:ListItem Text="D" Value="D" Selected="True" />
                                            <asp:ListItem Text="C" Value="C" />
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DEBIT" HeaderStyle-CssClass ="th_right" ItemStyle-CssClass="th_right" FooterStyle-CssClass  ="th_right">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblDebit" Text='<%#FormatNumber(IIf(Eval("Post") = "C", 0, Eval("Amount")), 0) %>' />
                                    </ItemTemplate>
                                     <EditItemTemplate> 
                                        <%--<uc2:ucnumberformat id="txtJumlahDebit" runat="server" style="width: 100px;" text='<%# formatnumber(iif(eval("Post")="C",0,eval("Amount")),0) %>'></uc2:ucnumberformat> --%>
                                            <asp:TextBox runat="server" ID="txtJumlahDebit" Width="80px" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign reguler_text"
                                            text='<%#FormatNumber(IIf(Eval("Post") = "C", 0, Eval("Amount")), 0) %>' ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalDebit" runat="server" /> 
                                    </FooterTemplate>
                                    <FooterStyle CssClass="th_right" />
                                    <HeaderStyle CssClass="th_right" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CREDIT" HeaderStyle-CssClass ="th_right" ItemStyle-CssClass="th_right" FooterStyle-CssClass  ="th_right">
                                    <ItemTemplate> 
                                        <asp:Label runat="server" ID="lblCredit"  Text='<%#FormatNumber(IIf(Eval("Post") = "D", 0, Eval("Amount")), 0) %>'/> 
                                    </ItemTemplate>
                                    <EditItemTemplate> 
                                        <asp:TextBox runat="server" ID="txtJumlahCredit" Width="80px" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign reguler_text"
                                            text='<%#FormatNumber(IIf(Eval("Post") = "D", 0, Eval("Amount")), 0) %>' ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalCredit" runat="server" /> 
                                    </FooterTemplate>
                                    <FooterStyle CssClass="th_right" />
                                    <HeaderStyle CssClass="th_right" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>


                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="true" Text="Add" CssClass="small button blue" ValidationGroup="header" />
                    <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" ValidationGroup="header" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/>
                    <asp:Button ID="btnDeleteEntry" runat="server" CausesValidation="true" Text="Delete" CssClass="small button red" ValidationGroup="header" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
                </div>
      
    </asp:Panel>




    <input id="htmlHiddenSortExpression" type="hidden" name="htmlHiddenSortExpression"
        runat="server" />
    </form>
</body>
</html>
