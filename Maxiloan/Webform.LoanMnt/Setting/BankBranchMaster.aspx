﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BankBranchMaster.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.BankBranchMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DAFTAR NAMA CABANG BANK</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR NAMA CABANG BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="BankBranchid" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BankCode" SortExpression="BankCode" HeaderText="SANDI BANK">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankName" SortExpression="BankName" HeaderText="NAMA LENGKAP">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ShortName" SortExpression="ShortName" HeaderText="NAMA SINGKAT">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankBranchName" SortExpression="BankBranchName" HeaderText="NAMA CABANG">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="City" SortExpression="City" HeaderText="City"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtgoPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtgoPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtgoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI NAMA CABANG BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Bank
                        </label>
                        <asp:DropDownList ID="cboNamaBank" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="BankBranchName">NAMA CABANG</asp:ListItem>
                            <asp:ListItem Value="BankCode">KODE BANK</asp:ListItem>
                            <asp:ListItem Value="Address">ALAMAT</asp:ListItem>
                            <asp:ListItem Value="City">KOTA</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"  MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="imbReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                    <asp:Button ID="imbBackMenu" runat="server" CausesValidation="False" Visible="false"
                        Text="Back" CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            MENAMBAHKAN CABANG BANK -&nbsp;
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        REKENING BANK *)
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Lengkap Bank
                        </label>
                        <asp:DropDownList ID="cboNamaLengkapBank" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Sandi Bank
                        </label>
                        <asp:TextBox ID="txtSandiBank" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtSandiBank" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Nama Cabang
                        </label>
                        <asp:TextBox ID="txtNamaCabang" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNamaCabang" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Alamat Cabang
                        </label>
                        <asp:TextBox ID="txtAlamatCabang" runat="server" Width="350px" MaxLength="100" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="txtAlamatCabang" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            City
                        </label>
                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtCity" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Aktif
                        </label>
                        <asp:CheckBox ID="checkAktif" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button red">
                    </asp:Button>
                </div>
            </asp:Panel>
            <input id="hdnBankBranchID" type="hidden" name="hdnBankBranchID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
