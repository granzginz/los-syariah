﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PaymentAllocation
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "uc control declaration"
    Protected WithEvents ucGLMasterAcc1 As ucGLMasterAcc
#End Region

#Region "PrivateConst"
    Private oCustomClass As New Parameter.PaymentAllocation
    Private m_controller As New PaymentAllocationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim strSortBy As String
#End Region


#Region "Property"

    Private Property AddEdt() As String
        Get
            Return (CType(ViewState("AddEdt"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdt") = Value
        End Set
    End Property

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.SearchBy = ""
            Me.SortBy = ""
            BindGridEntity(Me.SearchBy, Me.SortBy)
            txtGoPage.Text = "1"
            oSearchBy.ListData = "DESCRIPTION,Nama-PAYMENTALLOCATIONID,ID-COA,Chart Of Account"
            oSearchBy.BindData()
            Me.FormID = "SETPAYALLOCATION"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlAddEdit.Visible = False
        pnlList.Visible = True
        lblMessage.Text = ""
        pnlSearch.Visible = True
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String, ByVal strSortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = m_controller.GetPaymentAllocation(oCustomClass)

        dtsEntity = oCustomClass.ListPaymentAllocation
        dtvEntity = dtsEntity.DefaultView
        recordCount = oCustomClass.TotalRecord
        dtvEntity.Sort = Me.SortBy
        dtgEntity.DataSource = dtvEntity
        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try

        imbSave.Visible = False
        imgBack.Visible = False
        imgCancel.Visible = False
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False

        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan  .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lbltotrec.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Data Grid Command"
    Private Sub dtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                    lblMessage.Text = ""
                    Me.AddEdt = "EDIT"
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    pnlSearch.Visible = False
                    rfvPaymentAllocationId.Enabled = False
                    lblAddEdt.Text = "EDIT"
                    chkSystem.Enabled = False
                    chkScheme.Enabled = True
                    chkAgreement.Enabled = True
                    chkPaymentReceive.Enabled = True
                    chkPettyCash.Enabled = True
                    chkHeadOffice.Enabled = True
                    chkStatus.Enabled = True
                    txtPaymentAllocationId.Visible = False
                    lblPaymentAllocationId.Visible = True
                    lblDescription.Visible = False
                    lblCOA.Visible = False
                    txtDescription.Visible = True
                    lblDescription.Visible = False
                    txtCOA.Visible = True
                    lblCOA.Visible = False
                    Dim hypID As LinkButton
                    hypID = CType(dtgEntity.Items(e.Item.ItemIndex).FindControl("hypPaymentAllocationId"), LinkButton)

                    lblPaymentAllocationId.Text = hypID.Text.Trim
                    txtDescription.Text = e.Item.Cells(3).Text.Trim
                    txtCOA.Text = e.Item.Cells(5).Text.Trim
                    chkSystem.Checked = CType(e.Item.Cells(4).Text, Boolean)
                    chkScheme.Checked = CType(e.Item.Cells(7).Text, Boolean)
                    chkAgreement.Checked = CType(e.Item.Cells(8).Text, Boolean)
                    chkPaymentReceive.Checked = CType(e.Item.Cells(11).Text, Boolean)
                    chkPaymentRequest.Checked = CType(e.Item.Cells(12).Text, Boolean)
                    chkPettyCash.Checked = CType(e.Item.Cells(9).Text, Boolean)
                    chkHeadOffice.Checked = CType(e.Item.Cells(10).Text, Boolean)
                    chkStatus.Checked = CType(IIf(e.Item.Cells(6).Text = "Active", True, False), Boolean)

                    imbSave.Visible = True
                    imgBack.Visible = False
                    imgCancel.Visible = True
                    chkSystem.Text = ""
                    chkScheme.Text = ""
                    chkAgreement.Text = ""
                    chkPaymentReceive.Text = ""
                    chkPaymentRequest.Text = ""
                    chkPettyCash.Text = ""
                    chkHeadOffice.Text = ""
                    chkStatus.Text = ""
                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim hypID As LinkButton
                    lblMessage.Text = ""
                    hypID = CType(dtgEntity.Items(e.Item.ItemIndex).FindControl("hypPaymentAllocationId"), LinkButton)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .PaymentAllocationId = hypID.Text
                    End With
                    Try
                        m_controller.PaymentAllocationDelete(oCustomClass)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        BindGridEntity(Me.SearchBy, Me.SortBy)
                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try

                End If
            Case "SHOWVIEW"
                If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                    lblMessage.Text = ""
                    txtPaymentAllocationId.Visible = False
                    Dim hypID As LinkButton
                    hypID = CType(dtgEntity.Items(e.Item.ItemIndex).FindControl("hypPaymentAllocationId"), LinkButton)
                    lblPaymentAllocationId.Visible = True
                    lblDescription.Visible = True
                    lblCOA.Visible = True
                    txtDescription.Visible = False
                    txtCOA.Visible = False
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    lblAddEdt.Text = "VIEW"
                    oCustomClass = m_controller.GetPaymentAllocationID(GetConnectionString, hypID.Text.Trim)
                    With oCustomClass
                        lblPaymentAllocationId.Text = .PaymentAllocationId.Trim
                        lblDescription.Text = .Description.Trim
                        lblCOA.Text = .COA.Trim
                        chkSystem.Checked = .IsSystem
                        chkScheme.Checked = .IsScheme
                        chkAgreement.Checked = .IsAgreement
                        chkPaymentReceive.Checked = .IsPaymentReceive
                        chkPettyCash.Checked = .IsPettyCash
                        chkHeadOffice.Checked = .IsHeadOffice
                        chkStatus.Checked = .Status
                    End With
                    imbSave.Visible = False
                    imgBack.Visible = True
                    imgCancel.Visible = False
                    pnlSearch.Visible = False
                    chkSystem.Enabled = False
                    chkScheme.Enabled = False
                    chkAgreement.Enabled = False
                    chkPaymentReceive.Enabled = False
                    chkPettyCash.Enabled = False
                    chkHeadOffice.Enabled = False
                    chkStatus.Enabled = False
                    chkSystem.Text = CType(IIf(chkSystem.Checked, "Yes", "No"), String)
                    chkScheme.Text = CType(IIf(chkScheme.Checked, "Yes", "No"), String)
                    chkAgreement.Text = CType(IIf(chkAgreement.Checked, "Yes", "No"), String)
                    chkPaymentReceive.Text = CType(IIf(chkPaymentReceive.Checked, "Yes", "No"), String)
                    chkPettyCash.Text = CType(IIf(chkPettyCash.Checked, "Yes", "No"), String)
                    chkHeadOffice.Text = CType(IIf(chkHeadOffice.Checked, "Yes", "No"), String)
                    chkStatus.Text = CType(IIf(chkStatus.Checked, "Active", "Not Active"), String)
                End If
        End Select
    End Sub
#End Region

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click

        With oCustomClass
            .strConnection = GetConnectionString()
            .Description = txtDescription.Text
            .COA = txtCOA.Text
            .IsSystem = chkSystem.Checked
            .IsScheme = chkScheme.Checked
            .IsAgreement = chkAgreement.Checked
            .IsPaymentReceive = chkPaymentReceive.Checked
            .IsPettyCash = chkPettyCash.Checked
            .IsHeadOffice = chkHeadOffice.Checked
            .Status = chkStatus.Checked
            .isPaymentRequest = chkPaymentRequest.Checked
        End With

        Select Case Me.AddEdt
            Case "ADD"
                oCustomClass.PaymentAllocationId = txtPaymentAllocationId.Text
                Try
                    m_controller.PaymentAllocationAdd(oCustomClass)

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            Case "EDIT"
                oCustomClass.PaymentAllocationId = lblPaymentAllocationId.Text
                Try
                    m_controller.PaymentAllocationEdit(oCustomClass)
                    BindGridEntity(Me.SearchBy, Me.SortBy)

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
        End Select
    End Sub

    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            pnlSearch.Visible = False

            lblMessage.Text = ""
            lblAddEdt.Text = "ADD"
            Me.AddEdt = "ADD"
            rfvPaymentAllocationId.Enabled = True
            lblPaymentAllocationId.Visible = False
            lblDescription.Visible = False
            lblCOA.Visible = False
            txtPaymentAllocationId.Visible = True
            txtDescription.Visible = True
            txtCOA.Visible = True
            txtPaymentAllocationId.Text = ""
            txtDescription.Text = ""
            txtCOA.Text = ""
            chkSystem.Checked = False
            chkScheme.Checked = False
            chkAgreement.Checked = False
            chkPaymentReceive.Checked = True
            chkPettyCash.Checked = False
            chkHeadOffice.Checked = False
            chkStatus.Checked = True
            imbSave.Visible = True
            imgBack.Visible = False
            imgCancel.Visible = True
            chkSystem.Enabled = False
            chkScheme.Enabled = True
            chkAgreement.Enabled = True
            chkPaymentReceive.Enabled = True
            chkPettyCash.Enabled = True
            chkHeadOffice.Enabled = True
            chkStatus.Enabled = True
            chkSystem.Text = ""
            chkScheme.Text = ""
            chkAgreement.Text = ""
            chkPaymentReceive.Text = ""
            chkPettyCash.Text = ""
            chkHeadOffice.Text = ""
            chkStatus.Text = ""
        End If
    End Sub

    Public Function isVisible(ByVal isSystem As Boolean) As Boolean
        If isSystem = True Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SearchBy = ""
        lblMessage.Text = ""
        oSearchBy.Text = ""
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSearch.Click
        Me.SearchBy = ""
        lblMessage.Text = ""
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " LIKE '%" & oSearchBy.Text & "%'"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgEntity.SortCommand
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imgBack_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgBack.Click
        lblMessage.Text = ""
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgCancel.Click
        lblMessage.Text = ""
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#Region "OnChange"

    Protected Sub btnGLMasterAcc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGLMasterAcc.Click
        ucGLMasterAcc1.CmdWhere = "All"
        ucGLMasterAcc1.Sort = "CoAId ASC"
        ucGLMasterAcc1.Popup()
    End Sub
    Public Sub CatSelectedGLMasterAcc(ByVal CoAId As String, ByVal Description As String)
        txtCOA.Text = Description
        CoAId = CoAId
    End Sub
#End Region
End Class