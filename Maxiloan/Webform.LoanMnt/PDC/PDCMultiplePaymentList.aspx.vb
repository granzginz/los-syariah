﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class PDCMultiplePaymentList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
     
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 6
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            Me.FormID = "PDCMULTIPMENLIST"
            oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-InstallmentAmount, Angsuran"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage) 
    End Sub
  
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label 
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
            hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)  
            hypReceive.NavigateUrl = String.Format("PDCMultiPayment.aspx?CustomerId={0}&applicationid={1}&branchid={2}", Server.UrlEncode(lblTemp.Text.Trim), lblApplicationid.Text.Trim, oBranch.BranchID.Trim)
            CType(e.Item.FindControl("hyCustomerName"), HyperLink).NavigateUrl = String.Format("javascript:OpenCustomer('AccMnt', '{0}')", Server.UrlEncode(lblTemp.Text.Trim))
            CType(e.Item.FindControl("hyAgreementNo"), HyperLink).NavigateUrl = String.Format("javascript:OpenAgreementNo('AccMnt', '{0}')", Server.UrlEncode(lblApplicationid.Text.Trim))
            lblApplicationid.NavigateUrl = String.Format("javascript:OpenAgreementNo('AccMnt', '{0}')", Server.UrlEncode(lblApplicationid.Text.Trim)) 
        End If
    End Sub


    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
         
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.AgreementList(oCustomClass)
        recordCount = oCustomClass.TotalRecord 
        Dim DvUserList = oCustomClass.ListAgreement.DefaultView
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList 
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If 
        DtgAgree.DataBind() 
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) 
        Me.SortBy = IIf(InStr(Me.SortBy, "DESC") > 0, e.SortExpression, e.SortExpression + " DESC")
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PDCMultiplePaymentList.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click 
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' " 
        If oSearchBy.Text.Trim <> "" Then
            If oSearchBy.ValueID = "InstallmentAmount" And Not (IsNumeric(oSearchBy.Text.Trim)) Then
                ShowMessage(lblMessage, "Angsuran harus diisi dengan Angka", True)
                Exit Sub
            End If
            Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        End If 
        DoBind(Me.SearchBy, Me.SortBy)
        DtgAgree.Visible = True
    End Sub

End Class