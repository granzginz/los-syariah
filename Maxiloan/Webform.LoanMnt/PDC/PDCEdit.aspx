﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCEdit.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PDCEdit" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCEdit</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    EDIT PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status PDC
                </label>
                <asp:DropDownList ID="cboStatus" runat="server">
                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                    <asp:ListItem Value="OP" Selected="True">Open</asp:ListItem>
                    <asp:ListItem Value="HD">Hold</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
         
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" Visible="False" ShowFooter="True"
                        AllowSorting="True" OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="GiroNo"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="APPLICATION ID">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PDCreceiptno" HeaderText="PDCreceiptno">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCreceiptno" runat="server" Text='<%#Container.DataItem("PDCreceiptno")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BANKID" HeaderText="BANK PDC">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="Total" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GIRONO" HeaderText="NO PDC">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNo" runat="server" Text='<%#Container.DataItem("GIRONO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" SortExpression="PDCDueDate" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="TOtalPDCAmount" HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="13%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PDCStatusdesc" HeaderText="STATUS">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCStatus" runat="server" Text='<%#Container.DataItem("PDCStatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT KONTRAK">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px" Width="6%">
                                </HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="EditAgreement" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="EditAgreement" Visible='<%#iif(Container.DataItem("MaxGiroSeqNo")>1,True,False)%>'
                                        CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT ALOKASI">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px" Width="6%">
                                </HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="EditAllocation" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="EditAllocation" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="Giro Seq No">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGiroSeqNo" runat="server" Text='<%#Container.DataItem("MaxGiroSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" ControlToValidate="txtPage"
                            MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
