﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCBounceReconcile
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents osearchby As UcSearchBy

#Region "Property"
    Private Property BankAccountID() As String
        Get
            Return CStr(viewstate("BankAccountID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

    Private Property DueDate() As Date
        Get
            Return CDate(viewstate("DueDate"))
        End Get
        Set(ByVal Value As Date)
            viewstate("DueDate") = Value
        End Set
    End Property
    Private Property SelectAll2() As Double
        Get
            Return (CType(Viewstate("SelectAll"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("SelectAll") = Value
        End Set
    End Property

    Private Property GiroNo() As String
        Get
            Return (CType(Viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property

    Private Property ReceiptNo() As String
        Get
            Return (CType(Viewstate("ReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReceiptNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private temptotalPDC As Double
    Protected WithEvents oReason As ucReason
    Protected WithEvents oBankAccount As UcBankAccountID
    Private oCustomClass As New Parameter.PDCBounce
    Private oController As New PDCBounceReconcileController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PDCBOUNCERECONCILE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                oBankAccount.BankPurpose = ""
                oBankAccount.BankType = "B"
                oBankAccount.BindBankAccount()
                oSearchBy.ListData = "GiroNo, PDC No.-BANKID,Bank PDC"
                oSearchBy.BindData()

                Me.SearchBy = ""
                Me.SortBy = ""
                pnlList.Visible = True
                pnlDatagrid.Visible = False
                pnlBounce.Visible = False
            End If
        End If
    End Sub
#Region "Bind Grid"
    Private Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
            .BankAccountID = oBankAccount.BankAccountID
            .ClearDate = ConvertDate2(txtsdate.Text)
            .BranchId = Me.sesBranchId.Replace("'", "")
            .CurrentPage = currentPage
            .PageSize = pageSize
        End With
        oCustomClass = oController.PDCBounceList(oCustomClass)
        recordCount = oCustomClass.TotalRecord

        DtUserList = oCustomClass.ListPDCBounce
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList
        DtgAgree.DataBind()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
        PagingFooter()
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan  .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Text = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Dtg Command"
    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "BOUNCE"
                'If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                oReason.ReasonTypeID = "PDBNC"
                oReason.BindReason()
                pnlList.Visible = False
                pnlDatagrid.Visible = False
                pnlBounce.Visible = True
                Dim hyPDCReceiptNo As HyperLink
                Dim hyGiroNo As HyperLink
                Dim lblBranchid As Label

                hyPDCReceiptNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("hyPDCReceiptNo"), HyperLink)
                lblBranchid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblBranchid"), Label)
                hyGiroNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblGiroNo"), HyperLink)
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = lblBranchid.Text.Trim
                    .PDCReceiptNo = hyPDCReceiptNo.Text.Trim
                    .GiroNo = hyGiroNo.Text.Trim
                    Me.ReceiptNo = .PDCReceiptNo
                    Me.GiroNo = .GiroNo
                    Me.BranchID = lblBranchid.Text.Trim
                End With
                oCustomClass = oController.PDCBounceView(oCustomClass)
                With oCustomClass
                    HypGiroNo.Text = Me.GiroNo
                    HypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & Trim(Me.GiroNo) & "','" & Trim(Me.ReceiptNo) & "','" & Trim(lblBranchid.Text) & "','0')"

                    lblPDCDueDate.Text = .PDCDueDate.ToString("dd/MM/yyyy")
                    lblBankPDC.Text = .BankName.Trim
                    lblBankClearing.Text = .BankAccountName.Trim
                    lblViewAmount.Text = FormatNumber(.TotalPDCAmount, 2)
                    lblCummFlag.Text = IIf(.IsCummulative, "Yes", "No").ToString.Trim
                    lblInkasoFlag.Text = IIf(.IsInkaso, "Yes", "No").ToString.Trim
                    lblPDCType.Text = .PDCType
                    txtnotes.Text = ""
                End With
        End Select
    End Sub
#End Region

#Region "Datagrid Bound"
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim totalPDCAmount As Label
        Dim lblPDCAmount As Label
        Dim hypGiroNo As HyperLink
        Dim hypPDCReceiptNo As HyperLink

        Dim lblBranchid As Label
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lblPDCAmount.Text)

            hypPDCReceiptNo = CType(e.Item.FindControl("hyPDCReceiptNo"), HyperLink)

            lblBranchid = CType(e.Item.FindControl("lblBranchid"), Label)
            hypGiroNo = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
            hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & Trim(hypGiroNo.Text) & "','" & Trim(hypPDCReceiptNo.Text) & "','" & Trim(lblBranchid.Text) & "','0')"
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC, 2)
        End If
    End Sub
#End Region
#Region "Search Process"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        Me.SearchBy = ""
        Me.SortBy = ""
        If osearchby.Text.Trim <> "" Then
            If Right(osearchby.Text.Trim, 1) = "%" Then
                Me.SearchBy = osearchby.ValueID & " Like '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy = osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            End If

        End If
        DoBind(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region
#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PDCBounceReconcile.aspx")
    End Sub
#End Region

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        pnlBounce.Visible = False
        lblMessage.Visible = False
    End Sub

    Private Sub BtnBounce_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBounce.Click
        If Me.CheckFeature(Me.Loginid, Me.FormID, "BNC", Me.AppId) Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .GiroNo = Me.GiroNo
                .PDCReceiptNo = Me.ReceiptNo
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .ReasonID = oReason.ReasonID
                .Notes = txtnotes.Text.Trim
                .ReferenceNo = txtReferenceNo.Text
                .ValueDate = ConvertDate2(txtValueDate.Text)
            End With
            Try
                oController.ProcesPDCBounce(oCustomClass)
                BindReport()
                pnlDatagrid.Visible = True
                pnlList.Visible = True
                DoBind(Me.SearchBy, Me.SortBy)
                pnlDatagrid.Visible = True
                pnlList.Visible = True
                DtgAgree.Visible = True
                pnlBounce.Visible = False

                ShowMessage(lblMessage, "Data saved!", False)

            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
#Region "BindReport"

    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As RptPrintPDCBounceRecipt = New RptPrintPDCBounceRecipt
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .GiroNo = Me.GiroNo
            .PDCReceiptNo = Me.ReceiptNo
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
        End With
        oData = oController.PrintPDCBounce(oCustomClass)
        'oData.WriteXmlSchema("C:\InetPub\wwwroot\eStartunas\AccMnt\PDC\ReceiptPrint\PDCBounce.xmd")

        objReport.SetDataSource(oData)

        '========================================================
        'Dim discrete As ParameterDiscreteValue
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues
        'ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.BusinessDate
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamField = objReport.DataDefinition.ParameterFields("LoginID")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.Loginid
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "PDCBounce.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()

        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PDCBounce.pdf"
        
        Response.Write("<script language = javascript>" & vbCrLf _
        & "var x = screen.width; " & vbCrLf _
        & "var y = screen.height; " & vbCrLf _
        & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
        & "</script>")
    End Sub
#End Region

End Class