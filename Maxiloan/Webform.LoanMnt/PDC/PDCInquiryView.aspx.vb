﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCInquiryView
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchby As UcSearchBy
    Protected WithEvents obranch As UcBranch
    Protected WithEvents oBankPDC As UcBankMaster
    Dim temptotalPDC As Double
#Region "Property"

    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub

        End If

        If Not IsPostBack Then
            Me.FormID = "PDCINQUIRY"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If
                osearchby.ListData = "GiroNO, PDC No."
                osearchby.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.PDCInquiryStatus(oCustomClass)

        DtUserList = oCustomClass.listPDCStatus
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim hypGiroNo As HyperLink
        Dim flagfile As New Label
        Dim lblPDCRNo As Label
        flagfile.Text = "0"
        Dim lblTemp As Label
        Dim hyTemp As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If
        If e.Item.ItemIndex >= 0 Then
            lblPDCRNo = CType(e.Item.FindControl("lblPDCReceiptNo"), Label)
            hypGiroNo = CType(e.Item.FindControl("HyGiroNo"), HyperLink)
            'hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & lblPDCRNo.Text.Trim & "&branchid=" & obranch.BranchID.Trim & "&flagfile=" & flagfile.Text.Trim
            hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & hypGiroNo.Text.Trim & "','" & lblPDCRNo.Text.Trim & "','" & obranch.BranchID.Trim & "','" & flagfile.Text.Trim & "')"
        End If
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PDCInquiryView.aspx")
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim strParam As New StringBuilder
        Dim strSearch As New StringBuilder
        Dim par As String
        par = ""
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            strSearch.Append("  PH.branchid = '" & obranch.BranchID.Trim & "' ")
            strParam.Append("Branch : " & obranch.BranchName.Trim & " ")

            If cboPDCStatus.SelectedItem.Value <> "ALL" Then
                strSearch.Append(" and PDCStatus = '" & cboPDCStatus.SelectedItem.Value & "'")
                If strParam.ToString <> "" Then
                    strParam.Append(" , PDCStatus : " & cboPDCStatus.SelectedItem.Text.Trim & " ")
                Else
                    strParam.Append("PDCStatus : " & cboPDCStatus.SelectedItem.Text.Trim & " ")
                End If

            End If
            If txtsdate.Text <> "" Then
                strSearch.Append(" and PH.PDCDueDate = '" & ConvertDate2(txtsdate.Text) & "'")
                If strParam.ToString <> "" Then
                    strParam.Append(" , PDCDueDate : " & ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy") & " ")
                Else
                    strParam.Append("PDCDueDate : " & ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy") & " ")
                End If
            End If
            If cboCumm.SelectedItem.Value <> "ALL" Then
                strSearch.Append(" and PH.Iscummulative = '" & cboCumm.SelectedItem.Value.Trim & "'")
                If strParam.ToString <> "" Then
                    strParam.Append(" , IsCummulative : " & cboCumm.SelectedItem.Text.Trim & " ")
                Else
                    strParam.Append("IsCummulative : " & cboCumm.SelectedItem.Text.Trim & " ")
                End If
            End If

            If cboInkaso.SelectedItem.Value <> "ALL" Then
                strSearch.Append(" and PH.IsInkaso = '" & cboInkaso.SelectedItem.Value.Trim & "'")
                If strParam.ToString <> "" Then
                    strParam.Append(" , IsInkaso : " & cboInkaso.SelectedItem.Text.Trim & " ")
                Else
                    strParam.Append("IsInkaso : " & cboInkaso.SelectedItem.Text.Trim & " ")
                End If
            End If

            If oBankPDC.BankID <> "ALL" Then
                strSearch.Append(" and PH.bankID = '" & oBankPDC.BankID.Trim & "'")
                If strParam.ToString <> "" Then
                    strParam.Append(" , Bank PDC : " & oBankPDC.BankName.Trim & " ")
                Else
                    strParam.Append("bank PDC : " & oBankPDC.BankName.Trim & " ")
                End If
            End If

            If osearchby.Text.Trim <> "" Then
                If Right(osearchby.Text.Trim, 1) = "%" Then
                    strSearch.Append(" and " & osearchby.ValueID & " Like '" & osearchby.Text.Trim.Replace("'", "''") & "'")
                Else
                    strSearch.Append(" and " & osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'")
                End If
                If strParam.ToString <> "" Then
                    strParam.Append(" , " & osearchby.ValueID & " : '" & osearchby.Text.Trim.Replace("'", "''") & "'")
                Else
                    strParam.Append("" & osearchby.ValueID & " : '" & osearchby.Text.Trim.Replace("'", "''") & "'")
                End If
            End If
            Me.SearchBy = strSearch.ToString

            oCustomClass.paramReport = strParam.ToString
            Me.ParamReport = oCustomClass.paramReport
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class
