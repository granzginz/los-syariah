﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCReceive
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents cbobank As UcBankName
    Protected WithEvents txtPDCAmount As ucNumberFormat
    Protected WithEvents txtNoPDC As ucNumberFormat


    Dim temptotalPDC As Double
    Dim TotPDCAmount As Double

#Region "Property"
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(Viewstate("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("PaymentFrequency") = Value
        End Set
    End Property

    Private Property BankPDC() As String
        Get
            Return (CType(Viewstate("BankPDC"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankPDC") = Value
        End Set
    End Property

    Private Property ReceiveFrom() As String
        Get
            Return (CType(Viewstate("ReceiveFrom"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReceiveFrom") = Value
        End Set
    End Property

    Private Property GrandTotPDCAmount() As Double
        Get
            Return (CType(Viewstate("GrandTotPDCAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("GrandTotPDCAmount") = Value
        End Set
    End Property

    Private Property PDCReceiptNo() As String
        Get
            Return CType(viewstate("PDCReceiptNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PDCReceiptNo") = Value
        End Set
    End Property


    Private Property PDCSource As IList(Of PDCParam)
        Get
            Return CType(ViewState("PDCSource"), IList(Of PDCParam))
        End Get
        Set(value As IList(Of PDCParam))
            ViewState("PDCSource") = value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCReceiveController

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            PDCSource = New List(Of PDCParam)
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.CustomerID = Request.QueryString("CustomerId")
            Me.FormID = "PDCRECEIVE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim pstrFile As String

               
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.HpsXml = "1"
                oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                DoBind()

                InitiateUCnumberFormat(txtNoPDC, True, False)
                InitiateUCnumberFormat(txtPDCAmount, True, False)

                txtNoPDC.RangeValidatorEnable = True
                txtNoPDC.RangeValidatorMaximumValue = 120

            End If
        End If
       
    End Sub
#End Region
#Region "Do Bind"

    Sub DoBind()
        'If Not CheckCashier(Me.Loginid) Then

        '    ShowMessage(lblMessage, "Kasir belum Buka", True)
        '    btnAddNew.Visible = False
        'Else
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.BranchID
        oCustomClass = oController.PDCReceiveList(oCustomClass)
        With oCustomClass
            lblBranchID.Text = .BranchName
            Me.AgreementNo = .Agreementno
            hyAgreementNo.Text = .Agreementno
            If hyAgreementNo.Text.Trim.Length > 0 Then
                hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            End If
            hyCustomerName.Text = .CustomerName
            If hyCustomerName.Text.Trim.Length > 0 Then
                hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID) & "')"
            End If
            lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 0)
            txtPDCAmount.Text = FormatNumber(.InstallmentAmount, 0)
            lblNInsDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
            lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)
            Me.PaymentFrequency = .PaymentFrequency

            If CStr(.PaymentFrequency) = "1" Then
                lblPaymentFrequency.Text = "Monthly"
            ElseIf CStr(.PaymentFrequency) = "2" Then
                lblPaymentFrequency.Text = "Bimonthly"
            ElseIf CStr(.PaymentFrequency) = "3" Then
                lblPaymentFrequency.Text = "Quarterly"
            ElseIf CStr(.PaymentFrequency) = "6" Then
                lblPaymentFrequency.Text = "Semi Annualy"
            End If

            'lblPaymentFrequency.Text = CStr(.PaymentFrequency)
        End With
        'End If
    End Sub
#End Region

#Region "DtgPDCList ItemDataBound"
    Private Sub DtgPDCList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPDCList.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim totalPDCAmount As New Label
        Dim lPDCAmount As New Label

        With oCustomClass

            If e.Item.ItemIndex >= 0 Then
                lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
                temptotalPDC += CDbl(lPDCAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then
                TotPDCAmount = temptotalPDC
                Me.GrandTotPDCAmount = TotPDCAmount
                totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
                totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
            End If

        End With

        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then

            Dim textbx As TextBox = DirectCast(e.Item.FindControl("lblPDCNo2"), TextBox)
            Dim textbx2 As TextBox = DirectCast(e.Item.FindControl("lblPDCDueDate"), TextBox)

            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return fConfirm()")
        End If
         
    End Sub
#End Region



#Region "btn Cancel"
    Private Sub btnCancelNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click

        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
        pnlPDC.Visible = False
        pnlList.Visible = True
        Response.Redirect("PDCReceiveList.aspx")
    End Sub
#End Region
    Sub refreshGridNo()
        For Each item In DtgPDCList.Items
            Dim _seq = CInt(DirectCast(item.FindControl("hdSeq"), HiddenField).Value)
            Dim textbx As TextBox = DirectCast(item.FindControl("lblPDCNo2"), TextBox)
            Dim textbx2 As TextBox = DirectCast(item.FindControl("lblPDCDueDate"), TextBox)
            Dim prm = PDCSource.SingleOrDefault(Function(x) x.seq = _seq)
            If (Not prm Is Nothing) Then
                prm.PDCNo = textbx.Text
                prm.DueDate = ConvertDate2(textbx2.Text)
            End If
        Next
    End Sub


    Sub toPDCSource(tbl As DataTable)
        PDCSource.Clear()
        Dim i As Integer = 1
        For Each item In tbl.Rows

            PDCSource.Add(New PDCParam With {
                    .seq = i,
                    .Holiday = item("Holiday").ToString(),
                    .BankPDC = item("BankPDC").ToString(),
                    .BankID = item("BankID").ToString(),
                    .PDCNo = item("PDCNo").ToString(),
                    .PDCAmount = CDec(item("PDCAmount").ToString()),
                    .DueDate = CDate(item("DueDate").ToString()),
                    .IsInkaso = CBool(item("IsInkaso").ToString()),
                    .IsCummulative = CBool(item("IsCummulative").ToString()),
                    .Type = item("Type").ToString()
                          })
            i += 1
        Next
    End Sub

#Region "Add"
    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim lBytEnd As Integer
        Dim lBytCounter As Integer
        Dim lblListPDCNo As Label
        Dim lstrPDC As String
        Dim pStrFile As String
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            lblMessage.Text = ""

            If rdoInkaso.SelectedItem.Text = "Yes" Then
                If rdoPDCType.SelectedItem.Text = "Cheque" Then

                    ShowMessage(lblMessage, "Flag Inkaso hanya berlaku untuk jenis PDC Giro", True)
                    Exit Sub
                End If
            End If

            If CInt(txtNoPDC.Text) <= 0 Then

                ShowMessage(lblMessage, "Jumlah Lembar PDC harus > 0 ", True)
                Exit Sub
                'Else
                '    If CInt(txtNoPDC.Text) > 25 Then

                '        ShowMessage(lblMessage, "Jumlah lembar PDC harus < 25", True)
                '        Exit Sub
                '    End If
            End If


            If IsNumeric((Right(txtPDCNo.Text, 1))) Then
            Else

                ShowMessage(lblMessage, "No PDC Salah", True)
                Exit Sub
            End If
            If txtsdate.Text = "" Then

                ShowMessage(lblMessage, "Harap isi Tanggal jatuh Tempo PDC", True)
                Exit Sub
            Else

                Dim indate As Boolean
                indate = IsDate(ConvertDate2(txtsdate.Text))
                If indate Then
                Else

                    ShowMessage(lblMessage, "Tanggal jatuh Tempo Salah", True)
                    Exit Sub
                End If
            End If


            If CDbl(txtPDCAmount.Text) <= 0 Then

                ShowMessage(lblMessage, "Jumlah PDC harus > 0 ", True)
                Exit Sub
            End If
            With oCustomClass
                pStrFile = Me.Session.SessionID + Me.Loginid
                .LoginId = .LoginId
                .strConnection = GetConnectionString()
                .ReceivedFrom = txtReceiveFrom.Text
                .GiroNo = txtPDCNo.Text
                .PDCAmount = CInt(txtPDCAmount.Text)
                .BankPDCName = cbobank.BankName
                .BankPDC = cbobank.BankID
                Me.BankPDC = .BankPDC
                .NumPDC = CInt(txtNoPDC.Text)
                .BranchId = Me.BranchID
                .PaymentFrequency = Me.PaymentFrequency
                Me.ReceiveFrom = .ReceivedFrom
                .NextInstallmentDueDate = ConvertDate2(txtsdate.Text)
                .IsInkaso = CType(rdoInkaso.SelectedItem.Value, Boolean)
                .IsCumm = CType(rdoCumm.SelectedItem.Value, Boolean)
                .PDCType = rdoPDCType.SelectedItem.Value
                .FlagDelete = "0"
            End With

            oCustomClass = oController.GetTablePDC(oCustomClass, pStrFile)

            If Not oCustomClass.IsValidPDC Then

                ShowMessage(lblMessage, "No PDC sudah ada", True)
                'imbSave.Visible = False
                Exit Sub
            Else
                toPDCSource(oCustomClass.ListPDC)
                BtnSave.Visible = True
                DtgPDCList.DataSource = PDCSource ' oCustomClass.ListPDC
                DtgPDCList.DataBind()
                DtgPDCList.Visible = True
                pnlPDC.Visible = True
            End If


            txtPDCAmount.Text = ""
            txtNoPDC.Text = ""
            txtPDCNo.Text = ""
            txtsdate.Text = ""
            rdoInkaso.SelectedIndex = 1
            rdoCumm.SelectedIndex = 1
            rdoPDCType.SelectedIndex = 0
            cbobank.BankID = ""
            cbobank.DataBind()
            btnCancelNew.Visible = False

        Else
            DtgPDCList.Visible = True
            pnlPDC.Visible = True
            Exit Sub
        End If
    End Sub
#End Region

#Region "Imb Cancel"

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click

        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
        pnlPDC.Visible = False
        pnlList.Visible = True
        btnCancelNew.Visible = True
        'btnReset.Visible = False

        txtReceiveFrom.Text = ""
        txtReceiveFrom.Enabled = True
        txtPDCAmount.Enabled = True
        txtPDCAmount.Text = ""
        txtPDCNo.Text = ""
        rdoInkaso.SelectedIndex = 1
        rdoCumm.SelectedIndex = 1
        rdoPDCType.SelectedIndex = 0
        txtsdate.Text = ""
        cbobank.BankID = ""
        cbobank.BindData()
    End Sub
#End Region

#Region "DtgPDCList ItemCommand"
    Private Sub DtgPDCList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPDCList.ItemCommand
        Dim pstrFile As String


        If e.CommandName = "DELETE" Then
            Dim stor_id As String = DtgPDCList.DataKeys(e.Item.ItemIndex()).ToString()
            'With oCustomClass
            '    pstrFile = Me.Session.SessionID + Me.Loginid
            '    .LoginId = Me.Session.SessionID + Me.Loginid
            '    .BranchId = Me.BranchID
            '    .FlagDelete = "1"
            '    .CDtGrid = DtgPDCList.Items.Count
            '    .IndexDelete = CStr(e.Item.ItemIndex())
            '    .HpsXml = "2"
            'End With
            'oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
            'DtgPDCList.DataSource = oCustomClass.ListPDC
            refreshGridNo()


            Dim prm = PDCSource.SingleOrDefault(Function(x) x.seq = CInt(stor_id))
            PDCSource.Remove(prm)
            DtgPDCList.DataSource = PDCSource
            DtgPDCList.DataBind()
            DtgPDCList.Visible = True
            pnlPDC.Visible = True
        End If
    End Sub
#End Region

    Function getDataGridSource() As DataTable
        Dim source = CType(DtgPDCList.DataSource, DataTable)
        Return source
    End Function


#Region "Save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", "MAXILOAN") Then
            If DtgPDCList.Items.Count > 0 Then
                refreshGridNo()
                Dim Ndttable As DataTable
                Dim pStrFile As String
                Ndttable = GetStructPDC()
                With oCustomClass
                    'pStrFile = Me.Session.SessionID + Me.Loginid
                    pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                    .LoginId = Me.Loginid
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .strConnection = GetConnectionString()
                    .ApplicationID = Me.ApplicationID
                    .ReceivedFrom = Me.ReceiveFrom
                    .BusinessDate = Me.BusinessDate
                    .BankPDC = Me.BankPDC
                    .NumPDC = DtgPDCList.Items.Count
                    .GrandTotPDCAmount = Me.GrandTotPDCAmount
                    .PaymentFrequency = Me.PaymentFrequency
                    .BranchAgreement = Me.BranchID

                    .PDCSource = Me.PDCSource
                End With

                Try
                    'Me.PDCReceiptNo = oController.SavePDCReceive(oCustomClass, pStrFile)
                    'oCustomClass.HpsXml = "1"
                    'oCustomClass = oController.GetTablePDC(oCustomClass, pStrFile)
                    'BindReport()

                    Me.PDCReceiptNo = oController.SavePDCReceiveBp(GetConnectionString(), oCustomClass)
                    If Me.PDCReceiptNo <> "" Then                                                                        
                        oCustomClass.HpsXml = "1"
                        oCustomClass = oController.GetTablePDC(oCustomClass, pStrFile)
                        pnlPDC.Visible = False
                        pnlList.Visible = True
                         
                        Server.Transfer("PDCReceiveList.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
                    End If
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            Else

                ShowMessage(lblMessage, "Data Tidak ditemukan", True)
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "GetStructPDC"
    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("PDC")
        '  Dim dtTable As New DataTable("PDC")

        With lObjDataTable
            .Columns.Add("Holiday", GetType(System.String))
            .Columns.Add("BankPDC", GetType(System.String))
            .Columns.Add("PDCNo", GetType(System.String))
            .Columns.Add("PDCAmount", GetType(System.String))
            .Columns.Add("DueDate", GetType(System.DateTime))
            .Columns.Add("IsInkaso", GetType(System.String))
            .Columns.Add("IsCummulative", GetType(System.String))
            .Columns.Add("Type", GetType(System.String))
        End With

        Return lObjDataTable
    End Function
#End Region

#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As RptPrintReceiptNo = New RptPrintReceiptNo

        oData = oController.PrintReceiptNo(GetConnectionString, Me.PDCReceiptNo, Me.sesBranchId.Replace("'", ""))
        objReport.SetDataSource(oData)

        '========================================================
        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues
        ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BusinessDate
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "MultiPDC.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        Response.Redirect("PDCReceiveList.aspx?Message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "MultiPDC")
    End Sub
#End Region

    Protected Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign regular_text"
        End With
    End Sub
End Class