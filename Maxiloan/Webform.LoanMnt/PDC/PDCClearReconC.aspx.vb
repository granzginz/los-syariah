﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCClearReconC
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents txtamountRec As ucNumberFormat

    Dim intLoopGrid As Integer
    Dim temptotalPDC As Double
    Protected WithEvents osearchby As UcSearchBy    
    Dim flagfile As String
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCClearReconcileController

#End Region

#Region "Property"


    Private Property PDCReceiptNo() As String
        Get
            Return (CType(Viewstate("PDCReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property

    Private Property girono() As String
        Get
            Return (CType(Viewstate("girono"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("girono") = Value
        End Set
    End Property

    Private Property newstatus() As String
        Get
            Return (CType(Viewstate("newstatus"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("newstatus") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblgiro As New Label
            Dim lblnewStat As New Label
            Dim lblPDCReceiptno As New Label
            Me.FormID = "PDCCLEARRECONCILE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.BranchID = Request.QueryString("branchid")
                lblgiro.Text = Request.QueryString("girono")
                lblPDCReceiptno.Text = Request.QueryString("PDCReceiptNo")
                Me.PDCReceiptNo = lblPDCReceiptno.Text
                Me.girono = lblgiro.Text
                HyGiroNo.NavigateUrl = "javascript:OpenWinPDC(" & Me.girono & "," & Me.PDCReceiptNo & "," & Me.BranchID & ",'0')"
                DoBind()
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        oCustomClass.BranchId = Me.BranchID
        oCustomClass.GiroNo = Me.girono
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.PDCClearReconCList(oCustomClass)
        With oCustomClass
            flagfile = "0"
            HyGiroNo.Text = .GiroNo
            HyGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & Trim(Me.PDCReceiptNo) & "','" & Trim(Me.BranchID) & "','" & Trim(flagfile) & "')"
            lblBankPDC.Text = .BankPDCName
            'rngAmountRec.MinimumValue = "0"
            'rngAmountRec.MaximumValue = CStr(.PDCAmount)
            'rngAmountRec.ErrorMessage = "Jumlah diterima harus diantara 0.00 dan " & FormatNumber(.PDCAmount, 2)

            txtamountRec.rv.MinimumValue = "0"
            txtamountRec.rv.MaximumValue = CStr(.PDCAmount)
            txtamountRec.rv.ErrorMessage = "Jumlah diterima harus diantara 0.00 dan " & FormatNumber(.PDCAmount, 2)
            txtamountRec.RangeValidatorEnable = True
            txtamountRec.Enabled = True

            txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            lblPDCAmount.Text = FormatNumber(.PDCAmount, 2)
            lblPDCDueDate.Text = .PDCDueDate.ToString("dd/MM/yyyy")
            lblPDCType.Text = .PDCType
            lblBankClearing.Text = .BankClearingName
            If CStr(.IsInkaso) = "1" Then
                lblInkasoFlag.Text = "Yes"
            Else
                lblInkasoFlag.Text = "No"
            End If

            If CStr(.IsInkaso) = "1" Then
                lblCummFlag.Text = "Yes"
            Else
                lblCummFlag.Text = "No"
            End If
        End With
    End Sub
#End Region

    Private Sub btnClearRecon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearRecon.Click
        If CheckFeature(Me.Loginid, Me.FormID, "CLEAR", Me.AppId) Then

            With oCustomClass
                .PDCReceiptNo = Me.PDCReceiptNo
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .HoldUntildate = ConvertDate2(txtsdate.Text)
                .strConnection = GetConnectionString()
                .GiroNo = Me.girono
                .ivalueDate = ConvertDate2(txtsdate.Text)
                .Notes = txtnotes.Text
                .PDCAmount = CDbl(txtamountRec.Text)
                .BusinessDate = Me.BusinessDate

            End With
            Try
                oController.SavePDCClearCRecon(oCustomClass)
                Response.Redirect("PDCClearReconcile.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try

        End If
    End Sub

    Private Sub btnCancelRecon_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelRecon.Click
        Response.Redirect("PDCClearReconcile.aspx")
    End Sub

End Class