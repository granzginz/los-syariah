﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCNewInquiryDetail.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCNewInquiryDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCNewInquiryDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlview" Visible="true" runat="server" EnableViewState="False">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Tanda Terima PDC
                </label>
                <asp:HyperLink ID="lblPDCReceiptNoV" EnableViewState="False" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Terima
                </label>
                <asp:Label ID="lblReceiveDateV" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:Label ID="lblGiroNo" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <asp:Label ID="lblBankPDC" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblPDCAmount" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:Label ID="lblPDCDueDate" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis PDC
                </label>
                <asp:Label ID="lblPDCType" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kliring Bank
                </label>
                <asp:Label ID="lblBankClearing" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:Label ID="lblInkasoFlag" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:Label ID="lblCummFlag" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>STATUS PDC</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status
                </label>
                <asp:Label ID="lblStatus" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Tolakan
                </label>
                <asp:Label ID="lblNumOfBounce" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Hold
                </label>
                <asp:Label ID="lblHolddate" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlPDCAllocationList" EnableViewState="False" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        HISTORY PDC
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgPDC" EnableViewState="False" runat="server" CssClass="grid_general"
                            AutoGenerateColumns="False" Width="100%">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" Width="5%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PDCSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="StatusDate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="tdjudul"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="TRANSAKSI">
                                    <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrans" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PDCStatusDesc")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ALASAN">
                                    <HeaderStyle HorizontalAlign="Center" Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="45%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblReason" runat="server" EnableViewState="False" Text='<%#Container.DataItem("Reasondesc")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="LOKASI">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblLoc" runat="server" EnableViewState="False" Text='<%#Container.DataItem("branchInitialName")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left"  
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlbutton" EnableViewState="False" runat="server">
            <div class="form_button">
                <asp:Button ID="BtnBack" EnableViewState="False" runat="server" CausesValidation="False"
                    Text="Back" CssClass="small button gray"></asp:Button>
                <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
