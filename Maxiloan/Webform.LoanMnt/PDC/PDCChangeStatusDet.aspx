﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCChangeStatusDet.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCChangeStatusDet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../Webform.UserController/UCReason.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCChangeStatusDet</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h3>
                    DETAIL GANTI STATUS PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Tanda Terima PDC
                </label>
                <asp:HyperLink ID="HyPDCReceiptNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Terima
                </label>
                <asp:Label ID="lblReceiveDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:HyperLink ID="HyGiroNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <asp:Label ID="lblBankPDC" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblPDCAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:Label ID="lblPDCDueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis PDC
                </label>
                <asp:Label ID="lblPDCType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kliring Bank
                </label>
                <asp:Label ID="lblBankClearing" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:Label ID="lblInkasoFlag" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:Label ID="lblCummFlag" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    STATUS SAAT INI
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status
                </label>
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Tolakan
                </label>
                <asp:Label ID="lblBounce" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Hold Sampai Tanggal
                </label>
                <asp:Label ID="lblHoldUntilDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    STATUS BARU
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Baru
                </label>
                <asp:DropDownList ID="cboNewStatus" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan
                </label>
                <uc1:ucreason id="cboReason" runat="server"></uc1:ucreason>
            </div>
        </div>
        <asp:Panel ID="pnldate" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Hold Sampai tanggal
                    </label>
                    <asp:TextBox runat="server" ID="txtsdate" CssClass ="small_text" ></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
        </asp:Panel>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_general" >
                    Catatan
                </label>
                <asp:TextBox ID="txtnotes" runat="server" CssClass ="multiline_textbox" TextMode ="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveNew" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
