﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCToDeposit.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCToDeposit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../Webform.UserController/UcBankName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankMaster" Src="../../Webform.UserController/UcBankMaster.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDC To Deposit</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PDC TO DEPOSIT
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl jatuh Tempo PDC
                </label>
                <asp:TextBox runat="server" ID="txtsdate" CssClass="small_text" ToolTip="Filter lebih kecil sama dengan tanggal ini"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Bank PDC
                </label>
                <uc1:ucbankmaster id="oBank" runat="server"></uc1:ucbankmaster>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:DropDownList ID="cboInkaso" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:DropDownList ID="cboCumm" runat="server">
                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Hasil Tolakan</label>
                <asp:CheckBox runat="server" ID="chkHasilTolakan" />
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" Visible="False" AllowSorting="True"
                        OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="GiroNo" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" SortExpression="PDCReceiptNo" HeaderText="PDCReceiptNo">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPDCReceiptNo" runat="server" Text='<%#Container.DataItem("PDCReceiptNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BANKID" HeaderText="BANK PDC">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="Total" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GIRONO" HeaderText="NO PDC">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNo" runat="server" Text='<%#Container.DataItem("GIRONO")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblpbranchid" runat="server" Visible="False" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" SortExpression="PDCDueDate" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="TOtalPDCAmount" HeaderText="JUMLAH">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsInkaso" HeaderText="INKASO">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInkaso" runat="server" Text='<%#Iif(Container.DataItem("IsInkaso")="True" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsCummulative" HeaderText="KUMULATIF">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCumm" runat="server" Text='<%#Iif(Container.DataItem("IsCummulative")="True" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NumberofBounce" HeaderText="TOLAKAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("NumberofBounce")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkSelectAll" runat="server" OnCheckedChanged="SelectAll" AutoPostBack="true">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkSlct" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" ControlToValidate="txtPage"
                            MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ForeColor="#993300" font-name="Verdana"
                            Font-Size="11px" ControlToValidate="txtPage" ErrorMessage="No Halaman Salah"
                            Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Text="Next" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList2" runat="server" Visible="False">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DETAIL DEPOSIT PDC
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo PDC&lt;=
                </label>
                <asp:Label ID="lblPDCDueDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <asp:Label ID="lblBankPDC" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:Label ID="lblInk" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:Label ID="lblCummulative" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Deposit To Bank
                </label>
                <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas Masuk
                </label>
                <asp:TextBox ID="txtRefNo" runat="server"  Columns="22" MaxLength="20"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtGridDeposit" runat="server" Width="100%" Visible="False" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="GiroNo" CssClass="grid_general" AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="BANK PDC">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID2" runat="server" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="Label9" runat="server" Text="Total" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PDCReceiptNo" HeaderText="PDCReceiptNo">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPDCReceiptNo2" runat="server" Text='<%#Container.DataItem("PDCReceiptNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO PDC">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNo2" runat="server" Text='<%#Container.DataItem("GIRONO")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblpbranchid2" runat="server" Visible="False" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" HeaderText="DUE DATE" DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount2" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount2" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="INKASO">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInkaso2" runat="server" Text='<%#Iif(Container.DataItem("IsInkaso")="True" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KUMULATIF">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCumm2" runat="server" Text='<%#Iif(Container.DataItem("IsCummulative")="True" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TOLAKAN">                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBounce2" runat="server" Text='<%#Container.DataItem("NumberofBounce")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="PDCReceiptNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCreceiptNo" runat="server" Text='<%#Container.DataItem("PDCreceiptNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BranchID">
                                <ItemTemplate>
                                    <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btndeposit" runat="server" Text="Deposit" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
