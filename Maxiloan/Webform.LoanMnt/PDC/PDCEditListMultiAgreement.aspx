﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCEditListMultiAgreement.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCEditListMultiAgreement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCEditListMultiAgreement</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR ALOKASI KONTRAK
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang Kontrak
            </label>
            <asp:Label ID="lblBranchAgreement" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No PDC
            </label>
            <asp:Label ID="LblPDCReceiptNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Bilyet Giro
            </label>
            <asp:HyperLink ID="LblGiroNo" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgreementList" AutoGenerateColumns="False" DataKeyField="ApplicationID"
                    CssClass="grid_general" Width="100%" runat="server">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO APLIKASI">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO KONTRAK">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH DITERIMA">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAmountRcv" runat="server" Text='<%#FormatNumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="EDIT ALOKASI">
                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px" Width="6%">
                            </HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hyEdit" runat="server" ImageUrl="../../Images/iconedit.gif"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="GiroSeqNo" Visible="False">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblGiroSeqNo" runat="server" Text='<%#Container.DataItem("GiroSeqNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
