﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCMultiPayment.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PDCMultiPayment" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../Webform.UserController/UcBankName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register Src="../../webform.UserController/UcPaymentAllocationDetail.ascx" TagName="payment" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title>PDCMultiPayment</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3> PDC MULTIPLE TRANSAKSI </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label> Cabang Kontrak </label>
                <asp:Label ID="lblBranchID" runat="server" />
            </div>            
        </div>
        <div class="form_box">
            <div class="form_left">
                <label> No Kontrak </label>
                <asp:HyperLink ID="hyAgreementNo" runat="server">HyperLink</asp:HyperLink>
            </div>
            <div class="form_right">
                <label> Nama Customer </label>
                <asp:HyperLink ID="hyCustomerName" runat="server">HyperLink</asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label> Jumlah Angsuran </label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label> Jatuh Tempo Berikutnya </label>
                <asp:Label ID="lblNInsDate" runat="server" />&nbsp;&nbsp;|&nbsp;Angsuran&nbsp;
                <asp:Label ID="lblNextInstallmentNumber" Width="3px" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req"> Terima Dari </label>
                <asp:TextBox ID="txtReceiveFrom" runat="server"  MaxLength="50" Width="180px" />
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Harap diisi terima Dari" Display="Dynamic" ControlToValidate="txtReceiveFrom" Visible="true" CssClass="validator_general" />
            </div>            
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req"> No PDC </label>
                <asp:TextBox ID="txtPDCNo" runat="server"  MaxLength="20" />
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap diisi No PDC" ControlToValidate="txtPDCNo" Visible="true" Display="Dynamic" CssClass="validator_general" />
            </div>
            <div class="form_right">
                <label class="label_req"> Jumlah </label>               
                    <uc1:ucnumberformat id="txtPDCAmount" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label> Tgl Jatuh Tempo PDC </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate" Format="dd/MM/yyyy" />
            </div>
            <div class="form_right">
                <label class="label_req"> Bank PDC </label>
                <uc1:ucbankname id="cboBank" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_general" > Flag Inkaso </label>
                <asp:RadioButtonList ID="rdoInkaso" runat="server" RepeatDirection="Horizontal" CssClass ="opt_single">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="form_right">
                <label class ="label_general" > Jenis PDC </label>
                <asp:RadioButtonList ID="rdoPDCType" runat="server" RepeatDirection="Horizontal" CssClass ="opt_single">
                    <asp:ListItem Value="G" Selected="True">BG</asp:ListItem>
                    <asp:ListItem Value="C">Cheque</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_general" > Flag Kumulatif </label>
                <asp:RadioButtonList ID="rdoCumm" runat="server" RepeatDirection="Horizontal" CssClass ="opt_single">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
          <uc2:payment id="paymentAllDet" runat="server"/>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server"  Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
        </asp:Panel>
    </form>
</body>
</html>
