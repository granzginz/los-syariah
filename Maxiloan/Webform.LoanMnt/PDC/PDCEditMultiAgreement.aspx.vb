﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCEditMultiAgreement
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents txtAmountPayment As ucNumberFormat


#Region "Constanta"
    Protected WithEvents oLookupAgreement As UCLookupAgreement
    Protected WithEvents cboBank As System.Web.UI.WebControls.DropDownList
    Private oControllerAgreement As New PDCMultiAgreementController
    Private oCustomclass As New Parameter.PDCMultiAgreement
    Private oController As New PDCEditMultiAgreementController
    Protected WithEvents rfvcboBank As System.Web.UI.WebControls.RequiredFieldValidator
    Private TotalAmountReceive As Double
    Private m_controller As New DataUserControlController
#End Region
#Region "Property"
    Private Property SelectMode() As Boolean
        Get
            Return (CType(viewstate("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("SelectMode") = Value
        End Set
    End Property
    Private Property BankId() As String
        Get
            Return (CType(Viewstate("BankId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankId") = Value
        End Set
    End Property
    Private Property TablePDC() As DataTable
        Get
            Return (CType(Viewstate("TablePDC"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TablePDC") = Value
        End Set
    End Property

    Private Property OldTablePDC() As DataTable
        Get
            Return (CType(Viewstate("OldTablePDC"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("OldTablePDC") = Value
        End Set
    End Property

    Private Property PDCReceiptNo() As String
        Get
            Return (CType(viewstate("PDCReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property

    Private Property GiroNo() As String
        Get
            Return (CType(viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property
    Private Property PDCAmount() As Double
        Get
            Return (CType(viewstate("PDCAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("PDCAmount") = Value
        End Set
    End Property

    Private Property BranchPDC() As String
        Get
            Return CStr(viewstate("BranchPDC"))
        End Get
        Set(ByVal Value As String)
            viewstate("BranchPDC") = Value
        End Set
    End Property

    Private Property BranchLocation() As String
        Get
            Return CStr(Viewstate("BranchLocation"))
        End Get
        Set(ByVal Value As String)
            viewstate("BranchLocation") = Value
        End Set
    End Property

    Private Property TotalPDCGroupAmount() As Double
        Get
            Return CDbl(viewstate("TotalPDCGroupAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalPDCGroupAmount") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.PDCReceiptNo = Request.QueryString("ReceiptNo")
            Me.GiroNo = Request.QueryString("GiroNo")
            Me.FormID = "PDCEDIT"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                With oCustomclass
                    .strConnection = GetConnectionString
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .GiroNo = Me.GiroNo
                    .PDCReceiptNo = Me.PDCReceiptNo
                End With
                oCustomclass = oController.GetEditPDCMultiAgreement(oCustomclass)
                With oCustomclass
                    txtsdate.Text = .PDCDueDate.ToString("dd/MM/yyyy")
                    txtReceiveFrom.Text = .ReceivedFrom
                    Me.BankId = .BankID.Trim
                    txtPDCNo.Text = Me.GiroNo
                    txtPDCNo.NavigateUrl = "javascript:OpenWinPDC('" & txtPDCNo.Text.Trim & "','" & Me.sesBranchId.Replace("'", "").Trim & "','0')"
                    Me.PDCAmount = .PDCAmount
                    lblPDCAmount.Text = FormatNumber(.PDCAmount, 2)
                    If .PDCType = "G" Then
                        rdoPDCType.SelectedIndex = 0
                    ElseIf .PDCType = "Cheque" Then
                        rdoPDCType.SelectedIndex = 1
                    End If
                    If .IsCumm Then
                        rdoCumm.SelectedIndex = 1
                    Else
                        rdoCumm.SelectedIndex = 0
                    End If
                    Me.TablePDC = .AgreementList
                    Me.OldTablePDC = Me.TablePDC
                    Me.BranchPDC = .BranchPDC
                    DtgAgreement.DataSource = Me.TablePDC.DefaultView
                    DtgAgreement.DataBind()
                    Me.TotalPDCGroupAmount = .TotalPDCGroupAmount
                End With
                fillcboBank()
                oLookupAgreement.Style = "ACCMNT"
                pnlPDC.Visible = True
            End If
        End If
    End Sub
#Region "FillCboBank"
    Sub fillcboBank()
        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankName(GetConnectionString)
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
        cboBank.SelectedIndex = cboBank.Items.IndexOf(cboBank.Items.FindByValue(Me.BankId))
    End Sub
#End Region
#Region "Data Grid Item Data Bound"
    Private Sub DtgAgreement_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreement.ItemDataBound
        Dim lblTemp As Label
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblAmountPayment As ucNumberFormat
        Dim lblSumAmountPayment As Label
        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            Dim hyTemp As HyperLink
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName1"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
            lblAmountPayment = CType(e.Item.FindControl("txtAmountPayment"), ucNumberFormat)
            TotalAmountReceive += CDbl(IIf(lblAmountPayment.Text.Trim = "", 0, lblAmountPayment.Text.Trim))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblSumAmountPayment = CType(e.Item.FindControl("lblSumAmountPayment"), Label)
            lblSumAmountPayment.Text = FormatNumber(TotalAmountReceive, 2)
        End If
    End Sub
    Private Sub DtgAgreement_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgreement.ItemCommand
        If e.CommandName = "DELETE" Then
            Me.TablePDC.Rows.RemoveAt(CInt(e.Item.ItemIndex()))
            DtgAgreement.DataSource = Me.TablePDC.DefaultView
            DtgAgreement.DataBind()
        End If
    End Sub
#End Region

#Region "Process ADD ApplicationID"
    Private Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If Me.TablePDC Is Nothing Then
            Me.TablePDC = GetStructPDC()
        End If
        If oLookupAgreement.ApplicationID <> "" Then
            If CheckDuplikasiApplicationID(oLookupAgreement.ApplicationID) Then

                SaveDataGridToDataTable()
                Dim ds As New DataTable
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ApplicationID = oLookupAgreement.ApplicationID
                    .BranchId = oLookupAgreement.BranchAgreement
                End With
                ds = oControllerAgreement.AgreementInformation(oCustomclass)
                GenerateTable(ds)
                DtgAgreement.DataSource = Me.TablePDC.DefaultView
                DtgAgreement.DataBind()
                oLookupAgreement.AgreementNo = ""
                oLookupAgreement.ApplicationID = ""
                oLookupAgreement.BranchAgreement = ""
                pnlPDC.Visible = True
            End If
        End If
    End Sub
#End Region

#Region "Generate Table"
    Private Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("PDCMultiAgreement")
        With lObjDataTable
            .Columns.Add("BranchAgreement", System.Type.GetType("System.String"))
            .Columns.Add("ApplicationID", System.Type.GetType("System.String"))
            .Columns.Add("AgreementNo", System.Type.GetType("System.String"))
            .Columns.Add("CustomerID", System.Type.GetType("System.String"))
            .Columns.Add("Name", System.Type.GetType("System.String"))
            .Columns.Add("CustomerType", System.Type.GetType("System.String"))
            .Columns.Add("Address", System.Type.GetType("System.String"))
            .Columns.Add("ApplicationStep", System.Type.GetType("System.String"))
            .Columns.Add("ContractStatus", System.Type.GetType("System.String"))
            .Columns.Add("InstallmentAmount", System.Type.GetType("System.String"))
            .Columns.Add("ContractPrepaidAmount", System.Type.GetType("System.String"))
            .Columns.Add("AmountPayment", System.Type.GetType("System.String"))
            .Columns.Add("CanBeEdit", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function

    Private Sub GenerateTable(ByVal dtAgreement As DataTable)
        Dim lObjDataRow As DataRow
        Dim i As Integer
        'strZero = ""
        For i = 0 To dtAgreement.Rows.Count - 1
            lObjDataRow = Me.TablePDC.NewRow()
            If CheckDuplikasiApplicationID(dtAgreement.Rows(i).Item("ApplicationID").ToString) Then
                lObjDataRow("BranchAgreement") = dtAgreement.Rows(i).Item("BranchID")
                lObjDataRow("ApplicationID") = dtAgreement.Rows(i).Item("ApplicationID")
                lObjDataRow("AgreementNo") = dtAgreement.Rows(i).Item("AgreementNo")
                lObjDataRow("CustomerID") = dtAgreement.Rows(i).Item("CustomerID")
                lObjDataRow("Name") = dtAgreement.Rows(i).Item("Name")
                lObjDataRow("CustomerType") = dtAgreement.Rows(i).Item("CustomerType")
                lObjDataRow("Address") = dtAgreement.Rows(i).Item("Address")
                lObjDataRow("ApplicationStep") = dtAgreement.Rows(i).Item("ApplicationStep")
                lObjDataRow("ContractStatus") = dtAgreement.Rows(i).Item("ContractStatus")
                lObjDataRow("InstallmentAmount") = dtAgreement.Rows(i).Item("InstallmentAmount")
                lObjDataRow("AmountPayment") = dtAgreement.Rows(i).Item("InstallmentAmount")
                lObjDataRow("CanBeEdit") = "1"
                Me.TablePDC.Rows.Add(lObjDataRow)
            End If
        Next i
    End Sub

    Private Sub SaveDataGridToDataTable()
        Dim lObjDataRow As DataRow
        Dim i As Integer
        Dim hyApplicationID As HyperLink
        Dim hyagreementno As HyperLink
        Dim hyCustomerName As HyperLink
        Dim lblCustomerType As Label
        Dim lblCustomerID As Label
        Dim lblAddress As Label
        Dim lblApplicationStep As Label
        Dim lblContractStatus As Label
        Dim lblInstallment As Label
        'Dim txtAmountPayment As TextBox
        Dim txtAmountPayment As ucNumberFormat
        Dim lblBranchAgreement As Label
        Dim lblCanBeEdit As Label
        'strZero = ""
        Me.TablePDC.Clear()
        For i = 0 To DtgAgreement.Items.Count - 1
            lObjDataRow = Me.TablePDC.NewRow()
            hyApplicationID = CType(DtgAgreement.Items(i).FindControl("hyApplicationID"), HyperLink)
            hyagreementno = CType(DtgAgreement.Items(i).FindControl("hyagreementno"), HyperLink)
            hyCustomerName = CType(DtgAgreement.Items(i).FindControl("hyCustomerName1"), HyperLink)
            lblBranchAgreement = CType(DtgAgreement.Items(i).FindControl("lblBranchAgreement"), Label)
            lblCustomerType = CType(DtgAgreement.Items(i).FindControl("lblCustomerType"), Label)
            lblCustomerID = CType(DtgAgreement.Items(i).FindControl("lblCustomerID"), Label)
            lblAddress = CType(DtgAgreement.Items(i).FindControl("lblAddress"), Label)
            lblApplicationStep = CType(DtgAgreement.Items(i).FindControl("lblApplicationStep"), Label)
            lblContractStatus = CType(DtgAgreement.Items(i).FindControl("lblContractStatus"), Label)
            lblInstallment = CType(DtgAgreement.Items(i).FindControl("lblInstallment"), Label)
            txtAmountPayment = CType(DtgAgreement.Items(i).FindControl("txtAmountPayment"), ucNumberFormat)
            lblCanBeEdit = CType(DtgAgreement.Items(i).FindControl("lblCanBeEdit"), Label)

            lObjDataRow("BranchAgreement") = lblBranchAgreement.Text
            lObjDataRow("ApplicationID") = hyApplicationID.Text
            lObjDataRow("AgreementNo") = hyagreementno.Text
            lObjDataRow("CustomerID") = lblCustomerID.Text
            lObjDataRow("Name") = hyCustomerName.Text
            lObjDataRow("CustomerType") = lblCustomerType.Text
            lObjDataRow("Address") = lblAddress.Text
            lObjDataRow("ApplicationStep") = lblApplicationStep.Text
            lObjDataRow("ContractStatus") = lblContractStatus.Text
            lObjDataRow("InstallmentAmount") = lblInstallment.Text
            lObjDataRow("AmountPayment") = txtAmountPayment.Text
            lObjDataRow("CanBeEdit") = lblCanBeEdit.Text
            Me.TablePDC.Rows.Add(lObjDataRow)
        Next
    End Sub
#End Region

#Region "Cancel Button "
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("PDCEdit.aspx")
    End Sub

    Private Sub btnCancelNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        If Me.TablePDC Is Nothing Or Me.TablePDC.Rows.Count = 0 Then
            Response.Redirect("PDCEdit.aspx")
        Else
            Me.TablePDC.Clear()
            oLookupAgreement.AgreementNo = ""
            pnlPDC.Visible = False
        End If
    End Sub
#End Region

#Region "Check Duplikasi Application ID"
    Private Function CheckDuplikasiApplicationID(ByVal strApplicationID As String) As Boolean
        Dim i As Integer
        Dim isValid As Boolean = True
        Dim hyApplicationID As HyperLink
        For i = 0 To DtgAgreement.Items.Count - 1
            hyApplicationID = CType(DtgAgreement.Items(i).FindControl("hyApplicationID"), HyperLink)
            If hyApplicationID.Text = strApplicationID Then
                isValid = False
            End If
        Next
        Return isValid
    End Function
#End Region

#Region "Check Amount PDC"
    Private Function CheckAmountPDC() As Boolean
        'Dim txtAmountPayment As TextBox
        Dim txtAmountPayment As ucNumberFormat
        Dim isValid As Boolean = True
        Dim Amount As Double = 0
        Dim I As Integer
        For I = 0 To DtgAgreement.Items.Count - 1
            txtAmountPayment = CType(DtgAgreement.Items(I).FindControl("txtAmountPayment"), ucNumberFormat)
            Amount += CDbl(txtAmountPayment.Text)
        Next
        If CDbl(lblPDCAmount.Text.Trim) <> Amount Then
            isValid = False
        End If
        Return isValid
    End Function
#End Region

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If CheckAmountPDC() Then
            SaveDataGridToDataTable()
            With oCustomclass
                .strConnection = GetConnectionString()
                .ReceivedFrom = CStr(txtReceiveFrom.Text)
                .BusinessDate = Me.BusinessDate
                .BankPDC = cboBank.SelectedValue
                .IsInkaso = False
                .IsCumm = CBool(rdoCumm.SelectedValue)
                .PDCType = rdoPDCType.SelectedItem.Value
                .PDCReceiptNo = Me.PDCReceiptNo
                .GiroNo = txtPDCNo.Text.Trim
                .PDCAmount = CDbl(lblPDCAmount.Text)
                .PDCDueDate = ConvertDate2(txtsdate.Text)
                .BranchPDC = Me.BranchPDC
                .BranchLocation = Me.sesBranchId.Replace("'", "")
                .NumPDC = 1
                .TablePDC = Me.TablePDC
                .OldTablePDC = Me.OldTablePDC
                .TotalPDCGroupAmount = Me.TotalPDCGroupAmount
            End With
            Try
                oController.SaveEditPDCMultiAgreement(oCustomclass)
                Response.Redirect("PDCEdit.aspx")
            Catch ex As Exception

                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Else

            ShowMessage(lblMessage, "Jumlah PDC harus sama dengan Alokasi", True)
        End If
    End Sub

End Class