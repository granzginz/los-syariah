﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCChangeStatus
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchby As UcSearchBy
    Dim temptotalPDC As Double

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCChangeStatusController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PDCCHANGESTATUS"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                oSearchBy.ListData = "GiroNo, PDC No.-BANKID,Bank PDC"
                oSearchBy.BindData()


                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If

    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.PDCStatusList(oCustomClass)

        DtUserList = oCustomClass.ListPDC
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblBranchID As Label
        Dim lblGirono2 As HyperLink
        Dim lblstatus As Label
        Dim lblstatus2 As Label
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim hypGiroNo As HyperLink
        Dim flagfile As New Label
        Dim NewlblPDCreceiptno As Label
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        flagfile.Text = "0"
        If sessioninvalid() Then
            Exit Sub
        End If

        If e.Item.ItemIndex >= 0 Then
            lblGirono2 = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
            lblstatus = CType(e.Item.FindControl("lblPDCStatus"), Label)
            NewlblPDCreceiptno = CType(e.Item.FindControl("lblPDCreceiptno"), Label)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)
            hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)

            hypReceive.NavigateUrl = "PDCChangeStatusDet.aspx?PDCReceiptNo=" & NewlblPDCreceiptno.Text.Trim & "&Girono=" & lblGirono2.Text.Trim & "&BranchID=" & lblBranchID.Text.Trim & "&status=" & lblstatus.Text.Trim
            hypGiroNo = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
            hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & lblGirono2.Text.Trim & "','" & NewlblPDCreceiptno.Text.Trim & "','" & Me.sesBranchId.Replace("'", "") & "','" & flagfile.Text.Trim & "')"
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PDCChangeStatus.aspx")
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Me.SearchBy = " BranchLocation = '" & Me.sesBranchId.Replace("'", "") & "' "
            If cboStatus.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and PDCStatus = '" & cboStatus.SelectedItem.Value & "'"
            Else
                Me.SearchBy = Me.SearchBy & " and PDCStatus in ('HD','OP','BC')"
            End If
            If osearchby.Text.Trim <> "" Then
                If Right(osearchby.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & osearchby.ValueID & " like '" & osearchby.Text.Trim.Replace("'", "''") & "'"
                Else
                    Me.SearchBy = Me.SearchBy & " and " & osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'"
                End If

            End If
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class