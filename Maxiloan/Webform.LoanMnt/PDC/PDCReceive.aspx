﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCReceive.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PDCReceive" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../Webform.UserController/UcBankName.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCReceive</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
</head>
<body>    
    <script type = "text/javascript">
            function preventMultipleSubmissions() {
                $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
            }
            window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    TERIMA PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblBranchID" runat="server"></asp:Label>
            </div>           
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="hyAgreementNo" runat="server">HyperLink</asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="hyCustomerName" runat="server">HyperLink</asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Angsuran
                </label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jatuh Tempo Berikutnya
                </label>
                <asp:Label ID="lblNInsDate" runat="server"></asp:Label>&nbsp;&nbsp;|&nbsp;Angsuran&nbsp;
                <asp:Label ID="lblNextInstallmentNumber" runat="server" Width="3px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Terima Dari
                </label>
                <asp:TextBox ID="txtReceiveFrom" runat="server" MaxLength="50" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Visible="true"
                    ControlToValidate="txtReceiveFrom" Display="Dynamic" ErrorMessage="Harap diisi Terima dari"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Periode Jatuh Tempo
                </label>
                <asp:Label ID="lblPaymentFrequency" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    DETAIL PDC
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    No PDC
                </label>
                <asp:TextBox ID="txtPDCNo" runat="server" MaxLength="20" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Visible="true"
                    ControlToValidate="txtPDCNo" ErrorMessage="Harap isi No PDC" Display="Dynamic"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Jumlah
                </label>
                <uc1:ucnumberformat id="txtPDCAmount" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Bank PDC
                </label>
                <uc1:ucbankname id="cboBank" runat="server"></uc1:ucbankname>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Flag Inkaso
                </label>
                <asp:RadioButtonList ID="rdoInkaso" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Jumlah Lembar PDC
                </label>
                <uc1:ucnumberformat id="txtNoPDC" runat="server" /> 
            </div>
            <div class="form_right">
                <label class="label_general">
                    Flag Kumulatif
                </label>
                <asp:RadioButtonList ID="rdoCumm" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Jenis PDC
                </label>
                <asp:RadioButtonList ID="rdoPDCType" runat="server" RepeatDirection="Horizontal"
                    CssClass="opt_single">
                    <asp:ListItem Value="G" Selected="True">BG</asp:ListItem>
                    <asp:ListItem Value="C">Cheque</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" ausesValidation="True" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPDC" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPDCList" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyField="seq"
                        CssClass="grid_general" ShowFooter="True" Visible="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                        CausesValidation="False" CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LIBUR">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HiddenField id="hdSeq" runat="server" Value='<%#Eval("Seq")%>'/>
                                    <asp:Label ID="lblHoliday" runat="server" Text=' <%#Eval("Holiday")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BANK PDC">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBankPDC" runat="server" Text='<%#Eval("BankPDC")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="Total"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO PDC">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:textbox ID="lblPDCNo2" runat="server" Text='<%#Eval("PDCNo")%>'> </asp:textbox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Eval("PDCAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
<%--                            <asp:BoundColumn DataField="DueDate" HeaderText="TGL JATUH TEMPO" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                            </asp:BoundColumn>--%>
                            <asp:TemplateColumn SortExpression="DueDate" HeaderText="TGL JATUH TEMPO">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="lblPDCDueDate" runat="server" CssClass="small_text" Text='<%# DataBinder.Eval(Container.DataItem, "DueDate", "{0:d/M/yyyy}") %>'>
                                    </asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="lblPDCDueDate"
                                    Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="INKASO">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInkaso" runat="server" Text='<%#Iif(Eval("IsInkaso")="True" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KUMULATIF">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCumm" runat="server" Text='<%#Iif(Eval("IsCummulative")="True" , "Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TYPE">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%#Eval("Type")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
