﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PDCInquiryIncomplete
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController
    Private m_controller As New DataUserControlController
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PDCRINQINCOMPLETE"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim dtbranch As New DataTable
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With CboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                pnlList.Visible = True
                pnlDatagrid.Visible = False
            End If
        End If
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        BindGridInqPDCIncomplete(Me.CmdWhere, Me.SortBy)
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridInqPDCIncomplete(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInqPDCIncomplete(Me.CmdWhere, Me.SortBy)
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGridInqPDCIncomplete(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oInqPDCIncomplete As New Parameter.PDCReceive

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
        End With

        oInqPDCIncomplete = oController.InqPDCIncomplete(oCustomClass)

        With oInqPDCIncomplete
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInqPDCIncomplete.ListPDC
        dtvEntity = dtsEntity.DefaultView

        'dtvEntity.Sort = sortBy
        DtgPDCIncomplete.DataSource = dtvEntity
        Try
            DtgPDCIncomplete.DataBind()
        Catch
            DtgPDCIncomplete.CurrentPageIndex = 0
            DtgPDCIncomplete.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
#Region "DataBound"
    Private Sub DtgPDCIncomplete_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPDCIncomplete.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationid As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lnkCust = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"
        End If
    End Sub
#End Region
#Region "SortCommand"
    Private Sub DtgPDCIncomplete_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgPDCIncomplete.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInqPDCIncomplete(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "ResetClick"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PDCInquiryIncomplete.aspx")
    End Sub
#End Region
#Region "SearchClick"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            Dim filterBy As String
            Me.SearchBy = ""
            Me.BindMenu = ""
            Me.CmdWhere = ""
            Me.SortBy = ""
            Dim PDCOnHand As Integer
            If TxtPDCOnHand.Text <> "" Then
                PDCOnHand = CType(TxtPDCOnHand.Text.Trim, Integer)
                cmdwhere = cmdwhere + "PDCOnHand <= " & PDCOnHand & " and "
                filterBy = filterBy + "PDC On Hand <= " & TxtPDCOnHand.Text.Trim & " and "
            End If
            If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
                If cboSearchBy.SelectedItem.Value.Trim = "1" Then
                    If txtSearchBy.Text.Trim <> "" Then
                        If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                            cmdwhere = cmdwhere + "customername like '" & txtSearchBy.Text.Trim & "' and "
                            filterBy = filterBy + "Customer Name like = " & txtSearchBy.Text.Trim & " and "
                        Else
                            cmdwhere = cmdwhere + "customername = '" & txtSearchBy.Text.Trim & "' and "
                            filterBy = filterBy + "Customer Name = " & txtSearchBy.Text.Trim & " and "
                        End If
                    End If
                ElseIf cboSearchBy.SelectedItem.Value.Trim = "2" Then
                    If txtSearchBy.Text.Trim <> "" Then
                        If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                            cmdwhere = cmdwhere + "agreementNo like '" & txtSearchBy.Text.Trim & "%' and "
                            filterBy = filterBy + "agreementNo like = " & txtSearchBy.Text.Trim & " and "
                        Else
                            cmdwhere = cmdwhere + "agreementNo = '" & txtSearchBy.Text.Trim & "' and "
                            filterBy = filterBy + "agreementNo = " & txtSearchBy.Text.Trim & " and "
                        End If

                    End If
                End If
            End If
            cmdwhere = cmdwhere + "branchID = '" & CboParent.SelectedItem.Value.Trim & "'"
            filterBy = filterBy + "Branch = " & CboParent.SelectedItem.Text.Trim & ""
            Me.CmdWhere = cmdwhere
            Me.FilterBy = filterBy
            BindGridInqPDCIncomplete(Me.CmdWhere, Me.SortBy)
            pnlList.Visible = True
            pnlDatagrid.Visible = True
        End If
    End Sub
#End Region


End Class