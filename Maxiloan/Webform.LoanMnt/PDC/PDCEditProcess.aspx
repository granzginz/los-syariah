﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCEditProcess.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCEditProcess" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../Webform.UserController/UcBankName.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCEditProcess</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    EDIT TRANSAKSI PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblBranchID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="hyAgreementNo" runat="server">HyperLink</asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="hyCustomerName" runat="server">HyperLink</asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Angsuran
                </label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jatuh Tempo Berikutnya
                </label>
                <asp:Label ID="lblNInsDate" runat="server"></asp:Label>
                &nbsp;|&nbsp;Angsuran&nbsp;
                <asp:Label ID="lblNextInstallmentNumber" Width="3px" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Terima Dari
                </label>
                <asp:Label ID="txtReceiveFrom" runat="server" Width="180px"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Tanda Terima
                </label>
                <asp:Label ID="LblReceiptNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:HyperLink ID="txtPDCNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Jumlah
                </label>
                <%--<asp:Label ID="txtPDCAmount" runat="server"></asp:Label>--%>
                <asp:TextBox ID="txtPDCAmount" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Bank PDC
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboBank" runat="server" ControlToValidate="cboBank"
                    ErrorMessage="Harap pilih Bank" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Kumulatif
                </label>
                <asp:RadioButtonList ID="rdoCumm" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="form_right">
                <label>
                    Jenis PDC
                </label>
                <asp:RadioButtonList ID="rdoPDCType" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="G" Selected="True">BG</asp:ListItem>
                    <asp:ListItem Value="C">Cheque</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <asp:Panel ID="pnlIsInkaso" runat="server">
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Flag Inkaso
                    </label>
                    <asp:RadioButtonList ID="rdoInkaso" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">Yes</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="form_right">
                </div>
            </div>
        </asp:Panel>
        <div class="form_box">
            <div class="form_single">
                <label>
                    DETAIL TRANSAKSI PDC
                </label>
            </div>
        </div>
       
            <div class="form_box_uc">
                <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
            </div>
         
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" CausesValidation="True" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPDC" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPDCList" runat="server" Width="100%" Visible="False" AutoGenerateColumns="False"
                        CssClass="grid_general" ShowFooter="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBankPDC" runat="server" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCNo2" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
