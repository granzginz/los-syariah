﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCMultiAgreementList
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        If Not Page.IsPostBack Then
            If IsSingleBranch() Then
                txtGoPage.Text = "1"
                If CheckForm(Me.Loginid, "PDCMULTIAGREEMENT", "MAXILOAN") Then
                    If Request("cond") <> "" Then
                        Me.SearchBy = Request("cond")
                    Else
                        Me.SearchBy = "ALL"
                    End If
                    Me.SortBy = "Name ASC"
                    BindGrid(Me.SearchBy)
                    InitialDefaultPanel()
                End If
            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/NotAuthorized.html")
            End If
        End If
    End Sub
#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imgBadType As System.Web.UI.WebControls.Image
        Dim BadType As String = e.Item.Cells(4).Text
        If e.Item.ItemIndex >= 0 Then
            imgBadType = CType(e.Item.FindControl("imgBadType"), System.Web.UI.WebControls.Image)
            If BadType = "B" Then
                imgBadType.ImageUrl = "../../images/red.gif"
            ElseIf BadType = "W" Then
                imgBadType.ImageUrl = "../../images/yellow.gif"
            Else
                imgBadType.ImageUrl = "../../images/green.gif"
            End If
            Dim lnkCustName As LinkButton
            lnkCustName = CType(e.Item.FindControl("lnkCustName"), LinkButton)
            lnkCustName.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(5).Text & "','AccMnt');")
        End If
    End Sub
    Sub InitialDefaultPanel()
        pnlList.Visible = False
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Application
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.SortBy
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.isRO = True
        oCustomClass = m_controller.GetApplication(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If sessioninvalid() Then
            Exit Sub
        End If
        If e.Item.ItemIndex >= 0 Then
            If e.CommandName = "Receive" Then
                Dim oCustomer As Parameter.Customer
                Dim CustId As String = e.Item.Cells(5).Text.Trim
                Dim CustName As String = CType(e.Item.Cells(1).FindControl("lnkCustName"), LinkButton).Text
                Dim Type As String = e.Item.Cells(2).Text.Trim
                Response.Redirect("PDCMultiAgreement.aspx?CustomerID=" & CustId & "&name=" & CustName & "&type=" & Type & "")
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy)
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.SearchBy = "ALL"
        InitialDefaultPanel()
    End Sub
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 0 Then
                Me.SearchBy = cboSearch.SelectedItem.Value + " like '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
            Else
                Me.SearchBy = cboSearch.SelectedItem.Value + " like '%" + txtSearch.Text.Trim + "%'"
            End If
        Else
            Me.SearchBy = "ALL"
        End If
        BindGrid(Me.SearchBy)
    End Sub
#End Region

    Private Sub dtgPaging_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtgPaging.SelectedIndexChanged

    End Sub

End Class