﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCClearReconcile
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents obank As UcBankAccountID
    Protected WithEvents osearchby As UcSearchBy
    Dim intLoopGrid As Integer    
    Dim temptotalPDC As Double        

#Region "Property"

    Private Property SelectAll2() As Double
        Get
            Return (CType(Viewstate("SelectAll"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("SelectAll") = Value
        End Set
    End Property

    Private Property strGroupID() As String
        Get
            Return (CType(Viewstate("strGroupID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strGroupID") = Value
        End Set
    End Property
    Private Property strVoucherNO() As String
        Get
            Return (CType(Viewstate("strVoucherNO"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strVoucherNO") = Value
        End Set
    End Property

    Private Property strjournalno() As String
        Get
            Return (CType(Viewstate("strjournalno"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strjournalno") = Value
        End Set
    End Property
    Private Property strReceiptNo() As String
        Get
            Return (CType(Viewstate("strReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strReceiptNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCClearReconcileController
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            Me.FormID = "PDCCLEARRECONCILE"
            obank.BankPurpose = ""
            obank.BankType = "B"
            obank.BindBankAccount()
            oSearchBy.ListData = "GiroNo, PDC No."
            oSearchBy.BindData()
            ' Me.SelectAll2 = 0
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        Else
            Me.SelectAll2 = Me.SelectAll2
        End If
    End Sub
#End Region

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)

        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spPDCCLearReconListPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()

        If DtgAgree.Items.Count > 0 Then
            BtnNext.Visible = True
        Else
            BtnNext.Visible = False
        End If

        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub
#End Region

#Region "DoBind2"
    Sub DoBind2(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
            .BusinessDate = Me.BusinessDate
        End With
        oCustomClass = oController.PDCClearReconList(oCustomClass)
        DtUserList = oCustomClass.listPDCClear
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        DtGridClear.DataSource = DvUserList

        '  Try
        DtGridClear.DataBind()
        ' Catch
        '    DtGridDeposit.DataBind()
        ' End Try

        lblPDCDueDate.Text = ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy")
        lblBankPDC.Text = obank.BankAccountID

        lblMessage.Text = ""
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.SearchBy = " branchid = '" & Replace(Me.sesBranchId, "'", "") & "' AND PDCDueDate <= '" & ConvertDate2(txtsdate.Text) & "'"
        If obank.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " and  a.bankaccountid = '" & obank.BankAccountID & "'"
        End If
        If osearchby.Text.Trim <> "" Then
            If Right(osearchby.Text.Trim, 1) = "%" Then
                Me.SearchBy &= " and " & osearchby.ValueID & " Like '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy &= " and " & osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            End If

        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid2(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind2(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Select All"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ChkSelectAll As CheckBox
        Dim ChkSelect As CheckBox
        Dim x As Integer
        Dim chk As New CheckBox

        For x = 0 To DtgAgree.Items.Count - 1
            chk = CType(DtgAgree.Items(x).FindControl("ChkSlct"), CheckBox)
            If CType(sender, CheckBox).Checked Then
                chk.Checked = True
                Me.SelectAll2 = 1
            Else
                chk.Checked = False
                Me.SelectAll2 = 0
            End If
        Next
    End Sub
#End Region

#Region "Next"

    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim NewchkBox As CheckBox
            Dim lblPDCNONew As HyperLink
            Dim NlblVoucherno As Label
            Dim NlblJournalno As Label
            Dim NPDCReceiptNo As Label
            Dim strGroupID As String
            Dim strvoucherno As String
            Dim strjournalno As String
            Dim strReceiptNo As String
            Dim dtPDC As New DataTable


            strGroupID = ""
            strvoucherno = ""
            strjournalno = ""
            strReceiptNo = ""
            For intLoopGrid = 0 To DtgAgree.Items.Count - 1
                NewchkBox = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("ChkSlct"), CheckBox)
                lblPDCNONew = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblGiroNo"), HyperLink)
                NlblVoucherno = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblvoucherno"), Label)
                NlblJournalno = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lbljournalno"), Label)
                NPDCReceiptNo = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblPDCReceiptNo"), Label)

                If NewchkBox.Checked = True Then
                    strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & lblPDCNONew.Text.Replace("'", "") & "'"
                    strvoucherno &= CStr(IIf(strvoucherno = "", "", ",")) & "'" & NlblVoucherno.Text.Replace("'", "") & "'"
                    strjournalno &= CStr(IIf(strjournalno = "", "", ",")) & "'" & NlblJournalno.Text.Replace("'", "") & "'"
                    strReceiptNo &= CStr(IIf(strReceiptNo = "", "", ",")) & "'" & NPDCReceiptNo.Text.Replace("'", "") & "'"
                End If
            Next

            With oCustomClass
                oCustomClass.GroupPDCNo = Trim(strGroupID)
                oCustomClass.GroupVoucherno = Trim(strvoucherno)
                Me.strVoucherNO = oCustomClass.GroupVoucherno
                oCustomClass.GroupJournalno = Trim(strjournalno)
                Me.strjournalno = oCustomClass.GroupJournalno
                Me.strGroupID = oCustomClass.GroupPDCNo
                Me.strReceiptNo = Trim(strReceiptNo)
                oCustomClass.BranchId = Me.BranchID

            End With
            Me.SearchBy = " GiroNo in (" & Trim(strGroupID) & ") and PDCReceiptNo in (" & strReceiptNo.Trim & ")"

            DoBind2(Me.SearchBy, Me.SortBy)
            pnlList2.Visible = True
            pnlList.Visible = False
            DtgAgree.Visible = False
            pnlDatagrid.Visible = False
            DtGridClear.Visible = True
        End If
    End Sub
#End Region

#Region "DtgAgree ItemDataBound"
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim totalPDCAmount As New Label
        Dim lPDCAmount As New Label
        Dim flagfile As New Label
        Dim hypGiroNo As HyperLink
        Dim NlblGiroNo As HyperLink
        Dim NlblPDCReceiptno As Label
        Dim NhypClear As HyperLink
        Dim Nlblpbranchid As Label
        Dim Nlblinkaso As Label

        flagfile.Text = "0"

        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If


        If e.Item.ItemIndex >= 0 Then
            Dim lblTemp As Label

            NhypClear = CType(e.Item.FindControl("hypClear"), HyperLink)
            NlblPDCReceiptno = CType(e.Item.FindControl("lblPDCReceiptNo"), Label)
            Nlblpbranchid = CType(e.Item.FindControl("lblpbranchid"), Label)
            hypGiroNo = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
            ' hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & NlblPDCReceiptno.Text.Trim & "&branchid=" & Nlblpbranchid.Text.Trim & "&flagfile=" & flagfile.Text.Trim
            hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & hypGiroNo.Text.Trim & "','" & NlblPDCReceiptno.Text.Trim & "','" & Nlblpbranchid.Text.Trim & "','" & flagfile.Text.Trim & "')"

            If CheckFeature(Me.Loginid, Me.FormID, "CLIKS ", Me.AppId) Then
                lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
                NhypClear.NavigateUrl = "PDCClearReconC.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & NlblPDCReceiptno.Text.Trim & "&branchid=" & Nlblpbranchid.Text.Trim
            End If
        End If
    End Sub
#End Region

#Region "DtGridClear ItemDataBound"

    Private Sub DtGridClear_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtGridClear.ItemDataBound
        Dim totalPDCAmount As New Label
        Dim lPDCAmount As New Label
        Dim flagfile As New Label
        Dim hypGiroNo As HyperLink
        Dim NlblGiroNo As HyperLink
        Dim NlblPDCReceiptno As HyperLink
        Dim NhypClear As HyperLink
        Dim Nlblpbranchid As Label
        Dim Nlblinkaso As Label

        flagfile.Text = "0"

        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount2"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount2"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If


        If e.Item.ItemIndex >= 0 Then
            NhypClear = CType(e.Item.FindControl("hypClear2"), HyperLink)
            NlblPDCReceiptno = CType(e.Item.FindControl("lblPDCReceiptNo2"), HyperLink)
            Nlblpbranchid = CType(e.Item.FindControl("lblpbranchid2"), Label)
            hypGiroNo = CType(e.Item.FindControl("lblGiroNo2"), HyperLink)
            ' hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & NlblPDCReceiptno.Text.Trim & "&branchid=" & Nlblpbranchid.Text.Trim & "&flagfile=" & flagfile.Text.Trim
            'hypGiroNo.NavigateUrl = "javascript:OpenWinMain('" & hypGiroNo.Text.Trim & "','" & NlblPDCReceiptno.Text.Trim & "','" & Nlblpbranchid.Text.Trim & "','" & flagfile.Text.Trim & "')"
            hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & hypGiroNo.Text.Trim & "','" & NlblPDCReceiptno.Text.Trim & "','" & Nlblpbranchid.Text.Trim & "','" & flagfile.Text.Trim & "')"
        End If
    End Sub
#End Region

#Region "Clear"
    Private Sub Btnclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnclear.Click
        If CheckFeature(Me.Loginid, Me.FormID, "CLEAR", Me.AppId) Then

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .strConnection = GetConnectionString()
                .GroupPDCNo = Me.strGroupID
                .BusinessDate = Me.BusinessDate
                .GroupJournalno = Me.strjournalno
                .GroupVoucherno = Me.strVoucherNO
                .GroupReceiptNo = Me.strReceiptNo
            End With
            Try
                oCustomClass = oController.SavePDCClearRecon(oCustomClass)
                pnlList.Visible = True
                pnlList2.Visible = False
                DtGridClear.Visible = False
                If oCustomClass.strError <> "" Then

                    ShowMessage(lblMessage, "PDC sudah diCairkan user Lain", True)
                    Exit Sub
                Else

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                End If
            Catch ex As Exception

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)

                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("PDCClearReconcile.aspx")
    End Sub
#End Region

End Class