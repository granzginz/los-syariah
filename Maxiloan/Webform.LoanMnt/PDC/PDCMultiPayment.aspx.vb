﻿Imports System.Math
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class PDCMultiPayment
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents cbobank As UcBankName

    Protected WithEvents txtPDCAmount As ucNumberFormat
    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCMTransContorller 
    Protected WithEvents paymentAllDet As UcPaymentAllocationDetail
    Private Property PDCReceiptNo() As String
        Get
            Return CType(viewstate("PDCReceiptNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PDCReceiptNo") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.CustomerID = Request.QueryString("CustomerId")
            Me.FormID = "PDCMULTIPY"
            txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            paymentAllDet.ShowPanelCashier = False
            DoBind()

        End If
    End Sub

    Sub DoBind()
        'If Not CheckCashier(Me.Loginid) Then

        '    ShowMessage(lblMessage, "Kasir belum Buka", True)
        '    btnAddNew.Visible = False
        'Else

        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.BranchID
        Try
            oCustomClass = oController.PDCMTransList(oCustomClass)
            With oCustomClass
                lblBranchID.Text = .BranchName
                Me.AgreementNo = .Agreementno
                hyAgreementNo.Text = .Agreementno
                If hyAgreementNo.Text.Trim.Length > 0 Then
                    hyAgreementNo.NavigateUrl = String.Format("javascript:OpenAgreementNo('AccMnt', '{0}')", Server.UrlEncode(Me.ApplicationID.Trim))
                End If
                hyCustomerName.Text = .CustomerName
                If hyCustomerName.Text.Trim.Length > 0 Then
                    hyCustomerName.NavigateUrl = String.Format("javascript:OpenCustomer('AccMnt', '{0}')", Server.UrlEncode(Me.CustomerID))
                End If
                lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                lblNInsDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
                txtPDCAmount.Text = FormatNumber(.InstallmentAmount, 0)
                lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)
 
            End With 
         

        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
            'btnAddNew.Visible = False
            Exit Sub
        End Try
        'End If 
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("PDCMultiTransList.aspx")
    End Sub




    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim pDCSource As IList(Of Parameter.PDCParam) = New List(Of Parameter.PDCParam)
         
        If CDec(txtPDCAmount.Text) <= 0 Then 
            ShowMessage(lblMessage, "Jumlah  harus > 0 ", True)
            Exit Sub 
        End If


        If CDec(txtPDCAmount.Text) <> paymentAllDet.CountTotalBayar Then
            ShowMessage(lblMessage, "Jumlah  PDC Tidak sama dengan total Tagihan ", True)
            Exit Sub
        End If

        With oCustomClass 
            .LoginId = Me.Loginid
            .BranchId = Me.sesBranchId.Replace("'", "")
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ReceivedFrom = txtReceiveFrom.Text
            .BusinessDate = Me.BusinessDate
            .BankPDC = cbobank.BankID 
            .GrandTotPDCAmount = CDbl(txtPDCAmount.Text) 
            .BranchAgreement = Me.BranchID
            .GiroNo = txtPDCNo.Text

            .NextInstallmentDueDate = ConvertDate2(txtsdate.Text)
            .IsInkaso = CType(rdoInkaso.SelectedItem.Value, Boolean)
            .IsCumm = CType(rdoCumm.SelectedItem.Value, Boolean)
            .PDCType = rdoPDCType.SelectedItem.Value  
            '.PDCSource = Me.PDCSource 
        End With

        Dim tmpPrm = New Parameter.PDCParam With {
                .BankPDC = cbobank.BankName,
                .BankID = cbobank.BankID,
                .PDCNo = txtPDCNo.Text,
                .DueDate = ConvertDate2(txtsdate.Text),
                .IsInkaso = CType(rdoInkaso.SelectedItem.Value, Boolean),
                .IsCummulative = CType(rdoCumm.SelectedItem.Value, Boolean),
                .Type = rdoPDCType.SelectedItem.Value
            }

        oCustomClass.PDCSource = paymentAllDet.GetPDCDetails(tmpPrm)
        Try  
            Me.PDCReceiptNo = oController.SavePDCReceiveBp(GetConnectionString(), oCustomClass)
            Response.Redirect("PDCMultiTransList.aspx")
        Catch exp As Exception

            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
End Class