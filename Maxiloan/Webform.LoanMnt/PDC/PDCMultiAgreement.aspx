﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCMultiAgreement.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCMultiAgreement" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../Webform.UserController/UcBankName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCLookupAgreement" Src="../../Webform.UserController/UCLookupAgreement.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDC Multi Agreement</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>    
    <script language="javascript" type="text/javascript">
        function CalculateTotals(id) {
            var grd = document.getElementById("<%= DtgAgreement.ClientID %>");
            var row = grd.rows.length - 1;
            var totalP = 0;
            var totalPID = 0;

            $("#DtgAgreement tr").each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var persentase_ = $tds[9].getElementsByTagName('input')[0];
                    var persentase = persentase_.value;

                    if (persentase == "") {
                        persentase = "0";
                    }

                    if (i < row) {
                        totalP = parseFloat(totalP) + parseFloat(persentase.replace(/\s*,\s*/g, ''));
                    }
                    if (i == row) {
                        totalPID = persentase_.id;
                    }
                }                
                $('.TotalLabel').html(number_format(totalP, 2));
            });            
        }              
</script> 
    <script type="text/javascript">

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PDC MULTIPLE KONTRAK
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="hyCustomerName" runat="server">HyperLink</asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Terima Dari
                </label>
                <asp:TextBox ID="txtReceiveFrom" runat="server" Width="180px" MaxLength="50" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Visible="true"
                    ControlToValidate="txtReceiveFrom" Display="Dynamic" ErrorMessage="Harap diisi Terima Dari"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    No PDC
                </label>
                <asp:TextBox ID="txtPDCNo" runat="server" MaxLength="20" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Visible="true"
                    ControlToValidate="txtPDCNo" ErrorMessage="Harap diisi No PDC" Display="Dynamic"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Jumlah
                </label>
                <uc1:ucnumberformat id="txtPDCAmount" runat="server" />                
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:TextBox runat="server" ID="txtsdate" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Bank PDC
                </label>
                <uc1:ucbankname id="cboBank" runat="server"></uc1:ucbankname>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_general">
                    Flag Kumulatif
                </label>
                <asp:RadioButtonList ID="rdoCumm" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Jenis PDC
                </label>
                <asp:RadioButtonList ID="rdoPDCType" runat="server" RepeatDirection="Horizontal"
                    CssClass="opt_single">
                    <asp:ListItem Value="G" Selected="True">BG</asp:ListItem>
                    <asp:ListItem Value="C">Cheque</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box">
        <%--<div class="form_single">--%>
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <uc1:uclookupagreement id="oLookupAgreement" runat="server">
            </uc1:uclookupagreement>
        </div>
        <div class="form_right">
             <label class="label_req">
                    Jumlah Lembar PDC
             </label>
             <uc1:ucnumberformat id="txtNoPDC" runat="server" /> 
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnCopyNew" runat="server" CausesValidation="False" Text="Copy" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    <asp:Panel ID="pnlPDC" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgreement" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                        Width="100%" ShowFooter="True" DataKeyField="GiroNo" AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="GiroNo" HeaderText="PDC NO">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtGiroNo" runat="server" Text='<%#Container.DataItem("GiroNo")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>     
                            <asp:TemplateColumn SortExpression="PDCDueDate" HeaderText="PDC DUE DATE">
                                <HeaderStyle HorizontalAlign="Center" Height="10px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <%--<asp:TextBox ID="txtPDCDueDate" runat="server" CssClass="small_text" Text='<%# DataBinder.Eval(Container.DataItem, "PDCDueDate", "{0:dd/MM/yyyy}") %>'>--%>
                                    <%--<asp:TextBox ID="txtPDCDueDate" runat="server" CssClass="small_text" Text='<%#DateTime.Parse(Eval("PDCDueDate").ToString()).ToString("dd/MM/yyyy")%>'>--%>
                                    <asp:TextBox ID="txtPDCDueDate" runat="server" CssClass="small_text" Text='<%# Container.DataItem("PDCDueDate")  %>'>
                                    </asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtPDCDueDate"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>                                    
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblBranchAgreement" runat="server" Text='<%#Container.DataItem("branchagreement")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName1" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <uc1:ucnumberformat id="txtAmountPayment" runat="server" class="calculate"  OnClientChange="CalculateTotals(this.id)"
                                    Text='<%#formatnumber(Container.DataItem("AmountPayment"),0)%>'>
                                    </uc1:ucnumberformat>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblSumAmountPayment" runat="server" Class="TotalLabel"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AmountPayment" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
