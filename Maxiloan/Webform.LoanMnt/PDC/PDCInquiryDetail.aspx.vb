﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCInquiryDetail
    Inherits Maxiloan.Webform.WebBased
    Dim temptotalPDCPA As Double
    Dim temptotalPDCPAClear As Double    
    Dim temptotalPDCHis As Double
#Region "Property"


    Private Property PDCReceiptNo() As String
        Get
            Return (CType(Viewstate("PDCReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property

    Private Property GiroNo() As String
        Get
            Return (CType(Viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property
    Private Property flagfile() As String
        Get
            Return (CType(Viewstate("flagfile"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("flagfile") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblgiro As New Label
            Dim lblnewStat As New Label
            Dim inFlagfile As New Label
            Me.FormID = "PDCINQUIRYVIEWDETAIL"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.PDCReceiptNo = Request.QueryString("PDCReceiptNo")
                Me.BranchID = Request.QueryString("branchid")
                lblgiro.Text = Request.QueryString("girono")
                inFlagfile.Text = Request.QueryString("flagfile")
                Me.flagfile = inFlagfile.Text
                If Me.flagfile = "1" Then
                    BtnBack.Visible = True
                    pnlbutton.Visible = False
                Else
                    BtnBack.Visible = False
                    pnlbutton.Visible = True
                End If
                Me.GiroNo = lblgiro.Text
                lblnewStat.Text = Request.QueryString("status")


                DoBind()
                DoBindPA()
                DoBindHistory()

            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()
        Dim hypPDCReceiptno As HyperLink
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.GiroNo = Me.GiroNo
        oCustomClass = oController.PDCInquiryListDetail(oCustomClass)
        With oCustomClass
            lblPDCReceiptNoV.Text = .PDCReceiptNo

            lblPDCReceiptNoV.NavigateUrl = "javascript:OpenWinPDCView('" & Me.PDCReceiptNo.Trim & "','" & Me.BranchID.Trim & "','" & Me.flagfile.Trim & "','" & Me.GiroNo.Trim & "')"
            '"PDCRInquiryView.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & Me.BranchID.Trim & "&flagfile=" & Me.flagfile.Trim & "&Girono=" & Me.GiroNo.Trim
            lblReceiveDateV.Text = .RecDate.ToString("dd/MM/yyyy")
            lblAgreeBranchV.Text = .BranchLocationName
            HyAgreementNoV.Text = .Agreementno
            HyAgreementNoV.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
            HyCustomerNameV.Text = .CustomerName

            HyCustomerNameV.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID) & "')"
            lblGiroNo.Text = .GiroNo
            lblBankPDC.Text = .BankPDCName
            lblPDCAmount.Text = FormatNumber(.PDCAmount, 2)
            lblPDCDueDate.Text = .PDCDueDate.ToString("dd/MM/yyyy")
            lblPDCType.Text = .PDCType
            If .PDCType = "BG" Then
                lblPDCType.Text = "Bilyet Giro"
            Else
                lblPDCType.Text = "Cheque"
            End If
            If .BankClearing.Trim <> "-" Then
                lblBankClearing.Text = .BankClearingName
            Else
                lblBankClearing.Text = "-"
            End If

            If .IsInkaso Then
                lblInkasoFlag.Text = "Yes"
            Else
                lblInkasoFlag.Text = "No"
            End If
            If .IsCumm Then
                lblCummFlag.Text = "Yes"
            Else
                lblCummFlag.Text = "No"
            End If
            If .PDCStatus.Trim <> "Hold" Then
                lblHolddate.Text = "-"
            Else
                lblHolddate.Text = .HoldUntildate.ToString("dd/MM/yyyy")

            End If
            lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
            lblStatus.Text = .PDCStatus
            lblNumOfBounce.Text = CStr(.Bounce)

            'lblInkasoFlag.Text = IIf(.IsInkaso, "Yes", "No")
            'lblCummFlag.Text = IIf(.IsCumm, "Yes", "No")

        End With

    End Sub

    Sub DoBindPA()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.GiroNo = Me.GiroNo
        oCustomClass = oController.PDCInquiryDetail(oCustomClass)

        DtUserList = oCustomClass.listPDCPA
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgPA.DataSource = DvUserList

        Try
            DtgPA.DataBind()
        Catch
            'DtgPDC.CurrentPageIndex = 0
            DtgPA.DataBind()
        End Try
    End Sub

    Sub DoBindHistory()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.GiroNo = Me.GiroNo
        oCustomClass = oController.PDCInquiryDetail(oCustomClass)

        DtUserList = oCustomClass.listPDCHistory
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgPDC.DataSource = DvUserList

        Try
            DtgPDC.DataBind()
        Catch
            'DtgPDC.CurrentPageIndex = 0
            DtgPDC.DataBind()
        End Try
    End Sub
#End Region


    Private Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        'If Me.flagfile = "1" Then
        '    Response.Redirect("PDCInquiryView.aspx")

        'ElseIf Me.flagfile = "2" Then
        '    Response.Redirect("PDCRInquiryView.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & Me.BranchID.Trim)
        'ElseIf Me.flagfile = "3" Then
        '    Response.Redirect("PDCChangeStatus.aspx")
        'ElseIf Me.flagfile = "4" Then
        '    Response.Redirect("PDCToDeposit.aspx")
        'End If

        If Me.flagfile = "1" Then
            Response.Redirect("PDCRInquiryView.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & Me.BranchID.Trim & "&flagfile=" & Me.flagfile.Trim)
        End If
    End Sub

    Private Sub DtgPA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPA.ItemDataBound
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim lPDCAmountClear As Label
        Dim totalPDCAmountClear As Label
        Dim hypGiroNo As HyperLink


        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblAmount"), Label)
            temptotalPDCPA += CDbl(lPDCAmount.Text)
            lPDCAmountClear = CType(e.Item.FindControl("lblAmountClear"), Label)
            temptotalPDCPAClear += CDbl(lPDCAmountClear.Text)
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmt"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDCPA.ToString, 2)
            totalPDCAmountClear = CType(e.Item.FindControl("lblTotPDCAmtClear"), Label)
            totalPDCAmountClear.Text = FormatNumber(temptotalPDCPAClear.ToString, 2)
        End If
    End Sub

End Class