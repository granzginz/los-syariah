﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCEditListMultiAgreement
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCMTransContorller
    Dim orow As DataRow
#End Region
#Region "Property"
    Public Property GiroSeqNo() As Integer
        Get
            Return CType(viewState("GiroSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("GiroSeqNo") = Value
        End Set
    End Property
    Public Property PDCReceiptNo() As String
        Get
            Return CType(viewState("PDCReceiptNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PDCReceiptNo") = Value
        End Set
    End Property
    Public Property GiroNo() As String
        Get
            Return CType(viewState("GiroNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GiroNo") = Value
        End Set
    End Property
    Private Property PDCAmountRequest() As String
        Get
            Return (CType(Viewstate("PDCAmount"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCAmount") = Value
        End Set
    End Property
    Private Property BankId() As String
        Get
            Return (CType(Viewstate("BankId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankId") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PDCLISTMULTIAGR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.PDCReceiptNo = Request.QueryString("ReceiptNo")
                Me.GiroNo = Request.QueryString("GiroNo")
                Me.PDCAmountRequest = Request.QueryString("Amount")
                Me.BankId = Request.QueryString("bankId")
                LblGiroNo.NavigateUrl = "javascript:OpenWinPDC(" & Me.GiroNo & "," & Me.PDCReceiptNo & "," & Me.BranchID & ",'0')"
                BindAgreement()
            End If
        End If
    End Sub
    Public Sub BindAgreement()
        Dim DvAgreementList As New DataView
        Dim DtUserList As New DataTable
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .GiroNo = Me.GiroNo
            .PDCReceiptNo = Me.PDCReceiptNo
        End With

        oCustomClass = oController.GetPDCEditListMultiAgrementController(oCustomClass)
        DtUserList = oCustomClass.ListPDC
        orow = DtUserList.Rows(0)
        lblBranchAgreement.Text = orow.Item("BranchFullName").ToString.Trim
        LblPDCReceiptNo.Text = Me.PDCReceiptNo
        LblGiroNo.Text = Me.GiroNo
        DvAgreementList = DtUserList.DefaultView
        DvAgreementList.Sort = Me.SortBy
        DtgAgreementList.DataSource = DvAgreementList

        Try
            DtgAgreementList.DataBind()
        Catch
            DtgAgreementList.CurrentPageIndex = 0
            DtgAgreementList.DataBind()
        End Try
    End Sub
    Private Sub DtgAgreementList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreementList.ItemDataBound
        Dim lnkApp As HyperLink
        Dim lnkAgreementNo As HyperLink
        Dim lnkApplicationID As HyperLink
        Dim lblGiroSeqNo As Label
        Dim lblAmountRcv As Label
        Dim hyedit As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "')"
            End If


            If lnkApplicationID.Text.Trim.Length > 0 Then
                lnkApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "')"
            End If
            lblGiroSeqNo = CType(e.Item.FindControl("lblGiroSeqNo"), Label)
            lblAmountRcv = CType(e.Item.FindControl("lblAmountRcv"), Label)
            hyedit = CType(e.Item.FindControl("HyEdit"), HyperLink)
            hyedit.NavigateUrl = "PDCEditProcess.aspx?applicationid=" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "&PDCNo=" & Me.GiroNo & "&ReceiptNo=" & Me.PDCReceiptNo & "&Amount=" & Server.UrlEncode(lblAmountRcv.Text.Trim) & "&IsMultiAgreement=1&GiroSeqNo=" & Server.UrlEncode(lblGiroSeqNo.Text.Trim) & ""
        End If
    End Sub

End Class