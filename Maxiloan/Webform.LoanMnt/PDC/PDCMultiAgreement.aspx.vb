﻿#Region "Imports"
Imports System.Math
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.ComponentModel

#End Region

Public Class PDCMultiAgreement
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents txtPDCAmount As ucNumberFormat
    Protected WithEvents txtAmountPayment As ucNumberFormat
    Protected WithEvents txtNoPDC As ucNumberFormat

#Region "Constanta"
    Protected WithEvents oLookupAgreement As UCLookupAgreement
    Protected WithEvents cbobank As UcBankName
    Private oController As New PDCMultiAgreementController
    Private oCustomclass As New Parameter.PDCMultiAgreement
    Private isValid As Boolean = True
    'Private TotalAmountReceive As Double
#End Region

#Region "Property"
    Private Property SelectMode() As Boolean
        Get
            Return (CType(ViewState("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("SelectMode") = Value
        End Set
    End Property

    Private Property TablePDC() As DataTable
        Get
            Return (CType(ViewState("TablePDC"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TablePDC") = Value
        End Set
    End Property

    Private Property TotalAmountReceive() As Double
        Get
            Return (CType(ViewState("TotalAmountReceive"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmountReceive") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.CustomerID = Request.QueryString("CustomerId")
            Me.FormID = "PDCMULTI"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                oLookupAgreement.Style = "ACCMNT"
                hyCustomerName.Text = Request.QueryString("Name")
                If hyCustomerName.Text.Trim.Length > 0 Then
                    hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID) & "')"
                End If
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                pnlPDC.Visible = False
            End If
        End If
    End Sub

#Region "Data Grid Item Data Bound"
    Private Sub DtgAgreement_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreement.ItemDataBound
        Dim lblTemp As Label
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblInstallment As Label
        Dim lblAmountPayment As ucNumberFormat
        Dim lblSumAmountPayment As Label

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            Dim hyTemp As HyperLink
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName1"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
            lblInstallment = CType(e.Item.FindControl("lblInstallment"), Label)
            lblAmountPayment = CType(e.Item.FindControl("txtAmountPayment"), ucNumberFormat)
            'lblAmountPayment.Text = lblInstallment.Text
            lblAmountPayment.Text = lblAmountPayment.Text
            TotalAmountReceive += CDbl(IIf(lblAmountPayment.Text.Trim = "", 0, lblAmountPayment.Text.Trim))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblSumAmountPayment = CType(e.Item.FindControl("lblSumAmountPayment"), Label)
            lblSumAmountPayment.Text = FormatNumber(TotalAmountReceive, 0)
        End If
    End Sub
    Private Sub DtgAgreement_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgreement.ItemCommand
        If e.CommandName = "DELETE" Then
            Me.TablePDC.Rows.RemoveAt(CInt(e.Item.ItemIndex()))
            DtgAgreement.DataSource = Me.TablePDC.DefaultView
            DtgAgreement.DataBind()
        End If
    End Sub
#End Region

#Region "Process Copy And ADD ApplicationID"
    Private Sub btnCopyNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopyNew.Click
        If Me.TablePDC Is Nothing Then
            Me.TablePDC = GetStructPDC()
        End If
        SaveDataGridToDataTable()

        Dim ds As New DataTable
        With oCustomclass
            .strConnection = GetConnectionString()
            .CustomerID = Me.CustomerID
        End With
        ds = oController.ListAgreementCustomer(oCustomclass).AgreementList
        GenerateTable(ds)
        DtgAgreement.DataSource = Me.TablePDC.DefaultView
        DtgAgreement.DataBind()
        pnlPDC.Visible = True
    End Sub

    Private Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If Me.TablePDC Is Nothing Then
            Me.TablePDC = GetStructPDC()
        End If
        If oLookupAgreement.ApplicationID <> "" Then
            If CheckDuplikasiApplicationID(oLookupAgreement.ApplicationID) Then
                SaveDataGridToDataTable()
                Dim ds As New DataTable
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ApplicationID = oLookupAgreement.ApplicationID
                    .BranchId = oLookupAgreement.BranchAgreement
                    .GiroNo = txtPDCNo.Text
                    .NumPDC = CInt(txtNoPDC.Text)
                    .PDCDueDate = Date.ParseExact(txtsdate.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    .AmountReceive = txtPDCAmount.Text
                End With
                ds = oController.AgreementInformation(oCustomclass)
                GenerateTable(ds)
                DtgAgreement.DataSource = Me.TablePDC.DefaultView
                DtgAgreement.DataBind()
                oLookupAgreement.AgreementNo = ""
                oLookupAgreement.ApplicationID = ""
                oLookupAgreement.BranchAgreement = ""
                pnlPDC.Visible = True
            End If
        End If
    End Sub
#End Region

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If IsNumeric((Right(txtPDCNo.Text, 1))) Then
            'If CheckAmountPDC() Then
            'Wira
            CheckAmountPDC()
            If isValid = False Then
                ShowMessage(lblMessage, "Jumlah PDC Amount tidak sama dengan Total Jumlah Yang Dibayar", True)
                If (Not ClientScript.IsStartupScriptRegistered("alert")) Then
                    Page.ClientScript.RegisterStartupScript _
                    (Me.GetType(), "alert", "CalculateTotals('txtAmountPayment');", True)
                End If
                Exit Sub
            End If

            SaveDataGridToDataTable()
            With oCustomclass
                .strConnection = GetConnectionString()
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .ReceivedFrom = CStr(txtReceiveFrom.Text)
                .BusinessDate = Me.BusinessDate
                .BankPDC = cbobank.BankID
                .IsInkaso = False
                .IsCumm = CBool(rdoCumm.SelectedValue)
                .PDCType = rdoPDCType.SelectedItem.Value
                .GiroNo = txtPDCNo.Text.Trim
                .PDCAmount = CDbl(txtPDCAmount.Text)
                .PDCDueDate = Date.ParseExact(txtsdate.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                '.NumPDC = 1
                .NumPDC = CInt(txtNoPDC.Text)
                .TablePDC = Me.TablePDC
            End With
            Try
                oController.SavePDCMultiAgreement(oCustomclass)
                Response.Redirect("PDCMultiAgreementList.aspx")
            Catch ex As Exception

                ShowMessage(lblMessage, ex.Message, True)
            End Try
            'Else

            '    ShowMessage(lblMessage, "Jumlah PDC harus sama dengan Alokasi", True)
            'End If
        Else

            ShowMessage(lblMessage, "No PDC Salah", True)
        End If
    End Sub

#Region "Generate Table"
    Private Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("PDCMultiAgreement")
        With lObjDataTable
            .Columns.Add("BranchAgreement", System.Type.GetType("System.String"))
            .Columns.Add("ApplicationID", System.Type.GetType("System.String"))
            .Columns.Add("AgreementNo", System.Type.GetType("System.String"))
            .Columns.Add("CustomerID", System.Type.GetType("System.String"))
            .Columns.Add("Name", System.Type.GetType("System.String"))
            .Columns.Add("CustomerType", System.Type.GetType("System.String"))
            .Columns.Add("Address", System.Type.GetType("System.String"))
            .Columns.Add("ApplicationStep", System.Type.GetType("System.String"))
            .Columns.Add("ContractStatus", System.Type.GetType("System.String"))
            .Columns.Add("InstallmentAmount", System.Type.GetType("System.String"))            
            .Columns.Add("ContractPrepaidAmount", System.Type.GetType("System.String"))
            .Columns.Add("AmountPayment", System.Type.GetType("System.String"))
            .Columns.Add("GiroNo", System.Type.GetType("System.String"))
            .Columns.Add("PDCDueDate", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function

    Private Sub GenerateTable(ByVal dtAgreement As DataTable)
        Dim lObjDataRow As DataRow
        Dim i As Integer
        'strZero = ""
        For i = 0 To dtAgreement.Rows.Count - 1
            lObjDataRow = Me.TablePDC.NewRow()
            If CheckDuplikasiApplicationID(dtAgreement.Rows(i).Item("ApplicationID").ToString) Then
                lObjDataRow("BranchAgreement") = dtAgreement.Rows(i).Item("BranchID")
                lObjDataRow("ApplicationID") = dtAgreement.Rows(i).Item("ApplicationID")
                lObjDataRow("AgreementNo") = dtAgreement.Rows(i).Item("AgreementNo")
                lObjDataRow("CustomerID") = dtAgreement.Rows(i).Item("CustomerID")
                lObjDataRow("Name") = dtAgreement.Rows(i).Item("Name")
                lObjDataRow("CustomerType") = dtAgreement.Rows(i).Item("CustomerType")
                lObjDataRow("Address") = dtAgreement.Rows(i).Item("Address")
                lObjDataRow("ApplicationStep") = dtAgreement.Rows(i).Item("ApplicationStep")
                lObjDataRow("ContractStatus") = dtAgreement.Rows(i).Item("ContractStatus")
                lObjDataRow("InstallmentAmount") = dtAgreement.Rows(i).Item("InstallmentAmount")
                'lObjDataRow("AmountPayment") = dtAgreement.Rows(i).Item("InstallmentAmount")
                lObjDataRow("AmountPayment") = dtAgreement.Rows(i).Item("AmountPayment")
                lObjDataRow("GiroNo") = dtAgreement.Rows(i).Item("GiroNo")
                lObjDataRow("PDCDueDate") = dtAgreement.Rows(i).Item("PDCDueDate")
                Me.TablePDC.Rows.Add(lObjDataRow)
            End If
        Next i
        AutoSortGrid()
    End Sub

    Private Sub SaveDataGridToDataTable()
        Dim lObjDataRow As DataRow
        Dim i As Integer
        Dim hyApplicationID As HyperLink
        Dim hyagreementno As HyperLink
        Dim hyCustomerName As HyperLink
        Dim lblCustomerType As Label
        Dim lblCustomerID As Label
        Dim lblAddress As Label
        Dim lblApplicationStep As Label
        Dim lblContractStatus As Label
        Dim lblInstallment As Label
        Dim txtGiroNo As TextBox
        Dim txtPDCDueDate As TextBox

        'Dim txtAmountPayment As TextBox
        Dim txtAmountPayment As ucNumberFormat
        Dim lblBranchAgreement As Label

        'strZero = ""
        Me.TablePDC.Clear()
        For i = 0 To DtgAgreement.Items.Count - 1
            lObjDataRow = Me.TablePDC.NewRow()
            hyApplicationID = CType(DtgAgreement.Items(i).FindControl("hyApplicationID"), HyperLink)
            hyagreementno = CType(DtgAgreement.Items(i).FindControl("hyagreementno"), HyperLink)
            hyCustomerName = CType(DtgAgreement.Items(i).FindControl("hyCustomerName1"), HyperLink)
            lblBranchAgreement = CType(DtgAgreement.Items(i).FindControl("lblBranchAgreement"), Label)
            lblCustomerType = CType(DtgAgreement.Items(i).FindControl("lblCustomerType"), Label)
            lblCustomerID = CType(DtgAgreement.Items(i).FindControl("lblCustomerID"), Label)
            lblAddress = CType(DtgAgreement.Items(i).FindControl("lblAddress"), Label)
            lblApplicationStep = CType(DtgAgreement.Items(i).FindControl("lblApplicationStep"), Label)
            lblContractStatus = CType(DtgAgreement.Items(i).FindControl("lblContractStatus"), Label)
            lblInstallment = CType(DtgAgreement.Items(i).FindControl("lblInstallment"), Label)
            txtAmountPayment = CType(DtgAgreement.Items(i).FindControl("txtAmountPayment"), ucNumberFormat)
            txtGiroNo = CType(DtgAgreement.Items(i).FindControl("txtGiroNo"), TextBox)
            txtPDCDueDate = CType(DtgAgreement.Items(i).FindControl("txtPDCDueDate"), TextBox)

            lObjDataRow("BranchAgreement") = lblBranchAgreement.Text
            lObjDataRow("ApplicationID") = hyApplicationID.Text
            lObjDataRow("AgreementNo") = hyagreementno.Text
            lObjDataRow("CustomerID") = lblCustomerID.Text
            lObjDataRow("Name") = hyCustomerName.Text
            lObjDataRow("CustomerType") = lblCustomerType.Text
            lObjDataRow("Address") = lblAddress.Text
            lObjDataRow("ApplicationStep") = lblApplicationStep.Text
            lObjDataRow("ContractStatus") = lblContractStatus.Text
            lObjDataRow("InstallmentAmount") = lblInstallment.Text
            lObjDataRow("AmountPayment") = txtAmountPayment.Text ' DtgAgreement.Items(i).Cells(9).Text
            lObjDataRow("GiroNo") = txtGiroNo.Text
            lObjDataRow("PDCDueDate") = txtPDCDueDate.Text
            Me.TablePDC.Rows.Add(lObjDataRow)
        Next
    End Sub
#End Region

#Region "Cancel Button "
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("PDCMultiAgreementList.aspx")
    End Sub

    Private Sub btnCancelNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        If Me.TablePDC Is Nothing Then
            Response.Redirect("PDCMultiAgreementList.aspx")
        Else
            If Me.TablePDC.Rows.Count = 0 Then
                Response.Redirect("PDCMultiAgreementList.aspx")
            Else
                Me.TablePDC.Clear()
                oLookupAgreement.AgreementNo = ""
                pnlPDC.Visible = False
            End If
        End If
    End Sub
#End Region

#Region "Check Duplikasi Application ID"
    Private Function CheckDuplikasiApplicationID(ByVal strApplicationID As String) As Boolean
        Dim i As Integer
        Dim isValid As Boolean = True
        Dim hyApplicationID As HyperLink
        For i = 0 To DtgAgreement.Items.Count - 1
            hyApplicationID = CType(DtgAgreement.Items(i).FindControl("hyApplicationID"), HyperLink)
            If hyApplicationID.Text = strApplicationID Then
                isValid = False
                ShowMessage(lblMessage, "No Aplikasi sudah ada !", True)
            End If
        Next
        Return isValid
    End Function
#End Region

#Region "Check Amount PDC"
    Private Function CheckAmountPDC() As Boolean
        'Dim txtAmountPayment As TextBox
        Dim txtAmountPayment As ucNumberFormat
        'Dim isValid As Boolean = True
        Dim Amount As Double = 0
        Dim I As Integer
        For I = 0 To DtgAgreement.Items.Count - 1
            'txtAmountPayment = CType(DtgAgreement.Items(I).FindControl("txtAmountPayment"), ucNumberFormat)
            'Amount += CDbl(DtgAgreement.Items(I).Cells(9).Text)
            txtAmountPayment = CType(DtgAgreement.Items(I).FindControl("txtAmountPayment"), ucNumberFormat)
            Amount += CDbl(txtAmountPayment.Text.Trim)
        Next
        If Round(CDbl(txtPDCAmount.Text.Trim), 2) <> Round(Amount, 2) Then
            isValid = False
        End If
        Return isValid
    End Function
#End Region

    Sub AutoSortGrid()
        TablePDC.DefaultView.Sort = "GiroNo"
        TablePDC = TablePDC.DefaultView.ToTable()
    End Sub

End Class