﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCClearReconcile.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCClearReconcile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCClearReconcile</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PDC CAIR
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo PDC &lt;=
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label>
                    Rekening Bank
                </label>
                <uc1:ucbankaccountid id="obank" runat="server"></uc1:ucbankaccountid>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" Visible="False" AllowSorting="True"
                        OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="GiroNo" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" SortExpression="voucherno" HeaderText="VOUCHER NO">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblVoucherno" runat="server" Text='<%#Container.DataItem("voucherno")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="journalno" HeaderText="JOURNAL NO">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbljournalno" runat="server" Text='<%#Container.DataItem("journalno")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="branchid" HeaderText="BRANCH ID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblpbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PDCReceiptNo" HeaderText="PDCReceiptNo">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCReceiptNo" runat="server" Text='<%#Container.DataItem("PDCReceiptNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BANKID" HeaderText="BANK PDC">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="Total"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GIRONO" HeaderText="NO PDC">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNo" runat="server" Text='<%#Container.DataItem("GIRONO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" SortExpression="PDCDueDate" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">                                                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="TOtalPDCAmount" HeaderText="JUMLAH">
                                <HeaderStyle Width="13%"></HeaderStyle>
                                <ItemStyle Width="13%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsInkaso" HeaderText="INKASO">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInkaso" runat="server" Text='<%#Iif(Container.DataItem("IsInkaso")="1" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsCummulative" HeaderText="KUMULATIF">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCumm" runat="server" Text='<%#Iif(Container.DataItem("IsCummulative")="1" ,"Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NumberofBounce" HeaderText="TOLAK">                                
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("NumberofBounce")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypClear" runat="server" Text="CAIR" Visible='<%#iif(Container.Dataitem("IsInkaso")="1", True, False)%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <asp:CheckBox AutoPostBack="true" ID="ChkSelectAll" runat="server" OnCheckedChanged="SelectAll"
                                        Checked="false"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkSlct" runat="server" Visible='<%#iif(Container.Dataitem("IsInkaso")="0", True, False)%>'
                                        Checked="False"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                       
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" ControlToValidate="txtPage"
                            MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Text="Next" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList2" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PDC CAIR
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo PDC &lt;=
                </label>
                <asp:Label ID="lblPDCDueDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Rekening Bank
                </label>
                <asp:Label ID="lblBankPDC" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtGridClear" runat="server" Width="100%" Visible="False" AllowSorting="True"
                        OnSortCommand="SortGrid2" AutoGenerateColumns="False" DataKeyField="GiroNo" CssClass="grid_general"
                        AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="BANKID" HeaderText="BANK PDC">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID2" runat="server" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="branchid" HeaderText="BRANCH ID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblpbranchid2" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PDCReceiptNo" HeaderText="PDCReceiptNo">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPDCReceiptNo2" runat="server" Text='<%#Container.DataItem("PDCReceiptNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GIRONO" HeaderText="NO PDC">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNo2" runat="server" Text='<%#Container.DataItem("GIRONO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="Label9" runat="server" Text="Total"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="PDCDueDate">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="TotalPDCAmount" HeaderText="JUMLAH">
                                <HeaderStyle CssClass ="th_right" />                                 
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount2" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle CssClass ="item_grid_right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount2" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnclear" runat="server" Text="Clear" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
