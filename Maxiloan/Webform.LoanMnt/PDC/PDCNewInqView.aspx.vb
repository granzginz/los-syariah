﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCNewInqView
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchby As UcSearchBy
    Protected WithEvents obranch As UcBranch
    Protected WithEvents oBranchAll As UcBranchAll
    Protected WithEvents oBankPDC As UcBankMaster
    Dim temptotalPDC As Double
#Region "Property"
    Private Property ParamReport() As String
        Get
            Return (CType(Viewstate("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ParamReport") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PDCNEWINQUIRY"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If

                oSearchBy.ListData = "GiroNO, PDC No"
                oSearchBy.BindData()

                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.PDCInquiryStatus(oCustomClass)

        DtUserList = oCustomClass.listPDCStatus
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub



#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim hypGiroNo As HyperLink
        Dim flagfile As New Label
        Dim lblPDCRNo As Label
        flagfile.Text = "0"
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim lblApplicationid As Label

        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If


        If e.Item.ItemIndex >= 0 Then

            lblPDCRNo = CType(e.Item.FindControl("lblPDCReceiptNo"), Label)
            hypGiroNo = CType(e.Item.FindControl("HyGiroNo"), HyperLink)
            'hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & lblPDCRNo.Text.Trim & "&branchid=" & obranch.BranchID.Trim & "&flagfile=" & flagfile.Text.Trim
            hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & hypGiroNo.Text.Trim & "','" & lblPDCRNo.Text.Trim & "','" & oBranch.BranchID.Trim & "','" & flagfile.Text.Trim & "')"
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblcustomerid"), Label)
            hyTemp = CType(e.Item.FindControl("lblName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"

        End If


    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'Me.SearchBy = ""
        'osearchby.Text = ""
        'osearchby.BindData()
        'DoBind(Me.SearchBy, Me.SortBy)

        Response.Redirect("PDCNewInqView.aspx")
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        par = ""
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Me.SearchBy = "  PH.branchid = '" & obranch.BranchID.Trim & "' "
            par = par & "Branch : " & obranch.BranchName.Trim & " "

            If cboPDCStatus.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and PDCStatus = '" & cboPDCStatus.SelectedItem.Value & "'"
                If par <> "" Then
                    par = par & " , PDCStatus : " & cboPDCStatus.SelectedItem.Text.Trim & " "
                Else
                    par = par & "PDCStatus : " & cboPDCStatus.SelectedItem.Text.Trim & " "
                End If

            End If

            If txtsdate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and PH.PDCDueDate = '" & ConvertDate2(txtsdate.Text) & "'"
                If par <> "" Then
                    par = par & " , PDCDueDate : " & ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy") & " "
                Else
                    par = par & "PDCDueDate : " & ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy") & " "
                End If
            End If
            If cboCumm.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and PH.Iscummulative = '" & cboCumm.SelectedItem.Value.Trim & "'"
                If par <> "" Then
                    par = par & " , IsCummulative : " & cboCumm.SelectedItem.Text.Trim & " "
                Else
                    par = par & "IsCummulative : " & cboCumm.SelectedItem.Text.Trim & " "
                End If
            End If

            If cboInkaso.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and PH.IsInkaso = '" & cboInkaso.SelectedItem.Value.Trim & "'"
                If par <> "" Then
                    par = par & " , IsInkaso : " & cboInkaso.SelectedItem.Text.Trim & " "
                Else
                    par = par & "IsInkaso : " & cboInkaso.SelectedItem.Text.Trim & " "
                End If
            End If

            If oBankPDC.BankID <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and PH.bankID = '" & oBankPDC.BankID.Trim & "'"
                If par <> "" Then
                    par = par & " , Bank PDC : " & oBankPDC.BankName.Trim & " "
                Else
                    par = par & "bank PDC : " & oBankPDC.BankName.Trim & " "
                End If
            End If

            If osearchby.Text.Trim <> "" Then
                If Right(osearchby.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & osearchby.ValueID & " Like '" & osearchby.Text.Trim.Replace("'", "''") & "'"
                Else
                    Me.SearchBy = Me.SearchBy & " and " & osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'"
                End If
                If par <> "" Then
                    par = par & " , " & osearchby.ValueID & " : '" & osearchby.Text.Trim.Replace("'", "''") & "'"
                Else
                    par = par & "" & osearchby.ValueID & " : '" & osearchby.Text.Trim.Replace("'", "''") & "'"
                End If
            End If

            oCustomClass.paramReport = par
            Me.ParamReport = oCustomClass.paramReport
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies("COOKIES_PDCRINQ_REPORT")
            If Not cookie Is Nothing Then
                cookie.Values("paramreport") = Me.ParamReport
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("COOKIES_PDCRINQ_REPORT")
                cookieNew.Values.Add("paramreport", Me.ParamReport)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/PDCInqstatusReport.aspx")
        End If
    End Sub

End Class