﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCEditProcess
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oTrans As ucLookUpTransaction
    Dim temptotalPDC As Double
#Region "Property"

    Private Property TotPDCAmount() As Double
        Get
            Return (CType(Viewstate("TotPDCAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("TotPDCAmount") = Value
        End Set
    End Property

    Private Property BankPDC() As String
        Get
            Return (CType(Viewstate("BankPDC"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankPDC") = Value
        End Set
    End Property

    Private Property ReceiveFrom() As String
        Get
            Return (CType(Viewstate("ReceiveFrom"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReceiveFrom") = Value
        End Set
    End Property

    Private Property GiroNo() As String
        Get
            Return (CType(Viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property

    Private Property NextInstallmentDueDate() As Date
        Get
            Return (CType(Viewstate("NextInstallmentDueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("NextInstallmentDueDate") = Value
        End Set
    End Property

    Private Property PDCDueDate() As Date
        Get
            Return (CType(Viewstate("PDCDueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("PDCDueDate") = Value
        End Set
    End Property

    Private Property IsInkaso() As Boolean
        Get
            Return (CType(Viewstate("IsInkaso"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsInkaso") = Value
        End Set
    End Property


    Private Property IsCumm() As Boolean
        Get
            Return (CType(Viewstate("IsCumm"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsCumm") = Value
        End Set
    End Property

    Private Property PDCType() As String
        Get
            Return (CType(Viewstate("PDCType"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCType") = Value
        End Set
    End Property

    Private Property PDCAmount() As Double
        Get
            Return (CType(Viewstate("PDCAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("PDCAmount") = Value
        End Set
    End Property
    Private Property ReceiptNo() As String
        Get
            Return (CType(Viewstate("ReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReceiptNo") = Value
        End Set
    End Property
    Private Property PDCNo() As String
        Get
            Return (CType(Viewstate("PDCNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCNo") = Value
        End Set
    End Property
    Private Property PDCAmountRequest() As String
        Get
            Return (CType(Viewstate("PDCAmount"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCAmount") = Value
        End Set
    End Property
    Private Property BankId() As String
        Get
            Return (CType(Viewstate("BankId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankId") = Value
        End Set
    End Property
    Private Property IsMultiAgreement() As Boolean
        Get
            Return (CType(Viewstate("IsMultiAgreement"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsMultiAgreement") = Value
        End Set
    End Property
    Private Property GiroSeqNo() As Integer
        Get
            Return (CType(Viewstate("GiroSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("GiroSeqNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCMTransContorller
    Private m_controller As New DataUserControlController

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim isMultiAgreement As String
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.PDCNo = Request.QueryString("PDCNo")
            Me.ReceiptNo = Request.QueryString("ReceiptNo")
            Me.PDCAmountRequest = Request.QueryString("Amount")
            isMultiAgreement = Request.QueryString("isMultiAgreement")
            Me.BankId = Request.QueryString("bankId")
            If isMultiAgreement = "1" Then
                Me.IsMultiAgreement = True
            ElseIf isMultiAgreement = "0" Then
                Me.IsMultiAgreement = False
            End If
            If Me.IsMultiAgreement = True Then
                pnlIsInkaso.Visible = False
            ElseIf Me.IsMultiAgreement = False Then
                pnlIsInkaso.Visible = True
            End If
            Me.GiroSeqNo = CType(Request.QueryString("GiroSeqNo"), Integer)
            Me.BranchID = Me.sesBranchId.Replace("'", "")

            Me.FormID = "PDCEDITPROCESS"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim pstrFile As String
                Dim dtGetLastPDCDetail As DataTable
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
               
                With oCustomClass
                    .BranchId = Me.BranchID
                    .GiroNo = Me.PDCNo
                    .PDCReceiptNo = Me.ReceiptNo
                    .GiroSeqNo = Me.GiroSeqNo
                    .strConnection = GetConnectionString
                End With
                oCustomClass = oController.GetLastPDCDetail(oCustomClass, pstrFile)
                With oCustomClass
                    txtsdate.Text = CDate(.PDCDueDate).ToString("dd/MM/yyyy")
                    If .IsInkaso Then
                        rdoInkaso.SelectedIndex = 0
                    Else
                        rdoInkaso.SelectedIndex = 1
                    End If
                    If .PDCType = "G" Then
                        rdoPDCType.SelectedIndex = 0
                    ElseIf .PDCType = "Cheque" Then
                        rdoPDCType.SelectedIndex = 1
                    End If
                    If .IsCumm Then
                        rdoCumm.SelectedIndex = 0
                    Else
                        rdoCumm.SelectedIndex = 1
                    End If
                    txtReceiveFrom.Text = .ReceivedFrom
                    txtPDCAmount.Text = FormatNumber(.AmountReceive, 2)
                End With
                fillcboBank()
                DoBind()
            End If
        End If
    End Sub

#End Region
#Region "FillCboBank"
    Sub fillcboBank()
        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankName(GetConnectionString)
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
        cboBank.SelectedIndex = cboBank.Items.IndexOf(cboBank.Items.FindByValue(Me.BankId))
    End Sub
#End Region
#Region "DoBind"

    Sub DoBind()
        If Not CheckCashier(Me.Loginid) Then

            ShowMessage(lblMessage, "Kasir Belum Buka", True)
            btnAddNew.Visible = False
        Else
            oCustomClass.ApplicationID = Me.ApplicationID
            oCustomClass.strConnection = GetConnectionString
            oCustomClass.BranchId = Me.BranchID
            oCustomClass = oController.PDCMTransList(oCustomClass)

            If Me.IsHoBranch Then
                With oTrans
                    .ProcessID = "PDCEDIT"
                    .IsAgreement = "1"
                    .IsPettyCash = ""
                    .IsHOTransaction = ""
                    .IsPaymentReceive = "1"
                    .Style = "AccMnt"
                    .BindData()
                End With
            Else
                With oTrans
                    .ProcessID = "PDCEDIT"
                    .IsAgreement = "1"
                    .IsPettyCash = ""
                    .IsHOTransaction = "0"
                    .IsPaymentReceive = "1"
                    .Style = "AccMnt"
                    .BindData()
                End With
            End If

            With oCustomClass
                lblBranchID.Text = .BranchName
                Me.AgreementNo = .Agreementno
                hyAgreementNo.Text = .Agreementno
                If hyAgreementNo.Text.Trim.Length > 0 Then
                    hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(.ApplicationID.Trim) & "')"
                End If
                hyCustomerName.Text = .CustomerName
                Me.CustomerID = .CustomerID
                If hyCustomerName.Text.Trim.Length > 0 Then
                    hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID) & "')"
                End If
                lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                lblNInsDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
                lblNextInstallmentNumber.Text = CType(.nextInstallmentNumber, String)
                LblReceiptNo.Text = Me.ReceiptNo
                txtPDCNo.Text = Me.PDCNo
                txtPDCAmount.Text = Me.PDCAmountRequest
                oTrans.TransactionID = ""
                oTrans.Transaction = ""
                oTrans.Amount = 0
                oTrans.Description = ""
                oTrans.BindData()
            End With
            DtgPDCList.DataSource = oCustomClass.ListPDC
            DtgPDCList.DataBind()
            DtgPDCList.Visible = True
            pnlPDC.Visible = True
        End If
    End Sub
#End Region

#Region "DatagridCommand"
    Private Sub DtgPDCList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPDCList.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim totalPDCAmount As New Label
        Dim lPDCAmount As New Label

        With oCustomClass

            If e.Item.ItemIndex >= 0 Then
                lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
                temptotalPDC += CDbl(lPDCAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then

                totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
                Me.TotPDCAmount = temptotalPDC
                totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
            End If
        End With
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Private Sub DtgPDCList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPDCList.ItemCommand
        Dim pstrFile As String
        If e.CommandName = "DELETE" Then
            With oCustomClass
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = Me.Session.SessionID + Me.Loginid
                .BranchId = Me.BranchID
                .FlagDelete = "1"
                .CDtGrid = DtgPDCList.Items.Count
                .IndexDelete = CStr(e.Item.ItemIndex())
                .HpsXml = "2"
            End With
            oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
            DtgPDCList.DataSource = oCustomClass.ListPDC
            DtgPDCList.DataBind()
            DtgPDCList.Visible = True
            pnlPDC.Visible = True
        End If
    End Sub
#End Region

#Region "ButtonClick"
    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
            Dim lBytEnd As Integer
            Dim lBytCounter As Integer
            Dim lblListPDCNo As Label
            Dim lstrPDC As String
            Dim pStrFile As String
            Dim PayAllocid As Label
            Dim i As Integer

            lblMessage.Text = ""
            If rdoInkaso.SelectedItem.Text = "Yes" Then
                If rdoPDCType.SelectedItem.Text = "Cheque" Then

                    ShowMessage(lblMessage, "Flag Inkaso hanya Berlaku untuk jenis PDC Giro", True)
                    Exit Sub
                End If
            End If
            If Not IsNumeric((Right(txtPDCNo.Text, 1))) Then

                ShowMessage(lblMessage, "No PDC Salah", True)
                Exit Sub
            End If
            If txtsdate.Text = "" Then

                ShowMessage(lblMessage, "Harap isi tanggal Jatuh Tempo", True)
                Exit Sub
            Else
                Dim indate As Boolean
                indate = IsDate(ConvertDate2(txtsdate.Text))
                If indate Then
                Else

                    ShowMessage(lblMessage, "Tanggal Jatuh Tempo Salah", True)
                    Exit Sub
                End If
            End If
            If CDbl(txtPDCAmount.Text) <= 0 Then

                ShowMessage(lblMessage, "Jumlah PDC harus > 0 ", True)
                Exit Sub
            ElseIf oTrans.Amount <= 0 Then

                ShowMessage(lblMessage, "Jumlah Transaksi harus > 0 ", True)
                Exit Sub
            ElseIf oTrans.Description = "" Then

                ShowMessage(lblMessage, "Harap isi Keterangan", True)
                Exit Sub
            ElseIf oTrans.TransactionID = "" Then

                ShowMessage(lblMessage, "Harap isi Transaksi", True)
                Exit Sub
            End If
            For i = 0 To DtgPDCList.Items.Count - 1
                If oTrans.TransactionID = CType(DtgPDCList.Items(i).FindControl("lblBankPDC"), Label).Text.Trim Then

                    ShowMessage(lblMessage, "Alokasi Pembayaran sudah ada", True)
                    Exit Sub
                End If
            Next
            With oCustomClass
                pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = .LoginId
                .strConnection = GetConnectionString()
                .ReceivedFrom = txtReceiveFrom.Text
                .GiroNo = txtPDCNo.Text
                .NumPDC = 1
                .PDCAmount = CInt(txtPDCAmount.Text)
                .BankPDCName = cboBank.SelectedItem.Text.Trim
                .BankPDC = cboBank.SelectedItem.Value.Trim
                .BranchId = Me.BranchID
                .PDCDueDate = ConvertDate2(txtsdate.Text)
                .IsInkaso = CType(rdoInkaso.SelectedItem.Value, Boolean)
                .IsCumm = CType(rdoCumm.SelectedItem.Value, Boolean)
                .PDCType = rdoPDCType.SelectedItem.Value
                .PaymentAllocationID = oTrans.TransactionID
                .PaymentAllocationName = oTrans.Transaction
                .TransAmount = oTrans.Amount
                .Description = oTrans.Description
                .FlagDelete = "0"
                .BranchAgreement = Me.BranchID
                .HpsXml = "0"
            End With
            oCustomClass = oController.GetTablePDCEdit(oCustomClass, pStrFile)
            DtgPDCList.DataSource = oCustomClass.ListPDC
            DtgPDCList.DataBind()

            DtgPDCList.Visible = True
            pnlPDC.Visible = True
            rdoInkaso.SelectedIndex = 1
            rdoCumm.SelectedIndex = 1
            btnCancelNew.Visible = False

            oTrans.TransactionID = ""
            oTrans.Transaction = ""
            oTrans.Amount = 0
            oTrans.Description = ""
            oTrans.BindData()
        Else
            Exit Sub
        End If
        'End If
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click

        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oController.GetTablePDCEdit(oCustomClass, pstrFile)
        pnlPDC.Visible = True
        pnlList.Visible = True
        btnCancelNew.Visible = True

        oTrans.TransactionID = ""
        oTrans.Transaction = ""
        oTrans.Amount = 0
        oTrans.Description = ""
        oTrans.BindData()

        txtReceiveFrom.Text = ""
        txtReceiveFrom.Enabled = True
        txtPDCAmount.Enabled = True
        txtPDCAmount.Text = ""

        'rdoInkaso.SelectedIndex = 1
        'rdoCumm.SelectedIndex = 1
        'rdoPDCType.SelectedIndex = 1
        fillcboBank()

        Dim strFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oController.GetTablePDC(oCustomClass, strFile)
    End Sub

    Private Sub btnCancelNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        oCustomClass.HpsXml = "1"
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
        pnlPDC.Visible = False
        pnlList.Visible = True
        Response.Redirect("PDCEdit.aspx")
    End Sub
    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If DtgPDCList.Items.Count > 0 Then
                Dim Ndttable As DataTable
                Dim pStrFile As String
                If Me.TotPDCAmount = Me.PDCAmount Then
                    Ndttable = GetStructPDC()
                    If Me.BankPDC = "" Then
                        Me.BankPDC = cboBank.SelectedItem.Value.Trim
                    End If
                    If Me.PDCType = "" Then
                        Me.PDCType = rdoPDCType.SelectedItem.Value
                    End If
                    With oCustomClass
                        pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .strConnection = GetConnectionString()
                        .ApplicationID = Me.ApplicationID
                        .BankPDC = cboBank.SelectedItem.Value.Trim
                        .IsInkaso = CType(rdoInkaso.SelectedItem.Value, Boolean)
                        .IsCumm = CType(rdoCumm.SelectedItem.Value, Boolean)
                        .PDCType = rdoPDCType.SelectedItem.Value
                        .PDCDueDate = ConvertDate2(txtsdate.Text)
                        .PDCReceiptNo = Me.ReceiptNo
                        .GiroNo = txtPDCNo.Text
                        .GiroSeqNo = Me.GiroSeqNo
                        .PDCAmount = CInt(txtPDCAmount.Text)
                    End With
                    'oCustomClass.HpsXml = "1"
                    'oCustomClass = oController.GetTablePDCEdit(oCustomClass, pStrFile)
                    Dim errmsg As String = oController.SavePDCEdit(oCustomClass, pStrFile)
                    'ShowMessage(lblMessage, errmsg, True)
                    ShowMessage(lblMessage, errmsg, False)
                    Response.Redirect("PDCEdit.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                Else

                    ShowMessage(lblMessage, "Total Harus sama dengan Jumlah PDC", True)
                End If
            Else

                ShowMessage(lblMessage, "Tidak ada data", True)
                Exit Sub
            End If
        End If
    End Sub
#End Region

    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("PDC")
        With lObjDataTable
            .Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
            .Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
            .Columns.Add("Description", System.Type.GetType("System.String"))
            .Columns.Add("Amount", System.Type.GetType("System.String"))
        End With

        Return lObjDataTable
    End Function

End Class