﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCInquiryIncomplete.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCInquiryIncomplete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCInquiryIncomplete</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY PDC TIDAK LENGKAP
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="CboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                    ErrorMessage="Harap Pilih Cabang" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    PDC Ditangan &lt;=
                </label>
                <asp:TextBox ID="TxtPDCOnHand" runat="Server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="1">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="2">No Kontrak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"  Width="120px"></asp:TextBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC TIDAK LENGKAP
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPDCIncomplete" runat="server" Width="100%" Visible="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="customerid" HeaderText="CUSTOMER ID">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblcustomerid" runat="server" Text='<%#Container.DataItem("customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NextInstallmentDueDate" HeaderText="JT BERIKUTNYA">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDueDate" runat="server" Text='<%#Container.DataItem("NextInstallmentDueDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OSInstallmentNumber" SortExpression="OSInstallmentNumber"
                                HeaderText="SISA ANGSURAN">
                                <HeaderStyle HorizontalAlign="Center" Width="10%" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="PDCOnHand" HeaderText="PDC DITANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCOnHand" runat="server" Text='<%#Container.DataItem("PDCOnHand")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="Applicationid" HeaderText="Application ID">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="True" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="First" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="Prev" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="Next" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" OnCommand="NavigationLink_Click"
                            CommandName="Last" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rvGo" runat="server" Display="Dynamic" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
       
    </asp:Panel>
    </form>
</body>
</html>
