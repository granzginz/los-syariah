﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCNewInquiryDetail
    Inherits Maxiloan.Webform.WebBased
    Dim temptotalPDCPA As Double
    Dim temptotalPDCPAClear As Double
    Dim temptotalPDCHis As Double
#Region "Property"
    Private Property PDCReceiptNo() As String
        Get
            Return (CType(Viewstate("PDCReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property
    Private Property GiroNo() As String
        Get
            Return (CType(Viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property
    Private Property flagfile() As String
        Get
            Return (CType(Viewstate("flagfile"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("flagfile") = Value
        End Set
    End Property
    Public Property GiroSeqNo() As Integer
        Get
            Return CType(viewstate("GiroSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("GiroSeqNo") = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return CType(viewstate("CustomerId"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("CustomerId") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController
    Dim oUserControl As UCPDCAllocationList

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblgiro As New Label
            Dim lblnewStat As New Label
            Dim inFlagfile As New Label
            Me.FormID = "PDCNEWINQUIRYVIEWDETAIL"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.PDCReceiptNo = Request.QueryString("PDCReceiptNo")
                Me.BranchID = Request.QueryString("branchid")
                lblgiro.Text = Request.QueryString("girono")
                Me.GiroNo = Request.QueryString("girono")
                inFlagfile.Text = Request.QueryString("flagfile")
                Me.flagfile = inFlagfile.Text
                If Me.flagfile = "1" Then
                    BtnBack.Visible = True
                    pnlbutton.Visible = False
                Else
                    BtnBack.Visible = False
                    pnlbutton.Visible = True
                End If
                lblnewStat.Text = Request.QueryString("status")
                DoBind()
                DoBindPA()
                DoBindHistory()
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()
        Dim hypPDCReceiptno As HyperLink
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.GiroNo = Me.GiroNo
        oCustomClass = oController.PDCInquiryListDetail(oCustomClass)
        With oCustomClass
            lblPDCReceiptNoV.Text = .PDCReceiptNo

            lblPDCReceiptNoV.NavigateUrl = "javascript:OpenWinPDCView('" & Me.PDCReceiptNo.Trim & "','" & Me.BranchID.Trim & "','" & Me.flagfile.Trim & "','" & Me.GiroNo.Trim & "')"
            '"PDCRInquiryView.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & Me.BranchID.Trim & "&flagfile=" & Me.flagfile.Trim & "&Girono=" & Me.GiroNo.Trim
            lblReceiveDateV.Text = .RecDate.ToString("dd/MM/yyyy")
            'lblAgreeBranchV.Text = .BranchLocationName
            'HyAgreementNoV.Text = .Agreementno
            'HyAgreementNoV.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(HyAgreementNoV.Text) & "')"
            'HyCustomerNameV.Text = .CustomerName
            'HyCustomerNameV.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.CustomerID) & "')"
            Me.CustomerId = .customerid
            lblGiroNo.Text = .GiroNo
            lblBankPDC.Text = .BankPDCName
            lblPDCAmount.Text = FormatNumber(.PDCAmount, 2)
            lblPDCDueDate.Text = .PDCDueDate.ToString("dd/MM/yyyy")
            lblPDCType.Text = .PDCType
            If .PDCType = "BG" Then
                lblPDCType.Text = "Bilyet Giro"
            Else
                lblPDCType.Text = "Cheque"
            End If
            If .BankClearing.Trim <> "-" Then
                lblBankClearing.Text = .BankClearingName
            ElseIf .BankClearing.Trim = "" Then
                lblBankClearing.Text = "-"
            End If

            If .IsInkaso Then
                lblInkasoFlag.Text = "Yes"
            Else
                lblInkasoFlag.Text = "No"
            End If
            If .IsCumm Then
                lblCummFlag.Text = "Yes"
            Else
                lblCummFlag.Text = "No"
            End If
            If .PDCStatus.Trim <> "Hold" Then
                lblHolddate.Text = "-"
            Else
                lblHolddate.Text = .HoldUntildate.ToString("dd/MM/yyyy")
            End If
            lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")
            lblStatus.Text = .PDCStatus
            lblNumOfBounce.Text = CStr(.Bounce)
            Me.GiroSeqNo = CInt(.GiroSeqNo)
            'lblInkasoFlag.Text = IIf(.IsInkaso, "Yes", "No")
            'lblCummFlag.Text = IIf(.IsCumm, "Yes", "No")
        End With
    End Sub

    Sub DoBindPA()
        Dim i As Integer
        i = 1
        For i = 1 To Me.GiroSeqNo
            oUserControl = CType(LoadControl("../../Webform.UserController/ucPDCAllocationList.ascx"), ucPDCAllocationList)
            oUserControl.ID = "usc" & CStr(i)
            oUserControl.PDCReceiptNo = Me.PDCReceiptNo
            oUserControl.BranchID = Me.BranchID
            oUserControl.GiroNo = Me.GiroNo
            oUserControl.GiroSeqNo = i
            oUserControl.CustomerId = Me.CustomerId
            pnlPDCAllocationList.Controls.Add(oUserControl)
        Next
    End Sub

    Sub DoBindHistory()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.GiroNo = Me.GiroNo
        oCustomClass = oController.PDCInquiryDetail(oCustomClass)

        DtUserList = oCustomClass.listPDCHistory
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgPDC.DataSource = DvUserList

        Try
            DtgPDC.DataBind()
        Catch
            'DtgPDC.CurrentPageIndex = 0
            DtgPDC.DataBind()
        End Try
    End Sub
#End Region


    Private Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        'If Me.flagfile = "1" Then
        '    Response.Redirect("PDCInquiryView.aspx")

        'ElseIf Me.flagfile = "2" Then
        '    Response.Redirect("PDCRInquiryView.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & Me.BranchID.Trim)
        'ElseIf Me.flagfile = "3" Then
        '    Response.Redirect("PDCChangeStatus.aspx")
        'ElseIf Me.flagfile = "4" Then
        '    Response.Redirect("PDCToDeposit.aspx")
        'End If

        If Me.flagfile = "1" Then
            Response.Redirect("PDCRNewInqView.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & Me.BranchID.Trim & "&flagfile=" & Me.flagfile.Trim)
        End If
    End Sub


End Class