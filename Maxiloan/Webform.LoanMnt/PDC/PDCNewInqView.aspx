﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCNewInqView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCNewInqView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankMaster" Src="../../Webform.UserController/UcBankMaster.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCNewInqView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h3>
                    INQUIRY PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
            </div>
            <div class="form_right">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status PDC
                </label>
                <asp:DropDownList ID="cboPDCStatus" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="OP">Open</asp:ListItem>
                    <asp:ListItem Value="DP">Deposited</asp:ListItem>
                    <asp:ListItem Value="CL">Clear</asp:ListItem>
                    <asp:ListItem Value="BC">Bounce</asp:ListItem>
                    <asp:ListItem Value="HD">Hold</asp:ListItem>
                    <asp:ListItem Value="CC">Cancel</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:DropDownList ID="cboCumm" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:DropDownList ID="cboInkaso" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <uc1:ucbankmaster id="oBankPDC" runat="server"></uc1:ucbankmaster>
            </div>
        </div>
        
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
         
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single"><div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" Visible="False" ShowFooter="True"
                    AllowSorting="True" OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="GiroNo"
                     CssClass="grid_general">
                    <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                    <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                    <ItemStyle Height="25px" CssClass="tdganjil"></ItemStyle>
                    <HeaderStyle Height="25px" CssClass="tdjudul"></HeaderStyle>
                    <FooterStyle Font-Bold="True" Height="25px" CssClass="tdjudul"></FooterStyle>
                    <Columns>
                        <asp:TemplateColumn Visible="False" SortExpression="BranchID" HeaderText="BRANCH ID">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="PDCReceiptNo" HeaderText="PDCReceiptNo">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPDCReceiptNo" runat="server" Text='<%#Container.DataItem("PDCReceiptNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="APPLICATION ID">
                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="AGREEMENT NO" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="12%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="customerid" HeaderText="CUSTOMER ID">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcustomerid" runat="server" Text='<%#Container.DataItem("customerid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="CUSTOMER NAME" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankName" HeaderText="BANK PDC">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Container.DataItem("BankName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTot" runat="server" Text="Total"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="GiroNo" HeaderText="NO PDC">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyGiroNo" runat="server" Text='<%#Container.DataItem("GiroNo")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="PDCDueDate" SortExpression="PDCDueDate" HeaderText="TGL JATUH TEMPO"
                            DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" CssClass="tdjudul"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="TOtalPDCAmount" HeaderText="JUMLAH">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="12%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="IsInkaso" HeaderText="INKASO">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblInkaso" runat="server" Text='<%#Iif(Container.DataItem("IsInkaso")="True" ,"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="IsCummulative" HeaderText="KUMULATIF">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCumm" runat="server" Text='<%#Iif(Container.DataItem("IsCummulative")="True" ,"Yes","No")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="BranchInitialName" HeaderText="AGR. BRANCH">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="6%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%#Container.DataItem("BranchInitialName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PDCStatusDesc" HeaderText="STATUS">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("PDCStatusDesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="imgbtnPageNumb" runat="server"  CssClass="buttongo small blue" Text="Go"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div></div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
