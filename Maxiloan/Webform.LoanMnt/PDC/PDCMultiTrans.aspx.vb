﻿#Region "Imports"
Imports System.Math
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCMultiTrans
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents cbobank As UcBankName
    Protected WithEvents oTrans As ucLookUpTransaction
    Protected WithEvents txtPDCAmount As ucNumberFormat


    Dim temptotalPDC As Double
    Dim flagtrue As String
#Region "Property"

    Private Property TotPDCAmount() As Double
        Get
            Return (CType(Viewstate("TotPDCAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("TotPDCAmount") = Value
        End Set
    End Property

    Private Property BankPDC() As String
        Get
            Return (CType(Viewstate("BankPDC"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("BankPDC") = Value
        End Set
    End Property

    Private Property ReceiveFrom() As String
        Get
            Return (CType(Viewstate("ReceiveFrom"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReceiveFrom") = Value
        End Set
    End Property

    Private Property GiroNo() As String
        Get
            Return (CType(Viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property

    Private Property NextInstallmentDueDate() As Date
        Get
            Return (CType(Viewstate("NextInstallmentDueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("NextInstallmentDueDate") = Value
        End Set
    End Property

    Private Property PDCDueDate() As Date
        Get
            Return (CType(Viewstate("PDCDueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("PDCDueDate") = Value
        End Set
    End Property

    Private Property IsInkaso() As Boolean
        Get
            Return (CType(Viewstate("IsInkaso"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsInkaso") = Value
        End Set
    End Property


    Private Property IsCumm() As Boolean
        Get
            Return (CType(Viewstate("IsCumm"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsCumm") = Value
        End Set
    End Property

    Private Property PDCType() As String
        Get
            Return (CType(Viewstate("PDCType"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCType") = Value
        End Set
    End Property

    Private Property PDCAmount() As Double
        Get
            Return (CType(Viewstate("PDCAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("PDCAmount") = Value
        End Set
    End Property
    Private Property PDCReceiptNo() As String
        Get
            Return CType(viewstate("PDCReceiptNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PDCReceiptNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCMTransContorller

#End Region

#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.CustomerID = Request.QueryString("CustomerId")
            Me.FormID = "PDCMULTI"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.HpsXml = "1"
                oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
                DoBind()

            End If
        End If
    End Sub

#End Region

#Region "DoBind"

    Sub DoBind()
        If Not CheckCashier(Me.Loginid) Then

            ShowMessage(lblMessage, "Kasir belum Buka", True)
            btnAddNew.Visible = False
        Else

            oCustomClass.ApplicationID = Me.ApplicationID
            oCustomClass.strConnection = GetConnectionString
            oCustomClass.BranchId = Me.BranchID
            Try
                oCustomClass = oController.PDCMTransList(oCustomClass)

                If Me.IsHoBranch Then
                    With oTrans
                        .ProcessID = "PDCMTRS"
                        .IsAgreement = "1"
                        .IsPettyCash = ""
                        .IsHOTransaction = ""
                        .IsPaymentReceive = "1"
                        .Style = "AccMnt"
                        .BindData()
                    End With
                Else
                    With oTrans
                        .ProcessID = "PDCMTRS"
                        .IsAgreement = "1"
                        .IsPettyCash = ""
                        .IsHOTransaction = "0"
                        .IsPaymentReceive = "1"
                        .Style = "AccMnt"
                        .BindData()
                    End With
                End If

                With oCustomClass
                    lblBranchID.Text = .BranchName
                    Me.AgreementNo = .Agreementno
                    hyAgreementNo.Text = .Agreementno
                    If hyAgreementNo.Text.Trim.Length > 0 Then
                        hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                    End If
                    hyCustomerName.Text = .CustomerName
                    If hyCustomerName.Text.Trim.Length > 0 Then
                        hyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID) & "')"
                    End If
                    lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                    lblNInsDate.Text = .NextInstallmentDate.ToString("dd/MM/yyyy")
                    lblNextInstallmentNumber.Text = CType(.NextInstallmentNumber, String)

                    oTrans.TransactionID = .PaymentAllocationID
                    oTrans.Transaction = .PaymentAllocationName
                    oTrans.Amount = .TransAmount
                    oTrans.Description = .Description
                    'lblPaymentFrequency.Text = CStr(.PaymentFrequency)
                End With

            Catch ex As Exception

                ShowMessage(lblMessage, ex.Message, True)
                btnAddNew.Visible = False
                Exit Sub
            End Try
        End If


    End Sub

#End Region

#Region "DtgPDCList ItemDataBound"
    Private Sub DtgPDCList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPDCList.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim totalPDCAmount As New Label
        Dim lPDCAmount As New Label

        With oCustomClass

            If e.Item.ItemIndex >= 0 Then
                lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
                temptotalPDC += CDbl(lPDCAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then

                totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
                Me.TotPDCAmount = temptotalPDC
                totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
            End If
        End With
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

#Region "DtgPDCList ItemCommand"
    Private Sub DtgPDCList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPDCList.ItemCommand
        Dim pstrFile As String
        If e.CommandName = "DELETE" Then
            With oCustomClass
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                .LoginId = Me.Session.SessionID + Me.Loginid
                .BranchId = Me.BranchID
                .FlagDelete = "1"
                .CDtGrid = DtgPDCList.Items.Count
                .IndexDelete = CStr(e.Item.ItemIndex())
                .HpsXml = "2"
            End With
            oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
            DtgPDCList.DataSource = oCustomClass.ListPDC
            DtgPDCList.DataBind()
            DtgPDCList.Visible = True
            pnlPDC.Visible = True
        End If
    End Sub

#End Region

#Region "Add"
    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then

            Dim lBytEnd As Integer
            Dim lBytCounter As Integer
            Dim lblListPDCNo As Label
            Dim lstrPDC As String
            Dim pStrFile As String
            Me.FormID = "PDCMULTI"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then  
                If rdoInkaso.SelectedItem.Text = "Yes" Then
                    If rdoPDCType.SelectedItem.Text = "Cheque" Then

                        ShowMessage(lblMessage, "Flag Inkaso hanya berlaku untuk jenis PDC Giro", True)
                        Exit Sub
                    End If
                End If
                'If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(sdate.dateValue)) < 0 Then
                '    lblMessage.Text = "Due date must be >= businessdate"
                '    Exit Sub
                'End If

                If Not IsNumeric((Right(txtPDCNo.Text, 1))) Then

                    ShowMessage(lblMessage, "No PDC Salah", True)
                    Exit Sub
                End If

                If txtsdate.Text = "" Then

                    ShowMessage(lblMessage, "Harap isi tanggal jatuh Tempo", True)
                    Exit Sub
                Else

                    Dim indate As Boolean
                    indate = IsDate(ConvertDate2(txtsdate.Text))
                    If indate Then
                    Else

                        ShowMessage(lblMessage, "Tanggal jatuh Tempo Salah", True)
                        Exit Sub
                    End If
                End If
                If CDbl(txtPDCAmount.Text) <= 0 Then

                    ShowMessage(lblMessage, "Jumlah harus > 0 ", True)
                    Exit Sub
                ElseIf oTrans.Amount <= 0 Then

                    ShowMessage(lblMessage, "Nilai Transaksi harus > 0 ", True)
                    Exit Sub
                ElseIf oTrans.Description = "" Then

                    ShowMessage(lblMessage, "Harap isi Keterangan", True)
                    Exit Sub
                ElseIf oTrans.TransactionID = "" Then

                    ShowMessage(lblMessage, "Harap isi Transaksi", True)
                    Exit Sub
                End If

                With oCustomClass
                    pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                    .LoginId = .LoginId
                    .strConnection = GetConnectionString()
                    .ReceivedFrom = txtReceiveFrom.Text
                    .GiroNo = txtPDCNo.Text
                    .NumPDC = 1
                    Me.GiroNo = .GiroNo
                    .PDCAmount = CInt(txtPDCAmount.Text)
                    Me.PDCAmount = .PDCAmount
                    .BankPDCName = cbobank.BankName
                    .BankPDC = cbobank.BankID
                    Me.BankPDC = .BankPDC
                    .BranchId = Me.BranchID
                    Me.ReceiveFrom = .ReceivedFrom
                    .PDCDueDate = ConvertDate2(txtsdate.Text)
                    Me.PDCDueDate = .PDCDueDate
                    .IsInkaso = CType(rdoInkaso.SelectedItem.Value, Boolean)
                    Me.IsInkaso = .IsInkaso
                    .IsCumm = CType(rdoCumm.SelectedItem.Value, Boolean)
                    Me.IsCumm = .IsCumm
                    .PDCType = rdoPDCType.SelectedItem.Value
                    Me.PDCType = .PDCType
                    .PaymentAllocationID = oTrans.TransactionID
                    .PaymentAllocationName = oTrans.Transaction
                    .TransAmount = oTrans.Amount
                    .Description = oTrans.Description
                    .FlagDelete = "0"
                    .BranchAgreement = Me.BranchID
                End With

                oCustomClass = oController.GetTablePDC(oCustomClass, pStrFile)

                If Not oCustomClass.IsValidPDC Then

                    ShowMessage(lblMessage, "Transaksi sudah ada", True)
                    Exit Sub
                Else
                    DtgPDCList.DataSource = oCustomClass.ListPDC
                    DtgPDCList.DataBind()
                    DtgPDCList.Visible = True
                    pnlPDC.Visible = True
                End If
                oTrans.TransactionID = ""
                oTrans.Transaction = ""
                oTrans.Amount = 0
                oTrans.Description = ""
                oTrans.BindData()
                btnCancelNew.Visible = False
                rdoInkaso.SelectedIndex = 1
                rdoCumm.SelectedIndex = 1

                'cbobank.BankID = ""
                'cbobank.BindData()
            Else
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "Imb Save"
    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If DtgPDCList.Items.Count > 0 Then
                Dim Ndttable As DataTable
                Dim pStrFile As String
                If Round(Me.TotPDCAmount, 2) = Round(Me.PDCAmount, 2) Then
                    Ndttable = GetStructPDC()
                    With oCustomClass

                        pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                        .LoginId = Me.Loginid
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .strConnection = GetConnectionString()
                        .ApplicationID = Me.ApplicationID
                        .ReceivedFrom = Me.ReceiveFrom
                        .BusinessDate = Me.BusinessDate
                        .BankPDC = Me.BankPDC
                        .IsInkaso = Me.IsInkaso
                        .IsCumm = Me.IsCumm
                        .PDCType = Me.PDCType
                        .GiroNo = Me.GiroNo
                        .PDCAmount = Me.PDCAmount
                        .PDCDueDate = Me.PDCDueDate
                        .GrandTotPDCAmount = Me.TotPDCAmount
                        .NumPDC = DtgPDCList.Items.Count
                        .BranchAgreement = Me.BranchID
                    End With
                    Try
                        Me.PDCReceiptNo = oController.SavePDCTrans(oCustomClass, pStrFile)
                        oCustomClass.HpsXml = "1"
                        oCustomClass = oController.GetTablePDC(oCustomClass, pStrFile)
                        BindReport()
                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                Else

                    ShowMessage(lblMessage, "Total harus sama dengan Jumlah PDC", True)
                End If
            Else

                ' ShowMessage(lblMessage, "Tidak ada Data", True)
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "GetStructPDC"
    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("PDC")
        '  Dim dtTable As New DataTable("PDC")
        With lObjDataTable
            .Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
            .Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
            .Columns.Add("Description", System.Type.GetType("System.String"))
            .Columns.Add("Amount", System.Type.GetType("System.String"))
        End With

        Return lObjDataTable
    End Function
#End Region

#Region "img cancel"

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click

        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
        pnlPDC.Visible = False
        pnlList.Visible = True
        btnCancelNew.Visible = True

        oTrans.TransactionID = ""
        oTrans.Transaction = ""
        oTrans.Amount = 0
        oTrans.Description = ""
        oTrans.BindData()

        txtReceiveFrom.Text = ""
        txtReceiveFrom.Enabled = True
        txtPDCAmount.Enabled = True
        txtPDCAmount.Text = ""

        txtPDCNo.Text = ""
        rdoInkaso.SelectedIndex = 1
        rdoCumm.SelectedIndex = 1
        rdoPDCType.SelectedIndex = 1
        txtsdate.Text = ""
        cbobank.BankID = ""
        cbobank.BindData()

        Dim strFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.HpsXml = "1"
        oCustomClass = oController.GetTablePDC(oCustomClass, strFile)
    End Sub
#End Region

#Region " Button Cancel"
    Private Sub btnCancelNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        oCustomClass.HpsXml = "1"
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass = oController.GetTablePDC(oCustomClass, pstrFile)
        pnlPDC.Visible = False
        pnlList.Visible = True
        Response.Redirect("PDCMultiTransList.aspx")
    End Sub
#End Region

#Region "BindReport"    
    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As RptPrintReceiptNo = New RptPrintReceiptNo

        oData = oController.PrintReceiptNo(GetConnectionString, Me.PDCReceiptNo, Me.sesBranchId.Replace("'", ""))
        objReport.SetDataSource(oData)

        '========================================================
        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues
        ParamField = objReport.DataDefinition.ParameterFields("BusinessDate")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BusinessDate
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "PDCMultiTransaction.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        Response.Redirect("PDCMultiTransList.aspx?Message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "PDCMultiTransaction")
    End Sub
#End Region

End Class