﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCEdit
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchby As UcSearchBy
    Dim temptotalPDC As Double
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCChangeStatusController


#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PDCEDIT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If

                oSearchBy.ListData = "GiroNo, PDC No.-BANKID,Bank PDC"
                oSearchBy.BindData()


                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If

    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.PDCStatusList(oCustomClass)

        DtUserList = oCustomClass.ListPDC
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound


        Dim m As Int32
        'Dim hypReceive As HyperLink
        Dim HyEdit As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblGirono2 As HyperLink
        Dim lblstatus As Label
        Dim lblstatus2 As Label
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim hypGiroNo As HyperLink
        Dim flagfile As New Label
        Dim NewlblPDCreceiptno As Label
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim LblNewAmount As Label
        Dim lblBankID As Label


        flagfile.Text = "0"
        If sessioninvalid() Then
            Exit Sub
        End If

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
            lblGirono2 = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
            lblstatus = CType(e.Item.FindControl("lblPDCStatus"), Label)
            NewlblPDCreceiptno = CType(e.Item.FindControl("lblPDCreceiptno"), Label)


            'If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
            '    lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
            '    lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            '    LblNewAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            '    HyEdit = CType(e.Item.FindControl("HyEdit"), HyperLink)
            '    HyEdit.NavigateUrl = "PDCEditProcess.aspx?CustomerId=" & Server.UrlEncode(lblTemp.Text.Trim) & "&applicationid=" & lblApplicationid.Text.Trim & "&PDCNo=" & lblGirono2.Text.Trim & "&ReceiptNo=" & NewlblPDCreceiptno.Text.Trim & "&Amount=" & LblNewAmount.Text.Trim & "&BankId=" & Server.UrlEncode(lblBankID.Text.Trim) & "&branchid=" & Me.sesBranchId.Replace("'", "")
            'End If
            If CheckFeature(Me.Loginid, Me.FormID, "PDC", Me.AppId) Then
                hypGiroNo = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
                'hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & lblGirono2.Text.Trim & "&PDCReceiptNo=" & NewlblPDCreceiptno.Text.Trim & "&branchid=" & oBranch.BranchID.Trim & "&flagfile=" & flagfile.Text.Trim
                hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & lblGirono2.Text.Trim & "','" & NewlblPDCreceiptno.Text.Trim & "','" & Me.sesBranchId.Replace("'", "") & "','" & flagfile.Text.Trim & "')"
            End If

            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

        End If
        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'Me.SearchBy = ""
        'oSearchBy.Text = ""
        'oSearchBy.BindData()
        'DoBind(Me.SearchBy, Me.SortBy)
        Response.Redirect("PDCEdit.aspx")
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Me.SearchBy = " BranchLocation = '" & Me.sesBranchId.Replace("'", "") & "' "
            If cboStatus.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and PDCStatus = '" & cboStatus.SelectedItem.Value & "'"
            Else
                Me.SearchBy = Me.SearchBy & " and PDCStatus in ('HD','OP','BC')"
            End If
            If osearchby.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & osearchby.ValueID & " = '" & osearchby.Text.Trim.Replace("'", "''") & "'"
            End If
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
            Dim lbltemp As Label
            Dim hyTemp As HyperLink
            Dim LblNewAmount As Label
            Dim lblBankID As Label
            Dim lblApplicationid As HyperLink
            Dim lblGirono2 As HyperLink
            Dim NewlblPDCreceiptno As Label
            Dim lblMaxGiroSeqNo As Label
            Select Case e.CommandName
                Case "EditAgreement"
                    lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
                    LblNewAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
                    lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
                    lblGirono2 = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
                    NewlblPDCreceiptno = CType(e.Item.FindControl("lblPDCreceiptno"), Label)
                    Response.Redirect("PDCEditMultiAgreement.aspx?GiroNo=" & lblGirono2.Text.Trim & "&ReceiptNo=" & NewlblPDCreceiptno.Text.Trim & "")
                Case "EditAllocation"
                    lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
                    LblNewAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
                    lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
                    lblGirono2 = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
                    NewlblPDCreceiptno = CType(e.Item.FindControl("lblPDCreceiptno"), Label)
                    lblMaxGiroSeqNo = CType(e.Item.FindControl("LblGiroSeqNo"), Label)

                    If CInt(lblMaxGiroSeqNo.Text.Trim) > 1 Then
                        Response.Redirect("PDCEditListMultiAgreement.aspx?GiroNo=" & lblGirono2.Text.Trim & "&ReceiptNo=" & NewlblPDCreceiptno.Text.Trim & "&bankId=" & lblBankID.Text.Trim & "")
                    Else
                        Response.Redirect("PDCEditProcess.aspx?applicationid=" & lblApplicationid.Text.Trim & "&PDCNo=" & lblGirono2.Text.Trim & "&ReceiptNo=" & NewlblPDCreceiptno.Text.Trim & "&Amount=" & LblNewAmount.Text.Trim & "&IsMultiAgreement=0&GiroSeqNo=1&bankId=" & lblBankID.Text.Trim & "")
                    End If
            End Select
        End If
    End Sub

End Class