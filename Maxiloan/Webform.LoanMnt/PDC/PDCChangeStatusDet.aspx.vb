﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCChangeStatusDet
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents cboreason As UCReason
    Dim flagfile As String
#Region "Property"
    Private Property PDCReceiptNo() As String
        Get
            Return CStr(viewstate("PDCReceiptNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("PDCReceiptNo") = Value
        End Set
    End Property

    Private Property girono() As String
        Get
            Return (CType(Viewstate("girono"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("girono") = Value
        End Set
    End Property

    Private Property newstatus() As String
        Get
            Return (CType(Viewstate("newstatus"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("newstatus") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCChangeStatusController

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblnewStat As New Label
            Me.FormID = "PDCChangeStatusDet"
            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                HyGiroNo.Text = Request.QueryString("girono")
                lblnewStat.Text = Request.QueryString("status")
                Me.newstatus = lblnewStat.Text
                Me.PDCReceiptNo = Request.QueryString("PDCReceiptNo")
                Me.girono = Request.QueryString("girono")
                HyGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & Me.girono & "','" & Me.PDCReceiptNo & "','" & Me.sesBranchId.Replace("'", "") & "','0')"
                FillNewStatus()
                DoBind()
            End If
        End If
    End Sub
#End Region

#Region "DoBind"
    Private Sub DoBind()
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.GiroNo = Me.girono
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass = oController.PDCStatusListDet(oCustomClass)
        With oCustomClass
            HyPDCReceiptNo.Text = .PDCReceiptNo
            flagfile = "1"
            HyPDCReceiptNo.NavigateUrl = "javascript:OpenWinPDC('" & Me.girono.Trim & "','" & Trim(HyPDCReceiptNo.Text) & "','" & Trim(oCustomClass.BranchId) & "','" & Trim(flagfile) & "')"
            lblReceiveDate.Text = .RecDate.ToString("dd/MM/yyyy")

            HyGiroNo.Text = .GiroNo
            lblBankPDC.Text = .BankPDCName
            lblPDCAmount.Text = FormatNumber(.PDCAmount, 2)
            lblPDCDueDate.Text = .PDCDueDate.ToString("dd/MM/yyyy")
            lblPDCType.Text = .PDCType
            If .PDCType = "G" Then
                lblPDCType.Text = "Bilyet Giro"
            Else
                lblPDCType.Text = "Cheque"
            End If
            lblBankClearing.Text = .BankClearingName
            'lblInkasoFlag.Text = CStr(.IsInkaso)
            If CStr(.IsInkaso) = "1" Then
                lblInkasoFlag.Text = "Yes"
            Else
                lblInkasoFlag.Text = "No"
            End If

            If CStr(.IsInkaso) = "1" Then
                lblCummFlag.Text = "Yes"
            Else
                lblCummFlag.Text = "No"
            End If
            If .PDCStatus = "OP" Then
                lblStatus.Text = "Open"
            ElseIf .PDCStatus = "HD" Then
                lblStatus.Text = "Hold"
            ElseIf .PDCStatus = "CC" Then
                lblStatus.Text = "Cancel"
            Else
                lblStatus.Text = "Bounce"
            End If
            'lblStatus.Text = .PDCStatus
            lblStatusDate.Text = .PDCStatusDate.ToString("dd/MM/yyyy")
            lblBounce.Text = CStr(.Bounce)
            If .PDCStatus <> "HD" Then
                lblHoldUntilDate.Text = "-"
            Else
                lblHoldUntilDate.Text = .HoldUntildate.ToString("dd/MM/yyyy")
            End If
            If .PDCStatus = "HD" Then
                pnldate.Visible = False
            End If
            If Me.newstatus = "Deposited" Then
                pnldate.Visible = False
            End If
        End With
    End Sub
#End Region

    Private Sub FillNewStatus()
        Dim strListData As String
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        With cboNewStatus
            Select Case Me.newstatus
                Case "Hold"
                    strListData = "OP,Open"
                Case "Open"
                    strListData = "HD,Hold-CC,Cancel"
                Case "Bounce"
                    strListData = "OP,Open-HD,Hold-CC,Cancel"
                Case "Deposited"
                    strListData = "DP,Open" 'save fisik status DP --> OH
            End Select
            '-------------Untuk process mengisi combo
            oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
            oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

            oRow = oDataTable.NewRow()
            oRow("ID") = ""
            oRow("Description") = ""

            splitListData = Split(strListData, "-")
            For i = 0 To UBound(splitListData)
                splitRow = Split(splitListData(i), ",")
                oRow("ID") = splitRow(0)
                oRow("Description") = splitRow(1)
                oDataTable.Rows.Add(oRow)
                oRow = oDataTable.NewRow()
            Next

            cboNewStatus.DataValueField = "ID"
            cboNewStatus.DataTextField = "Description"
            cboNewStatus.DataSource = oDataTable
            cboNewStatus.DataBind()
            '--------------------------
        End With
        cboReason.ReasonTypeID = "PDCHG"
        cboReason.BindReason()
    End Sub

    Private Sub btnCancelNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelNew.Click
        Response.Redirect("PDCChangeStatus.aspx")
    End Sub

    Private Sub cbonewstatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewStatus.SelectedIndexChanged
        pnldate.Visible = True
        If cboNewStatus.SelectedItem.Value <> "HD" Then
            pnldate.Visible = False
        End If
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "PDCChangeStatusDet"
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If cboNewStatus.SelectedItem.Value = "HD" Then
                If txtsdate.Text = "" Then
                    ShowMessage(lblMessage, "Harap isi Tanggal Hold", True)
                    Exit Sub

                ElseIf DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtsdate.Text)) <= 0 Then

                    ShowMessage(lblMessage, "Tanggal Hold harus > tgl hari ini", True)
                    Exit Sub

                End If
            End If
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .ReasonID = cboreason.ReasonID
                .ReasonTypeID = cboreason.ReasonTypeID
                .PDCStatus = cboNewStatus.SelectedItem.Value
                If cboNewStatus.SelectedItem.Value = "HD" Then
                    .HoldUntildate = ConvertDate2(txtsdate.Text)
                Else
                    .HoldUntildate = Me.BusinessDate
                End If

                .strConnection = GetConnectionString()
                .GiroNo = Me.girono
                .BusinessDate = Me.BusinessDate
                .Notes = txtnotes.Text
                .PDCReceiptNo = HyPDCReceiptNo.Text
            End With

            Dim ErrMsg As String

            Try
                oController.PDCSaveStatusDet(oCustomClass)
                Response.Redirect("PDCChangeStatus.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
            Catch ex As Exception

                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

End Class