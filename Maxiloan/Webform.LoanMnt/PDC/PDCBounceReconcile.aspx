﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCBounceReconcile.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCBounceReconcile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../Webform.UserController/UCReason.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDC Bounce Reconcile</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    TRANSAKSI TOLAKAN PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo PDC &lt;=
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label>
                    Kliring Bank
                </label>
                <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="GiroNo" OnSortCommand="SortGrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <ItemStyle Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PDCReceiptNo" HeaderText="PDCReceiptNo">
                                <ItemStyle Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPDCReceiptNo" runat="server" Text='<%#Container.DataItem("PDCReceiptNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BANKID" HeaderText="PDC BANK">
                                <HeaderStyle Width="8%"></HeaderStyle>
                                <ItemStyle Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="Total" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GIRONO" HeaderText="NO PDC">
                                <HeaderStyle Width="8%"></HeaderStyle>
                                <ItemStyle Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNo" runat="server" Text='<%#Container.DataItem("GIRONO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" SortExpression="PDCDueDate" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" Width="10%" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="TOtalPDCAmount" HeaderText="JUMLAH">
                                <HeaderStyle CssClass="th_right" Width="12%"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right" Width="13%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),0)%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblPDCAmount" Visible="false" runat="server" Text='<%#Container.DataItem("TotalPDCAmount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PILIHAN">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkAction" runat="server" Text="DITOLAK" CommandName="BOUNCE"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlBounce" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TOLAKAN PDC
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:HyperLink ID="HypGiroNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <asp:Label ID="lblBankPDC" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblViewAmount" runat="server"></asp:Label></div>
            <div class="form_right">
                <label>
                    Tgl jatuh Tempo PDC
                </label>
                <asp:Label ID="lblPDCDueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis PDC
                </label>
                <asp:Label ID="lblPDCType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kliring Bank
                </label>
                <asp:Label ID="lblBankClearing" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:Label ID="lblInkasoFlag" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:Label ID="lblCummFlag" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>HASIL TOLAKAN PDC</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan
                </label>
                <uc1:ucreason id="oReason" runat="server"></uc1:ucreason>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    No Bukti Kas Keluar
                </label>
                <asp:TextBox ID="txtReferenceNo" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAmount" runat="server" Visible="true" ErrorMessage="Harap isi No Bukti Kas Keluar"
                    ControlToValidate="txtReferenceNo" Display="Dynamic" Enabled="False" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Tanggal Valuta
                </label>
                <asp:TextBox runat="server" ID="txtValueDate" CssClass ="small_text" ></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RftxtValueDate" runat="server" ErrorMessage="Harap isi tgl valuta"
                            ControlToValidate="txtValueDate" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_general" >
                    Catatan
                </label>
                <asp:TextBox ID="txtnotes" runat="server" CssClass="multiline_textbox"  TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnBounce" runat="server" Text="Ditolak" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
