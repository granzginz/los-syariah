﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCEditMultiAgreement.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCEditMultiAgreement" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankName" Src="../../Webform.UserController/UcBankName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCLookupAgreement" Src="../../Webform.UserController/UCLookupAgreement.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCEditMultiAgreement</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PDC MULTIPLE KONTRAK
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Terima Dari
                </label>
                <asp:TextBox ID="txtReceiveFrom" runat="server" Width="180px" MaxLength="50" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Visible="true"
                    ControlToValidate="txtReceiveFrom" Display="Dynamic" ErrorMessage="Harap diisi Terima Dari"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:HyperLink ID="txtPDCNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblPDCAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Bank PDC
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboBank" runat="server" ControlToValidate="cboBank"
                    ErrorMessage="Harap Pilih Bank" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Kumulatif
                </label>
                <asp:RadioButtonList ID="rdoCumm" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="form_right">
                <label>
                    Jenis PDC
                </label>
                <asp:RadioButtonList ID="rdoPDCType" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="G" Selected="True">BG</asp:ListItem>
                    <asp:ListItem Value="C">Cheque</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Kontrak
            </label>
            <uc1:uclookupagreement id="oLookupAgreement" runat="server">
            </uc1:uclookupagreement>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancelNew" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    <asp:Panel ID="pnlPDC" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgreement" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                        Width="100%" ShowFooter="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblBranchAgreement" runat="server" Text='<%#Container.DataItem("branchagreement")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName1" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <%--<asp:TextBox ID="txtAmountPayment" runat="server"  Width="100%"
                                        Text='<%#Container.DataItem("AmountPayment")%>' Enabled='<%#iif(Container.DataItem("CanBeEdit")="1", True, False)%>'>
                                    </asp:TextBox>--%>
                                    <uc1:ucnumberformat ID="txtAmountPayment" runat="server"  width="100%"
                                        text='<%#Container.DataItem("AmountPayment")%>' enabled='<%#iif(Container.DataItem("CanBeEdit")="1", True, False)%>' />
                                    </uc1:ucNumberFormat>
                                    <asp:Label ID="lblCanBeEdit" runat="server" Visible="False" Width="100%" Text='<%#Container.DataItem("CanBeEdit")%>'>
                                    </asp:Label>
                                    <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmountPayment"
                                        ErrorMessage="Harap diisi Jumlah Dibayar" Visible="True" Display="Dynamic" Enabled="True"
                                        CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rvfAmount" runat="server" ControlToValidate="txtAmountPayment"
                                        ErrorMessage="harap diisi dengan Angka" Enabled="True" Display="Dynamic" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                                        Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
                                    <asp:RangeValidator ID="rgValAmount" runat="server" Type="Double" ErrorMessage="Jumlah Dibayar harus > 0"
                                        ControlToValidate="txtAmountPayment" MinimumValue="1" MaximumValue='999999999999999'
                                        Visible="True" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblSumAmountPayment" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        Visible='<%#iif(Container.DataItem("CanBeEdit")="1",True,False)%>' CommandName="DELETE">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
