﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCClearReconC.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCClearReconC" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCClearReconC</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PDC CAIR
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:HyperLink ID="HyGiroNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <asp:Label ID="lblBankPDC" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah
                </label>
                <asp:Label ID="lblPDCAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:Label ID="lblPDCDueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis PDC
                </label>
                <asp:Label ID="lblPDCType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kliring Bank
                </label>
                <asp:Label ID="lblBankClearing" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:Label ID="lblInkasoFlag" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:Label ID="lblCummFlag" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>HASIL KLIRING PDC - CAIR</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Jumlah Diterima
                </label>               
                     <uc1:ucnumberformat id="txtamountRec" runat="server" /></uc1:ucNumberFormat>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan
                </label>
                <asp:TextBox ID="txtnotes" runat="server"  TextMode="MultiLine"
                    Width="464px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnClearRecon" runat="server" CausesValidation="True" Text="Clear"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnCancelRecon" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
