﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCRInquiryView
    Inherits Maxiloan.Webform.WebBased
    Dim temptotalPDC As Double
#Region "Property"

    Private Property GiroNo() As String
        Get
            Return (CType(Viewstate("GiroNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property
    Private Property PDCReceiptNo() As String
        Get
            Return (CType(Viewstate("PDCReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property
    Private Property flagfile() As String
        Get
            Return (CType(Viewstate("flagfile"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("flagfile") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lblgiro As New Label
            Dim lblnewStat As New Label
            Dim inFlagfile As New Label
            Me.FormID = "PDCRINQUIRYVIEW"

            If checkForm(Me.Loginid, Me.FormID, Me.AppId) Then
                inFlagfile.Text = Request.QueryString("flagfile")
                Me.flagfile = inFlagfile.Text
                Me.PDCReceiptNo = Request.QueryString("PDCReceiptNo")
                Me.BranchID = Request.QueryString("branchid")
                lblgiro.Text = Request.QueryString("girono")
                Me.GiroNo = lblgiro.Text
                lblnewStat.Text = Request.QueryString("status")
                If Me.flagfile <> "1" Then
                    BtnBack.Visible = True
                    pnlbutton.Visible = False
                Else
                    BtnBack.Visible = False
                    pnlbutton.Visible = True
                End If
                DoBind()
                DoBindDetail()
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass = oController.PDCRInquiryListV(oCustomClass)
        With oCustomClass
            lblPDCReceiptNoV.Text = .PDCReceiptNo
            lblReceiveDateV.Text = .RecDate.ToString("dd/MM/yyyy")

            lblReceiveFromV.Text = .ReceivedFrom
            lblReceiveByV.Text = .ReceivedBy

            lblNumOfPrint.Text = CStr(.NumOfPrint) + " " + "time(s)"
            If CStr(.NumOfPrint) > "0" Then
                lblLastPrintReciptV.Text = .LastPrintRecDate.ToString("dd/MM/yyyy")
            Else
                lblLastPrintReciptV.Text = CStr("-")
            End If

        End With

    End Sub

    Sub DoBindDetail()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        oCustomClass.PDCReceiptNo = Me.PDCReceiptNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass = oController.PDCRInquiryListDet(oCustomClass)

        DtUserList = oCustomClass.ListPDC
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgPDC.DataSource = DvUserList

        Try
            DtgPDC.DataBind()
        Catch
            'DtgPDC.CurrentPageIndex = 0
            DtgPDC.DataBind()
        End Try
    End Sub
#End Region
    Private Sub DtgPDC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPDC.ItemDataBound
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim hypGiroNo As HyperLink
        Dim flagfile As New Label
        If flagfile.Text.Trim <> "" Then
            flagfile.Text = "1"
        Else
            flagfile.Text = Me.flagfile
        End If
        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblTotalPDCAmountV"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmountV"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If
        If e.Item.ItemIndex >= 0 Then
            hypGiroNo = CType(e.Item.FindControl("lblGiroNoV"), HyperLink)
            hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?PDCReceiptNo=" & Me.PDCReceiptNo.Trim & "&branchid=" & oCustomClass.BranchId & "&girono=" & hypGiroNo.Text.Trim & "&flagfile=" & flagfile.Text.Trim
        End If
    End Sub


    Private Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Response.Redirect("PDCInquiryDetail.aspx?PDCReceiptNo=" & lblPDCReceiptNoV.Text.Trim & "&branchid=" & Me.BranchID.Trim & "&girono=" & Me.GiroNo.Trim & "&flagfile=" & Me.flagfile.Trim)
    End Sub

End Class