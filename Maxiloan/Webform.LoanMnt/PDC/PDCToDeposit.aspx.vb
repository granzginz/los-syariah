﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCToDeposit
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBank As UcBankMaster
    Protected WithEvents oBankAccount As UcBankAccountID
    Dim temptotalPDC As Double

#Region "Property"

    Private Property SelectAll2() As Double
        Get
            Return (CType(Viewstate("SelectAll"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("SelectAll") = Value
        End Set
    End Property

    Private Property strGroupID() As String
        Get
            Return (CType(Viewstate("strGroupID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strGroupID") = Value
        End Set
    End Property

    Private Property strReceiptNo() As String
        Get
            Return (CType(Viewstate("strReceiptNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strReceiptNo") = Value
        End Set
    End Property
    Private Property TempTable() As DataTable
        Get
            Return (CType(Viewstate("TempTable"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TempTable") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCDepositController
#End Region

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PDCDEPOSIT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                oBankAccount.BankPurpose = ""
                oBankAccount.BankType = "B"
                oBankAccount.BindBankAccount()
                oSearchBy.ListData = "GIRONO, PDC No."
                oSearchBy.BindData()
                ' Me.SelectAll2 = 0

                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        Else

            Me.SelectAll2 = Me.SelectAll2
        End If
        'Put user code to initialize the page here
    End Sub

#End Region


#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)

        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spPDCDepositListPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()

        If DtgAgree.Items.Count > 0 Then
            BtnNext.Visible = True
        Else
            BtnNext.Visible = False
        End If

        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'Me.SearchBy = ""
        'oSearchBy.Text = ""
        'oSearchBy.BindData()
        Response.Redirect("PDCToDeposit.aspx")
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Me.SearchBy = " BranchLocation = '" & Me.sesBranchId.Replace("'", "") & "' and PDCDueDate <= '" & ConvertDate2(txtsdate.Text) & "'"

            If cboInkaso.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & "  and  IsInkaso = '" & cboInkaso.SelectedItem.Value & "'"
            End If
            If cboCumm.SelectedItem.Value <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and  IsCummulative = '" & cboCumm.SelectedItem.Value & "'"
            End If
            If oBank.BankID <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and  bankid = '" & oBank.BankID & "'"
            End If

            If oSearchBy.Text.Trim <> "" Then
                'If Right(oSearchBy.Text.Trim, 1) = "%" Then
                'Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " Like '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
                Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " Like '" & "%" & oSearchBy.Text.Trim.Replace("'", "''") & "%" & "'"
                'Else
                'Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
                'End If
            End If

            If chkHasilTolakan.Checked = False Then
                Me.SearchBy = Me.SearchBy & " and PDCStatus = 'OP'"
            Else
                Me.SearchBy = Me.SearchBy & " and PDCStatus = 'BC'"
            End If

            pnlDatagrid.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
            pnlDatagrid.Visible = True
            pnlList.Visible = True
            DtgAgree.Visible = True
        End If

    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    'Public Sub SortGrid2(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
    '    If InStr(Me.SortBy, "DESC") > 0 Then
    '        Me.SortBy = e.SortExpression
    '    Else
    '        Me.SortBy = e.SortExpression + " DESC"
    '    End If
    '    DoBind2(Me.SearchBy, Me.SortBy)
    'End Sub
#End Region

#Region "Select All"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ChkSelectAll As CheckBox
        Dim ChkSelect As CheckBox
        Dim x As Integer
        Dim chk As New CheckBox

        For x = 0 To DtgAgree.Items.Count - 1
            chk = CType(DtgAgree.Items(x).FindControl("ChkSlct"), CheckBox)
            If CType(sender, CheckBox).Checked Then
                chk.Checked = True
                Me.SelectAll2 = 1
            Else
                chk.Checked = False
                Me.SelectAll2 = 0
            End If
        Next


        'If Me.SelectAll2 = 0 Then
        '    For x = 0 To DtgAgree.Items.Count - 1
        '        ChkSelect = CType(DtgAgree.Items(x).FindControl("ChkSlct"), CheckBox)
        '        ChkSelect.Checked = True
        '    Next
        '    ' Me.SelectAll2 = 1
        'Else
        '    For x = 0 To DtgAgree.Items.Count - 1
        '        ChkSelect = CType(DtgAgree.Items(x).FindControl("ChkSlct"), CheckBox)
        '        ChkSelect.Checked = False
        '    Next
        '    '  Me.SelectAll2 = 0
        ' End If
    End Sub
#End Region

#Region "Generate Temp Table"
    Private Function GenerateTempTable() As DataTable
        Dim lObjDataTable As New DataTable("TempTable")
        With lObjDataTable
            .Columns.Add("BankID", System.Type.GetType("System.String"))
            .Columns.Add("GIRONO", System.Type.GetType("System.String"))
            .Columns.Add("PDCDueDate", System.Type.GetType("System.DateTime"))
            .Columns.Add("TotalPDCAmount", System.Type.GetType("System.String"))
            .Columns.Add("IsInkaso", System.Type.GetType("System.String"))
            .Columns.Add("IsCummulative", System.Type.GetType("System.String"))
            .Columns.Add("NumberofBounce", System.Type.GetType("System.String"))
            .Columns.Add("PDCreceiptNo", System.Type.GetType("System.String"))
            .Columns.Add("BranchID", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function
#End Region

#Region "Next"

    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim NewchkBox As CheckBox
            Dim lblPDCNONew As HyperLink
            Dim strGroupID As String
            Dim dtPDC As New DataTable
            Dim lblPDCReceiptNo As HyperLink
            Dim strReceiptNo As String
            Dim pdcvaluedate As ValidDate


            'strGroupID = ""
            'For intLoopGrid = 0 To DtgAgree.Items.Count - 1
            '    NewchkBox = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("ChkSlct"), CheckBox)
            '    lblPDCNONew = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblGiroNo"), HyperLink)
            '    If NewchkBox.Checked = True Then
            '        strGroupID &= CStr(IIf(strGroupID = "", "", ",")) & "'" & lblPDCNONew.Text.Trim & "'"
            '    End If
            'Next

            'If Trim(strGroupID) = "" Then
            '    lblMessage.Text = "Please Check Item"
            '    Exit Sub
            'End If
            'strReceiptNo = ""
            'For intLoopGrid = 0 To DtgAgree.Items.Count - 1
            '    NewchkBox = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("ChkSlct"), CheckBox)
            '    lblPDCReceiptNo = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblPDCReceiptNo"), HyperLink)
            '    If NewchkBox.Checked = True Then
            '        strReceiptNo &= CStr(IIf(strReceiptNo = "", "", ",")) & "'" & lblPDCReceiptNo.Text.Trim & "'"
            '    End If
            'Next



            If Me.TempTable Is Nothing Then
                Me.TempTable = GenerateTempTable()
            End If
            Me.TempTable.Clear()
            Dim lObjDataRow As DataRow
            For intLoopGrid = 0 To DtgAgree.Items.Count - 1
                NewchkBox = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("ChkSlct"), CheckBox)
                If NewchkBox.Checked = True Then
                    lObjDataRow = Me.TempTable.NewRow()
                    lObjDataRow("BankID") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblBankID"), Label).Text
                    lObjDataRow("GIRONO") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblGiroNo"), HyperLink).Text
                    lObjDataRow("PDCDueDate") = ConvertDate2(DtgAgree.Items(intLoopGrid).Cells(3).Text).ToString("MM/dd/yyyy")
                    lObjDataRow("TotalPDCAmount") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblPDCAmount"), Label).Text
                    lObjDataRow("IsInkaso") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblInkaso"), Label).Text
                    lObjDataRow("IsCummulative") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblCumm"), Label).Text
                    lObjDataRow("NumberofBounce") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("Label1"), Label).Text
                    lObjDataRow("PDCreceiptNo") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("LblPDCReceiptNo"), HyperLink).Text
                    lObjDataRow("BranchID") = CType(DtgAgree.Items(intLoopGrid).Cells(0).FindControl("lblpbranchid"), Label).Text
                    Me.TempTable.Rows.Add(lObjDataRow)
                End If

            Next
            If Me.TempTable.Rows.Count > 0 Then
                'Me.strReceiptNo = strReceiptNo
                With oCustomClass
                    .ListPDCSave = Me.TempTable
                    oCustomClass.BranchId = Me.BranchID
                End With
                'Me.SearchBy = " GiroNo in (" & Trim(strGroupID) & ") and PDCReceiptno  in (" & Trim(strReceiptNo) & ")"
                DtGridDeposit.DataSource = Me.TempTable.DefaultView
                DtGridDeposit.DataBind()
                lblPDCDueDate.Text = ConvertDate2(txtsdate.Text).ToString("dd/MM/yyyy")
                lblInk.Text = cboInkaso.SelectedItem.Text
                lblCummulative.Text = cboCumm.SelectedItem.Text
                lblBankPDC.Text = oBank.BankName
                lblMessage.Text = ""

                pnlList2.Visible = True
                pnlList.Visible = False
                DtgAgree.Visible = False
                pnlDatagrid.Visible = False
                DtGridDeposit.Visible = True
            Else

                ShowMessage(lblMessage, "Harap pilih Item PDC untuk deposit", True)
            End If

        End If
    End Sub


#End Region

#Region "DtgAgree DataBound"

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim totalPDCAmount As New Label
        Dim lPDCAmount As New Label
        Dim flagfile As New Label
        Dim hypGiroNo As HyperLink
        Dim NlblGiroNo As HyperLink
        Dim NlblPDCReceiptno As HyperLink
        Dim Nlblpbranchid As Label
        flagfile.Text = "0"

        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblPDCAmount"), Label)
            temptotalPDC += CDbl(lPDCAmount.Text)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmount"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If
        If sessioninvalid() Then
            Exit Sub
        End If


        If e.Item.ItemIndex >= 0 Then
            If CheckFeature(Me.Loginid, Me.FormID, "PDC", Me.AppId) Then

                NlblPDCReceiptno = CType(e.Item.FindControl("lblPDCReceiptNo"), HyperLink)
                Nlblpbranchid = CType(e.Item.FindControl("lblpbranchid"), Label)
                hypGiroNo = CType(e.Item.FindControl("lblGiroNo"), HyperLink)
                'hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & NlblPDCReceiptno.Text.Trim & "&branchid=" & Nlblpbranchid.Text.Trim & "&flagfile=" & flagfile.Text.Trim
                hypGiroNo.NavigateUrl = "javascript:OpenWinPDC('" & Trim(hypGiroNo.Text) & "','" & Trim(NlblPDCReceiptno.Text) & "','" & Trim(Nlblpbranchid.Text) & "','" & Trim(flagfile.Text) & "')"
            End If
        End If
    End Sub
#End Region

#Region "DtGridDeposit DataBound"
    Private Sub DtGridDeposit_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtGridDeposit.ItemDataBound
        Dim totalPDCAmount2 As New Label
        Dim lPDCAmount2 As New Label
        Dim flagfile As New Label
        Dim hypGiroNo2 As HyperLink
        Dim NlblGiroNo2 As HyperLink
        Dim NlblPDCReceiptno2 As HyperLink
        Dim Nlblpbranchid2 As Label
        flagfile.Text = "0"


        If e.Item.ItemIndex >= 0 Then
            lPDCAmount2 = CType(e.Item.FindControl("lblPDCAmount2"), Label)
            temptotalPDC += CDbl(lPDCAmount2.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount2 = CType(e.Item.FindControl("lblTotPDCAmount2"), Label)
            totalPDCAmount2.Text = FormatNumber(temptotalPDC.ToString, 2)
        End If


        If e.Item.ItemIndex >= 0 Then
            If CheckFeature(Me.Loginid, Me.FormID, "PDC", Me.AppId) Then
                NlblPDCReceiptno2 = CType(e.Item.FindControl("lblPDCReceiptNo2"), HyperLink)
                Nlblpbranchid2 = CType(e.Item.FindControl("lblpbranchid2"), Label)
                hypGiroNo2 = CType(e.Item.FindControl("lblGiroNo2"), HyperLink)
                'hypGiroNo.NavigateUrl = "PDCInquiryDetail.aspx?GiroNo=" & hypGiroNo.Text.Trim & "&PDCReceiptNo=" & NlblPDCReceiptno.Text.Trim & "&branchid=" & Nlblpbranchid.Text.Trim & "&flagfile=" & flagfile.Text.Trim
                hypGiroNo2.NavigateUrl = "javascript:OpenWinMain('" & Trim(hypGiroNo2.Text) & "','" & Trim(NlblPDCReceiptno2.Text) & "','" & Trim(Nlblpbranchid2.Text) & "','" & Trim(flagfile.Text) & "')"
            End If
        End If
    End Sub
#End Region

#Region "Cancel"

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Me.SelectAll2 = 0
        Response.Redirect("PDCToDeposit.aspx")
    End Sub
#End Region

#Region "Deposit"

    Private Sub Btndeposit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btndeposit.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "DPST", Me.AppId) Then
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .strConnection = GetConnectionString()
                .ListPDCSave = Me.TempTable
                .BankaccountIDDep = oBankAccount.BankAccountID
                .BusinessDate = Me.BusinessDate
                .ReferenceNo = txtRefNo.Text
            End With
            Try
                oCustomClass = oController.SavePDCDeposit(oCustomClass)
                If oCustomClass.strError <> "" Then

                    ShowMessage(lblMessage, "PDC Sudah diDeposit oleh User lain", True)
                    Exit Sub
                Else
                    pnlList.Visible = True
                    pnlList2.Visible = False
                    DtGridDeposit.Visible = False

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                Exit Sub
            End Try
        End If
    End Sub
#End Region

End Class