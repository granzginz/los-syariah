﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCMultiAgreementList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCMultiAgreementList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCMultiAgreementList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinProductOfferingBranchView(pProductOfferingID, pProductID, pBranchID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductOfferingBranchView.aspx?ProductOfferingID=' + pProductOfferingID + '&ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                TERIMA PDC MULTIPLE KONTRAK
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan
            </label>
            <asp:DropDownList ID="cboSearch" runat="server">
                <asp:ListItem Value="Name">Nama Konsumen</asp:ListItem>
                <asp:ListItem Value="Address">Alamat</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="pnlList" runat="server" Width="100%" HorizontalAlign="center">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONSUMEN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server"   Width="100%" DataKeyField="CustomerID"
                        CssClass="grid_general" OnSortCommand="Sorting" AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:LinkButton CommandName="Receive" ID="lnkNewApp" CausesValidation="false" runat="server">TERIMA</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBadType" runat="server" CausesValidation="false" />
                                    <asp:LinkButton ID="lnkCustName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                    </asp:LinkButton>
                               </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CustomerType" SortExpression="CustomerType" HeaderText="TIPE">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="BadType" HeaderText="BadType"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID" HeaderText="CustomerID">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No halaman Salah" MinimumValue="1"
                            Type="integer" ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
