﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCRInquiryView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCRInquiryView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCRInquiryView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlview" runat="server" EnableViewState="False" Visible="true">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - TERIMA PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Tanda Terima PDC
                </label>
                <asp:Label ID="lblPDCReceiptNoV" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Terima
                </label>
                <asp:Label ID="lblReceiveDateV" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Terima Dari
                </label>
                <asp:Label ID="lblReceiveFromV" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>STATUS</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Diterima Oleh
                </label>
                <asp:Label ID="lblReceiveByV" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Terakhir Cetak
                </label>
                <asp:Label ID="lblLastPrintReciptV" EnableViewState="False" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Cetak ke
                </label>
                <asp:Label ID="lblNumOfPrint" EnableViewState="False" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPDC" EnableViewState="False" runat="server" CssClass="grid_general"
                        AutoGenerateColumns="False" AllowSorting="True" ShowFooter="True" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="BANK PDC">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBankIDV" runat="server" EnableViewState="False" Text='<%#Container.DataItem("BANKID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="Label9" runat="server" EnableViewState="False" Text="Total"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO PDC">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="11%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblGiroNoV" runat="server" EnableViewState="False" Text='<%#Container.DataItem("Girono")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="13%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalPDCAmountV" runat="server" EnableViewState="False" Text='<%#formatnumber(Container.DataItem("TotalPDCAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmountV" runat="server" EnableViewState="False"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PDCDueDate" HeaderText="TGL JATUH TEMPO" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="INKASO">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblInkasoV" runat="server" EnableViewState="False" Text='<%#Iif(Container.DataItem("IsInkaso")="True" ,"Yes","No")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KUMULATIF">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCummV" runat="server" EnableViewState="False" Text='<%#Iif(Container.DataItem("IsCummulative")="True" , "Yes","No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TYPE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTypeV" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PDCTypedesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <asp:Panel ID="pnlbutton" EnableViewState="False" runat="server">
            <div class="form_button">
                <asp:Button ID="BtnBack" EnableViewState="False" runat="server" CausesValidation="False"
                    Text="Cancel" CssClass="small button gray"></asp:Button>
                <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
