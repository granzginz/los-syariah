﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCInquiryDetail.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PDCInquiryDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCInquiryDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlview" Visible="true" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY PDC
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Tanda Terima PDC
                </label>
                <asp:HyperLink ID="lblPDCReceiptNoV" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Terima
                </label>
                <asp:Label ID="lblReceiveDateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblAgreeBranchV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="HyAgreementNoV" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="HyCustomerNameV" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No PDC
                </label>
                <asp:Label ID="lblGiroNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Bank PDC
                </label>
                <asp:Label ID="lblBankPDC" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah PDC
                </label>
                <asp:Label ID="lblPDCAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tgl Jatuh Tempo PDC
                </label>
                <asp:Label ID="lblPDCDueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis PDC
                </label>
                <asp:Label ID="lblPDCType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kliring Bank
                </label>
                <asp:Label ID="lblBankClearing" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Flag Inkaso
                </label>
                <asp:Label ID="lblInkasoFlag" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Flag Kumulatif
                </label>
                <asp:Label ID="lblCummFlag" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>STATUS PDC</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status
                </label>
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Ditolak
                </label>
                <asp:Label ID="lblNumOfBounce" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Hold
                </label>
                <asp:Label ID="lblHolddate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    ALOKASI PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPA" runat="server" Width="100%" ShowFooter="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSACTION" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="37%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPA" runat="server" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPA2" runat="server" Text='<%#Container.DataItem("PaymentAllocationDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="Total"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH ALOKASI">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmt" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH CAIR">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="19%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountClear" runat="server" Text='<%#formatnumber(Container.DataItem("AmountClear"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPDCAmtClear" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    HISTORY PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPDC" runat="server" Width="100%" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%#Container.DataItem("PDCSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="StatusDate" HeaderText="TGL STATUS" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTrans" runat="server" Text='<%#Container.DataItem("PDCStatusDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ALASAN">
                                <HeaderStyle HorizontalAlign="Center" Width="35%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="45%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblReason" runat="server" Text='<%#Container.DataItem("Reasondesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LOKASI">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLoc" runat="server" Text='<%#Container.DataItem("branchInitialName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <asp:Panel ID="pnlbutton" runat="server">
            <div class="form_button">
                <asp:Button ID="BtnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button>
                <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
