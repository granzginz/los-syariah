﻿Imports Maxiloan.Controller

Public Class BatchTransactionUpload
    Inherits Maxiloan.Webform.WebBased

    Private oController As New BatchVoucherController

#Region " Properties"
    Private Property BatchSequenceNo() As Integer
        Get
            Return CInt(Request.QueryString("BatchSequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            Request.QueryString("BatchSequenceNo") = CStr(Value)
        End Set
    End Property

    Private Property BatchNo() As String
        Get
            Return Request.QueryString("batchno")
        End Get
        Set(ByVal Value As String)
            Request.QueryString("batchno") = Value
        End Set
    End Property

    Private Property TableBatch() As DataTable
        Get
            Return (CType(Viewstate("TableBatch"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TableBatch") = Value
        End Set
    End Property

    Private Property ExcelFilePath() As String
        Get
            Return CType(viewstate("ExcelFilePath"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExcelFilePath") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("branchid")
            pnlResult.Visible = False
        End If
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If fileUploadCheck() Then
            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "Polis\" & Me.Session.SessionID + Me.Loginid + "BatchTransacation.xls"
            fileUpload.PostedFile.SaveAs(strFileLocation)
            Me.ExcelFilePath = strFileLocation

            'direct save on single server
            'SaveUpload()

            'save on separated web server and db server
            saveSingleBatch()
        End If
    End Sub

    Public Function fileUploadCheck() As Boolean
        If fileUpload.PostedFile Is Nothing Then Return False
        If fileUpload.PostedFile.ContentLength = 0 Then Return False
        Return True
    End Function

    Private Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("TableBatch")
        With lObjDataTable
            .Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
            .Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
            .Columns.Add("DepartmentID", System.Type.GetType("System.String"))
            .Columns.Add("DepartmentName", System.Type.GetType("System.String"))
            .Columns.Add("TransactionDescription", System.Type.GetType("System.String"))
            .Columns.Add("AmountTransaction", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function

    'ini save upload untuk direct open rowset jika web server 
    'satu mesin dengan server database
    '
    'Private Sub SaveUpload()
    '    Dim dt As New DataTable
    '    Dim oCustomClass As New Parameter.BatchVoucher
    '    Dim oCustomClass1 As New Parameter.BatchVoucher
    '    Try
    '        With oCustomClass
    '            .strConnection = GetConnectionString
    '            .BatchNo = Me.BatchNo
    '            .ExcelFilePath = Me.ExcelFilePath
    '        End With

    '        oCustomClass = oController.getTransactionBatchUpload(oCustomClass)
    '        dt = oCustomClass.listdata
    '    Catch ex As Exception
    '        LblErrorMessages.Text = ex.Message
    '        Exit Sub
    '    End Try

    '    If Me.TableBatch Is Nothing Then Me.TableBatch = GetStructPDC()

    '    Dim lObjDataRow As DataRow
    '    Dim i As Integer
    '    Dim bError As Boolean

    '    bError = False

    '    For i = 0 To dt.Rows.Count - 1
    '        Me.TableBatch.Clear()

    '        lObjDataRow = Me.TableBatch.NewRow()

    '        lObjDataRow("PaymentAllocationID") = "INSTALLRCV"
    '        lObjDataRow("PaymentAllocationName") = "Installment Receive"
    '        lObjDataRow("DepartmentID") = "-"
    '        lObjDataRow("DepartmentName") = ""
    '        lObjDataRow("TransactionDescription") = "Pembayaran Angsuran"
    '        lObjDataRow("AmountTransaction") = dt.Rows(i).Item("amount")

    '        Me.TableBatch.Rows.Add(lObjDataRow)

    '        With oCustomClass1
    '            .strConnection = GetConnectionString
    '            .BranchId = Me.BranchID
    '            .BatchNo = Me.BatchNo
    '            .BatchSequenceNo = Me.BatchSequenceNo
    '            .ReceivedFrom = dt.Rows(i).Item("customername").ToString
    '            .BusinessDate = Me.BusinessDate
    '            .BranchAgreement = dt.Rows(i).Item("branchagreement").ToString
    '            .ApplicationID = dt.Rows(i).Item("applicationid").ToString
    '            .ValueDate = CDate(dt.Rows(i).Item("valuedate"))
    '            .ReferenceNo = dt.Rows(i).Item("referenceno").ToString
    '            .BatchDescription = ""
    '            .Notes = ""
    '            .AmountReceive = CDbl(dt.Rows(i).Item("amount"))
    '            .TableBatch = Me.TableBatch
    '        End With

    '        Try
    '            oController.CashBankBatchVoucherAdd(oCustomClass1)
    '        Catch ex As Exception
    '            LblErrorMessages.Text = ex.Message
    '            bError = True
    '            Exit For
    '        End Try
    '    Next

    '    If Not bError Then Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
    'End Sub

    Private Sub saveSingleBatch()
        Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Me.ExcelFilePath & ";Extended Properties=""Excel 8.0;HDR=YES;"""
        Dim objComm As New OleDb.OleDbCommand
        Dim objConn As New OleDb.OleDbConnection(strConn)
        If objConn.State = ConnectionState.Closed Then objConn.Open()
        objComm.Connection = objConn
        objComm.CommandType = CommandType.Text
        objComm.CommandText = "select * from [Sheet1$]"
        Dim oReader As OleDb.OleDbDataReader
        Dim dt1 As New DataTable
        Dim oCols(5) As String
        oReader = objComm.ExecuteReader(CommandBehavior.CloseConnection)
        dt1.Columns.Add("nokontrak")
        dt1.Columns.Add("referenceno")
        dt1.Columns.Add("valuedate")
        dt1.Columns.Add("amount")
        dt1.Columns.Add("BatchNo")
        dt1.Columns.Add("BranchID")
        While oReader.Read
            oCols(0) = oReader.Item("nokontrak").ToString
            oCols(1) = oReader.Item("referenceno").ToString
            oCols(2) = oReader.Item("valuedate").ToString
            oCols(3) = oReader.Item("amount").ToString
            oCols(4) = Request("BatchNo")
            oCols(5) = Request("BranchID")
            dt1.Rows.Add(oCols)
        End While
        objConn.Close()

        Dim oCustomClass As New Parameter.BatchVoucher
        With oCustomClass
            .ListData = dt1
            .strConnection = GetConnectionString
        End With
        Dim bError As Boolean
        Try
            oController.saveSingleBatch(oCustomClass)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
            bError = True
        End Try
        objConn.Dispose()
        objComm.Dispose()
        oReader.Close()
        Dim dt As New DataTable
        Try
            oCustomClass.strConnection = GetConnectionString
            oCustomClass.BatchNo = Request("BatchNo")
            oCustomClass.BranchId = Request("BranchID")
            oCustomClass = oController.getCompleteBatch(oCustomClass)
            dt = oCustomClass.ListData
            dtResult.DataSource = dt
            dtResult.DataBind()
            pnlUpload.Visible = False
            pnlResult.Visible = True
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub

    Private Sub SaveUploadNew()
        Dim oCustomClass1 As New Parameter.BatchVoucher
        If Me.TableBatch Is Nothing Then Me.TableBatch = GetStructPDC()

        Dim lObjDataRow As DataRow
        Dim i As Integer
        Dim bError As Boolean

        bError = False

        For i = 0 To dtResult.Items.Count - 1
            Me.TableBatch.Clear()

            lObjDataRow = Me.TableBatch.NewRow()

            lObjDataRow("PaymentAllocationID") = "INSTALLRCV"
            lObjDataRow("PaymentAllocationName") = "Installment Receive"
            lObjDataRow("DepartmentID") = "-"
            lObjDataRow("DepartmentName") = ""
            lObjDataRow("TransactionDescription") = "Pembayaran Angsuran"
            lObjDataRow("AmountTransaction") = dtResult.Items(i).Cells(3).Text

            Me.TableBatch.Rows.Add(lObjDataRow)

            With oCustomClass1
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .BatchNo = Me.BatchNo
                .BatchSequenceNo = Me.BatchSequenceNo
                .ReceivedFrom = dtResult.Items(i).Cells(5).Text
                .BusinessDate = Me.BusinessDate
                .BranchAgreement = dtResult.Items(i).Cells(6).Text
                .ApplicationID = dtResult.Items(i).Cells(4).Text
                .ValueDate = CDate(dtResult.Items(i).Cells(2).Text)
                .ReferenceNo = dtResult.Items(i).Cells(1).Text
                .BatchDescription = ""
                .Notes = ""
                .AmountReceive = CDbl(dtResult.Items(i).Cells(3).Text)
                .TableBatch = Me.TableBatch
            End With

            Try
                oController.CashBankBatchVoucherAdd(oCustomClass1)
            Catch ex As Exception                
                ShowMessage(lblMessage, ex.Message, True)
                bError = True
                Exit For
            End Try
        Next
        If Not bError Then
            If deleteUploadedBatch() Then Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveUploadNew()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If deleteUploadedBatch() Then Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
    End Sub

    Private Function deleteUploadedBatch() As Boolean
        Dim oCustomClass As New Parameter.BatchVoucher
        With oCustomClass
            .strConnection = GetConnectionString
            .BatchNo = Request("BatchNo")
            .BranchId = Request("BranchID")
        End With
        Try
            oController.DeleteUploadedBatch(oCustomClass)
            Return True
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

End Class