﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BatchTransactionList
    Inherits Maxiloan.Webform.AccMntWebBased   
    Protected WithEvents oCashier As UcCashier
    Protected WithEvents oBatchVoucher As ucBatchVoucher
#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property BatchNo() As String
        Get
            Return CStr(viewstate("BatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("BatchNo") = Value
        End Set
    End Property

    Private Property BatchDescription() As String
        Get
            Return CStr(viewstate("BatchDescription"))
        End Get
        Set(ByVal Value As String)
            viewstate("BatchDescription") = Value
        End Set
    End Property

    Private Property ProcessID() As String
        Get
            Return CStr(viewstate("ProcessID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ProcessID") = Value
        End Set
    End Property

    Private Property Flag() As String
        Get
            Return CStr(viewstate("Flag"))
        End Get
        Set(ByVal Value As String)
            viewstate("Flag") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CStr(viewstate("BankAccountID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property
    Private Property Notes() As String
        Get
            Return CStr(viewstate("Notes"))
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.BatchVoucher
    Private oController As New BatchVoucherController
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "BATCHVOUCHER"
            Me.BranchID = Me.sesBranchId.Replace("'", "")

            oSearchBy.ListData = "BatchNo,Batch No.-BatchDescription,Description"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If Request.QueryString("BatchNo") <> "" Then
                    Me.SearchBy = " BranchID = '" & Request.QueryString("BranchID") & "' "
                    Me.SearchBy &= " And BatchNo = '" & Request.QueryString("BatchNo") & "' "
                    Me.BranchID = Request.QueryString("BranchID")
                    Me.SortBy = ""

                    DoBind(Me.SearchBy, Me.SortBy)
                    pnlList.Visible = True
                    pnlDtGrid.Visible = True
                Else
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If

            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = False

        pnlBGNo.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.BatchVoucherPaging(oCustomClass)

        DtUserList = oCustomClass.BatchList
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgBatchVoucher.DataSource = DvUserList

        Try
            dtgBatchVoucher.DataBind()
        Catch
            dtgBatchVoucher.CurrentPageIndex = 0
            dtgBatchVoucher.DataBind()
        End Try
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlBGNo.Visible = False
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If


        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If IsNumeric(txtpage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtpage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub dtgBatchVoucher_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgBatchVoucher.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                    Me.Mode = "EDIT"

                    lblTitle.Text = "EDIT - VOUCHER BATCH"

                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = True
                    oBatchVoucher.resetBankAccount()
                    Dim lnkbatchNo As HyperLink
                    lnkbatchNo = CType(dtgBatchVoucher.Items(e.Item.ItemIndex).FindControl("hypBatchNo"), HyperLink)
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .BatchNo = lnkbatchNo.Text.Trim
                        .BranchId = Me.BranchID
                    End With
                    oCustomClass = oController.BatchVoucherView(oCustomClass)
                    With oCustomClass
                        txtBatchNo.Visible = False
                        cboReceiveDisburseFlag.SelectedIndex = cboReceiveDisburseFlag.Items.IndexOf(cboReceiveDisburseFlag.Items.FindByValue(.ReceiveDisburseFlag))
                        cboProcessID.SelectedIndex = cboProcessID.Items.IndexOf(cboProcessID.Items.FindByValue(.ProcessID))
                        oBatchVoucher.ViewBankAccountID = .BankAccountName
                        oBatchVoucher.ViewWayOfPayment = .WayOfPaymentName
                        oBatchVoucher.Notes = .Notes
                        txtDescription.Text = .BatchDescription
                        lblEditBatchNo.Text = .BatchNo

                        Me.BatchDescription = .BatchDescription
                        Me.WayOfPayment = .WayOfPayment
                        Me.BankAccountID = .BankAccountID
                        Me.Notes = .Notes
                        Me.Flag = .ReceiveDisburseFlag
                        Me.ProcessID = .ProcessID
                    End With
                    oBatchVoucher.resetBankAccount()
                    pnlBGNo.Visible = False
                End If
            Case "DELETE"

                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim strError As String
                    Dim lnkbatchNo As HyperLink
                    lnkbatchNo = CType(dtgBatchVoucher.Items(e.Item.ItemIndex).FindControl("hypBatchNo"), HyperLink)
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .BatchNo = lnkbatchNo.Text.Trim
                        .BranchId = Me.BranchID
                    End With

                    Try
                        oController.BatchVoucherDelete(oCustomClass)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        InitialDefaultPanel()
                        DoBind(Me.SearchBy, Me.SortBy)
                        oBatchVoucher.resetBankAccount()
                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
            Case "POSTED"

                If CheckFeature(Me.Loginid, Me.FormID, "POST", Me.AppId) Then
                    Dim strError As String
                    Dim lnkbatchNo As HyperLink
                    lnkbatchNo = CType(dtgBatchVoucher.Items(e.Item.ItemIndex).FindControl("hypBatchNo"), HyperLink)
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .BatchNo = lnkbatchNo.Text.Trim
                        .BranchId = Me.BranchID
                        .LoginId = Me.Loginid
                        .BusinessDate = Me.BusinessDate
                    End With

                    Try
                        oController.BatchVoucherPosted(oCustomClass)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        InitialDefaultPanel()
                        DoBind(Me.SearchBy, Me.SortBy)
                        oBatchVoucher.resetBankAccount()
                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)

        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgBatchVoucher_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgBatchVoucher.ItemDataBound
        Dim imbDelete As ImageButton
        Dim hypBatchNo As HyperLink
        Dim imbEditDetail As HyperLink
        Dim imbPosted As LinkButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm();")
            imbPosted = CType(e.Item.FindControl("imbPosted"), LinkButton)
            imbPosted.Attributes.Add("onclick", "return fConfirmPosted();")
            hypBatchNo = CType(e.Item.FindControl("hypBatchNo"), HyperLink)
            hypBatchNo.NavigateUrl = "javascript:OpenBatchNo('" & "AccMnt" & "', '" & Server.UrlEncode(hypBatchNo.Text.Trim) & "', '" & Server.UrlEncode(Me.sesBranchId.Replace("'", "")) & "')"
            imbEditDetail = CType(e.Item.FindControl("imbEditDetail"), HyperLink)
            imbEditDetail.NavigateUrl = "BatchTransactionReceive.aspx?BatchNo=" & hypBatchNo.Text & "&BranchID=" & Me.BranchID
        End If
    End Sub
#End Region

#Region "Saving Process"
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAdd.Visible = True
            pnlDtGrid.Visible = False
            pnlList.Visible = False

            Me.Mode = "ADD"
            oBatchVoucher.resetBankAccount()
            oBatchVoucher.ViewBankAccountID = ""
            oBatchVoucher.ViewWayOfPayment = ""
            oBatchVoucher.Notes = ""
            txtDescription.Text = ""
            lblTitle.Text = "TAMBAH - VOUCHER BATCH"
            cboReceiveDisburseFlag.SelectedIndex = 0
            cboProcessID.SelectedIndex = 0
            txtBatchNo.Visible = True
            lblEditBatchNo.Text = ""
        End If
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        If cboReceiveDisburseFlag.SelectedValue = "R" Then
            pnlBGNo.Visible = False
            With oCustomClass
                .strConnection = GetConnectionString()
                If Me.Mode = "ADD" Then
                    .BatchNo = txtBatchNo.Text
                Else
                    .BatchNo = lblEditBatchNo.Text
                End If
                .BatchDescription = txtDescription.Text
                .WayOfPayment = oBatchVoucher.WayOfPayment
                .BankAccountID = oBatchVoucher.BankAccount
                .Notes = oBatchVoucher.Notes
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.BranchID
                .ReceiveDisburseFlag = cboReceiveDisburseFlag.SelectedValue.Trim
                .ProcessID = cboProcessID.SelectedValue
            End With
            Try
                If Me.Mode = "ADD" Then
                    oController.BatchVoucherAdd(oCustomClass)
                ElseIf Me.Mode = "EDIT" Then
                    oController.BatchVoucherEdit(oCustomClass)
                End If
                InitialDefaultPanel()
                oBatchVoucher.resetBankAccount()
                DoBind(Me.SearchBy, Me.SortBy)
            Catch ex As Exception

                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Else
            pnlBGNo.Visible = True
            If Me.Mode = "ADD" Then
                Me.BatchNo = txtBatchNo.Text
            Else
                Me.BatchNo = lblEditBatchNo.Text
            End If
            Me.BatchDescription = txtDescription.Text
            Me.WayOfPayment = oBatchVoucher.WayOfPayment
            Me.BankAccountID = oBatchVoucher.BankAccount
            Me.Notes = oBatchVoucher.Notes
            Me.Flag = cboReceiveDisburseFlag.SelectedValue
            Me.ProcessID = cboProcessID.SelectedValue
            BindBGNo()
            lblViewBatchNo.Text = Me.BatchNo
            lblViewDescription.Text = Me.BatchDescription
            lblViewWayOfPayment.Text = oBatchVoucher.WayOfPaymentName
            lblViewBankAccountID.Text = oBatchVoucher.BankAccountName
            lblviewNotes.Text = oBatchVoucher.Notes
            If Me.Flag = "R" Then
                lblViewReceiveDisburseFlag.Text = "Receive"
            Else
                lblViewReceiveDisburseFlag.Text = "Disburse"
            End If
            Select Case Me.ProcessID
                Case "PAYMENTRCV"
                    lblViewProcessID.Text = "Payment Receive"
            End Select
        End If
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click


        Me.SearchBy = " BranchID = '" & Me.BranchID & "' "
        Me.SearchBy &= " And CashierID = '" & oCashier.CashierName & "' "
        If txtBatchDate.Text <> "" Then
            Me.SearchBy &= " And BatchDate = '" & ConvertDate2(txtBatchDate.Text).ToString("yyyyMMdd") & "' "
        End If
        If oSearchBy.Text.Trim <> "" Then
            If Right(oSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID & " Like '" & oSearchBy.Text.Trim & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim & "' "
            End If
        End If
        If cboStatus.SelectedValue <> "-" Then
            Me.SearchBy &= " And Status ='" & cboStatus.SelectedValue & "' "
        End If

        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Print"
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then

            Dim cookie As HttpCookie = Request.Cookies(COOKIES_BANKACCOUNT_REPORT)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
            Else
                Dim cookieNew As New HttpCookie(COOKIES_BANKACCOUNT_REPORT)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/BatchVoucherListReport.aspx")
        End If
    End Sub
#End Region

#Region "Cancel Process"
    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelAdd.Click

        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlAdd.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub imbSaveBGNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveBGNo.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .BatchNo = Me.BatchNo
            .BatchDescription = Me.BatchDescription
            .WayOfPayment = Me.WayOfPayment
            .BankAccountID = Me.BankAccountID
            .Notes = Me.Notes
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.BranchID
            .ReceiveDisburseFlag = Me.Flag
            .ProcessID = Me.ProcessID
            .BGNo = cboBGNo.SelectedValue
            .BGDueDate = ConvertDate2(txtBGDueDate.Text).ToString("yyyyMMdd")
        End With
        Try
            If Me.Mode = "ADD" Then
                oController.BatchVoucherAdd(oCustomClass)
            ElseIf Me.Mode = "EDIT" Then
                oController.BatchVoucherEdit(oCustomClass)
            End If
            InitialDefaultPanel()
            oBatchVoucher.resetBankAccount()
            DoBind(Me.SearchBy, Me.SortBy)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub BindBGNo()
        Dim strWhere As String
        cboBGNo.Items.Clear()
        Dim DtBGNumber As New DataTable
        Dim dvBGNumber As New DataView
        strWhere = " bankaccountid = '" & Me.BankAccountID & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "'"
        DtBGNumber = m_controller.GetBGNumber(GetConnectionString, strWhere)
        dvBGNumber = DtBGNumber.DefaultView

        With cboBGNo
            .DataValueField = "Name"
            .DataTextField = "ID"
            .DataSource = dvBGNumber
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .Items.Insert(1, "None")
            .Items(1).Value = "None"
        End With
    End Sub
End Class