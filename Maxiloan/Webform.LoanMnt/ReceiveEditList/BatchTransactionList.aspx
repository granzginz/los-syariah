﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchTransactionList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.BatchTransactionList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBatchVoucher" Src="../../Webform.UserController/ucBatchVoucher.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchTransactionList</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function fConfirmPosted() {
            if (window.confirm("Apakah yakin anda akan melakukan proses ini? "))
                return true;
            else
                return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        VOUCHER BATCH
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Tanggal Batch &lt;=</label>
                        <asp:TextBox ID="txtBatchDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtBatchDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtBatchDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBatchDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Kasir</label>
                        <uc1:uccashier id="oCashier" runat="server"></uc1:uccashier>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="cboStatus" runat="server">
                            <asp:ListItem Value="-">ALL</asp:ListItem>
                            <asp:ListItem Value="N">BARU</asp:ListItem>
                            <asp:ListItem Value="C">BATAL</asp:ListItem>
                            <asp:ListItem Value="P">POSTED</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray" />
                </div>
                <asp:Panel ID="pnlDtGrid" runat="server">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                DAFTAR BATCH
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgBatchVoucher" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    CssClass="grid_general" BorderStyle="None" BorderWidth="0" DataKeyField="BatchNo"
                                    OnSortCommand="SortGrid" Width="100%">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="ENTRY">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="imbEditDetail" runat="server" causesvalidation="False" commandname="EDITDETAIL"
                                                    ImageUrl="../../Images/IconEdit.gif" Visible='<%#iif(Container.DataItem("Status")="New", "True", "False")%>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="EDIT">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" CommandName="EDIT"
                                                    ImageUrl="../../Images/IconEdit.gif" Visible='<%#iif(Container.DataItem("Status")="New", "True", "False")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DELETE">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" CommandName="DELETE"
                                                    ImageUrl="../../Images/IconDelete.gif" Visible='<%#iif(Container.DataItem("Status")="New", "True", "False")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ACTION">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="imbPosted" runat="server" CausesValidation="False" CommandName="POSTED"
                                                    Text="Post" Visible='<%#iif(Container.DataItem("Status")="New", "True", "False")%>'>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="NO BATCH" SortExpression="BatchNo">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypBatchNo" runat="server" Text='<%#Container.DataItem("batchNo")%>'>

                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="TGL BATCH" SortExpression="BatchDate">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblBatchDate" runat="server" Text='<%#Container.DataItem("BatchDate")%>'>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="NAMA BATCH" SortExpression="BatchDescription">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblBatchDescription" runat="server" Text='<%#Container.DataItem("BatchDescription")%>'>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PEMILIK" SortExpression="CashierID">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblOwner" runat="server" Text='<%#Container.DataItem("CashierID")%>'>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="FLAG" SortExpression="ReceiveDisburseFlag">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblReceiveDisburseFlag" runat="server" Text='<%#container.dataitem("ReceiveDisburseFlag")%>'>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ID PROSES" SortExpression="ProcessID">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblProcessID" runat="server" Text='<%#container.dataitem("ProcessID")%>'>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="STATUS" SortExpression="Status">
                                            <HeaderStyle Font-Underline="True" Height="30px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#container.dataitem("Status")%>'>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="ButtonAddNew" runat="server" CausesValidation="False" Text="Add"
                            CssClass="small button blue" />
                        <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue" />
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Batch</label>
                        <asp:TextBox ID="txtBatchNo" runat="server" ></asp:TextBox>
                        <asp:Label ID="lblEditBatchNo" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="reqBatchNo" runat="server" ControlToValidate="txtBatchNo"
                            Display="Dynamic" ErrorMessage="Harap isi No Batch" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Batch</label>
                        <asp:TextBox ID="txtDescription" runat="server"  MaxLength="50"></asp:TextBox>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="reqBatchDescription" runat="server" ControlToValidate="txtDescription"
                            Display="Dynamic" ErrorMessage="harap isi Nama Batch" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Flag</label>
                        <asp:DropDownList ID="cboReceiveDisburseFlag" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="R">Terima</asp:ListItem>
                            <asp:ListItem Value="D">Bayar</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cboReceiveDisburseFlag"
                            Display="Dynamic" ErrorMessage="Harap pilih Flag" CssClass="validator_general"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            ID Proses</label>
                        <asp:DropDownList ID="cboProcessID" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="PAYMENTRCV">Terima Pembayaran</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cboProcessID"
                            Display="Dynamic" ErrorMessage="Harap pilih ID Proses" CssClass="validator_general"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%-- <div class="form_box_uc">--%>
                <uc1:ucbatchvoucher id="oBatchVoucher" runat="server" />
                <%--</div>--%>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveAdd" runat="server" CausesValidation="True" Text="Add"
                        CssClass="small button blue" />
                    <asp:Button ID="ButtonCancelAdd" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBGNo" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitle1" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                No Batch</label>
                            <asp:Label ID="lblViewBatchNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama Batch</label>
                            <asp:Label ID="lblViewDescription" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Flag</label>
                            <asp:Label ID="lblViewReceiveDisburseFlag" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                ID Proses</label>
                            <asp:Label ID="lblViewProcessID" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Cara Pembayaran</label>
                            <asp:Label ID="lblViewWayOfPayment" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                ID Rekening Bank</label>
                            <asp:Label ID="lblViewBankAccountID" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Catatan</label>
                        <asp:Label ID="lblviewNotes" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Bilyet Giro</label>
                        <asp:DropDownList ID="cboBGNo" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="cboBGNo"
                            Display="Dynamic" ErrorMessage="Harap isi No Bilyet Giro" CssClass="validator_general"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tgl Jatuh Tempo BG</label>
                        <asp:TextBox ID="txtBGDueDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtBGDueDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtBGDueDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBGDueDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveBGNo" runat="server" CausesValidation="True" Text="Save"
                        CssClass="small button blue" />
                    <asp:Button ID="ButtonCancelBGNo" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
