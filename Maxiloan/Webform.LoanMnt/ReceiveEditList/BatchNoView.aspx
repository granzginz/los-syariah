﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchNoView.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.BatchNoView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchNoView</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - VOUCHER BATCH PEMBAYARAN CUSTOMER
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Batch</label>
                <asp:Label ID="lblViewBatchNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Batch</label>
                <asp:Label ID="lblViewBatchDescription" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Pemilik Batch</label>
            <asp:Label ID="lblViewCashierID" runat="server"></asp:Label>
            (<asp:Label ID="lblViewOpeningSequenceNo" runat="server"></asp:Label>)
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Flag</label>
                <asp:Label ID="lblViewReceiveDisburseFlag" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    ID Proses</label>
                <asp:Label ID="lblViewProcessID" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Cara Pembayaran</label>
                <asp:Label ID="lblViewWOP" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    ID Rekening Bank</label>
                <asp:Label ID="lblViewBankAccountID" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Bilyet Giro</label>
                <asp:Label ID="lblViewBGNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Jatuh Tempo BG</label>
                <asp:Label ID="lblViewBGDueDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Status</label>
                <asp:Label ID="lblViewStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status</label>
                <asp:Label ID="lblViewStatusDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    ID Kasir</label>
                <asp:Label ID="lblViewPostedCashierID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Posting</label>
                <asp:Label ID="lblViewPostedDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan</label>
            <asp:Label ID="lblviewNotes" runat="server"></asp:Label>
        </div>
    </div>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI BATCH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgBatchVoucher" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="BatchNo" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" OnSortCommand="SortGrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%#Container.DataItem("CustomerID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO BUKTI KAS">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSequenceNo" Visible="False" runat="server" Text='<%#Container.DataItem("SequenceNo")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblReferenceNo" runat="server" Text='<%#container.dataitem("ReferenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ValueDate" HeaderText="TGL VALUTA">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblValueDate" runat="server" Text='<%#container.dataitem("ValueDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AmountCBBatchVoucher" HeaderText="JUMLAH DITERIMA">
                                <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAmountBatchVoucher" runat="server" Text='<%#formatnumber(container.dataitem("AmountCBBatchVoucher"),2)%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="ButtonExit" runat="server" Text="Exit" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
