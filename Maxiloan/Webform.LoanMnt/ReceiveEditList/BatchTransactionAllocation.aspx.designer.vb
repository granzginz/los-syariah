﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BatchTransactionAllocation

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlList As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''hypBatchNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypBatchNo As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblBatchDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBatchDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtReceiveFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReceiveFrom As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Requiredfieldvalidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtReferenceNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReferenceNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Requiredfieldvalidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator


    '''<summary>
    '''txtValueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValueDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtValueDate_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValueDate_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''RequiredFieldValidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtBatchVoucherDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBatchVoucherDescription As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtBatchVoucherNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBatchVoucherNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboDepartmentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboDepartmentID As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''reqDepartmentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents reqDepartmentID As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''ButtonAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonAdd As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ButtonCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlListTransaction control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlListTransaction As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dtgBatchVoucher control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgBatchVoucher As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''ButtonSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ButtonimbCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonimbCancel As Global.System.Web.UI.WebControls.Button
End Class
