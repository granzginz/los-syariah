﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CashBankBatchVoucherView
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property BatchNo() As String
        Get
            Return Request.QueryString("batchno")
        End Get
        Set(ByVal Value As String)
            Request.QueryString("batchno") = Value
        End Set
    End Property

    Private Property BatchSequenceNo() As Integer
        Get
            Return CInt(Request.QueryString("BatchSequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            Request.QueryString("BatchSequenceNo") = CStr(Value)
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return CStr(viewstate("BankAccountName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountName") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CStr(viewstate("BankAccountID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return Request.QueryString("Mode")
        End Get
        Set(ByVal Value As String)
            Request.QueryString("Mode") = Value
        End Set
    End Property

    Private Property TableBatch() As DataTable
        Get
            Return (CType(Viewstate("TableBatch"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TableBatch") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private TotalAmountReceive As Double
    Private oCustomClass As New Parameter.BatchVoucher
    Private oController As New BatchVoucherController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then            
            Me.BranchID = Request.QueryString("branchid")
            With oCustomClass
                .strConnection = GetConnectionString
                .BatchNo = Me.BatchNo
                .BranchId = Me.BranchID
            End With
            oCustomClass = oController.BatchVoucherView(oCustomClass)
            With oCustomClass
                lblBatchDate.Text = .BatchDate.ToString("dd/MM/yyyy")
            End With

            With oCustomClass
                .strConnection = GetConnectionString
                .BatchNo = Me.BatchNo
                .BranchId = Me.BranchID
                .BatchSequenceNo = Me.BatchSequenceNo
            End With
            oCustomClass = oController.CashBankBatchVoucherView(oCustomClass)
            With oCustomClass
                hypBatchNo.Text = Me.BatchNo
                lblBatchAmount.Text = CStr(.AmountReceive)
                lblValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
                lblBatchVoucherDescription.Text = .BatchDescription
                lblBatchVoucherNotes.Text = .Notes
                hypAgreement.Text = .Agreementno
                lblReceiveFrom.Text = .ReceivedFrom
                lblReferenceNo.Text = .ReferenceNo
                Me.TableBatch = .TableBatch
                dtgBatchVoucher.DataSource = Me.TableBatch.DefaultView
                dtgBatchVoucher.DataBind()
            End With
            pnlListTransaction.Visible = True
        End If
    End Sub
#Region "Data Grid Item Data Bound"
    Private Sub dtgBatchVoucher_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgBatchVoucher.ItemDataBound
        Dim lblTemp As Label
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblBatchAmountAllocation As Label
        Dim lblTotalBatchAmount As Label
        If e.Item.ItemIndex >= 0 Then
            lblBatchAmountAllocation = CType(e.Item.FindControl("lblBatchAmountAllocation"), Label)
            TotalAmountReceive += CDbl(IIf(lblBatchAmountAllocation.Text.Trim = "", 0, lblBatchAmountAllocation.Text.Trim.Replace(",", "")))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotalBatchAmount = CType(e.Item.FindControl("lblTotalBatchAmount"), Label)
            lblTotalBatchAmount.Text = FormatNumber(TotalAmountReceive, 2)
        End If
    End Sub
#End Region

End Class