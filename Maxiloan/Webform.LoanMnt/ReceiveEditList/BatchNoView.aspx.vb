﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BatchNoView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.BatchVoucher
    Private oController As New BatchVoucherController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            ButtonExit.Attributes.Add("OnClick", "javascript:window.close();")
            With oCustomClass
                .strConnection = GetConnectionString
                .BatchNo = Request.QueryString("BatchNo")
                .BranchId = Request.QueryString("BranchID")
            End With

            Me.SearchBy = " BranchID = '" & Request.QueryString("BranchID") & "' "
            Me.SearchBy &= " And BatchNo = '" & Request.QueryString("BatchNo") & "' "
            Me.SortBy = ""
            Try
                oCustomClass = oController.BatchVoucherView(oCustomClass)
                With oCustomClass
                    lblViewBatchNo.Text = .BatchNo
                    lblViewBatchDescription.Text = .BatchDescription
                    lblViewWOP.Text = .WayOfPaymentName
                    lblViewBankAccountID.Text = .BankAccountName
                    lblViewStatus.Text = .Status
                    lblViewStatusDate.Text = .StatusDate
                    lblViewCashierID.Text = .LoginId
                    lblViewOpeningSequenceNo.Text = CStr(.OpeningSequenceNo)
                    lblViewPostedCashierID.Text = .PostedCashierID
                    lblViewPostedDate.Text = .PostedDate
                    lblviewNotes.Text = .Notes
                    lblViewBGNo.Text = .BGNo
                    lblViewBGDueDate.Text = .BGDueDate
                    If .ReceiveDisburseFlag = "R" Then
                        lblViewReceiveDisburseFlag.Text = "Receive"
                    Else
                        lblViewReceiveDisburseFlag.Text = "Disburse"
                    End If
                    Select Case .ProcessID
                        Case "PAYMENTRCV"
                            lblViewProcessID.Text = "Payment Receive"
                    End Select
                End With
                DoBind(Me.SearchBy, Me.SortBy)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Text = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.CashBankBatchVoucherPaging(oCustomClass)

        DtUserList = oCustomClass.BatchList
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgBatchVoucher.DataSource = DvUserList

        Try
            dtgBatchVoucher.DataBind()
        Catch
            dtgBatchVoucher.CurrentPageIndex = 0
            dtgBatchVoucher.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlDtGrid.Visible = True

        PagingFooter()
    End Sub
#Region "DataGrid Command"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgBatchVoucher_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgBatchVoucher.ItemDataBound
        Dim hypApplicationID As HyperLink
        Dim lblCustomerID As Label
        Dim hypCustomerName As HyperLink
        Dim hypAgreementNo As HyperLink
        Dim lblSequenceNo As Label
        Dim lblAmountBatchVoucher As HyperLink

        If e.Item.ItemIndex >= 0 Then
            hypApplicationID = CType(e.Item.FindControl("hypApplicationID"), HyperLink)

            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            hypCustomerName = CType(e.Item.FindControl("hypCustomerName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblAmountBatchVoucher = CType(e.Item.FindControl("lblAmountBatchVoucher"), HyperLink)
            lblSequenceNo = CType(e.Item.FindControl("lblSequenceNo"), Label)

            lblAmountBatchVoucher.NavigateUrl = "javascript:OpenBatchVoucher('" & "AccMnt" & "', '" & Server.UrlEncode(lblSequenceNo.Text.Trim) & "', '" & Server.UrlEncode(Request.QueryString("BatchNo")) & _
                "', '" & Request.QueryString("BranchID") & "')"

            If hypAgreementNo.Text.Trim.Length > 0 Then
                hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hypApplicationID.Text.Trim) & "')"
            End If

            If hypApplicationID.Text.Trim.Length > 0 Then
                hypApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hypApplicationID.Text.Trim) & "')"
            End If
            If lblCustomerID.Text.Trim.Length > 0 Then
                hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            End If
        End If
    End Sub
#End Region

End Class