﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BatchTransactionReceive
    Inherits Maxiloan.Webform.AccMntWebBased    
#Region "Property"

    Private Property BatchNo() As String
        Get
            Return Request.QueryString("BatchNo")
        End Get
        Set(ByVal Value As String)
            Request.QueryString("BatchNo") = Value
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return CStr(viewstate("BankAccountName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountName") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CStr(viewstate("BankAccountID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private totalamount As Double
    Private oCustomClass As New Parameter.BatchVoucher
    Private oController As New BatchVoucherController
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "BATCHVOUCHER"
            Me.BranchID = Request.QueryString("BranchID")
            oSearchBy.ListData = "ApplicationID,Application ID-AgreementNo,Agreement No-CustomerName,Customer Name-ReceivedFrom,Received From-ReferenceNo,Reference No"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Me.SearchBy = " BranchID = '" & Me.BranchID & "' "
                Me.SearchBy &= " And BatchNo = '" & Me.BatchNo & "' "
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If            
            BindHeader(Me.BatchNo)
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlDtGrid.Visible = False

    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.CashBankBatchVoucherPaging(oCustomClass)

        DtUserList = oCustomClass.BatchList
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgBatchVoucher.DataSource = DvUserList

        Try
            dtgBatchVoucher.DataBind()
        Catch
            dtgBatchVoucher.CurrentPageIndex = 0
            dtgBatchVoucher.DataBind()
        End Try
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        pnlDtGrid.Visible = True

        PagingFooter()
    End Sub

    Private Sub BindHeader(ByVal cBatchNo As String)
        With oCustomClass
            .strConnection = GetConnectionString
            .BatchNo = cBatchNo
            .BranchId = Me.BranchID
        End With
        oCustomClass = oController.BatchVoucherView(oCustomClass)
        With oCustomClass
            Me.WayOfPayment = .WayOfPayment
            Me.BankAccountID = .BankAccountID
            hypViewBatchNo.Text = Me.BatchNo
            hypViewBatchNo.NavigateUrl = "javascript:OpenBatchNo('" & "AccMnt" & "', '" & Server.UrlEncode(hypViewBatchNo.Text.Trim) & "', '" & Server.UrlEncode(Me.BranchID) & "')"
            lblViewBatchDate.Text = .BatchDate.ToString("dd/MM/yyyy")
            lblViewBatchDescription.Text = .BatchDescription
            lblViewStatus.Text = .Status
            lblViewStatusDate.Text = .StatusDate
            lblCashierID.Text = .loginid
            lblOpeningSequenceNo.Text = CStr(.OpeningSequenceNo)
            lblNotes.Text = .Notes
            lblWayOfPayment.Text = CStr(.WayOfPaymentName)
            lblBankAccount.Text = CStr(.BankAccountName)
            Select Case .ReceiveDisburseFlag
                Case "R"
                    lblViewTransactionType.Text = "Receive"
                Case "D"
                    lblViewTransactionType.Text = "Disburse"
            End Select

            Select Case .ProcessID
                Case "PAYMENTRCV"
                    lblViewProcessID.Text = "Payment Receive"
            End Select

        End With
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If


        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub dtgBatchVoucher_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgBatchVoucher.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                Dim lblSequenceNo As Label
                lblSequenceNo = CType(dtgBatchVoucher.Items(e.Item.ItemIndex).FindControl("lblSequenceNo"), Label)
                Response.Redirect("BatchTransactionAllocation.aspx?batchno=" & Me.BatchNo & "&branchid=" & Me.BranchID & "&Mode=EDIT" & "&BatchSequenceNo=" & lblSequenceNo.Text)
            Case "DELETE"

                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim strError As String
                    Dim hypSequenceNo As Label
                    hypSequenceNo = CType(dtgBatchVoucher.Items(e.Item.ItemIndex).FindControl("lblSequenceNo"), Label)
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .BatchNo = Me.BatchNo
                        .BranchId = Me.BranchID
                        .BatchSequenceNo = CInt(hypSequenceNo.Text)
                    End With
                    Try
                        oController.CashBankBatchVoucherDelete(oCustomClass)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        InitialDefaultPanel()
                        DoBind(Me.SearchBy, Me.SortBy)
                    Catch exp As Exception

                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)

        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgBatchVoucher_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgBatchVoucher.ItemDataBound
        Dim imbDelete As ImageButton
        Dim hypApplicationID As HyperLink
        Dim lblCustomerID As Label
        Dim hypCustomerName As HyperLink
        Dim hypAgreementNo As HyperLink
        Dim lblSequenceNo As Label
        Dim lblAmountBatchVoucher As HyperLink
        Dim lblTot As Label

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            hypApplicationID = CType(e.Item.FindControl("hypApplicationID"), HyperLink)
            lblSequenceNo = CType(e.Item.FindControl("lblSequenceNo"), Label)
            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            hypCustomerName = CType(e.Item.FindControl("hypCustomerName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblAmountBatchVoucher = CType(e.Item.FindControl("lblAmountBatchVoucher"), HyperLink)

            imbDelete.Attributes.Add("onclick", "return fConfirmPosted();")

            lblAmountBatchVoucher.NavigateUrl = "javascript:OpenBatchVoucher('" & "AccMnt" & "', '" & Server.UrlEncode(lblSequenceNo.Text.Trim) & "', '" & _
                                                Server.UrlEncode(Request.QueryString("BatchNo")) & _
                                                "', '" & Request.QueryString("BranchID") & "')"

            If hypAgreementNo.Text.Trim.Length > 0 Then
                hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hypApplicationID.Text.Trim) & "')"
            End If

            If hypApplicationID.Text.Trim.Length > 0 Then
                hypApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hypApplicationID.Text.Trim) & "')"
            End If
            If lblCustomerID.Text.Trim.Length > 0 Then
                hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            End If
            lblAmountBatchVoucher = CType(e.Item.FindControl("lblAmountBatchVoucher"), HyperLink)
            totalamount += CDbl(IIf(lblAmountBatchVoucher.Text.Trim = "", 0, lblAmountBatchVoucher.Text.Trim))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTot = CType(e.Item.FindControl("lblTot"), Label)
            lblTot.Text = FormatNumber(totalamount, 2)
        End If
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click


        Me.SearchBy = " BranchID = '" & Me.BranchID & "' "
        Me.SearchBy &= " And BatchNo = '" & Me.BatchNo & "' "

        If txtValueDate.Text <> "" Then
            Me.SearchBy &= " And ValueDate = '" & ConvertDate2(txtValueDate.Text).ToString("yyyyMMdd") & "' "
        End If
        If oSearchBy.Text.Trim <> "" Then
            If Right(oSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID & " Like '" & oSearchBy.Text.Trim & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim & "' "
            End If
        End If
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("BatchTransactionList.aspx?batchno=" & Me.BatchNo & "&branchid=" & Me.BranchID)
    End Sub

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        Response.Redirect("BatchTransactionAllocation.aspx?batchno=" & Me.BatchNo & "&branchid=" & Me.BranchID & "&Mode=ADD")
    End Sub

    Private Sub imgUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUpload.Click
        Response.Redirect("BatchTransactionUpload.aspx?batchno=" & Me.BatchNo & "&branchid=" & Me.BranchID & "&Mode=ADD")
    End Sub

End Class