﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CashBankBatchVoucherView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.CashBankBatchVoucherView" %>

<%@ Register TagPrefix="uc1" TagName="UCLookupAgreement" Src="../../Webform.UserController/UCLookupAgreement.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CashBankBatchVoucherView</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function fClose() {
            window.close();
            return false;
        }		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="pnlList" runat="server" EnableViewState="False">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    ALOKASI TRANSAKSI BATCH
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Batch</label>
                    <asp:HyperLink ID="hypBatchNo" runat="server" EnableViewState="False"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Batch</label>
                    <asp:Label ID="lblBatchDate" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Terima Dari</label>
                <asp:Label ID="lblReceiveFrom" runat="server" EnableViewState="False" Width="180px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Bukti</label>
                    <asp:Label ID="lblReferenceNo" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Batch</label>
                    <asp:Label ID="lblBatchAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Valuta</label>
                <asp:Label ID="lblValueDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Batch</label>
                <asp:Label ID="lblBatchVoucherDescription" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:Label ID="lblBatchVoucherNotes" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:HyperLink ID="hypAgreement" runat="server" EnableViewState="False"></asp:HyperLink>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlListTransaction" runat="server" EnableViewState="False" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgBatchVoucher" runat="server" EnableViewState="False" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentAllocationDescription" runat="server" EnableViewState="False"
                                        Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionDescription" runat="server" EnableViewState="False"
                                         Text='<%#Container.DataItem("TransactionDescription")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" EnableViewState="False" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DEPARTEMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" EnableViewState="False" Text='<%#Container.DataItem("DepartmentName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBatchAmountAllocation" runat="server" EnableViewState="False" Text='<%#formatNumber(Container.DataItem("AmountTransaction"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalBatchAmount" runat="server" EnableViewState="False"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false"
                Text="Close" CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
