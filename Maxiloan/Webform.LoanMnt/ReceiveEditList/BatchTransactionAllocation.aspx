﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchTransactionAllocation.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.BatchTransactionAllocation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCLookupAgreement" Src="../../Webform.UserController/UCLookupAgreement.ascx" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchTransactionAllocation</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ALOKASI BATCH TRANSAKSI
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label>
                        No Batch</label>
                    <asp:HyperLink ID="hypBatchNo" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Batch</label>
                    <asp:Label ID="lblBatchDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Terima Dari</label>
                <asp:TextBox ID="txtReceiveFrom" runat="server"  MaxLength="50"
                    Width="180px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Harap isi Terima Dari"
                    Display="Dynamic" ControlToValidate="txtReceiveFrom" Visible="true" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="form_left">
                    <label class="label_req">
                        No Bukti Kas</label>
                    <asp:TextBox ID="txtReferenceNo" runat="server"  MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi Bukti Kas"
                        ControlToValidate="txtReferenceNo" Visible="true" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label class="label_req">
                        Jumlah Transaksi</label>                    
                    <uc1:ucNumberFormat ID="txtBatchAmount" runat="server" />                    
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Tanggal Valuta</label>                
                <asp:TextBox ID="txtValueDate" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtValueDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                    ControlToValidate="txtValueDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                <asp:TextBox ID="txtBatchVoucherDescription" runat="server"  MaxLength="50"
                    Width="50%"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtBatchVoucherNotes" runat="server"  MaxLength="50"
                    Width="50%"></asp:TextBox>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Kontrak</label>
            <uc1:uclookupagreement id="oLookupAgreement" runat="server">
                </uc1:uclookupagreement>
        </div>
    </div>
     
        <div class="form_box_uc">
            <uc1:uclookuptransaction id="oTrans" runat="server">
                </uc1:uclookuptransaction>
        </div>
     
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Departemen</label>
            <asp:DropDownList runat="server" ID="cboDepartmentID">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="reqDepartmentID" runat="server" Visible="true" InitialValue="0"
                Display="Dynamic" ControlToValidate="cboDepartmentID" ErrorMessage="Harap Pilih Departemen"
                CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    <asp:Panel ID="pnlListTransaction" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgBatchVoucher" runat="server" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentAllocationID" Visible="False" runat="server" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblPaymentAllocationDescription" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTransactionDescription" runat="server"  Text='<%#Container.DataItem("TransactionDescription")%>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Left" Height="25px"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DEPARTEMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentID" runat="server" Visible="False" Text='<%#Container.DataItem("DepartmentID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>                                    
                                    <uc1:ucNumberFormat ID="txtBatchAmountAllocation" runat="server" />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalBatchAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AmountTransaction" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonimbCancel" runat="server" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
