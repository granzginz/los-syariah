﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchTransactionReceive.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.BatchTransactionReceive" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchTransactionReceive</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function fConfirmPosted() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        BATCH TRANSAKSI
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Batch</label>
                        <asp:HyperLink ID="hypViewBatchNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Batch</label>
                        <asp:Label ID="lblViewBatchDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Batch</label>
                        <asp:Label ID="lblViewBatchDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jenis Transaksi</label>
                        <asp:Label ID="lblViewTransactionType" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            ID Proses</label>
                        <asp:Label ID="lblViewProcessID" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Status</label>
                        <asp:Label ID="lblViewStatus" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Status</label>
                        <asp:Label ID="lblViewStatusDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cara Pembayaran</label>
                        <asp:Label ID="lblWayOfPayment" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Rekening Bank</label>
                        <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Pemilik Batch</label>
                        <asp:Label ID="lblCashierID" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Urut</label>
                        <asp:Label ID="lblOpeningSequenceNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Catatan</label>
                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Batch =</label>
                        <asp:TextBox ID="txtValueDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtValueDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtValueDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonBack" runat="server" Text="Back List" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSAKSI BATCH
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgBatchVoucher" runat="server" Width="100%" OnSortCommand="SortGrid"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                                        <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblCustomerID" runat="server" Text='<%#Container.DataItem("CustomerID")%>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO BUKTI">
                                        <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSequenceNo" runat="server" Visible="false" Text='<%#Container.DataItem("SequenceNo")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblReferenceNo" runat="server" Text='<%#container.dataitem("ReferenceNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ValueDate" HeaderText="TGL VALUTA">
                                        <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblValueDate" runat="server" Text='<%#container.dataitem("ValueDate")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            Total
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AmountCBBatchVoucher" HeaderText="JUMLAH DITERIMA">
                                        <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="right" Width="10%"></ItemStyle>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblAmountBatchVoucher" runat="server" Text='<%#formatnumber(container.dataitem("AmountCBBatchVoucher"),2)%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTot" runat="server" Font-Bold="True"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEditAllocate" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                                CommandName="EDIT" Visible='<%#iif(Container.DataItem("VoucherNo")="-" and Container.DataItem("Tr_Nomor")="-" , "True", "False")%>'>
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                                Visible='<%#iif(Container.DataItem("VoucherNo")="-" and Container.DataItem("Tr_Nomor")="-" , "True", "False")%>'
                                                CommandName="DELETE"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAddNew" runat="server" Text="Add" 
                        CssClass="small button blue" CausesValidation="False">
                    </asp:Button>
                    <asp:Button ID="ButtonUpload" runat="server" Text="Upload" 
                        CssClass="small button blue" CausesValidation="False">
                    </asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
