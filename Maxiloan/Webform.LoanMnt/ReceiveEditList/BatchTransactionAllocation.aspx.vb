﻿#Region "Imports"
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class BatchTransactionAllocation
    Inherits Maxiloan.Webform.WebBased    
    Protected WithEvents oTrans As ucLookUpTransaction
    Protected WithEvents oLookupAgreement As UCLookupAgreement
    Protected WithEvents txtBatchAmount As ucNumberFormat

#Region "Property"
    Private Property BatchNo() As String
        Get
            Return Request.QueryString("batchno")
        End Get
        Set(ByVal Value As String)
            Request.QueryString("batchno") = Value
        End Set
    End Property

    Private Property BatchSequenceNo() As Integer
        Get
            Return CInt(Request.QueryString("BatchSequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            Request.QueryString("BatchSequenceNo") = CStr(Value)
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return CStr(viewstate("BankAccountName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountName") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CStr(viewstate("BankAccountID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return Request.QueryString("Mode")
        End Get
        Set(ByVal Value As String)
            Request.QueryString("Mode") = Value
        End Set
    End Property

    Private Property TableBatch() As DataTable
        Get
            Return (CType(Viewstate("TableBatch"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TableBatch") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private TotalAmountReceive As Double
    Private oCustomClass As New Parameter.BatchVoucher
    Private oController As New BatchVoucherController
    Private m_Controller As New DataUserControlController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            With txtBatchAmount
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
            End With

            Me.BranchID = Request.QueryString("branchid")
            Me.FormID = "BATCHVOUCHER"
            hypBatchNo.Text = Me.BatchNo
            hypBatchNo.NavigateUrl = "javascript:OpenBatchNo('" & "AccMnt" & "', '" & Server.UrlEncode(hypBatchNo.Text.Trim) & "', '" & Server.UrlEncode(Me.BranchID) & "')"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Me.IsHoBranch Then
                    With oTrans
                        .ProcessID = "PAYMENTRCV"
                        .IsAgreement = "1"
                        .IsPettyCash = ""
                        .IsHOTransaction = ""
                        .IsPaymentReceive = "1"
                        .Style = "AccMnt"
                        .BindData()
                    End With
                Else
                    With oTrans
                        .ProcessID = "PAYMENTRCV"
                        .IsAgreement = "1"
                        .IsPettyCash = ""
                        .IsHOTransaction = "0"
                        .IsPaymentReceive = "1"
                        .Style = "AccMnt"
                        .BindData()
                    End With
                End If
                oLookupAgreement.Style = "ACCMNT"
                txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                pnlListTransaction.Visible = False
                GetComboDepartement()
                BindHeader()
                If Me.Mode = "EDIT" Then
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .BatchNo = Me.BatchNo
                        .BranchId = Me.BranchID
                        .BatchSequenceNo = Me.BatchSequenceNo
                    End With
                    oCustomClass = oController.CashBankBatchVoucherView(oCustomClass)
                    With oCustomClass
                        txtBatchAmount.Text = CStr(.AmountReceive)
                        txtValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
                        txtBatchVoucherDescription.Text = .BatchDescription
                        txtBatchVoucherNotes.Text = .Notes
                        oLookupAgreement.BranchAgreement = .BranchAgreement
                        oLookupAgreement.ApplicationID = .ApplicationID
                        oLookupAgreement.AgreementNo = .Agreementno
                        txtReferenceNo.Text = .ReferenceNo
                        txtReceiveFrom.Text = .ReceivedFrom
                        Me.TableBatch = .TableBatch
                        dtgBatchVoucher.DataSource = Me.TableBatch.DefaultView
                        dtgBatchVoucher.DataBind()
                    End With
                    pnlListTransaction.Visible = True
                End If
            End If
        End If
    End Sub
    Private Sub BindHeader()
        With oCustomClass
            .strConnection = GetConnectionString
            .BatchNo = Me.BatchNo
            .BranchId = Me.BranchID
        End With
        oCustomClass = oController.BatchVoucherView(oCustomClass)
        With oCustomClass
            lblBatchDate.Text = .BatchDate.ToString("dd/MM/yyyy")
        End With
    End Sub

#Region "Data Grid Item Data Bound"
    Private Sub dtgBatchVoucher_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgBatchVoucher.ItemDataBound
        Dim lblTemp As Label
        If sessioninvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim txtBatchAmountAllocation As ucNumberFormat
        Dim lblTotalBatchAmount As Label
        If e.Item.ItemIndex >= 0 Then
            txtBatchAmountAllocation = CType(e.Item.FindControl("txtBatchAmountAllocation"), ucNumberFormat)
            With txtBatchAmountAllocation
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign regular_text"
            End With

            TotalAmountReceive += CDbl(IIf(e.Item.Cells(5).Text.Trim = "", 0, e.Item.Cells(5).Text.Trim))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotalBatchAmount = CType(e.Item.FindControl("lblTotalBatchAmount"), Label)
            lblTotalBatchAmount.Text = FormatNumber(TotalAmountReceive, 2)
        End If
    End Sub
    Private Sub dtgBatchVoucher_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgBatchVoucher.ItemCommand
        If e.CommandName = "DELETE" Then
            Me.TableBatch.Rows.RemoveAt(CInt(e.Item.ItemIndex()))
            dtgBatchVoucher.DataSource = Me.TableBatch.DefaultView
            dtgBatchVoucher.DataBind()
        End If
    End Sub
#End Region
#Region "Generate Table"
    Private Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable("TableBatch")
        With lObjDataTable
            .Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
            .Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
            .Columns.Add("DepartmentID", System.Type.GetType("System.String"))
            .Columns.Add("DepartmentName", System.Type.GetType("System.String"))
            .Columns.Add("TransactionDescription", System.Type.GetType("System.String"))
            .Columns.Add("AmountTransaction", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function

    Private Sub GenerateTable()
        Dim lObjDataRow As DataRow
        Dim i As Integer
        'strZero = ""
        lObjDataRow = Me.TableBatch.NewRow()
        If CheckDuplikasiApplicationID(oTrans.TransactionID) Then
            lObjDataRow("PaymentAllocationID") = oTrans.TransactionID
            lObjDataRow("PaymentAllocationName") = oTrans.Transaction
            lObjDataRow("DepartmentID") = cboDepartmentID.SelectedValue
            lObjDataRow("DepartmentName") = cboDepartmentID.SelectedItem.Text
            lObjDataRow("TransactionDescription") = oTrans.Description
            lObjDataRow("AmountTransaction") = oTrans.Amount
            Me.TableBatch.Rows.Add(lObjDataRow)
        End If
    End Sub

    Private Sub SaveDataGridToDataTable()
        Dim lObjDataRow As DataRow
        Dim i As Integer
        Dim lblPaymentAllocationID As Label
        Dim lblPaymentAllocationDescription As Label
        Dim txtTransactionDescription As TextBox
        Dim lblDepartmentID As Label
        Dim lblDepartmentName As Label
        Dim txtBatchAmountAllocation As ucNumberFormat

        'strZero = ""
        Me.TableBatch.Clear()
        For i = 0 To dtgBatchVoucher.Items.Count - 1
            lObjDataRow = Me.TableBatch.NewRow()

            lblPaymentAllocationID = CType(dtgBatchVoucher.Items(i).FindControl("lblPaymentAllocationID"), Label)
            lblPaymentAllocationDescription = CType(dtgBatchVoucher.Items(i).FindControl("lblPaymentAllocationDescription"), Label)
            txtTransactionDescription = CType(dtgBatchVoucher.Items(i).FindControl("txtTransactionDescription"), TextBox)
            lblDepartmentID = CType(dtgBatchVoucher.Items(i).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(dtgBatchVoucher.Items(i).FindControl("lblDepartmentName"), Label)
            txtBatchAmountAllocation = CType(dtgBatchVoucher.Items(i).FindControl("txtBatchAmountAllocation"), ucNumberFormat)


            lObjDataRow("PaymentAllocationID") = lblPaymentAllocationID.Text
            lObjDataRow("PaymentAllocationName") = lblPaymentAllocationDescription.Text
            lObjDataRow("DepartmentID") = lblDepartmentID.Text
            lObjDataRow("DepartmentName") = lblDepartmentName.Text
            lObjDataRow("TransactionDescription") = txtTransactionDescription.Text
            lObjDataRow("AmountTransaction") = dtgBatchVoucher.Items(i).Cells(5).Text.Trim

            Me.TableBatch.Rows.Add(lObjDataRow)
        Next
    End Sub
#End Region
#Region "Check Duplikasi Application ID"
    Private Function CheckDuplikasiApplicationID(ByVal strPaymentAllocationID As String) As Boolean
        Dim i As Integer
        Dim isValid As Boolean = True
        Dim lblPaymentAllocationID As Label
        For i = 0 To dtgBatchVoucher.Items.Count - 1
            lblPaymentAllocationID = CType(dtgBatchVoucher.Items(i).FindControl("lblPaymentAllocationID"), Label)
            If lblPaymentAllocationID.Text = strPaymentAllocationID Then
                isValid = False
            End If
        Next
        Return isValid
    End Function
#End Region

#Region "Check Amount PDC"
    Private Function CheckAmountPDC() As Boolean
        Dim txtAmountPayment As ucNumberFormat
        Dim isValid As Boolean = True
        Dim Amount As Double = 0
        Dim I As Integer
        For I = 0 To dtgBatchVoucher.Items.Count - 1
            txtAmountPayment = CType(dtgBatchVoucher.Items(I).FindControl("txtBatchAmountAllocation"), ucNumberFormat)
            Amount += CDbl(dtgBatchVoucher.Items(I).Cells(5).Text.Trim)
        Next
        If Round(CDbl(txtBatchAmount.Text.Trim), 2) <> Round(Amount, 2) Then
            isValid = False
        End If
        Return isValid
    End Function
#End Region

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If Me.TableBatch Is Nothing Then
            Me.TableBatch = GetStructPDC()
        End If
        If oTrans.TransactionID <> "" Then
            If CheckDuplikasiApplicationID(oTrans.TransactionID) Then
                SaveDataGridToDataTable()
                GenerateTable()
                dtgBatchVoucher.DataSource = Me.TableBatch.DefaultView
                dtgBatchVoucher.DataBind()
                oTrans.TransactionID = ""
                oTrans.Transaction = ""
                oTrans.Description = ""
                oTrans.Amount = 0
                cboDepartmentID.SelectedIndex = 0

                pnlListTransaction.Visible = True
            End If
        End If
    End Sub
#Region "Departement ID"
    Private Sub GetComboDepartement()
        Dim dtDepartement As New DataTable

        dtDepartement = m_Controller.GetDepartement(GetConnectionString)

        cboDepartmentID.DataTextField = "Name"
        cboDepartmentID.DataValueField = "ID"
        cboDepartmentID.DataSource = dtDepartement
        cboDepartmentID.DataBind()

        cboDepartmentID.Items.Insert(0, "Select One")
        cboDepartmentID.Items(0).Value = "0"
        cboDepartmentID.Items.Insert(1, "None")
        cboDepartmentID.Items(1).Value = "-"
    End Sub
#End Region

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        If Me.TableBatch Is Nothing Then
            Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
        ElseIf Me.TableBatch.Rows.Count = 0 Then
            Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
        Else
            Me.TableBatch.Clear()
            oTrans.TransactionID = ""
            oTrans.Transaction = ""
            oTrans.Description = ""
            oTrans.Amount = 0
            cboDepartmentID.SelectedIndex = 0
            pnlListTransaction.Visible = False
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If CheckAmountPDC() Then
            If ConvertDate2(txtValueDate.Text) > Me.BusinessDate Then

                ShowMessage(lblMessage, "Tanggal Valuta harus < Hari ini", True)
            Else
                SaveDataGridToDataTable()
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .BatchNo = Me.BatchNo
                    If Me.Mode <> "ADD" Then
                        .BatchSequenceNo = Me.BatchSequenceNo
                    End If
                    .ReceivedFrom = txtReceiveFrom.Text
                    .BusinessDate = Me.BusinessDate
                    .BranchAgreement = oLookupAgreement.BranchAgreement
                    .ApplicationID = oLookupAgreement.ApplicationID
                    .ValueDate = ConvertDate2(txtValueDate.Text)
                    .ReferenceNo = txtReferenceNo.Text
                    .BatchDescription = txtBatchVoucherDescription.Text
                    .Notes = txtBatchVoucherNotes.Text
                    .AmountReceive = CDbl(txtBatchAmount.Text)
                    .TableBatch = Me.TableBatch
                End With
                Try
                    If Me.Mode = "ADD" Then
                        oController.CashBankBatchVoucherAdd(oCustomClass)
                    ElseIf Me.Mode = "EDIT" Then
                        oController.CashBankBatchVoucherEdit(oCustomClass)
                    End If

                    Response.Redirect("BatchTransactionReceive.aspx?BatchNo=" & Me.BatchNo & "&BranchID=" & Me.BranchID)
                Catch ex As Exception

                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            End If
        Else

            ShowMessage(lblMessage, "Jumlah Batch Harus sama dengan Alokasi", True)
        End If
    End Sub

End Class