﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchTransactionUpload.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.BatchTransactionUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchTransactionUpload</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                Batch Transaction Upload
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlUpload" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Pilih File
                </label>
                <input id="fileUpload" type="file" name="fileUpload" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlResult" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtResult" runat="server" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save"></asp:Button>&nbsp;
            <asp:Button ID="btnCancel" runat="server" Text="Cancel"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
