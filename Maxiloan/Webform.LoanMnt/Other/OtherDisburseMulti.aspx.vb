﻿#Region "Imports"
Imports System.Threading
Imports System.Math
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class OtherDisburseMulti
    Inherits Maxiloan.Webform.AccMntWebBased        
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oPaymentDetail As UCPaymentDetailD
    Protected WithEvents oBGNODate As ucBGNoDate

#Region "Property"
    Private Property ReceiveFrom() As String
        Get
            Return (CType(ViewState("ReceiveFrom"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReceiveFrom") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property JenisTransfer() As String
        Get
            Return (CType(ViewState("JenisTransfer"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("JenisTransfer") = Value
        End Set
    End Property

    Private Property BeneficiaryBankID() As String
        Get
            Return (CType(ViewState("BeneficiaryBankID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankID") = Value
        End Set
    End Property

    Private Property BeneficiaryBankBranchID() As String
        Get
            Return (CType(ViewState("BeneficiaryBankBranchId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankBranchId") = Value
        End Set
    End Property

    Private Property BeneficiaryBankName() As String
        Get
            Return (CType(ViewState("BeneficiaryBankName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankName") = Value
        End Set
    End Property

    Private Property BeneficiaryBankBranchCode() As String
        Get
            Return (CType(ViewState("BeneficiaryBankBranchCode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankBranchCode") = Value
        End Set
    End Property

    Private Property BeneficiaryBankCode() As String
        Get
            Return (CType(ViewState("BeneficiaryBankCode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankCode") = Value
        End Set
    End Property

    Private Property BeneficiaryBankCity() As String
        Get
            Return (CType(ViewState("BeneficiaryBankCity"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankCity") = Value
        End Set
    End Property

    Private Property BeneficiaryBankBranchName() As String
        Get
            Return (CType(ViewState("BeneficiaryBankBranchName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankBranchName") = Value
        End Set
    End Property

    Private Property BeneficiaryBankAccountNo() As String
        Get
            Return (CType(ViewState("BeneficiaryBankAccountNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankAccountNo") = Value
        End Set
    End Property

    Private Property BeneficiaryBankAccountName() As String
        Get
            Return (CType(ViewState("BeneficiaryBankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BeneficiaryBankAccountName") = Value
        End Set
    End Property

    Private Property ReferenceNo() As String
        Get
            Return (CType(ViewState("ReferenceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReferenceNo") = Value
        End Set
    End Property

    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return (CType(ViewState("BankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountName") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Public Property TotTransAmount() As Double
        Get
            Return (CType(hdTotal.Value, Double))
        End Get
        Set(ByVal TotTransAmount As Double)
            hdTotal.Value = TotTransAmount.ToString
        End Set
    End Property
#End Region

#Region "Constanta"
    Dim temptotalTrans As Double
    Dim intLoopGrid As Integer
    Private oCustomClass As New Parameter.OtherReceive
    Private oController As New OtherDisburseController
    Private m_Controller As New DataUserControlController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub

        If Not IsPostBack Then
            Me.FormID = "OTHERDISBURSE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                ' oValueDate.dateValue = CStr(Day(Me.BusinessDate)) + "/" + CStr(Month(Me.BusinessDate)) + "/" + CStr(Year(Me.BusinessDate))
                If Request.QueryString("msg") = "succes" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)                    
                End If


                Me.BranchID = Request.QueryString("branchid")
                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.hpsxml = "1"
                oCustomClass = oController.GetTable(oCustomClass, pstrFile)
                oPaymentDetail.IsTitle = True
                oPaymentDetail.ValueDate = CDate(Me.BusinessDate).ToString("dd/MM/yyyy")
                oPaymentDetail.WithOutPrepaid = True                
                'DoBind()
            End If
        End If
    End Sub
#End Region

#Region "DoBind"
    'Sub DoBind()
    '    If Me.IsHoBranch Then
    '        With oTrans
    '            .ProcessID = "OTHDSBNA"
    '            .IsAgreement = "0"
    '            .IsPettyCash = "0"
    '            .IsHOTransaction = ""
    '            .IsPaymentReceive = "0"
    '            .Style = "AccMnt"
    '            .BindData()
    '        End With
    '    Else
    '        With oTrans
    '            .ProcessID = "OTHDSBNA"
    '            .IsAgreement = "0"
    '            .IsPettyCash = "0"
    '            .IsHOTransaction = "0"
    '            .IsPaymentReceive = "0"
    '            .Style = "AccMnt"
    '            .BindData()
    '        End With
    '    End If
    'End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonimbCancel.Click        
        Response.Redirect("OtherDisburseMulti.aspx?msg=cancel")
    End Sub
#End Region

#Region "Databound"
    Private Sub DtgTransList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgTransList.ItemDataBound
        'If SessionInvalid() Then
        '    Exit Sub
        'End If
        'Dim totalTransAmount As New Label
        'Dim lTransAmount As New Label

        'With oCustomClass

        '    If e.Item.ItemIndex >= 0 Then
        '        lTransAmount = CType(e.Item.FindControl("lblAmountTrans"), Label)
        '        temptotalTrans += CDbl(lTransAmount.Text.Replace(",", ""))
        '    End If

        '    If e.Item.ItemType = ListItemType.Footer Then

        '        totalTransAmount = CType(e.Item.FindControl("lblTotTransAmount"), Label)
        '        Me.TotTransAmount = CDbl(temptotalTrans)
        '        totalTransAmount.Text = FormatNumber(temptotalTrans.ToString, 2)
        '    End If
        'End With
        'Dim imbDelete As ImageButton
        'If e.Item.ItemIndex >= 0 Then
        '    imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
        '    imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        'End If
    End Sub

#End Region

#Region "ItemCommand"
    'Private Sub DtgTransList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgTransList.ItemCommand
    '    Dim pstrFile As String
    '    If e.CommandName = "DELETE" Then
    '        With oCustomClass
    '            pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
    '            .LoginId = Me.Session.SessionID + Me.Loginid
    '            .BranchId = Me.BranchID
    '            .flagdelete = "1"
    '            .NumTrans = DtgTransList.Items.Count
    '            .IndexDelete = CStr(e.Item.ItemIndex())
    '            .hpsxml = "2"
    '        End With
    '        oCustomClass = oController.GetTable(oCustomClass, pstrFile)
    '        DtgTransList.DataSource = oCustomClass.listReceive
    '        DtgTransList.DataBind()
    '        DtgTransList.Visible = True
    '        pnlTrans.Visible = True
    '        pnlView.Visible = True
    '        pnlawal.Visible = False
    '    End If
    'End Sub
#End Region

#Region "Departement ID"
    Private Sub GetComboDepartement()
        Dim dtDepartement As New DataTable

        dtDepartement = m_Controller.GetDepartement(GetConnectionString)

        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"
        cboDepartement.Items.Insert(1, "None")
        cboDepartement.Items(1).Value = "-"
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click        
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "OTHERDISBURSE"
        ButtonSave.Attributes.Add("Onclick", "return checksubmit()")

        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim strIDTrans As String
            Dim strDepartmentID As String = ""
            Dim strDescTrans As String
            Dim strAmountTrans As String
            'Dim chk As New CheckBox
            Dim intLoopGrid As Integer = 0
            Dim lblDepartmentID As New Label
            Dim lblDepartmentName As New Label
            Dim lblPaymentAllocID As New Label
            Dim lblPaymentAllocDesc As New Label
            Dim txtKeterangan As New TextBox
            Dim txtAmountTrans As New TextBox
            'Dim status As Boolean = False
            Dim pStrFile As String = ""

            strIDTrans = ""
            strDescTrans = ""
            strAmountTrans = ""

            'For i = 0 To DtgTransList.Items.Count - 1
            '    chk = CType(DtgTransList.Items(i).FindControl("chk"), CheckBox)

            '    If chk.Checked Then
            '        status = True
            '    End If

            'Next

            'If Not status Then
            '    ShowMessage(lblMessage, "Harap periksa item", True)
            '    Exit Sub
            'End If

            If DtgTransList.Items.Count > 0 Then
                'Dim Ndttable As DataTable                

                If Me.TotTransAmount = Me.AmountReceive Then

                    For intLoopGrid = 0 To DtgTransList.Items.Count - 1
                        'chk = CType(DtgTransList.Items(intLoopGrid).FindControl("chk"), CheckBox)

                        lblDepartmentID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
                        lblDepartmentName = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
                        lblPaymentAllocID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
                        lblPaymentAllocDesc = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
                        txtKeterangan = CType(DtgTransList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
                        txtAmountTrans = CType(DtgTransList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)

                        'If chk.Checked Then
                        strIDTrans &= CStr(IIf(strIDTrans = "", "", ",")) & lblPaymentAllocID.Text.Replace("'", "")
                        strDepartmentID &= CStr(IIf(strDepartmentID = "", "", ",")) & lblDepartmentID.Text.Replace("'", "")
                        strDescTrans &= CStr(IIf(strDescTrans = "", "", ",")) & txtKeterangan.Text.Replace("'", "")
                        strAmountTrans &= CStr(IIf(strAmountTrans = "", "", ",")) & txtAmountTrans.Text.Replace("'", "").Replace(",", "")
                        'End If
                    Next

                    'Ndttable = GetStructTrans()

                    With oCustomClass
                        'pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                        .LoginId = Me.Loginid
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .strConnection = GetConnectionString()
                        .ReceivedFrom = Me.ReceiveFrom
                        .BusinessDate = Me.BusinessDate
                        .ReferenceNo = Me.ReferenceNo
                        .BankAccountID = Me.BankAccount
                        .WOP = Me.WayOfPayment
                        .Notes = oPaymentDetail.Notes.Trim
                        .CoyID = "001"
                        .AmountReceive = Me.AmountReceive
                        .ValueDate = Me.ValueDate
                        .NumTrans = DtgTransList.Items.Count
                        .strId = strIDTrans.Trim
                        .strDesc = strDescTrans.Trim
                        .strAmount = strAmountTrans.Trim

                        .DepartementID = strDepartmentID
                        .BranchIDX = oBranch.BranchID

                        If Me.WayOfPayment = "GT" Then
                            .JenisTransfer = Me.JenisTransfer
                            .BeneficiaryBankID = Me.BeneficiaryBankID
                            .BeneficiaryBankBranchID = Me.BeneficiaryBankBranchID
                            .BeneficiaryBankAccountNo = Me.BeneficiaryBankAccountNo
                            .BeneficiaryBankAccountName = Me.BeneficiaryBankAccountName
                        Else
                            If Not oBGNODate.Visible Then
                                .BGDueDate = "01/01/1900"
                                .BGNo = "0"
                            Else
                                If oBGNODate.BGNumber <> "None" Then
                                    .BGDueDate = CDate(oBGNODate.InDueDate)
                                Else
                                    .BGDueDate = "01/01/1900"
                                End If
                                .BGNo = oBGNODate.BGNumber
                            End If
                        End If
                    End With

                    Try
                        If Me.WayOfPayment = "GT" Then
                            oCustomClass = oController.OtherDisburseAdd(oCustomClass, pStrFile)
                            If oCustomClass.ErrorMessage <> "" Then
                                ShowMessage(lblMessage, oCustomClass.ErrorMessage, True)
                                Exit Sub
                            Else                                
                                Response.Redirect("OtherDisburseMulti.aspx?msg=succes")
                            End If
                        Else
                            oController.SaveTrans(oCustomClass, pStrFile)
                            'oCustomClass.hpsxml = "1"
                            'oCustomClass = oController.GetTable(oCustomClass, pStrFile)
                            Response.Redirect("OtherDisburseMulti.aspx?msg=succes")
                        End If

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)
                    End Try
                Else
                    ShowMessage(lblMessage, "Total Nilai harus Sama dengan Jumlah Dibayar", True)
                End If
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            End If
        End If
    End Sub
#End Region

#Region "GetStructTrans"
    Public Function GetStructTrans() As DataTable
        Dim lObjDataTable As New DataTable("Disburse")
        With lObjDataTable
            .Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
            .Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
            .Columns.Add("Description", System.Type.GetType("System.String"))
            .Columns.Add("Amount", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function
#End Region

#Region "Next"
    Private Sub ImbNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        txtJLookup.Text = "Belum dipilih!"
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            GetComboDepartement()

            Me.ReceiveFrom = oPaymentDetail.ReceivedFrom
            Me.WayOfPayment = oPaymentDetail.WayOfPayment
            Me.WayOfPaymentDesc = oPaymentDetail.WayOfPaymentName
            Me.JenisTransfer = oPaymentDetail.JenisTransfer
            Me.BeneficiaryBankID = oPaymentDetail.BeneficiaryBankID
            Me.BeneficiaryBankBranchID = oPaymentDetail.BeneficiaryBankBranchID
            Me.BeneficiaryBankName = oPaymentDetail.BeneficiaryBankName
            Me.BeneficiaryBankBranchCode = oPaymentDetail.BeneficiaryBankBranchCode
            Me.BeneficiaryBankCode = oPaymentDetail.BeneficiaryBankCode
            Me.BeneficiaryBankCity = oPaymentDetail.BeneficiaryBankCity
            Me.BeneficiaryBankBranchName = oPaymentDetail.BeneficiaryBankBranchName
            Me.BeneficiaryBankAccountNo = oPaymentDetail.BeneficiaryBankAccountNo
            Me.BeneficiaryBankAccountName = oPaymentDetail.BeneficiaryBankAccountName
            Me.BankAccount = oPaymentDetail.BankAccount
            Me.BankAccountName = oPaymentDetail.BankAccountName
            Me.ValueDate = ConvertDate2(oPaymentDetail.ValueDate)
            Me.ReferenceNo = oPaymentDetail.ReferenceNo
            Me.AmountReceive = oPaymentDetail.AmountReceive

            'If Me.ReferenceNo = "" Then
            '    ShowMessage(lblMessage, "Harap isi No Bukti Kas Keluar", True)
            '    pnlawal.Visible = True
            '    pnlView.Visible = False
            '    Exit Sub
            'Else
            If CStr(Me.ValueDate) = "" Then
                ShowMessage(lblMessage, "Harap isi Tanggal Valuta", True)
                pnlawal.Visible = True
                pnlView.Visible = False
                Exit Sub
            ElseIf CDbl(Me.AmountReceive) <= 0 Then
                ShowMessage(lblMessage, "Jumlah dibayar harus > 0 ", True)
                pnlawal.Visible = True
                pnlView.Visible = False
                Exit Sub
            ElseIf DateDiff(DateInterval.Day, Me.ValueDate, Me.BusinessDate) < 0 Then
                ShowMessage(lblMessage, "Tanggal Valuta Tidak boleh > Tanggal hari ini", True)
                pnlawal.Visible = True
                pnlView.Visible = False
                Exit Sub
            Else
                lblMessage.Visible = False
            End If

            pnlawal.Visible = False
            pnlView.Visible = True
            pnlButton.Visible = True

            lblRecipientName.Text = Me.ReceiveFrom
            lblWOP.Text = Me.WayOfPaymentDesc
            lblJenisTransfer.Text = Me.JenisTransfer
            lblNamaBank.Text = Me.BeneficiaryBankName
            lblNamaCabang.Text = Me.BeneficiaryBankBranchName
            lblNoRekening.Text = Me.BeneficiaryBankAccountNo
            lblNamaRekening.Text = Me.BeneficiaryBankAccountName
            lblReferenceNo.Text = Me.ReferenceNo
            lblAmountRec.Text = FormatNumber(Me.AmountReceive, 0)
            lblNotes.Text = oPaymentDetail.Notes.Trim
            lblBankAccount.Text = Me.BankAccountName
            lblValueDate.Text = Me.ValueDate.ToString("dd/MM/yyyy")

            Select Case Me.WayOfPayment
                Case "BA"
                    oBGNODate.BindBGNODate(" bankaccountid = '" & Me.BankAccount & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
                    oBGNODate.Visible = True
                    pnlGT.Visible = False
                Case "CA"
                    oBGNODate.Visible = False
                    pnlGT.Visible = False
                Case "GT"
                    oBGNODate.Visible = False
                    pnlGT.Visible = True
                Case Else
                    oBGNODate.Visible = False
                    pnlGT.Visible = False
            End Select
        End If
    End Sub
#End Region


    Private Sub ButtonDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDelete.Click
        Dim intLoopGrid As Integer
        Dim chk As New CheckBox
        Dim tempDels As String = ""

        For intLoopGrid = 0 To DtgTransList.Items.Count - 1
            chk = CType(DtgTransList.Items(intLoopGrid).FindControl("chk"), CheckBox)

            If chk.Checked Then
                If tempDels.Trim = "" Then
                    tempDels = tempDels + "" + intLoopGrid.ToString + ""
                Else
                    tempDels = tempDels + "," + intLoopGrid.ToString + ""
                End If
            End If

        Next

        Dim Dels() As String = tempDels.Split(",")

        For index = 0 To Dels.Length - 1
            Dim Del As String = Dels(index)

            DeleteBaris(CInt(Del))
        Next
    End Sub

    Private Sub Jlookup_Click(sender As Object, e As System.EventArgs) Handles Jlookup.Click
        pnlawal.Visible = False
        pnlView.Visible = True
        pnlTrans.Visible = True
        pnlButton.Visible = True

        If cboDepartement.SelectedValue.Trim <> "0" Then
            Dim Pays() As String = vsPay.Value.Split(",")
            Dim Descs() As String = vsDesc.Value.Split(",")
            Dim Jumlahs() As String = vsJumlah.Value.Split(",")

            If Pays.Length > 0 Then
                txtJLookup.Text = "Sudah dipilih!"
                For index = 0 To Pays.Length - 1
                    Dim Pay As String = Pays(index).Replace("'", "")
                    Dim Desc As String = Descs(index).Replace("'", "")
                    Dim Jumlah As String = Jumlahs(index).Replace("'", "")

                    AddRecord(Pay, Desc, CInt(Jumlah))
                Next
                RFVtxtJLookup.Visible = False
            Else
                txtJLookup.Text = "Belum dipilih!"                
            End If

        Else
            ShowMessage(lblMessage, "Departemen Harus diisi!", True)
            Exit Sub
        End If


        'Dim lBytEnd As Integer
        'Dim lBytCounter As Integer
        'Dim lblListTransNo As Label
        'Dim lstrTrans As String
        'Dim pStrFile As String

        'With oCustomClass
        '    pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        '    .LoginId = .LoginId
        '    .strConnection = GetConnectionString()
        '    .ReceivedFrom = Me.ReceiveFrom
        '    .ReferenceNo = Me.ReferenceNo
        '    .WOP = Me.WayOfPayment
        '    .ValueDate = Me.ValueDate
        '    .BankAccountID = Me.BankAccount
        '    Me.BankAccount = .BankAccountID
        '    .BankAccountName = Me.BankAccountName
        '    .AmountReceive = oPaymentDetail.AmountReceive
        '    .Notes = oPaymentDetail.Notes

        '    If Me.WayOfPayment = "BA" Then
        '        .BGNo = oBGNODate.BGNumber
        '        If oBGNODate.BGNumber <> "None" Then
        '            .BGDueDate = CDate(oBGNODate.InDueDate)
        '        Else
        '            .BGDueDate = "01/01/1900"
        '        End If
        '    End If

        '    .DepartementID = cboDepartement.SelectedValue
        '    .DepartmentName = cboDepartement.SelectedItem.Text
        '    .PaymentAllocationID = oTrans.TransactionID
        '    .PaymentAllocationdesc = oTrans.Transaction
        '    .AmountTrans = oTrans.Amount
        '    .Desc = oTrans.Description
        '    .flagdelete = "0"

        'End With

        'oCustomClass = oController.GetTable(oCustomClass, pStrFile)

        'If Not oCustomClass.IsValidTrans Then

        '    ShowMessage(lblMessage, "Transaksi Sudah ada", True)
        '    Exit Sub
        'Else
        '    DtgTransList.DataSource = oCustomClass.listReceive
        '    DtgTransList.DataBind()
        '    DtgTransList.Visible = True    
        'End If

        'pnlTrans.Visible = True        
        'cboDepartement.SelectedIndex = 0    
    End Sub

    Public Sub AddRecord(ByVal ID As String, ByVal Desc As String, ByVal i As Integer)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow

        Dim chk As New CheckBox
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim txtAmountTrans As New TextBox

        With objectDataTable
            .Columns.Add(New DataColumn("chk", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
            .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
            .Columns.Add(New DataColumn("txtAmountTrans", GetType(String)))
        End With

        For intLoopGrid = 0 To DtgTransList.Items.Count - 1
            chk = CType(DtgTransList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgTransList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgTransList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)



            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("chk") = CType(chk.Checked, String)
            oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
            oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
            oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
            oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
            oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
            oRow("txtAmountTrans") = CType(txtAmountTrans.Text, String)

            objectDataTable.Rows.Add(oRow)            
        Next

        If i = 0 Then
            i = i + 1
        End If


        For index = 1 To i
            oRow = objectDataTable.NewRow()
            oRow("chk") = "False"
            oRow("DepartmentID") = cboDepartement.SelectedValue
            oRow("DepartmentName") = cboDepartement.SelectedItem.Text
            oRow("PaymentAllocationID") = ID
            oRow("PaymentAllocationName") = Desc
            oRow("txtKeterangan") = ""
            oRow("txtAmountTrans") = "0"

            objectDataTable.Rows.Add(oRow)
        Next

        DtgTransList.DataSource = objectDataTable
        DtgTransList.DataBind()

        For intLoopGrid = 0 To DtgTransList.Items.Count - 1
            chk = CType(DtgTransList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgTransList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgTransList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)

            chk.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim)
            lblDepartmentID.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            lblDepartmentName.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblPaymentAllocID.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblPaymentAllocDesc.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtKeterangan.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtAmountTrans.Text = FormatNumber(objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim, 0)

        Next

        ScriptManager.RegisterStartupScript(DtgTransList, GetType(DataGrid), DtgTransList.ClientID, String.Format(" total(); ", DtgTransList.ClientID), True)
    End Sub
    Public Sub DeleteBaris(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim chk As New CheckBox
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim txtAmountTrans As New TextBox

        Dim oNewDataTable As New DataTable


        With oNewDataTable
            .Columns.Add(New DataColumn("chk", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
            .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
            .Columns.Add(New DataColumn("txtAmountTrans", GetType(String)))
        End With

        For intLoopGrid = 0 To DtgTransList.Items.Count - 1
            chk = CType(DtgTransList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgTransList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgTransList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            '----- Add row -------'
            If intLoopGrid <> Index And Not chk.Checked Then
                oRow = oNewDataTable.NewRow()
                oRow("chk") = CType(chk.Checked, String)
                oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
                oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
                oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
                oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
                oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
                oRow("txtAmountTrans") = CType(txtAmountTrans.Text, String)

                oNewDataTable.Rows.Add(oRow)
            End If
        Next
        DtgTransList.DataSource = oNewDataTable
        DtgTransList.DataBind()

        For intLoopGrid = 0 To DtgTransList.Items.Count - 1
            chk = CType(DtgTransList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgTransList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgTransList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgTransList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgTransList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)

            chk.Checked = CBool(oNewDataTable.Rows(intLoopGrid).Item(0).ToString.Trim)
            lblDepartmentID.Text = oNewDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            lblDepartmentName.Text = oNewDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblPaymentAllocID.Text = oNewDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblPaymentAllocDesc.Text = oNewDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtKeterangan.Text = oNewDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtAmountTrans.Text = FormatNumber(oNewDataTable.Rows(intLoopGrid).Item(6).ToString.Trim, 0)

        Next

        ScriptManager.RegisterStartupScript(DtgTransList, GetType(DataGrid), DtgTransList.ClientID, String.Format(" total(); ", DtgTransList.ClientID), True)
    End Sub
    Sub oPaymentDetail_load() Handles oPaymentDetail.Changed
        lblMessage.Visible = False
    End Sub
End Class
