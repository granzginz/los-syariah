﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferFundReconcile.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.TransferFundReconcile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountid" Src="../../Webform.UserController/UcBankAccountid.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TransferFundReconcile</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
   
</head>
<body>
 <script language="javascript" type="text/javascript">
     var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
     var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
     var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
     function OpenWinViewTransferFund(pStyle, pTransferNo, pBranch) {
         var x = screen.width; var y = screen.height - 100;
         window.open(ServerName + App + '/Webform.CashMgt/TransferFund/ViewTransferFund.aspx?style=' + pStyle + '&TransferNo=' + pTransferNo + '&Branch=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
     }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Literal ID="ltrBankID" runat="server"></asp:Literal>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        RECONCILE HASIL TRANSFER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Transfer</label>
                        <asp:DropDownList ID="cboTransferType" runat="server">
                            <asp:ListItem Value="0" Selected="True">Select One</asp:ListItem>
                            <asp:ListItem Value="1">Tarik Dari Cabang</asp:ListItem>
                            <asp:ListItem Value="2">Transfer Ke Cabang</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cboTransferType"
                            ErrorMessage="Harap pilih jenis Transfer" InitialValue="0" Display="Dynamic"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Valuta</label>
                        <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtdate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtdate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Bank</label>
                        <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSFER DANA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgTrans" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" DataKeyField="TransferNO" AutoGenerateColumns="False" OnSortCommand="SortGrid"
                                AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="hypReconcile" runat="server" Text="RECON" CommandName="Reconcile"
                                                Width="4%"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TransferNO" HeaderText="NO TRANSFER">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:HyperLink ID="HyTransferNo" runat="server" Text='<%#Container.DataItem("TransferNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTot" runat="server" Text="Total" Font-Bold="True"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="BankAccountIdFromTo" HeaderText="REKENING BANK">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountID" runat="server" Text='<%#Container.DataItem("BankAccountIdFromTo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BankAccountNameFromTo" HeaderText="NAMA REKENING">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountNameFromTo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="28%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNO" HeaderText="NO BUKTI">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReferenceNO" runat="server" Text='<%#Container.DataItem("ReferenceNO")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Right" CssClass="item_grid_right" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" CssClass="item_grid_right" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotAmount" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PostingDate" SortExpression="PostingDate" HeaderText="TGL TRANSF"
                                        DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ValueDate" SortExpression="ValueDate" HeaderText="TGL VALUTA"
                                        DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlHasil" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            RECONCILE HASIL TRANSFER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                Cabang</label>
                            <asp:Label ID="lblBranch" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                No Transfer</label>
                            <asp:Label ID="LblTransferNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Valuta</label>
                            <asp:Label ID="lblvalueDate" runat="server"></asp:Label>
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                Jenis Transfer</label>
                            <asp:Label ID="LblTransferType" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Posting</label>
                            <asp:Label ID="LblPostingDate" runat="server"></asp:Label>
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                No Bukti Kas</label>
                            <asp:Label ID="lblReffNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Jumlah Transfer</label>
                            <asp:Label ID="LblAmountTransfer" runat="server"></asp:Label>
                        </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Keterangan</label>
                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            TRANSFER DARI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                Cabang</label>
                            <asp:Label ID="LblBranchTransferFrom" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                No Voucher</label>
                            <asp:Label ID="lblVoucherNoTransferFrom" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Rekening Bank</label>
                            <asp:Label ID="lblBankAccountTransferForm" runat="server"></asp:Label>
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                No Bilyet Giro</label>
                            <asp:Label ID="lblBGNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Jatuh Tempo BG</label>
                            <asp:Label ID="lblBGDueDate" runat="server"></asp:Label>
                        </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            TRANSFER KE
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                Cabang</label>
                            <asp:Label ID="lblBranchTransferTo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label>
                                No Voucher</label>
                            <asp:Label ID="lblVoucherNoTransferTo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Rekening Bank</label>
                            <asp:Label ID="lblBankAccountTransferTo" runat="server"></asp:Label>
                        </div>
                </div>
                <div class="form_box">
                     <div class="form_left">
                            <label class="label_req">
                                No Bukti Kas Masuk</label>
                            <asp:TextBox ID="txtReffNo" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtReffNo"
                                ErrorMessage="Harap Isi No Bukti Kas Masuk" Display="Dynamic" Visible="False"
                                Enabled="False" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                        </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                   
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
