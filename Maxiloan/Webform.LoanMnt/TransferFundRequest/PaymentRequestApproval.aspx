﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestApproval.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentRequestApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../Webform.UserController/ucBranchHO.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentRequestApproval</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + '//' + window.location.host + '/';
        console.log(ServerName + App);
        function OpenWinMain(BranchId, RequestNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/PaymentRequest/PaymentRequestInquiryView.aspx?BranchId=' + BranchId + '&RequestNo=' + RequestNo + '&style=AccMnt', null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }					
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        APPROVAL PERMINTAAN PEMBAYARAN
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Cabang</label>
                        <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Request</label>
                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                            <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"  Width="88px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="cboStatus" runat="server">
                            <asp:ListItem Value="APR" Selected="True">Approval</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PERMINTAAN PEMBAYARAN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemStyle CssClass="command_col" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkApp" runat="server" CommandName="approve">APPROVE</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestBy" HeaderText="DIAJUKAN OLEH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestBy" runat="server" Text='<%#Container.DataItem("RequestBy")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Container.DataItem("Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAmount" runat="server" Visible="True" Text='0.0'></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL REQUEST"
                                        DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDetail" Visible="False">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            VIEW - PAYMENT REQUEST
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Cabang Request</label>
                            <asp:Label ID="lblBranchRequest" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Request</label>
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                No Request</label>
                            <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Rekening Bank</label>
                            <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Jumlah Request</label>
                            <asp:Label ID="lblAmountView" runat="server" EnableViewState="False"></asp:Label>
                        </div>
                    </div>
                </div>
                
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                        <label>
                            Keterangan</label>
                        <asp:Label ID="lblDescriptionView" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DETAIL TRANSAKSI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="Datagrid1" runat="server" Width="100%" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" Visible="False" ShowFooter="true">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="DEPARTEMENT">
                                        <HeaderStyle Width="25%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartement" runat="server" Text='<%#Container.DataItem("DepartementName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TRANSAKSI">
                                        <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyPettyCashNo" runat="server" Text='<%#Container.DataItem("TransactionName")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="Label5" runat="server">Total</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH REQUEST"  ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        <HeaderStyle Width="15%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestAmount" runat="server" Text='<%#formatnumber(Container.DataItem("RequestAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalRequestAmount" runat="server">0.0</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH TRANSFER" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        <HeaderStyle Width="25%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransferAmountDetail" runat="server" Text='<%#formatnumber(Container.DataItem("TransferAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTransferAmount" runat="server">0.0</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <HeaderStyle Width="25%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            INFORMASI TRANSFER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                No Voucher</label>
                            <asp:Label ID="lblTransferRefVoucherNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Transfer</label>
                            <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                No Bukti Kas</label>
                            <asp:Label ID="lblTransferRefNo" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Jumlah Transfer</label>
                            <asp:Label ID="lblTransferAmount" runat="server" EnableViewState="False"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            STATUS
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Status</label>
                            <asp:Label ID="lblStatusView" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Status</label>
                            <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label>
                                Diajukan Oleh</label>
                            <asp:Label ID="lblRequestByView" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonApp" runat="server" Text="Approve" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonDecline" runat="server" Text="Decline" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
