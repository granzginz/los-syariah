﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ExecutePaymentRequest
    Inherits Maxiloan.Webform.WebBased

#Region "InitialDefaultPanel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub
#End Region
#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable    
    Protected WithEvents oBankAccount As ucBankAccountNoCondition
    Private Const SP_NAME_PAGING As String = "spPaymentRequestReimburseSelect"
    Dim oController As New GeneralPagingController
    Private Const FILE_NAME_NEXT As String = ""

#End Region
#Region "Property"

    Private Property BankAccountID() As String
        Get
            Return (CType(viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

    Private Property StartSelectionDate() As String
        Get
            Return (CType(viewstate("StartSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartSelectionDate") = Value
        End Set
    End Property

    Private Property BulkofRequest() As String
        Get
            Return (CType(viewstate("BulkofRequest"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BulkofRequest") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "EXEPAYREQ"
        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'If IsSingleBranch And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 Then
                    InitialDefaultPanel()
                    'txtRequestDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                    oBankAccount.Type = "B"
                    oSearchBy.ListData = "paymentrequest.RequestNo,Request No-paymentrequest.Description, Description"
                    oSearchBy.BindData()
                Else
                    KickOutForMultipleBranch()
                End If
            End If
        End If

    End Sub
#Region "Ketika Tombol Search dipijit"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        PnlGrid.Visible = True
        Me.SearchBy = " And paymentrequest.BranchID in( '" & Replace(Me.sesBranchId, "'", "") & "') "
        Me.SearchBy &= " And IsReconcile = 1 And Status = 'OTO' "

        If oSearchBy.Text.Trim <> "" Then
            If oSearchBy.Text.IndexOf("%") >= 0 Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " Like  '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "

            End If
        End If

        If txtRequestDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " And RequestDate = Convert(Datetime,'" & Replace(txtRequestDate.Text, "'", "") & "',103) "
        End If

        If cboRequestType.SelectedValue <> "0" Then
            Me.SearchBy = Me.SearchBy & " And RequestType = '" & cboRequestType.SelectedValue & "' "

        End If

        Me.BankAccountID = oBankAccount.BankAccountID
        BindGrid(Me.SearchBy, Me.SortBy)

    End Sub
#End Region

#Region "BindGrid"
    Public Sub BindGrid(ByVal cmdWhere As String, ByVal cmdsortby As String)
        Dim oEntities As New Parameter.GeneralPaging

        If Me.SortBy = "" Then
            Me.SortBy = " RequestNo "
        End If

        With oEntities
            .SpName = SP_NAME_PAGING
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)
        Dim dt As DataSet
        If Not oEntities Is Nothing Then
            dt = oEntities.ListDataReport
        End If
        DtgExecute.DataSource = dt
        DtgExecute.DataBind()
    End Sub
#End Region
#Region "SortGrid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "DataBound"
    'lblGridRequestNo
    'DtgExecute
    Private Sub DtgExecute_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgExecute.ItemCommand

        If e.CommandName = "Execute" Then
            Dim lblGridRequestNo As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblGridRequestNo"), Label)
            Dim lblBankAccountID As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblBankAccountID"), Label)
            Dim lblBankAccountName As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblBankAccountName"), Label)
            Dim lblRequestType As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblRequestType"), Label)
            Dim lblRequestDate As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblRequestDate"), Label)
            Dim lblTransferDate As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblTransferDate"), Label)
            Dim LblDescription As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("LblDescription"), Label)
            Dim lblDepartementName As Label = CType(DtgExecute.Items(e.Item.ItemIndex).FindControl("lblDepartementName"), Label)

            Dim cookieNew As New HttpCookie("ExecutePaymentRequest")
            cookieNew.Values.Add("BranchRequest", Replace(Me.sesBranchId, "'", ""))
            cookieNew.Values.Add("RequestNo", lblGridRequestNo.Text.Trim)
            cookieNew.Values.Add("TransferDate", lblTransferDate.Text.Trim)
            cookieNew.Values.Add("RequestDate", lblRequestDate.Text.Trim)
            cookieNew.Values.Add("RequestType", lblRequestType.Text.Trim)
            cookieNew.Values.Add("Description", LblDescription.Text.Trim)
            cookieNew.Values.Add("BankAccountName", lblBankAccountName.Text.Trim)
            cookieNew.Values.Add("BankAccountID", lblBankAccountID.Text.Trim)
            cookieNew.Values.Add("DepartementName", lblDepartementName.Text.Trim)

            Response.AppendCookie(cookieNew)
            Response.Redirect("ExecutePaymentRequestNextStep.aspx?RequestNo='" & lblGridRequestNo.Text & "'&BranchID='" & Replace(Me.sesBranchId, "'", "") & "'")

        End If

    End Sub
#End Region




    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("ExecutePaymentRequest.aspx")
    End Sub

End Class