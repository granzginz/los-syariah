﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PaymentRequestApproval
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents osearchby As UcSearchBy
    Protected WithEvents oBranch As UcBranch    
    Protected WithEvents hyNo As System.Web.UI.WebControls.HyperLink
#Region "Properties"
    Private Property RequestNo() As String
        Get
            Return (CType(viewstate("RequestNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("RequestNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private Const STR_FORM_ID As String = "PAYREQAPR"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PaymentRequest
    Private oController As New Controller.PaymentRequestController
    Private m_TotalAmount As Double
    Private m_TotalTransferAmount As Double
    Private m_TotalRequestAmount As Double
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then Exit Sub

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = STR_FORM_ID

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then                
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oBranch.DataBind()
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        cboStatus.SelectedIndex = 0
        pnlDatagrid.Visible = False
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        txtPage.Text = "1"
        Me.SearchBy = "  PR.branchid = " & "'" & oBranch.BranchID.Trim & "'"

        If cboStatus.SelectedValue.ToUpper = "APR" Then
            Me.SearchBy = Me.SearchBy & " AND PR.IsReconcile <> 1 AND PR.Status = '" & cboStatus.SelectedItem.Value & "' "
        End If

        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & " and (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"
            End If
        End If

        If txtDate.Text.Trim.Length > 0 Then
            Me.SearchBy = Me.SearchBy & " AND RequestDate <= '" & ConvertDate2(txtDate.Text.Trim) & "'"
        End If

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label

        If e.Item.ItemIndex >= 0 Then
            hyNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            If Not hyNo Is Nothing Then
                hyNo.NavigateUrl = "javascript:OpenWinMain('" & oBranch.BranchID.Trim & "','" & hyNo.Text.Trim & "')"
            End If

            lblTemp = CType(e.Item.FindControl("lblAmount"), Label)
            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblTotalAmount"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            oCustomClass.ListData = oController.GetPaymentRequestPaging(oCustomClass).ListData

            If oCustomClass Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True



        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "approve"
                If CheckFeature(Me.Loginid, Me.FormID, "APV", Me.AppId) Then
                    pnlDetail.Visible = True
                    pnlDatagrid.Visible = False
                    pnlList.Visible = False

                    m_TotalRequestAmount = 0
                    m_TotalTransferAmount = 0

                    lblMessage.Visible = False

                    Dim hyRequestNo As HyperLink
                    hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)

                    Me.RequestNo = hyRequestNo.Text.Trim
                    doBindView(Me.RequestNo)
                End If
        End Select

    End Sub

    Private Sub doBindView(ByVal strRequestNo As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try

            With oCustomClass
                .BranchId = oBranch.BranchID.Trim
                .RequestNo = strRequestNo
                .strConnection = GetConnectionString
            End With

            oCustomClass = oController.GetPaymentRequestHeaderAndDetail(oCustomClass)

            If oCustomClass Is Nothing Then
                DisplayError("No record found. Search conditions: " & Me.SearchBy)
                Exit Sub
            End If

            With oCustomClass
                '*** HEADER
                lblRequestNo.Text = .RequestNo
                lblBranchRequest.Text = .BranchName
                lblBankAccount.Text = .BankAccount
                lblDescriptionView.Text = .Description
                lblDate.Text = .RequestDate.ToString("dd/MMyyyy")
                lblAmountView.Text = FormatNumber(.TotalAmount, 2)

                '*** DETAIL
                Datagrid1.DataSource = .ListData
                Datagrid1.DataBind()
                Datagrid1.Visible = True

                '*** FOOTER
                lblTransferRefVoucherNo.Text = .TransferRefVoucherNo
                lblTransferRefNo.Text = .TransferReferenceNo
                lblTransferDate.Text = .TransferDate.ToString("dd/MM/yyyy")
                lblTransferAmount.Text = FormatNumber(.TransferAmount, 2)
                lblStatusView.Text = .Status
                lblRequestByView.Text = .RequestBy
                lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")


            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlDetail.Visible = False
        pnlDatagrid.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub imgApp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonApp.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = Me.RequestNo
        End With

        Try
            oController.saveApprovalPaymentRequest(oCustomClass, "REQ")
            pnlDetail.Visible = False
            pnlDatagrid.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Private Sub imgDecline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDecline.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = Me.RequestNo
        End With

        Try
            oController.saveApprovalPaymentRequest(oCustomClass, "CAN")
            pnlDetail.Visible = False
            pnlDatagrid.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

End Class