﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Webform.CashMgt

Public Class PrintFormKuning
    Inherits Webform.WebBased


#Region "Property"
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private recordCount As Int64 = 1
    Public Property RequestNo As String
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private oController As New Controller.PaymentRequestController
    Private oCustomClass As New Parameter.PaymentRequest
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    Protected WithEvents pnlsearch As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents pnlDatagrid As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents Buttonsearch As Global.System.Web.UI.WebControls.Button
    Protected WithEvents BtnPrint As Global.System.Web.UI.WebControls.Button

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PAYREQUESTYLOW"
            If Request.QueryString("strFileLocation") <> "" Then
                Response.Write(String.Format("<script language = javascript>{0} var x = screen.width;{0} var y = screen.height;{0} window.open('../../XML/{1}.pdf','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') {0} </script>", vbCrLf, Request.QueryString("strFileLocation")))
            End If

            pnlsearch.Visible = True
            pnlDatagrid.Visible = False
        End If
    End Sub

    Public Function CetakBuktiBayar(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}
            oDataTable = CustomClass.ListAPAppr
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("RequestNo")
            Next
            CustomClass.hasil = 1
            Return CustomClass

            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, "spAPDisbAppMultipleUpdate_22", params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try


    End Function
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim loopitem As Int16
        For loopitem = 0 To CType(DtgAgree.Items.Count - 1, Int16)
            Dim Chk = CType(DtgAgree.Items(loopitem).FindControl("cbCheck"), CheckBox)
            If Chk.Enabled Then
                Chk.Checked = CType(sender, CheckBox).Checked
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Buttonsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String
        strFilterBy = ""
        Me.BranchID = cboParent.SelectedItem.Value.Trim
        Me.SearchBy = String.Format(" PR.branchid = '{0}' AND PR.IsReconcile <> 1 AND PR.Status in ('REQ','CAB', 'HQ','ACC') ", Me.BranchID.Replace("'", ""))
        strFilterBy = String.Format("Branch = {0}", Me.BranchID)

        If cboSearchBy.SelectedItem.Value <> "0" Then
            Me.SearchBy = String.Format("{0} and {1} Like '%{2}%' ", Me.SearchBy, cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim)
            strFilterBy = strFilterBy & ", " & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim

        End If

        If txtDate.Text.Trim.Length > 0 Then
            Me.SearchBy = String.Format("{0} and RequestDate = '{1}'", Me.SearchBy, ConvertDate2(txtDate.Text.Trim))
            strFilterBy = strFilterBy & ", RequestDate = " & txtDate.Text.Trim
        End If

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlsearch.Visible = True
        DoBind()
    End Sub

    Sub DoBind(Optional isFrNav As Boolean = False)
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            Dim listData = oController.GetPaymentRequestPaging(oCustomClass).ListData

            If listData Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            recordCount = oCustomClass.TotalRecord
            listData.DefaultView.Sort = Me.SortBy
            DtgAgree.DataSource = listData.DefaultView

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try

            'If (isFrNav = False) Then
            '    GridNavigator.Initialize(recordCount, pageSize)
            'End If

            pnlsearch.Visible = True
            pnlDatagrid.Visible = True


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.Source + " " + ex.StackTrace, True)
        End Try
    End Sub
#Region "BindGridEntity"
    Sub BindGridEntity()
        Dim dtEntity As New DataTable
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer = 0
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim custom As New Parameter.APDisbApp
        Dim oCustomClass As New Parameter.APDisbApp

        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.APDisbApp
                custom.RequestNo = TempDataTable.Rows(index).Item("lnkPVNo").ToString.Trim
                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            '.CurrentPage = currentPage
            '.PageSize = pageSize
            '.SortBy = Me.Sort
        End With

        oCustomClass = CetakBuktiBayar(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listDataReport
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        DtgAgree.DataSource = dtEntity.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        'PagingFooter()

        pnlDatagrid.Visible = True
        pnlsearch.Visible = True

        For intLoopGrid = 0 To DtgAgree.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(DtgAgree.Items(intLoopGrid).FindControl("lblxx"), CheckBox)
            Dim RequestNo As String = CType(DtgAgree.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim

            Me.RequestNo = RequestNo

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub

    Private Function PredicateFunction(obj As Parameter.APDisbApp) As Boolean
        Return obj.ApplicationID = obj.RequestNo = Me.RequestNo
    End Function
#End Region

    Function createDataTable() As DataTable

        Dim dt As DataTable = New DataTable

        dt.Columns.Add("NoKontrak", GetType(String))
        dt.Columns.Add("AtasNama", GetType(String))
        dt.Columns.Add("AssetName", GetType(String))
        dt.Columns.Add("BayarKepada", GetType(String))
        dt.Columns.Add("Bank", GetType(String))
        dt.Columns.Add("NoRekening", GetType(String))
        dt.Columns.Add("Jumlah", GetType(Decimal))


        Return dt

    End Function

    Sub BindDataPPK()
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim Application As New Parameter.APDisbApp
        Dim chck As Integer = 0
        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("RequestNo", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                Application = New Parameter.APDisbApp

                Application.RequestNo = TempDataTable.Rows(index).Item("RequestNo").ToString.Trim

                DataList.Add(Application)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To DtgAgree.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(DtgAgree.Items(intLoopGrid).FindControl("ItemCheckBox"), CheckBox)
            Dim RequestNo As String = CType(DtgAgree.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim
            chck += 1


            Me.RequestNo = RequestNo

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If
            If (chck <= 0) Then
                ShowMessage(lblMessage, "Silahkan Pilih Voucher", True)
                Exit Sub
            End If

            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("RequestNo") = RequestNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPPK()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("RequestNo", GetType(String)))
        End With
    End Sub
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer = 0
            Dim hasil As Integer = 0
            Dim cmdwhere As String = ""
            Dim oCustomClass As New Parameter.APDisbApp

            If TempDataTable Is Nothing Then
                BindDataPPK()
            Else
                If TempDataTable.Rows.Count = 0 Then
                    BindDataPPK()
                End If
            End If

            With oDataTable
                .Columns.Add(New DataColumn("RequestNo", GetType(String)))
            End With

            If TempDataTable.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Harap periksa item", True)
                Exit Sub
            End If

            For index = 0 To TempDataTable.Rows.Count - 1
                oRow = oDataTable.NewRow
                oRow("RequestNo") = TempDataTable.Rows(index).Item("RequestNo").ToString.Trim
                oDataTable.Rows.Add(oRow)
            Next

            Me.RequestNo = TempDataTable.Rows(0).Item("RequestNo").ToString.Trim

            With oCustomClass
                .strConnection = GetConnectionString()
                .ListAPAppr = oDataTable
            End With
            oCustomClass = CetakBuktiBayar(oCustomClass)

            Dim cookie As HttpCookie = Request.Cookies("RptBuktiPembayaranSPPL")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("BusinessDate") = Me.BusinessDate.ToString("dd/MM/yyyy")
                cookie.Values("RequestNo") = Me.RequestNo
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptBuktiPembayaranSPPL")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("BusinessDate", Me.BusinessDate.ToString("dd/MM/yyyy"))
                cookieNew.Values.Add("RequestNo", Me.RequestNo)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("CetakFormKuning.aspx")
        End If


    End Sub


End Class