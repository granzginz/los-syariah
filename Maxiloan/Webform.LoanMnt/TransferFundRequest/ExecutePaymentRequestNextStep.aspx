﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExecutePaymentRequestNextStep.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ExecutePaymentRequestNextStep" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ExecutePaymentRequestNextStep</title>
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script type="text/javascript">
        function selectAllCheckbox(val) {
            $('#DtgExecute input:checkbox').prop('checked', $(val).is(':checked'));
        }
        function Calculate() {
            var grid = document.getElementById('DtgExecute');
            var rowCount = grid.rows.length - 1;
            var total = 0;

            for (i = 0; i < rowCount; i++) {
                var amountused = document.getElementById('DtgExecute_txtAmountTrans_' + i).value;
                total = total + parseInt(amountused.replace(/\s*,\s*/g, ''));
            }
            $('#DtgExecute_lblsumUsed').html(number_format(total));
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>

    <script type="text/javascript">
        var submit = 0;
        function CheckDouble() {
            if (++submit > 1) {
                alert('This sometimes takes a few seconds - please be patient.');
                return false;
            }
        }
 </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                EKSEKUSI PERMINTAAN PEMBAYARAN
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Cabang Request</label>
                <asp:Label ID="LblBranchRequestID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Request</label>
                <asp:Label ID="LblRequestType" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    No Request</label>
                <asp:Label ID="LblRequestNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Request</label>
                <asp:Label ID="LblRequestDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Rekening Bank</label>
                <asp:Label ID="LblBankAccount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Transfer</label>
                <asp:Label ID="LblTransferDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="form_left">
                <label>
                    Keterangan</label>
                <asp:Label ID="LblDescription" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label_req">
                    No Bukti</label>
                <asp:TextBox ID="txtReferenceNo" runat="server" Width="136px"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL TRANSAKSI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgExecute" runat="server" Width="95%" CssClass="grid_general"
                    BorderStyle="None" BorderWidth="0" DataKeyField="RequestNo" AutoGenerateColumns="False"
                    AllowSorting="True" ShowFooter="true"  FooterStyle-CssClass="item_grid">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="selectAllCheckbox(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SEQNO">
                            <ItemTemplate>
                                <asp:Label ID="lblSeqNO" runat="server" Text='<%#Container.DataItem("SequenceNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DEPARTEMENT">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDepartement" runat="server" Text='<%#Container.DataItem("DepartementName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TRANSAKSI">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                            <ItemTemplate>
                                <asp:Label ID="lblRequestNo" runat="server" Text='<%#Container.DataItem("TRANSACTIONDESC")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KETERANGAN">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <FooterStyle CssClass="item_grid_right" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <FooterTemplate>
                                TOTAL
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="REQUEST" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle CssClass="item_grid_right" />
                            <ItemTemplate>
                                <asp:Label ID="lblTTotalAmount" runat="server" Text='<%# FormatNumber(Container.DataItem("RequestAmount"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                 <asp:Label ID="lblsumTTotalAmount" runat="server" Visible="True" Text='0.0'></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TRANSFER" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle CssClass="item_grid_right" />
                            <ItemTemplate>
                                <asp:Label ID="lblAmountTransfer" runat="server" Text='<%# FormatNumber(Container.DataItem("AmountTransfer"),2 )%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                 <asp:Label ID="lblsumTransfer" runat="server" Visible="True" Text='0.0'></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PENGGUNAAN" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                            <FooterStyle CssClass="item_grid_right" />
                            <ItemTemplate>
<%--                                <asp:TextBox runat="server" ID="txtAmountTrans" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);"
                                    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign reguler_text"
                                    Text='<%# FormatNumber(Container.DataItem("RequestAmount"),2 )%>' ></asp:TextBox>--%>
                                <asp:TextBox runat="server" ID="txtAmountTrans" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);"
                                    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign reguler_text"
                                    Text='<%# FormatNumber(Container.DataItem("AmountUsed"),2 )%>' ></asp:TextBox>
                                <asp:Label runat="server" ID="lblRequire" CssClass="validator_general" Visible="false">*</asp:Label>

                                <asp:Label ID="txtAmountUsed" Visible="false" runat="server" Text='<%# Container.DataItem("AmountUsed") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                 <asp:Label ID="lblsumUsed" runat="server" Visible="True" ></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblTransferNotes" runat="server" Text='<%# Container.DataItem("TransferNotes") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonExecute" runat="server" Text="Execute" CssClass="small button blue" OnClientClick="return CheckDouble();">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
