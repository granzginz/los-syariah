﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region
Public Class rptPaymentRequestViewer01
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private oCustomClass As New Parameter.PaymentRequest
    Private m_coll As New Controller.PaymentRequestController
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(viewstate("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
    End Sub
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataTable
        Dim objReport As rptPaymentRequest01 = New rptPaymentRequest01
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.BranchId = Me.Branch_ID
        oCustomClass.SortBy = ""
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_coll.GetPaymentRequestDataset(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If
        objReport.SetDataSource(oData)
        CrystalReportViewer1.ReportSource = objReport

        '========================================================
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "ppdro.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        Response.Redirect("PaymentRequestInquiry.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "ppdro")

    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PaymentRequestInquiry")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.Branch_ID = cookie.Values("BranchID")
    End Sub
#End Region
End Class