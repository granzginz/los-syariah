﻿<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="PaymentRequestMulti.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentRequestMulti" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucOtherBankAccount" Src="../../Webform.UserController/ucOtherBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountOther" Src="../../Webform.UserController/ucBankAccountOther.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Payment Request</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">

      
        function DeleteConfirm() {
            if (confirm("Apakah Yakin mau hapus data ini ? ")) {
                return true;
            }
            else {
                return false;
            }
        }
        //function click() {
        //    if (event.button == 2) {
        //        alert('Anda tidak Berhak');
        //    }
        //}

        //document.onmousedown = click

        function klikpotong(n, x) {
            var amount = $('#' + x).val();
            var a = amount.replace(/\s*,\s*/g, '');

            if ($('#' + n).is(":checked")) {
                amount = parseFloat(a) * -1;
            } else {
                amount = Math.abs(parseFloat(a));
            }

            $('#' + x).val(number_format(amount));
            total();
        }
        function total(e) {
            var grid = document.getElementById('DtgPRList');
            var rowCount = grid.rows.length - 2;
            var total = 0;

            for (i = 0; i < rowCount; i++) {
                var Amount = document.getElementById('DtgPRList_txtAmountTrans_' + i).value.replace(/,/gi, "");

                total = total + parseFloat(Amount);
            }

            $('#DtgPRList_txtTotTransAmount').val(number_format(total));
            $('#hdTotal').val(number_format(total));
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function DoPostBack() {
            __doPostBack('Jlookup', '');
            __doPostBack('btnJLookup', '');

        }

        function selectAllCheckbox(val) {
            $('#DtgPRList input:checkbox').prop('checked', $(val).is(':checked'));
        }
        function OpenLookup() {
            if ($('#cboDepartement').val() == "0") {
                alert("Departement harap diisi")
                return true;
            } else {
                return false;
            }
        }

        function validation() {
            if ($('#chk').val() == "true") {
                document.getElementById("chk1").checked = true;
                document.getElementById("chk").checked = false;
            } else {
                document.getElementById("chk1").checked = false;
                document.getElementById("chk").checked = true;
            }
            document.getElementById("txtBankAccountIDLain").value = "";
            document.getElementById("txtBankAccountNameLain").value = "";
        }
        function validation1() {
            if ($('#chk1').val() == "true") {
                document.getElementById("chk1").checked = false;
                document.getElementById("chk").checked = true;
            } else {
                document.getElementById("chk1").checked = true;
                document.getElementById("chk").checked = false;
            }
            document.getElementById("txtBankAccountIDCabang").value = "";
            document.getElementById("txtBankAccountNameCabang").value = "";
        }



    </script>

     <script type="text/javascript">
         var submit = 0;
         function CheckDouble() {
             if (++submit > 1) {
                 alert('This sometimes takes a few seconds - please be patient.');
                 return false;
             }
         }
 </script>


</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" Font-Bold="true"  Font-Size="Medium" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <%-- <asp:Label ID="lblSuccess" runat="server" Visible="false" Font-Bold="true"  Font-Size="Medium" style="background-color:#00ff00" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>--%>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PERMINTAAN PEMBAYARAN
            </h3>
        </div>
    </div>

    <asp:Panel ID="PnlHeader" runat="server"> 
        <div class="form_box">
            <div class="form_single" style="background-color:#ffd800">
                Info: <br />
                Jika No Rekening sudah pernah di input tekan tombol (...) dan Jika No Rekening baru harap beri ceklis <br />
                Pada Rekening Cabang atau Rekening Lain..
            </div>
        </div>
        <div class="form_box_header">
            <div>
                 <div class="form_single">
                     <label> Transfer ke rekening Tujuan</label>
                 </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label runat="server" id="lblBankAccountID">
                    Rekening Cabang</label><label runat="server" id="lblBankAccountIDNotValidate">
                    Rekening Cabang</label>
                <asp:CheckBox ID="chk" runat="server" OnClick="validation();" />
                <asp:TextBox ID="txtBankAccountIDCabang" runat="server" CssClass="form_box_hide"></asp:TextBox>
                <asp:TextBox ID="txtBankAccountNameCabang" runat="server"  Width="245px"></asp:TextBox>
                   <button class="small buttongo blue" id="btn1"
                            onclick ="validation();OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookupBankAccountCabang.aspx?BankAccountID=" & txtBankAccountIDCabang.ClientID & "&BankAccountName=" & txtBankAccountNameCabang.ClientID & "&AccountName=" & TxtNamaRekening.ClientID & "&AccountNo=" & TxtNoRekening.ClientID & "&BankId=" & cboBankName.ClientID) %>','Kode Bank Account','<%= jlookupContent.ClientID %>');return false;validation();">...</button> 
                <asp:Button runat="server" ID="btnReset1" CausesValidation="false" Text="Reset" CssClass="small buttongo gray" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label runat="server" id="Label1">
                    Rekening Lain</label> <label runat="server" id="Label2">
                    Rekening Lain</label>
                <asp:CheckBox ID="chk1" runat="server" OnClick="validation1();" /></asp:CheckBox>
                <asp:TextBox ID="txtBankAccountIDLain" runat="server" CssClass="form_box_hide"></asp:TextBox>
                <asp:TextBox ID="txtBankAccountNameLain" runat="server" Width="245px"></asp:TextBox>
                   <button class="small buttongo blue" id="btn2"
                            onclick ="validation1();OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookupBankAccountOther.aspx?BankAccountID=" & txtBankAccountIDLain.ClientID & "&BankAccountName=" & txtBankAccountNameLain.ClientID & "&AccountName=" & TxtNamaRekening.ClientID & "&AccountNo=" & TxtNoRekening.ClientID & "&BankId=" & cboBankName.ClientID) %>','Kode Bank Account','<%= jlookupContent.ClientID %>');return false;">...</button>  
                <asp:Button runat="server" ID="btnReset2" CausesValidation="false" Text="Reset" CssClass="small buttongo gray" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server">
         <div class="form_box_header">
            <div>
                 <div class="form_single">
                     <label>Detail</label>
                 </div>
            </div>
        </div>
        <div class="form_box" runat="server">
            <div class="form_single">
                <label>
                  Transfer Rekening Cabang</label>
                 <asp:CheckBox ID="CheckBox1" runat="server" />
                <asp:TextBox ID="txtBankAccountNameCabang_" runat="server" Width="245px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box" runat="server">
            <div class="form_single">
                <label>
                  Transfer Rekening Lain</label>
                <asp:CheckBox ID="CheckBox2" runat="server" /></asp:CheckBox>
                <asp:TextBox ID="txtBankAccountNameLain_" runat="server" Width="245px"></asp:TextBox>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
          <div class="form_box_header">
            <div>
                 <div class="form_single">
                     <label>Detail</label>
                 </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Bank</label>
                <asp:DropDownList ID="cboBankName" runat="server" Width="250px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                    ControlToValidate="cboBankName" ErrorMessage="Harap pilih Bank" InitialValue="0"
                    CssClass="validator_general" Visible="True" Enabled="True"></asp:RequiredFieldValidator>
                 <asp:TextBox ID="txtSandiBank" runat="server" Visible="False" Enabled="false"
                    Width="300px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box" runat="server">
            <div class="form_single">
                <label>
                   Nama Rekening</label>
                <asp:TextBox ID="TxtNamaRekening" runat="server" Width="300px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box" runat="server">
            <div class="form_single">
                <label>
                   Nomor Rekening</label>
                <asp:TextBox ID="TxtNoRekening" runat="server" Width="300px"
                onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                onfocus="this.value=resetNumber(this.value);" OnChange="javascript:void(0)" autocomplete="off"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNum" runat="server" ControlToValidate="TxtNoRekening"
                    ErrorMessage="Harap Isi Angka" Visible="True" Enabled="True" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rvfNum" runat="server" ControlToValidate="TxtNoRekening"
                    ErrorMessage="Harap Isi Angka" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                    Visible="True" Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="rgValNum" runat="server" font-name="Verdana" Font-Size="11px"
                    Type="String" ControlToValidate="TxtNoRekening" MinimumValue="0" MaximumValue="9999"
                    Visible="True" Display="Dynamic"></asp:RangeValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nomor Memo</label>
                <asp:label ID="NoMemo" runat="server">001/PD/201701/</asp:label> 
                <asp:TextBox ID="txtMemo" runat="server" CssClass="small_text" placeholder="XXX/99999" MaxLength="9"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqNomorMemo" runat="server" Display="Dynamic"
                    ControlToValidate="txtMemo" ErrorMessage="Harap diisi Nomor Memo" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtMemo_" runat="server" Visible="false"  Enabled="false"  CssClass="small_text" placeholder="XXX/99999" MaxLength="9"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jumlah</label>
                <uc1:ucnumberformat runat="server" id="txtAmount" ></uc1:ucnumberformat>
                <asp:textbox ID="txtAmount_" runat="server" Visible="False" Enabled="false" Width="300px"></asp:textbox> 
                <div style="font-size:8px">Masukan total jumlah yang akan di ajukan dalam satu memo</div>
            </div>
        </div>
         <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Tanggal Transaksi
                </label>
                <uc2:ucDateCE id="txtTanggalTransaksi" runat="server"></uc2:ucDateCE>
                <asp:TextBox ID="txtTanggalTransaksi_" runat="server" Visible="False" Enabled="false"
                    Width="300px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Departemen</label>
                <asp:DropDownList ID="cboDepartement" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ControlToValidate="cboDepartement" ErrorMessage="Harap pilih Departemen" InitialValue="0"
                    CssClass="validator_general" Visible="True" Enabled="True"></asp:RequiredFieldValidator>
                <asp:textbox ID="txtDepartement_" runat="server" Visible="False" Enabled="false"  Width="300px"></asp:textbox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Keterangan</label>
                <asp:TextBox ID="txtDescription" runat="server" Width="448px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    ControlToValidate="txtDescription" ErrorMessage="Harap diisi Keterangan" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:textbox ID="txtDescription_" runat="server" Visible="False" Enabled="false" Width="300px"></asp:textbox>
            </div>
        </div>
        <asp:Panel ID="pnlLookup" runat="server" Visible="true">
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Detail Transaksi</label>
                <asp:HiddenField runat="server" ID="vsPay" />
                <asp:HiddenField runat="server" ID="vsDesc" />
                <asp:HiddenField runat="server" ID="vsJumlah" />
                <asp:HiddenField runat="server" ID="isPaymentRequest" />
                <asp:TextBox runat="server" ID="txtJLookup" CssClass="regular_text" Enabled="false"></asp:TextBox>
<%--                <button class="small buttongo blue" onclick="OpenLookup();return false">
                    ...</button>--%>
                <button class="small buttongo blue" id="btnLookup" onclick="OpenJLookup('<%=ResolveClientUrl("~/Webform.LoanMnt/GeneralLookup/ucLookupTransMnt.aspx?vspay=vsPay&vsjumlah=vsJumlah&vsdesc=vsDesc&isPaymentRequest=1") %>' ,'DAFTAR TRANSAKSI','<%= jlookupContent.ClientID %>', 'DoPostBack');return false;">
                    ...</button>
                <asp:Button runat="server" ID="Jlookup" Style="display: none" />
                <asp:RequiredFieldValidator ID="RFVtxtJLookup" runat="server" ErrorMessage="Harap Pilih Traksaksi!"
                    ControlToValidate="txtJLookup" Display="Dynamic" InitialValue="Belum dipilih!"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPRList" runat="server" Visible="true" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" ShowFooter="True">
                        <HeaderStyle CssClass="th" />
                        <FooterStyle CssClass="item_grid" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:selectAllCheckbox(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>           
                            <asp:TemplateColumn HeaderText="DEPARTMEN">                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartmentID" runat="server" Visible="False" Text='<%#Container.DataItem("DepartmentID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                 
                                <asp:TemplateColumn HeaderText="TRANSAKSI">                                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentAllocID" runat="server" Visible="False" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                                </asp:Label>
                                                <asp:Label ID="lblPaymentAllocDesc" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KETERANGAN">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtKeterangan" CssClass="long_text"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtKeterangan" Display="Dynamic"
                                        ErrorMessage="Input ini harus diisi!" ControlToValidate="txtKeterangan" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtAmountTrans" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);total(this);"
                                        onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                        onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign long_text">0</asp:TextBox>
                                    <%--<asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 999999999999999"
                                        ControlToValidate="txtAmountTrans" MaximumValue="999999999999999" MinimumValue="-9999999"
                                        Type="Currency" CssClass="validator_general"></asp:RangeValidator>--%>
                                    <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                        ControlToValidate="txtAmountTrans" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:TextBox runat="server" ID="txtTotTransAmount" CssClass="numberAlign long_text"
                                        Enabled="false">0</asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Kredit">
                                <ItemTemplate>
                                     <asp:CheckBox ID="chkIsPotong" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:Panel ID="pnlbtn" runat="server">
                        <asp:Button ID="ButtonDelete" runat="server" CausesValidation="False" Text="Delete"
                            CssClass="small button red" Height="35" Width="70"></asp:Button>
                        <button class="small buttongo blue" style="height:35px;" id="Lookup" onclick="OpenJLookup('<%=ResolveClientUrl("~/Webform.LoanMnt/GeneralLookup/ucLookupTransMnt.aspx?vspay=vsPay&vsjumlah=vsJumlah&vsdesc=vsDesc") %>' ,'DAFTAR TRANSAKSI','<%= jlookupContent.ClientID %>', 'DoPostBack');return false;">
                             Add COA</button>
                        <asp:Button runat="server" ID="btnJLookup" Style="display: none" />
                        </asp:Panel>
                        <asp:Panel ID="pnlRevisi" runat="server">
                            <asp:Button ID="ButtonRevisi" runat="server" CausesValidation="False" Text="Revisi"
                                CssClass="small button green" Visible="false"></asp:Button>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonNext" runat="server" CausesValidation="False" Text="Next" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlApprovalReq" runat="server" Visible="False">
        <div class="form_button">
            <asp:Button ID="ButtonSaveApproval" runat="server" Text="Save" CssClass="small button blue" OnClientClick="return CheckDouble();">
            </asp:Button>
            <asp:Button ID="ButtonCancelApproval" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
