﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ReconcilePaymentRequest
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable    
    Protected WithEvents oBankAccount As ucBankAccountReconcile
    Private Const SP_NAME_PAGING As String = "spPaymentRequestReimburseSelect"
    Dim oController As New GeneralPagingController
    Private Const FILE_NAME_NEXT As String = "ReconcilePaymentRequestNextStep.Aspx"

#End Region
#Region "InitialDefaultPanel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub
#End Region
#Region "Property"

    Private Property BankAccountID() As String
        Get
            Return (CType(viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property

    Private Property StartSelectionDate() As String
        Get
            Return (CType(viewstate("StartSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartSelectionDate") = Value
        End Set
    End Property

    Private Property BulkofRequest() As String
        Get
            Return (CType(viewstate("BulkofRequest"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BulkofRequest") = Value
        End Set
    End Property

#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ButtonNext.Enabled = False

        Me.FormID = "RECONPAYREQ"
        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If IsSingleBranch() And Me.IsHoBranch = False Then

                    InitialDefaultPanel()
                    oSearchBy.ListData = "paymentrequest.RequestNo,Request No-paymentrequest.Description,Description-paymentrequest.ReferenceNo,No Memo"
                    oSearchBy.BindData()
                Else
                    KickOutForMultipleBranch()
                End If
            End If

            If Request("thingstodo") = "1" Then
                PnlGrid.Visible = True
                Me.SearchBy = " And paymentrequest.BranchID in( '" & Replace(Me.sesBranchId, "'", "") & "') "
                Me.SearchBy = Me.SearchBy & " And IsReconcile=0 and Status='TRF' "
                BindGrid(Me.SearchBy, Me.SortBy)
            End If

        End If
    End Sub
#End Region
#Region "CheckStatus"
    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusReimburse As CheckBox

        For loopitem = 0 To CType(DtgReimburse.Items.Count - 1, Int16)
            ChkStatusReimburse = New CheckBox
            ChkStatusReimburse = CType(DtgReimburse.Items(loopitem).FindControl("CheckBoxReimburse"), CheckBox)

            If ChkStatusReimburse.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    ChkStatusReimburse.Checked = True
                Else
                    ChkStatusReimburse.Checked = False
                End If
            Else
                ChkStatusReimburse.Checked = False
            End If
        Next
    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_PAYMENT_REQUEST_REIMBURSE)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = Me.SearchBy & Me.BulkofRequest
            cookie.Values("BankAccountID") = Me.BankAccountID
            cookie.Values("BankAccountName") = oBankAccount.BankAccountName
            cookie.Values("RequestDate") = txtRequestDate.Text
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_PAYMENT_REQUEST_REIMBURSE)
            cookieNew.Values("SearchBy") = Me.SearchBy
            cookieNew.Values("BankAccountID") = Me.BankAccountID
            cookieNew.Values("BankAccountName") = oBankAccount.BankAccountName
            cookieNew.Values("RequestDate") = txtRequestDate.Text
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region
#Region "Ketika Tombol Next Dipijit "
    Private Sub ButtonNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim LoopNext As Int32
            Dim TempRequestNo As String

            For LoopNext = 0 To DtgReimburse.Items.Count - 1
                Dim CurrentCheckBox As New CheckBox
                Dim strRequestNo As New Label
                Dim koma As String
                CurrentCheckBox = CType(DtgReimburse.Items(LoopNext).FindControl("CheckBoxReimburse"), CheckBox)
                strRequestNo = CType(DtgReimburse.Items(LoopNext).FindControl("LblRequestNo"), Label)

                If CurrentCheckBox.Checked = True Then
                    If TempRequestNo = "" Then koma = "" Else koma = ","
                    TempRequestNo = TempRequestNo & koma & strRequestNo.Text.Trim


                End If
            Next

            If TempRequestNo <> "" Then
                NextProcess(TempRequestNo)
            Else
                Exit Sub
            End If
        End If
    End Sub
#End Region
#Region "Proses ke langkah selanjutnya"
    Public Sub NextProcess(ByVal BulkRequest As String)
        Dim RequestThatChecked As String
        RequestThatChecked = Replace(BulkRequest, ",", "','").Trim
        RequestThatChecked = " And RequestNo in ('" & RequestThatChecked & "')".Trim

        Me.BulkofRequest = RequestThatChecked
        SendCookies()
        Dim paramBankAccout As String
        Dim paramRequestDate As String

        Server.Transfer(FILE_NAME_NEXT)
    End Sub
#End Region
#Region "Ketika Tombol Search dipijit"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            PnlGrid.Visible = True
            Me.SearchBy = " And paymentrequest.BranchID in( '" & Replace(Me.sesBranchId, "'", "") & "') "
            Me.SearchBy = Me.SearchBy & " And IsReconcile=0 and Status='TRF' "

            If oSearchBy.Text.Trim <> "" Then
                If oSearchBy.Text.IndexOf("%") >= 0 Then
                    Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
                Else
                    Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
                End If
            End If

            If txtRequestDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " And RequestDate = Convert(Datetime,'" & Replace(txtRequestDate.Text, "'", "") & "',103) "
            End If

            If cboRequestType.SelectedValue <> "0" Then
                Me.SearchBy = Me.SearchBy & " And RequestType = '" & cboRequestType.SelectedValue & "' "

            End If

            'Me.BankAccountID = oBankAccount.BankAccountID
            BindGrid(Me.SearchBy, Me.SortBy)
        End If
    End Sub

#End Region
#Region "BindGrid"
    Public Sub BindGrid(ByVal cmdWhere As String, ByVal cmdsortby As String)
        Dim oEntities As New Parameter.GeneralPaging

        If Me.SortBy = "" Then
            Me.SortBy = " RequestNo "
        End If

        With oEntities
            .SpName = SP_NAME_PAGING
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)
        Dim dt As DataSet
        If Not oEntities Is Nothing Then
            dt = oEntities.ListDataReport
        End If
        DtgReimburse.DataSource = dt
        DtgReimburse.DataBind()
    End Sub
#End Region
#Region "SortGrid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "DataBound"
    'lblGridRequestNo
    'DtgReimburse
    Private Sub DtgReimburse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgReimburse.ItemCommand

        If e.CommandName = "Reconcile" Then
            Dim lblGridRequestNo As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblGridRequestNo"), Label)
            Dim lblBankAccountID As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblBankAccountID"), Label)
            Dim lblBankAccountName As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblBankAccountName"), Label)
            Dim lblRequestType As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblRequestType"), Label)
            Dim lblRequestDate As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblRequestDate"), Label)
            Dim lblTransferDate As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblTransferDate"), Label)
            Dim LblDescription As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("LblDescription"), Label)
            Dim lblDepartementName As Label = CType(DtgReimburse.Items(e.Item.ItemIndex).FindControl("lblDepartementName"), Label)

            Dim cookieNew As New HttpCookie("ReconcilePaymentRequest")
            cookieNew.Values.Add("BranchRequest", Replace(Me.sesBranchId, "'", ""))
            cookieNew.Values.Add("RequestNo", lblGridRequestNo.Text.Trim)
            cookieNew.Values.Add("TransferDate", lblTransferDate.Text.Trim)
            cookieNew.Values.Add("RequestDate", lblRequestDate.Text.Trim)
            cookieNew.Values.Add("RequestType", lblRequestType.Text.Trim)
            cookieNew.Values.Add("Description", LblDescription.Text.Trim)
            cookieNew.Values.Add("BankAccountName", lblBankAccountName.Text.Trim)
            cookieNew.Values.Add("BankAccountID", lblBankAccountID.Text.Trim)
            cookieNew.Values.Add("DepartementName", lblDepartementName.Text.Trim)

            Response.AppendCookie(cookieNew)
            Response.Redirect("ReconcilePaymentRequestNextStepVersion2.aspx?RequestNo='" & lblGridRequestNo.Text & "'&BranchID='" & Replace(Me.sesBranchId, "'", "") & "'")

        End If

    End Sub
#End Region


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("ReconcilePaymentRequest.aspx")
    End Sub

End Class