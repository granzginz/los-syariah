﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class PaymentRequestView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"
    Private Property RequestNo() As String
        Get
            Return (CType(Viewstate("RequestNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("RequestNo") = Value
        End Set
    End Property
#End Region


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As Parameter.PaymentRequest
    Private oController As PaymentRequestController
    Private m_TotalTransferAmount As Double
    Private m_TotalRequestAmount As Double
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "PAYREQVIEW"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                m_TotalRequestAmount = 0
                m_TotalTransferAmount = 0

                lblMessage.Visible = False
                Me.RequestNo = Request.QueryString("RequestNo")

                DoBind()
            End If
        End If
    End Sub
#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub

    Private Sub DoBind()
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            lblMessage.Text = ""
            With oCustomClass
                .RequestNo = Me.RequestNo
                .strConnection = GetConnectionString
            End With

            oCustomClass = oController.GetPaymentRequestHeaderAndDetail(oCustomClass)

            If oCustomClass Is Nothing Then
                DisplayError("No record found. Search conditions: " & Me.SearchBy)
                Exit Sub
            End If

            With oCustomClass
                '*** HEADER
                lblRequestNo.Text = .RequestNo
                lblBranchRequest.Text = .BranchName
                lblBankAccount.Text = .BankAccount
                lblDescription.Text = .Description
                lblDate.Text = .RequestDate.ToString("dd/MMyyyy")
                lblAmount.Text = FormatNumber(.TotalAmount, 2)

                '*** DETAIL
                FillGrid(.ListData, DtgAgree)

                '*** FOOTER
                lblTransferRefVoucherNo.Text = .TransferRefVoucherNo
                lblTransferRefNo.Text = .TransferReferenceNo
                lblTransferDate.Text = .TransferDate.ToString("dd/MM/yyyy")
                lblTransferAmount.Text = FormatNumber(.TransferAmount, 2)
                lblStatus.Text = .Status
                lblRequestBy.Text = .RequestBy
                lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")


            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Public Sub New()
        oCustomClass = New Parameter.PaymentRequest
        oController = New Controller.PaymentRequestController
    End Sub

    Private Sub DtgAgree_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DtgAgree.SelectedIndexChanged

    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lbltemp As Label
        Dim lblTotal As Label

        If e.Item.ItemIndex >= 0 Then
            lbltemp = CType(e.Item.FindControl("lblTransferAmountDetail"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalTransferAmount += CType(lbltemp.Text, Double)
            End If
            lbltemp = CType(e.Item.FindControl("lblRequestAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalRequestAmount += CType(lbltemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotal = CType(e.Item.FindControl("lblTotalTransferAmount"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalTransferAmount.ToString, 2)
            End If
            lblTotal = CType(e.Item.FindControl("lblTotalRequestAmount"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalRequestAmount.ToString, 2)
            End If
        End If
    End Sub

    
End Class