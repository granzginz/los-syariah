﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports CrystalDecisions.Web.Design
#End Region

Public Class rptPaymentRequestViewer
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property"

    Private Property RequestNo() As String
        Get
            Return (CType(ViewState("RequestNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("RequestNo") = Value
        End Set
    End Property

    Private Property BranchID() As String
        Get
            Return (CType(ViewState("BranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'GetCookies()
            Me.RequestNo = Request.QueryString("RequestNo").ToString
            Me.BranchID = Request("BranchID").ToString
            Me.FilterBy = Request("FilterBy").ToString
            Me.SortBy = Request("SortBy").ToString

            If Me.RequestNo <> "" Then
                Me.SearchBy = "  PR.branchid = " & "'" & Me.BranchID & "' and PR.RequestNo = " & "'" & Me.RequestNo & "'"
            Else
                Me.SearchBy = "  PR.branchid = " & "'" & Me.BranchID & "'"
            End If

            BindReport()
        Catch ex As Exception

        End Try

    End Sub
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents crvKwitansi As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Constanta"

    Private oCustomClass As New Parameter.PaymentRequest
    Private m_coll As New Controller.PaymentRequestController
    Protected Property FilterBy() As String
        Get
            Return (CType(ViewState("FilterBy"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
#End Region

    Private m_oCustomClass As Parameter.PaymentRequest
    Private m_oController As Controller.PaymentRequestController

    Private m_Rpt As rptPaymentRequest
    Private m_DatasetRpt As DataSet
    '*** Parameter field
    Private m_ParamFieldDef As ParameterFieldDefinition
    Private m_ParamDiscreteValue As New ParameterDiscreteValue
    Private m_ParamValues As ParameterValues

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PaymentRequestInquiry")
        Me.SearchBy = cookie.Values("where")
        Me.SortBy = cookie.Values("sortby")
        Me.FilterBy = cookie.Values("FilterBy")
    End Sub
    Private Sub SetParamFieldValue(ByVal objReport As ReportClass, ByVal strParamFldName As String, ByVal oValue As Object)
        m_ParamFieldDef = objReport.DataDefinition.ParameterFields(strParamFldName)
        m_ParamDiscreteValue.Value = oValue
        m_ParamValues = m_ParamFieldDef.DefaultValues
        m_ParamValues.Add(m_ParamDiscreteValue)
        m_ParamFieldDef.ApplyCurrentValues(m_ParamValues)
    End Sub

    Private Sub BindReport()
        Dim odata As New DataTable
        'Dim objReport As rptPaymentRequest = New rptPaymentRequest
        Dim objReport As rptPaymentRequest2 = New rptPaymentRequest2
        m_oCustomClass = New Parameter.PaymentRequest
        m_oController = New PaymentRequestController

        With m_oCustomClass            
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        SetParamFieldValue(objReport, "CompanyId", Me.sesCompanyName)
        SetParamFieldValue(objReport, "BranchId", Me.BranchName.Trim)
        SetParamFieldValue(objReport, "ReportId", "rptPaymentInquiry")
        SetParamFieldValue(objReport, "FilterBy", Me.FilterBy)
        SetParamFieldValue(objReport, "PrintedDate", Me.BusinessDate)
        SetParamFieldValue(objReport, "PrintedBy", Me.Loginid)

        m_oCustomClass = m_oController.GetPaymentRequestDataset(m_oCustomClass)
        m_DatasetRpt = m_oCustomClass.PRDataSet
        objReport.SetDataSource(m_DatasetRpt)
        CrystalReportViewer1.ReportSource = objReport


        '========================================================
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "PPDRO.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("PaymentRequestInquiry.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "PPDRO")


    End Sub
End Class