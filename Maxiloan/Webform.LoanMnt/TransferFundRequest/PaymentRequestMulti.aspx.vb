﻿Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper



Public Class PaymentRequestMulti

    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtAmount As ucNumberFormat
    Protected WithEvents txtTanggalTransaksi As ucDateCE
    Protected WithEvents UcBranchAddress As ucBankAccountOther
#Region "Private Constanta"
    Private oController As New DataUserControlController
    Private m_controller As New PaymentRequestController
    Dim oCustomClass As New Parameter.PaymentRequest
    Dim tempTotalPRAmount As Double
    Private Const SCHEME_ID As String = "PYR"
#End Region
#Region "Property"
    Private Property Amount() As Double
        Get
            Return CDbl(txtAmount.Text)
        End Get
        Set(value As Double)
            txtAmount.Text = value
        End Set
    End Property
    Private Property Tanggal() As String
        Get
            Return txtTanggalTransaksi.Text
        End Get
        Set(ByVal value As String)
            txtTanggalTransaksi.Text = value
        End Set
    End Property
    Private Property BankAccountIDCabang() As String
        Get
            Return txtBankAccountIDCabang.Text
        End Get
        Set(ByVal value As String)
            txtBankAccountIDCabang.Text = value
        End Set
    End Property
    Private Property BankAccountIDLain() As String
        Get
            Return txtBankAccountIDCabang.Text
        End Get
        Set(value As String)
            txtBankAccountIDLain.Text = value
        End Set
    End Property
    Private Property TotPRAmount() As Double
        Get
            Return (CType(ViewState("TotPRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotPRAmount") = Value
        End Set
    End Property

    Private Property PRAmount() As Double
        Get
            Return (CType(ViewState("PRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PRAmount") = Value
        End Set
    End Property

    Private Property BankAccount() As String
        Get
            Return (CType(ViewState("BankAccount"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccount") = Value
        End Set
    End Property

    Private Property Departement() As String
        Get
            Return (CType(ViewState("Departement"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Departement") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return txtDescription.Text
        End Get
        Set(ByVal Value As String)
            txtDescription.Text = Value
        End Set
    End Property
    Private Property Memo() As String
        Get
            Return NoMemo.Text + txtMemo.Text
        End Get
        Set(value As String)
            txtMemo.Text = value
        End Set
    End Property
    Private Property PRDataTable() As DataTable
        Get
            Return (CType(ViewState("PRDataTable"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("PRDataTable") = Value
        End Set
    End Property

    Public Property IsRequired As Boolean
        Get
            Return CBool(ViewState("Validation"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Validation") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return (CType(ViewState("BankID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "PAYREQ"
        If Not Me.IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            If Request.QueryString("msg") = "ok" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            End If
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                GetCombo()
                BindBankAccount()
                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.hpsxml = "1"
                oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)
                If IsRequired = False Then
                    lblBankAccountID.Visible = False
                    lblBankAccountIDNotValidate.Visible = True
                    Label1.Visible = False
                    Label2.Visible = True
                    lblBankAccountID.Visible = True
                    lblBankAccountIDNotValidate.Visible = False
                    Label1.Visible = True
                    Label2.Visible = False
                End If
                Panel1.Visible = False

                pnlGrid.Visible = False
                txtAmount.RequiredFieldValidatorEnable = True
                txtAmount.RangeValidatorEnable = True
                txtAmount.TextCssClass = "numberAlign regular_text"
                txtTanggalTransaksi.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            End If
            NoMemo.Text = Me.sesBranchId.Replace("'", "") + "/" + "PD/" + Me.BusinessDate.ToString("yyyyMM") + "/"
        End If


    End Sub

    Private Sub InitialDefaultPanel()
        txtAmount_.Visible = False
        txtDepartement_.Visible = False
        txtTanggalTransaksi_.Visible = False
        txtDescription_.Visible = False
        ButtonRevisi.Visible = False
        txtMemo.Visible = True
        txtMemo_.Visible = False
        txtSandiBank.Visible = False
        TxtNamaRekening.Enabled = True
        TxtNoRekening.Enabled = True
        cboBankName.Enabled = True
        txtBankAccountNameCabang_.Visible = False
        txtBankAccountNameLain_.Visible = False
        txtBankAccountNameLain.Enabled = True
        txtBankAccountNameCabang.Enabled = True
        btnReset1.Visible = True
        btnReset2.Visible = True
        txtAmount.Visible = True
        cboDepartement.Visible = True
        cboBankName.Visible = True
        txtDescription.Visible = True
        txtTanggalTransaksi.Visible = True
        pnlLookup.Visible = True
        PnlHeader.Visible = True
        Panel1.Visible = False
        Panel2.Visible = True
        pnlGrid.Visible = True
        pnlApprovalReq.Visible = False
        ButtonNext.Visible = True
        ButtonDelete.Visible = True
        pnlbtn.Visible = True
        CheckBox1.Visible = False
        CheckBox2.Visible = False
        chk.Visible = True
        chk1.Visible = True
        chk.Enabled = True
        chk1.Enabled = True
        pnlRevisi.Visible = False
    End Sub
#End Region
    Public Sub BindBankAccount()
        Dim DtBankName As DataTable
        DtBankName = oController.GetBankName(GetConnectionString)
        cboBankName.DataValueField = "ID"
        cboBankName.DataTextField = "Name"
        cboBankName.DataSource = DtBankName
        cboBankName.DataBind()
        cboBankName.Items.Insert(0, "Select One")
        cboBankName.Items(0).Value = "0"
        cboBankName.SelectedIndex = 0
    End Sub
#Region "GetCombo"
    Private Sub GetCombo()
        Dim dtDepartement As New DataTable
        dtDepartement = oController.GetDepartement(GetConnectionString)
        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"

        cboDepartement.Items.Insert(1, "None")
        cboDepartement.Items(1).Value = "-"
    End Sub
#End Region
#Region "DtgPRList_ItemDataBound"
    Private Sub DtgPRList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPRList.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim lblTotPRAmount As New TextBox
        Dim lblPRAmount As TextBox
        With oCustomClass
            If e.Item.ItemIndex >= 0 Then
                lblPRAmount = CType(e.Item.FindControl("txtAmountTrans"), TextBox)
                tempTotalPRAmount += CDbl(lblPRAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then
                lblTotPRAmount = CType(e.Item.FindControl("txtTotTransAmount"), TextBox)
                lblTotPRAmount.Text = FormatNumber(tempTotalPRAmount.ToString, 2)
                Me.TotPRAmount = CDbl(lblTotPRAmount.Text.Trim)
            End If

        End With
    End Sub
#End Region
    Private Sub imgCancelApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelApproval.Click
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.hpsxml = "1"
        oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)

        Server.Transfer("PaymentRequestMulti.aspx")

    End Sub

    Private Sub imgSaveApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveApproval.Click
        Dim pstrFile As String
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim strRequestNo As String
            oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
            oCustomClass.BusinessDate = Convert.ToDateTime(ConvertDate2(txtTanggalTransaksi.Text))
            oCustomClass.ID = "PAYREQ"
            oCustomClass.strConnection = GetConnectionString()
            strRequestNo = m_controller.Get_RequestNo(oCustomClass)

            Dim oData As New DataTable
            Dim txtAmountTrans As TextBox
            Dim totalGrid As Double
            Dim lblDepartmentID As New Label
            Dim lblDepartmentName As New Label
            Dim lblPaymentAllocID As New Label
            Dim lblPaymentAllocDesc As New Label
            Dim txtKeterangan As New TextBox
            Dim chkPotong As New CheckBox

            For Each item As DataGridItem In DtgPRList.Items
                lblDepartmentID = CType(item.FindControl("lblDepartmentID"), Label)
                lblDepartmentName = CType(item.FindControl("lblDepartmentName"), Label)
                lblPaymentAllocID = CType(item.FindControl("lblPaymentAllocID"), Label)
                lblPaymentAllocDesc = CType(item.FindControl("lblPaymentAllocDesc"), Label)
                txtKeterangan = CType(item.FindControl("txtKeterangan"), TextBox)
                txtAmountTrans = CType(item.FindControl("txtAmountTrans"), TextBox)
                chkPotong = CType(item.FindControl("chkIsPotong"), CheckBox)

                totalGrid += CDbl(txtAmountTrans.Text)
            Next

            If Amount <> totalGrid Then
                ShowMessage(lblMessage, "Total Nilai harus sama dengan Total Permintaan pembayaran", True)
                Exit Sub
            End If

            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .RequestNo = strRequestNo
                .BusinessDate = Convert.ToDateTime(ConvertDate2(txtTanggalTransaksi.Text))
                .strConnection = GetConnectionString()
                .Description = Me.Description
                .Amount = Me.PRAmount
                .PRDataTable = Me.PRDataTable
                .NumPR = DtgPRList.Items.Count
                .Departement = cboDepartement.SelectedValue
                .NoMemo = Me.Memo
                If chk.Checked = True Then
                    .StatusBank = "Cabang"
                    .BankAccount = txtBankAccountIDCabang.Text
                    .NamaBank = txtBankAccountNameCabang.Text
                Else
                    .StatusBank = "Other"
                    .BankAccount = txtBankAccountIDLain.Text
                    .NamaBank = txtBankAccountNameLain.Text
                End If
                .BankId = cboBankName.SelectedValue
                .NamaRekening = TxtNamaRekening.Text
                .NoRekening = TxtNoRekening.Text
            End With

            'Request saja tidak pakai approval
            'bagian ini dipindah ke Verifikasi Pembayaran ACC (HO)
            'approval menggunakan scheme
            'Dim oEntitiesApproval As New Parameter.Approval
            'With oEntitiesApproval
            '    .BranchId = oCustomClass.BranchId
            '    .SchemeID = SCHEME_ID
            '    .RequestDate = oCustomClass.BusinessDate
            '    .ApprovalNote = txtRecommendation.Text.Trim
            '    .ApprovalValue = Me.PRAmount
            '    .UserRequest = Me.Loginid
            '    .UserApproval = cboApprovedBy.SelectedValue
            '    .AprovalType = Approval.ETransactionType.PaymentRequest_approval
            '    .Argumentasi = txtArgumentasi.Text.Trim
            'End With

            Dim ErrMsg As String
            ErrMsg = m_controller.SavePR(oCustomClass)
            If ErrMsg = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            ElseIf ErrMsg = "Double" Then
                ShowMessage(lblMessage, "Nomor Memo sudah digunakan, harap periksa kembali...!!", True)
                InitialDefaultPanel()
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                InitialDefaultPanel()
                Exit Sub
            End If

            'pStrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
            'oCustomClass.hpsxml = "1"
            'oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)

            '  Response.Redirect("rptPaymentRequestViewer.aspx?RedirectFiles=PaymentRequestMulti&RequestNo=" & strRequestNo & "&BranchID=" & Me.sesBranchId.Replace("'", "") & "")
            'Dim link As String = "../../Webform.Reports.RDLC/LoanMnt/PayReq/PayReqPrint.aspx?RequestNo=" & strRequestNo
            'Response.Write(String.Format("<script language = javascript>{0} var x = screen.width;{0} var y = screen.height;{0} window.open('{1}','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') {0} </script>", vbCrLf, link))

            'ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, True)
            Response.Redirect("PaymentRequestMulti.aspx?msg=ok")
            'InitialDefaultPanel()
            'Server.Transfer("PaymentRequestMulti.aspx")


        End If
    End Sub
    Private Sub ButtonRevisi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRevisi.Click
        InitialDefaultPanel()
    End Sub

    Private Sub imbNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        Dim oData As New DataTable
        Dim PRDataTable_ As New DataTable
        Dim txtAmountTrans As TextBox
        Dim totalGrid As Double
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim chkIsPotong As New CheckBox

        oCustomClass.strConnection = GetConnectionString()

        Dim oRow As DataRow
        If cboBankName.SelectedIndex = 0 Then
            ShowMessage(lblMessage, "Harap Pilih Bank", True)
            Exit Sub
        End If

        If chk.Checked = False AndAlso chk1.Checked = False Then
            ShowMessage(lblMessage, "Harap Centang Rekening dan kalau No Rekening Susah ada tinggal klik tombol (...)", True)
            Exit Sub
        End If

        If chk1.Checked = True Then
            If TxtNamaRekening.Text = "" Then
                ShowMessage(lblMessage, "Harap Isi Nama Rekening", True)
                Exit Sub
            End If
            If TxtNoRekening.Text = "" Then
                ShowMessage(lblMessage, "Harap Isi No Rekening", True)
                Exit Sub
            End If
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .NoMemo = Me.Memo
        End With


        Dim ErrMsg As String
        ErrMsg = m_controller.CekPR(oCustomClass)
        If ErrMsg = "" Then
            PRDataTable_.Clear()
            With PRDataTable_
                .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
                .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
                .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
                .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
                .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
                .Columns.Add(New DataColumn("txtAmountTrans", GetType(Double)))
                .Columns.Add(New DataColumn("chkIsPotong", GetType(String)))
            End With


            For Each item As DataGridItem In DtgPRList.Items
                lblDepartmentID = CType(item.FindControl("lblDepartmentID"), Label)
                lblDepartmentName = CType(item.FindControl("lblDepartmentName"), Label)
                lblPaymentAllocID = CType(item.FindControl("lblPaymentAllocID"), Label)
                lblPaymentAllocDesc = CType(item.FindControl("lblPaymentAllocDesc"), Label)
                txtKeterangan = CType(item.FindControl("txtKeterangan"), TextBox)
                txtAmountTrans = CType(item.FindControl("txtAmountTrans"), TextBox)
                chkIsPotong = CType(item.FindControl("chkIsPotong"), CheckBox)
                totalGrid += CDbl(txtAmountTrans.Text)


                '----- Add row -------'
                oRow = PRDataTable_.NewRow()
                oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
                oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
                oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
                oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
                oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
                oRow("txtAmountTrans") = CType(txtAmountTrans.Text, Double)
                oRow("chkIsPotong") = CType(chkIsPotong.Checked, String)


                PRDataTable_.Rows.Add(oRow)
            Next

            Me.PRDataTable = PRDataTable_

            lblMessage.Visible = False

            If Amount <> totalGrid Then
                ShowMessage(lblMessage, "Total Nilai harus sama dengan Total Permintaan pembayaran", True)
                Exit Sub
            End If

            Me.PRAmount = totalGrid

            txtTanggalTransaksi_.Text = Convert.ToDateTime(ConvertDate2(txtTanggalTransaksi.Text))
            txtBankAccountIDCabang.Text = txtBankAccountIDCabang.Text
            txtBankAccountNameCabang_.Text = txtBankAccountNameCabang.Text
            txtBankAccountIDLain.Text = txtBankAccountIDLain.Text
            txtBankAccountNameLain_.Text = txtBankAccountNameLain.Text
            CheckBox1.Checked = chk.Checked
            CheckBox2.Checked = chk1.Checked
            txtAmount_.Text = txtAmount.Text
            txtDepartement_.Text = cboDepartement.SelectedItem.ToString
            txtDescription_.Text = txtDescription.Text
            txtMemo_.Text = txtMemo.Text
            txtSandiBank.Text = cboBankName.SelectedItem.ToString
            TxtNamaRekening.Text = TxtNamaRekening.Text
            TxtNoRekening.Text = TxtNoRekening.Text
            btnReset1.Visible = False
            btnReset2.Visible = False

            txtBankAccountNameCabang.Enabled = False
            txtBankAccountNameCabang_.Enabled = False
            txtBankAccountNameCabang_.Visible = True
            CheckBox1.Enabled = False
            CheckBox1.Visible = True
            CheckBox2.Enabled = False
            CheckBox2.Visible = True
            txtBankAccountNameLain.Enabled = False
            txtBankAccountNameLain_.Enabled = False
            txtBankAccountNameLain_.Visible = True
            txtSandiBank.Visible = True
            cboBankName.Enabled = False
            TxtNamaRekening.Enabled = False
            TxtNoRekening.Enabled = False
            chk.Enabled = False
            chk1.Enabled = False

            txtAmount_.Visible = True
            txtTanggalTransaksi_.Visible = True
            txtDepartement_.Visible = True
            txtDescription_.Visible = True
            ButtonRevisi.Visible = True
            pnlRevisi.Visible = True
            txtMemo_.Visible = True
            txtAmount.Visible = False
            cboDepartement.Visible = False
            cboBankName.Visible = False
            txtDescription.Visible = False
            txtTanggalTransaksi.Visible = False
            pnlLookup.Visible = False
            txtMemo.Visible = False
            PnlHeader.Visible = False
            Panel1.Visible = True
            Panel2.Visible = True
            pnlGrid.Visible = True
            pnlApprovalReq.Visible = True
            ButtonNext.Visible = False
            ButtonDelete.Visible = False
            pnlbtn.Visible = False
            ScriptManager.RegisterStartupScript(DtgPRList, GetType(DataGrid), DtgPRList.ClientID, String.Format(" total(); ", DtgPRList.ClientID), True)
        ElseIf ErrMsg = "Double" Then
            ShowMessage(lblMessage, "Nomor Memo sudah digunakan, harap periksa kembali...!!", True)
            InitialDefaultPanel()
            Exit Sub
        End If

    End Sub
    Private Sub btnJlookup_Click(sender As Object, e As System.EventArgs) Handles btnJLookup.Click
        pnlGrid.Visible = True
        If cboDepartement.SelectedValue.Trim <> "0" Then
            Dim Pays() As String = vsPay.Value.Split(",")
            Dim Descs() As String = vsDesc.Value.Split(",")
            Dim Jumlahs() As String = vsJumlah.Value.Split(",")

            If Pays.Length > 0 Then
                txtJLookup.Text = "Sudah dipilih!"
                For index = 0 To Pays.Length - 1
                    Dim Pay As String = Pays(index).Replace("'", "")
                    Dim Desc As String = Descs(index).Replace("'", "")
                    Dim Jumlah As String = Jumlahs(index).Replace("'", "")

                    AddRecord(Pay, Desc, CInt(Jumlah))
                Next
                RFVtxtJLookup.Visible = False
            Else
                txtJLookup.Text = "Belum dipilih!"
            End If

        Else
            ShowMessage(lblMessage, "Departemen Harus diisi!", True)
            Exit Sub
        End If

    End Sub
    Private Sub Jlookup_Click(sender As Object, e As System.EventArgs) Handles Jlookup.Click
        pnlGrid.Visible = True
        If cboDepartement.SelectedValue.Trim <> "0" Then
            Dim Pays() As String = vsPay.Value.Split(",")
            Dim Descs() As String = vsDesc.Value.Split(",")
            Dim Jumlahs() As String = vsJumlah.Value.Split(",")

            If Pays.Length > 0 Then
                txtJLookup.Text = "Sudah dipilih!"
                For index = 0 To Pays.Length - 1
                    Dim Pay As String = Pays(index).Replace("'", "")
                    Dim Desc As String = Descs(index).Replace("'", "")
                    Dim Jumlah As String = Jumlahs(index).Replace("'", "")

                    AddRecord(Pay, Desc, CInt(Jumlah))
                Next
                RFVtxtJLookup.Visible = False
            Else
                txtJLookup.Text = "Belum dipilih!"
            End If

        Else
            ShowMessage(lblMessage, "Departemen Harus diisi!", True)
            Exit Sub
        End If

    End Sub

    Public Sub AddRecord(ByVal ID As String, ByVal Desc As String, ByVal i As Integer)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow

        Dim chk As New CheckBox
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim txtAmountTrans As New TextBox
        Dim chkIsPotong As New CheckBox

        With objectDataTable
            .Columns.Add(New DataColumn("chk", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
            .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
            .Columns.Add(New DataColumn("txtAmountTrans", GetType(String)))
            .Columns.Add(New DataColumn("chkIsPotong", GetType(String)))
        End With

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)



            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("chk") = CType(chk.Checked, String)
            oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
            oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
            oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
            oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
            oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
            oRow("txtAmountTrans") = CType(txtAmountTrans.Text, String)
            oRow("chkIsPotong") = CType(chkIsPotong.Checked, String)

            objectDataTable.Rows.Add(oRow)
        Next

        If i = 0 Then
            i = i + 1
        End If


        For index = 1 To i
            oRow = objectDataTable.NewRow()
            oRow("chk") = "False"
            oRow("DepartmentID") = cboDepartement.SelectedValue
            oRow("DepartmentName") = cboDepartement.SelectedItem.Text
            oRow("PaymentAllocationID") = ID
            oRow("PaymentAllocationName") = Desc
            oRow("txtKeterangan") = ""
            oRow("txtAmountTrans") = "0"
            oRow("chkIsPotong") = "False"

            objectDataTable.Rows.Add(oRow)
        Next

        DtgPRList.DataSource = objectDataTable
        DtgPRList.DataBind()

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)

            chk.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim)
            lblDepartmentID.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            lblDepartmentName.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblPaymentAllocID.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblPaymentAllocDesc.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtKeterangan.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtAmountTrans.Text = FormatNumber(objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim, 0)
            chkIsPotong.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item(7).ToString.Trim)
            chkIsPotong.Attributes.Add("OnChange", " klikpotong('" & chkIsPotong.ClientID & "','" & txtAmountTrans.ClientID & "')")
        Next

        ScriptManager.RegisterStartupScript(DtgPRList, GetType(DataGrid), DtgPRList.ClientID, String.Format(" total(); ", DtgPRList.ClientID), True)
    End Sub
    Private Sub ButtonDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDelete.Click
        Dim intLoopGrid As Integer
        Dim chk As New CheckBox
        Dim tempDels As String = ""

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            If chk.Checked = True Then
                tempDels = tempDels + "" + intLoopGrid.ToString + ""
            End If

        Next

        Dim Dels() As String = tempDels.Split(",")
        If Dels.Length > 0 Then
            For index = 0 To Dels.Length - 1
                Dim Del As String = Dels(index)

                DeleteBaris(CInt(Del))
            Next
        End If

    End Sub
    Public Sub DeleteBaris(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim chk As New CheckBox
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim txtAmountTrans As New TextBox
        Dim chkIsPotong As New CheckBox

        Dim oNewDataTable As New DataTable


        With oNewDataTable
            .Columns.Add(New DataColumn("chk", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
            .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
            .Columns.Add(New DataColumn("txtAmountTrans", GetType(String)))
            .Columns.Add(New DataColumn("chkIsPotong", GetType(String)))
        End With

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)

            '----- Add row -------'
            If intLoopGrid <> Index And Not chk.Checked Then
                oRow = oNewDataTable.NewRow()
                oRow("chk") = CType(chk.Checked, String)
                oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
                oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
                oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
                oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
                oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
                oRow("txtAmountTrans") = CType(txtAmountTrans.Text, String)
                oRow("chkIsPotong") = CType(chkIsPotong.Checked, String)

                oNewDataTable.Rows.Add(oRow)
            End If
        Next
        DtgPRList.DataSource = oNewDataTable
        DtgPRList.DataBind()

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)

            chk.Checked = CBool(oNewDataTable.Rows(intLoopGrid).Item(0).ToString.Trim)
            lblDepartmentID.Text = oNewDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            lblDepartmentName.Text = oNewDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblPaymentAllocID.Text = oNewDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblPaymentAllocDesc.Text = oNewDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtKeterangan.Text = oNewDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtAmountTrans.Text = FormatNumber(oNewDataTable.Rows(intLoopGrid).Item(6).ToString.Trim, 0)
            chkIsPotong.Checked = CBool(oNewDataTable.Rows(intLoopGrid).Item(7).ToString.Trim)
        Next

        ScriptManager.RegisterStartupScript(DtgPRList, GetType(DataGrid), DtgPRList.ClientID, String.Format(" total(); ", DtgPRList.ClientID), True)
    End Sub
    Private Sub btnReset1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset1.Click
        txtBankAccountIDCabang.Text = ""
        txtBankAccountNameCabang.Text = ""
        cboBankName.SelectedIndex = 0
        TxtNamaRekening.Text = ""
        TxtNoRekening.Text = ""
    End Sub
    Private Sub btnReset2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset2.Click
        txtBankAccountIDLain.Text = ""
        txtBankAccountNameLain.Text = ""
        cboBankName.SelectedIndex = 0
        TxtNamaRekening.Text = ""
        TxtNoRekening.Text = ""
    End Sub
End Class