﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class PaymentRequestYellowPage
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oBranch As UcBranch
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.PaymentRequest
    Private oController As New Controller.PaymentRequestController
    Private m_TotalAmount As Double

    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property RequestNo() As String
        Get
            Return (CType(ViewState("RequestNo"), String))
        End Get
        Set(value As String)
            ViewState("RequestNo") = value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Public Property GM1 As String
    Public Property GM2 As String
    Public Property GM3 As String
    Public Property Direksi As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "PAYREQUESTYLOW"

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If


            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
                Dim strFileLocation = String.Format("http://{0}/{1}/XML/{2}.pdf", Request.ServerVariables("SERVER_NAME"), strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1), Request.QueryString("strFileLocation"))
                Response.Write("<script language = javascript>" & vbCrLf & "var x = screen.width; " & vbCrLf & "var y = screen.heigth; " & vbCrLf & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf & "</script>")
            End If

            With cboParent
                .DataValueField = "ID"
                .DataTextField = "Name"
                If Me.IsHoBranch Then
                    .DataSource = (New DataUserControlController).GetBranchAll(GetConnectionString)
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(0).Value = "ALL"
                Else
                    Dim dtbranch As New DataTable
                    dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    If Not (dtbranch Is Nothing) Then
                        .Items.Insert(1, dtbranch.Rows(0)(1))
                        .Items(1).Value = Me.sesBranchId
                    End If
                End If


            End With

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If

    End Sub


    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Sub DoBind(Optional isFrNav As Boolean = False)
        Try
            Dim Application As New Parameter.PaymentRequest
            Dim DataList As New List(Of Parameter.PaymentRequest)
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With
            Dim dtEntity As New DataTable
            Dim listData = oController.GetPaymentRequestPaging(oCustomClass).ListData

            If listData Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            recordCount = oCustomClass.TotalRecord
            listData.DefaultView.Sort = Me.SortBy
            dtEntity = oCustomClass.ListData
            DtgAgree.DataSource = listData.DefaultView

            If TempDataTable Is Nothing Then
                TempDataTable = New DataTable
                With TempDataTable
                    .Columns.Add(New DataColumn("RequestNo", GetType(String)))
                End With
            Else
                For index = 0 To TempDataTable.Rows.Count - 1
                    Application = New Parameter.PaymentRequest

                    Application.RequestNo = TempDataTable.Rows(index).Item("RequestNo").ToString.Trim

                    DataList.Add(Application)
                Next
            End If
            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try

            If (isFrNav = False) Then
                GridNavigator.Initialize(recordCount, pageSize)
            End If

            pnlSearch.Visible = True
            pnlDatagrid.Visible = True


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.Source + " " + ex.StackTrace, True)
        End Try
    End Sub



    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oBranch.DataBind()
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        pnlDatagrid.Visible = False
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String
        strFilterBy = ""
        Me.BranchID = cboParent.SelectedItem.Value.Trim
        Me.SearchBy = String.Format(" PR.branchid = '{0}' AND PR.IsReconcile <> 1 AND PR.Status in ('REQ','CAB', 'HQ','ACC') ", Me.BranchID.Replace("'", ""))
        strFilterBy = String.Format("Branch = {0}", Me.BranchID)

        If cboSearchBy.SelectedItem.Value <> "0" Then
            Me.SearchBy = String.Format("{0} and {1} Like '%{2}%' ", Me.SearchBy, cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim)
            strFilterBy = strFilterBy & ", " & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim

        End If

        'If txtDate.Text.Trim.Length > 0 Then
        '    Me.SearchBy = String.Format("{0} and RequestDate = '{1}'", Me.SearchBy, ConvertDate2(txtDate.Text.Trim))
        '    strFilterBy = strFilterBy & ", RequestDate = " & txtDate.Text.Trim
        'End If

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlSearch.Visible = True
        DoBind()
        FillCbo(author1, 1)
        FillCbo(author2, 2)
        FillCbo(author3, 3)
        FillCbo(author4, 4)
    End Sub
    Public Function GetEmployee(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 30)
        params(0).Value = CustomClass.BranchId
        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 30)
        params(1).Value = CustomClass.EmployeePosition


        Try
            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Protected Sub FillCbo(ByVal cboName As DropDownList, ByVal EmployeePosition As String)
        Dim customClass As New Parameter.APDisbApp
        With customClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .EmployeePosition = EmployeePosition
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = ""
            .SpName = "spCboGetEmployee"
        End With
        customClass = GetEmployee(customClass)
        Dim dt As DataTable
        dt = customClass.listDataReport
        With cboName
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "ASC", "DESC"))
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label
        Dim lblRequestNo As New Label



        Me.RequestNo = lblRequestNo.Text.Trim
        If e.Item.ItemIndex >= 0 Then
            lblTemp = CType(e.Item.FindControl("lblAmount"), Label)
            lblRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label)
            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblTotalAmount"), Label)
            lblRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If
    End Sub
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Try
            Dim RequestNo As String = CType(e.Item.FindControl("lblRequestNo"), Label).Text.Trim
            If e.CommandName.Trim = "Verifikasi" Then
                Dim hyRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label).Text.Trim

                pnlSearch.Visible = False
                pnlDatagrid.Visible = False

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim loopitem As Int16
        For loopitem = 0 To CType(DtgAgree.Items.Count - 1, Int16)
            Dim Chk = CType(DtgAgree.Items(loopitem).FindControl("cbCheck"), CheckBox)
            If Chk.Enabled Then
                Chk.Checked = CType(sender, CheckBox).Checked
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        Dim j = 0
        Dim dtChoosen As IList(Of String) = New List(Of String)
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim lblRequestNo As String
        Dim cbCheck As CheckBox
        With oDataTable
            .Columns.Add(New DataColumn("RequestNo", GetType(String)))
        End With
        For intloop = 0 To DtgAgree.Items.Count - 1
            cbCheck = CType(DtgAgree.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
            If cbCheck.Checked Then

                lblRequestNo = CType(DtgAgree.Items(intloop).Cells(1).FindControl("lblRequestNo"), Label).Text.Trim
                GM1 = author1.SelectedValue.Trim
                GM2 = author2.SelectedValue.Trim
                GM3 = author3.SelectedValue.Trim
                Direksi = author4.SelectedValue.Trim

                Me.RequestNo = lblRequestNo
                Me.GM1 = GM1
                Me.GM2 = GM2
                Me.GM3 = GM3
                Me.Direksi = Direksi
            End If
        Next
        For i = 0 To DtgAgree.Items.Count - 1
            Dim cb = CType(DtgAgree.Items(i).FindControl("cbCheck"), CheckBox)
            If cb.Checked = True Then
                j += 1
                dtChoosen.Add(CType(DtgAgree.Items(i).FindControl("lblRequestNo"), Label).Text.Trim)
            End If
        Next
        If j <= 0 Then
            ShowMessage(lblMessage, "Harap Check Item", True)
            Exit Sub
        End If
        Session(COOKIES_PETTY_CASH_VOUCHER) = dtChoosen
        Dim cookie As HttpCookie = Request.Cookies("RptPmtPembayaranSPPL")
        If Not cookie Is Nothing Then
            cookie.Values("RequestNo") = Me.RequestNo
            cookie.Values("GM1") = Me.GM1
            cookie.Values("GM2") = Me.GM2
            cookie.Values("GM3") = Me.GM3
            cookie.Values("Direksi") = Me.Direksi
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("RptPmtPembayaranSPPL")
            cookieNew.Values.Add("RequestNo", Me.RequestNo)
            cookieNew.Values("GM1") = Me.GM1
            cookieNew.Values("GM2") = Me.GM2
            cookieNew.Values("GM3") = Me.GM3
            cookieNew.Values("Direksi") = Me.Direksi
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("CetakFormKuning.aspx")
    End Sub
End Class