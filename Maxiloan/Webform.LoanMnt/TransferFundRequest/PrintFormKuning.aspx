﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintFormKuning.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.PrintFormKuning" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../../Webform.UserController/ucBranchHO.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cetak Form Kuning Permintaan Pembayaran</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" /> 
<script language="javascript" type="text/javascript">

    function OpenWinMain(GiroNo, PDCReceiptNo, branchid, flagfile) {

        var x = screen.width; var y = screen.height - 100;
        window.open(ServerName + App + '/Webform.LoanMnt/PDC/PDCInquiryDetail.aspx?GiroNo=' + GiroNo + '&PDCReceiptNo=' + PDCReceiptNo + '&branchid=' + branchid + '&flagfile=' + flagfile, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
    }
     			
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" />
            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3> VERIFIKASI - PERMINTAAAN PEMBYARAN CABANG </h3>
                </div>
            </div>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <div class="form_left">
                            <label class="label_req"> Cabang</label>
                            <asp:DropDownList ID="cboParent" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" />
                        </div>
                        <div class="form_right">
                            <label class="label_req"> Tanggal Request</label>
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDate" Format="dd/MM/yyyy" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtDate" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                            <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"  Width="88px"></asp:TextBox>
                    </div>
                </div>
                 
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>

             <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4> DAFTAR PERMINTAAN PEMBAYARAN </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>

                                 <asp:TemplateColumn>
                                    <%--<HeaderTemplate>
                                            <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                    </HeaderTemplate>--%>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                    </ItemTemplate>
                                   </asp:TemplateColumn>

                                  <%--  <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemStyle CssClass="command_col" Width="7%" />
                                        <ItemTemplate>
                                             <asp:LinkButton ID="hypPrint" runat="server" Text="Verifikasi"  CommandName="Verifikasi"/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>

                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" />
                                        <ItemStyle HorizontalAlign="Left" Width="13%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestNo" runat="server" Text='<%#Eval("RequestNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestBy" HeaderText="DIAJUKAN OLEH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestBy" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" width="35%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Container.DataItem("Description") %>' />
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" />
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="12%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("TotalAmount"), 0)%>' />
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAmount" runat="server" Visible="True" Text='0.0' />
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="TGL REQUEST" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul" />
                                        <ItemStyle HorizontalAlign="Center" Width="8%" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" />
                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                              <uc2:ucGridNav id="GridNavigator" runat="server"/>
                        </div>
                    </div>
                </div> 

             <div class="form_button">
                <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Visible="True" Text="Next" CssClass="small button green"></asp:Button>
            </div>

            </asp:Panel>

           

    </ContentTemplate>
    </asp:UpdatePanel> 
    </form>
</body>
</html>
