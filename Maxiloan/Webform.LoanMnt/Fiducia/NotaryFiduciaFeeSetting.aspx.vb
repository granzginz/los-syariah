﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class NotaryFiduciaFeeSetting
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents TxtOTRFrom As ucNumberFormat
    Protected WithEvents TxtOTRUntil As ucNumberFormat
    Protected WithEvents TxtFiduciaFee As ucNumberFormat

#Region "Constanta"
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New GeneralPagingController
    Protected WithEvents oSearchBy As UcSearchBy
    'Protected WithEvents oBranch As UcBranch
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Dim Ocontroller As New FiduciaController
    Dim OEntities As New Parameter.Fiducia
#End Region
#Region "Property"
    Private Property NotaryID() As String
        Get
            Return CType(viewstate("NotaryID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("NotaryID") = Value
        End Set
    End Property
    Private Property NotaryName() As String
        Get
            Return CType(viewstate("NotaryName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("NotaryName") = Value
        End Set
    End Property
    Private Property ModeStatus() As String
        Get
            Return CType(viewstate("ModeStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ModeStatus") = Value
        End Set
    End Property

    Private Property SeqNo() As String
        Get
            Return CType(viewstate("SeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SeqNo") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            pnlEditFee.Visible = False
            PnlTop.Visible = True
            Me.NotaryID = Replace(Request.QueryString("NotaryID").Trim, "'", "")
            Me.NotaryName = Replace(Request.QueryString("NotaryName").Trim, "'", "")
            Me.SearchBy = " NotaryCharge.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' And NotaryCharge.NotaryID = '" & Replace(Me.NotaryID, "'", "") & "' "
            BindgridNotaryFeeDetail(Me.SearchBy, Me.SortBy)
            LblNotarID.Text = Replace(Me.NotaryID, "'", "")
            LblNotaryName.Text = Replace(Me.NotaryName, "'", "")
        End If

    End Sub
    Sub BindgridNotaryFeeDetail(ByVal cmdWhere As String, ByVal sort As String)
        Dim dtEntity As New DataTable
        Dim oEntitiesCommon As New Parameter.GeneralPaging
        If sort = "" Then sort = "SeqNo"
        Me.SearchBy = cmdWhere
        With oEntitiesCommon
            .SpName = "spPagingNotaryFeeDetail"
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString
        End With
        oEntitiesCommon = m_controller.GetGeneralPaging(oEntitiesCommon)

        If Not oEntitiesCommon Is Nothing Then
            dtEntity = oEntitiesCommon.ListData
            recordCount = oEntitiesCommon.TotalRecords
        Else
            recordCount = 0
        End If

        DtgFiduciaSetting.DataSource = dtEntity.DefaultView
        DtgFiduciaSetting.CurrentPageIndex = 0
        DtgFiduciaSetting.DataBind()
        PagingFooter()
    End Sub
    Private Sub DtgFiduciaSetting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgFiduciaSetting.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgNotary_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgFiduciaSetting.ItemCommand

        If e.CommandName = "Edit" Then
            pnlEditFee.Visible = True
            PnlTop.Visible = False
            Me.ModeStatus = "Edit"

            Dim LblOTRFrom As Label = CType(DtgFiduciaSetting.Items(e.Item.ItemIndex).FindControl("LblOTRFrom"), Label)
            Dim LblOTRUntil As Label = CType(DtgFiduciaSetting.Items(e.Item.ItemIndex).FindControl("LblOTRUntil"), Label)
            Dim LblFiduciaFee As Label = CType(DtgFiduciaSetting.Items(e.Item.ItemIndex).FindControl("LblFiduciaFee"), Label)
            Dim LblSeqNo As Label = CType(DtgFiduciaSetting.Items(e.Item.ItemIndex).FindControl("LblSeqNo"), Label)

            LblNotarID.Text = Replace(Me.NotaryID, "'", "")
            LblNotaryName.Text = Replace(Me.NotaryName, "'", "")

            TxtOTRFrom.Text = LblOTRFrom.Text
            TxtOTRUntil.Text = LblOTRUntil.Text
            TxtFiduciaFee.Text = LblFiduciaFee.Text
            Me.SeqNo = LblSeqNo.Text


        End If

        If e.CommandName = "Del" Then
            'If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
            Dim LblSeqNo As Label = CType(DtgFiduciaSetting.Items(e.Item.ItemIndex).FindControl("LblSeqNo"), Label)
            With oCustomClass
                .tableName = "NotaryCharge"
                .strConnection = GetConnectionString()
                .WhereCond = " BranchId='" & Replace(Me.sesBranchId, "'", "") & "' And NotaryID='" & Replace(Me.NotaryID, "'", "") & "' and SeqNo ='" & Replace(LblSeqNo.Text, "'", "") & "'"

            End With
            Dim bolDelete As Boolean
            Try
                bolDelete = True
                bolDelete = m_controller.DeleteGeneral(oCustomClass)
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("NotaryFiduciaFeeSetting.aspx.vb", "Del_dtgNotary_ItemCommand", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Response.Write(ex.Message & " - " & ex.StackTrace)
                bolDelete = False
            End Try

            If bolDelete Then
                '  lblMessage.Text = "Record has been Deleted successfully"
            Else
                ' lblMessage.Text = "Failed to Deleted the Notary "
            End If
            BindgridNotaryFeeDetail(Me.SearchBy, Me.SortBy)
            'Else
            '    Dim strHTTPServer As String
            '    Dim strHTTPApp As String
            '    Dim strNameServer As String
            '    strHTTPServer = Request.ServerVariables("PATH_INFO")
            '    strNameServer = Request.ServerVariables("SERVER_NAME")
            '    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            '    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            'End If

        End If
    End Sub
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region
#Region "Sorting "


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgFiduciaSetting.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        pnlEditFee.Visible = False
        PnlTop.Visible = True
        BindgridNotaryFeeDetail(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region " NavigationLink"
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        pnlEditFee.Visible = False
        PnlTop.Visible = True
        BindgridNotaryFeeDetail(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                pnlEditFee.Visible = False
                PnlTop.Visible = True
                BindgridNotaryFeeDetail(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "Back"
    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Response.Redirect("Notary.aspx")
    End Sub
#End Region
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        pnlEditFee.Visible = True
        PnlTop.Visible = False
        Me.ModeStatus = "Add"
        Me.SeqNo = "0"
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try
            With OEntities
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .NotaryID = Me.NotaryID
                .OTRFrom = CType(TxtOTRFrom.Text, Double)
                .OTRUntil = CType(TxtOTRUntil.Text, Double)
                .FiduciaFee = CType(TxtFiduciaFee.Text, Double)
                .SeqNo = Me.SeqNo
                .FullName = Me.ModeStatus
                .strConnection = GetConnectionString()
            End With
            Ocontroller.NotaryChargeAdd(OEntities)
            Response.Redirect("NotaryFiduciaFeeSetting.aspx?NotaryID=" & Me.NotaryID & "&NotaryName=" & Me.NotaryName & "")
        Catch ex As Exception
            Response.Write("Edit : " + ex.Message + ex.StackTrace)
        End Try

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        pnlEditFee.Visible = False
        PnlTop.Visible = True
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim cookie As HttpCookie = Request.Cookies("NotaryFiduciaFeeSetting")
        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = Me.SearchBy
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("NotaryFiduciaFeeSetting")
            cookieNew.Values.Add("cmdwhere", Me.SearchBy)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("NotaryFiduciaFeeSettingViewer.aspx")
    End Sub

End Class