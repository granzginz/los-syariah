﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NotaryAddEdit.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.NotaryAddEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NotaryAddEdit</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ADD/EDIT - NOTARIS
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                ID Notaris
            </label>
            <asp:Label ID="LblNotaryID" runat="server"></asp:Label><asp:TextBox ID="TxtNotaryID"
                runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="ReqFieldValNotaryID" Enabled="false"
                    runat="server" ErrorMessage="Harap isi ID Notaris" ControlToValidate="TxtNotaryID"
                    Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Notaris
            </label>
            <asp:TextBox ID="TxtNotaryName" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No SK Menkumham
            </label>
            <asp:TextBox ID="TxtSKMenkeh" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal SK Menkumham
            </label>            
            <uc1:ucdatece id="txtDateMenKeh" runat="server"></uc1:ucdatece>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                e-Mail
            </label>
            <asp:TextBox ID="TxtNotaryEmail" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:UcCompanyAddress id="UcNotaryAdress" runat="server" />
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                <strong>KONTAK</strong>
            </label>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:uccontactperson id="UcContactPerson" runat="server">
            </uc1:uccontactperson>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                <strong>REKENING BANK</strong>
            </label>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:ucbankaccount id="UcBankAccount" runat="server">
            </uc1:ucbankaccount>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Status Aktif
            </label>
            <asp:CheckBox ID="ChkStatus" runat="server" Checked="True"></asp:CheckBox>
        </div>
    </div>
        <div class="form_box">
        <div class="form_single">
            <%--edit npwp, ktp, telepon by ario--%>
            <label class="label_split_req">
                No NPWP
            </label>
            <asp:TextBox ID="TxtNoNPWP" runat="server" MaxLength="35" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatornpwp" runat="server"
                    ControlToValidate="txtNoNPWP" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*"
                    CssClass="validator_general">
            </asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoNPWP"
                ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
