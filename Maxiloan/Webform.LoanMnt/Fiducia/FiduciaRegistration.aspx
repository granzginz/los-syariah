﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FiduciaRegistration.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FiduciaRegistration" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcMasterNotary" Src="../../Webform.UserController/UcMasterNotary.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FiduciaRegistration</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
  
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="title_strip">
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        REGISTRASI FIDUCIA</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <uc1:ucsearchby id="oSearchBy" runat="server">
            </uc1:ucsearchby>
                    </div>
                    <div class="form_right">
                        <label>
                            Notaris
                        </label>
                        <uc1:ucmasternotary id="oNotary" runat="server">
            </uc1:ucmasternotary>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Tanggal Aktivasi
                        </label>
                        <asp:TextBox runat="server" ID="txtPeriodeFrom"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtPeriodeFrom"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <label class="label_auto">
                            s/d</label>
                        <asp:TextBox runat="server" ID="txtPeriodeTo"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtPeriodeTo"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Registrasi
                        </label>
                        <asp:TextBox runat="server" ID="txtOfferingDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtOfferingDate"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator runat="server" ID="rfvDateCE" ControlToValidate="txtOfferingDate"
                            Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general"
                            SetFocusOnError="true" >
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                            ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtOfferingDate"
                            SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
            <asp:Panel ID="PnlDgrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR REGISTRASI FIDUCIA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgFiduciaRegListing" runat="server" CssClass="grid_general" AllowSorting="True"
                                DataKeyField="AgreementNo" AutoGenerateColumns="False" OnSortCommand="Sorting">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle Width="3%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" AutoPostBack="True" runat="server" OnCheckedChanged="SelectAll">
                                            </asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:Label ID="LblAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'
                                                Visible="true">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Customer.Name" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:Label ID="LblCustomerName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="assetmaster.Description" HeaderText="NAMA ASSET">
                                        <ItemTemplate>
                                            <asp:Label ID="LblAssetDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetDescription") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="agreementasset.OTRPrice" HeaderText="HARGA OTR">
                                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblOTRPrice" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.OtrPrice"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="TglKontrak" HeaderText="TGL KONTRAK">
                                        <ItemTemplate>
                                            <asp:Label ID="LblTglKontrak" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TglKontrak") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                       <asp:TemplateColumn SortExpression="TglAktif" HeaderText="TGL AKT">
                                        <ItemTemplate>
                                            <asp:Label ID="LblTglAktif" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TglAktif") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="Asset Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssetType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetType") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="CustomerID">
                                        <ItemTemplate>
                                            <asp:Label ID="LblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="ApplicationID">
                                        <ItemTemplate>
                                            <asp:Label ID="LblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="AssetSeqNo">
                                        <ItemTemplate>
                                            <asp:Label ID="LblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"
                                    Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnSave" runat="server" Text="Save"  CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
        
    </form>
</body>
</html>
