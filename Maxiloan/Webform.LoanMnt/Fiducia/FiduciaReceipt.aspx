﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FiduciaReceipt.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FiduciaReceipt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FiduciaReceipt</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                TERIMA AKTA FIDUCIA
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cari Berdasarkan
            </label>
            <uc1:ucsearchby id="oSearchBy" runat="server">
                </uc1:ucsearchby>
        </div>
        <div class="form_right">
            <label>
                Tanggal Registrasi
            </label>
            <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Aging
            </label>
            <asp:TextBox ID="TxtAgingDays" runat="server" Width="72px"></asp:TextBox>&nbsp;Hari
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlDgrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR AKTA FIDUCIA
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFiduciaRegListing" runat="server" CssClass="grid_general" OnSortCommand="Sorting"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/icondetail.gif" runat="server"
                                        CommandName="Receive"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAgreementNo" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCustomerName" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerID" HeaderText="CustomerID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCustomerID" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NotaryName" HeaderText="NAMA NOTARIS">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblNotaryName" Text='<%# DataBinder.Eval(Container, "DataItem.NotaryName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetMaster.Description" HeaderText="NAMA ASSET">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetDescription" Text='<%# DataBinder.Eval(Container, "DataItem.AssetDescription") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
<%--                            <asp:TemplateColumn SortExpression="OfferingDate" HeaderText="TGL PENAWARAN AKTA">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblOfferingDate" Text='<%# DataBinder.Eval(Container, "DataItem.OFFERINGDATE") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                              <asp:TemplateColumn SortExpression="RegisterDate" HeaderText="TGL REG">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblRegisterDate" Text='<%# DataBinder.Eval(Container, "DataItem.RegisterDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                              <asp:TemplateColumn SortExpression="AgreementDate" HeaderText="TGL KONTRAK">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAgreementDate" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Aging" HeaderText="AGING">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAging" Text='<%# DataBinder.Eval(Container, "DataItem.Aging") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="HARGA OTR">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblOTRPrice" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.OTRPrice"),2) %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="AssetSeqNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetSeqNo" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="ApplicationID">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblApplicationID" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="AssetType">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetType" Text='<%# DataBinder.Eval(Container, "DataItem.AssetType") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="Condition">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCondition" Text='<%# DataBinder.Eval(Container, "DataItem.Condition") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="InvoiceDate" HeaderText="InvoiceDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblInvoiceDate" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="FiduciaFee" HeaderText="FiduciaFee">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblFiduciaFee" Text='<%# DataBinder.Eval(Container, "DataItem.FiduciaFee") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AktaDate" HeaderText="AktaDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAktaDate" Text='<%# DataBinder.Eval(Container, "DataItem.AktaDate") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CertificateDate" HeaderText="CertificateDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCertificateDate" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateDate") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AktaNo" HeaderText="AktaNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAktaNo" Text='<%# DataBinder.Eval(Container, "DataItem.AktaNo") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CertificateNo" HeaderText="CertificateNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCertificateNo" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateNo") %>'
                                        Visible="False" />
                                         <asp:Label runat="server" ID="LblReceiveDate" Text='' Visible="False" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--<asp:TemplateColumn Visible="False" SortExpression="ReceiveDate" HeaderText="CertificateDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblReceiveDate" Text='<%# DataBinder.Eval(Container, "DataItem.ReceiveDate") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> --%>                         
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"
                            Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
