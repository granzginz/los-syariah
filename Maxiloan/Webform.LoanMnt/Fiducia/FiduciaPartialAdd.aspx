﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FiduciaPartialAdd.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FiduciaPartialAdd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FiduciaReceiptDetailAdd</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
     <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                TERIMA AKTA FIDUCIA
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:Label ID="LblAgreementNo" runat="server" Visible="False"></asp:Label>
            <asp:HyperLink ID="HplinkAgreementNo" runat="server" NavigateUrl=""></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="LblCustomerName" runat="server" Visible="False"></asp:Label>
            <asp:HyperLink ID="HpLinkCustName" runat="server" NavigateUrl=""></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Aplikasi
            </label>
            <asp:HyperLink ID="hplinkApplicationID" runat="server" NavigateUrl=""></asp:HyperLink>
            <asp:Label ID="LblApplicationID" runat="server" Visible="false"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="LblAssetDescription" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="LblAssetType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Harga OTR
            </label>
            <asp:Label ID="LblOTRAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Kondisi
            </label>
            <asp:Label ID="LblCondition" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Notaris
            </label>
            <asp:Label ID="LblNotaryName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Registrasi
            </label>
            <asp:Label ID="LblAktaOfferingDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_left">
            <h5>
                DETAIL AKTA
            </h5>
        </div>
        <div class="form_right">
            <h5>
                DETAIL SERTIFIKAT</h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Akta
            </label>
            <asp:TextBox ID="TxtAktaNo" runat="server"></asp:TextBox>
        </div>
        <div class="form_right">
            <label>
                No Sertifikat
            </label>
            <asp:TextBox ID="TxtCertificateNo" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Akta
            </label>
            <asp:TextBox runat="server" ID="txtDateAkta" CssClass ="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDateAkta"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
        <div class="form_right">
            <label>
                Tanggal Sertifikat
            </label>
            <asp:TextBox runat="server" ID="txtDateCertificate" CssClass ="small_tex"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDateCertificate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
       <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Terima Dokumen
            </label>
            <asp:TextBox runat="server" ID="txtreceivedate" CssClass ="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtreceivedate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
        </div>
    <div class="form_button">
        <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
        <asp:Button ID="BtnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
