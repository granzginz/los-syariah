﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FiduciaRegistration
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents TxtOTRBottom As ucNumberFormat
    Protected WithEvents TxtOTRUp As ucNumberFormat
    Protected WithEvents oNotary As UcMasterNotary
#Region "Constanta"
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New GeneralPagingController
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

#End Region
#Region "Property"
    Protected Property PerideFrom() As String
        Get
            Return (CType(ViewState("PerideFrom"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PerideFrom") = Value
        End Set
    End Property
    Protected Property PerideTo() As String
        Get
            Return (CType(ViewState("PerideTo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PerideTo") = Value
        End Set
    End Property
    Protected Property OfferingDate() As String
        Get
            Return (CType(ViewState("OfferingDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("OfferingDate") = Value
        End Set
    End Property

#End Region
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To dtgFiduciaRegListing.Items.Count - 1
            chkItem = CType(dtgFiduciaRegListing.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            ' InitialDefaultPanel()
            PnlDgrid.Visible = False
            Me.FormID = "FiduciaRegistration"

            'If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            oSearchBy.ListData = "AGREEMENTNO,No. Kontrak-CUSTOMER.NAME, Nama Konsumen -AssetMaster.DESCRIPTION, Nama Asset "
            oSearchBy.BindData()
            Me.SearchBy = ""
            Me.SortBy = ""
            oNotary.BindNotary()
            oNotary.FillRequired = True
            'End If
        End If


    End Sub
#End Region
#Region "Search"
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click

        PnlDgrid.Visible = True
        Me.PerideFrom = txtPeriodeFrom.Text
        Me.PerideTo = txtPeriodeTo.Text
        Dim SearchByBuilder As New StringBuilder
        Me.SearchBy = ""
        SearchByBuilder.Append("  Agreement.IsFiduciaCovered='1' and Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' ")

        'Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            '  Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            SearchByBuilder.Append(" And " & oSearchBy.ValueID.Replace("'", "''") & " LIKE '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'  ")
        End If


        If txtPeriodeFrom.Text <> "" And txtPeriodeTo.Text <> "" Then
            SearchByBuilder.Append(" And  convert(varchar, agreement.GoLiveDate, 103) Between '" & Me.PerideFrom & "' And '" & Me.PerideTo & "'  ")
        Else
            SearchByBuilder.Append(" And  (Agreement.AgreementDate is not null and  Agreement.AgreementDate <> '1900-01-01') ")
        End If

        Me.SearchBy = SearchByBuilder.ToString
        Context.Trace.Write("Me.searchby = " & Me.SearchBy)
        Context.Trace.Write("Me.SortBy = " & Me.SortBy)
        Bindgrid(Me.SearchBy, Me.SortBy)


    End Sub
#End Region
#Region "BindGrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal sort As String)
        PnlDgrid.Visible = True
        Dim dtEntity As New DataTable
        Dim dtView As New DataView

        Dim oEntitiesCommon As New Parameter.GeneralPaging
        If sort = "" Then sort = " Agreement.ApplicationID "
        Me.SearchBy = cmdWhere
        With oEntitiesCommon
            .SpName = "spPagingFiduciaRegistration"
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString()
        End With
        Try
            oEntitiesCommon = m_controller.GetGeneralPaging(oEntitiesCommon)
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
        End Try


        If Not oEntitiesCommon Is Nothing Then
            dtEntity = oEntitiesCommon.ListData
            recordCount = oEntitiesCommon.TotalRecords
        Else
            recordCount = 0
        End If

        ''DtUserList = oCustomClass.ListData
        ''DvUserList = DtUserList.DefaultView
        ''DvUserList.Sort = Me.SortBy
        ''dtgPaging.DataSource = DvUserList

        'dtgFiduciaRegListing.DataSource = dtEntity.DefaultView
        'dtgFiduciaRegListing.CurrentPageIndex = 0
        'dtgFiduciaRegListing.DataBind()
        'PagingFooter()
        Try
            dtView = dtEntity.DefaultView
            dtgFiduciaRegListing.DataSource = dtView
            dtgFiduciaRegListing.DataBind()
            PagingFooter()
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace + ex.TargetSite.Name)
        End Try


    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region
#Region "Sorting "


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgFiduciaRegListing.SortCommand

        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "NavigationLink"

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        '    oSearchBy.Text.Trim = ""
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "DataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFiduciaRegListing.ItemDataBound
        Try

            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim m As Int32
            If e.Item.ItemIndex >= 0 Then
                '*** Customer Link
                lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
                hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
                '*** Agreement No link
                Dim lblApplicationId As Label
                lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
                hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)

        End Try


    End Sub

#End Region
#Region "save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try

            Me.OfferingDate = txtOfferingDate.Text
            Dim oFiduciaController As New FiduciaController
            Dim oEntities As New Parameter.Fiducia
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To dtgFiduciaRegListing.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(dtgFiduciaRegListing.Items(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Context.Trace.Write("oNotary.NotaryID = " & oNotary.NotaryID)
                    oEntities.BranchId = Replace(Me.sesBranchId, "'", "")
                    Dim AppID As Label = CType(dtgFiduciaRegListing.Items(n).FindControl("LblApplicationID"), Label)
                    Dim AssetSeqNo As Label = CType(dtgFiduciaRegListing.Items(n).FindControl("LblAssetSeqNo"), Label)
                    oEntities.ApplicationID = AppID.Text
                    oEntities.SeqNo = AssetSeqNo.Text
                    oEntities.NotaryID = oNotary.NotaryID '"N01"
                    oEntities.RegisterDate = Me.BusinessDate
                    oEntities.OfferingDate = ConvertDate2(Me.OfferingDate)
                    oEntities.RegisterNotes = ""
                    oEntities.RegisterBy = Me.Loginid
                    oFiduciaController.FiduciaAdd(oEntities)
                End If
            Next
            Bindgrid(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di simpan", False)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region


   
End Class