﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class Notary
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New GeneralPagingController
    Protected WithEvents oSearchBy As UcSearchBy
    'Protected WithEvents oBranch As UcBranch
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        PnlDgrid.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If        
        If Not IsPostBack Then
            If Request("back") = "no" Then
                If Request("msg") = "success" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, Request("msg").ToString, True)
                End If
            End If
            
            ' InitialDefaultPanel()
            Me.FormID = "NOTARY"
            'If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            oSearchBy.ListData = "NOTARYID,Notary ID-NOTARYNAME, Notary Name -NOTARYADDRESS, Notary Address "
            oSearchBy.BindData()
            Me.SearchBy = ""
            Me.SortBy = ""
            'End If
        End If

    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        PnlDgrid.Visible = True
        Me.SearchBy = " Notary.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)

    End Sub

    Private Sub dtgNotary_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgNotary.ItemDataBound

        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If

    End Sub

    Private Sub dtgNotary_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgNotary.ItemCommand



        If e.CommandName = "View" Then
            'LblNotarID.Text = e.Item.Cells(0).Text.Trim
            ' LblNotaryName.Text = e.Item.Cells(1).Text.Trim
            Dim LblNotaryID As Label = CType(dtgNotary.Items(e.Item.ItemIndex).FindControl("LblNotaryID"), Label)
            Dim LblNotaryName As Label = CType(dtgNotary.Items(e.Item.ItemIndex).FindControl("LblNotaryName"), Label)
            Response.Redirect("NotaryFiduciaFeeSetting.aspx?NotaryID='" & LblNotaryID.Text & "'&NotaryName='" & LblNotaryName.Text.Trim & "'")
        End If

        If e.CommandName = "Edit" Then
            'LblNotarID.Text = e.Item.Cells(0).Text.Trim
            'LblNotaryName.Text = e.Item.Cells(1).Text.Trim
            Dim LblNotaryID As Label = CType(dtgNotary.Items(e.Item.ItemIndex).FindControl("LblNotaryID"), Label)
            Dim LblNotaryName As Label = CType(dtgNotary.Items(e.Item.ItemIndex).FindControl("LblNotaryName"), Label)
            Response.Redirect("NotaryAddEdit.aspx?ModeStatus=Edit&NotaryID='" & LblNotaryID.Text & "'&NotaryName='" & LblNotaryName.Text.Trim & "'")
        End If

        If e.CommandName = "Del" Then
            'If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
            Dim LblNotaryID As Label = CType(dtgNotary.Items(e.Item.ItemIndex).FindControl("LblNotaryID"), Label)

            With oCustomClass
                .tableName = "NotaryCharge"
                .strConnection = GetConnectionString()
                .WhereCond = " branchId='" & Replace(Me.sesBranchId, "'", "") & "' and NotaryID ='" & Replace(LblNotaryID.Text, "'", "") & "'"
            End With
            Dim bolDelete As Boolean
            Try
                bolDelete = m_controller.DeleteGeneral(oCustomClass)
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("Notary.aspx.vb", "Del_dtgNotary_ItemCommand", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Response.Write(" ID Notaris : " & Replace(LblNotaryID.Text, "'", "") & ".. tidak dapat dihapus")
            End Try


            With oCustomClass
                .tableName = "Notary"
                .strConnection = GetConnectionString()
                .WhereCond = " branchId='" & Replace(Me.sesBranchId, "'", "") & "' and NotaryID ='" & Replace(LblNotaryID.Text, "'", "") & "'"
            End With
            Dim bolDeleteNotary As Boolean
            Try
                bolDeleteNotary = m_controller.DeleteGeneral(oCustomClass)
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("Notary.aspx.vb", "Del_dtgNotary_ItemCommand", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Response.Write(" ID Notaris : " & Replace(LblNotaryID.Text, "'", "") & ".. tidak dapat dihapus")
            End Try

            If bolDelete = True And bolDeleteNotary = True Then                
                ShowMessage(lblMessage, "Hapus Data Berhasil", False)
                Bindgrid(Me.SearchBy, Me.SortBy)
            Else                
                ShowMessage(lblMessage, "Hapus Data Gagal", True)
            End If


            'Else
            'Dim strHTTPServer As String
            'Dim strHTTPApp As String
            'Dim strNameServer As String
            'strHTTPServer = Request.ServerVariables("PATH_INFO")
            'strNameServer = Request.ServerVariables("SERVER_NAME")
            'strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            'Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            'End If

        End If
    End Sub


#Region "BindGrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal sort As String)
        PnlDgrid.Visible = True
        Dim dtEntity As New DataTable
        Dim oEntitiesCommon As New Parameter.GeneralPaging
        If sort = "" Then sort = "NotaryID"
        Me.SearchBy = cmdWhere
        With oEntitiesCommon
            .SpName = "spPagingNotary"
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString
        End With
        oEntitiesCommon = m_controller.GetGeneralPaging(oEntitiesCommon)

        If Not oEntitiesCommon Is Nothing Then
            dtEntity = oEntitiesCommon.ListData
            recordCount = oEntitiesCommon.TotalRecords
        Else
            recordCount = 0
        End If

        dtgNotary.DataSource = dtEntity.DefaultView
        dtgNotary.CurrentPageIndex = 0
        dtgNotary.DataBind()

        PagingFooter()
    End Sub
#End Region



#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region

#Region "Sorting "


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgNotary.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " Notary.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        '    oSearchBy.Text.Trim = ""
        Bindgrid(Me.SearchBy, Me.SortBy)

    End Sub

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click

        Response.Redirect("NotaryAddEdit.aspx?ModeStatus=Add")

    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim cookie As HttpCookie = Request.Cookies("NotaryMaster")
        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = Me.SearchBy
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("NotaryMaster")
            cookieNew.Values.Add("cmdwhere", Me.SearchBy)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("NotaryMasterViewer.aspx")
    End Sub

   
End Class