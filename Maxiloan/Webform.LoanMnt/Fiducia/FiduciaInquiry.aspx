﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FiduciaInquiry.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FiduciaInquiry" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FiduciaInquiry</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INQUIRY AKTA FIDUCIA
            </h3>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Aging
            </label>
            <asp:TextBox ID="TxtAgingDays" runat="server"  Width="69px"></asp:TextBox>Hari
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Harap diisi dengan Angka"
                MaximumValue="99999" MinimumValue="0" ControlToValidate="TxtAgingDays" Display="Dynamic"
                CssClass="validator_general"></asp:RangeValidator>
        </div>
        <div class="form_right">
            <label>
                Tanggal Registrasi
            </label>
            <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Batasan Harga OTR</label>            
            <uc1:ucnumberformat id="TxtOTRBottom" runat="server" />
            </uc1:ucNumberFormat>            
            <uc1:ucnumberformat id="TxtOTRUp" runat="server" />
            </uc1:ucNumberFormat>
        </div>
        <div class="form_right">
            <label>
                Status
            </label>
            <asp:DropDownList ID="cboStatusFiducia" runat="server">
                <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                <asp:ListItem Value="REQ">Request</asp:ListItem>
                <asp:ListItem Value="RCV">Receive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlDgrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR AKTA FIDUCIA
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFiduciaRegListing" runat="server" CssClass="grid_general" HorizontalAlign="Center"
                        AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="DETAIL">
                                <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbView" ImageUrl="../../Images/icondetail.gif" runat="server"
                                        CommandName="View"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                            <HeaderStyle HorizontalAlign="Center" Width="9%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="9%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAgreementNo" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCustomerName" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerID" HeaderText="CUSTOER ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCustomerID" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NotaryName" HeaderText="NAMA NOTARIS">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblNotaryName" Text='<%# DataBinder.Eval(Container, "DataItem.NotaryName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetMaster.Description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetDescription" Text='<%# DataBinder.Eval(Container, "DataItem.AssetDescription") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="OTRPrice" HeaderText="HARGA OTR">
                                <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblOTRPrice" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.OTRPrice"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="OfferingDate" HeaderText="TGL TRM">
                            <HeaderStyle HorizontalAlign="Center" Width="6%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="6%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblOfferingDate" Text='<%# DataBinder.Eval(Container, "DataItem.OFFERINGDATE") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Aging" HeaderText="AGING">
                                <HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="4%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="ALblging" Text='<%# DataBinder.Eval(Container, "DataItem.Aging") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationID" HeaderText="ApplicationID">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblApplicationID" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AssetType" HeaderText="AssetType">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetType" Text='<%# DataBinder.Eval(Container, "DataItem.AssetType") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AssetSeqNo" HeaderText="AssetSeqNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetSeqNo" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="Condition" HeaderText="Condition">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCondition" Text='<%# DataBinder.Eval(Container, "DataItem.Condition") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AktaNo" HeaderText="AktaNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAktaNo" Text='<%# DataBinder.Eval(Container, "DataItem.AktaNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="RegisterNotes" HeaderText="RegisterNotes">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblRegisterNotes" Text='<%# DataBinder.Eval(Container, "DataItem.RegisterNotes") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AktaDate" HeaderText="AktaDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAktaDate" Text='<%# DataBinder.Eval(Container, "DataItem.AktaDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CertificateNo" HeaderText="CertificateNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCertificateNo" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CertificateDate" HeaderText="CertificateDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCertificateDate" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="InvoiceNo" HeaderText="InvoiceNo">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblInvoiceNo" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="InvoiceNotes" HeaderText="InvoiceNotes">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblInvoiceNotes" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNotes") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="InvoiceDate" HeaderText="InvoiceDate">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblInvoiceDate" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'>
                                    </asp:Label>
                                     <asp:Label runat="server" ID="lblDueDate" Text='<%# DataBinder.Eval(Container, "DataItem.DueDate") %>'> />
                                     </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="FiduciaFee" HeaderText="FiduciaFee">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblFiduciaFee" Text='<%# DataBinder.Eval(Container, "DataItem.FiduciaFee") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGoPage"
                            ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
