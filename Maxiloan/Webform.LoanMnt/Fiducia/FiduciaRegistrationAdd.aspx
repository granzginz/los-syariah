﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FiduciaRegistrationAdd.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.FiduciaRegistrationAdd" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcMasterNotary" Src="../../Webform.UserController/UcMasterNotary.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fiducia Registration Add</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                REGISTRASI FIDUCIA
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="HplinkAgreementNo" runat="server" NavigateUrl=""></asp:HyperLink>
            <asp:Label ID="LblAgreementNo" runat="server" Visible="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="HpLinkCustName" runat="server" NavigateUrl=""></asp:HyperLink>
            <asp:Label ID="LblCustomerName" runat="server" Visible="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Aplikasi
            </label>
            <asp:HyperLink ID="HplinkApplicationID" runat="server" NavigateUrl=""></asp:HyperLink>
            <asp:Label ID="LblApplicationID" runat="server" Visible="False"></asp:Label>
        </div>       
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="LblAssetDescription" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="LblAssetType" runat="server" Visible="True"></asp:Label>
            <asp:HyperLink ID="HpLinkAssetDescription" runat="server" NavigateUrl="" Visible="False"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Harga OTR
            </label>
            <asp:Label ID="lblOTRAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Kondisi
            </label>
            <asp:Label ID="LblCondition" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Notaris
            </label>
            <uc1:ucmasternotary id="oNotary" runat="server">
            </uc1:ucmasternotary>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tgl Penawaran Akta
            </label>
            <asp:TextBox runat="server" ID="txtOfferingDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtOfferingDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_general">
                Catatan
            </label>
            <asp:TextBox ID="TxtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
        </div>
        <div class="form_box_hide">
            <label>
                Fiducia Fee
            </label>
            <uc1:ucnumberformat id="txtFiduciaFee" runat="server" />
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
