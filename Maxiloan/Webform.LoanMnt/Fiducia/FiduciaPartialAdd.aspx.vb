﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FiduciaPartialAdd
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents TxtFiduciaFee As ucNumberFormat

#Region "Property"
    Private Property AssetSeqNo() As String
        Get
            Return CType(ViewState("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetSeqNo") = Value
        End Set
    End Property
    Private Property ModeStatus() As String
        Get
            Return CType(ViewState("ModeStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ModeStatus") = Value
        End Set
    End Property
#End Region

#Region "Link"

    Function LinkToApplication(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreement(ByVal strStyle As String, ByVal strApplicationId As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "', '" & strApplicationId & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Dim cookie As HttpCookie = Request.Cookies("FiduciaReceipt")
            Dim ApplicationID As String = cookie.Values("ApplicationID")
            Dim AgreementNo As String = cookie.Values("AgreementNo")
            Dim CustomerID As String = cookie.Values("CustomerID")
            Dim CustomerName As String = cookie.Values("CustomerName")
            Dim AssetDescription As String = cookie.Values("AssetDescription")
            Dim AssetType As String = cookie.Values("AssetType")
            Dim OTRPrice As String = cookie.Values("OTRPrice")
            Dim Condition As String = cookie.Values("Condition")
            Dim NotaryName As String = cookie.Values("NotaryName")
            Dim AktaOfferingDate As String = cookie.Values("OfferingDate")
            Me.AssetSeqNo = cookie.Values("AssetSeqno")



            Dim AktaNo As String = cookie.Values("AktaNo")
            Dim RegisterNotes As String = cookie.Values("RegisterNotes")
            Dim AktaDate As String = cookie.Values("AktaDate")
            Dim CertificateNo As String = cookie.Values("CertificateNo")
            Dim CertificateDate As String = cookie.Values("CertificateDate")
            Dim InvoiceNotes As String = cookie.Values("InvoiceNotes")
            Dim InvoiceNo As String = cookie.Values("InvoiceNo")
            Dim InvoiceDate As String = cookie.Values("InvoiceDate")

            Dim FiduciaFee As String = cookie.Values("FiduciaFee")
            Dim ReceiveDate As String = cookie.Values("ReceiveDate")



            hplinkApplicationID.Text = ApplicationID
            HplinkAgreementNo.Text = AgreementNo
            HpLinkCustName.Text = CustomerName


            LblAgreementNo.Text = AgreementNo
            LblOTRAmount.Text = OTRPrice
            LblNotaryName.Text = NotaryName
            LblCustomerName.Text = CustomerName
            HpLinkCustName.NavigateUrl = LinkToCustomer(CustomerID, "AccMnt")
            LblAssetType.Text = AssetType
            LblCondition.Text = Condition
            LblAktaOfferingDate.Text = AktaOfferingDate
            LblAssetDescription.Text = AssetDescription
            LblApplicationID.Text = ApplicationID
            HplinkAgreementNo.NavigateUrl = LinkToAgreement("AccMnt", ApplicationID)
            TxtAktaNo.Text = AktaNo
            TxtCertificateNo.Text = CertificateNo
            txtDateCertificate.Text = IIf(CertificateDate = "", Me.BusinessDate.ToString("dd/MM/yyyy"), CertificateDate)
            txtDateAkta.Text = IIf(AktaDate = "", Me.BusinessDate.ToString("dd/MM/yyyy"), AktaDate)
            txtreceivedate.Text = IIf(ReceiveDate = "", Me.BusinessDate.ToString("dd/MM/yyyy"), ReceiveDate)

            hplinkApplicationID.Text = ApplicationID
            HplinkAgreementNo.Text = AgreementNo
            HpLinkCustName.Text = CustomerName

            hplinkApplicationID.NavigateUrl = LinkToApplication(ApplicationID, "AccMnt")
            HpLinkCustName.NavigateUrl = LinkToCustomer(CustomerID, "AccMnt")
            HplinkAgreementNo.NavigateUrl = LinkToAgreement("AccMnt", ApplicationID)
            cookie.Values.Remove("FiduciaReceipt")
           

        End If

    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try

            BtnSave.Visible = True
            Dim oController As New FiduciaController
            Dim oEntities As New Parameter.Fiducia
            With oEntities
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .ApplicationID = LblApplicationID.Text.Trim
                .SeqNo = Me.AssetSeqNo
                .AktaNo = TxtAktaNo.Text.Trim
                .AktaDate = ConvertDate2(txtDateAkta.Text)
                .CertificateNo = TxtCertificateNo.Text
                .CertificateDate = ConvertDate2(txtDateCertificate.Text)
                .RegisterBy = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .strConnection = GetConnectionString()
                .ReceiveDate = ConvertDate2(txtreceivedate.Text)
            End With
            oController.FiduciaFarsial(oEntities)
            Response.Redirect("FiduciaReceipt.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            BtnSave.Visible = False
        End Try



    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
       
        Response.Redirect("FiduciaReceipt.aspx")

    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
      
        Response.Redirect("FiduciaReceipt.aspx")
    End Sub

End Class