﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TerimaTagihanFiducia.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.TerimaTagihanFiducia" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>TerimaTagihanFiducia</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.6.4.js"></script>
  
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
     
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
         <script language="javascript" type="text/javascript">
             function onCheked(cb) {
                 var format = cb.id;
                 var my_array = format.split("_");
               
           if (cb.checked == true) {
               $("#dtgFiduciaRegListing_txtCertificateNo_" + my_array[2]).prop('disabled', false);
               $("#dtgFiduciaRegListing_txtCertificateDate_" + my_array[2]).prop('disabled', false);
               $("#dtgFiduciaRegListing_txtAktaNo_" + my_array[2]).prop('disabled', false);
               $("#dtgFiduciaRegListing_txtAktaDate_" + my_array[2]).prop('disabled', false);
               $("#dtgFiduciaRegListing_TxtFiduciaFee_" + my_array[2] + "_txtNumber_" + my_array[2]).prop('disabled', false);
             
           }
           else {
               $("#dtgFiduciaRegListing_txtCertificateNo_" + my_array[2]).prop('disabled', true);
               $("#dtgFiduciaRegListing_txtCertificateDate_" + my_array[2]).prop('disabled', true);
               $("#dtgFiduciaRegListing_txtAktaNo_" + my_array[2]).prop('disabled', true);
               $("#dtgFiduciaRegListing_txtAktaDate_" + my_array[2]).prop('disabled', true);
               $("#dtgFiduciaRegListing_TxtFiduciaFee_" + my_array[2] + "_txtNumber_" + my_array[2]).prop('disabled', true);
           }


       }
    </script>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
       
                  
            <asp:Panel ID="PnlDgrid" runat="server">
             <div class="form_title">
                    <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TERIMA TAGIHAN FIDUCIA
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                     <label class="label_req">
                        No Invoice
                    </label>
                    <asp:TextBox ID="TxtInvoiceNo" runat="server"></asp:TextBox>
                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidatorInvoiceNo" ControlToValidate="TxtInvoiceNo"
                            Display="Dynamic" ErrorMessage="No Invoice harus di isi!" CssClass="validator_general"
                            SetFocusOnError="true" > </asp:RequiredFieldValidator>
                </div>
             
            </div>
            <div class="form_box">
                <div class="form_left">
                   <label class="label_req">
                        Tanggal Invoice
                    </label>
                    <asp:TextBox runat="server" ID="txtDateInvoice" CssClass="small_tex"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceDateInvoice" TargetControlID="txtDateInvoice"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                      <asp:RequiredFieldValidator runat="server" ID="rfvDateInvoice" ControlToValidate="txtDateInvoice"
                            Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general"
                            SetFocusOnError="true" >
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionDateInvoice" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                            ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtDateInvoice"
                            SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                        </asp:RegularExpressionValidator>
                </div>
                    <div class="form_right">
                     <label class="label_req">
                        Tanggal Rencana Bayar
                    </label>
                    <asp:TextBox runat="server" ID="txtAPDueDate" CssClass="small_tex"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceAPDueDate" TargetControlID="txtAPDueDate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldAPDueDate" ControlToValidate="txtAPDueDate"
                            Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general"
                            SetFocusOnError="true" >
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorAPDueDate" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                            ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtAPDueDate"
                            SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                        </asp:RegularExpressionValidator>
                        
                </div>
                
            </div>
           
          
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR AKTA FIDUCIA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgFiduciaRegListing" runat="server" CssClass="grid_general" OnSortCommand="Sorting"  DataKeyField="AgreementNo"
                                AutoGenerateColumns="False" AllowSorting="True" Font-Size="XX-Small">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                          
                                              <asp:CheckBox ID="chkItem" runat="server" onclick="onCheked(this)" > </asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblAgreementNo" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblCustomerName" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerID" HeaderText="CustomerID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblCustomerID" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="NotaryName" Visible="False" HeaderText="NAMA NOTARIS">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblNotaryName" Text='<%# DataBinder.Eval(Container, "DataItem.NotaryName") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AssetMaster.Description" HeaderText="NAMA ASSET">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblAssetDescription" Text='<%# DataBinder.Eval(Container, "DataItem.AssetDescription") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="HARGA OTR">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblOTRPrice" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.OTRPrice"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CertificateNo" HeaderText="NO SERT">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtCertificateNo" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateNo") %>' Enabled="False">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CertificateDate" HeaderText="TGL SERT">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtCertificateDate" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateDate") %>' Enabled="False"> </asp:TextBox>
                                            <asp:CalendarExtender runat="server" ID="ceCertificateDate" TargetControlID="txtCertificateDate"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AktaNo" HeaderText="NO AKTA">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtAktaNo" Text='<%# DataBinder.Eval(Container, "DataItem.AktaNo") %>' Enabled="False">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AktaDate" HeaderText="TGL AKTA">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtAktaDate" Text='<%# DataBinder.Eval(Container, "DataItem.AktaDate") %>' Enabled="False"> </asp:TextBox>
                                            <asp:CalendarExtender runat="server" ID="ceAktaDate" TargetControlID="txtAktaDate"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="AssetSeqNo">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblAssetSeqNo" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="ApplicationID">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblApplicationID" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="AssetType">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblAssetType" Text='<%# DataBinder.Eval(Container, "DataItem.AssetType") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="Condition">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LblCondition" Text='<%# DataBinder.Eval(Container, "DataItem.Condition") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="FiduciaFee" HeaderText="Fiducia Fee">
                                        <ItemTemplate>
                                            <%--<uc1:ucnumberformat id="TxtFiduciaFee"  Enabled="False" runat="server" text='<%# DataBinder.Eval(Container, "DataItem.FiduciaFee") %>' />--%>
                                            <uc1:ucnumberformat id="TxtFiduciaFee"  Enabled="true" runat="server" text='<%# DataBinder.Eval(Container, "DataItem.FiduciaFee") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"
                                    Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                            </div>                           
                        </div>
                    </div>
                </div>
                   <div class="form_button">
                    <asp:Button ID="BtnSave" runat="server" Text="Save"  CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
                 <div class="form_title">
            
                <div class="form_single">
                    <h3>
                        CARI REGISTRASI FIDUCIA
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Cari Berdasarkan
                    </label>
                    <uc1:ucsearchby id="oSearchBy" runat="server">
                </uc1:ucsearchby>
                </div>
             
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Aging
                    </label>
                    <asp:TextBox ID="TxtAgingDays" runat="server" Width="72px"></asp:TextBox>&nbsp;Hari
                </div>
                
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Tanggal Registrasi
                    </label>
                    <asp:TextBox runat="server" ID="txtDate"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtenderDate" TargetControlID="txtDate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                        
                </div>
             
            </div>
            <div class="form_button">
                <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
