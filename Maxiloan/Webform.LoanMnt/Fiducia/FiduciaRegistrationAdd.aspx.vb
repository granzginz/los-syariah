﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FiduciaRegistrationAdd
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oNotary As UcMasterNotary
    Protected WithEvents TxtFiduciaFee As ucNumberFormat



    Private Property AssetSeqNo() As String
        Get
            Return CType(viewstate("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property

#Region "Link"

    Function LinkToApplication(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreement(ByVal strStyle As String, ByVal strApplicationId As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "', '" & strApplicationId & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
    Function LinkToAsset(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinAsset('" & strApplicationID & "','" & strStyle & "')"
    End Function

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                BtnSave.Enabled = True
                Dim cookie As HttpCookie = Request.Cookies("FiduciaRegistration")
                Dim ApplicationID As String = cookie.Values("ApplicationID")
                Dim AgreementNo As String = cookie.Values("AgreementNo")
                Dim CustomerID As String = cookie.Values("CustomerID")
                Dim CustomerName As String = cookie.Values("CustomerName")
                Dim AssetDescription As String = cookie.Values("AssetDescription")
                Dim AssetType As String = cookie.Values("AssetType")
                Dim OTRPrice As String = cookie.Values("OTRPrice")
                Dim Condition As String = cookie.Values("Condition")


                Me.AssetSeqNo = cookie.Values("AssetSeqNo")

                LblAgreementNo.Text = AgreementNo
                LblApplicationID.Text = ApplicationID
                LblAssetDescription.Text = AssetDescription
                LblAssetType.Text = AssetType
                lblOTRAmount.Text = OTRPrice
                TxtFiduciaFee.Visible = False

                LblCustomerName.Text = CustomerName
                LblCondition.Text = Condition

                HplinkApplicationID.Text = ApplicationID
                HplinkAgreementNo.Text = AgreementNo
                HpLinkCustName.Text = CustomerName
                HpLinkAssetDescription.Text = AssetDescription

                HplinkApplicationID.NavigateUrl = LinkToApplication(ApplicationID, "ACCMNT")
                'HplinkAgreementNo.NavigateUrl = LinkToAgreement("Insurance", ApplicationID)
                HplinkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(LblApplicationID.Text.Trim) & "')"

                HpLinkCustName.NavigateUrl = LinkToCustomer(CustomerID, "AccMnt")
                HpLinkAssetDescription.NavigateUrl = LinkToAsset(ApplicationID, "AccMnt")

                oNotary.BindNotary()
                oNotary.FillRequired = True

                txtOfferingDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                BtnSave.Visible = True

            Catch ex As Exception
                Response.Write(ex.Message + ex.StackTrace + ex.TargetSite.Name)
                BtnSave.Visible = False
            End Try


        End If

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("FiduciaRegistration.aspx")
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        BtnSave.Visible = True
        Dim oFiduciaController As New FiduciaController
        Dim oEntities As New Parameter.Fiducia
        With oEntities
            Context.Trace.Write("oNotary.NotaryID = " & oNotary.NotaryID)
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .ApplicationID = LblApplicationID.Text.Trim
            .SeqNo = Me.AssetSeqNo
            .NotaryID = oNotary.NotaryID '"N01"
            .RegisterDate = Me.BusinessDate
            .OfferingDate = ConvertDate2(txtOfferingDate.Text)
            .RegisterNotes = TxtNotes.Text
            '.FiduciaFee = CType(TxtFiduciaFee.Text, Double)
            .RegisterBy = Me.Loginid
            .strConnection = GetConnectionString()
        End With
        Try
            oFiduciaController.FiduciaAdd(oEntities)
            Response.Redirect("FiduciaRegistration.aspx")
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace + ex.TargetSite.Name)
            BtnSave.Visible = False
        End Try

    End Sub

    Private Sub ImbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

End Class