﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FiduciaReceipt
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New GeneralPagingController
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            ' InitialDefaultPanel()
            PnlDgrid.Visible = False
            Me.FormID = "FIDUCIA"
            ' If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            oSearchBy.ListData = "NOTARYNAME, Nama Notary-AGREEMENTNO,No. Kontrak-CUSTOMER.NAME, Nama Konsumen -AssetMaster.DESCRIPTION, Nama Asset "
            oSearchBy.BindData()
            Me.SearchBy = ""
            Me.SortBy = ""
            'End If

        End If


    End Sub
#End Region
#Region "Search"
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try

            PnlDgrid.Visible = True
            Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' And Fiducia.Status = 'REQ' "
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
            End If


            If TxtAgingDays.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " And datediff(d,fiducia.OfferingDate,Fiducia.RegisterDate) = '" & TxtAgingDays.Text & "'"
            End If

            Bindgrid(Me.SearchBy, Me.SortBy)

        Catch ex As Exception
            Response.Write(ex.StackTrace + ex.Message)
        End Try

    End Sub
#End Region
#Region "BindGrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal sort As String)
        PnlDgrid.Visible = True
        Dim dtEntity As New DataTable
        Dim oEntitiesCommon As New Parameter.GeneralPaging
        If sort = "" Then sort = "Fiducia.NotaryID"
        Me.SearchBy = cmdWhere
        With oEntitiesCommon
            .SpName = "spPagingFiduciaAktaReceive"
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString
        End With
        Try
            oEntitiesCommon = m_controller.GetGeneralPaging(oEntitiesCommon)
        Catch ex As Exception
            Response.Write(ex.Message + " - " + ex.StackTrace)
        End Try


        If Not oEntitiesCommon Is Nothing Then
            dtEntity = oEntitiesCommon.ListData
            recordCount = oEntitiesCommon.TotalRecords
        Else
            recordCount = 0
        End If

        dtgFiduciaRegListing.DataSource = dtEntity.DefaultView
        dtgFiduciaRegListing.CurrentPageIndex = 0
        dtgFiduciaRegListing.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region
#Region "Sorting "


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgFiduciaRegListing.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "NavigationLink"

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' And Fiducia.Status = 'REQ' "
        '    oSearchBy.Text.Trim = ""
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Item Command"
    Private Sub dtgFiduciaRegListing_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFiduciaRegListing.ItemCommand

        If e.CommandName = "Receive" Then
            Try

                Dim LblAgreementNo As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblAgreementNo"), Label)
                Dim LblApplicationID As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblApplicationId"), Label)
                Dim LblCustomerId As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblCustomerId"), Label)
                Dim LblCustomerName As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblCustomerName"), Label)
                Dim LblAssetDescription As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblAssetDescription"), Label)
                Dim LblAssetType As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblAssetType"), Label)
                Dim LblOTRPrice As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblOTRPrice"), Label)
                Dim LblAssetSeqNo As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblAssetSeqNo"), Label)
                Dim LblCondition As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblCondition"), Label)
                Dim LblNotaryName As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblNotaryName"), Label)
                'Dim LblOfferingDate As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblOfferingDate"), Label)
                Dim LblInvoiceDate As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblInvoiceDate"), Label)
                Dim lblFiduciaFee As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("lblFiduciaFee"), Label)

                Dim LblAktaDate As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblAktaDate"), Label)
                Dim LblCertificateDate As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblCertificateDate"), Label)

                Dim LblAktaNo As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblAktaNo"), Label)
                Dim LblCertificateNo As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblCertificateNo"), Label)
                Dim LblReceiveDate As Label = CType(dtgFiduciaRegListing.Items(e.Item.ItemIndex).FindControl("LblReceiveDate"), Label)

                Dim cookieNew As New HttpCookie("FiduciaReceipt")
                cookieNew.Values.Add("AgreementNo", LblAgreementNo.Text.Trim)
                cookieNew.Values.Add("ApplicationID", LblApplicationID.Text.Trim)
                cookieNew.Values.Add("CustomerID", LblCustomerId.Text.Trim)
                cookieNew.Values.Add("CustomerName", LblCustomerName.Text.Trim)
                cookieNew.Values.Add("AssetDescription", LblAssetDescription.Text.Trim)
                cookieNew.Values.Add("AssetType", LblAssetType.Text.Trim)
                cookieNew.Values.Add("OTRPrice", LblOTRPrice.Text.Trim)
                cookieNew.Values.Add("AssetSeqNo", LblAssetSeqNo.Text.Trim)
                cookieNew.Values.Add("Condition", LblCondition.Text.Trim)
                cookieNew.Values.Add("NotaryName", LblNotaryName.Text.Trim)
                'cookieNew.Values.Add("OfferingDate", LblOfferingDate.Text.Trim)
                cookieNew.Values.Add("InvoiceDate", LblInvoiceDate.Text.Trim)
                cookieNew.Values.Add("FiduciaFee", lblFiduciaFee.Text.Trim)

                cookieNew.Values.Add("AktaDate", LblAktaDate.Text.Trim)
                cookieNew.Values.Add("CertificateDate", LblCertificateDate.Text.Trim)
                cookieNew.Values.Add("AktaNo", LblAktaNo.Text.Trim)
                cookieNew.Values.Add("CertificateNo", LblCertificateNo.Text.Trim)
                cookieNew.Values.Add("ReceiveDate", LblReceiveDate.Text.Trim)

                Response.AppendCookie(cookieNew)
                Response.Redirect("FiduciaPartialAdd.aspx")

            Catch ex As Exception
                Response.Write(ex.Message + ex.StackTrace)
            End Try




        End If


    End Sub
#End Region
#Region "DataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFiduciaRegListing.ItemDataBound
        Try

            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim m As Int32
            If e.Item.ItemIndex >= 0 Then
                '*** Customer Link
                lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
                hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
                '*** Agreement No link
                Dim lblApplicationId As Label
                lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
                hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)

        End Try


    End Sub

#End Region

   
End Class