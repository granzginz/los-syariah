﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class NotaryAddEdit
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property ModeStatus() As String
        Get
            Return CType(viewstate("ModeStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ModeStatus") = Value
        End Set
    End Property
    Private Property BankNameState() As String
        Get
            Return CStr(viewstate("BankNameState"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankNameState") = Value
        End Set
    End Property

#End Region

    Dim Ocontroller As New FiduciaController
    Dim OEntities As New Parameter.Fiducia
    Dim dtNotary As New DataTable
    Protected WithEvents UcNotaryAdress As UcCompanyAddress
    Protected WithEvents UcContactPerson As UcContactPerson
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents UcBankAccountAdd As UcBankAccount    
    Protected WithEvents txtDateMenkeh As ucDateCE


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then

            Me.ModeStatus = Request.QueryString("ModeStatus")
            If Me.ModeStatus = "Add" Then
                TxtNotaryID.Visible = True
                ReqFieldValNotaryID.Enabled = True
                LblNotaryID.Visible = False
                

                With UcNotaryAdress
                    .Style = "AccMnt"
                    .showMandatoryAll()
                    .ValidatorDefault()
                End With
                UcContactPerson.EnabledContactPerson()
                UcBankAccount.ShowMandatoryAll()
                UcBankAccount.ValidatorTrue()
                UcBankAccount.BindBankAccount()
            End If
            If Me.ModeStatus = "Edit" Then
                TxtNotaryID.Visible = False
                ReqFieldValNotaryID.Enabled = False
                Dim NotaryID As String = Request.QueryString("NotaryID")
                LblNotaryID.Text = Replace(NotaryID, "'", "")
                GetDataNotary(NotaryID)
                lblMessage.Visible = False
            End If
        End If

    End Sub

    Private Sub GetDataNotary(ByVal NotaryID As String)

        With OEntities
            .NotaryID = Replace(NotaryID, "'", "")
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .strConnection = GetConnectionString
        End With
        Try
            OEntities = Ocontroller.GetNotary(OEntities)
            dtNotary = OEntities.listdata
            TxtNotaryName.Text = CStr(dtNotary.Rows(0).Item("NotaryName")).Trim
            TxtSKMenkeh.Text = CStr(dtNotary.Rows(0).Item("NoSKMenKeh")).Trim
            txtDateMenkeh.Text = CDate(dtNotary.Rows(0).Item("TglSKMenKeh")).ToString("dd/MM/yyyy")
            Dim oConvertDate As New CommonSimpleRuleHelper            
            TxtNotaryEmail.Text = CStr(dtNotary.Rows(0).Item("Email")).Trim

            With UcNotaryAdress
                .Address = CStr(dtNotary.Rows(0).Item("NotaryAddress")).Trim
                .RT = CStr(dtNotary.Rows(0).Item("NotaryRT")).Trim
                .RW = CStr(dtNotary.Rows(0).Item("NotaryRW")).Trim
                .Kelurahan = CStr(dtNotary.Rows(0).Item("NotaryKelurahan")).Trim
                .Kecamatan = CStr(dtNotary.Rows(0).Item("NotaryKecamatan")).Trim
                .City = CStr(dtNotary.Rows(0).Item("NotaryCity")).Trim
                .ZipCode = CStr(dtNotary.Rows(0).Item("NotaryZipCode")).Trim
                .AreaPhone1 = CStr(dtNotary.Rows(0).Item("NotaryAreaPhone1")).Trim
                .Phone1 = CStr(dtNotary.Rows(0).Item("NotaryPhone1")).Trim
                .AreaPhone2 = CStr(dtNotary.Rows(0).Item("NotaryAreaPhone2")).Trim
                .Phone2 = CStr(dtNotary.Rows(0).Item("NotaryPhone2")).Trim
                .AreaFax = CStr(dtNotary.Rows(0).Item("NotaryAreaFax")).Trim
                .Fax = CStr(dtNotary.Rows(0).Item("NotaryFax")).Trim
                .Style = "AccMnt"
                .BindAddress()
            End With
            With UcContactPerson
                .ContactPerson = CStr(dtNotary.Rows(0).Item("ContactPersonName")).Trim
                .ContactPersonTitle = CStr(dtNotary.Rows(0).Item("ContactPersonTitle")).Trim
                .MobilePhone = CStr(dtNotary.Rows(0).Item("MobilePhone")).Trim
                .Email = CStr(dtNotary.Rows(0).Item("Email")).Trim               
                .BindContacPerson()
            End With

            With UcBankAccount
                .BindBankAccount()
                .BankID = CStr(dtNotary.Rows(0).Item("NotaryBankID")).Trim
                .BankBranch = CStr(dtNotary.Rows(0).Item("NotarybankBranch"))
                .AccountNo = CStr(dtNotary.Rows(0).Item("NotaryAccountNo")).Trim
                .AccountName = CStr(dtNotary.Rows(0).Item("NotaryAccountName")).Trim
                .BankName = CStr(dtNotary.Rows(0).Item("NotaryBankName")).Trim
                .BankBranchId = CStr(dtNotary.Rows(0).Item("NotaryBankBranchID")).Trim
                .BankCodeBank = CStr(dtNotary.Rows(0).Item("NotaryBankID")).Trim
                .BankCodeCabang = CStr(dtNotary.Rows(0).Item("NotaryBankBranchID")).Trim
                .City = CStr(dtNotary.Rows(0).Item("NotaryBankBranchCity")).Trim
            End With

            If CStr(dtNotary.Rows(0).Item("IsActive")).Trim = "0" Then
                ChkStatus.Checked = False
            Else
                ChkStatus.Checked = True
            End If

            If dtNotary.Rows(0).Item("NotaryNPWP").ToString().Length = 0 Then
                TxtNoNPWP.Text = "-"
            Else
                TxtNoNPWP.Text = CStr(dtNotary.Rows(0).Item("NotaryNPWP")).Trim
            End If

        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
        End Try

    End Sub

    Sub SendCookiesBankName()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_BANK_NAME)
        If Not cookie Is Nothing Then
            cookie.Values("InsCoBranchID") = Me.sesBranchId
            cookie.Values("BankName") = Me.BankNameState
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_BANK_NAME)
            cookieNew.Values.Add("InsCoBranchID", Me.sesBranchId)
            cookieNew.Values.Add("BankName", Me.BankNameState)
            Response.AppendCookie(cookieNew)
        End If
    End Sub


    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        BtnSave.Visible = True

        If txtDateMenkeh.Text = "" Then
            txtDateMenkeh.Text = "01/01/1900"
        End If
        If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtDateMenkeh.Text)) >= 1 Then

            ShowMessage(lblMessage, "Tanggal SK Menkumham harus <= tgl Hari ini", True)
            Exit Sub
        End If


        With OEntities
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            If Me.ModeStatus = "Add" Then
                TxtNotaryID.Visible = True
                .NotaryID = TxtNotaryID.Text
            Else
                .NotaryID = LblNotaryID.Text
                TxtNotaryID.Visible = False
            End If
            Context.Trace.Write("TxtNotaryName.Text = " & TxtNotaryName.Text)
            Context.Trace.Write("TxtSKMenkeh.Text = " & TxtSKMenkeh.Text)
            Context.Trace.Write("ChkStatus.Checked = " & ChkStatus.Checked)
            .NotaryName = TxtNotaryName.Text
            .NoSKMenkeh = TxtSKMenkeh.Text
            .TglSKMenkeh = ConvertDate2(txtDateMenkeh.Text)
            .NotaryAddress = UcNotaryAdress.Address
            .NotaryRT = UcNotaryAdress.RT
            .NotaryRW = UcNotaryAdress.RW
            .NotaryKelurahan = UcNotaryAdress.Kelurahan
            .NotaryKecamatan = UcNotaryAdress.Kecamatan
            .NotaryCity = UcNotaryAdress.City
            .NotaryZipCode = UcNotaryAdress.ZipCode
            .NotaryAreaPhone1 = UcNotaryAdress.AreaPhone1
            .NotaryPhone1 = UcNotaryAdress.Phone1
            .NotaryAreaPhone2 = UcNotaryAdress.AreaPhone2
            .NotaryPhone2 = UcNotaryAdress.Phone2
            .NotaryAreaFax = UcNotaryAdress.AreaFax
            .NotaryFax = UcNotaryAdress.Fax
            .EMail = TxtNotaryEmail.Text.Trim
            .MobilePhone = UcContactPerson.MobilePhone
            .ContactPersonName = UcContactPerson.ContactPerson
            .ContactPersonTitle = UcContactPerson.ContactPersonTitle
            .NotaryBankID = UcBankAccount.BankID
            .NotaryBankBranch = UcBankAccount.BankBranch
            .NotaryBankBranchID = UcBankAccount.BankCodeCabang
            .NotaryBankName = UcBankAccount.BankName
            .NotaryAccountName = UcBankAccount.AccountName
            .NotaryAccountNo = UcBankAccount.AccountNo
            .NotaryBankBranchCity = UcBankAccount.City
            .IsActive = ChkStatus.Checked
            .ModeStatus = Me.ModeStatus
            .NotaryNPWP = TxtNoNPWP.Text

        End With

        Try
            Ocontroller.AddNotary(OEntities)
            Response.Redirect("Notary.aspx?back=no&msg=success")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("Notary.aspx?back=yes")
    End Sub

End Class