﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FiduciaRoya
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New GeneralPagingController
    Private m_RoyaController As New FiduciaController
    Dim oFiducia As New Parameter.Fiducia
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

#End Region
#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            PnlDgrid.Visible = False
            oSearchby.ListData = "CERTIFICATENO, CertificateNo-AGREEMENTNO,AgreementNo-CUSTOMER.NAME, Customer Name -AssetMaster.DESCRIPTION, Asset Description "
            oSearchby.BindData()
            Me.SearchBy = ""
            Me.SortBy = ""

        End If
    End Sub

#End Region
#Region "Search"


    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            PnlDgrid.Visible = True
            Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "

            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
            End If

            If cboPrint.SelectedValue = "0" Then
                Me.SearchBy = Me.SearchBy & " And RoyaRegisterNo = '-' "
            Else
                Me.SearchBy = Me.SearchBy & " And RoyaRegisterNo <> '-' "
            End If

            If txtCertificateDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " And CertificateDate = Convert(DateTime,'" & txtCertificateDate.Text & "',103) "
            End If

            Bindgrid(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            Response.Write(ex.StackTrace + ex.Message)
        End Try

    End Sub
#End Region
#Region "BindGrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal sort As String)
        PnlDgrid.Visible = True
        Dim dtEntity As New DataTable
        Dim oEntitiesCommon As New Parameter.GeneralPaging
        If sort = "" Then sort = "Fiducia.ApplicationID"
        Me.SearchBy = cmdWhere
        With oEntitiesCommon
            .SpName = "spFiduciaPagingRoya"
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString
        End With
        Try
            oEntitiesCommon = m_controller.GetGeneralPaging(oEntitiesCommon)
        Catch ex As Exception
            Response.Write(ex.Message + " - " + ex.StackTrace)
        End Try


        If Not oEntitiesCommon Is Nothing Then
            dtEntity = oEntitiesCommon.ListData
            recordCount = oEntitiesCommon.TotalRecords
        Else
            recordCount = 0
        End If

        dtgFiduciaRoya.DataSource = dtEntity.DefaultView
        dtgFiduciaRoya.CurrentPageIndex = 0
        dtgFiduciaRoya.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region
#Region "Sorting "


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgFiduciaRoya.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "NavigationLink"

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click

        Me.SearchBy = " Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "")
        '    oSearchBy.Text.Trim = ""
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "ChkStatus Checked Changed"


    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusApplicationID As CheckBox

        For loopitem = 0 To CType(dtgFiduciaRoya.Items.Count - 1, Int16)
            Context.Trace.Write("loopitem = " & loopitem)
            ChkStatusApplicationID = New CheckBox
            ChkStatusApplicationID = CType(dtgFiduciaRoya.Items(loopitem).FindControl("ChkStatusApplicationID"), CheckBox)

            If ChkStatusApplicationID.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = True ")
                    ChkStatusApplicationID.Checked = True
                Else
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = False ")
                    ChkStatusApplicationID.Checked = False
                End If
            Else
                Context.Trace.Write("ChkStatusAgreementNo.Checked = False")
                ChkStatusApplicationID.Checked = False
            End If
        Next

    End Sub
#End Region
#Region "DataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFiduciaRoya.ItemDataBound
        Try

            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim m As Int32
            If e.Item.ItemIndex >= 0 Then
                '*** Customer Link
                lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
                hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
                '*** Agreement No link
                Dim lblApplicationId As Label
                lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
                hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)

        End Try


    End Sub

#End Region
#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click

        Dim adacheck As Int16 = 0
        Dim iloop As Integer
        Dim chkPrint As CheckBox
        Dim strMultiApplicationID As String = ""
        Dim lblApplicationID As New Label


        For iloop = 0 To dtgFiduciaRoya.Items.Count - 1
            chkPrint = CType(dtgFiduciaRoya.Items(iloop).Cells(0).FindControl("ChkStatusApplicationID"), CheckBox)
            lblApplicationID = CType(dtgFiduciaRoya.Items(iloop).Cells(1).FindControl("lblApplicationID"), Label)


            If chkPrint.Checked Then
                adacheck = 1
                If strMultiApplicationID.Trim = "" Then
                    strMultiApplicationID = strMultiApplicationID + "'" + lblApplicationID.Text.Trim + "'"
                Else
                    strMultiApplicationID = strMultiApplicationID + ",'" + lblApplicationID.Text.Trim + "'"
                End If
                'Berikan Roya RegisterNo
                With oFiducia
                    .BranchId = Replace(Me.sesBranchId, "'", "")
                    .ApplicationID = Replace(lblApplicationID.Text.Trim, "'", "")
                    .BusinessDate = Me.BusinessDate
                    .strConnection = GetConnectionString()
                End With
                Try
                    m_RoyaController.GenerateFiduciaRoyaRegisterNo(oFiducia)
                Catch ex As Exception
                    Response.Write(ex.Message + ex.StackTrace)
                    Exit Sub
                End Try

            End If

        Next

        If adacheck = 0 Then
            Response.Write("Harap periksa kontrak yang dicetak dibawah ini")
            Exit Sub
        End If
        Dim filtercmdwhere As String = " Where fiducia.ApplicationID in(" + strMultiApplicationID + ")"
        Context.Trace.Write("filtercmdwhere = " & filtercmdwhere)

        Dim cookie As HttpCookie = Request.Cookies("FiduciaRoyaPrint")
        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = filtercmdwhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("FiduciaRoyaPrint")
            cookieNew.Values.Add("cmdwhere", filtercmdwhere)

            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("FiduciaRoyaViewer.aspx")



    End Sub
#End Region

 
 
End Class