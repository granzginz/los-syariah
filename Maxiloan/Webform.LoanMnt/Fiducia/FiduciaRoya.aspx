﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FiduciaRoya.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.FiduciaRoya" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FiduciaRoya</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ROYA
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cari Berdasarkan
            </label>
            <uc1:ucsearchbywithnotable id="oSearchby" runat="server">
                </uc1:ucsearchbywithnotable>
        </div>
        <div class="form_right">
            <label>
                Tanggal Sertifikat
            </label>
            <asp:TextBox runat="server" ID="txtCertificateDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtCertificateDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Status Cetak
            </label>
            <asp:DropDownList ID="cboPrint" runat="server">
                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlDgrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR FIDUCIA ROYA
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFiduciaRoya" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="RoyaRegisterNo" HeaderText="NO REGISTER">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblRegisterNo" Text='<%# DataBinder.Eval(Container, "DataItem.RoyaRegisterNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CertificateNo" HeaderText="NO SERTIFIKAT">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblCertificateno" Text='<%# DataBinder.Eval(Container, "DataItem.CertificateNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RoyaDate" HeaderText="TGL ROYA">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblRoyaDate" Text='<%# DataBinder.Eval(Container, "DataItem.RoyaDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAgreementNo" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCustomerName" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDescription" HeaderText="NAMA ASSET">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblAssetDescription" Text='<%# DataBinder.Eval(Container, "DataItem.AssetDescription") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblContractStatus" Text='<%# DataBinder.Eval(Container, "DataItem.ContractStatus") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ACTION">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    <p>
                                        STATUS<br />
                                        <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                            AutoPostBack="True"></asp:CheckBox></p>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <p>
                                        <asp:CheckBox ID="ChkStatusApplicationID" runat="server" Width="67px" Font-Size="Smaller">
                                        </asp:CheckBox><br />
                                        <asp:Label ID="Label1" runat="server" Text='<%#  container.dataitem("ApplicationID") %>'
                                            Visible="False" Font-Size="Smaller">
                                        </asp:Label></p>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="APPLICATION ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblApplicationID" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerID" HeaderText="CustomerID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LblCustomerID" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"
                            CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
