﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class TerimaTagihanFiducia
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New GeneralPagingController
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            ' InitialDefaultPanel()
            PnlDgrid.Visible = False
            Me.FormID = "TMTGFIDUCIA"
            ' If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            oSearchBy.ListData = "NOTARYNAME, Nama Notary-AGREEMENTNO,No. Kontrak-CUSTOMER.NAME, Nama Konsumen -AssetMaster.DESCRIPTION, Nama Asset "
            oSearchBy.BindData()
            Me.SearchBy = ""
            Me.SortBy = ""
            'End If

        End If
    End Sub
#Region "Search"
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try

            PnlDgrid.Visible = True
            Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' And Fiducia.Status = 'REQ' "
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
            End If


            If TxtAgingDays.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " And datediff(d,fiducia.OfferingDate,Fiducia.RegisterDate) = '" & TxtAgingDays.Text & "'"
            End If

            If txtDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " And Fiducia.RegisterDate IN ('" & ConvertDate2(txtDate.Text).ToString("yyyyMMdd") & "')"
            End If

            Bindgrid(Me.SearchBy, Me.SortBy)

        Catch ex As Exception
            Response.Write(ex.StackTrace + ex.Message)
        End Try

    End Sub
#End Region
#Region "BindGrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal sort As String)
        PnlDgrid.Visible = True
        Dim dtEntity As New DataTable
        Dim oEntitiesCommon As New Parameter.GeneralPaging
        If sort = "" Then sort = "Fiducia.NotaryID"
        Me.SearchBy = cmdWhere
        With oEntitiesCommon
            .SpName = "spPagingFiduciaAktaReceive"
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString
        End With
        Try
            oEntitiesCommon = m_controller.GetGeneralPaging(oEntitiesCommon)
        Catch ex As Exception
            Response.Write(ex.Message + " - " + ex.StackTrace)
        End Try


        If Not oEntitiesCommon Is Nothing Then
            dtEntity = oEntitiesCommon.ListData
            'PageSize = oEntitiesCommon.TotalRecords
            recordCount = oEntitiesCommon.TotalRecords
        Else
            recordCount = 0
        End If

        dtgFiduciaRegListing.DataSource = dtEntity.DefaultView
        dtgFiduciaRegListing.CurrentPageIndex = 0
        dtgFiduciaRegListing.DataBind()
        PagingFooter()
    End Sub
#End Region

#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region
#Region "Sorting "


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgFiduciaRegListing.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "NavigationLink"
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' And Fiducia.Status = 'REQ' "
        '    oSearchBy.Text.Trim = ""

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "DataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFiduciaRegListing.ItemDataBound
        Try

            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim m As Int32
          
            
            If e.Item.ItemIndex >= 0 Then
                '*** Customer Link

                lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)

                hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
                '*** Agreement No link
                Dim lblApplicationId As Label
                lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
                hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

             
            End If
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)

        End Try


    End Sub

#End Region
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try

            BtnSave.Visible = True
            Dim oController As New FiduciaController
            For n As Integer = 0 To dtgFiduciaRegListing.DataKeys.Count - 1

                Dim chkItem As CheckBox = CType(dtgFiduciaRegListing.Items(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim TxtFiduciaFee As ucNumberFormat = CType(dtgFiduciaRegListing.Items(n).FindControl("TxtFiduciaFee"), ucNumberFormat)
                    If CType(TxtFiduciaFee.Text, Double) > 0 Then



                        Dim oEntities As New Parameter.Fiducia
                        With oEntities
                            .BranchId = Replace(Me.sesBranchId, "'", "")
                            Dim AppID As Label = CType(dtgFiduciaRegListing.Items(n).FindControl("LblApplicationID"), Label)
                            .ApplicationID = AppID.Text.Trim
                            Dim AssetSeqNo As Label = CType(dtgFiduciaRegListing.Items(n).FindControl("LblAssetSeqNo"), Label)
                            .SeqNo = AssetSeqNo.Text
                            Dim txtAktaNo As TextBox = CType(dtgFiduciaRegListing.Items(n).FindControl("txtAktaNo"), TextBox)
                            .AktaNo = txtAktaNo.Text.Trim
                            Dim txtAktaDate As TextBox = CType(dtgFiduciaRegListing.Items(n).FindControl("txtAktaDate"), TextBox)
                            .AktaDate = ConvertDate2(txtAktaDate.Text)
                            If txtAktaDate.Text <> "" Then
                                .AktaDate = ConvertDate2(txtAktaDate.Text)
                                '.AktaDate = txtAktaDate.Text
                            Else
                                .AktaDate = "1990-01-01"
                            End If
                            Dim txtCertificateNo As TextBox = CType(dtgFiduciaRegListing.Items(n).FindControl("txtCertificateNo"), TextBox)
                            .CertificateNo = txtCertificateNo.Text
                            Dim txtCertificateDate As TextBox = CType(dtgFiduciaRegListing.Items(n).FindControl("txtCertificateDate"), TextBox)
                            '.CertificateDate = ConvertDate2(txtCertificateDate.Text) 
                            If txtCertificateDate.Text <> "" Then
                                .CertificateDate = ConvertDate2(txtCertificateDate.Text)
                            Else
                                .CertificateDate = "1990-01-01"
                            End If

                            .InvoiceNo = TxtInvoiceNo.Text
                            .InvoiceDate = ConvertDate2(txtDateInvoice.Text)
                            .InvoiceNotes = ""

                            .FiduciaFee = CType(TxtFiduciaFee.Text, Double)
                            .RegisterBy = Me.Loginid
                            .BusinessDate = Me.BusinessDate
                            .DueDate = ConvertDate2(txtAPDueDate.Text)
                            .strConnection = GetConnectionString()
                        End With
                        oController.FiduciaAktaReceive(oEntities)
                    Else
                        ShowMessage(lblMessage, "Fiducia Fee tidak boleh kosong", True)

                        Exit Sub


                    End If
                End If

            Next

            Bindgrid(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di simpan", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try



    End Sub
End Class