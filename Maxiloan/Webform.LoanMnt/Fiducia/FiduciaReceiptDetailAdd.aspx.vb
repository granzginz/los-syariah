﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FiduciaReceiptDetailAdd
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents TxtFiduciaFee As ucNumberFormat

#Region "Property"
    Private Property AssetSeqNo() As String
        Get
            Return CType(ViewState("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetSeqNo") = Value
        End Set
    End Property
    Private Property ModeStatus() As String
        Get
            Return CType(ViewState("ModeStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ModeStatus") = Value
        End Set
    End Property
#End Region

#Region "Link"

    Function LinkToApplication(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreement(ByVal strStyle As String, ByVal strApplicationId As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "', '" & strApplicationId & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            With TxtFiduciaFee
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign regular_text"
            End With

            Me.ModeStatus = Request.QueryString("ModeStatus")

            Dim cookie As HttpCookie = Request.Cookies("FiduciaReceipt")
            Dim ApplicationID As String = cookie.Values("ApplicationID")
            Dim AgreementNo As String = cookie.Values("AgreementNo")
            Dim CustomerID As String = cookie.Values("CustomerID")
            Dim CustomerName As String = cookie.Values("CustomerName")
            Dim AssetDescription As String = cookie.Values("AssetDescription")
            Dim AssetType As String = cookie.Values("AssetType")
            Dim OTRPrice As String = cookie.Values("OTRPrice")
            Dim Condition As String = cookie.Values("Condition")
            Dim NotaryName As String = cookie.Values("NotaryName")
            Dim AktaOfferingDate As String = cookie.Values("OfferingDate")
            Me.AssetSeqNo = cookie.Values("AssetSeqno")



            Dim AktaNo As String = cookie.Values("AktaNo")
            Dim RegisterNotes As String = cookie.Values("RegisterNotes")
            Dim AktaDate As String = cookie.Values("AktaDate")
            Dim CertificateNo As String = cookie.Values("CertificateNo")
            Dim CertificateDate As String = cookie.Values("CertificateDate")
            Dim InvoiceNotes As String = cookie.Values("InvoiceNotes")
            Dim InvoiceNo As String = cookie.Values("InvoiceNo")
            Dim InvoiceDate As String = cookie.Values("InvoiceDate")

            Dim FiduciaFee As String = cookie.Values("FiduciaFee")

            Dim dueDate As String = cookie.Values("DueDate")

            cookie.Values.Remove("FiduciaReceipt")

            hplinkApplicationID.Text = ApplicationID
            HplinkAgreementNo.Text = AgreementNo
            HpLinkCustName.Text = CustomerName


            LblAgreementNo.Text = AgreementNo
            LblOTRAmount.Text = OTRPrice
            LblNotaryName.Text = NotaryName
            LblCustomerName.Text = CustomerName
            HpLinkCustName.NavigateUrl = LinkToCustomer(CustomerID, "AccMnt")
            LblAssetType.Text = AssetType
            LblCondition.Text = Condition
            LblAktaOfferingDate.Text = AktaOfferingDate
            LblAssetDescription.Text = AssetDescription
            LblApplicationID.Text = ApplicationID
            HplinkAgreementNo.NavigateUrl = LinkToAgreement("AccMnt", ApplicationID)

            If Me.ModeStatus = "Inquiry" Then

                BtnSave.Visible = False
                BtnSave.Enabled = False
                BtnBack.Visible = True
                BtnBack.Enabled = True
                BtnCancel.Visible = False
                BtnCancel.Enabled = False

                TxtAktaNo.Text = AktaNo
                TxtAktaNo.Enabled = False
                TxtCertificateNo.Text = CertificateNo
                TxtCertificateNo.Enabled = False
                TxtInvoiceNotes.Text = InvoiceNotes
                TxtInvoiceNotes.Enabled = False

                TxtInvoiceNo.Text = InvoiceNo
                TxtInvoiceNo.Enabled = False

                TxtFiduciaFee.Text = FormatNumber(FiduciaFee, 0)
                TxtFiduciaFee.Enabled = False

                hplinkApplicationID.Text = ApplicationID
                HplinkAgreementNo.Text = AgreementNo
                HpLinkCustName.Text = CustomerName

                hplinkApplicationID.NavigateUrl = LinkToApplication(ApplicationID, "AccMnt")
                HpLinkCustName.NavigateUrl = LinkToCustomer(CustomerID, "AccMnt")
                HplinkAgreementNo.NavigateUrl = LinkToAgreement("AccMnt", AgreementNo)

                txtDateCertificate.Text = CertificateDate
                txtDateCertificate.Enabled = False
                txtDateAkta.Text = AktaDate
                txtDateAkta.Enabled = False
                txtDateInvoice.Text = InvoiceDate
                txtDateInvoice.Enabled = False


                txtdueDate.Text = dueDate
                txtdueDate.Enabled = False

            End If

            If Me.ModeStatus = "Receipt" Then
                BtnSave.Visible = True
                BtnSave.Enabled = True
                BtnBack.Visible = False
                BtnBack.Enabled = False
                BtnCancel.Visible = True
                BtnCancel.Enabled = True

                txtDateCertificate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtDateAkta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtDateInvoice.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                hplinkApplicationID.Text = ApplicationID
                HplinkAgreementNo.Text = AgreementNo
                HpLinkCustName.Text = CustomerName

                hplinkApplicationID.NavigateUrl = LinkToApplication(ApplicationID, "AccMnt")
                HpLinkCustName.NavigateUrl = LinkToCustomer(CustomerID, "AccMnt")
                HplinkAgreementNo.NavigateUrl = LinkToAgreement("AccMnt", ApplicationID)

                'oDateCertificate.FillRequired = True
                'oDateAkta.FillRequired = True
                'oDateInvoice.FillRequired = True

                TxtFiduciaFee.Text = FormatNumber(FiduciaFee, 0)
                pnldueDate.Visible = False
            End If

        End If

    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try

            BtnSave.Visible = True
            Dim oController As New FiduciaController
            Dim oEntities As New Parameter.Fiducia
            With oEntities
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .ApplicationID = LblApplicationID.Text.Trim
                .SeqNo = Me.AssetSeqNo
                .AktaNo = TxtAktaNo.Text.Trim
                .AktaDate = ConvertDate2(txtDateAkta.Text)
                .CertificateNo = TxtCertificateNo.Text
                .CertificateDate = ConvertDate2(txtDateCertificate.Text)
                .InvoiceNo = TxtInvoiceNo.Text
                .InvoiceDate = ConvertDate2(txtDateInvoice.Text)
                .InvoiceNotes = TxtInvoiceNotes.Text
                .FiduciaFee = CType(TxtFiduciaFee.Text, Double)
                .RegisterBy = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .strConnection = GetConnectionString()
            End With
            oController.FiduciaAktaReceive(oEntities)
            Response.Redirect("FiduciaReceipt.aspx")
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace + ex.TargetSite.Name)
            BtnSave.Visible = False
        End Try



    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        If Me.ModeStatus = "Inquiry" Then
            Response.Redirect("FiduciaInquiry.aspx")
        End If

        If Me.ModeStatus = "Receipt" Then
            Response.Redirect("FiduciaReceipt.aspx")
        End If


    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        If Me.ModeStatus = "Inquiry" Then
            Response.Redirect("FiduciaInquiry.aspx")
        End If

        If Me.ModeStatus = "Receipt" Then
            Response.Redirect("FiduciaReceipt.aspx")
        End If

    End Sub

End Class