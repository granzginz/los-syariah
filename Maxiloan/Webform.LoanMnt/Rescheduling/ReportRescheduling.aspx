﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportRescheduling.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ReportRescheduling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Rescheduling</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                LAPORAN TRANSAKSI RESCHEDULING
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Cabang</label>
            <asp:DropDownList ID="cboBranch" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                ErrorMessage="Harap pilih Cabang" ControlToValidate="cboBranch" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <label class="label_req">
                        Periode
                    </label>
                    <asp:TextBox ID="txtTglPeriodeFrom" runat="server" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender ID="txtTglPeriodeFrom_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtTglPeriodeFrom" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Isi tanggal!"
                        Display="Dynamic" CssClass="validator_general" ControlToValidate="txtTglPeriodeFrom"></asp:RequiredFieldValidator>
                    s/d
                    <asp:TextBox ID="txtTglPeriodeTo" runat="server" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender ID="txtTglPeriodeTo_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtTglPeriodeTo" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Isi tanggal!"
                        Display="Dynamic" CssClass="validator_general" ControlToValidate="txtTglPeriodeTo"></asp:RequiredFieldValidator>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Produk</label>
            <asp:DropDownList ID="CboProduct" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvCboProduct" runat="server" InitialValue="0" Display="Dynamic"
                ErrorMessage="Harap pilih Produk" ControlToValidate="CboProduct" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="imbViewReport" runat="server" Text="View Report" CssClass="small button blue"
            Enabled="True"></asp:Button>
    </div>
    </form>
</body>
</html>
