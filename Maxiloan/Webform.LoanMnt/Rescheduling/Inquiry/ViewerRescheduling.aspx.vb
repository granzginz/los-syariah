﻿#Region "Import"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class ViewerRescheduling
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Property "
    Property RequestNo() As String
        Get
            Return ViewState("RequestNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("RequestNo") = Value
        End Set
    End Property
    Property Prepaid() As Double
        Get
            Return CDbl(ViewState("Prepaid"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Prepaid") = Value
        End Set
    End Property
    'Property EffectiveDate() As String
    '    Get
    '        Return ViewState("EffectiveDate").ToString
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("EffectiveDate") = Value
    '    End Set
    'End Property
    Private Property MaxSeqNo() As Integer
        Get
            Return (CType(ViewState("MaxSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("MaxSeqNo") = Value
        End Set
    End Property
    Private Property NewNumInst() As Integer
        Get
            Return (CType(ViewState("NewNumInst"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewNumInst") = Value
        End Set
    End Property
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property FRate() As Double
        Get
            Return CDbl(ViewState("FRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("FRate") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(ViewState("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(ViewState("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(ViewState("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(ViewState("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("RejectMinimumIncome") = Value
        End Set
    End Property
    Property ReschedulingFeeBehaviour() As String
        Get
            Return ViewState("ReschedulingFeeBehaviour").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ReschedulingFeeBehaviour") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property

    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Private Property AccruedAmount() As Double
        Get
            Return (CType(ViewState("AccruedAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AccruedAmount") = Value
        End Set
    End Property
    Private Property NewPrinciple() As Double
        Get
            Return (CType(ViewState("NewPrinciple"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NewPrinciple") = Value
        End Set
    End Property
    Private Property ReschedulingFee() As Double
        Get
            Return (CType(ViewState("ReschedulingFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ReschedulingFee") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property

    Private Property sdate() As String
        Get
            Return (CType(ViewState("sdate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(ViewState("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(ViewState("Product"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Product") = Value
        End Set
    End Property
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum
    Private Property Mode() As ProcessMode
        Get
            Return (CType(ViewState("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return CType(ViewState("SeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(ViewState("BindMenu"))
        End Get
        Set(ByVal Value As String)
            ViewState("BindMenu") = Value
        End Set
    End Property

    Property ApplicationModule() As String
        Get
            Return ViewState("ApplicationModule").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationModule") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo").ToString
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private oRescheduling As New Parameter.Rescheduling
    Dim oApplication As New Parameter.Application
    Private m_controller As New ApplicationController
    Private oControllerResc As New DChangeController
    Dim m_controllerFinancial As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim m_controllerResch As New ReschedulingController
    Dim j As Integer = 0
    Dim RunRate As Double
    Dim FlateRate As Double
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim TotAdvAmount As Double
        Dim TotDisc As Double
        Dim PPh As Double
        Dim TotalAmountToBe As Double
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            '  If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            Me.FormID = "RESCHEDULINGEXEC"
            'Me.ApplicationID = Request.QueryString("Applicationid")
            Me.BranchID = Replace(Me.sesBranchId, "'", "")
            Me.RequestNo = Request.QueryString("RequestNo")
            Select Case Request.QueryString("mode")
                Case "0"
                    Me.Mode = ProcessMode.Cancel
                Case "1"
                    Me.Mode = ProcessMode.Execute
                Case "2"
                    Me.Mode = ProcessMode.View
            End Select

            Call DoBind()

            With Entities
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .RequestWONo = Me.RequestNo
            End With
            Entities = m_controllerResch.ViewRescheduling(Entities)
            With Entities

                lblAdmFee.Text = FormatNumber(.AdministrationFee, 2)
                lblEffectiveRate.Text = FormatNumber(.EffectiveRate, 6)
                lbljudul.Text = ConvertDate2(.EffectiveDate)
                Select Case .PaymentFrequency
                    Case "1"
                        lblPaymentFrequency.Text = "Monthly"
                    Case "2"
                        lblPaymentFrequency.Text = "Bimonthly"
                    Case "3"
                        lblPaymentFrequency.Text = "Quarterly"
                    Case "6"
                        lblPaymentFrequency.Text = "Semi Annualy"
                End Select
                lblInstallmentNum.Text = CStr(.NumOfInstallment)
                lblTenor.Text = CStr(.Tenor)
                lblFlatRate.Text = FormatNumber(.FlatRate, 2) & "% per " & lblTenor.Text & " month "
                lblInstallmentAmount.Text = FormatNumber(.installmentamount, 2)
                lblPartialPrepayment.Text = FormatNumber(.PartialPrepaymentAmount, 2)
                lblInstallLC.Text = FormatNumber(.LcInstallment, 2)
                lblInstallCF.Text = FormatNumber(.InstallmentCollFee, 2)
                lblInsuranceLC.Text = FormatNumber(.LcInsurance, 2)
                lblInsuranceCF.Text = FormatNumber(.InsuranceCollFee, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKRenewal.Text = FormatNumber(.STNKRenewalFee, 2)
                lblRepossesionFee.Text = FormatNumber(.RepossessionFee, 2)
                InsuranceCF.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblReason.Text = .ReasonID
                lblToBe.Text = .RequestTo
                lblUcNotes.Text = .Notes
                dtgTC.DataSource = .Data1
                dtgTC.DataBind()
                dtgTC2.DataSource = .data2
                dtgTC2.DataBind()
                dtgCrossDefault.DataSource = .data3
                dtgCrossDefault.DataBind()
                TotAdvAmount = .PartialPrepaymentAmount + .AdministrationFee
                'TotDisc = .LcInstallment + .InstallmentCollFee + .LcInsurance +
                '                     .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee +
                '                     .RepossessionFee + .InsuranceClaimExpense
                TotDisc = .LcInstallment + .InstallmentCollFee + .LcInsurance +
                                     .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee +
                                     .RepossessionFee + .InsuranceClaimExpense + .PPh + .Penalty
                lblTotAdvAmt.Text = FormatNumber(TotAdvAmount, 2)
                lblTotalAmountToBePaid.Text = FormatNumber(TotAdvAmount, 2)
                lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
                lblBalanceAmount.Text = FormatNumber(Me.Prepaid - TotAdvAmount, 2)
                lblTotDiscAmt.Text = FormatNumber(TotDisc, 2)

                'modidfy Nofi 03052019 karena ada PPh 
                'lblNPA.Text = FormatNumber(Me.AccruedAmount - .PartialPrepaymentAmount - TotDisc, 2)

                lblPerjanjianNo.Text = .PerjanjianNo
                PPh = .PPh
                lblPPh.Text = FormatNumber(.PPh, 2)
                lblNPA.Text = FormatNumber(Me.AccruedAmount - .PartialPrepaymentAmount - TotDisc, 2)

                lblPenalty.Text = FormatNumber(.Penalty, 2)

                ViewInstallment(Me.ApplicationID, Me.RequestNo)

            End With
        End If
    End Sub

    Sub DoBind()
        Dim totalPrepayment As Double
        'lbljudul.Text = Me.EffectiveDate
        With oPaymentInfo
            '.ApplicationID = Me.ApplicationID
            '.ValueDate = ConvertDate2(Me.EffectiveDate)
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            .RequestNo = Me.RequestNo.Trim
            '.IsPenaltyTerminationShow = False
            .IsPenaltyTerminationShow = True
            .PaymentInfoView()

            Me.AgreementNo = .AgreementNo
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            Me.ApplicationID = .ApplicationID
            Me.ApplicationModule = .ApplicationModule
            Me.InvoiceNo = .InvoiceNo
            Me.InvoiceSeqNo = .InvoiceSeqNo
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            totalPrepayment = .TotalPrePaymentAmountResch
            lblStopAccruedAmount.Text = FormatNumber(.TotalPrePaymentAmountResch, 2)
            Me.AccruedAmount = totalPrepayment
            Me.Prepaid = .ContractPrepaidAmount
        End With
        If Me.ApplicationModule.Trim = "AUTO" Then
            GetList()
        Else
            GetListFactAndMDKJ()
        End If

    End Sub

#Region "GetList"
    Sub GetListFactAndMDKJ()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.InvoiceNo = Me.InvoiceNo
        oCustomClass.InvoiceSeqNo = Me.InvoiceSeqNo
        'oCustomClass = DController.GetList(oCustomClass)
        oCustomClass = DController.GetListFactAndMU(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            Me.InterestType = .InterestType
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            'Me.GuarantorID = .GuarantorID
            'lblEffRate.Text = CStr(.EffectiveRate)
            lblEffRate.Text = FormatNumber(.EffectiveRate, 6)
            lblPaymentFreq.Text = .PaymentFrequency
            lblPaymentFrequency.Text = .PaymentFrequency
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                    lblPaymentFrequency.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                    lblPaymentFrequency.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                    lblPaymentFrequency.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
                    lblPaymentFrequency.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)
        End With
    End Sub
#End Region

    'Sub DoBind()
    '    Dim totalPrepayment As Double
    '    With Entities
    '        .strConnection = GetConnectionString()
    '        .BranchId = Me.BranchID
    '        .ApplicationID = Me.ApplicationID
    '        .RequestWONo = Me.RequestNo
    '    End With
    '    Entities = m_controllerResch.GetEffectiveDate(Entities)
    '    Me.EffectiveDate = Entities.EffectiveDate.ToString("dd/MM/yyyy")
    '    lbljudul.Text = Me.EffectiveDate
    '    With oPaymentInfo
    '        .ApplicationID = Me.ApplicationID
    '        .ValueDate = ConvertDate2(Me.EffectiveDate)
    '        .PrepaymentType = "DI"
    '        .BranchID = Me.BranchID.Trim
    '        '.IsPenaltyTerminationShow = False
    '        .IsPenaltyTerminationShow = True
    '        .PaymentInfo()
    '        Me.AgreementNo = .AgreementNo
    '        Me.CustomerID = .CustomerID
    '        Me.CustomerType = .CustomerType
    '        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Me.ApplicationID.Trim & "')"
    '        lblAgreementNo.Text = .AgreementNo
    '        lblCustName.Text = .CustomerName
    '        lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Me.CustomerID.Trim & "')"
    '        totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
    '        lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
    '        Me.AccruedAmount = totalPrepayment
    '        Me.Prepaid = .ContractPrepaidAmount
    '    End With
    '    GetList()
    'End Sub
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = DController.GetList(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            'lblEffRate.Text = CStr(.EffectiveRate)
            lblEffRate.Text = FormatNumber(.EffectiveRate, 6)
            Me.ReschedulingFee = .ReschedulingFee
            Me.ReschedulingFeeBehaviour = .ReschedulingFeeBehaviour
            Me.Product = .ProductId
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)

        End With
        oCustomClass.CustomerID = Me.CustomerID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetCust(oCustomClass)
        Me.CustomerType = oCustomClass.CustomerType
    End Sub

    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgCrossDefault_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCrossDefault.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCDNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub ViewInstallment(ByVal ApplicationID As String, ByVal RequestNo As String)
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAmortisasiReschFactAndMU"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = ApplicationID.Trim
            objCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 20).Value = RequestNo.Trim
            objReader = objCommand.ExecuteReader

            dtgViewInstallment.DataSource = objReader
            dtgViewInstallment.DataBind()
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

End Class