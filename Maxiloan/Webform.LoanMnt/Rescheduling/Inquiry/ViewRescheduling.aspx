﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewRescheduling.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ViewRescheduling" %>

<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewRescheduling</title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';		   
    </script>
</head>
<body>
    <form id="Form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                RESCHEDULING VIEW
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Agreement No.</label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Customer Name</label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                CURRENT DATA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Interest Type</label>
            <asp:Label ID="lblInterestType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Product</label>
            <asp:Label ID="lblProduct" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Installment Scheme</label>
            <asp:Label ID="lblInstallScheme" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Package</label>
            <asp:Label ID="lblPackage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Finance Type</label>
            <asp:Label ID="lblFinanceType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Guarantor</label>
            <asp:Label ID="lblGuarantor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Effective Rate/Year</label>
            <asp:Label ID="lblEffRate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Installment#</label>
            <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Payment Frequency</label>
            <asp:Label ID="lblPaymentFreq" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Rescheduling#</label>
            <asp:Label ID="lblReschedNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                ACCOUNT RECEIVABLE AS OF</label>
            <asp:Label ID="lbljudul" runat="server"></asp:Label>
        </div>
    </div>
    <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server"></uc1:ucfullprepayinfo>
    <div class="form_box">
        <div class="form_single">
            <label>
                Total OS Over Due</label>
            <asp:Label ID="lblStopAccruedAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                ADVANCE AMOUNT
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Partial Prepayment</label>
            <asp:Label ID="lblPartialPrepayment" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Install. Late Charges</label>
            <asp:Label ID="lblInstallLC" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Administration Fee</label>
            <asp:Label ID="lblAdmFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Install. Collection Fee</label>
            <asp:Label ID="lblInstallCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Insurance Late Charges</label>
            <asp:Label ID="lblInsuranceLC" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Insurance Collection Fee</label>
            <asp:Label ID="lblInsuranceCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                PDC Bounce Fee</label>
            <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                STNK Renewal Fee</label>
            <asp:Label ID="lblSTNKRenewal" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Insurance Claim Fee</label>
            <asp:Label ID="InsuranceCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Repossession Fee</label>
            <asp:Label ID="lblRepossesionFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Total Advance Amount</label>
            <asp:Label ID="lblTotAdvAmt" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Total Discount</label>
            <asp:Label ID="lblTotDiscAmt" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                NEW FINANCIAL STRUCTURE
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                New Principle Amount</label>
            <asp:Label ID="lblNPA" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Effective Rate (%)</label>
            <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Flat Rate / Year</label>
            <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Payment Frequency</label>
            <asp:Label ID="lblPaymentFrequency" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Number of Installment</label>
            <asp:Label ID="lblInstallmentNum" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tenor</label>
            <asp:Label ID="lblTenor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Installment Amount</label>
            <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                Cross Default
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCrossDefault" runat="server" Width="100%" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyField="Agreement" BorderStyle="None" BorderWidth="0"
                    CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblCDNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="AGREEMENT">
                            <ItemTemplate>
                                <asp:Label ID="lblCDAgreementNo" runat="server" Text='<%#Container.DataItem("Agreement")%> '
                                    Visible="false">
                                </asp:Label>
                                <asp:Label ID="lblCDApplicationID" Text='<%#Container.DataItem("CrossDefaultApplicationId")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAME">
                            <ItemTemplate>
                                <asp:Label ID="lblCDName" Text='<%#Container.DataItem("Name")%>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="AgreementDate" SortExpression="AgreementDate" HeaderText="AGREEMENT DATE"
                            DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="DEFAULT STATUS">
                            <ItemTemplate>
                                <asp:Label ID="lblCDDefaultStatus" Text='<%#Container.DataItem("DefaultStatus")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CONTRACT STATUS">
                            <ItemTemplate>
                                <asp:Label ID="lblCDContractStatus" Text='<%#Container.DataItem("ContractStatus")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DELETE" Visible="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbCDDelete" CommandName="CDDelete" runat="server" ImageUrl="../../../images/icondelete.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                Term and Condition
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyField="TCName" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOCUMENT"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CHECKED">
                            <ItemTemplate>
                                <asp:CheckBox disabled ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.IsChecked") %>'
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NOTES">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                Term and Condition Check List
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC2" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyField="TCName" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOCUMENT"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CHECKED">
                            <ItemTemplate>
                                <asp:CheckBox Enabled="false" ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.IsChecked") %>'
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NOTES">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes2" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlApproval" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Rescheduling Request - Detail
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Reason</label>
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approved By</label>
                <asp:Label ID="lblToBe" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Notes</label>
                <asp:Label ID="lblUcNotes" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Total Amount to be Paid
                </label>
                <asp:Label ID="lblTotalAmountToBePaid" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Prepaid Amount</label>
                <asp:Label ID="lblPrepaidAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Balance</label>
                <asp:Label ID="lblBalanceAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="buttonClose" runat="server" OnClientClick="Close();" Text="Close"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
