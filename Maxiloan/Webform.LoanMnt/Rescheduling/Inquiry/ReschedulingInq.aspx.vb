﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class ReschedulingInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("filterby"))
        End Get
        Set(ByVal Value As String)
            viewstate("filterby") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property SeqNo() As Integer
        Get
            Return CType(Viewstate("SeqNo"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("SeqNo") = Value
        End Set
    End Property
    Property AssetSeqno() As Integer
        Get
            Return CType(Viewstate("AssetSeqno"), Integer)

        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqno") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New GeneralPagingController
    Private oCustomClass As New Parameter.GeneralPaging
    Private m_controller As New DataUserControlController
    Protected WithEvents EffDate As ValidDate
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("file") <> "" Then
                Dim strFileLocation As String
                strFileLocation = "../../../XML/" & Request.QueryString("file")

                Response.Write("<script language = javascript>" & vbCrLf _
                                & "var x = screen.width; " & vbCrLf _
                                & "var y = screen.height; " & vbCrLf _
               & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
               & "</script>")
            End If

            Me.FormID = "RESCHEDULINGINQ"
            pnlSearch.Visible = True
            pnlDatagrid.Visible = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region " BindGrid"
    Sub BindGrid()
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
            .SpName = "spReschedulinglInquiryPaging"
        End With

        oCustomClass = oController.GetGeneralPaging(oCustomClass)

        recordCount = oCustomClass.TotalRecords
        dtgPaging.DataSource = oCustomClass.ListData
        Try
            dtgPaging.DataBind()
        Catch
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        End Try
        dtgPaging.Visible = True
        pnlDatagrid.Visible = True
        PagingFooter()
    End Sub
#End Region
#Region "linkTo"
    Function LinkToViewRequestNo(ByVal strStyle As String, ByVal strRequestNo As String) As String
        Return "javascript:OpenWinViewRequestNaNo('" & strStyle & "','" & strRequestNo & "')"
    End Function
#End Region
   
    Private Sub buttonSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim strSearchBy As New StringBuilder
            Dim strfilterby As New StringBuilder
            pnlDatagrid.Visible = True
            If cboSearch.SelectedItem.Value.Trim <> "0" Then
                If txtSearch.Text.Trim <> "" Then
                    If cboSearch.SelectedItem.Value.Trim = "1" Then
                        If IsNumeric(txtSearch.Text) And Len(txtSearch.Text) <= 5 Then
                            strSearchBy.Append("Rescheduling.SeqNo like '" & txtSearch.Text.Trim & "' and ")
                    Else
                        Exit Sub
                    End If
                        strfilterby.Append("Request No. = " & txtSearch.Text.Trim & " and ")
                    ElseIf cboSearch.SelectedItem.Value.Trim = "2" Then
                        strSearchBy.Append("Agreement.AgreementNo like '" & txtSearch.Text.Trim & "' and ")
                        strfilterby.Append("Agreement No. = " & txtSearch.Text.Trim & " and ")
                    ElseIf cboSearch.SelectedItem.Value.Trim = "3" Then
                        If Not IsNumeric(txtSearch.Text.Trim) Then
                            strSearchBy.Append("Customer.Name like '" & txtSearch.Text.Trim & "' and ")
                            strfilterby.Append("Customer Name = " & txtSearch.Text.Trim & " and ")
                    End If
                    End If
                End If
            End If
            If txtEffDate.Text.Trim <> "" Then
                strSearchBy.Append("Rescheduling.RequestDate <= '" & ConvertDate2(txtEffDate.Text.Trim).ToString("yyyyMMdd") & "' and ")
                strfilterby.Append("Effective Date <= " & txtEffDate.Text.Trim & " and ")
            End If
            If CboStatus.SelectedItem.Value.Trim <> "0" Then
                strSearchBy.Append("Rescheduling.Status = '" & CboStatus.SelectedItem.Value.Trim & "' and ")
                strfilterby.Append("Status = " & CboStatus.SelectedItem.Text.Trim & " and ")
            End If
            If cboParent.SelectedItem.Value.Trim <> "ALL" Then
                strSearchBy.Append("Rescheduling.branchID = '" & cboParent.SelectedItem.Value.Trim & "'")
                'Else
                '    If strSearchBy.ToString.Trim <> "" Then
                '        strSearchBy = Left(SearchBy, Len(SearchBy.Trim) - 4)
                '    End If
            End If
            Me.SearchBy = strSearchBy.ToString
            'If strfilterby.ToString <> "" Then
            '    filterby = Left(filterby, Len(filterby.Trim) - 4)
            'End If
            Me.FilterBy = strSearchBy.ToString
            BindGrid()
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If

    End Sub
    Private Sub buttonReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        Response.Redirect("AssetReplacementInquiry.aspx")
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim hyAssetSetReQno As HyperLink
        Dim HyAgreementNo As HyperLink
        Dim HyCustomerName As HyperLink
        Dim hyRequestNo As HyperLink
        Dim lblCustID As Label
        Dim lblApplicationId, lblEffectiveDate, lblRequestNo As Label
        Dim LblBranchID As Label
        Dim lblSeqNo As Label
        Dim lblView As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblView = CType(e.Item.FindControl("lnkview"), HyperLink)
            LblBranchID = CType(e.Item.FindControl("LblBranchID"), Label)
            HyCustomerName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)
            HyAgreementNo = CType(e.Item.FindControl("HyAgreementNo"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblSeqNo = CType(e.Item.FindControl("lblSeqNo"), Label)
            lblEffectiveDate = CType(e.Item.FindControl("lblEffectiveDate"), Label)
            lblRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label)

            Me.BranchID = LblBranchID.Text.Trim
            Me.ApplicationID = lblApplicationId.Text.Trim
            Me.Seqno = CInt(lblSeqNo.Text)

            If lblView.Text.Trim.Length > 0 Then
                'lblView.NavigateUrl = "javascript:OpenWinViewRescheduling('" & "AccMnt" & "', '" & LblBranchID.Text.Trim & " ','" & Server.UrlEncode(lblApplicationId.Text.Trim) & "', '" & Server.UrlEncode(lblRequestNo.Text.Trim) & "')"
                lblView.NavigateUrl = "~/Webform.LoanMnt/Rescheduling/ReschedullingExec.aspx?no=" & lblRequestNo.Text & "&effectivedate=" & lblEffectiveDate.Text.Trim & "&mode=2&seqno=" & CStr(lblSeqNo.Text.Trim) & "&applicationid=" & Server.UrlEncode(lblApplicationId.Text.Trim) & "&branchid=" & Me.BranchID & "&customerid=" & Server.UrlEncode(CStr(lblCustID.Text.Trim))
                'hypbpkb.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewCollateralBPKB.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
            End If
            If HyAgreementNo.Text.Trim.Length > 0 Then
                HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
            If lblCustID.Text.Trim.Length > 0 Then
                HyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
        End If
    End Sub
    Private Sub dtgPaging_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
End Class