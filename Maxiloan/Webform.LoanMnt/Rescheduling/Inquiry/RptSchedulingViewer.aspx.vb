﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper


Public Class RptSchedulingViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property ReportType() As String
        Get
            Return CType(viewstate("ReportType"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("ReportType") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BranchFullName() As String
        Get
            Return CType(viewstate("BranchFullName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchFullName") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private m_controllerUC As New DataUserControlController
    Private m_controller As New WriteOffController
    Private oCustomClass As New Parameter.WriteOff
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub BindReport()
        GetCookies()        

        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPPrint As New DataSet        
        Dim intr As Integer = 0
        Dim oReport As rptReschedulling = New rptReschedulling


        With oCustomClass
            .strConnection = GetConnectionString
            .Search = Me.SearchBy
            .SpName = "spReschedullingInquiryReport"
        End With

        oCustomClass = m_controller.WOInquiryReport(oCustomClass)

        oReport.SetDataSource(oCustomClass.ListDataReport)
        oReport.SetParameterValue(0, Me.sesCompanyName)
        oReport.SetParameterValue(1, Me.BranchFullName)
        oReport.SetParameterValue(2, Me.Loginid)
        oReport.SetParameterValue(3, Me.FilterBy)
        CRVRescheduling.ReportSource = oReport
        CRVRescheduling.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "PrintReschedulling" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "PrintReschedulling.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("ReschedulingInq.aspx?file=" & Me.Session.SessionID + Me.Loginid + "PrintReschedulling.pdf")
    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("COOKIES_RESCHEDULING")
        'Me.BranchID = cookie.Values("BranchID")
        Me.SearchBy = cookie.Values("SearchBy")
        Me.BranchFullName = cookie.Values("BranchName")
        Me.FilterBy = cookie.Values("FilterBy")
        Me.Loginid = cookie.Values("LoginID")
        Me.ReportType = cookie.Values("ReportType")
    End Sub   

End Class