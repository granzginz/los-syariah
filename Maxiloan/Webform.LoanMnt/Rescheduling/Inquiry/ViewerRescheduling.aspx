﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewerRescheduling.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ViewerRescheduling" %>

<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../../Webform.UserController/ucApprovalRequest.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewRescheduling</title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';		   
    </script>
</head>
<body>
    <form id="Form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                RESCHEDULING VIEW
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DATA SAAT INI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Margin
            </label>
            <asp:Label ID="lblInterestType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Produk
            </label>
            <asp:Label ID="lblProduct" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Skema Angsuran
            </label>
            <asp:Label ID="lblInstallScheme" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Package
            </label>
            <asp:Label ID="lblPackage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Finance
            </label>
            <asp:Label ID="lblFinanceType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Penjamin
            </label>
            <asp:Label ID="lblGuarantor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Margin Effective (%)
            </label>
            <asp:Label ID="lblEffRate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Lama Angsuran
            </label>
            <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Pola Pembayaran
            </label>
            <asp:Label ID="lblPaymentFreq" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Rescheduling
            </label>
            <asp:Label ID="lblReschedNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                A/R PER TANGGAL
                <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server">
                </uc1:ucfullprepayinfo>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Jatuh Tempo
            </label>
            <asp:Label ID="lblStopAccruedAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                JUMLAH DIBAYAR DIMUKA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Pelunasan Sebagian
            </label>
            <asp:Label ID="lblPartialPrepayment" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan Angsuran
            </label>
            <asp:Label ID="lblInstallLC" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Administrasi
            </label>
            <asp:Label ID="lblAdmFee" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Angsuran
            </label>
            <asp:Label ID="lblInstallCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan Asuransi
            </label>
            <asp:Label ID="lblInsuranceLC" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Asuransi
            </label>
            <asp:Label ID="lblInsuranceCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Perpanjangan STNK/BBN
            </label>
            <asp:Label ID="lblSTNKRenewal" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Klaim Asuransi
            </label>
            <asp:Label ID="InsuranceCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblRepossesionFee" runat="server"></asp:Label>
        </div>
    </div>

     <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Penalty
            </label>
            <asp:Label ID="lblPenalty" runat="server"></asp:Label>
        </div>
    </div>

     <div class="form_box">
        <div class="form_left"> 
             <label>
                Surat Perjanjian
            </label>
            <asp:Label ID="lblPerjanjianNo" runat="server" CssClass="numberAlign label"></asp:Label> 
        </div>
        <div class="form_right">
            <label>
                PPh (Perjanjian Penyelesaian Hutang)
            </label>
            <asp:Label ID="lblPPh" runat="server"></asp:Label> 
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">
            <label>
                Total Dibayar Dimuka
            </label>
            <asp:Label ID="lblTotAdvAmt" runat="server" align="right"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Total Discount
            </label>
            <asp:Label ID="lblTotDiscAmt" runat="server" align="right"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                STRUKTUR FINANCIAL BARU
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jumlah Pokok Baru
            </label>
            <asp:Label ID="lblNPA" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Margin Effective (%)
            </label>
            <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Margin Flat
            </label>
            <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Pola Pembayaran
            </label>
            <asp:Label ID="lblPaymentFrequency" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lama Angsuran
            </label>
            <asp:Label ID="lblInstallmentNum" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jangka waktu
            </label>
            <asp:Label ID="lblTenor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left">
            <label>
                Jumlah Angsuran
            </label>
            <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>

     <asp:Panel ID="pnlViewST" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgViewInstallment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Installment">
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallment" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>                            
                            <asp:Label ID="lblTotInstallmentAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                 <FooterTemplate>
                                      <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblInterestAmount" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.InterestAmount"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                        <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                                    </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblOsPrincipal" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"), 2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                  <FooterTemplate>
                                        <asp:Label ID="lblTotOSPrincipal" runat="server"></asp:Label>
                                    </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="LblOSInterest" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"), 2) %>'>
                                    </asp:Label>  
                                </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotOSInterest" runat="server"></asp:Label>
                                    </FooterTemplate> 
                            </asp:TemplateColumn> 
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>

<div class="form_box_hide">
    <div class="form_title">
        <div class="form_single">
            <h4>
                CROSS DEFAULT
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCrossDefault" runat="server" AutoGenerateColumns="False" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblCDNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NO KONTRAK">
                            <ItemTemplate>
                                <asp:Label ID="lblCDAgreementNo" runat="server" Text='<%#Container.DataItem("Agreement")%> '
                                    Visible="false">
                                </asp:Label>
                                <asp:Label ID="lblCDApplicationID" Text='<%#Container.DataItem("CrossDefaultApplicationId")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                            <ItemTemplate>
                                <asp:Label ID="lblCDName" Text='<%#Container.DataItem("Name")%>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="AgreementDate" SortExpression="AgreementDate" HeaderText="TGL KONTRAK"
                            DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="STATUS DEFAULT">
                            <ItemTemplate>
                                <asp:Label ID="lblCDDefaultStatus" Text='<%#Container.DataItem("DefaultStatus")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS KONTRAK">
                            <ItemTemplate>
                                <asp:Label ID="lblCDContractStatus" Text='<%#Container.DataItem("ContractStatus")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DELETE" Visible="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbCDDelete" CommandName="CDDelete" runat="server" ImageUrl="../../../images/icondelete.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                    DataKeyField="TCName">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.Eval(Container, "DataItem.IsChecked") %>'
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes" Text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI CHECK LIST
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC2" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                    DataKeyField="TCName" PageSize="3">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox disabled ID="chkTCCheck2" Checked='<%# DataBinder.Eval(Container, "DataItem.IsChecked") %>'
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes2" Text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</div>
    <asp:Panel ID="pnlApproval" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL RESCHEDULING
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Alasan
                </label>
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="lblToBe" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblUcNotes" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    <strong>Total yg harus dibayar: </strong>
                </label>
                <strong>
                    <asp:Label ID="lblTotalAmountToBePaid" runat="server"></asp:Label></strong>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Prepaid:
                </label>
                <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Saldo :
                </label>
                <asp:Label ID="lblBalanceAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </asp:Panel>
    <div class="form_button"> 
        <asp:Button ID="buttonClose" runat="server" OnClientClick="Close();" Text="Close"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>