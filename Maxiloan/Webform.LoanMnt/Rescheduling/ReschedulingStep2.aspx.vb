﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ReschedulingStep2
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents txtTenor As ucNumberFormat
    Protected WithEvents oPaymentInfo As UcPaymentInfo
#Region "Property"
    Private Property NewTenor() As Integer
        Get
            Return (CType(Viewstate("NewTenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewTenor") = Value
        End Set
    End Property

    Private Property NextInstallmentNumber() As Integer
        Get
            Return (CType(Viewstate("NextInstallmentNumber"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NextInstallmentNumber") = Value
        End Set
    End Property
    Private Property InsSeqNo() As Integer
        Get
            Return (CType(Viewstate("InsSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("InsSeqNo") = Value
        End Set
    End Property
    Private Property MaxSeqNo() As Integer
        Get
            Return (CType(Viewstate("MaxSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("MaxSeqNo") = Value
        End Set
    End Property
    Private Property NewNumInst() As Integer
        Get
            Return (CType(Viewstate("NewNumInst"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewNumInst") = Value
        End Set
    End Property
    Private Property Tenor() As Integer
        Get
            Return (CType(Viewstate("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("Tenor") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(Viewstate("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("SeqNo") = Value
        End Set
    End Property
    Private Property DueDate() As Date
        Get
            Return (CType(Viewstate("DueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("DueDate") = Value
        End Set
    End Property
    Private Property CurrDueDate() As Date
        Get
            Return (CType(Viewstate("CurrDueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("CurrDueDate") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private oControllerResc As New DChangeController
    Private oCustomClassResh As New Parameter.FinancialData
    Private m_ControllerResc As New ReschedulingController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Visible = False
        Page.Header.DataBind()
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "RESCHEDULINGREQ"
        If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                With txtTenor
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = True
                    .TextCssClass = "numberAlign regular_text"
                    .RangeValidatorMinimumValue = "0"
                    .RangeValidatorMaximumValue = "999999"
                End With

                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("BranchID")
                oPaymentInfo.IsTitle = True
                'sdate.isCalendarPostBack = False
                'sdate.FieldRequiredMessage = "Harap isi Tanggal Efektif"
                'sdate.ValidationErrMessage = "harap isi Tanggal efektif dengan format dd/MM/yyyy"
                'sdate.Display = "Dynamic"
                DoBind()
                rdoSTTYpe.SelectedIndex = 0

                oCustomClassResh.strConnection = GetConnectionString()
                oCustomClassResh.ApplicationID = Me.ApplicationID
                oCustomClassResh.BusinessDate = Me.BusinessDate
                oCustomClassResh.BranchId = Me.BranchID
                oCustomClassResh = m_ControllerResc.GetMinDueDate(oCustomClassResh)
                With oCustomClassResh
                    Me.SeqNo = .SeqNo
                    Me.DueDate = .DueDate
                    Me.Tenor = .Tenor
                    Me.MaxSeqNo = .MaxSeqNo
                    Me.NewNumInst = .NewNumInst

                End With
                If Me.BusinessDate >= Me.DueDate Then
                    txtTglEfektif.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                Else
                    txtTglEfektif.Text = DateAdd(DateInterval.Month, -1, Me.DueDate).ToString("dd/MM/yyyy")
                End If
                txtTenor.Text = CStr(Me.Tenor)
            End If
        End If
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        Dim totalPrepayment As Double
        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With
        With oPaymentInfo
            .IsTitle = True
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        GetList()
    End Sub
#End Region

#Region "GetList"
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = DController.GetList(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            lblEffRate.Text = CStr(.EffectiveRate)
            lblPaymentFreq.Text = .PaymentFrequency
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)
            Me.NextInstallmentNumber = .NextInstallmentNumber
        End With
    End Sub
#End Region

#Region "Next"
    Private Sub imbNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbNext.Click
        Dim tempdate As Date
        Call RangeValidatorBind()
        tempdate = DateAdd(DateInterval.Month, -1, Me.DueDate)

        'If ConvertDate2(txtTglEfektif.Text) >= tempdate And ConvertDate2(txtTglEfektif.Text) <= Me.BusinessDate Then
        If ConvertDate2(txtTglEfektif.Text) <= Me.BusinessDate Then
            'If ConvertDate2(txtTglEfektif.Text) = Me.BusinessDate Then
            'Me.NewTenor = CInt(txtTenor.Text) - (Me.SeqNo - 1)

            'belum jadi dipakai by nofi 20200612
            'If Me.DueDate < ConvertDate2(txtTglEfektif.Text) And Me.NewNumInst = 1 Then
            '    ShowMessage(lblMessage, "Tanggal efektif harus lebih besar dari tanggal jatuh tempo angsuran pertama belum dibayar ", True)
            '    Exit Sub
            'End If


            'fix
            Me.NewTenor = CInt(txtTenor.Text)
            Dim cookie As HttpCookie = Request.Cookies("Rescheduling")
            If cboInstallment.SelectedItem.Value = "RF" Or cboInstallment.SelectedItem.Value = "IR" Or cboInstallment.SelectedItem.Value = "EP" Then
                rdoSTTYpe.SelectedIndex = 0
            End If
            If Not cookie Is Nothing Then
                cookie.Values("sdate") = txtTglEfektif.Text.Trim
                cookie.Values("InstallmentScheme") = cboInstallment.SelectedItem.Value
                cookie.Values("Tenor") = txtTenor.Text
                cookie.Values("MaxSeqNo") = CStr(Me.MaxSeqNo)
                cookie.Values("NewNumInst") = CStr(Me.NewNumInst)
                cookie.Values("SeqNo") = CStr(Me.SeqNo)
                cookie.Values("Type") = rdoSTTYpe.SelectedItem.Value
                cookie.Values("NewTenor") = CStr(Me.NewTenor)
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("Rescheduling")
                cookieNew.Values.Add("sdate", txtTglEfektif.Text.Trim)
                cookieNew.Values.Add("InstallmentScheme", cboInstallment.SelectedItem.Value)
                cookieNew.Values.Add("Tenor", txtTenor.Text)
                cookieNew.Values.Add("MaxSeqNo", CStr(Me.MaxSeqNo))
                cookieNew.Values.Add("NewNumInst", CStr(Me.NewNumInst))
                cookieNew.Values.Add("SeqNo", CStr(Me.SeqNo))
                cookieNew.Values.Add("NewTenor", CStr(Me.NewTenor))
                cookieNew.Values.Add("Type", rdoSTTYpe.SelectedItem.Value)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("ReschedulingProc.aspx?Applicationid=" & Me.ApplicationID & "&branchID=" & Me.BranchID.Trim)

        Else

			ShowMessage(lblMessage, "Tanggal efektif harus lebih besar dari tanggal jatuh tempo angsuran belum dibayar (" & _
                        tempdate.ToString("dd/MM/yyyy") & ") dan kurang dari tanggal sistem (" & Me.BusinessDate.ToString("dd/MM/yyyy") & ")", True)
            Exit Sub
        End If

    End Sub
#End Region

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("ReschedulingList.aspx")
    End Sub
    Private Sub RangeValidatorBind()
        txtTenor.RangeValidatorMinimumValue = CStr(Me.NextInstallmentNumber + 1)
    End Sub

End Class