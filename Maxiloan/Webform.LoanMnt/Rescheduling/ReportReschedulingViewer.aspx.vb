﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class ReportReschedulingViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controllerUC As New DataUserControlController
    Private m_controller As New WriteOffController
    Private oCustomClass As New Parameter.WriteOff
#End Region
#Region "Property"
    Private Property Product() As String
        Get
            Return CType(viewstate("Product"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Product") = Value
        End Set
    End Property
    Private Property PODateFrom() As String
        Get
            Return CType(viewstate("PODateFrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PODateFrom") = Value
        End Set
    End Property
    Private Property PODateTo() As String
        Get
            Return CType(viewstate("PODateTo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PODateTo") = Value
        End Set
    End Property
    Private Property SerialNo1Label() As String
        Get
            Return CType(viewstate("SerialNo1Label"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SerialNo1Label") = Value
        End Set
    End Property
    Private Property SerialNo2Label() As String
        Get
            Return CType(viewstate("SerialNo2Label"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SerialNo2Label") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()

    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("COOKIES_REPORT")
        'Me.PODateFrom = ConvertDate2(cookie.Values("PODateFrom")).ToString("yyyyMMdd")
        'Me.PODateTo = ConvertDate2(cookie.Values("PODateTo")).ToString("yyyyMMdd")
        Me.PODateFrom = cookie.Values("PODateFrom")
        Me.PODateTo = cookie.Values("PODateTo")
        Me.BranchID = cookie.Values("BranchID")
        'Me.BranchName = cookie.Values("BranchName")
        Me.Product = cookie.Values("Product")
        Me.SearchBy = cookie.Values("SearchBy")
    End Sub
    Sub BindReport()
        GetCookies()

        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPPrint As New DataSet
        Dim intr As Integer = 0
        Dim oReport As rptReschedulingReport = New rptReschedulingReport
        'Dim dtvEntity As New DataView
        'Dim dtsEntity As New DataTable
        Dim dsSPAsset As New DataSet
        'Dim strPODateFrom As String = ""
        'Dim strPODateTo As String = ""
        'Dim dtPODateFrom As DateTime
        'Dim dtPODateTo As DateTime
        'Dim oReport As rptReschedulingReport = New rptReschedulingReport
        'Dim numCopies As Int16 = 1
        'Dim Collate As Boolean = False
        'Dim startpage As Int16 = 1
        'Try

        With oCustomClass
            .strConnection = GetConnectionString()
            .Search = Me.SearchBy
            .SpName = "spListReschedulling"
        End With

        oCustomClass = m_controller.WOInquiryReport(oCustomClass)

        'dsSPAsset = oCustomClass.ListDataReport
        'oReport.SetDataSource(dsSPAsset)

        'CRVReschedulingReport.HasDrillUpButton = False
        'CRVReschedulingReport.EnableDrillDown = False
        'CRVReschedulingReport.DisplayGroupTree = False
        'CRVReschedulingReport.ReportSource = oReport
        'CRVReschedulingReport.Visible = True
        'CRVReschedulingReport.DataBind()

        'Dim discrete As ParameterDiscreteValue
        'Dim ParamFields As ParameterFields
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("CompanyName")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.sesCompanyName
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("BranchName")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.BranchName
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("PrintedBy")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.Loginid
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("PODate")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = ConvertDate2(Me.PODateFrom)
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("POTo")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = ConvertDate2(Me.PODateTo)
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("FilterBy")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.SearchBy
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = oReport.DataDefinition.ParameterFields("@SearchBy")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.SearchBy
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try

        oReport.SetDataSource(oCustomClass.ListDataReport)
        oReport.SetParameterValue(0, Me.sesCompanyName)
        oReport.SetParameterValue(1, Me.BranchName)
        oReport.SetParameterValue(2, ConvertDate2(Me.PODateFrom))
        oReport.SetParameterValue(3, ConvertDate2(Me.PODateTo))
        oReport.SetParameterValue(4, Me.Loginid)        
        oReport.SetParameterValue(5, Me.SearchBy)


        CRVReschedulingReport.ReportSource = oReport
        CRVReschedulingReport.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "PrintReschedulingReport" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "PrintReschedulingReport.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("ReportRescheduling.aspx?file=" & Me.Session.SessionID + Me.Loginid + "PrintReschedulingReport.pdf")
    End Sub
End Class