﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReschedulingStep1.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.ReschedulingStep1" %>

<%@ Register TagPrefix="uc1" TagName="UcCustExposure" Src="../../Webform.UserController/UcCustExposure.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcAgreeExposure" Src="../../Webform.UserController/UcAgreeExposure.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReschedulingStep1</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PENGAJUAN RESCHEDULING
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                ANGSURAN JATUH TEMPO PER TANGGAL
            </label>
            <asp:Label ID="lbljudul" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <uc1:ucpaymentinfo id="oPaymentInfo" runat="server">
                </uc1:ucpaymentinfo>
    <uc1:ucagreeexposure id="oAgree" runat="server">
                </uc1:ucagreeexposure>
    <uc1:uccustexposure id="oCust" runat="server">
                </uc1:uccustexposure>
    <div class="form_button">
        <asp:Button ID="imbNext" runat="server" CausesValidation="true" Text="Next" CssClass="small button green">
        </asp:Button>
        <asp:Button ID="imbCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
