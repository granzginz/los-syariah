﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ReschedulingStep1
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Protected WithEvents oCust As UcCustExposure
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    'Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    Protected WithEvents oAgree As UcAgreeExposure
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
#End Region
#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "RESCHEDULINGREQ"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack() Then
                oPaymentInfo.isTitle = True
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("BranchID")
                DoBind()
            End If

        End If
    End Sub

    Sub DoBind()
        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With
        With oPaymentInfo
            .IsTitle = True
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .PaymentInfo()
        End With
        With oAgree
            .ApplicationID = Me.ApplicationID
            .AgreementExposure()
        End With

        With oCust
            .CustomerID = Me.strCustomerid
            .CustomerExposure()
        End With
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("ReschedulingList.aspx")
    End Sub

    Private Sub imbNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then            
            Response.Redirect("ReschedulingStep2.aspx?Applicationid=" & Me.ApplicationID & "&branchid=" & Me.BranchID.Trim)
        End If
    End Sub

End Class