﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class ReschedullingFinal
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
#Region "Property"
    Property Prepaid() As Double
        Get
            Return CDbl(viewstate("Prepaid"))
        End Get
        Set(ByVal Value As Double)
            viewstate("Prepaid") = Value
        End Set
    End Property
    Private Property No() As Integer
        Get
            Return (CType(Viewstate("No"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("No") = Value
        End Set
    End Property

    Private Property NewTenor() As Integer
        Get
            Return (CType(Viewstate("NewTenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewTenor") = Value
        End Set
    End Property
    Property MyDataSet() As DataSet
        Get
            Return (CType(viewstate("MyDataSet"), DataSet))
        End Get
        Set(ByVal Value As DataSet)
            viewstate("MyDataSet") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return viewstate("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("StepUpDownType") = Value
        End Set
    End Property
    Private Property MinSeqNo() As Integer
        Get
            Return (CType(Viewstate("MinSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("MinSeqNo") = Value
        End Set
    End Property
    Private Property newNo() As Integer
        Get
            Return (CType(Viewstate("newNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("newNo") = Value
        End Set
    End Property
    Private Property NewNum() As Integer
        Get
            Return (CType(Viewstate("NewNum"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewNum") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(Viewstate("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("SeqNo") = Value
        End Set
    End Property
    Private Property GuarantorID() As String
        Get
            Return (CType(Viewstate("GuarantorID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("GuarantorID") = Value
        End Set
    End Property
    Private Property InterestType() As String
        Get
            Return (CType(Viewstate("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("InterestType") = Value
        End Set
    End Property
    Private Property ReasonID() As String
        Get
            Return (CType(Viewstate("ReasonID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReasonID") = Value
        End Set
    End Property
    Private Property ReasonTypeID() As String
        Get
            Return (CType(Viewstate("ReasonTypeID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ReasonTypeID") = Value
        End Set
    End Property
    Private Property payFreq() As Integer
        Get
            Return (CType(Viewstate("payFreq"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("payFreq") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(Viewstate("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(Viewstate("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property

    Private Property FlatRate() As Double
        Get
            Return (CType(ViewState("FlatRate"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Private Property sdate() As String
        Get
            Return (CType(Viewstate("sdate"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(Viewstate("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(Viewstate("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(Viewstate("Product"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Product") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property
    Private Property dtgData() As DataTable
        Get
            Return (CType(Viewstate("dtgData"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("dtgData") = Value
        End Set
    End Property
    Private Property CrossDefault() As DataTable
        Get
            Return (CType(Viewstate("CrossDefault"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("CrossDefault") = Value
        End Set
    End Property
    Private Property Term() As DataTable
        Get
            Return (CType(Viewstate("Term"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("Term") = Value
        End Set
    End Property
    Private Property Condition() As DataTable
        Get
            Return (CType(Viewstate("Condition"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("Condition") = Value
        End Set
    End Property

    Private Property InsSeqNo() As Integer
        Get
            Return (CType(ViewState("InsSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("InsSeqNo") = Value
        End Set
    End Property

    Property TerminationPenalty() As Double
        Get
            Return CDbl(ViewState("TerminationPenalty"))
        End Get
        Set(ByVal Value As Double)
            ViewState("TerminationPenalty") = Value
        End Set
    End Property

    Private TotalAgreement As Double
    Private myCmd As New SqlCommand
    Private objCon As SqlConnection
#End Region

#Region "Constanta"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    ' Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private oRescheduling As New Parameter.Rescheduling
    Dim m_controller As New FinancialDataController
    Dim m_controllerResc As New ReschedulingController
    Dim Entities As New Parameter.FinancialData
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "RESCHEDULINGREQ"
        If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then                
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("branchID")
                Me.sdate = Request.QueryString("sdate")
                Me.SeqNo = CInt(Request.QueryString("seqno"))
                Me.No = CInt(Me.SeqNo)
                Dim strRes As String = Request.QueryString("strRes")

                DoBind()                
                oRescheduling = Session(strRes)

                With oRescheduling
                    lblPartialPrepayment.Text = FormatNumber(.PartialPay, 2)
                    lblAdmFee.Text = FormatNumber(.AdminFee, 2)
                    lblInstallLC.Text = FormatNumber(.LcInstallment, 2)
                    lblInstallCF.Text = FormatNumber(.InstallmentCollFee, 2)
                    lblInsuranceLC.Text = FormatNumber(.LcInsurance, 2)
                    lblInsuranceCF.Text = FormatNumber(.InsuranceCollFee, 2)
                    lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                    lblSTNKRenewal.Text = FormatNumber(.STNKRenewalFee, 2)
                    InsuranceCF.Text = FormatNumber(.InsuranceClaimExpense, 2)
                    lblRepossesionFee.Text = FormatNumber(.RepossessionFee, 2)
                    lblTotAdvAmt.Text = FormatNumber(.TotAdvAmt, 2)
                    lblTotalAmountToBePaid.Text = FormatNumber(.TotAdvAmt, 2)
                    lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
                    lblBalanceAmount.Text = FormatNumber(Me.Prepaid - .TotAdvAmt, 2)
                    lblTotDiscAmt.Text = FormatNumber(.TotDiscamt, 2)
                    'lblEffectiveRate.Text = FormatNumber(.Rate, 2)
                    lblEffectiveRate.Text = FormatNumber(.Rate, 6)
                    lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                    lblReason.Text = .ReasonDescription
                    lblToBe.Text = .RequestTo
                    lblUcNotes.Text = .Notes
                    lblTenor.Text = CStr(.Tenor)
                    lblInstallmentNum.Text = .PODateTo
                    lblFlatRate.Text = .flat & "% per " & lblTenor.Text & " month "
                    lblNPA.Text = FormatNumber(.NewPrincipleAmount, 2)
                    lblPaymentFrequency.Text = .PaymentFreq
                    lblNumberOfStep.Text = .lblNumOfStep
                    lblStep.Text = CStr(.NoStep)
                    lblCummulative.Text = .lblCumm
                    lbCumm.Text = CStr(.Cummulative)

                    lblPerjanjianNo.Text = .PerjanjianNo
                    lblPPh.Text = FormatNumber(.PPh, 2)
                    lblPenalty.Text = FormatNumber(.Penalty, 2)
                    Me.TerminationPenalty = FormatNumber(.PenaltyAmount, 2)

                    Me.StepUpDownType = .txtSearch
                    Me.InstallmentScheme = .InstallmentScheme
                    If Me.InterestType = "FX" Then
                        If Me.InstallmentScheme = "ST" Then
                            If Me.StepUpDownType = "NM" Then
                                lblStep.Visible = True
                                lblNumberOfStep.Visible = True
                                lbCumm.Visible = False
                                lblCummulative.Visible = False
                            ElseIf Me.StepUpDownType = "RL" Then
                                lblStep.Visible = False
                                lblNumberOfStep.Visible = False
                                lbCumm.Visible = True
                                lblCummulative.Visible = True
                            ElseIf Me.StepUpDownType = "LS" Then
                                lblStep.Visible = True
                                lblNumberOfStep.Visible = True
                                lbCumm.Visible = True
                                lblCummulative.Visible = True
                            End If
                        Else
                            lblStep.Visible = False
                            lbCumm.Visible = False
                            lblNumberOfStep.Visible = False
                            lblCummulative.Visible = False
                        End If
                    End If

                    If Me.InterestType = "FL" Then
                        lblStep.Visible = False
                        lbCumm.Visible = False
                        lblNumberOfStep.Visible = False
                        lblCummulative.Visible = False
                    End If


                    Me.ReasonID = .ReasonID
                    Me.ReasonTypeID = .ReasonTypeID

                    Me.dtgData = .ListData
                    Me.CrossDefault = .ListCrossDefault
                    Me.Term = .ListTerm
                    Me.Condition = .ListCondition
                    Me.FlatRate = CDbl(.flat)
                    Me.payFreq = CInt(.PayFreq)
					Me.NewTenor = .NewTenor

                    ''''
                    'lblaccrued.Text = CDbl(.PphAccrued)
                    'lbldenda.Text = CDbl(.PphDenda)
                    'lbltotalpph.Text = CDbl(.PphAccrued + .PphDenda)

                    If Me.dtgData.Rows.Count = 0 Then
                        pnlViewST.Visible = False
                        dtgViewInstallment.Visible = False
                    Else
                        dtgViewInstallment.DataSource = Me.dtgData
                        dtgViewInstallment.DataBind()
                    End If

                    If Me.InstallmentScheme = "RF" Then
                        ViewInstallmentRF()
                        dtgViewInstallment.Visible = False
                        dtgViewInstallment2.Visible = True
                        txtEffRate.Text = FormatNumber(.Rate, 2)
                    End If

                    dtgCrossDefault.DataSource = Me.CrossDefault
                    dtgCrossDefault.DataBind()
                    dtgTC.DataSource = Me.Term
                    dtgTC.DataBind()
                    dtgTC2.DataSource = Me.Condition
                    dtgTC2.DataBind()

                    If Me.InstallmentScheme = "IR" Then Me.MyDataSet = .MydataSet
                End With
            End If
        End If
    End Sub


    Public Sub ViewInstallmentRF()
        Dim oFinancialData As New Parameter.FinancialData
        BuatTableView()
        oFinancialData.MydataSet = MakeAmortTableView(CInt(Me.NewTenor), CDbl(lblInstallmentAmount.Text), CDbl(lblNPA.Text), CDbl(lblEffectiveRate.Text), CInt(Me.payFreq))
        dtgViewInstallment2.DataSource = oFinancialData.MydataSet
        pnlViewST.Visible = True
        oFinancialData.MydataSet.Tables(0).Rows(0).Delete()
        dtgViewInstallment2.DataBind()
        dtgViewInstallment2.CurrentPageIndex = 0
    End Sub


#Region " Sub & Function "
    Public Sub BuatTableView()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "DueDate"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
    Public Function MakeAmortTableView(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        Dim m_controller As New FinancialDataController
        Dim oFinancialData As New Parameter.FinancialData
        With oFinancialData
            .PaymentFrequency = CStr(Me.payFreq)
        End With

        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Dim tempdate As Date
        Dim j As Integer = 0
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        'Me.newNo = (Me.No - 1)
        'Me.MinSeqNo = Me.newNo - 1
        'fix
        Me.MinSeqNo = -1
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0 'Me.SeqNo
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = Me.MinSeqNo + i
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 0)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 0)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 0)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 0)


            If DateDiff(DateInterval.Day, ConvertDate2(Me.sdate), Me.BusinessDate) >= 0 Then

                j = j + 1
                If j = 1 Then
                    tempdate = ConvertDate2(Me.sdate)
                Else
                    tempdate = DateAdd(DateInterval.Month, CInt(Me.payFreq), tempdate)
                End If
                myDataRow("DueDate") = tempdate.ToString("dd/MM/yyyy")
            End If

            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 0)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 0)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 0)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 0)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 0)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 0)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 0)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 0)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 0)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 0)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region

#Region "GetList"
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = DController.GetList(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            Me.InterestType = .InterestType
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            Me.GuarantorID = .GuarantorID
            'lblEffRate.Text = CStr(.EffectiveRate)
            lblEffRate.Text = FormatNumber(.EffectiveRate, 6)
            lblPaymentFreq.Text = .PaymentFrequency
            lblPaymentFrequency.Text = .PaymentFrequency
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                    lblPaymentFrequency.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                    lblPaymentFrequency.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                    lblPaymentFrequency.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
                    lblPaymentFrequency.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)

        End With
    End Sub
#End Region

#Region "Dobind"
    Sub DoBind()
        Dim totalPrepayment As Double
        lbljudul.Text = Me.sdate
        With oPaymentInfo
            '.IsTitle = True
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(Me.sdate)
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            '.IsPenaltyTerminationShow = False
            .IsPenaltyTerminationShow = True
            .PaymentInfo()

            Me.AgreementNo = .AgreementNo
            Me.strCustomerid = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
            lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
            'Me.AccruedAmount = totalPrepayment
            Me.Prepaid = .ContractPrepaidAmount

        End With
        GetList()

    End Sub
#End Region

#Region " Data Bound"
    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgCrossDefault_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCrossDefault.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCDNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

#End Region

#Region " Sub & Function "
    Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
        Return (dblInterestTotal * 1200) / (dblNTF * CDbl(lblTenor.Text))
    End Function
    Public Sub BuatTable()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "DueDate"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Double")
        'myDataColumn.ColumnName = "InterestTotal"
        'myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Dim tempdate As Date
        Dim j As Integer = 0
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
		'Me.newNo = (Me.No - 1)
		'Me.MinSeqNo = Me.newNo - 1
		'fix
		Me.MinSeqNo = -1
		For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0 'Me.SeqNo
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = Me.MinSeqNo + i
                myDataRow("Installment") = dblInstAmt
				myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 0)
				myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 0)
			End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
				myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 0)
			End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
			TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 0)

			'error due date tidak ke generate
			'If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(Me.sdate)) >= 0 Then

			'ini dibuka jika effective date boleh lebih kecil dari business date
			If DateDiff(DateInterval.Day, ConvertDate2(Me.sdate), Me.BusinessDate) >= 0 Then

				j = j + 1
				If j = 1 Then
					tempdate = ConvertDate2(Me.sdate)
				Else
					tempdate = DateAdd(DateInterval.Month, CDbl(Me.payFreq), tempdate)
				End If
				myDataRow("DueDate") = tempdate.ToString("yyyyMMdd")
			End If

			myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
			SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 0)
		Else
			KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 0)
		End If

        If SisaPrincipal > 0 Then
			Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 0)
		End If
        If KurangPrincipal > 0 Then
			Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 0)
		End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
			PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 0)
			dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 0)
		Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
					custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 0)
					custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 0)
					HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
					custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 0)
					custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 0)
					HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        Dim FlatRate As Double
        Dim dtEntity As New DataTable
		Dim oFinancialData As New Parameter.FinancialData
		Dim oRescheduling As New Parameter.Rescheduling
		Dim oData1 As New DataTable
        Dim oData2 As New DataTable
        Dim oData3 As New DataTable
        Dim oData4 As New DataTable

        'If CInt(lblEffectiveRate.Text) <= 0 Then
        If CInt(txtEffRate.Text) <= 0 Then
            ShowMessage(lblMessage, "Bunga Effective harus >= 0 ", True)
            Exit Sub
        End If

        BuatTable()
        'Me.MinSeqNo = Me.NewTenor - 1
        Me.NewNum = Me.NewTenor

        If Me.InstallmentScheme = "RF" Then
            oFinancialData.MydataSet = MakeAmortTable2(CInt(Me.NewTenor), CDbl(lblInstallmentAmount.Text), CDbl(lblNPA.Text), CDbl(lblEffectiveRate.Text), CInt(Me.payFreq)) 
        End If

        If Me.InstallmentScheme = "RF" Then  'untuk regular  
            If HitungUlangSisa Then
                dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
            If HitungUlangKurang Then
                dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
        End If

        If Me.InstallmentScheme = "RF" Then  'untuk regular    
            FlatRate = Math.Round(GetFlatRate(dblInterestTotal, CDbl(lblNPA.Text), CInt(Me.payFreq), CInt(lblTenor.Text)), 5)
            oFinancialData.FlatRate = FlatRate
            oFinancialData.InstallmentAmount = CDbl(lblInstallmentAmount.Text)
            oFinancialData.NumOfInstallment = CInt(lblInstallmentNum.Text)
            oFinancialData.FloatingPeriod = "0"
        End If


        Try
			With oFinancialData
				.strConnection = GetConnectionString()
				.BranchId = Me.BranchID
				.ApplicationID = Me.ApplicationID
                '.EffectiveRate = CDbl(lblEffectiveRate.Text)
                .EffectiveRate = CDbl(txtEffRate.Text)
                .PaymentFrequency = CStr(Me.payFreq)
				.Tenor = CInt(lblTenor.Text)
				.BusinessDate = Me.BusinessDate
				.EffectiveDate = ConvertDate2(Me.sdate)
				.CustomerID = Me.strCustomerid
				.FlatRate = Me.FlatRate
				.PaymentFrequency = CStr(Me.payFreq)
				.InstallmentScheme = Me.InstallmentScheme
                '.NumOfInstallment = CInt(lblInstallmentNum.Text)
                If lblInstallmentNum.Text = "" Then
                    .NumOfInstallment = 0
                Else
                    .NumOfInstallment = CInt(lblInstallmentNum.Text)
                End If
                .OutstandingTenor = Me.NewTenor
				.Tenor = CInt(lblTenor.Text)
				.InstallmentAmount = CDbl(lblInstallmentAmount.Text)
				.PartialPrepaymentAmount = CDbl(lblPartialPrepayment.Text)
				.AdministrationFee = CDbl(lblAdmFee.Text)
				.ContractPrepaidAmount = CDbl(lblPrepaidAmount.Text)
				.OutstandingPrincipalNew = CDbl(lblNPA.Text)
				.OutstandingInterestNew = 0
				.OutstandingPrincipalOld = oPaymentInfo.MaximumInstallment
				.OutstandingInterestOld = 0
				.InstallmentDue = oPaymentInfo.InstallmentDueAmount
				.InsuranceDue = oPaymentInfo.MaximumInsurance
				.LcInstallment = oPaymentInfo.MaximumLCInstallFee
				.LcInsurance = oPaymentInfo.MaximumLCInsuranceFee
				.InsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee
				.InstallmentCollFee = oPaymentInfo.MaximumInstallCollFee
				.PDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
				.InsuranceClaimExpense = oPaymentInfo.MaximumInsuranceClaimFee
				.CollectionExpense = oPaymentInfo.MaximumReposessionFee
				.AccruedInterest = oPaymentInfo.AccruedInterest
				.STNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee

				.LCInstallmentAmountDisc = CDbl(lblInstallLC.Text)
				.LCInsuranceAmountDisc = CDbl(lblInsuranceLC.Text)
				.InstallCollectionFeeDisc = CDbl(lblInstallCF.Text)
				.InsurCollectionFeeDisc = CDbl(lblInsuranceCF.Text)
				.PDCBounceFeeDisc = CDbl(lblPDCBounceFee.Text)
				.STNKFeeDisc = CDbl(lblSTNKRenewal.Text)
				.InsuranceClaimExpenseDisc = CDbl(InsuranceCF.Text)
				.RepossesFeeDisc = CDbl(lblRepossesionFee.Text)
				.TotalDiscount = CDbl(lblTotDiscAmt.Text)
				.TotalAmountToBePaid = CDbl(lblNPA.Text)            'CDbl(lblTotalAmountToBePaid.Text)
				.TotalOSAR = CDbl(lblStopAccruedAmount.Text)

				.ReasonTypeID = Me.ReasonTypeID
				.TR_Nomor = "-"
				.RequestTo = lblToBe.Text
				.ReasonID = Me.ReasonID
				.Notes = lblUcNotes.Text
				.Status = "R"
				.LoginId = Me.Loginid
				.GuarantorID = Me.GuarantorID
				.InterestType = Me.InterestType
				.txtSearch = ""
				oData1 = Me.Term
				oData2 = Me.Condition
				oData3 = Me.CrossDefault
				oData4 = Me.dtgData
				.NTF = CDbl(lblNPA.Text)
				.SupplierRate = CDbl(lblEffectiveRate.Text)
				.FirstInstallment = "AR"
				.NumofActiveAgreement = Me.NewTenor 'Me.NewNum
				.GracePeriod = 0
				.GracePeriodType = "0"
				.DiffRate = 0
				.StepUpStepDownType = Me.StepUpDownType
				.InstallmentScheme = Me.InstallmentScheme
                If Me.InstallmentScheme = "IR" Then .MydataSet = Me.MyDataSet
                .Flag = "Resch"

                .PerjanjianNo = lblPerjanjianNo.Text
                .PPh = CDbl(lblPPh.Text)
                .Penalty = CDbl(lblPenalty.Text)
                .TerminationPenalty = Me.TerminationPenalty
                ''''
                '.PphAccrued = CDbl(lblaccrued.Text)
                '.PphDenda = CDbl(lbldenda.Text)
            End With

			If Me.InstallmentScheme = "RF" Then
				oFinancialData = m_controllerResc.SaveReschedulingData(oFinancialData, oData1, oData2, oData3)
			ElseIf Me.InstallmentScheme = "IR" Then
                oFinancialData = m_controllerResc.SaveReschedulingIRR(oFinancialData, oData1, oData2, oData3)
            ElseIf Me.InstallmentScheme = "ST" Then
                oFinancialData = m_controllerResc.SaveReschedulingStepUpStepDown(oFinancialData, oData1, oData2, oData3, oData4)
            Else
                oFinancialData = m_controllerResc.SaveReschedulingEP(oFinancialData, oData1, oData2, oData3)
            End If
            Response.Redirect("ReschedulingList.aspx?Message=")
        Catch ex As Exception        
            Response.Redirect("ReschedulingList.aspx?Message=" + ex.Message + "")
        End Try
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Server.Transfer("ReschedulingList.aspx")
    End Sub

End Class