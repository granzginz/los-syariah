﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class ReschedullingExec
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
#Region "Property "
    Property RequestNo() As String
        Get
            Return viewstate("RequestNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("RequestNo") = Value
        End Set
    End Property
    Property Prepaid() As Double
        Get
            Return CDbl(viewstate("Prepaid"))
        End Get
        Set(ByVal Value As Double)
            viewstate("Prepaid") = Value
        End Set
    End Property
    Private Property DueDate() As Date
        Get
            Return (CType(Viewstate("DueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("DueDate") = Value
        End Set
    End Property
    Property EffectiveDate() As String
        Get
            Return viewstate("EffectiveDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("EffectiveDate") = Value
        End Set
    End Property
    Private Property MaxSeqNo() As Integer
        Get
            Return (CType(Viewstate("MaxSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("MaxSeqNo") = Value
        End Set
    End Property
    Private Property NewNumInst() As Integer
        Get
            Return (CType(Viewstate("NewNumInst"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("NewNumInst") = Value
        End Set
    End Property
    Property Status() As Boolean
        Get
            Return CType(viewstate("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("Status") = Value
        End Set
    End Property
    Property FRate() As Double
        Get
            Return CDbl(viewstate("FRate"))
        End Get
        Set(ByVal Value As Double)
            viewstate("FRate") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(viewstate("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            viewstate("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(viewstate("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(viewstate("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            viewstate("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(viewstate("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            viewstate("RejectMinimumIncome") = Value
        End Set
    End Property
    Property ReschedulingFeeBehaviour() As String
        Get
            Return viewstate("ReschedulingFeeBehaviour").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ReschedulingFeeBehaviour") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return viewstate("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property

    Property CustName() As String
        Get
            Return viewstate("CustName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return viewstate("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Private Property AccruedAmount() As Double
        Get
            Return (CType(ViewState("AccruedAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AccruedAmount") = Value
        End Set
    End Property
    Private Property NewPrinciple() As Double
        Get
            Return (CType(ViewState("NewPrinciple"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NewPrinciple") = Value
        End Set
    End Property
    Private Property ReschedulingFee() As Double
        Get
            Return (CType(Viewstate("ReschedulingFee"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("ReschedulingFee") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(Viewstate("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(Viewstate("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(Viewstate("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strCustomerid") = Value
        End Set
    End Property

    Private Property sdate() As String
        Get
            Return (CType(Viewstate("sdate"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(Viewstate("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            Viewstate("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(Viewstate("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(Viewstate("Product"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Product") = Value
        End Set
    End Property
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum
    Private Property Mode() As ProcessMode
        Get
            Return (CType(viewstate("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            viewstate("Mode") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return CType(viewstate("SeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("SeqNo") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property


    Dim tempInstallmentAmount As Double = 0
    Dim tempPaidAmount As Double = 0
    Dim tempWaivedAmount As Double = 0
    Dim tempLC As Double = 0
    Dim tempPrincipalAmount As Double = 0
    Dim tempInterestAmount As Double = 0
    Dim tempOSPrincipal As Double = 0
    Dim tempOSInterest As Double = 0
    Dim tempPaidPrincipal As Double = 0
    Dim tempPaidInterest As Double = 0
    Dim tempPaidLateCharges As Double = 0

    Dim TotDenda As Label
    Dim TotInstallmentAmount As Label
    Dim TotPaidAmount As Label
    Dim TotWaivedAmount As Label
    Dim TotLC As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipal As Label
    Dim TotOSInterest As Label

    Dim totPaidPrincipal As Label
    Dim totPaidInterest As Label
    Dim totPaidLateCharges As Label

#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private oRescheduling As New Parameter.Rescheduling
    Dim oApplication As New Parameter.Application
    Private m_controller As New ApplicationController
    Private oControllerResc As New DChangeController
    Dim m_controllerFinancial As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim m_controllerResch As New ReschedulingController
    Dim j As Integer = 0
    Dim RunRate As Double
    Dim FlateRate As Double
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim TotAdvAmount As Double
        Dim TotDisc As Double
        Dim PPh As Double
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            '  If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
            Me.FormID = "RESCHEDULINGEXEC"
            Me.ApplicationID = Request.QueryString("Applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.SeqNo = CType(Request.QueryString("seqno"), Integer)
            Me.CustomerID = Request.QueryString("customerid")
            Me.EffectiveDate = Request.QueryString("effectivedate")
            Me.RequestNo = Request.QueryString("no")
            Select Case Request.QueryString("mode")
                Case "0"
                    Me.Mode = ProcessMode.Cancel
                Case "1"
                    Me.Mode = ProcessMode.Execute
                Case "2"
                    Me.Mode = ProcessMode.View
            End Select
            InitializeForm()
            Call DoBind()
            With Entities
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .RequestWONo = Me.RequestNo
            End With
            Entities = m_controllerResch.ViewRescheduling(Entities)
            With Entities

                lblAdmFee.Text = FormatNumber(.AdministrationFee, 2)
                lblEffectiveRate.Text = FormatNumber(.EffectiveRate, 0)
                Select Case .PaymentFrequency
                    Case "1"
                        lblPaymentFrequency.Text = "Monthly"
                    Case "2"
                        lblPaymentFrequency.Text = "Bimonthly"
                    Case "3"
                        lblPaymentFrequency.Text = "Quarterly"
                    Case "6"
                        lblPaymentFrequency.Text = "Semi Annualy"
                End Select
                lblInstallmentNum.Text = CStr(.NumOfInstallment)
                lblTenor.Text = CStr(.Tenor)
                lblFlatRate.Text = FormatNumber(.FlatRate, 2) & "% per " & lblTenor.Text & " month "
                lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                lblPartialPrepayment.Text = FormatNumber(.PartialPrepaymentAmount, 2)
                lblInstallLC.Text = FormatNumber(.LcInstallment, 2)
                lblInstallCF.Text = FormatNumber(.InstallmentCollFee, 2)
                lblInsuranceLC.Text = FormatNumber(.LcInsurance, 2)
                lblInsuranceCF.Text = FormatNumber(.InsuranceCollFee, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKRenewal.Text = FormatNumber(.STNKRenewalFee, 2)
                lblRepossesionFee.Text = FormatNumber(.RepossessionFee, 2)
                InsuranceCF.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblReason.Text = .ReasonID
                lblToBe.Text = .RequestTo
                lblUcNotes.Text = .Notes
                dtgTC.DataSource = .Data1
                dtgTC.DataBind()
                dtgTC2.DataSource = .data2
                dtgTC2.DataBind()
                dtgCrossDefault.DataSource = .data3
                dtgCrossDefault.DataBind()
                TotAdvAmount = .PartialPrepaymentAmount + .AdministrationFee
                'TotDisc = .LcInstallment + .InstallmentCollFee + .LcInsurance +
                '                     .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee +
                '                     .RepossessionFee + .InsuranceClaimExpense
                TotDisc = .LcInstallment + .InstallmentCollFee + .LcInsurance +
                                     .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee +
                                     .RepossessionFee + .InsuranceClaimExpense + .PPh + .Penalty
                lblTotAdvAmt.Text = FormatNumber(TotAdvAmount, 2)
                lblTotalAmountToBePaid.Text = FormatNumber(TotAdvAmount, 2)
                lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
                lblBalanceAmount.Text = FormatNumber(Me.Prepaid - TotAdvAmount, 2)
                lblTotDiscAmt.Text = FormatNumber(TotDisc, 2)

                'modidfy Nofi 03052019 karena ada PPh 
                'lblNPA.Text = FormatNumber(Me.AccruedAmount - .PartialPrepaymentAmount - TotDisc, 2)

                lblPerjanjianNo.Text = .PerjanjianNo
                PPh = .PPh
                lblPPh.Text = FormatNumber(.PPh, 2)
                lblNPA.Text = FormatNumber(Me.AccruedAmount - .PartialPrepaymentAmount - TotDisc, 2)

                lblPenalty.Text = FormatNumber(.Penalty, 2)

                ViewInstallment(Me.ApplicationID, Me.RequestNo)

            End With
        End If
    End Sub

    Private Sub ViewInstallment(ByVal ApplicationID As String, ByVal RequestNo As String)
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAmortisasiReschFactAndMU"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = ApplicationID.Trim
            objCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 20).Value = RequestNo.Trim
            objReader = objCommand.ExecuteReader

            dtgViewInstallment.DataSource = objReader
            dtgViewInstallment.DataBind()
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub dtgViewInstallment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgViewInstallment.ItemDataBound
        Dim lb As New Label

        If e.Item.ItemIndex >= 0 Then

            lb = CType(e.Item.FindControl("lblInstallment"), Label)
            tempInstallmentAmount = tempInstallmentAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblInterestAmount"), Label)
            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblOsPrincipal"), Label)
            tempOSPrincipal = tempOSPrincipal + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("LblOSInterest"), Label)
            tempOSInterest = tempOSInterest + CDbl(lb.Text)

        End If

        If e.Item.ItemType = ListItemType.Footer Then

            TotInstallmentAmount = CType(e.Item.FindControl("lblTotInstallmentAmount"), Label)
            TotInstallmentAmount.Text = FormatNumber(tempInstallmentAmount, 2)

            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2)

            TotInterestAmount = CType(e.Item.FindControl("lblTotInterestAmount"), Label)
            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2)

            TotOSPrincipal = CType(e.Item.FindControl("lblTotOSPrincipal"), Label)
            TotOSPrincipal.Text = FormatNumber(tempOSPrincipal, 2)

            TotOSInterest = CType(e.Item.FindControl("lblTotOSInterest"), Label)
            TotOSInterest.Text = FormatNumber(tempOSInterest, 2)

        End If
    End Sub

    Private Sub InitializeForm()
        If Me.Mode = ProcessMode.Cancel Then
            imbExecute.Visible = False
            imbCancel.Visible = True
            imbSave.Visible = True
            imbBack.Visible = False
        ElseIf Me.Mode = ProcessMode.Execute Then
            imbExecute.Visible = True
            imbCancel.Visible = True
            imbSave.Visible = False
            imbBack.Visible = False
        ElseIf Me.Mode = ProcessMode.View Then
            imbExecute.Visible = False
            imbCancel.Visible = False
            imbSave.Visible = False
            imbBack.Visible = True
        End If
    End Sub

    Sub DoBind()
        Dim totalPrepayment As Double
        'With Entities
        '    .strConnection = GetConnectionString
        '    .BranchId = Me.BranchID
        '    .ApplicationID = Me.ApplicationID
        '    .RequestWONo = Me.RequestNo
        'End With
        'Entities = m_controllerResch.GetEffectiveDate(Entities)
        'Me.EffectiveDate = Parameter.EffectiveDate.ToString("dd/MM/yyyy")
        lbljudul.Text = Me.EffectiveDate
        With oPaymentInfo
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(Me.EffectiveDate)
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            '.IsPenaltyTerminationShow = False
            .IsPenaltyTerminationShow = True
            .PaymentInfo()

            Me.AgreementNo = .AgreementNo
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            'totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
            'lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
            totalPrepayment = .TotalPrePaymentAmountResch
            lblStopAccruedAmount.Text = FormatNumber(.TotalPrePaymentAmountResch, 2)
            Me.AccruedAmount = totalPrepayment
            Me.Prepaid = .ContractPrepaidAmount
        End With
        GetList()
    End Sub
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = DController.GetList(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            lblEffRate.Text = CStr(.EffectiveRate)
            Me.ReschedulingFee = .ReschedulingFee
            Me.ReschedulingFeeBehaviour = .ReschedulingFeeBehaviour
            Me.Product = .ProductId
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)

        End With
        oCustomClass.CustomerID = Me.CustomerID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = oController.GetCust(oCustomClass)
        Me.CustomerType = oCustomClass.CustomerType
    End Sub

    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgCrossDefault_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCrossDefault.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCDNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        'Untuk Proces Cancel
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim m_ControllerResc As New ReschedulingController
            Entities.strConnection = GetConnectionString()
            Entities.ApplicationID = Me.ApplicationID
            Entities.BusinessDate = ConvertDate2(Me.EffectiveDate)
            Entities.BranchId = Me.BranchID
            Entities = m_ControllerResc.GetMinDueDate(Entities)
            With Entities
                Me.SeqNo = .SeqNo
                Me.DueDate = .DueDate
                Me.Tenor = .Tenor
                Me.MaxSeqNo = .MaxSeqNo
                Me.NewNumInst = .NewNumInst
            End With

            With Entities
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .SeqNo = Me.SeqNo
                .BusinessDate = Me.BusinessDate
                .EffectiveDate = ConvertDate2(Me.EffectiveDate)
            End With
            Try
                m_controllerResch.ReschedulingCancel(Entities)
                Server.Transfer("Execution.aspx")
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    Private Sub imbExecute_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbExecute.Click
        Dim tempdate As Date
        Dim Entities As New Parameter.FinancialData
        Dim m_ControllerResc As New ReschedulingController
        If CheckFeature(Me.Loginid, Me.FormID, "EXEC", Me.AppId) Then
            Entities.strConnection = GetConnectionString()
            Entities.ApplicationID = Me.ApplicationID
            Entities.BusinessDate = ConvertDate2(Me.EffectiveDate)
            Entities.BranchId = Me.BranchID
            Entities = m_ControllerResc.GetMinDueDate(Entities)
            With Entities
                'Me.SeqNo = .SeqNo
                Me.DueDate = .DueDate
                Me.Tenor = .Tenor
                Me.MaxSeqNo = .MaxSeqNo
                Me.NewNumInst = .NewNumInst
            End With
            tempdate = DateAdd(DateInterval.Month, 1, Me.DueDate)
            'If Me.BusinessDate <= Me.DueDate Then
            'modify nofi 17062019 karena Nasabah yg harusnya WO (tapi belum di WO) masih bisa di Restruk jadi DueDate disini tidak berlaku
            'If Me.BusinessDate <= Me.DueDate.AddMonths(1) Then
            If CDbl(lblPrepaidAmount.Text) >= CDbl(lblTotalAmountToBePaid.Text) Then
                With Entities
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .SeqNo = Me.SeqNo
                    .BusinessDate = Me.BusinessDate
                    .EffectiveDate = ConvertDate2(Me.EffectiveDate)
                End With
                Try
                    m_controllerResch.ReschedulingExecute(Entities)
                    Server.Transfer("Execution.aspx")
                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
            'Else 
            '	ShowMessage(lblMessage, " Jumlah Prepaid harus >= Total yang harus dibayar", True)
            '         End If
        Else
            'ShowMessage(lblMessage, " Eksekusi Reschedulling harus <= '" & Me.DueDate.ToString("dd/MM/yyyy"), True)
            ShowMessage(lblMessage, " Eksekusi Reschedulling can't CheckFeature", True)
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("Execution.aspx")
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbBack.Click
        Response.Redirect("Execution.aspx")
    End Sub

End Class