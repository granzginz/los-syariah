﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
#End Region

Public Class ReportRescheduling
    Inherits Maxiloan.Webform.WebBased

#Region "Deklarasi dan constanta"
    'Protected WithEvents PODateFrom As ValidDate
    'Protected WithEvents PODateTo As ValidDate
    Private m_controller As New DataUserControlController
    Private ocontroller As New ReschedulingController
    Private oCustomclass As New Parameter.FinancialData
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "RESCHREPORT"

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not Page.IsPostBack Then
                If Request.QueryString("file") <> "" Then
                    Dim strFileLocation As String
                    strFileLocation = "../../XML/" & Request.QueryString("file")

                    Response.Write("<script language = javascript>" & vbCrLf _
                                    & "var x = screen.width; " & vbCrLf _
                                    & "var y = screen.height; " & vbCrLf _
                   & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                   & "</script>")
                End If

                Dim dtbranch As New DataTable
                Dim dtAssetType As New DataTable

                'PODateFrom.isCalendarPostBack = False
                'PODateFrom.FieldRequiredMessage = "Harap isi Tanggal Awal"
                'PODateFrom.ValidationErrMessage = "Harap isi Tanggal Awal dengan format dd/MM/yyyy"
                'PODateFrom.dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
                'PODateFrom.Display = "Dynamic"
                'PODateTo.isCalendarPostBack = False
                'PODateTo.FieldRequiredMessage = "Harap isi Tanggal Akhir"
                'PODateTo.ValidationErrMessage = "Harap isi Tanggal Akhir dengan format dd/MM/yyyy"
                'PODateTo.dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
                'PODateTo.Display = "Dynamic"

                dtAssetType = m_controller.GetAssetType(GetConnectionString)
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End If
                End With
                Dim dtAgency As New DataTable
                Dim row As DataRow
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .BranchId = Replace(Me.sesBranchId, "'", "")

                End With
                dtAgency = ocontroller.ReschedulingProduct(oCustomclass.strConnection, Replace(Me.sesBranchId, "'", ""))
                With CboProduct
                    .DataSource = dtAgency.DefaultView
                    .DataValueField = "ProductID"
                    .DataTextField = "Description"
                    .DataBind()
                    .Items.Insert(0, "ALL")
                    .Items(0).Value = "ALL"
                End With

            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("COOKIES_REPORT")
        Dim TglPeriodeFrom As Date
        Dim TglPeriodeTo As Date

        If CboProduct.SelectedItem.Value = "ALL" Then
            Me.SearchBy = " Rescheduling.BranchID = '" & cboBranch.SelectedItem.Value & "'"
        Else
            Me.SearchBy = " Rescheduling.BranchID = '" & cboBranch.SelectedItem.Value & "' And ProductBranch.ProductID = '" & CboProduct.SelectedItem.Value & "'"
        End If

        TglPeriodeFrom = Date.ParseExact(txtTglPeriodeFrom.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        TglPeriodeTo = Date.ParseExact(txtTglPeriodeTo.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        Me.SearchBy += "And Rescheduling.EffectiveDate between  '" & TglPeriodeFrom & "' And  '" & TglPeriodeTo & "'"

        If CheckFeature(Me.Loginid, Me.FormID, "print", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("PODateFrom") = ConvertDate2(txtTglPeriodeFrom.Text.Trim).ToString("dd/MM/yyyy")
                    cookie.Values("PODateTo") = ConvertDate2(txtTglPeriodeTo.Text.Trim).ToString("dd/MM/yyyy")
                    cookie.Values("BranchID") = cboBranch.SelectedItem.Value
                    cookie.Values("BranchName") = cboBranch.SelectedItem.Text
                    cookie.Values("Product") = CboProduct.SelectedItem.Value
                    cookie.Values("SearchBy") = Me.SearchBy
                    'this is dynamis cookie,it's value return the name of crystal report file we want to open 
                    cookie.Values("ReportType") = "RptReschedulingReport"
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("COOKIES_REPORT")
                    cookieNew.Values.Add("PODateFrom", txtTglPeriodeFrom.Text.Trim)
                    cookieNew.Values.Add("PODateTo", txtTglPeriodeTo.Text.Trim)
                    cookieNew.Values.Add("BranchID", cboBranch.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboBranch.SelectedItem.Text)
                    cookieNew.Values.Add("Product", CboProduct.SelectedItem.Text)
                    cookieNew.Values.Add("SearchBy", Me.SearchBy)
                    cookieNew.Values.Add("ReportType", "RptReschedulingReport")
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("ReportReschedulingViewer.aspx")
            End If
        End If
    End Sub
End Class