﻿#Region "Imports"
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ReversalPaymentGatewayLog
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtAngsuran1 As ucNumberFormat
    Protected WithEvents txtAngsuran2 As ucNumberFormat
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging

    Private oCustomClass As New Parameter.AgreementList


#End Region
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            If Request.QueryString("msg") IsNot Nothing Then
                If Request.QueryString("msg") = "success" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                Else
                    If Request.QueryString("msg").Trim.Length > 0 Then
                        ShowMessage(lblMessage, Request.QueryString("msg").Trim, True)
                    End If
                End If
            End If


            Me.FormID = "REVERSALLOG"


            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If

    End Sub
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
            .SpName = "spReversalPaymentGatewayLogList"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        recordCount = oContract.TotalRecords
        DtgReversalLog.DataSource = oContract.ListData

        Try
            DtgReversalLog.DataBind()
        Catch
            DtgReversalLog.CurrentPageIndex = 0
            DtgReversalLog.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgReversalLog.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If
    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        txtSearchDate.Text = ""
        DoBind("", "")
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click

        If txtSearchDate.Text.Trim <> "" Then
            Me.SearchBy = " ReversalDate = '" & ConvertDate2(txtSearchDate.Text).ToString("yyyy-MM-dd") & "' and flag='N'"
        End If
        If txtAngsuran1.Text > 0 Then
            If txtAngsuran2.Text > 0 Then
                Me.SearchBy = Me.SearchBy & " and Amount between '" & CDbl(txtAngsuran1.Text) & "' and '" & CDbl(txtAngsuran2.Text) & "' "
            Else
                Me.SearchBy = Me.SearchBy & " and Amount = '" & CDbl(txtAngsuran1.Text) & "' "
            End If
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)


    End Sub

    Private Sub DtgReversalLog_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgReversalLog.ItemCommand
        Select Case e.CommandName
            Case "Alokasi"
                Dim lblBankAccountID As Label
                Dim lblValueDate As Label
                Dim lblReferenceNo As Label
                Dim lblApplicationID As Label
                Dim lblAmount As Label
                Dim lblPaymentTypeID As Label

                lblBankAccountID = CType(DtgReversalLog.Items(e.Item.ItemIndex).FindControl("lblBankAccountID"), Label)
                lblValueDate = CType(DtgReversalLog.Items(e.Item.ItemIndex).FindControl("lblValueDate"), Label)
                lblReferenceNo = CType(DtgReversalLog.Items(e.Item.ItemIndex).FindControl("lblReferenceNo"), Label)
                lblApplicationID = CType(DtgReversalLog.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label)
                lblAmount = CType(DtgReversalLog.Items(e.Item.ItemIndex).FindControl("lblAmount"), Label)
                lblPaymentTypeID = CType(DtgReversalLog.Items(e.Item.ItemIndex).FindControl("lblPaymentTypeID"), Label)

                Response.Redirect("KoreksiAngsuranPaymentGatewayList.aspx?ValueDate=" & lblValueDate.Text.Trim & "&BankAccountID=" & lblBankAccountID.Text.Trim & "&" _
                                  & "ReferenceNo=" & lblReferenceNo.Text.Trim & "&ApplicationID=" & lblApplicationID.Text.Trim & "&Amount=" & lblAmount.Text.Trim.Replace(",", "") & "&PaymentTypeID=" & lblPaymentTypeID.Text.Trim)
        End Select
    End Sub


End Class