﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReversalLog.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.ReversalLog" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucIdentifikasiPemb" Src="../../Webform.UserController/ucIdentifikasiPemb.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentReversal</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <%--<script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>--%>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    KOREKSI ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Reversal
                </label>
                <asp:TextBox runat="server" ID="txtSearchDate" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="ceSearchDate" TargetControlID="txtSearchDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Reversal</label>
                <uc4:ucnumberformat id="txtAngsuran1" runat="server" cssclass="small_text" />
                s/d
                <uc4:ucnumberformat id="txtAngsuran2" runat="server" cssclass="small_text" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR TRANSAKSI REVERSAL
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgReversalLog" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="Applicationid" CssClass="grid_general"
                            OnSortCommand="SortGrid">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <%--<asp:LinkButton ID="HypAlokasi" runat="server" CommandName="Alokasi" Text='ALOKASI'></asp:LinkButton>--%>
                                        <asp:LinkButton ID="HypAlokasi" runat="server" CommandName="Alokasi" Text='AR'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="">
                                    <ItemStyle CssClass="command_col"></ItemStyle>                                                                   
                                    <ItemTemplate>
                                        <asp:LinkButton ID="HypAlokasiNonAR" runat="server" CommandName="AlokasiNonAR" Text='NON'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="">
                                    <ItemStyle CssClass="command_col"></ItemStyle>                                                                   
                                    <ItemTemplate>
                                        <asp:LinkButton ID="HypAlokasiTolakanBG"  Width="70px" runat="server" CommandName="AlokasiTolakanBG" Text='TOLAK BG'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                               
                                <asp:BoundColumn DataField="ReversalDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL REV"
                                    SortExpression="ReversalDate"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ValueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL VAL"
                                    SortExpression="ValueDate"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO BUKTI KAS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblApplicationID" Visible="false" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%> &nbsp;'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%> &nbsp;'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentTypeName" HeaderText="CARA BAYAR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaymentTypeName" runat="server" Text='<%#Container.DataItem("PaymentTypeName")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="REKENING BANK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblBankAccountID" Visible="false" runat="server" Text='<%#Container.DataItem("BankAccountID")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblValueDate" Visible="false" runat="server" Text='<%#Container.DataItem("ValueDate")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentTypeID" HeaderText="ID CARA BAYAR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaymentTypeID" runat="server" Text='<%#Container.DataItem("PaymentTypeID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationModule" HeaderText="MODUL APLIKASI">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationModule" runat="server" Text='<%#Container.DataItem("ApplicationModule")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
