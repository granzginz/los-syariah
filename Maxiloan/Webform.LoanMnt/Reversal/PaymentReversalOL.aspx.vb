﻿#Region "Imports"
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentReversalOL
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New PaymentReversalController
    Private oContract As New Parameter.PaymentReversal
    Private totalAmount As Double
    Private totalDebitAmount As Double
    Private totalCreditAmount As Double
    Private oCustomClass As New Parameter.PaymentHistory
    Private oController As New PaymentHistoryController
#End Region

#Region "Property"

    Private Property LastAR() As Double
        Get
            Return CDbl(ViewState("LastAR"))
        End Get
        Set(ByVal Value As Double)
            ViewState("LastAR") = Value
        End Set
    End Property

    Private Property HistorySequenceNo() As Integer
        Get
            Return CInt(ViewState("HistorySequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("HistorySequenceNo") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PAYREVOL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.ApplicationID = Request.QueryString("ApplicationID")
                Me.SortBy = ""
                Me.BranchID = Request.QueryString("BranchID")
                DoBind()
                Me.SearchBy = ""
                Me.SortBy = ""
                pnlPaymentDetail.Visible = False
                pnlPaymentReversalList.Visible = True
            End If
        End If
    End Sub

    Private Sub DoBind()
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oControllerAR As New UCPaymentInfoController
        Dim oCustomClassAR As New Parameter.AccMntBase
        Dim ARCurrent As Double
        With oCustomClassAR
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
        End With
        oCustomClassAR = oControllerAR.GetPaymentInfo(oCustomClassAR)
        With oCustomClassAR
            Me.LastAR = .Prepaid + _
                        .MaximumInstallment + _
                        .LcInstallment + _
                        .InstallmentCollFee + _
                        .InsuranceDue + _
                        .LcInsurance + _
                        .InsuranceCollFee + _
                        .PDCBounceFee + _
                        .STNKRenewalFee + _
                        .InsuranceClaimExpense + _
                        .RepossessionFee
        End With
        With oContract
            .strConnection = GetConnectionString()
            .ApplicationID = Request.QueryString("ApplicationID")
            .BranchReceivedID = Me.sesBranchId.Replace("'", "")
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
        End With
        oContract = cContract.PaymentReversalListOL(oContract)
        recordCount = oContract.TotalRecord

        DtgAgree.DataSource = oContract.ListReversalPayment
        If oContract.ListReversalPayment.Rows.Count > 0 Then
            With oContract.ListReversalPayment.Rows(0)
                lblAgreementBranch.Text = .Item("BranchAgreement").ToString
                lblAgreementNo.Text = .Item("AgreementNo").ToString
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                lblCustomerName.Text = .Item("CustomerName").ToString
                lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(.Item("CustomerID").ToString) & "')"
                Me.CustomerID = .Item("CustomerID").ToString
                lblPrepaid.Text = FormatNumber(.Item("ContractPrepaidAmount"), 2)
                lblTotalAmount.Text = FormatNumber(.Item("TotalAmount"), 2)
            End With
        End If
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind()
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind()
    End Sub

#End Region

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblPaymentAmount As Label
        Dim lblTotalAmountperPage As Label
        Dim lblTransactionDesc As HyperLink
        Dim lblNo As HyperLink
        Dim lnkReverse As LinkButton

        If e.Item.ItemIndex >= 0 Then
            lblTransactionDesc = CType(e.Item.FindControl("lblTransactionDesc"), HyperLink)
            lblNo = CType(e.Item.FindControl("lblNo"), HyperLink)

            lblTransactionDesc.NavigateUrl = "javascript:OpenWinPaymentHistoryDetail('AccMnt', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Me.BranchID.Trim & "','" & lblNo.Text.Trim & "')"
            lblNo.NavigateUrl = "javascript:OpenWinPaymentHistoryDetail('AccMnt', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Me.BranchID.Trim & "','" & lblNo.Text.Trim & "')"
            lnkReverse = CType(e.Item.FindControl("lnkReverse"), LinkButton)
            lnkReverse.Attributes.Add("onclick", "return reverseconfirm();")

            lblPaymentAmount = CType(e.Item.FindControl("lblPaymentAmount"), Label)
            totalAmount += CDbl(lblPaymentAmount.Text)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblTotalAmountperPage = CType(e.Item.FindControl("lblTotalAmountperPage"), Label)
            lblTotalAmountperPage.Text = FormatNumber(totalAmount, 2)
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        Server.Transfer("PaymentReversalListOL.aspx")
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "reverse"
                If CheckFeature(Me.Loginid, Me.FormID, "REV", Me.AppId) Then

                    lblMessage.Visible = False
                    cboAlasan.SelectedIndex = -1

                    Dim lblno As HyperLink
                    lblno = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblNo"), HyperLink)
                    Me.HistorySequenceNo = CInt(lblno.Text.Trim)
                    pnlPaymentDetail.Visible = True
                    pnlPaymentReversalList.Visible = False
                    Dim DtUserList As New DataTable
                    Dim DvUserList As New DataView
                    Dim intloop As Integer
                    Dim hypID As HyperLink
                    txtReferenceNo.Text = ""
                    With oCustomClass
                        .ApplicationID = Me.ApplicationID
                        .HistorySequenceNo = Me.HistorySequenceNo
                        .strConnection = GetConnectionString()
                        .CurrentPage = currentPage
                        .PageSize = pageSize
                        .SortBy = SortBy
                    End With

                    oCustomClass = oController.ListPaymentHistoryDetail(oCustomClass)
                    With oCustomClass
                        hypAgreementNo.Text = .Agreementno
                        hypCustomerName.Text = .CustomerName
                        hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                        hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID) & "')"

                        lblTransactionType.Text = .TransactionType
                        lblHistorySequenceNo.Text = CStr(Me.HistorySequenceNo)
                        lblViewPostingDate.Text = .BusinessDate.ToString("dd/MM/yyyy")
                        lblViewValueDate.Text = .ValueDate.ToString("dd/MM/yyyy")
                        lblReceiveFrom.Text = .ReceivedFrom
                        lblReferenceNo.Text = .ReferenceNo
                        lblStatusPayment.Text = .Status
                        lblStatusDate.Text = .StatusDate
                        lblBankAccountID.Text = .BankAccountID
                        lblWayOfPayment.Text = .PaymentTypeID
                        lblReceiptNo.Text = .ReceiptNo
                        lblPrintBy.Text = .PrintBy
                        lblNumOfPrint.Text = CStr(.NumOfPrint)
                        lblLastPrintDate.Text = .LastPrintDate
                        DtUserList = .ListPaymentHistoryDetail
                        lblBankName.Text = .BankName
                        lblGiroNo.Text = .GiroNo
                    End With

                    DvUserList = DtUserList.DefaultView
                    recordCount = oCustomClass.TotalRecord
                    DvUserList.Sort = Me.SortBy
                    dtgPaymentHistory.DataSource = DvUserList
                    Try
                        dtgPaymentHistory.DataBind()
                    Catch
                        dtgPaymentHistory.CurrentPageIndex = 0
                        dtgPaymentHistory.DataBind()
                    End Try
                End If
        End Select
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbSave.Click
        Dim lblNo As HyperLink
        Dim lblTransactionDesc As HyperLink
        Dim ARCurrent As Double
        Dim oControllerAR As New UCPaymentInfoController
        Dim oCustomClassAR As New Parameter.AccMntBase

        With oCustomClassAR
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
        End With

        oCustomClassAR = oControllerAR.GetPaymentInfo(oCustomClassAR)

        With oCustomClassAR
            ARCurrent = .Prepaid + _
                         .MaximumInstallment + _
                         .LcInstallment + _
                         .InstallmentCollFee + _
                         .InsuranceDue + _
                         .LcInsurance + _
                         .InsuranceCollFee + _
                         .PDCBounceFee + _
                         .STNKRenewalFee + _
                         .InsuranceClaimExpense + _
                         .RepossessionFee
        End With

        If Me.LastAR <> LastAR Then
            ShowMessage(lblMessage, "User Lain Sudah Update Kontrak ini, harap diperiksa lagi...", True)
        Else
            With oContract
                .HistorySequenceNo = Me.HistorySequenceNo
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .BranchReceivedID = Me.sesBranchId.Replace("'", "")
                .ReferenceNo = txtReferenceNo.Text
                .ReasonReversal = cboAlasan.SelectedValue
            End With

            Try
                cContract.ProcessPaymentReversalOL(oContract)
                DoBind()
                Me.SearchBy = ""
                Me.SortBy = ""
                pnlPaymentDetail.Visible = False
                pnlPaymentReversalList.Visible = True

                ShowMessage(lblMessage, "Reversal Berhasil", False)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

#Region "DataGrid Command"
    Private Sub dtgPaymentHistory_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaymentHistory.ItemDataBound
        Dim lblTotalDebitAmount As Label
        Dim lblTotalCreditAmount As Label
        Dim lblDebitAmount As Label
        Dim lblCreditAmount As Label
        If e.Item.ItemIndex >= 0 Then
            lblDebitAmount = CType(e.Item.FindControl("lblDebitAmount"), Label)
            lblCreditAmount = CType(e.Item.FindControl("lblCreditAmount"), Label)
            totalDebitAmount += CDbl(lblDebitAmount.Text)
            totalCreditAmount += CDbl(lblCreditAmount.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotalDebitAmount = CType(e.Item.FindControl("lblTotalDebitAmount"), Label)
            lblTotalDebitAmount.Text = FormatNumber(totalDebitAmount, 2)
            lblTotalCreditAmount = CType(e.Item.FindControl("lblTotalCreditAmount"), Label)
            lblTotalCreditAmount.Text = FormatNumber(totalCreditAmount, 2)
        End If
    End Sub
#End Region

    Private Sub ImbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImbBack.Click
        DoBind()
        Me.SearchBy = ""
        Me.SortBy = ""
        pnlPaymentDetail.Visible = False
        pnlPaymentReversalList.Visible = True
    End Sub

End Class