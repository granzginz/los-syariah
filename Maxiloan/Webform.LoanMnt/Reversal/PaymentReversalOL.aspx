﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentReversalOL.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.PaymentReversalOL" %>

<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentReversalOperatingLease</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function reverseconfirm() {
            if (window.confirm("Apakah yakin mau hapus transaksi ini ? "))
                return true;
            else
                return false;
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel runat="server" ID="pnlPaymentReversalList">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    REVERSAL PEMBAYARAN CUSTOMER OPERATING LEASE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang Kontrak
                </label>
                <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                        ShowFooter="True" OnSortCommand="SortGrid" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReverse" runat="server" Text="REVERSE" CommandName="reverse"
                                        Visible='<%#iif(Container.DataItem("IsReverse")="1", true, false)%>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO" SortExpression="PHH.HistorySequenceNo">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblNo" runat="server" Text='<%#Container.DataItem("HistorySequenceNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TRANSAKSI" SortExpression="TransType.Description">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblTransactionDesc" runat="server" Text='<%#Container.DataItem("PaymentDescription")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                                <FooterTemplate>
                                    Total Amount Per Page
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL VALUTA" SortExpression="PHH.ValueDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblValueDate" runat="server" Text='<%#Container.DataItem("ValueDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL POSTING" SortExpression="PHH.PostingDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblPostingDate" runat="server" Text='<%#Container.DataItem("PostingDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CABANG TERIMA" SortExpression="Branch.BranchInitialName">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchReceive" runat="server" Text='<%#Container.DataItem("BranchReceive")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CARA BAYAR" SortExpression="PaymentTypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblWOP" runat="server" Text='<%#Container.DataItem("WOP")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR" SortExpression="TblTotalAmount.Amount"
                                HeaderStyle-CssClass="th_right">
                                <ItemStyle CssClass="item_grid_right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentAmount" runat="server" Text='<%#formatnumber(Container.DataItem("TotalAmount"),2)%> &nbsp;'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalAmountperPage" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="STATUS" SortExpression="CorrectionHistSequence">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Saldo Prepaid
                </label>
                <asp:Label ID="lblPrepaid" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total
                </label>
                <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentDetail" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    REVERSAL PEMBAYARAN CUSTOMER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Transaksi
                </label>
                <asp:Label ID="lblTransactionType" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Urut History
                </label>
                <asp:Label ID="lblHistorySequenceNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Posting
                </label>
                <asp:Label ID="lblViewPostingDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Valuta
                </label>
                <asp:Label ID="lblViewValueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Terima Dari
                </label>
                <asp:Label ID="lblReceiveFrom" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas Masuk
                </label>
                <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cara Pembayaran
                </label>
                <asp:Label ID="lblWayOfPayment" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Rekening Bank
                </label>
                <asp:Label ID="lblBankAccountID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Bank
                </label>
                <asp:Label ID="lblBankName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Bilyet Giro
                </label>
                <asp:Label ID="lblGiroNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                STATUS
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kwitansi
                </label>
                <asp:Label ID="lblReceiptNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Cetak ke
                </label>
                <asp:Label ID="lblNumOfPrint" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Dicetak Oleh
                </label>
                <asp:Label ID="lblPrintBy" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Terakhir Cetak
                </label>
                <asp:Label ID="lblLastPrintDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    STATUS
                </label>
                <asp:Label ID="lblStatusPayment" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    No Bukti Kas Keluar
                </label>
                <asp:TextBox ID="txtReferenceNo" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValidator" runat="server" Display="Dynamic" ErrorMessage="harap isi No Bukti Kas Keluar"
                    ControlToValidate="txtReferenceNo" Enabled="False" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Alasan Reversal
                </label>
                <asp:DropDownList runat="server" ID="cboAlasan">
                    <asp:ListItem Text="Pilih Alasan" Value=""></asp:ListItem>
                    <asp:ListItem Text="Salah Alokasi" Value="SA"></asp:ListItem>
                    <asp:ListItem Text="Salah Input" Value="SI"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rvAlasan" ControlToValidate="cboAlasan"
                    InitialValue="" ErrorMessage="Pilih alasan reversal!" CssClass="validator_general">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL HISTORY PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaymentHistory" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                        ShowFooter="True" AllowSorting="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <ItemTemplate>
                                    <asp:Label ID="lblTransaction" runat="server" Text='<%#Container.DataItem("TransactionType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO URUT">
                                <ItemTemplate>
                                    <asp:Label ID="lblSeqNo" runat="server" Text='<%#Container.DataItem("InsSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <label>
                                        Total Amount Per Page</label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DEBET">
                                <ItemTemplate>
                                    <asp:Label ID="lblDebitAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("DebitAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalDebitAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH KREDIT">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreditAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("CreditAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalCreditAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn HeaderText="HARI TELAT"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="DENDA TELAT"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ImbBack" runat="server" Text="Back" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
