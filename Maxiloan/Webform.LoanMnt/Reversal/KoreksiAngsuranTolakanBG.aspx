﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiAngsuranTolakanBG.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.KoreksiAngsuranTolakanBG" %>

<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Koreksi Angsuran Tolakan BG</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
      <script src="../../Maxiloan.js" type="text/javascript"></script>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        ALOKASI TITIPAN KOREKSI PEMBAYARAN KARENA TOLAKAN BG
                    </h4>
                </div>
            </div>
             <asp:Panel ID="pnlDetailList" runat="server" >
                <div class="form_box">
                <div class="form_left">              
                    <label>
                        No Bukti Kas
                    </label>
                    <asp:Label ID="lblRefNo" runat="server"></asp:Label>
                </div>
                <div class="form_right">

                </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Rekening Bank
                        </label>
                        <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Valuta
                        </label>               
                        <asp:Label ID="lblvalueDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Diterima
                        </label>
                        <asp:Label ID="lblAmountRec" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Keterangan
                        </label>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server" >
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALOKASI TOLAKAN BG
                        </h4>
                    </div>
                </div>
             <div class="form_box">
                    <div class="form_left"> 
                        <label class="label_req">
                            Cabang
                        </label>
                        <asp:DropDownList ID="cboCabang" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                  <%-- <div class="form_box">
                    <div class="form_left"> 
                        <label class="label_req">
                            Departemen
                        </label>
                        <asp:DropDownList ID="cboDepartemen" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                        <label class="label_req">
                            Transaksi
                        </label>
                        <input id="hdnTransaction" type="hidden" name="hdnTransaction" runat="server" />
                        <asp:TextBox runat="server" ID="txtTransaction" CssClass="medium_text"></asp:TextBox>
                        <asp:Button ID="btnLookupTransaction" runat="server" CausesValidation="False" Text="..."
                            CssClass="small buttongo blue" />
                        <uc1:uclookupcoa id="ucLookUpCOA" runat="server" oncatselected="CatSelectedTransaction" />
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Input ini harus diisi!"
                                ControlToValidate="txtTransaction" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>--%>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah
                        </label>                        
                        <uc1:ucNumberFormat ID="txtJumlah" runat="server"  />                        
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_general">
                            Alasan Tolak
                        </label>
                        <asp:TextBox ID="txtKeteranganAlokasi" runat="server" CssClass="multiline_textbox_uc"
                            TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                     <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue" 
                         OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false">
                     </asp:Button>
                    <asp:Button ID="imbCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
