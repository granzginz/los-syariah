﻿#Region "Imports"
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region


Public Class OtorisasiKoreksiAngsuranTolakanBG
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oSearchBy As UcSearchBy
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private oCustomClass As New Parameter.PaymentHistory
    Private oController As New PaymentHistoryController
    Private bContract As New PaymentReversalController
    Private dContract As New Parameter.PaymentReversal
#End Region
#Region "Property"
    Private Property HistorySequenceNo() As Integer
        Get
            Return CInt(ViewState("HistorySequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("HistorySequenceNo") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property ReversalType() As String
        Get
            Return ViewState("ReversalType")
        End Get
        Set(ByVal Value As String)
            ViewState("ReversalType") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "REVBGLISTOTOR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("Message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("Message"), True)
                End If

                oSearchBy.ListData = "Name, Name-ApplicationID,Application ID-Agreementno, Agreement No-Address, Address-InstallmentAmount, Installment"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .SpName = "spReversalTolakanBGListOtorPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        DtUserList = oContract.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = DvUserList
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True
            pnlDatagridFactoring.Visible = False
            pnlDatagridModalUsaha.Visible = False
            lblMessage.Text = ""
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "otor"
                Try
                    Dim hypID As HyperLink
                    Dim lblHistorySeqNo As Label
                    Dim lblReferenceNo As Label
                    lblMessage.Text = ""
                    hypID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                    lblHistorySeqNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblHistorySeqNo"), Label)
                    lblReferenceNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblReferenceNo"), Label)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .PaymentAllocationID = hypID.Text
                        .HistorySequenceNo = lblHistorySeqNo.Text
                        .ReferenceNo = lblReferenceNo.Text
                    End With

                    With dContract
                        .HistorySequenceNo = lblHistorySeqNo.Text
                        .ApplicationID = hypID.Text
                        .strConnection = GetConnectionString()
                        .LoginId = Me.Loginid
                        .BusinessDate = Me.BusinessDate
                        .BranchReceivedID = Me.sesBranchId.Replace("'", "")
                        .ReferenceNo = lblReferenceNo.Text
                    End With

                    bContract.ProcessReversalTolakanBGOtor(dContract)
                    Response.Redirect("OtorisasiKoreksiAngsuranTolakanBG.aspx")

                    DoBind(Me.SearchBy, "")
                    Me.SearchBy = ""
                    Me.SortBy = ""

                    ShowMessage(lblMessage, "Otorisasi Tolakan BG Berhasil", False)

                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try
        End Select
    End Sub

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnSave.Click
        Dim lblNo As HyperLink
        Dim lblTransactionDesc As HyperLink
        Dim ARCurrent As Double
        Dim oControllerAR As New UCPaymentInfoController
        Dim oCustomClassAR As New Parameter.AccMntBase

        With oCustomClassAR
            .strConnection = GetConnectionString()
            .ApplicationID = Me.AppId
            .ValueDate = Me.BusinessDate
        End With

        oCustomClassAR = oControllerAR.GetPaymentInfo(oCustomClassAR)

        Try
            For n As Integer = 0 To DtgAgree.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(DtgAgree.Items(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim x As HyperLink = CType(DtgAgree.Items(n).FindControl("lblApplicationid"), HyperLink)

                    With dContract
                        .HistorySequenceNo = Me.HistorySequenceNo
                        .ApplicationID = x.Text
                        .strConnection = GetConnectionString()
                        .LoginId = Me.Loginid
                        .BusinessDate = Me.BusinessDate
                        .BranchReceivedID = Me.sesBranchId.Replace("'", "")
                    End With

                    bContract.ProcessReversalTolakanBGOtor(dContract)
                    Response.Redirect("OtorisasiKoreksiAngsuranTolakanBG.aspx")
                End If

            Next

            DoBind(Me.SearchBy, "")
            Me.SearchBy = ""
            Me.SortBy = ""

            ShowMessage(lblMessage, "Otorisasi Tolakan BG Berhasil", False)

        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "'"
        Response.Redirect("OtorisasiKoreksiAngsuranTolakanBG.aspx")
    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
#End Region

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink
        Dim lnkOtor As LinkButton

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lnkOtor = CType(e.Item.FindControl("lnkOtor"), LinkButton)
            lnkOtor.Attributes.Add("onclick", "return Otorconfirm();")


            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblAgreementNo.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        'strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' and contractstatus in ('AKT', 'OSP', 'OSD') ")
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "' ")


        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")

        End If
        Me.SearchBy = strSearch.ToString

        'Me.ReversalType = cboReversal.SelectedValue

        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class