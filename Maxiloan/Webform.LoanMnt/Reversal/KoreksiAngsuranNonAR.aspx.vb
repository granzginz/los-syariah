﻿#Region "Import"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class KoreksiAngsuranNonAR
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents UcBankAccountID As UcBankAccountID
    Protected WithEvents ucLookUpCOA As ucLookUpCOA
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtJumlah As ucNumberFormat


#Region "Property"
    Private Property AmountReceive() As Double
        Get
            Return CType(ViewState("AmountReceive"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountReceive") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property WayOfPaymentDesc() As String
        Get
            Return (CType(ViewState("WayOfPaymentDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("WayOfPaymentDesc") = Value
        End Set
    End Property

    Private Property ValidationData() As Double
        Get
            Return (CType(ViewState("ValidationData"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ValidationData") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property
    Private Property ValueDateString() As String
        Get
            Return CType(ViewState("ValueDateString"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ValueDateString") = Value
        End Set
    End Property
    Private Property ReferenceNoLog() As String
        Get
            Return CType(ViewState("ReferenceNoLog"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ReferenceNoLog") = Value
        End Set
    End Property
    Private Property ApplicationIDLog() As String
        Get
            Return CType(ViewState("ApplicationIDLog"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationIDLog") = Value
        End Set
    End Property
#End Region
    Public Property oClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property

    Private Property PaymentTypeID() As String
        Get
            Return CType(ViewState("PaymentTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentTypeID") = Value
        End Set
    End Property

#Region "Constanta"
    Private oCustomClass As New Parameter.InstallRcv
    Private oController As New InstallRcvController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("BranchID")
            Me.ValueDateString = Request.QueryString("ValueDate")
            Me.BankAccountID = Request.QueryString("BankAccountID")
            Me.ReferenceNoLog = Request.QueryString("ReferenceNo")
            Me.ApplicationIDLog = Request.QueryString("ApplicationIDLog")
            Me.AmountReceive = IIf(Request.QueryString("Amount") IsNot Nothing, CDbl(Request.QueryString("Amount")), 0)
            Me.PaymentTypeID = Request.QueryString("PaymentTypeID")

            If ValueDateString.Trim = "" Or BankAccountID.Trim = "" Then
                Response.Redirect("ReversalLog.aspx?msg=Data tidak valid!")
            Else
                Me.ValueDateString = CDate(Request.QueryString("ValueDate")).ToString("dd/MM/yyyy")
                Me.ValueDate = CDate(Request.QueryString("ValueDate"))
            End If

            Me.FormID = "REVERSALLOG"
            Me.Mode = "Normal"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                lblRefNo.Text = Me.ReferenceNoLog
                lblBankAccount.Text = Me.BankAccountID
                lblvalueDate.Text = Me.ValueDateString
                lblAmountRec.Text = FormatNumber(Me.AmountReceive, 0)
                lblDescription.Text = ""

                txtJumlah.Text = FormatNumber(Me.AmountReceive, 0)
                txtJumlah.Enabled = False

                BindComboCabang()
                BindComboDepartemen()
            End If
        End If
    End Sub

    Sub BindComboCabang()
        oCustomClass = New Parameter.InstallRcv
        Dim tbl As New DataTable
        oCustomClass.strConnection = GetConnectionString()
        tbl = oController.GetComboCabang(oCustomClass)

        cboCabang.DataTextField = "BranchFullName"
        cboCabang.DataValueField = "BranchID"
        cboCabang.DataSource = tbl
        cboCabang.DataBind()

        cboCabang.SelectedIndex = cboCabang.Items.IndexOf(cboCabang.Items.FindByValue(Me.sesBranchId.Replace("'", "")))
    End Sub

    Sub BindComboDepartemen()
        oCustomClass = New Parameter.InstallRcv
        Dim tbl As New DataTable
        oCustomClass.strConnection = GetConnectionString()
        tbl = oController.GetComboDepartemen(oCustomClass)

        cboDepartemen.DataTextField = "DepartementName"
        cboDepartemen.DataValueField = "DepartementId"
        cboDepartemen.DataSource = tbl
        cboDepartemen.DataBind()
    End Sub

    Protected Sub btnLookupTransaction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupTransaction.Click
        ucLookUpCOA.Visible = True
        ucLookUpCOA.CmdWhere = "All"
        ucLookUpCOA.Sort = "PaymentAllocationID ASC"
        ucLookUpCOA.Popup()
        ucLookUpCOA.BindGridEntity(ucLookUpCOA.CmdWhere)
    End Sub

    Public Sub CatSelectedTransaction(ByVal PaymentAllocationID As String,
                                           ByVal Description As String,
                                           ByVal COA As String)
        txtTransaction.Text = Description
        hdnTransaction.Value = PaymentAllocationID
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        oCustomClass = New Parameter.InstallRcv
        Dim ErrMessage As String = ""
        If SessionInvalid() Then
            Exit Sub
        End If

        With oCustomClass
            .ReferenceNo = Me.ReferenceNoLog.Trim
            .PaymentAllocationID = hdnTransaction.Value.Trim
            .BranchId = cboCabang.SelectedItem.Value
            .Departemen = cboDepartemen.SelectedItem.Value
            .DepartementID = cboDepartemen.SelectedIndex
            .Amount = Me.AmountReceive
            .Description = txtKeteranganAlokasi.Text.Trim
            .strConnection = GetConnectionString()
            .BusinessDate = Me.BusinessDate
        End With

        oController.AlokasiPembNonARSaveEdit(oCustomClass)

        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Response.Redirect("ReversalLog.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("ReversalLog.aspx")
    End Sub
End Class