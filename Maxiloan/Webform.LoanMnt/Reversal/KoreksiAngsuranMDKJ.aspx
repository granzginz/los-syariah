﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiAngsuranMDKJ.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.KoreksiAngsuranFACTMDKJ" %>

<%@ Register TagPrefix="uc1" TagName="UCPaymentDetail" Src="../../Webform.UserController/UCPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCViewPaymentDetail" Src="../../Webform.UserController/UCViewPaymentDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentAllocationDetail" Src="../../Webform.UserController/UcPaymentAllocationDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCashier" Src="../../Webform.UserController/UcCashier.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="../../Webform.UserController/UcInstallmentSchedule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InstallRCV_Bank_ModalKerja</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.ServerVariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlPaymentReceive" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    PEMBAYARAN CUSTOMER MODAL KERJA (via BANK) OTOR
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <uc1:ucpaymentdetail id="oPaymentDetail" runat="server" isdatemandatory="true" />
            <div class="form_right">
                <label>Tanggal Bayar</label>
                <uc1:ucdatece id="oValueDateFACTMDKJ" runat="server" />
            </div>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label>Tanggal Invoice</label>
                <uc1:ucdatece ID="oInvoiceDate" runat="server" />
            </div>
            <div class="form_right">
                <label>Tanggal JT Invoice</label>
                 <uc1:ucdatece ID="oInvoiceDueDate" runat="server"/>
            </div>
        </div>
        
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">No Referensi</label>
                <asp:TextBox ID="NoRefrensi" runat="server" CssClass="" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNoRefrensi" runat="server" ControlToValidate="NoRefrensi" 
                    CssClass="validator_general" Display="Dynamic" ErrorMessage="Input No Referensi"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>No Invoice</label>
                <asp:Label ID="lblInvoiceNo" runat="server" CssClass="" ></asp:Label> 
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Jumlah yang dapat dialokasikan</label>
                <asp:Label id ="lblTotalAlokasi" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
             <div class="form_left">
                <label>Bayar Pokok</label>
                <uc1:UcNumberFormat id="principalPaid" runat="server" />
            </div>
            <div class="form_right">
                <label>Total Invoice</label>
                <asp:Label id ="lblInvoiceAmount" runat="server" ></asp:Label>  
            </div>
        </div>
         <div class="form_box">
            <div class="form_left">
                <label>Bayar Margin</label>
                <uc1:UcNumberFormat id="interestPaid" runat="server" />
            </div>
             <div class="form_right">
                 <label>Sisa Pokok</label>
                 <asp:Label id="lblTotalPembiayaan" runat="server" ></asp:Label>  
             </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Bayar Ta'widh</label>
                <uc1:UcNumberFormat id="dendapaid" runat="server" />
            </div>
            <div class="form_right">
                <label>Margin Invoice</label>
                <asp:Label ID="lblInterestAmount" runat="server" ></asp:Label>  
            </div>
        </div>
        <div class="form_box" style="display:none">
            <div class="form_left">
                <label>Bayar Retensi</label>
                <uc1:UcNumberFormat id="RetensiPaid" runat="server" />
            </div>
             <div class="form_right">
                <label>Retensi Invoice</label>
                <asp:Label ID="lblRetensiAmount" runat="server" ></asp:Label>  
            </div>
        </div>
        <div class="form_box">
            <div class="form_left"></div>
            <div class="form_right">
                <label>Periode</label>
                <asp:Label ID="lblInsseqno" runat="server" />
            </div>
        </div>
         
<%--        <asp:Panel runat="server" ID="pnlBiaya">
            <div class="form_box">
                 <div class="form_left">
                    <h4>Biaya-biaya</h4>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Admin</label>
                    <asp:Label runat="server" ID="lblBiayaAdmin"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Provisi / Annual</label>
                    <asp:Label runat="server" ID="lblBiayaProvisi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Handling</label>
                    <asp:Label runat="server" ID="lblBiayaHandling"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Asuransi Kredit</label>
                    <asp:Label runat="server" ID="lblAsuransiKredit"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Polis</label>
                    <asp:Label runat="server" ID="lblBiayaPolis"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Survey</label>
                    <asp:Label runat="server" ID="lblBiayaSurvey"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Notaris</label>
                    <asp:Label runat="server" ID="lblBiayaNotaris"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Discount charges</label>
                    <asp:Label runat="server" ID="lblDiscountCharges"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Biaya</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblTotalBiaya"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div>
        </asp:Panel>--%>
<%--        <asp:Panel runat="server" ID="pnlListInvoice">
            <div class="form_box_header">
                <div class="form_single">
                    <h5>
                        TABEL BUNGA YANG JATUH TEMPO
                    </h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InsSeqNo" HeaderText="Periode"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="DueDate" HeaderText="Jatuh Tempo" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PrincipalAmount" HeaderText="Pokok" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Bunga" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestPaidAmount" HeaderText="Bunga Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="LateChargeAmount" HeaderText="Denda"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>--%>
        <asp:Panel ID="pnlBtnGroupPaymentAllocation" runat="server">
            <div class="form_button">
                <asp:Button ID="imbSaveProcess" runat="server" Text="Save" CssClass="small button blue" />
                &nbsp;
                <asp:Button ID="imbCancel2Process" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray" />
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>

