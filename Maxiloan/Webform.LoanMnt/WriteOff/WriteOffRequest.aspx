﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WriteOffRequest.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.WriteOffRequest" %>

<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WriteOffRequest</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PENGAJUAN WRITE OFF
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang Kontrak
            </label>
            <asp:Label ID="lblAgreementBranch" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Pembayaran Terakhir
            </label>
            <asp:Label ID="lblLastPaymentDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Hari Telat
            </label>
            <asp:Label ID="lblPastDueDays" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Stop Hitung Margin
            </label>
            <asp:Label ID="lblSADate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Stop Hitung Margin
            </label>
            <asp:Label ID="lblSAAmount" runat="server" CssClass="numberAlign label" ></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Rekening Lain Dimiliki
            </label>
            <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
        </div>
    </div>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server"></uc1:ucfullprepayinfo>
        <div class="form_box">
            <div>
                <div class="form_left">
                </div>
                <div class="form_right">
                    Jumlah Stop Hitung Margin
                    <asp:Label ID="lblStopAccruedAmount" runat="server" CssClass="numberAlign label"></asp:Label>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlRequestDetail" runat="server">
        <uc1:ucapprovalrequest id="oApprovalRequest" runat="server">
        </uc1:ucapprovalrequest>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="imbRequest" runat="server" Text="Request" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
