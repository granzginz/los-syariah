﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class WriteOffView
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.WriteOff
    Private oController As New StopAccruedController
    Private oControllerWO As New WriteOffController

#End Region
#Region "Property"
    Private ReadOnly Property RequestWONo() As String
        Get
            Return CType(Request.QueryString("RequestWONo"), String)
        End Get
    End Property
    Private ReadOnly Property CSSStyle() As String
        Get
            Return Request("Style").ToString
        End Get
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            With oCustomClass
                .strConnection = GetConnectionString
                .RequestWONo = Me.RequestWONo
            End With
            oCustomClass = oControllerWO.WriteOffView(oCustomClass)
            With oCustomClass
                lblRequestNo.Text = .RequestWONo
                lblAgreementNo.Text = .Agreementno
                lblCustomerName.Text = .CustomerName
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & Me.CSSStyle & "','" & .ApplicationID.Trim & "')"
                lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & Me.CSSStyle & "', '" & .CustomerID & "')"
                lblCrossDefault.Text = CStr(IIf(.CrossDefault, "Yes", "No"))
                lblRequestDate.Text = .ValueDate.ToString("dd/MM/yyyy")
                lblSAAmount.Text = FormatNumber(.NAAmount, 2)
                lblNAAmount.Text = FormatNumber(.TotalAmount, 2)
                lblOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
                '  lblOutstandingInterest.Text = FormatNumber(.OutStandingInterest, 2)
                lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                lblInstallCollFee.Text = FormatNumber(.InstallmentCollFee, 2)
                lblInsuranceCollFee.Text = FormatNumber(.InsuranceCollFee, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKRenewalFee.Text = FormatNumber(.STNKRenewalFee, 2)
                lblRepoFee.Text = FormatNumber(.RepossessionFee, 2)
                lblInsuranceClaimFee.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
                lblUcNotes.Text = .Notes
                lblReason.Text = .ReasonDescription
                lblToBe.Text = .RequestBy
                lblSADate.Text = .StatusDate.ToString("dd/MM/yyyy")
                lblPastDueDays.Text = CStr(.PastDueDays)
                lblLastPaymentDate.Text = .LastPayment.ToString("dd/MM/yyyy")
            End With

            Dim imb As Button
            imb = CType(Me.FindControl("imbClose"), Button)
            imb.Attributes.Add("onclick", "return Close()")
        End If
    End Sub

End Class