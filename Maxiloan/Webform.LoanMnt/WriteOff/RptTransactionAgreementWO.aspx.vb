﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class RptTransactionAgreementWO
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.Sales
    Private oController As New RptSalesController
#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        PnlTOP.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        cboParent.Items(0).Value = "0"
        cboProduct.Items(0).Value = "0"
        PnlTOP.Visible = True
    End Sub
#End Region
#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BulanFrom() As String
        Get
            Return CType(viewstate("BulanFrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BulanFrom") = Value
        End Set
    End Property
    Private Property BulanTo() As String
        Get
            Return CType(viewstate("BulanTo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BulanTo") = Value
        End Set
    End Property
    Private Property TahunFrom() As String
        Get
            Return CType(viewstate("TahunFrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("TahunFrom") = Value
        End Set
    End Property
    Private Property TahunTo() As String
        Get
            Return CType(viewstate("TahunTo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("TahunTo") = Value
        End Set
    End Property
    Private Property Sdate() As String
        Get
            Return CType(viewstate("Sdate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sdate") = Value
        End Set
    End Property
    Private Property Edate() As String
        Get
            Return CType(viewstate("Edate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Edate") = Value
        End Set
    End Property
#End Region
#Region "Private Sub"
    Private Sub bindComboBranch()
        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId.Replace("'", ""))
        With cboParent
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
    Private Sub bindComboProduct()
        Dim dtProduct As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With
        dtProduct = oController.GetProductBranch(oCustomClass)
        With cboProduct
            .DataTextField = "Description"
            .DataValueField = "ProductID"
            .DataSource = dtProduct
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "ALL"
        End With
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Me.IsPostBack Then
            Me.FormID = "OSWO"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            bindComboBranch()
            bindComboProduct()
            InitialDefaultPanel()
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles BtnReset.Click
        InitialDefaultPanel()
    End Sub

    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbViewReport.Click
        Me.SearchBy = ""
        Me.FilterBy = ""
        If cboProduct.SelectedItem.Text = "ALL" Then
            Me.SearchBy = "Agreement.BranchID = '" & cboParent.SelectedItem.Value & "'"
            Me.FilterBy = "BranchID = " & cboParent.SelectedItem.Text.Trim
        Else
            Me.SearchBy = "Agreement.BranchID = '" & cboParent.SelectedItem.Value & "' And "
            Me.SearchBy = Me.SearchBy + "Agreement.ProductID = '" & cboProduct.SelectedItem.Value & "'"
            Me.FilterBy = "BranchID = " & cboParent.SelectedItem.Text.Trim & " and ProductID = " & cboProduct.SelectedItem.Text.Trim
        End If

        Me.BulanFrom = cboBulan1.SelectedValue.Trim
        Me.BulanTo = cboBulan2.SelectedValue.Trim
        Me.TahunFrom = txtYear1.Text
        Me.TahunTo = txtYear2.Text

        Me.Sdate = "" & cboBulan1.SelectedItem.Text & " " & txtYear1.Text
        Me.Edate = "" & cboBulan2.SelectedItem.Text & " " & txtYear2.Text
        Me.CmdWhere = cboParent.SelectedItem.Text.Trim
        Dim cookie As HttpCookie = Request.Cookies("reportTransactionAgreementWO")
        If Not cookie Is Nothing Then
            cookie.Values("ReportType") = "TransactionagreementWORpt"
            cookie.Values("cmdwhere") = Me.SearchBy
            cookie.Values("sDate") = Me.Sdate
            cookie.Values("eDate") = Me.Edate
            cookie.Values("LoginID") = Me.Loginid
            cookie.Values("FilterBy") = Me.FilterBy
            cookie.Values("BranchName") = Me.CmdWhere
            cookie.Values("BulanFrom") = Me.BulanFrom
            cookie.Values("BulanTo") = Me.BulanTo
            cookie.Values("TahunFrom") = Me.TahunFrom
            cookie.Values("TahunTo") = Me.TahunTo
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("reportTransactionAgreementWO")
            cookieNew.Values.Add("ReportType", "TransactionagreementWORpt")
            cookieNew.Values.Add("cmdwhere", Me.SearchBy)
            cookieNew.Values.Add("sDate", Me.Sdate)
            cookieNew.Values.Add("eDate", Me.Edate)
            cookieNew.Values.Add("LoginID", Me.Loginid)
            cookieNew.Values.Add("FilterBy", Me.FilterBy)
            cookieNew.Values.Add("BranchName", Me.CmdWhere)
            cookieNew.Values.Add("BulanFrom", Me.BulanFrom)
            cookieNew.Values.Add("BulanTo", Me.BulanTo)
            cookieNew.Values.Add("TahunFrom", Me.TahunFrom)
            cookieNew.Values.Add("TahunTo", Me.TahunTo)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("RptTransactionAgreementWOViewer.aspx")
    End Sub

End Class