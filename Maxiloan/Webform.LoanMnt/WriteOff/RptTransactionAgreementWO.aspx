﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RptTransactionAgreementWO.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.RptTransactionAgreementWO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RptTransactionAgreementWO</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label> 
    <asp:Panel ID="PnlTOP" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    KONTRAK WRITE OFF
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                    ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Produk
                </label>
                <asp:DropDownList ID="cboProduct" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Bulan/Tahun
                </label>
                <asp:DropDownList ID="cboBulan1" runat="server">
                    <asp:ListItem Value="1">Januari</asp:ListItem>
                    <asp:ListItem Value="2">Februari</asp:ListItem>
                    <asp:ListItem Value="3">Maret</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">Mei</asp:ListItem>
                    <asp:ListItem Value="6">Juni</asp:ListItem>
                    <asp:ListItem Value="7">Juli</asp:ListItem>
                    <asp:ListItem Value="8">Agustus</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">Oktober</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">Desember</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtYear1" runat="server" Width="40px" Height="18px" ></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="vfvtxtYear1" runat="server" Display="Dynamic" ErrorMessage="Harap isi Tahun"
                    ControlToValidate="txtYear1" CssClass="validator_general"></asp:RequiredFieldValidator>&nbsp;&nbsp;to&nbsp;
                <asp:DropDownList ID="cboBulan2" runat="server">
                    <asp:ListItem Value="1">Januari</asp:ListItem>
                    <asp:ListItem Value="2">Februari</asp:ListItem>
                    <asp:ListItem Value="3">Maret</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">Mei</asp:ListItem>
                    <asp:ListItem Value="6">Juni</asp:ListItem>
                    <asp:ListItem Value="7">Juli</asp:ListItem>
                    <asp:ListItem Value="8">Agustus</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">Oktober</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">Desember</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtYear2" runat="server" Width="40px" Height="18px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtYear2" runat="server" Display="Dynamic" ErrorMessage="Harap isi Tahun"
                    ControlToValidate="txtYear2" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbViewReport" runat="server" Enabled="true" Text="View Report" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
