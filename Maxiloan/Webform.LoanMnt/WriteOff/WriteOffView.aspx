﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WriteOffView.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.WriteOffView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WriteOffView</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title"><div class="title_strip"> </div>
        <div class="form_single">
            <h4>
                VIEW WRITE OFF
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Request
            </label>
            <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tgl Pembayaran Terakhir
            </label>
            <asp:Label ID="lblLastPaymentDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Hari Telat
            </label>
            <asp:Label ID="lblPastDueDays" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Stop Hitung Margin
            </label>
            <asp:Label ID="lblSADate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Stop Hitung Margin
            </label>
            <asp:Label ID="lblSAAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Rekening Lain dimiliki
            </label>
            <asp:Label ID="lblCrossDefault" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <asp:Panel ID="pnlPaymentInfo" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL INFORMASI PEMBAYARAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Sisa Pokok
                </label>
                <asp:Label ID="lblOutstandingPrincipal" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Accrued Interest
                </label>
                <asp:Label ID="lblAccruedInterest" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Angsuran Jatuh Tempo
                </label>
                <asp:Label ID="lblInstallmentDue" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Premi Asuransi Jatuh Tempo
                </label>
                <asp:Label ID="lblInsuranceDue" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Denda Keterlambatan Angsuran
                </label>
                <asp:Label ID="lblLCInstall" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan Asuransi
                </label>
                <asp:Label ID="lblLCInsurance" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tagih Angsuran
                </label>
                <asp:Label ID="lblInstallCollFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Tagih Asuransi
                </label>
                <asp:Label ID="lblInsuranceCollFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tolakan PDC
                </label>
                <asp:Label ID="lblPDCBounceFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Perpanjang STNK/BBN
                </label>
                <asp:Label ID="lblSTNKRenewalFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Klaim Asuransi
                </label>
                <asp:Label ID="lblInsuranceClaimFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Tarik
                </label>
                <asp:Label ID="lblRepoFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    <b>Stop Hitung Margin</b>
                </label>
                <asp:Label ID="lblNAAmount" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Alasan
                </label>
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="lblToBe" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblUcNotes" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="ImbClose" runat="server" Text="Close" CssClass="small button gray">
        </asp:Button><a href="javascript:window.close();"></a>
    </div>
    </form>
</body>
</html>
