﻿#Region "Imports"
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class WriteOffRequest
    Inherits Maxiloan.Webform.AccMntWebBased

    Private Property AccruedAmount() As Double
        Get
            Return CType(viewstate("AccruedAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("AccruedAmount") = Value
        End Set
    End Property

#Region "Constanta"
    Private oCustomClass As New Parameter.StopAccrued
    Private oCustomClassWO As New Parameter.WriteOff
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Private oController As New StopAccruedController
    Private oControllerWO As New WriteOffController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "WRITEOFFLIST"

            ' If CheckFeature(Me.Loginid, Me.FormID, "SRC", Me.AppId) Then
            Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId

            oApprovalRequest.ReasonTypeID = "NOACR"
            oApprovalRequest.ApprovalScheme = "WO__"
            GetPaymentInfo()
            With oCustomClass
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .BusinessDate = Me.BusinessDate
            End With
            oCustomClass = oController.StopAccruedGetPastDueDays(oCustomClass)
            lblLastPaymentDate.Text = oCustomClass.LastPayment.ToString("dd/MM/yyyy")
            lblPastDueDays.Text = CStr(oCustomClass.PastDueDays)

            With oCustomClassWO
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .BusinessDate = Me.BusinessDate
            End With
            oCustomClassWO = oControllerWO.WriteOffGetSAAmount(oCustomClassWO)
            lblSADate.Text = oCustomClassWO.NADate.ToString("dd/MM/yyyy")
            lblSAAmount.Text = FormatNumber(oCustomClassWO.NAAmount, 0)
        End If
        'End If
    End Sub

    Private Sub GetPaymentInfo()
        With oPaymentInfo
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            .IsPenaltyTerminationShow = False
            .PaymentInfo()
            lblAgreementBranch.Text = .BranchAgreement
            Me.AgreementNo = .AgreementNo
            Me.CustomerID = .CustomerID
            Me.CustomerType = .CustomerType
            Me.AccruedAmount = .MaximumOutStandingPrincipal + .MaximumOutStandingInterest + .InstallmentDueAmount
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            'lblStopAccruedAmount.Text = FormatNumber(.MaximumOutStandingPrincipal + .MaximumOutStandingInterest + .InstallmentDueAmount, 2)
            lblStopAccruedAmount.Text = FormatNumber(.MaximumOutStandingPrincipal, 2)
            If Not IsPostBack Then
                'Select Case .DefaultStatus.Trim
                '    Case "NA"
                '        lblDefaultStatus.Text = "Stop Accrued"
                '    Case "WO"
                '        lblDefaultStatus.Text = "Write Off"
                '    Case "NM"
                '        lblDefaultStatus.Text = "Normal"
                'End Select
                If .IsCrossDefault Then
                    lblCrossDefault.Text = "Yes"
                Else
                    lblCrossDefault.Text = "No"
                End If
            End If
        End With
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("WriteOffList.aspx")
    End Sub

    Private Sub imbRequest_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbRequest.Click
        lblMessage.Text = ""
        '   If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then

        GetPaymentInfo()
        With oCustomClassWO
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.BusinessDate
            .PrepaymentType = "DI"
            .BusinessDate = Me.BusinessDate
            .NAAmount = Me.AccruedAmount
            .ReasonTypeID = "NOACR"
            .ReasonID = oApprovalRequest.ReasonID
            .LoginId = Me.Loginid
            .RequestTo = oApprovalRequest.ToBeApprove
            .Notes = oApprovalRequest.Notes
        End With
        Try
            oControllerWO.WORequest(oCustomClassWO)
            Response.Redirect("WriteOffList.aspx")
        Catch exp As Exception

            ShowMessage(lblMessage, "Write Off Gagal" & exp.Message, True)
        End Try
        '  End If
    End Sub

End Class