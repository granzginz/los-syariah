﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WriteOffInquiry.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.WriteOffInquiry" %>
   
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">


<head runat="server">
    <title>WriteOffInquiry</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
<%--    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewRequestWONo(pStyle, pRequestWONo) {
            var x = screen.width; var y = screen.height - 100;
            alert(ServerName+App);
            window.open(ServerName + App + '/Webform.LoanMnt/WriteOff/WriteOffView.aspx?style=' + pStyle + '&RequestWONo=' + pRequestWONo, 'RequestNoLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>--%>
</head>
<body>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewRequestNaNo(pStyle, pRequestNo) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/WriteOff/WriteOffView.aspx?style=' + pStyle + '&RequestWONo=' + pRequestWONo, 'RequestNoLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    INQUIRY WRITE OFF
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                    ErrorMessage="Harap Pilih Cabang" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Request
                </label> 
                 <asp:TextBox runat="server" ID="EffDate"></asp:TextBox>
                 <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="EffDate" Format="dd/MM/yyyy" ></asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="0">Select one</asp:ListItem>
                    <asp:ListItem Value="1">No Request</asp:ListItem>
                    <asp:ListItem Value="2">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="3">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtSearch" runat="server"  Width="123px"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Status
                </label>
                <asp:DropDownList ID="cbostatus" runat="server">
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="R">Request</asp:ListItem>
                    <asp:ListItem Value="A">Approval</asp:ListItem>
                    <asp:ListItem Value="J">Reject</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR WRITE OFF
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgStopAccruedInquiry" runat="server" Visible="False" CssClass="grid_general"
                        AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle HorizontalAlign="Right" Height="25px" Width="30%" CssClass="tdjudul">
                        </FooterStyle>
                        <Columns>
                            <asp:TemplateColumn SortExpression="RequestNaNo" HeaderText="NO REQUEST">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyRequestNo" runat="server" Text='<%#Container.DataItem("RequestWONo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="LblApplicationId" runat="server" Visible="False" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustID" runat="Server" Text='<%#Container.DataItem("CustomerID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="WOTotalAmount" HeaderText="JUMLAH">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("NaTotalAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="EfectiveDate" SortExpression="RequestDate" HeaderText="TGL REQUEST"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
