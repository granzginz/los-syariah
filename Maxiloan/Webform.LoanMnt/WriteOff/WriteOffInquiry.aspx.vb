﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class WriteOffInquiry
    Inherits Maxiloan.Webform.WebBased

    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("filterby"))
        End Get
        Set(ByVal Value As String)
            viewstate("filterby") = Value
        End Set
    End Property
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New WriteOffController
    Private oCustomClass As New Parameter.WriteOff
    Private m_controller As New DataUserControlController


#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "WOINQ"
            pnlSearch.Visible = True
            pnlDatagrid.Visible = False
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'With cboParent
                '    .DataTextField = "Name"
                '    .DataValueField = "ID"
                '    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                '    .DataBind()
                '    .Items.Insert(0, "Select One")
                '    .Items(0).Value = "0"
                'End With
                Dim dtbranch As New DataTable
                Dim dtInsCo As New DataTable

                Me.sesBranchId = Replace(Me.sesBranchId, "'", "")

                If Me.IsHoBranch Then
                    dtbranch = m_controller.GetBranchName(GetConnectionString, "All")
                Else
                    dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                End If

                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGrid(ByVal SearchBy As String, ByVal sortBy As String)
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
            .spname = "spWriteOffInquiry"
        End With

        oCustomClass = oController.WriteOffInquiry(oCustomClass)

        recordCount = oCustomClass.TotalRecord
        DtgStopAccruedInquiry.DataSource = oCustomClass.listdata
        Try
            DtgStopAccruedInquiry.DataBind()
        Catch
            DtgStopAccruedInquiry.CurrentPageIndex = 0
            DtgStopAccruedInquiry.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region



    Private Sub DtgStopAccruedInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgStopAccruedInquiry.ItemDataBound
        Dim hyAgreementNo As HyperLink
        Dim HyCustomerName As HyperLink
        Dim hyRequestNo As HyperLink
        Dim lblCustID As Label
        Dim lblApplicationId As Label

        If e.Item.ItemIndex >= 0 Then
            'Dim oInqInsuranceInvoice As New Parameter.InsInqEntities
            'Dim strconn As String = getConnectionString()
            'Dim dt As New DataTable

            hyAgreementNo = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            HyCustomerName = CType(e.Item.FindControl("HyCustomerName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            If hyAgreementNo.Text.Trim.Length > 0 Then
                hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If
            If lblCustID.Text.Trim.Length > 0 Then
                HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
            hyRequestNo = CType(e.Item.FindControl("hyRequestNo"), HyperLink)
            hyRequestNo.NavigateUrl = LinkToViewRequestNo("ACCMNT", hyRequestNo.Text.Trim)

        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim strSearchBy As New StringBuilder
            Dim strfilterby As New StringBuilder

            If cboSearch.SelectedItem.Value.Trim <> "0" Then
                If txtSearch.Text.Trim <> "" Then
                    If cboSearch.SelectedItem.Value.Trim = "1" Then
                        If Right(txtSearch.Text.Trim, 1) = "%" Then
                            strSearchBy.Append("WO.RequestNaNo like '" & txtSearch.Text.Trim & "' and ")
                        Else
                            strSearchBy.Append("WO.RequestNaNo = '" & txtSearch.Text.Trim & "' and ")
                        End If
                        strfilterby.Append("Prepayment No. = " & txtSearch.Text.Trim & " and ")
                    ElseIf cboSearch.SelectedItem.Value.Trim = "2" Then
                        If Right(txtSearch.Text.Trim, 1) = "%" Then
                            strSearchBy.Append("Agreement.AgreementNo like '" & txtSearch.Text.Trim & "' and ")
                        Else
                            strSearchBy.Append("Agreement.AgreementNo = '" & txtSearch.Text.Trim & "' and ")
                        End If
                        strfilterby.Append("Agreement No. = " & txtSearch.Text.Trim & " and ")
                    ElseIf cboSearch.SelectedItem.Value.Trim = "3" Then
                        If Not IsNumeric(txtSearch.Text.Trim) Then
                            If Right(txtSearch.Text.Trim, 1) = "%" Then
                                strSearchBy.Append("Customer.Name like '" & txtSearch.Text.Trim & "' and ")
                            Else
                                strSearchBy.Append("Customer.Name = '" & txtSearch.Text.Trim & "' and ")
                            End If
                            strfilterby.Append("Customer Name = " & txtSearch.Text.Trim & " and ")
                        End If
                    End If
                End If
            End If
            If EffDate.Text.Trim <> "" Then
                strSearchBy.Append("WO.RequestDate = '" & ConvertDate2(EffDate.Text.Trim).ToString("yyyyMMdd") & "' and ")
                strfilterby.Append("Effective Date = " & EffDate.Text.Trim & " and ")
            End If
            If cbostatus.SelectedItem.Value.Trim <> "0" Then
                strSearchBy.Append("WO.ApprovalStatus = '" & cbostatus.SelectedItem.Value.Trim & "' and ")
                strfilterby.Append("Status = " & cbostatus.SelectedItem.Text.Trim & " and ")
            End If
            If cboParent.SelectedItem.Value.Trim <> "ALL" Then
                strSearchBy.Append("WO.branchID = '" & cboParent.SelectedItem.Value.Trim & "'")
                'Else
                '    If strSearchBy.ToString.Trim <> "" Then
                '        strSearchBy = Left(SearchBy, Len(SearchBy.Trim) - 4)
                '    End If
            End If
            Me.SearchBy = strSearchBy.ToString
            'If strfilterby.ToString <> "" Then
            '    filterby = Left(filterby, Len(filterby.Trim) - 4)
            'End If
            Me.FilterBy = strfilterby.ToString
            BindGrid(Me.SearchBy, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
            DtgStopAccruedInquiry.Visible = True
        End If

    End Sub
#Region "linkTo"
    Function LinkToViewRequestNo(ByVal strStyle As String, ByVal strRequestNo As String) As String
        Return "javascript:OpenWinViewRequestWONo('" & strStyle & "','" & strRequestNo & "')"
    End Function
#End Region

    Private Sub DtgStopAccruedInquiry_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgStopAccruedInquiry.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Response.Redirect("WriteOffInquiry.aspx")
    End Sub

End Class