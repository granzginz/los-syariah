﻿
Imports System.IO
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController

Public MustInherit Class InstallRcv3rdPostBase
    Inherits InstallRcv3rdBase
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected Shadows pageSize As Int16 = 15
    Protected WithEvents btnFind As Global.System.Web.UI.WebControls.Button

    Protected Overloads Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        doBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Protected Overloads Sub doBind(Optional isFrNav As Boolean = False)
        lblMessage.Text = ""
        controller = New InstallRcv3rdController()
        Dim result = controller.GetUploadFilePage(GetConnectionString(), currentPage, pageSize, ConvertDate2(txtPeriodeFrom.Text), ViaType)

        recordCount = result.TotalRecords

        DtgAgree.DataSource = result.DataSetResult.Tables(0)
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        If (result.DataSetResult.Tables(0).Rows.Count <= 0) Then
            ShowMessage(lblMessage, "Tidak Ada Data", False)
        End If


        lblTotalTransaksi.Text = "0"
        lblTotalPembayaran.Text = "0"
        lblBankPenerima.Text = "-"
        lblTotalFeePemTransaksi.Text = "0"
        lblRekeningBankPenerima.Text = ""
        lblTotalPPN.Text = "0"
        lblTotalTrnsferKeBank.Text = ""
        btnProses.Visible = False
        If (result.DataSetResult.Tables(1).Rows.Count > 0) Then
            lblTotalTransaksi.Text = result.DataSetResult.Tables(1).Rows(0)("TotalRec").ToString
            lblTotalPembayaran.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("Totalbayar"), 0)

            lblBankPenerima.Text = result.DataSetResult.Tables(1).Rows(0)("BankName").ToString
            lblTotalFeePemTransaksi.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("TotalFee"), 0)

            lblRekeningBankPenerima.Text = result.DataSetResult.Tables(1).Rows(0)("RekeningBank").ToString
            lblTotalPPN.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("PPN"), 0)

            lblTotalTrnsferKeBank.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("totalTransfer"), 0)
            btnProses.Visible = True
        End If
        pnlUpload.Visible = False
        pnlConfirmGrid.Visible = True

    End Sub

    Protected Sub doPreBind(type As EnViaType)
        ViaType = type
        doBind()
    End Sub
    Private Function getChecked() As IList(Of String)
        Dim chkDtList As CheckBox
        Dim selectedPV As IList(Of String) = New List(Of String)
        For Each item In DtgAgree.Items
            chkDtList = CType(item.FindControl("ItemCheckBox"), CheckBox)
            If (Not Object.ReferenceEquals(chkDtList, Nothing)) Then
                If chkDtList.Checked Then
                    selectedPV.Add(CType(item.FindControl("lbllogId"), Label).Text.Trim)
                End If
            End If
        Next 
        Return selectedPV
    End Function
    Protected Sub doPostInstallment(type As EnViaType)
        Try
            Dim selectedPV As IList(Of String) = getChecked()
            If selectedPV.Count <= 0 Then
                ShowMessage(lblMessage, "silahkan pilih data terlebih dahulu", True)
                Return
            End If

            ViaType = type
            controller = New InstallRcv3rdController()

            Dim result = controller.DoPostingInstallmentUpload(GetConnectionString(), sesBranchId.Replace("'", ""), ConvertDate2(txtPeriodeFrom.Text), BusinessDate, SesCompanyID, type, selectedPV)
            'Dim result = controller.PostingInstallmentUpload(GetConnectionString(), SesCompanyID, sesBranchId.Replace("'", ""), ConvertDate2(txtPeriodeFrom.Text), BusinessDate, Loginid, type)
            doBind()
            ShowMessage(lblMessage, result, False)
        Catch ex As Exception
            doBind()
            ShowMessage(lblMessage, ex.Message, True)
        End Try
         
    End Sub
End Class
