﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcvIndomaretPost.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InstallRcvIndomaretPost" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script  type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
        <style> .lblNum { float: right; padding-right: 290px; } </style>


</head>
<body>
 <script type="text/javascript">

     $(document).ready(function () {
         $("#DtgAgree_chkHeader").click(function () {
             $("#<%=DtgAgree.ClientID%> input[id*='ItemCheckBox']:checkbox").attr('checked', $(this).prop('checked'));
         });

         $('#btnProses').click(function () {
             if ($('input:checkbox[id^="DtgAgree_ItemCheckBox_"]:checked').length <= 0) {
                 alert("Silahkan pilih data terlebih dahulu");
                 return false;
             }
             return true;
         });
     });
         
    </script>

    <form id="form1" runat="server">
   <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>PENERIMAAN VIA INDOMARET</h3>
        </div>
    </div>

     <asp:Panel ID="pnlUpload" runat="server">
            <div class="form_box">
                <label>Tanggal Transaksi</label>
                <uc1:ucDateCE ID="txtPeriodeFrom" runat="server" />             
            </div>
     
            <div class="form_button">
                <asp:Button ID="btnFind" runat="server" Text="Search"  CssClass="small button green" />
                <asp:Button ID="btnCancelC" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
            </div>
    </asp:Panel>


     <asp:Panel ID="pnlConfirmGrid" runat="server">

       <div class="form_box">
            <div class="form_left">
                <label>Total Transaksi</label>
                <asp:Label ID="lblTotalTransaksi" runat="server" />
            </div>
            <div class="form_right">
                <label>Total Pembayaran</label> 
                <asp:Label ID="lblTotalPembayaran" class='lblNum' runat="server" />
            </div>
        </div>

         <div class="form_box">
            <div class="form_left">
                <label>Bank Penerima</label>
                <asp:Label ID="lblBankPenerima" runat="server" />
            </div>
            <div class="form_right">
                <label>Total Fee Transaksi</label> 
                <asp:Label ID="lblTotalFeePemTransaksi" class='lblNum' runat="server" />
            </div>
        </div>

        <div class="form_box">
            <div class="form_left">
                <label>Rekening Bank Penerima</label>
                <asp:Label ID="lblRekeningBankPenerima" runat="server" />
            </div>
            <div class="form_right">
                <label>Total PPN</label> 
                <asp:Label ID="lblTotalPPN"  class='lblNum' runat="server" />
            </div>
        </div>

        <div class="form_box">
            <div class="form_left"> 
            </div>
            <div class="form_right">
                <label>Total Transfer Ke Bank</label> 
                <asp:Label ID="lblTotalTrnsferKeBank" class='lblNum' runat="server" />
            </div>
        </div>
         
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0" BorderStyle="None" CssClass="grid_general"  AllowSorting="True">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="PILIH">
                        <HeaderTemplate> <asp:CheckBox ID="chkHeader" runat="server" /> </HeaderTemplate>
                        <ItemTemplate>  
                            <asp:CheckBox ID="ItemCheckBox" runat="server" />
                          </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:BoundColumn DataField="branchid" HeaderText="BRANCH" />
                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK" HeaderStyle-Width="120">
                        <ItemTemplate>
                            <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                        <ItemTemplate>
                            <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>' />
                             <asp:Label ID="lblApplicationid"  Visible="False"   runat="server" Text='<%#Container.DataItem("Applicationid")%>' />
                            <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>' />
                             <asp:Label ID="lbllogId" runat="server" Visible="False" Text='<%#Container.DataItem("LogId")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="LicensePlate" HeaderText="NO. POLISI" />
                    <asp:BoundColumn DataField="MerekType" HeaderText="MERK/TYPE" />
                    <asp:BoundColumn DataField="MerchantId" HeaderText="MERCHANT ID" />
                    <asp:BoundColumn DataField="TransactionDate" HeaderText="TGL. BAYAR" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundColumn DataField="InstallAmount" HeaderText="JUMLAH" DataFormatString="{0:###,###,###}"/>
                    <asp:BoundColumn DataField="fee" HeaderText="Fee" DataFormatString="{0:###,###,###}"/>
                </Columns>
                </asp:DataGrid>
                   <uc2:ucGridNav id="GridNavigator" runat="server"/>  
                </div>
            </div>
            <div class="form_button">
               <asp:Button ID="btnProses" runat="server" Text="Proses"  CssClass="small button green" /> 
                <asp:Button ID="btnCancelProses" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
            </div>
        </div>
    </asp:Panel>

    </form>
</body>
</html>
