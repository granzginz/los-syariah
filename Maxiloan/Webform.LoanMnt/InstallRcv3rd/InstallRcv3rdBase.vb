﻿Imports System.IO
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController

Public MustInherit Class InstallRcv3rdBase
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    Protected WithEvents pnlUpload As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents uplInvoice As Global.System.Web.UI.WebControls.FileUpload
    Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnCancelC As Global.System.Web.UI.WebControls.Button
    Protected WithEvents pnlConfirmGrid As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents lblTotalTransaksi As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblTotalPembayaran As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblBankPenerima As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblTotalFeePemTransaksi As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblRekeningBankPenerima As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblTotalPPN As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblTotalTrnsferKeBank As Global.System.Web.UI.WebControls.Label
    Protected WithEvents DtgAgree As Global.System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnProses As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnCancelProses As Global.System.Web.UI.WebControls.Button
    Protected WithEvents GridNavigator As ucGridNav
     
    Protected controller As InstallRcv3rdController
    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 20
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1
     
    Protected Property SessionId() As String
        Get
            Return CType(ViewState("SESSIONID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SESSIONID") = Value
        End Set
    End Property
    Protected Property ViaType() As EnViaType
        Get
            Return CType(ViewState("EnViaType"), EnViaType)
        End Get
        Set(ByVal Value As EnViaType)
            ViewState("EnViaType") = Value
        End Set
    End Property
    Protected Function pathFile() As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\LoanMnt\InstallRcv\" & Me.sesBranchId & "\"
        If Not Directory.Exists(strDirectory) Then
            Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

     
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        doBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

     
    Protected Sub doBind(Optional isFrNav As Boolean = False)
        controller = New InstallRcv3rdController()
        Dim result = controller.GetUploadFilePage(GetConnectionString(), currentPage, pageSize, SessionId, ViaType)

        recordCount = result.TotalRecords

        DtgAgree.DataSource = result.DataSetResult.Tables(0)
        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If


        'lblTotalTransaksi.Text = result.DataSetResult.Tables(1).Rows(0)("TotalRec").ToString
        'lblTotalPembayaran.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("Totalbayar"), 0)

        'lblBankPenerima.Text = result.DataSetResult.Tables(1).Rows(0)("BankName").ToString
        'lblTotalFeePemTransaksi.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("TotalFee"), 0)

        'lblRekeningBankPenerima.Text = result.DataSetResult.Tables(1).Rows(0)("RekeningBank").ToString
        'lblTotalPPN.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("PPN"), 0)

        'lblTotalTrnsferKeBank.Text = FormatNumber(result.DataSetResult.Tables(1).Rows(0)("totalTransfer"), 0)
        pnlUpload.Visible = False
        pnlConfirmGrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Protected Sub doSave(sr As StreamReader, type As EnViaType)
        Dim datas = New InstallRcv3rd 
        Try
            ViaType = type 
            Dim ret = InstallRcv3rd.DoParse(sr, type, BusinessDate)

            If (ret.InstallRcv3rdRows.Count <= 0) Then
                ShowMessage(lblMessage, "CSV File Tidak Ada Data", False)
                Return
            End If

            controller = New InstallRcv3rdController()
            controller.DoUpload3rdFile(GetConnectionString, ret, SessionId)
            doBind()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            If Not (sr Is Nothing) Then
                sr.Dispose()
            End If
        End Try
    End Sub


    'Protected Sub doSave(fullFileName As String, type As EnViaType)
    '    Dim datas = New InstallRcv3rd
    '    Dim sr As StreamReader = Nothing
    '    Try
    '        ViaType = type
    '        uplInvoice.PostedFile.SaveAs(fullFileName)
    '        sr = New StreamReader(fullFileName)

    '        Dim ret = InstallRcv3rd.DoParse(sr, type, BusinessDate)

    '        If (ret.InstallRcv3rdRows.Count <= 0) Then
    '            ShowMessage(lblMessage, "CSV File Tidak Ada Data", False)
    '            Return
    '        End If

    '        controller = New InstallRcv3rdController()
    '        controller.DoUpload3rdFile(GetConnectionString, ret, SessionId)
    '        doBind()

    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    Finally
    '        If Not (sr Is Nothing) Then
    '            sr.Dispose()
    '        End If
    '    End Try
    'End Sub



    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            Dim lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)

            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            Dim lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            Dim lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub

    MustOverride Sub responseCancel()
    Protected Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    Protected Sub btnCancelProses_Click(sender As Object, e As EventArgs) Handles btnCancelProses.Click
        responseCancel()
    End Sub
End Class
