﻿Imports System.IO
Public Class InstallRcvPermata
    Inherits InstallRcv3rdBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        Me.FormID = "INSRCVPMT"
        lblMessage.Text = ""
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then
            If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then Exit Sub

            Dim result = BitConverter.ToInt64(Guid.NewGuid.ToByteArray(), 8)
            SessionId = result.ToString
            pnlUpload.Visible = True
            pnlConfirmGrid.Visible = False
        End If
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'Dim fullFileName As String = String.Format("{0}PKSFINNET{1}{2}{3}{4}{5}", pathFile(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), DateTime.Now.Hour.ToString(), DateTime.Now.Minute.ToString(), DateTime.Now.Second.ToString(), Path.GetExtension(uplInvoice.PostedFile.FileName))
        'doSave(fullFileName, Parameter.EnViaType.Finnet

        If (uplInvoice.HasFile) Then
            Dim reader = New StreamReader(uplInvoice.FileContent)
            doSave(reader, Parameter.EnViaType.Permata)
        End If
    End Sub
    Overrides Sub responseCancel()
        Response.Redirect("InstallRcvPermata.aspx")
    End Sub
    'Protected Sub btnCancelProses_Click(sender As Object, e As EventArgs) Handles btnCancelProses.Click
    '    Response.Redirect("InstallRcvFinnet.aspx")
    'End Sub
End Class