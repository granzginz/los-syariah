﻿
Imports System.IO
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Public Class InstallRcv3rdGenPaymentGeteway
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected Shadows pageSize As Int16 = 15

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If

        Me.FormID = "INSRCVUPLD"
        lblMessage.Text = "" 
        If Not Me.IsPostBack Then
            If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then Exit Sub
            Dim result = BitConverter.ToInt64(Guid.NewGuid.ToByteArray(), 8)
            txtPeriodeFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            pnlUpload.Visible = True
            pnlInfo.Visible = False
            pnlGenerate.Visible = False
        End If
    End Sub
    Protected Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
     
    Protected Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        If (txtPeriodeFrom.Text.Trim = "") Then
            Return
        End If
        lblMessage.Text = ""
        getFiles()
    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        Try 
            Dim Controller = New InstallRcv3rdController()
            Dim result = Controller.GeneratePaymentGatewayFile(GetConnectionString(), ConvertDate2(txtPeriodeFrom.Text.Trim), pathFile)
            If (result) Then
                getFiles()
                Return
            End If
            ShowMessage(lblMessage, "Maaf, Telah terjadi kesalahan generate file.", True)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub getFiles()

        Dim Controller = New InstallRcv3rdController()
        Dim result As DataTable = Controller.GetPaymentGatewayFile(GetConnectionString(), ConvertDate2(txtPeriodeFrom.Text.Trim))

        If result.Rows.Count = 0 Then
            pnlInfo.Visible = False
            pnlGenerate.Visible = True
            Return
        End If
        pnlInfo.Visible = True
        pnlGenerate.Visible = False
        Dim FileDirectory = IIf(IsDBNull(result.Rows(0)("FileDirectory")), "", result.Rows(0).Item("FileDirectory").Trim())
        lblTglUpload.Text = IIf(IsDBNull(result.Rows(0)("DateOfUpload")), "", CDate(result.Rows(0).Item("DateOfUpload")).ToString("dd/MM/yyyy"))
        lblJumlahTagihan.Text = IIf(IsDBNull(result.Rows(0)("InstallmentCount")), "0", result.Rows(0).Item("InstallmentCount").ToString())
        lblTotalTagihan.Text = IIf(IsDBNull(result.Rows(0)("TotalInstallment")), "0", FormatNumber(result.Rows(0).Item("TotalInstallment"), 2).Trim)
         
        lnkIndomaret.Text = IIf(IsDBNull(result.Rows(0)("IndomaretFileName")), "", result.Rows(0).Item("IndomaretFileName").ToString())
        lnkIndomaret.Enabled = File.Exists(String.Format("{0}{1}", pathFile, lnkIndomaret.Text))
       

        lnkFilePos.Text = IIf(IsDBNull(result.Rows(0)("PtPosFileName")), "", result.Rows(0).Item("PtPosFileName").ToString())
        lnkFilePos.Enabled = File.Exists(String.Format("{0}{1}", pathFile, lnkFilePos.Text))

        'lnkFileFinnet.Text = IIf(IsDBNull(result.Rows(0)("FinnetFileName")), "", result.Rows(0).Item("FinnetFileName").ToString())
        'lnkFileFinnet.Enabled = File.Exists(String.Format("{0}{1}", pathFile, lnkFileFinnet.Text))

        lnkFileBCA.Text = IIf(IsDBNull(result.Rows(0)("BCAFileName")), "", result.Rows(0).Item("BCAFileName").ToString())
        lnkFileBCA.Enabled = File.Exists(String.Format("{0}{1}", pathFile, lnkFileBCA.Text))

        lnkFilePermataHeader.Text = IIf(IsDBNull(result.Rows(0)("PermataFileName")).ToString().Replace("@", "1"), "", result.Rows(0).Item("PermataFileName").ToString().Replace("@", "1"))
        lnkFilePermataHeader.Enabled = File.Exists(String.Format("{0}{1}", pathFile, lnkFilePermataHeader.Text))

        lnkFilePermataDetail.Text = IIf(IsDBNull(result.Rows(0)("PermataFileName")).ToString().Replace("@", "2"), "", result.Rows(0).Item("PermataFileName").ToString().Replace("@", "2"))
        lnkFilePermataDetail.Enabled = File.Exists(String.Format("{0}{1}", pathFile, lnkFilePermataDetail.Text))

        'If (Not lnkIndomaret.Enabled Or Not lnkFilePos.Enabled Or Not lnkFileFinnet.Enabled Or Not lnkFilePermataHeader.Enabled Or Not lnkFilePermataDetail.Enabled Or Not lnkFileBCA.Enabled) Then
        If (Not lnkIndomaret.Enabled Or Not lnkFilePos.Enabled Or Not lnkFilePermataHeader.Enabled Or Not lnkFilePermataDetail.Enabled Or Not lnkFileBCA.Enabled) Then
            btnGenerate_Click(Nothing, Nothing)
        End If

    End Sub
    Protected Function pathFile() As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\LoanMnt\InstallRcv\" & Me.sesBranchId.Replace("'", "") & "\"
        If Not Directory.Exists(strDirectory) Then
            Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Sub download(thefile As String, strFriendlyName As String)
        Try

        
        Dim fs As System.IO.FileStream = Nothing
        fs = System.IO.File.Open(thefile, System.IO.FileMode.Open)

        Dim btFile(fs.Length) As Byte
        fs.Read(btFile, 0, fs.Length)
        fs.Close()
        With Response
            .AddHeader("Content-disposition", "attachment;filename=" & strFriendlyName)
            .ContentType = "application/octet-stream"
            .BinaryWrite(btFile)
            .End()
            End With
        Catch ex As Exception
            Throw New Exception()
        End Try
    End Sub
     
    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles lnkIndomaret.Click
        Try 
            download(String.Format("{0}{1}", pathFile, lnkIndomaret.Text), lnkIndomaret.Text) 
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub lnkFilePos_Click(sender As Object, e As EventArgs) Handles lnkFilePos.Click
        Try
            download(String.Format("{0}{1}", pathFile, lnkFilePos.Text), lnkFilePos.Text)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    'Protected Sub lnkFileFinnet_Click(sender As Object, e As EventArgs) Handles lnkFileFinnet.Click
    '    Try
    '        download(String.Format("{0}{1}", pathFile, lnkFileFinnet.Text), lnkFileFinnet.Text)
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub

    Protected Sub lnkFileBCA_Click(sender As Object, e As EventArgs) Handles lnkFileBCA.Click
        Try
            download(String.Format("{0}{1}", pathFile, lnkFileBCA.Text), lnkFileBCA.Text)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub lnkFilePermataHeader_Click(sender As Object, e As EventArgs) Handles lnkFilePermataHeader.Click
        Try
            download(String.Format("{0}{1}", pathFile, lnkFilePermataHeader.Text), lnkFilePermataHeader.Text)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub lnkFilePermataDetail_Click(sender As Object, e As EventArgs) Handles lnkFilePermataDetail.Click
        Try
            download(String.Format("{0}{1}", pathFile, lnkFilePermataDetail.Text), lnkFilePermataDetail.Text)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
End Class