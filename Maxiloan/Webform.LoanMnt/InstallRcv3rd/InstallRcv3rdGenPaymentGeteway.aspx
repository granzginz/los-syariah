﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InstallRcv3rdGenPaymentGeteway.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.InstallRcv3rdGenPaymentGeteway" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title></title>
     <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script  type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
        <style> .lblNum { float: right; padding-right: 290px; } </style>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>Upload File Payment Gateway</h3>
        </div>
    </div>

     <asp:Panel ID="pnlUpload" runat="server">
            <div class="form_box">
                <label>Tanggal Upload</label>
                <uc1:ucDateCE ID="txtPeriodeFrom" runat="server" />             
            </div>
     
            <div class="form_button">
                <asp:Button ID="btnFind" runat="server" Text="Search"  CssClass="small button green" />
                <asp:Button ID="btnCancelC" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
            </div>
    </asp:Panel>

      <asp:Panel ID="pnlGenerate" runat="server">
            <div class="form_single">
                    <h3>File Belum Tersedia Silahkan Generate.</h3>
            </div>
            <div class="form_button">
                <asp:Button ID="btnGenerate" runat="server" Text="Generate"  CssClass="small button green" /> 
            </div>
        </asp:Panel>

     <asp:Panel ID="pnlInfo" runat="server">
     
     <div class="form_title">
        <div class="title_strip"> </div>
        <div class="form_single">
            <h3>File Payment Gateway</h3>
        </div>
    </div>

        <div class="form_box">
            <label>Tanggal Upload</label>
            <asp:Label ID="lblTglUpload" runat="server" />
        </div>

      
        <div class="form_box">
            <label>Jumlah Tagihan</label>
            <asp:Label ID="lblJumlahTagihan" runat="server" />
        </div>

        <div class="form_box">
            <label>Total Tagihan</label>
            
            <asp:Label ID="lblTotalTagihan" runat="server" />
        </div>

        <div class="form_box">
            <label>Indomaret</label>  
             <asp:LinkButton ID="lnkIndomaret" runat="server" OnClick="btnDownload_Click" />
        </div>

        <div class="form_box">
            <label>PT. POS</label>
              <asp:LinkButton ID="lnkFilePos" runat="server" OnClick="lnkFilePos_Click" />
             
        </div>

<%--        <div class="form_box">
            <label>FINNET</label>
             <asp:LinkButton ID="lnkFileFinnet" runat="server" OnClick="lnkFileFinnet_Click" /> 
        </div>
--%>
        <div class="form_box">
            <label>BCA</label>
             <asp:LinkButton ID="lnkFileBCA" runat="server" OnClick="lnkFileBCA_Click" /> 
        </div>

        <div class="form_box">
            <label>PERMATA HEADER</label>
             <asp:LinkButton ID="lnkFilePermataHeader" runat="server" OnClick="lnkFilePermataHeader_Click" /> 
        </div>
        <div class="form_box">
            <label>PERMATA DETAIL</label>
             <asp:LinkButton ID="lnkFilePermataDetail" runat="server" OnClick="lnkFilePermataDetail_Click" /> 
        </div>

     </asp:Panel>
    </form>
</body>
</html>
