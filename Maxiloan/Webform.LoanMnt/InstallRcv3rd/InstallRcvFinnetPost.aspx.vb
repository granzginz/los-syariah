﻿Imports System.IO
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController

Public Class InstallRcvFinnetPost
    Inherits InstallRcv3rdPostBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        Me.FormID = "INSRCVFNTPS"
        lblMessage.Text = ""
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then
            If Not CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then Exit Sub
            Dim result = BitConverter.ToInt64(Guid.NewGuid.ToByteArray(), 8)
            txtPeriodeFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            pnlUpload.Visible = True
            pnlConfirmGrid.Visible = False
        End If

    End Sub

    Overrides Sub responseCancel()
        Response.Redirect("InstallRcvFinnetPost.aspx")
    End Sub

    Protected Sub BtnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        If (txtPeriodeFrom.Text.Trim = "") Then
            Return
        End If
        doPreBind(EnViaType.Finnet)
    End Sub

    Protected Sub btnProses_Click(sender As Object, e As EventArgs) Handles btnProses.Click
        
        doPostInstallment(EnViaType.Finnet)
    End Sub
End Class