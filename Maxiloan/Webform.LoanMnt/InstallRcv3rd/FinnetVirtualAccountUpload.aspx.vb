﻿ 
Imports System.IO
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController


Public Class FinnetVirtualAccountUpload
    Inherits InstallRcv3rdBase
    Protected Property AgreementVAcc() As AgreementVirtualAcc
        Get
            Return CType(ViewState("AgreementVirtualAcc"), AgreementVirtualAcc)
        End Get
        Set(ByVal Value As AgreementVirtualAcc)
            ViewState("AgreementVirtualAcc") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        Me.FormID = "FINNETVAUPLD"
        lblMessage.Text = ""

        If Not IsPostBack Then
            If Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("Message"), False)
            End If

            pnlUpload.Visible = True
            pnlConfirmGrid.Visible = False
        End If
    End Sub

    Overrides Sub responseCancel()
        Response.Redirect("FinnetVirtualAccountUpload.aspx")
    End Sub
     

    Protected Sub BtnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click 
        If (uplInvoice.HasFile) Then
            Dim reader = New StreamReader(uplInvoice.FileContent)
            _doSave(reader, Parameter.EnViaType.Finnet)
        End If 
    End Sub

    Sub _doSave(sr As StreamReader, type As EnViaType)
        Dim datas = New AgreementVirtualAcc
        Try
            ViaType = type
            Dim ret = AgreementVirtualAcc.DoParse(sr, type, "FINNET")

            If (ret.VirtualAccountRows.Count <= 0) Then
                ShowMessage(lblMessage, "File Tidak Ada Data", False)
                Return
            End If

            If (ret.VirtualAccountRows.Count > 10000) Then
                ShowMessage(lblMessage, "Jumlah Record Terlalu banyak. Maximum 10000 records.", True)
                Return
            End If

            AgreementVAcc = ret


            'controller = New InstallRcv3rdController()
            'controller.DoUploadAgreementVirtualAccount(GetConnectionString, AgreementVAcc)
            'Response.Redirect("FinnetVirtualAccountUpload.aspx?Message=Proses Upload Berhasil.")

            Try
                DtgAgree_.DataSource = ret.VirtualAccountRows
                DtgAgree_.DataBind()
                pnlUpload.Visible = False
                pnlConfirmGrid.Visible = True
            Catch 
            End Try

            
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            If Not (sr Is Nothing) Then
                sr.Dispose()
            End If
        End Try
    End Sub

    Protected Sub btnProses_Click(sender As Object, e As EventArgs) Handles btnProses.Click

        Try
            controller = New InstallRcv3rdController()
            controller.DoUploadAgreementVirtualAccount(GetConnectionString, AgreementVAcc)
            Response.Redirect("FinnetVirtualAccountUpload.aspx?Message=Proses Upload Berhasil.")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

End Class