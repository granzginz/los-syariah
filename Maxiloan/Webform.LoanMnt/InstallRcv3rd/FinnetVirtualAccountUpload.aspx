﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinnetVirtualAccountUpload.aspx.vb" Inherits="Maxiloan.Webform.LoanMnt.FinnetVirtualAccountUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script  type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <style> .lblNum { float: right; padding-right: 290px; } </style> 
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip"> </div>
        <div class="form_single"> <h3>UPLOAD FINNET VIRTUAL ACCOUNT</h3> </div>
    </div>
    <asp:Panel ID="pnlUpload" runat="server">
            <div class="form_box">
                <label class="label">Finnet VA File </label>
                <asp:FileUpload ID="uplInvoice" runat="server" accept="text/plain" /> 
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="validator_general"   Display="Dynamic" runat="server" ControlToValidate="uplInvoice" ErrorMessage="Silahkan Pilih Finnet VA File.." />
            </div>
     
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Upload"  CssClass="small button green" />
                <asp:Button ID="btnCancelC" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
            </div>
    </asp:Panel>

       <asp:Panel ID="pnlConfirmGrid" runat="server">

       <div class="form_button">
                 <asp:Button ID="btnProses" runat="server" Text="Proses"  CssClass="small button green" /> 
                <asp:Button ID="btnCancelProses" runat="server" Text="Close" CssClass="small button gray"  CausesValidation="False" />
       </div>

       <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                <asp:GridView ID="DtgAgree_" runat="server" AutoGenerateColumns="False" BorderWidth="0" BorderStyle="None" CssClass="grid_general"  AllowSorting="True">
                <HeaderStyle CssClass="th" /> 
                <Columns>
                    <asp:BoundField DataField="AgreementNo" HeaderText="No Kontrak" />
                   <asp:BoundField DataField="Nama" HeaderText="Nama" />
                    <asp:BoundField DataField="VirtualAccount" HeaderText="Virtual Account" /> 
                </Columns> 
                </asp:GridView>
            </div>
             </div>
        </div>


       </asp:Panel>
     
    </form>
</body>
</html>
