﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GL_temporary.aspx.vb"
    Inherits="Maxiloan.Webform.LoanMnt.GL_temporary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GL_temporary</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function delaction() {
            if (confirm("Apakah yakin mau hapus data ini ? ")) {
                return true;
            }
            else {
                return false;
            }
        }
								
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                JURNAL TRANSAKSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang
            </label>
            <asp:DropDownList ID="oBranch" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                Display="Dynamic" ErrorMessage="Harap Pilih Cabang" ControlToValidate="oBranch"
                CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Periode
            </label>
            <asp:TextBox runat="server" ID="txtDateFrom"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDateFrom"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            &nbsp;S/D&nbsp;
            <asp:TextBox runat="server" ID="txtDateTo"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDateTo"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Jenis Transaksi
            </label>
            <asp:DropDownList ID="ddlTransactionType" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                Display="Dynamic" ErrorMessage="Harap pilih Jenis Transaksi" ControlToValidate="ddlTransactionType"
                CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan
            </label>
            <asp:DropDownList ID="ddlSearchBy" runat="server">
                <asp:ListItem Value="SO" Selected="True">Select One</asp:ListItem>
                <asp:ListItem Value="reff_no">No Reff</asp:ListItem>
                <asp:ListItem Value="tr_desc">Keterangan</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchBy" runat="server" Width="314px"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlPaging" runat="server" Visible="True">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgIndType" runat="server" Width="100%" CssClass="grid_general"
                        OnSortCommand="Sorting" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        DataKeyField="tr_nomor">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="transactionid" SortExpression="transactionid" HeaderText="TRANSAKSI">
                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="tr_nomor" HeaderText="NO VOUCHER">
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBatchNo" runat="server" CommandName="ShowView" Text='<%# Container.DataItem("tr_nomor")%>'
                                        CausesValidation="False">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                              <asp:BoundColumn DataField="CustomerName" HeaderText="Customer" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                               
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="tr_date" HeaderText="TANGGAL">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_date" runat="server">
													<%# format(Container.DataItem("tr_date"),"dd MMM yyyy")%>
                                    </asp:Label>
                                    <input type="hidden" id="hid_date" runat="server" value='<%# container.dataitem("tr_date")%>'
                                        name="hid_date" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="tr_desc" HeaderText="KETERANGAN">
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="status" HeaderText="STATUS">
                                <ItemStyle Width="5%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="reff_no" HeaderText="NO REFF" ItemStyle-Width="20%">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL PAGING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server"  CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah"
                            CssClass="validator_general" Display="Dynamic" Type="Integer" MaximumValue="999999999"
                            MinimumValue="1"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                            ErrorMessage="No Halaman Salah" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button" style="display:none">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
