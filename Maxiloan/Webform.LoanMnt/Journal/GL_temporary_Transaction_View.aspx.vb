﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class GL_temporary_Transaction_View
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents add_lblcompanybranch As System.Web.UI.WebControls.Label
    Protected WithEvents add_lbldesc As System.Web.UI.WebControls.Label
    Protected WithEvents add_lbltransaction As System.Web.UI.WebControls.Label
    Protected WithEvents add_lblref As System.Web.UI.WebControls.Label
    Protected WithEvents add_lbltr_nomor As System.Web.UI.WebControls.Label
    Protected WithEvents add_lblrefdate As System.Web.UI.WebControls.Label
    Protected WithEvents add_lbltrdate As System.Web.UI.WebControls.Label
    Protected WithEvents DtgIndType As System.Web.UI.WebControls.DataGrid
    Protected WithEvents PnlPaging As System.Web.UI.WebControls.Panel
    Protected WithEvents btn_Trans_view_back As System.Web.UI.WebControls.Button
    Protected WithEvents outError As System.Web.UI.HtmlControls.HtmlGenericControl
#End Region

#Region "Public Code"
    Private Property Date_From() As String
        Get
            Return CType(viewstate("datefrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("datefrom") = Value
        End Set
    End Property
    Private Property Date_To() As String
        Get
            Return CType(viewstate("dateto"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("dateto") = Value
        End Set
    End Property
    Private Property Transaction_Type() As String
        Get
            Return CType(viewstate("transactiontype"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("transactiontype") = Value
        End Set
    End Property
    Private Property Search_Type() As String
        Get
            Return CType(viewstate("searchtype"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("searchtype") = Value
        End Set
    End Property
    Private Property Search_Value() As String
        Get
            Return CType(viewstate("searchvalue"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("searchvalue") = Value
        End Set
    End Property
    Private Property Tr_Nomor() As String
        Get
            Return CType(viewstate("tr_nomor"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("tr_nomor") = Value
        End Set
    End Property
    Public ReadOnly Property datefrom() As String
        Get
            Return CType(viewstate("datefrom"), String)
        End Get
    End Property
    Public ReadOnly Property dateto() As String
        Get
            Return CType(viewstate("dateto"), String)
        End Get
    End Property
    Public ReadOnly Property transactiontype() As String
        Get
            Return CType(viewstate("transactiontype"), String)
        End Get
    End Property
    Public ReadOnly Property searchtype() As String
        Get
            Return CType(viewstate("searchtype"), String)
        End Get
    End Property
    Public ReadOnly Property searchvalue() As String
        Get
            Return CType(viewstate("searchvalue"), String)
        End Get
    End Property
#End Region
    Private Total As Double
    Private TotalD As Double
    Private TotalC As Double

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim lobjpage As GL_temporary
            lobjpage = CType(context.Handler, GL_temporary)
            With lobjpage
                Me.Date_From = .datefrom
                Me.Date_To = .dateto
                Me.Transaction_Type = .transactiontype
                Me.Search_Type = .searchtype
                Me.Search_Value = .searchvalue
                Me.SortBy = ""
            End With
            Me.Tr_Nomor = Request("tr_nomor")
            PageHeader()
            Bindgrid()
        End If
    End Sub
#End Region
#Region "Bindgrid"
    Sub Bindgrid(Optional ByVal pStrPageCommand As String = "")
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        Dim i As Integer
        Me.SearchBy = " tr_nomor='" & Me.Tr_Nomor & "' order by SequenceNo asc"
        With oContract
            .strConnection = GetConnectionString
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy

            .SpName = "spJournal2"
        End With
        oContract = cContract.GetGeneralPaging(oContract)

        DtUserList = oContract.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oContract.TotalRecords
        Try
            DtgIndType.DataSource = DvUserList
            DtgIndType.DataBind()
        Catch en As System.Web.HttpException
            DtgIndType.CurrentPageIndex = 0
            DtgIndType.DataBind()
        End Try
    End Sub
#End Region
#Region "PageHeader"
    Private Sub PageHeader()
        Dim strSQL As String
        Dim strConn As String
        Dim i As Integer
        Dim dtvoucher As Date
        Dim dtref As Date
        Dim objcommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        add_lbltr_nomor.Text = Me.Tr_Nomor
        strSQL = "select tr_date,tr_desc,reff_no,reff_date " & _
                 " from gljournalh where tr_nomor=@TrNomor"
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcommand.Parameters.Add("@TrNomor", SqlDbType.VarChar, 20).Value = Me.Tr_Nomor
            With objcommand
                .Connection = objcon
                .CommandText = strSQL
                objread = .ExecuteReader()
                If objread.Read() Then
                    dtvoucher = CDate(objread("tr_date"))
                    Try
                        add_lbltrdate.Text = dtvoucher.ToString("dd/MM/yyyy")
                    Catch en As Exception
                        add_lbltrdate.Text = ""
                    End Try
                    Try
                        add_lbldesc.Text = CType(objread("tr_desc"), String)
                    Catch en As Exception
                        add_lbldesc.Text = ""
                    End Try
                    Try
                        add_lblref.Text = CType(objread("reff_no"), String)
                    Catch es As Exception
                        add_lblref.Text = ""
                    End Try
                    Try
                        dtref = CDate(objread("reff_date"))
                        add_lblrefdate.Text = dtref.ToString("dd/MM/yyyy")
                    Catch es As Exception
                        add_lblrefdate.Text = ""
                    End Try
                End If
                objread.Close()
            End With
        Catch oExcept As Exception
            oExcept.Message.ToString()
        End Try

        strSQL = "select distinct coaco,coabranch,transactionid "
        strSQL &= " from gljournald where tr_nomor=@TrNomor"
        Try
            With objcommand
                .Connection = objcon
                .CommandText = strSQL
                objread = .ExecuteReader()
                If objread.Read() Then
                    add_lblcompanybranch.Text = CType(objread("coaco"), String) & "-" & CType(objread("coabranch"), String)
                    'add_lblcompanybranch.Width = Unit.Percentage(100)
                    add_lbltransaction.Text = CType(objread("transactionid"), String)
                End If
                objread.Close()
            End With
        Catch oExcept As Exception
            oExcept.Message.ToString()
        Finally
            objcommand.Parameters.Clear()
            objcommand.Dispose()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub
#End Region
#Region "Navigation"

#End Region
#Region "btn_Trans_view_back_Click"
    Private Sub btn_Trans_view_back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Trans_view_back.Click
        Server.Transfer("gl_temporary.aspx", False)
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgIndType.SortCommand
        Me.SortBy = e.SortExpression
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid()
    End Sub
#End Region

    Private Sub DtgIndType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgIndType.ItemDataBound
        Dim i As Integer
        Dim lblPost As Label
        Dim lblAmount As Label
        Dim lblAmountD As Label
        Dim lblAmountC As Label
        Dim add_lbltotal As Label
        Dim add_lbltotalD As Label
        Dim add_lbltotalC As Label
        Dim lblSelilih As Label

        If e.Item.ItemIndex >= 0 Then
            lblPost = CType(e.Item.FindControl("lblPost"), Label)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)

            lblAmountD = CType(e.Item.FindControl("lblAmountD"), Label)
            lblAmountC = CType(e.Item.FindControl("lblAmountC"), Label)



            If lblPost.Text.Trim = "C" Then
                Total = Total - CType(lblAmount.Text.Trim, Double)
            End If

            If lblPost.Text.Trim = "D" Then
                Total = Total + CType(lblAmount.Text.Trim, Double)
            End If

            TotalC = TotalC + CType(lblAmountC.Text.Trim, Double)
            TotalD = TotalD + CType(lblAmountD.Text.Trim, Double)

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            add_lbltotal = CType(e.Item.FindControl("add_lbltotal"), Label)
            add_lbltotal.Text = FormatNumber(Total, 2)
            add_lbltotalD = CType(e.Item.FindControl("add_lbltotalD"), Label)
            add_lbltotalD.Text = FormatNumber(TotalD, 2)
            add_lbltotalC = CType(e.Item.FindControl("add_lbltotalC"), Label)
            add_lbltotalC.Text = FormatNumber(TotalC, 2)

            lblSelilih = CType(e.Item.FindControl("lblSelilih"), Label)
            lblSelilih.Text = "Selisih: " + Total.ToString
        End If
    End Sub

End Class