﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class BatchDateChangeList
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Me.IsPostBack Then

            drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            Me.FormID = "FUNEDITBATCHDATE"

            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""
            bindComboCompany()
            Response.Write(GenerateScript(getComboContract, getComboBatchDate))
            lblMessage.Visible = False
            lblMessage.Text = ""
            pnlTop.Visible = True
            pnlList.Visible = False
        End If
    End Sub

#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region

#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContract"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Function
#End Region

#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception 
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Function
#End Region

#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception 
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
#End Region

    Public Function drdCompanyChange() As String

        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"

    End Function

    Public Function drdContractChange() As String

        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"

    End Function
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString

        pnlList.Visible = True
        pnlTop.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlTop.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlTop.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlTop.Visible = True
    End Sub

#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingChangeBatchDateListing"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        'drdCompany.SelectedIndex = 0
        bindComboCompany()
        Response.Write(GenerateScript(getComboContract, getComboBatchDate))
        PagingFooter()
    End Sub
#End Region
    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim hyBranch As HyperLink
        Dim hyContract As HyperLink
        Dim lblFundingCoyID As Label
        Dim lblBankID As Label
        Dim hyBatchNo As HyperLink
        Dim lblContractName As Label
        Dim lblBankName As Label

        hyBranch = CType(e.Item.FindControl("lnkBranch"), HyperLink)
        hyContract = CType(e.Item.FindControl("lnkContractNo"), HyperLink)
        lblBankName = CType(e.Item.FindControl("lblBankName"), Label)
        lblFundingCoyID = CType(e.Item.FindControl("lblFundingCoyID"), Label)
        lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
        hyBatchNo = CType(e.Item.FindControl("lnkBatchNo"), HyperLink)
        lblContractName = CType(e.Item.FindControl("lblContractName"), Label)

        If e.Item.ItemIndex >= 0 Then
            Me.CompanyID = lblFundingCoyID.Text.Trim
            Me.FundingContractNo = hyContract.Text.Trim
            Me.FundingBatchNo = hyBatchNo.Text.Trim
            hyBranch.NavigateUrl = "javascript:OpenFundingCompanyView('" & "channeling" & "', '" & lblFundingCoyID.Text.Trim & "')"
            hyContract.NavigateUrl = "javascript:OpenFundingCompanyContractView('" & "channeling" & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & hyContract.Text.Trim & "')"
            hyBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & lblFundingCoyID.Text.Trim & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & lblBankID.Text.Trim & "','" & lblContractName.Text.Trim & "','" & hyContract.Text.Trim & "','" & hyBatchNo.Text.Trim & "')"
        End If

    End Sub

    Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        Select Case e.CommandName
            Case "Change"
                Response.Redirect("BatchDateChangeProcess.aspx?CompanyID=" & Me.CompanyID & "&FundingContractNO=" & Me.FundingContractNo & "&FundingBatchNO=" & Me.FundingBatchNo)
        End Select
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim cmdWhere As String
        Me.SortBy = ""
        If drdCompany.SelectedItem.Value = "All" Then
            cmdWhere = ""
        Else
            If tempChild.Value.Trim = "-" Or tempChild.Value.Trim = "" Then
                cmdWhere = "FundingBatch.FundingCoyID = '" & drdCompany.SelectedItem.Value & "' "
            Else
                If tempChild2.Value.Trim = "-" Or tempChild2.Value.Trim = "" Then
                    cmdWhere = "FundingBatch.FundingCoyID = '" & drdCompany.SelectedItem.Value & "' " & _
                        " and FundingBatch.FundingContractNo = '" & tempChild.Value.Trim & "' "
                Else
                    cmdWhere = "FundingBatch.FundingCoyID = '" & drdCompany.SelectedItem.Value & "' " & _
                        " and FundingBatch.FundingContractNo = '" & tempChild.Value.Trim & "' " & _
                        " and FundingBatch.FundingBatchNo = '" & tempChild2.Value.Trim & "' "
                End If
            End If
        End If
        Me.SearchBy = cmdWhere
        BindGridEntity(cmdWhere, Me.SortBy)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("BatchDateChangeList.aspx")
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtContract As DataTable = getComboContract()
        Dim i As Integer

        For i = 0 To DtContract.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdContract.UniqueID, CStr(DtContract.Rows(i).Item("FundingContractNo")).Trim)
        Next

        RegisterFirstItem(drdContract)

        Dim DtBatchDate As DataTable = getComboBatchDate()
        Dim j As Integer

        For j = 0 To DtBatchDate.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdBatchDate.UniqueID, CStr(DtBatchDate.Rows(j).Item("FundingBatchNo")).Trim)
        Next
        RegisterFirstItem(drdBatchDate)

        MyBase.Render(writer)
    End Sub

    Private Sub RegisterFirstItem(ByVal ddl As DropDownList)
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "0")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "All")
    End Sub
End Class