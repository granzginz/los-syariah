﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchDateChangeList.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.BatchDateChangeList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchDateChangeList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }	
			-->
    </script>
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms[0].' + tampungGrandChild2).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenFundingCompanyView(Style, CompanyID) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyView.aspx?Style=' + Style + '&CompanyID=' + CompanyID, 'Channeling', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=1,scrollbars=1');
        }
        function OpenFundingCompanyContractView(pStyle, pCompanyName, pBankName, pFundingContractNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyContractView.aspx?Style=' + pStyle + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&FundingContractNo=' + pFundingContractNo, 'Channeling', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function OpenFundingBatchView(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingBatchView.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'Channeling', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }							
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempChild2" type="hidden" name="tempChild2" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    GANTI TANGGAL JATUH TEMPO BATCH
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:DropDownList ID="drdCompany" runat="server" onchange="<%#drdCompanyChange()%>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:DropDownList ID="drdContract" runat="server" onchange="<%#drdContractChange()%>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:DropDownList ID="drdBatchDate" runat="server" onchange="setCboBatchDate(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR BATCH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFundingCoyID" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" runat="server" Enabled="True" Text='<%# Container.dataitem("BankName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblContractName" runat="server" Enabled="True" Text='<%# Container.dataitem("ContractName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Enabled="True" Text='<%# Container.dataitem("BankID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBranch" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyName") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkContractNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingContractNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO BATCH">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBatchNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingBatchNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BATCHDATE" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL BATCH">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrincipalAmtToFunCoy" DataFormatString="{0:N0}" HeaderText="JUMLAH PENCAIRAN">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OSAmtToFunCoy" DataFormatString="{0:N0}" HeaderText="SISA PINJAMAN">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="GANTI TGL JT">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBatchDate" runat="server" CausesValidation="False" CommandName="Change"
                                        ImageUrl="../../Images/IconAmortize.gif" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" CommandName="First"
                        ImageUrl="../../Images/grid_navbutton01.png" OnCommand="NavigationLink_Click" />
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" CommandName="Prev"
                        ImageUrl="../../Images/grid_navbutton02.png" OnCommand="NavigationLink_Click" />
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" CommandName="Next"
                        ImageUrl="../../Images/grid_navbutton03.png" OnCommand="NavigationLink_Click" />
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" CommandName="Last"
                        ImageUrl="../../Images/grid_navbutton04.png" OnCommand="NavigationLink_Click" />
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="InpType" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                     
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah"
                        CssClass="validator_general"  MaximumValue="999999999"
                        MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                        Display="Dynamic" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
