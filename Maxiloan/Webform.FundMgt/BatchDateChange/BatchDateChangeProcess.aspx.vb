﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class BatchDateChangeProcess
    Inherits Maxiloan.Webform.WebBased
#Region "uccomponent"
    Protected WithEvents txtsDate As ucDateCE
#End Region
#Region "Property"
    Private ReadOnly Property FundingBatchNo() As String
        Get
            Return Request.QueryString("FundingBatchNo")
        End Get
    End Property

    Private ReadOnly Property CompanyID() As String
        Get
            Return Request.QueryString("CompanyID")
        End Get
    End Property
    Private ReadOnly Property FundingContractNo() As String
        Get
            Return Request.QueryString("FundingContractNo")
        End Get
    End Property
    Private Property TotAmountTobePaid() As Double
        Get
            Return (CType(ViewState("TotAmountTobePaid"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotAmountTobePaid") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(ViewState("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property InSeqNo() As String
        Get
            Return (CType(ViewState("InSeqNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InSeqNo") = Value
        End Set
    End Property
    Private Property FacilityKind() As String
        Get
            Return (CType(ViewState("FacilityKind"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityKind") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property
    Private Property TotInterest() As Double
        Get
            Return (CType(ViewState("TotInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInterest") = Value
        End Set
    End Property
    Private Property OSPrincipal() As Double
        Get
            Return (CType(ViewState("OSPrincipal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("OSPrincipal") = Value
        End Set
    End Property

    Private Property EffDate() As Date
        Get
            Return (CType(ViewState("EffDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("EffDate") = Value
        End Set
    End Property

    Private Property flagdel() As String
        Get
            Return (CType(ViewState("flagdel"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("flagdel") = Value
        End Set
    End Property

    Private Property VTempDate() As Date
        Get
            Return (CType(ViewState("VTempDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("VTempDate") = Value
        End Set
    End Property
#End Region
#Region "Declare Variable"
    Dim tempPrincipalAmount As Double
    Dim tempInterestAmount As Double
    Dim tempOSPrincipalAmount As Double
    Dim tempOSInterestAmount As Double
    Dim tempInstallmentAmount As Double
    Dim tempdate As Date
    Dim i As Integer
    Dim j As Integer = 0
    Dim TotInstallmentAmount As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipalAmount As Label
    Dim TotOSInterestAmount As Label
    Dim intcounter As Integer = 1
#End Region
#Region "Privateconst"
    Private oCustomClass As New Parameter.FundingContractBatch
    Private oController As New FundingCompanyController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "FUNDUEDATECHGPROC"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                lblFundingCompanyName.Text = Me.CompanyID
                lblFacilityNo.Text = Me.FundingContractNo
                lblBatchNo.Text = Me.FundingBatchNo
                pnlTop.Visible = True
                pnlList.Visible = False
                BindDataTop()
            End If
        End If
    End Sub
    Private Sub BindDataTop()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch

        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .SpName = "spFundingChangeDueDateViewDetail"
        End With
        oReceive = cReceive.GetGeneralEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("PrincipalAmtToFunCoy")) Then
                lblDrawdownAmount.Text = FormatNumber(oRow("PrincipalAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("BatchDate")) Then
                lblDrawdownPlanDate.Text = CType(oRow("BatchDate"), String)
            End If
            If Not IsDBNull(oRow("InsPeriod")) Then
                lblInterestType.Text = CType(oRow("InsPeriod"), String)
            End If
            If Not IsDBNull(oRow("InstallmentPeriod")) Then
                Me.PaymentFrequency = CType(oRow("InstallmentPeriod"), Integer)
            End If
            If Not IsDBNull(oRow("MaxDueDate")) Then
                lblNextInstallmentDueDate.Text = CType(oRow("MaxDueDate"), String)
                txtsDate.Text = CType(oRow("MaxDueDate"), String)
            End If
            If Not IsDBNull(oRow("FacilityKind")) Then
                Me.FacilityKind = CType(oRow("FacilityKind"), String)
            End If

        End If
    End Sub
#Region "Dobind"
    Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .SpName = "spFundingInstallmentDueDateGrid"
        End With
        oReceive = cReceive.GetGeneralEditView(oCustomClass)
        DtUserList = oReceive.ListData
        dtg.DataSource = DtUserList.DefaultView
        dtg.DataBind()
    End Sub
#End Region

#Region "Databound"
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lb As New Label
        Dim NextDueDate As Date
        NextDueDate = ConvertDate2(lblNextInstallmentDueDate.Text.Trim)
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)

            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblINTERESTAMOUNT"), Label)
            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)
            Me.TotInterest = tempInterestAmount

            lb = CType(e.Item.FindControl("lblOSPrincipal"), Label)
            tempOSPrincipalAmount = tempOSPrincipalAmount + CDbl(lb.Text)
            Me.OSPrincipal = tempOSPrincipalAmount

            lb = CType(e.Item.FindControl("lblOSInterest"), Label)
            tempOSInterestAmount = tempOSInterestAmount + CDbl(lb.Text)

            '==========================
            If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(e.Item.Cells(2).Text)) > 0 Then
                j = j + 1
                If j = 1 Then
                    tempdate = ConvertDate2(txtsDate.Text)
                    Me.DaysDiff = CInt(DateDiff(DateInterval.Day, ConvertDate2(txtsDate.Text), ConvertDate2(e.Item.Cells(2).Text)))
                    If Me.DaysDiff < 0 Then
                        Me.DaysDiff = 0
                    End If
                Else
                    tempdate = DateAdd(DateInterval.Month, CDbl(Me.PaymentFrequency), tempdate)
                End If
                e.Item.Cells(2).Text = tempdate.ToString("dd/MM/yyy")
                e.Item.Cells(2).Font.Bold = True
            Else
                e.Item.Cells(2).Text = ConvertDate2(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            End If

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2).ToString

            TotInterestAmount = CType(e.Item.FindControl("lblTotINTERESTAMOUNT"), Label)
            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2).ToString

            TotOSPrincipalAmount = CType(e.Item.FindControl("lblTotOSPrincipalAmount"), Label)
            TotOSPrincipalAmount.Text = FormatNumber(tempOSPrincipalAmount, 2).ToString

            TotOSInterestAmount = CType(e.Item.FindControl("lblTotOSInterestAmount"), Label)
            TotOSInterestAmount.Text = FormatNumber(tempOSInterestAmount, 2).ToString

        End If
    End Sub
#End Region

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If dtg.Items.Count > 0 Then
                Dim i As Integer
                Dim DtAdd As New DataTable
                Dim dr As DataRow
                Dim induedate As String
                Dim inseqno As String
                DtAdd.Columns.Add("InSeqNo", GetType(System.String))
                DtAdd.Columns.Add("DueDate", GetType(System.String))
                For i = 0 To dtg.Items.Count - 1
                    ' If CInt(dtg.Items(i).Cells(1).Text) >= CInt(Me.InSeqNo) Then
                    inseqno = dtg.Items(i).Cells(1).Text
                    induedate = ConvertDate2(dtg.Items(i).Cells(2).Text).ToString("yyyyMMdd")
                    dr = DtAdd.NewRow()
                    dr("InSeqNo") = dtg.Items(i).Cells(1).Text
                    dr("DueDate") = induedate
                    DtAdd.Rows.Add(dr)
                    'End If
                Next

                With oCustomClass
                    .strConnection = GetConnectionString()
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .ListData = DtAdd
                    .FacilityKind = Me.FacilityKind
                End With
                Try
                    oController.DueDateChangeSave(oCustomClass)
                    Response.Redirect("BatchDateChangeList.aspx")
                Catch exp As Exception

                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
        End If
    End Sub
    Private Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        If txtsDate.Text = lblNextInstallmentDueDate.Text.Trim Then
            ShowMessage(lblMessage, "Tanggal Jatuh tempo Angsuran baru Tidak boleh sama dengan Tgl Jatuhtempo berikutnya", True)
            Exit Sub
        End If
        pnlTop.Visible = True
        pnlList.Visible = True
        DoBind()
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("BatchDateChangeList.aspx")
    End Sub

End Class