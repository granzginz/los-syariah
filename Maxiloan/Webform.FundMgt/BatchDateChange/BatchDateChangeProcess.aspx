﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BatchDateChangeProcess.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.BatchDateChangeProcess" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchDateChangeProcess</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server" Visible="False">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    GANTI TANGGAL JATUH TEMPO BATCH
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblFundingCompanyName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="lblFacilityNo" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:Label ID="lblBatchNo" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Pencairan</label>
                <asp:Label ID="lblDrawdownAmount" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Rencana Pencairan</label>
                <asp:Label ID="lblDrawdownPlanDate" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Margin</label>
                <asp:Label ID="lblInterestType" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Angsuran JT Berikut</label>
                <asp:Label ID="lblNextInstallmentDueDate" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal JT Angsuran Baru</label>
                <%--<asp:TextBox runat="server" ID="txtsDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtsDate" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCalculate" runat="server" CausesValidation="false" Text="Calculate"
                CssClass="small button green"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" Visible="True" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    JADWAL ANGSURAN BARU
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# formatnumber(container.dataitem("InsSeqNO"),0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSeqNo"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DUEDATE" HeaderText="TGL JT">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH POKOK">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# formatnumber(container.dataitem("PrincipalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH MARGIN">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%# formatnumber(container.dataitem("INTERESTAMOUNT"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOSPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("OSPrincipalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOSInterest" runat="server" Text='<%# formatnumber(container.dataitem("OSInterestAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSInterestAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
