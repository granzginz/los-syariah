﻿
#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class FundingCetakPengantarCairViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property FundingContractNo() As String
        Get
            Return CType(ViewState("FundingContractNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingCoyID() As String
        Get
            Return CType(ViewState("FundingCoyID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CType(ViewState("FundingBatchNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property
    Private Property CompanyName() As String
        Get
            Return CType(ViewState("CompanyName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CompanyName") = Value
        End Set
    End Property
    Private Property BankName() As String
        Get
            Return CType(ViewState("BankName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property
    Private Property BankId() As String
        Get
            Return CType(ViewState("BankId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankId") = Value
        End Set
    End Property
    Private Property ContractName() As String
        Get
            Return CType(ViewState("ContractName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property
    Private Property FundingContractId() As String
        Get
            Return CType(ViewState("FundingContractId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractId") = Value
        End Set
    End Property
    Private Property CompanyID() As String
        Get
            Return CType(ViewState("CompanyID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CompanyID") = Value
        End Set
    End Property
    Private Property NoReff() As String
        Get
            Return CType(ViewState("NoReff"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoReff") = Value
        End Set
    End Property
    Private Property Up() As String
        Get
            Return CType(ViewState("Up"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Up") = Value
        End Set
    End Property
    Private Property Signer1() As String
        Get
            Return CType(ViewState("Signer1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Signer1") = Value
        End Set
    End Property
    Private Property Signer2() As String
        Get
            Return CType(ViewState("Signer2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Signer2") = Value
        End Set
    End Property
    Private Property Jabatan1() As String
        Get
            Return CType(ViewState("Jabatan1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Jabatan1") = Value
        End Set
    End Property
    Private Property Jabatan2() As String
        Get
            Return CType(ViewState("Jabatan2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Jabatan2") = Value
        End Set
    End Property
    Private Property JabatanUp() As String
        Get
            Return CType(ViewState("JabatanUp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("JabatanUp") = Value
        End Set
    End Property
    Private Property Perihal() As String
        Get
            Return CType(ViewState("Perihal"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Perihal") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oController As New FundingCompanyController
    Private oCustomClass As New Parameter.FundingCompany
    Private NamaFile As String = "SuratPengantarCair"
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        'Media Tester
        'Me.FundingCoyID = "BRI"

        'Remak Sementara Untuk Parameter
        If Request.QueryString("CompanyID") <> "" Then Me.FundingCoyID = Request.QueryString("CompanyID")
        If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
        If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
        If Request.QueryString("BankID") <> "" Then Me.BankId = Request.QueryString("BankID")
        If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
        If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo")
        If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
        If Request.QueryString("NoReff") <> "" Then Me.NoReff = Request.QueryString("NoReff")
        If Request.QueryString("Up") <> "" Then Me.Up = Request.QueryString("Up")
        If Request.QueryString("Signer1") <> "" Then Me.Signer1 = Request.QueryString("Signer1")
        If Request.QueryString("Signer2") <> "" Then Me.Signer2 = Request.QueryString("Signer2")
        If Request.QueryString("Jabatan1") <> "" Then Me.Jabatan1 = Request.QueryString("Jabatan1")
        If Request.QueryString("Jabatan2") <> "" Then Me.Jabatan2 = Request.QueryString("Jabatan2")
        If Request.QueryString("JabatanUp") <> "" Then Me.JabatanUp = Request.QueryString("JabatanUp")
        If Request.QueryString("Perihal") <> "" Then Me.Perihal = Request.QueryString("Perihal")

        'If Request.QueryString("FundingCoyID") <> "" Then Me.FundingCoyID = Request.QueryString("CompanyID")

        If Me.FundingCoyID = "BRI" Then
            BindReportBRI()
        ElseIf Me.FundingCoyID = "MNDR" Then

        ElseIf Me.FundingCoyID = "PERMATA" Then

        ElseIf Me.FundingCoyID = "BII" Then

        ElseIf Me.FundingCoyID = "CFIN" Then

        ElseIf Me.FundingCoyID = "GCFL" Then

        ElseIf Me.FundingCoyID = "NIAGA" Then

        ElseIf Me.FundingCoyID = "NISP" Then

        ElseIf Me.FundingCoyID = "KSWN" Then

        ElseIf Me.FundingCoyID = "MNC" Then
            BindReportMNC()
        ElseIf Me.FundingCoyID = "BUK" Then

        ElseIf Me.FundingCoyID = "BBP" Then

        ElseIf Me.FundingCoyID = "BEIN" Then

        ElseIf Me.FundingCoyID = "VICT" Then
            BindReportVICT()
        ElseIf Me.FundingCoyID = "BDKI" Then

        ElseIf Me.FundingCoyID = "MUTI" Then
            BindReportMUTI()
        ElseIf Me.FundingCoyID = "BUK" Then
            BindReportBUK()
        ElseIf Me.FundingCoyID = "SIMAS" Then
            BindReportSIMAS()
        End If



    End Sub
    Sub redirect(ByVal strLocation As String)
        'CompanyID=BRI&CompanyName=BANK BRI&BankName=PT. BANK RAKYAT INDONESIA TBK.&BankId=002&FundingContractId=BR105&ContractName=BANK BRI-BR105
        Response.Redirect("FundingContractBatch.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & Me.NamaFile & "&CompanyID=" & Me.FundingCoyID & "&CompanyName=" & _
                            Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankId & "&FundingContractId=" & _
                            Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub
#Region "GetCookies"
    Sub GetCookies()

        Dim cookie As HttpCookie = Request.Cookies("FundingCetakPengantarCair")
        Me.FundingCoyID = cookie.Values("FundingCoyID")
        Me.FundingContractNo = cookie.Values("FundingContractNo")
        Me.FundingBatchNo = cookie.Values("FundingBatchNo")
        Me.NoReff = cookie.Values("NoReff")
        Me.Up = cookie.Values("Up")
        Me.Signer1 = cookie.Values("Signer1")
        Me.Signer2 = cookie.Values("Signer2")
        Me.Jabatan1 = cookie.Values("Jabatan1")
        Me.Jabatan2 = cookie.Values("Jabatan2")
        Me.JabatanUp = cookie.Values("JabatanUp")
        Me.Perihal = cookie.Values("Perihal")
    End Sub
#End Region
#Region "BindReport"
   
    Sub BindReportBRI()
        GetCookies()
        'Tester
        'Me.FundingCoyID = "BRI"
        'Me.FundingContractNo = "BR115"
        'Me.FundingBatchNo = "BATCH 102"
        'Me.CompanyName = "BANK BRI"
        'Me.BankName = "PT. BANK RAKYAT INDONESIA TBK."
        'Me.BankId = "002"
        'Me.ContractName = "BANK BRI-BR105"
        'Me.CompanyID = "BRI"

        Dim objReport As PencairanBankBRI = New PencairanBankBRI
        Dim oData As New DataSet

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.FundingCoyID = Me.FundingCoyID
        oCustomClass.FundingContractNo = Me.FundingContractNo
        oCustomClass.FundingBatchNo = Me.FundingBatchNo
        oData = oController.ListSPCReport(oCustomClass)
        objReport.SetDataSource(oData)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "A4" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("NoReff")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.NoReff
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Up")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Up
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("JabatanUp")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.JabatanUp
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.NamaFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        redirect(strFileLocation)

    End Sub
    Sub BindReportVICT()
        GetCookies()
        Dim objReport As PencairanBankVictoria = New PencairanBankVictoria
        Dim oData As New DataSet

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.FundingCoyID = Me.FundingCoyID
        oCustomClass.FundingContractNo = Me.FundingContractNo
        oCustomClass.FundingBatchNo = Me.FundingBatchNo
        oData = oController.ListSPCReport(oCustomClass)
        objReport.SetDataSource(oData)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "A4" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("NoReff")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.NoReff
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Up")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Up
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat


        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.NamaFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        redirect(strFileLocation)

    End Sub
    Sub BindReportMNC()
        GetCookies()
        Dim objReport As PencairanBankMNC = New PencairanBankMNC
        Dim oData As New DataSet

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.FundingCoyID = Me.FundingCoyID
        oCustomClass.FundingContractNo = Me.FundingContractNo
        oCustomClass.FundingBatchNo = Me.FundingBatchNo
        oData = oController.ListSPCReport(oCustomClass)
        objReport.SetDataSource(oData)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "A4" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("NoReff")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.NoReff
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Up")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Up
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.NamaFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        redirect(strFileLocation)

    End Sub
    Sub BindReportBUK()
        GetCookies()
        Dim objReport As PencairanBankBukopin = New PencairanBankBukopin
        Dim oData As New DataSet

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.FundingCoyID = Me.FundingCoyID
        oCustomClass.FundingContractNo = Me.FundingContractNo
        oCustomClass.FundingBatchNo = Me.FundingBatchNo
        oData = oController.ListSPCReport(oCustomClass)
        objReport.SetDataSource(oData)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "A4" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("NoReff")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.NoReff
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Up")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Up
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.NamaFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        redirect(strFileLocation)

    End Sub
    Sub BindReportMUTI()
        GetCookies()
        Dim objReport As PencairanBankMutiara = New PencairanBankMutiara
        Dim oData As New DataSet

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.FundingCoyID = Me.FundingCoyID
        oCustomClass.FundingContractNo = Me.FundingContractNo
        oCustomClass.FundingBatchNo = Me.FundingBatchNo
        oData = oController.ListSPCReport(oCustomClass)
        objReport.SetDataSource(oData)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "A4" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("NoReff")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.NoReff
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Up")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Up
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.NamaFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        redirect(strFileLocation)

    End Sub

    Sub BindReportSIMAS()
        GetCookies()
        Dim objReport As PencairanBankSinarMas = New PencairanBankSinarMas
        Dim oData As New DataSet

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.FundingCoyID = Me.FundingCoyID
        oCustomClass.FundingContractNo = Me.FundingContractNo
        oCustomClass.FundingBatchNo = Me.FundingBatchNo
        oData = oController.ListSPCReport(oCustomClass)
        objReport.SetDataSource(oData)

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "A4" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                objReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("NoReff")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.NoReff
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Up")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Up
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Signer2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Signer2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan1")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan1
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Jabatan2")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Jabatan2
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("JabatanUp")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.JabatanUp
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objReport.DataDefinition.ParameterFields("Perihal")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Perihal
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        'Export ke PDF
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat



        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.NamaFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        redirect(strFileLocation)

    End Sub
#End Region

    Private Function getMonthID() As String
        Dim rtn As String = ""

        If Month(Me.BusinessDate) = "1" Then
            rtn = "Januari"
        End If
        If Month(Me.BusinessDate) = "2" Then
            rtn = "Febuari"
        End If
        If Month(Me.BusinessDate) = "3" Then
            rtn = "Maret"
        End If
        If Month(Me.BusinessDate) = "4" Then
            rtn = "April"
        End If
        If Month(Me.BusinessDate) = "5" Then
            rtn = "Mai"
        End If
        If Month(Me.BusinessDate) = "6" Then
            rtn = "Juni"
        End If
        If Month(Me.BusinessDate) = "7" Then
            rtn = "Juli"
        End If
        If Month(Me.BusinessDate) = "8" Then
            rtn = "Augustus"
        End If
        If Month(Me.BusinessDate) = "9" Then
            rtn = "September"
        End If
        If Month(Me.BusinessDate) = "10" Then
            rtn = "Oktober"
        End If
        If Month(Me.BusinessDate) = "11" Then
            rtn = "November"
        End If
        If Month(Me.BusinessDate) = "12" Then
            rtn = "Desember"
        End If

        Return rtn

    End Function
    'Private Sub AddParamField(ByVal objReport As PencairanBankBRI, ByVal fieldName As String, ByVal value As Object)

    '    Dim discrete As ParameterDiscreteValue
    '    Dim ParamField As ParameterFieldDefinition
    '    Dim CurrentValue As ParameterValues

    '    ParamField = objReport.DataDefinition.ParameterFields(fieldName)
    '    discrete = New ParameterDiscreteValue
    '    discrete.Value = value
    '    CurrentValue = New ParameterValues
    '    CurrentValue = ParamField.DefaultValues
    '    CurrentValue.Add(discrete)
    '    ParamField.ApplyCurrentValues(CurrentValue)
    'End Sub

End Class