﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingCompanyMaintenance.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingCompanyMaintenance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAPBankPaymentAllocation" Src="../../Webform.UserController/ucAPBankPaymentAllocation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucSatu" Src="../../webform.UserController/ucAPBankPaymentAllocation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Funding Company Maintenance</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }		
		
    </script>
    <script language="javascript" type="text/javascript">
        function OpenFundingCompanyView(Style, CompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.FundMgt/View/FundingCompanyView.aspx?Style=' + Style + '&CompanyID=' + CompanyID, 'Channeling', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=1,scrollbars=1');
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    FUNDING BANK
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                    <asp:ListItem Value="FundingCoyId" Selected="True">ID Funding Bank</asp:ListItem>
                    <asp:ListItem Value="BankName">Nama Bank</asp:ListItem>
                    <asp:ListItem Value="FundingCoyName">Nama Funding Bank</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearchAtas" runat="server" CausesValidation="False" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnResetAtas" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR FUNDING BANK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingCoList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="FundingCoyId"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="FUNDINGCOYID" HeaderText="ID FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCompanyID" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyID") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BANKNAME" SortExpression="BANKNAME" HeaderText="NAMA FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FUNDINGCOYNAME" SortExpression="FUNDINGCOYNAME" HeaderText="NAMA CABANG">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="FASILITAS">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../Images/iconcontract.gif"
                                        CommandName="Contract" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BankId" Visible="False">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <div style="display:none">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <div class="form_title">
            <div class="form_single">
                <h3>
                    FUNDING BANK -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID Funding Bank</label>
                <asp:TextBox ID="txtID" runat="server" Width="120px" MaxLength="20"></asp:TextBox>
                <asp:Label ID="lblID" runat="server" Width="129px" Visible="False"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Harap Pilih ID Funding Bank" ControlToValidate="txtID" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Funding Bank</label>
                <asp:DropDownList ID="drdBankId" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rvBankID" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Salah Satu"
                    ControlToValidate="drdBankId" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Funding Cabang Bank</label>
                <asp:TextBox ID="txtName" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" Display="Dynamic"
                    ErrorMessage="Harap isi Nama" ControlToValidate="txtName" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyadress id="UcCompanyAddress" runat="server"></uc1:uccompanyadress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rekening Bank</label>
                <asp:TextBox ID="txtTFSBankAccount" runat="server" Width="291px" MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvTFSBankaccount" runat="server" Display="Dynamic"
                    ErrorMessage="Harap Isi No Rekening Bank" ControlToValidate="txtTFSBankAccount"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Rekening Bank</label>
                <td class="tdganjil" style="height: 20px">
                    <asp:TextBox ID="txtTFSBankAccountName" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rvTFSBankaccountName" runat="server" Display="Dynamic"
                        ErrorMessage="Harap isi Nama Rekening Bank" ControlToValidate="txtTFSBankAccountName"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Margin Harian</label>
                <%--<asp:TextBox ID="txtDailyInterest" runat="server" Width="48px" MaxLength="4">360</asp:TextBox>
                <asp:RangeValidator ID="rvDailyInt" runat="server" Display="Dynamic" ErrorMessage="Harap isi 360 atau 365"
                    ControlToValidate="txtDailyInterest" MinimumValue="360" MaximumValue="365"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtDailyInterest" runat="server" />
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccontactperson id="UcContactPerson" runat="server"></uc1:uccontactperson>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    COA Funding (A/P)</label>
                <uc1:ucapbankpaymentallocation id="UcAPBankPaymentAllocation1" runat="server"></uc1:ucapbankpaymentallocation>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    COA Interest Expenses</label>
                <%--<asp:TextBox ID="txtCOAIntExpense" runat="server" MaxLength="9"></asp:TextBox>--%>
                <asp:TextBox runat="server" ID="txtCOAIntExpense" MaxLength="9"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi COA Interest Expense"
                    ControlToValidate="txtCOAIntExpense" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    COA Prepaid</label>
                <%--<asp:TextBox ID="txtCOAPrepaid" runat="server" MaxLength="9"></asp:TextBox>--%>
                <asp:TextBox runat="server" ID="txtCOAPrep" MaxLength="9"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Harap isi COA Prepaid"
                    ControlToValidate="txtCOAPrep" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW - FUNDING BANK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Funding Bank</label>
                <asp:Label ID="lblFundingCoyID" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Funding bank</label>
                <asp:Label ID="lblBankName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat</label>
                <asp:Label ID="lblAddress" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    RT/RW</label>
                <asp:Label ID="lblRT" runat="server" Width="32px"></asp:Label>/
                <asp:Label ID="lblRW" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kota</label>
                <asp:Label ID="lblCity" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kode Pos</label>
                <asp:Label ID="lblZipCode" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Telepon-1</label>
                <asp:Label ID="lblAreaPhone1" runat="server" Width="32px"></asp:Label>-
                <asp:Label ID="lblPhone1" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Telepon-2</label>
                <asp:Label ID="lblAreaPhone2" runat="server" Width="32px"></asp:Label>-
                <asp:Label ID="lblPhone2" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fax</label>
                <asp:Label ID="lblAreaFax" runat="server" Width="16px"></asp:Label>-
                <asp:Label ID="lblFax" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>KONTAK</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama</label>
                <asp:Label ID="lblCPName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:Label ID="lblCPTitle" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    e-Mail</label>
                <asp:Label ID="lblCPEmail" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No HandPhone</label>
                <asp:Label ID="lblCPMobilePhone" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnExit" runat="server" CausesValidation="False" Text="Exit" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
