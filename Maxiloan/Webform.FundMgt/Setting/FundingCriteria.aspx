﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingCriteria.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingCriteria" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TEMPLATE FUNDING KRITERIA</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnCriteriaID" type="hidden" name="hdnCriteriaID" runat="server" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    TEMPLATE FUNDING KRITERIA
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="CriteriaID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI KRITERIA">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbKriteria" runat="server" CausesValidation="False" ImageUrl="../../Images/ftv2doc.gif"
                                        CommandName="Kriteria"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CriteriaID" SortExpression="CriteriaID" HeaderText="ID">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaDescription" SortExpression="CriteriaDescription"
                                HeaderText="NAMA KRITERIA"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="JENIS PILIHAN" SortExpression="CriteriaOption">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <input id="hdnCriteriaOption" type="hidden" name="hdnCriteriaOption" runat="server"
                                        value='<%# DataBinder.eval(Container,"DataItem.CriteriaOption") %>' />
                                    <asp:Label ID="CriteriaOption" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtGoPage"
                        Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAdd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False">
            </asp:Button>
            <asp:Button ID="BtnPrint" runat="server" Visible="false" Enabled="true" Text="Print"
                CssClass="small button blue"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI FUNDING KRITERIA</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" AutoPostBack="true" runat="server">
                    <asp:ListItem Value="CriteriaID">ID</asp:ListItem>
                    <asp:ListItem Value="CriteriaDescription">Nama Kriteria</asp:ListItem>
                    <asp:ListItem Value="CriteriaOption">Jenis Pilihan</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <asp:TextBox ID="txtSearch" runat="server" Width="50%"  MaxLength="100"></asp:TextBox>
                <asp:DropDownList ID="cboSJenisPilihan" runat="server">
                    <asp:ListItem Value="SEL">SELECT</asp:ListItem>
                    <asp:ListItem Value="MUL">MULTIPLE SELECT</asp:ListItem>
                    <asp:ListItem Value="MAX">MAX</asp:ListItem>
                    <asp:ListItem Value="MIN">MIN</asp:ListItem>
                    <asp:ListItem Value="RAN">RANGE</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TEMPLATE FUNDING KRITERIA -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    ID Kriteria 
                </label>
                <asp:Label ID="lblKriteria" runat="server"></asp:Label>
                <asp:TextBox ID="txtKriteria" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtKriteria" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Nama Kriteria 
                </label>
                <asp:Label ID="lblNamaKriteria" runat="server"></asp:Label>
                <asp:TextBox ID="txtNamaKriteria" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="txtNamaKriteria" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Jenis Pilihan 
                </label>
                <asp:Label ID="lblJenisPilihan" runat="server"></asp:Label>
                <asp:DropDownList ID="cboJenisPilihan" runat="server">
                    <asp:ListItem Value="SEL">SELECT</asp:ListItem>
                    <asp:ListItem Value="MUL">MULTIPLE SELECT</asp:ListItem>
                    <asp:ListItem Value="MAX">MAX</asp:ListItem>
                    <asp:ListItem Value="MIN">MIN</asp:ListItem>
                    <asp:ListItem Value="RAN">RANGE</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                    ControlToValidate="cboJenisPilihan" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
