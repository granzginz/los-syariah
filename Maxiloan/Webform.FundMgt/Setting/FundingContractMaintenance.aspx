﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingContractMaintenance.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingContractMaintenance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAPBankPaymentAllocation" Src="../../Webform.UserController/ucAPBankPaymentAllocation.ascx" %>
<%@ Register Assembly="CKEditor.NET, Version=3.6.6.2, Culture=neutral, PublicKeyToken=e379cdf2f8354999" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingContractMaintenance</title>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function OpenFundingCompanyContractView(pStyle, pCompanyName, pBankName, pFundingContractNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyContractView.aspx?Style=' + pStyle + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&FundingContractNo=' + pFundingContractNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        //        function toggle(obj) {
        //            alert(obj);
        //            
        //            if (obj == 1) 
        //            {
        //                var el = document.getElementById("divAddEdit");

        //            }

        //            else if (obj == 2) 
        //            {
        //                var el = document.getElementById("pnlKriteria");
        //            }
        //                      
        //            el.style.display = (el.style.display != 'none' ? 'none' : '');             
        //        }
    </script>
    <script src="~/ckeditor/ckeditor.js"></script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>     
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    FASILITAS
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankName" runat="server" Visible="False">lblBankName</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server" Visible="False">lblFundingCoyName</asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR FASILITAS
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>                       
                           <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="FUNDINGCONTRACTNO" HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkContractNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingContractNo") %>'>&nbsp;
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn  DataField="ContractName" HeaderText="NAMA KONTRAK"></asp:BoundColumn>     
                            <asp:BoundColumn DataField="PERIODFROM" SortExpression="PERIODFROM" HeaderText="PERIODE AWAL"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PERIODTO" SortExpression="PERIODTO" HeaderText="PERIODE AKHIR"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="FundingCoyId"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="WAKTU PILIH" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Enabled="True" Text='<%# Container.dataitem("FLAGCS") %>'>&nbsp;
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="FacilityKind" HeaderText="JENIS"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PLAFON &lt;br&gt;BRANCH" Visible="false">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgPlafon" runat="server" ImageUrl="../../Images/iconPlafonBranch.gif"
                                        CommandName="Plafond" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DOC" Visible="false">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDoc" runat="server" ImageUrl="../../Images/iconDocument.gif"
                                        CommandName="Doc" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BATCH">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBatch" runat="server" ImageUrl="../../Images/iconDrawDown.gif"
                                        CommandName="Batch" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RATE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEditRate" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Rate" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBacknew" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    FASILITAS -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank
                </label>
                <asp:Label ID="lblFundingCompany" runat="server">lblFundingCompany</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang
                </label>
                <asp:Label ID="lblBranch" runat="server">lblBranch</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    No Fasilitas
                </label>
                <asp:TextBox ID="txtFContractNo" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                <asp:Label ID="lblFContractNo" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Harap isi No Fasilitas" ControlToValidate="txtFContractNo" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Fasilitas
                </label>
                <asp:TextBox ID="txtContractName" runat="server" CssClass="medium_text" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Tanggal Fasilitas
                </label>
                <uc1:ucdatece id="txtContractDate" runat="server"></uc1:ucdatece>
            </div>  
            <div class="form_right">
                <label>
                    Tgl Jatuh Tempo Final
                </label>
                <%--<asp:TextBox runat="server" ID="txtFinalMaturityDate" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender4" TargetControlID="txtFinalMaturityDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtFinalMaturityDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Periode Pencarian (Bulan)</label>
                    <asp:textbox runat="server" ID="txtPeriodeBulan" CssClass="small_text"  
                    onblur="extractNumber(this,2,true);blockInvalid(this);this.value=blankToZero(this.value);"
                    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                    onfocus="this.value=resetNumber(this.value);" AutoPostBack="true"></asp:textbox>
                </div>
                <div class="form_right">
                    <label>
                        Prosentase Pokok Hutang
                    </label>
                    <%--<asp:TextBox ID="txtPersenPokokHutang" runat="server" MaxLength="18" CssClass="small_text">0</asp:TextBox>%
                    <asp:RangeValidator ID="RangeValidator11" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                        ControlToValidate="txtPersenPokokHutang" Type="Double" MinimumValue="0" MaximumValue="999"></asp:RangeValidator>--%>
                    <uc1:ucnumberformat id="txtPersenPokokHutang" runat="server" />
                    %
                </div>
                <%--Security di set pada batch sesuai dengan pilihan  pokok hutang berdasarkan apa--%>
                <div class="form_right" style="display: none;">
                    <label>
                        Prosentase Security Berdasarkan
                    </label>
                    <asp:DropDownList ID="drdSecurityPercentageOf" runat="server">
                        <asp:ListItem Value="P" Selected="True">Base on Principal</asp:ListItem>
                        <asp:ListItem Value="A">Base on A/R</asp:ListItem>
                        <asp:ListItem Value="O">Base on OTR Price</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Periode Pencairan
                </label>
                <%--<asp:TextBox runat="server" ID="txtPeriodFrom" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtPeriodFrom"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtPeriodFrom" runat="server"></uc1:ucdatece>
                <label class="label_auto">
                    s/d
                </label>
                <%--<asp:TextBox runat="server" ID="txtPeriodTo" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtPeriodTo"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator runat="server" ID="rfvPeriodFrom" ControlToValidate="txtPeriodFrom"
                    Display="Dynamic" CssClass="validator_general" ErrorMessage="Harap isi tanggal mulai kontrak!"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPeriodTo"
                    Display="Dynamic" CssClass="validator_general" ErrorMessage="Harap isi tanggal berakhir kontrak!"></asp:RequiredFieldValidator>--%>
                <uc1:ucdatece id="txtPeriodTo" runat="server"></uc1:ucdatece>
            </div>
            <div class="form_right">
                <label>
                    Prosentase Security
                </label>
                <%--<asp:TextBox ID="txtSecurityPercentage" runat="server" Width="80px" MaxLength="10">100</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="Harap isi antara 0 s/d 100"
                    ControlToValidate="txtSecurityPercentage" Type="Double" MinimumValue="0" MaximumValue="1000"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtSecurityPercentage" runat="server" />
                %
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Fasilitas
                    </label>
                    <asp:DropDownList ID="drdFacilityKind" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drdFacilityKind_IndexChanged">
                        <asp:ListItem Value="JFINC">Joint Financing</asp:ListItem>
                        <%--<asp:ListItem Value="KUK">KUK</asp:ListItem>--%>
                        <%--<asp:ListItem Value="CHANN">Channeling</asp:ListItem>--%>
                        <%--<asp:ListItem Value="ASBUY">Asset Buy</asp:ListItem>--%>
                        <%--<asp:ListItem Value="UKM">UKM</asp:ListItem>--%>
                        <%--<asp:ListItem Value="KMK">KMK</asp:ListItem>--%>
                        <asp:ListItem Value="TLOAN">Term Loan</asp:ListItem>
                        <%--<asp:ListItem Value="BONDS">Obligasi</asp:ListItem>--%>
                        <%--<asp:ListItem Value="BTURN">Bunga Menurun</asp:ListItem>--%>
                    </asp:DropDownList>
                    <label>
                        Jenis Facility
                    </label>                    
                </div>
                <div class="form_right">
                    <label class="label_general">
                        Jenis Security
                    </label>
                    <div class="checkbox_group">
                        <asp:CheckBox ID="chkCorporate" runat="server" Text="Corporate Guarantee" CssClass="checkbox_group_item">
                        </asp:CheckBox>
                        <asp:CheckBox ID="chkParent" runat="server" Text="Parent Guarantee" CssClass="checkbox_group_item">
                        </asp:CheckBox>
                        <asp:CheckBox ID="chkCash" runat="server" Text="Cash Deposit" CssClass="checkbox_group_item">
                        </asp:CheckBox>
                        <asp:CheckBox ID="chkFixed" runat="server" Text="Fixed Asset" CssClass="checkbox_group_item">
                        </asp:CheckBox>
                        <asp:CheckBox ID="chkAR" runat="server" Text="Account Receivebility" CssClass="checkbox_group_item">
                        </asp:CheckBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status Balance Sheet
                </label>
                <asp:DropDownList ID="drdBalanceSheetStatus" runat="server">
                    <asp:ListItem Value="N" Selected="True">On Balance Sheet</asp:ListItem>
                    <asp:ListItem Value="F">Off Balance Sheet</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Waktu Pemilihan Fasilitas
                </label>
                <asp:DropDownList ID="drdSelectionTime" runat="server">
                    <asp:ListItem Value="B" Selected="True">Sebelum Loan ACT</asp:ListItem>
                    <asp:ListItem Value="A">Setelah Loan ACT</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        </div>        
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Jumlah Plafond
                </label>
                <uc1:ucnumberformat id="txtPlafondAmmount" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Porsi Funding Bank
                </label>
                <uc1:ucnumberformat id="txtFundingCoyPortion" runat="server" />
                <label class="label_auto">
                    %</label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <%--<label class="label_req">--%>
                <label>
                    Minimum Pencairan Per Batch
                </label>
                <uc1:ucnumberformat id="txtMaxDD" runat="server" />
            </div>
            <div class="form_right">
                <%--<label>
                    Provisi
                </label>--%>
                <%--<asp:TextBox ID="txtProvision" runat="server" Width="80px" MaxLength="10">0</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator2" runat="server" Display="Dynamic" ErrorMessage="Harap isi antara 0 s/d 100"
                    ControlToValidate="txtProvision" Type="Double" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>--%>
               <%-- <uc1:ucnumberformat id="txtProvision" runat="server" />
                %--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <%--<label class="label_req">--%>
                <label>
                    Minimum Pencairan
                </label>
                <uc1:ucnumberformat id="txtMinimumPencairan" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Provisi
                </label>
                <%--<asp:TextBox ID="txtProvision" runat="server" Width="80px" MaxLength="10">0</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator2" runat="server" Display="Dynamic" ErrorMessage="Harap isi antara 0 s/d 100"
                    ControlToValidate="txtProvision" Type="Double" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtProvision" runat="server" />
                %
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Mata Uang
                </label>
                <asp:DropDownList ID="drdCurrency" runat="server">
                    <asp:ListItem Value="IDR" Selected="True">Rupiah</asp:ListItem>
                    <asp:ListItem Value="USD">US Dollar</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                   <%-- Admin Fee Per BPKB--%>
                     Admin Fee Per Jaminan
                </label>
                <%--<asp:TextBox ID="txtAdminFeePerAct" runat="server" MaxLength="15">0</asp:TextBox>--%>
                <uc1:ucnumberformat id="txtAdminFeePerAct" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Type Fasilitas
                </label>
                <asp:DropDownList ID="drdFacilityType" runat="server">
                    <asp:ListItem Value="R" Selected="True">Revolving</asp:ListItem>
                    <asp:ListItem Value="N">Non Revolving</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Cara Pembayaran</label>
                <asp:DropDownList runat="server" ID="ddlCaraPembayaran">
                    <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="BATCH">Batch</asp:ListItem>
                    <asp:ListItem Value="JT">Jatuh Tempo</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                    InitialValue="" ErrorMessage="*" ControlToValidate="ddlCaraPembayaran" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Skema Pembayaran
                </label>
                <asp:DropDownList ID="drdPaymentScheme" runat="server" AutoPostBack="true" Width="370PX">
                    <asp:ListItem Value="1" Selected="True">Principal &amp; Interest on Drawdown Date</asp:ListItem>
                    <asp:ListItem Value="2">Principal on Drawdown Date Interest on Specific Date</asp:ListItem>
                    <asp:ListItem Value="3">Interest on Drawdown Date Principal on Specific Date</asp:ListItem>
                    <asp:ListItem Value="4">Principal &amp; Interest on EoM</asp:ListItem>
                    <asp:ListItem Value="7">Mirroring</asp:ListItem>
                    <asp:ListItem Value="5">Principal Drawdown By Range</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_box_hide">
                <%--<label class="label_req">
                    Batas Angsuran ke
                </label>
                <uc1:ucnumberformat id="txtAdminFeePerAct" runat="server" />--%>
            </div>
        </div>                
        <div class="form_box_hide">
            <div id="divSpecificDate" runat="server" class="form_left">
                <label class="label_req">
                    Tanggal Pembayaran
                </label>
                <%--<asp:TextBox ID="txtSpecificDate" runat="server" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender ID="txtSpecificDate_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtSpecificDate">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="rfvSpecificDate" runat="server" ControlToValidate="txtSpecificDate"
                    CssClass="validator_general" Display="Dynamic" Enabled="false" ErrorMessage="Isi tanggal jatuh tempo setiap bulannya!"
                    InitialValue=""></asp:RequiredFieldValidator>--%>
                <uc1:ucdatece id="txtSpecificDate" runat="server"></uc1:ucdatece>
            </div>
            
        </div>
        <div class="form_box">
            <div class="form_left">
                
            </div>
            <div class="form_right">
                <label>
                    Commintment Fee
                </label>
                <%--<asp:TextBox ID="txtCommitmentFee" runat="server" Width="80px" MaxLength="10">0</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator3" runat="server" Display="Dynamic" ErrorMessage="Harap diisi antara 0 s/d 100"
                    ControlToValidate="txtCommitmentFee" Type="Double" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtCommitmentFee" runat="server" />
                %
            </div>
        </div>        
        <div class="form_box">
            <div class="form_left">
                <label>
                    Suku Margin
                </label>
                <%--<asp:TextBox ID="txtInterestRate" runat="server" Width="80px" MaxLength="10">0</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator6" runat="server" Display="Dynamic" ErrorMessage="Harap diisi antara 0 s/d 100"
                    ControlToValidate="txtInterestRate" Type="Double" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtInterestRate" runat="server" />
                %
            </div>
            <div class="form_right">
                <label>
                    Biaya Per Pencairan
                </label>
                <%--<asp:TextBox ID="txtFeePerDrawDown" runat="server" MaxLength="15">0</asp:TextBox>--%>
                <uc1:ucnumberformat id="txtFeePerDrawDown" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Margin
                </label>
                <asp:DropDownList ID="drdInterestType" runat="server">
                    <asp:ListItem Value="L" Selected="True">Floating</asp:ListItem>
                    <asp:ListItem Value="X">Fixed</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Biaya Per Fasilitas
                </label>
                <%--<asp:TextBox ID="txtFeePerFacility" runat="server" MaxLength="15">0</asp:TextBox>--%>
                <uc1:ucnumberformat id="txtFeePerFacility" runat="server" />
            </div>
        </div>       
        <div class="form_box">
            <div class="form_box_hide">
                <label>
                    Tanggal Evaluasi
                </label>
                <%--<asp:TextBox runat="server" ID="txtEvaluationDate" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender5" TargetControlID="txtEvaluationDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtEvaluationDate" runat="server"></uc1:ucdatece>
            </div>
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan
                </label>
                <%--<asp:TextBox ID="txtLatePaymentFee" runat="server" Width="80px" MaxLength="10">0</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator5" runat="server" Display="Dynamic" ErrorMessage="Harap diisi antara 0 s/d 100"
                    ControlToValidate="txtLatePaymentFee" Type="Double" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtLatePaymentFee" runat="server" />
                %
            </div>
        </div>                
        <div class="form_box">
            <div class="form_left">
                <label>
                    Floating Mulai
                </label>
                <%--<asp:TextBox runat="server" ID="txtFloatingStart"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender6" TargetControlID="txtFloatingStart"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtFloatingStart" runat="server"></uc1:ucdatece>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan Margin
                </label>
                <asp:TextBox ID="txtInterestNote" runat="server" Width="220px" MaxLength="50"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Jenis Prepayment
                </label>
                <asp:DropDownList ID="drdPrepaymentType" runat="server">
                    <asp:ListItem Value="C" Selected="True">Harus DiBayar</asp:ListItem>
                    <%--<asp:ListItem Value="B">Harus Ganti BPKB</asp:ListItem>--%>
                    <asp:ListItem Value="B">Harus Ganti Jaminan</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Commitment Status
                </label>
                <asp:DropDownList ID="drdCommitmentStatus" runat="server">
                    <asp:ListItem Value="True">Yes</asp:ListItem>
                    <asp:ListItem Value="False">No</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    <%--Lokasi BPKB--%>
                    Lokasi Jaminan
                </label>
                <asp:DropDownList ID="drdBPKBLocation" runat="server">
                    <asp:ListItem Value="H" Selected="True">Kantor Pusat MF</asp:ListItem>
                    <asp:ListItem Value="B">Kantor Cabang MF</asp:ListItem>
                    <asp:ListItem Value="P">Kantor Pusat Funding Bank</asp:ListItem>
                    <asp:ListItem Value="C">kantor Cabang Funding Bank</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Penjaminan (Recourse)
                </label>
                <asp:DropDownList ID="drdRecoursetype" runat="server">
                    <asp:ListItem Selected="True" Value="W">WithOut Recourse</asp:ListItem>
                    <asp:ListItem Value="F">Full Recourse</asp:ListItem>
                    <asp:ListItem Value="L">Limited Recourse</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Late Charges Grace Period
                </label>
                <%--<asp:TextBox ID="txtLCGracePeriod" runat="server" MaxLength="4" Width="50px">0</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLCGracePeriod"
                    CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi Periode"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="txtLCGracePeriod"
                    CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                    MaximumValue="100" MinimumValue="0"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtLCGracePeriod" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Notaris
                </label>
                <asp:TextBox ID="txtNotaryName" runat="server" CssClass="medium_text" MaxLength="20"></asp:TextBox>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Jumlah Hari dlm Bulan
                </label>
                <asp:RadioButtonList ID="rblHariBulan" runat="server" CssClass="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Text="Tetap" Value="F"></asp:ListItem>
                    <asp:ListItem Text="Aktual" Value="A"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
               <%-- <label class="label_req">--%>
                 <label>
                    COA Funding (A/P)</label>
                <uc1:ucapbankpaymentallocation id="UcAPBankPaymentAllocation1" runat="server"></uc1:ucapbankpaymentallocation>
            </div>
            <div class="form_right">
                <label>
                    Denda Prepayment
                </label>
                <%--<asp:TextBox ID="txtSecurityPrepaymentPenalty" runat="server" Width="80px" MaxLength="10">0</asp:TextBox>%
                <asp:RangeValidator ID="RangeValidator4" runat="server" Display="Dynamic" ErrorMessage="Harap diisi antara 0 s/d 100"
                    ControlToValidate="txtSecurityPrepaymentPenalty" Type="Double" MinimumValue="0"
                    MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtSecurityPrepaymentPenalty" runat="server" />
                %
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    COA Interest Expenses</label>                
                    <asp:TextBox ID="txtCOAIntExpense" runat="server" MaxLength="9"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi COA Interest Expense"
                        ControlToValidate="txtCOAIntExpense" CssClass="validator_general"></asp:RequiredFieldValidator>                        
            </div>
            <div class="form_right">
                <label>Pinalty Prepayment</label>
                    <uc1:ucnumberformat id="txtPinalty" runat="server" />%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    COA Beban Margin</label>                
                    <asp:DropDownList ID="drdBankBunga" runat="server">
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator  ID="rvBankID" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Salah Satu"
                        ControlToValidate="drdBankBunga" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>                    --%>
            </div>
            <div class="form_right">
                <label>
                    COA Beban Administrasi</label>                
                    <asp:DropDownList ID="drdBankAdministrasi" runat="server">
                    </asp:DropDownList>
                  <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Salah Satu"
                        ControlToValidate="drdBankAdministrasi" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
           <label class ="label_req">
                    Request Criteria</label>
                <asp:Label ID="lblReq" runat="server"></asp:Label>
               <%--<asp:TextBox ID="txtReq" runat="server" TextMode="MultiLine" 
                    CssClass="multiline_textbox" Height="57px" Width="736px"></asp:TextBox>--%>
                   
                    <CKEditor:CKEditorControl ID="CKEditor1" BasePath="~/ckeditor" runat="server" >
                    </CKEditor:CKEditorControl>
                   
                  
                <script>
                    CKEDITOR.replace('CKEditor1');
                    // resize the editor after it has been fully initialized
                    CKEDITOR.on('instanceLoaded', function (e) { e.editor.resize(700, 350) });
            </script>
            </div>
        </div>
        
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    COA Prepaid</label>
                <asp:TextBox ID="txtCOAPrep" runat="server" MaxLength="9"></asp:TextBox>            
            </div>
        </div>
        <div style="display:none"> <!-- Ganti Metode (menggunakan SQL Query dengan menu baru), jd di hide aja -->
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KRITERIA PEMILIHAN FASILITAS
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="CriteriaID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <input id="hdncheckPilih" type="hidden" name="hdncheckPilih" runat="server" value='<%# DataBinder.eval(Container,"DataItem.ChekPilih") %>' />
                                    <asp:CheckBox ID="ChekPilih" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaDescription" HeaderText="KRITERIA"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaOption" HeaderText="JENIS PILIHAN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaValue1" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaValue2" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="NILAI KRITERIA">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtKriteria1" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtKriteria2" runat="server" Visible="false"></asp:TextBox>
                                    <asp:DropDownList ID="cboKriteria" runat="server" Visible="false">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="cboKriteriaValue" runat="server" Visible="false">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CriteriaValueID" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <%--</div>--%>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW - FASILITAS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Funding Bank
                </label>
                <asp:Label ID="lblFundingCompanyV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Prosentase Security
                </label>
                <asp:Label ID="lblSecurityPercentage" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <asp:Label ID="lblBranchV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Prosentase Security Berdasarkan
                </label>
                <asp:Label ID="lblSecurityPercentageOf" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Fasilitas
                </label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Security
                </label>
                <asp:CheckBox ID="chkCorporateV" runat="server" Text="Corporate Guarantee"></asp:CheckBox>
                <asp:CheckBox ID="chkParentV" runat="server" Text="Parent Guarantee"></asp:CheckBox>
                <asp:CheckBox ID="chkCashV" runat="server" Text="Cash Deposit"></asp:CheckBox>
                <asp:CheckBox ID="chkFixedV" runat="server" Text="Fixed Asset"></asp:CheckBox>
                <asp:CheckBox ID="chkARV" runat="server" Text="Fixed Asset"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Fasilitas
                </label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Waktu Pemilihan Fasilitas
                </label>
                <asp:Label ID="lblSelectionTime" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Fasilitas
                </label>
                <asp:Label ID="lblContractDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Porsi Funding Bank
                </label>
                <asp:Label ID="lblFundingCoyPortion" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Plafond
                </label>
                <asp:Label ID="lblPlafondAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Provisi
                </label>
                <asp:Label ID="lblProvision" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Mata Uang
                </label>
                <asp:Label ID="lblCurrency" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   <%-- Admin Fee Per BPKB--%>
                     Admin Fee Per Jaminan
                </label>
                <asp:Label ID="lblAdminFeePerAct" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Type Fasilitas
                </label>
                <asp:Label ID="lblFacilityType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Prepayment
                </label>
                <asp:Label ID="lblPrepaymentPenalty" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Berlaku Dari
                </label>
                <asp:Label ID="lblPeriodFrom" runat="server"></asp:Label>&nbsp;to
                <asp:Label ID="lblPeriodTo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Commintment Fee
                </label>
                <asp:Label ID="lblCommitmentFee" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo Final
                </label>
                <asp:Label ID="lblFinalMaturityDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan
                </label>
                <asp:Label ID="lblLatePaymentFee" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Evaluasi
                </label>
                <asp:Label ID="lblEvaluationDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Fee Per Pencairan
                </label>
                <asp:Label ID="lblFeePerDrawDown" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Suku Margin
                </label>
                <asp:Label ID="lblInterestRate" runat="server"></asp:Label>%
            </div>
            <div class="form_right">
                <label>
                    Fee Per Fasilitas
                </label>
                <asp:Label ID="lblFeePerFacility" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Marin
                </label>
                <asp:Label ID="lblInterestType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Maximum Pencairan
                </label>
                <asp:Label ID="lblMaxDD" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Floating Mulai
                </label>
                <asp:Label ID="lblFloatingStart" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Prepayment
                </label>
                <asp:Label ID="lblPrepaymentType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan Margin
                </label>
                <asp:Label ID="lblInterestNote" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <%--Lokasi BPKB--%>
                    Lokasi Jaminan
                </label>
                <asp:Label ID="lblBPKBLocation" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Commitment Status
                </label>
                <asp:Label ID="lblCommitmentStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Late Charges Grace Period
                </label>
                <asp:Label ID="lblLCGracePeriod" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Skema Pembayaran
                </label>
                <asp:Label ID="lblPaymentScheme" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Penjaminan (Recourse)
                </label>
                <asp:Label ID="lblRecourseType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Fasilitas
                </label>
                <asp:Label ID="lblFacilityKind" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Status Balance Sheet
                </label>
                <asp:Label ID="lblBalanceSheetStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Mirroring
                </label>
                <asp:Label ID="lblMirroring" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Notaris
                </label>
                <asp:Label ID="lblNotaryName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnExit" runat="server" CausesValidation="False" Text="Exit" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRate">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    RATE FASILITAS
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankNameRate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyNameRate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="lblFundingContractNoRate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR RATE FASILITAS
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgRateFunding" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general"
                        Width="25%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TENOR">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTenor" runat="server"> </asp:Label>
                                    <asp:Label ID="lblTenorFrom" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TenorFrom") %>'> </asp:Label>
                                    <asp:Label ID="lblTenorTo" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TenorTo") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RATE" HeaderStyle-CssClass="item_grid_center">
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtRate" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Rate"),2) %>'
                                        onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                        onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                        onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" autocomplete="off">0</asp:TextBox>
                                    <asp:RangeValidator runat="server" ID="rvRatae" Display="Dynamic" ErrorMessage="Input hanya boleh 0.01 s/d 99.99"
                                        ControlToValidate="txtRate" MaximumValue="99.99" MinimumValue="0.01" Type="Double"
                                        CssClass="validator_general"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSaveRate" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancelRate" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:HiddenField runat="server" ID="hdfBankID" />
    <asp:HiddenField runat="server" ID="hdfCompanyID" />
    <asp:HiddenField runat="server" ID="hdfFundingContractNo" />
    </ContentTemplate> 
    </asp:UpdatePanel>
    </form>
</body>
</html>

