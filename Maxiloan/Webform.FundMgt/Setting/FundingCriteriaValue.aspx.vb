﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingCriteriaValue
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New FundingCriteriaValueController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property CriteriaID() As String
        Get
            Return CType(ViewState("CriteriaID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CriteriaID") = Value
        End Set
    End Property
    Private Property CriteriaDescription() As String
        Get
            Return CType(ViewState("CriteriaDescription"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CriteriaDescription") = Value
        End Set
    End Property
    Private Property CriteriaOption() As String
        Get
            Return CType(ViewState("CriteriaOption"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CriteriaOption") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            'Me.FormID = "FUNDCRTVALUE"
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppID) Then
            txtgoPage.Text = "1"
            Me.Sort = "CriteriaValueID ASC"
            Me.CriteriaID = Request("CriteriaID")
            Me.CriteriaDescription = Request("CriteriaDescription")
            Me.CriteriaOption = Request("CriteriaOption")
            'End If
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oFundingCriteriaValue As New Parameter.FundingCriteriaValue
        InitialDefaultPanel()
        Dim temCMD As String = cmdWhere

        cmdWhere = "CriteriaID = '" + Me.CriteriaID + "'"
        cmdWhere += temCMD

        oFundingCriteriaValue.strConnection = GetConnectionString()
        oFundingCriteriaValue.WhereCond = cmdWhere
        oFundingCriteriaValue.CurrentPage = currentPage
        oFundingCriteriaValue.PageSize = pageSize
        oFundingCriteriaValue.SortBy = Me.Sort
        oFundingCriteriaValue = m_controller.GetFundingCriteriaValue(oFundingCriteriaValue)

        If Not oFundingCriteriaValue Is Nothing Then
            dtEntity = oFundingCriteriaValue.Listdata
            recordCount = oFundingCriteriaValue.Totalrecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            BtnPrint.Enabled = False
        Else
            BtnPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

        txtNmKriteria.Text = CriteriaDescription
        cboJenisPilihan.SelectedIndex = cboJenisPilihan.Items.IndexOf(cboJenisPilihan.Items.FindByValue(CriteriaOption.Trim))

    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oFundingCriteriaValue As New Parameter.FundingCriteriaValue
        If e.CommandName = "Edit" Then
            'If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppID) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            'End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            BtnBack.Visible = False
            BtnCancel.Visible = True
            BtnSave.Visible = True
            BtnClose.Visible = False
            BtnCancel.CausesValidation = False

            lblTitleAddEdit.Text = Me.AddEdit
            oFundingCriteriaValue.CriteriaValueID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oFundingCriteriaValue.strConnection = GetConnectionString()
            oFundingCriteriaValue = m_controller.GetFundingCriteriaValueList(oFundingCriteriaValue)
            BtnCancel.CausesValidation = False

            hdnCriteriaValueID.Value = oFundingCriteriaValue.CriteriaValueID.Trim
            txtIDNilai.Text = oFundingCriteriaValue.CriteriaValueID.Trim
            txtNamaKriteria.Text = oFundingCriteriaValue.CriteriaDescription.Trim
            txtValue.Text = IIf(canInputValue(), oFundingCriteriaValue.CriteriaValue.Trim, "")
            txtValue.Enabled = canInputValue()
            rfvValue.Enabled = canInputValue()

            If canInputValue() Then
                lblValueLabel.Attributes.Add("class", "label_req")
            Else
                lblValueLabel.Attributes.Remove("class")
            End If

            txtSQLData.Text = oFundingCriteriaValue.SQLData.Trim

        ElseIf e.CommandName = "Delete" Then
            'If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            'End If
            Dim customClass As New Parameter.FundingCriteriaValue
            With customClass
                .CriteriaValueID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.FundingCriteriaValueDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
        End If
    End Sub
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.FundingCriteriaValue
        Dim ErrMessage As String = ""

        With customClass
            .CriteriaID = CriteriaID
            .CriteriaValueIDEdit = txtIDNilai.Text
            .CriteriaDescription = txtNamaKriteria.Text
            .CriteriaValue = txtValue.Text
            .SQLData = txtSQLData.Text
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.FundingCriteriaValueSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.CriteriaValueID = hdnCriteriaValueID.Value
            m_controller.FundingCriteriaValueSaveEdit(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppID) Then
        If SessionInvalid() Then
            Exit Sub
        End If
        'End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        BtnBack.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True
        BtnClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        BtnCancel.CausesValidation = False

        txtIDNilai.Text = Nothing
        txtNamaKriteria.Text = Nothing
        txtValue.Text = Nothing
        txtValue.Enabled = canInputValue()
        rfvValue.Enabled = canInputValue()

        If canInputValue() Then
            lblValueLabel.Attributes.Add("class", "label_req")
        Else
            lblValueLabel.Attributes.Remove("class")
        End If

        txtSQLData.Text = Nothing
    End Sub
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppID) Then
        If SessionInvalid() Then
            Exit Sub
        End If
        'End If
        SendCookies()
        Response.Redirect("Report/FundingCriteriaValue.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("FundingCriteriaValue")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("FundingCriteriaValue")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        CmdWhere = ""
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = " and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnBackMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBackMenu.Click
        Response.Redirect("FundingCriteria.aspx")
    End Sub
    Private Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Response.Redirect("FundingCriteriaValue.aspx")
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("FundingCriteriaValue.aspx?cmd=dtl&CriteriaID=" + CriteriaID + _
                                          "&CriteriaDescription=" + CriteriaDescription + "" + _
                                          "&CriteriaOption=" + CriteriaOption + "")
    End Sub

    Function canInputValue() As Boolean
        Select Case Me.CriteriaOption
            Case "MUL", "SEL"
                Return True
            Case "RAN", "MIN", "MAX"
                Return False
            Case Else
                Return True
        End Select
    End Function
End Class