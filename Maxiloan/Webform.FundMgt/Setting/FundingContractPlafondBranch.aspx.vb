﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingContractPlafondBranch
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"

    Protected WithEvents txtPlafondAmount As ucNumberFormat

#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString


        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        pnlDoc.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractPlafondBranchList"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
           ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingPlafondBranch.DataSource = dtvEntity
        Try
            dtgFundingPlafondBranch.DataBind()
        Catch
            dtgFundingPlafondBranch.CurrentPageIndex = 0
            dtgFundingPlafondBranch.DataBind()
        End Try
        bindTitleTop()
        PagingFooter()
    End Sub
#End Region

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.FundingContractBranch
        Dim sType As String

        Try
            With customClass
                .strConnection = GetConnectionString
                .BankId = Me.BankID
                .FundingCoyID = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .BranchId = drdBranch.SelectedItem.Value
                If txtPlafondAmount.Text.Trim = "" Then
                    .PlafondAmount = 0
                    .OSAmount = 0
                Else
                    .PlafondAmount = CDec(txtPlafondAmount.Text)
                    .OSAmount = CDec(txtPlafondAmount.Text)
                End If
                .BookAmount = 0
                .AcqStatus = "Y"
            End With


            m_Company.AddFundingContractPlafondBranch(customClass)
          ShowMessage(lblMessage, "Simpan Data Berhasil ", False)

            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

#Region "Edit"
    Private Sub Edit()

        Dim customClass As New Parameter.FundingContractBranch
        Dim sType As String

        Try
            With customClass
                .strConnection = GetConnectionString
                .BankId = Me.BankID
                .FundingCoyID = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .BranchId = Me.BranchID
                If txtPlafondAmount.Text.Trim = "" Then
                    .PlafondAmount = 0
                    .OSAmount = 0
                Else
                    .PlafondAmount = CDec(txtPlafondAmount.Text)
                    .OSAmount = CDec(txtPlafondAmount.Text)
                End If
                .BookAmount = 0
                .AcqStatus = "Y"
            End With


            m_Company.EditFundingContractPlafondBranch(customClass)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)

            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

#Region "ADD-DOC"
    Private Sub AddDoc()

        Dim customClass As New Parameter.FundingContract
        Try
            customClass.strConnection = GetConnectionString
            customClass.BankId = Me.BankID
            customClass.FundingCoyId = Me.CompanyID
            customClass.FundingContractNo = Me.FundingContractNo
            m_Company.DeleteFundingContractDoc(customClass)
            If chkDoc1.Checked Then
                customClass.FundingDocId = 1
                customClass.DocumentNote = txtNote1.Text
                m_Company.AddFundingContractDoc(customClass)
            End If
            If chkDoc2.Checked Then
                customClass.FundingDocId = 2
                customClass.DocumentNote = txtNote2.Text
                m_Company.AddFundingContractDoc(customClass)
            End If
            If chkDoc3.Checked Then
                customClass.FundingDocId = 3
                customClass.DocumentNote = txtNote3.Text
                m_Company.AddFundingContractDoc(customClass)
            End If
            If chkDoc4.Checked Then
                customClass.FundingDocId = 4
                customClass.DocumentNote = txtNote4.Text
                m_Company.AddFundingContractDoc(customClass)
            End If



            'm_Company.AddFundingContractDoc(customClass)
            'lblMessage.Text = "Record is added successfully "
            'lblMessage.Visible = True

            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

#Region "View"

#Region "EditView"
    Private Sub editView(ByVal Companyid As String, ByVal BranchId As String)
        Dim oCompany As New Parameter.FundingContractBranch
        Dim dtCompany As New DataTable

        Try
            Dim strConnection As String = getConnectionString

            With oCompany
                .strConnection = getConnectionString()
                .FundingContractNo = Companyid
                .BranchId = BranchId
            End With

            oCompany = m_Company.ListFundingContractPlafondBranchByID(oCompany)
            dtCompany = oCompany.ListData


            'Isi semua field Edit Action
            drdBranch.Visible = False
            lblBranch.Visible = True
            lblBranch.Text = CStr(dtCompany.Rows(0).Item("BranchFullName")).Trim
            txtPlafondAmount.Text = CStr(dtCompany.Rows(0).Item("PlafondAmount")).Trim
            'lblNonAllocatedAmount.Text = CStr(CInt(dtCompany.Rows(0).Item("PlafondAmount")) - CInt(dtCompany.Rows(0).Item("TAPlafond")))



            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region "DocEditView"

    Private Sub docEditView()        
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = Me.FundingContractNo
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortby
            .SpName = "spFundingContractDocView"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        Dim dtRow As DataRow
        Dim item As Char
        Try
            dtsEntity = oContract.ListData
            For Each dtRow In dtsEntity.Rows
                item = CStr(dtRow.Item("FundingDocId"))
                Select Case item
                    Case "1"
                        chkDoc1.Checked = True
                        txtNote1.Text = CStr(dtRow.Item("DocumentNote"))
                    Case "2"
                        chkDoc2.Checked = True
                        txtNote2.Text = CStr(dtRow.Item("DocumentNote"))
                    Case "3"
                        chkDoc3.Checked = True
                        txtNote3.Text = CStr(dtRow.Item("DocumentNote"))
                    Case "4"
                        chkDoc4.Checked = True
                        txtNote4.Text = CStr(dtRow.Item("DocumentNote"))
                End Select
            Next


        Catch e As Exception
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            btnSaveDoc.Visible = True
        End Try

    End Sub
#End Region

#End Region

#Region "MISC"

#Region "ClearAddForm"
    Private Sub ClearAddForm()
        txtPlafondAmount.Text = 0
        lblBranch.Visible = False
        drdBranch.Visible = True
    End Sub
#End Region

#Region "BindComboBranch"
    Private Sub bindComboIDbranch()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spGetComboBranch"
        End With


        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            Dim i As Integer


            drdBranch.DataValueField = "BranchID"
            drdBranch.DataTextField = "BranchFullName"

            drdBranch.DataSource = dtvEntity

            drdBranch.DataBind()
        Catch e As Exception
          showmessage(lblmessage, e.message , true)
        End Try

    End Sub
#End Region

#Region "BindTitleTop"
    Private Sub bindTitleTop()        
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortby
            .SpName = "spFundingContractPlafondBranchTitle"
        End With


        oContract = cContract.GetGeneralPaging(oContract)
        Try
            lblBankName.Text = Me.BankName
            lblFundingCoyName.Text = Me.CompanyName
            lblFundingContractNo.Text = Me.FundingContractNo
            lblContractName.Text = Me.ContractName

            dtsEntity = oContract.ListData
            'dtvEntity = dtsEntity.DefaultView
            lblContractPlafondAmount.Text = FormatNumber(dtsEntity.Rows(0).Item("cPlafondAmount"), 2).Trim
            lblNonAllocatedAmount.Text = FormatNumber(dtsEntity.Rows(0).Item("cPlafondAmount") - dtsEntity.Rows(0).Item("TAPlafond"), 2)

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)

        End Try

    End Sub
#End Region

#Region "BindDocForm"
    Private Sub bindDocRef()        
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        Dim dtsEntitycount As Int16

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = Me.FundingContractNo
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortby
            .SpName = "spFundingContractGetDocRef"
        End With

        'Clear Contract Doc
        txtNote1.Text = ""
        txtNote2.Text = ""
        txtNote3.Text = ""
        txtNote4.Text = ""
        chkDoc1.Checked = False
        chkDoc2.Checked = False
        chkDoc3.Checked = False
        chkDoc4.Checked = False

        chkDoc1.Visible = False
        txtNote1.Visible = False
        chkDoc2.Visible = False
        txtNote2.Visible = False
        chkDoc3.Visible = False
        txtNote3.Visible = False
        chkDoc4.Visible = False
        txtNote4.Visible = False

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtsEntitycount = dtsEntity.Rows.Count
            If dtsEntitycount = 1 Then
                chkDoc1.Text = CStr(dtsEntity.Rows(0).Item("FundingDocName"))
                chkDoc1.Visible = True
                txtNote1.Visible = True
            ElseIf dtsEntitycount = 2 Then
                chkDoc1.Text = CStr(dtsEntity.Rows(0).Item("FundingDocName"))
                chkDoc1.Visible = True
                txtNote1.Visible = True
                chkDoc2.Text = CStr(dtsEntity.Rows(1).Item("FundingDocName"))
                chkDoc2.Visible = True
                txtNote2.Visible = True
            ElseIf dtsEntitycount = 3 Then
                chkDoc1.Text = CStr(dtsEntity.Rows(0).Item("FundingDocName"))
                chkDoc1.Visible = True
                txtNote1.Visible = True
                chkDoc2.Text = CStr(dtsEntity.Rows(1).Item("FundingDocName"))
                chkDoc2.Visible = True
                txtNote2.Visible = True
                chkDoc3.Text = CStr(dtsEntity.Rows(2).Item("FundingDocName"))
                chkDoc3.Visible = True
                txtNote3.Visible = True
            ElseIf dtsEntitycount = 4 Then
                chkDoc1.Text = CStr(dtsEntity.Rows(0).Item("FundingDocName"))
                chkDoc1.Visible = True
                txtNote1.Visible = True
                chkDoc2.Text = CStr(dtsEntity.Rows(1).Item("FundingDocName"))
                chkDoc2.Visible = True
                txtNote2.Visible = True
                chkDoc3.Text = CStr(dtsEntity.Rows(2).Item("FundingDocName"))
                chkDoc3.Visible = True
                txtNote3.Visible = True
                chkDoc4.Text = CStr(dtsEntity.Rows(3).Item("FundingDocName"))
                chkDoc4.Visible = True
                txtNote4.Visible = True
            End If



            'chkDoc2.Text = CStr(dtsEntity.Rows(1).Item("FundingDocName"))
            'chkDoc3.Text = CStr(dtsEntity.Rows(2).Item("FundingDocName"))
            'chkDoc4.Text = CStr(dtsEntity.Rows(3).Item("FundingDocName"))

        Catch e As Exception

            ShowMessage(lblMessage, "Harap Pilih Referensi Dokumen Funding secara Manual", True)
            btnSaveDoc.Visible = False
        End Try

    End Sub
#End Region

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
            InitialDefaultPanel()

            Me.FormID = "Company"

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")


            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""
            If Command = "Doc" Then
                bindTitleTop()
                bindDocRef()
                docEditView()
                pnlDoc.Visible = True
                pnlList.Visible = False

            Else
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
            End If

            InitiateUCnumberFormat(txtPlafondAmount, False, True)

            txtPlafondAmount.RangeValidatorMinimumValue = "0"
            txtPlafondAmount.RangeValidatorMaximumValue = "99999999999999"

        End If
    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddnew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            bindTitleTop()
            ClearAddForm()
            bindComboIDbranch()

            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            pnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True

            Case "EDIT"
                PanelAllFalse()
                Edit()
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
        End Select
    End Sub

    Private Sub dtgFundingPlafondBranch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingPlafondBranch.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    bindTitleTop()
                    Me.BranchID = e.Item.Cells(0).Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    'lblMenuAddEdit.Text = "EDIT"
                    editView(Me.FundingContractNo, Me.BranchID)
                End If

            Case "Delete"

                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    'lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), LinkButton)
                    Me.BranchID = e.Item.Cells(0).Text
                    Dim customClass As New Parameter.FundingContractBranch
                    With customClass
                        .strConnection = getConnectionString
                        .FundingContractNo = Me.FundingContractNo
                        .BranchId = Me.BranchID
                    End With

                    Try
                        m_Company.DeleteFundingContractPlafondBranch(customClass)
                        ShowMessage(lblMessage, "Hapus Data Berhasil ", False)

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub

    Private Sub dtgFundingPlafondBranch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingPlafondBranch.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub btnSaveDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveDoc.Click
        Try
            AddDoc()
        Finally
            Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
        End Try
    End Sub

    Private Sub btnCancelDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelDoc.Click
        Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
    End Sub

    Private Sub BtnCancelList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelList.Click
        Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub
    
End Class