﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingCriteria
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New FundingCriteriaController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            Me.FormID = "FUNDCRTVAL"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtgoPage.Text = "1"
                Me.Sort = "CriteriaID ASC"
                Me.CmdWhere = "All"
                cboSJenisPilihan.Visible = False
            End If
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oFundingCriteria As New Parameter.FundingCriteria
        InitialDefaultPanel()
        oFundingCriteria.strConnection = GetConnectionString()
        oFundingCriteria.WhereCond = cmdWhere
        oFundingCriteria.CurrentPage = currentPage
        oFundingCriteria.PageSize = pageSize
        oFundingCriteria.SortBy = Me.Sort
        oFundingCriteria = m_controller.GetFundingCriteria(oFundingCriteria)

        If Not oFundingCriteria Is Nothing Then
            dtEntity = oFundingCriteria.Listdata
            recordCount = oFundingCriteria.Totalrecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            BtnPrint.Enabled = False
        Else
            BtnPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim CriteriaOption As Label
        Dim hdnCriteriaOption As HtmlInputHidden        

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
            hdnCriteriaOption = CType(e.Item.FindControl("hdnCriteriaOption"), HtmlInputHidden)
            CriteriaOption = CType(e.Item.FindControl("CriteriaOption"), Label)            

            If hdnCriteriaOption.Value = "SEL" Then
                CriteriaOption.Text = "SELECT"
            ElseIf hdnCriteriaOption.Value = "MUL" Then
                CriteriaOption.Text = "MULTIPLE SELECT"
            ElseIf hdnCriteriaOption.Value = "MAX" Then
                CriteriaOption.Text = "MAX"
            ElseIf hdnCriteriaOption.Value = "MIN" Then
                CriteriaOption.Text = "MIN"
            ElseIf hdnCriteriaOption.Value = "RAN" Then
                CriteriaOption.Text = "RANGE"
            End If
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oFundingCriteria As New Parameter.FundingCriteria
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            BtnBack.Visible = False
            BtnCancel.Visible = True
            BtnSave.Visible = True
            BtnClose.Visible = False
            BtnCancel.CausesValidation = False

            lblTitleAddEdit.Text = Me.AddEdit
            oFundingCriteria.CriteriaID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oFundingCriteria.strConnection = GetConnectionString()
            oFundingCriteria = m_controller.GetFundingCriteriaList(oFundingCriteria)
            BtnCancel.CausesValidation = False

            hdnCriteriaID.Value = oFundingCriteria.CriteriaID.Trim
            txtKriteria.Text = oFundingCriteria.CriteriaID.Trim            
            txtNamaKriteria.Text = oFundingCriteria.CriteriaDescription.Trim
            cboJenisPilihan.SelectedIndex = cboJenisPilihan.Items.IndexOf(cboJenisPilihan.Items.FindByValue(oFundingCriteria.CriteriaOption.Trim))

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.FundingCriteria
            With customClass
                .CriteriaID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.FundingCriteriaDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
        ElseIf e.CommandName = "Kriteria" Then
            'If CType(e.Item.FindControl("hdnCriteriaOption"), HtmlInputHidden).Value = "SEL" Or
            '    CType(e.Item.FindControl("hdnCriteriaOption"), HtmlInputHidden).Value = "MUL" Then
            Response.Redirect("FundingCriteriaValue.aspx?cmd=dtl&CriteriaID=" + dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString.Trim + _
                              "&CriteriaDescription=" + e.Item.Cells.Item(4).Text.Trim + "" + _
                              "&CriteriaOption=" + CType(e.Item.FindControl("hdnCriteriaOption"), HtmlInputHidden).Value + "")
            'End If
        End If
    End Sub
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.FundingCriteria
        Dim ErrMessage As String = ""

        With customClass
            .CriteriaIDEdit = txtKriteria.Text
            .CriteriaDescription = txtNamaKriteria.Text
            .CriteriaOption = cboJenisPilihan.SelectedItem.Value
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.FundingCriteriaSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.CriteriaID = hdnCriteriaID.Value
            m_controller.FundingCriteriaSaveEdit(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        BtnBack.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True
        BtnClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        BtnCancel.CausesValidation = False

        txtKriteria.Text = Nothing
        txtNamaKriteria.Text = Nothing
        cboJenisPilihan.SelectedIndex = 0
    End Sub
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/FundingCriteria.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("FundingCriteria")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("FundingCriteria")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "All"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If cboSJenisPilihan.Visible Then
            Me.CmdWhere = "CriteriaOption = '" + cboSJenisPilihan.SelectedItem.Value + "'"
        Else
            If txtSearch.Text.Trim <> "" Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            End If
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    
    Private Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Response.Redirect("FundingCriteria.aspx")
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("FundingCriteria.aspx")
    End Sub
    Private Sub cboSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cboSearch.SelectedIndexChanged
        If cboSearch.SelectedItem.Value = "CriteriaOption" Then
            cboSJenisPilihan.Visible = True
            txtSearch.Visible = False
        Else
            cboSJenisPilihan.Visible = False
            txtSearch.Visible = True
        End If
    End Sub
End Class