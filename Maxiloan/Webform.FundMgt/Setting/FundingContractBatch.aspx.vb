﻿#Region "Imports"
'Imports CrystalDecisions.ReportSource
'Imports CrystalDecisions.Web
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Framework.SQLEngine
Imports System.Data
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.IO
Imports System.Data.Odbc
Imports System.Data.OleDb
Imports System.Text
Imports ClosedXML.Excel
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions


#End Region

Public Class FundingContractBatch
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"

    Protected WithEvents txtAmount As ucNumberFormat
    'Protected WithEvents txtInterestRate As ucNumberFormat
    Protected WithEvents txtProvisionAmount As ucNumberFormat
    Protected WithEvents txtAdminAmount As ucNumberFormat
    Protected WithEvents txtExchangeRate As ucNumberFormat
    'Protected WithEvents txtTenor As ucNumberFormat
    Protected WithEvents txtBatchDate As ucDateCE
    'Protected WithEvents txtFinalMaturityDate As ucDateCE
    Protected WithEvents txtDueDate As ucDateCE
    Protected WithEvents txtStartFrom As ucDateCE
    Protected WithEvents txtPeriodFrom As ucDateCE
    Protected WithEvents txtPeriodTo As ucDateCE

#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
    Private m_controller As New DataUserControlController
#End Region
#Region "Propertis"
    Public Property InsPeriod() As String
        Get
            Return CStr(ViewState("InsPeriod"))
        End Get
        Set(value As String)
            ViewState("InsPeriod") = value
        End Set
    End Property
    Public Property Amount As Double
        Get
            Return CDbl(ViewState("Amount"))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Public Property DueDate_() As Date
        Get
            Return CDate(ViewState("DueDate"))
        End Get
        Set(value As Date)
            ViewState("DueDate") = value
        End Set
    End Property
    Public Property ExchangeRate() As Double
        Get
            Return CStr(ViewState("ExchangeRate"))
        End Get
        Set(value As Double)
            ViewState("ExchangeRate") = value
        End Set
    End Property
    Public Property Currency() As String
        Get
            Return CStr(ViewState("Currency"))
        End Get
        Set(value As String)
            ViewState("Currency") = value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return CStr(ViewState("BankAccountID"))
        End Get
        Set(value As String)
            ViewState("BankAccountID") = value
        End Set
    End Property
    Public Property BPKBLocation() As String
        Get
            Return CStr(ViewState("BPKBLocation"))
        End Get
        Set(value As String)
            ViewState("BPKBLocation") = value
        End Set
    End Property
    Public Property CaraPembayaran() As String
        Get
            Return CStr(ViewState("CaraPembayaran"))
        End Get
        Set(value As String)
            ViewState("CaraPembayaran") = value
        End Set
    End Property
    Public Property IsFundingNTF() As Boolean
        Get
            Return CStr(ViewState("IsFundingNTF"))
        End Get
        Set(value As Boolean)
            ViewState("IsFundingNTF") = value
        End Set
    End Property
    Public Property AdminAmount() As Double
        Get
            Return CStr(ViewState("AdminAmount"))
        End Get
        Set(value As Double)
            ViewState("AdminAmount") = value
        End Set
    End Property
    Public Property ProvisionAmount() As Double
        Get
            Return CStr(ViewState("ProvisionAmount"))
        End Get
        Set(value As Double)
            ViewState("ProvisionAmount") = value
        End Set
    End Property
    Public Property FinalMaturityDate() As Date
        Get
            Return CDate(ViewState("FinalMaturityDate"))
        End Get
        Set(value As Date)
            ViewState("FinalMaturityDate") = value
        End Set
    End Property
    Public Property HitungBUnga() As String
        Get
            Return CStr(ViewState("HitungBUnga"))
        End Get
        Set(value As String)
            ViewState("HitungBUnga") = value
        End Set
    End Property
    Public Property FirstInstallment() As String
        Get
            Return CStr(ViewState("FirstInstallment"))
        End Get
        Set(value As String)
            ViewState("FirstInstallment") = value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return CStr(ViewState("InterestType"))
        End Get
        Set(value As String)
            ViewState("InterestType") = value
        End Set
    End Property
    Public Property BatchDate() As DateTime
        Get
            Return CDate(ViewState("BatchDate"))
        End Get
        Set(value As Date)
            ViewState("BatchDate") = value
        End Set
    End Property
    Public Property InsScheme() As String
        Get
            Return CStr(ViewState("InsScheme"))
        End Get
        Set(value As String)
            ViewState("InsScheme") = value
        End Set
    End Property
    Public Property PaymentScheme() As String
        Get
            Return CStr(ViewState("PaymentScheme"))
        End Get
        Set(value As String)
            ViewState("PaymentScheme") = value
        End Set
    End Property
    Public Property SPName() As String
        Get
            Return CStr(ViewState("SPName"))
        End Get
        Set(value As String)
            ViewState("SPName") = value
        End Set
    End Property

#End Region

#Region "FormLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'InitialDefaultPanel()

        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "FUNDINGBATCH"
            Me.IsReschedule = False
            Me.FundingBatchNo = ""

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strFileLocation As String

                strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                               & "var x = screen.width;" & vbCrLf _
                               & "var y = screen.height;" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If

            Me.IsReschedule = False
            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""
            If Request.QueryString("FundingBatchNo") <> "" Then
                Me.FundingBatchNo = Request.QueryString("FundingBatchNo").Trim
                If Command = "Edit" Then
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    div3Btn.Visible = True
                    'divDueDate.Visible = True                    
                    divTotalAccount.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "- EDIT"
                    editView()
                    EnabledWidgetControl()
                    CheckDrawDownStatus()
                    'If Me.StatusDrawn = "D" Or drdInterestType.SelectedItem.Value.Trim = "X" Or drdInterestType.SelectedItem.Value.Trim = "B" Then
                    '    DisableWidgetControl()
                    'End If

                    If Me.StatusDrawn = "D" Then
                        DisableWidgetControl()
                    End If

                    bindTitleTop()
                End If
                'If checkFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
                '    If sessioninvalid() Then
                '        Exit Sub
                '    End If
                '    pnlList.Visible = False
                '    pnlAddEdit.Visible = False
                '    pnlView.Visible = True
                '    Me.ActionAddEdit = "VIEW"
                '    lblMenuAddEdit.Text = "- VIEW"
                '    bindTitleTop()
                '    JustView()
                'End If
            Else
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
            End If



            InitiateUCnumberFormat(txtAmount, True, True)
            'InitiateUCnumberFormat(txtInterestRate, True, True)
            InitiateUCnumberFormat(txtProvisionAmount, True, True)
            InitiateUCnumberFormat(txtAdminAmount, True, True)
            'InitiateUCnumberFormat(txtTenor, True, True)            

            txtAmount.RangeValidatorMinimumValue = "0"
            txtAmount.RangeValidatorMaximumValue = "99999999999999"
            'txtInterestRate.RangeValidatorMinimumValue = "0"
            'txtInterestRate.RangeValidatorMaximumValue = "100"
            txtProvisionAmount.RangeValidatorMinimumValue = "0"
            txtProvisionAmount.RangeValidatorMaximumValue = "99999999999999"
            txtAdminAmount.RangeValidatorMinimumValue = "0"
            txtAdminAmount.RangeValidatorMaximumValue = "99999999999999"
            'txtTenor.RangeValidatorMinimumValue = "0"
            'txtTenor.RangeValidatorMaximumValue = "999"
        End If
    End Sub

#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString

        pnlList.Visible = True
        pnlTop.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlTop.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlTop.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlTop.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
    End Sub

#End Region
#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property
    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property
    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property
    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property
    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property
    Private Property _BankID() As String
        Get
            Return CStr(ViewState("_BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("_BankID") = Value
        End Set
    End Property
    Private Property _FileType() As String
        Get
            Return CStr(ViewState("_FileType"))
        End Get
        Set(ByVal Value As String)
            ViewState("_FileType") = Value
        End Set
    End Property
    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property
    Public Property StatusDrawn() As String
        Get
            Return CStr(ViewState("StatusDrawn"))
        End Get
        Set(ByVal Value As String)
            ViewState("StatusDrawn") = Value
        End Set
    End Property
    Public Property FacilityKind() As String
        Get
            Return CStr(ViewState("FacilityKind"))
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityKind") = Value
        End Set
    End Property
    Public Property InterestRate() As Double
        Get
            Return CDbl(ViewState("InterestRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InterestRate") = Value
        End Set
    End Property
    Public Property IsReschedule() As Boolean
        Get
            Return CBool(ViewState("IsReschedule"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsReschedule") = Value
        End Set
    End Property
    Private Property FlagCS() As String
        Get
            Return CStr(ViewState("FlagCS"))
        End Get
        Set(ByVal Value As String)
            ViewState("FlagCS") = Value
        End Set
    End Property
    Property WhereCond() As String
        Get
            Return CType(ViewState("WhereCond"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond") = Value
        End Set
    End Property
    Public Property TenorMax() As Short
        Get
            Return CStr(ViewState("TenorMax"))
        End Get
        Set(ByVal Value As Short)
            ViewState("TenorMax") = Value
        End Set
    End Property
#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
        pnlView.Visible = False
        lblMenuAddEdit.Visible = False
        pnlDownload.Visible = False
    End Sub
    Private Sub DisableWidgetControl()
        'oBatchDate.Disable()
        'If Me.FacilityKind <> "TLOAN" Then
        '    txtAmount.Enabled = False
        'Else
        txtAmount.Enabled = True
        'End If
        drdInterestType.Enabled = False
        drdPaymentScheme.Enabled = False
        txtDueDate.Enabled = True
        drdCurrency.Enabled = False
        txtExchangeRate.Enabled = False
        'txtTenor.Enabled = False
        drdInsPeriod.Enabled = False
        drdInsScheme.Enabled = False
        'txtTenor.Enabled = False
    End Sub
    Private Sub EnabledWidgetControl()
        txtAmount.Enabled = True
        drdInterestType.Enabled = True
        drdPaymentScheme.Enabled = True
        drdCurrency.Enabled = True
        txtExchangeRate.Enabled = True
        'txtTenor.Enabled = True
        drdInsPeriod.Enabled = True
        drdInsScheme.Enabled = True
        'txtTenor.Enabled = True
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region
    Sub bindRate()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClass As New Parameter.FundingContractRate
        With customClass
            .strConnection = GetConnectionString()
            .FundingCoyID = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FUndingBatchNo = Me.FundingBatchNo
        End With

        customClass = m_Company.FundingBatchRate(customClass)
        dtsEntity = customClass.Listdata
        dtvEntity = dtsEntity.DefaultView
        gridRate.DataSource = dtsEntity
        gridRate.DataBind()

    End Sub
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractBatchList"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractBatch.DataSource = dtvEntity
        Try
            dtgFundingContractBatch.DataBind()
        Catch
            dtgFundingContractBatch.CurrentPageIndex = 0
            dtgFundingContractBatch.DataBind()
        End Try
        bindTitleTop()
        PagingFooter()
    End Sub
#End Region
#Region "ADD"
    Private Function simpatRate() As DataTable
        Dim dt As New DataTable
        Dim dr As DataRow
        Dim TenorFrom, TenorTo, Rate As New TextBox
        With dt
            .Columns.Add(New DataColumn("StartFrom", GetType(Integer)))
            .Columns.Add(New DataColumn("StartTo", GetType(Integer)))
            .Columns.Add(New DataColumn("Rate", GetType(Decimal)))
        End With

        For intloop = 0 To gridRate.Items.Count - 1
            dr = dt.NewRow
            TenorFrom = CType(gridRate.Items(intloop).FindControl("txtTenorFrom"), TextBox)
            TenorTo = CType(gridRate.Items(intloop).FindControl("txtTenorTo"), TextBox)
            Rate = CType(gridRate.Items(intloop).FindControl("txtRate"), TextBox)

            dr("StartFrom") = TenorFrom.Text.Trim
            dr("StartTo") = TenorTo.Text.Trim
            dr("Rate") = Rate.Text.Trim

            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub Add()
        Dim customClass As New Parameter.FundingContractBatch
        Try

            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = txtBatchNo.Text
                .BatchDate = ConvertDate2(IIf(txtBatchDate.Text.Trim = "", "01/01/1900", txtBatchDate.Text))
                .PrincipalAmtToFunCoy = CDec(txtAmount.Text.Trim)
                .InterestRate = CDec(txtInterestRate.Text.Trim)
                '.InterestRate = 0
                .InterestType = drdInterestType.SelectedItem.Value
                '.FinalMaturityDate = ConvertDate2(IIf(txtFinalMaturityDate.Text.Trim = "", "01/01/1900", txtFinalMaturityDate.Text))
                .FinalMaturityDate = ConvertDate2(IIf(txtPeriodTo.Text.Trim = "", "01/01/1900", txtPeriodTo.Text))
                .PaymentScheme = drdPaymentScheme.SelectedItem.Value
                .ProvisionFeeAmount = CDec(txtProvisionAmount.Text.Trim)
                .AdminAmount = CDec(txtAdminAmount.Text.Trim)
                .CurrencyID = drdCurrency.SelectedItem.Value

                If drdCurrency.SelectedItem.Value = "IDR" Then
                    .ExchangeRate = 1
                Else
                    .ExchangeRate = txtExchangeRate.Text.Trim
                End If

                '.Tenor = CInt(txtTenor.Text.Trim)
                .Tenor = 0
                .InstallmentPeriod = drdInsPeriod.SelectedItem.Value
                .InstallmentScheme = drdInsScheme.SelectedItem.Value
                .ProposeDate = ConvertDate2(IIf(txtBatchDate.Text.Trim = "", "01/01/1900", txtBatchDate.Text))
                .AragingDate = ConvertDate2("02/09/1976")
                .RealizedDate = ConvertDate2("02/09/1976")
                .InstallmentDueDate = ConvertDate2("02/09/1976")
                .AccProposedNum = 0
                .AccRealizedNum = 0
                .OSAmtToFunCoy = CDec(txtAmount.Text.Trim)
                .AssetDocLocation = drdBPKBLocation.SelectedItem.Value
                .BankAccountID = cboBank.SelectedItem.Value.Trim
                .FirstInstallment = ddlFirstInstallment.SelectedValue.Trim
                .isFundingNTF = ddlIsFundingNTF.SelectedValue.Trim
                .perhitunganBunga = ddlHitungBUnga.SelectedValue.Trim
                .CaraBayar = ddlCaraPembayaran.SelectedValue.Trim
                .PeriodeBulan = txtPeriodeBulan.Text.Trim
                .PeriodFrom = ConvertDate2(IIf(txtPeriodFrom.Text.Trim = "", "01/01/1900", txtPeriodFrom.Text))
                .PeriodTo = ConvertDate2(IIf(txtPeriodTo.Text.Trim = "", "01/01/1900", txtPeriodTo.Text))
                .ListData2 = simpatRate()
            End With

            m_Company.AddFundingContractBatch(customClass)
            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region
#Region "EDIT"
    Private Sub edit()
        Dim customClass As New Parameter.FundingContractBatch
        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNoUsed = Me.FundingBatchNo
                .FundingBatchNo = txtBatchNo.Text.Trim
                .BatchDate = ConvertDate2(IIf(txtBatchDate.Text.Trim = "", "01/01/1900", txtBatchDate.Text))
                .PrincipalAmtToFunCoy = CDec(txtAmount.Text.Trim)
                .InterestRate = CDec(txtInterestRate.Text.Trim)
                '.InterestRate = 0
                .InterestType = drdInterestType.SelectedItem.Value
                '.FinalMaturityDate = ConvertDate2(IIf(txtFinalMaturityDate.Text.Trim = "", "01/01/1900", txtFinalMaturityDate.Text))
                .FinalMaturityDate = ConvertDate2(IIf(txtPeriodTo.Text.Trim = "", "01/01/1900", txtPeriodTo.Text))
                .PaymentScheme = drdPaymentScheme.SelectedItem.Value
                .ProvisionFeeAmount = CDec(txtProvisionAmount.Text.Trim)
                .AdminAmount = CDec(txtAdminAmount.Text.Trim)
                .CurrencyID = drdCurrency.SelectedItem.Value

                If drdCurrency.SelectedItem.Value = "IDR" Then
                    .ExchangeRate = 1
                Else
                    .ExchangeRate = txtExchangeRate.Text.Trim
                End If

                '.Tenor = CInt(txtTenor.Text.Trim)
                '.Tenor = 0
                .InstallmentPeriod = drdInsPeriod.SelectedItem.Value
                .InstallmentScheme = drdInsScheme.SelectedItem.Value
                .ProposeDate = ConvertDate2(IIf(txtBatchDate.Text.Trim = "", "01/01/1900", txtBatchDate.Text))
                .AragingDate = ConvertDate2("02/09/1976")
                .RealizedDate = ConvertDate2("02/09/1976")
                '.InstallmentDueDate = ConvertDate2(txtDueDate.Text)
                '.InstallmentDueDate = ConvertDate2(IIf(txtDueDate.Text.Trim = "", ConvertDate2(Me.BusinessDate), txtDueDate.Text)) 
                .InstallmentDueDate = ConvertDate2(IIf(txtPeriodTo.Text.Trim = "", "01/01/1900", txtPeriodTo.Text))
                .AccProposedNum = 0
                .AccRealizedNum = 0
                .OSAmtToFunCoy = CDec(txtAmount.Text.Trim)
                .AssetDocLocation = drdBPKBLocation.SelectedItem.Value
                .BankAccountID = cboBank.SelectedValue.Trim
                .FirstInstallment = ddlFirstInstallment.SelectedValue.Trim
                .isFundingNTF = ddlIsFundingNTF.SelectedValue.Trim
                .perhitunganBunga = ddlHitungBUnga.SelectedValue.Trim
                .CaraBayar = ddlCaraPembayaran.SelectedValue.Trim
                .PeriodeBulan = txtPeriodeBulan.Text.Trim
                .PeriodFrom = ConvertDate2(IIf(txtPeriodFrom.Text.Trim = "", "01/01/1900", txtPeriodFrom.Text))
                .PeriodTo = ConvertDate2(IIf(txtPeriodTo.Text.Trim = "", "01/01/1900", txtPeriodTo.Text))
                .ListData2 = simpatRate()
            End With

            m_Company.EditFundingContractBatch(customClass)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region
#Region "View"

#Region "EditView"
    Private Sub editView()
        Dim dt As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging



        fillcboBank()
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractBatchById"
        End With
        Try


            oContract = cContract.GetGeneralPaging(oContract)
            dt = oContract.ListData
            txtBatchNo.Text = CStr(dt.Rows(0).Item("FundingBatchNo")).Trim
            txtBatchDate.Text = CDate(dt.Rows(0).Item("BatchDate")).ToString("dd/MM/yyyy")
            txtAmount.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
            txtInterestRate.Text = CStr(dt.Rows(0).Item("InterestRate")).Trim
            Me.InterestRate = CStr(dt.Rows(0).Item("InterestRate")).Trim
            drdInterestType.SelectedIndex = drdInterestType.Items.IndexOf(drdInterestType.Items.FindByValue(CStr(dt.Rows(0).Item("InterestType"))))
            'txtFinalMaturityDate.Text = CDate(dt.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            txtPeriodTo.Text = CDate(dt.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            drdPaymentScheme.SelectedIndex = drdPaymentScheme.Items.IndexOf(drdPaymentScheme.Items.FindByValue(CStr(dt.Rows(0).Item("PaymentScheme"))))
            txtProvisionAmount.Text = CStr(dt.Rows(0).Item("ProvisionFeeAmount")).Trim
            txtAdminAmount.Text = CStr(dt.Rows(0).Item("AdminAmount")).Trim
            drdCurrency.SelectedIndex = drdCurrency.Items.IndexOf(drdCurrency.Items.FindByValue(CStr(dt.Rows(0).Item("CurrencyId"))))
            txtExchangeRate.Text = CStr(dt.Rows(0).Item("ExchangeRate")).Trim
            'txtTenor.Text = CStr(dt.Rows(0).Item("Tenor")).Trim
            drdInsPeriod.SelectedIndex = drdInsPeriod.Items.IndexOf(drdInsPeriod.Items.FindByValue(CStr(dt.Rows(0).Item("InstallmentPeriod"))))
            drdInsScheme.SelectedIndex = drdInsScheme.Items.IndexOf(drdInsScheme.Items.FindByValue(CStr(dt.Rows(0).Item("InstallmentScheme"))))
            drdBPKBLocation.SelectedIndex = drdBPKBLocation.Items.IndexOf(drdBPKBLocation.Items.FindByValue(CStr(dt.Rows(0).Item("AssetDocLocation"))))
            cboBank.SelectedIndex = cboBank.Items.IndexOf(cboBank.Items.FindByValue(CStr(dt.Rows(0).Item("BankAccountID")).Trim))

            ddlFirstInstallment.SelectedIndex = ddlFirstInstallment.Items.IndexOf(ddlFirstInstallment.Items.FindByValue(CStr(dt.Rows(0).Item("FirstInstallment"))))
            ddlIsFundingNTF.SelectedValue = CInt(dt.Rows(0).Item("isFundingNTF"))
            ddlHitungBUnga.SelectedIndex = ddlHitungBUnga.Items.IndexOf(ddlHitungBUnga.Items.FindByValue(CStr(dt.Rows(0).Item("perhitunganBunga"))))
            ddlCaraPembayaran.SelectedIndex = ddlCaraPembayaran.Items.IndexOf(ddlCaraPembayaran.Items.FindByValue(IIf(IsDBNull(dt.Rows(0).Item("CaraBayar")), "", dt.Rows(0).Item("CaraBayar").ToString.Trim)))

            txtPeriodeBulan.Text = CStr(dt.Rows(0).Item("LamaDlmBulan")).Trim
            txtPeriodFrom.Text = CDate(dt.Rows(0).Item("PeriodFrom")).ToString("dd/MM/yyyy")

            If IsDBNull(dt.Rows(0).Item("InstallmentDueDate")) Then
                txtDueDate.Text = CDate(DateAdd(DateInterval.Month, 1, ConvertDate2(txtBatchDate.Text))).ToString("dd/MM/yyyy")
            Else
                txtDueDate.Text = CDate(dt.Rows(0).Item("InstallmentDueDate")).ToString("dd/MM/yyyy")
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


        Dim ctrContract As New FundingCompanyController
        Dim ojbContract As New Parameter.FundingContractBatch

        With ojbContract
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
        End With

        ojbContract = ctrContract.FundingTotalSelected(ojbContract)
        dt = ojbContract.Listdata
        If dt.Rows.Count > 0 Then
            lblTotalAccount.Text = CStr(dt.Rows(0).Item("TotalAccount"))
        Else
            lblTotalAccount.Text = 0
        End If


        bindRate()
    End Sub
#End Region

#Region "JustView"
    Private Sub JustView()
        Dim dtvEntity As DataView
        Dim dt As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        pnlTop.Visible = True
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        pnlView.Visible = True

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractBatchById"
        End With
        Try
            oContract = cContract.GetGeneralPaging(oContract)
            dt = oContract.ListData
            lblBatchNoV.Text = CStr(dt.Rows(0).Item("FundingBatchNo")).Trim
            lblAmountV.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmtToFunCoy"), 0).Trim
            lblDateV.Text = CDate(dt.Rows(0).Item("BatchDate")).ToString("dd/MM/yyyy")
            lblInterestRateV.Text = FormatNumber(dt.Rows(0).Item("InterestRate"), 0).Trim & "%"
            lblFinalMaturityDateV.Text = CDate(dt.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            lblProvisionAmountV.Text = FormatNumber(dt.Rows(0).Item("ProvisionFeeAmount"), 0).Trim
            lblAdminAmountV.Text = FormatNumber(dt.Rows(0).Item("AdminAmount"), 0).Trim



        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub
#End Region

#End Region
    Private Function updateAgreementExecution() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Dim DrawDownAmount, PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal
        Dim InsAmount() As Decimal
        Dim InterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal

        'Try
        With customClass
            .strConnection = GetConnectionString()
            .BankId = Me.BankID
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
        End With

        If Me.StatusDrawn = "D" Then
            m_Company.FundingUpdateAgreementSecondExecution(customClass)
        Else
            m_Company.UpdateFundingAgreementExecution(customClass)
        End If

        'Tenor = CDec(txtTenor.Text)
        Tenor = Me.TenorMax

        If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
            MonthPeriod = 6
            Tenor = Math.Round(Tenor / 2, 0)
        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
            MonthPeriod = 4
            Tenor = Math.Round(Tenor / 3, 0)
        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
            MonthPeriod = 2
            Tenor = Math.Round(Tenor / 6, 0)
        Else
            MonthPeriod = 12
        End If

        ReDim PrincipalAmount(CInt(Tenor))
        ReDim InterestAmount(CInt(Tenor))
        ReDim OSPrincipalAmount(CInt(Tenor))
        ReDim InsAmount(CInt(Tenor))
        ReDim OSInterestAmount(CInt(Tenor))

        If Me.FacilityKind = "JFINC" Then
            txtAmount.Text = getDrawdownAmount()
        End If

        DrawDownAmount = CDec(txtAmount.Text)
        InterestRate = CDec(txtInterestRate.Text)
        InterestRate = Me.InterestRate
        Batchdate = ConvertDate2(txtBatchDate.Text)

        If drdPaymentScheme.SelectedItem.Value = "1" Or drdPaymentScheme.SelectedItem.Value = "2" Or drdPaymentScheme.SelectedItem.Value = "6" Then
            iDueDate = ConvertDate2(txtBatchDate.Text)
            iDueDate = DateAdd(DateInterval.Month, 1, iDueDate)
        ElseIf drdPaymentScheme.SelectedItem.Value = "3" Then
            iDueDate = ConvertDate2(txtDueDate.Text)
        ElseIf drdPaymentScheme.SelectedItem.Value = "4" Then
            iDueDate = ConvertDate2(txtDueDate.Text)
            Select Case Month(iDueDate)
                Case 1
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 2
                    If (Year(txtDueDate.Text) Mod 4) = 0 Then
                        greaterDay = CStr(Month(iDueDate)).Trim + "/29/" + CStr(Year(iDueDate)).Trim
                    Else
                        greaterDay = CStr(Month(iDueDate)).Trim + "/28/" + CStr(Year(iDueDate)).Trim
                    End If
                Case 3
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 4
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 5
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 6
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 7
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 8
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 9
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 10
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 11
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 12
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
            End Select
            iDueDate = CDate(greaterDay.Trim)
        End If

        InsScheme = drdInsScheme.SelectedItem.Value

        If Tenor > 0 Then
            If InsScheme = "A" Then
                InsAmount(0) = Math.Round(Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, 0), 0)
                TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount

                If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                    InterestAmount(0) = Math.Round(CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount)), 0)
                Else
                    InterestAmount(0) = Math.Round((DrawDownAmount * InterestRate) / (MonthPeriod * 100), 0)
                End If

                PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 0)
                OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                For i = 1 To Tenor - 1
                    InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                    PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                    OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                    OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                Next

            ElseIf InsScheme = "P" Then
                TotalInterest = 0
                PrincipalAmount(0) = DrawDownAmount / Tenor
                If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                    InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount))
                    'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(InterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                Else
                    InterestAmount(0) = (DrawDownAmount * InterestRate) / (MonthPeriod * 100)
                End If
                InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                TotalInterest = InterestAmount(0)
                For i = 1 To Tenor - 1
                    InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                    InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                    OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                    TotalInterest += InterestAmount(i)
                Next
                OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                For i = 1 To Tenor - 1
                    OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                Next

            ElseIf InsScheme = "I" Then
                TotalInterest = 0
                PrincipalAmount(0) = 0
                InterestAmount(0) = 0
                InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                OSPrincipalAmount(0) = 0
                TotalInterest = InterestAmount(0)
                For i = 1 To Tenor - 1
                    InterestAmount(i) = 0
                    InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                    OSPrincipalAmount(i) = 0
                Next
                OSInterestAmount(0) = 0
                For i = 1 To Tenor - 1
                    OSInterestAmount(i) = 0
                Next
            End If

            'Write To Database Installment
            j = 0
            For i = 0 To Tenor - 1

                With customClass
                    .strConnection = GetConnectionString()
                    .BankId = Me.BankID
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .InsSecNo = i + 1

                    If i = 0 Then
                        .DueDate = iDueDate
                    Else
                        .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                    End If

                    If InsScheme = "A" Then
                        .PrincipalAmount = PrincipalAmount(i)
                    Else
                        .PrincipalAmount = PrincipalAmount(0)
                    End If

                    .InterestAmount = InterestAmount(i)
                    .PrincipalPaidAmount = 0
                    .InterestPaidAmount = 0
                    .OSPrincipalAmount = OSPrincipalAmount(i)
                    .OSInterestAmount = OSInterestAmount(i)
                End With

                m_Company.AddFundingContractBatchIns(customClass)

                If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                    j = j + 2
                ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                    j = j + 3
                ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                    j = j + 6
                Else
                    j = i + 1
                End If
            Next

        End If

        ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
        Return True
        'Catch ex As Exception
        '    ShowMessage(lblMessage, ex.Message, True)
        '    Return False
        'End Try

    End Function
    Private Function updateAgreementExecution3() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim dt As DataTable
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Dim DrawDownAmount, PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal
        Dim InsAmount() As Decimal
        Dim InterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With
            If Me.StatusDrawn = "D" Then
                m_Company.FundingUpdateAgreementSecondExecution(customClass)
            Else
                m_Company.UpdateFundingAgreementExecution(customClass)
            End If

            'Tenor = CDec(txtTenor.Text)
            Tenor = 0

            If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                MonthPeriod = 6
                Tenor = Math.Round(Tenor / 2, 0)
            ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                MonthPeriod = 4
                Tenor = Math.Round(Tenor / 3, 0)
            ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                MonthPeriod = 2
                Tenor = Math.Round(Tenor / 6, 0)
            Else
                MonthPeriod = 12
            End If
            ReDim PrincipalAmount(CInt(Tenor))
            ReDim InterestAmount(CInt(Tenor))
            ReDim OSPrincipalAmount(CInt(Tenor))
            ReDim InsAmount(CInt(Tenor))
            ReDim OSInterestAmount(CInt(Tenor))

            DrawDownAmount = CDec(txtAmount.Text)
            'InterestRate = CDec(txtInterestRate.Text)
            InterestRate = 0
            Batchdate = ConvertDate2(txtBatchDate.Text)
            If drdPaymentScheme.SelectedItem.Value = "1" Or drdPaymentScheme.SelectedItem.Value = "2" Or drdPaymentScheme.SelectedItem.Value = "6" Then
                iDueDate = ConvertDate2(txtBatchDate.Text)
                iDueDate = DateAdd(DateInterval.Month, 1, iDueDate)
            ElseIf drdPaymentScheme.SelectedItem.Value = "3" Then
                iDueDate = ConvertDate2(txtDueDate.Text)
            ElseIf drdPaymentScheme.SelectedItem.Value = "4" Then
                iDueDate = ConvertDate2(txtDueDate.Text)
                Select Case Month(iDueDate)
                    Case 1
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 2
                        If (Year(txtDueDate.Text) Mod 4) = 0 Then
                            greaterDay = CStr(Month(iDueDate)).Trim + "/29/" + CStr(Year(iDueDate)).Trim
                        Else
                            greaterDay = CStr(Month(iDueDate)).Trim + "/28/" + CStr(Year(iDueDate)).Trim
                        End If
                    Case 3
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 4
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 5
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 6
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 7
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 8
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 9
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 10
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 11
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 12
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                End Select
                iDueDate = CDate(greaterDay.Trim)
            End If

            InsScheme = drdInsScheme.SelectedItem.Value
            If Tenor > 0 Then
                If InsScheme = "A" Then
                    InsAmount(0) = Math.Round(Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, 0), 0)
                    TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount
                    If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                        InterestAmount(0) = Math.Round(CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount)), 0)

                    Else
                        InterestAmount(0) = Math.Round((DrawDownAmount * InterestRate) / (MonthPeriod * 100), 0)
                    End If

                    PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 0)
                    OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                    OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                    For i = 1 To Tenor - 1
                        InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                        PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                    Next


                ElseIf InsScheme = "P" Then
                    TotalInterest = 0
                    PrincipalAmount(0) = DrawDownAmount / Tenor
                    If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                        InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount))
                        'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(InterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                    Else
                        InterestAmount(0) = (DrawDownAmount * InterestRate) / (MonthPeriod * 100)
                    End If
                    InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                    OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                    TotalInterest = InterestAmount(0)
                    For i = 1 To Tenor - 1
                        InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                        InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                        TotalInterest += InterestAmount(i)
                    Next
                    OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                    For i = 1 To Tenor - 1
                        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                    Next
                ElseIf InsScheme = "I" Then
                    TotalInterest = 0
                    PrincipalAmount(0) = 0
                    InterestAmount(0) = 0
                    InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                    OSPrincipalAmount(0) = 0
                    TotalInterest = InterestAmount(0)
                    For i = 1 To Tenor - 1
                        InterestAmount(i) = 0
                        InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                        OSPrincipalAmount(i) = 0
                    Next
                    OSInterestAmount(0) = 0
                    For i = 1 To Tenor - 1
                        OSInterestAmount(i) = 0
                    Next
                End If

                'Write To Database Installment
                j = 0
                For i = 0 To Tenor - 1

                    With customClass
                        .strConnection = GetConnectionString()
                        .BankId = Me.BankID
                        .FundingCoyId = Me.CompanyID
                        .FundingContractNo = Me.FundingContractNo
                        .FundingBatchNo = Me.FundingBatchNo
                        .InsSecNo = i + 1
                        If i = 0 Then
                            .DueDate = iDueDate
                        Else
                            .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                        End If
                        If InsScheme = "A" Then
                            .PrincipalAmount = PrincipalAmount(i)
                        Else
                            .PrincipalAmount = PrincipalAmount(0)
                        End If
                        .InterestAmount = InterestAmount(i)
                        .PrincipalPaidAmount = 0
                        .InterestPaidAmount = 0
                        .OSPrincipalAmount = OSPrincipalAmount(i)
                        .OSInterestAmount = OSInterestAmount(i)
                    End With

                    m_Company.AddFundingContractBatchIns(customClass)

                    If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                        j = j + 2
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                        j = j + 3
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                        j = j + 6
                    Else
                        j = i + 1
                    End If
                Next

            End If
            ShowMessage(lblMessage, "Eksekusi Berhasil ", False)

            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try

    End Function
    Private Function updateAgreementExecutionAD() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim dt As DataTable

        Dim DrawDownAmount As Decimal

        Dim InsAmount(), PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal

        Dim InterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            m_Company.UpdateFundingAgreementExecution(customClass)

            'Tenor = CDec(txtTenor.Text)            
            Tenor = Me.TenorMax

            If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                MonthPeriod = 6
                Tenor = Math.Round(Tenor / 2, 0)
            ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                MonthPeriod = 4
                Tenor = Math.Round(Tenor / 3, 0)
            ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                MonthPeriod = 2
                Tenor = Math.Round(Tenor / 6, 0)
            Else
                'Installment Period, monthly
                MonthPeriod = 12
            End If

            ReDim PrincipalAmount(CInt(Tenor))
            ReDim InterestAmount(CInt(Tenor))
            ReDim OSPrincipalAmount(CInt(Tenor))
            ReDim InsAmount(CInt(Tenor))
            ReDim OSInterestAmount(CInt(Tenor))

            If Me.FacilityKind = "JFINC" Then
                txtAmount.Text = getDrawdownAmount()
            End If

            DrawDownAmount = CDec(txtAmount.Text)
            'InterestRate = CDec(txtInterestRate.Text)
            InterestRate = 0
            Batchdate = ConvertDate2(txtBatchDate.Text)

            'Payment Scheme, Principal & Interest on Drawdown Date (1)
            If drdPaymentScheme.SelectedItem.Value = "1" Or drdPaymentScheme.SelectedItem.Value = "2" Or drdPaymentScheme.SelectedItem.Value = "6" Then
                iDueDate = ConvertDate2(txtBatchDate.Text)
            ElseIf drdPaymentScheme.SelectedItem.Value = "3" Then
                iDueDate = ConvertDate2(txtDueDate.Text)
            ElseIf drdPaymentScheme.SelectedItem.Value = "4" Then
                iDueDate = ConvertDate2(txtDueDate.Text)
                Select Case Month(iDueDate)
                    Case 1
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 2
                        If (Year(txtDueDate.Text) Mod 4) = 0 Then
                            greaterDay = CStr(Month(iDueDate)).Trim + "/29/" + CStr(Year(iDueDate)).Trim
                        Else
                            greaterDay = CStr(Month(iDueDate)).Trim + "/28/" + CStr(Year(iDueDate)).Trim
                        End If
                    Case 3
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 4
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 5
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 6
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 7
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 8
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 9
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 10
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 11
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 12
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                End Select
                iDueDate = CDate(greaterDay.Trim)
            End If

            InsScheme = drdInsScheme.SelectedItem.Value

            If Tenor > 0 Then
                'Fixed amount
                If InsScheme = "A" Then
                    InsAmount(0) = Math.Round(Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, DueDate.BegOfPeriod), 2)
                    TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount

                    If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                        InterestAmount(0) = Math.Round(CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount)), 0)
                    Else
                        'Payment scheme Principal & Interest on Drawdown Date (1)
                        InterestAmount(1) = Math.Round((DrawDownAmount * InterestRate) / (MonthPeriod * 100), 2)
                    End If

                    PrincipalAmount(0) = Math.Round(InsAmount(0), 2)
                    OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                    OSInterestAmount(0) = TotalInterest

                    For i = 1 To Tenor - 1
                        InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                        PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                    Next

                ElseIf InsScheme = "P" Then
                    TotalInterest = 0
                    PrincipalAmount(0) = DrawDownAmount / Tenor
                    If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                        InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount))
                        'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(InterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                    Else
                        InterestAmount(0) = (DrawDownAmount * InterestRate) / (MonthPeriod * 100)
                    End If
                    InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                    OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                    TotalInterest = InterestAmount(0)
                    For i = 1 To Tenor - 1
                        InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                        InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                        TotalInterest += InterestAmount(i)
                    Next
                    OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                    For i = 1 To Tenor - 1
                        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                    Next

                ElseIf InsScheme = "I" Then
                    TotalInterest = 0
                    PrincipalAmount(0) = 0
                    InterestAmount(0) = 0
                    InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                    OSPrincipalAmount(0) = 0
                    TotalInterest = InterestAmount(0)
                    For i = 1 To Tenor - 1
                        InterestAmount(i) = 0
                        InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                        OSPrincipalAmount(i) = 0
                    Next
                    OSInterestAmount(0) = 0
                    For i = 1 To Tenor - 1
                        OSInterestAmount(i) = 0
                    Next
                End If

                'Write To Database Installment
                j = 0
                For i = 0 To Tenor - 1

                    With customClass
                        .strConnection = GetConnectionString()
                        .BankId = Me.BankID
                        .FundingCoyId = Me.CompanyID
                        .FundingContractNo = Me.FundingContractNo
                        .FundingBatchNo = Me.FundingBatchNo
                        .InsSecNo = i + 1

                        If i = 0 Then
                            .DueDate = iDueDate
                        Else
                            .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                        End If

                        If InsScheme = "A" Then
                            .PrincipalAmount = PrincipalAmount(i)
                        Else
                            .PrincipalAmount = PrincipalAmount(0)
                        End If

                        .InterestAmount = InterestAmount(i)
                        .PrincipalPaidAmount = 0
                        .InterestPaidAmount = 0
                        .OSPrincipalAmount = OSPrincipalAmount(i)
                        .OSInterestAmount = OSInterestAmount(i)
                    End With

                    m_Company.AddFundingContractBatchIns(customClass)

                    If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                        j = j + 2
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                        j = j + 3
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                        j = j + 6
                    Else
                        j = i + 1
                    End If
                Next

            End If
            ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
#Region "MISC"

#Region "ClearAddForm"
    Private Sub ClearAddForm()
        txtAdminAmount.Text = 0
        txtAmount.Text = 0
        txtExchangeRate.Text = 1
        'txtTenor.Text = "0"
        txtProvisionAmount.Text = 0
        'txtInterestRate.Text = 0
        drdCurrency.SelectedIndex = 0
        drdInsPeriod.SelectedIndex = 0
        drdInsScheme.SelectedIndex = 0
        drdInterestType.SelectedIndex = 0
        drdPaymentScheme.SelectedIndex = 0
        txtBatchDate.Text = ""
        'txtFinalMaturityDate.Text = ""
        txtPeriodFrom.Text = ""
        txtPeriodTo.Text = ""
        txtDueDate.Text = ""
        ddlCaraPembayaran.SelectedIndex = 0
    End Sub
#End Region

#Region "BindTitleTop"
    Private Sub bindTitleTop()
        lblBankName.Text = Me.BankName
        lblFundingCoyName.Text = Me.CompanyName
        lblFundingContractNo.Text = Me.FundingContractNo
        lblContractName.Text = Me.ContractName
    End Sub
#End Region

#End Region
#Region "ButtonClick"
    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        Dim dtvEntity As DataView
        Dim dt As DataTable
        Dim oCompany As New Parameter.FundingContract
        Dim m_Company As New FundingCompanyController

        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            bindTitleTop()
            ClearAddForm()
            fillcboBank()

            bindRate()
            'divDueDate.Visible = False
            'txtDueDate.Visible = True
            drdPaymentScheme_SelectedIndexChanged(sender, e)
            divTotalAccount.Visible = False
            div3Btn.Visible = False
            With oCompany
                .strConnection = GetConnectionString()
                .FundingContractNo = Me.FundingContractNo
            End With
            oCompany = m_Company.ListFundingContractByID(oCompany)
            dt = oCompany.ListData
            txtProvisionAmount.Text = CStr(dt.Rows(0).Item("ProvisionFeeAmount")).Trim
            txtAdminAmount.Text = CStr(dt.Rows(0).Item("AdminFeePerAccount")).Trim
            'txtFinalMaturityDate.Text = CDate(dt.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            txtPeriodTo.Text = CDate(dt.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            'ddlCaraPembayaran.SelectedIndex = ddlCaraPembayaran.Items.IndexOf(ddlCaraPembayaran.Items.FindByValue(dt.Rows(0).Item("CaraBayar")))
            drdPaymentScheme.SelectedIndex = drdPaymentScheme.Items.IndexOf(drdPaymentScheme.Items.FindByValue(dt.Rows(0).Item("PaymentScheme")))
            ddlCaraPembayaran.SelectedIndex = ddlCaraPembayaran.Items.IndexOf(ddlCaraPembayaran.Items.FindByValue(dt.Rows(0).Item("CaraBayar")))
            drdBPKBLocation.SelectedIndex = drdBPKBLocation.Items.IndexOf(drdBPKBLocation.Items.FindByValue(dt.Rows(0).Item("AssetDocLocation")))
            drdInterestType.SelectedIndex = drdInterestType.Items.IndexOf(drdInterestType.Items.FindByValue(dt.Rows(0).Item("InterestType")))
            'ddlCaraPembayaran.Enabled = False
            'drdPaymentScheme.Enabled = False
            'drdBPKBLocation.Enabled = False
            'drdInterestType.Enabled = False

            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "- ADD"
            pnlAddEdit.Visible = True
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
            Case "EDIT"
                PanelAllFalse()
                'If txtInterestRate.Text.Trim <> Me.InterestRate Then
                '    pnlList.Visible = False
                '    pnlAddEdit.Visible = False
                '    pnlView.Visible = True
                '    'Me.ActionAddEdit = "VIEW"
                '    lblMenuAddEdit.Text = "- EDIT CONFIRMATION"
                '    bindTitleTop()
                '    JustView()
                '    lblInterestRateV.Text = txtInterestRate.Text.Trim

                '    ShowMessage(lblMessage, "Suku Bunga berubah, Lanjutkan dengan Rescheduling", True)
                'Else
                '    edit()
                '    BindGridEntity(Me.SearchBy, Me.SortBy)
                '    pnlList.Visible = True
                'End If
                edit()
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
        End Select
    End Sub
    Private Sub btnAddAgreement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAgreement.Click
        CheckFundingContractSelectionTime()
        If Me.FlagCS = "B" Then

            ShowMessage(lblMessage, "Pemilihan Kontrak ini sebelum Loan ACT, Penambahan Kontrak Setelah Loan ACT tidak diperkenankan", True)
            Exit Sub
        Else
            CheckFacilityKind()
            CheckDrawDownStatus()
            If (Me.FacilityKind = "JFINC" Or Me.FacilityKind = "CHANN") And Me.StatusDrawn = "D" Then

                'ShowMessage(lblMessage, "Batch ini sudah diCairkan, Penambahan Kontrak tidak diperkenankan", True)
                ShowMessage(lblMessage, "Batch ini sudah dieksekusi, Penambahan Kontrak tidak diperkenankan", True)
                Exit Sub
            End If
        End If
        Response.Redirect("fundingagreement.aspx?Command=ADD&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&Tenor=0")
    End Sub

    Private Sub btnAgreementSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgreementSelection.Click
        CheckFacilityKind()
        CheckDrawDownStatus()
        If (Me.FacilityKind = "JFINC" Or Me.FacilityKind = "CHANN") And Me.StatusDrawn = "D" Then

            'ShowMessage(lblMessage, "Batch ini sudah diCairkan, Pemilihan Kontrak tidak diperkenankan", True)
            ShowMessage(lblMessage, "Batch ini sudah diEksekusi, Pemilihan Kontrak tidak diperkenankan", True)
            Exit Sub
        End If
        Response.Redirect("FundingAgreementSelected.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&Plafond=" & txtAmount.Text)

    End Sub
    Private Sub BtnCancelList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelList.Click
        Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
    End Sub
    Private Sub btnCancelbawah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancelbawah.Click
        Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub
#End Region
#Region "DTGFundingContractBatchProperties"
    Private Sub dtgFundingContractBatch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractBatch.ItemCommand
        Dim lnkBatchNo As HyperLink
        Dim Plafond As New Label
        Dim lblType As New Label
        Dim lblStatus As New Label
        Dim ImbExecute As ImageButton
        Dim lblBatchExecute As New Label
        Dim imbupload As ImageButton

        lnkBatchNo = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("lnkFundingBatchNo"), HyperLink)
        'Plafond = CType(e.Item.FindControl("lbljumlah"), Label)
        Plafond = CType(e.Item.FindControl("lblPlafoundReal"), Label)
        lblType = CType(e.Item.FindControl("lblType"), Label)
        lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
        ImbExecute = CType(e.Item.FindControl("ImbExecute"), ImageButton)
        lblBatchExecute = CType(e.Item.FindControl("lblBatchExecute"), Label)
        imbupload = CType(e.Item.FindControl("imbupload"), ImageButton)


        'If lblStatus.Text.Trim = "D" Then
        If lblBatchExecute.Text.Trim = "1" Then
            ImbExecute.Visible = False
        End If

        If lblType.Text.Trim <> "TLOAN" Then
            imbupload.Visible = False
        End If

        Me.FundingBatchNo = lnkBatchNo.Text

        Select Case e.CommandName
            Case "View"
            Case "Edit"

                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If


                    Me.FundingBatchNo = lnkBatchNo.Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    div3Btn.Visible = True
                    'divDueDate.Visible = True
                    'txtDueDate.Visible = True
                    drdPaymentScheme_SelectedIndexChanged(source, e)
                    divTotalAccount.Visible = True

                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "- EDIT"
                    editView()
                    CheckDrawDownStatus()
                    CheckFacilityKind()
                    EnabledWidgetControl()

                    'If Me.StatusDrawn = "D" Or drdInterestType.SelectedItem.Value.Trim = "X" Or drdInterestType.SelectedItem.Value.Trim = "B" Then
                    '    DisableWidgetControl()
                    'End If

                    If Me.StatusDrawn = "D" Then
                        DisableWidgetControl()
                    End If
                End If

            Case "Softcopy"
                GenSoftcopyDraft()


            Case "Selection"
                If (lblType.Text = "JFINC" Or lblType.Text = "CHANN") And lblStatus.Text = "Drawn" Then
                    'ShowMessage(lblMessage, "Batch ini sudah diCairkan, Pemilihan Kontrak tidak diperkenankan", True)
                    ShowMessage(lblMessage, "Batch ini sudah dieksekusi, Pemilihan Kontrak tidak diperkenankan", True)
                Else
                    'Modify by Wira 20170831, untuk membedakan JF dan TL
                    'Response.Redirect("FundingAgreementSelected.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&Plafond=" & Plafond.Text)
                    If (lblType.Text = "JFINC" Or lblType.Text = "CHANN") Then
                        Response.Redirect("FundingAgreementSelected.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&Plafond=" & Plafond.Text)
                    Else
                        Response.Redirect("FundingAgreementSelectedTL.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&Plafond=" & Plafond.Text)
                    End If
                End If

            Case "PengantarCair"
                Response.Redirect("FundingCetakPengantarCair.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractNo=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName)

            Case "upload"
                Response.Redirect("UploadFundingBatchInstallmentTL.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&Plafond=" & Plafond.Text)
            Case "TTB"
                'Response.Redirect("../../Webform.Reports.RDLC/Funding/TTB/FundingCreateTTBViewer.aspx?CompanyID=" & Me.CompanyID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo)
                Response.Write("<script>")
                Response.Write("window.open('../../Webform.Reports.RDLC/Funding/TTB/FundingCreateTTBViewer.aspx?CompanyID=" & Me.CompanyID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "','_blank')")
                Response.Write("</script>")
            Case "Nom"
                'Response.Redirect("../../Webform.Reports.RDLC/Funding/TTB/FundingCreateTTBViewer.aspx?CompanyID=" & Me.CompanyID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo)
                Response.Write("<script>")
                Response.Write("window.open('../../Webform.Reports.RDLC/Funding/Nominatif/FundingNominatifViewer.aspx?CompanyID=" & Me.CompanyID & "&FundingContractId=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "','_blank')")
                Response.Write("</script>")

            Case "Execute"
                Dim isProcessSuccess As Boolean
                Dim dt As New DataTable
                dt = GetDetailFundingBatch(Me.FundingBatchNo, Me.BankID, Me.CompanyID, Me.FundingContractNo)

                Me.InsPeriod = CStr(dt.Rows(0).Item("InstallmentPeriod"))
                Me.InsScheme = CStr(dt.Rows(0).Item("InstallmentScheme"))
                Me.PaymentScheme = CStr(dt.Rows(0).Item("PaymentScheme"))
                Me.StatusDrawn = CStr(dt.Rows(0).Item("Status"))
                Me.BatchDate = CDate(dt.Rows(0).Item("BatchDate"))
                Me.FundingBatchNo = CStr(dt.Rows(0).Item("FundingBatchNo"))
                Me.InterestType = CStr(dt.Rows(0).Item("InterestType"))
                Me.FinalMaturityDate = CDate(dt.Rows(0).Item("FinalMaturityDate"))
                Me.ProvisionAmount = CDbl(dt.Rows(0).Item("ProvisionFeeAmount"))
                Me.AdminAmount = CDbl(dt.Rows(0).Item("AdminAmount"))
                Me.CaraPembayaran = CStr(dt.Rows(0).Item("CaraBayar"))
                Me.HitungBUnga = CStr(dt.Rows(0).Item("perhitunganBunga"))
                Me.IsFundingNTF = CBool(dt.Rows(0).Item("IsFundingNTF"))
                Me.FirstInstallment = CStr(dt.Rows(0).Item("FirstInstallment"))
                Me.BankAccountID = CStr(dt.Rows(0).Item("BankAccountID"))
                Me.BPKBLocation = CStr(dt.Rows(0).Item("AssetDocLocation"))
                Me.Currency = CStr(dt.Rows(0).Item("CurrencyID"))
                Me.ExchangeRate = CDbl(dt.Rows(0).Item("ExchangeRate"))



                If IsDBNull(dt.Rows(0).Item("InstallmentDueDate")) Then
                    Me.DueDate_ = CDate(DateAdd(DateInterval.Month, 1, CDate(dt.Rows(0).Item("BatchDate"))))
                Else
                    Me.DueDate_ = CDate(dt.Rows(0).Item("InstallmentDueDate"))
                End If

                Me.Amount = CDbl(dt.Rows(0).Item("OSAmtToFunCoy"))

                edit()

                Select Case CStr(dt.Rows(0).Item("perhitunganBunga"))
                    Case "EFEKTIF"
                        If CStr(dt.Rows(0).Item("FirstInstallment")) = "AR" Then
                            isProcessSuccess = updateAgreementExecution4()
                        Else
                            ShowMessage(lblMessage, "Angsuran pertama harus Arrear!", True)
                            isProcessSuccess = False
                        End If

                    Case "ANUITAS"
                        If dt.Rows(0).Item("FirstInstallment") = "AR" Then
                            isProcessSuccess = updateAgreementExecution5()
                        Else
                            isProcessSuccess = updateAgreementExecutionAD()
                        End If
                    Case "FLAT"
                End Select

                Select Case CStr(dt.Rows(0).Item("perhitunganBunga"))
                    Case "ANUITAS"
                        If CStr(dt.Rows(0).Item("FirstInstallment")) = "AR" Then
                            isProcessSuccess = AddFundingAgreementInstallmentExecution()
                        Else
                            isProcessSuccess = AddFundingAgreementInstallmentExecutionAD()
                        End If

                    Case "EFEKTIF"
                        If CStr(dt.Rows(0).Item("FirstInstallment")) = "AR" Then
                            isProcessSuccess = genFAIEfektif()
                        Else
                            ShowMessage(lblMessage, "Angsuran pertama harus Arrear!", True)
                            isProcessSuccess = False
                        End If
                End Select

                UpdateDrawdownAmount()

                BindGridEntity(Me.SearchBy, Me.SortBy)
        End Select
    End Sub
    Private Function updateAgreementExecution5() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Dim DrawDownAmount, PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal
        Dim InsAmount() As Decimal
        Dim InterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal

        'Try
        With customClass
            .strConnection = GetConnectionString()
            .BankId = Me.BankID
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
        End With

        If Me.StatusDrawn = "D" Then
            m_Company.FundingUpdateAgreementSecondExecution(customClass)
        Else
            m_Company.UpdateFundingAgreementExecution(customClass)
        End If

        'Tenor = CDec(txtTenor.Text)
        Tenor = Me.TenorMax

        If Me.InsPeriod = "2" Then
            MonthPeriod = 6
            Tenor = Math.Round(Tenor / 2, 0)
        ElseIf Me.InsPeriod = "3" Then
            MonthPeriod = 4
            Tenor = Math.Round(Tenor / 3, 0)
        ElseIf Me.InsPeriod = "6" Then
            MonthPeriod = 2
            Tenor = Math.Round(Tenor / 6, 0)
        Else
            MonthPeriod = 12
        End If

        ReDim PrincipalAmount(CInt(Tenor))
        ReDim InterestAmount(CInt(Tenor))
        ReDim OSPrincipalAmount(CInt(Tenor))
        ReDim InsAmount(CInt(Tenor))
        ReDim OSInterestAmount(CInt(Tenor))

        'If Me.FacilityKind = "JFINC" Then
        '    txtAmount.Text = getDrawdownAmount()
        'End If

        If Me.FacilityKind = "JFINC" Then
            DrawDownAmount = getDrawdownAmount()
        Else
            DrawDownAmount = Me.Amount
        End If

        'DrawDownAmount =  CDec(txtAmount.Text)
        'InterestRate = CDec(txtInterestRate.Text)
        InterestRate = Me.InterestRate
        Batchdate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)

        If Me.PaymentScheme = "1" Or Me.PaymentScheme = "2" Or Me.PaymentScheme = "6" Then
            iDueDate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)
            iDueDate = DateAdd(DateInterval.Month, 1, iDueDate)
        ElseIf Me.PaymentScheme = "3" Then
            iDueDate = Me.DueDate_ ' ConvertDate2(txtDueDate.Text)
        ElseIf Me.PaymentScheme = "4" Then
            iDueDate = Me.DueDate_ 'ConvertDate2(txtDueDate.Text)
            Select Case Month(iDueDate)
                Case 1
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 2
                    If (Year(Me.DueDate_) Mod 4) = 0 Then
                        greaterDay = CStr(Month(iDueDate)).Trim + "/29/" + CStr(Year(iDueDate)).Trim
                    Else
                        greaterDay = CStr(Month(iDueDate)).Trim + "/28/" + CStr(Year(iDueDate)).Trim
                    End If
                Case 3
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 4
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 5
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 6
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 7
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 8
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 9
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 10
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 11
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 12
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
            End Select
            iDueDate = CDate(greaterDay.Trim)
        End If

        InsScheme = Me.InsScheme

        If Tenor > 0 Then
            If InsScheme = "A" Then
                InsAmount(0) = Math.Round(Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, 0), 0)
                TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount

                If Me.PaymentScheme = "4" Or Me.PaymentScheme = "3" Then
                    InterestAmount(0) = Math.Round(CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount)), 0)
                Else
                    InterestAmount(0) = Math.Round((DrawDownAmount * InterestRate) / (MonthPeriod * 100), 0)
                End If

                PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 0)
                OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                For i = 1 To Tenor - 1
                    InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                    PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                    OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                    OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                Next

            ElseIf InsScheme = "P" Then
                TotalInterest = 0
                PrincipalAmount(0) = DrawDownAmount / Tenor
                If Me.PaymentScheme = "4" Or Me.PaymentScheme = "3" Then
                    InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount))
                    'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(InterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                Else
                    InterestAmount(0) = (DrawDownAmount * InterestRate) / (MonthPeriod * 100)
                End If
                InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                TotalInterest = InterestAmount(0)
                For i = 1 To Tenor - 1
                    InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                    InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                    OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                    TotalInterest += InterestAmount(i)
                Next
                OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                For i = 1 To Tenor - 1
                    OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                Next

            ElseIf InsScheme = "I" Then
                TotalInterest = 0
                PrincipalAmount(0) = 0
                InterestAmount(0) = 0
                InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                OSPrincipalAmount(0) = 0
                TotalInterest = InterestAmount(0)
                For i = 1 To Tenor - 1
                    InterestAmount(i) = 0
                    InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                    OSPrincipalAmount(i) = 0
                Next
                OSInterestAmount(0) = 0
                For i = 1 To Tenor - 1
                    OSInterestAmount(i) = 0
                Next
            End If

            'Write To Database Installment
            j = 0
            For i = 0 To Tenor - 1

                With customClass
                    .strConnection = GetConnectionString()
                    .BankId = Me.BankID
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .InsSecNo = i + 1

                    If i = 0 Then
                        .DueDate = iDueDate
                    Else
                        .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                    End If

                    If InsScheme = "A" Then
                        .PrincipalAmount = PrincipalAmount(i)
                    Else
                        .PrincipalAmount = PrincipalAmount(0)
                    End If

                    .InterestAmount = InterestAmount(i)
                    .PrincipalPaidAmount = 0
                    .InterestPaidAmount = 0
                    .OSPrincipalAmount = OSPrincipalAmount(i)
                    .OSInterestAmount = OSInterestAmount(i)
                End With

                m_Company.AddFundingContractBatchIns(customClass)

                If Me.InsPeriod = "2" Then
                    j = j + 2
                ElseIf Me.InsPeriod = "3" Then
                    j = j + 3
                ElseIf Me.InsPeriod = "6" Then
                    j = j + 6
                Else
                    j = i + 1
                End If
            Next

        End If

        ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
        Return True
        'Catch ex As Exception
        '    ShowMessage(lblMessage, ex.Message, True)
        '    Return False
        'End Try

    End Function
    Private Function updateAgreementExecution4() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            CheckDrawDownStatus()

            If Me.StatusDrawn = "D" Then
                m_Company.FundingUpdateAgreementSecondExecution(customClass)
            Else
                m_Company.UpdateFundingAgreementExecution(customClass)
            End If


            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .InterestRate = Me.InterestRate
                .Tenor = Me.TenorMax
                .DueDate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)
                If Me.FacilityKind = "JFINC" Then
                    .PrincipalAmount = getDrawdownAmount()
                Else
                    .PrincipalAmount = Me.Amount
                End If
            End With

            m_Company.AddFundingContractBatchIns2(customClass)

            ShowMessage(lblMessage, "Eksekusi-2 Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Function GetDetailFundingBatch(ByVal FundingBatchNo As String, ByVal BankID As String, ByVal CompanyID As String, ByVal FundingContractNo As String) As DataTable
        Dim dt As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " FundingBatch.FundingBatchNo='" & FundingBatchNo & "' and FundingBatch.BankId='" & BankID & "' and FundingBatch.FundingCoyId='" & CompanyID & "' and FundingBatch.FundingContractNo='" & FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractBatchById"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        dt = oContract.ListData
        Return dt
    End Function

    Private Sub dtgFundingContractBatch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractBatch.ItemDataBound
        Dim lbl1, lblStatus, lbljumlah As Label
        Dim imbEdit, imbSpc As ImageButton
        Dim imbDraft As ImageButton
        Dim hyBatchNo As HyperLink
        Dim ImbExecute As ImageButton
        Dim lblBatchExecute As Label
        Dim ImbSelection As ImageButton
        Dim imbupload As ImageButton
        Dim imbttb, imbNom As ImageButton
        Dim lblType As Label

        If e.Item.ItemIndex >= 0 Then
            lbl1 = CType(e.Item.FindControl("lbl1"), Label)
            lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lbljumlah = CType(e.Item.FindControl("lbljumlah"), Label)

            lbljumlah.Text = FormatNumber(lbljumlah.Text, 0)

            imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)
            imbSpc = CType(e.Item.FindControl("imbSpc"), ImageButton)
            imbDraft = CType(e.Item.FindControl("imbDraft"), ImageButton)
            hyBatchNo = CType(e.Item.FindControl("lnkFundingBatchNo"), HyperLink)
            ImbExecute = CType(e.Item.FindControl("ImbExecute"), ImageButton)
            lblBatchExecute = CType(e.Item.FindControl("lblBatchExecute"), Label)
            ImbSelection = CType(e.Item.FindControl("ImbSelection"), ImageButton)
            imbupload = CType(e.Item.FindControl("imbupload"), ImageButton)
            imbttb = CType(e.Item.FindControl("imbttb"), ImageButton)
            imbNom = CType(e.Item.FindControl("imbNom"), ImageButton)
            lblType = CType(e.Item.FindControl("lblType"), Label)

            hyBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & hyBatchNo.Text.Trim & "')"
            If lbl1.Text.Trim = "L" Then
                lbl1.Text = "Floating"
            ElseIf lbl1.Text.Trim = "X" Then
                lbl1.Text = "Fixed"
            Else
                lbl1.Text = "Fixed Per Batch"
            End If

            If lblStatus.Text.Trim = "P" Then
                lblStatus.Text = "Paid"
                imbEdit.Visible = False
            ElseIf lblStatus.Text.Trim = "D" Then
                lblStatus.Text = "Drawn"
                'imbSpc.Visible = False
                'ImbExecute.Visible = False
            Else
                lblStatus.Text = "Selecting"
            End If

            If lblBatchExecute.Text.Trim = "1" Then
                ImbExecute.Visible = False
                imbEdit.Visible = False
                ImbSelection.Visible = False
                imbttb.Visible = True
                imbNom.Visible = True
            End If

            If lblType.Text.Trim <> "TLOAN" Then
                imbupload.Visible = False
            End If

        End If
    End Sub
#End Region
#Region "ExecutionClick"
    Private Sub btnExecution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExecution.Click
        Dim isProcessSuccess As Boolean

        'sebelum execution hitung principal amount dari selected agreement
        If Me.FacilityKind = "JFINC" Then
            txtAmount.Text = getDrawdownAmount()
        End If

        isProcessSuccess = False
        PanelAllFalse()
        CheckDrawDownStatus()

        If Me.StatusDrawn.Trim = "D" Then
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            ShowMessage(lblMessage, "Batch ini Sudah dicairkan", True)
            Exit Sub
        End If

        'save batch
        edit()

        'If ddlCaraPembayaran.SelectedValue = "BATCH" Then
        'Generate funding batch installment berdasarkan tipe perhitungan bunga
        Select Case ddlHitungBUnga.SelectedValue.Trim
            'Case "MENURUN" 'efektif
            '    isProcessSuccess = 22()
            'Case "TERM LOAN"
            '    isProcessSuccess = updateAgreementExecution3()
            'Case Else
            '    'anuitas modified
            '    isProcessSuccess = updateAgreementExecutionKMK()

            Case "EFEKTIF"
                If ddlFirstInstallment.SelectedValue = "AR" Then
                    isProcessSuccess = updateAgreementExecution2()
                Else
                    'tidak ada rumus untuk metode efektif advance, tampilkan warning
                    'isProcessSuccess = updateAgreementExecution2()
                    ShowMessage(lblMessage, "Angsuran pertama harus Arrear!", True)
                    isProcessSuccess = False
                End If

            Case "ANUITAS"
                If ddlFirstInstallment.SelectedValue = "AR" Then
                    isProcessSuccess = updateAgreementExecution()
                Else
                    isProcessSuccess = updateAgreementExecutionAD()
                End If
            Case "FLAT"
                'belum ada
        End Select
        'Else
        'Generate funding agreement installment berdasarkan tipe perhitungan bunga
        Select Case ddlHitungBUnga.SelectedValue.Trim
            'Case "MENURUN"
            '    Throw New Exception("Perhitungan funding agreement installment untuk tipe bunga menurun belum tersedia.")
            'Case "TERM LOAN"
            '    Throw New Exception("Perhitungan funding agreement installment untuk tipe bunga term loan belum tersedia.")
            'Case Else
            '    'anuitas
            '    isProcessSuccess = AddFundingAgreementInstallmentExecutionKMK()

            Case "ANUITAS"
                If ddlFirstInstallment.SelectedValue = "AR" Then
                    isProcessSuccess = AddFundingAgreementInstallmentExecution()
                Else
                    isProcessSuccess = AddFundingAgreementInstallmentExecutionAD()
                End If

            Case "EFEKTIF"
                If ddlFirstInstallment.SelectedValue = "AR" Then
                    isProcessSuccess = genFAIEfektif()
                Else
                    'tidak ada rumus untuk metode efektif advance, tampilkan warning
                    'isProcessSuccess = updateAgreementExecution2()
                    ShowMessage(lblMessage, "Angsuran pertama harus Arrear!", True)
                    isProcessSuccess = False
                End If
        End Select
        'End If

        If isProcessSuccess = False Then
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            Exit Sub
        Else
            isProcessSuccess = False
        End If

        ' sementara proses di bawah ini dikomen karena tidak proses miroring untuk JFINC dan CHANN
        ' isProcessSuccess = RollAllFundingBatchInstallment()

        'If isProcessSuccess = False Then
        '    lblMessage.Visible = "True"
        '    BindGridEntity(Me.SearchBy, Me.SortBy)
        '    pnlList.Visible = True
        '    Exit Sub
        'Else
        '    isProcessSuccess = False
        'End If

        UpdateDrawdownAmount()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub
#End Region
#Region "updateAgreementExecutionInterestOnly"
    Private Function updateAgreementExecutionInterestOnly() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim dt As DataTable
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Dim DrawDownAmount, PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal
        Dim InsAmount() As Decimal
        Dim InterestRate As Decimal
        Dim Tenor As Decimal
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim InsScheme As Char
        Dim MonthPeriod As Decimal

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            m_Company.UpdateFundingAgreementExecution(customClass)


            'Tenor = CDec(txtTenor.Text)
            Tenor = 0

            If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                MonthPeriod = 6
                Tenor = Math.Round(Tenor / 2, 0)
            ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                MonthPeriod = 4
                Tenor = Math.Round(Tenor / 3, 0)
            ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                MonthPeriod = 2
                Tenor = Math.Round(Tenor / 6, 0)
            Else
                MonthPeriod = 12
            End If
            ReDim PrincipalAmount(CInt(Tenor))
            ReDim InterestAmount(CInt(Tenor))
            ReDim OSPrincipalAmount(CInt(Tenor))
            ReDim InsAmount(CInt(Tenor))
            ReDim OSInterestAmount(CInt(Tenor))

            If Me.FacilityKind = "JFINC" Then
                txtAmount.Text = getDrawdownAmount()
            End If


            DrawDownAmount = CDec(txtAmount.Text)
            'InterestRate = CDec(txtInterestRate.Text)
            InterestRate = 0
            iDueDate = ConvertDate2(txtBatchDate.Text)
            iDueDate = DateAdd(DateInterval.Month, 1, iDueDate)
            InsScheme = drdInsScheme.SelectedItem.Value
            If Tenor > 0 Then
                If InsScheme = "A" Then
                    ''InsAmount(0) = Pmt(InterestRate / 1200, Tenor, -DrawDownAmount, 0, DueDate.EndOfPeriod)
                    InterestAmount(0) = CDec(DrawDownAmount) * CDec(InterestRate / 1200)
                    TotalInterest = CDec((InterestAmount(0) * Tenor))
                    InsAmount(0) = InterestAmount(0)
                    PrincipalAmount(0) = 0
                    OSPrincipalAmount(0) = CDec(DrawDownAmount - PrincipalAmount(0))
                    OSInterestAmount(0) = CDec(TotalInterest - InterestAmount(0))

                    For i = 1 To Tenor - 1
                        If i = Tenor - 1 Then
                            InterestAmount(i) = CDec(OSPrincipalAmount(i - 1)) * CDec(InterestRate / 1200)
                            PrincipalAmount(i) = DrawDownAmount
                            OSPrincipalAmount(i) = DrawDownAmount
                            OSInterestAmount(i) = CDec(OSInterestAmount(i - 1) - InterestAmount(i))
                            InsAmount(i) = InterestAmount(i)
                        Else
                            InterestAmount(i) = CDec(OSPrincipalAmount(i - 1)) * CDec(InterestRate / 1200)
                            PrincipalAmount(i) = 0
                            OSPrincipalAmount(i) = CDec(OSPrincipalAmount(i - 1) - PrincipalAmount(i))
                            OSInterestAmount(i) = CDec(OSInterestAmount(i - 1) - InterestAmount(i))
                            InsAmount(i) = InterestAmount(i)
                        End If
                    Next
                End If

                'Write To Database Installment
                j = 0
                For i = 0 To Tenor - 1
                    With customClass
                        .strConnection = GetConnectionString()
                        .BankId = Me.BankID
                        .FundingCoyId = Me.CompanyID
                        .FundingContractNo = Me.FundingContractNo
                        .FundingBatchNo = Me.FundingBatchNo
                        .InsSecNo = i + 1
                        If i = 0 Then
                            .DueDate = iDueDate
                        Else
                            .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                        End If
                        .PrincipalAmount = PrincipalAmount(i)
                        .InterestAmount = InterestAmount(i)
                        .PrincipalPaidAmount = 0
                        .InterestPaidAmount = 0
                        .OSPrincipalAmount = OSPrincipalAmount(i)
                        .OSInterestAmount = OSInterestAmount(i)
                    End With
                    m_Company.AddFundingContractBatchIns(customClass)
                    If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                        j = j + 2
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                        j = j + 3
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                        j = j + 6
                    Else
                        j = i + 1
                    End If
                Next
            End If

            ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try

    End Function
#End Region
#Region "AddFundingAgreementExecution"
    Public Function AddFundingAgreementInstallmentExecutionAD() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim FundingCoyPortion As Decimal
        Dim TenorAgr As Integer
        Dim Tenor As Integer

        Dim Beginingbalance As Decimal
        Dim SecPercentage As Decimal
        Dim SecCoverType As String
        Dim OutStandingPrincipal As Decimal
        Dim ARAgreement As Decimal
        Dim TotalOTR As Decimal
        Dim MonthPeriod As Integer
        Dim TotalInterest As Decimal
        Dim InterestRate As Decimal
        Dim BranchID, ApplicationID As String
        Dim SDuedate As DateTime
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingAgreementBatchInstallmentTable"
            End With

            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData

            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OutstandingPrincipal")
                        ARAgreement = dtslist.Rows(intLoopOmset).Item("OSARAgreement")
                        TotalOTR = dtslist.Rows(intLoopOmset).Item("TotalOTR")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("SDuedate")
                        InterestRate = dtslist.Rows(intLoopOmset).Item("InterestRate")
                        TenorAgr = dtslist.Rows(intLoopOmset).Item("TenorAgr")
                        FundingCoyPortion = dtslist.Rows(intLoopOmset).Item("FundingCoyPortion")
                        SecCoverType = dtslist.Rows(intLoopOmset).Item("SecurityCoverageType")
                        SecPercentage = dtslist.Rows(intLoopOmset).Item("SecurityCoveragePercentage")
                        'Tenor = CInt(txtTenor.Text)
                        Tenor = dtslist.Rows(intLoopOmset).Item("SisaTenor")

                        If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                            MonthPeriod = 6
                            Tenor = Math.Round(Tenor / 2, 0)
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                            MonthPeriod = 4
                            Tenor = Math.Round(Tenor / 3, 0)
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                            MonthPeriod = 2
                            Tenor = Math.Round(Tenor / 6, 0)
                        Else
                            MonthPeriod = 12
                        End If

                        'If TenorAgr < Tenor Then
                        '    Tenor = TenorAgr
                        'End If

                        ReDim PrincipalAmount(CInt(Tenor))
                        ReDim InterestAmount(CInt(Tenor))
                        ReDim OSPrincipalAmount(CInt(Tenor))
                        ReDim OSInterestAmount(CInt(Tenor))
                        ReDim InsAmount(CInt(Tenor))

                        'Beginingbalance = Math.Round(CDec((OutStandingPrincipal * FundingCoyPortion) / 100), 2)
                        Beginingbalance = Math.Round(OutStandingPrincipal, 2)
                        InsAmount(0) = Math.Round((Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -Beginingbalance, 0, DueDate.BegOfPeriod)), 2)
                        TotalInterest = Math.Round((InsAmount(0) * Tenor) - Beginingbalance, 2)

                        'InterestAmount(0) = Math.Round((Beginingbalance * InterestRate) / (MonthPeriod * 100), 2)
                        InterestAmount(0) = 0

                        PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 2)
                        OSPrincipalAmount(0) =  Beginingbalance - PrincipalAmount(0)
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = 1
                            .DueDate = DateAdd(DateInterval.Month, 0, SDuedate)
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = CDec(InterestRate)
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(drdInsPeriod.SelectedItem.Value.Trim)
                            .MaximumDateForDD = CInt(MonthPeriod)

                        End With
                        cReceive.AddFundingAgreementInstallment(oCustomClass)
                    Next
                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

    Public Function AddFundingAgreementInstallmentExecution() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim FundingCoyPortion As Decimal
        Dim TenorAgr As Integer
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim InitialDueDate As Integer

        Dim Beginingbalance As Decimal
        Dim SecPercentage As Decimal
        Dim SecCoverType As String
        Dim OutStandingPrincipal As Decimal
        Dim ARAgreement As Decimal
        Dim TotalOTR As Decimal
        Dim MonthPeriod As Integer
        Dim TotalInterest As Decimal
        Dim InterestRate As Decimal
        Dim BranchID, ApplicationID As String
        Dim SDuedate As DateTime
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingAgreementBatchInstallmentTable"
            End With

            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData

            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OutstandingPrincipal")
                        ARAgreement = dtslist.Rows(intLoopOmset).Item("OSARAgreement")
                        TotalOTR = dtslist.Rows(intLoopOmset).Item("TotalOTR")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("SDuedate")
                        InterestRate = dtslist.Rows(intLoopOmset).Item("InterestRate")
                        TenorAgr = dtslist.Rows(intLoopOmset).Item("TenorAgr")
                        FundingCoyPortion = dtslist.Rows(intLoopOmset).Item("FundingCoyPortion")
                        SecCoverType = dtslist.Rows(intLoopOmset).Item("SecurityCoverageType")
                        SecPercentage = dtslist.Rows(intLoopOmset).Item("SecurityCoveragePercentage")
                        'Tenor = CInt(txtTenor.Text)
                        Tenor = dtslist.Rows(intLoopOmset).Item("SisaTenor")

                        If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                            MonthPeriod = 6
                            Tenor = Math.Round(Tenor / 2, 0)
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                            MonthPeriod = 4
                            Tenor = Math.Round(Tenor / 3, 0)
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                            MonthPeriod = 2
                            Tenor = Math.Round(Tenor / 6, 0)
                        Else
                            MonthPeriod = 12
                        End If

                        'If TenorAgr < Tenor Then
                        '    Tenor = TenorAgr
                        'End If

                        ReDim PrincipalAmount(CInt(Tenor))
                        ReDim InterestAmount(CInt(Tenor))
                        ReDim OSPrincipalAmount(CInt(Tenor))
                        ReDim OSInterestAmount(CInt(Tenor))
                        ReDim InsAmount(CInt(Tenor))

                        'Beginingbalance = Math.Round(CDec((OutStandingPrincipal * FundingCoyPortion) / 100), 2)
                        Beginingbalance = Math.Round(OutStandingPrincipal, 2)
                        InsAmount(0) = Math.Round((Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -Beginingbalance, 0, DueDate.EndOfPeriod)), 2)
                        TotalInterest = Math.Round((InsAmount(0) * Tenor) - Beginingbalance, 2)
                        InterestAmount(0) = Math.Round((Beginingbalance * InterestRate) / (MonthPeriod * 100), 2)
                        PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 2)
                        OSPrincipalAmount(0) = Beginingbalance - PrincipalAmount(0)
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = 1
                            .DueDate = DateAdd(DateInterval.Month, 0, SDuedate)
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = CDec(InterestRate)
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(drdInsPeriod.SelectedItem.Value.Trim)
                            .MaximumDateForDD = CInt(MonthPeriod)

                        End With
                        cReceive.AddFundingAgreementInstallment(oCustomClass)
                    Next
                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

    Public Function AddFundingAgreementInstallmentExecutionKMK() As Boolean
        Dim dtslist As DataTable
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim BranchID As String
        Dim ApplicationID As String
        Dim cReceive As New FundingCompanyController
        Dim Tenor As Integer
        Dim SDuedate As DateTime
        Dim PrincipalAmount As Decimal
        Dim i As Integer

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingAgreementBatchInstallmentTable"
            End With

            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData

            If dtslist.Rows.Count > 0 Then
                Try
                    For i = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(i).Item("BranchID")
                        ApplicationID = dtslist.Rows(i).Item("ApplicationID")

                        PrincipalAmount = dtslist.Rows(i).Item("OutstandingPrincipal")
                        SDuedate = dtslist.Rows(i).Item("SDuedate")
                        InterestRate = dtslist.Rows(i).Item("InterestRate")
                        'Tenor = CInt(txtTenor.Text)
                        Tenor = 0

                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .DueDate = SDuedate
                            .PrincipalAmount = PrincipalAmount
                            .InterestRate = CDec(InterestRate)
                            .Tenor = CInt(Tenor)
                        End With

                        cReceive.AddFundingAgreementInstallmentKMK(oCustomClass)
                    Next

                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try

            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
#End Region
#Region "RollAllFundingBatchInstallment"
    Public Function RollAllFundingBatchInstallment() As Boolean
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingUpdateBatchInstallmentForPayOut"
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo

            If Me.IsReschedule = True Then
                'objCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = Me.Businessdate
                objCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = ConvertDate2(txtStartFrom.Text)
            Else
                objCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = DBNull.Value
            End If

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Return True
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
            Return False
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Function

#End Region
#Region "UpdateDrawdownAmount"
    Public Sub UpdateDrawdownAmount()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingUpdateDrawDownAmount"
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region
    Public Function getDrawdownAmount() As Decimal
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetDrawdownAmount"

            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Parameters.Add("@DrawdownAmount ", SqlDbType.Decimal).Direction = ParameterDirection.Output

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            Return IIf(IsDBNull(objCommand.Parameters("@DrawdownAmount ").Value), "0", objCommand.Parameters("@DrawdownAmount ").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Function
#Region "UpdateAgreementSecondExecution"
    Public Sub UpdateAgreementSecondExecution()
        Dim customClass As New Parameter.FundingContractBatch
        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With
            m_Company.UpdateFundingAgreementExecution(customClass)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
#Region "CheckAmountMustBeAdd"
    Public Sub CheckAmountMustBeAdd()
        Dim DrawDownAmount, AmountMustBeAdd, OutstandingLoan As Decimal
        Dim PrincipalAmount, ARAmount, OTRAmount As Decimal
        Dim TotalAccount As Integer
        Dim SecurityPercentage As Decimal
        Dim SecurityType As String
        Dim dt As DataTable
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging
        Try
            lblBankName.Text = Me.BankName
            lblFundingCoyName.Text = Me.CompanyName
            lblFundingContractNo.Text = Me.FundingContractNo
            lblContractName.Text = Me.ContractName

            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and FundingBankId='" & Me.BankID & "' and FundingContractId='" & Me.FundingContractNo & "' and FundingBatchId='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spFundingTotalAgreement"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            If Not IsDBNull(dt.Rows(0).Item("PrincipalAmount")) Then
                PrincipalAmount = dt.Rows(0).Item("PrincipalAmount")
            Else
                PrincipalAmount = 0
            End If

            With oGeneral
                .strConnection = GetConnectionString()
                '.WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetSecurityType"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            SecurityPercentage = dt.Rows(0).Item("SecurityCoveragePercentage")
            SecurityType = CStr(dt.Rows(0).Item("SecurityCoverageType")).Trim


            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetDrawDown"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            DrawDownAmount = dt.Rows(0).Item("PrincipalAmtToFunCoy")
            OutstandingLoan = dt.Rows(0).Item("OSAmtToFunCoy")

            Select Case SecurityType
                Case "P"
                    AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - PrincipalAmount
                Case "A"
                    AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - ARAmount
                Case "O"
                    AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - OTRAmount
            End Select
            If AmountMustBeAdd > 0 Then

                ShowMessage(lblMessage, "Nilai terpilih untuk batch ini <= Jumlah diCairkan", True)
                Exit Sub
            End If
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "CheckDrawDownStatus"
    Public Sub CheckDrawDownStatus()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingCheckFundingBatchInstallment"
            objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Parameters.Add("@status", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@Tenor", SqlDbType.SmallInt).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@InterestRate", SqlDbType.Float).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            Me.StatusDrawn = IIf(IsDBNull(objCommand.Parameters("@status").Value), "S", objCommand.Parameters("@status").Value)
            Me.TenorMax = IIf(IsDBNull(objCommand.Parameters("@Tenor").Value), 0, objCommand.Parameters("@Tenor").Value)
            Me.InterestRate = IIf(IsDBNull(objCommand.Parameters("@InterestRate").Value), 0, objCommand.Parameters("@InterestRate").Value)


        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region
#Region "CheckFacilityKind"
    Public Sub CheckFacilityKind()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingCheckFundingContractFacilityKind"
            objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FacilityKind", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.FacilityKind = IIf(IsDBNull(objCommand.Parameters("@FacilityKind").Value), "", objCommand.Parameters("@FacilityKind").Value)

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region
#Region "GenerateFloatingSchedule"
    Public Function GenerateFloatingAgreementSchedule() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim InsSeqNo As Integer
        Dim Beginingbalance As Decimal
        Dim OutStandingPrincipal As Decimal
        Dim BranchID As String
        Dim ApplicationID As String
        Dim SDuedate As DateTime
        Dim MonthPeriod As Integer
        Dim totalInterest As Decimal
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal
        Dim newInterestRate As Decimal
        ReDim PrincipalAmount(100)
        ReDim InterestAmount(100)
        ReDim OSPrincipalAmount(100)
        ReDim OSInterestAmount(100)
        ReDim InsAmount(100)

        'newInterestRate = CDec(txtInterestRate.Text)
        newInterestRate = 0
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                '.BusinessDate = Me.BusinessDate
                .BusinessDate = ConvertDate2(txtStartFrom.Text.Trim)
                .SpName = "spFundingAgreementInstallmentScheduleTable"
            End With
            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData
            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OSPrincipalAmount")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("DueDate")
                        InsSeqNo = dtslist.Rows(intLoopOmset).Item("InsSeqNo")
                        Tenor = dtslist.Rows(intLoopOmset).Item("Tenor")

                        If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                            MonthPeriod = 6
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                            MonthPeriod = 4
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                            MonthPeriod = 2
                        Else
                            MonthPeriod = 12
                        End If
                        InsAmount(0) = Math.Round((Pmt(((newInterestRate) / (MonthPeriod * 100)), Tenor, -OutStandingPrincipal, 0, 0)), 0)
                        totalInterest = Math.Round((InsAmount(0) * Tenor) - OutStandingPrincipal, 0)
                        InterestAmount(0) = Math.Round((OutStandingPrincipal * newInterestRate) / (MonthPeriod * 100), 0)
                        PrincipalAmount(0) = InsAmount(0) - InterestAmount(0)
                        OSPrincipalAmount(0) = OutStandingPrincipal - PrincipalAmount(0)
                        OSInterestAmount(0) = totalInterest - InterestAmount(0)
                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = InsSeqNo
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = newInterestRate
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(drdInsPeriod.SelectedItem.Value.Trim)
                            .MaximumDateForDD = CInt(MonthPeriod)
                        End With
                        cReceive.FundingRescheduleAgreementInstallment(oCustomClass)
                        'For i = 1 To Tenor - 1
                        '    If i = Tenor - 1 Then
                        '        PrincipalAmount(i) = OSPrincipalAmount(i - 1)
                        '        InterestAmount(i) = InsAmount(0) - PrincipalAmount(i)
                        '        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        '        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                        '    Else
                        '        InterestAmount(i) = Math.Round(((OSPrincipalAmount(i - 1) * newInterestRate) / (MonthPeriod * 100)), 0)
                        '        PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                        '        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        '        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                        '    End If

                        'Next
                        'j = 0
                        'For i = 0 To Tenor - 1
                        '    With oCustomClass
                        '        .strConnection = GetConnectionString
                        '        .FundingCoyId = Me.CompanyID
                        '        .FundingContractNo = Me.FundingContractNo
                        '        .FundingBatchNo = Me.FundingBatchNo
                        '        .BranchId = BranchID
                        '        .ApplicationID = ApplicationID
                        '        .InsSecNo = InsSeqNo + i
                        '        .PrepaymentAmount = InsAmount(0)
                        '        .PrincipalAmount = PrincipalAmount(i)
                        '        .InterestAmount = InterestAmount(i)
                        '        .PrincipalPaidAmount = 0
                        '        .InterestPaidAmount = 0
                        '        .OSPrincipalAmount = OSPrincipalAmount(i)
                        '        .OSInterestAmount = OSInterestAmount(i)
                        '        .InterestRate = newInterestRate
                        '    End With
                        '    cReceive.FundingRescheduleAgreementInstallment(oCustomClass)
                        'Next
                    Next
                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Public Function GenerateFloatingBatchSchedule() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim dt As DataTable
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch

        Dim DrawDownAmount, PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal
        Dim InsAmount() As Decimal
        Dim NewInterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim intLoopOmset As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal
        Dim InsSeqNo As Integer
        'NewInterestRate = CDec(txtInterestRate.Text)
        NewInterestRate = 0
        Batchdate = ConvertDate2(txtBatchDate.Text)
        Try
            'With customClass
            '    .strConnection = GetConnectionString
            '    .BankId = Me.BankID
            '    .FundingCoyId = Me.CompanyID
            '    .FundingContractNo = Me.FundingContractNo
            '    .FundingBatchNo = Me.FundingBatchNo
            'End With

            'm_Company.UpdateFundingAgreementExecution(customClass)


            With customClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                '.BusinessDate = Me.BusinessDate
                .BusinessDate = ConvertDate2(txtStartFrom.Text)
                .SpName = "spFundingBatchInstallmentScheduleTable"
            End With
            oReceive = cReceive.GetGeneralEditView(customClass)
            dt = oReceive.ListData
            If dt.Rows.Count > 0 Then
                Try
                    InsSeqNo = dt.Rows(0).Item("InsSeqNo")
                    iDueDate = dt.Rows(0).Item("DueDate")
                    DrawDownAmount = dt.Rows(0).Item("OSPrincipalAmount")
                    Tenor = dt.Rows(0).Item("Tenor")
                    ReDim PrincipalAmount(CInt(Tenor))
                    ReDim InterestAmount(CInt(Tenor))
                    ReDim OSPrincipalAmount(CInt(Tenor))
                    ReDim OSInterestAmount(CInt(Tenor))
                    ReDim InsAmount(CInt(Tenor))
                    If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                        MonthPeriod = 6
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                        MonthPeriod = 4
                    ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                        MonthPeriod = 2
                    Else
                        MonthPeriod = 12
                    End If
                    InsScheme = drdInsScheme.SelectedItem.Value
                    If InsScheme = "A" Then
                        InsAmount(0) = Pmt(((NewInterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, 0)
                        TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount
                        If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                            InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(NewInterestRate / (36000)) * CDec(DrawDownAmount))
                        Else
                            InterestAmount(0) = (DrawDownAmount * NewInterestRate) / (MonthPeriod * 100)
                        End If

                        PrincipalAmount(0) = InsAmount(0) - InterestAmount(0)
                        OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                        For i = 1 To Tenor - 1
                            InterestAmount(i) = (OSPrincipalAmount(i - 1) * NewInterestRate) / (MonthPeriod * 100)
                            PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                            OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                            OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                        Next

                    ElseIf InsScheme = "P" Then
                        TotalInterest = 0
                        PrincipalAmount(0) = DrawDownAmount / Tenor
                        If drdPaymentScheme.SelectedItem.Value.Trim = "4" Or drdPaymentScheme.SelectedItem.Value.Trim = "3" Then
                            InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(NewInterestRate / (36000)) * CDec(DrawDownAmount))
                            'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(NewInterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                        Else
                            InterestAmount(0) = (DrawDownAmount * NewInterestRate) / (MonthPeriod * 100)
                        End If
                        InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                        OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                        TotalInterest = InterestAmount(0)
                        For i = 1 To Tenor - 1
                            InterestAmount(i) = (OSPrincipalAmount(i - 1) * NewInterestRate) / (MonthPeriod * 100)
                            InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                            OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                            TotalInterest += InterestAmount(i)
                        Next
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                        For i = 1 To Tenor - 1
                            OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                        Next
                    ElseIf InsScheme = "I" Then
                        TotalInterest = 0
                        PrincipalAmount(0) = 0
                        InterestAmount(0) = 0
                        InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                        OSPrincipalAmount(0) = 0
                        TotalInterest = InterestAmount(0)
                        For i = 1 To Tenor - 1
                            InterestAmount(i) = 0
                            InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                            OSPrincipalAmount(i) = 0
                        Next
                        OSInterestAmount(0) = 0
                        For i = 1 To Tenor - 1
                            OSInterestAmount(i) = 0
                        Next
                    End If

                    'Write To Database Installment
                    j = 0
                    For i = 0 To Tenor - 1
                        With customClass
                            .strConnection = GetConnectionString()
                            .BankId = Me.BankID
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .InsSecNo = InsSeqNo + i
                            If InsScheme = "A" Then
                                .PrincipalAmount = PrincipalAmount(i)
                            Else
                                .PrincipalAmount = PrincipalAmount(0)
                            End If
                            .InterestAmount = InterestAmount(i)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(i)
                            .OSInterestAmount = OSInterestAmount(i)
                            .InterestRate = NewInterestRate
                        End With
                        m_Company.FundingUpdateContractBatchInst(customClass)
                    Next

                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
#End Region
#Region "GenerateFloatingAgreementPrepaymentSchedule"
    Public Function GenerateFloatingAgreementPrepaymentSchedule() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim InsSeqNo As Integer
        Dim Beginingbalance As Decimal
        Dim OutStandingPrincipal As Decimal
        Dim BranchID As String
        Dim ApplicationID As String
        Dim SDuedate As DateTime
        Dim MonthPeriod As Integer
        Dim totalInterest As Decimal
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal
        Dim newInterestRate As Decimal
        ReDim PrincipalAmount(100)
        ReDim InterestAmount(100)
        ReDim OSPrincipalAmount(100)
        ReDim OSInterestAmount(100)
        ReDim InsAmount(100)

        'newInterestRate = CDec(txtInterestRate.Text)
        newInterestRate = 0
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                '.BusinessDate = Me.BusinessDate
                .BusinessDate = ConvertDate2(txtStartFrom.Text.Trim)
                .SpName = "spFundingAgreementPrepaymentInstallmentScheduleTable"
            End With
            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData
            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OSPrincipalAmount")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("DueDate")
                        InsSeqNo = dtslist.Rows(intLoopOmset).Item("InsSeqNo")
                        Tenor = dtslist.Rows(intLoopOmset).Item("Tenor")

                        If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                            MonthPeriod = 6
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                            MonthPeriod = 4
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                            MonthPeriod = 2
                        Else
                            MonthPeriod = 12
                        End If
                        InsAmount(0) = Math.Round((Pmt(((newInterestRate) / (MonthPeriod * 100)), Tenor, -OutStandingPrincipal, 0, 0)), 0)
                        totalInterest = Math.Round((InsAmount(0) * Tenor) - OutStandingPrincipal, 0)
                        InterestAmount(0) = Math.Round((OutStandingPrincipal * newInterestRate) / (MonthPeriod * 100), 0)
                        PrincipalAmount(0) = InsAmount(0) - InterestAmount(0)
                        OSPrincipalAmount(0) = OutStandingPrincipal - PrincipalAmount(0)
                        OSInterestAmount(0) = totalInterest - InterestAmount(0)
                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = InsSeqNo
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = newInterestRate
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(drdInsPeriod.SelectedItem.Value.Trim)
                            .MaximumDateForDD = CInt(MonthPeriod)
                        End With
                        cReceive.FundingReschedulePrepaymentAgreementInstallment(oCustomClass)
                        'For i = 1 To Tenor - 1
                        '    If i = Tenor - 1 Then
                        '        PrincipalAmount(i) = OSPrincipalAmount(i - 1)
                        '        InterestAmount(i) = InsAmount(0) - PrincipalAmount(i)
                        '        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        '        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                        '    Else
                        '        InterestAmount(i) = Math.Round(((OSPrincipalAmount(i - 1) * newInterestRate) / (MonthPeriod * 100)), 0)
                        '        PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                        '        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        '        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                        '    End If

                        'Next
                        'j = 0
                        'For i = 0 To Tenor - 1
                        '    With oCustomClass
                        '        .strConnection = GetConnectionString
                        '        .FundingCoyId = Me.CompanyID
                        '        .FundingContractNo = Me.FundingContractNo
                        '        .FundingBatchNo = Me.FundingBatchNo
                        '        .BranchId = BranchID
                        '        .ApplicationID = ApplicationID
                        '        .InsSecNo = InsSeqNo + i
                        '        .PrepaymentAmount = InsAmount(0)
                        '        .PrincipalAmount = PrincipalAmount(i)
                        '        .InterestAmount = InterestAmount(i)
                        '        .PrincipalPaidAmount = 0
                        '        .InterestPaidAmount = 0
                        '        .OSPrincipalAmount = OSPrincipalAmount(i)
                        '        .OSInterestAmount = OSInterestAmount(i)
                        '        .InterestRate = newInterestRate
                        '    End With
                        '    cReceive.FundingReschedulePrepaymentAgreementInstallment(oCustomClass)
                        'Next
                    Next
                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
#End Region
#Region "GenerateRoolUPFundingBatchInstallmentFromPrepaymentAgreementInstallment"
    Public Function RollAllFundingBatchInstallmentFromPrepaymentAgreementInstallment() As Boolean
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingRoolUPBatchInstallmentFromPrepayment"
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo

            If Me.IsReschedule = True Then
                'objCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = Me.Businessdate
                objCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = ConvertDate2(txtStartFrom.Text)
            Else
                objCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = DBNull.Value
            End If

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Function
#End Region
#Region "CheckFundingContractSelectionTime"
    Public Sub CheckFundingContractSelectionTime()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingCheckFundingContractFlagCS"
            objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FlagCS", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.FlagCS = IIf(IsDBNull(objCommand.Parameters("@FlagCS").Value), "", objCommand.Parameters("@FlagCS").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

#End Region
#Region "FillCboBank"
    Sub fillcboBank()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetHOBranch"
            objCommand.Parameters.Add("@CompanyID", SqlDbType.Char, 3).Value = Me.SesCompanyID.Trim
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.BranchID = IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankAccount(GetConnectionString, Me.BranchID.Trim, "BA", "")
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub
#End Region
    Private Sub BtnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Dim isProcessSuccess As Boolean
        isProcessSuccess = False
        Me.IsReschedule = True


        CheckFacilityKind()
        isProcessSuccess = GenerateFloatingBatchSchedule()
        If isProcessSuccess = False Then
            lblMessage.Visible = True
            PanelAllFalse()
            pnlTop.Visible = True
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            pnlView.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            Exit Sub
        Else
            isProcessSuccess = False
        End If
        If Me.FacilityKind = "JFINC" Then
            isProcessSuccess = GenerateFloatingAgreementSchedule()
            If isProcessSuccess = False Then
                lblMessage.Visible = True
                PanelAllFalse()
                pnlTop.Visible = True
                pnlList.Visible = True
                pnlAddEdit.Visible = False
                pnlView.Visible = False
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
                Exit Sub
            Else
                isProcessSuccess = False
            End If
            isProcessSuccess = RollAllFundingBatchInstallment()
            If isProcessSuccess = False Then
                lblMessage.Visible = True
                PanelAllFalse()
                pnlTop.Visible = True
                pnlList.Visible = True
                pnlAddEdit.Visible = False
                pnlView.Visible = False
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
                Exit Sub
            Else
                isProcessSuccess = False
            End If
            edit()
            isProcessSuccess = GenerateFloatingAgreementPrepaymentSchedule()
            If isProcessSuccess = False Then
                lblMessage.Visible = True
                PanelAllFalse()
                pnlTop.Visible = True
                pnlList.Visible = True
                pnlAddEdit.Visible = False
                pnlView.Visible = False
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
                Exit Sub
            Else
                isProcessSuccess = False
            End If

            isProcessSuccess = RollAllFundingBatchInstallmentFromPrepaymentAgreementInstallment()
            If isProcessSuccess = False Then
                lblMessage.Visible = True
                PanelAllFalse()
                pnlTop.Visible = True
                pnlList.Visible = True
                pnlAddEdit.Visible = False
                pnlView.Visible = False
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
                Exit Sub
            Else
                isProcessSuccess = False
            End If
        Else
            edit()
        End If

        PanelAllFalse()
        pnlTop.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True

    End Sub
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub
    Private Sub btnDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDraft.Click
        Dim isProcessSuccess As Boolean = False

        PanelAllFalse()
        CheckDrawDownStatus()
        CheckFacilityKind()

        'sebelum execution hitung principal amount dari selected agreement
        If Me.FacilityKind = "JFINC" Then
            txtAmount.Text = getDrawdownAmount()
        End If


        'batch installment
        'Select Case Me.FacilityKind
        '    'Case "BTURN"
        '    '    isProcessSuccess = False

        '    '    ShowMessage(lblMessage, "BTURN n/a", True)
        '    'Case "TLOAN"
        '    '    isProcessSuccess = False

        '    '    ShowMessage(lblMessage, "TLOAN n/a", True)
        '    Case "JFINC", "CHANN"
        '        If drdPaymentScheme.SelectedItem.Value = "5" Then
        '            isProcessSuccess = updateAgreementExecutionInterestOnly()
        '        Else
        '            If ddlFirstInstallment.SelectedValue.Trim = "AD" Then
        '                isProcessSuccess = updateAgreementExecutionAD()
        '            Else
        '                isProcessSuccess = updateAgreementExecution()
        '            End If
        '        End If
        '    Case Else
        '        isProcessSuccess = False
        '        ShowMessage(lblMessage, "Facility Kind n/a", True)
        'End Select

        'If isProcessSuccess = False Then
        '    ShowMessage(lblMessage, lblMessage.Text & ", Create Draft Gagal", True)
        '    Exit Sub
        'End If


        'If ddlCaraPembayaran.SelectedValue = "JT" Then
        'agreement installment
        isProcessSuccess = AddFundingAgreementInstallmentExecutionDraft()
        'Else
        'batch installment
        If ddlFirstInstallment.SelectedValue.Trim = "AD" Then
            isProcessSuccess = updateAgreementExecutionAD()
            'Else
            isProcessSuccess = updateAgreementExecution()
        End If
        'End If

        If isProcessSuccess = False Then
            ShowMessage(lblMessage, lblMessage.Text & ", Create Draft Gagal", True)
            Exit Sub
        Else
            ShowMessage(lblMessage, "Create Draft Berhasil", False)
            isProcessSuccess = False
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub
    Public Function AddFundingAgreementInstallmentExecutionDraft() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim FundingCoyPortion As Decimal
        Dim TenorAgr As Integer
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim InitialDueDate As Integer

        Dim Beginingbalance As Decimal
        Dim SecPercentage As Decimal
        Dim SecCoverType As String
        Dim OutStandingPrincipal As Decimal
        Dim ARAgreement As Decimal
        Dim TotalOTR As Decimal
        Dim MonthPeriod As Integer
        Dim TotalInterest As Decimal
        Dim InterestRate As Decimal
        Dim BranchID, ApplicationID As String
        Dim SDuedate As DateTime
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingAgreementBatchInstallmentTableDraft"
            End With

            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData

            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OutstandingPrincipal")
                        ARAgreement = dtslist.Rows(intLoopOmset).Item("OSARAgreement")
                        TotalOTR = dtslist.Rows(intLoopOmset).Item("TotalOTR")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("SDuedate")
                        InterestRate = dtslist.Rows(intLoopOmset).Item("InterestRate")
                        TenorAgr = dtslist.Rows(intLoopOmset).Item("TenorAgr")
                        FundingCoyPortion = dtslist.Rows(intLoopOmset).Item("FundingCoyPortion")
                        SecCoverType = dtslist.Rows(intLoopOmset).Item("SecurityCoverageType")
                        SecPercentage = dtslist.Rows(intLoopOmset).Item("SecurityCoveragePercentage")
                        'Tenor = CInt(txtTenor.Text)                        
                        Tenor = dtslist.Rows(intLoopOmset).Item("SisaTenor")

                        If drdInsPeriod.SelectedItem.Value.Trim = "2" Then
                            MonthPeriod = 6
                            Tenor = Math.Round(Tenor / 2, 0)
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "3" Then
                            MonthPeriod = 4
                            Tenor = Math.Round(Tenor / 3, 0)
                        ElseIf drdInsPeriod.SelectedItem.Value.Trim = "6" Then
                            MonthPeriod = 2
                            Tenor = Math.Round(Tenor / 6, 0)
                        Else
                            MonthPeriod = 12
                        End If

                        'If TenorAgr < Tenor Then
                        '    Tenor = TenorAgr
                        'End If

                        ReDim PrincipalAmount(CInt(Tenor))
                        ReDim InterestAmount(CInt(Tenor))
                        ReDim OSPrincipalAmount(CInt(Tenor))
                        ReDim OSInterestAmount(CInt(Tenor))
                        ReDim InsAmount(CInt(Tenor))

                        'Beginingbalance = Math.Round(CDec((OutStandingPrincipal * FundingCoyPortion) / 100), 2)
                        Beginingbalance = Math.Round(OutStandingPrincipal, 2)

                        If ddlFirstInstallment.SelectedValue.Trim = "AD" Then
                            InsAmount(0) = Math.Round((Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -Beginingbalance, 0, DueDate.BegOfPeriod)), 2)
                        Else
                            InsAmount(0) = Math.Round((Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -Beginingbalance, 0, DueDate.EndOfPeriod)), 2)
                        End If

                        TotalInterest = Math.Round((InsAmount(0) * Tenor) - Beginingbalance, 2)

                        If ddlFirstInstallment.SelectedValue.Trim = "AD" Then
                            InterestAmount(0) = 0
                        Else
                            InterestAmount(0) = Math.Round((Beginingbalance * InterestRate) / (MonthPeriod * 100), 2)
                        End If

                        PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 2)

                        OSPrincipalAmount(0) = Beginingbalance - PrincipalAmount(0)
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = 1
                            .DueDate = DateAdd(DateInterval.Month, 0, SDuedate)
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = CDec(InterestRate)
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(drdInsPeriod.SelectedItem.Value.Trim)
                            .MaximumDateForDD = CInt(MonthPeriod)
                        End With

                        cReceive.AddFundingAgreementInstallmentDraft(oCustomClass)
                    Next

                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    'bunga menurun
    Private Function updateAgreementExecution2() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            CheckDrawDownStatus()

            If Me.StatusDrawn = "D" Then
                m_Company.FundingUpdateAgreementSecondExecution(customClass)
            Else
                m_Company.UpdateFundingAgreementExecution(customClass)
            End If


            If Me.FacilityKind = "JFINC" Then
                txtAmount.Text = getDrawdownAmount()
            End If


            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .InterestRate = Me.InterestRate
                .Tenor = Me.TenorMax
                .DueDate = Me.BatchDate  'ConvertDate2(txtBatchDate.Text)
                .PrincipalAmount = CDec(txtAmount.Text)
            End With

            m_Company.AddFundingContractBatchIns2(customClass)

            ShowMessage(lblMessage, "Eksekusi-2 Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Private Function genFAIEfektif() As Boolean
        Dim customClass As New Parameter.FundingContractBatch

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .InterestRate = Me.InterestRate

                ' tenor ambil dari sisa tenor yang belum dibayar ada di SP
                '.Tenor = CDec(txtTenor.Text)
                .Tenor = 0

                .DueDate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)
            End With

            m_Company.genFAIEfektif(customClass)
            ShowMessage(lblMessage, "Funding Agreement Installment saved!", False)
            Return True

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    'Anuitas Modified
    Private Function updateAgreementExecutionKMK() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            If Me.StatusDrawn = "D" Then
                m_Company.FundingUpdateAgreementSecondExecution(customClass)
            Else
                m_Company.UpdateFundingAgreementExecution(customClass)
            End If

            If Me.FacilityKind = "JFINC" Then
                txtAmount.Text = getDrawdownAmount()
            End If


            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = txtBatchNo.Text.Trim
                .InterestRate = Me.InterestRate
                '.Tenor = CDec(txtTenor.Text)
                .Tenor = 0
                .DueDate = ConvertDate2(txtBatchDate.Text)
                .PrincipalAmount = CDec(txtAmount.Text)
            End With

            m_Company.AddFundingContractBatchInsAddKMK(customClass)

            ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Public Function GetExel(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim sp As String

        sp = Me.SPName.ToString

        Try
            params(0) = New SqlParameter("@fundingcoyid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId

            params(1) = New SqlParameter("@fundingcontractno", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo

            params(2) = New SqlParameter("@fundingbatchno", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo


            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, sp, params).Tables(0)
            Return customclass
        Catch exp As Exception

        End Try
        
    End Function
    Sub GenSoftcopyDraft()
        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        Dim DtDownload As New DataTable
        Dim oDS As New DataSet
        Dim m_controller As New FundingCompanyController
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim FileType As String = ""
        Dim FileCount As Integer = 0

        Try
            oInstal.strConnection = GetConnectionString()
            oInstal.SPName = "spGetFundingSoftCopy"
            oInstal.WhereCond = " BankID='" & Me.CompanyID.Trim & "' Order by SequenceNo ASC"
            If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
                DtDownload = oConInst.GetSP(oInstal).ListData
                FileType = oConInst.GetSP(oInstal).ListData.Rows(0).Item("FileType").Trim
                FileCount = oConInst.GetSP(oInstal).ListData.Rows.Count
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            oDS = m_controller.FundingDraftSoftcopy(oCustomClass)

            For index = 0 To DtDownload.Rows.Count - 1
                If FileType.ToUpper = "TXT" Then
                    Convert(oDS.Tables(index), DtDownload.Rows(index).Item("SPName").ToString.Trim)
                ElseIf FileType.ToUpper = "XLS" Then
                    ConvertToExcel(oDS.Tables(index), DtDownload.Rows(index).Item("SPName").ToString.Trim)
                ElseIf FileType.ToUpper = "DBF" Then
                    GenDBF(oDS.Tables(index), DtDownload.Rows(index).Item("SPName").ToString.Trim)
                End If
            Next




            DgSoftCopy.DataSource = DtDownload
            DgSoftCopy.DataBind()

            PanelAllFalse()
            pnlDownload.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Public Sub Convert(ByVal dt As DataTable, ByVal strFileName As String)
        Dim FileName As String
        FileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        FileName += strFileName + ".txt"

        Dim sw As New StreamWriter(FileName + "", False)
        Dim iColCount As Integer = dt.Columns.Count
        For index = 0 To dt.Rows.Count - 1
            Dim tempStr As String = ""
            For i As Integer = 0 To iColCount - 1
                sw.Write(dt.Rows(index).Item(i).ToString())
            Next
            sw.Write(sw.NewLine)
        Next
        sw.Close()
        
    End Sub
    Public Sub ConvertToExcel(ByVal dt As DataTable, ByVal strFileName As String)
        Dim FileName As String
        FileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        FileName += strFileName + ".xls"

        Dim sw As New StreamWriter(FileName + "", False)
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)

        Dim dg As New DataGrid
        dg.DataSource = dt
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        sw.Write(stringWrite.ToString)
        sw.Close()
    End Sub
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)


        Using wb As New XLWorkbook()
            wb.Worksheets.Add(dtdata)

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub
    Sub GenDBF(ByVal DS As DataTable, ByVal SPName As String)
        If SPName.Contains("PRESCORE") Then
            GenDBFPRESCORE(DS, "PRESCORE")
        End If
        If SPName.Contains("AGUNAN") Then
            GenDBFAGUNAN(DS, "AGUNAN")
        End If
        If SPName.Contains("DEBITUR") Then
            GenDBFDEBITUR(DS, "DEBITUR")
        End If
        If SPName.Contains("KREDIT") Then
            GenDBFKREDIT(DS, "KREDIT")
        End If
    End Sub
    Sub GenDBFAGUNAN(ByVal Data As DataTable, ByVal FileName As String)
        Dim Str As String = ""
        Str = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        Dim cn As New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Str & ";Extended Properties='dBASE III';")
        cn.Open()

        If File.Exists(Str & "\" & "" & FileName & ".DBF") Then
            Dim cmd_Delete As New OleDbCommand("Drop Table " & FileName & "", cn)
            cmd_Delete.ExecuteNonQuery()
        End If

        Dim cmd_Create As New OleDbCommand("Create Table " & FileName & "(" _
                                                & "NOMOR FLOAT," _
                                                & "NAMPEM CHAR(40)," _
                                                & "JNSAGU CHAR(20)," _
                                                & "KLAKEN CHAR(20)," _
                                                & "DESAGU CHAR(1)," _
                                                & "JNSCUR CHAR(3)," _
                                                & "MERKEN CHAR(15)," _
                                                & "MERK_2 CHAR(30)," _
                                                & "WARKEN CHAR(30)," _
                                                & "JNSKEN CHAR(15)," _
                                                & "THNKEN FLOAT," _
                                                & "NOMESN CHAR(20)," _
                                                & "NORANG CHAR(20)," _
                                                & "BKTPML CHAR(40)," _
                                                & "NMBPKB CHAR(50)," _
                                                & "NRBPKB CHAR(40)," _
                                                & "TGBPKB CHAR(10)," _
                                                & "ALBPKB CHAR(99)," _
                                                & "KELURA CHAR(50)," _
                                                & "KECAMA CHAR(50)," _
                                                & "LOKDAT CHAR(4)," _
                                                & "NLLIKR FLOAT," _
                                                & "NILPSR FLOAT," _
                                                & "NILLIK FLOAT," _
                                                & "PRONPS FLOAT," _
                                                & "PRONLK FLOAT," _
                                                & "PNLAGU CHAR(10)," _
                                                & "PNLIND CHAR(1)," _
                                                & "TGLPNL CHAR(8)," _
                                                & "JNSIKT CHAR(3)," _
                                                & "NOBKTI CHAR(20)," _
                                                & "NILIKT FLOAT," _
                                                & "PARIPS CHAR(5)," _
                                                & "BLANK1 CHAR(1)," _
                                                & "BLANK2 CHAR(1)," _
                                                & "BLANK3 CHAR(1)," _
                                                & "ELIGIB CHAR(10)" _
                                                  & ")", cn)
        cmd_Create.ExecuteNonQuery()

        If Data.Rows.Count > 0 Then
            For index = 0 To Data.Rows.Count - 1
                Dim cmd_Insert As New OleDbCommand("INSERT INTO " & FileName & "(NOMOR,NAMPEM,JNSAGU,KLAKEN,DESAGU,JNSCUR,MERKEN,MERK_2,WARKEN,JNSKEN,THNKEN,NOMESN,NORANG,BKTPML,NMBPKB,NRBPKB,TGBPKB,ALBPKB,KELURA,KECAMA,LOKDAT,NLLIKR,NILPSR,NILLIK,PRONPS,PRONLK,PNLAGU,PNLIND,TGLPNL,JNSIKT,NOBKTI,NILIKT,PARIPS,BLANK1,BLANK2,BLANK3,ELIGIB)" _
                                                          & " VALUES('" _
                                                            & Data.Rows(index).Item("NOMOR") & "','" _
                                                            & Data.Rows(index).Item("NAMPEM").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSAGU").ToString & "','" _
                                                            & Data.Rows(index).Item("KLAKEN").ToString & "','" _
                                                            & Data.Rows(index).Item("DESAGU").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSCUR").ToString & "','" _
                                                            & Data.Rows(index).Item("MERKEN").ToString & "','" _
                                                            & Data.Rows(index).Item("MERK_2").ToString & "','" _
                                                            & Data.Rows(index).Item("WARKEN").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSKEN").ToString & "','" _
                                                            & Data.Rows(index).Item("THNKEN") & "','" _
                                                            & Data.Rows(index).Item("NOMESN").ToString & "','" _
                                                            & Data.Rows(index).Item("NORANG").ToString & "','" _
                                                            & Data.Rows(index).Item("BKTPML").ToString & "','" _
                                                            & Data.Rows(index).Item("NMBPKB").ToString & "','" _
                                                            & Data.Rows(index).Item("NRBPKB").ToString & "','" _
                                                            & Data.Rows(index).Item("TGBPKB").ToString & "','" _
                                                            & Data.Rows(index).Item("ALBPKB").ToString & "','" _
                                                            & Data.Rows(index).Item("KELURA").ToString & "','" _
                                                            & Data.Rows(index).Item("KECAMA").ToString & "','" _
                                                            & Data.Rows(index).Item("LOKDAT").ToString & "','" _
                                                            & Data.Rows(index).Item("NLLIKR") & "','" _
                                                            & Data.Rows(index).Item("NILPSR") & "','" _
                                                            & Data.Rows(index).Item("NILLIK") & "','" _
                                                            & Data.Rows(index).Item("PRONPS") & "','" _
                                                            & Data.Rows(index).Item("PRONLK") & "','" _
                                                            & Data.Rows(index).Item("PNLAGU").ToString & "','" _
                                                            & Data.Rows(index).Item("PNLIND").ToString & "','" _
                                                            & Data.Rows(index).Item("TGLPNL").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSIKT").ToString & "','" _
                                                            & Data.Rows(index).Item("NOBKTI").ToString & "','" _
                                                            & Data.Rows(index).Item("NILIKT") & "','" _
                                                            & Data.Rows(index).Item("PARIPS").ToString & "','" _
                                                            & Data.Rows(index).Item("BLANK1").ToString & "','" _
                                                            & Data.Rows(index).Item("BLANK2").ToString & "','" _
                                                            & Data.Rows(index).Item("BLANK3").ToString & "','" _
                                                            & Data.Rows(index).Item("ELIGIB").ToString & "'" _
                                                          & ")", cn)
                cmd_Insert.ExecuteNonQuery()
            Next

        End If

        cn.Close()
    End Sub
    Sub GenDBFDEBITUR(ByVal Data As DataTable, ByVal FileName As String)
        Dim Str As String = ""
        Str = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        Dim cn As New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Str & ";Extended Properties='dBASE III';")
        cn.Open()

        If File.Exists(Str & "\" & "" & FileName & ".DBF") Then
            Dim cmd_Delete As New OleDbCommand("Drop Table " & FileName & "", cn)
            cmd_Delete.ExecuteNonQuery()
        End If

        Dim cmd_Create As New OleDbCommand("Create Table " & FileName & "(" _
                                                & "NOMOR FLOAT," _
                                                & "NO_KONTRAK CHAR(40)," _
                                                & "BLANK1 CHAR(1)," _
                                                & "NAMPHL CHAR(40)," _
                                                & "NAMIBU CHAR(40)," _
                                                & "JNSKKB CHAR(40)," _
                                                & "GELAR CHAR(2)," _
                                                & "KETGLR CHAR(50)," _
                                                & "NOMKTP CHAR(30)," _
                                                & "NKTPSD CHAR(8)," _
                                                & "JNSKLM CHAR(1)," _
                                                & "TPTLHR CHAR(50)," _
                                                & "TGLLHR CHAR(8)," _
                                                & "ALPEM1 CHAR(100)," _
                                                & "KELURA CHAR(50)," _
                                                & "KECAMA CHAR(50)," _
                                                & "KDDATI CHAR(4)," _
                                                & "KODEPS CHAR(5)," _
                                                & "NOTELP CHAR(12)," _
                                                & "HPHONE CHAR(12)," _
                                                & "EMAILD CHAR(20)," _
                                                & "STSRMH CHAR(1)," _
                                                & "LAMTGL CHAR(2)," _
                                                & "TGLKER CHAR(8)," _
                                                & "NONPWP CHAR(30)," _
                                                & "WRGNEG CHAR(2)," _
                                                & "NEGDOM CHAR(2)," _
                                                & "PENDDK CHAR(1)," _
                                                & "STSKWN CHAR(1)," _
                                                & "CATPOR CHAR(2)," _
                                                & "GOLDEB CHAR(3)," _
                                                & "GOLDBL CHAR(3)," _
                                                & "KELHUB CHAR(40)," _
                                                & "TLPKEL CHAR(12)," _
                                                & "TGNGAN CHAR(2)," _
                                                & "TGLSTJ CHAR(8)," _
                                                & "JNSREK CHAR(1)," _
                                                & "NMBKLN CHAR(1)," _
                                                & "STSPEG CHAR(1)," _
                                                & "JOB_ID CHAR(3)," _
                                                & "NMPRSK CHAR(30)," _
                                                & "LAMKER CHAR(2)," _
                                                & "ALUPEM CHAR(40)," _
                                                & "BIDUSH CHAR(4)," _
                                                & "GAJPEM FLOAT," _
                                                & "SKPPER CHAR(1)," _
                                                & "SKKAKH CHAR(1)," _
                                                & "DAPEMK CHAR(1)," _
                                                & "NOLOKT CHAR(1)," _
                                                & "USIAMP FLOAT," _
                                                & "LGBMPK CHAR(5)," _
                                                & "LABMPK CHAR(5)," _
                                                & "RESFLG CHAR(1)," _
                                                & "FEDWHC CHAR(1)," _
                                                & "CUSTYP CHAR(1)," _
                                                & "SCUSTP CHAR(1)," _
                                                & "HUBDEB CHAR(4)," _
                                                & "SEGBIS CHAR(5)," _
                                                & "PINJLN CHAR(5)," _
                                                & "SUMUTM FLOAT," _
                                                & "TRNNOR FLOAT," _
                                                & "AGAMA CHAR(10)," _
                                                & "ALDOM CHAR(250)," _
                                                & "KDPDOM CHAR(5)," _
                                                & "JBT CHAR(2)," _
                                                & "KDPKRJ CHAR(5)," _
                                                & "ALMSRT CHAR(1)" _
                                                  & ")", cn)
        cmd_Create.ExecuteNonQuery()

        If Data.Rows.Count > 0 Then
            For index = 0 To Data.Rows.Count - 1
                Dim cmd_Insert As New OleDbCommand("INSERT INTO " & FileName & "(NOMOR,NAMPEM,BLANK1,NAMPHL,NAMIBU,JNSKKB,GELAR,KETGLR,NOMKTP,NKTPSD,JNSKLM,TPTLHR,TGLLHR,ALPEM1,KELURA,KECAMA,KDDATI,KODEPS,NOTELP,HPHONE,EMAILD,STSRMH,LAMTGL,TGLKER,NONPWP,WRGNEG,NEGDOM,PENDDK,STSKWN,CATPOR,GOLDEB,GOLDBL,KELHUB,TLPKEL,TGNGAN,TGLSTJ,JNSREK,NMBKLN,STSPEG,JOB_ID,NMPRSK,LAMKER,ALUPEM,BIDUSH,GAJPEM,SKPPER,SKKAKH,DAPEMK,NOLOKT,USIAMP,LGBMPK,LABMPK,RESFLG,FEDWHC,CUSTYP,SCUSTP,HUBDEB,SEGBIS,PINJLN,SUMUTM,TRNNOR,AGAMA,ALDOM,KDPDOM,JBT,KDPKRJ,ALMSRT)" _
                                                          & " VALUES('" _
                                                            & Data.Rows(index).Item("NOMOR").ToString & "','" _
                                                            & Data.Rows(index).Item("NAMPEM").ToString & "','" _
                                                            & Data.Rows(index).Item("BLANK1").ToString & "','" _
                                                            & Data.Rows(index).Item("NAMPHL").ToString & "','" _
                                                            & Data.Rows(index).Item("NAMIBU").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSKKB").ToString & "','" _
                                                            & Data.Rows(index).Item("GELAR").ToString & "','" _
                                                            & Data.Rows(index).Item("KETGLR").ToString & "','" _
                                                            & Data.Rows(index).Item("NOMKTP").ToString & "','" _
                                                            & Data.Rows(index).Item("NKTPSD").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSKLM").ToString & "','" _
                                                            & Data.Rows(index).Item("TPTLHR").ToString & "','" _
                                                            & Data.Rows(index).Item("TGLLHR").ToString & "','" _
                                                            & Data.Rows(index).Item("ALPEM1").ToString & "','" _
                                                            & Data.Rows(index).Item("KELURA").ToString & "','" _
                                                            & Data.Rows(index).Item("KECAMA").ToString & "','" _
                                                            & Data.Rows(index).Item("KDDATI").ToString & "','" _
                                                            & Data.Rows(index).Item("KODEPS").ToString & "','" _
                                                            & Data.Rows(index).Item("NOTELP").ToString & "','" _
                                                            & Data.Rows(index).Item("HPHONE").ToString & "','" _
                                                            & Data.Rows(index).Item("EMAILD").ToString & "','" _
                                                            & Data.Rows(index).Item("STSRMH").ToString & "','" _
                                                            & Data.Rows(index).Item("LAMTGL").ToString & "','" _
                                                            & Data.Rows(index).Item("TGLKER").ToString & "','" _
                                                            & Data.Rows(index).Item("NONPWP").ToString & "','" _
                                                            & Data.Rows(index).Item("WRGNEG").ToString & "','" _
                                                            & Data.Rows(index).Item("NEGDOM").ToString & "','" _
                                                            & Data.Rows(index).Item("PENDDK").ToString & "','" _
                                                            & Data.Rows(index).Item("STSKWN").ToString & "','" _
                                                            & Data.Rows(index).Item("CATPOR").ToString & "','" _
                                                            & Data.Rows(index).Item("GOLDEB").ToString & "','" _
                                                            & Data.Rows(index).Item("GOLDBL").ToString & "','" _
                                                            & Data.Rows(index).Item("KELHUB").ToString & "','" _
                                                            & Data.Rows(index).Item("TLPKEL").ToString & "','" _
                                                            & Data.Rows(index).Item("TGNGAN").ToString & "','" _
                                                            & Data.Rows(index).Item("TGLSTJ").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSREK").ToString & "','" _
                                                            & Data.Rows(index).Item("NMBKLN").ToString & "','" _
                                                            & Data.Rows(index).Item("STSPEG").ToString & "','" _
                                                            & Data.Rows(index).Item("JOB_ID").ToString & "','" _
                                                            & Data.Rows(index).Item("NMPRSK").ToString & "','" _
                                                            & Data.Rows(index).Item("LAMKER").ToString & "','" _
                                                            & Data.Rows(index).Item("ALUPEM").ToString & "','" _
                                                            & Data.Rows(index).Item("BIDUSH").ToString & "','" _
                                                            & Data.Rows(index).Item("GAJPEM").ToString & "','" _
                                                            & Data.Rows(index).Item("SKPPER").ToString & "','" _
                                                            & Data.Rows(index).Item("SKKAKH").ToString & "','" _
                                                            & Data.Rows(index).Item("DAPEMK").ToString & "','" _
                                                            & Data.Rows(index).Item("NOLOKT").ToString & "','" _
                                                            & Data.Rows(index).Item("USIAMP").ToString & "','" _
                                                            & Data.Rows(index).Item("LGBMPK").ToString & "','" _
                                                            & Data.Rows(index).Item("LABMPK").ToString & "','" _
                                                            & Data.Rows(index).Item("RESFLG").ToString & "','" _
                                                            & Data.Rows(index).Item("FEDWHC").ToString & "','" _
                                                            & Data.Rows(index).Item("CUSTYP").ToString & "','" _
                                                            & Data.Rows(index).Item("SCUSTP").ToString & "','" _
                                                            & Data.Rows(index).Item("HUBDEB").ToString & "','" _
                                                            & Data.Rows(index).Item("SEGBIS").ToString & "','" _
                                                            & Data.Rows(index).Item("PINJLN").ToString & "','" _
                                                            & Data.Rows(index).Item("SUMUTM").ToString & "','" _
                                                            & Data.Rows(index).Item("TRNNOR").ToString & "','" _
                                                            & Data.Rows(index).Item("AGAMA").ToString & "','" _
                                                            & Data.Rows(index).Item("ALDOM").ToString & "','" _
                                                            & Data.Rows(index).Item("KDPDOM").ToString & "','" _
                                                            & Data.Rows(index).Item("JBT").ToString & "','" _
                                                            & Data.Rows(index).Item("KDPKRJ").ToString & "','" _
                                                            & Data.Rows(index).Item("ALMSRT").ToString & "'" _
                                                          & ")", cn)
                cmd_Insert.ExecuteNonQuery()
            Next

        End If

        cn.Close()
    End Sub
    Sub GenDBFKREDIT(ByVal Data As DataTable, ByVal FileName As String)
        Dim Str As String = ""
        Str = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        Dim cn As New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Str & ";Extended Properties='dBASE III';")
        cn.Open()

        If File.Exists(Str & "\" & "" & FileName & ".DBF") Then
            Dim cmd_Delete As New OleDbCommand("Drop Table " & FileName & "", cn)
            cmd_Delete.ExecuteNonQuery()
        End If

        Dim cmd_Create As New OleDbCommand("Create Table " & FileName & "(" _
                                                    & "NOMOR FLOAT," _
                                                    & "BARUPP CHAR(1)," _
                                                    & "JNSFAS CHAR(4)," _
                                                    & "TUJREK CHAR(1)," _
                                                    & "SEGOWN CHAR(5)," _
                                                    & "SSEGOW CHAR(5)," _
                                                    & "KODJKW CHAR(1)," _
                                                    & "LAMANG FLOAT," _
                                                    & "SISAJW FLOAT," _
                                                    & "GUNAKR CHAR(2)," _
                                                    & "TUJGKR CHAR(28)," _
                                                    & "VALUTA CHAR(3)," _
                                                    & "MAXPLA FLOAT," _
                                                    & "PLAIND FLOAT," _
                                                    & "PROVIS FLOAT," _
                                                    & "BIAADM FLOAT," _
                                                    & "PENALT FLOAT," _
                                                    & "INTPYF FLOAT," _
                                                    & "SIFSBG CHAR(7)," _
                                                    & "DISKON FLOAT," _
                                                    & "PADTDK CHAR(40)," _
                                                    & "BUPLN CHAR(40)," _
                                                    & "AGRBIS CHAR(1)," _
                                                    & "SIFKRD CHAR(2)," _
                                                    & "GOLKRD CHAR(2)," _
                                                    & "SANDIS CHAR(4)," _
                                                    & "JNSGUN CHAR(2)," _
                                                    & "ORIGUN CHAR(1)," _
                                                    & "SEKEKO CHAR(4)," _
                                                    & "JKRLBU CHAR(2)," _
                                                    & "SFKSID CHAR(2)," _
                                                    & "KATLBU CHAR(1)," _
                                                    & "JNSLBU CHAR(2)," _
                                                    & "LOKPRO CHAR(4)," _
                                                    & "NILPRO FLOAT," _
                                                    & "FASDAN CHAR(4)," _
                                                    & "SUMAPL CHAR(17)," _
                                                    & "TGLAPL CHAR(8)," _
                                                    & "KODAPL CHAR(3)," _
                                                    & "AGUHIT CHAR(1)," _
                                                    & "BLANK1 FLOAT," _
                                                    & "BLANK2 FLOAT," _
                                                    & "SYARLN CHAR(10)," _
                                                    & "CARLNS CHAR(10)," _
                                                    & "NOMKTR CHAR(20)," _
                                                    & "KODEMF CHAR(8)," _
                                                    & "DESKOD CHAR(15)," _
                                                    & "TTLFLP FLOAT" _
                                                  & ")", cn)
        cmd_Create.ExecuteNonQuery()

        If Data.Rows.Count > 0 Then
            For index = 0 To Data.Rows.Count - 1
                Dim cmd_Insert As New OleDbCommand("INSERT INTO " & FileName & "(NOMOR,BARUPP,JNSFAS,TUJREK,SEGOWN,SSEGOW,KODJKW,LAMANG,SISAJW,GUNAKR,TUJGKR,VALUTA,MAXPLA,PLAIND,PROVIS,BIAADM,PENALT,INTPYF,SIFSBG,DISKON,PADTDK,BUPLN,AGRBIS,SIFKRD,GOLKRD,SANDIS,JNSGUN,ORIGUN,SEKEKO,JKRLBU,SFKSID,KATLBU,JNSLBU,LOKPRO,NILPRO,FASDAN,SUMAPL,TGLAPL,KODAPL,AGUHIT,BLANK1,BLANK2,SYARLN,CARLNS,NOMKTR,KODEMF,DESKOD,TTLFLP)" _
                                                          & " VALUES('" _
                                                          & Data.Rows(index).Item("NOMOR").ToString & "','" _
                                                            & Data.Rows(index).Item("BARUPP").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSFAS").ToString & "','" _
                                                            & Data.Rows(index).Item("TUJREK").ToString & "','" _
                                                            & Data.Rows(index).Item("SEGOWN").ToString & "','" _
                                                            & Data.Rows(index).Item("SSEGOW").ToString & "','" _
                                                            & Data.Rows(index).Item("KODJKW").ToString & "','" _
                                                            & Data.Rows(index).Item("LAMANG").ToString & "','" _
                                                            & Data.Rows(index).Item("SISAJW").ToString & "','" _
                                                            & Data.Rows(index).Item("GUNAKR").ToString & "','" _
                                                            & Data.Rows(index).Item("TUJGKR").ToString & "','" _
                                                            & Data.Rows(index).Item("VALUTA").ToString & "','" _
                                                            & Data.Rows(index).Item("MAXPLA").ToString & "','" _
                                                            & Data.Rows(index).Item("PLAIND").ToString & "','" _
                                                            & Data.Rows(index).Item("PROVIS").ToString & "','" _
                                                            & Data.Rows(index).Item("BIAADM").ToString & "','" _
                                                            & Data.Rows(index).Item("PENALT").ToString & "','" _
                                                            & Data.Rows(index).Item("INTPYF").ToString & "','" _
                                                            & Data.Rows(index).Item("SIFSBG").ToString & "','" _
                                                            & Data.Rows(index).Item("DISKON").ToString & "','" _
                                                            & Data.Rows(index).Item("PADTDK").ToString & "','" _
                                                            & Data.Rows(index).Item("BUPLN").ToString & "','" _
                                                            & Data.Rows(index).Item("AGRBIS").ToString & "','" _
                                                            & Data.Rows(index).Item("SIFKRD").ToString & "','" _
                                                            & Data.Rows(index).Item("GOLKRD").ToString & "','" _
                                                            & Data.Rows(index).Item("SANDIS").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSGUN").ToString & "','" _
                                                            & Data.Rows(index).Item("ORIGUN").ToString & "','" _
                                                            & Data.Rows(index).Item("SEKEKO").ToString & "','" _
                                                            & Data.Rows(index).Item("JKRLBU").ToString & "','" _
                                                            & Data.Rows(index).Item("SFKSID").ToString & "','" _
                                                            & Data.Rows(index).Item("KATLBU").ToString & "','" _
                                                            & Data.Rows(index).Item("JNSLBU").ToString & "','" _
                                                            & Data.Rows(index).Item("LOKPRO").ToString & "','" _
                                                            & Data.Rows(index).Item("NILPRO").ToString & "','" _
                                                            & Data.Rows(index).Item("FASDAN").ToString & "','" _
                                                            & Data.Rows(index).Item("SUMAPL").ToString & "','" _
                                                            & Data.Rows(index).Item("TGLAPL").ToString & "','" _
                                                            & Data.Rows(index).Item("KODAPL").ToString & "','" _
                                                            & Data.Rows(index).Item("AGUHIT").ToString & "','" _
                                                            & Data.Rows(index).Item("BLANK1").ToString & "','" _
                                                            & Data.Rows(index).Item("BLANK2").ToString & "','" _
                                                            & Data.Rows(index).Item("SYARLN").ToString & "','" _
                                                            & Data.Rows(index).Item("CARLNS").ToString & "','" _
                                                            & Data.Rows(index).Item("NOMKTR").ToString & "','" _
                                                            & Data.Rows(index).Item("KODEMF").ToString & "','" _
                                                            & Data.Rows(index).Item("DESKOD").ToString & "','" _
                                                            & Data.Rows(index).Item("TTLFLP").ToString & "'" _
                                                          & ")", cn)
                cmd_Insert.ExecuteNonQuery()
            Next

        End If

        cn.Close()
    End Sub
    Sub GenDBFPRESCORE(ByVal Data As DataTable, ByVal FileName As String)
        Dim Str As String = ""
        Str = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        Dim cn As New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Str & ";Extended Properties='dBASE III';")
        cn.Open()

        If File.Exists(Str & "\" & "" & FileName & ".DBF") Then
            Dim cmd_Delete As New OleDbCommand("Drop Table " & FileName & "", cn)
            cmd_Delete.ExecuteNonQuery()
        End If

        Dim cmd_Create As New OleDbCommand("Create Table " & FileName & "(" _
                                                  & "NOMOR FLOAT," _
                                                  & "POTBLN FLOAT," _
                                                  & "BIAYRT FLOAT," _
                                                  & "BIASKL FLOAT," _
                                                  & "BIAYLN FLOAT," _
                                                  & "BIATOT FLOAT," _
                                                  & "HSLTTP FLOAT," _
                                                  & "JLANGB FLOAT," _
                                                  & "PERBEB FLOAT," _
                                                  & "LAMANG FLOAT," _
                                                  & "MAXPLA FLOAT," _
                                                  & "MHNKRE FLOAT," _
                                                  & "PLAUSL FLOAT," _
                                                  & "OMZET FLOAT," _
                                                  & "OMZNET FLOAT," _
                                                  & "OMZBSH FLOAT," _
                                                  & "JNSKEN CHAR(15)," _
                                                  & "HRGOTR FLOAT," _
                                                  & "UMBANK FLOAT," _
                                                  & "PERUMD CHAR(5)," _
                                                  & "REKBNK CHAR(1)," _
                                                  & "RIWBNK CHAR(1)" _
                                                  & ")", cn)
        cmd_Create.ExecuteNonQuery()

        If Data.Rows.Count > 0 Then
            For index = 0 To Data.Rows.Count - 1
                Dim cmd_Insert As New OleDbCommand("INSERT INTO " & FileName & "(NOMOR,POTBLN,BIAYRT,BIASKL,BIAYLN,BIATOT,HSLTTP,JLANGB,PERBEB,LAMANG,MAXPLA,MHNKRE,PLAUSL,OMZET,OMZNET,OMZBSH,JNSKEN,HRGOTR,UMBANK,PERUMD,REKBNK,RIWBNK)" _
                                                          & " VALUES('" _
                                                          & Data.Rows(index).Item("NOMOR").ToString & "','" _
                                                          & Data.Rows(index).Item("POTBLN").ToString & "','" _
                                                          & Data.Rows(index).Item("BIAYRT").ToString & "','" _
                                                          & Data.Rows(index).Item("BIASKL").ToString & "','" _
                                                          & Data.Rows(index).Item("BIAYLN").ToString & "','" _
                                                          & Data.Rows(index).Item("BIATOT").ToString & "','" _
                                                          & Data.Rows(index).Item("HSLTTP").ToString & "','" _
                                                          & Data.Rows(index).Item("JLANGB").ToString & "','" _
                                                          & Data.Rows(index).Item("PERBEB").ToString & "','" _
                                                          & Data.Rows(index).Item("LAMANG").ToString & "','" _
                                                          & Data.Rows(index).Item("MAXPLA").ToString & "','" _
                                                          & Data.Rows(index).Item("MHNKRE").ToString & "','" _
                                                          & Data.Rows(index).Item("PLAUSL").ToString & "','" _
                                                          & Data.Rows(index).Item("OMZET").ToString & "','" _
                                                          & Data.Rows(index).Item("OMZNET").ToString & "','" _
                                                          & Data.Rows(index).Item("OMZBSH").ToString & "','" _
                                                          & Data.Rows(index).Item("JNSKEN").ToString & "','" _
                                                          & Data.Rows(index).Item("HRGOTR").ToString & "','" _
                                                          & Data.Rows(index).Item("UMBANK").ToString & "','" _
                                                          & Data.Rows(index).Item("PERUMD").ToString & "','" _
                                                          & Data.Rows(index).Item("REKBNK").ToString & "','" _
                                                          & Data.Rows(index).Item("RIWBNK").ToString & "'" _
                                                          & ")", cn)
                cmd_Insert.ExecuteNonQuery()
            Next

        End If

        cn.Close()
    End Sub
    Private Sub dtgFundingContractBatch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtgFundingContractBatch.SelectedIndexChanged

    End Sub
    Private Sub drdPaymentScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drdPaymentScheme.SelectedIndexChanged
        If drdPaymentScheme.SelectedValue = "2" Or drdPaymentScheme.SelectedValue = "3" Or drdPaymentScheme.SelectedValue = "4" Then
            'divDueDate.Visible = True
            txtDueDate.Visible = True
        Else
            txtDueDate.Visible = False
        End If
    End Sub
    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With

    End Sub
    Private Sub ButtonBack_Click(sender As Object, e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub
    Private Sub DgSoftCopy_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DgSoftCopy.ItemCommand
        Dim dtViewData As New DataTable
        Dim oEntities As New Parameter.FundingContractBatch
        Dim DtDownload As New DataTable
        Dim oDS As New DataSet

        Select Case e.CommandName
            Case "Download"
                Dim lblSPName As Label
                lblSPName = CType(DgSoftCopy.Items(e.Item.ItemIndex).FindControl("lblSPName"), Label)
                Dim lblFileType As Label
                lblFileType = CType(DgSoftCopy.Items(e.Item.ItemIndex).FindControl("lblFileType"), Label)

                Me.SPName = lblSPName.Text.Trim

                If lblFileType.Text.Trim = "XLS" Then
                    With oEntities
                        .FundingCoyId = Me.CompanyID
                        .FundingContractNo = Me.FundingContractNo
                        .FundingBatchNo = Me.FundingBatchNo
                        .SP = lblSPName.Text.Trim
                        .strConnection = GetConnectionString()
                    End With

                    oEntities = GetExel(oEntities)
                    dtViewData = oEntities.ListData
                    ExportTableData(dtViewData, lblSPName.Text.Trim)
                End If

                If lblFileType.Text.Trim = "DBF" Then
                    If lblSPName.Text.Contains("PRESCORE") Then
                        lblSPName.Text = "PRESCORE"
                    End If
                    If lblSPName.Text.Contains("AGUNAN") Then
                        lblSPName.Text = "AGUNAN"
                    End If
                    If lblSPName.Text.Contains("DEBITUR") Then
                        lblSPName.Text = "DEBITUR"
                    End If
                    If lblSPName.Text.Contains("KREDIT") Then
                        lblSPName.Text = "KREDIT"
                    End If
                End If

                Response.ContentType = "application/" & lblFileType.Text.Trim & ""
                Response.AddHeader("content-disposition", "attachment; filename=" & lblSPName.Text.Trim & "." & lblFileType.Text.Trim & "")
                Response.WriteFile(Server.MapPath("../../XML/" & lblSPName.Text.Trim & "." & lblFileType.Text.Trim & ""))
                Response.End()

        End Select
    End Sub

    Sub txtPeriodFrom_onchange() Handles txtPeriodFrom.TextChanged
        If txtPeriodeBulan.Text <> "" Then
            txtPeriodTo.Text = CDate(DateAdd(DateInterval.Month, CDbl(txtPeriodeBulan.Text), ConvertDate2(txtPeriodFrom.Text))).ToString("dd/MM/yyyy")
        End If
    End Sub
    Sub txtPeriodeBulan_onChange() Handles txtPeriodeBulan.TextChanged
        If txtPeriodFrom.Text <> "" Then
            txtPeriodTo.Text = CDate(DateAdd(DateInterval.Month, CDbl(txtPeriodeBulan.Text), ConvertDate2(txtPeriodFrom.Text))).ToString("dd/MM/yyyy")
        End If
    End Sub
    Sub txtPeriodeTo_onChage() Handles txtPeriodTo.TextChanged
        Dim h As Long
        If txtPeriodFrom.Text <> "" Then
            h = DateDiff(DateInterval.Month, ConvertDate2(txtPeriodFrom.Text), ConvertDate2(txtPeriodTo.Text))
            If h <= 0 Then
                txtPeriodeBulan.Text = ""
            Else
                txtPeriodeBulan.Text = h.ToString
            End If

        Else
            ShowMessage(lblMessage, "Periode Awal belum diisi!!!", True)
        End If
    End Sub
End Class