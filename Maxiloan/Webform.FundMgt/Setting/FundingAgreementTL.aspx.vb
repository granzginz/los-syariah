﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingAgreementTL
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"

    Protected WithEvents txtAdditionalBranchAmount As ucNumberFormat
    Protected WithEvents dateFrom As ucDateCE
    Protected WithEvents dateTo As ucDateCE

#End Region
#Region " Property "
    Private Property AmountMustBeAdd() As Double
        Get
            Return CDbl(ViewState("AmountMustBeAdd"))
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountMustBeAdd") = Value
        End Set
    End Property
    Public Property FacilityKind() As String
        Get
            Return CStr(ViewState("FacilityKind"))
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityKind") = Value
        End Set
    End Property
    Public Property TenorMax() As Short
        Get
            Return CStr(ViewState("TenorMax"))
        End Get
        Set(ByVal Value As Short)
            ViewState("TenorMax") = Value
        End Set
    End Property
    Public Property InterestRate() As Double
        Get
            Return CDbl(ViewState("InterestRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InterestRate") = Value
        End Set
    End Property

    Public Property TotalAR As Double
        Get
            Return CDbl(ViewState("TotalAR"))
        End Get
        Set(value As Double)
            ViewState("TotalAR") = value
        End Set
    End Property
    Public Property TotalPokok As Double
        Get
            Return CDbl(ViewState("TotalPokok"))
        End Get
        Set(value As Double)
            ViewState("TotalPokok") = value
        End Set
    End Property
    Public Property TotalOTR As Double
        Get
            Return CDbl(ViewState("TotalOTR"))
        End Get
        Set(value As Double)
            ViewState("TotalOTR") = value
        End Set
    End Property
    Public Property TotalNTF As Double
        Get
            Return CDbl(ViewState("TotalNTF"))
        End Get
        Set(value As Double)
            ViewState("TotalNTF") = value
        End Set
    End Property
    Public Property TotalTobeAdd As Double
        Get
            Return CDbl(ViewState("TotalTobeAdd"))
        End Get
        Set(value As Double)
            ViewState("TotalTobeAdd") = value
        End Set
    End Property
    Public Property TotalAccounts As Double
        Get
            Return CDbl(ViewState("TotalAccounts"))
        End Get
        Set(value As Double)
            ViewState("TotalAccounts") = value
        End Set
    End Property
    Public Property TotalPokokPerPage As Double
        Get
            Return CDbl(ViewState("TotalPokokPerPage"))
        End Get
        Set(value As Double)
            ViewState("TotalPokokPerPage") = value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Public Property PlafondAmount As Double
        Get
            Return CDbl(ViewState("PlafondAmount"))
        End Get
        Set(value As Double)
            ViewState("PlafondAmount") = value
        End Set
    End Property
#End Region

#Region " Private Const "
    Private dt As DataTable
    Private cGeneral As New GeneralPagingController
    Private oGeneral As New Parameter.GeneralPaging

    Private m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 25
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        'InitialDefaultPanel()
        lblMessage.Visible = False

        If Not Me.IsPostBack Then
            Me.FormID = "FUNDINGBATCH"

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
            If Request.QueryString("Tenor") <> "" Then Me.Tenor = Request.QueryString("Tenor")
            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")
            If Request.QueryString("Plafond") <> "" Then lbljumlahplafond.Text = FormatNumber(Request.QueryString("Plafond"), 0).Trim

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""

                'dateFrom.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                'dateTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                'pnlList.Visible = True
                'pnlAddDetail.Visible = False
                'PnlSearchBy.Visible = False
                'dvsearch.Visible = False
                'btnSearchAgreement.Visible = False
                'btnSelection.Text = "Selection"

                'DisplayHeaderInfo()
                'DisplaySelected()
                'DisplayTotalSelected()

                pnlAddDetail.Visible = True
                PnlSearchBy.Visible = True
                dvsearch.Visible = True
                btnSearchAgreement.Visible = True
                pnlList.Visible = True
                btnSelection.Text = "Selection"
                btnSave.Visible = False
                DisplayHeaderInfo()
                dtgFundingAgreementList.DataSource = Nothing
                dtgFundingAgreementList.DataBind()
                DisplayTotalSelected()

                'DisplayAgreementSelection(Me.SearchBy, Me.SortBy)


                'If Command = "ADD" Then
                '    Me.ActionAddEdit = "ADD"
                '    pnlAddDetail.Visible = True
                '    PnlAdd.Visible = True
                '    PnlSearchBy.Visible = False

                '    DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
                'Else
                '    Me.ActionAddEdit = "EDIT"
                '    pnlList.Visible = True
                '    pnlAddDetail.Visible = True
                '    PnlSearchBy.Visible = True
                '    DisplayHeaderInfo()
                'End If
            End If

            InitiateUCnumberFormat(txtAdditionalBranchAmount, True, True)
            txtAdditionalBranchAmount.RangeValidatorMinimumValue = "0"
            txtAdditionalBranchAmount.RangeValidatorMaximumValue = "1"
            'Else
            '    DisplayTotalSelectedByCheckBox()

        End If
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If
        lbltotrec.Text = recordCount.ToString
        pnlList.Visible = True
        PnlSearchBy.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearchBy.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        PnlSearchBy.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearchBy.Visible = True
    End Sub

#End Region

#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

    Private Property Tenor() As String
        Get
            Return CStr(ViewState("Tenor"))
        End Get
        Set(ByVal Value As String)
            ViewState("Tenor") = Value
        End Set
    End Property
#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        PnlAdd.Visible = False
        lblMessage.Visible = False
        PnlSearchBy.Visible = False
        'pnlView.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        'pnlList.Visible = True
    End Sub
#End Region


    Sub DisplayAgreementSelection(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New FundingCompanyController
        Dim oContract As New Parameter.FundingContractBatch


        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .Filter = "1"
        End With

        oContract = cContract.FundingAgreementSelection(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oContract.Listdata

        If Not IsNothing(dtsEntity) Then
            dtvEntity = dtsEntity.DefaultView
            dtvEntity.Sort = Me.SortBy
            dtgFundingAgreementList.DataSource = dtvEntity
            dtgFundingAgreementList.DataBind()
        Else
            dtgFundingAgreementList.CurrentPageIndex = 0
            dtgFundingAgreementList.DataBind()
            dtgFundingAgreementList.DataSource = Nothing
        End If

        Dim chek As CheckBox
        For index = 0 To dtgFundingAgreementList.Items.Count - 1
            chek = CType(dtgFundingAgreementList.Items(index).FindControl("chkSelect"), CheckBox)
            chek.Checked = False
        Next


        PagingFooter()
    End Sub

#Region "UpdateAgreementSelected"
    Private Sub UpdateAgreementSelected()
        Dim customClass As New Parameter.FundingContractBatch
        Dim SecurityCoverageType As String
        Dim BatchDate As DateTime

        Try
            With oGeneral
                .strConnection = GetConnectionString()
                '.WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetSecurityType"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData
            SecurityCoverageType = CStr(dt.Rows(0).Item("SecurityCoverageType")).Trim

            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "' and FundingBatchNo='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetBatchDate"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData
            BatchDate = dt.Rows(0).Item("BatchDate")

            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                '.PlafondAmount = CDec(txtAdditionalBranchAmount.Text)
                .PlafondAmount = CDbl(lblDrawDownAmount.Text) + CDbl(txtAdditionalBranchAmount.Text)
                .BranchId = drdBranch.SelectedValue
                .SecurityType = SecurityCoverageType
                .BatchDate = BatchDate
                '.GoLiveDate = ConvertDate2(txtGoLiveDate.Text)
                '.NetDP = CDec(txtDP.Text)
                '.Tenor = CInt(Me.Tenor)
            End With

            m_Company.UpdateFundingAgreementSelected(customClass)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

#Region "MISC"


    Private Sub DisplayHeaderInfo()
        Dim DrawDownAmount, OutstandingLoan As Decimal
        'Dim PrincipalAmount, ARAmount, OTRAmount, NTFAmount As Decimal
        Dim TotalAccount As Integer
        Dim SecurityPercentage As Decimal
        Dim SecurityType As String
        Dim cContract As New FundingCompanyController
        Dim oContract As New Parameter.FundingContractBatch

        Try
            bindComboIDbranch()
            lblBankName.Text = Me.BankName
            lblFundingCoyName.Text = Me.CompanyName
            lblFundingContractNo.Text = Me.FundingContractNo
            lblContractName.Text = Me.ContractName
            lblBatchNo.Text = Me.FundingBatchNo


            'With oContract
            '    .strConnection = GetConnectionString()
            '    .FundingCoyId = Me.CompanyID
            '    .FundingContractNo = Me.FundingContractNo
            '    .FundingBatchNo = Me.FundingBatchNo
            'End With

            'oContract = cContract.FundingTotalSelected(oContract)
            'dt = oContract.ListData


            'ARAmount = IIf(IsDBNull(dt.Rows(0).Item("ARAmount")), 0, dt.Rows(0).Item("ARAmount"))
            'PrincipalAmount = IIf(IsDBNull(dt.Rows(0).Item("PrincipalAmount")), 0, dt.Rows(0).Item("PrincipalAmount"))
            'OTRAmount = IIf(IsDBNull(dt.Rows(0).Item("OTRAmount")), 0, dt.Rows(0).Item("OTRAmount"))
            'NTFAmount = IIf(IsDBNull(dt.Rows(0).Item("NTF")), 0, dt.Rows(0).Item("NTF"))
            'TotalAccount = IIf(IsDBNull(dt.Rows(0).Item("TotalAccount")), 0, dt.Rows(0).Item("TotalAccount"))

            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spFundingGetSecurityType"
            End With

            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            SecurityPercentage = dt.Rows(0).Item("SecurityCoveragePercentage")
            SecurityType = CStr(dt.Rows(0).Item("SecurityCoverageType")).Trim


            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetDrawDown"
            End With

            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            DrawDownAmount = dt.Rows(0).Item("PrincipalAmtToFunCoy")
            OutstandingLoan = dt.Rows(0).Item("OSAmtToFunCoy")
            Me.FacilityKind = dt.Rows(0).Item("FacilityKind")

            If Me.FacilityKind = "TLOAN" Then
                Me.PlafondAmount = (DrawDownAmount * SecurityPercentage) / 100
            Else
                Me.PlafondAmount = DrawDownAmount
            End If

            'Me.AmountMustBeAdd = Me.PlafondAmount - PrincipalAmount             

            'Select Case SecurityType
            '    Case "0"
            '        lblSecurityPercentageOf.Text = "Principal Amount"
            '        AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - PrincipalAmount
            '        Me.AmountMustBeAdd = AmountMustBeAdd
            '    Case "1"
            '        lblSecurityPercentageOf.Text = "NTF"
            '        AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - NTFAmount
            '        Me.AmountMustBeAdd = AmountMustBeAdd
            '    Case "2"
            '        lblSecurityPercentageOf.Text = "OTR Amount"
            '        AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - OTRAmount
            '        Me.AmountMustBeAdd = AmountMustBeAdd
            'End Select


            'Select Case SecurityType
            '    Case "0"
            '        lblSecurityPercentageOf.Text = "Outstanding Principal"

            '    Case "1"
            '        lblSecurityPercentageOf.Text = "NTF"

            '    Case "2"
            '        lblSecurityPercentageOf.Text = "OTR"

            'End Select
            '' nilai principal bisa berarti OS principal, NTF atau OTR tergantung pada SP spFundingTotalAgreement
            'AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - PrincipalAmount
            'Me.AmountMustBeAdd = AmountMustBeAdd


            lblOutstandingLoan.Text = FormatNumber(OutstandingLoan, 0).Trim
            'lblAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0).Trim
            lblTotalAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0).Trim
            lblSecurityPercentage.Text = FormatNumber(SecurityPercentage, 0).Trim
            'lblPrincipalAmount.Text = FormatNumber(PrincipalAmount, 0).Trim
            'lblARAmount.Text = FormatNumber(ARAmount, 0).Trim
            'lblOTRAmount.Text = FormatNumber(OTRAmount, 0).Trim
            lblAccount.Text = FormatNumber(TotalAccount, 0).Trim
            lblDrawDownAmount.Text = FormatNumber(DrawDownAmount, 0).Trim

            'With oGeneral
            '    .strConnection = GetConnectionString()
            '    .WhereCond1 = "faAgreement.FundingCoyId='" & Me.CompanyID & "' and faAgreement.BankId='" & Me.BankID & "' and faAgreement.FundingContractNo='" & Me.FundingContractNo & "'"
            '    .WhereCond2 = "faAgreement.FundingCoyId='" & Me.CompanyID & "' and faAgreement.BankId='" & Me.BankID & "' and faAgreement.FundingContractNo='" & Me.FundingContractNo & "' and faAgreement.FundingBatchNo='" & Me.FundingBatchNo & "'"
            '    .WhereCond3 = "faAgreement.FundingCoyId='" & Me.CompanyID & "' and faAgreement.BankId='" & Me.BankID & "' and faAgreement.FundingContractNo='" & Me.FundingContractNo & "' and faAgreement.FundingBatchNo='" & Me.FundingBatchNo & "'"
            '    .CurrentPage = currentPage
            '    .PageSize = pageSize
            '    .SortBy = ""
            '    .SpName = "spFundingTotalAgreementPerBranch"
            'End With

            'oGeneral = cGeneral.GetGeneralSP(oGeneral)
            'dt = oGeneral.ListData

            'lblBranchPrincipalAmt.Text = 0
            'lblBranchOTRAmount.Text = 0
            'lblBranchARAmount.Text = 0
            'lblBranchAccount.Text = 0

            'If dt.Rows.Count > 0 Then
            '    If CStr(dt.Rows(0).Item("BranchID")).Trim = drdBranch.SelectedItem.Value Then
            '        lblBranchPrincipalAmt.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmount"), 0)
            '        lblBranchOTRAmount.Text = FormatNumber(dt.Rows(0).Item("OTRAmount"), 0)
            '        lblBranchARAmount.Text = FormatNumber(dt.Rows(0).Item("ARAmount"), 0)
            '        lblBranchAccount.Text = CStr(dt.Rows(0).Item("TotalAccount"))
            '    End If
            'End If

            CheckISExecutedAndJFINC()
            'Response.Write(GenerateBranchChangeJavaScript(dt))

            'If AmountMustBeAdd > 0 Then
            '    rvAddAmount.MaximumValue = AmountMustBeAdd
            'Else
            '    rvAddAmount.MaximumValue = 1
            'End If

            If Me.AmountMustBeAdd < 1 Then
                txtAdditionalBranchAmount.Enabled = False
                btnAgreementSelection.Visible = False
            End If

            'If drdBranch.Items.Count = 0 Then
            '    txtAdditionalBranchAmount.Enabled = False
            '    btnAgreementSelection.Visible = False

            '    ShowMessage(lblMessage, "Harap isi Plafon Cabang Terlebih dahulu ", True)
            'End If
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            btnAgreementSelection.Visible = False

        End Try
    End Sub


#Region "BindComboBranch"
    Private Sub bindComboIDbranch()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBranchIn"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            Dim i As Integer


            drdBranch.DataValueField = "BranchID"
            drdBranch.DataTextField = "BranchFullName"

            drdBranch.DataSource = dtvEntity

            drdBranch.DataBind()
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
#End Region

#Region "GenerateBranchChangeJavaScript"
    Private Function GenerateBranchChangeJavaScript(ByVal dt As DataTable) As String
        Dim strScript As String
        Dim DataRow As DataRow
        Dim i As Integer


        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript += "function drdBranchChange(){" & vbCrLf
        strScript += "document.all['lblBranchPrincipalAmt'].innerHTML = '0';" & vbCrLf
        strScript += "document.all['lblBranchOTRAmount'].innerHTML = '0';" & vbCrLf
        strScript += "document.all['lblBranchARAmount'].innerHTML = '0';" & vbCrLf
        strScript += "document.all['lblBranchAccount'].innerHTML = '0';" & vbCrLf
        i = 0
        For Each DataRow In dt.Rows
            If i = 0 Then
                strScript += "if (document.Form1.drdBranch.value =='" & dt.Rows(i).Item("BranchID") & "') {" & vbCrLf
            Else
                strScript += "}else if (document.Form1.drdBranch.value =='" & dt.Rows(i).Item("BranchID") & "') {" & vbCrLf
            End If
            strScript += "document.all['lblBranchPrincipalAmt'].innerHTML = '" & FormatNumber(dt.Rows(i).Item("PrincipalAmount"), 0) & "';" & vbCrLf
            strScript += "document.all['lblBranchOTRAmount'].innerHTML = '" & FormatNumber(dt.Rows(i).Item("OTRAmount"), 0) & "';" & vbCrLf
            strScript += "document.all['lblBranchARAmount'].innerHTML = '" & FormatNumber(dt.Rows(i).Item("ARAmount"), 0) & "';" & vbCrLf
            strScript += "document.all['lblBranchAccount'].innerHTML = '" & CStr(dt.Rows(i).Item("TotalAccount")) & "';" & vbCrLf

            If i = (dt.Rows.Count - 1) Then
                strScript += "}"
            End If

            i += 1

        Next
        strScript += "}"
        strScript &= "</script>"
        Return strScript

    End Function

#End Region


#End Region

    Private Sub btnAgreementSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgreementSelection.Click
        PanelAllFalse()
        UpdateAgreementSelected()
        DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
        DisplayHeaderInfo()
        pnlAddDetail.Visible = True
        PnlSearchBy.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub drdBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drdBranch.SelectedIndexChanged
        'pnlAdd.Visible = True
        'DisplayHeaderInfo()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oDataTable As New DataTable
        Dim customClass As New Parameter.FundingContractBatch
        Dim customClass1 As New Parameter.FundingContractBatch
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chk As CheckBox
        Dim lblApplicationID As New Label
        Dim txtOSPRINCIPAL As New Label

        With oDataTable
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("OutstandingPrincipal", GetType(Decimal)))
            .Columns.Add(New DataColumn("IsChecked", GetType(Boolean)))
        End With

        For intloop = 0 To dtgFundingAgreementList.Items.Count - 1
            chk = CType(dtgFundingAgreementList.Items(intloop).FindControl("chkSelect"), CheckBox)
            lblApplicationID = CType(dtgFundingAgreementList.Items(intloop).FindControl("lblApplicationID"), Label)
            txtOSPRINCIPAL = CType(dtgFundingAgreementList.Items(intloop).FindControl("txtOSPRINCIPAL"), Label)

            oRow = oDataTable.NewRow
            oRow("ApplicationID") = lblApplicationID.Text.Trim
            oRow("OutstandingPrincipal") = txtOSPRINCIPAL.Text.Trim
            oRow("IsChecked") = chk.Checked
            oDataTable.Rows.Add(oRow)
        Next


        If oDataTable.Rows.Count > 0 Then
            '    Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo)
            'Else
            Try
                TotalPokokPerPage = 0
                With customClass
                    .strConnection = GetConnectionString()
                    .FundingCoyId = Me.CompanyID.Trim
                    .FundingContractNo = Me.FundingContractNo.Trim
                    .FundingBatchNo = Me.FundingBatchNo.Trim
                    .Listdata = oDataTable
                End With

                m_Company.FundingAgreementSelect(customClass)

                'If FacilityKind.Trim = "JFINC" Then
                '    JFINC()
                'ElseIf FacilityKind.Trim = "TLOAN" Then

                'End If

                'PanelAllFalse()
                'pnlList.Visible = True
                'PnlSearchBy.Visible = True
                'DisplayHeaderInfo()
                'DisplaySelected()
                'DisplayTotalSelected()

                'pnlAddDetail.Visible = False
                'dvsearch.Visible = False
                'btnSearchAgreement.Visible = False
                'btnSelection.Text = "Selection"

                'For intloop = 0 To dtgFundingAgreementList.Items.Count - 1
                '    TotalPokokPerPage += CDbl(dtgFundingAgreementList.Items(intloop).Cells(5).Text)
                '    chk = CType(dtgFundingAgreementList.Items(intloop).FindControl("chkSelect"), CheckBox)
                '    chk.Checked = True
                'Next

                Response.Redirect("FundingAgreementSelectedTL.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" &
                              Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" &
                              Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName &
                              "&Plafond=" & Me.PlafondAmount)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo)
        Response.Redirect("FundingAgreementSelectedTL.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo & "&Plafond=" & Me.PlafondAmount)
    End Sub

    Private Sub btnCancelView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelView.Click
        'Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo)
        Response.Redirect("FundingAgreementSelectedTL.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo & "&Plafond=" & Me.PlafondAmount)
    End Sub

    Private Sub dtgFundingAgreementList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingAgreementList.ItemCommand
        Select Case e.CommandName
            Case "ViewAgreement"
                Dim lnkAgreementNo As LinkButton
                lnkAgreementNo = CType(dtgFundingAgreementList.Items(e.Item.ItemIndex).FindControl("lnkAgreementNo"), LinkButton)
                Response.Redirect("../../AccAcq/Credit/ViewStatementOfAccount.aspx?Style=Channeling.css&AgreementNo=" & lnkAgreementNo.Text)
            Case "ViewCustomer"
                Response.Redirect("../../AccAcq/Credit/ViewPersonalCustomer.aspx?Style=Channeling.css&CustomerID=" & e.Item.Cells(0).Text)
        End Select
    End Sub

    Private Sub dtgFundingAgreementList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingAgreementList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label
        Dim ChkBox As CheckBox
        Dim lblFundingPledgeStatus As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCustName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)


            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Channeling" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "Channeling" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"

            ChkBox = CType(e.Item.Cells(0).FindControl("chkSelect"), CheckBox)


            lblFundingPledgeStatus = CType(e.Item.Cells(0).FindControl("lblFundingPledgeStatus"), Label)

            'If lblFundingPledgeStatus.Text.Trim = "S" And btnSelection.Text = "Selection" Or lblFundingPledgeStatus.Text.Trim = "-" And btnSelection.Text = "Selection" Or lblFundingPledgeStatus.Text.Trim = "" And btnSelection.Text = "Selection" Then
            '    ChkBox.Enabled = True
            '    ChkBox.Checked = True
            'ElseIf btnSelection.Text = "Selected" Then
            '    ChkBox.Enabled = True
            '    ChkBox.Checked = False
            'Else
            '    ChkBox.Enabled = False
            '    ChkBox.Checked = False
            'End If

            If ChkBox.Checked Then
                TotalPokokPerPage += CDbl(e.Item.Cells(5).Text)
            End If

        End If

    End Sub

    Public Sub CheckISExecutedAndJFINC()
        Dim Status As String
        Dim FacilityKind As String
        Dim objCommand As New SqlCommand
        Dim objCommand1 As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objConnection1 As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingCheckFundingBatchInstallment"
            objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Parameters.Add("@status", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@Tenor", SqlDbType.SmallInt).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@InterestRate", SqlDbType.Float).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Status = IIf(IsDBNull(objCommand.Parameters("@status").Value), "S", objCommand.Parameters("@status").Value)
            Me.TenorMax = IIf(IsDBNull(objCommand.Parameters("@Tenor").Value), 0, objCommand.Parameters("@Tenor").Value)
            Me.InterestRate = IIf(IsDBNull(objCommand.Parameters("@InterestRate").Value), 0, objCommand.Parameters("@InterestRate").Value)
        Catch ex As Exception
            'Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        Try
            If objConnection1.State = ConnectionState.Closed Then objConnection1.Open()
            objCommand1.CommandType = CommandType.StoredProcedure
            objCommand1.CommandText = "spFundingCheckFundingContractFacilityKind"
            objCommand1.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand1.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand1.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand1.Parameters.Add("@FacilityKind", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output
            objCommand1.Connection = objConnection1
            objCommand1.ExecuteNonQuery()
            FacilityKind = IIf(IsDBNull(objCommand1.Parameters("@FacilityKind").Value), "", objCommand1.Parameters("@FacilityKind").Value)
            Me.FacilityKind = FacilityKind.Trim
        Catch ex As Exception
            'Response.Write(ex.Message)
        Finally
            If objConnection1.State = ConnectionState.Open Then objConnection1.Close()
            objConnection1.Dispose()
            objCommand1.Dispose()
        End Try
        If Status.Trim = "D" And FacilityKind.Trim = "JFINC" Then
            Dim dtslist As DataTable
            Dim cReceive As New FundingCompanyController
            Dim oReceive As New Parameter.FundingContractBatch
            Dim oCustomClass As New Parameter.FundingContractBatch
            Dim orow As DataRow
            Try
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .BusinessDate = Me.BusinessDate
                    .SpName = "spFundingGetOSSelectingAmount"
                End With
                oReceive = cReceive.GetGeneralEditView(oCustomClass)
                dt = oReceive.Listdata
                If dt.Rows.Count > 0 Then
                    orow = dt.Rows(0)
                    If CStr(dt.Rows(0).Item("BranchID")).Trim = drdBranch.SelectedItem.Value Then
                        If Not IsDBNull(orow("PrincipalAmount")) Then
                            lblBranchPrincipalAmt.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmount"), 0)
                        End If
                        If Not IsDBNull(orow("ARAmount")) Then
                            lblBranchARAmount.Text = FormatNumber(dt.Rows(0).Item("ARAmount"), 0)
                        End If
                        If Not IsDBNull(orow("ARAmount")) Then
                            lblBranchOTRAmount.Text = FormatNumber(dt.Rows(0).Item("OTRAmount"), 0)
                        End If
                        If Not IsDBNull(orow("ARAmount")) Then
                            lblBranchAccount.Text = CStr(dt.Rows(0).Item("TotalAccount"))
                        End If

                    End If
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If


    End Sub

    Private Sub btnSearchAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchAgreement.Click
        TotalPokokPerPage = 0

        Me.SearchBy = ""

        If cbosearch.SelectedItem.Value.Trim <> "0" And txtsearch.Text.Trim <> "" Then
            Me.SearchBy = cbosearch.SelectedItem.Value & " LIKE '%" & txtsearch.Text.Trim & "%'"
        End If

        If DateFrom.Text.Trim <> "" And DateTo.Text.Trim <> "" Then
            If Me.SearchBy.Trim <> "" Then Me.SearchBy &= " AND "
            Me.SearchBy &= " CONVERT(CHAR(10),Agreement.GoLiveDate,121) BETWEEN '" & ConvertDate2(DateFrom.Text).ToString("yyyy-MM-dd") & "' AND '" & ConvertDate2(DateTo.Text).ToString("yyyy-MM-dd") & "' "
        End If

        'di remark dulu.. kalo sudah ok di lepas lagi ya.. by hendrik
        'If cbostsdok.SelectedItem.Value.Trim <> "A" Then
        '    If Me.SearchBy.Trim <> "" Then Me.SearchBy &= " AND "
        '    Me.SearchBy &= " status.Status = '" & cbostsdok.SelectedItem.Value.Trim & "'"
        'Else
        '    If Me.SearchBy.Trim <> "" Then Me.SearchBy &= " AND "
        '    Me.SearchBy &= " status.Status IN ('O','I') "
        'End If

        DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
        DisplayHeaderInfo()

        DisplayTotalSelected()
        pnlAddDetail.Visible = True
        PnlSearchBy.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub btnSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelection.Click
        Response.Redirect("FundingAgreementSelectedTL.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" &
                              Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" &
                              Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName &
                              "&Plafond=" & Me.PlafondAmount)
        'If btnSelection.Text = "Selection" Then
        '    pnlAddDetail.Visible = True
        '    PnlSearchBy.Visible = True
        '    dvsearch.Visible = True
        '    btnSearchAgreement.Visible = True
        '    pnlList.Visible = True
        '    btnSelection.Text = "Selected"

        '    DisplayHeaderInfo()
        '    dtgFundingAgreementList.DataSource = Nothing
        '    dtgFundingAgreementList.DataBind()
        '    DisplayTotalSelected()
        'Else
        '    pnlAddDetail.Visible = False
        '    PnlSearchBy.Visible = True
        '    dvsearch.Visible = False
        '    btnSearchAgreement.Visible = False
        '    pnlList.Visible = True
        '    btnSelection.Text = "Selection"

        '    TotalPokokPerPage = 0
        '    DisplayHeaderInfo()
        '    DisplaySelected()
        '    DisplayTotalSelected()
        'End If

    End Sub

    Private Sub DisplaySelected()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New FundingCompanyController
        Dim oContract As New Parameter.FundingContractBatch


        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .Filter = "1"
        End With

        oContract = cContract.FundingAgreementList(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oContract.Listdata
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgFundingAgreementList.DataSource = dtvEntity

        Try
            dtgFundingAgreementList.DataBind()
        Catch
            dtgFundingAgreementList.CurrentPageIndex = 0
            dtgFundingAgreementList.DataBind()
        End Try

        PagingFooter()
    End Sub

    Private Sub DisplayTotalSelected()
        Try
            Dim cContract As New FundingCompanyController
            Dim oContract As New Parameter.FundingContractBatch

            With oContract
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            oContract = cContract.FundingTotalSelected(oContract)
            dt = oContract.Listdata


            TotalAR = IIf(IsDBNull(dt.Rows(0).Item("ARAmount")), 0, dt.Rows(0).Item("ARAmount"))
            TotalPokok = IIf(IsDBNull(dt.Rows(0).Item("PrincipalAmount")), 0, dt.Rows(0).Item("PrincipalAmount"))
            TotalOTR = IIf(IsDBNull(dt.Rows(0).Item("OTRAmount")), 0, dt.Rows(0).Item("OTRAmount"))
            TotalNTF = IIf(IsDBNull(dt.Rows(0).Item("NTF")), 0, dt.Rows(0).Item("NTF"))
            TotalAccounts = IIf(IsDBNull(dt.Rows(0).Item("TotalAccount")), 0, dt.Rows(0).Item("TotalAccount"))
            Me.AmountMustBeAdd = PlafondAmount - TotalPokok

            lblPrincipalAmountL.Text = FormatNumber(TotalPokok, 0)
            lblARAmountL.Text = FormatNumber(TotalAR, 0)
            lblOTRAmountL.Text = FormatNumber(TotalOTR, 0)
            lblNTFtl.Text = FormatNumber(TotalNTF, 0)
            lblTotalAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0).Trim
            lblAccountL.Text = CStr(TotalAccounts)
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

    Sub DisplayTotalSelectedByCheckBox()
        Dim pokok As Double
        Dim ar As Double
        Dim otr As Double
        Dim ntf As Double
        Dim totalacc As Integer

        TotalChecked(pokok, ar, otr, ntf, totalacc)

        'lblPrincipalAmountL.Text = FormatNumber(pokok)
        'lblPrincipalAmountL.Text = FormatNumber(TotalPokok)
        'lblARAmountL.Text = FormatNumber(TotalAR, 0)
        'lblOTRAmountL.Text = FormatNumber(TotalOTR, 0)
        'lblNTFtl.Text = FormatNumber(TotalNTF, 0)
        lblAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0)
        'lblAccountL.Text = FormatNumber(TotalAccounts, 0)


    End Sub
    Sub DisplayTotalSelectedBychkAll()
        Dim pokok As Double
        Dim ar As Double
        Dim otr As Double
        Dim ntf As Double
        Dim totalacc As Integer

        TotalChecked(pokok, ar, otr, ntf, totalacc)

        'lblPrincipalAmountL.Text = FormatNumber(pokok)
        'lblPrincipalAmountL.Text = FormatNumber(TotalPokok)
        lblARAmountL.Text = FormatNumber(TotalAR, 0)
        lblOTRAmountL.Text = FormatNumber(TotalOTR, 0)
        lblNTFtl.Text = FormatNumber(TotalNTF, 0)
        lblAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0)
        lblAccountL.Text = FormatNumber(TotalAccounts, 0)


    End Sub

    Sub TotalChecked(pokok As Double, ar As Double, otr As Double, ntf As Double, totalacc As Integer)
        Dim chkSelect As CheckBox
        pokok = 0
        otr = 0
        ar = 0
        totalacc = 0

        For Each row As DataGridItem In dtgFundingAgreementList.Items
            chkSelect = CType(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                ar += CDbl(row.Cells(3).Text)
                pokok += CDbl(row.Cells(5).Text)
                otr += CDbl(row.Cells(6).Text)
                totalacc = totalacc + 1
            End If
        Next
        'Modify by Wira 20180802, Request dari Pak Agus
        'If TotalPokok - TotalPokokPerPage + pokok > PlafondAmount Then
        '    ShowMessage(lblMessage, "Nilai kontrak melebihi nilai plafond!", True)
        '    btnSave.Visible = False
        'Else

        lblPrincipalAmountL.Text = FormatNumber(TotalPokok - TotalPokokPerPage + pokok, 2)
        lblTotalAmountMustBeAdd.Text = FormatNumber(PlafondAmount - (TotalPokok - TotalPokokPerPage + pokok), 2)
        lblARAmountL.Text = FormatNumber(ar, 2)
        lblOTRAmountL.Text = FormatNumber(otr, 2)
        lblAccountL.Text = FormatNumber(totalacc, 2)
        btnSave.Visible = True
        'End If

    End Sub
    Sub JFINC()
        Dim oDataTable As New DataTable
        Dim customClass As New Parameter.FundingContractBatch
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chk As CheckBox
        Dim lblApplicationID As New Label
        Dim ApplicationID As String = ""
        With oDataTable
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("IsChecked", GetType(Boolean)))
        End With
        For intloop = 0 To dtgFundingAgreementList.Items.Count - 1
            chk = CType(dtgFundingAgreementList.Items(intloop).FindControl("chkSelect"), CheckBox)
            lblApplicationID = CType(dtgFundingAgreementList.Items(intloop).FindControl("lblApplicationID"), Label)
            If chk.Checked Then
                oRow = oDataTable.NewRow
                oRow("ApplicationID") = CType(dtgFundingAgreementList.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                oDataTable.Rows.Add(oRow)
                If ApplicationID.Trim = "" Then
                    ApplicationID = "'" & CType(oRow("ApplicationID"), String) & "'"
                Else
                    ApplicationID = ApplicationID & ",'" & CType(oRow("ApplicationID"), String) & "'"
                End If
            End If
        Next

        ApplicationID = "ApplicationID in (" & ApplicationID & ")"
        ApplicationID = ApplicationID

        With customClass
            .strConnection = GetConnectionString()
            .ApplicationID = ApplicationID
        End With

        m_Company.FundingAgreementJnFnc(customClass)

    End Sub

End Class