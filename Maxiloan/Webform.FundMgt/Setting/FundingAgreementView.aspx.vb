﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingAgreementView
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
    Dim totalInstallmentAmount As Double
    Dim totalPRINCIPALAMOUNT As Double
    Dim totalINTERESTAMOUNT As Double
    Dim totalPRINCIPALPAIDAMOUNT As Double
    Dim totalINTERESTPAIDAMOUNT As Double
    Dim totalOSPrincipalAmount As Double
    Dim totalOSInterestAmount As Double

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        InitialDefaultPanel()

        If Not Me.IsPostBack Then

            Me.FormID = "FundingBatch"

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")

            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")


            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""

            'BindGridEntity(Me.SearchBy, Me.SortBy)
            'pnlList.Visible = True
            If Command = "INS" Then
                Me.ActionAddEdit = "INS"
                pnlInsList.Visible = True
                BindGridEntity(Me.SearchBy, Me.SortBy)
            Else

            End If

        End If
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()

                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)

        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlInsList.Visible = False
        lblMessage.Visible = False

    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlInsList.Visible = True
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingBatchInstallmentList"
        End With

        Try
            oContract = cContract.GetGeneralPaging(oContract)
        Catch e As Exception
           showmessage(lblmessage, e.message , true)
        End Try

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            lblDrawDownDate.Text = CStr(dtsEntity.Rows(0).Item("BatchDate")).Trim
            lblDrawdownAmount.Text = FormatNumber(dtsEntity.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
            lblNumOfAccount.Text = FormatNumber(dtsEntity.Rows(0).Item("AccRealizednum"), 0)
            LblInterestRate.Text = FormatNumber(dtsEntity.Rows(0).Item("InterestRate"), 2)
            LblSecPercentage.Text = FormatNumber(dtsEntity.Rows(0).Item("SecurityCoveragePercentage"), 2)
            LblSecPercentageBaseOf.Text = CStr(dtsEntity.Rows(0).Item("SecurityCoverageType")).Trim
            'imgPrint.Enabled = True
        End If

        dtgInstallment.DataSource = dtvEntity
        Try
            dtgInstallment.DataBind()
        Catch
            dtgInstallment.CurrentPageIndex = 0
            dtgInstallment.DataBind()
        End Try
        bindTitleTop()
        PagingFooter()
    End Sub
#End Region

#Region "MISC"

#Region "BindTitleTop"
    Private Sub bindTitleTop()
        lblBankName.Text = Me.BankName
        lblFundingCoyName.Text = Me.CompanyName
        lblFundingContractNo.Text = Me.FundingContractNo
        lblContractName.Text = Me.ContractName
        lblBatchNo.Text = Me.FundingBatchNo
    End Sub
#End Region

#End Region

    Private Sub dtgInstallment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInstallment.ItemDataBound
        Dim lblINSTALLMENTAMOUNT As Label
        Dim lblPRINCIPALAMOUNT As Label
        Dim lblINTERESTAMOUNT As Label
        Dim lblPRINCIPALPAIDAMOUNT As Label
        Dim lblINTERESTPAIDAMOUNT As Label
        Dim lblOSPrincipalAmount As Label
        Dim lblOSInterestAmount As Label

        Dim lblTotINSTALLMENTAMOUNT As Label
        Dim lblTotPRINCIPALAMOUNT As Label
        Dim lblTotINTERESTAMOUNT As Label
        Dim lblTotPRINCIPALPAIDAMOUNT As Label
        Dim lblTotINTERESTPAIDAMOUNT As Label
        Dim lblTotOSPrincipalAmount As Label
        Dim lblTotOSInterestAmount As Label



        Dim hyRequestNo As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblINSTALLMENTAMOUNT = CType(e.Item.FindControl("lblINSTALLMENTAMOUNT"), Label)
            lblPRINCIPALAMOUNT = CType(e.Item.FindControl("lblPRINCIPALAMOUNT"), Label)
            lblINTERESTAMOUNT = CType(e.Item.FindControl("lblINTERESTAMOUNT"), Label)
            lblPRINCIPALPAIDAMOUNT = CType(e.Item.FindControl("lblPRINCIPALPAIDAMOUNT"), Label)
            lblINTERESTPAIDAMOUNT = CType(e.Item.FindControl("lblINTERESTPAIDAMOUNT"), Label)
            lblOSPrincipalAmount = CType(e.Item.FindControl("lblOSPrincipalAmount"), Label)
            lblOSInterestAmount = CType(e.Item.FindControl("lblOSInterestAmount"), Label)

            totalInstallmentAmount += CDbl(lblINSTALLMENTAMOUNT.Text.Replace(",", ""))
            totalPRINCIPALAMOUNT += CDbl(lblPRINCIPALAMOUNT.Text.Replace(",", ""))
            totalINTERESTAMOUNT += CDbl(lblINTERESTAMOUNT.Text.Replace(",", ""))
            totalPRINCIPALPAIDAMOUNT += CDbl(lblPRINCIPALPAIDAMOUNT.Text.Replace(",", ""))
            totalINTERESTPAIDAMOUNT += CDbl(lblINTERESTPAIDAMOUNT.Text.Replace(",", ""))
            totalOSPrincipalAmount += CDbl(lblOSPrincipalAmount.Text.Replace(",", ""))
            totalOSInterestAmount += CDbl(lblOSInterestAmount.Text.Replace(",", ""))

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotINSTALLMENTAMOUNT = CType(e.Item.FindControl("lblTotINSTALLMENTAMOUNT"), Label)
            lblTotPRINCIPALAMOUNT = CType(e.Item.FindControl("lblTotPRINCIPALAMOUNT"), Label)
            lblTotINTERESTAMOUNT = CType(e.Item.FindControl("lblTotINTERESTAMOUNT"), Label)
            lblTotPRINCIPALPAIDAMOUNT = CType(e.Item.FindControl("lblTotPRINCIPALPAIDAMOUNT"), Label)
            lblTotINTERESTPAIDAMOUNT = CType(e.Item.FindControl("lblTotINTERESTPAIDAMOUNT"), Label)
            lblTotOSPrincipalAmount = CType(e.Item.FindControl("lblTotOSPrincipalAmount"), Label)
            lblTotOSInterestAmount = CType(e.Item.FindControl("lblTotOSInterestAmount"), Label)

            lblTotINSTALLMENTAMOUNT.Text = FormatNumber(totalInstallmentAmount, 2)
            lblTotPRINCIPALAMOUNT.Text = FormatNumber(totalPRINCIPALAMOUNT, 2)
            lblTotINTERESTAMOUNT.Text = FormatNumber(totalINTERESTAMOUNT, 2)
            lblTotPRINCIPALPAIDAMOUNT.Text = FormatNumber(totalPRINCIPALPAIDAMOUNT, 2)
            lblTotINTERESTPAIDAMOUNT.Text = FormatNumber(totalINTERESTPAIDAMOUNT, 2)
            lblTotOSPrincipalAmount.Text = FormatNumber(totalOSPrincipalAmount, 2)
            lblTotOSInterestAmount.Text = FormatNumber(totalOSInterestAmount, 2)
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../View/FundingBatchView.aspx?CompanyID=" & Me.CompanyID.Trim & "&CompanyName=" & Me.CompanyName.Trim & "&" & _
                         "BankName=" & Me.BankName.Trim & "&BankID=" & Me.BankID.Trim & "&ContractName=" & Me.ContractName.Trim & "&" & _
                         "FundingContractNo=" & Me.FundingContractNo.Trim & "&FundingBatchNo=" & Me.FundingBatchNo.Trim & "")
    End Sub
End Class