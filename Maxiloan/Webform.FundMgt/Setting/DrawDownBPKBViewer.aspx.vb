﻿Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports System.Data.SqlClient
Public Class DrawDownBPKBViewer
    Inherits Maxiloan.Webform.WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub
        GetCookies()
      
    End Sub


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Reports")
        Dim MultiApplicationID = cookie.Values("MultiApplicationID")
        Dim dt = New DataTable()
        Dim strArray As String() = New String() {"id", "value1", "value2"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next

        Dim ids As String() = MultiApplicationID.Split(New Char() {","c})

        For Each v In ids
            Dim row = dt.NewRow()
            row("id") = v.Trim().Replace("'", "")
            row("value1") = ""
            row("value2") = ""
            dt.Rows.Add(row)
        Next

        Dim ds = GetData(dt) 
        Dim objReport As DrawdownBPKBPrint = New DrawdownBPKBPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions


        objReport.SetDataSource(ds)
        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()


        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "DrawDownBPKB.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()


        Response.Redirect("DrawDownBPKB.aspx?filekwitansi=" & Me.Session.SessionID & Me.Loginid & "DrawDownBPKB")
 
    End Sub
    Public Function GetData(data As DataTable) As DataSet
        Dim ds As New DataSet
        Dim adapter As New SqlDataAdapter
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)

        Try
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objcon
            objCommand.CommandText = "sp_DaftarPengembalianBPKB"
            objCommand.Parameters.Add("@NoPinjaman", SqlDbType.Structured).Value = data
            objCommand.CommandTimeout = 60

            adapter.SelectCommand = objCommand
            adapter.Fill(ds)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try

        Return ds
    End Function

 
End Class