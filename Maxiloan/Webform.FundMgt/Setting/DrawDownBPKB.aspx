﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DrawDownBPKB.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.DrawDownBPKB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DrawDownBPKB</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingCompanyView(Style, CompanyID) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyView.aspx?Style=' + Style + '&CompanyID=' + CompanyID, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1,scrollbars=1');
        }
        function OpenFundingCompanyContractView(pStyle, pCompanyName, pBankName, pFundingContractNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyContractView.aspx?Style=' + pStyle + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&FundingContractNo=' + pFundingContractNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function OpenFundingBatchView(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingBatchView.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }							
    </script>
    <script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah mau hapus data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }	
			-->
    </script>
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms[0].' + tampungGrandChild2).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" runat="server" name="tempChild" />
    <input id="tempChildName" type="hidden" runat="server" name="tempChildName" />
    <input id="tempChild2" type="hidden" runat="server" name="tempChild2" />
    <input id="tempGrandChild" type="hidden" runat="server" name="tempGrandChild" />
    <input id="tempGrandChildName" type="hidden" runat="server" name="tempGrandChildName" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    PENGAMBILAN KEMBALI DOKUMEN ASSET
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank / Cabang</label>
                <asp:DropDownList ID="drdCompany" runat="server" onchange="<%#drdCompanyChange()%>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas Bank</label>
                <asp:DropDownList ID="drdContract" runat="server" onchange="<%#drdContractChange()%>">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:DropDownList ID="drdBatchDate" runat="server" onchange="setCboBatchDate(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Dokumen</label>
                <asp:DropDownList ID="cboDocStatus" runat="server">
                    <asp:ListItem Value="All" Selected="true">ALL</asp:ListItem>
                    <asp:ListItem Value="P">Prospect</asp:ListItem>
                    <asp:ListItem Value="O">On Hand</asp:ListItem>
                    <asp:ListItem Value="B">Borrow</asp:ListItem>
                    <asp:ListItem Value="R">Release</asp:ListItem>
                    <asp:ListItem Value="W">Waiting</asp:ListItem>
                    <asp:ListItem Value="G">Pledging</asp:ListItem>
                    <asp:ListItem Value="I">Intransit</asp:ListItem>
                    <asp:ListItem Value="C">Cancel</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Cetak</label>
                <asp:DropDownList ID="cboPrintedStatus" runat="server">
                    <asp:ListItem Value="All" Selected="true">ALL</asp:ListItem>
                    <asp:ListItem Value="P">Printed</asp:ListItem>
                    <asp:ListItem Value="U">Un Printed</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Pilihan Cetak</label>
                <asp:RadioButtonList ID="rdoPrintType" runat="Server" RepeatDirection="Horizontal"
                    CssClass="opt_single">
                    <asp:ListItem Value="All" Text="Print All Pages"></asp:ListItem>
                    <asp:ListItem Value="Page" Selected="True" Text="Print per Page"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="AgreementNo">Agreement No.</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="128px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSelect" runat="server" Text="Select" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlSearchDetail" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PENGAMBILAN KEMBALI DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank / Cabang</label>
                <asp:Label ID="lblFundingCoy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas Bank</label>
                <asp:Label ID="LblFundingContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:Label ID="LblFundingBatch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Dokumen</label>
                <asp:Label ID="LblDocStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Cetak</label>
                <asp:Label ID="LblPrintedStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Pilihan Cetak</label>
                <asp:Label ID="LblPrintType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnExit" runat="server" Text="Exit" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PENGAMBILAN KEMBALI DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" Width="100%" CssClass="grid_general"
                        CellPadding="3" BorderWidth="0px" CellSpacing="1" OnSortCommand="Sorting" AllowSorting="True"
                        AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="APPLICATIONID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Enabled="True" Text='<%# Container.dataitem("ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CustomerID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Enabled="True" Text='<%# Container.dataitem("CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCust" runat="server" Enabled="True" Text='<%# Container.dataitem("Name") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OSAR" HeaderText="SISA PIUTANG" DataFormatString="{0:N0}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OUTSTANDINGPRINCIPAL" HeaderText="SISA POKOK" DataFormatString="{0:N0}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InstallmentPaid" HeaderText="No Angs Lunas">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NextInstallmentDueDate" HeaderText="JT TERAKHIR" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DocumentNo" HeaderText="NO BPKB">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="BranchID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblBranchID" runat="server" Enabled="True" Text='<%# Container.dataitem("BranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetDocLocation" HeaderText="LOKASI BPKB">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AssetDocStatus" HeaderText="STATUS DOK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="AssetDocStatusID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblAssetDocStatusID" runat="server" Enabled="True" Text='<%# Container.dataitem("AssetDocStatusID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ALASAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblBPKBDrawReason" runat="server" Enabled="True" Text='<%# Container.dataitem("BPKBDrawReason") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" CommandName="First"
                        ImageUrl="../../Images/grid_navbutton01.png" OnCommand="NavigationLink_Click" />
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" CommandName="Prev"
                        ImageUrl="../../Images/grid_navbutton02.png" OnCommand="NavigationLink_Click" />
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" CommandName="Next"
                        ImageUrl="../../Images/grid_navbutton03.png" OnCommand="NavigationLink_Click" />
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" CommandName="Last"
                        ImageUrl="../../Images/grid_navbutton04.png" OnCommand="NavigationLink_Click" />
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrintBawah" runat="server" CausesValidation="False" Text="Print"
                CssClass="small button green"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
