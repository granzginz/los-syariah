﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingAgreementSelected.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingAgreementSelected" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingAgreement</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }					
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnl1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR FASILITAS TERPILIH
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total A/R
                    </label>
                    <asp:Label ID="lblARAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right" style="display:none">
                    <label>
                        Jumlah harus ditambah
                    </label>
                    <asp:Label ID="lblTotalAmountMustBeAdd" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total Pokok
                    </label>
                    <asp:Label ID="lblPrincipalAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Debitur
                    </label>
                    <asp:Label ID="lblAccountL" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total OTR
                    </label>
                    <asp:Label ID="lblOTRAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Total NTF
                    </label>
                    <asp:Label ID="lblNTFtl" runat="server"></asp:Label>
                </div>

                
            </div>
            <div class="form_box" style="display:none">
                <div class="form_left" >
                    <label>
                        Total Plafond
                    </label>
                    <asp:Label ID="lbljumlahplafond" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    &nbsp;
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSelection" runat="server" Text="Selection" CssClass="small button blue" CausesValidation="False"></asp:Button>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button><a href="javascript:history.back();"> </a>

                <asp:FileUpload ID="FileUpload" Width="450px" runat="server" visible="true"/>
                <asp:Button ID="btnImport" runat="server" Text="Import Data" CssClass="small button blue" Visible="true">  </asp:Button>
            </div>
            <!--Here-->
            <asp:Panel ID="pnl1" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgSelected" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="AgreementNO">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NAME" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkCustName" runat="server" Enabled="True" Text='<%# Container.dataitem("CustName") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="OSAR" HeaderText="SISA A/R" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="SISA A/R" Visible="false">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblsisaAR" runat="server" Text='<%# Container.DataItem("OSAR")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="OSPRINCIPAL" HeaderText="POKOK HUTANG" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="TotalOTR" HeaderText="OTR" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <%--<asp:BoundColumn DataField="NTF" HeaderText="NTF" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <%--<asp:BoundColumn DataField="TENOR" HeaderText="JK WAKTU">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="Sisa" HeaderText="SISA TENOR">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                <asp:BoundColumn DataField="StatusBPKB" HeaderText="STATUS BPKB">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TahunKendaraan" HeaderText="TAHUN KENDARAAN">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                <asp:BoundColumn DataField="DESCRIPTION" HeaderText="MERK KENDARAAN">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="POKOK HUTANG" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                    <HeaderStyle ></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>

                                        <asp:TextBox runat="server" ID="txtOSPRINCIPAL" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                              onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true)" onfocus="this.value=resetNumber(this.value);" 
                                            CssClass="numberAlign small" OnChange="CalculatePokokHutang(this);" 
                                            autocomplete="off" Text='<%#FormatNumber(Container.DataItem("OutstandingPrincipalEdited"), 0)%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>                                    
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked = "true" OnCheckedChanged="DisplayTotalSelectedByCheckBox">
                                        </asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# Container.DataItem("ApplicationID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFundingPledgeStatus" runat="server" Text='<%# Container.DataItem("FundingPledgeStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            </asp:Panel>
            <div class="form_button">
                    <asp:Button ID="btnExecution" runat="server" Text="Execution" CssClass="small buttongo blue" Visible="false">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExel" runat="server"  Text="Export To Exel" CssClass="small button blue" visible="false"></asp:Button>
            <div id="divupload" runat="server">
        <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR UPLOAD KONTRAK
                    </h4>
                </div>
        </div>       
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgViewUploadFromExcel" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                    <asp:TemplateColumn SortExpression="" HeaderText="No PJJ" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoPJJ" runat="server" Text='<%# Container.DataItem("NoPJJ")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="OS Pokok" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOSPOkok" runat="server" Text='<%# Container.DataItem("OutstandingPrincipal")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="OS Margin" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOSBunga" runat="server" Text='<%# Container.DataItem("OutstandingInterest")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="Sisa Tenor" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSisaTenor" runat="server" Text='<%# Container.DataItem("SisaTenor")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="Rate Batch" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRateBatch" runat="server" Text='<%# Container.DataItem("RateBatch")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                </div>
            </div>
        </div>
           <div class="form_button">
                    <asp:Button ID="btnExecuteFromUpload" runat="server" Text="Execute" CssClass="small button blue" Visible="true">  </asp:Button>
                    <asp:Button ID="btnCancelExecuteFromUpload" runat="server" Text="Cancel" CssClass="small button gray" Visible="true">  </asp:Button>
            </div>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
