﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingAgreementSelectedTL.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingAgreementSelectedTL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingAgreement</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
      <script src="../../js/jquery-1.6.4.js"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        $(document).ready(function () {
            CalculatePokokHutang();
        });
        function CalculatePokokHutang(lnk) {

            var Total = 0;
            var TotalAR = 0;
            var TotalOTR = 0;
            var TotalAccount = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');
            for (var i = 1; 0 < $("#<%=dtgSelected.ClientID%> tr").length - 1; i++) {
                if ($("#<%=dtgSelected.ClientID%> tr:eq(" + i + ") [id*='txtOSPRINCIPAL']").val() != "") {
                    if ($("#<%=dtgSelected.ClientID%> tr:eq(" + i + ") [id*='chkSelect']").prop("checked") == true)
                 {

                     Total = Total + parseFloat($("#<%=dtgSelected.ClientID%> tr:eq(" + i.toString() + ") [id*='txtOSPRINCIPAL']").val().replace(re, ""));
                     TotalAR = TotalAR + parseFloat($("#<%=dtgSelected.ClientID%> tr:eq(" + i.toString() + ") [id*='lblsisaAR']").text().replace(re, ""));
                     TotalOTR = TotalOTR + parseFloat($("#<%=dtgSelected.ClientID%> tr:eq(" + i.toString() + ") [id*='lblTotalOTR']").text().replace(re, ""));                                                
                     TotalAccount = TotalAccount + 1;
                  }
                  
                }
                if (($("#<%=dtgSelected.ClientID%> tr").length - 1) == i)
                    break;
            }
            var _plafond = $("#lbljumlahplafond").text().replace(re, "");
            var _total = parseFloat(Total);
            var _totalAR = parseFloat(TotalAR);
            var _totalOTR = parseFloat(TotalOTR);
            var _sisa = parseFloat(_plafond) - parseFloat(Total);
            var _totalAccount = parseFloat(TotalAccount);

            $("#lblARAmountL").text(number_format(_totalAR));
            $("#lblPrincipalAmountL").text(number_format(_total));
            $("#lblOTRAmountL").text(number_format(_totalOTR));
            $("#lblTotalAmountMustBeAdd").text(number_format(_sisa));
            $("#lblAccountL").text(number_format(_totalAccount));
           
            
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>   
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnl1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div id="divfasilitasterpilih" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR FASILITAS TERPILIH
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total A/R
                    </label>
                    <asp:Label ID="lblARAmountL" runat="server"></asp:Label>
                    
                </div>
                <div id="divmustbeadd" runat="server" visible="false">
                <div class="form_right">
                    <label>
                        Jumlah harus ditambah
                    </label>
                    <asp:Label ID="lblTotalAmountMustBeAdd" runat="server"></asp:Label>
                </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total Pokok
                    </label>
                    <asp:Label ID="lblPrincipalAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Kontrak
                    </label>
                    <asp:Label ID="lblAccountL" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total OTR
                    </label>
                    <asp:Label ID="lblOTRAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        <%--Nilai Rencana Cair--%>
                        Total Plafond
                    </label>
                    <asp:Label ID="lbljumlahplafond" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box" runat="server" visible="false">
                <div class="form_left">
                    <label>
                        Total NTF
                    </label>
                    <asp:Label ID="lblNTFtl" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    &nbsp;
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSelection" runat="server" Text="Selection" CssClass="small button blue" CausesValidation="False"></asp:Button>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnSaveUpdate" runat="server" Text="Save Update Kontrak " CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button><a href="javascript:history.back();"> </a>
                
            </div>
                <div class="form_button">
                    <asp:FileUpload ID="FileUpload"  runat="server" visible="true" />

                <asp:Button ID="btnImport" runat="server" Text="Upload Kontrak Yang Dijaminkan" CssClass="small button blue" Visible="true">  </asp:Button>
                     </div>
        </div>
            <!--Here-->
            <div id="divkontrakterpilih" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgSelected" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO FASILITAS">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NAME" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkCustName" runat="server" Enabled="True" Text='<%# Container.dataitem("CustName") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                              <%--  <asp:BoundColumn DataField="OSAR" HeaderText="SISA A/R" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <asp:TemplateColumn SortExpression="" HeaderText="SISA A/R CURR" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblsisaAR" runat="server" Text='<%# FormatNumber(Container.DataItem("OSAR"),0)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="OutstandingCurrent" HeaderText="OS POK CUR" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="OSPrincipal" HeaderText="OS POK BATCH" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                </asp:BoundColumn>
                               <%-- <asp:BoundColumn DataField="OSInterest" HeaderText="OS INT BATCH" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                </asp:BoundColumn>--%>
                                    <asp:TemplateColumn SortExpression="" HeaderText="POKOK HUTANG" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                    <HeaderStyle ></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>

                                        <asp:TextBox runat="server" ID="txtOSPRINCIPAL" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                              onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true)" onfocus="this.value=resetNumber(this.value);" 
                                            CssClass="numberAlign small" OnChange="CalculatePokokHutang(this);" 
                                            autocomplete="off" Text='<%#FormatNumber(Container.DataItem("OutstandingPrincipalEdited"), 0)%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                               <%-- <asp:BoundColumn DataField="TotalOTR" HeaderText="OTR" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                </asp:BoundColumn>--%>
                                 <asp:TemplateColumn SortExpression="" HeaderText="OTR" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalOTR" runat="server" Text='<%# FormatNumber(Container.DataItem("TotalOTR"),0)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:BoundColumn DataField="NTF" HeaderText="NTF" DataFormatString="{0:N2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <%--<asp:BoundColumn DataField="TENOR" HeaderText="JK WAKTU">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="Sisa" HeaderText="SISA TENOR">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                <asp:BoundColumn DataField="StatusBPKB" HeaderText="STATUS BPKB" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>                                    
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked = "true" onclick="CalculatePokokHutang(this);">
                                        </asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# Container.DataItem("ApplicationID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                    <HeaderStyle Width="35%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFundingPledgeStatus" runat="server" Text='<%# Container.DataItem("FundingPledgeStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
           </div>
            <div class="form_button">
                    <asp:Button ID="btnExecution" runat="server" Text="Execution" CssClass="small button blue" visible="false">
                    </asp:Button>
                    &nbsp;&nbsp;
               <asp:Button ID="BtnExportToExel" runat="server"  Text="Export To Exel" CssClass="small button blue" visible="false"></asp:Button>
            </div>
      <div class="form_button">
        <div id="divupload" runat="server">
        <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR UPLOAD KONTRAK
                    </h4>
                </div>
        </div>       
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgViewUploadFromExcel" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                    <asp:TemplateColumn SortExpression="" HeaderText="No Kontrak" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoPJJ" runat="server" Text='<%# Container.DataItem("NoPJJ")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="OS Pokok" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOSPOkok" runat="server" Text='<%# Container.DataItem("OutstandingPrincipal")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="OS Margin" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOSBunga" runat="server" Text='<%# Container.DataItem("OutstandingInterest")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="Sisa Tenor" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSisaTenor" runat="server" Text='<%# Container.DataItem("SisaTenor")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="Rate Batch" >
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRateBatch" runat="server" Text='<%# Container.DataItem("RateBatch")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                </div>
            </div>
        </div>
           <div class="form_button">
                    <asp:Button ID="btnExecuteFromUpload" runat="server" Text="Execute" CssClass="small button blue" Visible="true">  </asp:Button>
                    <asp:Button ID="btnCancelExecuteFromUpload" runat="server" Text="Cancel" CssClass="small button gray" Visible="true">  </asp:Button>
            </div>
        </div>
       <div id="divdaftarangsuran" runat="server">
        <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR ANGSURAN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
<%--          <asp:GridView ID="GridView1" CssClass="grid_general" Width="100%" DataKeyNames="SeqNo"
                            AutoGenerateColumns="false" runat="server" HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid"
                            GridLines="None">
                            <Columns>
                            <asp:BoundField DataField="SeqNo" HeaderText="No."  HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />     
                            <asp:BoundField DataField="TanggalAngsuran" HeaderText="Tanggal"  HeaderStyle-HorizontalAlign="left" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="Pokok" HeaderText="Pokok"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="Bunga" HeaderText="Bunga"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="Angsuran" HeaderText="Angsuran"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="SaldoPokok" HeaderText="OS Pokok"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="SaldoBunga" HeaderText="OS Bunga"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                            </Columns>
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                        </asp:GridView>--%>

                        <asp:DataGrid ID="DtgViewAngsuran" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                    <asp:BoundColumn DataField="SeqNo" HeaderText="No.">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TanggalAngsuran" HeaderText="Tanggal">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Pokok" HeaderText="Pokok">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Bunga" HeaderText="Margin">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Angsuran" HeaderText="Angsuran">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SaldoPokok" HeaderText="OS Pokok">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>									
                                    <asp:BoundColumn DataField="SaldoBunga" HeaderText="OS Margin">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>																		
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
