﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingNegCovMnt.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingNegCovMnt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingNegCovMnt</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlHead" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    PERJANJIAN NEGATIVE COVENANT
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankName" runat="server" Width="192px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server" Width="192px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Facility/Kontrak</label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Facility/Kontrak</label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PERJANJIAN NEGATIVE COVENANT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    SYARAT
                </label>
                <label>
                    N1
                </label>
            </div>
            <div class="form_right">
                <label>
                    N2
                </label>
                <label>
                    EDIT
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    OD N1 hari pada N2 Angs Pertama
                </label>
                <asp:Label ID="lblCov1N1" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblCov1N2" runat="server"></asp:Label>
                <asp:ImageButton ID="imbEdit1" runat="server" Width="16px" ImageUrl="../../Images/IconEdit.gif"
                    Height="13px"></asp:ImageButton><a href="FundingSourceContractNegCovEdit.htm"></a>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Overdue days &gt; N1 days
                </label>
                <asp:Label ID="LblCov3N1" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                </label>
                <asp:ImageButton ID="imbEdit2" runat="server" Width="16px" ImageUrl="../../Images/IconEdit.gif"
                    Height="13px"></asp:ImageButton><a href="FundingSourceContractNegCovEdit.htm"></a>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlEdit" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    EDIT - PERJANJIAN NEGATIVE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankNameEdit" runat="server" Width="192px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyNameEdit" runat="server" Width="192px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Facility/Kontrak</label>
                <asp:Label ID="lblFundingContractNoEdit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Facility/Kontrak</label>
                <asp:Label ID="lblContractNameEdit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Syarat</label>
                <asp:Label ID="lblTerm" runat="server" Width="224px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Parameter N1</label>
                <asp:TextBox ID="txtN1" runat="server" Width="104px" Height="20px" CssClass="inptype"></asp:TextBox>
                <asp:RangeValidator ID="rvN1" runat="server" Type="Integer" MinimumValue="0" MaximumValue="100000"
                    ErrorMessage="N1 Harus diisi dengan Angka" Display="Dynamic" ControlToValidate="txtN1"></asp:RangeValidator>
            </div>
        </div>
        <asp:Panel ID="pnlN2" Visible="False" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Parameter N2</label>
                    <asp:TextBox ID="txtN2" runat="server" Width="104px" Height="20px" CssClass="inptype"></asp:TextBox>
                    <asp:RangeValidator ID="rvN2" runat="server" Type="Integer" MinimumValue="0" MaximumValue="100000"
                        ErrorMessage="N2 Harus diisi dengan Angka" Display="Dynamic" ControlToValidate="txtN2"></asp:RangeValidator>
                </div>
            </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
