﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingContractPlafondBranch.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingContractPlafondBranch" %>

<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingContractPlafondBranch</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus Data ini ?"))
                return true;
            else
                return false;
        }
		-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PLAFOND PER CABANG
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankName" runat="server" Width="192px">lblBankName</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server" Width="192px">lblFundingCoyName</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Fasilitas</label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Plafond</label>
                <asp:Label ID="lblContractPlafondAmount" runat="server"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PLAFOND PER CABANG
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingPlafondBranch" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="BRANCHID" HeaderText="ID CABANG">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BRANCHFULLNAME" HeaderText="NAMA CABANG">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAFONDAMOUNT" HeaderText="JUMLAH PLAFOND" DataFormatString="{0:N2}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OSAMOUNT" HeaderText="JUMLAH DIALOKASI" DataFormatString="{0:N2}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"  CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                        ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelList" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Tidak Dialokasi</label>
                <asp:Label ID="lblNonAllocatedAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:DropDownList ID="drdBranch" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblBranch" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alokasi di Cabang ini</label>
                <%--<asp:TextBox ID="txtPlafondAmount" runat="server"  MaxLength="15">0</asp:TextBox>
                <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                    ControlToValidate="txtPlafondAmount" MinimumValue="0" MaximumValue="99999999999999"></asp:RangeValidator>--%>
                    <uc1:ucnumberformat id="txtPlafondAmount" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button><a href="javascript:history.back();"></a>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDoc" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Dokumen</label>
                <label>
                    Catatan</label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:CheckBox ID="chkDoc1" runat="server"></asp:CheckBox>
                <asp:TextBox ID="txtNote1" runat="server" Width="350px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:CheckBox ID="chkDoc2" runat="server"></asp:CheckBox>
                <asp:TextBox ID="txtNote2" runat="server" Width="350px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:CheckBox ID="chkDoc3" runat="server"></asp:CheckBox>
                <asp:TextBox ID="txtNote3" runat="server" Width="350px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:CheckBox ID="chkDoc4" runat="server"></asp:CheckBox>
                <asp:TextBox ID="txtNote4" runat="server" Width="350px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveDoc" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelDoc" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
