﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DrawDown.aspx.vb" Inherits="Maxiloan.Webform.FundMgt.DrawDown" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucdatece.ascx" TagName="ucdatece"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DrawDown</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingCompanyView(Style, CompanyID) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyView.aspx?Style=' + Style + '&CompanyID=' + CompanyID, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1,scrollbars=1');
        }
        function OpenFundingCompanyContractView(pStyle, pCompanyName, pBankName, pFundingContractNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyContractView.aspx?Style=' + pStyle + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&FundingContractNo=' + pFundingContractNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function OpenFundingBatchView(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingBatchView.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'Channeling', 'left=50, top=10, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }							
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR PENCAIRAN PINJAMAN BANK
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFundingCoyID" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" runat="server" Enabled="True" Text='<%# Container.dataitem("BankName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblContractName" runat="server" Enabled="True" Text='<%# Container.dataitem("ContractName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Enabled="True" Text='<%# Container.dataitem("BankID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBranch" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyName") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO FASILITAS BANK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkContractNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingContractNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO BATCH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBatchNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingBatchNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BATCHDATE" HeaderText="TGL BATCH" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrincipalAmtToFunCoy" HeaderText="JUMLAH" DataFormatString="{0:N0}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CurrencyID" HeaderText="MATA UANG">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="CAIR">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imbreceive" runat="server" ImageUrl="../../Images/IconReceived.gif"
                                        CommandName="RECEIVE" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblJumlah" runat="server" Enabled="True" Text='<%# Container.DataItem("PrincipalAmtToFunCoy") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        Display="Dynamic" ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlReceive" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PENCAIRAN PINJAMAN BANK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblFundingCompanyName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCompanyBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="lblFacilityNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Fasilitas</label>
                <asp:Label ID="lblFacilityName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:Label ID="lblBatchNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Rencana Cair</label>
                <asp:Label ID="lblDrawdownPlanDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Periode Pencairan</label>
                <asp:Label ID="lblPeriodeFrom" runat="server"></asp:Label> s/d 
                <asp:Label ID="lblPeriodeTo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Pencairan</label>
                <asp:Label ID="lblDrawdownAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Suku Margin</label>
                <asp:Label ID="lblInterestRate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Margin</label>
                <asp:Label ID="lblInterestType" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Rekening Bank 1
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboBank" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Rekening Bank"
                    ControlToValidate="cboBank" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jumlah Rekening Bank 1</label>
                <uc1:ucnumberformat id="TxtboxAmountBankAccount1" runat="server"></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    No BBT Rekening 1</label>
                <asp:TextBox ID="TxtReferenceNo" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rekening Bank 2</label>
                <asp:DropDownList ID="cboBank2" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Rekening Bank 2</label>
                <uc1:ucnumberformat id="TxtboxAmountBankAccount2" runat="server"></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No BBT Rekening 2</label>
                <asp:TextBox ID="TxtReferenceNo2" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rekening Bank 3</label>
                <asp:DropDownList ID="CboBank3" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Rekening Bank 3</label>
                <uc1:ucnumberformat id="TxtboxAmountBankAccount3" runat="server"></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No BBT Rekening 3</label>
                <asp:TextBox ID="TxtReferenceNo3" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">                                                                    
                <label class="label_req">
                    Tanggal Pencairan</label>
                <uc1:ucdatece runat="server" id="txtDrawDownDate"></uc1:ucdatece>                                                                           
            </div>
        </div>

        <asp:Panel ID="PanelRange" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DRAWDOWN RANGE
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False" Width="40%" HeaderStyle-HorizontalAlign="Center"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>                                                        
                            <asp:TemplateColumn HeaderText="DATE FROM" >
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>                                    
                                    <%--<uc1:ucnumberformat id="txtdatefrom" runat="server" Width="100%" />    --%>
                                    <uc1:ucnumberformat id="txtdatefrom" runat="server" />            
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DATE TO" >
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>                                    
                                    <%--<uc1:ucnumberformat id="txtdateto" runat="server" Width="100%" />         --%>      
                                    <uc1:ucnumberformat id="txtdateto" runat="server"  />                              
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DUE DATE" >
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>                                    
                                    <%--<uc1:ucnumberformat id="txtduedate" runat="server" Width="100%" />       --%>
                                    <uc1:ucnumberformat id="txtduedate" runat="server"  />                                    
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>        
        </asp:Panel>

        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>    
    </form>
</body>
</html>
