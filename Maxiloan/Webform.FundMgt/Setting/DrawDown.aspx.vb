﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DrawDown
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "uccomponent"

    Protected WithEvents TxtboxAmountBankAccount1 As ucNumberFormat
    Protected WithEvents TxtboxAmountBankAccount2 As ucNumberFormat
    Protected WithEvents TxtboxAmountBankAccount3 As ucNumberFormat
    Protected WithEvents txtDrawDownDate As ucDateCE

#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

    Public Property DrawDownAmount() As Decimal
        Get
            Return CDec(ViewState("DrawDownAmount"))
        End Get
        Set(ByVal Value As Decimal)
            viewstate("DrawDownAmount") = Value
        End Set
    End Property
    Public Property SumPrincipalAmount() As Decimal
        Get
            Return CDec(ViewState("SumPrincipalAmount"))
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumPrincipalAmount") = Value
        End Set
    End Property
    Public Property isExecute() As Boolean
        Get
            Return CBool(ViewState("isExecute"))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isExecute") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController
    Dim customClass As New Parameter.FundingContractBatch
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Me.IsHoBranch = False Then
            ShowMessage(lblMessage, "Harap Login di kantor pusat", True)
            pnlList.Visible = False
            pnlReceive.Visible = False
            PanelRange.Visible = False
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "DRAWDOWN"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If


            txtDrawDownDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            Me.SearchBy = ""
            Me.SortBy = ""
            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            pnlReceive.Visible = False
            btnSave.Visible = True



        End If
    End Sub

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingDrawDownList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlReceive.Visible = False
    End Sub
    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim hyBranch As HyperLink
        Dim hyContract As HyperLink
        Dim lblFundingCoyID As Label
        Dim lblBankID As Label
        Dim hyBatchNo As HyperLink
        Dim lblContractName As Label
        Dim lblBankName As Label

        hyBranch = CType(e.Item.FindControl("lnkBranch"), HyperLink)
        hyContract = CType(e.Item.FindControl("lnkContractNo"), HyperLink)
        lblBankName = CType(e.Item.FindControl("lblBankName"), Label)
        lblFundingCoyID = CType(e.Item.FindControl("lblFundingCoyID"), Label)
        lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
        hyBatchNo = CType(e.Item.FindControl("lnkBatchNo"), HyperLink)
        lblContractName = CType(e.Item.FindControl("lblContractName"), Label)

        If e.Item.ItemIndex >= 0 Then
            hyBranch.NavigateUrl = "javascript:OpenFundingCompanyView('" & "channeling" & "', '" & lblFundingCoyID.Text.Trim & "')"
            hyContract.NavigateUrl = "javascript:OpenFundingCompanyContractView('" & "channeling" & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & hyContract.Text.Trim & "')"
            hyBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & lblFundingCoyID.Text.Trim & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & lblBankID.Text.Trim & "','" & lblContractName.Text.Trim & "','" & hyContract.Text.Trim & "','" & hyBatchNo.Text.Trim & "')"
        End If
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString

        pnlList.Visible = True
        pnlReceive.Visible = False

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlReceive.Visible = False
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlReceive.Visible = False
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlReceive.Visible = False
    End Sub

#End Region
#Region "FillCboBank"
    Sub fillcboBank()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetHOBranch"
            objCommand.Parameters.Add("@CompanyID", SqlDbType.Char, 3).Value = Me.SesCompanyID.Trim
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.BranchID = IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankAccount(GetConnectionString, Me.branchID.trim, "BA", "")
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
        With cboBank2
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
        With CboBank3
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With

    End Sub
#End Region

    Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        Dim lblBankName, lblBankID, lblContractName, lblFundingCoyID, lblJumlah As Label
        Dim lnkBranch, lnkBatchNo As HyperLink
        Dim cmdWhere As String

        lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
        lblFundingCoyID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblFundingCoyID"), Label)
        lnkBatchNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkBatchNo"), HyperLink)
        lblBankID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBankID"), Label)
        lblJumlah = CType(e.Item.FindControl("lblJumlah"), Label)
        Me.FundingContractNo = lnkContractNo.Text.Trim
        Me.CompanyID = lblFundingCoyID.Text.Trim
        Me.FundingBatchNo = lnkBatchNo.Text.Trim
        Me.BankID = lblBankID.Text.Trim
        Me.DrawDownAmount = CDec(lblJumlah.Text.Trim)

        Select Case e.CommandName
            Case "RECEIVE"
                pnlReceive.Visible = True
                pnlList.Visible = False
                fillcboBank()
                cmdWhere = "FundingContract.FundingContractNo = '" & Me.FundingContractNo.Trim & "' and "
                cmdWhere = cmdWhere + "FundingContract.FundingCoyID = '" & Me.CompanyID & "' and "
                cmdWhere = cmdWhere + "FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                If Me.IsHoBranch Then
                    'If Not CheckCashier(Me.Loginid) Then

                    '    ShowMessage(lblMessage, "Kasir Belum Buka", True)
                    '    btnSave.Visible = False
                    '    Exit Sub
                    'Else
                    edit(cmdWhere)
                    btnSave.Visible = True
                    'End If
                Else

                ShowMessage(lblMessage, "Harap Login di kantor Pusat", True)
                btnSave.Visible = False
                Exit Sub
                End If

                CheckRangeScheme()                
        End Select
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlReceive.Visible = False
        pnlList.Visible = True
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
    Public Sub edit(ByVal cmdwhere As String)
        Dim dtslist As DataTable
        Dim cReceive As New GeneralPagingController
        Dim oReceive As New Parameter.GeneralPaging

        With oReceive
            .strConnection = GetConnectionString
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            '.SortBy = SortBy
            .SortBy = ""
            .SpName = "spFundingDrawDownFundSelect"
        End With
        oReceive = cReceive.GetGeneralPaging(oReceive)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            If dtslist.Rows(0).Item("BankName") <> "" Then
                lblFundingCompanyName.Text = CStr(dtslist.Rows(0).Item("BankName")).Trim
            End If
            If dtslist.Rows(0).Item("FundingCoyName") <> "" Then
                lblFundingCompanyBranch.Text = CStr(dtslist.Rows(0).Item("FundingCoyName")).Trim
            End If
            If dtslist.Rows(0).Item("FundingContractNo") <> "" Then
                lblFacilityNo.Text = CStr(dtslist.Rows(0).Item("FundingContractNo")).Trim
            End If
            If dtslist.Rows(0).Item("ContractName") <> "" Then
                lblFacilityName.Text = CStr(dtslist.Rows(0).Item("ContractName")).Trim
            End If
            If dtslist.Rows(0).Item("FundingBatchNo") <> "" Then
                lblBatchNo.Text = CStr(dtslist.Rows(0).Item("FundingBatchNo")).Trim
            End If
            lblDrawdownPlanDate.Text = CStr(dtslist.Rows(0).Item("ProposeDate")).Trim
            'lblDrawdownAmount.Text = FormatNumber(dtslist.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
            'Me.DrawDownAmount = FormatNumber(dtslist.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
            lblDrawdownAmount.Text = FormatNumber(Me.DrawDownAmount, 0)
            lblInterestRate.Text = FormatNumber(dtslist.Rows(0).Item("InterestRate"), 2)

            lblPeriodeFrom.Text = CStr(dtslist.Rows(0).Item("PeriodeFrom")).Trim
            lblPeriodeTo.Text = CStr(dtslist.Rows(0).Item("PeriodeTo")).Trim

            'cboBank.SelectedIndex = cboBank.Items.IndexOf(cboBank.Items.FindByValue(CStr(dtslist.Rows(0).Item("BankAccountID")).Trim))
            'TxtboxAmountBankAccount1.Text = FormatNumber(dtslist.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
            TxtboxAmountBankAccount1.Text = FormatNumber(Me.DrawDownAmount, 0)

            If dtslist.Rows(0).Item("InterestType") <> "" Then
                lblInterestType.Text = CStr(dtslist.Rows(0).Item("InterestType")).Trim
            End If
            Try
                dtgFundingContractList.DataBind()
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Else

            ShowMessage(lblMessage, "Harap Lakukan Batch Execution Terlebih Dahulu", True)
            Exit Sub
        End If
        pnlList.Visible = False
        pnlReceive.Visible = True
        ' cboBank.SelectedItem.Value = "0"
        cboBank2.SelectedItem.Value = "0"
        CboBank3.SelectedItem.Value = "0"
        'TxtboxAmountBankAccount2.Text = "0"
        'TxtboxAmountBankAccount3.Text = "0"
        'TxtboxAmountBankAccount1.Text = "0"
        TxtReferenceNo.Text = ""
        TxtReferenceNo2.Text = ""
        TxtReferenceNo3.Text = ""
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim sType As String
        lblMessage.Text = ""

        If CDbl(TxtboxAmountBankAccount1.Text) + CDbl(TxtboxAmountBankAccount2.Text) + CDbl(TxtboxAmountBankAccount3.Text) <> CDbl(lblDrawdownAmount.Text) Then
            ShowMessage(lblMessage, "Total Nilai pada masing-masing bank harus sama dengan Jumlah Pencairan, Harap Periksa kembali!!", True)
            Exit Sub
        End If



        Me.isExecute = False
        CheckCountApplicationID()

        If Me.isExecute = False Then

            ShowMessage(lblMessage, "Harap lakukan Batch Execution Terlebih Dahulu", True)
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            pnlReceive.Visible = False
            Exit Sub
        End If


        If cboBank.SelectedItem.Value.Trim = "0" Then

            ShowMessage(lblMessage, "Harap pilih Rekening Bank", True)
            Exit Sub
        End If
        If TxtboxAmountBankAccount1.Text.Trim <> "0" And TxtboxAmountBankAccount2.Text.Trim = "0" And TxtboxAmountBankAccount3.Text.Trim = "0" Then
            If CDec(TxtboxAmountBankAccount1.Text.Trim) <> Me.DrawDownAmount Then

                ShowMessage(lblMessage, "Jumlah Pencairan Harus cocok dengan Jumlah Rekening Bank 1", True)
                Exit Sub
            End If
        End If
        If TxtboxAmountBankAccount1.Text.Trim <> "0" And TxtboxAmountBankAccount2.Text.Trim <> "0" And TxtboxAmountBankAccount3.Text.Trim = "0" Then
            If cboBank2.SelectedItem.Value.Trim = "0" Then

                ShowMessage(lblMessage, "Please Select Bank Account 2", True)
                Exit Sub
            Else
                If CDec(CDec(TxtboxAmountBankAccount1.Text) + CDec(TxtboxAmountBankAccount2.Text)) <> CDec(Me.DrawDownAmount) Then

                    ShowMessage(lblMessage, "Jumlah Pencairan Harus cocok dengan Jumlah Rekening Bank 2", True)
                    Exit Sub
                End If
            End If
        End If
        If TxtboxAmountBankAccount1.Text.Trim <> "0" And TxtboxAmountBankAccount2.Text.Trim <> "0" And TxtboxAmountBankAccount3.Text.Trim <> "0" Then
            If cboBank2.SelectedItem.Value.Trim = "0" Or CboBank3.SelectedItem.Value.Trim = "0" Then

                ShowMessage(lblMessage, "Harap Pilih Rekening Bank 2 atau Rekening Bank 3", True)
                Exit Sub
            Else
                If CDec(CDec(TxtboxAmountBankAccount1.Text) + CDec(TxtboxAmountBankAccount2.Text) + CDec(TxtboxAmountBankAccount3.Text)) <> CDec(Me.DrawDownAmount) Then

                    ShowMessage(lblMessage, "Jumlah Pencairan Harus cocok dengan Jumlah Rekening Bank 1, 2 dan 3", True)
                    Exit Sub
                End If
            End If
        End If

        'cek period pencairan
        Try
            With customClass
                .strConnection = GetConnectionString()                
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .ValueDate = ConvertDate2(txtDrawDownDate.Text)
            End With

            m_Company.FundingDrowDownReceiveCheckPeriod(customClass)

            If customClass.ErrLabel = "Tanggal Pencairan Invalid" Then
                ShowMessage(lblMessage, "Tanggal Pencairan Invalid", True)
                Exit Sub
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
        
            Try
                With customClass
                    .strConnection = GetConnectionString()
                    .BankId = Me.BankID
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .PrincipalAmtToFunCoy = CDec(lblDrawdownAmount.Text.Trim)
                    .ValueDate = ConvertDate2(txtDrawDownDate.Text)
                    .ReferenceNo = TxtReferenceNo.Text.Trim
                    .ReferenceNo2 = TxtReferenceNo2.Text.Trim
                    .ReferenceNo3 = TxtReferenceNo3.Text.Trim
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .BankAccountID = cboBank.SelectedItem.Value.Trim
                    .BankAccountID2 = cboBank2.SelectedItem.Value.Trim
                    .BankAccountID3 = CboBank3.SelectedItem.Value.Trim
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .CompanyID = Me.SesCompanyID
                    .AmtDrawDown1 = CDec(TxtboxAmountBankAccount1.Text.Trim)
                    .AmtDrawDown2 = CDec(TxtboxAmountBankAccount2.Text.Trim)
                    .AmtDrawDown3 = CDec(TxtboxAmountBankAccount3.Text.Trim)
                End With

            m_Company.FundingDrowDownReceive(customClass)

            'For n As Integer = 0 To dtgPaging.Items.Count - 1
            '    Dim DateFrom As ucNumberFormat = CType(dtgPaging.Items(n).FindControl("txtdatefrom"), ucNumberFormat)
            '    Dim DateTo As ucNumberFormat = CType(dtgPaging.Items(n).FindControl("txtdateto"), ucNumberFormat)
            '    Dim DueDate As ucNumberFormat = CType(dtgPaging.Items(n).FindControl("txtduedate"), ucNumberFormat)

            '    With customClass
            '        .strConnection = GetConnectionString()                    
            '        .FundingCoyId = Me.CompanyID
            '        .FundingContractNo = Me.FundingContractNo
            '        .FundingBatchNo = Me.FundingBatchNo
            '        .ReferenceNo = DateFrom.Text
            '        .ReferenceNo2 = DateTo.Text
            '        .ReferenceNo3 = DueDate.Text
            '    End With

            '    m_Company.FundingDrowDownReceiveDueDateRangeAdd(customClass)
            'Next

                ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
                Me.SearchBy = ""
                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
                pnlReceive.Visible = False
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
    End Sub

#Region "CheckCountApplicationID"
    Public Sub CheckCountApplicationID()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Dim objReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingCheckDrawDownAmount"
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNO", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo.Trim
            objCommand.Parameters.Add("@IsExecution", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.isExecute = IIf(IsDBNull(objCommand.Parameters("@IsExecution").Value), "", objCommand.Parameters("@IsExecution").Value)
            'objReader = objCommand.ExecuteReader()

            'If objReader.Read Then
            '    Me.CountAgrApplicationID = CInt(objReader.Item("CountAgrApplicatonID"))
            '    Me.CountFundingAgrApplicationID = CInt(objReader.Item("CountFundingAgrApplicatonID"))
            'End If

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound

        Dim txtdatefrom As ucNumberFormat
        Dim txtdateto As ucNumberFormat
        Dim txtduedate As ucNumberFormat        

        If e.Item.ItemIndex >= 0 Then                         

            txtdatefrom = CType(e.Item.FindControl("txtdatefrom"), ucNumberFormat)
            txtdateto = CType(e.Item.FindControl("txtdateto"), ucNumberFormat)
            txtduedate = CType(e.Item.FindControl("txtduedate"), ucNumberFormat)

            InitiateUCnumberFormat(txtdatefrom, False, True)
            InitiateUCnumberFormat(txtdateto, False, True)
            InitiateUCnumberFormat(txtduedate, False, True)

            txtdatefrom.RangeValidatorMinimumValue = "1"
            txtdatefrom.RangeValidatorMaximumValue = "31"
            txtdatefrom.setErrMsg()
            txtdateto.RangeValidatorMinimumValue = "1"
            txtdateto.RangeValidatorMaximumValue = "31"
            txtdateto.setErrMsg()
            txtduedate.RangeValidatorMinimumValue = "1"
            txtduedate.RangeValidatorMaximumValue = "31"
            txtduedate.setErrMsg()

            txtdatefrom.Text = "0"
            txtdateto.Text = "0"
            txtduedate.Text = "0"

        End If
    End Sub

    Private Sub CheckRangeScheme()

        'cek scheme
        Try
            With customClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo                
            End With


            m_Company.FundingDrowDownReceiveCheckScheme(customClass)
            If customClass.Scheme = "5" Then
                PanelRange.Visible = True
                SetInitialRow()
            Else
                PanelRange.Visible = False
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)            
        End Try

    End Sub

    Private Sub SetInitialRow()

        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        Try
            With oContract
                .strConnection = GetConnectionString()                
                .SpName = "GetFundingDueDateByRange"                
                .WhereCond = ""
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy                
            End With

            oContract = cContract.GetGeneralPaging(oContract)

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            dtvEntity.Sort = Me.SortBy

            If dtsEntity.Rows.Count = 0 Then
                dtsEntity.Rows.Add("Row")
                dtsEntity.Rows.Add("Row")
                dtsEntity.Rows.Add("Row")
                dtsEntity.Rows.Add("Row")
            Else
                dtsEntity.Rows.Add("Row")
                dtsEntity.Rows.Add("Row")
                dtsEntity.Rows.Add("Row")
            End If

            dtgPaging.DataSource = dtvEntity
            dtgPaging.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

End Class