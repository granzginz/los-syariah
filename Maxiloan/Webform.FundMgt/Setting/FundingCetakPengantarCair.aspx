﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingCetakPengantarCair.aspx.vb" Inherits="Maxiloan.Webform.FundMgt.FundingCetakPengantarCair" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>FundingCetakPengantarCair</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CETAK PENCAIRAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="PnlInterest" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    KONFIRMASI CETAK SURAT PENCAIRAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Reff</label>
                <asp:TextBox ID="TxtNoReff" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <%--<div class="form_single">--%>
            <div class="form_left">
                <label>
                    Up.</label>
                <asp:TextBox ID="txtUp" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Jabatan</label>
                <asp:TextBox ID="txtJabatanUp" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama</label>
                <asp:TextBox ID="txtSigner1" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Jabatan</label>
                <asp:TextBox ID="txtJabatan1" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama</label>
                <asp:TextBox ID="txtSigner2" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Jabatan</label>
                <asp:TextBox ID="txtJabatan2" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perihal Surat</label>
                <asp:TextBox ID="txtPerihal" runat="server" Width="192px" CssClass="InpType"></asp:TextBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnViewReport" runat="server" CausesValidation="False" Text="Print"
                CssClass="small button green"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>