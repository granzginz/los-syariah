﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingCriteriaValue.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingCriteriaValue" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NILAI KRITERIA</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnCriteriaValueID" type="hidden" name="hdnCriteriaValueID" runat="server" />
    <asp:Label ID="lblMessage" runat="server" ></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    NILAI KRITERIA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Kriteria *)
                </label>
                <asp:TextBox ID="txtNmKriteria" Enabled="false" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Pilihan *)
                </label>
                <asp:DropDownList ID="cboJenisPilihan" Enabled="false" runat="server">
                    <asp:ListItem Value="SEL">SELECT</asp:ListItem>
                    <asp:ListItem Value="MUL">MULTIPLE SELECT</asp:ListItem>
                    <asp:ListItem Value="MAX">MAX</asp:ListItem>
                    <asp:ListItem Value="MIN">MIN</asp:ListItem>
                    <asp:ListItem Value="RAN">RANGE</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    NILAI KRITERIA</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="CriteriaValueID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CriteriaValueID" SortExpression="CriteriaValueID" HeaderText="IDNILAI">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaDescription" SortExpression="CriteriaDescription"
                                HeaderText="NILAI KRITERIA"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaValue" SortExpression="CriteriaValue" HeaderText="VALUE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SQLData" SortExpression="SQLData" HeaderText="SQL-DATA">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtGoPage"
                        Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnPrint" runat="server" Visible="false" Enabled="true" Text="Print"
                CssClass="small button blue"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI FUNDING KRITERIA</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="CriteriaValueID">IDNILAI</asp:ListItem>
                    <asp:ListItem Value="CriteriaDescription">NILAI KRITERIA</asp:ListItem>
                    <asp:ListItem Value="CriteriaValue">VALUE</asp:ListItem>
                    <asp:ListItem Value="SQLData">SQL-DATA</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <asp:TextBox ID="txtSearch" runat="server" Width="50%"  MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnBackMenu" runat="server" CausesValidation="False" Text="Back"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    MENAMBAHKAN FUNDING KRITERIA -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label></h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    ID NILAI
                </label>
                <asp:Label ID="lblIDNilai" runat="server"></asp:Label>
                <asp:TextBox ID="txtIDNilai" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi!"
                    ControlToValidate="txtIDNilai" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Nama Kriteria
                </label>
                <asp:Label ID="lblNamaKriteria" runat="server" ></asp:Label>
                <asp:TextBox ID="txtNamaKriteria" runat="server" CssClass ="long_text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap isi!"
                    ControlToValidate="txtNamaKriteria" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label runat = "server" id="lblValueLabel">
                    Value
                </label>
                <asp:Label ID="lblValue" runat="server"></asp:Label>
                <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvValue" runat="server" ErrorMessage="Harap isi!"
                    ControlToValidate="txtValue" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">               
			   <label class ="label_req">
                    SQL-Data
                </label>
                <asp:Label ID="lblSQLData" runat="server"></asp:Label>
                <asp:TextBox ID="txtSQLData" runat="server" TextMode="MultiLine"  CssClass="multiline_textbox"  ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap isi!"
                    ControlToValidate="txtSQLData" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
