﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Linq
#End Region

Public Class FundingContractMaintenance
    Inherits Maxiloan.Webform.WebBased    

#Region "uccomponent"

    Protected WithEvents txtContractDate As ucDateCE
    Protected WithEvents txtPeriodFrom As ucDateCE
    Protected WithEvents txtPeriodTo As ucDateCE
    Protected WithEvents txtSpecificDate As ucDateCE
    Protected WithEvents txtFinalMaturityDate As ucDateCE
    Protected WithEvents txtEvaluationDate As ucDateCE
    Protected WithEvents txtFloatingStart As ucDateCE    
    Protected WithEvents txtPersenPokokHutang As ucNumberFormat
    Protected WithEvents txtSecurityPercentage As ucNumberFormat
    Protected WithEvents txtPlafondAmmount As ucNumberFormat
    Protected WithEvents txtFundingCoyPortion As ucNumberFormat
    Protected WithEvents txtMinimumPencairan As ucNumberFormat
    Protected WithEvents txtProvision As ucNumberFormat
    Protected WithEvents txtAdminFeePerAct As ucNumberFormat
    'Protected WithEvents txtBatasAngsuran As ucNumberFormat
    Protected WithEvents txtSecurityPrepaymentPenalty As ucNumberFormat
    Protected WithEvents txtCommitmentFee As ucNumberFormat
    Protected WithEvents txtLatePaymentFee As ucNumberFormat
    Protected WithEvents txtInterestRate As ucNumberFormat
    Protected WithEvents txtFeePerDrawDown As ucNumberFormat
    Protected WithEvents txtFeePerFacility As ucNumberFormat
    Protected WithEvents txtMaxDD As ucNumberFormat
    Protected WithEvents txtLCGracePeriod As ucNumberFormat
    Protected WithEvents txtPinalty As ucNumberFormat
    Protected WithEvents UcAPBankPaymentAllocation1 As ucAPBankPaymentAllocation

#End Region
#Region " Private Const "    
    Dim m_Company As New FundingCompanyController
    Dim m_Kriteria As New FundingContractCriteriaController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink

#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If
        lbltotrec.Text = recordCount.ToString
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
    End Sub

    Private Sub btnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
    End Sub

#End Region

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        pnlView.Visible = False
        lblMessage.Visible = False
        pnlRate.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region "FormLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
            'InitialDefaultPanel()

            Me.FormID = "FundingContract"

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")

            lblFundingCoyName.Text = Me.CompanyName
            lblBankName.Text = Me.BankName
            lblFundingCoyName.Visible = True
            lblBankName.Visible = True
            toggleSpecificDateValidator()

            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""

            drdBalanceSheetStatus.Enabled = False

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""
            If Request.QueryString("FundingContractNo") <> "" Then
                Me.FundingContractNo = Request.QueryString("FundingContractNo").Trim
                If CheckFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                    pnlList.Visible = False
                    pnlAddEdit.Visible = False
                    pnlView.Visible = True
                    JustView(Me.FundingContractNo)
                End If
            Else
                BindGridEntity(Me.SearchBy, Me.SortBy)
                PanelAllFalse()
                pnlList.Visible = True
            End If

            InitiateUCnumberFormat(txtPersenPokokHutang, False, True)
            InitiateUCnumberFormat(txtSecurityPercentage, False, True)
            InitiateUCnumberFormat(txtPlafondAmmount, True, True)
            InitiateUCnumberFormat(txtFundingCoyPortion, False, True)
            InitiateUCnumberFormat(txtMinimumPencairan, True, True)
            InitiateUCnumberFormat(txtProvision, False, True)
            InitiateUCnumberFormat(txtAdminFeePerAct, False, False)
            'InitiateUCnumberFormat(txtBatasAngsuran, True, True)
            InitiateUCnumberFormat(txtSecurityPrepaymentPenalty, False, True)
            InitiateUCnumberFormat(txtCommitmentFee, False, True)
            InitiateUCnumberFormat(txtLatePaymentFee, False, True)
            InitiateUCnumberFormat(txtInterestRate, False, True)
            InitiateUCnumberFormat(txtFeePerDrawDown, False, False)
            InitiateUCnumberFormat(txtFeePerFacility, False, False)
            InitiateUCnumberFormat(txtMaxDD, True, False)
            InitiateUCnumberFormat(txtLCGracePeriod, True, True)           

            txtPlafondAmmount.AutoPostBack = True
            txtContractDate.IsRequired = True
            txtPersenPokokHutang.TextCssClass = "small_text"
            txtPersenPokokHutang.RangeValidatorMinimumValue = "0"
            txtPersenPokokHutang.RangeValidatorMaximumValue = "999"
            txtPeriodFrom.IsRequired = True
            txtPeriodTo.IsRequired = True
            'txtSecurityPercentage.RangeValidatorMinimumValue = "100.01"
            txtSecurityPercentage.RangeValidatorMinimumValue = "100"
            txtSecurityPercentage.RangeValidatorMaximumValue = "1000"
            txtPlafondAmmount.RangeValidatorMinimumValue = "0"
            txtPlafondAmmount.RangeValidatorMaximumValue = "999999999999999"
            txtFundingCoyPortion.RangeValidatorMinimumValue = "0"
            txtFundingCoyPortion.RangeValidatorMaximumValue = "100"
            txtFundingCoyPortion.TextCssClass = "small_text"
            txtMinimumPencairan.RangeValidatorMinimumValue = "0"
            txtMinimumPencairan.RangeValidatorMaximumValue = "999999999999999"
            txtProvision.RangeValidatorMinimumValue = "0"
            txtProvision.RangeValidatorMaximumValue = "100"
            'txtBatasAngsuran.RangeValidatorMinimumValue = "0"
            'txtBatasAngsuran.RangeValidatorMaximumValue = "60"
            txtSpecificDate.IsRequired = True
            txtSecurityPrepaymentPenalty.RangeValidatorMinimumValue = "0"
            txtSecurityPrepaymentPenalty.RangeValidatorMaximumValue = "100"
            txtCommitmentFee.RangeValidatorMinimumValue = "0"
            txtCommitmentFee.RangeValidatorMaximumValue = "100"
            txtLatePaymentFee.RangeValidatorMinimumValue = "0"
            txtLatePaymentFee.RangeValidatorMaximumValue = "100"
            txtInterestRate.RangeValidatorMinimumValue = "0"
            txtInterestRate.RangeValidatorMaximumValue = "100"
            txtMaxDD.RangeValidatorMinimumValue = "0"
            txtMaxDD.RangeValidatorMaximumValue = "100"
            txtLCGracePeriod.RangeValidatorMinimumValue = "0"
            txtLCGracePeriod.RangeValidatorMaximumValue = "100"

            txtPeriodFrom.AutoPostBack = True
            txtPeriodTo.AutoPostBack = True
            bindComboIDBankBeban()
            bindComboIDBankAdministrasi()
        End If
    End Sub
#End Region
    Private Sub bindComboIDBankBeban()
        Dim strcon As String = GetConnectionString()
        Dim dt As New DataTable
        Try
            dt = m_Company.GetComboInputBankBeban(strcon)

            Dim i As Integer


            drdBankBunga.DataValueField = "id"
            drdBankBunga.DataTextField = "name"

            drdBankBunga.DataSource = dt

            drdBankBunga.DataBind()
            drdBankBunga.Items.Insert(0, "Select One")
            drdBankBunga.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
    Private Sub bindComboIDBankAdministrasi()
        Dim strcon As String = GetConnectionString()
        Dim dt As New DataTable
        Try
            dt = m_Company.GetComboInputBankAdministrasi(strcon)

            Dim i As Integer


            drdBankAdministrasi.DataValueField = "id"
            drdBankAdministrasi.DataTextField = "name"

            drdBankAdministrasi.DataSource = dt

            drdBankAdministrasi.DataBind()
            drdBankAdministrasi.Items.Insert(0, "Select One")
            drdBankAdministrasi.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " Where FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractList"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

#Region "MISC"

#Region "ClearAddForm"
    Private Sub ClearAddForm()

        txtFContractNo.Text = ""
        txtFContractNo.Visible = True
        lblFContractNo.Visible = False
        lblFContractNo.Text = ""
        txtContractName.Text = ""
        drdSelectionTime.SelectedIndex = 0
        drdCurrency.SelectedIndex = 0
        txtPlafondAmmount.Text = "0"
        drdFacilityType.SelectedIndex = 0
        drdInterestType.SelectedIndex = 0
        txtInterestRate.Text = "0"
        txtLatePaymentFee.Text = "0"
        txtInterestNote.Text = ""
        txtLCGracePeriod.Text = "0"
        txtFundingCoyPortion.Text = "0"
        txtSecurityPrepaymentPenalty.Text = "0"
        drdPaymentScheme.SelectedIndex = 0
        drdRecoursetype.SelectedIndex = 0
        drdFacilityKind.SelectedIndex = 0
        txtSecurityPercentage.Text = "0"
        drdSecurityPercentageOf.SelectedIndex = 0
        drdBalanceSheetStatus.SelectedIndex = 0
        txtProvision.Text = "0"
        txtCommitmentFee.Text = "0"
        txtFeePerFacility.Text = "0"
        txtAdminFeePerAct.Text = "0"
        txtFeePerDrawDown.Text = "0"
        drdPrepaymentType.SelectedIndex = 0
        drdBPKBLocation.SelectedIndex = 0
        txtMaxDD.Text = "0"
        drdCommitmentStatus.SelectedIndex = 0
        txtContractDate.Text = ""
        txtPeriodFrom.Text = ""
        txtPeriodTo.Text = ""
        txtFinalMaturityDate.Text = ""
        txtEvaluationDate.Text = ""
        txtFloatingStart.Text = ""
        chkCorporate.Checked = False
        chkCash.Checked = False
        chkFixed.Checked = False
        chkParent.Checked = False
        chkAR.Checked = False
        txtMinimumPencairan.Text = ""
        'txtBatasAngsuran.Text = ""        
    End Sub
#End Region

#End Region

#Region "Populate Grid Criteria"
    Private Sub PopulateGridCriteria(ByVal CustomCriteria As List(Of Parameter.FundingContractCriteria))
        Dim cboKriteria, cboKriteriaValue As DropDownList
        Dim chekPilih As CheckBox
        Dim txtKriteria1 As TextBox
        Dim txtKriteria2 As TextBox

        For i As Integer = 0 To dtgPaging.Items.Count - 1
            Dim Custom As New Parameter.FundingContractCriteria


            cboKriteria = CType(dtgPaging.Items(i).FindControl("cboKriteria"), DropDownList)
            cboKriteriaValue = CType(dtgPaging.Items(i).FindControl("cboKriteriaValue"), DropDownList)
            chekPilih = CType(dtgPaging.Items(i).FindControl("ChekPilih"), CheckBox)
            txtKriteria1 = CType(dtgPaging.Items(i).FindControl("txtKriteria1"), TextBox)
            txtKriteria2 = CType(dtgPaging.Items(i).FindControl("txtKriteria2"), TextBox)

            Custom.ChekPilih = chekPilih.Checked
            Custom.CriteriaID = dtgPaging.DataKeys.Item(i).ToString.Trim

            If dtgPaging.Items(i).Cells(3).Text.Trim = "SELECT" Then
                If cboKriteria.Items.Count > 0 Then
                    Custom.CriteriaValue1 = cboKriteria.SelectedItem.Value.Trim
                Else
                    Custom.CriteriaValue1 = ""
                End If
                Custom.CriteriaValue2 = ""
                cboKriteriaValue.SelectedIndex = cboKriteria.SelectedIndex
                Custom.CriteriaValueID = cboKriteriaValue.SelectedValue.Trim
            ElseIf dtgPaging.Items(i).Cells(3).Text.Trim = "MULTIPLE SELECT" Then
                If cboKriteria.Items.Count > 0 Then
                    Custom.CriteriaValue1 = cboKriteria.SelectedItem.Value.Trim
                Else
                    Custom.CriteriaValue1 = ""
                End If
                Custom.CriteriaValue2 = ""
                cboKriteriaValue.SelectedIndex = cboKriteria.SelectedIndex
                Custom.CriteriaValueID = cboKriteriaValue.SelectedValue.Trim
            ElseIf dtgPaging.Items(i).Cells(3).Text.Trim = "MAX" Then
                Custom.CriteriaValue1 = txtKriteria1.Text.Trim
                Custom.CriteriaValue2 = ""
                Custom.CriteriaValueID = cboKriteriaValue.SelectedValue.Trim
            ElseIf dtgPaging.Items(i).Cells(3).Text.Trim = "MIN" Then
                Custom.CriteriaValue1 = txtKriteria1.Text.Trim
                Custom.CriteriaValue2 = ""
                Custom.CriteriaValueID = cboKriteriaValue.SelectedValue.Trim
            ElseIf dtgPaging.Items(i).Cells(3).Text.Trim = "RANGE" Then
                Custom.CriteriaValue1 = txtKriteria1.Text.Trim
                Custom.CriteriaValue2 = txtKriteria2.Text.Trim
                Custom.CriteriaValueID = cboKriteriaValue.SelectedValue.Trim
            End If

            Custom.FundingCoyID = Me.CompanyID.Trim
            Custom.FundingContractNo = Me.FundingContractNo.Trim
            Custom.BankID = Me.BankID.Trim

            CustomCriteria.Add(Custom)
        Next
    End Sub
#End Region

#Region "ADD"
    Private Function Add() As Boolean
        Dim customClass As New Parameter.FundingContract
        Dim sType As String
        Dim str As String = CKEditor1.Text
        Dim str1 As String = Server.HtmlEncode(str)
        Dim str2 As String = Server.HtmlDecode(str)
        Try
            If drdFacilityKind.SelectedItem.Value = "JFINC" Then
                If CDbl(txtFundingCoyPortion.Text) < 90 Then
                    ShowMessage(lblMessage, "Porsi Bank untuk Join Finance tidak boleh kurang dari 90%", True)
                    Return False
                End If
            End If

            With customClass
                .strConnection = GetConnectionString()

                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = txtFContractNo.Text.Trim
                .ContractName = txtContractName.Text
                .FlagCS = drdSelectionTime.SelectedItem.Value
                .CurrencyID = drdCurrency.SelectedItem.Value
                .PlafondAmount = CDec(txtPlafondAmmount.Text.Trim)
                .FacilityType = drdFacilityType.SelectedItem.Value
                .RateToFundingCoy = CDec(txtInterestRate.Text)
                .LCPerDay = CDec(txtLatePaymentFee.Text)
                .InterestType = drdInterestType.SelectedItem.Value
                .InterestNotes = txtInterestNote.Text
                .LCGracePeriod = CInt(txtLCGracePeriod.Text.Trim)
                .FlagCoBranding = False
                .FundingCoyPortion = txtFundingCoyPortion.Text
                .PrepaymentPenalty = CDec(txtSecurityPrepaymentPenalty.Text)
                .PaymentScheme = drdPaymentScheme.SelectedItem.Value
                .RecourseType = drdRecoursetype.SelectedItem.Value
                .AcqActiveStatus = True
                .CashHoldBack = 0
                .ContractStatus = ""
                .FacilityKind = drdFacilityKind.SelectedItem.Value
                .SecurityCoveragePercentage = CDec(txtSecurityPercentage.Text.Trim)
                .SecurityCoverageType = drdSecurityPercentageOf.SelectedItem.Value

                .BalanceSheetStatus = drdBalanceSheetStatus.SelectedItem.Value
                .ProvisionFeeAmount = CDec(txtProvision.Text.Trim)
                .AdminFeeFacility = CDec(txtFeePerFacility.Text.Trim)
                .AdminFeePerAccount = CDec(txtAdminFeePerAct.Text.Trim)
                .AdminFeePerDrawDown = CDec(txtFeePerDrawDown.Text.Trim)
                .CommitmentFee = CDec(txtCommitmentFee.Text.Trim)

                If txtMaxDD.Text.Trim = "" Then
                    .MaximumDateForDD = 0
                Else
                    .MaximumDateForDD = CDec(txtMaxDD.Text.Trim)
                End If

                .PrepaymentType = drdPrepaymentType.SelectedItem.Value
                .AssetDocLocation = drdBPKBLocation.SelectedItem.Value

                If CBool(drdCommitmentStatus.SelectedItem.Value) Then
                    .CommitmentStatus = True
                Else
                    .CommitmentStatus = False
                End If

                sType = "0"
                If chkCorporate.Checked Then
                    sType = "1"
                End If
                If chkParent.Checked Then
                    sType += "2"
                End If
                If chkCash.Checked Then
                    sType += "3"
                End If
                If chkFixed.Checked Then
                    sType += "4"
                End If
                If chkAR.Checked Then
                    sType += "5"
                End If
                .SecurityType = sType

                .NegCovDate = ConvertDate2("02/09/1976")
                If txtContractDate.Text.Trim = "" Then
                    .ContractDate = ConvertDate2("02/09/1976")
                Else
                    .ContractDate = ConvertDate2(txtContractDate.Text)
                End If
                If txtPeriodFrom.Text.Trim = "" Then
                    .PeriodFrom = ConvertDate2("02/09/1976")
                Else
                    .PeriodFrom = ConvertDate2(txtPeriodFrom.Text)
                End If
                If txtPeriodTo.Text.Trim = "" Then
                    .PeriodTo = ConvertDate2("02/09/1976")
                Else
                    .PeriodTo = ConvertDate2(txtPeriodTo.Text)
                End If
                If txtFinalMaturityDate.Text.Trim = "" Then
                    .FinalMaturityDate = ConvertDate2("02/09/1976")
                Else
                    .FinalMaturityDate = ConvertDate2(txtFinalMaturityDate.Text)
                End If
                If txtEvaluationDate.Text.Trim = "" Then
                    .EvaluationDate = ConvertDate2("02/09/1976")
                Else
                    .EvaluationDate = ConvertDate2(txtEvaluationDate.Text)
                End If
                If txtFloatingStart.Text.Trim = "" Then
                    .FloatingStart = ConvertDate2("02/09/1976")
                Else
                    .FloatingStart = ConvertDate2(txtFloatingStart.Text)
                End If

                '.Mirroring = ddlMirroring.SelectedValue
                .persenPokokHutang = CDbl(txtPersenPokokHutang.Text.Trim)
                .NotarisName = Trim(txtNotaryName.Text)
                If drdPaymentScheme.SelectedValue = "2" Or drdPaymentScheme.SelectedValue = "3" Then
                    .TglJatuhTempoSpesifik = ConvertDate2(txtSpecificDate.Text)
                Else
                    .TglJatuhTempoSpesifik = "01/01/1900"
                End If
                .JumlahHaridlmBulan = rblHariBulan.SelectedValue.ToString
                .MinimumPencairan = CDec(txtMinimumPencairan.Text.Trim)
                '.BatasAngsuran = CInt(txtBatasAngsuran.Text.Trim)
                .BatasAngsuran = 0
                .CaraBayar = ddlCaraPembayaran.SelectedValue.Trim

                .COAFunding = UcAPBankPaymentAllocation1.ComboID
                .COAIntExp = txtCOAIntExpense.Text.Trim
                .COAPrepaid = txtCOAPrep.Text.Trim
                .Pinalty = CDbl(txtPinalty.Text)
                .PeriodeBulan = CInt(txtPeriodeBulan.Text)
                .Req = str2
                .CoaBeban = drdBankBunga.SelectedValue.Trim
                .CoaAdmin = drdBankAdministrasi.SelectedValue.Trim

            End With

            m_Company.AddFundingContract(customClass)
            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
            Return True
        Catch ex As Exception

            ShowMessage(lblMessage, "Error: " & ex.Message & "<br>" & ex.Source, True)
            Return False
        End Try
    End Function

#End Region

#Region "EDIT"
    Private Function edit(ByVal Companyid As String) As Boolean
        Dim customClass As New Parameter.FundingContract
        Dim sType As String
        Dim str As String = CKEditor1.Text
        Dim str1 As String = Server.HtmlEncode(str)
        Dim str2 As String = Server.HtmlDecode(str)

        If drdFacilityKind.SelectedItem.Value = "JFINC" Then
            If CDbl(txtFundingCoyPortion.Text) < 90 Then
                ShowMessage(lblMessage, "Porsi Bank untuk Join Finance tidak boleh kurang dari 90%", True)
                Return False
            End If
        End If
        With customClass
            .strConnection = GetConnectionString()

            .BankId = Me.BankID
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Companyid
            .ContractName = txtContractName.Text
            .FlagCS = drdSelectionTime.SelectedItem.Value
            .CurrencyID = drdCurrency.SelectedItem.Value
            .PlafondAmount = CDec(txtPlafondAmmount.Text.Trim)
            .FacilityType = drdFacilityType.SelectedItem.Value
            .RateToFundingCoy = CDec(txtInterestRate.Text)
            .LCPerDay = CDec(txtLatePaymentFee.Text)
            .InterestType = drdInterestType.SelectedItem.Value
            .InterestNotes = txtInterestNote.Text
            .LCGracePeriod = CInt(txtLCGracePeriod.Text.Trim)
            .FlagCoBranding = False
            .FundingCoyPortion = txtFundingCoyPortion.Text
            .PrepaymentPenalty = CDec(txtSecurityPrepaymentPenalty.Text)
            .PaymentScheme = drdPaymentScheme.SelectedItem.Value
            .RecourseType = drdRecoursetype.SelectedItem.Value
            .AcqActiveStatus = True
            .CashHoldBack = 0
            .ContractStatus = ""
            .FacilityKind = drdFacilityKind.SelectedItem.Value
            .SecurityCoveragePercentage = CDec(txtSecurityPercentage.Text.Trim)
            .SecurityCoverageType = drdSecurityPercentageOf.SelectedItem.Value

            .BalanceSheetStatus = drdBalanceSheetStatus.SelectedItem.Value
            .ProvisionFeeAmount = CDec(txtProvision.Text.Trim)
            .AdminFeeFacility = CDec(txtFeePerFacility.Text.Trim)
            .AdminFeePerAccount = CDec(txtAdminFeePerAct.Text.Trim)
            .AdminFeePerDrawDown = CDec(txtFeePerDrawDown.Text.Trim)
            .CommitmentFee = CDec(txtCommitmentFee.Text.Trim)
            If txtMaxDD.Text.Trim = "" Then
                .MaximumDateForDD = 0
            Else
                .MaximumDateForDD = CDec(txtMaxDD.Text.Trim)
            End If

            .PrepaymentType = drdPrepaymentType.SelectedItem.Value
            .AssetDocLocation = drdBPKBLocation.SelectedItem.Value
            If CBool(drdCommitmentStatus.SelectedItem.Value) Then
                .CommitmentStatus = True
            Else
                .CommitmentStatus = False
            End If


            sType = "0"
            If chkCorporate.Checked Then
                sType = "1"
            End If
            If chkParent.Checked Then
                sType += "2"
            End If
            If chkCash.Checked Then
                sType += "3"
            End If
            If chkFixed.Checked Then
                sType += "4"
            End If
            If chkAR.Checked Then
                sType += "5"
            End If
            .SecurityType = sType

            .NegCovDate = ConvertDate2("02/09/1976")
            If txtContractDate.Text.Trim = "" Then
                .ContractDate = ConvertDate2("02/09/1976")
            Else
                .ContractDate = ConvertDate2(txtContractDate.Text)
            End If
            If txtPeriodFrom.Text.Trim = "" Then
                .PeriodFrom = ConvertDate2("02/09/1976")
            Else
                .PeriodFrom = ConvertDate2(txtPeriodFrom.Text)
            End If
            If txtPeriodTo.Text.Trim = "" Then
                .PeriodTo = ConvertDate2("02/09/1976")
            Else
                .PeriodTo = ConvertDate2(txtPeriodTo.Text)
            End If
            If txtFinalMaturityDate.Text.Trim = "" Then
                .FinalMaturityDate = ConvertDate2("02/09/1976")
            Else
                .FinalMaturityDate = ConvertDate2(txtFinalMaturityDate.Text)
            End If
            If txtEvaluationDate.Text.Trim = "" Then
                .EvaluationDate = ConvertDate2("02/09/1976")
            Else
                .EvaluationDate = ConvertDate2(txtEvaluationDate.Text)
            End If
            If txtFloatingStart.Text.Trim = "" Then
                .FloatingStart = ConvertDate2("02/09/1976")
            Else
                .FloatingStart = ConvertDate2(txtFloatingStart.Text)
            End If

            '.Mirroring = ddlMirroring.SelectedValue
            .persenPokokHutang = CDbl(txtPersenPokokHutang.Text.Trim)
            .NotarisName = txtNotaryName.Text.Trim
            If drdPaymentScheme.SelectedValue = "2" Or drdPaymentScheme.SelectedValue = "3" Then
                .TglJatuhTempoSpesifik = ConvertDate2(txtSpecificDate.Text)
            Else
                .TglJatuhTempoSpesifik = "01/01/1900"
            End If
            .JumlahHaridlmBulan = rblHariBulan.SelectedValue.ToString
            .MinimumPencairan = CDec(txtMinimumPencairan.Text.Trim)
            .BatasAngsuran = 0
            .CaraBayar = ddlCaraPembayaran.SelectedValue.Trim

            .COAFunding = UcAPBankPaymentAllocation1.ComboID
            .COAIntExp = txtCOAIntExpense.Text.Trim
            .COAPrepaid = txtCOAPrep.Text.Trim

            .Pinalty = CDbl(txtPinalty.Text)
            .PeriodeBulan = CInt(txtPeriodeBulan.Text)
            .Req = str2
            .CoaBeban = drdBankBunga.SelectedValue.Trim
            .CoaAdmin = drdBankAdministrasi.SelectedValue.Trim

        End With

        Try
            m_Company.EditFundingContract(customClass)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

#End Region

#Region "SAVE CRITERIA"
    Private Function SaveCriteria() As Boolean
        Try
            Dim CollCriteria As New List(Of Parameter.FundingContractCriteria)
            Dim CollcustomCriteria As New List(Of Parameter.FundingContractCriteria)
            Dim customCriteria As New Parameter.FundingContractCriteria

            customCriteria.FundingCoyID = Me.CompanyID.Trim
            customCriteria.FundingContractNo = Me.FundingContractNo.Trim
            customCriteria.BankID = Me.BankID.Trim
            customCriteria.strConnection = GetConnectionString()
            CollcustomCriteria = m_Kriteria.GetFundingContractCriteriaListColl(customCriteria)
            PopulateGridCriteria(CollCriteria)

            For i As Integer = 0 To CollCriteria.Count - 1
                customCriteria = New Parameter.FundingContractCriteria
                customCriteria = CollCriteria(i)
                customCriteria.strConnection = GetConnectionString()

                If CollcustomCriteria.Count > 0 Then
                    Dim C = From W In CollcustomCriteria _
                    Where W.CriteriaID.Trim = customCriteria.CriteriaID.Trim _
                    Select W.CriteriaID.Trim

                    If customCriteria.ChekPilih And C.Count > 0 Then
                        'Update
                        m_Kriteria.FundingContractCriteriaSaveEdit(customCriteria)
                    End If
                    If customCriteria.ChekPilih And C.Count = 0 Then
                        'Add
                        m_Kriteria.FundingContractCriteriaSaveAdd(customCriteria)
                    End If
                    If Not customCriteria.ChekPilih And C.Count > 0 Then
                        'Delete
                        m_Kriteria.FundingContractCriteriaDelete(customCriteria)
                    End If
                Else
                    If customCriteria.ChekPilih Then
                        'Add
                        m_Kriteria.FundingContractCriteriaSaveAdd(customCriteria)
                    End If
                End If
            Next
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try

    End Function
#End Region

#Region "View"

#Region "EditView"
    Private Sub editView(ByVal Companyid As String)
        Dim oCompany As New Parameter.FundingContract
        Dim dtCompany As New DataTable
        Dim str As String = CKEditor1.Text
        Dim str1 As String = Server.HtmlEncode(str)
        Dim str2 As String = Server.HtmlDecode(str)
        Try
            Dim strConnection As String = GetConnectionString()

            With oCompany
                .strConnection = GetConnectionString()
                .FundingContractNo = Companyid
            End With

            oCompany = m_Company.ListFundingContractByID(oCompany)
            dtCompany = oCompany.ListData

            'Isi semua field Edit Action
            lblFundingCompany.Text = lblBankName.Text
            lblBranch.Text = lblFundingCoyName.Text
            txtFContractNo.Visible = False
            lblFContractNo.Visible = True
            lblFContractNo.Text = Companyid
            txtContractName.Text = CStr(dtCompany.Rows(0).Item("ContractName")).Trim
            drdSelectionTime.SelectedIndex = drdSelectionTime.Items.IndexOf(drdSelectionTime.Items.FindByValue(CStr(dtCompany.Rows(0).Item("FlagCS"))))
            drdCurrency.SelectedIndex = drdCurrency.Items.IndexOf(drdCurrency.Items.FindByValue(CStr(dtCompany.Rows(0).Item("CurrencyID"))))
            txtPlafondAmmount.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("PlafondAmount")).Trim, 0)
            drdFacilityType.SelectedIndex = drdFacilityType.Items.IndexOf(drdFacilityType.Items.FindByValue(CStr(dtCompany.Rows(0).Item("FacilityType"))))
            drdInterestType.SelectedIndex = drdInterestType.Items.IndexOf(drdInterestType.Items.FindByValue(CStr(dtCompany.Rows(0).Item("InterestType"))))
            txtInterestNote.Text = CStr(dtCompany.Rows(0).Item("InterestNotes")).Trim
            txtLCGracePeriod.Text = CStr(dtCompany.Rows(0).Item("LCGracePeriod")).Trim
            txtFundingCoyPortion.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("FundingCoyPortion")).Trim, 2)
            txtSecurityPrepaymentPenalty.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("PrepaymentPenalty")).Trim, 2)
            drdPaymentScheme.SelectedIndex = drdPaymentScheme.Items.IndexOf(drdPaymentScheme.Items.FindByValue(CStr(dtCompany.Rows(0).Item("PaymentScheme"))))
            drdRecoursetype.SelectedIndex = drdRecoursetype.Items.IndexOf(drdRecoursetype.Items.FindByValue(CStr(dtCompany.Rows(0).Item("RecourseType"))))
            drdFacilityKind.SelectedIndex = drdFacilityKind.Items.IndexOf(drdFacilityKind.Items.FindByValue(CStr(dtCompany.Rows(0).Item("FacilityKind")).Trim))
            txtSecurityPercentage.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("SecurityCoveragePercentage")).Trim, 2)
            drdSecurityPercentageOf.SelectedIndex = drdSecurityPercentageOf.Items.IndexOf(drdSecurityPercentageOf.Items.FindByValue(CStr(dtCompany.Rows(0).Item("SecurityCoverageType"))))
            drdBalanceSheetStatus.SelectedIndex = drdBalanceSheetStatus.Items.IndexOf(drdBalanceSheetStatus.Items.FindByValue(CStr(dtCompany.Rows(0).Item("BalanceSheetStatus"))))
            txtProvision.Text = CStr(dtCompany.Rows(0).Item("ProvisionFeeAmount")).Trim
            txtCommitmentFee.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("CommitmentFee")).Trim, 2)
            txtInterestRate.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("RateToFundingCoy")).Trim, 2)
            txtLatePaymentFee.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("LCPerDay")).Trim, 2)
            txtFeePerFacility.Text = CStr(dtCompany.Rows(0).Item("AdminFeeFacility")).Trim
            txtAdminFeePerAct.Text = CStr(dtCompany.Rows(0).Item("AdminFeePerAccount")).Trim
            txtFeePerDrawDown.Text = CStr(dtCompany.Rows(0).Item("AdminFeePerDrawDown")).Trim
            drdPrepaymentType.SelectedIndex = drdPrepaymentType.Items.IndexOf(drdPrepaymentType.Items.FindByValue(CStr(dtCompany.Rows(0).Item("PrepaymentType"))))
            drdBPKBLocation.SelectedIndex = drdBPKBLocation.Items.IndexOf(drdBPKBLocation.Items.FindByValue(CStr(dtCompany.Rows(0).Item("AssetDocLocation"))))
            txtMaxDD.Text = FormatNumber(CStr(dtCompany.Rows(0).Item("MaximumDateForDD")).Trim, 0)
            drdCommitmentStatus.SelectedIndex = drdCommitmentStatus.Items.IndexOf(drdCommitmentStatus.Items.FindByValue(CStr(dtCompany.Rows(0).Item("CommitmentStatus"))))
            txtContractDate.Text = CDate(dtCompany.Rows(0).Item("ContractDate")).ToString("dd/MM/yyyy")
            txtPeriodFrom.Text = CDate(dtCompany.Rows(0).Item("PeriodFrom")).ToString("dd/MM/yyyy")
            txtPeriodTo.Text = CDate(dtCompany.Rows(0).Item("PeriodTo")).ToString("dd/MM/yyyy")


            If IsDBNull(dtCompany.Rows(0).Item("FinalMaturityDate")) Then
                txtFinalMaturityDate.Text = ""
            Else
                txtFinalMaturityDate.Text = CDate(dtCompany.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            End If
            If IsDBNull(dtCompany.Rows(0).Item("EvaluationDate")) Then
                txtEvaluationDate.Text = ""
            Else
                txtEvaluationDate.Text = CDate(dtCompany.Rows(0).Item("EvaluationDate")).ToString("dd/MM/yyyy")
            End If
            If IsDBNull(dtCompany.Rows(0).Item("FloatingStart")) Then
                txtFloatingStart.Text = ""
            Else
                txtFloatingStart.Text = CDate(dtCompany.Rows(0).Item("FloatingStart")).ToString("dd/MM/yyyy")
            End If

            Dim item As Char
            For Each item In CStr(dtCompany.Rows(0).Item("SecurityType")).Trim
                Select Case item
                    Case "1"
                        chkCorporate.Checked = True
                    Case "2"
                        chkParent.Checked = True
                    Case "3"
                        chkCash.Checked = True
                    Case "4"
                        chkFixed.Checked = True
                    Case "5"
                        chkAR.Checked = True
                End Select
            Next

            'ddlMirroring.SelectedValue = dtCompany.Rows(0).Item("Mirroring")
            txtPersenPokokHutang.Text = FormatNumber(dtCompany.Rows(0).Item("persenPokokHutang").ToString.Trim, 2)
            txtNotaryName.Text = dtCompany.Rows(0).Item("NotarisName").ToString.Trim
            If IsDBNull(dtCompany.Rows(0).Item("tglJatuhTempoSpesifik")) Then
                txtSpecificDate.Text = ""
            Else
                txtSpecificDate.Text = CDate(dtCompany.Rows(0).Item("tglJatuhTempoSpesifik")).ToString("dd/MM/yyyy")
            End If

            toggleSpecificDateValidator()
            rblHariBulan.SelectedIndex = rblHariBulan.Items.IndexOf(rblHariBulan.Items.FindByValue(dtCompany.Rows(0).Item("JumlahHaridlmBulan").ToString))
            ddlCaraPembayaran.SelectedIndex = ddlCaraPembayaran.Items.IndexOf(ddlCaraPembayaran.Items.FindByValue(IIf(IsDBNull(dtCompany.Rows(0).Item("CaraBayar")), "", dtCompany.Rows(0).Item("CaraBayar").ToString.Trim)))
            'txtBatasAngsuran.Text = IIf(IsDBNull(dtCompany.Rows(0).Item("BatasAngsuran")), "", dtCompany.Rows(0).Item("BatasAngsuran").ToString).ToString.Trim
            txtMinimumPencairan.Text = FormatNumber(IIf(IsDBNull(dtCompany.Rows(0).Item("MinimumPencairan")), "", CStr(dtCompany.Rows(0).Item("MinimumPencairan"))).ToString.Trim, 0)
            drdFacilityKind_IndexChanged()

            UcAPBankPaymentAllocation1.ComboID = CStr(dtCompany.Rows(0).Item("COAFunding")).Trim
            'UcAPBankPaymentAllocation1.BindData()

            txtCOAIntExpense.Text = CStr(dtCompany.Rows(0).Item("COAIntExp")).Trim
            txtCOAPrep.Text = CStr(dtCompany.Rows(0).Item("COAPrepaid")).Trim

            txtPeriodeTo_onChage()
            CKEditor1.Text = CStr(dtCompany.Rows(0).Item("Notes")).Trim
            drdBankBunga.SelectedIndex = drdBankBunga.Items.IndexOf(drdBankBunga.Items.FindByValue(CStr(dtCompany.Rows(0).Item("COABebanBunga"))))
            drdBankAdministrasi.SelectedIndex = drdBankAdministrasi.Items.IndexOf(drdBankAdministrasi.Items.FindByValue(CStr(dtCompany.Rows(0).Item("COABebanAdministrasi"))))
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region "JustView"
    Private Sub JustView(ByVal Companyid As String)
        Dim oCompany As New Parameter.FundingContract
        Dim dtCompany As New DataTable
        Dim item As Char

        Try
            Dim strConnection As String = GetConnectionString()

            With oCompany
                .strConnection = GetConnectionString()
                .FundingContractNo = Companyid
                .SortBy = ""
                '.WhereCond = "FundingCoyID='" & Companyid & "'"
            End With

            oCompany = m_Company.ListFundingContractByID(oCompany)
            dtCompany = oCompany.ListData

            'bindComboIDBank()
            'Isi semua field View Action
            lblFundingCompanyV.Text = Me.BankName
            lblBranchV.Text = Me.CompanyName
            lblFundingContractNo.Text = Companyid
            lblContractName.Text = CStr(dtCompany.Rows(0).Item("ContractName")).Trim
            If CStr(dtCompany.Rows(0).Item("FlagCS")) = "A" Then
                lblSelectionTime.Text = "After"
            Else
                lblSelectionTime.Text = "Before"
            End If

            lblPlafondAmount.Text = FormatNumber(dtCompany.Rows(0).Item("PlafondAmount"), 0).Trim
            If CStr(dtCompany.Rows(0).Item("FacilityType")) = "R" Then
                lblFacilityType.Text = "Revolving"
            Else
                lblFacilityType.Text = "Non Revolving"
            End If
            item = CStr(dtCompany.Rows(0).Item("InterestType")).Trim
            Select Case item
                Case "L"
                    lblInterestType.Text = "Floating"
                Case "X"
                    lblInterestType.Text = "Fixed"
                Case "B"
                    lblInterestType.Text = "Fixed Per Batch"
            End Select
            lblInterestRate.Text = FormatNumber(dtCompany.Rows(0).Item("RateToFundingCoy"), 0).Trim
            lblInterestNote.Text = CStr(dtCompany.Rows(0).Item("InterestNotes")).Trim
            lblLCGracePeriod.Text = CStr(dtCompany.Rows(0).Item("LCGracePeriod")).Trim
            lblFundingCoyPortion.Text = FormatNumber(dtCompany.Rows(0).Item("FundingCoyPortion"), 0).Trim
            lblPrepaymentPenalty.Text = FormatNumber(dtCompany.Rows(0).Item("PrepaymentPenalty"), 0).Trim
            item = CStr(dtCompany.Rows(0).Item("PaymentScheme")).Trim
            Select Case item
                Case "1"
                    lblPaymentScheme.Text = "Principal & Interest on Drawdown Date"
                Case "2"
                    lblPaymentScheme.Text = "Principal on Drawdown Date Interest on EoM"
                Case "3"
                    lblPaymentScheme.Text = "Principal & Interest on Due Date"
                Case "4"
                    lblPaymentScheme.Text = "Principal & Interest on EoM"
                Case "5"
                    lblPaymentScheme.Text = "Interest on Drawdown Date Principal on EoL"
            End Select
            item = CStr(dtCompany.Rows(0).Item("Recoursetype")).Trim
            Select Case item
                Case "W"
                    lblRecourseType.Text = "Without ecourse"
                Case "F"
                    lblRecourseType.Text = "Full Recourse"
                Case "L"
                    lblRecourseType.Text = "Limited Recourse"
            End Select
            item = CStr(dtCompany.Rows(0).Item("SecurityCoverageType")).Trim
            Select Case item
                Case "P"
                    lblSecurityPercentageOf.Text = "Without ecourse"
                Case "A"
                    lblSecurityPercentageOf.Text = "Full Recourse"
                Case "O"
                    lblSecurityPercentageOf.Text = "Limited Recourse"
            End Select
            item = CStr(dtCompany.Rows(0).Item("BalanceSheetStatus")).Trim
            Select Case item
                Case "N"
                    lblBalanceSheetStatus.Text = "On Balance Sheet"
                Case "F"
                    lblBalanceSheetStatus.Text = "Of Balance Sheet"
            End Select
            item = CStr(dtCompany.Rows(0).Item("PrepaymentType")).Trim
            Select Case item
                Case "C"
                    lblPrepaymentType.Text = "Must Be Paid"
                Case "B"
                    lblPrepaymentType.Text = "Must Change to Another BPKB"
            End Select
            item = CStr(dtCompany.Rows(0).Item("AssetDocLocation")).Trim
            Select Case item
                Case "H"
                    lblBPKBLocation.Text = "Head Office"
                Case "B"
                    lblBPKBLocation.Text = "Branch of Aggrement Holder"
                Case "P"
                    lblBPKBLocation.Text = "Head Office of Funding Company"
                Case "C"
                    lblBPKBLocation.Text = "Branch of Funding Company"
            End Select
            Select Case CStr(dtCompany.Rows(0).Item("CommitmentStatus")).Trim
                Case "True"
                    lblCommitmentStatus.Text = "Yes"
                Case "False"
                    lblCommitmentStatus.Text = "No"
            End Select

            lblLatePaymentFee.Text = FormatNumber(dtCompany.Rows(0).Item("RateToFundingCoy"), 0).Trim
            lblSecurityPercentage.Text = FormatNumber(dtCompany.Rows(0).Item("SecurityCoveragePercentage"), 0).Trim
            lblProvision.Text = FormatNumber(dtCompany.Rows(0).Item("ProvisionFeeAmount"), 0).Trim
            lblCommitmentFee.Text = FormatNumber(dtCompany.Rows(0).Item("CommitmentFee"), 0).Trim
            lblFeePerFacility.Text = FormatNumber(dtCompany.Rows(0).Item("AdminFeeFacility"), 0).Trim
            lblAdminFeePerAct.Text = FormatNumber(dtCompany.Rows(0).Item("AdminFeePerAccount"), 0).Trim
            lblFeePerDrawDown.Text = FormatNumber(dtCompany.Rows(0).Item("AdminFeePerDrawDown"), 0).Trim
            lblContractDate.Text = CDate(dtCompany.Rows(0).Item("ContractDate")).ToString("dd/MM/yyyy")
            lblPeriodFrom.Text = CDate(dtCompany.Rows(0).Item("PeriodFrom")).ToString("dd/MM/yyyy")
            lblPeriodTo.Text = CDate(dtCompany.Rows(0).Item("PeriodTo")).ToString("dd/MM/yyyy")
            lblMaxDD.Text = CStr(dtCompany.Rows(0).Item("MaximumDateForDD")).Trim

            Select Case CStr(dtCompany.Rows(0).Item("FacilityKind"))
                Case "JFINC"
                    lblFacilityKind.Text = "Joint Financing"
                Case "KUK"
                    lblFacilityKind.Text = "KUK"
                Case "CHANN"
                    lblFacilityKind.Text = "Channeling"
                Case "ASBUY"
                    lblFacilityKind.Text = "Asset Buy"
                Case "UKM"
                    lblFacilityKind.Text = "UKM"
            End Select

            lblCurrency.Text = CStr(dtCompany.Rows(0).Item("CurrencyID"))

            If IsDBNull(dtCompany.Rows(0).Item("FinalMaturityDate")) Then
                lblFinalMaturityDate.Text = "-"
            Else
                lblFinalMaturityDate.Text = CDate(dtCompany.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            End If
            If IsDBNull(dtCompany.Rows(0).Item("EvaluationDate")) Then
                lblEvaluationDate.Text = "-"
            Else
                lblEvaluationDate.Text = CDate(dtCompany.Rows(0).Item("EvaluationDate")).ToString("dd/MM/yyyy")
            End If
            If IsDBNull(dtCompany.Rows(0).Item("FloatingStart")) Then
                lblFloatingStart.Text = "-"
            Else
                lblFloatingStart.Text = CDate(dtCompany.Rows(0).Item("FloatingStart")).ToString("dd/MM/yyyy")
            End If

            chkCorporateV.Checked = False
            chkParentV.Checked = False
            chkParentV.Checked = False
            chkFixedV.Checked = False
            chkARV.Checked = False

            For Each item In CStr(dtCompany.Rows(0).Item("SecurityType")).Trim
                Select Case item
                    Case "1"
                        chkCorporateV.Checked = True
                    Case "2"
                        chkParentV.Checked = True
                    Case "3"
                        chkParentV.Checked = True
                    Case "4"
                        chkFixedV.Checked = True
                    Case "5"
                        chkARV.Checked = True
                End Select
            Next

            lblMirroring.Text = IIf(CBool(dtCompany.Rows(0).Item("Mirroring")), "True", "No")

            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#End Region

#Region " BindGridPemilihanKontrak "
    Sub BindGridKontrak(ByVal _FundingContractNo As String)
        Dim dtsEntity As DataTable = Nothing
        Dim oContract As New Parameter.FundingContractCriteria

        With oContract
            .strConnection = GetConnectionString()
            .FundingContractNo = _FundingContractNo
            .BankID = Me.BankID
        End With

        oContract = m_Kriteria.GetFundingContractCriteriaList(oContract)
        dtsEntity = oContract.Listdata
        dtgPaging.DataSource = dtsEntity
        Try
            dtgPaging.DataBind()
        Catch
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        End Try
    End Sub
#End Region

#Region "Bind Combo"
    Sub BindCombo(ByVal cboKriteria As DropDownList, ByVal Criteria As String, Optional ByVal IsCriteriaValue As Boolean = False)
        Dim oCustom As New Parameter.FundingContractCriteria
        Dim tbl As New DataTable
        oCustom.CriteriaID = Criteria.Trim
        oCustom.strConnection = GetConnectionString()
        tbl = m_Kriteria.GetCombo(oCustom)

        If IsCriteriaValue Then
            cboKriteria.DataTextField = "CriteriaDescription"
            cboKriteria.DataValueField = "CriteriaValueID"
        Else
            cboKriteria.DataTextField = "CriteriaDescription"
            cboKriteria.DataValueField = "CriteriaValue"
        End If
        
        cboKriteria.DataSource = tbl
        cboKriteria.DataBind()
    End Sub
#End Region


#Region "Handles"

    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim imbDelete As ImageButton
        'Dim lbCompanyID As LinkButton
        Dim lbl1 As Label
        Dim hyContract As HyperLink

        If e.Item.ItemIndex >= 0 Then

            lbl1 = CType(e.Item.FindControl("lbl1"), Label)

            If lbl1.Text.Trim = "B" Then
                lbl1.Text = "Before"
            Else
                lbl1.Text = "After"
            End If
            hyContract = CType(e.Item.FindControl("lnkContractNo"), HyperLink)
            hyContract.NavigateUrl = "javascript:OpenFundingCompanyContractView('" & "channeling" & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & hyContract.Text.Trim & "')"

            'Me.CompanyID = e.Item.Cells(0).Text

            'lbCompanyID = CType(e.Item.FindControl("lnkCompanyID"), LinkButton)
            'lbCompanyID.Attributes.Add("OnClick", "return OpenWindowCompany('" & Me.CompanyID & "')")

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim hdncheckPilih As HtmlInputHidden
        Dim chekPilih As CheckBox
        Dim txtKriteria1 As TextBox
        Dim txtKriteria2 As TextBox
        Dim cboKriteria As DropDownList
        Dim cboKriteriaValue As DropDownList

        If e.Item.ItemIndex >= 0 Then
            hdncheckPilih = CType(e.Item.FindControl("hdncheckPilih"), HtmlInputHidden)
            chekPilih = CType(e.Item.FindControl("ChekPilih"), CheckBox)
            txtKriteria1 = CType(e.Item.FindControl("txtKriteria1"), TextBox)
            txtKriteria2 = CType(e.Item.FindControl("txtKriteria2"), TextBox)
            cboKriteria = CType(e.Item.FindControl("cboKriteria"), DropDownList)
            cboKriteriaValue = CType(e.Item.FindControl("cboKriteriaValue"), DropDownList)

            chekPilih.Checked = CBool(hdncheckPilih.Value.Trim)
            e.Item.Cells(1).Text = e.Item.ItemIndex + 1

            If e.Item.Cells(3).Text.Trim = "SEL" Then
                txtKriteria1.Visible = False
                cboKriteria.Visible = True
                e.Item.Cells(3).Text = "SELECT"
                BindCombo(cboKriteria, dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString)
                cboKriteria.Items.IndexOf(cboKriteria.Items.FindByValue(e.Item.Cells(4).Text.Trim))
                cboKriteria.SelectedIndex = cboKriteria.Items.IndexOf(cboKriteria.Items.FindByValue(e.Item.Cells(4).Text.Trim))
            ElseIf e.Item.Cells(3).Text.Trim = "MUL" Then
                txtKriteria1.Visible = False
                cboKriteria.Visible = True
                e.Item.Cells(3).Text = "MULTIPLE SELECT"
                BindCombo(cboKriteria, dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString)
                cboKriteria.SelectedIndex = cboKriteria.Items.IndexOf(cboKriteria.Items.FindByValue(e.Item.Cells(4).Text.Trim))
            ElseIf e.Item.Cells(3).Text.Trim = "MAX" Then
                txtKriteria1.Visible = True
                cboKriteria.Visible = False
                e.Item.Cells(3).Text = "MAX"
                txtKriteria1.Text = e.Item.Cells(4).Text.Trim.Replace("&nbsp;", "")
            ElseIf e.Item.Cells(3).Text.Trim = "MIN" Then
                txtKriteria1.Visible = True
                cboKriteria.Visible = False
                e.Item.Cells(3).Text = "MIN"
                txtKriteria1.Text = e.Item.Cells(4).Text.Trim.Replace("&nbsp;", "")
            ElseIf e.Item.Cells(3).Text.Trim = "RAN" Then
                txtKriteria1.Visible = True
                txtKriteria2.Visible = True
                txtKriteria1.Text = e.Item.Cells(4).Text.Trim.Replace("&nbsp;", "")
                txtKriteria2.Text = e.Item.Cells(5).Text.Trim.Replace("&nbsp;", "")
                cboKriteria.Visible = False
                e.Item.Cells(3).Text = "RANGE"
            End If

            BindCombo(cboKriteriaValue, dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString, True)
        End If
    End Sub
#End Region

    Private Sub BtnAddnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddnew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If             
            ClearAddForm()
            PanelAllFalse()
            lblFundingCompany.Text = Me.BankName
            lblBranch.Text = Me.CompanyName           
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True
            BindGridKontrak(Nothing)
            drdFacilityKind_IndexChanged()
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                Me.FundingContractNo = txtFContractNo.Text.Trim
                If Add() Then 'If Add() And SaveCriteria() Then
                    pnlList.Visible = True
                    PanelAllFalse()
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            Case "EDIT"

                If edit(Me.FundingContractNo) Then 'If edit(Me.FundingContractNo) And SaveCriteria() Then
                    PanelAllFalse()
                    pnlList.Visible = True
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
        End Select
    End Sub

    Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        lblMessage.Visible = False
        Select Case e.CommandName
            'Case "TnC"
            '    'Me.CompanyID = e.Item.Cells(0).Text
            '    'Me.CompanyName = dtgPaging.Item.Cells(3).Text
            '    'Me.BankName = e.Item.Cells(2).Text
            '    'Me.BankID = e.Item.Cells(7).Text
            '    lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
            '    Me.FundingContractNo = lnkContractNo.Text

            '    Response.Redirect("fundingcontracttermmnt.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & e.Item.Cells(0).Text)
            Case "Doc"
                lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
                Me.FundingContractNo = lnkContractNo.Text

                Response.Redirect("fundingcontractplafondbranch.aspx?CompanyID=" & Me.CompanyID & "&Command=Doc" & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & e.Item.Cells(1).Text)

            Case "Plafond"

                lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
                Me.FundingContractNo = lnkContractNo.Text

                Response.Redirect("fundingcontractplafondbranch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & e.Item.Cells(1).Text)
            Case "Batch"

                lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
                Me.FundingContractNo = lnkContractNo.Text

                Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & e.Item.Cells(2).Text)

                'Case "NegCov"
                '    lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
                '    Me.FundingContractNo = lnkContractNo.Text
                '    Response.Redirect("FundingNegCovMnt.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & e.Item.Cells(0).Text)
            Case "View"
                'If checkFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
                '    If sessioninvalid() Then
                '        Exit Sub
                '    End If

                '    lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), LinkButton)
                '    Me.FundingContractNo = lnkContractNo.Text
                '    pnlList.Visible = False
                '    pnlAddEdit.Visible = False
                '    pnlView.Visible = True
                '    'Me.ActionAddEdit = "VIEW"
                '    'lblMenuAddEdit.Text = "VIEW"
                '    JustView(Me.FundingContractNo)
                'End If
            Case "Edit"

                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                    lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
                    Me.FundingContractNo = lnkContractNo.Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    editView(Me.FundingContractNo)
                    BindGridKontrak(Me.FundingContractNo)
                End If

            Case "Delete"

                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
                    Me.FundingContractNo = lnkContractNo.Text
                    Dim customClass As New Parameter.FundingContract
                    With customClass
                        .strConnection = GetConnectionString()
                        .FundingContractNo = Me.FundingContractNo
                    End With

                    Try
                        m_Company.DeleteFundingContract(customClass)
                        ShowMessage(lblMessage, "Hapus Data Berhasil ", False)
                        SaveCriteria()
                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                    SaveCriteria()
                End If
            Case "Rate"
                hdfBankID.Value = Me.BankID
                lblBankNameRate.Text = Me.BankName
                hdfCompanyID.Value = Me.CompanyID
                lblFundingCoyNameRate.Text = Me.CompanyName
                hdfFundingContractNo.Value = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink).Text.Trim
                lblFundingContractNoRate.Text = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink).Text.Trim
                PanelAllFalse()
                pnlRate.Visible = True
                BindDataRate()
        End Select
    End Sub
    Sub BindDataRate()
        Dim oCustom As New Parameter.RefundInsentif
        Dim m_Insentif As New RefundInsentifController

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " BankID = '" & hdfBankID.Value.Trim & "' and FundingCoyID = '" & hdfCompanyID.Value.Trim & "' and FundingContractNo = '" & hdfFundingContractNo.Value.Trim & "'"
            .SPName = "spGetFundingContractRate"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            dtgRateFunding.DataSource = oCustom.ListDataTable
            dtgRateFunding.DataBind()
        End If
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        PanelAllFalse()
        lblMessage.Text = ""
    End Sub

    Private Sub btnBacknew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBacknew.Click
        Response.Redirect("FundingCompanyMaintenance.aspx")
    End Sub

    Private Sub drdPaymentScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drdPaymentScheme.SelectedIndexChanged
        toggleSpecificDateValidator()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindGridEntity(Me.SearchBy, Me.SortBy)
        PanelAllFalse()
        pnlList.Visible = True
    End Sub

    Sub toggleSpecificDateValidator()
        If drdPaymentScheme.SelectedValue = "2" Or drdPaymentScheme.SelectedValue = "3" Then
            'rfvSpecificDate.Enabled = True
            divSpecificDate.Visible = True
        Else
            'rfvSpecificDate.Enabled = False
            divSpecificDate.Visible = False
        End If
    End Sub


    Public Function TogglePanel() As String
        Return "javascript:toggle('" & pnlAddEdit.ClientID & "');"
    End Function
    Private Sub drdFacilityKind_DropDownChanged() Handles drdFacilityKind.SelectedIndexChanged
        drdFacilityKind_IndexChanged()
    End Sub
    Sub drdFacilityKind_IndexChanged()
        If drdFacilityKind.SelectedValue = "JFINC" Or drdFacilityKind.SelectedValue = "CHANN" Then
            ddlCaraPembayaran.SelectedValue = "JT"
            ddlCaraPembayaran.Enabled = True
            txtFeePerDrawDown.Enabled = False
            txtFeePerFacility.Enabled = False
            txtPersenPokokHutang.Enabled = False
            txtSecurityPercentage.Enabled = False
            txtFundingCoyPortion.Enabled = True
            txtPersenPokokHutang.Text = "0"

            txtSecurityPercentage.Text = "0"
            txtSecurityPercentage.RangeValidatorMinimumValue = "0"
            InitiateUCnumberFormat(txtSecurityPercentage, False, False)
        Else
            ddlCaraPembayaran.Enabled = False
            txtFeePerDrawDown.Enabled = True
            txtFeePerFacility.Enabled = True
            txtPersenPokokHutang.Enabled = True
            txtSecurityPercentage.Enabled = True
            'txtSecurityPercentage.RangeValidatorMinimumValue = "100.1"
            txtSecurityPercentage.RangeValidatorMinimumValue = "100"
            txtFundingCoyPortion.Enabled = False
            txtFundingCoyPortion.Text = "0"
        End If

    End Sub

    Private Sub BtnCancelRate_Click(sender As Object, e As System.EventArgs) Handles BtnCancelRate.Click
        BindGridEntity(Me.SearchBy, Me.SortBy)
        PanelAllFalse()
        pnlList.Visible = True        
    End Sub

    Private Sub BtnSaveRate_Click(sender As Object, e As System.EventArgs) Handles BtnSaveRate.Click
        Dim oCustom As New Parameter.FundingContractRate
        Dim TenorFrom As Integer = 0
        Dim TenorTo As Integer = 0

        Try
            For index = 0 To dtgRateFunding.Items.Count - 1
                oCustom = New Parameter.FundingContractRate

                TenorFrom = TenorFrom + 1
                TenorTo = (index + 1) * 12

                With oCustom
                    .strConnection = GetConnectionString()
                    .BankID = hdfBankID.Value.Trim
                    .FundingCoyID = hdfCompanyID.Value.Trim
                    .FundingContractNo = hdfFundingContractNo.Value.Trim
                    .TenorFrom = TenorFrom
                    .TenorTo = TenorTo
                    .FundingRate = CDec(IIf(CType(dtgRateFunding.Items(index).FindControl("txtRate"), TextBox).Text.Trim = "", "0", CType(dtgRateFunding.Items(index).FindControl("txtRate"), TextBox).Text))
                End With

                TenorFrom = TenorTo

                m_Company.FundingContractRateEdit(oCustom)
            Next

            BindGridEntity(Me.SearchBy, Me.SortBy)
            PanelAllFalse()
            pnlList.Visible = True
            ShowMessage(lblMessage, "Data Rate berhasil di simpan", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgRateFunding_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRateFunding.ItemDataBound
        Dim lblTenor = New Label
        Dim lblTenorFrom = New Label
        Dim lblTenorTo = New Label
        If e.Item.ItemIndex >= 0 Then
            lblTenor = CType(e.Item.FindControl("lblTenor"), Label)
            lblTenorFrom = CType(e.Item.FindControl("lblTenorFrom"), Label)
            lblTenorTo = CType(e.Item.FindControl("lblTenorTo"), Label)

            lblTenor.Text = " " & CInt(lblTenorTo.Text) / 12 & " ( " & lblTenorFrom.Text.Trim & " - " & lblTenorTo.Text.Trim & " ) "
        End If
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

    Sub txtPlafondAmmount_onchange() Handles txtPlafondAmmount.TextChanged
        txtMaxDD.Text = FormatNumber(txtPlafondAmmount.Text, 0)
        txtPlafondAmmount.Text = FormatNumber(txtPlafondAmmount.Text, 0)
    End Sub
    Sub txtPeriodFrom_onchange() Handles txtPeriodFrom.TextChanged
        If txtPeriodeBulan.Text <> "" Then
            txtPeriodTo.Text = CDate(DateAdd(DateInterval.Month, CDbl(txtPeriodeBulan.Text), ConvertDate2(txtPeriodFrom.Text))).ToString("dd/MM/yyyy")
        End If
    End Sub
    Sub txtPeriodeBulan_onChange() Handles txtPeriodeBulan.TextChanged
        If txtPeriodFrom.Text <> "" Then
            txtPeriodTo.Text = CDate(DateAdd(DateInterval.Month, CDbl(txtPeriodeBulan.Text), ConvertDate2(txtPeriodFrom.Text))).ToString("dd/MM/yyyy")
        End If
    End Sub
    Sub txtPeriodeTo_onChage() Handles txtPeriodTo.TextChanged
        Dim h As Long
        If txtPeriodFrom.Text <> "" Then
            h = DateDiff(DateInterval.Month, ConvertDate2(txtPeriodFrom.Text), ConvertDate2(txtPeriodTo.Text))
            If h <= 0 Then
                txtPeriodeBulan.Text = ""
            Else
                txtPeriodeBulan.Text = h.ToString
            End If

        Else
            ShowMessage(lblMessage, "Periode Awal belum diisi!!!", True)
        End If

        If txtPeriodFrom.Text <> "" And txtPeriodTo.Text <> "" Then
            txtFinalMaturityDate.Text = txtPeriodTo.Text
        End If

    End Sub
End Class