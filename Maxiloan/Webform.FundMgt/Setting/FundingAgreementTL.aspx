﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingAgreementTL.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingAgreementTL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingAgreement</title>    
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }
        function selectAllCheckbox(val) {
            $('#dtgFundingAgreementList input:checkbox').prop('checked', $(val).is(':checked'));
        }  					
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
     <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnl1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlAddDetail" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            MEMILIH FASILITAS DALAM BATCH
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Funding Bank</label>
                        <asp:Label ID="lblBankName" runat="server" Width="192px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Funding Cabang Bank</label>
                        <asp:Label ID="lblFundingCoyName" runat="server" Width="192px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Fasilitas Bank</label>
                        <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Fasilitas</label>
                        <asp:Label ID="lblContractName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Batch</label>
                        <asp:Label ID="lblBatchNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Pencairan</label>
                        <asp:Label ID="lblDrawDownAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Sisa Pinjaman</label>
                        <asp:Label ID="lblOutstandingLoan" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Status Dokumen</label>
                            <asp:DropDownList ID="cbostsdok" runat="server">
                                <asp:ListItem Value="A"> All </asp:ListItem>
                                <asp:ListItem Value="O">On Hand</asp:ListItem>
                                <asp:ListItem Value="I">Intransit</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlSearchBy" runat="server">
                <div id="dvsearch" runat="server">
                    <div class="form_box" runat="server">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="cbosearch" runat="server">
                                <asp:ListItem Value="0"> - Pilih - </asp:ListItem>
                                <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                                <asp:ListItem Value="Agreement.agreementno">No. Kontrak</asp:ListItem>
                                <asp:ListItem Value="Agreement.NoPjj">No. Kontrak Lama</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtsearch" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Tanggal Aktivasi</label>
                            <uc1:ucdatece runat="server" id="DateFrom"></uc1:ucdatece>
                            <label class="label_auto">
                                s/d</label>
                            <uc1:ucdatece runat="server" id="DateTo"></uc1:ucdatece>
                        </div>
                    </div>
                </div>
                <div class="form_button" runat="server" id="dvaction">
                    <asp:Button ID="btnSearchAgreement" runat="server" Text="Load" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlAdd" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Akan diTambah</label>
                        <asp:Label ID="lblAmountMustBeAdd" runat="server" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Pokok Terpilih</label>
                        <asp:Label ID="lblPrincipalAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah A/R Terpilih</label>
                        <asp:Label ID="lblARAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah OTR Terpilih</label>
                        <asp:Label ID="lblOTRAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Fasilitas terpilih</label>
                        <asp:Label ID="lblAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Prosentase Security</label>
                        <asp:Label ID="lblSecurityPercentage" runat="server"></asp:Label>% of
                        <asp:Label ID="lblSecurityPercentageOf" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Prosentase Security Sekarang</label>
                        <asp:Label ID="lblCurrentSecurity" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:DropDownList ID="drdBranch" runat="server" onchange="return drdBranchChange()">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Pokok Terpilih</label>
                        <asp:Label ID="lblBranchPrincipalAmt" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah A/R terpilih</label>
                        <asp:Label ID="lblBranchARAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah OTR Terpilih</label>
                        <asp:Label ID="lblBranchOTRAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Fasilitas Terpilih</label>
                        <asp:Label ID="lblBranchAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tambahan Nilai Cabang ini</label>
                        <%--<asp:TextBox ID="txtAdditionalBranchAmount" runat="server"  MaxLength="15"
                    Text="0"></asp:TextBox>
                <asp:RangeValidator ID="rvAddAmount" runat="server" MaximumValue="1" MinimumValue="0"
                    Type="Double" ControlToValidate="txtAdditionalBranchAmount" ErrorMessage="Tambahan Nilai harus <= Jumlah yg harus ditambah"
                    Display="Dynamic"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAdditionalBranchAmount"
                    ErrorMessage="Harap isi dengan Angka" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <uc1:ucnumberformat id="txtAdditionalBranchAmount" runat="server" />
                    </div>
                </div>
                <%--<div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Pencairan Mulai Tgl</label>
                <asp:TextBox runat="server" ID="txtGoLiveDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtGoLiveDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jangka Waktu</label>
                <asp:TextBox ID="txtTenor" runat="server"  ReadOnly="True"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Prosentase Jumlah DP NET (%)</label>
                <asp:TextBox ID="txtDP" runat="server"  MaxLength="3"></asp:TextBox>
                <asp:RangeValidator ID="Rangevalidator2" runat="server" MaximumValue="100" MinimumValue="0"
                    Type="Double" ControlToValidate="txtDP" ErrorMessage="Tidak boleh lebih dari 100"
                    Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtDP"
                    ErrorMessage="Harap isi Prosentase DP Net" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>--%>
                <div class="form_button">
                    <asp:Button ID="btnAgreementSelection" runat="server" Text="Select" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancelView" runat="server" CausesValidation="False" Text="Back"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR FASILITAS TERPILIH
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgFundingAgreementList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO FASILITAS">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="NAME" HeaderText="NAMA CUSTOMER">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkCustName" runat="server" Enabled="True" Text='<%# Container.dataitem("CustName") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="OSAR" HeaderText="SISA A/R" DataFormatString="{0:N2}">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="SISA A/R" Visible="false">
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblsisaAR" runat="server" Text='<%# Container.DataItem("OSAR")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="OSPRINCIPAL" HeaderText="POKOK HUTANG" DataFormatString="{0:N2}">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TotalOTR" HeaderText="OTR" DataFormatString="{0:N2}">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <%--<asp:BoundColumn DataField="NTF" HeaderText="NTF" DataFormatString="{0:N2}">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="Sisa" HeaderText="SISA TENOR">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="StatusBPKB" HeaderText="STATUS BPKB">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="OD" HeaderText="OD">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TahunKendaraan" HeaderText="TAHUN">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AssetDescription" HeaderText="MERK KENDARAAN">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="DESCRIPTION" HeaderText="NAMA ASSET">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" Checked="false" AutoPostBack="true" OnCheckedChanged="DisplayTotalSelectedBychkAll"  runat="server" onclick="javascript:selectAllCheckbox(this);" />
                                            <%--<asp:CheckBox ID="chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="DisplayTotalSelectedBychkAll"></asp:CheckBox>--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="DisplayTotalSelectedByCheckBox"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationID" runat="server" Text='<%# Container.DataItem("ApplicationID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFundingPledgeStatus" runat="server" Text='<%# Container.DataItem("FundingPledgeStatus")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                        <HeaderStyle Width="35%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="txtOSPRINCIPAL" runat="server" Text='<%# Container.DataItem("OSPRINCIPAL")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                                MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" CssClass="validator_general"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <!--Here-->
                <%--<div runat="server" id="divFasilitas" visible="false">--%>
                <div runat="server" id="divFasilitas" >
                <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR FASILITAS TERPILIH
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total A/R
                    </label>
                    <asp:Label ID="lblARAmountL" runat="server"></asp:Label>
                    
                </div>
                <div id="divmustbeadd" runat="server" visible="false">
                <div class="form_right">
                    <label>
                        Jumlah harus ditambah
                    </label>
                    <asp:Label ID="lblTotalAmountMustBeAdd" runat="server"></asp:Label>
                </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total Pokok
                    </label>
                    <asp:Label ID="lblPrincipalAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Kontrak
                    </label>
                    <asp:Label ID="lblAccountL" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total OTR
                    </label>
                    <asp:Label ID="lblOTRAmountL" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        <%--Nilai Rencana Cair--%>
                        Total Plafond
                    </label>
                    <asp:Label ID="lbljumlahplafond" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box" runat="server" visible="false">
                <div class="form_left">
                    <label>
                        Total NTF
                    </label>
                    <asp:Label ID="lblNTFtl" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    &nbsp;
                </div>
            </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSelection" runat="server" Text="Selection" CssClass="small button blue"
                        CausesValidation="False" Visible="false"></asp:Button>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                    </asp:Button><a href="javascript:history.back();"> </a>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
