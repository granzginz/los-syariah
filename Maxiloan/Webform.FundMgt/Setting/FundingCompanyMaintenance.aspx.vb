﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingCompanyMaintenance
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"

    Protected WithEvents UcCompanyAddress As UcCompanyAddress
    Protected WithEvents UcContactPerson As UcContactPerson
    Protected WithEvents UcAPBankPaymentAllocation1 As ucAPBankPaymentAllocation
    Protected WithEvents txtDailyInterest As ucNumberFormat    

#End Region

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private oCompany As New Parameter.FundingCompany
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If
        lbltotrec.Text = recordCount.ToString
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        PnlView.Visible = False
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        PnlView.Visible = False
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        PnlView.Visible = False
    End Sub

#End Region

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property BankId() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        PnlView.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region "FormLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub

        If Me.IsHoBranch = False Then
            ShowMessage(lblMessage, "Harap Login di kantor pusat", True)
            pnlList.Visible = False
            pnlAddEdit.Visible = False
            PnlView.Visible = False
            Exit Sub
        End If

        If Not Me.IsPostBack Then
                InitialDefaultPanel()

                Me.FormID = "FundingCompany"

                'createxmdlist()
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Me.SearchBy = ""
                Me.SortBy = ""

                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""

                If Request.QueryString("CompanyID") <> "" Then
                    Me.CompanyID = Request.QueryString("CompanyID").Trim
                    If CheckFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                        pnlList.Visible = False
                        pnlAddEdit.Visible = False
                        PnlView.Visible = True
                        JustView(Me.CompanyID)
                    End If

                Else
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                    pnlList.Visible = True
                End If

                InitiateUCnumberFormat(txtDailyInterest, False, True)

                txtDailyInterest.RangeValidatorMinimumValue = "360"
                txtDailyInterest.RangeValidatorMaximumValue = "365"
                UcAPBankPaymentAllocation1.FillRequired = False

            End If

    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable


        With oCompany
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCompany = m_Company.ListFundingCompany(oCompany)

        With oCompany
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCompany.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            BtnPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            BtnPrint.Enabled = True
        End If

        dtgFundingCoList.DataSource = dtvEntity
        Try
            dtgFundingCoList.DataBind()
        Catch
            dtgFundingCoList.CurrentPageIndex = 0
            dtgFundingCoList.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

#Region "ADD"
    Private Sub Add()
        Dim customClass As New Parameter.FundingCompany
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .FundingCoyID = txtID.Text.Trim
            .FundingCoyName = txtName.Text.Trim
            .FundingCoBankBranchAccount = txtTFSBankAccount.Text.Trim
            .FundingCoBankAccountName = txtTFSBankAccountName.Text.Trim
            .InterestCalculationOption = txtDailyInterest.Text.Trim
            .BankId = drdBankId.SelectedItem.Value
            .COAFunding = UcAPBankPaymentAllocation1.ComboID
            .COAIntExp = txtCOAIntExpense.Text.Trim
            .COAPrepaid = txtCOAPrep.Text.Trim
        End With

        With oClassAddress
            .Address = UcCompanyAddress.Address
            .RT = UcCompanyAddress.RT.Trim
            .RW = UcCompanyAddress.RW.Trim
            .Kelurahan = UcCompanyAddress.Kelurahan
            .Kecamatan = UcCompanyAddress.Kecamatan
            .City = UcCompanyAddress.City
            .ZipCode = UcCompanyAddress.ZipCode
            .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
            .Phone1 = UcCompanyAddress.Phone1.Trim
            .AreaPhone2 = UcCompanyAddress.AreaPhone2.Trim
            .Phone2 = UcCompanyAddress.Phone2.Trim
            .AreaFax = UcCompanyAddress.AreaFax.Trim
            .Fax = UcCompanyAddress.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle.TrimEnd
            .MobilePhone = UcContactPerson.MobilePhone.Trim
            .Email = UcContactPerson.Email.Trim
        End With

        Try
            m_Company.AddFundingCompany(customClass, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region "EDIT"
    Private Sub edit(ByVal Companyid As String)
        Dim customClass As New Parameter.FundingCompany
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .FundingCoyID = Companyid.Trim
            .FundingCoyName = txtName.Text.Trim
            .FundingCoBankBranchAccount = txtTFSBankAccount.Text.Trim
            .FundingCoBankAccountName = txtTFSBankAccountName.Text.Trim
            .InterestCalculationOption = txtDailyInterest.Text.Trim
            .BankId = drdBankId.SelectedItem.Value
            .COAFunding = UcAPBankPaymentAllocation1.ComboID.Trim
            .COAIntExp = txtCOAIntExpense.Text.Trim
            .COAPrepaid = txtCOAPrep.Text.Trim
        End With

        With oClassAddress
            .Address = UcCompanyAddress.Address
            .RT = UcCompanyAddress.RT.Trim
            .RW = UcCompanyAddress.RW.Trim
            .Kelurahan = UcCompanyAddress.Kelurahan
            .Kecamatan = UcCompanyAddress.Kecamatan
            .City = UcCompanyAddress.City

            .ZipCode = UcCompanyAddress.ZipCode.Trim
            .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
            .Phone1 = UcCompanyAddress.Phone1.Trim
            .AreaPhone2 = UcCompanyAddress.AreaPhone2.Trim
            .Phone2 = UcCompanyAddress.Phone2.Trim
            .AreaFax = UcCompanyAddress.AreaFax.Trim
            .Fax = UcCompanyAddress.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle.Trim
            .MobilePhone = UcContactPerson.MobilePhone.Trim
            .Email = UcContactPerson.Email.Trim
        End With

        Try
            m_Company.EditFundingCompany(customClass, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

#Region "View"

#Region "JustView"
    Private Sub JustView(ByVal Companyid As String)
        Dim oCompany As New Parameter.FundingCompany
        Dim dtCompany As New DataTable

        Try
            Dim strConnection As String = GetConnectionString()

            With oCompany
                .strConnection = GetConnectionString()
                .FundingCoyID = Companyid
                .SortBy = ""
                '.WhereCond = "FundingCoyID='" & Companyid & "'"
            End With

            oCompany = m_Company.ListFundingCompanyByID(oCompany)
            dtCompany = oCompany.ListData

            'bindComboIDBank()
            'Isi semua field View Action

            lblFundingCoyID.Text = Companyid
            lblFundingCoyName.Text = CStr(dtCompany.Rows(0).Item("FundingCoyName")).Trim
            lblBankName.Text = CStr(dtCompany.Rows(0).Item("BankName").trim)


            lblAddress.Text = CStr(dtCompany.Rows(0).Item("Address")).Trim
            lblRT.Text = CStr(dtCompany.Rows(0).Item("RT")).Trim
            lblRW.Text = CStr(dtCompany.Rows(0).Item("RW")).Trim
            lblKelurahan.Text = CStr(dtCompany.Rows(0).Item("Kelurahan")).Trim
            lblKecamatan.Text = CStr(dtCompany.Rows(0).Item("Kecamatan")).Trim
            lblCity.Text = CStr(dtCompany.Rows(0).Item("City")).Trim
            lblZipCode.Text = CStr(dtCompany.Rows(0).Item("ZipCode")).Trim
            lblAreaPhone1.Text = CStr(dtCompany.Rows(0).Item("AreaPhone1")).Trim
            lblPhone1.Text = CStr(dtCompany.Rows(0).Item("Phone1")).Trim
            lblAreaPhone2.Text = CStr(dtCompany.Rows(0).Item("AreaPhone2")).Trim
            lblPhone2.Text = CStr(dtCompany.Rows(0).Item("Phone2")).Trim
            lblAreaFax.Text = CStr(dtCompany.Rows(0).Item("AreaFax")).Trim
            lblFax.Text = CStr(dtCompany.Rows(0).Item("Fax")).Trim

            lblCPName.Text = CStr(dtCompany.Rows(0).Item("ContactName")).Trim
            lblCPTitle.Text = CStr(dtCompany.Rows(0).Item("ContactJobTitle")).Trim
            lblCPMobilePhone.Text = CStr(dtCompany.Rows(0).Item("ContactMobilePhone")).Trim
            lblCPEmail.Text = CStr(dtCompany.Rows(0).Item("ContactEmail")).Trim


            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception

        End Try

    End Sub
#End Region

#Region "EditView"
    Private Sub editView(ByVal Companyid As String)
        Dim oCompany As New Parameter.FundingCompany
        Dim dtCompany As New DataTable

        Try
            Dim strConnection As String = GetConnectionString()

            With oCompany
                .strConnection = GetConnectionString()
                .FundingCoyID = Companyid
                .SortBy = ""
                '.WhereCond = "FundingCoyID='" & Companyid & "'"
            End With

            oCompany = m_Company.ListFundingCompanyByID(oCompany)
            dtCompany = oCompany.ListData

            bindComboIDBank()
            'Isi semua field Edit Action
            txtID.Visible = False
            lblID.Visible = True
            lblID.Text = Companyid
            txtName.Text = CStr(dtCompany.Rows(0).Item("FundingCoyName")).Trim
            drdBankId.SelectedIndex = drdBankId.Items.IndexOf(drdBankId.Items.FindByValue(CStr(dtCompany.Rows(0).Item("BankId"))))

            txtTFSBankAccount.Text = CStr(dtCompany.Rows(0).Item("FundingCoBankBranchAccount")).Trim
            txtTFSBankAccountName.Text = CStr(dtCompany.Rows(0).Item("FundingCoBankAccountName")).Trim
            txtDailyInterest.Text = CStr(dtCompany.Rows(0).Item("InterestCalculationOption")).Trim

            With UcCompanyAddress
                .Address = CStr(dtCompany.Rows(0).Item("Address")).Trim
                .RT = CStr(dtCompany.Rows(0).Item("RT")).Trim
                .RW = CStr(dtCompany.Rows(0).Item("RW")).Trim
                .Kelurahan = CStr(dtCompany.Rows(0).Item("Kelurahan")).Trim
                .Kecamatan = CStr(dtCompany.Rows(0).Item("Kecamatan")).Trim
                .City = CStr(dtCompany.Rows(0).Item("City")).Trim
                .ZipCode = CStr(dtCompany.Rows(0).Item("ZipCode")).Trim
                .AreaPhone1 = CStr(dtCompany.Rows(0).Item("AreaPhone1")).Trim
                .Phone1 = CStr(dtCompany.Rows(0).Item("Phone1")).Trim
                .AreaPhone2 = CStr(dtCompany.Rows(0).Item("AreaPhone2")).Trim
                .Phone2 = CStr(dtCompany.Rows(0).Item("Phone2")).Trim
                .AreaFax = CStr(dtCompany.Rows(0).Item("AreaFax")).Trim
                .Fax = CStr(dtCompany.Rows(0).Item("Fax")).Trim
                .Style = "Setting"
                .BindAddress()
            End With

            With UcContactPerson
                .ContactPerson = CStr(dtCompany.Rows(0).Item("ContactName")).Trim
                .ContactPersonTitle = CStr(dtCompany.Rows(0).Item("ContactJobTitle")).Trim
                .MobilePhone = CStr(dtCompany.Rows(0).Item("ContactMobilePhone")).Trim
                .Email = CStr(dtCompany.Rows(0).Item("ContactEmail")).Trim
                .EnabledContactPerson()
                .BindContacPerson()
            End With

            UcAPBankPaymentAllocation1.ComboID = CStr(dtCompany.Rows(0).Item("COAFunding")).Trim
            'UcAPBankPaymentAllocation1.BindData()
            txtCOAIntExpense.Text = CStr(dtCompany.Rows(0).Item("COAIntExp")).Trim
            txtCOAPrep.Text = CStr(dtCompany.Rows(0).Item("COAPrepaid")).Trim

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#End Region

#Region "MISC"

#Region "ClearAddForm"
    Private Sub ClearAddForm()

        txtID.Text = ""
        txtName.Text = ""

        drdBankId.SelectedIndex = 0
        lblID.Visible = False
        txtID.Visible = True

        txtTFSBankAccount.Text = ""
        txtTFSBankAccountName.Text = ""
        txtDailyInterest.Text = 360

        With UcCompanyAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Setting"
            .BindAddress()
        End With

        With UcContactPerson
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .MobilePhone = ""
            .Email = ""
            .EnabledContactPerson()
            .BindContacPerson()
        End With

    End Sub
#End Region

#Region "BindComboBank"
    Private Sub bindComboIDBank()
        Dim strcon As String = GetConnectionString()
        Dim dt As New DataTable
        Try
            dt = m_Company.GetComboInputBank(strcon)

            Dim i As Integer


            drdBankId.DataValueField = "BankID"
            drdBankId.DataTextField = "BankName"

            drdBankId.DataSource = dt

            drdBankId.DataBind()
            drdBankId.Items.Insert(0, "Select One")
            drdBankId.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblmessage, e.Message, True)
        End Try

    End Sub
#End Region

#End Region

#Region "Handles"

    Private Sub BtnResetAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetAtas.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub BtnSearchAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchAtas.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            bindComboIDBank()
            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True

            Case "EDIT"
                PanelAllFalse()
                edit(Me.CompanyID)
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
        End Select
    End Sub



    Private Sub dtgFundingCoList_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingCoList.ItemCommand
        Select Case e.CommandName
            Case "Contract"
                Me.CompanyID = e.Item.Cells(0).Text
                Me.CompanyName = e.Item.Cells(3).Text
                Me.BankName = e.Item.Cells(2).Text
                Me.BankId = e.Item.Cells(7).Text

                Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankId)
            Case "View"
                'If checkFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
                '    If sessioninvalid() Then
                '        Exit Sub
                '    End If

                '    Me.CompanyID = e.Item.Cells(0).Text
                '    pnlList.Visible = False
                '    pnlAddEdit.Visible = False
                '    PnlView.Visible = True
                '    'Me.ActionAddEdit = "VIEW"
                '    'lblMenuAddEdit.Text = "VIEW"
                '    JustView(Me.CompanyID)
                'End If
            Case "Edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.CompanyID = e.Item.Cells(0).Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    editView(Me.CompanyID)
                End If

            Case "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.CompanyID = e.Item.Cells(0).Text
                    Dim customClass As New Parameter.FundingCompany
                    With customClass
                        .strConnection = GetConnectionString()
                        .FundingCoyID = Me.CompanyID
                    End With

                    Try
                        m_Company.DeleteFundingCompany(customClass)
                        ShowMessage(lblMessage, "Hapus Data Berhasil ", False)

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub

    Private Sub dtgFundingCoList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingCoList.ItemDataBound
        Dim imbDelete As ImageButton
        Dim hyBranch As HyperLink
        If e.Item.ItemIndex >= 0 Then
            Me.CompanyID = e.Item.Cells(0).Text
            hyBranch = CType(e.Item.FindControl("lnkCompanyID"), HyperLink)
            hyBranch.NavigateUrl = "javascript:OpenFundingCompanyView('" & "channeling" & "', '" & Me.CompanyID & "')"


            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
#End Region
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("report/fundingcompanyreport.aspx")
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        PanelAllFalse()
        pnlList.Visible = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        PanelAllFalse()
        pnlList.Visible = True
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

End Class