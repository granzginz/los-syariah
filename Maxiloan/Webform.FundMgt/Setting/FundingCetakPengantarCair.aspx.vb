﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class FundingCetakPengantarCair
    Inherits Maxiloan.Webform.WebBased
#Region " Property "
    Private Property Cmdwhere() As String
        Get
            Return CStr(ViewState("Cmdwhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("Cmdwhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("FilterBy"))
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property FundingContractNo() As String
        Get
            Return CType(ViewState("FundingContractNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingCoyID() As String
        Get
            Return CType(ViewState("FundingCoyID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CType(ViewState("FundingBatchNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property
    Private Property CompanyName() As String
        Get
            Return CType(ViewState("CompanyName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CompanyName") = Value
        End Set
    End Property
    Private Property BankName() As String
        Get
            Return CType(ViewState("BankName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property
    Private Property BankId() As String
        Get
            Return CType(ViewState("BankId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankId") = Value
        End Set
    End Property
    Private Property ContractName() As String
        Get
            Return CType(ViewState("ContractName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property
    Private Property FundingContractId() As String
        Get
            Return CType(ViewState("FundingContractId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractId") = Value
        End Set
    End Property
    Private Property CompanyID() As String
        Get
            Return CType(ViewState("CompanyID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CompanyID") = Value
        End Set
    End Property
    Private Property NoReff() As String
        Get
            Return CType(ViewState("NoReff"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoReff") = Value
        End Set
    End Property
    Private Property Up() As String
        Get
            Return CType(ViewState("Up"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Up") = value
        End Set
    End Property
    Private Property Signer1() As String
        Get
            Return CType(ViewState("Signer1"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Signer1") = value
        End Set
    End Property
    Private Property Signer2() As String
        Get
            Return CType(ViewState("Signer2"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Signer2") = value
        End Set
    End Property
    Private Property Jabatan1() As String
        Get
            Return CType(ViewState("Jabatan1"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Jabatan1") = value
        End Set
    End Property
    Private Property Jabatan2() As String
        Get
            Return CType(ViewState("Jabatan2"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Jabatan2") = value
        End Set
    End Property
    Private Property JabatanUp() As String
        Get
            Return CType(ViewState("JabatanUp"), String)
        End Get
        Set(ByVal value As String)
            ViewState("JabatanUp") = value
        End Set
    End Property
    Private Property Perihal() As String
        Get
            Return CType(ViewState("Perihal"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Perihal") = value
        End Set
    End Property
#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private NamaFile As String = "SuratPengantarCair"
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Request.QueryString("CompanyID") <> "" Then Me.FundingCoyID = Request.QueryString("CompanyID")
        If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
        If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
        If Request.QueryString("BankID") <> "" Then Me.BankId = Request.QueryString("BankID")
        If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
        If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo")
        If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
        If Request.QueryString("NoReff") <> "" Then Me.NoReff = Request.QueryString("NoReff")
        If Request.QueryString("Jabatan1") <> "" Then Me.Jabatan1 = Request.QueryString("Jabatan1")
        If Request.QueryString("Jabatan2") <> "" Then Me.Jabatan2 = Request.QueryString("Jabatan2")
        If Request.QueryString("Signer1") <> "" Then Me.Signer1 = Request.QueryString("Signer1")
        If Request.QueryString("Signer2") <> "" Then Me.Signer2 = Request.QueryString("Signer2")
        If Request.QueryString("Up") <> "" Then Me.Up = Request.QueryString("Up")
        If Request.QueryString("JabatanUp") <> "" Then Me.JabatanUp = Request.QueryString("JabatanUp")
        If Request.QueryString("Perihal") <> "" Then Me.JabatanUp = Request.QueryString("Perihal")

        If Request.QueryString("strFileLocation") <> "" Then
            Dim strFileLocation As String

            strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
            Response.Write("<script language = javascript>" & vbCrLf _
                           & "var x = screen.width;" & vbCrLf _
                           & "var y = screen.height;" & vbCrLf _
            & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
            & "</script>")

        End If
    End Sub
    Private Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("FundingCetakPengantarCair")

        If Not cookie Is Nothing Then
            cookie.Values("CompanyID") = Me.FundingCoyID
            cookie.Values("CompanyName") = Me.CompanyName
            cookie.Values("BankName") = Me.BankName
            cookie.Values("BankID") = Me.BankId
            cookie.Values("ContractName") = Me.ContractName
            cookie.Values("FundingCoyID") = Me.FundingCoyID
            cookie.Values("FundingContractNO") = Me.FundingContractNo
            cookie.Values("FundingBatchNo") = Me.FundingBatchNo
            cookie.Values("NoReff") = TxtNoReff.Text.Trim
            cookie.Values("Up") = txtUp.Text.Trim
            cookie.Values("Signer1") = txtSigner1.Text.Trim
            cookie.Values("Signer2") = txtSigner2.Text.Trim
            cookie.Values("Jabatan1") = txtJabatan1.Text.Trim
            cookie.Values("Jabatan2") = txtJabatan2.Text.Trim
            cookie.Values("JabatanUp") = txtJabatanUp.Text.Trim
            cookie.Values("Perihal") = txtPerihal.Text.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("FundingCetakPengantarCair")
            cookieNew.Values.Add("CompanyID", Me.FundingCoyID)
            cookieNew.Values.Add("CompanyName", Me.CompanyName)
            cookieNew.Values.Add("BankName", Me.BankName)
            cookieNew.Values.Add("BankID", Me.BankId)
            cookieNew.Values.Add("ContractName", Me.ContractName)
            cookieNew.Values.Add("FundingCoyID", Me.FundingCoyID)
            cookieNew.Values.Add("FundingContractNO", Me.FundingContractNo)
            cookieNew.Values.Add("FundingBatchNo", Me.FundingBatchNo)
            cookieNew.Values.Add("NoReff", TxtNoReff.Text.Trim)
            cookieNew.Values.Add("Up", txtUp.Text.Trim)
            cookieNew.Values.Add("Signer1", txtSigner1.Text.Trim)
            cookieNew.Values.Add("Signer2", txtSigner2.Text.Trim)
            cookieNew.Values.Add("Jabatan1", txtJabatan1.Text.Trim)
            cookieNew.Values.Add("Jabatan2", txtJabatan2.Text.Trim)
            cookieNew.Values.Add("JabatanUp", txtJabatanUp.Text.Trim)
            cookieNew.Values.Add("Perihal", txtPerihal.Text.Trim)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("FundingCetakPengantarCairViewer.aspx?CompanyID=" & Me.FundingCoyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankId & "&FundingContractNo=" & Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & "&NoReff=" & Me.NoReff & "&Up=" & Me.Up & "&Signer1=" & Me.Signer1 & "&Signer2=" & Me.Signer2 & "&Jabatan1=" & Me.Jabatan1 & "&Jabatan2=" & Me.Jabatan2 & "&JabatanUp=" & Me.JabatanUp)
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("FundingContractBatch.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & Me.NamaFile & "&CompanyID=" & Me.FundingCoyID & "&CompanyName=" & _
                            Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankId & "&FundingContractId=" & _
                            Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub
End Class