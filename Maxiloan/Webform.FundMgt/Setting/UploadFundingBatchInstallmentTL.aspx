﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadFundingBatchInstallmentTL.aspx.vb" Inherits="Maxiloan.Webform.FundMgt.UploadFundingBatchInstallmentTL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingAgreement</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }					
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnl1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR BATCH
                    </h4>
                </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Fasilitas</label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
        </div>
            <div class="form_button">
                <asp:FileUpload ID="FileUpload" Width="450px" runat="server" visible="true"/>
                <asp:Button ID="btnImport" runat="server" Text="Import Data" CssClass="small button blue" Visible="true">  </asp:Button>
            </div>
            <div id="divupload" runat="server">
        <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR UPLOAD KONTRAK
                    </h4>
                </div>
        </div>       
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgViewUploadFromExcel" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                             BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                    <asp:TemplateColumn SortExpression="" HeaderText="Seq NO" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblSeqNo" runat="server" Text='<%# Container.DataItem("InsSeqNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="" HeaderText="Due Date"  >
                                        <ItemTemplate>
                                            <asp:Label ID="lblDueDate" runat="server" Text='<%# Container.DataItem("DueDate")%>' DataFormatString="{0:dd/MM/yyyy}">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="" HeaderText="Principal Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# Container.DataItem("PrincipalAmount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="" HeaderText="Interest Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblInterestAmount" runat="server" Text='<%# Container.DataItem("InterestAmount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="" HeaderText="Principal Paid Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrincipalPaidAmount" runat="server" Text='<%# Container.DataItem("PrincipalPaidAmount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="" HeaderText="Interest Paid Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblInterestPaidAmount" runat="server" Text='<%# Container.DataItem("InterestPaidAmount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="" HeaderText="OS Principal Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblOSPrincipalAmount" runat="server" Text='<%# Container.DataItem("OSPrincipalAmount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="" HeaderText="OS Interest Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblOSInterestAmount" runat="server" Text='<%# Container.DataItem("OSInterestAmount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="" HeaderText="Interest Rate" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblInterestRate" runat="server" Text='<%# Container.DataItem("InterestRate")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                </div>
            </div>
        </div>
           <div class="form_button">
                    <asp:Button ID="btnExecuteFromUpload" runat="server" Text="Execute" CssClass="small button blue" Visible="true">  </asp:Button>
                    <asp:Button ID="btnCancelExecuteFromUpload" runat="server" Text="Cancel" CssClass="small button gray" Visible="true">  </asp:Button>
            </div>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
