﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports ClosedXML.Excel
Imports System.IO
Imports Maxiloan.Framework.SQLEngine
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.OpenXml4Net.OPC
Imports NPOI.XSSF.UserModel

#End Region



Public Class UploadFundingBatchInstallmentTL
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property
    Public Property PlafondAmount As Double
        Get
            Return CDbl(ViewState("PlafondAmount"))
        End Get
        Set(value As Double)
            ViewState("PlafondAmount") = value
        End Set
    End Property

#End Region

#Region " Private Const "
    Private dt As DataTable
    Private cGeneral As New GeneralPagingController
    Private oGeneral As New Parameter.GeneralPaging

    Private m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 25
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If SessionInvalid() Then Exit Sub
        lblMessage.Visible = False

        Dim scriptManager__3 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__3.RegisterPostBackControl(Me.btnImport)
        If Not Me.IsPostBack Then
            Me.FormID = "FUNDINGBATCH"

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")

            lblBankName.Text = Me.BankName
            lblFundingCoyName.Text = Me.CompanyName
            lblFundingContractNo.Text = Me.FundingContractNo
            lblContractName.Text = Me.ContractName
            divupload.Visible = False

        End If
    End Sub
    Function CheckFileType(ByVal fileName As String) As Boolean
        Dim ext As String = Path.GetExtension(fileName)

        Select Case ext.ToLower()
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select

    End Function
    Private Function pathFile(ByVal Group As String, ByVal Data As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\Upload\" & Group & "\" & Data & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Sub UploadFunding(FileUpload1 As FileUpload, FileName As String)

        If (FileUpload1.HasFile) Then
            If (CheckFileType(FileUpload1.FileName)) Then

                Dim filePath As String = pathFile("Funding", "DataUploadFundingAgreement") & FileName & ".xls"
                FileUpload1.PostedFile.SaveAs(filePath)
            Else
                ShowMessage(lblMessage, "Upload File salah, harap perhatikan tipe file!", True)
                Exit Sub
            End If
        End If

    End Sub

    Public Shared Function ExcelToDataTable(filePath As String) As DataTable
        Dim dtExcel As New DataTable()
        Dim hssfworkbook As HSSFWorkbook
        Using file As New FileStream(filePath, FileMode.Open, FileAccess.Read)
            hssfworkbook = New HSSFWorkbook(file)
        End Using
        Dim sheet As ISheet = hssfworkbook.GetSheetAt(0)
        Dim rows As System.Collections.IEnumerator = sheet.GetRowEnumerator()

        Dim headerRow As IRow = sheet.GetRow(0)
        Dim cellCount As Integer = headerRow.LastCellNum

        For j As Integer = 0 To cellCount - 1
            Dim cell As ICell = headerRow.GetCell(j)
            dtExcel.Columns.Add(cell.ToString())
        Next

        For i As Integer = (sheet.FirstRowNum + 1) To sheet.LastRowNum
            Dim row As IRow = sheet.GetRow(i)
            Dim dataRow As DataRow = dtExcel.NewRow()
            If row Is Nothing Then
                Exit For
            End If
            For j As Integer = row.FirstCellNum To cellCount - 1
                If row.GetCell(j) IsNot Nothing Then
                    dataRow(j) = row.GetCell(j).ToString()
                End If
            Next

            dtExcel.Rows.Add(dataRow)
        Next
        Return dtExcel

    End Function

    Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        Dim dtUploadFromExcel As DataTable
        Dim strFileName As String

        If (FileUpload.HasFile) Then

            If Not IsDBNull(FileUpload.PostedFile) And
                FileUpload.PostedFile.ContentLength > 0 Then
                Try

                    UploadFunding(FileUpload, "DataUploadFundingAgreement")
                    strFileName = pathFile("Funding", "DataUploadFundingAgreement") & "DataUploadFundingAgreement.xls"

                    If FileUpload.HasFile Then
                        If File.Exists(strFileName) Then
                            dtUploadFromExcel = ExcelToDataTable(strFileName)

                            DtgViewUploadFromExcel.DataSource = dtUploadFromExcel
                            DtgViewUploadFromExcel.DataBind()


                            ShowMessage(lblMessage, "Upload Data Berhasil ", False)
                            divupload.Visible = True
                            File.Delete(strFileName)
                        End If
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)

                Finally

                End Try
            Else
                ShowMessage(lblMessage, "Upload Data Gagal ", True)
            End If
        Else
            ShowMessage(lblMessage, "Upload Data Gagal ", True)
        End If
    End Sub

    'Upload FundingBatchInstallment
    Protected Sub btnExecuteFromUpload_Click(sender As Object, e As EventArgs) Handles btnExecuteFromUpload.Click
        Dim dtUploadFromExcelx As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim lblSeqNo As New Label
        Dim lblDueDate As New Label
        Dim lblPrincipalAmount As New Label
        Dim lblInterestAmount As New Label
        Dim lblPrincipalPaidAmount As New Label
        Dim lblInterestPaidAmount As New Label
        Dim lblOSPrincipalAmount As New Label
        Dim lblOSInterestAmount As New Label
        Dim lblInterestRate As New Label
        Dim customClass As New Parameter.FundingContractBatch

        With dtUploadFromExcelx
            .Columns.Add(New DataColumn("InsSeqNo", GetType(String)))
            .Columns.Add(New DataColumn("DueDate", GetType(DateTime)))
            .Columns.Add(New DataColumn("PrincipalAmount", GetType(Double)))
            .Columns.Add(New DataColumn("InterestAmount", GetType(Double)))
            .Columns.Add(New DataColumn("PrincipalPaidAmount", GetType(Double)))
            .Columns.Add(New DataColumn("InterestPaidAmount", GetType(Double)))
            .Columns.Add(New DataColumn("OSPrincipalAmount", GetType(Double)))
            .Columns.Add(New DataColumn("OSInterestAmount", GetType(Double)))
            .Columns.Add(New DataColumn("InterestRate", GetType(Decimal)))
        End With

        For intloop = 0 To DtgViewUploadFromExcel.Items.Count - 1
            lblSeqNo = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblSeqNo"), Label)
            lblDueDate = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblDueDate"), Label)
            lblPrincipalAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblPrincipalAmount"), Label)
            lblInterestAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblInterestAmount"), Label)
            lblPrincipalPaidAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblPrincipalPaidAmount"), Label)
            lblInterestPaidAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblInterestPaidAmount"), Label)
            lblOSPrincipalAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblOSPrincipalAmount"), Label)
            lblOSInterestAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblOSInterestAmount"), Label)
            lblInterestRate = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblInterestRate"), Label)

            oRow = dtUploadFromExcelx.NewRow
            oRow("InsSeqNo") = lblSeqNo.Text.Trim
            oRow("DueDate") = ConvertDate2(lblDueDate.Text.Trim)
            oRow("PrincipalAmount") = If((String.IsNullOrEmpty(lblPrincipalAmount.Text.Trim)), 0, lblPrincipalAmount.Text.Trim)
            oRow("InterestAmount") = If((String.IsNullOrEmpty(lblInterestAmount.Text.Trim)), 0, lblInterestAmount.Text.Trim)
            oRow("PrincipalPaidAmount") = If((String.IsNullOrEmpty(lblPrincipalPaidAmount.Text.Trim)), 0, lblPrincipalPaidAmount.Text.Trim)
            oRow("InterestPaidAmount") = If((String.IsNullOrEmpty(lblInterestPaidAmount.Text.Trim)), 0, lblInterestPaidAmount.Text.Trim)
            oRow("OSPrincipalAmount") = If((String.IsNullOrEmpty(lblOSPrincipalAmount.Text.Trim)), 0, lblOSPrincipalAmount.Text.Trim)
            oRow("OSInterestAmount") = If((String.IsNullOrEmpty(lblOSInterestAmount.Text.Trim)), 0, lblOSInterestAmount.Text.Trim)
            oRow("InterestRate") = lblInterestRate.Text.Trim
            dtUploadFromExcelx.Rows.Add(oRow)
        Next

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID.Trim
                .FundingContractNo = Me.FundingContractNo.Trim
                .FundingBatchNo = Me.FundingBatchNo.Trim
                .Listdata = dtUploadFromExcelx
            End With

            m_Company.FundingBatchInstallmentSelectFromUpload(customClass)

            Response.Redirect("FundingAgreementSelected.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" &
                              Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" &
                              Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName &
                              "&Plafond=" & Me.PlafondAmount)


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

End Class