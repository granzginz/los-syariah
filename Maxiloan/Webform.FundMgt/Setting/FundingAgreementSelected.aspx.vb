﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports ClosedXML.Excel
Imports System.IO
Imports Maxiloan.Framework.SQLEngine
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.OpenXml4Net.OPC
Imports NPOI.XSSF.UserModel

#End Region

Public Class FundingAgreementSelected
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"

#End Region
#Region " Property "
    Private Property AmountMustBeAdd() As Double
        Get
            Return CDbl(ViewState("AmountMustBeAdd"))
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountMustBeAdd") = Value
        End Set
    End Property
    Public Property FacilityKind() As String
        Get
            Return CStr(ViewState("FacilityKind"))
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityKind") = Value
        End Set
    End Property
    Public Property TenorMax() As Short
        Get
            Return CStr(ViewState("TenorMax"))
        End Get
        Set(ByVal Value As Short)
            ViewState("TenorMax") = Value
        End Set
    End Property
    Public Property InterestRate() As Double
        Get
            Return CDbl(ViewState("InterestRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InterestRate") = Value
        End Set
    End Property

    Public Property TotalAR As Double
        Get
            Return CDbl(ViewState("TotalAR"))
        End Get
        Set(value As Double)
            ViewState("TotalAR") = value
        End Set
    End Property
    Public Property TotalPokok As Double
        Get
            Return CDbl(ViewState("TotalPokok"))
        End Get
        Set(value As Double)
            ViewState("TotalPokok") = value
        End Set
    End Property
    Public Property TotalOTR As Double
        Get
            Return CDbl(ViewState("TotalOTR"))
        End Get
        Set(value As Double)
            ViewState("TotalOTR") = value
        End Set
    End Property
    Public Property TotalNTF As Double
        Get
            Return CDbl(ViewState("TotalNTF"))
        End Get
        Set(value As Double)
            ViewState("TotalNTF") = value
        End Set
    End Property
    Public Property TotalTobeAdd As Double
        Get
            Return CDbl(ViewState("TotalTobeAdd"))
        End Get
        Set(value As Double)
            ViewState("TotalTobeAdd") = value
        End Set
    End Property
    Public Property TotalAccounts As Double
        Get
            Return CDbl(ViewState("TotalAccounts"))
        End Get
        Set(value As Double)
            ViewState("TotalAccounts") = value
        End Set
    End Property
    Public Property TotalPokokPerPage As Double
        Get
            Return CDbl(ViewState("TotalPokokPerPage"))
        End Get
        Set(value As Double)
            ViewState("TotalPokokPerPage") = value
        End Set
    End Property

    Public Property PlafondAmount As Double
        Get
            Return CDbl(ViewState("PlafondAmount"))
        End Get
        Set(value As Double)
            ViewState("PlafondAmount") = value
        End Set
    End Property
    Public Property SelectedStatus As String
        Get
            Return CStr(ViewState("SelectedStatus"))
        End Get
        Set(value As String)
            ViewState("SelectedStatus") = value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

    Private Property Tenor() As String
        Get
            Return CStr(ViewState("Tenor"))
        End Get
        Set(ByVal Value As String)
            ViewState("Tenor") = Value
        End Set
    End Property
    Public Property lblDrawDownAmount As Decimal
        Get
            Return CDec(ViewState("lblDrawDownAmount"))
        End Get
        Set(value As Decimal)
            ViewState("lblDrawDownAmount") = value
        End Set
    End Property
    Public Property Amount As Double
        Get
            Return CDbl(ViewState("Amount"))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Public Property StatusDrawn() As String
        Get
            Return CStr(ViewState("StatusDrawn"))
        End Get
        Set(ByVal Value As String)
            ViewState("StatusDrawn") = Value
        End Set
    End Property
    Public Property PaymentScheme() As String
        Get
            Return CStr(ViewState("PaymentScheme"))
        End Get
        Set(value As String)
            ViewState("PaymentScheme") = value
        End Set
    End Property
    Public Property BatchDate() As Date
        Get
            Return CDate(ViewState("BatchDate"))
        End Get
        Set(value As Date)
            ViewState("BatchDate") = value
        End Set
    End Property
    Public Property DueDate_() As Date
        Get
            Return CDate(ViewState("DueDate"))
        End Get
        Set(value As Date)
            ViewState("DueDate") = value
        End Set
    End Property
    Public Property FinalMaturityDate() As Date
        Get
            Return CDate(ViewState("FinalMaturityDate"))
        End Get
        Set(value As Date)
            ViewState("FinalMaturityDate") = value
        End Set
    End Property

    Public Property InsScheme() As String
        Get
            Return CStr(ViewState("InsScheme"))
        End Get
        Set(value As String)
            ViewState("InsScheme") = value
        End Set
    End Property
    Public Property InsPeriod() As String
        Get
            Return CStr(ViewState("InsPeriod"))
        End Get
        Set(value As String)
            ViewState("InsPeriod") = value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return CStr(ViewState("InterestType"))
        End Get
        Set(value As String)
            ViewState("InterestType") = value
        End Set
    End Property
    Public Property Currency() As String
        Get
            Return CStr(ViewState("Currency"))
        End Get
        Set(value As String)
            ViewState("Currency") = value
        End Set
    End Property
    Public Property BPKBLocation() As String
        Get
            Return CStr(ViewState("BPKBLocation"))
        End Get
        Set(value As String)
            ViewState("BPKBLocation") = value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return CStr(ViewState("BankAccountID"))
        End Get
        Set(value As String)
            ViewState("BankAccountID") = value
        End Set
    End Property
    Public Property FirstInstallment() As String
        Get
            Return CStr(ViewState("FirstInstallment"))
        End Get
        Set(value As String)
            ViewState("FirstInstallment") = value
        End Set
    End Property
    Public Property CaraPembayaran() As String
        Get
            Return CStr(ViewState("CaraPembayaran"))
        End Get
        Set(value As String)
            ViewState("CaraPembayaran") = value
        End Set
    End Property
    Public Property HitungBUnga() As String
        Get
            Return CStr(ViewState("HitungBUnga"))
        End Get
        Set(value As String)
            ViewState("HitungBUnga") = value
        End Set
    End Property
    Public Property ProvisionAmount() As Double
        Get
            Return CStr(ViewState("ProvisionAmount"))
        End Get
        Set(value As Double)
            ViewState("ProvisionAmount") = value
        End Set
    End Property
    Public Property AdminAmount() As Double
        Get
            Return CStr(ViewState("AdminAmount"))
        End Get
        Set(value As Double)
            ViewState("AdminAmount") = value
        End Set
    End Property
    Public Property ExchangeRate() As Double
        Get
            Return CStr(ViewState("ExchangeRate"))
        End Get
        Set(value As Double)
            ViewState("ExchangeRate") = value
        End Set
    End Property
    Public Property IsFundingNTF() As Boolean
        Get
            Return CStr(ViewState("IsFundingNTF"))
        End Get
        Set(value As Boolean)
            ViewState("IsFundingNTF") = value
        End Set
    End Property
    Public Property IsExecute() As Int32
        Get
            Return CStr(ViewState("IsExecute"))
        End Get
        Set(value As Int32)
            ViewState("IsExecute") = value
        End Set
    End Property
#End Region

#Region " Private Const "
    Private dt As DataTable
    Private cGeneral As New GeneralPagingController
    Private oGeneral As New Parameter.GeneralPaging

    Private m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 25
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        'InitialDefaultPanel()
        lblMessage.Visible = False
        Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__1.RegisterPostBackControl(Me.BtnExportToExel)

        Dim scriptManager__3 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__3.RegisterPostBackControl(Me.btnImport)
        If Not Me.IsPostBack Then
            Me.FormID = "FUNDINGBATCH"

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
            If Request.QueryString("Tenor") <> "" Then Me.Tenor = Request.QueryString("Tenor")
            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")
            If Request.QueryString("Plafond") <> "" Then lbljumlahplafond.Text = FormatNumber(Request.QueryString("Plafond"), 0).Trim

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""

                btnSelection.Text = "Selection"
                SelectedStatus = "Display"

                DisplayHeaderInfo()
                DisplaySelected()
                DisplayTotalSelected()
            End If

            divupload.Visible = False

        End If
    End Sub

#Region " Navigation "
    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then
    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = CType(totalPages, String)
    '        rgvGo.MaximumValue = CType(totalPages, String)

    '    End If
    '    lbltotrec.Text = recordCount.ToString
    '    pnlList.Visible = True
    '    PnlSearchBy.Visible = True

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If
    'End Sub

    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
    '    pnlList.Visible = True
    '    PnlSearchBy.Visible = True
    'End Sub

    'Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

    '    If txtGoPage.Text = "" Then
    '        txtGoPage.Text = "0"
    '    Else
    '        If IsNumeric(txtGoPage.Text) Then
    '            If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
    '                'SavePage()
    '                currentPage = CType(txtGoPage.Text, Int32)
    '                DisplayAgreementSelection(Me.SearchBy, Me.SortBy)
    '            End If
    '        End If
    '    End If
    '    pnlList.Visible = True
    '    PnlSearchBy.Visible = True
    'End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DisplayAgreementSelection(Me.SearchBy, Me.SortBy)        
    End Sub
#End Region



#Region " PanelAllFalse "
    Private Sub PanelAllFalse()           
        lblMessage.Visible = False        
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()        
    End Sub
#End Region


    Sub DisplayAgreementSelection(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New FundingCompanyController
        Dim oContract As New Parameter.FundingContractBatch


        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = SortBy
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .Filter = "1"
        End With

        oContract = cContract.FundingAgreementSelection(oContract)

        'With oContract
        '    lbltotrec.Text = CType(.TotalRecord, String)
        '    lblTotPage.Text = CType(.PageSize, String)
        '    lblPage.Text = CType(.CurrentPage, String)
        '    recordCount = .TotalRecord
        'End With

        dtsEntity = oContract.ListData

        If Not IsNothing(dtsEntity) Then
            dtvEntity = dtsEntity.DefaultView
            dtvEntity.Sort = Me.SortBy
            dtgSelected.DataSource = dtvEntity
            dtgSelected.DataBind()
        Else
            dtgSelected.CurrentPageIndex = 0
            dtgSelected.DataBind()
            dtgSelected.DataSource = Nothing
        End If

        'PagingFooter()
    End Sub

#Region "UpdateAgreementSelected"
    Private Sub UpdateAgreementSelected()
        Dim customClass As New Parameter.FundingContractBatch
        Dim SecurityCoverageType As String
        Dim BatchDate As DateTime

        Try
            With oGeneral
                .strConnection = GetConnectionString()
                '.WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetSecurityType"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData
            SecurityCoverageType = CStr(dt.Rows(0).Item("SecurityCoverageType")).Trim

            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "' and FundingBatchNo='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetBatchDate"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData
            BatchDate = dt.Rows(0).Item("BatchDate")

            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                '.PlafondAmount = CDec(txtAdditionalBranchAmount.Text)
                .PlafondAmount = lblDrawDownAmount
                .BranchId = Me.BranchID
                .SecurityType = SecurityCoverageType
                .BatchDate = BatchDate
                '.GoLiveDate = ConvertDate2(txtGoLiveDate.Text)
                '.NetDP = CDec(txtDP.Text)
                '.Tenor = CInt(Me.Tenor)
            End With

            m_Company.UpdateFundingAgreementSelected(customClass)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

#Region "MISC"


    Private Sub DisplayHeaderInfo()
        Dim DrawDownAmount, OutstandingLoan As Decimal
        'Dim PrincipalAmount, ARAmount, OTRAmount, NTFAmount As Decimal
        Dim TotalAccount As Integer
        Dim SecurityPercentage As Decimal
        Dim SecurityType As String
        Dim cContract As New FundingCompanyController
        Dim oContract As New Parameter.FundingContractBatch

        Try
            bindComboIDbranch()
            'lblBankName.Text = Me.BankName
            'lblFundingCoyName.Text = Me.CompanyName
            'lblFundingContractNo.Text = Me.FundingContractNo
            'lblContractName.Text = Me.ContractName
            'lblBatchNo.Text = Me.FundingBatchNo


            'With oContract
            '    .strConnection = GetConnectionString()
            '    .FundingCoyId = Me.CompanyID
            '    .FundingContractNo = Me.FundingContractNo
            '    .FundingBatchNo = Me.FundingBatchNo
            'End With

            'oContract = cContract.FundingTotalSelected(oContract)
            'dt = oContract.ListData


            'ARAmount = IIf(IsDBNull(dt.Rows(0).Item("ARAmount")), 0, dt.Rows(0).Item("ARAmount"))
            'PrincipalAmount = IIf(IsDBNull(dt.Rows(0).Item("PrincipalAmount")), 0, dt.Rows(0).Item("PrincipalAmount"))
            'OTRAmount = IIf(IsDBNull(dt.Rows(0).Item("OTRAmount")), 0, dt.Rows(0).Item("OTRAmount"))
            'NTFAmount = IIf(IsDBNull(dt.Rows(0).Item("NTF")), 0, dt.Rows(0).Item("NTF"))
            'TotalAccount = IIf(IsDBNull(dt.Rows(0).Item("TotalAccount")), 0, dt.Rows(0).Item("TotalAccount"))

            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spFundingGetSecurityType"
            End With

            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            SecurityPercentage = dt.Rows(0).Item("SecurityCoveragePercentage")
            SecurityType = CStr(dt.Rows(0).Item("SecurityCoverageType")).Trim


            With oGeneral
                .strConnection = GetConnectionString()
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spFundingGetDrawDown"
            End With

            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            DrawDownAmount = dt.Rows(0).Item("PrincipalAmtToFunCoy")
            OutstandingLoan = dt.Rows(0).Item("OSAmtToFunCoy")

            Me.PlafondAmount = (DrawDownAmount * SecurityPercentage) / 100
            'Me.AmountMustBeAdd = Me.PlafondAmount - PrincipalAmount             

            'Select Case SecurityType
            '    Case "0"
            '        lblSecurityPercentageOf.Text = "Principal Amount"
            '        AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - PrincipalAmount
            '        Me.AmountMustBeAdd = AmountMustBeAdd
            '    Case "1"
            '        lblSecurityPercentageOf.Text = "NTF"
            '        AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - NTFAmount
            '        Me.AmountMustBeAdd = AmountMustBeAdd
            '    Case "2"
            '        lblSecurityPercentageOf.Text = "OTR Amount"
            '        AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - OTRAmount
            '        Me.AmountMustBeAdd = AmountMustBeAdd
            'End Select


            'Select Case SecurityType
            '    Case "0"
            '        lblSecurityPercentageOf.Text = "Outstanding Principal"

            '    Case "1"
            '        lblSecurityPercentageOf.Text = "NTF"

            '    Case "2"
            '        lblSecurityPercentageOf.Text = "OTR"

            'End Select
            '' nilai principal bisa berarti OS principal, NTF atau OTR tergantung pada SP spFundingTotalAgreement
            'AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - PrincipalAmount
            'Me.AmountMustBeAdd = AmountMustBeAdd


            'lblOutstandingLoan.Text = FormatNumber(OutstandingLoan, 0).Trim
            'lblAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0).Trim
            lblTotalAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0).Trim
            'lblSecurityPercentage.Text = FormatNumber(SecurityPercentage, 0).Trim
            'lblPrincipalAmount.Text = FormatNumber(PrincipalAmount, 0).Trim
            'lblARAmount.Text = FormatNumber(ARAmount, 0).Trim
            'lblOTRAmount.Text = FormatNumber(OTRAmount, 0).Trim
            'lblAccount.Text = FormatNumber(TotalAccount, 0).Trim
            'lblDrawDownAmount.Text = FormatNumber(DrawDownAmount, 0).Trim

            'With oGeneral
            '    .strConnection = GetConnectionString()
            '    .WhereCond1 = "faAgreement.FundingCoyId='" & Me.CompanyID & "' and faAgreement.BankId='" & Me.BankID & "' and faAgreement.FundingContractNo='" & Me.FundingContractNo & "'"
            '    .WhereCond2 = "faAgreement.FundingCoyId='" & Me.CompanyID & "' and faAgreement.BankId='" & Me.BankID & "' and faAgreement.FundingContractNo='" & Me.FundingContractNo & "' and faAgreement.FundingBatchNo='" & Me.FundingBatchNo & "'"
            '    .WhereCond3 = "faAgreement.FundingCoyId='" & Me.CompanyID & "' and faAgreement.BankId='" & Me.BankID & "' and faAgreement.FundingContractNo='" & Me.FundingContractNo & "' and faAgreement.FundingBatchNo='" & Me.FundingBatchNo & "'"
            '    .CurrentPage = currentPage
            '    .PageSize = pageSize
            '    .SortBy = ""
            '    .SpName = "spFundingTotalAgreementPerBranch"
            'End With

            'oGeneral = cGeneral.GetGeneralSP(oGeneral)
            'dt = oGeneral.ListData

            'lblBranchPrincipalAmt.Text = 0
            'lblBranchOTRAmount.Text = 0
            'lblBranchARAmount.Text = 0
            'lblBranchAccount.Text = 0

            'If dt.Rows.Count > 0 Then
            '    If CStr(dt.Rows(0).Item("BranchID")).Trim = drdBranch.SelectedItem.Value Then
            '        lblBranchPrincipalAmt.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmount"), 0)
            '        lblBranchOTRAmount.Text = FormatNumber(dt.Rows(0).Item("OTRAmount"), 0)
            '        lblBranchARAmount.Text = FormatNumber(dt.Rows(0).Item("ARAmount"), 0)
            '        lblBranchAccount.Text = CStr(dt.Rows(0).Item("TotalAccount"))
            '    End If
            'End If

            CheckISExecutedAndJFINC()
            'Response.Write(GenerateBranchChangeJavaScript(dt))

            'If AmountMustBeAdd > 0 Then
            '    rvAddAmount.MaximumValue = AmountMustBeAdd
            'Else
            '    rvAddAmount.MaximumValue = 1
            'End If

            'If Me.AmountMustBeAdd < 1 Then
            '    txtAdditionalBranchAmount.Enabled = False
            '    btnAgreementSelection.Visible = False
            'End If

            'If drdBranch.Items.Count = 0 Then
            '    txtAdditionalBranchAmount.Enabled = False
            '    btnAgreementSelection.Visible = False

            '    ShowMessage(lblMessage, "Harap isi Plafon Cabang Terlebih dahulu ", True)
            'End If
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
            'btnAgreementSelection.Visible = False

        End Try
    End Sub


#Region "BindComboBranch"
    Private Sub bindComboIDbranch()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBranchIn"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            Dim i As Integer


            'drdBranch.DataValueField = "BranchID"
            'drdBranch.DataTextField = "BranchFullName"

            'drdBranch.DataSource = dtvEntity

            'drdBranch.DataBind()
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
#End Region

#Region "GenerateBranchChangeJavaScript"
    Private Function GenerateBranchChangeJavaScript(ByVal dt As DataTable) As String
        Dim strScript As String
        Dim DataRow As DataRow
        Dim i As Integer


        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript += "function drdBranchChange(){" & vbCrLf
        strScript += "document.all['lblBranchPrincipalAmt'].innerHTML = '0';" & vbCrLf
        strScript += "document.all['lblBranchOTRAmount'].innerHTML = '0';" & vbCrLf
        strScript += "document.all['lblBranchARAmount'].innerHTML = '0';" & vbCrLf
        strScript += "document.all['lblBranchAccount'].innerHTML = '0';" & vbCrLf
        i = 0
        For Each DataRow In dt.Rows
            If i = 0 Then
                strScript += "if (document.Form1.drdBranch.value =='" & dt.Rows(i).Item("BranchID") & "') {" & vbCrLf
            Else
                strScript += "}else if (document.Form1.drdBranch.value =='" & dt.Rows(i).Item("BranchID") & "') {" & vbCrLf
            End If
            strScript += "document.all['lblBranchPrincipalAmt'].innerHTML = '" & FormatNumber(dt.Rows(i).Item("PrincipalAmount"), 0) & "';" & vbCrLf
            strScript += "document.all['lblBranchOTRAmount'].innerHTML = '" & FormatNumber(dt.Rows(i).Item("OTRAmount"), 0) & "';" & vbCrLf
            strScript += "document.all['lblBranchARAmount'].innerHTML = '" & FormatNumber(dt.Rows(i).Item("ARAmount"), 0) & "';" & vbCrLf
            strScript += "document.all['lblBranchAccount'].innerHTML = '" & CStr(dt.Rows(i).Item("TotalAccount")) & "';" & vbCrLf

            If i = (dt.Rows.Count - 1) Then
                strScript += "}"
            End If

            i += 1

        Next
        strScript += "}"
        strScript &= "</script>"
        Return strScript

    End Function

#End Region
#End Region
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oDataTable As New DataTable
        Dim customClass As New Parameter.FundingContractBatch
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chk As CheckBox
        Dim lblApplicationID As New Label
        Dim txtOSPRINCIPAL As New TextBox


        With oDataTable
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("OutstandingPrincipal", GetType(Decimal)))
            .Columns.Add(New DataColumn("IsChecked", GetType(Boolean)))
        End With

        For intloop = 0 To dtgSelected.Items.Count - 1
            chk = CType(dtgSelected.Items(intloop).FindControl("chkSelect"), CheckBox)
            lblApplicationID = CType(dtgSelected.Items(intloop).FindControl("lblApplicationID"), Label)
            txtOSPRINCIPAL = CType(dtgSelected.Items(intloop).FindControl("txtOSPRINCIPAL"), TextBox)

            oRow = oDataTable.NewRow
            oRow("ApplicationID") = lblApplicationID.Text.Trim
            oRow("OutstandingPrincipal") = txtOSPRINCIPAL.Text.Trim
            oRow("IsChecked") = chk.Checked
            oDataTable.Rows.Add(oRow)
        Next

        If oDataTable.Rows.Count > 0 Then
            '    Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo)
            'Else
            Try
                TotalPokokPerPage = 0
                With customClass
                    .strConnection = GetConnectionString()
                    .FundingCoyId = Me.CompanyID.Trim
                    .FundingContractNo = Me.FundingContractNo.Trim
                    .FundingBatchNo = Me.FundingBatchNo.Trim
                    .ListData = oDataTable
                End With

                m_Company.FundingAgreementSelect(customClass)

                PanelAllFalse()                       
                DisplayHeaderInfo()
                DisplaySelected()
                DisplayTotalSelected()

                btnSelection.Text = "Selection"

                For intloop = 0 To dtgSelected.Items.Count - 1
                    TotalPokokPerPage += CDbl(dtgSelected.Items(intloop).Cells(5).Text)
                    chk = CType(dtgSelected.Items(intloop).FindControl("chkSelect"), CheckBox)
                    chk.Checked = True
                Next

                ShowMessage(lblMessage, "Selection saved!", False)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&Command=Edit&FundingBatchNo=" & Me.FundingBatchNo)
        Response.Redirect("fundingcontractbatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub


    Private Sub dtgSelected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgSelected.ItemCommand
        Select Case e.CommandName
            Case "ViewAgreement"
                Dim lnkAgreementNo As LinkButton
                lnkAgreementNo = CType(dtgSelected.Items(e.Item.ItemIndex).FindControl("lnkAgreementNo"), LinkButton)
                Response.Redirect("../../AccAcq/Credit/ViewStatementOfAccount.aspx?Style=Channeling.css&AgreementNo=" & lnkAgreementNo.Text)
            Case "ViewCustomer"
                Response.Redirect("../../AccAcq/Credit/ViewPersonalCustomer.aspx?Style=Channeling.css&CustomerID=" & e.Item.Cells(0).Text)
        End Select
    End Sub

    Private Sub dtgSelected_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSelected.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label
        Dim ChkBox As CheckBox
        Dim lblFundingPledgeStatus As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCustName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)


            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Channeling" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "Channeling" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"


            ChkBox = CType(e.Item.Cells(0).FindControl("chkSelect"), CheckBox)
            lblFundingPledgeStatus = CType(e.Item.Cells(0).FindControl("lblFundingPledgeStatus"), Label)

            'If lblFundingPledgeStatus.Text.Trim = "S" And btnSelection.Text = "Selection" Or lblFundingPledgeStatus.Text.Trim = "-" And btnSelection.Text = "Selection" Or lblFundingPledgeStatus.Text.Trim = "" And btnSelection.Text = "Selection" Then
            '    ChkBox.Enabled = True
            '    ChkBox.Checked = True
            'ElseIf btnSelection.Text = "Selected" Then
            '    ChkBox.Enabled = True
            '    ChkBox.Checked = False
            'Else
            '    ChkBox.Enabled = False
            '    ChkBox.Checked = False
            'End If

            If ChkBox.Checked Then
                TotalPokokPerPage += CDbl(e.Item.Cells(5).Text)
            End If

        End If

    End Sub

    Public Sub CheckISExecutedAndJFINC()
        Dim Status As String
        Dim FacilityKind As String
        Dim objCommand As New SqlCommand
        Dim objCommand1 As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objConnection1 As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingCheckFundingBatchInstallment"
            objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Parameters.Add("@status", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@Tenor", SqlDbType.SmallInt).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@InterestRate", SqlDbType.Float).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Status = IIf(IsDBNull(objCommand.Parameters("@status").Value), "S", objCommand.Parameters("@status").Value)
            Me.TenorMax = IIf(IsDBNull(objCommand.Parameters("@Tenor").Value), 0, objCommand.Parameters("@Tenor").Value)
            Me.InterestRate = IIf(IsDBNull(objCommand.Parameters("@InterestRate").Value), 0, objCommand.Parameters("@InterestRate").Value)
        Catch ex As Exception
            'Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        Try
            If objConnection1.State = ConnectionState.Closed Then objConnection1.Open()
            objCommand1.CommandType = CommandType.StoredProcedure
            objCommand1.CommandText = "spFundingCheckFundingContractFacilityKind"
            objCommand1.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand1.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand1.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand1.Parameters.Add("@FacilityKind", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output
            objCommand1.Connection = objConnection1
            objCommand1.ExecuteNonQuery()
            FacilityKind = IIf(IsDBNull(objCommand1.Parameters("@FacilityKind").Value), "", objCommand1.Parameters("@FacilityKind").Value)
            Me.FacilityKind = FacilityKind.Trim
        Catch ex As Exception
            'Response.Write(ex.Message)
        Finally
            If objConnection1.State = ConnectionState.Open Then objConnection1.Close()
            objConnection1.Dispose()
            objCommand1.Dispose()
        End Try
        If Status.Trim = "D" And FacilityKind.Trim = "JFINC" Then
            Dim dtslist As DataTable
            Dim cReceive As New FundingCompanyController
            Dim oReceive As New Parameter.FundingContractBatch
            Dim oCustomClass As New Parameter.FundingContractBatch
            Dim orow As DataRow
            Try
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .BusinessDate = Me.BusinessDate
                    .SpName = "spFundingGetOSSelectingAmount"
                End With
                oReceive = cReceive.GetGeneralEditView(oCustomClass)
                dt = oReceive.ListData
                If dt.Rows.Count > 0 Then
                    orow = dt.Rows(0)
                    'If CStr(dt.Rows(0).Item("BranchID")).Trim = drdBranch.SelectedItem.Value Then
                    '    If Not IsDBNull(orow("PrincipalAmount")) Then
                    '        lblBranchPrincipalAmt.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmount"), 0)
                    '    End If
                    '    If Not IsDBNull(orow("ARAmount")) Then
                    '        lblBranchARAmount.Text = FormatNumber(dt.Rows(0).Item("ARAmount"), 0)
                    '    End If
                    '    If Not IsDBNull(orow("ARAmount")) Then
                    '        lblBranchOTRAmount.Text = FormatNumber(dt.Rows(0).Item("OTRAmount"), 0)
                    '    End If
                    '    If Not IsDBNull(orow("ARAmount")) Then
                    '        lblBranchAccount.Text = CStr(dt.Rows(0).Item("TotalAccount"))
                    '    End If

                    'End If
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If


    End Sub

    

    Private Sub btnSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelection.Click        
        Response.Redirect("FundingAgreement.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" & _
                              Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & _
                              Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName & _
                              "&Plafond=" & Me.PlafondAmount)
    End Sub

    Private Sub DisplaySelected()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New FundingCompanyController
        Dim oContract As New Parameter.FundingContractBatch


        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = SortBy
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .Filter = "1"
        End With

        oContract = cContract.FundingAgreementList(oContract)

        'With oContract
        '    lbltotrec.Text = CType(.TotalRecord, String)
        '    lblTotPage.Text = CType(.PageSize, String)
        '    lblPage.Text = CType(.CurrentPage, String)
        '    recordCount = .TotalRecord
        'End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgSelected.DataSource = dtvEntity

        Try
            dtgSelected.DataBind()
        Catch
            dtgSelected.CurrentPageIndex = 0
            dtgSelected.DataBind()
        End Try


        Dim chek As CheckBox
        For index = 0 To dtgSelected.Items.Count - 1
            chek = CType(dtgSelected.Items(index).FindControl("chkSelect"), CheckBox)
            chek.Checked = True
        Next

    End Sub

    Private Sub DisplayTotalSelected()
        Try
            Dim cContract As New FundingCompanyController
            Dim oContract As New Parameter.FundingContractBatch


            With oContract
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            oContract = cContract.FundingTotalSelected(oContract)
            dt = oContract.ListData


            TotalAR = IIf(IsDBNull(dt.Rows(0).Item("ARAmount")), 0, dt.Rows(0).Item("ARAmount"))
            TotalPokok = IIf(IsDBNull(dt.Rows(0).Item("PrincipalAmount")), 0, dt.Rows(0).Item("PrincipalAmount"))
            TotalOTR = IIf(IsDBNull(dt.Rows(0).Item("OTRAmount")), 0, dt.Rows(0).Item("OTRAmount"))
            TotalNTF = IIf(IsDBNull(dt.Rows(0).Item("NTF")), 0, dt.Rows(0).Item("NTF"))
            TotalAccounts = IIf(IsDBNull(dt.Rows(0).Item("TotalAccount")), 0, dt.Rows(0).Item("TotalAccount"))
            IsExecute = IIf(IsDBNull(dt.Rows(0).Item("IsExecute")), 0, dt.Rows(0).Item("IsExecute"))
            StatusDrawn = IIf(IsDBNull(dt.Rows(0).Item("Status")), 0, dt.Rows(0).Item("Status"))
            Me.AmountMustBeAdd = PlafondAmount - TotalPokok

            lblPrincipalAmountL.Text = FormatNumber(TotalPokok, 0)
            lblARAmountL.Text = FormatNumber(TotalAR, 0)
            lblOTRAmountL.Text = FormatNumber(TotalOTR, 0)
            lblNTFtl.Text = FormatNumber(TotalNTF, 0)
            Me.IsExecute = IsExecute

            If StatusDrawn = "D" Then
                btnSelection.Visible = False
                btnSave.Visible = False
            End If

            lblTotalAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0).Trim
            lblAccountL.Text = CStr(TotalAccounts)
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

    Sub DisplayTotalSelectedByCheckBox()
        Dim pokok As Double
        Dim ar As Double
        Dim otr As Double
        Dim ntf As Double
        Dim totalacc As Integer

        TotalChecked(pokok, ar, otr, ntf, totalacc)

        'lblPrincipalAmountL.Text = FormatNumber(pokok)
        'lblPrincipalAmountL.Text = FormatNumber(TotalPokok)
        lblARAmountL.Text = FormatNumber(TotalAR, 0)
        lblOTRAmountL.Text = FormatNumber(TotalOTR, 0)
        lblNTFtl.Text = FormatNumber(TotalNTF, 0)
        'lblAmountMustBeAdd.Text = FormatNumber(Me.AmountMustBeAdd, 0)
        lblAccountL.Text = FormatNumber(TotalAccounts, 0)


    End Sub

    Sub TotalChecked(pokok As Double, ar As Double, otr As Double, ntf As Double, totalacc As Integer)
        Dim chkSelect As CheckBox
        pokok = 0

        For Each row As DataGridItem In dtgSelected.Items
            chkSelect = CType(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                pokok = pokok + CDbl(row.Cells(5).Text)
            End If
        Next

        'dimatiin ajah... by hendirk 10-11-2017
        'If pokok > PlafondAmount Then
        '    ShowMessage(lblMessage, "Nilai kontrak melebihi nilai plafond!", True)
        '    btnSave.Visible = False
        'Else

        '    lblPrincipalAmountL.Text = FormatNumber(pokok, 0)
        '    lblTotalAmountMustBeAdd.Text = FormatNumber(PlafondAmount - pokok, 0)
        '    btnSave.Visible = True
        'End If

    End Sub

    Private Sub btnExecution_Click(sender As Object, e As System.EventArgs) Handles btnExecution.Click
        Dim isProcessSuccess As Boolean
        Dim dt As New DataTable
        dt = GetDetailFundingBatch(Me.FundingBatchNo, Me.BankID, Me.CompanyID, Me.FundingContractNo)

        Me.InsPeriod = CStr(dt.Rows(0).Item("InstallmentPeriod"))
        Me.InsScheme = CStr(dt.Rows(0).Item("InstallmentScheme"))
        Me.PaymentScheme = CStr(dt.Rows(0).Item("PaymentScheme"))
        Me.StatusDrawn = CStr(dt.Rows(0).Item("Status"))
        Me.BatchDate = CDate(dt.Rows(0).Item("BatchDate"))
        Me.FundingBatchNo = CStr(dt.Rows(0).Item("FundingBatchNo"))
        Me.InterestType = CStr(dt.Rows(0).Item("InterestType"))
        Me.FinalMaturityDate = CDate(dt.Rows(0).Item("FinalMaturityDate"))
        Me.ProvisionAmount = CDbl(dt.Rows(0).Item("ProvisionFeeAmount"))
        Me.AdminAmount = CDbl(dt.Rows(0).Item("AdminAmount"))
        Me.CaraPembayaran = CStr(dt.Rows(0).Item("CaraBayar"))
        Me.HitungBUnga = CStr(dt.Rows(0).Item("perhitunganBunga"))
        Me.IsFundingNTF = CBool(dt.Rows(0).Item("IsFundingNTF"))
        Me.FirstInstallment = CStr(dt.Rows(0).Item("FirstInstallment"))
        Me.BankAccountID = CStr(dt.Rows(0).Item("BankAccountID"))
        Me.BPKBLocation = CStr(dt.Rows(0).Item("AssetDocLocation"))
        Me.Currency = CStr(dt.Rows(0).Item("CurrencyID"))
        Me.ExchangeRate = CDbl(dt.Rows(0).Item("ExchangeRate"))



        If IsDBNull(dt.Rows(0).Item("InstallmentDueDate")) Then
            Me.DueDate_ = CDate(DateAdd(DateInterval.Month, 1, CDate(dt.Rows(0).Item("BatchDate"))))
        Else
            Me.DueDate_ = CDate(dt.Rows(0).Item("InstallmentDueDate"))
        End If

        Me.Amount = CDbl(dt.Rows(0).Item("OSAmtToFunCoy"))

        edit()

        Select Case CStr(dt.Rows(0).Item("perhitunganBunga"))
            Case "EFEKTIF"
                If CStr(dt.Rows(0).Item("FirstInstallment")) = "AR" Then
                    isProcessSuccess = updateAgreementExecution2()
                Else
                    'tidak ada rumus untuk metode efektif advance, tampilkan warning
                    'isProcessSuccess = updateAgreementExecution2()
                    ShowMessage(lblMessage, "Angsuran pertama harus Arrear!", True)
                    isProcessSuccess = False
                End If

            Case "ANUITAS"
                If dt.Rows(0).Item("FirstInstallment") = "AR" Then
                    isProcessSuccess = updateAgreementExecution()
                Else
                    isProcessSuccess = updateAgreementExecutionAD()
                End If
            Case "FLAT"
        End Select

        Select Case CStr(dt.Rows(0).Item("perhitunganBunga"))
            'Case "MENURUN"
            '    Throw New Exception("Perhitungan funding agreement installment untuk tipe bunga menurun belum tersedia.")
            'Case "TERM LOAN"
            '    Throw New Exception("Perhitungan funding agreement installment untuk tipe bunga term loan belum tersedia.")
            'Case Else
            '    'anuitas
            '    isProcessSuccess = AddFundingAgreementInstallmentExecutionKMK()

            Case "ANUITAS"
                If CStr(dt.Rows(0).Item("FirstInstallment")) = "AR" Then
                    isProcessSuccess = AddFundingAgreementInstallmentExecution()
                Else
                    isProcessSuccess = AddFundingAgreementInstallmentExecutionAD()
                End If

            Case "EFEKTIF"
                If CStr(dt.Rows(0).Item("FirstInstallment")) = "AR" Then
                    isProcessSuccess = genFAIEfektif()
                Else
                    'tidak ada rumus untuk metode efektif advance, tampilkan warning
                    'isProcessSuccess = updateAgreementExecution2()
                    ShowMessage(lblMessage, "Angsuran pertama harus Arrear!", True)
                    isProcessSuccess = False
                End If
        End Select

        UpdateDrawdownAmount()
    End Sub
    Private Sub edit()
        Dim customClass As New Parameter.FundingContractBatch
        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNoUsed = Me.FundingBatchNo
                .FundingBatchNo = Me.FundingBatchNo.Trim
                .BatchDate = Me.BatchDate
                .PrincipalAmtToFunCoy = Me.Amount
                '.InterestRate = CDec(txtInterestRate.Text.Trim)
                '.InterestRate = 0
                .InterestType = Me.InterestType
                .FinalMaturityDate = Me.FinalMaturityDate
                .PaymentScheme = Me.PaymentScheme
                .ProvisionFeeAmount = Me.ProvisionAmount
                .AdminAmount = Me.AdminAmount
                .CurrencyID = Me.Currency

                If Me.Currency = "IDR" Then
                    .ExchangeRate = 1
                Else
                    .ExchangeRate = Me.ExchangeRate
                End If

                '.Tenor = CInt(txtTenor.Text.Trim)
                '.Tenor = 0
                .InstallmentPeriod = Me.InsPeriod
                .InstallmentScheme = Me.InsScheme
                .ProposeDate = Me.BatchDate
                .AragingDate = ConvertDate2("02/09/1976")
                .RealizedDate = ConvertDate2("02/09/1976")
                .InstallmentDueDate = Me.DueDate_
                .AccProposedNum = 0
                .AccRealizedNum = 0
                .OSAmtToFunCoy = Me.Amount
                .AssetDocLocation = Me.BPKBLocation.Trim
                .BankAccountID = Me.BankAccountID.Trim
                .FirstInstallment = Me.FirstInstallment.Trim
                .isFundingNTF = Me.IsFundingNTF
                .perhitunganBunga = Me.HitungBUnga
                .CaraBayar = Me.CaraPembayaran
            End With

            m_Company.EditFundingContractBatch(customClass)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Function GetDetailFundingBatch(ByVal FundingBatchNo As String, ByVal BankID As String, ByVal CompanyID As String, ByVal FundingContractNo As String) As DataTable
        Dim dt As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " FundingBatch.FundingBatchNo='" & FundingBatchNo & "' and FundingBatch.BankId='" & BankID & "' and FundingBatch.FundingCoyId='" & CompanyID & "' and FundingBatch.FundingContractNo='" & FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingContractBatchById"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        dt = oContract.ListData
        Return dt
    End Function
    Public Sub UpdateDrawdownAmount()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingUpdateDrawDownAmount"
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Private Function updateAgreementExecution() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Dim DrawDownAmount, PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal
        Dim InsAmount() As Decimal
        Dim InterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal

        'Try
        With customClass
            .strConnection = GetConnectionString()
            .BankId = Me.BankID
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
        End With

        If Me.StatusDrawn = "D" Then
            m_Company.FundingUpdateAgreementSecondExecution(customClass)
        Else
            m_Company.UpdateFundingAgreementExecution(customClass)
        End If

        'Tenor = CDec(txtTenor.Text)
        Tenor = Me.TenorMax

        If Me.InsPeriod = "2" Then
            MonthPeriod = 6
            Tenor = Math.Round(Tenor / 2, 0)
        ElseIf Me.InsPeriod = "3" Then
            MonthPeriod = 4
            Tenor = Math.Round(Tenor / 3, 0)
        ElseIf Me.InsPeriod = "6" Then
            MonthPeriod = 2
            Tenor = Math.Round(Tenor / 6, 0)
        Else
            MonthPeriod = 12
        End If

        ReDim PrincipalAmount(CInt(Tenor))
        ReDim InterestAmount(CInt(Tenor))
        ReDim OSPrincipalAmount(CInt(Tenor))
        ReDim InsAmount(CInt(Tenor))
        ReDim OSInterestAmount(CInt(Tenor))

        'If Me.FacilityKind = "JFINC" Then
        '    txtAmount.Text = getDrawdownAmount()
        'End If

        If Me.FacilityKind = "JFINC" Then
            DrawDownAmount = getDrawdownAmount()
        Else
            DrawDownAmount = Me.Amount
        End If

        'DrawDownAmount =  CDec(txtAmount.Text)
        'InterestRate = CDec(txtInterestRate.Text)
        InterestRate = Me.InterestRate
        Batchdate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)

        If Me.PaymentScheme = "1" Or Me.PaymentScheme = "2" Or Me.PaymentScheme = "6" Then
            iDueDate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)
            iDueDate = DateAdd(DateInterval.Month, 1, iDueDate)
        ElseIf Me.PaymentScheme = "3" Then
            iDueDate = Me.DueDate_ ' ConvertDate2(txtDueDate.Text)
        ElseIf Me.PaymentScheme = "4" Then
            iDueDate = Me.DueDate_ 'ConvertDate2(txtDueDate.Text)
            Select Case Month(iDueDate)
                Case 1
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 2
                    If (Year(Me.DueDate_) Mod 4) = 0 Then
                        greaterDay = CStr(Month(iDueDate)).Trim + "/29/" + CStr(Year(iDueDate)).Trim
                    Else
                        greaterDay = CStr(Month(iDueDate)).Trim + "/28/" + CStr(Year(iDueDate)).Trim
                    End If
                Case 3
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 4
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 5
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 6
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 7
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 8
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 9
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 10
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                Case 11
                    greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                Case 12
                    greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
            End Select
            iDueDate = CDate(greaterDay.Trim)
        End If

        InsScheme = Me.InsScheme

        If Tenor > 0 Then
            If InsScheme = "A" Then
                InsAmount(0) = Math.Round(Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, 0), 0)
                TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount

                If Me.PaymentScheme = "4" Or Me.PaymentScheme = "3" Then
                    InterestAmount(0) = Math.Round(CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount)), 0)
                Else
                    InterestAmount(0) = Math.Round((DrawDownAmount * InterestRate) / (MonthPeriod * 100), 0)
                End If

                PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 0)
                OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                For i = 1 To Tenor - 1
                    InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                    PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                    OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                    OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                Next

            ElseIf InsScheme = "P" Then
                TotalInterest = 0
                PrincipalAmount(0) = DrawDownAmount / Tenor
                If Me.PaymentScheme = "4" Or Me.PaymentScheme = "3" Then
                    InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount))
                    'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(InterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                Else
                    InterestAmount(0) = (DrawDownAmount * InterestRate) / (MonthPeriod * 100)
                End If
                InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                TotalInterest = InterestAmount(0)
                For i = 1 To Tenor - 1
                    InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                    InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                    OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                    TotalInterest += InterestAmount(i)
                Next
                OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                For i = 1 To Tenor - 1
                    OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                Next

            ElseIf InsScheme = "I" Then
                TotalInterest = 0
                PrincipalAmount(0) = 0
                InterestAmount(0) = 0
                InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                OSPrincipalAmount(0) = 0
                TotalInterest = InterestAmount(0)
                For i = 1 To Tenor - 1
                    InterestAmount(i) = 0
                    InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                    OSPrincipalAmount(i) = 0
                Next
                OSInterestAmount(0) = 0
                For i = 1 To Tenor - 1
                    OSInterestAmount(i) = 0
                Next
            End If

            'Write To Database Installment
            j = 0
            For i = 0 To Tenor - 1

                With customClass
                    .strConnection = GetConnectionString()
                    .BankId = Me.BankID
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .InsSecNo = i + 1

                    If i = 0 Then
                        .DueDate = iDueDate
                    Else
                        .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                    End If

                    If InsScheme = "A" Then
                        .PrincipalAmount = PrincipalAmount(i)
                    Else
                        .PrincipalAmount = PrincipalAmount(0)
                    End If

                    .InterestAmount = InterestAmount(i)
                    .PrincipalPaidAmount = 0
                    .InterestPaidAmount = 0
                    .OSPrincipalAmount = OSPrincipalAmount(i)
                    .OSInterestAmount = OSInterestAmount(i)
                End With

                m_Company.AddFundingContractBatchIns(customClass)

                If Me.InsPeriod = "2" Then
                    j = j + 2
                ElseIf Me.InsPeriod = "3" Then
                    j = j + 3
                ElseIf Me.InsPeriod = "6" Then
                    j = j + 6
                Else
                    j = i + 1
                End If
            Next

        End If

        ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
        Return True
        'Catch ex As Exception
        '    ShowMessage(lblMessage, ex.Message, True)
        '    Return False
        'End Try

    End Function
    Private Function updateAgreementExecution2() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            If Me.StatusDrawn = "D" Then
                m_Company.FundingUpdateAgreementSecondExecution(customClass)
            Else
                m_Company.UpdateFundingAgreementExecution(customClass)
            End If


            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .InterestRate = Me.InterestRate
                .Tenor = Me.TenorMax
                .DueDate = Me.BatchDate ' ConvertDate2(txtBatchDate.Text)
                If Me.FacilityKind = "JFINC" Then
                    .PrincipalAmount = getDrawdownAmount()
                Else
                    .PrincipalAmount = Me.Amount
                End If
            End With

            m_Company.AddFundingContractBatchIns2(customClass)

            ShowMessage(lblMessage, "Eksekusi-2 Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Private Function updateAgreementExecutionAD() As Boolean
        Dim customClass As New Parameter.FundingContractBatch
        Dim dt As DataTable

        Dim DrawDownAmount As Decimal

        Dim InsAmount(), PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount(), TotalInterest As Decimal

        Dim InterestRate As Decimal
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim iDueDate As DateTime
        Dim Batchdate As DateTime
        Dim InsScheme As Char
        Dim greaterDay As String
        Dim MonthPeriod As Decimal

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            m_Company.UpdateFundingAgreementExecution(customClass)

            'Tenor = CDec(txtTenor.Text)            
            Tenor = Me.TenorMax

            If Me.InsPeriod = "2" Then
                MonthPeriod = 6
                Tenor = Math.Round(Tenor / 2, 0)
            ElseIf Me.InsPeriod = "3" Then
                MonthPeriod = 4
                Tenor = Math.Round(Tenor / 3, 0)
            ElseIf Me.InsPeriod = "6" Then
                MonthPeriod = 2
                Tenor = Math.Round(Tenor / 6, 0)
            Else
                'Installment Period, monthly
                MonthPeriod = 12
            End If

            ReDim PrincipalAmount(CInt(Tenor))
            ReDim InterestAmount(CInt(Tenor))
            ReDim OSPrincipalAmount(CInt(Tenor))
            ReDim InsAmount(CInt(Tenor))
            ReDim OSInterestAmount(CInt(Tenor))

            

            If Me.FacilityKind = "JFINC" Then
                DrawDownAmount = getDrawdownAmount()
            Else
                DrawDownAmount = Me.Amount
            End If
            'InterestRate = CDec(txtInterestRate.Text)
            InterestRate = 0
            Batchdate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)

            'Payment Scheme, Principal & Interest on Drawdown Date (1)
            If Me.PaymentScheme = "1" Or Me.PaymentScheme = "2" Or Me.PaymentScheme = "6" Then
                iDueDate = Me.BatchDate 'ConvertDate2(txtBatchDate.Text)
            ElseIf Me.PaymentScheme = "3" Then
                iDueDate = Me.DueDate_ 'ConvertDate2(txtDueDate.Text)
            ElseIf Me.PaymentScheme = "4" Then
                iDueDate = Me.DueDate_ 'ConvertDate2(txtDueDate.Text)
                Select Case Month(iDueDate)
                    Case 1
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 2
                        If (Year(Me.DueDate_) Mod 4) = 0 Then
                            greaterDay = CStr(Month(iDueDate)).Trim + "/29/" + CStr(Year(iDueDate)).Trim
                        Else
                            greaterDay = CStr(Month(iDueDate)).Trim + "/28/" + CStr(Year(iDueDate)).Trim
                        End If
                    Case 3
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 4
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 5
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 6
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 7
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 8
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 9
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 10
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                    Case 11
                        greaterDay = CStr(Month(iDueDate)).Trim + "/30/" + CStr(Year(iDueDate)).Trim
                    Case 12
                        greaterDay = CStr(Month(iDueDate)).Trim + "/31/" + CStr(Year(iDueDate)).Trim
                End Select
                iDueDate = CDate(greaterDay.Trim)
            End If

            InsScheme = Me.InsScheme 'drdInsScheme.SelectedItem.Value

            If Tenor > 0 Then
                'Fixed amount
                If InsScheme = "A" Then
                    InsAmount(0) = Math.Round(Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -DrawDownAmount, 0, DueDate.BegOfPeriod), 2)
                    TotalInterest = (InsAmount(0) * Tenor) - DrawDownAmount

                    If Me.PaymentScheme = "4" Or Me.PaymentScheme = "3" Then
                        InterestAmount(0) = Math.Round(CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount)), 0)
                    Else
                        'Payment scheme Principal & Interest on Drawdown Date (1)
                        InterestAmount(1) = Math.Round((DrawDownAmount * InterestRate) / (MonthPeriod * 100), 2)
                    End If

                    PrincipalAmount(0) = Math.Round(InsAmount(0), 2)
                    OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                    OSInterestAmount(0) = TotalInterest

                    For i = 1 To Tenor - 1
                        InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                        PrincipalAmount(i) = InsAmount(0) - InterestAmount(i)
                        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(i)
                        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                    Next

                ElseIf InsScheme = "P" Then
                    TotalInterest = 0
                    PrincipalAmount(0) = DrawDownAmount / Tenor
                    If Me.PaymentScheme = "4" Or Me.PaymentScheme = "3" Then
                        InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) * CDec(InterestRate / (36000)) * CDec(DrawDownAmount))
                        'InterestAmount(0) = CDec(CDec(DateDiff(DateInterval.Day, Batchdate, CDate(iDueDate))) / 30) * CDec(InterestRate / (MonthPeriod * 100)) * CDec(DrawDownAmount)
                    Else
                        InterestAmount(0) = (DrawDownAmount * InterestRate) / (MonthPeriod * 100)
                    End If
                    InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                    OSPrincipalAmount(0) = DrawDownAmount - PrincipalAmount(0)
                    TotalInterest = InterestAmount(0)
                    For i = 1 To Tenor - 1
                        InterestAmount(i) = (OSPrincipalAmount(i - 1) * InterestRate) / (MonthPeriod * 100)
                        InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                        OSPrincipalAmount(i) = OSPrincipalAmount(i - 1) - PrincipalAmount(0)
                        TotalInterest += InterestAmount(i)
                    Next
                    OSInterestAmount(0) = TotalInterest - InterestAmount(0)
                    For i = 1 To Tenor - 1
                        OSInterestAmount(i) = OSInterestAmount(i - 1) - InterestAmount(i)
                    Next

                ElseIf InsScheme = "I" Then
                    TotalInterest = 0
                    PrincipalAmount(0) = 0
                    InterestAmount(0) = 0
                    InsAmount(0) = PrincipalAmount(0) + InterestAmount(0)
                    OSPrincipalAmount(0) = 0
                    TotalInterest = InterestAmount(0)
                    For i = 1 To Tenor - 1
                        InterestAmount(i) = 0
                        InsAmount(i) = PrincipalAmount(0) + InterestAmount(i)
                        OSPrincipalAmount(i) = 0
                    Next
                    OSInterestAmount(0) = 0
                    For i = 1 To Tenor - 1
                        OSInterestAmount(i) = 0
                    Next
                End If

                'Write To Database Installment
                j = 0
                For i = 0 To Tenor - 1

                    With customClass
                        .strConnection = GetConnectionString()
                        .BankId = Me.BankID
                        .FundingCoyId = Me.CompanyID
                        .FundingContractNo = Me.FundingContractNo
                        .FundingBatchNo = Me.FundingBatchNo
                        .InsSecNo = i + 1

                        If i = 0 Then
                            .DueDate = iDueDate
                        Else
                            .DueDate = DateAdd(DateInterval.Month, j, iDueDate)
                        End If

                        If InsScheme = "A" Then
                            .PrincipalAmount = PrincipalAmount(i)
                        Else
                            .PrincipalAmount = PrincipalAmount(0)
                        End If

                        .InterestAmount = InterestAmount(i)
                        .PrincipalPaidAmount = 0
                        .InterestPaidAmount = 0
                        .OSPrincipalAmount = OSPrincipalAmount(i)
                        .OSInterestAmount = OSInterestAmount(i)
                    End With

                    m_Company.AddFundingContractBatchIns(customClass)

                    If Me.InsScheme = "2" Then
                        j = j + 2
                    ElseIf Me.InsScheme = "3" Then
                        j = j + 3
                    ElseIf Me.InsScheme = "6" Then
                        j = j + 6
                    Else
                        j = i + 1
                    End If
                Next

            End If
            ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Public Function getDrawdownAmount() As Decimal
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetDrawdownAmount"

            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@FundingBatchNo", SqlDbType.VarChar, 20).Value = Me.FundingBatchNo
            objCommand.Parameters.Add("@DrawdownAmount ", SqlDbType.Decimal).Direction = ParameterDirection.Output

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            Return IIf(IsDBNull(objCommand.Parameters("@DrawdownAmount ").Value), "0", objCommand.Parameters("@DrawdownAmount ").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Function
    Public Function AddFundingAgreementInstallmentExecutionAD() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim FundingCoyPortion As Decimal
        Dim TenorAgr As Integer
        Dim Tenor As Integer

        Dim Beginingbalance As Decimal
        Dim SecPercentage As Decimal
        Dim SecCoverType As String
        Dim OutStandingPrincipal As Decimal
        Dim ARAgreement As Decimal
        Dim TotalOTR As Decimal
        Dim MonthPeriod As Integer
        Dim TotalInterest As Decimal
        Dim InterestRate As Decimal
        Dim BranchID, ApplicationID As String
        Dim SDuedate As DateTime
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingAgreementBatchInstallmentTable"
            End With

            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData

            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OutstandingPrincipal")
                        ARAgreement = dtslist.Rows(intLoopOmset).Item("OSARAgreement")
                        TotalOTR = dtslist.Rows(intLoopOmset).Item("TotalOTR")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("SDuedate")
                        InterestRate = dtslist.Rows(intLoopOmset).Item("InterestRate")
                        TenorAgr = dtslist.Rows(intLoopOmset).Item("TenorAgr")
                        FundingCoyPortion = dtslist.Rows(intLoopOmset).Item("FundingCoyPortion")
                        SecCoverType = dtslist.Rows(intLoopOmset).Item("SecurityCoverageType")
                        SecPercentage = dtslist.Rows(intLoopOmset).Item("SecurityCoveragePercentage")
                        'Tenor = CInt(txtTenor.Text)
                        Tenor = dtslist.Rows(intLoopOmset).Item("SisaTenor")

                        If Me.InsPeriod = "2" Then
                            MonthPeriod = 6
                            Tenor = Math.Round(Tenor / 2, 0)
                        ElseIf Me.InsPeriod = "3" Then
                            MonthPeriod = 4
                            Tenor = Math.Round(Tenor / 3, 0)
                        ElseIf Me.InsPeriod = "6" Then
                            MonthPeriod = 2
                            Tenor = Math.Round(Tenor / 6, 0)
                        Else
                            MonthPeriod = 12
                        End If

                        'If TenorAgr < Tenor Then
                        '    Tenor = TenorAgr
                        'End If

                        ReDim PrincipalAmount(CInt(Tenor))
                        ReDim InterestAmount(CInt(Tenor))
                        ReDim OSPrincipalAmount(CInt(Tenor))
                        ReDim OSInterestAmount(CInt(Tenor))
                        ReDim InsAmount(CInt(Tenor))

                        'Beginingbalance = Math.Round(CDec((OutStandingPrincipal * FundingCoyPortion) / 100), 2)
                        Beginingbalance = Math.Round(OutStandingPrincipal, 2)
                        InsAmount(0) = Math.Round((Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -Beginingbalance, 0, DueDate.BegOfPeriod)), 2)
                        TotalInterest = Math.Round((InsAmount(0) * Tenor) - Beginingbalance, 2)

                        'InterestAmount(0) = Math.Round((Beginingbalance * InterestRate) / (MonthPeriod * 100), 2)
                        InterestAmount(0) = 0

                        PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 2)
                        OSPrincipalAmount(0) = Beginingbalance - PrincipalAmount(0)
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = 1
                            .DueDate = DateAdd(DateInterval.Month, 0, SDuedate)
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = CDec(InterestRate)
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(Me.InsPeriod)
                            .MaximumDateForDD = CInt(MonthPeriod)

                        End With
                        cReceive.AddFundingAgreementInstallment(oCustomClass)
                    Next
                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

    Public Function AddFundingAgreementInstallmentExecution() As Boolean
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Dim intLoopOmset As Integer
        Dim FundingCoyPortion As Decimal
        Dim TenorAgr As Integer
        Dim Tenor As Integer
        Dim i As Integer
        Dim j As Integer
        Dim InitialDueDate As Integer

        Dim Beginingbalance As Decimal
        Dim SecPercentage As Decimal
        Dim SecCoverType As String
        Dim OutStandingPrincipal As Decimal
        Dim ARAgreement As Decimal
        Dim TotalOTR As Decimal
        Dim MonthPeriod As Integer
        Dim TotalInterest As Decimal
        Dim InterestRate As Decimal
        Dim BranchID, ApplicationID As String
        Dim SDuedate As DateTime
        Dim PrincipalAmount(), InterestAmount() As Decimal
        Dim OSPrincipalAmount(), OSInterestAmount() As Decimal
        Dim InsAmount() As Decimal

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingAgreementBatchInstallmentTable"
            End With

            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData

            If dtslist.Rows.Count > 0 Then
                Try
                    For intLoopOmset = 0 To dtslist.Rows.Count - 1
                        BranchID = dtslist.Rows(intLoopOmset).Item("BranchID")
                        ApplicationID = dtslist.Rows(intLoopOmset).Item("ApplicationID")
                        OutStandingPrincipal = dtslist.Rows(intLoopOmset).Item("OutstandingPrincipal")
                        ARAgreement = dtslist.Rows(intLoopOmset).Item("OSARAgreement")
                        TotalOTR = dtslist.Rows(intLoopOmset).Item("TotalOTR")
                        SDuedate = dtslist.Rows(intLoopOmset).Item("SDuedate")
                        InterestRate = dtslist.Rows(intLoopOmset).Item("InterestRate")
                        TenorAgr = dtslist.Rows(intLoopOmset).Item("TenorAgr")
                        FundingCoyPortion = dtslist.Rows(intLoopOmset).Item("FundingCoyPortion")
                        SecCoverType = dtslist.Rows(intLoopOmset).Item("SecurityCoverageType")
                        SecPercentage = dtslist.Rows(intLoopOmset).Item("SecurityCoveragePercentage")
                        'Tenor = CInt(txtTenor.Text)
                        Tenor = dtslist.Rows(intLoopOmset).Item("SisaTenor")

                        If Me.InsPeriod = "2" Then
                            MonthPeriod = 6
                            Tenor = Math.Round(Tenor / 2, 0)
                        ElseIf Me.InsPeriod = "3" Then
                            MonthPeriod = 4
                            Tenor = Math.Round(Tenor / 3, 0)
                        ElseIf Me.InsPeriod = "6" Then
                            MonthPeriod = 2
                            Tenor = Math.Round(Tenor / 6, 0)
                        Else
                            MonthPeriod = 12
                        End If

                        'If TenorAgr < Tenor Then
                        '    Tenor = TenorAgr
                        'End If

                        ReDim PrincipalAmount(CInt(Tenor))
                        ReDim InterestAmount(CInt(Tenor))
                        ReDim OSPrincipalAmount(CInt(Tenor))
                        ReDim OSInterestAmount(CInt(Tenor))
                        ReDim InsAmount(CInt(Tenor))

                        'Beginingbalance = Math.Round(CDec((OutStandingPrincipal * FundingCoyPortion) / 100), 2)
                        Beginingbalance = Math.Round(OutStandingPrincipal, 2)
                        InsAmount(0) = Math.Round((Pmt(((InterestRate) / (MonthPeriod * 100)), Tenor, -Beginingbalance, 0, DueDate.EndOfPeriod)), 2)
                        TotalInterest = Math.Round((InsAmount(0) * Tenor) - Beginingbalance, 2)
                        InterestAmount(0) = Math.Round((Beginingbalance * InterestRate) / (MonthPeriod * 100), 2)
                        PrincipalAmount(0) = Math.Round(InsAmount(0) - InterestAmount(0), 2)
                        OSPrincipalAmount(0) = Beginingbalance - PrincipalAmount(0)
                        OSInterestAmount(0) = TotalInterest - InterestAmount(0)

                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .FundingCoyId = Me.CompanyID
                            .FundingContractNo = Me.FundingContractNo
                            .FundingBatchNo = Me.FundingBatchNo
                            .BranchId = BranchID
                            .ApplicationID = ApplicationID
                            .InsSecNo = 1
                            .DueDate = DateAdd(DateInterval.Month, 0, SDuedate)
                            .PrepaymentAmount = InsAmount(0)
                            .PrincipalAmount = PrincipalAmount(0)
                            .InterestAmount = InterestAmount(0)
                            .PrincipalPaidAmount = 0
                            .InterestPaidAmount = 0
                            .OSPrincipalAmount = OSPrincipalAmount(0)
                            .OSInterestAmount = OSInterestAmount(0)
                            .InterestRate = CDec(InterestRate)
                            .Tenor = CInt(Tenor)
                            .LCGracePeriod = CInt(Me.InsPeriod) ' CInt(drdInsPeriod.SelectedItem.Value.Trim)
                            .MaximumDateForDD = CInt(MonthPeriod)

                        End With
                        cReceive.AddFundingAgreementInstallment(oCustomClass)
                    Next
                    ShowMessage(lblMessage, "Eksekusi Berhasil ", False)
                    Return True
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    Return False
                End Try
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                Return False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Private Function genFAIEfektif() As Boolean
        Dim customClass As New Parameter.FundingContractBatch

        Try
            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .InterestRate = Me.InterestRate

                ' tenor ambil dari sisa tenor yang belum dibayar ada di SP
                '.Tenor = CDec(txtTenor.Text)
                .Tenor = 0

                .DueDate = Me.BatchDate
            End With

            m_Company.genFAIEfektif(customClass)
            ShowMessage(lblMessage, "Funding Agreement Installment saved!", False)
            Return True

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function
    Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        Dim dtUploadFromExcel As DataTable
        Dim strFileName As String

        If (FileUpload.HasFile) Then

            If Not IsDBNull(FileUpload.PostedFile) And
                FileUpload.PostedFile.ContentLength > 0 Then
                Try

                    UploadFunding(FileUpload, "DataUploadFundingAgreement")
                    strFileName = pathFile("Funding", "DataUploadFundingAgreement") & "DataUploadFundingAgreement.xls"

                    If FileUpload.HasFile Then
                        If File.Exists(strFileName) Then
                            dtUploadFromExcel = ExcelToDataTable(strFileName)

                            DtgViewUploadFromExcel.DataSource = dtUploadFromExcel
                            DtgViewUploadFromExcel.DataBind()


                            ShowMessage(lblMessage, "Upload Data Berhasil ", False)
                            divupload.Visible = True
                            File.Delete(strFileName)
                        End If
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)

                Finally

                End Try
            Else
                ShowMessage(lblMessage, "Upload Data Gagal ", True)
            End If
        Else
            ShowMessage(lblMessage, "Upload Data Gagal ", True)
        End If
    End Sub
    Protected Sub btnExecuteFromUpload_Click(sender As Object, e As EventArgs) Handles btnExecuteFromUpload.Click
        Dim dtUploadFromExcelx As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim lblNoPJJ As New Label
        Dim lblOSPrincipal As New Label
        Dim lblOSInterest As New Label
        Dim lblSisaTenor As New Label
        Dim lblRateBatch As New Label
        Dim customClass As New Parameter.FundingContractBatch

        With dtUploadFromExcelx
            .Columns.Add(New DataColumn("NoPJJ", GetType(String)))
            .Columns.Add(New DataColumn("OutstandingPrincipal", GetType(Decimal)))
            '.Columns.Add(New DataColumn("OutstandingInterest", GetType(Decimal)))
            '.Columns.Add(New DataColumn("SisaTenor", GetType(Integer)))
            '.Columns.Add(New DataColumn("RateBatch", GetType(Decimal)))
            .Columns.Add(New DataColumn("IsChecked", GetType(Boolean)))

        End With

        For intloop = 0 To DtgViewUploadFromExcel.Items.Count - 1
            lblNoPJJ = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblNoPJJ"), Label)
            lblOSPrincipal = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblOSPOkok"), Label)
            'lblOSInterest = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblOSBunga"), Label)
            'lblSisaTenor = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblSisaTenor"), Label)
            'lblRateBatch = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblRateBatch"), Label)

            oRow = dtUploadFromExcelx.NewRow
            oRow("NoPJJ") = lblNoPJJ.Text.Trim
            oRow("OutstandingPrincipal") = If((String.IsNullOrEmpty(lblOSPrincipal.Text.Trim)), 0, lblOSPrincipal.Text.Trim) 'lblOSPrincipal.Text.Trim
            'oRow("OutstandingInterest") = If((String.IsNullOrEmpty(lblOSInterest.Text.Trim)), 0, lblOSInterest.Text.Trim) 'lblOSInterest.Text.Trim
            'oRow("SisaTenor") = lblSisaTenor.Text.Trim
            'oRow("RateBatch") = lblRateBatch.Text.Trim
            oRow("IsChecked") = True
            dtUploadFromExcelx.Rows.Add(oRow)
        Next

        Try
            With customClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID.Trim
                .FundingContractNo = Me.FundingContractNo.Trim
                .FundingBatchNo = Me.FundingBatchNo.Trim
                .Listdata = dtUploadFromExcelx
                .IsExcel = True
            End With

            m_Company.FundingAgreementSelectFromUpload(customClass)

            Response.Redirect("FundingAgreementSelected.aspx?Command=SELECT&CompanyID=" & Me.CompanyID & "&CompanyName=" &
                              Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" &
                              Me.FundingContractNo & "&FundingBatchNo=" & Me.FundingBatchNo & "&ContractName=" & Me.ContractName &
                              "&Plafond=" & Me.PlafondAmount)


            pnl1.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    Public Shared Function ExcelToDataTable(filePath As String) As DataTable
        Dim dtExcel As New DataTable()
        Dim hssfworkbook As HSSFWorkbook
        Using file As New FileStream(filePath, FileMode.Open, FileAccess.Read)
            hssfworkbook = New HSSFWorkbook(file)
        End Using
        Dim sheet As ISheet = hssfworkbook.GetSheetAt(0)
        Dim rows As System.Collections.IEnumerator = sheet.GetRowEnumerator()

        Dim headerRow As IRow = sheet.GetRow(0)
        Dim cellCount As Integer = headerRow.LastCellNum

        For j As Integer = 0 To cellCount - 1
            Dim cell As ICell = headerRow.GetCell(j)
            dtExcel.Columns.Add(cell.ToString())
        Next

        For i As Integer = (sheet.FirstRowNum + 1) To sheet.LastRowNum
            Dim row As IRow = sheet.GetRow(i)
            Dim dataRow As DataRow = dtExcel.NewRow()
            If row Is Nothing Then
                Exit For
            End If
            For j As Integer = row.FirstCellNum To cellCount - 1
                If row.GetCell(j) IsNot Nothing Then
                    dataRow(j) = row.GetCell(j).ToString()
                End If
            Next

            dtExcel.Rows.Add(dataRow)
        Next
        Return dtExcel

    End Function
    Private Function pathFile(ByVal Group As String, ByVal Data As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\Upload\" & Group & "\" & Data & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Sub UploadFunding(FileUpload1 As FileUpload, FileName As String)

        If (FileUpload1.HasFile) Then
            If (CheckFileType(FileUpload1.FileName)) Then

                Dim filePath As String = pathFile("Funding", "DataUploadFundingAgreement") & FileName & ".xls"
                FileUpload1.PostedFile.SaveAs(filePath)
            Else
                ShowMessage(lblMessage, "Upload File salah, harap perhatikan tipe file!", True)
                Exit Sub
            End If
        End If

    End Sub
    Function CheckFileType(ByVal fileName As String) As Boolean
        Dim ext As String = Path.GetExtension(fileName)

        Select Case ext.ToLower()
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select

    End Function


    Public Function GetNumber(ByVal EventType As String, ByVal FileName As String)

        Dim kode As String
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)


        Dim objReader As SqlDataReader

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()

        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "spUploadFile"
        objCommand.Connection = objConnection
        objCommand.Parameters.Add("@Login", SqlDbType.Char, 10).Value = Me.Loginid
        objCommand.Parameters.Add("@EventType", SqlDbType.Char, 1).Value = EventType
        objCommand.Parameters.Add("@FileName", SqlDbType.VarChar, 800).Value = FileName
        objCommand.Parameters.Add("@eID", SqlDbType.VarChar, 10)
        objCommand.Parameters("@eID").Direction = ParameterDirection.Output
        objReader = objCommand.ExecuteReader
        kode = objCommand.Parameters("@eID").Value
        objReader.Close()
        Return kode
    End Function

End Class
