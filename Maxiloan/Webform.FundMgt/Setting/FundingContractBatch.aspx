﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingContractBatch.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingContractBatch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingContractBatch</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingBatchView(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingBatchView.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function downloadSoftcopy(parAGU, parDEB, parKRE, parPRE) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/XML/' + parAGU)
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/XML/' + parDEB)
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/XML/' + parKRE)
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/XML/' + parPRE)
        }
    </script>
    <script language="javascript" type="text/javascript">
		
        function fConfirm() {
            if (window.confirm("Suku Margin Berubah, Apakah akan dilanjutkan dengan rescheduling ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function fclose() {
            window.close();
        }	
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR BATCH -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankName" runat="server" Width="192px">lblBankName</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblFundingCoyName" runat="server" Width="192px">lblFundingCoyName</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Fasilitas</label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractBatch" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="FUNDINGBATCHNO" HeaderText="NO BATCH">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkFundingBatchNo" runat="server" Text='<%# Container.dataitem("FundingBatchNo") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BATCHDATE" HeaderText="TGL BATCH" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
          
                             <asp:TemplateColumn  HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:label ID="lbljumlah" runat="server" Text='<%# Container.dataitem("PRINCIPALAMTTOFUNCOY") %>'></asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="JENIS MARGIN">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Enabled="True" Text='<%# Container.dataitem("InterestType") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="FINALMATURITYDATE" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="STATUS">
                                <HeaderStyle HorizontalAlign="CENTER"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Enabled="True" Text='<%# Container.dataitem("Status") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblExecute" runat="server" visible="false" Text='<%# Container.DataItem("IsExecute") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBatchExecute" runat="server" visible="false" Text='<%# Container.DataItem("isBatchExecute") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AccRealizedNum" HeaderText="# FASILITAS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="TYPE">
                                <HeaderStyle HorizontalAlign="CENTER"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Enabled="True" Text='<%# Container.dataitem("FacilityKind") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PlafoundReal" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblPlafoundReal" runat="server" Enabled="True" Text='<%# Container.DataItem("PlafoundReal") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EXECUTE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbExecute" runat="server" ImageUrl="../../Images/IconReceived.gif"
                                        CommandName="Execute" CausesValidation="False" OnClientClick="return confirm('Eksekusi batch?');"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SELEC">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbSelection" runat="server" ImageUrl="../../Images/Selection.png"
                                        CommandName="Selection" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SCOPY">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDraft" runat="server" ImageUrl="../../Images/IconDocument.gif"
                                        CommandName="Softcopy" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SPC">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbSpc" runat="server" ImageUrl="../../Images/pdf.png"
                                        CommandName="PengantarCair" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Upload">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbupload" runat="server" ImageUrl="../../Images/IconRequest.png"
                                        CommandName="upload" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="TTB">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbttb" runat="server" ImageUrl="../../Images/pdf.png"
                                        CommandName="TTB" CausesValidation="False" Visible="false"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Nom">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbNom" runat="server" ImageUrl="../../Images/pdf.png"
                                        CommandName="Nom" CausesValidation="False" Visible="false"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancelList" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_title_nobg">
                    <div class="form_single">
                        <label class="label_req">
                            No Batch</label>
                        <asp:TextBox ID="txtBatchNo" runat="server"  MaxLength="20"></asp:TextBox>
                        <asp:Label ID="lblBatchNo" runat="server" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Harap Pilih No Batch" ControlToValidate="txtBatchNo" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <%--<asp:TextBox runat="server" ID="txtBatchDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtBatchDate"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtBatchDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <%--<label class="label_req">--%>
                <label>
                    Jumlah</label>
                <%--<asp:TextBox ID="txtAmount" runat="server"  MaxLength="15"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                            ControlToValidate="txtAmount" DESIGNTIMEDRAGDROP="642" Type="Double" MinimumValue="0"
                            MaximumValue="99999999999999"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtAmount" DESIGNTIMEDRAGDROP="645"
                            CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                <uc1:ucnumberformat id="txtAmount" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Suku Margin</label>
                <asp:TextBox ID="txtInterestRate" runat="server" Width="80px"
                            MaxLength="10">0</asp:TextBox>
                        <asp:RangeValidator Visible="false" ID="RangeValidator2" runat="server" Display="Dynamic" ErrorMessage="Harap isi antara 0 - 100"
                            ControlToValidate="txtInterestRate" Type="Double" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>
                        <asp:RequiredFieldValidator Visible="false" ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            ErrorMessage="Harap isi antara 0 - 100" ControlToValidate="txtInterestRate" CssClass="validator_general"></asp:RequiredFieldValidator>
                <%--<uc1:ucnumberformat id="txtInterestRate" runat="server" />--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Margin</label>
                <asp:DropDownList ID="drdInterestType" runat="server">
                    <asp:ListItem Value="L" Selected="True">Floating</asp:ListItem>
                    <asp:ListItem Value="X">Fixed</asp:ListItem>
                    <asp:ListItem Value="B">Fixed per Batch</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <%--<div class="form_box">
            <div class="form_single">
                <label>
                    Tgl. Jatuh Tempo Final</label>
                <asp:TextBox runat="server" ID="txtFinalMaturityDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtFinalMaturityDate"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                <uc1:ucdatece id="txtFinalMaturityDate" runat="server"></uc1:ucdatece>
            </div>
        </div>--%>
        <div class="form_box">
                <div class="form_left">
                    <label class="label_req">>Periode Pencarian (Bulan)</label>
                    <asp:textbox runat="server" ID="txtPeriodeBulan" CssClass="small_text"  
                    onblur="extractNumber(this,2,true);blockInvalid(this);this.value=blankToZero(this.value);"
                    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                    onfocus="this.value=resetNumber(this.value);" AutoPostBack="true"></asp:textbox>
                </div>
                <div class="form_right">
                    <label class="label_req">
                        Periode Pencairan
                    </label>
                    <uc1:ucdatece id="txtPeriodFrom" runat="server"></uc1:ucdatece>
                    <label class="label_auto">
                        s/d
                    </label>
                    <uc1:ucdatece id="txtPeriodTo" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Pembayaran</label>
                <asp:DropDownList ID="drdPaymentScheme" runat="server" AutoPostBack="true" Width="370PX">
                    <%--<asp:ListItem Value="1" Selected="True">Principal &amp; Interest on Drawdown Date</asp:ListItem>
                    <asp:ListItem Value="2">Principal on Drawdown Date Interest on EoM</asp:ListItem>
                    <asp:ListItem Value="3">Principal &amp; Interest on Due Date</asp:ListItem>
                    <asp:ListItem Value="4">Principal &amp; Interest on EoM</asp:ListItem>
                    <asp:ListItem Value="5">Interest on Drawdown Date Principal on EoL</asp:ListItem>
                    <asp:ListItem Value="6">Principal on Drawdown Date Interest on Due Date</asp:ListItem>--%>
                    <asp:ListItem Value="1" Selected="True">Principal &amp; Interest on Drawdown Date</asp:ListItem>
                    <asp:ListItem Value="2">Principal on Drawdown Date Interest on Specific Date</asp:ListItem>
                    <asp:ListItem Value="3">Interest on Drawdown Date Principal on Specific Date</asp:ListItem>
                    <asp:ListItem Value="4">Principal &amp; Interest on EoM</asp:ListItem>
                    <asp:ListItem Value="7">Mirroring</asp:ListItem>
                </asp:DropDownList>
                <%--<asp:TextBox runat="server" ID="txtDueDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtDueDate"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtDueDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cara Pembayaran</label>
                <asp:DropDownList runat="server" ID="ddlCaraPembayaran">
                    <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="BATCH">Batch</asp:ListItem>
                    <asp:ListItem Value="JT">Jatuh Tempo</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                    InitialValue="" ErrorMessage="*" ControlToValidate="ddlCaraPembayaran" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <%--<div id="divDueDate" class="form_box" runat="server">
            <div class="form_single">
                <label>
                    Due Date</label>
                <asp:TextBox runat="server" ID="txtDueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtDueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jumlah Provisi</label>
                <%--<asp:TextBox ID="txtProvisionAmount" runat="server"  MaxLength="15">0</asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                            ControlToValidate="txtProvisionAmount" Type="Double" MinimumValue="0" MaximumValue="99999999999999"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtProvisionAmount"
                            CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                <uc1:ucnumberformat id="txtProvisionAmount" runat="server" /> %
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jumlah Admin</label>
                <%--<asp:TextBox ID="txtAdminAmount" runat="server"  MaxLength="15">0</asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator4" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                            ControlToValidate="txtAdminAmount" Type="Double" MinimumValue="0" MaximumValue="99999999999999"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtAdminAmount" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                <uc1:ucnumberformat id="txtAdminAmount" runat="server" />
            </div>
        </div>
        <div id="divTotalAccount" class="form_box" runat="server">
            <div class="form_single">
                <label>
                    Total Fasilitas
                </label>
                <asp:Label ID="lblTotalAccount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Mata Uang</label>
                <asp:DropDownList ID="drdCurrency" runat="server">
                    <asp:ListItem Value="IDR" Selected="True">Rupiah</asp:ListItem>
                    <asp:ListItem Value="USD">US Dollar</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Nikai Tukar</label>
                <%--<asp:TextBox ID="txtExchangeRate" runat="server" Width="100px"
                            MaxLength="10">1</asp:TextBox>--%>
                <uc1:ucnumberformat id="txtExchangeRate" runat="server" />
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label class="label_req">
                    Jangka Waktu</label>
                <%--<asp:TextBox ID="txtTenor" runat="server" Width="50px"  MaxLength="3"></asp:TextBox>
                        <asp:RequiredFieldValidator Visible="false" ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtTenor" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator Visible="false" ID="RangeValidator5" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan Angka"
                            ControlToValidate="txtTenor" Type="Double" MinimumValue="0" MaximumValue="999"
                            CssClass="validator_general"></asp:RangeValidator>--%>
                <%--<uc1:ucnumberformat id="txtTenor" runat="server" />--%>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Pola Bayar</label>
                <asp:DropDownList ID="drdInsPeriod" runat="server">
                    <asp:ListItem Value="1" Selected="True">Monthly</asp:ListItem>
                    <asp:ListItem Value="2">Bi Monthly</asp:ListItem>
                    <asp:ListItem Value="3">Quaterly</asp:ListItem>
                    <asp:ListItem Value="6">Semesterly</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Angsuran</label>
                <asp:DropDownList ID="drdInsScheme" runat="server">
                    <asp:ListItem Value="A" Selected="True">Fix Amount</asp:ListItem>
                    <asp:ListItem Value="P">Fix Principal</asp:ListItem>
                    <asp:ListItem Value="I">Irregular</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <%--Lokasi BPKB</label>--%>
                    Lokasi Jaminan</label>
                <asp:DropDownList ID="drdBPKBLocation" runat="server" Width="230px">
                    <asp:ListItem Value="H">Kantor Pusat</asp:ListItem>
                    <asp:ListItem Value="B">kantor Cabang</asp:ListItem>
                    <asp:ListItem Value="P">Kantor Pusat Funding Bank</asp:ListItem>
                    <asp:ListItem Value="C">Kantor Cabang Funding Bank</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Rekening Bank</label>
                <asp:DropDownList ID="cboBank" runat="server" Width="230px">
                </asp:DropDownList>
                <%--<label class="validator_general">
                            Please fill for OverDraft facility only</label>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Angsuran Pertama Batch</label>
                <asp:DropDownList ID="ddlFirstInstallment" runat="server">
                    <%--sudah tidak dipakai sebagai filter agreement selection, sekarang dijadikan dasar generate funding batch installment--%>
                    <%--<asp:ListItem Value="AX" Selected="True">Advance &amp; Arrear</asp:ListItem>--%>
                    <asp:ListItem Value="AR" Selected="True">Arrear</asp:ListItem>
                    <asp:ListItem Value="AD">Advance</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Pokok Hutang</label>
                <asp:DropDownList ID="ddlIsFundingNTF" runat="server">
                    <asp:ListItem Value="0" Selected="True">OS PRINCIPAL</asp:ListItem>
                    <asp:ListItem Value="1">NTF</asp:ListItem>
                    <asp:ListItem Value="2">OTR</asp:ListItem>
                    <asp:ListItem Value="3">NTF - INSTALLMENT</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perhitungan Margin</label>
                <asp:DropDownList ID="ddlHitungBUnga" runat="server">
                    <%--<asp:ListItem Value="ADVANCE" Selected="True">ADVANCE</asp:ListItem>
                    <asp:ListItem Value="ARREAR">ARREAR</asp:ListItem>
                    <asp:ListItem Value="ANUITAS">ANUITAS</asp:ListItem>
                    <asp:ListItem Value="MENURUN">MENURUN</asp:ListItem>
                    <asp:ListItem Value="TERM LOAN">TERM LOAN</asp:ListItem>--%>
                    <asp:ListItem Value="ANUITAS" Selected="True">ANUITAS</asp:ListItem>
                    <asp:ListItem Value="EFEKTIF">EFEKTIF</asp:ListItem>
                    <%-- <asp:ListItem Value="FLAT" >FLAT</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="gridRate"  runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general"
                        Width="45%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Tenor Dari" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTenorFrom" Enabled="false" runat="server" Text='<%# Container.DataItem("TenorFrom") %>' CssClass="small_text" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Tenor Sampai" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTenorTo"  Enabled="false" runat="server" Text='<%# Container.DataItem("TenorTo") %>' CssClass="small_text" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Rate" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtRate" runat="server" Text='<%# Container.DataItem("Rate") %>' CssClass="small_text" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div id="div3Btn" class="form_box_hide" runat="server">
            <div class="form_single">
                <asp:Button ID="btnAgreementSelection" runat="server" Text="Selection" CssClass="small buttongo blue">
                </asp:Button>
                <asp:Button ID="btnAddAgreement" runat="server" Text="Add" CssClass="small buttongo blue" Visible="false">
                </asp:Button>
                <asp:Button ID="btnExecution" runat="server" Text="Execution" CssClass="small buttongo blue">
                </asp:Button>
                <asp:Button ID="btnDraft" runat="server" Text="Draft Execution" CssClass="small buttongo blue" Visible="false">
                </asp:Button>
            </div>
        </div>
        
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:Label ID="lblBatchNoV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Pencairan</label>
                <asp:Label ID="lblAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Pencairan</label>
                <asp:Label ID="lblDateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Suku Margin</label>
                <asp:Label ID="lblInterestRateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Jatuh Tempo Final</label>
                <asp:Label ID="lblFinalMaturityDateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Provisi</label>
                <asp:Label ID="lblProvisionAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Admin</label>
                <asp:Label ID="lblAdminAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Mulai Dari Tanggal</label>
                <%--<asp:TextBox runat="server" ID="txtStartFrom"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender4" TargetControlID="txtStartFrom"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtStartFrom" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnOk" runat="server" CausesValidation="False" Text="OK" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancelbawah" runat="server" OnClientClick="fclose();" CausesValidation="False"
                Text="Cancel" CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDownload">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOWNLOAD FILE
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgSoftCopy" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general"
                        Width="65%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NAMA FUNDING BANK"  SortExpression="BankName" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" runat="server" Enabled="True" Text='<%# Container.DataItem("BankName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA FILE"  SortExpression="SPName" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSPName" runat="server" Enabled="True" Text='<%# Container.DataItem("SPName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JENIS FILE"  SortExpression="FileType" >
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFileType" runat="server" Enabled="True" Text='<%# Container.DataItem("FileType") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DOWNLOAD">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDraft" runat="server" ImageUrl="../../Images/IconDocument.gif"
                                        CommandName="Download" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>        
        <div class="form_button">
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
