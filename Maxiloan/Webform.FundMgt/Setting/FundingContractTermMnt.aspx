﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingContractTermMnt.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingContractTermMnt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingContractTermMnt</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
<!--
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }
        //-->

        function checkSelected(inpObj, varSelected, varIndexSelected) {

            var arrData = "";
            var arrIndexData = "";
            var blCondition = false;
            intDataSelected = inpObj.selectedIndex;

            if (inpObj.options[intDataSelected].selected == true) {
                if (varSelected.value != "") {
                    split_arrIndexData = varIndexSelected.value.split(",");
                    split_arrData = varSelected.value.split(",");
                    for (i = 0; i < split_arrIndexData.length; i++) {
                        if (parseInt(intDataSelected) == parseInt(split_arrIndexData[i])) {
                            //kalo yg di-select sudah pernah di-select(ada di arrIndexData, hilangkan select-nya
                            inpObj.options[split_arrIndexData[i]].selected = false;
                            blCondition = true;
                        }
                        else {
                            inpObj.options[split_arrIndexData[i]].selected = true;
                        }
                    }
                    if (blCondition == false) {
                        //tambahkan item ini ke variabel arrData
                        arrData = varSelected.value + "," + inpObj.options[intDataSelected].value;
                        arrIndexData = varIndexSelected.value + "," + parseInt(intDataSelected);
                    }
                    else {
                        //kurangi dari arrData & varSelected
                        for (j = 0; j < split_arrIndexData.length; j++) {
                            if (parseInt(intDataSelected) != parseInt(split_arrIndexData[j])) {
                                if (arrData == "") {
                                    arrData = split_arrData[j];
                                    arrIndexData = split_arrIndexData[j];
                                }
                                else {
                                    arrData = arrData + "," + split_arrData[j];
                                    arrIndexData = arrIndexData + "," + split_arrIndexData[j];
                                }
                            }
                        }
                    }
                    varSelected.value = arrData;
                    varIndexSelected.value = arrIndexData;
                }
                else {
                    varSelected.value = inpObj.options[intDataSelected].value;
                    varIndexSelected.value = intDataSelected;
                }
            }
        }
        /*
        function addSrcToDestList(srcList,destList,varSelectedFrom,varIndexSelectedFrom){
        var len=destList.length;
        var lenSrc=0;
        var objSelected=varIndexSelectedFrom.value;
        var arrIndexNew=objSelected.split(",");
        var hit
	
        hit=false;
	
	
        var i=0;
    while(i<(srcList.length +1)){
        if (hit){
        hit=false;
        i--;
        }		
        if ((srcList.options[i] != null) && (srcList.options[i].selected)) {		    
        hit=true;
			
        destList.options[len]=new Option(srcList.options[i].text);
        destList.options[len].value = srcList.options[i].value;
        len++;
        srcList.options[i]=null;	
        }
        i++;
        }
	
        varSelectedFrom.value="";
        varIndexSelectedFrom.Value="";
        }
        */

        function addSrcToDestList(srcList, destList) {

            var j = destList.length;
            var scrSelectIndex = ""

            for (var i = 0; i < srcList.length; i++) {
                if ((srcList.options[i] != null) && (srcList.options[i].selected)) {
                    destList.options[j] = new Option(srcList.options[i].text);
                    destList.options[j].value = srcList.options[i].value;
                    j++;
                    if (scrSelectIndex == "") {
                        scrSelectIndex += i;
                    }
                    else {
                        scrSelectIndex += "," + i;

                    }
                }
            }
            var scrSelectArr = scrSelectIndex.split(",");
            //if (scrSelectArr != 0) {
            for (var i = 0; i <= scrSelectArr.length; i++) {
                srcList.options[scrSelectArr[i] - i] = null;
            }
            //}
        }


        function doSubmitAdd(destList1, destList2, pilihan1, pilihan2) {
            if (typeof (document.all.lstIncludeRight) != 'undefined') {
                for (var i = 0; i < destList1.length; i++) {
                    pilihan1.value = pilihan1.value + "''" + destList1.options[i].value;

                    if (i + 1 != destList1.length) {
                        pilihan1.value = pilihan1.value + "'',";
                    }
                }
                pilihan1.value = pilihan1.value + "''";

                if (pilihan1.value == "''") {
                    pilihan1.value = "''''";
                }


            }
            if (typeof (document.all.lstExcludeRight) != 'undefined') {
                pilihan2.value = "";
                for (i = 0; i < destList2.length; i++) {
                    pilihan2.value = pilihan2.value + "''" + destList2.options[i].value;

                    if (i + 1 != destList2.length) {
                        pilihan2.value = pilihan2.value + "'',";
                    }
                }
                pilihan2.value = pilihan2.value + "''";

                if (pilihan2.value == "''") {
                    pilihan2.value = pilihan2.value + "''";
                }

            }

            document.all.saveadd.value = "saveadd";
            document.frmindustry.submit();

        }


        //addSrcToDestList(ListBox Source, ListBox Destination, varSelectedSource, varSelectedIndexSource)
        function doSubmitEdit(destList1, destList2, pilihan1, pilihan2) {
            if (typeof (document.all.lstIncludeRight) != 'undefined') {
                for (var i = 0; i < destList1.length; i++) {
                    pilihan1.value = pilihan1.value + "'" + destList1.options[i].value;

                    if (i + 1 != destList1.length) {
                        pilihan1.value = pilihan1.value + "',";
                    }
                }
                pilihan1.value = pilihan1.value + "'";

                if (pilihan1.value == "'") {
                    pilihan1.value = pilihan1.value + "'";
                }

            }
            if (typeof (document.all.lstExcludeRight) != 'undefined') {
                pilihan2.value = "";
                for (i = 0; i < destList2.length; i++) {
                    pilihan2.value = pilihan2.value + "'" + destList2.options[i].value;

                    if (i + 1 != destList2.length) {
                        pilihan2.value = pilihan2.value + "',";
                    }
                }
                pilihan2.value = pilihan2.value + "'";

                if (pilihan2.value == "'") {
                    pilihan2.value = pilihan2.value + "'";
                }

            }

            document.all.saveedit.value = "saveedit";
            document.frmindustry.submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" name="pilihan1" /><input type="hidden" name="pilihan2" />
    <input type="hidden" name="saveedit" /><input type="hidden" name="saveadd" />
    <asp:Label runat="server" id="lblMessage" Visible="false"></asp:Label>  
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR SYARAT DAN KONDISI
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblTitleFundingCoyID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="lblTitleFundingCompanyName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Facility/Kontrak</label>
                <asp:Label ID="lblTitleFundingContractID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Facility/Kontrak</label>
                <asp:Label ID="lblTitleContractName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgTC" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="SyaratID" OnItemCommand="ShowView" BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:ButtonColumn DataTextField="SyaratID" SortExpression="SyaratID" HeaderText="ID"
                                CommandName="ShowView">
                                <HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:ButtonColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="40px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEditRec" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CausesValidation="False" CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <HeaderStyle Width="50px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDel" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL PAGING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" CommandName="First" Width="25px"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" CssClass="InpType" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                        MinimumValue="1" ControlToValidate="txtPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="Page No. is not valid" ControlToValidate="txtPage"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s) </td> </tr>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <!--Panel Add/Edit ------------------------------------>
    <asp:Panel ID="pnlAddEdit" runat="server">        
        <div class="form_title">
            <div class="form_single">
                <h4>
                    SYARAT dan KONDISI -&nbsp;
                    <asp:Label ID="lblAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Syarat</label>
                <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" DataTextField="Description"
                    DataValueField="SyaratID">
                </asp:DropDownList>
                <asp:Label ID="lblSyaratID" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlAddTypeR" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        From</label>
                    <asp:TextBox ID="txtAddFrom" runat="server" Width="104px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="This field can not be empty" ControlToValidate="txtAddFrom" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="Dynamic" ErrorMessage="This field must >= 0"
                        ControlToValidate="txtAddFrom" ValueToCompare="0" Type="Double" Operator="GreaterThan" CssClass="validator_general" ></asp:CompareValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                  <label class ="label_req">
                        To</label>
                    <asp:TextBox ID="txtAddTo" runat="server" Width="104px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        ErrorMessage="This field can not be empty" ControlToValidate="txtAddTo" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="Dynamic" ErrorMessage="This field must >= 0"
                        ControlToValidate="txtAddTo" ValueToCompare="0" Type="Double" Operator="GreaterThanEqual" CssClass="validator_general" ></asp:CompareValidator>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ErrorMessage="Field To must be >= field From"
                        ControlToValidate="txtAddTo" Type="Double" Operator="GreaterThanEqual" ControlToCompare="txtAddFrom" CssClass="validator_general" ></asp:CompareValidator>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlAddTypeNU" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        From New</label>
                    <asp:TextBox ID="TxtFromNew" runat="server" Width="104px">0</asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtFromNew" runat="server" Display="Dynamic" ErrorMessage="This field can not be empty"
                        ControlToValidate="TxtFromNew" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        To New</label>
                    <asp:TextBox ID="TxtToNew" runat="server" Width="104px">0</asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtToNew" runat="server" Display="Dynamic" ErrorMessage="This field can not be empty"
                        ControlToValidate="TxtToNew" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="ComVTxtToNew" runat="server" ErrorMessage="Field To must be >= field From"
                        ControlToValidate="TxtToNew" Type="Double" Operator="GreaterThanEqual" ControlToCompare="TxtFromNew"></asp:CompareValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        From Used</label>
                    <asp:TextBox ID="TxtFromUsed" runat="server" Width="104px">0</asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtFromUsed" runat="server" Display="Dynamic"
                        ErrorMessage="This field can not be empty" ControlToValidate="TxtFromUsed" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        To Used</label>
                    <asp:TextBox ID="TxtToUsed" runat="server" Width="104px">0</asp:TextBox>
                    <asp:RequiredFieldValidator ID="RVTxtToUsed" runat="server" Display="Dynamic" ErrorMessage="This field can not be empty"
                        ControlToValidate="TxtToUsed" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompVTxtToUsed" runat="server" ErrorMessage="Field To must be >= field From"
                        ControlToValidate="TxtToUsed" Type="Double" Operator="GreaterThanEqual" ControlToCompare="TxtFromUsed"></asp:CompareValidator>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddTypeCS" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Include</label>
                    <asp:DropDownList ID="ddlListIncludeCS" runat="server" Width="168px">
                    </asp:DropDownList>
                    <asp:Label ID="errorCS" runat="server" ForeColor="Red">You only may select Include field or Exclude field</asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Exclude</label>
                    <asp:DropDownList ID="ddlListExcludeCS" runat="server" Width="168px">
                    </asp:DropDownList>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddTypeCM" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_left">
                </div>
                <div class="form_right">
                    <label>
                        Available List
                    </label>
                    <label>
                        Selected List
                    </label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Include
                    </label>
                </div>
                <div class="form_right">
                    <asp:ListBox ID="lstIncludeLeft" runat="server" Width="200px" SelectionMode="Multiple"
                        Height="200px"></asp:ListBox>
                    <input style="width: 24px; height: 22px" class="pBttn" onclick="addSrcToDestList(window.document.forms[0].lstIncludeRight,window.document.forms[0].lstIncludeLeft)"
                        value="<" type="button" name="btnRemove" />
                    <input style="width: 24px; height: 22px" class="pBttn" onclick="addSrcToDestList(window.document.forms[0].lstIncludeLeft,window.document.forms[0].lstIncludeRight)"
                        value=">" type="button" name="btnAdd" />
                    <asp:ListBox ID="lstIncludeRight" runat="server" Width="200px" SelectionMode="Multiple"
                        Height="200px"></asp:ListBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                </div>
                <div class="form_right">
                    <label>
                        Available List
                    </label>
                    <label>
                        Selected List
                    </label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Exclude
                    </label>
                </div>
                <div class="form_right">
                    <asp:ListBox ID="lstExcludeLeft" runat="server" Width="200px" SelectionMode="Multiple"
                        Height="200px"></asp:ListBox>
                    <input style="width: 24px; height: 22px" class="pBttn" onclick="addSrcToDestList(window.document.forms[0].lstExcludeRight,window.document.forms[0].lstExcludeLeft)"
                        value="<" type="button" name="btnRemoveEx" />
                    <input style="width: 24px; height: 22px" class="pBttn" onclick="addSrcToDestList(window.document.forms[0].lstExcludeLeft,window.document.forms[0].lstExcludeRight)"
                        value=">" type="button" name="btnAddEx" />
                    <asp:ListBox ID="lstExcludeRight" runat="server" Width="200px" SelectionMode="Multiple"
                        Height="200px"></asp:ListBox>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddTypeN1" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_left">
                   <label class ="label_req">
                        Parameter
                    </label>
                </div>
                <div class="form_right">
                    <asp:TextBox ID="txtAddParameter" runat="server" Width="104px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                        ErrorMessage="This field can not be empty" ControlToValidate="txtAddParameter" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator5" runat="server" Display="Dynamic" ErrorMessage="This field must >= 0"
                        ControlToValidate="txtAddParameter" ValueToCompare="0" Type="Double" Operator="GreaterThan" CssClass="validator_general" ></asp:CompareValidator>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddTypeN2" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_left">
                <label class ="label_req">
                        Parameter N1
                    </label>
                </div>
                <div class="form_right">
                    <asp:TextBox ID="txtAddParameterN1" runat="server" Width="104px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                        ErrorMessage="This field can not be empty" ControlToValidate="txtAddParameterN1" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator6" runat="server" Display="Dynamic" ErrorMessage="This field must >= 0"
                        ControlToValidate="txtAddParameterN1" ValueToCompare="0" Type="Double" Operator="GreaterThan" CssClass="validator_general" ></asp:CompareValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class ="label_req">
                        Parameter N2
                    </label>
                </div>
                <div class="form_right">
                    <asp:TextBox ID="txtAddParameterN2" runat="server" Width="104px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                        ErrorMessage="This field can not be empty" ControlToValidate="txtAddParameterN2" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator7" runat="server" Display="Dynamic" ErrorMessage="This field must >= 0"
                        ControlToValidate="txtAddParameterN2" ValueToCompare="0" Type="Double" Operator="GreaterThan" CssClass="validator_general" ></asp:CompareValidator>
                </div>
            </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="btnSaveAdd" runat="server" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnSaveEdit" runat="server" Text="Edit" CssClass="small button blue">
            </asp:Button>
            <asp:Panel ID="pnlBtnSave" runat="server" Visible="False">
                <input title="Save" onclick="doSubmitAdd(document.all.lstIncludeRight,document.all.lstExcludeRight,document.all.pilihan1,document.all.pilihan2);"
                    value="SAVE" src="..\..\images\buttonsave.gif" type="image" name="btnSaveAdd" />
            </asp:Panel>
            <asp:Panel ID="pnlBtnEdit" runat="server" Visible="False">
                <input title="Save" onclick="doSubmitEdit(document.all.lstIncludeRight,document.all.lstExcludeRight,document.all.pilihan1,document.all.pilihan2);"
                    value="SAVE" src="..\..\images\buttonsave.gif" type="image" name="btnSaveEdit" />
            </asp:Panel>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <!--Panel View------------------------------------------------>
    <asp:Panel ID="pnlView" Visible="False" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW - SYARAT dan KONDISI
                </h4>
            </div>
        </div>
        <asp:Panel ID="pnlViewTypeR" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        ID Syarat</label>
                    <asp:Label ID="lblDetailSyaratID" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Syarat dan Kondisi</label>
                    <asp:Label ID="lblDetailDescription" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        From</label>
                    <asp:Label ID="lblDetailFrom" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        To</label>
                    <asp:Label ID="lblDetailTo" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlViewTypeNU" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        From New</label>
                    <asp:Label ID="LblDetailFromNew" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        To New</label>
                    <asp:Label ID="LblDetailToNew" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        From Used</label>
                    <asp:Label ID="LblDetailFromUsed" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        To Used</label>
                    <asp:Label ID="LblDetailToUsed" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlViewTypeCS" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Include</label>
                    <asp:Label ID="lblDetailInclude" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Exclude</label>
                    <asp:Label ID="lblDetailExclude" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlViewTypeN1" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Parameter</label>
                    <asp:Label ID="lblDetailParameter" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlViewTypeN2" runat="server" Visible="False">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Parameter N1</label>
                    <asp:Label ID="lblDetailParameterN1" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Parameter N2</label>
                    <asp:Label ID="lblDetailParameterN2" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="BtnViewOK" runat="server" Text="OK" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
