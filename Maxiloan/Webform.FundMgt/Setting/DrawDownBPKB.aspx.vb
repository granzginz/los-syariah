﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DrawDownBPKB
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim oFundingContractBatch As New Parameter.FundingContractBatch
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString

        PnlSearchDetail.Visible = True
        pnlTop.Visible = False
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        PnlSearchDetail.Visible = True
        pnlTop.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        PnlSearchDetail.Visible = True
        pnlTop.Visible = False
        pnlList.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        PnlSearchDetail.Visible = True
        pnlTop.Visible = False
        pnlList.Visible = True
    End Sub

#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property
    Private Property PrintType() As String
        Get
            Return CStr(viewstate("PrintType"))
        End Get
        Set(ByVal Value As String)
            viewstate("PrintType") = Value
        End Set
    End Property

#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlTop.Visible = True
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlTop.Visible = True
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        InitialDefaultPanel()
        If Not Me.IsPostBack Then


            drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            Me.FormID = "DRAWBPKBBACK"

            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            'If Request.QueryString("strFileLocation") <> "" Then
            '    Dim strHTTPServer As String
            '    Dim StrHTTPApp As String
            '    Dim strNameServer As String
            '    Dim strFileLocation As String

            '    strHTTPServer = Request.ServerVariables("PATH_INFO")
            '    strNameServer = Request.ServerVariables("SERVER_NAME")
            '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            '    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
            '    '& "history.back(-1) " & vbCrLf _
            '    'Call file PDF 
            '    Response.Write("<script language = javascript>" & vbCrLf _
            '    & "window.open('" & strFileLocation & "','channeling', 'left=0, top=0, width=600, height=800, menubar=0, scrollbars=yes') " & vbCrLf _
            '    & "</script>")

            'End If
            Me.SearchBy = ""
            Me.SortBy = ""

            bindComboCompany()
            Response.Write(GenerateScript(getComboContract, getComboBatchDate))

        End If
    End Sub

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        Dim ChkBox As CheckBox
        Dim intLoopGrid As Integer
        Dim lblAssetDocStatusID As Label
        With oContract
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingDrowDownBPKBList"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        For intLoopGrid = 0 To dtgFundingContractList.Items.Count - 1
            ChkBox = CType(dtgFundingContractList.Items(intLoopGrid).Cells(0).FindControl("chkPrint"), CheckBox)
            lblAssetDocStatusID = CType(dtgFundingContractList.Items(intLoopGrid).Cells(0).FindControl("lblAssetDocStatusID"), Label)
            If Me.PrintType = "All" Then
                ChkBox.Enabled = False
            Else
                ChkBox.Enabled = True
            End If
            If lblAssetDocStatusID.Text = "G" Then
                ChkBox.Checked = True
                ChkBox.Enabled = True
            Else
                ChkBox.Checked = False
                ChkBox.Enabled = False
            End If

        Next
        drdCompany.SelectedIndex = 0
        Response.Write(GenerateScript(getComboContract, getComboBatchDate))
        PagingFooter()
    End Sub
#End Region
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf

        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region
#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContract"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            showmessage(lblmessage, e.message , true)
        End Try

    End Function
#End Region
#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
           showmessage(lblmessage, e.message , true)
        End Try

    End Function
#End Region
#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception
         showmessage(lblmessage, e.message , true)
        End Try

    End Sub
#End Region

    Protected Function drdCompanyChange() As String

        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"

    End Function

    Protected Function drdContractChange() As String

        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"

    End Function

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim CmdWhere As String
        Me.SearchBy = ""
        Me.SortBy = ""
        CmdWhere = ""
        If drdCompany.SelectedItem.Value.Trim <> "All" Then
            Me.SearchBy = "FundingBPKBToDraw.FundingCoyID='" & drdCompany.SelectedItem.Value.Trim & "' and "
            lblFundingCoy.Text = drdCompany.SelectedItem.Text.Trim
        End If
        If tempChild.Value.Trim = "" Or tempChild.Value.Trim = "0" Then
        Else
            Me.SearchBy = Me.SearchBy + "FundingBPKBToDraw.FundingContractNO= '" & tempChild.Value.Trim & "' and "
            LblFundingContractNo.Text = tempChild.Value.Trim
            If tempChild2.Value.Trim = "" Or tempChild2.Value.Trim = "0" Then
            Else
                Me.SearchBy = Me.SearchBy + "FundingBPKBToDraw.FundingBatchNo = '" & tempChild2.Value.Trim & "' And "
                LblFundingBatch.Text = tempChild2.Value.Trim
            End If
        End If
        If cboPrintedStatus.SelectedValue.Trim <> "All" Then
            If cboPrintedStatus.SelectedValue.Trim = "P" Then
                Me.SearchBy = Me.SearchBy + "FundingBPKBtoDraw.PrintedStatus = '" & cboPrintedStatus.SelectedValue.Trim & "' and "
            Else
                Me.SearchBy = Me.SearchBy + "FundingBPKBtoDraw.PrintedStatus <> '" & cboPrintedStatus.SelectedValue.Trim & "' and "
            End If
        End If
        LblPrintedStatus.Text = cboPrintedStatus.SelectedItem.Text.Trim
        If txtSearchBy.Text.Trim <> "" Then
            If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy = Me.SearchBy + " Agreement.AgreementNo Like '" & txtSearchBy.Text.Trim & "'  and "
            Else
                Me.SearchBy = Me.SearchBy + " Agreement.AgreementNo = '" & txtSearchBy.Text.Trim & "' and "
            End If
        End If
        If cboDocStatus.SelectedValue.Trim <> "All" Then
            If Me.SearchBy <> "" Then
                CmdWhere = CmdWhere + " and AssetDocumentContent.AssetDocStatus = '" & cboDocStatus.SelectedValue.Trim & "'"
            Else
                CmdWhere = CmdWhere + " AssetDocumentContent.AssetDocStatus = '" & cboDocStatus.SelectedValue.Trim & "'"
            End If

        End If
        If Me.SearchBy.Trim <> "" Then
            Me.SearchBy = Left(Me.SearchBy, Len(Me.SearchBy.Trim) - 4)
        End If
        Me.PrintType = rdoPrintType.SelectedItem.Value.Trim
        LblPrintType.Text = Me.PrintType.Trim
        LblDocStatus.Text = cboDocStatus.SelectedItem.Text.Trim
        BindGridEntity(Me.SearchBy + CmdWhere.Trim, Me.SortBy)
        PnlSearchDetail.Visible = True
        pnlTop.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "channeling" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "channeling" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"

        End If
    End Sub

    Private Sub btnPrintbawah_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintBawah.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim lblApplicationID As New Label
            Dim lblBranchID As New Label
            Dim hasil As Integer
            Dim cmdwhere As String
            Dim strMultiApplicationID As String = ""
            Dim strMultiBranch As String = ""
            If Me.PrintType = "Page" Then
                With oDataTable
                    .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                    .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
                End With
                For intloop = 0 To dtgFundingContractList.Items.Count - 1
                    chkPrint = CType(dtgFundingContractList.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                    lblApplicationID = CType(dtgFundingContractList.Items(intloop).Cells(1).FindControl("lblApplicationID"), Label)
                    lblAgreementNo = CType(dtgFundingContractList.Items(intloop).Cells(2).FindControl("lnkAgreementNo"), HyperLink)
                    lblBranchID = CType(dtgFundingContractList.Items(intloop).Cells(11).FindControl("lblBranchID"), Label)
                    If chkPrint.Checked Then
                        oRow = oDataTable.NewRow
                        oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                        oRow("ApplicationID") = lblApplicationID.Text.Trim
                        oDataTable.Rows.Add(oRow)
                        strMultiBranch = lblBranchID.Text.Trim
                        If strMultiApplicationID.Trim = "" Then
                            'cmdwhere = "Agreement.ApplicationID='" & CType(oRow("ApplicationID"), String) & "'"
                            strMultiApplicationID = strMultiApplicationID + "'" + lblApplicationID.Text.Trim + "'"
                        Else
                            'cmdwhere = cmdwhere & " or Agreement.ApplicationID='" & CType(oRow("applicationID"), String) & "' "
                            strMultiApplicationID = strMultiApplicationID + ",'" + lblApplicationID.Text.Trim + "'"
                        End If
                    End If
                Next
                cmdwhere = "Agreement.ApplicationID in (" + strMultiApplicationID + ")"
                If oDataTable.Rows.Count = 0 Then

                    ShowMessage(lblMessage, "Harap Periksa Item", True)
                    drdCompany.SelectedIndex = 0
                    Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                    Exit Sub
                End If
            Else
                If Me.SearchBy <> "" Then
                    cmdwhere = Me.SearchBy.Trim + " and AssetDocumentContent.assetDocStatus = 'G' "
                Else
                    cmdwhere = Me.SearchBy.Trim + " AssetDocumentContent.assetDocStatus = 'G' "
                End If

            End If
            Dim cookie As HttpCookie = Request.Cookies("Reports")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("MultiApplicationID") = strMultiApplicationID
                cookie.Values("BranchID") = strMultiBranch
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("Reports")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("MultiApplicationID", strMultiApplicationID)
                cookieNew.Values.Add("BranchID", strMultiBranch)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("DrawDownBPKBViewer.aspx")
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("DrawDownBPKB.aspx")
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtContract As DataTable = getComboContract()
        Dim i As Integer

        For i = 0 To DtContract.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdContract.UniqueID, CStr(DtContract.Rows(i).Item("FundingContractNo")).Trim)
        Next

        RegisterFirstItem(drdContract)

        Dim DtBatchDate As DataTable = getComboBatchDate()
        Dim j As Integer

        For j = 0 To DtBatchDate.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(drdBatchDate.UniqueID, CStr(DtBatchDate.Rows(j).Item("FundingBatchNo")).Trim)
        Next
        RegisterFirstItem(drdBatchDate)

        MyBase.Render(writer)
    End Sub

    Private Sub RegisterFirstItem(ByVal ddl As DropDownList)
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "0")
        Page.ClientScript.RegisterForEventValidation(ddl.UniqueID, "All")
    End Sub

End Class