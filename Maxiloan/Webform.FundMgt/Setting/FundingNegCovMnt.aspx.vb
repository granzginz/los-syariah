﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingNegCovMnt
    Inherits Maxiloan.Webform.WebBased

#Region "Private Const & declaration variable"
    Private Command As String
    Private m_Controler As New FundingCompanyController
    Private m_Error As Boolean = False
#End Region

#Region "Property"
    Private Property Covinient() As String
        Get
            Return CStr(viewstate("Covinient"))
        End Get
        Set(ByVal Value As String)
            viewstate("Covinient") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        InitialDefaultPanel()
        If Not Me.IsPostBack Then
            Me.FormID = "FundingContract"
            'Me.IsReschedule = False
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("Command") <> "" Then Command = Request.QueryString("Command")
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            BindNegCov()
        End If
        BindTitleTop()
    End Sub

#Region "BindTitleTop"
    Private Sub BindTitleTop()
        '--------Header Part
        lblBankName.Text = Me.BankName
        lblFundingCoyName.Text = Me.CompanyName
        lblFundingContractNo.Text = Me.FundingContractNo
        lblContractName.Text = Me.ContractName
        '--------Edit Part
        lblBankNameEdit.Text = Me.BankName
        lblFundingCoyNameEdit.Text = Me.CompanyName
        lblFundingContractNoEdit.Text = Me.FundingContractNo
        lblContractNameEdit.Text = Me.ContractName
    End Sub
#End Region

#Region "Panel "
    Private Sub AllPanelHide()
        pnlHead.Visible = False
        pnlEdit.Visible = False
    End Sub

    Private Sub InitialDefaultPanel()
        AllPanelHide()
        pnlHead.Visible = True
    End Sub
#End Region

#Region "Private Sub And Function"

    Private Sub BindNegCov()

        Dim customClass As New Parameter.FundingContract
        Dim sType As String
        Dim dttFundingContractNegCov As DataTable
        Try
            With customClass
                .strConnection = GetConnectionString
                .FundingCoyId = Me.BankID
                .FundingContractNo = Me.FundingContractNo
            End With
            dttFundingContractNegCov = m_Controler.FundingContractNegCovGet(customClass).ListData
            With dttFundingContractNegCov.Rows(0)
                lblCov1N1.Text = GetIfNull(.Item("NegativeCov1N1").ToString.Trim)
                lblCov1N2.Text = GetIfNull(.Item("NegativeCov1N2").ToString.Trim)
                LblCov3N1.Text = GetIfNull(.Item("NegativeCov3N1").ToString.Trim)
            End With
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateNegCov()
        Dim SetField As String
        Dim WhereBy As String
        Dim cov1 As Integer
        Dim cov2 As Integer
        If txtN1.Text.Trim = "" Then
            cov1 = 0
        Else
            cov1 = CType(txtN1.Text.Trim, Integer)
        End If
        If txtN2.Text.Trim = "" Then
            cov2 = 0
        Else
            cov2 = CType(txtN2.Text.Trim, Integer)
        End If


        If Me.Covinient = "COV1" Then
            SetField = "NegativeCov1N1 = " & cov1 & " , NegativeCov1N2 = " & cov2
        Else
            SetField = "NegativeCov3N1 = " & cov1
        End If


        WhereBy = " FundingCoyID  = '" & Me.BankID & "' And  FundingContractNo = '" & Me.FundingContractNo & "'"

        Try
            m_Controler.FundingContractNegCovUpdate(GetConnectionString, SetField, WhereBy)

        Catch ex As Exception
            m_Error = True
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Protected Function GetIfNull(ByVal field As String) As String
        Return IIf(field = "0", "", field).ToString
    End Function
#End Region

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
    End Sub

    Private Sub imbEdit1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbEdit1.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
            AllPanelHide()
            Me.Covinient = "COV1"
            pnlEdit.Visible = True
            lblTerm.Text = "Ovd N1 hari pada N2 Ang Pertama"
            txtN1.Text = lblCov1N1.Text
            txtN2.Visible = True
            txtN2.Text = lblCov1N2.Text
            pnlN2.Visible = True
        End If
    End Sub

    Private Sub imbEdit2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbEdit2.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
            AllPanelHide()
            Me.Covinient = "COV3"
            pnlEdit.Visible = True
            lblTerm.Text = "OVD days > N1 days"
            txtN1.Text = LblCov3N1.Text
            txtN2.Visible = False
            pnlN2.Visible = False
        End If
    End Sub



    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        UpdateNegCov()
        If Not (m_Error) Then

            InitialDefaultPanel()
            BindNegCov()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("FundingNegCovMnt.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & lblContractName.Text.Trim)
    End Sub

End Class