﻿Imports System.Data
Imports System.Data.SqlClient

Public Class FundingContractTermMnt
    Inherits Maxiloan.Webform.WebBased

    Dim conIndType As SqlConnection
    Dim cmdIndType As SqlCommand
    Dim dsIndType As DataSet
    Dim GObjStrSelectEdit As String
    Dim GObjStrSelectView As String
    Dim GObjStrSelectAdd As String
#Region "Property"
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property
#End Region

#Region " PageLoad+Connection "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        

        errorCS.Visible = False

        'GObjStrSelectEdit = "select FundingContractNo," & _
        '                "FundingContractTerm.SyaratID," & _
        '                "Description," & _
        '                "ContentInc," & _
        '                "ContentExc," & _
        '                "RangeFrom," & _
        '                "RangeTo," & _
        '                "FlagEntry, " & _
        '                "TypeOfContent, " & _
        '                "SQLCmd, " & _
        '                "SQLCmdLookUp, " & _
        '                "FlagFixedVar " & _
        '                "from FundingContractTerm " & _
        '                "inner join FundingMasterTerm on FundingContractTerm.SyaratID=FundingMasterTerm.SyaratID " & _
        '                "where FundingContractNo = '" & Request.QueryString("fundingcontractid") & "' "

        GObjStrSelectEdit = "select FundingContractNo," & _
                        "FundingContractTerm.SyaratID," & _
                        "Description," & _
                        "ContentInc," & _
                        "ContentExc," & _
                        "RangeFrom," & _
                        "RangeTo," & _
                        "RangeFromUsed," & _
        "RangeToUsed," & _
                        "FlagEntry, " & _
                        "TypeOfContent, " & _
                        "SQLCmd, " & _
                        "SQLCmdLookUp, " & _
                        "FlagFixedVar " & _
                        "from FundingContractTerm " & _
                        "inner join FundingMasterTerm on FundingContractTerm.SyaratID=FundingMasterTerm.SyaratID " & _
                        "where FundingContractNo = '" & Request.QueryString("fundingcontractid") & "' "

        lblTitleFundingContractID.Text = Request.QueryString("fundingcontractid")

        'If Request("page") = "TermCondition" Then
        GObjStrSelectView = "select FundingContractNo," & _
            "FundingContractTerm.SyaratID," & _
            "Description " & _
            "from FundingContractTerm " & _
            "inner join FundingMasterTerm on FundingContractTerm.SyaratID=FundingMasterTerm.SyaratID " & _
            "where FundingContractNo = '" & Request.QueryString("fundingcontractid") & "' " & _
            " and FlagFixedVar<>'F' and FlagAcqCov<>'C' and FlagEntry='C' "

        '" and FlagFixedVar<>'F' and FlagAcqCov<>'C' and FlagEntry='" & IIf(Request.QueryString("Status") = "C", "C", "O") & "' "

        GObjStrSelectAdd = "select SyaratID,Description,TypeOfContent " & _
                            "from FundingMasterTerm " & _
                            "where FlagFixedVar<>'F' and FlagAcqCov='A' and (FlagCS='A' or FlagCS=(select FlagCS from FundingContract where FundingContractNo='" & Request.QueryString("fundingcontractid") & "')) " & _
                            "and SyaratID not in (select SyaratID from FundingContractTerm where FundingContractNo='" & Request.QueryString("fundingcontractid") & "') " & _
                            "order by SequenceNum asc "
        'ElseIf Request("page") = "NegCovenant" Then
        '    GObjStrSelectView = "select FundingContractNo," & _
        '        "FundingContractTerm.SyaratID," & _
        '        "Description " & _
        '        "from FundingContractTerm " & _
        '        "inner join FundingMasterTerm on FundingContractTerm.SyaratID=FundingMasterTerm.SyaratID " & _
        '        "where FundingContractNo = '" & Request.QueryString("fundingcontractid") & "' " & _
        '        " and FlagFixedVar<>'F' and FlagAcqCov='C' And FlagEntry='" & IIf(Request.QueryString("Status") = "C", "C", "O") & "' "

        '    GObjStrSelectAdd = "select SyaratID,Description,TypeOfContent " & _
        '                        "from FundingMasterTerm " & _
        '                        "where FlagFixedVar<>'F' and FlagAcqCov='C' and (FlagCS='A' or FlagCS=(select FlagCS from FundingContract where FundingContractNo='" & Request.QueryString("fundingcontractid") & "')) " & _
        '                        "and SyaratID not in (select SyaratID from FundingContractTerm where FundingContractNo='" & Request.QueryString("fundingcontractid") & "') " & _
        '                        "order by SequenceNum asc "
        'End If

        ' Isi FundingCoyID, FundingCompanyName, FundingContractID, ContractName
        Dim GObjStrSelecttitle As String
        GObjStrSelecttitle = "select FundingContract.FundingCoyID,FundingCoyName,FundingContractNo,ContractName from FundingContract " & _
                            "inner join FundingCoy on FundingCoy.FundingCoyID = FundingContract.FundingCoyID " & _
                            "where FundingContract.FundingContractNo='" & Request.QueryString("fundingcontractid") & "' "
        Try
            Connection()
            Dim objAdapter As SqlDataAdapter
            objAdapter = New SqlDataAdapter(GObjStrSelecttitle, conIndType)
            'populate the DataSet
            dsIndType = New DataSet
            objAdapter.Fill(dsIndType, "FundingContract")
            Dim objRow As DataRowView = dsIndType.Tables("FundingContract").DefaultView(0)



            If Request("saveadd") <> "" Then
                'imgSaveAdd_Click(sender, e)
                SaveAdd()
            End If

            If Request("saveedit") <> "" Then
                'imgSaveEdit_Click(sender, e)
                SaveEdit()
            End If
        Catch ex As Exception
            'Label1.Text = ex.Message & "1"

        End Try
        If Not (IsPostBack) Then
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            'lstIncludeLeft.Attributes.Add("onChange", "javascript:checkSelected(this,document.frmindustry.varSelectedFrom,document.frmindustry.varIndexSelectedFrom);")
            'lstIncludeRight.Attributes.Add("onChange", "javascript:checkSelected(this,document.frmindustry.varSelectedFrom,document.frmindustry.varIndexSelectedFrom);")
            'lstExcludeLeft.Attributes.Add("onChange", "javascript:checkSelected(this,document.frmindustry.varSelectedFrom,document.frmindustry.varIndexSelectedFrom);")
            'lstExcludeRight.Attributes.Add("onChange", "javascript:checkSelected(this,document.frmindustry.varSelectedFrom,document.frmindustry.varIndexSelectedFrom);")
            Bindgrid()
        End If
    End Sub

    Sub Connection()
        Dim strConnect As String

        strConnect = GetConnectionString

        'dsIndType = New DataSet()
        'Try
        conIndType = New SqlConnection(strConnect)
        'Catch objError As Exception
        'outError.InnerHtml = "<b>* Error while accessing data</b>.<br />" _
        '   & objError.Message & "<br />" & objError.Source
        'Exit Sub
        'End Try
    End Sub
#End Region

#Region " BindGrid "
    Sub BindListCM(ByVal lSyaratID As String)
        Dim objAdapter As SqlDataAdapter
        Dim SQLStatement As String

        Connection()
        SQLStatement = "declare @S varchar(1000) " & _
                        "select @S = SQLCmdLookup from FundingMasterTerm where SyaratId='" & lSyaratID & "' " & _
                        "set @S=Replace(@S,'@FundingContractID', '''" & lblTitleFundingContractID.Text & "''') " & _
                        "exec(@S) "
        objAdapter = New SqlDataAdapter(SQLStatement, conIndType)
        objAdapter.Fill(dsIndType, "Syarat")

        lstIncludeLeft.Items.Clear()
        lstIncludeLeft.DataTextField = "Description"
        lstIncludeLeft.DataValueField = "ID"
        lstIncludeLeft.DataSource = dsIndType.Tables("Syarat")
        lstIncludeLeft.DataBind()

        lstExcludeLeft.Items.Clear()
        lstExcludeLeft.DataTextField = "Description"
        lstExcludeLeft.DataValueField = "ID"
        lstExcludeLeft.DataSource = dsIndType.Tables("Syarat")
        lstExcludeLeft.DataBind()

        lstIncludeRight.Items.Clear()
        lstExcludeRight.Items.Clear()
    End Sub

    Sub BindIncludeExcludeCS(ByVal lSyaratID As String)
        Dim objAdapter As SqlDataAdapter
        Dim SQLStatement As String

        Connection()
        SQLStatement = "declare @S varchar(1000) " & _
                        "select @S=SQLCmdLookup from FundingMasterTerm where SyaratId='" & lSyaratID & "' " & _
                        "exec(@S) "
        objAdapter = New SqlDataAdapter(SQLStatement, conIndType)
        objAdapter.Fill(dsIndType, "Syarat")

        ddlListIncludeCS.Items.Clear()
        ddlListIncludeCS.DataTextField = "Description"
        ddlListIncludeCS.DataValueField = "ID"
        ddlListIncludeCS.DataSource = dsIndType.Tables("Syarat")
        ddlListIncludeCS.DataBind()

        Dim lstItem As ListItem
        lstItem = New ListItem("Select one", "")
        ddlListIncludeCS.Items.Insert(0, lstItem)

        ddlListExcludeCS.Items.Clear()
        ddlListExcludeCS.DataTextField = "Description"
        ddlListExcludeCS.DataValueField = "ID"
        ddlListExcludeCS.DataSource = dsIndType.Tables("Syarat")
        ddlListExcludeCS.DataBind()

        ddlListExcludeCS.Items.Insert(0, lstItem)
    End Sub

    Sub BindgridTerm()

        GObjStrSelectAdd = "select SyaratID,Description,TypeOfContent " & _
                    "from FundingMasterTerm " & _
                    "where FlagFixedVar<>'F' and FlagAcqCov='A' and (FlagCS='A' or FlagCS=(select FlagCS from FundingContract where FundingContractNo='" & Request.QueryString("fundingcontractid") & "')) " & _
                    "and SyaratID not in (select SyaratID from FundingContractTerm where FundingContractNo='" & Request.QueryString("fundingcontractid") & "') " & _
                    "order by SequenceNum asc "

        If IsNothing(conIndType) Then Connection()

        Dim objAdapter As SqlDataAdapter



        dsIndType = New DataSet
        objAdapter = New SqlDataAdapter(GObjStrSelectAdd, conIndType)
        objAdapter.Fill(dsIndType, "Syarat")

        ddlTerm.Items.Clear()
        ddlTerm.DataSource = dsIndType
        ddlTerm.DataBind()
    End Sub

    Sub Bindgrid(Optional ByVal pStrPageCommand As String = "")
        lblTitleFundingCoyID.Text = Me.BankName
        lblTitleFundingCompanyName.Text = Me.CompanyName
        lblTitleFundingContractID.Text = Me.FundingContractNo
        lblTitleContractName.Text = Me.ContractName
        If IsNothing(conIndType) Then Connection()

        Dim objAdapter As SqlDataAdapter
        dsIndType = New DataSet
        Try
            objAdapter = New SqlDataAdapter(GObjStrSelectView, conIndType)
            objAdapter.Fill(dsIndType, "TC")

            Dim dvIndType As DataView
            dvIndType = New DataView(dsIndType.Tables("TC"))

            If Viewstate("search") = "DescCode" Then
                ' dvIndType.RowFilter = IIf(txtSearch.Text.Trim <> "", cboSearch.SelectedItem.Value & " LIKE '" & txtSearch.Text.Trim.Replace("'", "''") & "'", "")
            End If

            dvIndType.Sort = Viewstate("Sort")

            Select Case pStrPageCommand
                Case "First"
                    DtgTC.CurrentPageIndex = 0
                Case "Prev"
                    If DtgTC.CurrentPageIndex > 0 Then
                        DtgTC.CurrentPageIndex -= 1
                    End If
                Case "Next"
                    If DtgTC.CurrentPageIndex < DtgTC.PageCount - 1 Then
                        DtgTC.CurrentPageIndex += 1
                    End If
                Case "Last"
                    DtgTC.CurrentPageIndex = DtgTC.PageCount - 1
                Case "Paging"
                    Dim LBytePage As Integer
                    If IsNumeric(txtPage.Text) Then
                        LBytePage = CInt(txtPage.Text)
                        If LBytePage > DtgTC.PageCount Then
                            LBytePage = DtgTC.PageCount
                        End If
                    Else
                        LBytePage = 1
                    End If
                    DtgTC.CurrentPageIndex = LBytePage - 1
            End Select

            DtgTC.DataSource = dvIndType
            DtgTC.DataBind()
            Dim m As Int16
            Dim imgDelete As ImageButton
            For m = 0 To DtgTC.Items.Count - 1
                imgDelete = New ImageButton
                imgDelete = CType(DtgTC.Items(m).Cells(3).FindControl("imgDel"), ImageButton)
                imgDelete.Attributes.Add("onclick", "return fConfirm()")
            Next

            lblrecord.Text = dsIndType.Tables("TC").Rows.Count
            lblrecord.Text = dvIndType.Count
            lblPage.Text = DtgTC.CurrentPageIndex + 1
            lblTotPage.Text = DtgTC.PageCount

        Catch ex As Exception
            'Label1.Text = ex.Message & "2" & " " & Request.QueryString("fundingcontractid")
        End Try
    End Sub
#End Region

#Region " Delete "
    Sub DtgTC_Delete(ByVal pStrKey As String)
        'fires when Delete button is clicked
        Connection() 'create connection

        'put parameter into the strDelete 
        Dim strDelete As String = "DELETE from FundingContractTerm where SyaratID= @SyaratID and FundingContractNo=@FundingContractID"

        'create an instance of the SQLcommand passing the parameter of strDelete and connection object
        cmdIndType = New SqlCommand(strDelete, conIndType)

        'deleting process
        With cmdIndType
            .Parameters.Add(New SqlParameter("@SyaratID", SqlDbType.VarChar, 20))
            .Parameters("@SyaratID").Value = pStrKey
            .Parameters.Add(New SqlParameter("@FundingContractID", SqlDbType.VarChar, 20))
            .Parameters("@FundingContractID").Value = lblTitleFundingContractID.Text

            'open connection
            .Connection.Open()

            'test the deletion query
            Try
                .ExecuteNonQuery()
                'outError.InnerHtml = "<b>Record deleted</b><br>" & strDelete
                'Response.Write("<b>Record deleted</b><br>" & strDelete)
            Catch Exc As SqlException
                ShowMessage(lblMessage, "Cannot delete this record because a contract has been made", True)
                'Else
                '    Response.Write(Exc)
                'End If                
            End Try

            'close connection
            .Connection.Close()
        End With
        cmdIndType = Nothing

        Bindgrid()
    End Sub
#End Region

#Region " Add+Save "
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Connection()
        ClearInputs()
        lblAddEdit.Text = "ADD"
    End Sub

    Sub SaveAdd()
        If (pnlAddTypeCS.Visible = True) And ((ddlListIncludeCS.SelectedIndex = 0 And ddlListExcludeCS.SelectedIndex = 0) Or (ddlListIncludeCS.SelectedIndex <> 0 And ddlListExcludeCS.SelectedIndex <> 0)) Then
            errorCS.Visible = True
        Else
            'fires when Add button is clicked
            Connection() 'create connection

            'declare strSubCmd string variable to hold the input value
            Dim strSubInsCmd As String
            If pnlAddTypeR.Visible = True Then
                strSubInsCmd = "INSERT INTO FundingContractTerm (BankId,FundingCoyId,FundingContractNo,SyaratID,RangeFrom,RangeTo,FlagEntry) values " & _
                                "('" & Me.BankID & "', " & _
                                 "'" & Me.CompanyID & "', " & _
                                "'" & lblTitleFundingContractID.Text & "', " & _
                                "'" & ddlTerm.SelectedItem.Value & "', " & _
                                " " & txtAddFrom.Text & ", " & _
                                " " & txtAddTo.Text & ", " & _
                                "'C' " & _
                                ")"
            ElseIf pnlAddTypeCS.Visible = True Then
                strSubInsCmd = "INSERT INTO FundingContractTerm (BankId,FundingCoyId,FundingContractNo,SyaratID,ContentInc,ContentExc,FlagEntry) values " & _
                                "('" & Me.BankID & "', " & _
                                 "'" & Me.CompanyID & "', " & _
                                "'" & lblTitleFundingContractID.Text & "', " & _
                                "'" & ddlTerm.SelectedItem.Value & "', " & _
                                "'''" & ddlListIncludeCS.SelectedItem.Value & "''', " & _
                                "'''" & ddlListExcludeCS.SelectedItem.Value & "''', " & _
                                "'C' " & _
                                ")"
            ElseIf pnlAddTypeCM.Visible = True Then
                Dim i As Integer

                strSubInsCmd = "INSERT INTO FundingContractTerm (BankId,FundingCoyId,FundingContractNo,SyaratID,ContentInc,ContentExc,FlagEntry) values " & _
                                     "('" & Me.BankID & "', " & _
                                 "'" & Me.CompanyID & "', " & _
                                "'" & lblTitleFundingContractID.Text & "', " & _
                                "'" & ddlTerm.SelectedItem.Value & "', " & _
                                "'" & Request("pilihan1") & "', " & _
                                "'" & Request("pilihan2") & "', " & _
                                "'C' " & _
                                ")"
            ElseIf pnlAddTypeN1.Visible = True Then
                strSubInsCmd = "INSERT INTO FundingContractTerm (BankId,FundingCoyId,FundingContractNo,SyaratID,RangeFrom,FlagEntry) values " & _
                                "('" & Me.BankID & "', " & _
                                 "'" & Me.CompanyID & "', " & _
                                "'" & lblTitleFundingContractID.Text & "', " & _
                                "'" & ddlTerm.SelectedItem.Value & "', " & _
                                " " & txtAddParameter.Text & ", " & _
                                "'C' " & _
                                ")"
            ElseIf pnlAddTypeN2.Visible = True Then
                strSubInsCmd = "INSERT INTO FundingContractTerm (BankId,FundingCoyId,FundingContractNo,SyaratID,RangeFrom,RangeTo,FlagEntry) values " & _
                                "('" & Me.BankID & "', " & _
                                 "'" & Me.CompanyID & "', " & _
                                "'" & lblTitleFundingContractID.Text & "', " & _
                                "'" & ddlTerm.SelectedItem.Value & "', " & _
                                " " & txtAddParameterN1.Text & ", " & _
                                " " & txtAddParameterN2.Text & ", " & _
                                "'C' " & _
                                ")"
            ElseIf PnlAddTypeNU.Visible = True Then
                strSubInsCmd = "INSERT INTO FundingContractTerm (BankId,FundingCoyId,FundingContractNo,SyaratID,RangeFrom,RangeTo,RangeFromUsed,RangeToUsed,FlagEntry) values " & _
                                "('" & Me.BankID & "', " & _
                                 "'" & Me.CompanyID & "', " & _
                                "'" & lblTitleFundingContractID.Text & "', " & _
                                "'" & ddlTerm.SelectedItem.Value & "', " & _
                                " " & TxtFromNew.Text & ", " & _
                                " " & TxtToNew.Text & ", " & _
                                " " & TxtFromUsed.Text & ", " & _
                                " " & TxtToUsed.Text & ", " & _
                                "'C' " & _
                                ")"
            End If

            'create SQLcommand instance
            cmdIndType = New SqlCommand(strSubInsCmd, conIndType)

            'inputting user information
            With cmdIndType
                'open connection
                .Connection.Open()

                'test if the query executes
                Try
                    .ExecuteNonQuery()
                Catch Exp As SqlException

                    If Exp.Number = 2627 Then
                        ShowMessage(lblMessage, "A record already exists with the same primary key", True)
                    Else
                        Response.Write(Exp)
                    End If                    
                    Exit Sub
                End Try

                'close connection
                .Connection.Close()
            End With
            cmdIndType = Nothing

            'binding
            Bindgrid()
            pnlAddEdit.Visible = False
            'hide the corresponding panel and label
            pnlTop.Visible = True
        End If
    End Sub

    Sub SaveEdit()
        If (pnlAddTypeCS.Visible = True) And ((ddlListIncludeCS.SelectedIndex = 0 And ddlListExcludeCS.SelectedIndex = 0) Or (ddlListIncludeCS.SelectedIndex <> 0 And ddlListExcludeCS.SelectedIndex <> 0)) Then
            errorCS.Visible = True
        Else
            'create instance of the connection object
            Connection()
            'declare string variable to hold the SQL statement
            Dim UpdateCmd As String

            If pnlAddTypeR.Visible = True Then
                UpdateCmd = "UPDATE FundingContractTerm SET  " & _
                                        "RangeFrom = @RangeFrom, " & _
                                        "RangeTo = @RangeTo " & _
                                        "WHERE FundingContractNo = @FundingContractID and SyaratID = @SyaratID "

                'create an instance of the SQLCommand
                cmdIndType = New SqlCommand(UpdateCmd, conIndType)

                With cmdIndType
                    .Parameters.Add("@FundingContractID", SqlDbType.VarChar, 20).Value = lblTitleFundingContractID.Text
                    .Parameters.Add("@SyaratID", SqlDbType.VarChar, 20).Value = lblSyaratID.Text
                    .Parameters.Add("@RangeFrom", SqlDbType.Money).Value = txtAddFrom.Text
                    .Parameters.Add("@RangeTo", SqlDbType.Money).Value = txtAddTo.Text
                End With
            ElseIf PnlAddTypeNU.Visible = True Then
                UpdateCmd = "UPDATE FundingContractTerm SET  " & _
                                        "RangeFrom = @RangeFrom, " & _
                                        "RangeTo = @RangeTo, " & _
                                        "RangeFromUsed = @RangeFromUsed, " & _
                                        "RangeToUsed = @RangeToUsed " & _
                                        "WHERE FundingContractNo = @FundingContractID and SyaratID = @SyaratID "

                'create an instance of the SQLCommand
                cmdIndType = New SqlCommand(UpdateCmd, conIndType)

                With cmdIndType
                    .Parameters.Add("@FundingContractID", SqlDbType.VarChar, 20).Value = lblTitleFundingContractID.Text
                    .Parameters.Add("@SyaratID", SqlDbType.VarChar, 20).Value = lblSyaratID.Text
                    .Parameters.Add("@RangeFrom", SqlDbType.Decimal).Value = CInt(TxtFromNew.Text.Trim)
                    .Parameters.Add("@RangeTo", SqlDbType.Decimal).Value = CInt(TxtToNew.Text.Trim)
                    .Parameters.Add("@RangeFromUsed", SqlDbType.Decimal).Value = CInt(TxtFromUsed.Text.Trim)
                    .Parameters.Add("@RangeToUsed", SqlDbType.Decimal).Value = CInt(TxtToUsed.Text.Trim)
                End With
            ElseIf pnlAddTypeCS.Visible = True Then
                UpdateCmd = "UPDATE FundingContractTerm SET  " & _
                            "ContentInc = @ContentInc, " & _
                            "ContentExc = @ContentExc " & _
                            "WHERE FundingContractNo = @FundingContractID and SyaratID = @SyaratID "

                'create an instance of the SQLCommand
                cmdIndType = New SqlCommand(UpdateCmd, conIndType)

                With cmdIndType
                    .Parameters.Add("@FundingContractID", SqlDbType.VarChar, 20).Value = lblTitleFundingContractID.Text
                    .Parameters.Add("@SyaratID", SqlDbType.VarChar, 20).Value = lblSyaratID.Text
                    .Parameters.Add("@ContentInc", SqlDbType.VarChar, 8000).Value = "'" & ddlListIncludeCS.SelectedItem.Value & "'"
                    .Parameters.Add("@ContentExc", SqlDbType.VarChar, 8000).Value = "'" & ddlListExcludeCS.SelectedItem.Value & "'"
                End With
            ElseIf pnlAddTypeCM.Visible = True Then
                UpdateCmd = "UPDATE FundingContractTerm SET  " & _
                            "ContentInc = @ContentInc, " & _
                            "ContentExc = @ContentExc " & _
                            "WHERE FundingContractNo = @FundingContractID and SyaratID = @SyaratID "

                'create an instance of the SQLCommand
                cmdIndType = New SqlCommand(UpdateCmd, conIndType)

                With cmdIndType
                    .Parameters.Add("@FundingContractID", SqlDbType.VarChar, 20).Value = lblTitleFundingContractID.Text
                    .Parameters.Add("@SyaratID", SqlDbType.VarChar, 20).Value = lblSyaratID.Text
                    .Parameters.Add("@ContentInc", SqlDbType.VarChar, 8000).Value = Request("pilihan1")
                    .Parameters.Add("@ContentExc", SqlDbType.VarChar, 8000).Value = Request("pilihan2")
                End With
            ElseIf pnlAddTypeN1.Visible = True Then
                UpdateCmd = "UPDATE FundingContractTerm SET  " & _
                            "RangeFrom = @Parameter " & _
                            "WHERE FundingContractNo = @FundingContractID and SyaratID = @SyaratID "

                'create an instance of the SQLCommand
                cmdIndType = New SqlCommand(UpdateCmd, conIndType)

                With cmdIndType
                    .Parameters.Add("@FundingContractID", SqlDbType.VarChar, 20).Value = lblTitleFundingContractID.Text
                    .Parameters.Add("@SyaratID", SqlDbType.VarChar, 20).Value = lblSyaratID.Text
                    .Parameters.Add("@Parameter", SqlDbType.Money).Value = txtAddParameter.Text
                End With
            ElseIf pnlAddTypeN2.Visible = True Then
                UpdateCmd = "UPDATE FundingContractTerm SET  " & _
                            "RangeFrom = @ParameterN1, " & _
                            "RangeTo = @ParameterN2, " & _
                            "WHERE FundingContractNo = @FundingContractID and SyaratID = @SyaratID "

                'create an instance of the SQLCommand
                cmdIndType = New SqlCommand(UpdateCmd, conIndType)

                With cmdIndType
                    .Parameters.Add("@FundingContractID", SqlDbType.VarChar, 20).Value = lblTitleFundingContractID.Text
                    .Parameters.Add("@SyaratID", SqlDbType.VarChar, 20).Value = lblSyaratID.Text
                    .Parameters.Add("@ParameterN1", SqlDbType.Money).Value = txtAddParameterN1.Text
                    .Parameters.Add("@ParameterN2", SqlDbType.Money).Value = txtAddParameterN2.Text
                End With
            End If

            With cmdIndType

                'open connection
                .Connection.Open()

                'test whether the record is properly updated
                Try
                    .ExecuteNonQuery()

                Catch Exp As SqlException

                    If Exp.Number = 2627 Then
                        ShowMessage(lblMessage, "A record already exists with the same primary key", True)
                    Else
                        ShowMessage(lblMessage, Exp.Message, True)                        
                    End If                    
                    Exit Sub
                End Try
                'close connection
                .Connection.Close()
                pnlTop.Visible = True
            End With

            'binding the result
            Bindgrid()

            'hide the corresponding panel and label
            pnlAddEdit.Visible = False
            btnSaveEdit.Visible = True
            btnSaveAdd.Visible = False
        End If
    End Sub
#End Region

#Region " ShowView "
    Sub ShowView(ByVal obj As Object, ByVal ev As DataGridCommandEventArgs)
        Connection()
        If ev.CommandName = "ShowView" Then
            Dim drInd As DataRow

            GObjStrSelectEdit = GObjStrSelectEdit & "and FundingContractTerm.SyaratID='" & DtgTC.DataKeys(ev.Item.ItemIndex) & "'"
            'create an instance of SQLDataAdapter
            Dim objAdapter As SqlDataAdapter
            objAdapter = New SqlDataAdapter(GObjStrSelectEdit, conIndType)

            'populate the DataSet
            dsIndType = New DataSet
            objAdapter.Fill(dsIndType, "TC")

            Dim objRow As DataRowView = dsIndType.Tables("TC").DefaultView(0)

            'Show the data
            lblDetailSyaratID.Text = objRow("SyaratID")
            lblDetailDescription.Text = objRow("Description")

            pnlViewTypeR.Visible = False
            pnlViewTypeCS.Visible = False
            pnlViewTypeN1.Visible = False
            pnlViewTypeN2.Visible = False
            PnlAddTypeNU.Visible = False

            If Trim(objRow("TypeOfContent")) = "R" Then
                pnlViewTypeR.Visible = True
                lblDetailFrom.Text = Format(objRow("RangeFrom"), "#,##0")
                lblDetailTo.Text = Format(objRow("RangeTo"), "#,##0")
            ElseIf Trim(objRow("TypeOfContent")) = "CS" Or Trim(objRow("TypeOfContent")) = "CM" Then
                pnlViewTypeCS.Visible = True
                lblDetailInclude.Text = objRow("ContentInc")
                lblDetailExclude.Text = objRow("ContentExc")
            ElseIf Trim(objRow("TypeOfContent")) = "N1" Then
                pnlViewTypeN1.Visible = True
                lblDetailParameter.Text = Format(objRow("RangeFrom"), "#,##0")
            ElseIf Trim(objRow("TypeOfContent")) = "N2" Then
                pnlViewTypeN2.Visible = True
                lblDetailParameterN1.Text = Format(objRow("RangeFrom"), "#,##0")
                lblDetailParameterN2.Text = Format(objRow("RangeTo"), "#,##0")
            ElseIf Trim(objRow("TypeOfContent")) = "NU" Then
                PnlAddTypeNU.Visible = True
                LblDetailFromNew.Text = Format(objRow("RangeFrom"), "#,##0")
                LblDetailToNew.Text = Format(objRow("RangeTo"), "#,##0")
                LblDetailFromUsed.Text = Format(objRow("RangeFromUsed"), "#,##0")
                LblDetailToUsed.Text = Format(objRow("RangeToUsed"), "#,##0")
            End If

            If Not IsPostBack Then
                Bindgrid()
            End If

            pnlView.Visible = True
            pnlAddEdit.Visible = False
            pnlTop.Visible = False

        ElseIf ev.CommandName = "Edit" Then
            EditData(DtgTC.DataKeys(ev.Item.ItemIndex))
        ElseIf ev.CommandName = "Delete" Then
            DtgTC_Delete(DtgTC.DataKeys(ev.Item.ItemIndex))
        End If
    End Sub
#End Region

#Region " Edit-EditData "
    Public Sub EditData(ByVal pStrKey As String)
        lblAddEdit.Text = "EDIT"
        Connection()
        'instantiate connection object

        Dim objDataRow As DataRow

        GObjStrSelectEdit = GObjStrSelectEdit & "and FundingContractTerm.SyaratID='" & pStrKey & "'"

        'create an instance of SQLDataAdapter
        Dim objAdapter As New SqlDataAdapter(GObjStrSelectEdit, conIndType)

        'populate the DataSet
        dsIndType = New DataSet
        objAdapter.Fill(dsIndType, "TC")

        Dim objRow As DataRowView = dsIndType.Tables("TC").DefaultView(0)

        'input the values into the corresponding interface
        ddlTerm.Visible = False
        lblSyaratID.Text = Trim(objRow("SyaratID"))
        lblDescription.Text = " - " & objRow("Description")
        lblSyaratID.Visible = True
        lblDescription.Visible = True

        pnlAddTypeR.Visible = False
        pnlAddTypeCS.Visible = False
        pnlAddTypeCM.Visible = False
        pnlAddTypeN1.Visible = False
        pnlAddTypeN2.Visible = False
        PnlAddTypeNU.Visible = False

        pnlAddEdit.Visible = True
        pnlView.Visible = False

        pnlBtnSave.Visible = False
        pnlBtnEdit.Visible = False
        btnSaveAdd.Visible = False
        btnSaveEdit.Visible = False

        pnlTop.Visible = False

        If Trim(objRow("TypeOfContent")) = "R" Then
            pnlAddTypeR.Visible = True
            btnSaveEdit.Visible = True
            txtAddFrom.Text = objRow("RangeFrom")
            txtAddTo.Text = objRow("RangeTo")
        ElseIf Trim(objRow("TypeOfContent")) = "NU" Then
            PnlAddTypeNU.Visible = True
            btnSaveEdit.Visible = True
            TxtFromNew.Text = objRow("RangeFrom")
            TxtToNew.Text = objRow("RangeTo")
            TxtFromUsed.Text = objRow("RangeFromUsed")
            TxtToUsed.Text = objRow("RangeToUsed")
        ElseIf Trim(objRow("TypeOfContent")) = "CS" Then
            pnlAddTypeCS.Visible = True
            btnSaveEdit.Visible = True
            BindIncludeExcludeCS(objRow("SyaratID"))
            If Trim(Replace(objRow("ContentInc"), "'", "")) <> "" Then
                ddlListIncludeCS.SelectedIndex = ddlListIncludeCS.Items.IndexOf(ddlListIncludeCS.Items.FindByValue(Replace(objRow("ContentInc"), "'", "")))
            End If
            If Trim(Replace(objRow("ContentExc"), "'", "")) <> "" Then
                ddlListExcludeCS.SelectedIndex = ddlListExcludeCS.Items.IndexOf(ddlListExcludeCS.Items.FindByValue(Replace(objRow("ContentExc"), "'", "")))
            End If
        ElseIf Trim(objRow("TypeOfContent")) = "CM" Then
            pnlAddTypeCM.Visible = True
            pnlBtnEdit.Visible = True
            Dim vContent() As String
            Dim vContent2() As String
            Dim i As Integer
            Dim posisi As Integer
            Dim lstItem As ListItem

            BindListCM(objRow("SyaratID"))

            If Trim(Replace(objRow("ContentInc"), "'", "")) <> "" Then
                vContent = Split(objRow("ContentInc"), ",")
                For i = 0 To vContent.Length - 1
                    vContent(i) = Replace(vContent(i), "'", "")

                    posisi = lstIncludeLeft.Items.IndexOf(lstIncludeLeft.Items.FindByValue(vContent(i)))

                    lstItem = New ListItem(lstIncludeLeft.Items(posisi).Text, lstIncludeLeft.Items(posisi).Value)
                    lstIncludeRight.Items.Insert(0, lstItem)
                    lstIncludeLeft.Items.RemoveAt(posisi)
                Next
            End If

            If Trim(Replace(objRow("ContentExc"), "'", "")) <> "" Then
                vContent2 = Split(objRow("ContentExc"), ",")
                For i = 0 To vContent2.Length - 1
                    vContent2(i) = Replace(vContent2(i), "'", "")
                    posisi = lstExcludeLeft.Items.IndexOf(lstExcludeLeft.Items.FindByValue(vContent2(i)))

                    lstItem = New ListItem(lstExcludeLeft.Items(posisi).Text, lstExcludeLeft.Items(posisi).Value)
                    lstExcludeRight.Items.Insert(0, lstItem)
                    lstExcludeLeft.Items.RemoveAt(posisi)
                Next
            End If

        ElseIf Trim(objRow("TypeOfContent")) = "N1" Then
            pnlAddTypeN1.Visible = True
            btnSaveEdit.Visible = True
            txtAddParameter.Text = objRow("RangeFrom")
        ElseIf Trim(objRow("TypeOfContent")) = "N2" Then
            pnlAddTypeN2.Visible = True
            btnSaveEdit.Visible = True
            txtAddParameterN1.Text = objRow("RangeFrom")
            txtAddParameterN2.Text = objRow("RangeTo")
        End If
    End Sub


#End Region

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Viewstate("search") = "DescCode"
        DtgTC.CurrentPageIndex = 0
        Bindgrid()

        If DtgTC.Items.Count = 0 Then
            Response.Write("No records matched with your criteria.")
            DtgTC.Visible = False
            btnAdd.Visible = True
        Else
            'bind the result from the DataView to be presented in the DataGrid
            Response.Write("")
            'show the DataGrid
            DtgTC.Visible = True
        End If
    End Sub

#Region " PageCommand+Paging "
    Sub PageCommand(ByVal sender As Object, ByVal e As EventArgs)
        Dim LstrArg As String = sender.commandArgument()
        Bindgrid(LstrArg)
    End Sub


#End Region

#Region " Cancel+OKbutton+Sorting "


    Private Sub btnViewOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnViewOK.Click
        pnlView.Visible = False
        pnlTop.Visible = True
    End Sub

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(ViewState("Sort"), "DESC") > 0 Then
            ViewState("Sort") = e.SortExpression
        Else
            ViewState("Sort") = e.SortExpression + " DESC"
        End If
        Bindgrid()
    End Sub
#End Region

    Sub ClearInputs()
        BindgridTerm()

        txtAddFrom.Text = ""
        txtAddTo.Text = ""

        If ddlTerm.Items.Count <> 0 Then
            UbahSyarat()

            pnlAddEdit.Visible = True
            pnlView.Visible = False
            pnlTop.Visible = False
        Else                        
            ShowMessage(lblMessage, "No more Term can be added", True)            
            pnlTop.Visible = True


        End If

    End Sub

    Sub UbahSyarat()
        Dim objAdapter As SqlDataAdapter
        dsIndType = New DataSet
        Dim SQLStatement As String
        If IsNothing(conIndType) Then Connection()

        SQLStatement = "select TypeOfContent from FundingMasterTerm where SyaratID='" & ddlTerm.SelectedItem.Value & "'"
        objAdapter = New SqlDataAdapter(SQLStatement, conIndType)
        objAdapter.Fill(dsIndType, "Type")

        Dim objRow As DataRowView = dsIndType.Tables("Type").DefaultView(0)

        ddlTerm.Visible = True
        lblSyaratID.Visible = False
        lblDescription.Visible = False

        pnlAddTypeR.Visible = False
        pnlAddTypeCS.Visible = False
        pnlAddTypeCM.Visible = False
        pnlAddTypeN1.Visible = False
        pnlAddTypeN2.Visible = False
        PnlAddTypeNU.Visible = False

        pnlBtnSave.Visible = False
        pnlBtnEdit.Visible = False
        btnSaveAdd.Visible = False
        btnSaveEdit.Visible = False

        If Trim(objRow("TypeOfContent")) = "R" Then
            pnlAddTypeR.Visible = True
            btnSaveAdd.Visible = True
            txtAddFrom.Text = ""
            txtAddTo.Text = ""
        ElseIf Trim(objRow("TypeOfContent")) = "CS" Then
            pnlAddTypeCS.Visible = True
            btnSaveAdd.Visible = True
            BindIncludeExcludeCS(ddlTerm.SelectedItem.Value)
        ElseIf Trim(objRow("TypeOfContent")) = "CM" Then
            pnlAddTypeCM.Visible = True
            pnlBtnSave.Visible = True
            BindListCM(ddlTerm.SelectedItem.Value)
        ElseIf Trim(objRow("TypeOfContent")) = "N1" Then
            pnlAddTypeN1.Visible = True
            btnSaveAdd.Visible = True
            txtAddParameter.Visible = True
        ElseIf Trim(objRow("TypeOfContent")) = "N2" Then
            pnlAddTypeN2.Visible = True
            btnSaveAdd.Visible = True
            txtAddParameterN1.Visible = True
            txtAddParameterN2.Visible = True
        ElseIf Trim(objRow("TypeOfContent")) = "NU" Then
            PnlAddTypeNU.Visible = True
            btnSaveAdd.Visible = True
        End If

    End Sub

    Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        UbahSyarat()
    End Sub

    'Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
    '    If Request.QueryString("Status") = "C" Then
    '        Response.Redirect("FundingContract.aspx?fundingcoyid=" & lblTitleFundingCoyID.Text)
    '    Else
    '        Response.Redirect("../../../SecureMaint/SecureProposed.aspx?ContractID=" & lblTitleFundingContractID.Text)
    '    End If
    'End Sub

    Private Overloads Sub BtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        Bindgrid("Paging")
    End Sub



    Private Sub imbFirstPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFirstPage.Click
        Dim LstrArg As String = sender.commandArgument()
        Bindgrid(LstrArg)
    End Sub

    Private Sub imbPrevPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrevPage.Click
        Dim LstrArg As String = sender.commandArgument()
        Bindgrid(LstrArg)
    End Sub

    Private Sub imbNextPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbNextPage.Click
        Dim LstrArg As String = sender.commandArgument()
        Bindgrid(LstrArg)
    End Sub

    Private Sub imbLastPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbLastPage.Click
        Dim LstrArg As String = sender.commandArgument()
        Bindgrid(LstrArg)
    End Sub




    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click
        SaveAdd()
    End Sub

    Private Sub btnSaveEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEdit.Click
        SaveEdit()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlAddEdit.Visible = False
        pnlTop.Visible = True
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("fundingcontractmaintenance.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
    End Sub

End Class