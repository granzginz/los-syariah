Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper

Public Class BonHijauFundingDisbViewerRePrint
    Inherits Maxiloan.Webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtReferenceNo As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnPrint As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub directPrint()
        Dim cookie As HttpCookie = Request.Cookies("BonHijauFundingDisburse")

        If Not cookie Is Nothing Then
            cookie.Values("referenceNo") = txtReferenceNo.Text.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("BonHijauFundingDisburse")

            cookieNew.Values.Add("referenceNo", txtReferenceNo.Text.Trim)
            Response.AppendCookie(cookieNew)
        End If

        Response.Redirect("BonHijauFundingDisbViewer.aspx?callingFrom=reprint")
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        directPrint()
    End Sub
End Class
