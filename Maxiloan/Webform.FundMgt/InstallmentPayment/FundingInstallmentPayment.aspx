﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingInstallmentPayment.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingInstallmentPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingInstallmentPayment</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }

    </script>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function total(e,parTotal,totalOS,totalInt) {
        document.getElementById(e + '_lblSum').innerHTML = number_format(parTotal, 2);
        document.getElementById(e + '_lblSumOS').innerHTML = number_format(totalOS, 2);
        document.getElementById(e + '_lblSumInt').innerHTML = number_format(totalInt, 2);
        //'Datagrid1_lblSum'
    }


        function number_format(number, decimals, dec_point, thousands_sep) {
            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updPanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel1" runat="server">
        <ContentTemplate>

        

    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PEMBAYARAN ANGSURAN BERDASARKAN TANGGAL JATUH TEMPO
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Funding Bank</label>
                <asp:DropDownList ID="drdCompany" runat="server" AutoPostBack="True" Width="350px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="drdCompany" ErrorMessage="Harap pilih Funding Bank"
                    Visible="True" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No. Fasilitas</label>
                <asp:DropDownList ID="drdContract" runat="server"  Width="350px">
                </asp:DropDownList>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="drdContract" ErrorMessage="Harap pilih No Kontrak"
                    Visible="True" CssClass="validator_general" ></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Batch No</label>
                <asp:TextBox runat="server" ID="txtBatchNo" Width="290px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Jatuh Tempo</label>
                <%--<asp:TextBox runat="server" ID="txtAgingDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtAgingDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtAgingDate" runat="server"></uc1:ucdatece>
                <label class="label_auto">
                    s/d</label>                
                <uc1:ucdatece id="txtAgingDateto" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnFind" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL PEMBAYARAN ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Fasilitas
                </label>
                <asp:Label ID="lblJumlahKontrak" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Tagihan
                </label>
                <asp:Label ID="lblJumlahAngsuran" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pokok
                </label>
                <asp:Label ID="lblJumlahPokok" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Refund Margin
                </label>
                <asp:Label ID="lblRefundBunga" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Margin
                </label>
                <asp:Label ID="lblJumlahBunga" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tagihan Net
                </label>
                <asp:Label ID="lblTagihanNet" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="Datagrid1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general" ShowFooter="True" >
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkHeader" runat="server" AutoPostBack="True" OnCheckedChanged="SelectAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server" OnCheckedChanged="SelectDtl" AutoPostBack="true"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO . KONTRAK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblApplicationId" Visible="FALSE" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.dataitem("SupplierID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerIDx" runat="server" Visible="False" Text='<%#Container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypCustomerx" runat="server" Text='<%#Container.dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsSeqNo" HeaderText="ANGS KE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.dataitem("InsSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    Grand Total
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DueDate" HeaderText="TGL JT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDueDate" runat="server" Text='<%#Container.DataItem("DueDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="JUMLAH ANGSURAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%#formatnumber(Container.dataitem("InstallmentAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblSum" runat="server" Text=''></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PrincipalAmount" HeaderText="POKOK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%#formatnumber(Container.dataitem("PrincipalAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblSumOS" runat="server" Text=''></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InterestAmount" HeaderText="MARGIN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInterestAmount" runat="server" Text='<%#formatnumber(Container.dataitem("InterestAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblSumInt" runat="server" Text=''></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RefundInterest" HeaderText="REFUND MARGIN" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRefundInterest" runat="server" Text='<%#formatnumber(Container.dataitem("RefundInterest"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Tenor" HeaderText="JK WAKTU">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Container.dataitem("tenor")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="akhirBayar" HeaderText="TGL AKHIR BAYAR">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%#Container.DataItem("akhirBayar")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="FundingContractID" HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%#Container.dataitem("FundingContractID")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblContractName" runat="server" Text='<%#Container.DataItem("ContractName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="FundingBatchID" HeaderText="NO BATCH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%#Container.dataitem("FundingBatchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green">
            </asp:Button>
             <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnDelete" runat="server" Text="Hapus" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlTransfer" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PEMBAYARAN ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
              <label class ="label">
                    No Bukti Kas Keluar</label>
                <asp:TextBox ID="txtReferenceNo" runat="server" placeholder="Auto Jika kosong" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Cara Pembayaran</label>
                <asp:DropDownList ID="cmbWayOfPayment" runat="server" AutoPostBack="True" >
                </asp:DropDownList>
                <asp:Button ID="btnRefreshBankAccount" runat="server" Text="Refresh" CausesValidation="False">
                </asp:Button>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="cmbWayOfPayment" ErrorMessage="Harap pilih Cara Pembayaran"
                    Visible="True" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Rekening Bank</label>
                <asp:DropDownList ID="cmbBankAccount" runat="server"  AutoPostBack="true" Width="350px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap Pilih rekening Bank"
                    Visible="True" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Valuta</label>
                <%--<asp:TextBox runat="server" ID="txtValueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah</label>
                <asp:Label ID="lblAmountRec" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" Width="100%"  TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID ="btnPrint" />

    </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
