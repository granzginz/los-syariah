﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class BonHijauFundingDisbViewer
    Inherits Maxiloan.Webform.WebBased

    Private oParameter As New Parameter.PembayaranAngsuranFunding
    Private oController As New PembayaranAngsuranFundingController

    Private Property ReferenceNo() As String
        Get
            Return CType(viewstate("ReferenceNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ReferenceNo") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        bindReport()
        'createData()
    End Sub

    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oReport As BonHijauFundingDisb = New BonHijauFundingDisb

        With oParameter
            .strConnection = GetConnectionString
            .ReferenceNoBonHijau = Me.ReferenceNo            
        End With

        oParameter = oController.GetListBonHijauFundingDisburse(oParameter)
        oDataSet = oParameter.ListBonHijau

        oReport.SetDataSource(oDataSet)
        CrystalReportViewer.ReportSource = oReport
        CrystalReportViewer.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "BonHijauFundingDisburse.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
        End With

        Response.Redirect("FundingInstallmentPayment.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "BonHijauFundingDisburse")
    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("BonHijauFundingDisburse")
        Me.ReferenceNo = cookie.Values("referenceNo")
    End Sub

    Private Sub createData()
        Dim oController As New PembayaranAngsuranFundingController
        Dim oDataSet As New DataSet
        Dim oParameter As New Parameter.PembayaranAngsuranFunding

        oParameter.strConnection = GetConnectionString
        oParameter.ReferenceNoBonHijau = "'BON007'"
        oParameter = oController.GetListBonHijauFundingDisburse(oParameter)
        oDataSet = oParameter.ListBonHijau
        oDataSet.WriteXmlSchema("D:\Project\AndalanFinance\Maxiloan\Webform.FundMgt\Report\BonHijauFundingDisburse.xml")
    End Sub

End Class