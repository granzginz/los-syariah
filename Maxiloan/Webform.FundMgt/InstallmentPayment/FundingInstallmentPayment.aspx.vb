﻿
#Region " Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper

#End Region

Public Class FundingInstallmentPayment
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"
    Protected WithEvents txtAgingDate As ucDateCE
    Protected WithEvents txtAgingDateto As ucDateCE
    Protected WithEvents txtValueDate As ucDateCE
#End Region
#Region " Declares"
    Dim Total, TotalOS, TotalInt As Double
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 1000
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    'Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    'Protected WithEvents Datagrid1 As System.Web.UI.WebControls.DataGrid
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PembayaranAngsuranFunding
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator

    Private oController As New PembayaranAngsuranFundingController
    Private dtBankAccount As New DataTable
    Private m_controller As New DataUserControlController
    Protected WithEvents oBGNODate As ucBGNoDate
    'Protected WithEvents lblTagihanNet As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRefundBunga As System.Web.UI.WebControls.Label

    Protected TotalAmount As Double
    Protected TotalPrincipal As Double
    Protected TotalInterest As Double
#End Region

#Region " Property"
    Private Property PrincipalPaidAmount() As Double
        Get
            Return (CType(ViewState("PrincipalPaidAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipalPaidAmount") = Value
        End Set
    End Property

    Private Property InterestPaidAmount() As Double
        Get
            Return (CType(ViewState("InterestPaidAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("InterestPaidAmount") = Value
        End Set
    End Property

    Private Property RefundInterest() As Double
        Get
            Return (CType(ViewState("RefundInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("RefundInterest") = Value
        End Set
    End Property

    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Private Property SelectMode() As Boolean
        Get
            Return (CType(ViewState("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("SelectMode") = Value
        End Set
    End Property

    Private Property cmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property

    Private Property SelectedApplicationIDs() As String
        Get
            Return CType(ViewState("SelectedApplicationIDs"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SelectedApplicationIDs") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub
        If Not Page.IsPostBack Then
            txtAgingDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtAgingDateto.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            'Direct print
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                & "</script>")
            End If

            bindFundingCompany()
            bindWayOfPayment()
            Me.SelectMode = True
        End If
    End Sub

    Sub DoBind(ByVal strWhere As String, ByVal SortBy As String)
        Dim oDataTable As New DataTable
        Dim oDataView As New DataView

        'not use this parameter
        'strWhere = IIf(strWhere.Trim = "", "", strWhere)
        If txtAgingDate.Text = "" Or txtAgingDateto.Text = "" Then
            ShowMessage(lblMessage, "Harap Isi Tanggal Jatuh Tempo ...", True)
            Exit Sub
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyID = drdCompany.SelectedValue
            .FundingContractNo = drdContract.SelectedValue
            .FundingBatchNo = txtBatchNo.Text
            .InstallmentDueDate = ConvertDate2(txtAgingDate.Text).ToString("yyyy/MM/dd")
            .InstallmentDueDateTo = ConvertDate2(txtAgingDateto.Text).ToString("yyyy/MM/dd")
        End With

        oCustomClass = oController.GetListPembayaranAngsuran(oCustomClass)
        oDataTable = oCustomClass.ListPembayaranAngsuran.Tables(0)
        oDataView = oDataTable.DefaultView
        oDataView.Sort = Me.SortBy
        Datagrid1.DataSource = oDataView

        Try
            Datagrid1.DataBind()
        Catch
            Datagrid1.CurrentPageIndex = 0
            Datagrid1.DataBind()
        End Try

        lblJumlahKontrak.Text = FormatNumber(oCustomClass.JumlahKontrak, 0)
        lblJumlahPokok.Text = FormatNumber(oCustomClass.JumlahPokok, 2)
        lblJumlahAngsuran.Text = FormatNumber(oCustomClass.JumlahAngsuran, 2)
        lblJumlahBunga.Text = FormatNumber(oCustomClass.JumlahBunga, 2)
        lblRefundBunga.Text = FormatNumber(oCustomClass.RefundBunga, 2)
        lblTagihanNet.Text = FormatNumber(oCustomClass.JumlahAngsuran - oCustomClass.RefundBunga, 2)

        pnlView.Visible = True
        lblMessage.Text = ""
    End Sub

    Private Sub bindFundingCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"
            drdCompany.DataSource = dtvEntity
            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "Select One")
            drdCompany.Items(0).Value = "0"
        Catch e As Exception

            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub bindFundindContract()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " where FundingCoyID = '" & drdCompany.SelectedValue.Trim & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingContractList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdContract.DataValueField = "FundingContractNo"
            drdContract.DataTextField = "ContractName"
            drdContract.DataSource = dtvEntity
            drdContract.DataBind()
            drdContract.Items.Insert(0, "--All--")
            drdContract.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        DoBind(Me.cmdWhere, Me.SortBy)
    End Sub

    Private Sub drdCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drdCompany.SelectedIndexChanged
        bindFundindContract()
    End Sub

    Public Sub SelectDtl(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkItem As CheckBox
        Dim chkSender As CheckBox = sender
        Dim index As Integer = DirectCast(CType(sender, CheckBox).BindingContainer, System.Web.UI.WebControls.DataGridItem).ItemIndex
        Dim lblAmount, lblOS, lblInt As New Label

        If chkSender.Checked = True Then
            chkItem = CType(Datagrid1.Items(index).FindControl("chkPrint"), CheckBox)
            chkItem.Checked = True
        Else
            chkItem = CType(Datagrid1.Items(index).FindControl("chkPrint"), CheckBox)
            chkItem.Checked = False
        End If

        Total = 0
        TotalOS = 0
        totalInt = 0

        Dim x As Integer
        For x = 0 To Datagrid1.Items.Count - 1

            chkItem = CType(Datagrid1.Items(x).FindControl("chkPrint"), CheckBox)
            lblAmount = CType(Datagrid1.Items(x).Cells(4).FindControl("lblInstallmentAmount"), Label)
            lblOS = CType(Datagrid1.Items(x).Cells(5).FindControl("lblPrincipalAmount"), Label)
            lblInt = CType(Datagrid1.Items(x).Cells(6).FindControl("lblInterestAmount"), Label)

            If chkItem.Checked = True Then
                Total += CDbl(lblAmount.Text)
                TotalOS += CDbl(lblOS.Text)
                TotalInt += CDbl(lblInt.Text)
            Else
                Total += CDbl("0.00")
                TotalOS += CDbl("0.00")
                TotalInt += CDbl("0.00")

            End If

        Next

        ScriptManager.RegisterStartupScript(Datagrid1, GetType(DataGrid), Datagrid1.ClientID, String.Format("total('{0}','{1}','{2}','{3}'); ", Datagrid1.ClientID, Total, TotalOS, TotalInt), True)
    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim lblAmount, lblOS, lblInt As New Label

        Dim x As Integer
        For x = 0 To Datagrid1.Items.Count - 1

            chkItem = CType(Datagrid1.Items(x).FindControl("chkPrint"), CheckBox)
            lblAmount = CType(Datagrid1.Items(x).Cells(4).FindControl("lblInstallmentAmount"), Label)
            lblOS = CType(Datagrid1.Items(x).Cells(5).FindControl("lblPrincipalAmount"), Label)
            lblInt = CType(Datagrid1.Items(x).Cells(6).FindControl("lblInterestAmount"), Label)

            chkItem.Checked = chkSender.Checked

            'If chkSender.Checked = True Then
            '    Total += CDbl(lblAmount.Text)
            'Else
            '    Total += CDbl("0.00")
            'End If


            If chkSender.Checked = True Then
                Total += CDbl(lblAmount.Text)
                TotalOS += CDbl(lblOS.Text)
                TotalInt += CDbl(lblInt.Text)
            Else
                Total += CDbl("0.00")
                TotalOS += CDbl("0.00")
                TotalInt += CDbl("0.00")

            End If
        Next

        'ScriptManager.RegisterStartupScript(Datagrid1, GetType(DataGrid), Datagrid1.ClientID, String.Format("total('{0}','{1}'); ", Datagrid1.ClientID, Total), True)
        ScriptManager.RegisterStartupScript(Datagrid1, GetType(DataGrid), Datagrid1.ClientID, String.Format("total('{0}','{1}','{2}','{3}'); ", Datagrid1.ClientID, Total, TotalOS, TotalInt), True)


    End Sub

    Private Sub bindWayOfPayment()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'only bank account
        strListData = "BA,Bank"
        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbWayOfPayment.DataValueField = "ID"
        cmbWayOfPayment.DataTextField = "Description"
        cmbWayOfPayment.DataSource = oDataTable
        cmbWayOfPayment.DataBind()
        cmbWayOfPayment.Items.Insert(0, "Select One")
        cmbWayOfPayment.Items(0).Value = "0"
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If Not getSelectedRow() Then Exit Sub
        oBGNODate.BindBGNODate(" bankaccountid = '" & cmbBankAccount.SelectedValue.Trim & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
        If cmbWayOfPayment.SelectedValue = "BA" Then
            oBGNODate.Visible = True
        Else
            oBGNODate.Visible = False
        End If
        pnlTop.Visible = False
        pnlView.Visible = False
        pnlTransfer.Visible = True
    End Sub

    Private Sub cmbWayOfPayment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWayOfPayment.SelectedIndexChanged
        bindBankAccount(cmbWayOfPayment.SelectedValue)
        If cmbWayOfPayment.SelectedValue = "BA" Then
            oBGNODate.Visible = True
        Else
            oBGNODate.Visible = False
        End If
    End Sub

    Private Sub bindBankAccount(ByVal strBankType As String)

        Dim dtBankAccountCache As New DataTable
        dtBankAccountCache = m_controller.GetBankAccount(GetConnectionString, Me.sesBranchId.Trim, strBankType, "")
        With cmbBankAccount
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankAccountCache
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlTop.Visible = True
        pnlView.Visible = True
        pnlTransfer.Visible = False
    End Sub

    Private Function getSelectedRow() As Boolean
        Dim iLoop As Integer
        Dim chkPrint As CheckBox
        Dim lblPrincipalAmount As New Label
        Dim lblInterestAmount As New Label
        Dim lblInstallmentAmount As New Label
        Dim lblApplicationID As New Label
        Dim lblRefundInterest As New Label
        Dim strApplicationID As String = ""
        lblAmountRec.Text = 0
        Dim dblPrincipalPaidAmount As Double
        Dim dblInterestPaidAmount As Double
        Dim dblRefundInterestAmount As Double

        For iLoop = 0 To Datagrid1.Items.Count - 1
            chkPrint = CType(Datagrid1.Items(iLoop).Cells(0).FindControl("chkPrint"), CheckBox)
            lblPrincipalAmount = CType(Datagrid1.Items(iLoop).Cells(5).FindControl("lblPrincipalAmount"), Label)
            lblInterestAmount = CType(Datagrid1.Items(iLoop).Cells(6).FindControl("lblInterestAmount"), Label)
            lblInstallmentAmount = CType(Datagrid1.Items(iLoop).Cells(4).FindControl("lblInstallmentAmount"), Label)
            lblApplicationID = CType(Datagrid1.Items(iLoop).Cells(1).FindControl("lblApplicationId"), Label)
            lblRefundInterest = CType(Datagrid1.Items(iLoop).Cells(7).FindControl("lblRefundInterest"), Label)

            If chkPrint.Checked Then
                dblPrincipalPaidAmount += CDbl(lblPrincipalAmount.Text)
                dblInterestPaidAmount += CDbl(lblInterestAmount.Text)
                dblRefundInterestAmount += CDbl(lblRefundInterest.Text)
                If strApplicationID.Trim = "" Then
                    strApplicationID = lblApplicationID.Text.Trim
                Else
                    strApplicationID = strApplicationID & "," & lblApplicationID.Text.Trim
                End If
            End If
        Next

        Me.SelectedApplicationIDs = strApplicationID
        Me.PrincipalPaidAmount = dblPrincipalPaidAmount
        Me.InterestPaidAmount = dblInterestPaidAmount
        Me.RefundInterest = dblRefundInterestAmount
        lblAmountRec.Text = FormatNumber(Me.PrincipalPaidAmount + Me.InterestPaidAmount - _
        Me.RefundInterest, 2)

        If CDbl(lblAmountRec.Text) = 0 Then

            ShowMessage(lblMessage, "Harap pilih baris untuk proses", True)
            Return False
        Else
            lblMessage.Text = ""
            Return True
        End If
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oCustomClass1 As New Parameter.DisbursePembayaranAngsuranFunding
        With oCustomClass1
            .strConnection = GetConnectionString()
            .BranchId = Replace(sesBranchId, "'", "").Trim
            .LoginId = Me.Loginid
            .CompanyId = SesCompanyID
            .BusinessDate = Me.BusinessDate
            .FundingCoyID = drdCompany.SelectedValue.Trim
            .FundingContractNo = drdContract.SelectedValue.Trim
            .PrincipalPaidAmount = Me.PrincipalPaidAmount
            .InterestPaidAmount = Me.InterestPaidAmount - Me.RefundInterest
            .PenaltyPaidAmount = 0
            .BankAccountId = cmbBankAccount.SelectedValue
            .ValueDate = ConvertDate2(txtAgingDate.Text)
            .ReferenceNo = txtReferenceNo.Text.Trim
            .PPHPaid = 0
            .SelectedApplicationIDs = Me.SelectedApplicationIDs
        End With
        Try
            oController.DisbursePembayaranAngsuranFunding(oCustomClass1)
            pnlTop.Visible = True
            pnlView.Visible = False
            pnlTransfer.Visible = False
            'directPrint()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub directPrint()
        Dim cookie As HttpCookie = Request.Cookies("BonHijauFundingDisburse")

        If Not cookie Is Nothing Then
            cookie.Values("referenceNo") = txtReferenceNo.Text.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("BonHijauFundingDisburse")

            cookieNew.Values.Add("referenceNo", txtReferenceNo.Text.Trim)
            Response.AppendCookie(cookieNew)
        End If

        Response.Redirect("BonHijauFundingDisbViewer.aspx")
    End Sub



    Private Sub Datagrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Datagrid1.ItemDataBound
        'Dim chkItem As CheckBox
        'Dim lblAmount As New Label
        Dim lblSum, lblSumOS, lblSumInt As New Label

        Dim hyagreementno As HyperLink
        Dim hypCustomerx As HyperLink
        Dim lblCustimerid As New Label

        Dim instyle As String = "AccMnt"

        If e.Item.ItemIndex >= 0 Then
            '    chkItem = CType(e.Item.FindControl("chkPrint"), CheckBox)
            '    lblAmount = CType(e.Item.FindControl("lblInstallmentAmount"), Label)

            '    If chkItem.Checked = True Then
            '        Total += CDbl(lblAmount.Text)
            '    End If
            hyagreementno = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hypCustomerx = CType(e.Item.FindControl("hypCustomerx"), HyperLink)
            lblCustimerid = CType(e.Item.FindControl("lblCustomerIDx"), Label)

            hyagreementno.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyagreementno.Text.Trim) & "')"
            hypCustomerx.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustimerid.Text.Trim) & "')"


            Dim lblAmount = CType(e.Item.FindControl("lblInstallmentAmount"), Label)
            Dim lblPrincipal = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            Dim lblInterest = CType(e.Item.FindControl("lblInterestAmount"), Label)
            TotalAmount += CDbl(lblAmount.Text)
            TotalPrincipal += CDbl(lblPrincipal.Text)
            TotalInterest += CDbl(lblInterest.Text)

        End If



        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSumOS = CType(e.Item.FindControl("lblSumOS"), Label)
            lblSumInt = CType(e.Item.FindControl("lblSumInt"), Label)

            lblSum.Text = FormatNumber(TotalAmount.ToString, 2)
            lblSumOS.Text = FormatNumber(TotalPrincipal.ToString, 2)
            lblSumInt.Text = FormatNumber(TotalInterest.ToString, 2)
        End If

    End Sub
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Response.Redirect("FundingInstallmentPayment.aspx")
    End Sub

    Private Sub cmbBankAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBankAccount.SelectedIndexChanged
        oBGNODate.BindBGNODate(" bankaccountid = '" & cmbBankAccount.SelectedValue.Trim & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim oClass As New Parameter.ControlsRS
        Dim lblApplicationID As Label
        Dim lblInsSeqno As Label
        Dim lblFundingContractID As Label
        Dim lblFundingBatchID As Label
        Dim hyAgreementNo As HyperLink
        Dim chkDtList As CheckBox
        Dim oController As New PembayaranAngsuranFundingController
        Dim strApplicationID As String = ""
        Dim strAgreementNo As String = ""
        Dim strInsSeqno As String = ""
        Dim strFundingContractID As String = ""
        Dim strFundingBatchID As String = ""
        'Dim pstrFile As String
        Dim dt As DataTable = createNewDT()

        For i = 0 To Datagrid1.Items.Count - 1
            chkDtList = CType(Datagrid1.Items(i).FindControl("chkPrint"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lblApplicationID = CType(Datagrid1.Items(i).FindControl("lblApplicationId"), Label)
                    hyAgreementNo = CType(Datagrid1.Items(i).FindControl("hyAgreementNo"), HyperLink)
                    lblInsSeqno = CType(Datagrid1.Items(i).FindControl("Label1"), Label)
                    lblFundingContractID = CType(Datagrid1.Items(i).FindControl("Label7"), Label)
                    lblFundingBatchID = CType(Datagrid1.Items(i).FindControl("Label8"), Label)
                    strApplicationID &= CStr(IIf(strApplicationID = "", "", ",")) & lblApplicationID.Text.Trim.Replace("'", "").Replace(",", "")
                    strAgreementNo &= CStr(IIf(strAgreementNo = "", "", ",")) & hyAgreementNo.Text.Trim.Replace("'", "").Replace(",", "")
                    strInsSeqno &= CStr(IIf(strInsSeqno = "", "", ",")) & lblInsSeqno.Text.Trim.Replace("'", "").Replace(",", "")
                    strFundingContractID &= CStr(IIf(strFundingContractID = "", "", ",")) & lblFundingContractID.Text.Trim.Replace("'", "").Replace(",", "")
                    strFundingBatchID &= CStr(IIf(strFundingBatchID = "", "", ",")) & lblFundingBatchID.Text.Trim.Replace("'", "").Replace(",", "")
                    ' dt.Rows.Add((i + 1).ToString, CStr(lblApplicationID.Text), CInt(lblInsSeqno.Text))
                End If
            End If
        Next



        With oClass

            Try

                .SelectedBranch = ""
                .AreaID = ""
                .RekeningBankAccount = ""
                .NamaKonsumen = ""
                '.ApplicationID = ""
                If txtAgingDate.Text.Trim <> "" Then
                    .StartDate = ConvertDate2(txtAgingDate.Text).ToString("MM/dd/yyyy")
                End If
                If txtAgingDateto.Text.Trim <> "" Then
                    .EndDate = ConvertDate2(txtAgingDateto.Text).ToString("MM/dd/yyyy")
                End If

            Catch ex As Exception

            End Try

            .BankCompany = drdCompany.SelectedValue
            '.Kontrak = drdContract.SelectedValue
            '.NoBatch = txtBatchNo.Text.Trim ' cboNoBatch.SelectedValue
            '.ApplicationID = strApplicationID
            .NoBatch = strFundingBatchID
            .Kontrak = strAgreementNo
            .FundingContractNo = strFundingContractID
            .InsSeqno = strInsSeqno
            .ListData = dt
        End With

        Dim cookie As New HttpCookie("FUNDCOOKIE")


        cookie.Values.Add("StartDate", oClass.StartDate)
        cookie.Values.Add("EndDate", oClass.EndDate)
        cookie.Values.Add("NoBatch", txtBatchNo.Text.Trim)
        cookie.Values.Add("BankCompany", drdCompany.SelectedValue)
        cookie.Values.Add("Kontrak", oClass.Kontrak)
        cookie.Values.Add("InsSeqno", oClass.InsSeqno)
        cookie.Values.Add("FundingContractNo", drdContract.SelectedValue)


        cookie.Expires = DateTime.Now.AddHours(1)
        Response.Cookies.Add(cookie)



        'Response.Cookies.Clear()
        'Response.AppendCookie(Webform.UserController.RSConfig1.setCookies(Request.Cookies("RSCOOKIES"), oClass))


        'Dim strReportFile As String = "../../Webform.Reports/ucRSViewerPopup.aspx?formid=ANGSJATTEMP&rsname=/BataviaReport/FundingMgt/DaftarAngsuranPerjatuhTempo"
        'Response.Write("<script language = javascript>" & vbCrLf _
        '                & "var x = screen.width; " & vbCrLf _
        '                & "var y = screen.height; " & vbCrLf _
        '                   & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
        '                   & "</script>")


        '  Response.Redirect("rptPaymentRequestViewer.aspx?RedirectFiles=PaymentRequestMulti&RequestNo=" & strRequestNo & "&BranchID=" & Me.sesBranchId.Replace("'", "") & "")
        'Dim link As String = "../../Webform.Reports.RDLC/Funding/FundingJatuhTempoViewer.aspx?StartDate=" & oClass.StartDate & "&EndDate=" & oClass.EndDate & "&NoBatch=" & oClass.NoBatch & "&Bank=" & oClass.BankCompany & "&Kontrak=" & oClass.Kontrak 
        Dim link As String = "../../Webform.Reports.RDLC/Funding/FundingJatuhTempoViewer.aspx?"
        Response.Write(String.Format("<script language = javascript>{0} var x = screen.width;{0} var y = screen.height;{0} window.open('{1}','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') {0} </script>", vbCrLf, link))

        'Server.Transfer("FundingInsatllmentPayment.aspx")

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Not getSelectedRow() Then Exit Sub
        Dim lblApplicationID As Label
        Dim chkDtList As CheckBox
        Dim oController As New PembayaranAngsuranFundingController
        Dim oCustomClass1 As New Parameter.FundingContractBatch

        Dim dt As DataTable = createNewDT()

        For i = 0 To Datagrid1.Items.Count - 1
            chkDtList = CType(Datagrid1.Items(i).FindControl("chkPrint"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lblApplicationID = CType(Datagrid1.Items(i).FindControl("lblApplicationId"), Label)
                    dt.Rows.Add((i + 1).ToString, lblApplicationID.Text.Trim, String.Empty)
                End If
            End If
        Next


        oCustomClass1.Listdata = dt
        oCustomClass1.strConnection = GetConnectionString()

        Try
            oCustomClass1 = oController.RemoveSelectionAgreementBatch(oCustomClass1)
            DoBind(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")

        Return dt
    End Function
End Class