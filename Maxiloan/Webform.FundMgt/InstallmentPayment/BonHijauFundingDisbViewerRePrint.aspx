<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BonHijauFundingDisbViewerRePrint.aspx.vb" Inherits="Maxiloan.Webform.FundMgt.BonHijauFundingDisbViewerRePrint" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>BonHijauFundingDisbViewerRePrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../Include/channeling.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		</script>
		<SCRIPT src="../../Maxiloan.js"></SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:TextBox id="txtReferenceNo" runat="server"></asp:TextBox>
			<asp:Button id="btnPrint" runat="server" Text="Print"></asp:Button>
		</form>
	</body>
</HTML>
