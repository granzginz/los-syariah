﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentOutBatchDueDate.aspx.vb" Inherits="Maxiloan.Webform.FundMgt.PaymentOutBatchDueDate" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PAYMENT OUT BATCH DUE DATE</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnltop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PEMBAYARAN ANGSURAN PER BATCH PER JATUH TEMPO
                </h3>
            </div>
        </div>        
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:DropDownList ID="drdCompany" runat="server" AutoPostBack="True">
                </asp:DropDownList>                
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No. Fasilitas</label>
                <asp:DropDownList ID="drdContract" runat="server" Width="150px">
                </asp:DropDownList>                
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Jatuh Tempo Batch</label>                
                <uc1:ucdatece id="txtAgingDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="true" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="true" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnllist" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR ANGUSRAN BATCH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general" >
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="APPLICATIONID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Enabled="True" Text='<%# Container.dataitem("ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="FundingCoyName" HeaderText="FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FundingContractNo" HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FundingBatchNo" HeaderText="NO BATCH" >
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InstallmentDueDate" HeaderText="DUE DATE" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="NO FASILITAS" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                            <asp:TemplateColumn HeaderText="CustomerID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Enabled="True" Text='<%# Container.dataitem("CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA CUSTOMER" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCust" runat="server" Enabled="True" Text='<%# Container.dataitem("Name") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                            <asp:BoundColumn DataField="FundingCoyId" HeaderText="FUNDINGCOYID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>                                                        
                            <asp:BoundColumn DataField="OSAR" HeaderText="SISA A/R" DataFormatString="{0:N0}" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OUTSTANDINGPRINCIPAL" HeaderText="SISA POKOK" DataFormatString="{0:N0}" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="NAMA ASSET" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="BranchID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Enabled="True" Text='<%# Container.dataitem("BranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="InstallmentPaid" HeaderText="INSTALLMENT PAID">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>                                                        
                            <asp:BoundColumn DataField="BankName" HeaderText="BANKNAME" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>                            
                            <asp:BoundColumn DataField="BankID" HeaderText="BANKID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ContractName" HeaderText="FASILITASNAME" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="LastPrincipalPaid" HeaderText="POKOK" DataFormatString="{0:N0}" >
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrincipalDue" HeaderText="MARGIN" DataFormatString="{0:N0}" >
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="BAYAR">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbPrepayment" runat="server" ImageUrl="../../Images/iconPrepayment.gif"
                                        CommandName="Prepayment" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>                                                                                    
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
