﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentOutCompany
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private oCompany As New Parameter.FundingCompany
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If
        lbltotrec.Text = recordCount.ToString
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True

    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True

    End Sub

#End Region

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property BankId() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region "FormLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        InitialDefaultPanel()

        If Not Me.IsPostBack Then

            Me.FormID = "PAYOUT"

            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""



            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True

        End If
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable


        With oCompany
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCompany = m_Company.ListFundingCompany(oCompany)

        With oCompany
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCompany.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else

        End If

        dtgFundingCoList.DataSource = dtvEntity
        Try
            dtgFundingCoList.DataBind()
        Catch
            dtgFundingCoList.CurrentPageIndex = 0
            dtgFundingCoList.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

#Region "Handles"

    Private Sub BtnResetnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetnew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub BtnSearchnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchnew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
            If StrSearchByValue = "" Then
                Me.SearchBy = "all"
                Me.SortBy = ""
            Else
                If Right(TxtSearchByValue.Text.Trim, 1) = "%" Then
                    Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim + " Like '" + TxtSearchByValue.Text.Trim + "'"
                Else
                    Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim + "='" + TxtSearchByValue.Text.Trim + "'"
                End If

            End If
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub
    Private Sub dtgFundingCoList_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingCoList.ItemCommand
        Select Case e.CommandName
            Case "Contract"
                Me.CompanyID = e.Item.Cells(0).Text
                Me.CompanyName = e.Item.Cells(2).Text
                Me.BankName = e.Item.Cells(3).Text
                Me.BankId = e.Item.Cells(5).Text
                Response.Redirect("PaymentOutContract.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankId)
        End Select
    End Sub

    Private Sub dtgFundingCoList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingCoList.ItemDataBound
        Dim hyBranch As HyperLink
        If e.Item.ItemIndex >= 0 Then
            Me.CompanyID = e.Item.Cells(0).Text
            hyBranch = CType(e.Item.FindControl("lnkCompanyID"), HyperLink)
            hyBranch.NavigateUrl = "javascript:OpenFundingCompanyView('" & "channeling" & "', '" & Me.CompanyID & "')"
        End If
    End Sub
#End Region

End Class