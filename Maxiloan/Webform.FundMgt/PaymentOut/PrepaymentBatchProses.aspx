﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrepaymentBatchProses.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.PrepaymentBatchProses" %>

<%@ Register TagPrefix="uc1" TagName="UcBankMaster" Src="../../webform.UserController/UcBankMaster.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNumber" Src="../../Webform.UserController/ucBGNumber.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
   <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">  
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress> 
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:UpdatePanel runat="server" ID="upnl1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"/>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            PENGAJUAN PELUNASAN KE BANK</h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Bank
                        </label>
                        <asp:DropDownList ID="drdCompany" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="fa.ApplicationID">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">Tanggal Effektif</label>
                         <uc1:ucdatece id="txtEffDate" runat="server"></uc1:ucdatece>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR KONTRAK</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtg1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" DataKeyField="Name" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>
                                            <asp:Button ID="AddButton" runat="server" Text="Add" CommandName="Add" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ApplicationID" HeaderText="NAMA" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AgreementNo" HeaderText="NO KONTRAK"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Name" HeaderText="NAMA"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Tenor" HeaderText="Tenor" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsSeqNo" HeaderText="ANGS. KE"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OSP" HeaderText="SISA POKOK" DataFormatString="{0:N0}" HeaderStyle-CssClass="item_grid_right" ItemStyle-CssClass="item_grid_right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OSI" HeaderText="SISA MARGIN" DataFormatString="{0:N0}" HeaderStyle-CssClass="item_grid_right" ItemStyle-CssClass="item_grid_right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Total" HeaderText="TOTAL" DataFormatString="{0:N0}" HeaderStyle-CssClass="item_grid_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FundingContractNo" HeaderText="FASILITAS"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FundingBatchNo" HeaderText="BATCH"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FundingCoyID" HeaderText="FASILITAS" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankID" HeaderText="BankID" Visible="false"></asp:BoundColumn>                                    
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                                    ErrorMessage="No Halaman Salah" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR KONTRAK AKAN DILUNASI</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtg2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" DataKeyField="Name" BorderStyle="None" ShowFooter="True"
                                BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkCheck" runat="server" Checked ="true" Enabled="false" ></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ApplicationID" HeaderText="NAMA" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AgreementNo" HeaderText="NO KONTRAK"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Name" HeaderText="NAMA"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Tenor" HeaderText="Tenor" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsSeqNo" HeaderText="ANGS. KE"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OSP" HeaderText="SISA POKOK" DataFormatString="{0:N0}" HeaderStyle-CssClass="item_grid_right" ItemStyle-CssClass="item_grid_right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OSI" HeaderText="SISA MARGIN" DataFormatString="{0:N0}" HeaderStyle-CssClass="item_grid_right" ItemStyle-CssClass="item_grid_right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Total" HeaderText="TOTAL" DataFormatString="{0:N0}" HeaderStyle-CssClass="item_grid_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FundingContractNo" HeaderText="FASILITAS"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FundingBatchNo" HeaderText="BATCH"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FundingCoyID" HeaderText="FASILITAS" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankID" HeaderText="BankID" Visible="false"></asp:BoundColumn>
                                     <asp:TemplateColumn HeaderText="Remove">
                                        <ItemTemplate>
                                           <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"  CommandName="Remove" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnNext" runat="server" Text="Next" CssClass="small button blue">
                    </asp:Button>                    
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" Visible="false">
                <div class="form_title">
                    <div class="form_single">
                        <h3>
                            DETAIL PEMBAYARAN BANK</h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Bukti Kas</label>
                        <asp:Label ID="lblNoBukti" runat="server"></asp:Label>
                        <asp:TextBox ID="txtNoBukti" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNoBukti" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Bank
                        </label>
                        <uc1:ucbankaccountid id="oBankAccount" runat="server">
                        </uc1:ucbankaccountid>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Valuta</label>
                         <uc1:ucdatece id="txtValDate" runat="server"></uc1:ucdatece>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah
                        </label>
                        <uc1:ucnumberformat id="txtJumlah" runat="server" />
                    </div>
                </div>                
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Catatan</label>
                        <asp:TextBox ID="txtCatatan" runat="server" TextMode="MultiLine" class="multiline_textbox"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonBack" runat="server" Text="Back" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
