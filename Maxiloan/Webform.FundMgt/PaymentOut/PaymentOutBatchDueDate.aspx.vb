﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentOutBatchDueDate
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "uccomponent"

    Protected WithEvents txtAgingDate As ucDateCE    
#End Region

#Region " Declares"    
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1    
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PembayaranAngsuranFunding    
    Private oController As New PembayaranAngsuranFundingController
    Private dtBankAccount As New DataTable
    Private m_controller As New DataUserControlController        
#End Region

#Region " Property"
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Private Property cmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Me.IsPostBack Then
            Me.FormID = "PAYOUTBATCHDD"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If

            bindFundingCompany()

            If Request.QueryString("Flag") = "1" Then                
                drdCompany.SelectedValue = Request.QueryString("Filter1").ToString()
                drdContract.SelectedValue = Request.QueryString("Filter2").ToString()
                txtAgingDate.Text = Request.QueryString("Filter3").ToString()

                Dim DueDate As Date
                DueDate = ConvertDate2(txtAgingDate.Text).ToString("yyyy/MM/dd")

                If drdCompany.SelectedIndex > 0 And drdContract.SelectedIndex > 0 Then
                    Me.cmdWhere = "FundingContract.FundingCoyId = '" + drdCompany.SelectedValue + "' and FundingContract.FundingContractNo = '" + drdContract.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
                ElseIf drdCompany.SelectedIndex > 0 And drdContract.SelectedIndex = 0 Then
                    Me.cmdWhere = "FundingContract.FundingCoyId = '" + drdCompany.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
                ElseIf drdCompany.SelectedIndex = 0 And drdContract.SelectedIndex > 0 Then
                    Me.cmdWhere = "FundingContract.FundingContractNo = '" + drdContract.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
                Else
                    Me.cmdWhere = "FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
                End If

                BindGridEntity(Me.cmdWhere)                
            Else
                txtGoPage.Text = "1"
                txtAgingDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")                
                pnllist.Visible = False                
            End If

        End If
    End Sub

    Private Sub bindFundingCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"
            drdCompany.DataSource = dtvEntity
            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "Select All")
            drdCompany.Items(0).Value = "0"
        Catch e As Exception

            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub bindFundindContract()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " where FundingCoyID = '" & drdCompany.SelectedValue.Trim & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingContractList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdContract.DataValueField = "FundingContractNo"
            drdContract.DataTextField = "ContractName"
            drdContract.DataSource = dtvEntity
            drdContract.DataBind()
            drdContract.Items.Insert(0, "Select All")
            drdContract.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub drdCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drdCompany.SelectedIndexChanged
        bindFundindContract()

        Dim DueDate As Date
        DueDate = ConvertDate2(txtAgingDate.Text).ToString("yyyy/MM/dd")

        If drdCompany.SelectedIndex > 0 And drdContract.SelectedIndex > 0 Then
            Me.cmdWhere = "FundingContract.FundingCoyId = '" + drdCompany.SelectedValue + "' and FundingContract.FundingContractNo = '" + drdContract.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        ElseIf drdCompany.SelectedIndex > 0 And drdContract.SelectedIndex = 0 Then
            Me.cmdWhere = "FundingContract.FundingCoyId = '" + drdCompany.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        ElseIf drdCompany.SelectedIndex = 0 And drdContract.SelectedIndex > 0 Then
            Me.cmdWhere = "FundingContract.FundingContractNo = '" + drdContract.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        Else
            Me.cmdWhere = "FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        End If

        BindGridEntity(Me.cmdWhere)
        pnllist.Visible = True
    End Sub

    Sub BindGridEntity(ByVal cmdwhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingPaymentOutBatchDUeDateList"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)        
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.CmdWhere)
                End If
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub

#End Region

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click

        Dim DueDate As Date
        DueDate = ConvertDate2(txtAgingDate.Text).ToString("yyyy/MM/dd")

        If drdCompany.SelectedIndex > 0 And drdContract.SelectedIndex > 0 Then
            Me.cmdWhere = "FundingContract.FundingCoyId = '" + drdCompany.SelectedValue + "' and FundingContract.FundingContractNo = '" + drdContract.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        ElseIf drdCompany.SelectedIndex > 0 And drdContract.SelectedIndex = 0 Then
            Me.cmdWhere = "FundingContract.FundingCoyId = '" + drdCompany.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        ElseIf drdCompany.SelectedIndex = 0 And drdContract.SelectedIndex > 0 Then
            Me.cmdWhere = "FundingContract.FundingContractNo = '" + drdContract.SelectedValue + "' and FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        Else
            Me.cmdWhere = "FundingBatch.InstallmentDueDate = CONVERT(smalldatetime, '" + DueDate + "')"
        End If

        BindGridEntity(Me.cmdWhere)
        pnllist.Visible = True
    End Sub

    Private Sub BtnResetAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PaymentOutBatchDueDate.aspx")
    End Sub

    Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        Dim LblApplicationId As Label
        Dim LBlBranchID As Label
        LblApplicationId = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label)
        LBlBranchID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)

        Dim FundingCoyId As String
        Dim BankName As String
        Dim FundingCoyName As String
        Dim BankId As String
        Dim FundingContractNo As String
        Dim ContractName As String
        Dim FundingBatchNo As String
        Dim a As String
        Dim b As String
        Dim c As String
        Dim d As String

        If e.CommandName = "Prepayment" Then
            FundingCoyId = e.Item.Cells(8).Text.Trim
            BankName = e.Item.Cells(14).Text.Trim
            FundingCoyName = e.Item.Cells(1).Text.Trim
            BankId = e.Item.Cells(15).Text.Trim
            FundingContractNo = e.Item.Cells(2).Text.Trim
            ContractName = e.Item.Cells(16).Text.Trim
            FundingBatchNo = e.Item.Cells(3).Text.Trim

            Response.Redirect("PaymentOutInstallment.aspx?CompanyID=" & FundingCoyId & "&CompanyName=" & FundingCoyName & "&BankName=" & BankName & "&BankId=" & BankId & "&FundingContractID=" & FundingContractNo & "&ContractName=" & ContractName & "&ContractBatchNo=" & FundingBatchNo & "&Flag=PAYOUTBATCHDD&Filter1=" & drdCompany.SelectedValue.ToString() & "&Filter2=" & drdContract.SelectedValue.ToString() & "&Filter3=" & txtAgingDate.Text.Trim)
        End If

    End Sub

End Class