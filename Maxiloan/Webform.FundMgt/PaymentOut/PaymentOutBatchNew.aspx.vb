﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class PaymentOutBatchNew
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region
#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region
#Region "FormLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'InitialDefaultPanel()
        If Not Me.IsPostBack Then
            Me.FormID = "PAYOUT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""


            bindFundingCompany()
            pnlList.Visible = False
        End If
    End Sub
#End Region
    Private Sub drdCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drdCompany.SelectedIndexChanged
        bindFundindContract()
    End Sub
    Private Sub bindFundingCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"
            drdCompany.DataSource = dtvEntity
            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "Select One")
            drdCompany.Items(0).Value = "0"
        Catch e As Exception

            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub bindFundindContract()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " where FundingCoyID = '" & drdCompany.SelectedValue.Trim & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingContractList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdContract.DataValueField = "FundingContractNo"
            drdContract.DataTextField = "ContractName"
            drdContract.DataSource = dtvEntity
            drdContract.DataBind()
            drdContract.Items.Insert(0, "--All--")
            drdContract.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString
        ' pnlList.Visible = True


        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        'pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        'pnlList.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        'pnlList.Visible = True
    End Sub

#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere ' "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingPaymentOutBatchList"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractBatch.DataSource = dtvEntity
        Try
            dtgFundingContractBatch.DataBind()
        Catch
            dtgFundingContractBatch.CurrentPageIndex = 0
            dtgFundingContractBatch.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
    Private Sub dtgFundingContractBatch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractBatch.ItemCommand
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim lnkbatchNo As HyperLink

            Dim hdncompanyID As New Label
            Dim hdncompanyName As New Label
            Dim hdnbankName As New Label
            Dim hdnbankId As New Label
            Dim hdnFundingContractNo As New Label
            Dim hdnContractName As New Label


            lnkbatchNo = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("lnkFundingBatchNo"), HyperLink)
            Me.FundingBatchNo = lnkbatchNo.Text.Trim

            hdncompanyID = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("hdnCompanyId"), Label)
            Me.CompanyID = hdncompanyID.Text.Trim

            hdncompanyName = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("hdnCompanyName"), Label)
            Me.CompanyName = hdncompanyName.Text.Trim

            hdnbankName = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("hdnBankName"), Label)
            Me.BankName = hdnbankName.Text.Trim

            hdnbankId = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("hdnBankID"), Label)
            Me.BankID = hdnbankId.Text.Trim

            hdnFundingContractNo = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("hdnFundingContractNo"), Label)
            Me.FundingContractNo = hdnFundingContractNo.Text.Trim

            hdnContractName = CType(dtgFundingContractBatch.Items(e.Item.ItemIndex).FindControl("hdnContractName"), Label)
            Me.ContractName = hdnContractName.Text.Trim


            Select Case e.CommandName
                Case "Installment"
                    Response.Redirect("PaymentOutInstallment.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractID=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&ContractBatchNo=" & Me.FundingBatchNo)
                Case "Prepayment"
                    Response.Redirect("PaymentOutPrepayment.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractID=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&ContractBatchNo=" & Me.FundingBatchNo)
                Case "Fees"
                    Response.Redirect("PaymentOutFees.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractID=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&ContractBatchNo=" & Me.FundingBatchNo)
            End Select
        End If
    End Sub
    Private Sub dtgFundingContractBatch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractBatch.ItemDataBound
        Dim imbDelete As ImageButton
        'Dim lbCompanyID As LinkButton
        Dim lbl1, lblStatus As Label
        Dim imbEdit As ImageButton
        Dim hyBatchNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lbl1 = CType(e.Item.FindControl("lbl1"), Label)
            lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
            imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)
            hyBatchNo = CType(e.Item.FindControl("lnkFundingBatchNo"), HyperLink)
            ' hyBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & hyBatchNo.Text.Trim & "')"
            If lbl1.Text.Trim = "L" Then
                lbl1.Text = "Floating"
            ElseIf lbl1.Text.Trim = "X" Then
                lbl1.Text = "Fixed"
            Else
                lbl1.Text = "Fixed Per Batch"
            End If
        End If
    End Sub
    Private Sub btnBacknew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBacknew.Click
        Response.Redirect("PaymentOutContract.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID)
    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        Me.SearchBy = "FundingBatch.FundingCoyId='" & drdCompany.SelectedValue.Trim & "'"
        If drdContract.SelectedValue.Trim <> "0" Then
            Me.SearchBy = Me.SearchBy + " and FundingBatch.FundingContractNo = '" & drdContract.SelectedValue.Trim & "'"
        End If
        If txtBatchNo.Text <> "" Then
            Me.SearchBy = Me.SearchBy + " and FundingBatch.FundingBatchNo like '%" & txtBatchNo.Text.Trim & "%'  "
        End If


        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub
End Class