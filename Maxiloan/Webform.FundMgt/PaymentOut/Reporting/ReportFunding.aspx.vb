﻿Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class ReportFunding
    Inherits Maxiloan.Webform.WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            Dim oTSData As New Parameter.GeneralPaging
            oTSData.strConnection = GetConnectionString()
            oTSData.FundingBatchNo = Request("FundingBatchNo")
            oTSData.FundingID = Request("FundingID")
            oTSData.FasilitasNo = Request("FasilitasNo")
            oTSData.SeqNo = Request("SeqNo")
            'oTSData. = Request("brc")
            Dim reportPath As String
            reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.FundMgt\PaymentOut\Reporting\PrintPengeluaranBank.rdlc"

            Dim datasource As ReportDataSource = New ReportDataSource("DataSet1", ListPrintTS(oTSData).ListData)


            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.ReportPath = reportPath
            ReportViewer1.LocalReport.DataSources.Add(datasource)
            ReportViewer1.LocalReport.Refresh()
            Dim reportType As String = "pdf"
            Dim mimeType As String
            Dim encoding As String
            Dim fileNameExtension As String



            ''11.69in
            Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>8.27in</PageWidth>" + "  <PageHeight>15.69in</PageHeight>" + "  <MarginTop>0.19685in</MarginTop>" + "  <MarginLeft>0.19685in</MarginLeft>" + "  <MarginRight>0.19685in</MarginRight>" + "  <MarginBottom>0.19685in</MarginBottom>" + "</DeviceInfo>"
            ' Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>8in</PageWidth>" + "  <PageHeight>5in</PageHeight>" + "  <MarginTop>0in</MarginTop>" + "  <MarginLeft>0.5in</MarginLeft>" + "  <MarginRight>0.5in</MarginRight>" + "  <MarginBottom>0in</MarginBottom>" + "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streams As String()
            Dim renderedBytes As Byte()
            renderedBytes = ReportViewer1.LocalReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)
            Response.Clear()

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", "inline; filename=monthlyBill.pdf")
            Response.BinaryWrite(renderedBytes)

            Response.End()
        End If

    End Sub
    Public Function ListPrintTS(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 50)
        params(0).Value = customclass.FundingBatchNo
        params(1) = New SqlParameter("@SeqNo", SqlDbType.Int)
        params(1).Value = customclass.SeqNo
        params(2) = New SqlParameter("@FundingID", SqlDbType.VarChar, 50)
        params(2).Value = customclass.FundingID
        params(3) = New SqlParameter("@FasilitasNo", SqlDbType.VarChar, 50)
        params(3).Value = customclass.FasilitasNo

        oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "SP_PrintPengeluaranBank", params).Tables(0)
        Return oReturnValue


        'Public Function SPPrintBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        'Dim params() As SqlParameter = New SqlParameter(3) {}
        'params(0) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 50)
        'params(0).Value = customclass.FundingBatchNo
        'params(1) = New SqlParameter("@SeqNo", SqlDbType.Int)
        'params(1).Value = customclass.SeqNo
        'params(2) = New SqlParameter("@FundingID", SqlDbType.VarChar, 50)
        'params(2).Value = customclass.FundingID
        'params(3) = New SqlParameter("@FasilitasNo", SqlDbType.VarChar, 50)
        'params(3).Value = customclass.FasilitasNo
        'customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "SP_PrintPengeluaranBank", params)
        'Return customclass
    End Function
End Class