﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentOutInstallment.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.PaymentOutInstallment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentOutInstallment</title>
</head>
<body>
    
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <link rel="Stylesheet" href="../../Include/default/easyui.css" type="text/css">
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.easyui.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-1.6.4.js"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';

        function OpenFundingViewPaymentHistory(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/ViewPaymentHistory.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'PaymentHistory', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function onBayarPokok(cb) {
            if (cb.checked == true) {
                var TotalPokok = 0;

                var replace = ",";
                var re = new RegExp(replace, 'g');

                for (var i = 1; 0 < $("#<%=GridView2.ClientID%> tr").length - 1; i++) {
                    if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input:[id*='chkSelect']:checkbox").is(':checked')) {
                        if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input[id*='hddPokok']").val() != "") {
                            TotalPokok = TotalPokok + parseFloat($("#<%=GridView2.ClientID%> tr:eq(" + i.toString() + ") input[id*='hddPokok']").val().replace(re, ""));
                        }
                      

                    }
                    if (($("#<%=GridView2.ClientID%> tr").length - 2) == i)
                        break;
                }

                $("#<%= txtPrincipalPaid.ClientID %>").val(number_format(TotalPokok));

            }
            else {
                $("#<%= txtPrincipalPaid.ClientID %>").val(number_format(0));
            }
        }

        function onBayarBunga(cb) {
            if (cb.checked == true) {
              
                var TotalBunga = 0;
                var replace = ",";
                var re = new RegExp(replace, 'g');

                for (var i = 1; 0 < $("#<%=GridView2.ClientID%> tr").length - 1; i++) {
                    if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input:[id*='chkSelect']:checkbox").is(':checked')) {


                        if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input[id*='hddBunga']").val() != "") {
                            TotalBunga = TotalBunga + parseFloat($("#<%=GridView2.ClientID%> tr:eq(" + i.toString() + ") input[id*='hddBunga']").val().replace(re, ""));
                        }
                    }
                    if (($("#<%=GridView2.ClientID%> tr").length - 2) == i)
                        break;
                }


                $("#<%= txtInterestPaid.ClientID %>").val(number_format(TotalBunga));

            }
            else {
                $("#<%= txtInterestPaid.ClientID %>").val(number_format(0));
            }
        }

        function CheckNilaiData(objRef) {

//            var row = objRef.parentNode.parentNode;
//            //         alert(row.rowIndex);

//            var b = row.rowIndex;
//            var x = b - 1;
//            var x2 = x + 1;
//            var x3 = x - 1;

//            if ($("#GridView2_lnkPilih_" + x + "").is(":checked")) {
//                if (x > 0) {
//                    $("#GridView2_lnkPilih_" + x3 + "").attr("disabled", "disabled"); 
//                }
//                $("#GridView2_lnkPilih_" + x2 + "").removeAttr("disabled");
//                $("#GridView2_hddSelect_" + x + "").val("TRUE");
//            }
//            else {
//                $("#GridView2_lnkPilih_" + x3 + "").removeAttr("disabled");
//                $("#GridView2_lnkPilih_" + x2 + "").attr("disabled", "disabled");
//                $("#GridView2_hddSelect_" + x + "").val("FALSE");
//            }


            var row = objRef.parentNode.parentNode;
            //         alert(row.rowIndex);

            var x = row.rowIndex;
            var x2 = x + 1;
            var x3 = x - 1;
            //         var row = lnk.parentNode.parentNode;
            //         var rowIndex = row.rowIndex - 1;
            //         var customerId = row.cells[1].innerHTML;

            //         alert(customerId);

            if ($("#<%=GridView2.ClientID%> tr:eq(" + x + ") input:[id*='chkSelect']:checkbox").is(':checked')) {
                if (x > 1) {
                    $("#<%=GridView2.ClientID%> tr:eq(" + x3 + ") input:[id*='chkSelect']:checkbox").attr("disabled", "disabled");
                }

                $("#<%=GridView2.ClientID%> tr:eq(" + x2 + ") input:[id*='chkSelect']:checkbox").removeAttr("disabled");
                $("#<%=GridView2.ClientID%> tr:eq(" + x + ") input[id*='hddSelect']").val("TRUE");


            }
            else {
                $("#<%=GridView2.ClientID%> tr:eq(" + x3 + ") input:[id*='chkSelect']:checkbox").removeAttr("disabled");
                $("#<%=GridView2.ClientID%> tr:eq(" + x2 + ") input:[id*='chkSelect']:checkbox").attr("disabled", "disabled");
                $("#<%=GridView2.ClientID%> tr:eq(" + x + ") input[id*='hddSelect']").val("FALSE");


            }

          


            var TotalPokok = 0;
            var TotalBunga = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');

            for (var i = 1; 0 < $("#<%=GridView2.ClientID%> tr").length - 1; i++) {
                if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input:[id*='chkSelect']:checkbox").is(':checked')) {
                    if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input[id*='hddPokok']").val() != "") {
                        TotalPokok = TotalPokok + parseFloat($("#<%=GridView2.ClientID%> tr:eq(" + i.toString() + ") input[id*='hddPokok']").val().replace(re, ""));
                    }

                    if ($("#<%=GridView2.ClientID%> tr:eq(" + i + ") input[id*='hddBunga']").val() != "") {
                        TotalBunga = TotalBunga + parseFloat($("#<%=GridView2.ClientID%> tr:eq(" + i.toString() + ") input[id*='hddBunga']").val().replace(re, ""));
                    }
                }
                if (($("#<%=GridView2.ClientID%> tr").length - 2) == i)
                    break;
            }

            $("#<%= txtPrincipalPaid.ClientID %>").val(number_format(TotalPokok));
            $("#<%= txtInterestPaid.ClientID %>").val(number_format(TotalBunga));
            if (TotalPokok == 0) {
                $("#<%= chbPokok.ClientID %>").attr('checked', false);
                $("#<%= chbPokok.ClientID %>").attr("disabled", "disabled");
            }
            else {
                $("#<%= chbPokok.ClientID %>").attr('checked', true);
                $("#<%= chbPokok.ClientID %>").removeAttr("disabled");
             }

            if (TotalBunga == 0) {
                $("#<%= chbBunga.ClientID %>").attr('checked', false);
                $("#<%= chbBunga.ClientID %>").attr("disabled", "disabled");
            }
            else {
                $("#<%= chbBunga.ClientID %>").attr('checked', true);
                $("#<%= chbBunga.ClientID %>").removeAttr("disabled");
             }
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }


        function GetLinkHistoryData(e) {
           

            $.ajax({
                type: 'GET',
                url: 'PaymentOutInstallment.aspx/GetLinkHistory',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var arr = JSON.parse(response.d);
                    OpenFundingViewPaymentHistory(arr.channeling, arr.compan, arr.cName, arr.bName, arr.BkID, arr.CcttName, arr.fcn, arr.fbn)
                }
               
            });


        }
    </script>
    
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
     
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PEMBAYARAN ANGSURAN PER BATCH
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Funding Bank
                </label>
                <asp:Label ID="lblFundingCompany" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Funding Cabang Bank
                </label>
                <asp:Label ID="lblPundingCompanyBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Fasilitas
                </label>
                <asp:Label ID="LblfacilityNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Fasilitas
                </label>
                <asp:Label ID="lblfacilityName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Batch
                </label>
                <asp:Label ID="LblBatchNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Pencairan
                </label>
                <asp:Label ID="LbldrowdownDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pencairan
                </label>
                <asp:Label ID="LblDrowdownAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Sisa Pinjaman
                </label>
                <asp:Label ID="LblOutstandingLoan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Total Pembayaran Pokok
                </label>
                <asp:Label ID="LblPrincipalPaid" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Angsuran Ke
                </label>
                <asp:Label ID="LblInsSeqNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pokok Jatuh Tempo
                </label>
                <asp:Label ID="LblPrincipalDue" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Jatuh Tempo
                </label>
                <asp:Label ID="LblDuedate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Margin Jatuh Tempo
                </label>
                <asp:Label ID="LblInterestDue" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Margin Yg Masih Harus Dibayar
                </label>
                <asp:Label ID="LblAccruedInterest" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Margin Bulan ini
                </label>
                <asp:Label ID="LblInsterestMonth" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="easyui-accordion" data-options="multiple:true">
                <div title="Sudah Bayar" style="overflow: auto; padding: 10px;" data-options="selected:false">
                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="200">
                        <asp:GridView ID="GridView1" CssClass="grid_general" Width="100%" DataKeyNames="SeqNo"
                            AutoGenerateColumns="false" runat="server" HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid"
                            GridLines="None">
                            <Columns>
                             <asp:TemplateField HeaderText="PRINT" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPrint" runat="server" Text="PRINT" CommandName="Print"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%-- <asp:TemplateField>
                                        <HeaderStyle Width="10"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBtn" ImageUrl="images/Plus.gif" CommandName="print" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                               <%-- <asp:BoundField DataField="SeqNo" HeaderText="No." HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />--%>
                                    <asp:TemplateField SortExpression="" HeaderText="SEQ NO" Visible="true">
                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSeqNO" runat="server" Text='<%#Container.DataItem("SeqNO")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TanggalAngsuran" HeaderText="Tanggal" HeaderStyle-HorizontalAlign="left"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="Pokok" HeaderText="Pokok" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="Bunga" HeaderText="Margin" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="Angsuran" HeaderText="Angsuran" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="SaldoPokok" HeaderText="OS Pokok" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="SaldoBunga" HeaderText="OS Margin" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                              <%-- <asp:BoundField DataField="PRINT" HeaderText="PRINT" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />--%>

                            </Columns>
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                        </asp:GridView>
                    </asp:Panel>
                </div>
                <div title="Belum DiBayar" style="overflow: auto; padding: 10px;" data-options="selected:true">
                    <asp:Panel runat="server" ScrollBars="Vertical" Height="200">
                        <asp:GridView ID="GridView2" CssClass="grid_general" Width="100%" DataKeyNames="SeqNo"
                            AutoGenerateColumns="false" runat="server" HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid"
                            GridLines="None">
                            <Columns>
                            <asp:TemplateField HeaderText="PRINT" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPrint" runat="server" Text="PRINT" CommandName="PrintBelumBayar"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pilih">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" Enabled='<%# Eval("EnableData") %>' Checked='<%# Eval("ChekedData") %>'
                                            onChange="CheckNilaiData(this);" />
                                      
                                        <asp:HiddenField ID="hddSelect" runat="server" Value='<%# Eval("ChekedData") %>'></asp:HiddenField>
                                            </ItemTemplate>
                                </asp:TemplateField>

                                    <asp:TemplateField HeaderText="No." HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSeqNo" runat="server" Text='<%# Eval("SeqNo") %>' />
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>

                          
                                <asp:BoundField DataField="TanggalAngsuran" HeaderText="Tanggal" HeaderStyle-HorizontalAlign="left"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle" />
                                <asp:TemplateField HeaderText="Pokok" HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle"
                                    ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPokok" runat="server" Text='<%# Eval("Pokok") %>' />
                                        <asp:HiddenField ID="hddPokok" runat="server" Value='<%# Eval("Pokok") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Margin" HeaderStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Middle"
                                    ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBunga" runat="server" Text='<%# Eval("Bunga") %>' />
                                        <asp:HiddenField ID="hddBunga" runat="server" Value='<%# Eval("Bunga") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Angsuran" HeaderText="Angsuran" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="SaldoPokok" HeaderText="OS Pokok" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                <asp:BoundField DataField="SaldoBunga" HeaderText="OS Margin" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                            </Columns>
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pembayaran Pokok
                </label>
                <%--<asp:TextBox ID="txtPrincipalPaid" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RVtxtPrincipalPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="-9999999999999" ControlToValidate="txtPrincipalPaid"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtPrincipalPaid" runat="server" enabled="False" />
                <asp:CheckBox ID="chbPokok" runat="server" Checked="True"  onclick='onBayarPokok(this);' />
            </div>
            <div class="form_right">
                <label>
                    Pembayaran Denda
                </label>
                <%--<asp:TextBox ID="txtPenaltyPaid" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RVtxtPenaltyPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="-9999999999999" ControlToValidate="txtPenaltyPaid"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtPenaltyPaid" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pembayaran Margin
                </label>
                <%--<asp:TextBox ID="txtInterestPaid" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RVtxtInterestPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="-9999999999999" ControlToValidate="txtInterestPaid"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtInterestPaid" runat="server" enabled="False" />
                <asp:CheckBox ID="chbBunga" runat="server" Checked="True"  onclick='onBayarBunga(this);' />
            </div>
            <div class="form_right">
                <label>
                    Pembayaran PPH
                </label>
                <%--<asp:TextBox ID="TxtPPHPaid" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RVTxtPPHPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="-9999999999999" ControlToValidate="TxtPPHPaid"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="TxtPPHPaid" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <%-- <label class ="label_req" runat="server" id="labelclassduedate">
                    Pembayaran Angs. Jatuh Tempo
                </label>
                 <asp:Label ID="lblDateJatuhTempo" runat="server"></asp:Label>
                 
                  &nbsp;&nbsp;&nbsp; Angsuran Ke &nbsp;- &nbsp; <asp:Label ID="lblInstalment" runat="server"></asp:Label>--%>
                <%-- <asp:DropDownList ID="cboDueDateToBePaid" runat="server">
                </asp:DropDownList>                
                <asp:RequiredFieldValidator ID="RfvcboDueDateToBePaid" runat="server" ErrorMessage="Harap Pilih Tanggal Jatuh Tempo"
                    ControlToValidate="cboDueDateToBePaid" Display="Dynamic" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>--%>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Valuta
                </label>
                <%--<asp:TextBox runat="server" ID="txtValueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Rekening Bank
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboBank" runat="server" ErrorMessage="Harap Pilih Bank"
                    ControlToValidate="cboBank" Display="Dynamic" InitialValue="0" CssClass="validator_general" Visible="false"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas Keluar
                </label>
                <asp:TextBox ID="txtReferenceNo" runat="server" Width="196px"></asp:TextBox>
            </div>
        </div>
         <div class="form_box">
            <div class="form_left">
                <label>
                   Notes
                </label>
                <asp:TextBox ID="TxtNotes" runat="server" TextMode="MultiLine">
                </asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                </label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
                <input type="button" value="Payment History" onclick="GetLinkHistoryData(this);" class="small button green"/>
            <%--<asp:Button ID="btnPaymentHistory"  runat="server" CausesValidation="False" onclick="GetLinkHistoryData(this);"  Text="Payment History"
                CssClass="small button green"></asp:Button>--%>
        </div>
    </asp:Panel>
   
    </form>
</body>
</html>
