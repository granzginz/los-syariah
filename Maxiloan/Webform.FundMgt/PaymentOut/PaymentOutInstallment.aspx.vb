﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Framework.SQLEngine
Imports System.Web.Services
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

#End Region

Public Class PaymentOutInstallment
    Inherits Maxiloan.Webform.AccMntWebBased
#Region "uccomponent"

    Protected WithEvents txtPrincipalPaid As ucNumberFormat
    Protected WithEvents txtPenaltyPaid As ucNumberFormat
    Protected WithEvents txtInterestPaid As ucNumberFormat
    Protected WithEvents TxtPPHPaid As ucNumberFormat
    Protected WithEvents txtValueDate As ucDateCE

#End Region

#Region " Property "

    Private Shared CompanyID2 As String
    Private Shared BankID2 As String
    Private Shared FundingContractNo2 As String
    Private Shared FundingBatchNo2 As String
    Private Shared CompanyName2 As String
    Private Shared BankName2 As String
    Private Shared ContractName2 As String


    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property
    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property
    Public Property OutStandingPrincipalMustPaid() As Decimal
        Get
            Return CDec(ViewState("OutStandingPrincipalMustPaid"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("OutStandingPrincipalMustPaid") = Value
        End Set
    End Property
    Public Property InterestMustPaid() As Decimal
        Get
            Return CDec(ViewState("InterestMustPaid"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("InterestMustPaid") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController
    Private oCashierCustomClass As New Parameter.CashierTransaction

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        'scriptManager__1.RegisterPostBackControl(Me.btnSave)

        'Dim scriptManager__2 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        'scriptManager__2.RegisterPostBackControl(Me.btnCancel)

        'Dim scriptManager__3 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        'scriptManager__3.RegisterPostBackControl(Me.btnPaymentHistory)

        'Dim scriptManager__4 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        'scriptManager__4.RegisterPostBackControl(Me.GridView1)

        'Dim scriptManager__5 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        'scriptManager__5.RegisterPostBackControl(Me.GridView2)

        If Not Me.IsPostBack Then
            Me.FormID = "PAYOUT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""
            Dim cmdwhere As String

            txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyID") <> "" Then CompanyID2 = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("CompanyName") <> "" Then CompanyName2 = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankName") <> "" Then BankName2 = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("BankID") <> "" Then BankID2 = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("ContractName") <> "" Then ContractName2 = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("Fundingcontractid") <> "" Then FundingContractNo2 = Request.QueryString("Fundingcontractid")
            If Request.QueryString("ContractBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("ContractBatchNo")
            If Request.QueryString("ContractBatchNo") <> "" Then FundingBatchNo2 = Request.QueryString("ContractBatchNo")
            fillcboDueDate()
            fillcboBank()
            cmdwhere = "FundingContract.FundingContractNo = '" & Me.FundingContractNo.Trim & "' and "
            cmdwhere = cmdwhere + "FundingContract.FundingCoyID = '" & Me.CompanyID & "' and "
            cmdwhere = cmdwhere + "FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
            pnlView.Visible = True
            If Me.IsHoBranch Then
                'If Not CheckCashier(Me.Loginid) Then
                '    ShowMessage(lblMessage, "Kasir belum open", True)
                '    View(cmdwhere)
                '    btnSave.Visible = False
                'Else
                View(cmdwhere)
                btnSave.Visible = True
                'End If
            Else

                ShowMessage(lblMessage, "Harap Login di Kantor Pusat", True)
                View(cmdwhere)
                btnSave.Visible = False
            End If

            InitiateUCnumberFormat(txtPrincipalPaid, False, True)
            InitiateUCnumberFormat(txtPenaltyPaid, False, True)
            InitiateUCnumberFormat(txtInterestPaid, False, True)
            InitiateUCnumberFormat(TxtPPHPaid, False, True)

            txtPrincipalPaid.RangeValidatorMinimumValue = "-9999999999999"
            txtPrincipalPaid.RangeValidatorMaximumValue = "9999999999999"

            txtPenaltyPaid.RangeValidatorMinimumValue = "-9999999999999"
            txtPenaltyPaid.RangeValidatorMaximumValue = "9999999999999"

            txtInterestPaid.RangeValidatorMinimumValue = "-9999999999999"
            txtInterestPaid.RangeValidatorMaximumValue = "9999999999999"

            TxtPPHPaid.RangeValidatorMinimumValue = "-9999999999999"
            TxtPPHPaid.RangeValidatorMaximumValue = "9999999999999"
            BindDataAngsuranFundingPaid()
            BindDataAngsuranFundingNoPaid()
        End If
    End Sub
#Region "FillCboBank"
    Sub fillcboBank()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetHOBranch"
            objCommand.Parameters.Add("@CompanyID", SqlDbType.Char, 3).Value = Me.SesCompanyID.Trim
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.BranchID = IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try


        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankAccount(GetConnectionString, Me.BranchID.Trim, "BA", "")
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub
#End Region
#Region "FillCBODueDate"
    Sub fillcboDueDate()
        Dim dtDueDate As New DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'and FundingBatchNo='" & Me.FundingBatchNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingGetDueDateInstallment"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        dtDueDate = oContract.ListData

        'With cboDueDateToBePaid
        '    .DataValueField = "InsSeqNo"
        '    .DataTextField = "DueDate"
        '    .DataSource = dtDueDate
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = "0"
        '    .SelectedIndex = 0
        'End With
    End Sub
#End Region
    Public Sub View(ByVal cmdwhere As String)
        Dim dtslist As DataTable
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BusinessDate = Me.BusinessDate
                .SpName = "spFundingPaymentOutInstallmentView"
            End With
            oReceive = cReceive.GetGeneralEditView(oCustomClass)
            dtslist = oReceive.ListData
            LblBatchNo.Text = CStr(dtslist.Rows(0).Item("FundingBatchNo")).Trim
            LbldrowdownDate.Text = CStr(dtslist.Rows(0).Item("BatchDate")).Trim
            LblDrowdownAmount.Text = FormatNumber(dtslist.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
            LblOutstandingLoan.Text = FormatNumber(dtslist.Rows(0).Item("OSAmtToFunCoy"), 0)
            LblPrincipalPaid.Text = FormatNumber(dtslist.Rows(0).Item("LastPrincipalPaid"), 0)
            LblInsSeqNo.Text = FormatNumber(dtslist.Rows(0).Item("SequenceNo"), 0)
            LblPrincipalDue.Text = FormatNumber(dtslist.Rows(0).Item("PrincipalDue"), 0)
            txtPrincipalPaid.Text = FormatNumber(dtslist.Rows(0).Item("PrincipalDue"), 0)
            Me.OutStandingPrincipalMustPaid = CDbl(txtPrincipalPaid.Text)
            LblDuedate.Text = CStr(dtslist.Rows(0).Item("DueDate")).Trim
            LblInterestDue.Text = FormatNumber(dtslist.Rows(0).Item("InterestDue"), 0)
            txtInterestPaid.Text = FormatNumber(dtslist.Rows(0).Item("InterestDue"), 0)
            Me.InterestMustPaid = CDbl(txtInterestPaid.Text)
            LblAccruedInterest.Text = FormatNumber(dtslist.Rows(0).Item("LastInterestAccrued"), 0)
            LblInsterestMonth.Text = FormatNumber(dtslist.Rows(0).Item("InterestMonth"), 0)

            txtPenaltyPaid.Text = "0"
            TxtPPHPaid.Text = "0"
            BindTitleTop()
            If CDbl(dtslist.Rows(0).Item("PrincipalDue")) <= 0 Then
                chbPokok.Checked = False
                chbPokok.Enabled = False
            Else
                chbPokok.Checked = True
                chbPokok.Enabled = True
            End If

            If CDbl(dtslist.Rows(0).Item("InterestDue")) <= 0 Then
                chbBunga.Checked = False
                chbBunga.Enabled = False
            Else
                chbBunga.Checked = True
                chbBunga.Enabled = True
            End If
            'If Request.QueryString("Filter3") <> Nothing Then
            '    Dim dd As String
            '    dd = Request.QueryString("Filter3").ToString()
            '    With cboDueDateToBePaid                    
            '        .Items.Insert(0, dd)
            '        .Items(0).Value = dd
            '        .SelectedIndex = 0
            '        .Enabled = False
            '    End With

            '    labelclassduedate.Attributes.Remove("class")                
            'End If


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.FundingContractBatch
        lblMessage.Text = ""
        rfvcboBank.Visible = True
        Try
            If cboBank.SelectedItem.Value.Trim = "0" Then

                ShowMessage(lblMessage, "Harap pilih Rekening Bank", True)
                display()
                Exit Sub
            End If
            If TxtNotes.Text = "" Then
                TxtNotes.Text = "-"
            End If
            If txtPrincipalPaid.Text.Trim = "" Then
                txtPrincipalPaid.Text = "0"
            End If
            If txtInterestPaid.Text.Trim = "" Then
                txtInterestPaid.Text = "0"
            End If
            If txtPenaltyPaid.Text.Trim = "" Then
                txtPenaltyPaid.Text = "0"
            End If
            If TxtPPHPaid.Text.Trim = "" Then
                TxtPPHPaid.Text = "0"
            End If
            If LblOutstandingLoan.Text = "0" And (txtPrincipalPaid.Text <> "0" Or txtPrincipalPaid.Text <> "") Then

                ShowMessage(lblMessage, "Sisa Saldo nol, Pokok yang dibayar harus Nol", True)
                display()
                Exit Sub
            End If

            'Dim chkDtList As CheckBox
            Dim dt As DataTable = createNewDT()
            Dim lblSeqNo As Label
            Dim hddPokok As HiddenField
            Dim hddBunga As HiddenField
            Dim hddSelect As HiddenField




            For i = 0 To GridView2.Rows.Count - 1
                hddSelect = CType(GridView2.Rows(i).FindControl("hddSelect"), HiddenField)

                If hddSelect.Value.ToUpper = "TRUE" Then
                    lblSeqNo = CType(GridView2.Rows(i).FindControl("lblSeqNo"), Label)
                    hddPokok = CType(GridView2.Rows(i).FindControl("hddPokok"), HiddenField)
                    hddBunga = CType(GridView2.Rows(i).FindControl("hddBunga"), HiddenField)
                    dt.Rows.Add(CInt(lblSeqNo.Text), CDbl(hddPokok.Value.Replace(",", "")), CDbl(hddBunga.Value.Replace(",", "")))
                End If

            Next

            With customClass
                .strConnection = GetConnectionString()
                .BankId = Me.BankID
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .PrincipalPaidAmount = CDec(txtPrincipalPaid.Text.Trim)
                .InterestPaidAmount = CDec(txtInterestPaid.Text.Trim)
                .PenaltyPaidAmount = CDec(txtPenaltyPaid.Text.Trim)
                .ValueDate = ConvertDate2(txtValueDate.Text)
                .ReferenceNo = txtReferenceNo.Text.Trim
                .BusinessDate = Me.BusinessDate
                .BankAccountID = cboBank.SelectedItem.Value.Trim
                .BranchId = Me.sesBranchId.Replace("'", "")
                .LoginId = Me.Loginid
                .CompanyID = Me.SesCompanyID
                .InsSecNo = CInt(LblInsSeqNo.Text)
                .PPHPaid = CDec(TxtPPHPaid.Text.Trim)
                .AmtDrawDown1 = Me.OutStandingPrincipalMustPaid
                .AmtDrawDown2 = Me.InterestMustPaid
                .IsPrincipalPaidAmount = chbPokok.Checked
                .IsInterestPaidAmount = chbBunga.Checked
                .Notes = TxtNotes.Text.Trim
                .ListData = dt
            End With
            m_Company.PaymentOutInstallmentProcess(customClass)

            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
            btnSave.Visible = True
            btnCancel.Visible = True

            display()
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("asp", "function", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub display()
        Dim cmdwhere As String
        'fillcboDueDate()
        'fillcboBank()
        cmdwhere = "FundingContract.FundingContractNo = '" & Me.FundingContractNo.Trim & "' and "
        cmdwhere = cmdwhere + "FundingContract.FundingCoyID = '" & Me.CompanyID & "' and "
        cmdwhere = cmdwhere + "FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
        View(cmdwhere)
        BindDataAngsuranFundingPaid()
        BindDataAngsuranFundingNoPaid()
    End Sub
    Public Sub BindTitleTop()
        lblFundingCompany.Text = Me.BankName.Trim
        lblPundingCompanyBranch.Text = Me.CompanyName.Trim
        LblfacilityNo.Text = Me.FundingContractNo.Trim
        lblfacilityName.Text = Me.ContractName.Trim
    End Sub

    'Private Sub btnPaymentHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPaymentHistory.Click
    '    ' Response.Redirect("../View/ViewPaymentHistory.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&FundingBatchNo=" & Me.FundingBatchNo)
    '    btnPaymentHistory.Attributes.Add("onClick", "javascript:OpenFundingViewPaymentHistory('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & Me.FundingBatchNo.Trim & "');")
    '    display()
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Request.QueryString("Flag") <> "PAYOUTBATCHDD" Then
            Response.Redirect("PaymentOutBatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
        Else
            Response.Redirect("PaymentOutBatchDueDate.aspx?Flag=1&Filter1=" & Request.QueryString("Filter1").ToString() & "&Filter2=" & Request.QueryString("Filter2").ToString() & "&Filter3=" & Request.QueryString("Filter3").ToString())
        End If
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

    Public Function GetFundingBatchInstallmentPaid(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingBatchNo

            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(1).Value = customclass.BankId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewFundingBatchInstallmentPaid", params).Tables(0)
            Return customclass
        Catch exp As Exception

        End Try

    End Function

    Private Sub BindDataAngsuranFundingPaid()
        Dim oEntities As New Parameter.FundingContractBatch
        Dim dtViewData As New DataTable
        With oEntities
            .FundingBatchNo = Me.FundingBatchNo.Trim
            .BankId = Me.BankID.Trim
            .FundingContractNo = Me.FundingContractNo.Trim
            .strConnection = GetConnectionString()
        End With
        oEntities = GetFundingBatchInstallmentPaid(oEntities)
        dtViewData = oEntities.ListData
        GridView1.DataSource = dtViewData

        GridView1.DataBind()
    End Sub
    Public Function GetFundingBatchInstallmentNoPaid(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingBatchNo

            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(1).Value = customclass.BankId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewFundingBatchInstallmentNoPaid", params).Tables(0)
            Return customclass
        Catch exp As Exception

        End Try

    End Function

    Private Sub BindDataAngsuranFundingNoPaid()
        Dim oEntities As New Parameter.FundingContractBatch
        Dim dtViewData As New DataTable
        With oEntities
            .FundingBatchNo = Me.FundingBatchNo.Trim
            .BankId = Me.BankID.Trim
            .FundingContractNo = Me.FundingContractNo.Trim
            .strConnection = GetConnectionString()
        End With
        oEntities = GetFundingBatchInstallmentNoPaid(oEntities)
        dtViewData = oEntities.ListData
        GridView2.DataSource = dtViewData

        GridView2.DataBind()
    End Sub

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("InsSeqNo")
        dt.Columns.Add("PrincipalPaidAmount")
        dt.Columns.Add("InterestPaidAmount")


        Return dt
    End Function

   
  
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True)> _
    Public Shared Function GetLinkHistory() As String

        Dim serializer As New JavaScriptSerializer()
        Dim channeling As String = "channeling"
        Dim compan As String = CompanyID2.Trim
        Dim cName As String = CompanyName2.Trim
        Dim bName As String = BankName2.Trim
        Dim BkID As String = BankID2.Trim
        Dim CcttName As String = ContractName2.Trim
        Dim fcn As String = FundingContractNo2.Trim
        Dim fbn As String = FundingBatchNo2.Trim
        Return serializer.Serialize(New With {Key .channeling = channeling, Key .compan = compan, Key .cName = cName, Key .bName = bName, Key .BkID = BkID, Key .CcttName = CcttName, Key .fcn = fcn, Key .fbn = fbn})


    End Function

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand



        If (e.CommandName = "Print") Then

            Dim lblSeqNO As Label

            Dim row As GridViewRow
            row = DirectCast(DirectCast(e.CommandSource, LinkButton).Parent.Parent, GridViewRow)

            LblSeqno = GridView1.Rows(row.RowIndex).FindControl("lblSeqNO")

            rfvcboBank.Visible = False

            Dim FundingID As String = Me.BankID.Trim
            Dim FasilitasNo As String = LblfacilityNo.Text.Trim
            Dim BatchNo As String = LblBatchNo.Text.Trim
            Dim SeqNo As String = LblInsSeqNo.Text.Trim

            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            Dim strFileLocation As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/Webform.FundMgt/PaymentOut/Reporting/ReportFunding.aspx?FundingID=" + FundingID.Trim + "&SeqNo=" + lblSeqNO.Text.Trim + "&FasilitasNo=" + FasilitasNo.Trim + "&FundingBatchNo=" + FundingBatchNo.Trim
            'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/Webform.FundMgt/PaymentOut/Reporting/ReportFunding.aspx?FundingBatchNo=" + FundingBatchNo.Trim + "&SeqNo=" + SeqNo.Trim
            Response.Write("<script language = javascript>" & vbCrLf _
                        & "window.open('" & strFileLocation & "') " & vbCrLf _
                        & "</script>")
        End If

      
    End Sub
    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView2.RowCommand



        If (e.CommandName = "PrintBelumBayar") Then

            Dim lblSeqNO As Label

            Dim row As GridViewRow
            row = DirectCast(DirectCast(e.CommandSource, LinkButton).Parent.Parent, GridViewRow)

            lblSeqNO = GridView2.Rows(row.RowIndex).FindControl("lblSeqNO")

            rfvcboBank.Visible = False

            Dim FundingID As String = Me.BankID.Trim
            Dim FasilitasNo As String = LblfacilityNo.Text.Trim
            Dim BatchNo As String = LblBatchNo.Text.Trim
            Dim SeqNo As String = LblInsSeqNo.Text.Trim

            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            Dim strFileLocation As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/Webform.FundMgt/PaymentOut/Reporting/ReportFundingBelumBayar.aspx?FundingID=" + FundingID.Trim + "&SeqNo=" + lblSeqNO.Text.Trim + "&FasilitasNo=" + FasilitasNo.Trim + "&FundingBatchNo=" + FundingBatchNo.Trim
            'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/Webform.FundMgt/PaymentOut/Reporting/ReportFunding.aspx?FundingBatchNo=" + FundingBatchNo.Trim + "&SeqNo=" + SeqNo.Trim
            Response.Write("<script language = javascript>" & vbCrLf _
                        & "window.open('" & strFileLocation & "') " & vbCrLf _
                        & "</script>")
        End If


    End Sub

End Class