﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentOutFees.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.PaymentOutFees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentOutFees</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingViewPaymentHistory(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/ViewPaymentHistory.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'PaymentHistory', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function OpenFundingBatchView(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingBatchView.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }							
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PEMBAYARAN BIAYA-BIAYA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Funding Bank
                </label>
                <asp:Label ID="lblFundingCompany" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Funding Cabang Bank
                </label>
                <asp:Label ID="lblPundingCompanyBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Fasilitas
                </label>
                <asp:Label ID="LblfacilityNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Fasilitas
                </label>
                <asp:Label ID="lblfacilityName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Batch
                </label>
                <asp:hyperlink ID="LblBatchNo" runat="server"></asp:hyperlink>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Pencairan
                </label>
                <asp:Label ID="LbldrowdownDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pencairan
                </label>
                <asp:Label ID="LblDrowdownAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Sisa Pinjaman
                </label>
                <asp:Label ID="LblOutstandingLoan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Administrasi (BPKB)
                </label>
                <%--<asp:TextBox ID="txtAdministrationFee" runat="server" Width="196px"></asp:TextBox>
                <asp:RangeValidator ID="RVtxtPrincipalPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="txtAdministrationFee"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                    <uc1:ucnumberformat id="txtAdministrationFee" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Comitment Fee
                </label>
                <%--<asp:TextBox ID="txtComitmentFee" runat="server" Width="196px"></asp:TextBox>
                <asp:RangeValidator ID="RVtxtPenaltyPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="txtComitmentFee"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                    <uc1:ucnumberformat id="txtComitmentFee" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Provisi
                </label>
                <%--<asp:TextBox ID="txtProvision" runat="server" Width="196px"></asp:TextBox>
                <asp:RangeValidator ID="RVtxtInterestPaid" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="txtProvision"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                    <uc1:ucnumberformat id="txtProvision" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Biaya Lainnya
                </label>
                <%--<asp:TextBox ID="txtOthersFee" runat="server" Width="196px"></asp:TextBox>
                <asp:RangeValidator ID="RVtxtOthersFee" runat="server" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="txtOthersFee"
                    Display="Dynamic" Type="Double"></asp:RangeValidator>--%>
                    <uc1:ucnumberformat id="txtOthersFee" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label class ="label_req">
                    Tanggal Valuta
                </>
                <%--<asp:TextBox runat="server" ID="txtValueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    Rekening Bank
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboBank" runat="server" ErrorMessage="Harap pilih Bank"
                    ControlToValidate="cboBank" Display="Dynamic" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas Keluar
                </label>
                <asp:TextBox ID="txtReferenceNo" runat="server" Width="196px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Button ID="btnPaymentHistory" runat="server" CausesValidation="False" Text="Payment History"
                CssClass="small button green"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
