﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
#End Region

Public Class PaymentOutPrepayment
    Inherits Maxiloan.Webform.AccMntWebBased


#Region "uccomponent"

    Protected WithEvents txtInterestPaid As ucNumberFormat
    Protected WithEvents txtPenaltyPaid As ucNumberFormat
    Protected WithEvents TxtPrincipalCorrection As ucNumberFormat
    Protected WithEvents txtValiddate1 As ucDateCE
    Protected WithEvents txtValueDate As ucDateCE

#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property
    Private Property ContractName() As String
        Get
            Return CStr(ViewState("ContractName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ContractName") = Value
        End Set
    End Property
    Private Property PrepaymentType() As String
        Get
            Return CStr(ViewState("PrepaymentType"))
        End Get
        Set(ByVal Value As String)
            ViewState("PrepaymentType") = Value
        End Set
    End Property

    Public Property OutStandingPrincipalUndue() As Decimal
        Get
            Return CDec(ViewState("OutStandingPrincipalUndue"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("OutStandingPrincipalUndue") = Value
        End Set
    End Property
    Public Property CmdWhere() As String
        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
    Public Property isBatchPrepayment() As Boolean
        Get
            Return CBool(ViewState("isBatchPrepayment"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isBatchPrepayment") = Value
        End Set
    End Property
    Public Property OutStandingPrincipalMustPaid() As Decimal
        Get
            Return CDec(ViewState("OutStandingPrincipalMustPaid"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("OutStandingPrincipalMustPaid") = Value
        End Set
    End Property
    Public Property InterestMustPaid() As Decimal
        Get
            Return CDec(ViewState("InterestMustPaid"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("InterestMustPaid") = Value
        End Set
    End Property
    Public Property BranchIDHO() As String
        Get
            Return CStr(ViewState("BranchIDHO"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDHO") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController
    Private oCashierCustomClass As New Parameter.CashierTransaction
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Me.IsPostBack Then
            Me.FormID = "PAYOUT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtValiddate1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            Me.SearchBy = ""
            Me.SortBy = ""
            Dim cmdwhere As String
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("ContractBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("ContractBatchNo")
            'fillcboBank()
            BindTitleTop()
            pnlTop.Visible = True
            pnlList.Visible = True
            pnlPrepaymentBatch.Visible = False
            PNLBPKBReplacing.Visible = False
            PnlPaymentOut.Visible = False
            Me.CmdWhere = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
            BindGridEntity(Me.CmdWhere)
            'View()

            InitiateUCnumberFormat(txtInterestPaid, False, True)
            InitiateUCnumberFormat(txtPenaltyPaid, False, True)
            InitiateUCnumberFormat(TxtPrincipalCorrection, False, True)

            txtInterestPaid.RangeValidatorMinimumValue = "0"
            txtInterestPaid.RangeValidatorMaximumValue = "9999999999999"

            txtPenaltyPaid.RangeValidatorMinimumValue = "0"
            txtPenaltyPaid.RangeValidatorMaximumValue = "9999999999999"

            TxtPrincipalCorrection.RangeValidatorMinimumValue = "0"
            TxtPrincipalCorrection.RangeValidatorMaximumValue = "9999999999999"

        End If
    End Sub
    Public Sub BindTitleTop()
        LblCompanyTop.Text = Me.BankName.Trim
        LblCompanyBranchTop.Text = Me.CompanyName.Trim
        LblContractNoTop.Text = Me.FundingContractNo.Trim
        LblContractNameTop.Text = Me.ContractName.Trim
        lblFundingCompany.Text = Me.BankName.Trim
        lblPundingCompanyBranch.Text = Me.CompanyName.Trim
        LblfacilityNo.Text = Me.FundingContractNo.Trim
        lblfacilityName.Text = Me.ContractName.Trim
        LblBatchNoTop.Text = Me.FundingBatchNo.Trim
        LblBatchNo.Text = Me.FundingBatchNo.Trim
        BindDataTop()
        txtReferenceNo.Text = ""
        txtSearchBy.Text = ""
    End Sub
#Region "FillCboBank"
    Sub fillcboBank()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetHOBranch"
            objCommand.Parameters.Add("@CompanyID", SqlDbType.Char, 3).Value = Me.SesCompanyID.Trim
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.BranchIDHO = IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankAccount(GetConnectionString, Me.BranchIDHO.Trim, "BA", "")
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdwhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingInquiryAgreementPledgeWillFinish"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString
        pnlTop.Visible = True


        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.CmdWhere)
        pnlTop.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.CmdWhere)
                End If
            End If
        End If
        pnlTop.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.CmdWhere)
        pnlTop.Visible = True
    End Sub

#End Region
    Public Sub View()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim StrapplicationID As String
        Dim StrCustomerId As String
        Dim SPName As String
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        If Me.PrepaymentType = "B" Then
            SPName = "spFundingPrepaymentBPKBReplacingView"
        ElseIf Me.PrepaymentType = "C" Then
            SPName = "spFundingPrepaymentBPKBReplacing"
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .BranchIDApplication = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .SpName = SPName
        End With
        oReceive = cReceive.GetPrepaymentEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("BatchDate")) Then
                LbldrowdownDate.Text = CType(oRow("BatchDate"), String)
            End If
            If Not IsDBNull(oRow("PrincipalAmtToFunCoy")) Then
                LblDrowdownAmount.Text = FormatNumber(oRow("PrincipalAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("OSAmtToFunCoy")) Then
                LblOutstandingLoan.Text = FormatNumber(oRow("OSAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("ApplicationID")) Then
                StrapplicationID = CType(oRow("ApplicationID"), String)
            End If
            If Not IsDBNull(oRow("AgreementNo")) Then
                hyAgreementNo.Text = CType(oRow("AgreementNo"), String)
            End If
            If Not IsDBNull(oRow("CustomerID")) Then
                StrCustomerId = CType(oRow("CustomerID"), String)
            End If
            If Not IsDBNull(oRow("Name")) Then
                HyCust.Text = CType(oRow("Name"), String)
            End If
            If Not IsDBNull(oRow("OutStandingPrincipal")) Then
                lblOSPrincipalCust.Text = FormatNumber(oRow("OutStandingPrincipal"), 0)
            End If
            If Not IsDBNull(oRow("OSARCust")) Then
                LblOSARCust.Text = FormatNumber(oRow("OSARCust"), 0)
            End If
            If Not IsDBNull(oRow("OutStandingPrincipalUndue")) Then
                LblOSPrincipalComp.Text = FormatNumber(oRow("OutStandingPrincipalUndue"), 0)
                Me.OutStandingPrincipalUndue = CDec(LblOSPrincipalComp.Text)
                If Me.isBatchPrepayment = False Then
                    TxtPrincipalCorrection.Text = FormatNumber(oRow("OutStandingPrincipalUndue"), 0)
                Else
                    TxtPrincipalCorrection.Text = FormatNumber(oRow("OSAmtToFunCoy"), 0)
                End If
                Me.OutStandingPrincipalMustPaid = CDbl(TxtPrincipalCorrection.Text)
            End If
            If Not IsDBNull(oRow("DocumentNo")) Then
                LblBPKBNo.Text = CType(oRow("DocumentNo"), String)
            End If
            If Not IsDBNull(oRow("SerialNo1")) Then
                LblChassisNo.Text = CType(oRow("SerialNo1"), String)
            End If
            If Not IsDBNull(oRow("SerialNo2")) Then
                LblEngineNo.Text = CType(oRow("SerialNo2"), String)
            End If
        End If
        Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("PaymentOutBatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
        pnlTop.Visible = True
        pnlList.Visible = True
        pnlPrepaymentBatch.Visible = False
        PNLBPKBReplacing.Visible = False
        PnlPaymentOut.Visible = False
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        Dim LblApplicationId As Label
        Dim LBlBranchID As Label
        LblApplicationId = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label)
        LBlBranchID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)
        Me.ApplicationID = LblApplicationId.Text.Trim
        Me.BranchID = LBlBranchID.Text.Trim
        txtReferenceNo.Text = ""
        fillcboBank()
        cboBank.SelectedItem.Value = 0

        Select Case e.CommandName
            Case "Prepayment"
                pnlTop.Visible = False
                pnlList.Visible = False
                pnlAsOf.Visible = True
            Case "PrepaymentBatch"
                Dim PrepaymentType As String
                Dim objCommand As New SqlCommand
                Dim objConnection As New SqlConnection(GetConnectionString)
                Dim objReader As SqlDataReader
                Try
                    If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                    objCommand.CommandType = CommandType.StoredProcedure
                    objCommand.CommandText = "spFundingContractGetPrepaymentType"
                    objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
                    objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
                    objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
                    objCommand.Parameters.Add("@PrepaymentType", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output
                    objCommand.Connection = objConnection
                    objCommand.ExecuteNonQuery()
                    PrepaymentType = IIf(IsDBNull(objCommand.Parameters("@PrepaymentType").Value), "", objCommand.Parameters("@PrepaymentType").Value)
                Catch ex As Exception
                    Response.Write(ex.Message)
                Finally
                    If objConnection.State = ConnectionState.Open Then objConnection.Close()
                    objConnection.Dispose()
                    objCommand.Dispose()
                End Try
                If PrepaymentType = "B" Then
                    pnlTop.Visible = False
                    pnlList.Visible = False
                    pnlPrepaymentBatch.Visible = True
                    PNLBPKBReplacing.Visible = False
                    PnlPaymentOut.Visible = False
                    lblTopMenuText.Text = "PREPAYMENT BATCH BY BPKB REPLACING"
                    Me.PrepaymentType = "B"
                    Me.isBatchPrepayment = True
                    View()
                    btnSave.Visible = True
                ElseIf PrepaymentType = "C" Then
                    pnlTop.Visible = False
                    pnlList.Visible = False
                    pnlPrepaymentBatch.Visible = True
                    PNLBPKBReplacing.Visible = False
                    PnlPaymentOut.Visible = True
                    lblTopMenuText.Text = "PREPAYMENT BATCH BY PAYMENT OUT"
                    Me.PrepaymentType = "C"
                    Me.isBatchPrepayment = True
                    If Me.IsHoBranch Then
                        'If Not CheckCashier(Me.Loginid) Then

                        '    ShowMessage(lblMessage, "Kasir Belum Open", True)
                        '    pnlTop.Visible = True
                        '    pnlList.Visible = True
                        '    Exit Sub
                        'Else
                        View()
                        ViewBatchPaymentOut()
                        Exit Sub
                        'End If
                    Else

                        ShowMessage(lblMessage, "Harap Login di kantor Pusat", True)
                        pnlTop.Visible = True
                        pnlList.Visible = True
                        Exit Sub
                    End If
                End If
        End Select
    End Sub
    Public Sub ViewPaymentOut()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim StrapplicationID As String
        Dim StrCustomerId As String
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .PaymentOutAsOfDate = ConvertDate2(txtValiddate1.Text)
            .BranchIDApplication = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .SpName = "spFundingPrepaymentPaymentOutView"
        End With
        oReceive = cReceive.GetPrepaymentEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("AccruedInterest")) Then
                LblAccruedInterest.Text = FormatNumber(oRow("AccruedInterest"), 0)
            End If
            If Not IsDBNull(oRow("PrepaymentAmount")) Then
                LblPrepaymentAmount.Text = FormatNumber(oRow("PrepaymentAmount"), 0)
            End If
            If Not IsDBNull(oRow("PrepaymentPenalty")) Then
                lblPrepaymentPenalty.Text = FormatNumber(oRow("PrepaymentPenalty"), 0)
                txtPenaltyPaid.Text = FormatNumber(oRow("PrepaymentPenalty"), 0)
            End If
            If Not IsDBNull(oRow("InterestProporsional")) Then
                LblproporsionalInterest.Text = FormatNumber(oRow("InterestProporsional"), 0)
                txtInterestPaid.Text = FormatNumber(oRow("InterestProporsional"), 0)
                Me.InterestMustPaid = CDbl(txtInterestPaid.Text)
            End If
        End If
        Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Public Sub ViewBatchPaymentOut()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim StrapplicationID As String
        Dim StrCustomerId As String
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .SpName = "spFundingPrepaymentBatchPaymentOutView"
        End With
        oReceive = cReceive.GetGeneralEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("AccruedInterest")) Then
                LblAccruedInterest.Text = FormatNumber(oRow("AccruedInterest"), 0)
            End If
            If Not IsDBNull(oRow("PrepaymentAmount")) Then
                LblPrepaymentAmount.Text = FormatNumber(oRow("PrepaymentAmount"), 0)
            End If
            If Not IsDBNull(oRow("PrepaymentPenalty")) Then
                lblPrepaymentPenalty.Text = FormatNumber(oRow("PrepaymentPenalty"), 0)
                txtPenaltyPaid.Text = FormatNumber(oRow("PrepaymentPenalty"), 0)
            End If
            ''If Not IsDBNull(oRow("OutStandingPrincipalUndue")) Then
            ''Me.OutStandingPrincipalUndue = CDec(oRow("OutStandingPrincipalUndue"))
            ''LblOSPrincipalComp.Text = FormatNumber(oRow("OutStandingPrincipalUndue"), 0)
            ''End If
            If Not IsDBNull(oRow("InterestProporsional")) Then
                LblproporsionalInterest.Text = FormatNumber(oRow("InterestProporsional"), 0)
                txtInterestPaid.Text = FormatNumber(oRow("InterestProporsional"), 0)
                Me.InterestMustPaid = CDbl(txtInterestPaid.Text)
            End If
        End If
        Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        If Me.PrepaymentType = "C" Then
            If cboBank.SelectedItem.Value.Trim = "0" Then

                ShowMessage(lblMessage, "Harap pilih Rekening Bank", True)
                Exit Sub
            End If
        End If
        'If Me.isBatchPrepayment = True Then
        '    Me.OutStandingPrincipalUndue = CDec(LblOutstandingLoanTop.Text.Trim)
        'End If

        'If TxtPrincipalCorrection.Text.Trim <> "0" And TxtPrincipalCorrection.Text.Trim <> "" Then
        '    Me.OutStandingPrincipalUndue = CDec(TxtPrincipalCorrection.Text.Trim)
        'End If
        Try
            If Me.PrepaymentType = "B" Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .BankId = Me.BankID
                    .ApplicationID = Me.ApplicationID
                    .BranchId = Me.BranchID
                    .BusinessDate = Me.BusinessDate
                    .IsBatchPrepayment = Me.isBatchPrepayment
                End With
                cReceive.PrepaymentBPKBReplacing(oCustomClass)
            ElseIf Me.PrepaymentType = "C" Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BankId = Me.BankID
                    .FundingCoyId = Me.CompanyID
                    .FundingContractNo = Me.FundingContractNo
                    .FundingBatchNo = Me.FundingBatchNo
                    .ReferenceNo = txtReferenceNo.Text.Trim
                    .BankAccountID = cboBank.SelectedItem.Value.Trim
                    .ApplicationID = Me.ApplicationID
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .LoginId = Me.Loginid
                    .CompanyID = Me.SesCompanyID
                    .BusinessDate = Me.BusinessDate
                    .ValueDate = ConvertDate2(txtValueDate.Text)
                    .AccruedInterest = CDec(LblAccruedInterest.Text.Trim)
                    .PrepaymentAmount = CDec(LblPrepaymentAmount.Text.Trim)
                    .OSPrincipalAmount = CDec(TxtPrincipalCorrection.Text.Trim)
                    .PrincipalPaidAmount = CDec(txtInterestPaid.Text.Trim) + CDec(TxtPrincipalCorrection.Text.Trim) + CDec(LblAccruedInterest.Text.Trim)
                    .PenaltyPaidAmount = CDec(txtPenaltyPaid.Text.Trim)
                    .PrepaymentPenalty = CDec(lblPrepaymentPenalty.Text.Trim)
                    .BranchIDApplication = Me.BranchID
                    .InterestForThisMonth = CDec(txtInterestPaid.Text.Trim)
                    .IsBatchPrepayment = Me.isBatchPrepayment
                    .AmtDrawDown1 = Me.OutStandingPrincipalMustPaid
                    .AmtDrawDown2 = Me.InterestMustPaid
                End With
                cReceive.PrepaymentPaymentOut(oCustomClass)
            End If
            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
            pnlTop.Visible = True
            pnlList.Visible = True
            pnlPrepaymentBatch.Visible = False
            PNLBPKBReplacing.Visible = False
            PnlPaymentOut.Visible = False
            BindGridEntity(Me.CmdWhere)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnsearchPnlTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearchPnlTop.Click
        Dim StrSearchBy As String
        StrSearchBy = ""

        StrSearchBy = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"

        If txtSearchBy.Text.Trim <> "" Then
            StrSearchBy = StrSearchBy & " AND " & cboSearchBy.SelectedValue & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If

        BindGridEntity(StrSearchBy)
        pnlTop.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub btnResetPnlTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetPnlTop.Click
        pnlTop.Visible = True
        txtSearchBy.Text = ""
        pnlList.Visible = True
        pnlPrepaymentBatch.Visible = False
        PNLBPKBReplacing.Visible = False
        PnlPaymentOut.Visible = False
        Me.CmdWhere = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub
#Region "BindDataTop"
    Public Sub BindDataTop()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .SpName = "spFundingBatchPaymentHistoryTopView"
        End With
        oReceive = cReceive.GetGeneralEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("BatchDate")) Then
                lblBatchDateTop.Text = CType(oRow("BatchDate"), String)
            End If
            If Not IsDBNull(oRow("PrincipalAmtToFunCoy")) Then
                lblDrowDownAmtTop.Text = FormatNumber(oRow("PrincipalAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("OSAmtToFunCoy")) Then
                LblOutstandingLoanTop.Text = FormatNumber(oRow("OSAmtToFunCoy"), 0)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Channeling" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "Channeling" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"

        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click

        If Request.QueryString("Flag") <> "PREPAYMAENTBATCH" Then
            Response.Redirect("PaymentOutBatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
        Else
            Response.Redirect("PrepaymentBatch.aspx?Flag=1&Filter=" & Request.QueryString("Filter").ToString() & "&Value=" & Request.QueryString("Value").ToString())
        End If
    End Sub

    Private Sub PrepaymentByAsOfDate()
        Dim PrepaymentType As String
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingContractGetPrepaymentType"
            objCommand.Parameters.Add("@BankID", SqlDbType.VarChar, 5).Value = Me.BankID.Trim
            objCommand.Parameters.Add("@FundingCoyId", SqlDbType.VarChar, 20).Value = Me.CompanyID
            objCommand.Parameters.Add("@FundingContractNo", SqlDbType.VarChar, 20).Value = Me.FundingContractNo.Trim
            objCommand.Parameters.Add("@PrepaymentType", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            PrepaymentType = IIf(IsDBNull(objCommand.Parameters("@PrepaymentType").Value), "", objCommand.Parameters("@PrepaymentType").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        If PrepaymentType = "B" Then
            pnlTop.Visible = False
            pnlList.Visible = False
            pnlAsOf.Visible = False
            pnlPrepaymentBatch.Visible = True
            PNLBPKBReplacing.Visible = True
            PnlPaymentOut.Visible = False
            lblTopMenuText.Text = "PREPAYMENT BY BPKB REPLACING"
            Me.PrepaymentType = "B"
            Me.isBatchPrepayment = False
            View()
            btnSave.Visible = True
        ElseIf PrepaymentType = "C" Then
            pnlTop.Visible = False
            pnlList.Visible = False
            pnlAsOf.Visible = False
            pnlPrepaymentBatch.Visible = True
            PNLBPKBReplacing.Visible = True
            PnlPaymentOut.Visible = True
            lblTopMenuText.Text = "PREPAYMENT BY PAYMENT OUT"
            Me.PrepaymentType = "C"
            Me.isBatchPrepayment = False
            If Me.IsHoBranch Then
                'If Not CheckCashier(Me.Loginid) Then

                '    ShowMessage(lblMessage, "Kasir Belum Open", True)
                '    pnlTop.Visible = True
                '    pnlList.Visible = True
                '    Exit Sub
                'Else
                View()
                ViewPaymentOut()
                Exit Sub
                'End If
        Else

            ShowMessage(lblMessage, "Harap Login di Kantor Pusat", True)
            pnlTop.Visible = True
            pnlList.Visible = True
            Exit Sub
        End If
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        PrepaymentByAsOfDate()
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub

End Class