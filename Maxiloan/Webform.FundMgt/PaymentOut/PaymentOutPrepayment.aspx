﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentOutPrepayment.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.PaymentOutPrepayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentOutPrepayment</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PELUNASAN FUNDING
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="LblCompanyTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Cabang Bank</label>
                <asp:Label ID="LblCompanyBranchTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas</label>
                <asp:Label ID="LblContractNoTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Fasilitas</label>
                <asp:Label ID="LblContractNameTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:Label ID="LblBatchNoTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Batch</label>
                <asp:Label ID="lblBatchDateTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Pencairan</label>
                <asp:Label ID="lblDrowDownAmtTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Sisa Pinjaman</label>
                <asp:Label ID="LblOutstandingLoanTop" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="128px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearchPnlTop" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnResetPnlTop" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR kONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="APPLICATIONID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Enabled="True" Text='<%# Container.dataitem("ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CustomerID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Enabled="True" Text='<%# Container.dataitem("CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCust" runat="server" Enabled="True" Text='<%# Container.dataitem("Name") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OSAR" HeaderText="SISA A/R" DataFormatString="{0:N0}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OUTSTANDINGPRINCIPAL" HeaderText="SISA POKOK" DataFormatString="{0:N0}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="BranchID" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Enabled="True" Text='<%# Container.dataitem("BranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PELUNASAN PER BATCH">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbPrepaymentBatch" runat="server" ImageUrl="../../Images/iconPrepayment.gif"
                                        CommandName="PrepaymentBatch" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PELUNASAN PER KONTRAK">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbPrepayment" runat="server" ImageUrl="../../Images/iconPrepayment.gif"
                                        CommandName="Prepayment" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAsOf" runat="server" Visible="False">
        <div class="form_box_title_nobg">
            <div class="form_single">
                <label>
                    Pelunasan Per Tanggal</label>
                <%--<asp:TextBox runat="server" ID="txtValiddate1"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValiddate1"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtValiddate1" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPrepaymentBatch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTopMenuText" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Funding Bank
                </label>
                <asp:Label ID="lblFundingCompany" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Funding Cabang Bank
                </label>
                <asp:Label ID="lblPundingCompanyBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Fasilitas
                </label>
                <asp:Label ID="LblfacilityNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Fasilitas
                </label>
                <asp:Label ID="lblfacilityName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Batch
                </label>
                <asp:Label ID="LblBatchNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Batch
                </label>
                <asp:Label ID="LbldrowdownDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pencairan
                </label>
                <asp:Label ID="LblDrowdownAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Sisa Pinjaman
                </label>
                <asp:Label ID="LblOutstandingLoan" runat="server"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PNLBPKBReplacing" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h5>
                    KONTRAK INFO</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="hyAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Sisa Pokok Per Versi Customer
                </label>
                <asp:Label ID="lblOSPrincipalCust" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="HyCust" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Sisa A/R Versi Customer
                </label>
                <asp:Label ID="LblOSARCust" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No BPKB
                </label>
                <asp:Label ID="LblBPKBNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Mesin
                </label>
                <asp:Label ID="LblEngineNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Rangka
                </label>
                <asp:Label ID="LblChassisNo" runat="server"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlPaymentOut" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h5>
                    PERINCIAN PELUNASAN</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Sisa Pokok
                </label>
                <asp:Label ID="LblOSPrincipalComp" runat="server" CssClass= "numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Pokok yang dibayar
                </label>
                <%--<asp:TextBox ID="TxtPrincipalCorrection" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RvTxtPrincipalCorrection" runat="server" Type="Double" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="TxtPrincipalCorrection"
                    Display="Dynamic"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="TxtPrincipalCorrection" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Margin Berjalan
                </label>
                <asp:Label ID="LblproporsionalInterest" runat="server" CssClass= "numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Margin yang Dibayar
                </label>
                <%--<asp:TextBox ID="txtInterestPaid" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RVtxtInterestPaid" runat="server" Type="Double" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="txtInterestPaid"
                    Display="Dynamic"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtInterestPaid" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Denda Pelunasan
                </label>
                <asp:Label ID="lblPrepaymentPenalty" runat="server" CssClass= "numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda yang dibayar
                </label>
                <%--<asp:TextBox ID="txtPenaltyPaid" runat="server" Width="196px" ></asp:TextBox>
                <asp:RangeValidator ID="RVtxtPenaltyPaid" runat="server" Type="Double" MaximumValue="9999999999999"
                    ErrorMessage="Harap isi dengan Angka" MinimumValue="0" ControlToValidate="txtPenaltyPaid"
                    Display="Dynamic"></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtPenaltyPaid" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Accrued Interest
                </label>
                <asp:Label ID="LblAccruedInterest" runat="server" CssClass= "numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pelunasan
                </label>
                <asp:Label ID="LblPrepaymentAmount" runat="server" CssClass= "numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Valuta
                </label>
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>
                <%--<asp:TextBox runat="server" ID="txtValueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
            </div>
            <div class="form_right">
                <label>
                    No Bukti Kas Keluar
                </label>
                <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Rek. Bank
                </label>
                <asp:DropDownList ID="cboBank" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboBank" runat="server" ErrorMessage="Please Select Bank"
                    ControlToValidate="cboBank" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
