﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
#End Region

Public Class PrepaymentBatch
    Inherits Maxiloan.Webform.AccMntWebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController    

#End Region

#Region "Property"
    Public Property CmdWhere() As String
        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Me.IsPostBack Then
            Me.FormID = "PREBATCH"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If

            txtGoPage.Text = "1"
            Me.Sort = ""

            If Request.QueryString("Flag") = "1" Then
                cboSearchBy.SelectedValue = Request.QueryString("Filter").ToString()
                TxtSearchByValue.Text = Request.QueryString("Value").ToString()

                If TxtSearchByValue.Text.Trim <> "" Then
                    Me.CmdWhere = cboSearchBy.SelectedItem.Value + " = '" + TxtSearchByValue.Text.Trim + "'"
                Else
                    Me.CmdWhere = "ALL"
                End If
                BindGridEntity(Me.CmdWhere)
            Else
                pnllist.Visible = False
            End If

        End If
    End Sub

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdwhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingPrepaymentBatchList"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)          
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)                        
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.CmdWhere)        
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then                    
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.CmdWhere)
                End If
            End If
        End If        
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.CmdWhere)        
    End Sub

#End Region

    Private Sub BtnSearchAtas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearchAtas.Click
        If TxtSearchByValue.Text.Trim <> "" Then
            Me.CmdWhere = cboSearchBy.SelectedItem.Value + " = '" + TxtSearchByValue.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
        pnllist.Visible = True
    End Sub

    Private Sub BtnResetAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetAtas.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
        pnllist.Visible = False

        Response.Redirect("PrepaymentBatch.aspx")
    End Sub

    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label
        Dim pnl As ImageButton

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Channeling" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "Channeling" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"
        End If

    End Sub

    Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        Dim LblApplicationId As Label
        Dim LBlBranchID As Label
        LblApplicationId = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label)
        LBlBranchID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)

        Dim FundingCoyId As String
        Dim BankName As String
        Dim FundingCoyName As String
        Dim BankId As String
        Dim FundingContractNo As String
        Dim ContractName As String
        Dim FundingBatchNo As String        

        If e.CommandName = "Prepayment" Then
            FundingCoyId = e.Item.Cells(4).Text.Trim
            BankName = e.Item.Cells(12).Text.Trim
            FundingCoyName = e.Item.Cells(13).Text.Trim
            BankId = e.Item.Cells(14).Text.Trim
            FundingContractNo = e.Item.Cells(5).Text.Trim
            ContractName = e.Item.Cells(15).Text.Trim
            FundingBatchNo = e.Item.Cells(6).Text.Trim

            Response.Redirect("PaymentOutPrepayment.aspx?CompanyID=" & FundingCoyId & "&CompanyName=" & FundingCoyName & "&BankName=" & BankName & "&BankId=" & BankId & "&FundingContractID=" & FundingContractNo & "&ContractName=" & ContractName & "&ContractBatchNo=" & FundingBatchNo & "&Flag=PREPAYMAENTBATCH&Filter=" & cboSearchBy.SelectedValue.ToString() & "&Value=" & TxtSearchByValue.Text.Trim)
        End If

    End Sub
End Class