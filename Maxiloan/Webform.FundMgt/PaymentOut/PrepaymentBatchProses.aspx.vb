﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController


Public Class PrepaymentBatchProses
    Inherits WebBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private ctrContract As New GeneralPagingController
    ' Private objContract As New Parameter.GeneralPaging

    Private myDataColumn As DataColumn
    Private myDataRow As DataRow
    Private total As Double = 0

    Protected WithEvents oBankAccount As UcBankAccountID
    Protected WithEvents txtValDate As ucDateCE
    Protected WithEvents txtJumlah As ucNumberFormat
    Protected WithEvents txtEffDate As ucDateCE

    Private Property dt2 As DataTable
        Set(value As DataTable)
            ViewState("dt2") = value
        End Set
        Get
            Return CType(ViewState("dt2"), DataTable)
        End Get
    End Property

    Private Property PrepaymentType() As String
        Get
            Return CStr(viewstate("PrepaymentType"))
        End Get
        Set(ByVal Value As String)
            viewstate("PrepaymentType") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "PREPAYFUNDB"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                txtEffDate.Text = BusinessDate.ToString("dd/MM/yyyy")
                txtEffDate.IsRequired = True
                bindComboCompany()
                CreateAgreementTable()
                obankaccount.BankPurpose = ""
                oBankAccount.BankType = "BA"
                obankaccount.IsAll = True
                obankaccount.BindBankAccount()
            End If
        End If
    End Sub

    Protected Sub InitialDefaultPanel()

    End Sub

    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView

            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()            
        Catch e As Exception

            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub

    Protected Sub SortGrid(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)

    End Sub

    Private Sub Btnsearch_Click(sender As Object, e As System.EventArgs) Handles Btnsearch.Click
        If drdCompany.SelectedItem.Value.Trim = "ALL" Then
            Me.SearchBy = ""
        Else
            Me.SearchBy = "fa.FundingCoyID = '" & drdCompany.SelectedItem.Value.Trim & "'"
        End If

        If txtSearchBy.Text.Trim <> "" And cboSearchBy.SelectedValue <> "" Then
            Me.SearchBy = Me.SearchBy & " AND " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub BtnReset_Click(sender As Object, e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PrepaymentBatchProses.aspx")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim objContract As New Parameter.GeneralPaging
        With objContract
            .SpName = "spFundingPrepaymentBatchList"
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        objContract = ctrContract.GetGeneralPaging(objContract)

        DtUserList = objContract.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = objContract.TotalRecords
        DvUserList.Sort = Me.SortBy
        dtg1.DataSource = DvUserList

        Try
            dtg1.DataBind()
        Catch
            dtg1.CurrentPageIndex = 0
            dtg1.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If

        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    DoBind(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
    End Sub

    Private Sub dtg1_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg1.ItemCommand
        If e.CommandName.ToUpper = "ADD" Then

            If (dt2.Rows.Count >= 30) Then
                ShowMessage(lblMessage, "Maksimum daftar kontrak akan dilunasi 30", False)
                Return
            End If
            Dim row As DataRow = (From column In dt2.Rows Where column("ApplicationID") = e.Item.Cells(1).Text.Trim).FirstOrDefault()
            If (row Is Nothing) Then

                myDataRow = dt2.NewRow()

                myDataRow("ApplicationID") = e.Item.Cells(1).Text.Trim
                myDataRow("AgreementNo") = e.Item.Cells(2).Text
                myDataRow("Name") = e.Item.Cells(3).Text
                myDataRow("Tenor") = e.Item.Cells(4).Text
                myDataRow("InsSeqNo") = e.Item.Cells(5).Text
                myDataRow("OSP") = e.Item.Cells(6).Text
                myDataRow("OSI") = e.Item.Cells(7).Text
                myDataRow("Total") = e.Item.Cells(8).Text
                myDataRow("FundingContractNo") = e.Item.Cells(9).Text
                myDataRow("FundingBatchNo") = e.Item.Cells(10).Text
                myDataRow("FundingCoyID") = e.Item.Cells(11).Text
                myDataRow("BankID") = e.Item.Cells(12).Text

                total = 0
                dt2.Rows.Add(myDataRow)
                dtg2.DataSource = dt2
                dtg2.DataBind()

            End If
            CType(e.Item.FindControl("AddButton"), Button).Enabled = False
            pnlDtGrid.Visible = True
        End If
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg2.ItemCommand

        If e.CommandName.ToUpper = "REMOVE" Then
            Dim imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            Dim row As DataRow = (From column In dt2.Rows Where column("ApplicationID") = e.Item.Cells(1).Text.Trim).FirstOrDefault()
            If (Not row Is Nothing) Then
                dt2.Rows.Remove(row)
                dtg2.DataSource = dt2
                dtg2.DataBind()


                Dim cel = (From x As DataGridItem In dtg1.Items Where x.Cells(1).Text.Trim = e.Item.Cells(1).Text.Trim()).FirstOrDefault()
                If (Not cel Is Nothing) Then
                    CType(cel.FindControl("AddButton"), Button).Enabled = True
                End If

            End If
        End If
    End Sub
    Sub CreateAgreementTable()
        dt2 = New DataTable("AgreementSelected")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ApplicationID"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "AgreementNo"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Name"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "Tenor"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "InsSeqNo"
        dt2.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "OSP"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "OSI"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Total"
        dt2.Columns.Add(myDataColumn)

        

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "FundingContractNo"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "FundingBatchNo"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "FundingCoyID"
        dt2.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "BankID"
        dt2.Columns.Add(myDataColumn)
    End Sub

    Private Sub BtnNext_Click(sender As Object, e As System.EventArgs) Handles BtnNext.Click
        If (txtEffDate.Text.Trim = "") Then
            ShowMessage(lblMessage, "Tanggal Effektif tidak valid", True)
            Return
        End If

        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        Panel2.Visible = True
        total = 0
        For Each x As DataGridItem In dtg2.Items
            If (x.ItemType = ListItemType.AlternatingItem Or x.ItemType = ListItemType.Item) And CType(x.FindControl("chkCheck"), CheckBox).Checked Then
                total += CDbl(x.Cells(8).Text)
            End If
        Next

        txtValDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtJumlah.Text = FormatNumber(total, 0)
        txtJumlah.Enabled = False

    End Sub

    Private Sub ButtonBack_Click(sender As Object, e As System.EventArgs) Handles ButtonBack.Click
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        Panel2.Visible = False
    End Sub
    Function toDataTable() As DataTable
        Dim dt = New DataTable()
        Dim strArray As String() = New String() {"id", "value1", "value2"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next


        For Each v In dt2.Rows
            Dim row = dt.NewRow()
            row("id") = v("applicationID").Trim()
            row("value1") = ""
            row("value2") = ""
            dt.Rows.Add(row)
        Next
        Return dt
    End Function
    Private Sub ButtonSave_Click(sender As Object, e As System.EventArgs) Handles ButtonSave.Click
        Try  
            Dim cReceive As New PembayaranAngsuranFundingController
            Dim oReceive As New Parameter.FundingContractBatch
            Dim oCustomClass As New Parameter.FundingContractBatch() With {
                .strConnection = GetConnectionString(),
                .ReferenceNo = txtNoBukti.Text.Trim,
                .BankAccountID = oBankAccount.BankAccountID,
                .AgreementNoTbl = toDataTable(),
                .BranchId = sesBranchId.Replace("'", ""),
                .LoginId = Loginid,
                .CompanyID = SesCompanyID,
                .BusinessDate = BusinessDate,
                .ValueDate = ConvertDate2(txtValDate.Text),
                .TotalPrepaymentAmount = CDec(txtJumlah.Text),
                .EffectiveDate = ConvertDate2(txtEffDate.Text),
                .Catatan = txtCatatan.Text
            }

            cReceive.prepaymentBatchProcess(oCustomClass)

            Response.Redirect("PrepaymentBatchProses.aspx")
            
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtg2_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg2.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            total += CDbl(e.Item.Cells(8).Text)
        End If 
        If e.Item.ItemType = ListItemType.Footer Then e.Item.Cells(8).Text = FormatNumber(total, 0)  
    End Sub


    Private Sub dtg1_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then 
            Dim row As DataRow = (From column In dt2.Rows Where column("ApplicationID") = e.Item.Cells(1).Text.Trim).FirstOrDefault()
            CType(e.Item.FindControl("AddButton"), Button).Enabled = row Is Nothing
        End If
    End Sub
End Class