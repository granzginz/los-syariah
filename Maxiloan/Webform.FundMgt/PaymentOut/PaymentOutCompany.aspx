﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentOutCompany.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.PaymentOutCompany" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PaymentOutCompany</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingCompanyView(Style, CompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.FundMgt/View/FundingCompanyView.aspx?Style=' + Style + '&CompanyID=' + CompanyID, 'Channeling', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=1,scrollbars=1');
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    FUNDING BANK
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                    <asp:ListItem Value="0" Selected="True">Not Specified</asp:ListItem>
                    <asp:ListItem Value="FundingCoyId">ID Funding Bank</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearchnew" runat="server" CausesValidation="False" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnResetnew" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR FUNDING BANK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingCoList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="FundingCoyId"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="FUNDINGCOYID" HeaderText="ID FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCompanyID" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyID") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BANKNAME" SortExpression="BANKNAME" HeaderText="NAMA FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FUNDINGCOYNAME" SortExpression="FUNDINGCOYNAME" HeaderText="NAMA CABANG">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="FASILITAS">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../Images/iconcontract.gif"
                                        CommandName="Contract" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BankId" Visible="False">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
