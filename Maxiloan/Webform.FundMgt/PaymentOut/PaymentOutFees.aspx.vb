﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class PaymentOutFees
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "uccomponent"

    Protected WithEvents txtAdministrationFee As ucNumberFormat
    Protected WithEvents txtComitmentFee As ucNumberFormat
    Protected WithEvents txtProvision As ucNumberFormat
    Protected WithEvents txtOthersFee As ucNumberFormat    
    Protected WithEvents txtValueDate As ucDateCE

#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property
    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController
    Private oCashierCustomClass As New Parameter.CashierTransaction    

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Me.IsPostBack Then
            Me.FormID = "PAYOUT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If

            Me.SearchBy = ""
            Me.SortBy = ""
            Dim cmdwhere As String
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("Fundingcontractid") <> "" Then Me.FundingContractNo = Request.QueryString("Fundingcontractid")
            If Request.QueryString("ContractBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("ContractBatchNo")
            lblFundingCompany.Text = Me.BankName
            lblPundingCompanyBranch.Text = Me.CompanyName
            LblfacilityNo.Text = Me.FundingContractNo
            lblfacilityName.Text = Me.ContractName
            fillcboBank()
            pnlView.Visible = True
            If Me.IsHoBranch Then
                'If Not CheckCashier(Me.Loginid) Then

                '    ShowMessage(lblMessage, "Kasir Tidak Open", True)
                '    View()
                '    btnSave.Visible = False
                'Else
                View()
                btnSave.Visible = True
                'End If
            Else
                ShowMessage(lblMessage, "Harap Login di kantor pusat", True)
                View()
                btnSave.Visible = False
            End If

            InitiateUCnumberFormat(txtAdministrationFee, False, True)
            InitiateUCnumberFormat(txtComitmentFee, False, True)
            InitiateUCnumberFormat(txtProvision, False, True)
            InitiateUCnumberFormat(txtOthersFee, False, True)

            txtComitmentFee.RangeValidatorMinimumValue = "0"
            txtComitmentFee.RangeValidatorMaximumValue = "9999999999999"

            txtOthersFee.RangeValidatorMinimumValue = "0"
            txtOthersFee.RangeValidatorMaximumValue = "9999999999999"


            LblBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & Me.FundingBatchNo & "')"

        End If
    End Sub
#Region "FillCboBank"
    Sub fillcboBank()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFundingGetHOBranch"
            objCommand.Parameters.Add("@CompanyID", SqlDbType.Char, 3).Value = Me.SesCompanyID.Trim
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.BranchID = IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
        Dim dtBankName As New DataTable
        dtBankName = m_controller.GetBankAccount(GetConnectionString, Me.BranchID.Trim, "BA", "")
        With cboBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dtBankName
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub
#End Region
    Public Sub BindTitleTop()
        lblFundingCompany.Text = Me.BankName.Trim
        lblPundingCompanyBranch.Text = Me.CompanyName.Trim
        LblfacilityNo.Text = Me.FundingContractNo.Trim
        lblfacilityName.Text = Me.ContractName.Trim
        LblBatchNo.Text = Me.FundingBatchNo.Trim
    End Sub
    Public Sub View()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim StrapplicationID As String
        Dim StrCustomerId As String
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        With oCustomClass
            .strConnection = GetConnectionString
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .SpName = "spFundingPaymentOutInstallmentView"
        End With
        oReceive = cReceive.GetGeneralEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("FundingBatchNo")) Then
                LblBatchNo.Text = CStr(dtslist.Rows(0).Item("FundingBatchNo")).Trim
            End If

            If Not IsDBNull(oRow("BatchDate")) Then
                LbldrowdownDate.Text = CType(oRow("BatchDate"), String)
            End If
            If Not IsDBNull(oRow("PrincipalAmtToFunCoy")) Then
                LblDrowdownAmount.Text = FormatNumber(oRow("PrincipalAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("OSAmtToFunCoy")) Then
                LblOutstandingLoan.Text = FormatNumber(oRow("OSAmtToFunCoy"), 0)
            End If

            txtAdministrationFee.Text = FormatNumber(oRow("AdminPerBPKB"), 0)
            txtProvision.Text = FormatNumber(oRow("ProvisionFeeAmount"), 0)

            txtProvision.RangeValidatorMinimumValue = "0"
            txtProvision.RangeValidatorMaximumValue = oRow("ProvisionFeeAmount")

            txtAdministrationFee.RangeValidatorMinimumValue = "0"
            txtAdministrationFee.RangeValidatorMaximumValue = oRow("AdminPerBPKB")

        End If
    End Sub

    Private Sub btnPaymentHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPaymentHistory.Click
        btnPaymentHistory.Attributes.Add("onClick", "javascript:OpenFundingViewPaymentHistory('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & Me.FundingBatchNo.Trim & "');")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("PaymentOutBatch.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        If cboBank.SelectedItem.Value.Trim = "0" Then

            ShowMessage(lblMessage, "Harap Pilih Rekening Bank", True)
            Exit Sub
        End If
        Try

            With oCustomClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .BankId = Me.BankID
                .AdminFeeFacility = CDec(txtAdministrationFee.Text.Trim)
                .CommitmentFee = CDec(txtComitmentFee.Text.Trim)
                .ProvisionFeeAmount = CDec(txtProvision.Text.Trim)
                .OtherFeeAmount = CDec(txtOthersFee.Text.Trim)
                .BankAccountID = cboBank.SelectedItem.Value.Trim
                .ReferenceNo = txtReferenceNo.Text
                .LoginId = Me.Loginid
                .CompanyID = Me.SesCompanyID
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BusinessDate = Me.BusinessDate
                .ValueDate = ConvertDate2(txtValueDate.Text)
            End With
            cReceive.PaymentOutFees(oCustomClass)

            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign medium_text"
        End With
    End Sub
End Class