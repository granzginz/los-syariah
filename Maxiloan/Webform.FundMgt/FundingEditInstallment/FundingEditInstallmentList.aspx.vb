﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services

#End Region


Public Class FundingEditInstallmentList
    Inherits Maxiloan.Webform.AccMntWebBased
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController
    Dim customClass As New Parameter.FundingContractBatch

    Private Shared conn2 As String
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.FormID = "FUNEDITINSTALL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            conn2 = GetConnectionString()
            Me.SearchBy = ""

            Me.SortBy = ""
            BindGridEntity(Me.SearchBy, Me.SortBy)
            bindFundingCoy()
            bindFundingContract(drdFundingCoy.SelectedValue.Trim)
            bindFundingBatch(drdFundingCoy.SelectedValue.Trim, drdContract.SelectedValue.Trim)
            'hideContract.Value = "0"
        End If
    End Sub
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingEditDrawDownList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        PagingFooter()

    End Sub
    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        Dim hyBranch As HyperLink
        Dim hyContract As HyperLink
        Dim lblFundingCoyID As Label
        Dim lblBankID As Label
        Dim hyBatchNo As HyperLink
        Dim lblContractName As Label
        Dim lblBankName As Label
        hyBranch = CType(e.Item.FindControl("lnkBranch"), HyperLink)
        hyContract = CType(e.Item.FindControl("lnkContractNo"), HyperLink)
        lblBankName = CType(e.Item.FindControl("lblBankName"), Label)
        lblFundingCoyID = CType(e.Item.FindControl("lblFundingCoyID"), Label)
        lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
        hyBatchNo = CType(e.Item.FindControl("lnkBatchNo"), HyperLink)
        lblContractName = CType(e.Item.FindControl("lblContractName"), Label)
        If e.Item.ItemIndex >= 0 Then
            hyBranch.NavigateUrl = "javascript:OpenFundingCompanyView('" & "channeling" & "', '" & lblFundingCoyID.Text.Trim & "')"
            hyContract.NavigateUrl = "javascript:OpenFundingCompanyContractView('" & "channeling" & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & hyContract.Text.Trim & "')"
            hyBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & lblFundingCoyID.Text.Trim & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & lblBankID.Text.Trim & "','" & lblContractName.Text.Trim & "','" & hyContract.Text.Trim & "','" & hyBatchNo.Text.Trim & "')"
        End If
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString



        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)

    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)

    End Sub

#End Region
    Protected Sub bindFundingCoy()
        Dim constr As String = GetConnectionString()
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("SELECT DISTINCT FundingCoyID,FundingCoyName FROM FundingCoy a")
                cmd.CommandType = CommandType.Text
                cmd.Connection = con
                con.Open()
                drdFundingCoy.DataSource = cmd.ExecuteReader()
                drdFundingCoy.DataTextField = "FundingCoyName"
                drdFundingCoy.DataValueField = "FundingCoyID"
                drdFundingCoy.DataBind()
                con.Close()
            End Using
        End Using
        drdFundingCoy.Items.Insert(0, New ListItem("ALL", "0"))

    End Sub


    '<System.Web.Services.WebMethod()> _
    'Public Shared Function GetCountries() As List(Of Contract)
    '    Dim _Contract As List(Of Contract) = New List(Of Contract)

    '    Dim country As New Country
    '    country.Name = "India"
    '    country.Id = "1"
    '    countries.Add(country)

    '    country = New Country
    '    country.Name = "USA"
    '    country.Id = "2"
    '    countries.Add(country)

    '    country = New Country
    '    country.Name = "Canada"
    '    country.Id = "3"
    '    countries.Add(country)

    '    Return _Contract
    'End Function


    <WebMethod()> _
    Public Shared Function GetFundingCoy() As List(Of ListItem)
        Dim query As String = "select 'ALL' FundingCoyID, 'ALL' FundingCoyName union all SELECT DISTINCT FundingCoyID,FundingCoyName FROM FundingCoy "
        Dim constr As String = conn2
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(query)
                Dim customers As New List(Of ListItem)()
                cmd.CommandType = CommandType.Text
                cmd.Connection = con
                con.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(New ListItem() With { _
                          .Value = sdr("FundingCoyID").ToString(), _
                          .Text = sdr("FundingCoyName").ToString() _
                        })
                    End While
                End Using
                con.Close()
                Return customers
            End Using
        End Using
    End Function

    <WebMethod()> _
    Public Shared Function GetFundingContract(ByVal _FundingCoyID As String) As List(Of ListItem)
        Dim Xquery = ""
        If _FundingCoyID = "0" Then
            Xquery = "SELECT DISTINCT a.FundingContractNo,a.ContractName  FROM FundingContract a "
        Else
            Xquery = " SELECT DISTINCT a.FundingContractNo,a.ContractName  FROM FundingContract a where a.FundingCoyID = '" & _FundingCoyID & "' "
        End If
        Dim query As String = Xquery
        Dim constr As String = conn2
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(query)
                Dim customers As New List(Of ListItem)()
                cmd.CommandType = CommandType.Text
                cmd.Connection = con
                con.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(New ListItem() With { _
                          .Value = sdr("FundingContractNo").ToString(), _
                          .Text = sdr("ContractName").ToString() _
                        })
                    End While
                End Using
                con.Close()
                Return customers
            End Using
        End Using
    End Function


    '<WebMethod()> _
    'Public Shared Function GetFundingBatch(ByVal _FundingCoyID2 As String, ByVal _FundingContractNo2 As String) As List(Of ListItem)
    '    Dim Xquery = ""
    '    If _FundingContractNo2 = "0" And _FundingContractNo2 = "0" Then
    '        Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb "
    '    ElseIf _FundingContractNo2 <> "0" And _FundingContractNo2 = "0" Then
    '        Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb where fb.FundingCoyID = '" & _FundingCoyID2 & "' "
    '    ElseIf _FundingContractNo2 = "0" And _FundingContractNo2 <> "0" Then
    '        Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb where fb.FundingContractNo = '" & _FundingContractNo2 & "'"
    '    ElseIf _FundingContractNo2 <> "0" And _FundingContractNo2 <> "0" Then
    '        Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb where fb.FundingCoyID = '" & _FundingCoyID2 & "' and fb.FundingContractNo = '" & _FundingContractNo2 & "' "
    '    End If
    '    Dim query As String = Xquery
    '    Dim constr As String = conn2
    '    Using con As New SqlConnection(constr)
    '        Using cmd As New SqlCommand(query)
    '            Dim customers As New List(Of ListItem)()
    '            cmd.CommandType = CommandType.Text
    '            cmd.Connection = con
    '            con.Open()
    '            Using sdr As SqlDataReader = cmd.ExecuteReader()
    '                While sdr.Read()
    '                    customers.Add(New ListItem() With { _
    '                      .Value = sdr("FundingBatchNo").ToString(), _
    '                      .Text = sdr("FundingBatchNo").ToString() _
    '                    })
    '                End While
    '            End Using
    '            con.Close()
    '            Return customers
    '        End Using
    '    End Using
    'End Function
    'Public Class Contract
    '    Private _name As String
    '    Public Property Name As String
    '        Get
    '            Return _name
    '        End Get
    '        Set(ByVal value As String)
    '            _name = value
    '        End Set
    '    End Property

    '    Private _id As String
    '    Public Property Id As String
    '        Get
    '            Return _id
    '        End Get
    '        Set(ByVal value As String)
    '            _id = value
    '        End Set
    '    End Property
    'End Class

    Protected Sub bindFundingBatch(ByVal _FundingCoyID As String, ByVal _FundingContractNo As String)
        Dim Xquery As String = ""
        If _FundingCoyID = "0" And _FundingContractNo = "0" Then
            Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb "
        ElseIf _FundingCoyID <> "0" And _FundingContractNo = "0" Then
            Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb where fb.FundingCoyID = '" & _FundingCoyID & "' "
        ElseIf _FundingCoyID = "0" And _FundingContractNo <> "0" Then
            Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb where fb.FundingContractNo = '" & _FundingContractNo & "'"
        ElseIf _FundingCoyID <> "0" And _FundingContractNo <> "0" Then
            Xquery = " SELECT DISTINCT fb.FundingBatchNo FROM dbo.FundingBatch AS fb where fb.FundingCoyID = '" & _FundingCoyID & "' and fb.FundingContractNo = '" & _FundingContractNo & "' "
        End If
        Dim constr As String = GetConnectionString()
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(Xquery)
                cmd.CommandType = CommandType.Text
                cmd.Connection = con
                con.Open()
                drdBatchNo.DataSource = cmd.ExecuteReader()
                drdBatchNo.DataTextField = "FundingBatchNo"
                drdBatchNo.DataValueField = "FundingBatchNo"
                drdBatchNo.DataBind()
                con.Close()
            End Using
        End Using
        drdBatchNo.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub

    Protected Sub bindFundingContract(ByVal _FundingCoyID As String)
        Dim Xquery As String = ""
        If _FundingCoyID = "0" Then
            Xquery = "SELECT DISTINCT a.FundingContractNo,a.ContractName  FROM FundingContract a "
        Else
            Xquery = " SELECT DISTINCT a.FundingContractNo,a.ContractName  FROM FundingContract a where a.FundingCoyID = '" & _FundingCoyID & "' "
        End If
        Dim constr As String = GetConnectionString()
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(Xquery)
                cmd.CommandType = CommandType.Text
                cmd.Connection = con
                con.Open()
                drdContract.DataSource = cmd.ExecuteReader()
                drdContract.DataTextField = "ContractName"
                drdContract.DataValueField = "FundingContractNo"
                drdContract.DataBind()
                con.Close()
            End Using
        End Using
        drdContract.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub
    Protected Sub drdFundingCoy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drdFundingCoy.SelectedIndexChanged
        bindFundingContract(drdFundingCoy.SelectedValue.Trim)
        bindFundingBatch(drdFundingCoy.SelectedValue.Trim, drdContract.SelectedValue.Trim)
    End Sub

    Protected Sub drdContract_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drdContract.SelectedIndexChanged
        bindFundingBatch(drdFundingCoy.SelectedValue.Trim, drdContract.SelectedValue.Trim)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Me.SearchBy = " and FundingContract.FundingCoyID =" & If(drdFundingCoy.SelectedValue.Trim = "0", " FundingContract.FundingCoyID ", "'" & drdFundingCoy.SelectedValue.Trim & "'") & " " & _
                      " and FundingContract.FundingContractNo =" & If(drdContract.SelectedValue.Trim = "0", " FundingContract.FundingContractNo ", "'" & drdContract.SelectedValue.Trim & "'") & " " & _
                      " and FundingBatch.FundingBatchNo =" & If(drdBatchNo.SelectedValue.Trim = "0", " FundingBatch.FundingBatchNo ", "'" & drdBatchNo.SelectedValue.Trim & "'") & " "
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Me.SearchBy = " "
        BindGridEntity(Me.SearchBy, Me.SortBy)
        bindFundingCoy()
        bindFundingContract(drdFundingCoy.SelectedValue.Trim)
        bindFundingBatch(drdFundingCoy.SelectedValue.Trim, drdContract.SelectedValue.Trim)
    End Sub

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
        'If e.CommandName = "View" Then
        '    Response.Redirect("ApplicationMultiAsset_002.aspx?id=" & CustId & "&name=" & CustName & "&type=" & Type & "")
        'End If

        Dim lblBankName, lblBankID, lblContractName, lblFundingCoyID As Label
        Dim lnkBranch, lnkBatchNo As HyperLink
        Dim cmdWhere As String

        lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), HyperLink)
        lblFundingCoyID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblFundingCoyID"), Label)
        lnkBatchNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkBatchNo"), HyperLink)
        lblBankID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBankID"), Label)
      

        If e.Item.ItemIndex >= 0 Then
            If e.CommandName = "RECEIVE" Then
                'Response.Redirect("FundingEditInstallment.aspx?ApplicationID=" & AppId & "")
                Response.Redirect("FundingEditInstallment.aspx?FundingCoyID=" & lblFundingCoyID.Text.Trim & " &FundingContractNo=" & lnkContractNo.Text.Trim & " &FundingBatchNo=" & lnkBatchNo.Text.Trim & "")
            End If
        End If
    End Sub
#End Region
End Class