﻿
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingEditInstallmentList.aspx.vb"  Inherits="Maxiloan.Webform.FundMgt.FundingEditInstallmentList" ValidateRequest="false" EnableEventValidation="false"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>FundingEditInstallment</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.6.4.js"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingCompanyView(Style, CompanyID) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyView.aspx?Style=' + Style + '&CompanyID=' + CompanyID, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1,scrollbars=1');
        }
        function OpenFundingCompanyContractView(pStyle, pCompanyName, pBankName, pFundingContractNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingCompanyContractView.aspx?Style=' + pStyle + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&FundingContractNo=' + pFundingContractNo, 'Channeling', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function OpenFundingBatchView(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.FundMgt/View/FundingBatchView.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'Channeling', 'left=50, top=10, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        //        var ddlCountries;
        //        function GetCountries() {
        //            ddlCountries = document.getElementById("<%=drdFundingCoy.ClientID %>");
        //            ddlCountries.options.length == 0;
        //            AddOption("Loading", "0");
        //            PageMethods.GetCountries(OnSuccess);
        //        }
        //        window.onload = GetCountries;

        //        function OnSuccess(response) {
        //            ddlCountries.options.length = 0;
        //            AddOption("Please select", "0");
        //            for (var i in response) {
        //                AddOption(response[i].Name, response[i].value);
        //            }
        //        }
        //        function AddOption(text, value) {
        //            var option = document.createElement('option');
        //            option.value = value;
        //            option.innerHTML = text;
        //            ddlCountries.options.add(option);
        //        }

      

    </script>
</head>
<body>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" >
    </asp:ScriptManager>
        <script type="text/javascript">
//      $(function () {
//            $.ajax({
//                type: "POST",
//                url: "FundingEditInstallmentList.aspx/GetFundingCoy",
//                data: '{}',
//                contentType: "application/json; charset=utf-8",
//                dataType: "json",
//                success: function (r) {
//                    var ddlFundingCoy = $("[id*=drdFundingCoy]");
//                    ddlFundingCoy.empty().append('<option selected="selected" value="ALL">ALL</option>');
//                    $.each(r.d, function () {
//                        ddlFundingCoy.append($("<option></option>").val(this['Value']).html(this['Text']));
//                    });
//                }
//            });
//        });
//        function OnFundingContract() {

//            var _drdFundingCoy = '';
//        
//            _drdFundingCoy = document.getElementById("<%=drdFundingCoy.ClientID %>").value;

//            $(function () {
//                $.ajax({
//                    type: "POST",
//                    url: "FundingEditInstallmentList.aspx/GetFundingContract",
//                    data: "{_FundingCoyID:" + _drdFundingCoy + "}",
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    success: function (r) {
//                        var ddlContract = $("[id*=drdContract]");
//                        ddlContract.empty().append('<option selected="selected" value="0">ALL</option>');
//                        $.each(r.d, function () {
//                            ddlContract.append($("<option></option>").val(this['Value']).html(this['Text']));
//                        });



//                    }
//                });
//            });
////            OnLoadFundingBatch();
//        }

//        window.onload = OnFundingContract;


//        function OnFundingBatch() {
//            var _drdContract2 = '';
//            _drdContract2 = document.getElementById("<%=drdContract.ClientID %>").value;

           
//            var _drdFundingCoy2 = '';
//            var _drdContract2 = '';

//            _drdFundingCoy2 = document.getElementById("<%=drdFundingCoy.ClientID %>").value;
//            _drdContract2 = document.getElementById("<%=drdContract.ClientID %>").value;

//            //            var oParam = { "_FundingCoyID": "", "_FundingContractNo": ""};
//            //            var oParam = {  "_FundingContractNo": "" };
//            //            oParam._FundingCoyID = _drdFundingCoy;
//            //            oParam._FundingContractNo = _drdContract;

//            //            alert(oParam);

//            $(function () {
//                $.ajax({
//                    type: "POST",
//                    url: "FundingEditInstallmentList.aspx/GetFundingBatch",
//                    data: "{_FundingCoyID2:" + _drdFundingCoy2 + ", _FundingContractNo2:" + _drdContract2 + "}",
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    success: function (r) {
//                        var drdNoBatch = $("[id*=drdNoBatch]");
//                        drdNoBatch.empty().append('<option selected="selected" value="0">ALL</option>');
//                        $.each(r.d, function () {
//                            drdNoBatch.append($("<option></option>").val(this['Value']).html(this['Text']));
//                        });
//                    }
//                });
//            });
    //    }



</script>
 <%--   <asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>--%>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
 

           <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI FUNDING BATCH
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank / Cabang</label>
                <asp:DropDownList ID="drdFundingCoy" runat="server" AutoPostBack="True"  >
                </asp:DropDownList>
            </div>
            <%--onchange="OnFundingContract();"--%>
          <%--  onchange="OnFundingContract();"--%>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Kontrak Bank</label>
                
                <asp:DropDownList ID="drdContract" runat="server" AutoPostBack="True" >
                </asp:DropDownList>
               <%-- <asp:HiddenField ID="hideContract" runat="server" />--%>
            </div>
        </div>

         <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                
                <asp:DropDownList ID="drdBatchNo" runat="server" >
                </asp:DropDownList>
               <%-- <asp:HiddenField ID="hideContract" runat="server" />--%>
            </div>
        </div>
        <%--<div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:DropDownList ID="drdNoBatch" runat="server" >
                </asp:DropDownList>
            </div>
        </div>--%>
       
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
         <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR FUNDING BATCH
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingContractList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFundingCoyID" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" runat="server" Enabled="True" Text='<%# Container.dataitem("BankName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblContractName" runat="server" Enabled="True" Text='<%# Container.dataitem("ContractName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankID" runat="server" Enabled="True" Text='<%# Container.dataitem("BankID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FUNDING BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBranch" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingCoyName") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO FASILITAS BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkContractNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingContractNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO BATCH">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBatchNo" runat="server" Enabled="True" Text='<%# Container.dataitem("FundingBatchNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BATCHDATE" HeaderText="TGL BATCH" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrincipalAmtToFunCoy" HeaderText="JUMLAH" DataFormatString="{0:N0}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CurrencyID" HeaderText="MATA UANG">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle> 
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imbreceive" runat="server" ImageUrl="../../Images/IconReceived.gif"
                                        CommandName="RECEIVE" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        Display="Dynamic" ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
      <%--  </ContentTemplate>
        </asp:UpdatePanel>--%>
    </form>
</body>
</html>
