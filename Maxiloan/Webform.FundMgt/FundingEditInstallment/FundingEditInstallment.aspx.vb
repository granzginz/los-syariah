﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class FundingEditInstallment
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents txtBungaPokok As ucNumberFormat
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink
    Private m_controller As New DataUserControlController
    Dim customClass As New Parameter.FundingContractBatch

    Private Shared conn2 As String
#End Region

#Region " Property "
    Private Property FundingCoyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(ViewState("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property





#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.FormID = "FUNEDITINSTALL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            Me.FundingCoyID = Request("FundingCoyID")
            Me.FundingContractNo = Request("FundingContractNo")
            Me.FundingBatchNo = Request("FundingBatchNo")
            Dim cmdWhere As String
            cmdWhere = "FundingContract.FundingContractNo = '" & Me.FundingContractNo.Trim & "' and "
            cmdWhere = cmdWhere + "FundingContract.FundingCoyID = '" & Me.FundingCoyID & "' and "
            cmdWhere = cmdWhere + "FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
            display(cmdWhere, Me.FundingBatchNo)
            BindDataAngsuranFunding()

        End If
    End Sub
    Private Sub BindDataAngsuranFunding()
        Dim oEntities As New Parameter.FundingContractBatch
        Dim dtViewData As New DataTable
        With oEntities
            .FundingBatchNo = Me.FundingBatchNo.Trim
            .BankId = Me.FundingCoyID.Trim
            .FundingContractNo = Me.FundingContractNo.Trim
            .strConnection = GetConnectionString()
        End With
        oEntities = GetFundingBatchInstallment(oEntities)
        dtViewData = oEntities.ListData
        GridView1.DataSource = dtViewData

        GridView1.DataBind()
    End Sub
    Public Function GetFundingBatchInstallment(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingBatchNo

            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(1).Value = customclass.BankId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewFundingBatchInstallmentEdit", params).Tables(0)
            Return customclass
        Catch exp As Exception

        End Try

    End Function

    Public Sub display(ByVal cmdwhere As String, ByVal FundingBatchNo As String)
        Try
            Dim dtslist As DataTable
            Dim cReceive As New GeneralPagingController
            Dim oReceive As New Parameter.GeneralPaging
            With oReceive
                .strConnection = GetConnectionString()
                .WhereCond = cmdwhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = FundingBatchNo
                .SpName = "spFundingDrawDownFundSelect"
            End With
            oReceive = cReceive.GetGeneralPaging(oReceive)
            dtslist = oReceive.ListData
            If dtslist.Rows.Count > 0 Then
                If dtslist.Rows(0).Item("BankName") <> "" Then
                    lblFundingCompanyName.Text = CStr(dtslist.Rows(0).Item("BankName")).Trim
                End If
                If dtslist.Rows(0).Item("FundingCoyName") <> "" Then
                    lblFundingCompanyBranch.Text = CStr(dtslist.Rows(0).Item("FundingCoyName")).Trim
                End If
                If dtslist.Rows(0).Item("FundingContractNo") <> "" Then
                    lblFacilityNo.Text = CStr(dtslist.Rows(0).Item("FundingContractNo")).Trim
                End If
                If dtslist.Rows(0).Item("ContractName") <> "" Then
                    lblFacilityName.Text = CStr(dtslist.Rows(0).Item("ContractName")).Trim
                End If
                If dtslist.Rows(0).Item("FundingBatchNo") <> "" Then
                    lblBatchNo.Text = CStr(dtslist.Rows(0).Item("FundingBatchNo")).Trim
                End If
                lblDrawdownPlanDate.Text = CStr(dtslist.Rows(0).Item("ProposeDate")).Trim
                lblDrawdownAmount.Text = FormatNumber(dtslist.Rows(0).Item("PrincipalAmtToFunCoy"), 0)
                lblPokok.Text = FormatNumber(dtslist.Rows(0).Item("FirstPrincipalAmount"), 0)
                'lblBunga.Text = FormatNumber(dtslist.Rows(0).Item("FirstInterestAmount"), 0)
                txtBungaPokok.Text = FormatNumber(dtslist.Rows(0).Item("FirstInterestAmount"), 0)
                If dtslist.Rows(0).Item("InterestType") <> "" Then
                    lblInterestType.Text = CStr(dtslist.Rows(0).Item("InterestType")).Trim
                End If
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub
    Public Sub FundingBatchInstallmentEdit(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId
            params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo
            params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo
            params(3) = New SqlParameter("@FundingBatchInstallmentEdit", SqlDbType.Structured)
            params(3).Value = customclass.ListData

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingBatchInstallmentEdit", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()

            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("InsSeqNo")
        dt.Columns.Add("PrincipalAmount")
        dt.Columns.Add("InterestAmount")
        dt.Columns.Add("OSPrincipalAmount")
        dt.Columns.Add("OSInterestAmount")

        Return dt
    End Function

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.FundingContractBatch
        lblMessage.Text = ""
        Try
            Dim dt As DataTable = createNewDT()
            Dim lblSeqNo As Label
            Dim txtPokok As TextBox
            Dim txtBunga As TextBox
            Dim txtSaldoPokok As TextBox
            Dim txtSaldoBunga As TextBox

            Dim hdnAngsuran As HiddenField
            Dim hdnSaldoPokok As HiddenField
            Dim hdnSaldoBunga As HiddenField

            For i = 0 To GridView1.Rows.Count - 1
                lblSeqNo = CType(GridView1.Rows(i).FindControl("lblSeqNo"), Label)
                txtPokok = CType(GridView1.Rows(i).FindControl("txtPokok"), TextBox)
                txtBunga = CType(GridView1.Rows(i).FindControl("txtBunga"), TextBox)
                hdnSaldoPokok = CType(GridView1.Rows(i).FindControl("hdnSaldoPokok"), HiddenField)
                hdnSaldoBunga = CType(GridView1.Rows(i).FindControl("hdnSaldoBunga"), HiddenField)
                dt.Rows.Add(CInt(lblSeqNo.Text), CDbl(txtPokok.Text.Replace(",", "")), CDbl(txtBunga.Text.Replace(",", "")), CDbl(hdnSaldoPokok.Value.Replace(",", "")), CDbl(hdnSaldoBunga.Value.Replace(",", "")))
            Next
            With customClass
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.FundingCoyID.Trim
                .FundingContractNo = Me.FundingContractNo.Trim
                .FundingBatchNo = Me.FundingBatchNo.Trim
                .ListData = dt
            End With
            FundingBatchInstallmentEdit(customClass)

            Response.Redirect("FundingEditInstallmentList.aspx")

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("asp", "function", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("FundingEditInstallmentList.aspx")
    End Sub
End Class