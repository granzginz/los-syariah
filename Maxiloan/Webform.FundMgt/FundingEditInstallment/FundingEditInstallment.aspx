﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingEditInstallment.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingEditInstallment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditInstallment</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.6.4.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CalculateInstalment(lnk) {
            var TotalBunga = 0;
            var replace = ",";

            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var replace = ",";
            var re = new RegExp(replace, 'g');






            var nilaiPokok = $("#GridView1_txtPokok_" + rowIndex + "").val();
            var nilaiBunga = $("#GridView1_txtBunga_" + rowIndex + "").val();
            var _pokok = parseFloat(nilaiPokok.replace(re, ""));
            var _bunga = parseFloat(nilaiBunga.replace(re, ""));

           

            $("#GridView1_txtAngsuran_" + rowIndex + "").val(number_format(_pokok + _bunga));
            $("#GridView1_hdnAngsuran_" + rowIndex + "").val(_pokok + _bunga);
            if (rowIndex != "0") {

                var firstrowIndex = rowIndex - 1;
                var nilaiOUTPokok = $("#GridView1_txtSaldoPokok_" + firstrowIndex + "").val();
                var nilaiOUTBunga = $("#GridView1_txtSaldoBunga_" +  firstrowIndex + "").val();
                var _OUTPokok = parseFloat(nilaiOUTPokok.replace(re, ""));
                var _OUTBunga = parseFloat(nilaiOUTBunga.replace(re, ""));
                $("#GridView1_txtSaldoPokok_" +  rowIndex + "").val(number_format(_OUTPokok - _pokok));
                $("#GridView1_txtSaldoBunga_" + rowIndex + "").val(number_format(_OUTBunga - _bunga));

                $("#GridView1_hdnSaldoPokok_" + rowIndex + "").val(_OUTPokok - _pokok);
                $("#GridView1_hdnSaldoBunga_" + rowIndex + "").val(_OUTBunga - _bunga);
            }
            if (rowIndex == "0") {

                var FirstPrincipalAmount = 0;
                var FirstInterestAmount = 0;


                FirstPrincipalAmount = parseFloat(document.getElementById("lblPokok").innerHTML.replace(re, ""));
                FirstInterestAmount = parseFloat($("#txtBungaPokok_txtNumber").val().replace(re, ""));


                $("#GridView1_txtSaldoPokok_" +  rowIndex + "").val(number_format(FirstPrincipalAmount - _pokok));
                $("#GridView1_txtSaldoBunga_" + rowIndex + "").val(number_format(FirstInterestAmount - _bunga));
                $("#GridView1_hdnSaldoPokok_" + rowIndex + "").val(FirstPrincipalAmount - _pokok);
                $("#GridView1_hdnSaldoBunga_" + rowIndex + "").val(FirstInterestAmount - _bunga);

            }

        }



        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h4>
                EDIT FUNDING BATCH INSTALLENT
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Funding Bank</label>
            <asp:Label ID="lblFundingCompanyName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Funding Cabang Bank</label>
            <asp:Label ID="lblFundingCompanyBranch" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Fasilitas</label>
            <asp:Label ID="lblFacilityNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Fasilitas</label>
            <asp:Label ID="lblFacilityName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Batch</label>
            <asp:Label ID="lblBatchNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Rencana Cair</label>
            <asp:Label ID="lblDrawdownPlanDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jumlah Pencairan</label>
            <asp:Label ID="lblDrawdownAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jumlah Pokok</label>
            <asp:Label ID="lblPokok" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jumlah Margin</label>
            <%--  <asp:Label ID="lblBunga" runat="server"></asp:Label>--%>
            <uc1:ucnumberformat id="txtBungaPokok" runat="server" textcssclass="numberAlign small"
                text="0" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Margin</label>
            <asp:Label ID="lblInterestType" runat="server" Width="32px"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:GridView ID="GridView1" CssClass="grid_general" Width="100%" DataKeyNames="SeqNo"
                    AutoGenerateColumns="false" runat="server" HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid"
                    GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderText="No." HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"
                            ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:Label ID="lblSeqNo" runat="server" Text='<%# Eval("SeqNo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TanggalAngsuran" HeaderText="Tanggal" HeaderStyle-HorizontalAlign="left"
                            HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle" />
                        <asp:TemplateField HeaderText="Pokok" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"
                            ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtPokok" onblur="extractNumber(this,9,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                    onkeyup="extractNumber(this,9,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0);return CalculateInstalment(this);"
                                    autocomplete="off" Text='<%#FormatNumber(Container.DataItem("Pokok"),0)%>' Enabled='<%#Container.DataItem("IsPokok")%>'></asp:TextBox>
                                <asp:RangeValidator runat="server" ID="rvPokok" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 999999999999999"
                                    ControlToValidate="txtPokok" MaximumValue="999999999999999" MinimumValue="0"
                                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator runat="server" ID="rfvPokok" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                    ControlToValidate="txtPokok" CssClass="validator_general" Enabled="false"></asp:RequiredFieldValidator>
                                <%--<uc1:ucnumberformat id="txtPokok" runat="server" textcssclass="numberAlign small"
                                    text='<%#FormatNumber(Container.DataItem("Pokok"),0)%>' Enabled='<%#Container.DataItem("IsPokok")%>' onchange="return CalculateInstalment(this);" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margin" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"
                            ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtBunga" onblur="extractNumber(this,9,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                    onkeyup="extractNumber(this,9,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0);return CalculateInstalment(this);"
                                    autocomplete="off" Text='<%#FormatNumber(Container.DataItem("Bunga"),0)%>' Enabled='<%#Container.DataItem("IsBunga")%>'></asp:TextBox>
                                <asp:RangeValidator runat="server" ID="rvBunga" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 999999999999999"
                                    ControlToValidate="txtBunga" MaximumValue="999999999999999" MinimumValue="0"
                                    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator runat="server" ID="rfvBunga" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                    ControlToValidate="txtBunga" CssClass="validator_general" Enabled="false"></asp:RequiredFieldValidator>
                                <%--                                <uc1:ucnumberformat id="txtBunga" runat="server" textcssclass="numberAlign small"
                                    text='<%#FormatNumber(Container.DataItem("Bunga"),0)%>' Enabled='<%#Container.DataItem("IsBunga")%>' onchange="return CalculateInstalment(this);" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Angsuran" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"
                            ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtAngsuran" onblur="extractNumber(this,9,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                    onkeyup="extractNumber(this,9,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0);"
                                    autocomplete="off" Text='<%#FormatNumber(Container.DataItem("Angsuran"),0)%>'
                                    Enabled='false'></asp:TextBox>
                                <asp:HiddenField ID="hdnAngsuran" runat="server" Value='<%#FormatNumber(Container.DataItem("Angsuran"),0)%>'/>
                                <%--  <uc1:ucnumberformat id="txtAngsuran" runat="server" textcssclass="numberAlign small"
                                    text='<%#FormatNumber(Container.DataItem("Angsuran"),0)%>'   isReadOnly="True" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OS Pokok" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"
                            ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                              <asp:TextBox runat="server" ID="txtSaldoPokok" onblur="extractNumber(this,9,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                    onkeyup="extractNumber(this,9,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0);"
                                    autocomplete="off" Text='<%#FormatNumber(Container.DataItem("SaldoPokok"),0)%>'
                                    Enabled='false'></asp:TextBox>
                                    <asp:HiddenField ID="hdnSaldoPokok" runat="server" Value='<%#FormatNumber(Container.DataItem("SaldoPokok"),0)%>' />
                              <%--  <uc1:ucnumberformat id="txtSaldoPokok" runat="server" textcssclass="numberAlign small"
                                    text='<%#FormatNumber(Container.DataItem("SaldoPokok"),0)%>' isreadonly="True" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OS Margin" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"
                            ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtSaldoBunga" onblur="extractNumber(this,9,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                    onkeyup="extractNumber(this,9,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0);"
                                    autocomplete="off" Text='<%#FormatNumber(Container.DataItem("SaldoBunga"),0)%>'
                                    Enabled='false'></asp:TextBox>
                                     <asp:HiddenField ID="hdnSaldoBunga" runat="server" Value='<%#FormatNumber(Container.DataItem("SaldoBunga"),0)%>' />
                             <%--   <uc1:ucnumberformat id="txtSaldoBunga" runat="server" textcssclass="numberAlign small"
                                    text='<%#FormatNumber(Container.DataItem("SaldoBunga"),0)%>' isreadonly="True" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="th" />
                    <RowStyle CssClass="item_grid" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
