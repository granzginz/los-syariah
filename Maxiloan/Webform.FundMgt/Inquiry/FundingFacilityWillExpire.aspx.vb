﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingFacilityWillExpire
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString

        pnlList.Visible = True
        pnlTop.Visible = False
        PnlTopNextView.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlTop.Visible = False
        PnlTopNextView.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
        pnlTop.Visible = False
        PnlTopNextView.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlTop.Visible = False
        PnlTopNextView.Visible = True
    End Sub

#End Region

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlTop.Visible = False
        pnlList.Visible = False
        lblMessage.Visible = False
        PnlTopNextView.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlTop.Visible = True
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        InitialDefaultPanel()
        If Not Me.IsPostBack Then

            Me.FormID = "FundingContract"

            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            Me.SortBy = ""
            'BindGridEntity(Me.SearchBy, Me.SortBy)
            'pnlList.Visible = True
            bindComboCompany()
        End If
    End Sub

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            'If drdCompany.SelectedItem.Value = "-" Then
            '    .WhereCond = " and (FundingContract.PeriodTo between getdate() and '" & ConvertDate2(oDateTo.dateValue) & "') "
            'Else
            '    .WhereCond = " and (FundingContract.PeriodTo between getdate() and '" & ConvertDate2(oDateTo.dateValue) & "') and FundingContract.FundingCoyID='" & drdCompany.SelectedItem.Value & "'"

            'End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingInquiryFacilityWillExpire"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lbltotrec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingContractList.DataSource = dtvEntity
        Try
            dtgFundingContractList.DataBind()
        Catch
            dtgFundingContractList.CurrentPageIndex = 0
            dtgFundingContractList.DataBind()
        End Try
        drdCompany.SelectedIndex = 0
        PagingFooter()
    End Sub
#End Region

#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            Dim i As Integer


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception

            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
#End Region


    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        lblViewFundingCO.Text = "All"
        Me.SearchBy = ""
        Me.SortBy = ""
        If drdCompany.SelectedItem.Value.Trim <> "All" Then
            Me.SearchBy = "FundingContract.FundingCoyID='" & drdCompany.SelectedItem.Value.Trim & "' and "
            lblViewFundingCO.Text = drdCompany.SelectedItem.Text.Trim
        End If
        If Me.SearchBy.Trim <> "" Then
            Me.SearchBy = Left(Me.SearchBy, Len(Me.SearchBy.Trim) - 4)
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlTop.Visible = False
        PnlTopNextView.Visible = True
    End Sub

    'Private Sub dtgFundingContractList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingContractList.ItemCommand
    '    Dim lblBankName, lblBranch, lblBankID, lblContractName, lblFundingCoyID As Label

    '    lnkContractNo = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lnkContractNo"), LinkButton)
    '    lblBranch = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBranch"), Label)
    '    lblBankName = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBankName"), Label)
    '    lblFundingCoyID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblFundingCoyID"), Label)
    '    lblBankID = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblBankID"), Label)
    '    lblContractName = CType(dtgFundingContractList.Items(e.Item.ItemIndex).FindControl("lblContractName"), Label)

    '    Me.FundingContractNo = lnkContractNo.Text
    '    Me.CompanyID = lblFundingCoyID.Text

    '    Select Case e.CommandName
    '        Case "Contract"
    '            Response.Redirect("../setting/fundingcontractmaintenance.aspx?FundingContractNo=" & Me.FundingContractNo & "&CompanyName=" & lblBranch.Text & "&BankName=" & lblBankName.Text)
    '    End Select
    'End Sub

    Private Sub dtgFundingContractList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingContractList.ItemDataBound
        'Dim hyBranch As HyperLink
        Dim hyContract As HyperLink
        Dim lblFundingCoyID As Label
        Dim lblBankID As Label
        Dim hyBatchNo As HyperLink
        Dim lblContractName As Label
        Dim lblBankName As Label
        Dim lblBranch As Label

        'hyBranch = CType(e.Item.FindControl("lnkBranch"), HyperLink)
        lblBranch = CType(e.Item.FindControl("lblBranch"), Label)
        hyContract = CType(e.Item.FindControl("lnkContractNo"), HyperLink)
        lblBankName = CType(e.Item.FindControl("lblBankName"), Label)
        lblFundingCoyID = CType(e.Item.FindControl("lblFundingCoyID"), Label)
        lblBankID = CType(e.Item.FindControl("lblBankID"), Label)
        'hyBatchNo = CType(e.Item.FindControl("lnkBatchNo"), HyperLink)
        lblContractName = CType(e.Item.FindControl("lblContractName"), Label)

        If e.Item.ItemIndex >= 0 Then

            'hyBranch.NavigateUrl = "javascript:OpenFundingCompanyView('" & "channeling" & "', '" & lblFundingCoyID.Text.Trim & "')"
            hyContract.NavigateUrl = "javascript:OpenFundingCompanyContractView('" & "channeling" & "','" & lblBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & hyContract.Text.Trim & "')"
            'hyBatchNo.NavigateUrl = "javascript:OpenFundingBatchView('" & "channeling" & "','" & lblFundingCoyID.Text.Trim & "','" & hyBranch.Text.Trim & "','" & lblBankName.Text.Trim & "','" & lblBankID.Text.Trim & "','" & lblContractName.Text.Trim & "','" & hyContract.Text.Trim & "','" & hyBatchNo.Text.Trim & "')"
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("FundingFacilityWillExpire.aspx")
    End Sub

End Class