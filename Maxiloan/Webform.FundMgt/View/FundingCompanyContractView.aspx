﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingCompanyContractView.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingCompanyContractView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingCompanyContractView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - FASILITAS
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Funding Bank
                </label>
                <asp:Label ID="lblFundingCompanyV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Prosentase Security
                </label>
                <asp:Label ID="lblSecurityPercentage" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <asp:Label ID="lblBranchV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Prosentase Security Berdasarkan
                </label>
                <asp:Label ID="lblSecurityPercentageOf" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Fasilitas Bank
                </label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Security
                </label>
                <asp:Label ID="lblSecurityType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Fasilitas
                </label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    waktu Pemilihan Fasilitas
                </label>
                <asp:Label ID="lblSelectionTime" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Fasilitas
                </label>
                <asp:Label ID="lblContractDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Porsi Funding Bank
                </label>
                <asp:Label ID="lblFundingCoyPortion" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Plafond
                </label>
                <asp:Label ID="lblPlafondAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Provisi
                </label>
                <asp:Label ID="lblProvision" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Mata Uang
                </label>
                <asp:Label ID="lblCurrency" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Admin Fee Per Fasilitas
                </label>
                <asp:Label ID="lblAdminFeePerAct" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Fasilitas
                </label>
                <asp:Label ID="lblFacilityType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Pelunasan
                </label>
                <asp:Label ID="lblPrepaymentPenalty" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Berlaku Dari
                </label>
                <asp:Label ID="lblPeriodFrom" runat="server"></asp:Label>&nbsp;S/D
                <asp:Label ID="lblPeriodTo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Commintment Fee
                </label>
                <asp:Label ID="lblCommitmentFee" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Jatuh Tempo Final
                </label>
                <asp:Label ID="lblFinalMaturityDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan
                </label>
                <asp:Label ID="lblLatePaymentFee" runat="server"></asp:Label>%
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Evaluasi
                </label>
                <asp:Label ID="lblEvaluationDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Per pencairan
                </label>
                <asp:Label ID="lblFeePerDrawDown" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Suku Margin
                </label>
                <asp:Label ID="lblInterestRate" runat="server"></asp:Label>%
            </div>
            <div class="form_right">
                <label>
                    Biaya Per Fasilitas
                </label>
                <asp:Label ID="lblFeePerFacility" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Margin
                </label>
                <asp:Label ID="lblInterestType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Maximum Pencairan
                </label>
                <asp:Label ID="lblMaxDD" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Floating Mulai
                </label>
                <asp:Label ID="lblFloatingStart" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Pelunasan
                </label>
                <asp:Label ID="lblPrepaymentType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan Margin
                </label>
                <asp:Label ID="lblInterestNote" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   <%-- Lokasi BPKB--%>
                     Lokasi Jaminan
                </label>
                <asp:Label ID="lblBPKBLocation" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status Commitment
                </label>
                <asp:Label ID="lblCommitmentStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Late Charges Grace Period
                </label>
                <asp:Label ID="lblLCGracePeriod" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Skema Pembayaran
                </label>
                <asp:Label ID="lblPaymentScheme" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Penjaminan (Recourse)
                </label>
                <asp:Label ID="lblRecourseType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Fasilitas
                </label>
                <asp:Label ID="lblFacilityKind" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Status Balance Sheet
                </label>
                <asp:Label ID="lblBalanceSheetStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
