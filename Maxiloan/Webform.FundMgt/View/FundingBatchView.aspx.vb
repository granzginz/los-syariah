﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingBatchView
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region
#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If Not Me.IsPostBack Then
            If sessioninvalid() Then
                Exit Sub
            End If
            'imbPaymentHistory.Attributes.Add("onClick", "javascript:OpenFundingViewPaymentHistory('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & Me.FundingBatchNo.Trim & "');")
            'imbAgreementSelected.Attributes.Add("onClick", "javascript:OpenFundingViewAgreementSelected('" & "channeling" & "','" & Me.CompanyID.Trim & "','" & Me.CompanyName.Trim & "','" & Me.BankName.Trim & "','" & Me.BankID.Trim & "','" & Me.ContractName.Trim & "','" & Me.FundingContractNo.Trim & "','" & Me.FundingBatchNo.Trim & "');")

            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("FundingcontractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingcontractNo")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo").Trim
            pnlView.Visible = True
            JustView()

        End If
    End Sub
#Region "JustView"
    Private Sub JustView()
        Dim dtvEntity As DataView
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim dt3 As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = " FundingBatchNo='" & Me.FundingBatchNo & "' and BankId='" & Me.BankID & "' and FundingCoyId='" & Me.CompanyID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingContractBatchById"
        End With
        Try


            oContract = cContract.GetGeneralPaging(oContract)
            dt = oContract.ListData
            lblBatchNoV.Text = CStr(dt.Rows(0).Item("FundingBatchNo")).Trim
            lblAmountV.Text = FormatNumber(dt.Rows(0).Item("PrincipalAmtToFunCoy"), 0).Trim
            lblDateV.Text = CDate(dt.Rows(0).Item("BatchDate")).ToString("dd/MM/yyyy")
            lblInterestRateV.Text = FormatNumber(dt.Rows(0).Item("InterestRate"), 2).Trim & "%"
            Select Case CStr(dt.Rows(0).Item("InterestType")).Trim
                Case "L"
                    lblInterestTypeV.Text = "Floating"
                Case "X"
                    lblInterestTypeV.Text = "Fixed"
                Case "B"
                    lblInterestTypeV.Text = "Fixed Per Batch"
            End Select
            lblFinalMaturityDateV.Text = CDate(dt.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")

            Select Case CStr(dt.Rows(0).Item("PaymentScheme")).Trim
                Case "1"
                    lblPaymentSchemeV.Text = "Principal & Interest on Drawdown Date"
                Case "2"
                    lblPaymentSchemeV.Text = "Principal on Drawdown Date Interest on EoM"
                Case "3"
                    lblPaymentSchemeV.Text = "Principal & Interest on Due Date"
                Case "4"
                    lblPaymentSchemeV.Text = "Principal & Interest on EoM"
                Case "5"
                    lblPaymentSchemeV.Text = "Interest on Drawdown Date Principal on EoL"
            End Select

            lblProvisionAmountV.Text = FormatNumber(dt.Rows(0).Item("ProvisionFeeAmount"), 0).Trim
            lblAdminAmountV.Text = FormatNumber(dt.Rows(0).Item("AdminAmount"), 0).Trim
            lblCurrencyV.Text = CStr(dt.Rows(0).Item("CurrencyId")).Trim
            lblExchangeRateV.Text = FormatNumber(dt.Rows(0).Item("ExchangeRate"), 0).Trim
            lblTenorV.Text = CStr(dt.Rows(0).Item("Tenor")).Trim
            lblOutstandingLoanV.Text = FormatNumber(dt.Rows(0).Item("OSAmtToFunCoy"), 0).Trim
            Select Case CStr(dt.Rows(0).Item("InstallmentScheme")).Trim
                Case "A"
                    lblInstallmentSchemeV.Text = "Fix Amount"
                Case "P"
                    lblInstallmentSchemeV.Text = "Fix Principal"
                Case "I"
                    lblInstallmentSchemeV.Text = "Irregular"
            End Select
            Select Case CStr(dt.Rows(0).Item("InstallmentPeriod")).Trim
                Case "1"
                    lblInstallmentPeriodV.Text = "Monthly"
                Case "3"
                    lblInstallmentPeriodV.Text = "Quaterly"
            End Select



            Dim cContract2 As New FundingCompanyController
            Dim oContract2 As New Parameter.FundingContractBatch

            With oContract2
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            dt2 = cContract2.FundingTotalSelected(oContract2).ListData
            
            If CDbl(dt2.Rows(0).Item("TotalAccount")) > 0 Then
                lblOutstandingPrincipalV.Text = FormatNumber(dt2.Rows(0).Item("PrincipalAmount"), 0)
                lblOutstandingARV.Text = FormatNumber(dt2.Rows(0).Item("ARAmount"), 0)
                lblNumOfAccountV.Text = FormatNumber(dt2.Rows(0).Item("TotalAccount"), 0)
                lblTotalOTRAmountV.Text = FormatNumber(dt2.Rows(0).Item("OTRAmount"), 0)
            Else
                lblOutstandingPrincipalV.Text = 0
                lblOutstandingARV.Text = 0
                lblNumOfAccountV.Text = 0
                lblTotalOTRAmountV.Text = 0
            End If

            With oContract
                .strConnection = GetConnectionString
                '.WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spFundingGetSecurityType"
            End With
            oContract = cContract.GetGeneralPaging(oContract)
            dt3 = oContract.ListData

            lblSecurityPercentageV.Text = FormatNumber(dt3.Rows(0).Item("SecurityCoveragePercentage"), 0)
            lblSecurityPercentageOfV.Text = CStr(dt3.Rows(0).Item("SecurityCoverageType")).Trim
            Select Case CStr(dt3.Rows(0).Item("SecurityCoverageType")).Trim
                Case "P"
                    lblSecurityPercentageOfV.Text = "Principal Amount"
                Case "A"
                    lblSecurityPercentageOfV.Text = "A/R Amount"
                Case "O"
                    lblSecurityPercentageOfV.Text = "OTR Amount"
            End Select

        Catch ex As Exception
        End Try


    End Sub
#End Region
    Private Sub btnInstallment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInstallment.Click
        Response.Redirect("../setting/FundingAgreementView.aspx?Command=INS&CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName & "&BankName=" & Me.BankName & "&BankId=" & Me.BankID & "&FundingContractId=" & Me.FundingContractNo & "&ContractName=" & Me.ContractName & "&FundingBatchNo=" & Me.FundingBatchNo)
    End Sub

    Private Sub btnPaymentHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPaymentHistory.Click
        Response.Redirect("../View/ViewPaymentHistory.aspx?CompanyID=" & Me.CompanyID.Trim & "&CompanyName=" & Me.CompanyName.Trim & "&" & _
                          "BankName=" & Me.BankName.Trim & "&BankID=" & Me.BankID.Trim & "&ContractName=" & Me.ContractName.Trim & "&" & _
                          "FundingContractNo=" & Me.FundingContractNo.Trim & "&FundingBatchNo=" & Me.FundingBatchNo.Trim & "")
    End Sub

    Private Sub btnAgreementSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgreementSelected.Click
        Response.Redirect("../View/ViewAgreementSelected.aspx?CompanyID=" & Me.CompanyID.Trim & "&CompanyName=" & Me.CompanyName.Trim & "&" & _
                          "BankName=" & Me.BankName.Trim & "&BankID=" & Me.BankID.Trim & "&ContractName=" & Me.ContractName.Trim & "&" & _
                          "FundingContractNo=" & Me.FundingContractNo.Trim & "&FundingBatchNo=" & Me.FundingBatchNo.Trim & "")
    End Sub

End Class