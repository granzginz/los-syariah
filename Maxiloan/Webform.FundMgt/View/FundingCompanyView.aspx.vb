﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class FundingCompanyView
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private oCompany As New Parameter.FundingCompany
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property BankId() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Request.QueryString("CompanyID") <> "" Then
            Me.CompanyID = Request.QueryString("CompanyID").Trim
            PnlView.Visible = True
            JustView(Me.CompanyID)

        End If
    End Sub
#Region "JustView"
    Private Sub JustView(ByVal Companyid As String)
        Dim oCompany As New Parameter.FundingCompany
        Dim dtCompany As New DataTable

        Try
            Dim strConnection As String = getConnectionString

            With oCompany
                .strConnection = getConnectionString()
                .FundingCoyID = Companyid
                .SortBy = ""
                '.WhereCond = "FundingCoyID='" & Companyid & "'"
            End With

            oCompany = m_Company.ListFundingCompanyByID(oCompany)
            dtCompany = oCompany.ListData

            'bindComboIDBank()
            'Isi semua field View Action

            lblFundingCoyID.Text = Companyid
            lblFundingCoyName.Text = CStr(dtCompany.Rows(0).Item("FundingCoyName")).Trim
            lblBankName.Text = CStr(dtCompany.Rows(0).Item("BankName").trim)


            lblAddress.Text = CStr(dtCompany.Rows(0).Item("Address")).Trim
            lblRT.Text = CStr(dtCompany.Rows(0).Item("RT")).Trim
            lblRW.Text = CStr(dtCompany.Rows(0).Item("RW")).Trim
            lblKelurahan.Text = CStr(dtCompany.Rows(0).Item("Kelurahan")).Trim
            lblKecamatan.Text = CStr(dtCompany.Rows(0).Item("Kecamatan")).Trim
            lblCity.Text = CStr(dtCompany.Rows(0).Item("City")).Trim
            lblZipCode.Text = CStr(dtCompany.Rows(0).Item("ZipCode")).Trim
            lblAreaPhone1.Text = CStr(dtCompany.Rows(0).Item("AreaPhone1")).Trim
            lblPhone1.Text = CStr(dtCompany.Rows(0).Item("Phone1")).Trim
            lblAreaPhone2.Text = CStr(dtCompany.Rows(0).Item("AreaPhone2")).Trim
            lblPhone2.Text = CStr(dtCompany.Rows(0).Item("Phone2")).Trim
            lblAreaFax.Text = CStr(dtCompany.Rows(0).Item("AreaFax")).Trim
            lblFax.Text = CStr(dtCompany.Rows(0).Item("Fax")).Trim

            lblCPName.Text = CStr(dtCompany.Rows(0).Item("ContactName")).Trim
            lblCPTitle.Text = CStr(dtCompany.Rows(0).Item("ContactJobTitle")).Trim
            lblCPMobilePhone.Text = CStr(dtCompany.Rows(0).Item("ContactMobilePhone")).Trim
            lblCPEmail.Text = CStr(dtCompany.Rows(0).Item("ContactEmail")).Trim
        Catch ex As Exception

        End Try

    End Sub
#End Region

End Class