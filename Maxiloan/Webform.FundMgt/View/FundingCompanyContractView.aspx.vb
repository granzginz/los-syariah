﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FundingCompanyContractView
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton
#End Region
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If Not Me.IsPostBack Then
            If sessioninvalid() Then
                Exit Sub
            End If

            ''If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            ''If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo").Trim
            pnlView.Visible = True
            JustView(Me.FundingContractNo)
        End If
    End Sub
#Region "JustView"
    Private Sub JustView(ByVal FundingContractNo As String)
        Dim oCompany As New Parameter.FundingContract
        Dim dtCompany As New DataTable
        Dim item As Char

        Try
            Dim strConnection As String = getConnectionString

            With oCompany
                .strConnection = getConnectionString()
                .FundingContractNo = FundingContractNo
                .SortBy = ""
                '.WhereCond = "FundingCoyID='" & Companyid & "'"
            End With

            oCompany = m_Company.ListFundingContractByID(oCompany)
            dtCompany = oCompany.ListData

            'bindComboIDBank()
            'Isi semua field View Action
            lblFundingCompanyV.Text = Me.BankName
            lblBranchV.Text = Me.CompanyName
            lblFundingContractNo.Text = FundingContractNo
            lblContractName.Text = CStr(dtCompany.Rows(0).Item("ContractName")).Trim
            If CStr(dtCompany.Rows(0).Item("FlagCS")) = "A" Then
                lblSelectionTime.Text = "After"
            Else
                lblSelectionTime.Text = "Before"
            End If

            lblPlafondAmount.Text = FormatNumber(dtCompany.Rows(0).Item("PlafondAmount"), 2).Trim
            If CStr(dtCompany.Rows(0).Item("FacilityType")) = "R" Then
                lblFacilityType.Text = "Revolving"
            Else
                lblFacilityType.Text = "Non Revolving"
            End If
            item = CStr(dtCompany.Rows(0).Item("InterestType")).Trim
            Select Case item
                Case "L"
                    lblInterestType.Text = "Floating"
                Case "X"
                    lblInterestType.Text = "Fixed"
                Case "B"
                    lblInterestType.Text = "Fixed Per Batch"
            End Select
            lblInterestRate.Text = FormatNumber(dtCompany.Rows(0).Item("RateToFundingCoy"), 6).Trim
            lblInterestNote.Text = CStr(dtCompany.Rows(0).Item("InterestNotes")).Trim
            lblLCGracePeriod.Text = CStr(dtCompany.Rows(0).Item("LCGracePeriod")).Trim
            lblFundingCoyPortion.Text = FormatNumber(dtCompany.Rows(0).Item("FundingCoyPortion"), 2).Trim
            lblPrepaymentPenalty.Text = FormatNumber(dtCompany.Rows(0).Item("PrepaymentPenalty"), 2).Trim
            item = CStr(dtCompany.Rows(0).Item("PaymentScheme")).Trim
            Select Case item
                Case "1"
                    lblPaymentScheme.Text = "Principal & Interest on Drawdown Date"
                Case "2"
                    lblPaymentScheme.Text = "Principal on Drawdown Date Interest on EoM"
                Case "3"
                    lblPaymentScheme.Text = "Principal & Interest on Due Date"
                Case "4"
                    lblPaymentScheme.Text = "Principal & Interest on EoM"
                Case "5"
                    lblPaymentScheme.Text = "Interest on Drawdown Date Principal on EoL"
            End Select
            item = CStr(dtCompany.Rows(0).Item("Recoursetype")).Trim
            Select Case item
                Case "W"
                    lblRecourseType.Text = "Without ecourse"
                Case "F"
                    lblRecourseType.Text = "Full Recourse"
                Case "L"
                    lblRecourseType.Text = "Limited Recourse"
            End Select
            item = CStr(dtCompany.Rows(0).Item("SecurityCoverageType")).Trim
            Select Case item
                Case "P"
                    lblSecurityPercentageOf.Text = "Without ecourse"
                Case "A"
                    lblSecurityPercentageOf.Text = "Full Recourse"
                Case "O"
                    lblSecurityPercentageOf.Text = "Limited Recourse"
            End Select
            item = CStr(dtCompany.Rows(0).Item("BalanceSheetStatus")).Trim
            Select Case item
                Case "N"
                    lblBalanceSheetStatus.Text = "On Balance Sheet"
                Case "F"
                    lblBalanceSheetStatus.Text = "Of Balance Sheet"
            End Select
            item = CStr(dtCompany.Rows(0).Item("PrepaymentType")).Trim
            Select Case item
                Case "C"
                    lblPrepaymentType.Text = "Must Be Paid"
                Case "B"
                    'lblPrepaymentType.Text = "Must Change to Another BPKB"
                    lblPrepaymentType.Text = "Must Change to Another Collateral"
            End Select
            item = CStr(dtCompany.Rows(0).Item("AssetDocLocation")).Trim
            Select Case item
                Case "H"
                    lblBPKBLocation.Text = "Head Office"
                Case "B"
                    lblBPKBLocation.Text = "Branch of Aggrement Holder"
                Case "P"
                    lblBPKBLocation.Text = "Head Office of Funding Company"
                Case "C"
                    lblBPKBLocation.Text = "Branch of Funding Company"
            End Select
            Select Case CStr(dtCompany.Rows(0).Item("CommitmentStatus")).Trim
                Case "True"
                    lblCommitmentStatus.Text = "Yes"
                Case "False"
                    lblCommitmentStatus.Text = "No"
            End Select

            lblLatePaymentFee.Text = FormatNumber(dtCompany.Rows(0).Item("RateToFundingCoy"), 2).Trim
            lblSecurityPercentage.Text = FormatNumber(dtCompany.Rows(0).Item("SecurityCoveragePercentage"), 2).Trim
            lblProvision.Text = FormatNumber(dtCompany.Rows(0).Item("ProvisionFeeAmount"), 2).Trim
            lblCommitmentFee.Text = FormatNumber(dtCompany.Rows(0).Item("CommitmentFee"), 2).Trim
            lblFeePerFacility.Text = FormatNumber(dtCompany.Rows(0).Item("AdminFeeFacility"), 2).Trim
            lblAdminFeePerAct.Text = FormatNumber(dtCompany.Rows(0).Item("AdminFeePerAccount"), 2).Trim
            lblFeePerDrawDown.Text = FormatNumber(dtCompany.Rows(0).Item("AdminFeePerDrawDown"), 2).Trim
            lblContractDate.Text = CDate(dtCompany.Rows(0).Item("ContractDate")).ToString("dd/MM/yyyy")
            lblPeriodFrom.Text = CDate(dtCompany.Rows(0).Item("PeriodFrom")).ToString("dd/MM/yyyy")
            lblPeriodTo.Text = CDate(dtCompany.Rows(0).Item("PeriodTo")).ToString("dd/MM/yyyy")
            lblMaxDD.Text = FormatNumber(dtCompany.Rows(0).Item("MaximumDateForDD"), 2).Trim()


            Select Case CStr(dtCompany.Rows(0).Item("FacilityKind"))
                Case "JFINC"
                    lblFacilityKind.Text = "Joint Financing"
                Case "KUK"
                    lblFacilityKind.Text = "KUK"
                Case "CHANN"
                    lblFacilityKind.Text = "Channeling"
                Case "ASBUY"
                    lblFacilityKind.Text = "Asset Buy"
                Case "UKM"
                    lblFacilityKind.Text = "UKM"
                Case "KMK"
                    lblFacilityKind.Text = "KMK"
                Case "TLOAN"
                    lblFacilityKind.Text = "Term Loan"
                Case "BONDS"
                    lblFacilityKind.Text = "Obligasi"
            End Select

            lblCurrency.Text = CStr(dtCompany.Rows(0).Item("CurrencyID"))

            If IsDBNull(dtCompany.Rows(0).Item("FinalMaturityDate")) Then
                lblFinalMaturityDate.Text = "-"
            Else
                lblFinalMaturityDate.Text = CDate(dtCompany.Rows(0).Item("FinalMaturityDate")).ToString("dd/MM/yyyy")
            End If
            If IsDBNull(dtCompany.Rows(0).Item("EvaluationDate")) Then
                lblEvaluationDate.Text = "-"
            Else
                lblEvaluationDate.Text = CDate(dtCompany.Rows(0).Item("EvaluationDate")).ToString("dd/MM/yyyy")
            End If
            If IsDBNull(dtCompany.Rows(0).Item("FloatingStart")) Then
                lblFloatingStart.Text = "-"
            Else
                lblFloatingStart.Text = CDate(dtCompany.Rows(0).Item("FloatingStart")).ToString("dd/MM/yyyy")
            End If

            'chkCorporateV.Checked = False
            'chkParentV.Checked = False
            'chkCashV.Checked = False
            'chkFixedV.Checked = False

            For Each item In CStr(dtCompany.Rows(0).Item("SecurityType")).Trim
                Select Case item
                    Case "1"
                        lblSecurityType.Text = "Corporate Guarantee"
                    Case "2"
                        lblSecurityType.Text = "Parent Guarantee"
                    Case "3"
                        lblSecurityType.Text = "Cash Deposit"
                    Case "4"
                        lblSecurityType.Text = "Fixed Asset"

                        'Case "1"
                        '    chkCorporateV.Checked = True
                        'Case "2"
                        '    chkParentV.Checked = True
                        'Case "3"
                        '    chkCashV.Checked = True
                        'Case "4"
                        '    chkFixedV.Checked = True
                End Select
            Next
        Catch ex As Exception

        End Try

    End Sub
#End Region

End Class