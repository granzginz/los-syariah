﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAgreementSelected.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.ViewAgreementSelected" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAgreementSelected</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - FASILITAS TERPILIH
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgFundingAgreementList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO FASILITAS">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Enabled="True" Text='<%# Container.dataitem("AgreementNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NAME" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCustName" runat="server" Enabled="True" CausesValidation="False"
                                        CommandName="ViewCustomer" Text='<%# Container.dataitem("CustName") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OSAR" HeaderText="SISA A/R" DataFormatString="{0:N2}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OSPRINCIPAL" HeaderText="SISA POKOK" DataFormatString="{0:N2}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="RIght"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TENOR" HeaderText="JK WAKTU">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TotalOTR" HeaderText="HARGA OTR" DataFormatString="{0:N2}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DESCRIPTION" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                <HeaderStyle Width="35%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="" HeaderText="" Visible="False">
                                <HeaderStyle Width="35%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%# Container.DataItem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AMORTISASI">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbAmor" runat="server" ImageUrl="../../Images/ftv2doc.gif"
                                        CommandName="Amortisasi" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" CausesValidation="False"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton01.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" CausesValidation="False"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton02.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" CausesValidation="False"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton03.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" CausesValidation="False"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton04.png">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px"  MaxLength="3">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" ImageAlign="AbsBottom"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  MaximumValue="999999999"
                        MinimumValue="0" Type="Integer"   ControlToValidate="txtGoPage"
                        ErrorMessage="No Halaman salah"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"  Display="Dynamic" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <!--Here-->
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TOTAL FASILITAS TERPILIH
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Total Pokok
                </label>
                <asp:Label ID="lblPrincipalAmountL" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total OTR
                </label>
                <asp:Label ID="lblOTRAmountL" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Total A/R
                </label>
                <asp:Label ID="lblARAmountL" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total Fasilitas
                </label>
                <asp:Label ID="lblAccountL" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Harus Ditambah
                </label>
                <asp:Label ID="LblAmountMustBeAdd" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnprint" runat="server" Text="Print" Visible="false" CssClass="small button green">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button blue" />
            <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
