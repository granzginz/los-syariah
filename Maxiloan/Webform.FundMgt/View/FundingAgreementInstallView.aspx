﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingAgreementInstallView.aspx.vb" Inherits="Maxiloan.Webform.FundMgt.FundingAgreementInstallView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>FundingAgreementView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		function fback() {
		    window.history.go(-3);
            return false;
        }

        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                AMORTISASI FUNDING PER KONTRAK DEBITUR
            </h3>
        </div>
    </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Funding Bank</label>
                <asp:Label ID="lblBankName" runat="server" Width="192px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fasilitas Bank</label>
                <asp:Label ID="lblFundingContractNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Fasilitas</label>
                <asp:Label ID="lblContractName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:Label ID="lblBatchNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Pencairan</label>
                <asp:Label ID="lblDrawDownDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    AR Amount</label>
                <asp:Label ID="lblARAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    AR Principal</label>
                <asp:Label ID="lblARPrincipal" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    OS AR Amount</label>
                <asp:Label ID="lblOSARAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    OS Principal Amount</label>
                <asp:Label ID="lblOSPrincipalAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tenor</label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInstallment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                       BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="INSSEQNO" HeaderText="NO">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINSSEQNO" runat="server" Text='<%#Container.DataItem("INSSEQNO")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:BoundColumn DataField="DUEDATE" SortExpression="DUEDATE" HeaderText="TGL JT POKOK" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="10%" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="INSTALLMENTAMOUNT" HeaderText="JUMLAH ANGSURAN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINSTALLMENTAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("INSTALLMENTAMOUNT"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINSTALLMENTAMOUNT" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PRINCIPALAMOUNT" HeaderText="JUMLAH POKOK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPRINCIPALAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("PRINCIPALAMOUNT"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPRINCIPALAMOUNT" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="INTERESTAMOUNT" HeaderText="JUMLAH MARGIN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("INTERESTAMOUNT"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINTERESTAMOUNT" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PRINCIPALPAIDAMOUNT" HeaderText="POKOK SUDAH BAYAR">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPRINCIPALPAIDAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("PRINCIPALPAIDAMOUNT"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotPRINCIPALPAIDAMOUNT" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="INTERESTPAIDAMOUNT" HeaderText="MARGIN SUDAH BAYAR">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINTERESTPAIDAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("INTERESTPAIDAMOUNT"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotINTERESTPAIDAMOUNT" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="OSPrincipalAmount" HeaderText="SISA POKOK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOSPrincipalAmount" runat="server" Text='<%#formatnumber(Container.DataItem("OSPrincipalAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSPrincipalAmount" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="OSInterestAmount" HeaderText="SISA MARGIN" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOSInterestAmount" runat="server" Text='<%#formatnumber(Container.DataItem("OSInterestAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="7%"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotOSInterestAmount" runat="server" Text="TOTAL"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <a class="small button blue" onclick="window.history.go(-1);return false;">Back</a>
        </div>
    </form>
</body>
</html>
