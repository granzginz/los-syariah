﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class FundingAgreementInstallView
    Inherits Maxiloan.Webform.WebBased
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property
    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Decimal = 1
    Private recordCount As Int64 = 1
    Private Command As String
    Private cmdWhere As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
            If Request.QueryString("ApplicationID") <> "" Then Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.SearchBy = ""
            Me.SortBy = ""

            BindGridEntity(Me.SearchBy, Me.SortBy)
        End If

       


    End Sub
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim customClass As New Parameter.FundingContractBatch
        Dim customController As New FundingCompanyController
        Dim tmp As New DataTable
        Dim tmp2 As New DataTable

        With customClass
            .strConnection = Me.GetConnectionString
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .ApplicationID = Me.ApplicationID
        End With
        customClass = customController.FundingAgreementInstallmentView(customClass)
        tmp = customClass.ListData
        dtgInstallment.DataSource = tmp.DefaultView
        dtgInstallment.DataBind()

        tmp2 = customClass.ListData2

        lblBankName.Text = tmp2.Rows(0).Item("FundingCoyID").ToString
        lblFundingContractNo.Text = tmp2.Rows(0).Item("FundingContractNo").ToString
        lblBatchNo.Text = tmp2.Rows(0).Item("FundingBatchNo").ToString
        lblContractName.Text = tmp2.Rows(0).Item("ContractName").ToString
        lblTenor.Text = tmp2.Rows(0).Item("Tenor").ToString
        lblARAmount.Text = FormatNumber(tmp2.Rows(0).Item("ARAmount"), 0)
        lblARPrincipal.Text = FormatNumber(tmp2.Rows(0).Item("PrincipalAmount"), 0)
        lblOSARAmount.Text = FormatNumber(tmp2.Rows(0).Item("OSARAmount"), 0)
        lblDrawDownDate.Text = CDate(tmp2.Rows(0).Item("EffectiveDate")).ToString("dd/MM/yyyy")
        lblOSPrincipalAmount.Text = FormatNumber(tmp2.Rows(0).Item("OSPrincipalAmount"), 0)

    End Sub
#End Region

End Class