﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewAgreementSelected
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Private dt As DataTable
    Private cGeneral As New GeneralPagingController
    Private oGeneral As New Parameter.GeneralPaging

    Private m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Decimal = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
    Private cmdWhere As String
#End Region
#Region "Prop"
    Public Property TotalAR As Decimal
        Get
            Return CDec(ViewState("TotalAR"))
        End Get
        Set(value As Decimal)
            ViewState("TotalAR") = value
        End Set
    End Property
    Public Property TotalPokok As Decimal
        Get
            Return CDbl(ViewState("TotalPokok"))
        End Get
        Set(value As Decimal)
            ViewState("TotalPokok") = value
        End Set
    End Property
    Public Property TotalOTR As Decimal
        Get
            Return CDbl(ViewState("TotalOTR"))
        End Get
        Set(value As Decimal)
            ViewState("TotalOTR") = value
        End Set
    End Property
    Public Property TotalNTF As Decimal
        Get
            Return CDbl(ViewState("TotalNTF"))
        End Get
        Set(value As Decimal)
            ViewState("TotalNTF") = value
        End Set
    End Property
    Public Property TotalTobeAdd As Decimal
        Get
            Return CDbl(ViewState("TotalTobeAdd"))
        End Get
        Set(value As Decimal)
            ViewState("TotalTobeAdd") = value
        End Set
    End Property
    Public Property TotalAccounts As Decimal
        Get
            Return CDbl(ViewState("TotalAccounts"))
        End Get
        Set(value As Decimal)
            ViewState("TotalAccounts") = value
        End Set
    End Property
    Public Property TotalPokokPerPage As Decimal
        Get
            Return CDbl(ViewState("TotalPokokPerPage"))
        End Get
        Set(value As Decimal)
            ViewState("TotalPokokPerPage") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Me.IsPostBack Then
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")
            'createxmdlist()

            If sessioninvalid() Then
                Exit Sub
            End If
            Me.SearchBy = ""
            Me.SortBy = ""
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
        End If
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Decimal))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If
        lblTotRec.Text = recordCount.ToString
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        lblMessage.Visible = False
        'pnlView.Visible = False
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        'Dim oCustomClass As New Parameter.FundingContractBatch


        With oContract
            .strConnection = GetConnectionString
            .WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "' and FundingBatchNo='" & Me.FundingBatchNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingAgreementSelectedList"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With
        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            'lblMessage.Text = "Data not found..."
            'lblMessage.Visible = True
        Else
            'imgPrint.Enabled = True
        End If

        dtgFundingAgreementList.DataSource = dtvEntity
        Try
            dtgFundingAgreementList.DataBind()
        Catch
            dtgFundingAgreementList.CurrentPageIndex = 0
            dtgFundingAgreementList.DataBind()
        End Try
        Try            
            Dim customContract As New FundingCompanyController
            Dim objContract As New Parameter.FundingContractBatch

            With objContract
                .strConnection = GetConnectionString()
                .FundingCoyId = Me.CompanyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
            End With

            objContract = customContract.FundingTotalSelected(objContract)

            If objContract.ListData.Rows.Count > 0 Then
                TotalAR = IIf(IsDBNull(objContract.ListData.Rows(0).Item("ARAmount")), 0, objContract.ListData.Rows(0).Item("ARAmount"))
                TotalPokok = IIf(IsDBNull(objContract.ListData.Rows(0).Item("PrincipalAmount")), 0, objContract.ListData.Rows(0).Item("PrincipalAmount"))
                TotalOTR = IIf(IsDBNull(objContract.ListData.Rows(0).Item("OTRAmount")), 0, objContract.ListData.Rows(0).Item("OTRAmount"))
                TotalNTF = IIf(IsDBNull(objContract.ListData.Rows(0).Item("NTF")), 0, objContract.ListData.Rows(0).Item("NTF"))
                TotalAccounts = IIf(IsDBNull(objContract.ListData.Rows(0).Item("TotalAccount")), 0, objContract.ListData.Rows(0).Item("TotalAccount"))
                lblPrincipalAmountL.Text = FormatNumber(TotalPokok, 0)
                lblARAmountL.Text = FormatNumber(TotalAR, 0)
                lblOTRAmountL.Text = FormatNumber(TotalOTR, 0)
                lblAccountL.Text = CStr(TotalAccounts)
            End If
            DisplayAmountMustBeAdd()
        Catch e As Exception
           showmessage(lblmessage, e.message , true)
        End Try


        PagingFooter()
    End Sub
#End Region

    Private Sub dtgFundingAgreementList_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgFundingAgreementList.ItemCommand
        Dim lblApplicationID As New Label

        lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)


        Select Case e.CommandName
            Case "Amortisasi"
                Response.Redirect("../View/FundingAgreementInstallView.aspx?CompanyID=" & Me.CompanyID.Trim & "&CompanyName=" & Me.CompanyName.Trim & "&" & _
                         "BankName=" & Me.BankName.Trim & "&BankID=" & Me.BankID.Trim & "&ContractName=" & Me.ContractName.Trim & "&" & _
                         "FundingContractNo=" & Me.FundingContractNo.Trim & "&FundingBatchNo=" & Me.FundingBatchNo.Trim & "&ApplicationID=" & lblApplicationID.Text & "")
        End Select
    End Sub


    Private Sub dtgFundingAgreementListd(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgFundingAgreementList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkCustName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"
        End If

    End Sub

    Private Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Dim FilterBy As String
        FilterBy = ""
        cmdWhere = ""
        cmdWhere = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
        FilterBy = "CompanyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractId='" & Me.FundingContractNo & "' and FundingBatchId='" & Me.FundingBatchNo & "'"
        Dim cookie As HttpCookie = Request.Cookies("Report")
        If Not cookie Is Nothing Then
            cookie.Values("where") = cmdWhere
            cookie.Values("BatchNo") = Me.FundingBatchNo
            cookie.Values("BankName") = Me.BankName
            cookie.Values("filterBy") = FilterBy
            cookie.Values("ReportType") = "AGRSelected"
            cookie.Values("FundingCoyId") = Me.CompanyID
            cookie.Values("FundingBankId") = Me.BankID
            cookie.Values("FundingContractId") = Me.FundingContractNo
            cookie.Values("FundingBatchId") = Me.FundingBatchNo
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Report")
            cookieNew.Values.Add("where", cmdWhere)
            cookieNew.Values.Add("BatchNo", Me.FundingBatchNo)
            cookieNew.Values.Add("BankName", Me.BankName)
            cookieNew.Values.Add("filterBy", FilterBy.Trim)
            cookieNew.Values.Add("ReportType", "AGRSelected")
            cookieNew.Values.Add("FundingCoyId", Me.CompanyID)
            cookieNew.Values.Add("FundingBankId", Me.BankID)
            cookieNew.Values.Add("FundingContractId", Me.FundingContractNo)
            cookieNew.Values.Add("FundingBatchId", Me.FundingBatchNo)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("../Report/FundingReportView.aspx")
    End Sub
#Region "DisplayAmountMustBeAdd"
    Public Sub DisplayAmountMustBeAdd()
        Dim DrawDownAmount, AmountMustBeAdd, OutstandingLoan As Decimal           
        Dim SecurityPercentage As Decimal
        Dim SecurityType As String
        Dim dt As DataTable
        Dim cGeneral As New GeneralPagingController
        Dim oGeneral As New Parameter.GeneralPaging
        Try            
            With oGeneral
                .strConnection = GetConnectionString
                '.WhereCond = "FundingCoyId='" & Me.CompanyID & "' and BankId='" & Me.BankID & "' and FundingContractNo='" & Me.FundingContractNo & "'"
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' AND FundingBatch.FundingBatchNo = '" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = sortby
                .SpName = "spFundingGetSecurityType"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            SecurityPercentage = dt.Rows(0).Item("SecurityCoveragePercentage")
            SecurityType = CStr(dt.Rows(0).Item("SecurityCoverageType")).Trim


            With oGeneral
                .strConnection = GetConnectionString
                .WhereCond = "FundingBatch.FundingCoyId='" & Me.CompanyID & "' and FundingBatch.BankId='" & Me.BankID & "' and FundingBatch.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatch.FundingBatchNo='" & Me.FundingBatchNo & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = sortby
                .SpName = "spFundingGetDrawDown"
            End With
            oGeneral = cGeneral.GetGeneralPaging(oGeneral)
            dt = oGeneral.ListData

            DrawDownAmount = dt.Rows(0).Item("PrincipalAmtToFunCoy")
            OutstandingLoan = dt.Rows(0).Item("OSAmtToFunCoy")

            Select Case SecurityType
                Case "P"
                    AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - TotalPokok
                Case "A"
                    AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - TotalAR
                Case "O"
                    AmountMustBeAdd = ((DrawDownAmount * SecurityPercentage) / 100) - TotalOTR
            End Select
            LblAmountMustBeAdd.Text = FormatNumber(AmountMustBeAdd, 0)
        Catch e As Exception
            showmessage(lblmessage, e.message , true)
            Exit Sub
        End Try
    End Sub
#End Region

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../View/FundingBatchView.aspx?CompanyID=" & Me.CompanyID.Trim & "&CompanyName=" & Me.CompanyName.Trim & "&" & _
                         "BankName=" & Me.BankName.Trim & "&BankID=" & Me.BankID.Trim & "&ContractName=" & Me.ContractName.Trim & "&" & _
                         "FundingContractNo=" & Me.FundingContractNo.Trim & "&FundingBatchNo=" & Me.FundingBatchNo.Trim & "")
    End Sub
End Class