﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewPaymentHistory
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region
#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(viewstate("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingBatchNo") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyID") = Value
        End Set
    End Property

    Private Property ContractName() As String
        Get
            Return CStr(viewstate("ContractName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ContractName") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(viewstate("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingContractNo") = Value
        End Set
    End Property

    Private Property BankID() As String
        Get
            Return CStr(viewstate("BankID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Private Property BankName() As String
        Get
            Return CStr(viewstate("BankName"))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("FundingCoyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property


    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region
#Region "FormLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        InitialDefaultPanel()
        If Not Me.IsPostBack Then
            Me.FormID = "PAYOUT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.SearchBy = ""
            Me.SortBy = ""
            If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
            If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
            If Request.QueryString("BankName") <> "" Then Me.BankName = Request.QueryString("BankName")
            If Request.QueryString("BankID") <> "" Then Me.BankID = Request.QueryString("BankID")
            If Request.QueryString("ContractName") <> "" Then Me.ContractName = Request.QueryString("ContractName")
            If Request.QueryString("FundingContractNo") <> "" Then Me.FundingContractNo = Request.QueryString("FundingContractNo")
            If Request.QueryString("FundingBatchNo") <> "" Then Me.FundingBatchNo = Request.QueryString("FundingBatchNo")

            lblBankName.Text = Me.BankName
            lblFundingCoyName.Text = Me.CompanyName
            lblFundingContractNo.Text = Me.FundingContractNo
            lblContractName.Text = Me.ContractName
            BindDataTop()
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
        End If
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblTotRec.Text = recordCount.ToString
        pnlList.Visible = True


        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
        pnlList.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = "FundingBatchPayOut.FundingCoyId='" & Me.CompanyID & "' and FundingBatchPayOut.BankId='" & Me.BankID & "' and FundingBatchPayOut.FundingContractNo='" & Me.FundingContractNo & "' and FundingBatchPayOut.FundingBatchNo='" & Me.FundingBatchNo & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spFundingBatchPaymentHistoryView"
        End With


        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With
        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        End If
        dtgFundingContractBatch.DataSource = dtvEntity
        Try
            dtgFundingContractBatch.DataBind()
        Catch
            dtgFundingContractBatch.CurrentPageIndex = 0
            dtgFundingContractBatch.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region "BindDataTop"
    Public Sub BindDataTop()
        Dim dtslist As DataTable
        Dim oRow As DataRow
        Dim cReceive As New FundingCompanyController
        Dim oReceive As New Parameter.FundingContractBatch
        Dim oCustomClass As New Parameter.FundingContractBatch
        With oCustomClass
            .strConnection = GetConnectionString
            .FundingCoyId = Me.CompanyID
            .FundingContractNo = Me.FundingContractNo
            .FundingBatchNo = Me.FundingBatchNo
            .BusinessDate = Me.BusinessDate
            .SpName = "spFundingBatchPaymentHistoryTopView"
        End With
        oReceive = cReceive.GetGeneralEditView(oCustomClass)
        dtslist = oReceive.ListData
        If dtslist.Rows.Count > 0 Then
            oRow = dtslist.Rows(0)
            If Not IsDBNull(oRow("FundingBatchNo")) Then
                LblBatchNo.Text = CStr(dtslist.Rows(0).Item("FundingBatchNo")).Trim
            End If

            If Not IsDBNull(oRow("BatchDate")) Then
                LblDrawdownDate.Text = CType(oRow("BatchDate"), String)
            End If
            If Not IsDBNull(oRow("PrincipalAmtToFunCoy")) Then
                LblDrawdownAmount.Text = FormatNumber(oRow("PrincipalAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("OSAmtToFunCoy")) Then
                LblOutstandingLoan.Text = FormatNumber(oRow("OSAmtToFunCoy"), 0)
            End If
            If Not IsDBNull(oRow("AccRealizedNum")) Then
                LblNumOfAccount.Text = FormatNumber(oRow("AccRealizedNum"), 0)
            End If
        End If
    End Sub
#End Region

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../View/FundingBatchView.aspx?CompanyID=" & Me.CompanyID.Trim & "&CompanyName=" & Me.CompanyName.Trim & "&" & _
                         "BankName=" & Me.BankName.Trim & "&BankID=" & Me.BankID.Trim & "&ContractName=" & Me.ContractName.Trim & "&" & _
                         "FundingContractNo=" & Me.FundingContractNo.Trim & "&FundingBatchNo=" & Me.FundingBatchNo.Trim & "")
    End Sub
End Class