﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingBatchView.aspx.vb"
    Inherits="Maxiloan.Webform.FundMgt.FundingBatchView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingBatchView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenFundingViewPaymentHistory(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.FundMgt/View/ViewPaymentHistory.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'PaymentHistory', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function OpenFundingViewAgreementSelected(pStyle, pCompanyID, pCompanyName, pBankName, pBankID, pContractName, pFundingContractNo, pFundingBatchNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.FundMgt/View/ViewAgreementSelected.aspx?Style=' + pStyle + '&CompanyID=' + pCompanyID + '&CompanyName=' + pCompanyName + '&BankName=' + pBankName + '&BankID=' + pBankID + '&ContractName=' + pContractName + '&FundingContractNo=' + pFundingContractNo + '&FundingBatchNo=' + pFundingBatchNo, 'PaymentHistory', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }
        function fclose() {
            window.close();
        }						
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - FUNDING BATCH
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Batch</label>
                <asp:Label ID="lblBatchNoV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Pencairan</label>
                <asp:Label ID="lblAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Pencairan</label>
                <asp:Label ID="lblDateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Suku Margin</label>
                <asp:Label ID="lblInterestRateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Margin</label>
                <asp:Label ID="lblInterestTypeV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Jatuh Tempo Final</label>
                <asp:Label ID="lblFinalMaturityDateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Pembayaran</label>
                <asp:Label ID="lblPaymentSchemeV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Biaya Provisi</label>
                <asp:Label ID="lblProvisionAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Biaya Administrasi</label>
                <asp:Label ID="lblAdminAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Mata Uang</label>
                <asp:Label ID="lblCurrencyV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nilai Tukar</label>
                <asp:Label ID="lblExchangeRateV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jangka Waktu</label>
                <asp:Label ID="lblTenorV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Periode Angsuran</label>
                <asp:Label ID="lblInstallmentPeriodV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Angsuran</label>
                <asp:Label ID="lblInstallmentSchemeV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Sisa Pinjaman</label>
                <asp:Label ID="lblOutstandingLoanV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Sisa Pokok
                </label>
                <asp:Label ID="lblOutstandingPrincipalV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Sisa A/R
                </label>
                <asp:Label ID="lblOutstandingARV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Fasilitas
                </label>
                <asp:Label ID="lblNumOfAccountV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total OTR
                </label>
                <asp:Label ID="lblTotalOTRAmountV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Prosentase Security
                </label>
                <asp:Label ID="lblSecurityPercentageV" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Prosentase Security Berdasarkan
                </label>
                <asp:Label ID="lblSecurityPercentageOfV" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnInstallment" runat="server" Text="Installment" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPaymentHistory" runat="server" Text="Payment History" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnAgreementSelected" runat="server" Text="Agreement Selected" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
