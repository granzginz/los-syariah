﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PrepayFundPrintViewer
    Inherits Maxiloan.Webform.WebBased
#Region " Property "
    Private Property FundingBatchNo() As String
        Get
            Return CStr(ViewState("FundingBatchNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingBatchNo") = Value
        End Set
    End Property

    Private Property FundingCoyID() As String
        Get
            Return CStr(ViewState("FundingCoyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyID") = Value
        End Set
    End Property

    Private Property FundingContractNo() As String
        Get
            Return CStr(ViewState("FundingContractNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("FundingContractNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property NoReff() As String
        Get
            Return CStr(ViewState("NoReff"))
        End Get
        Set(ByVal Value As String)
            ViewState("NoReff") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If
            BindReport()
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()

        Dim cookie As HttpCookie = Request.Cookies("PrepayFundPrint")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.FundingCoyID = cookie.Values("FundingCoyID")
        Me.FundingContractNo = cookie.Values("FundingContractNo")
        Me.FundingBatchNo = cookie.Values("FundingBatchNo")
        Me.NoReff = cookie.Values("NoReff")
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        GetCookies()

        Dim objreport As ReportDocument
        Dim spName As String

        objreport = New BBN
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .ApplicationId = Me.ApplicationID
                .FundingCoyID = Me.FundingCoyID
                .FundingContractNo = Me.FundingContractNo
                .FundingBatchNo = Me.FundingBatchNo
                .SpName = "spFundingReportBBNRequest"
            End With
            oCustomClass = oController.ProcessReportBBNRequest(oCustomClass)

            objreport.SetDataSource(oCustomClass.ListDataReport)

            CrystalReportViewer1.ReportSource = objreport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()

            '========================================================
            Dim discrete As ParameterDiscreteValue
            Dim ParamFields As ParameterFields
            Dim ParamField As ParameterFieldDefinition
            Dim CurrentValue As ParameterValues

            ParamFields = New ParameterFields
            ParamField = objreport.DataDefinition.ParameterFields("NoReff")
            discrete = New ParameterDiscreteValue
            discrete.Value = Me.NoReff
            CurrentValue = New ParameterValues
            CurrentValue = ParamField.DefaultValues
            CurrentValue.Add(discrete)
            ParamField.ApplyCurrentValues(CurrentValue)

            '========================================================

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PrepayFundPrint.pdf"
            DiskOpts.DiskFileName = strFileLocation
            objreport.ExportOptions.DestinationOptions = DiskOpts
            objreport.Export()
            objreport.Close()
            objreport.Dispose()
            Response.Redirect("PrepayFundPrint.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PrepayFundPrint")
        Catch exp As Exception
            Response.Write(exp.Message)
        End Try


    End Sub
#End Region
End Class