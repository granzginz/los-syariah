﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class PaymentOutList
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.FundingContractBatch
    Private oController As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property NoReff() As String
        Get
            Return CType(ViewState("NoReff"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoReff") = Value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "PrintPelunasanPrepay"
            Me.CmdWhere = "ConfLetterPrintNum=0"
            Me.Sort = "NoReff ASC"
        End If
    End Sub
#Region "BindGridEntity"
    Sub BindGridEntity()
        Dim dtEntity As New DataTable
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer = 0


        Dim DataList As New List(Of Parameter.FundingContractBatch)
        Dim custom As New Parameter.FundingContractBatch
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.FundingContractBatch

                custom.ReferenceNo = TempDataTable.Rows(index).Item("NoReff").ToString.Trim

                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
        End With

        oCustomClass = oController.ListPrintPaymentout(oCustomClass)


        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.Listdata
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtgGrid.DataSource = dtEntity.DefaultView
        dtgGrid.CurrentPageIndex = 0
        dtgGrid.DataBind()
        PagingFooter()

        pnlDtGrid.Visible = True
        pnlsearch.Visible = True


        For intLoopGrid = 0 To dtgGrid.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgGrid.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim ReferenceNo As String = CType(dtgGrid.Items(intLoopGrid).FindControl("hyNoReff"), HyperLink).Text.Trim

            Me.NoReff = ReferenceNo

            Dim query As New Parameter.FundingContractBatch
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub
#End Region
    Public Function PredicateFunction(ByVal custom As Parameter.FundingContractBatch) As Boolean
        Return custom.ReferenceNo = Me.NoReff
    End Function
    Sub BindDataPrepay()
        Dim DataList As New List(Of Parameter.FundingContractBatch)
        Dim NoReff As New Parameter.FundingContractBatch

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("NoReff", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                NoReff = New Parameter.FundingContractBatch

                NoReff.ReferenceNo = TempDataTable.Rows(index).Item("NoReff").ToString.Trim
                DataList.Add(NoReff)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgGrid.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgGrid.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim ReferenceNo As String = CType(dtgGrid.Items(intLoopGrid).FindControl("hyNoReff"), HyperLink).Text.Trim


            Me.NoReff = ReferenceNo

            Dim query As New Parameter.FundingContractBatch
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("NoReff") = ReferenceNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPrepay()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("NoReff", GetType(String)))
        End With
    End Sub
#Region "LinkToCustomer"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function
#End Region
#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer = 0
        Dim hasil As Integer = 0
        Dim cmdwhere As String = ""

        If TempDataTable Is Nothing Then
            BindDataPrepay()
        Else
            If TempDataTable.Rows.Count = 0 Then
                BindDataPrepay()
            End If
        End If

        With oDataTable
            .Columns.Add(New DataColumn("NoReff", GetType(String)))
        End With


        If TempDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap periksa item", True)
            Exit Sub
        End If

        For index = 0 To TempDataTable.Rows.Count - 1
            oRow = oDataTable.NewRow

            oRow("NoReff") = TempDataTable.Rows(index).Item("NoReff").ToString.Trim
            oDataTable.Rows.Add(oRow)
            If cmdwhere = "" Then
                cmdwhere = "FundingMailTransaction.NoReff='" & CType(oRow("NoReff"), String) & "'"
            Else
                cmdwhere += " or FundingMailTransaction.NoReff='" & CType(oRow("NoReff"), String) & "' "
            End If
        Next

        With oCustomClass
            .strConnection = GetConnectionString()
            .ListConfLetter = oDataTable
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid

        End With
        oCustomClass = oController.SavePrint(oCustomClass)
        hasil = oCustomClass.hasil

        If hasil = 0 Then
            ShowMessage(lblMessage, "Gagal", True)
            Exit Sub
        Else
            cmdwhere = "(" & cmdwhere & ") "
            Dim cookie As HttpCookie = Request.Cookies("RptPrint")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("BusinessDate") = Me.BusinessDate.ToString("dd/MM/yyyy")
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptPrint")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("BusinessDate", Me.BusinessDate.ToString("dd/MM/yyyy"))
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("PaymentOutListViewer.aspx")
        End If
    End Sub
#End Region
#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.CmdWhere = " FundingMailTransaction.ConfLetterPrintNum=0"
        Else
            Me.CmdWhere = " FundingMailTransaction.ConfLetterPrintNum > 0"
        End If


        Dim SearchTemp As String

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
            Me.CmdWhere = Me.CmdWhere & " and " & SearchTemp
        End If

        Me.Sort = "NoReff ASC"
        BindGridEntity()
    End Sub
#End Region
#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.CmdWhere = " ConfLetterPrintNum=0 "
        Me.Sort = "NoReff ASC"
        BindClearPrepay()
        BindGridEntity()
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindDataPrepay()
        BindGridEntity()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindDataPrepay()
                BindGridEntity()
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgGrid.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindDataPrepay()
        BindGridEntity()
    End Sub
#End Region

End Class