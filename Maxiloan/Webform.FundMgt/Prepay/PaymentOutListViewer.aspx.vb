﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class PaymentOutListViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property BDate() As String
        Get
            Return CType(ViewState("BDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BDate") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New FundingCompanyController
    Private oCustomClass As New Parameter.FundingContractBatch
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New FundingCompanyController
        Dim oPrepay As New Parameter.FundingContractBatch

        Dim objReport As PelunasanNormal = New PelunasanNormal
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oPrepay.strConnection = GetConnectionString()
        oPrepay.WhereCond = Me.CmdWhere
        oPrepay.LoginId = Me.Loginid
        oPrepay = m_controller.ListReportPaymentOut(oPrepay)
        oData = oPrepay.ListReport

        objReport.SetDataSource(oPrepay.ListReport)

        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "RptPrint.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("PaymentOutList.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "RptPrint")

    End Sub
    Private Sub AddParamField(ByVal objReport As PelunasanNormal, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPrint")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.BDate = cookie.Values("BusinessDate")
    End Sub
#End Region

End Class