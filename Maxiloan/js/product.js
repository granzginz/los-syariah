function cboIsSpAutomaticChange(cbo, cbob, txt) {
    var oComboval = $('#' + cbo + ' option:selected').val();
    var oComboBehaviour = $('#' + cbob);
    var otxt = $('#' + txt);
    if (oComboval == "0") {
        $('#' + cbob).prop("selectedIndex", 0); 
        $('#' + cbob).attr("disabled", "disabled");
        otxt.val("0");
        otxt.attr("disabled", "disabled");
    }
    else { $('#' + cbob).removeAttr('disabled'); otxt.removeAttr('disabled');
    } 
}

function cboIsSpAutomaticChange(cbo,  txt) {
    var oComboval = $('#' + cbo + ' option:selected').val();
     
    var otxt = $('#' + txt);
    if (oComboval == "0") { 
        otxt.val("0");
        otxt.attr("disabled", "disabled");
    }
    else {
        otxt.removeAttr('disabled');
    }
}
function EffectiveRate() {
        var oCombo = $('#Tabs_pnlTabSkema_SkemaTab_E_cboEffectiveRateBehaviour').val(); //document.forms[0].Hide_EffectiveRateBeh; 
        var otxt = $('#Tabs_pnlTabSkema_SkemaTab_E_txtEffectiveRate').val(); ; //document.forms[0].E_txtEffectiveRate;
        var otxt_GYR = $('#Tabs_pnlTabSkema_SkemaTab_E_txtGrossYieldRate').val(); //document.forms[0].E_txtGrossYieldRate;
        var ohide = $('#Tabs_pnlTabSkema_SkemaTab_E_Hide_EffectiveRate').val(); //  document.forms[0].Hide_EffectiveRate;  
        return;
    }


function MinimumTenor(min,max,hide) {
    var otxt = $('#' + min).val();  
    var otxt_max = $('#' + max).val();  
    var ohide = $('#' + hide).val();  
    if (otxt < ohide) { alert('Harap isi jangka waktu minimum antara ' + ohide + ' dan 999'); $('#' + min).val(ohide); } 
    if ( otxt >  otxt_max )  { alert('Jangka Waktu Minimum harus <= Jangka waktu Maximum'); $('#' + min).val(ohide); }
}

//Maximum Tenor
function MaximumTenor(min, max, hide) {    
var otxt = $('#' + min).val();   
var otxt_max = $('#' + max).val();  
var ohide = $('#' + hide).val();  
 

if (otxt_max > ohide) { alert('Harap isi jangka waktu Maximum antara  0 dan ' + ohide); $('#' + max).val(ohide); }
if (otxt_max < otxt) { alert('Jangka Waktu Minimum harus <= Jangka waktu Maximum'); $('#' + max).val(ohide); }
}
 

function com_NX(cbo,txt,hide,msg,maxval) {
    var oCombo =   $('#' + cbo + ' option:selected').val(); 
    var otxt = $('#' + txt).val();  
    var ohide = $('#' + hide).val();  
    if (typeof maxval === 'undefined' || maxval === null) {
        maxval = '9999'
    }
    if (oCombo == "N") {
        if ( otxt <  ohide ) {
            alert('Harap isi '+msg+' antara ' + ohide + ' dan '+maxval);
            $('#' + txt).val(ohide); 
        }
    }
    else {
        if (oCombo == "X") {
            if ( otxt  > ohide ) {
                alert('Harap isi '+msg+' antara 0 dan ' + ohide );
                $('#' + txt).val(ohide);  
            }
        }
    }
}
