﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClosedJurnal.aspx.vb" Inherits="Maxiloan.Webform.GL.ClosedJurnal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../js/jquery-1.9.1.min.js"></script>
	<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">
        //var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        //var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        //var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        var ServerName = window.location.protocol + '//' + window.location.host + '/';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);					
    </script>

    <script type="text/javascript">
        var submit = 0;
        function checkValidation() {
            if (++submit > 1) {
                alert('Ini terkadang memakan waktu beberapa detik - mohon bersabar.');
                return false;
            }
            return true;
        }
</script>
</head>
<body>
    <form id="form1" runat="server">
<asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1" >
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" Font-Bold="true"  Font-Size="Medium" onclick="hideMessage()" ></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        Closing Jurnal
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                <div class="form_single">
                    <label class="label_general">
                        Bulan</label>
                    <asp:DropDownList runat="server" ID="cboMonth">
                        <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                        <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                        <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                        <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                        <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                        <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
                    </asp:DropDownList> 
                </div>
                </div>
                <div class="form_box">
                        <div class="form_single">
                        <label class="label_general">
                            Tahun
                        </label>
                        <asp:TextBox runat="server" ID="txtTahun" CssClass="small_text"></asp:TextBox>
                        <asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 9999"
                            ControlToValidate="txtTahun" MaximumValue="9999" MinimumValue="0"
                                Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RegularExpressionValidator runat="server" ID="regexYear" ErrorMessage="Input Tahun Salah!"
                            Display="Dynamic" ControlToValidate="txtTahun" CssClass="validator_general" ValidationExpression="[0-9]{4}">
                        </asp:RegularExpressionValidator>
                        </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonClosed" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if(this.value === 'Saving...') { return false; } else { this.value = 'Saving...'; }">
                    </asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
