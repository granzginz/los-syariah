﻿
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter

Public Class JournalPostHarian
    Inherits WebBased 
    Private ReadOnly _jvController As New JournalVoucherController
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected WithEvents GridNavigator As ucGridNav

    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If


        Me.FormID = "JRNHRIAN"

        If SessionInvalid() Then
            Exit Sub
        End If
#If Not Debug Then
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
#End If
        
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If
        lblMessage.Visible = False
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not IsPostBack Then
            BindDdlBranch(ddlCabang, GetConnectionString, sesBranchId, IsHoBranch)
            ddlCabang.Items(0).Text = "ALL"
            Dim tglList = New List(Of ValueTextObject)
            Dim mnthList = New List(Of ValueTextObject)
            Dim yearList = New List(Of ValueTextObject)
            pnlGrid.Visible = False
            For i = 1 To 31
                tglList.Add(New ValueTextObject(i.ToString(), i.ToString()))
                If (i < 13) Then
                    Dim d = New DateTime(2000, i, 1)
                    mnthList.Add(New ValueTextObject(i, MonthName(d.Month, False)))
                End If
            Next
            With ddlDayFrom
                .DataSource = tglList
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataBind()
            End With
            With ddlDayTo
                .DataSource = tglList
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataBind()
            End With
            With ddlMonth
                .DataSource = mnthList
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataBind()
            End With

            txtTahun.Text = BusinessDate.Year
        End If
    End Sub

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs) 
        Dim dfrom = New DateTime(txtTahun.Text, CInt(ddlMonth.SelectedValue), ddlDayFrom.SelectedValue)
        Dim dto = New DateTime(txtTahun.Text, CInt(ddlMonth.SelectedValue), ddlDayTo.SelectedValue) 
        Dim result = _jvController.GetGlJournalAvailablePostPage(GetConnectionString(), SesCompanyID, ddlCabang.SelectedValue, dfrom, dto, PageSize, 1, _recordCount)

        ViewState("SOURCE") = result
        dtgCSV.DataSource = result
        dtgCSV.DataBind()
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetVoucher.Click
        Try
            Dim dfrom = New DateTime(txtTahun.Text, CInt(ddlMonth.SelectedValue), ddlDayFrom.SelectedValue)
            Dim dto = New DateTime(txtTahun.Text, CInt(ddlMonth.SelectedValue), ddlDayTo.SelectedValue)
             
            Dim result = _jvController.GetGlJournalAvailablePostPage(GetConnectionString(), SesCompanyID, ddlCabang.SelectedValue, dfrom, dto, PageSize, 1, _recordCount)
             
            If (result.Count <= 0) Then
                ShowMessage(lblMessage, "Tidak Ada Transaksi", False)
                pnlGrid.Visible = False
                Return
            End If
             
            ViewState("SOURCE") = result
            pnlGrid.Visible = True
            dtgCSV.DataSource = result
            dtgCSV.DataBind()
            GridNavigator.Initialize(_recordCount, PageSize)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Sub OrderGridView_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

    End Sub
    Private Sub DtgAsset_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dtgCSV.RowDataBound
        Dim imgBtn As ImageButton
        Dim data = CType(ViewState("SOURCE"), IList(Of JournalApproval))
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            imgBtn = CType(e.Row.FindControl("ImgBtn"), ImageButton)
            imgBtn.CommandArgument = e.Row.RowIndex.ToString() 
            Dim txtid = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionNo"))
            Dim objChildGrid As GridView = CType(e.Row.FindControl("grdDetails"), GridView)
            objChildGrid.DataSource = data.SingleOrDefault(Function(x) x.TransactionNo = txtid).Items
            objChildGrid.DataBind()
            objChildGrid.Visible = True
            Dim objPH As PlaceHolder = CType(e.Row.FindControl("objPHOrderDetails"), PlaceHolder)
            objPH.Visible = False 
            imgBtn.ImageUrl = "Images/Plus.gif"
            If (txtid = ViewState("OrderId")) Then
                objPH.Visible = True
                imgBtn.ImageUrl = "Images/Minus.gif"
            End If 
        End If
    End Sub


    Protected Sub grdOrders_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dtgCSV.RowCommand

        If (e.CommandName = "Expand") Then 
            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString()) 
            Dim imgbtn As ImageButton = CType(gv.Rows(rowIndex).FindControl("ImgBtn"), ImageButton)
             
            Dim key = gv.DataKeys(rowIndex)(0).ToString()
            ViewState("OrderId") = key
            Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails"), PlaceHolder)


            If (imgbtn.ImageUrl.Trim().ToUpper() = "IMAGES/PLUS.GIF") Then
                objPH.Visible = True
                imgbtn.ImageUrl = "images/Minus.gif" 
            Else
                imgbtn.ImageUrl = "images/Plus.gif"
                objPH.Visible = False 
            End If

        End If
    End Sub


    Private Sub doApprove(ByVal sender As Object, ByVal e As EventArgs) Handles btnApprove.Click

        Dim checkedIDs = (From msgRow As GridViewRow In dtgCSV.Rows
                 Where (CType(msgRow.FindControl("chkUpdateStatus"), CheckBox).Checked)
                 Select dtgCSV.DataKeys(msgRow.RowIndex).Value).ToList()

        If (checkedIDs.Count <= 0) Then
            ShowMessage(lblMessage, "Silahkan Pilih Journal yang akan di Post", False)
            Return
        End If
         
        Try
            Dim result = _jvController.GlJournalPost(GetConnectionString, checkedIDs.Cast(Of String)().ToList())
            btnFind_Click(Nothing, Nothing)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
End Class