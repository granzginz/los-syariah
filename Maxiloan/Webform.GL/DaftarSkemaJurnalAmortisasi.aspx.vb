﻿#Region "Imports"
Imports System.Data.SqlClient
Imports System.Web.Security
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
#End Region

Public Class DaftarSkemaJurnalAmortisasi
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property AssetParent() As String
        Get
            Return (CType(ViewState("AssetParent"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetParent") = Value
        End Set
    End Property

    Private Property MaxLevel() As String
        Get
            Return (CType(ViewState("MaxLevel"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MaxLevel") = Value
        End Set
    End Property

    Private Property Criteria() As String
        Get
            Return (CType(ViewState("Criteria"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Criteria") = Value
        End Set
    End Property

    Private Property AssetCode() As String
        Get
            Return (CType(ViewState("assetcode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("assetcode") = Value
        End Set
    End Property

    Private Property AssetLevel() As Integer
        Get
            Return (CType(ViewState("AssetLevel"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetLevel") = Value
        End Set
    End Property
    Private Property AssetLeveltest() As Integer
        Get
            Return (CType(ViewState("AssetLeveltest"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetLeveltest") = Value
        End Set
    End Property
    Private Property JumlahLevel() As Integer
        Get
            Return CType(ViewState("jumlahlevel"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("jumlahlevel") = Value
        End Set
    End Property
    Private Property Process() As String
        Get
            Return (CType(ViewState("Process"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New AmortisasiJurnalController
    Private m_controller As New AmortisasiJurnalController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        'If lblMessage.Text = "Data tidak ditemukan ....." Then
        '    lblMessage.Visible = False
        'End If
        If Not IsPostBack Then
            Me.JumlahLevel = 0
            DtgAgree.PageSize = 10
            Me.CmdWhere = ""
            InitialPanel()
            Me.SortBy = ""
            'If Request("cmd") = "dtlAssetMasterPrice" Then
            '    BindViewAsset(Request("AssetCode").Trim, 0, 0, "")
            'End If
        End If
    End Sub

    Private Sub InitialPanel()
        pnlList.Visible = True
        pnlGrid.Visible = True
        pnlView.Visible = False
        pnlViewAdd.Visible = False
        txtPage.Text = "1"
        doBinding("", "")
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
#End Region
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        txtSearchBy.Text = ""
        txtPage.Text = "1"
        pnlGrid.Visible = False
    End Sub

#Region "Bind Data Grid"
    Private Sub doBinding(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim dtEntity As DataTable = Nothing
        Dim oAssetMasterPrice As New Parameter.AmortisasiJurnal
        oAssetMasterPrice.strConnection = GetConnectionString()
        oAssetMasterPrice.WhereCond = cmdWhere
        oAssetMasterPrice.CurrentPage = currentPage
        oAssetMasterPrice.PageSize = pageSize
        oAssetMasterPrice.SortBy = ""
        oAssetMasterPrice = m_controller.GetData(oAssetMasterPrice)

        If Not oAssetMasterPrice Is Nothing Then
            dtEntity = oAssetMasterPrice.ListData
            recordCount = oAssetMasterPrice.TotalRecords
        Else
            recordCount = 0
        End If

        DtgAgree.DataSource = dtEntity.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()
    End Sub
#End Region

    Private Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click
        If cboSearchBy.Text = "SeqNo" Then
            Me.SearchBy = "SeqNoScheme =" & txtSearchBy.Text.Trim

            Me.SortBy = ""
            doBinding(Me.SearchBy, Me.SortBy)
        ElseIf cboSearchBy.Text = "Nama" Then
            Me.SearchBy = "Nama =" & " like '%" & txtSearchBy.Text.Trim & "%'"

            Me.SortBy = ""
            doBinding(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            doBinding(Me.SearchBy, Me.SortBy)
        End If
    End Sub


#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        doBinding("", "")
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                doBinding("", "")
            End If
        End If
    End Sub
#End Region
#Region "Data Grid Command"
    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Dim oCustomClass As New Parameter.AmortisasiJurnal
        Dim dtEntity As New DataTable
        Dim err As String
        Dim imbDelete As ImageButton
        Dim lblseqno As New Label

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
        Try
            If e.CommandName.Trim = "Edit" Then

                'If CheckFeature(Me.Loginid, "SIPP0010", "EDIT", "MAXILOAN") Then
                '    If SessionInvalid() Then
                '        Exit Sub
                '    End If
                'End If
                Me.Process = "EDIT"
                pnlList.Visible = False
                pnlGrid.Visible = False
                pnlView.Visible = True
                pnlViewAdd.Visible = True
                lblseqno.Visible = True
                txtSeqNo.Visible = True

                oCustomClass.ID = DtgAgree.DataKeys.Item(e.Item.ItemIndex).ToString
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass = oController.GetDataEdit(oCustomClass)
                If Not oCustomClass Is Nothing Then
                    dtEntity = oCustomClass.ListData
                End If

                txtID.Text = oCustomClass.ListData.Rows(0)("ID")
                txtSeqNo.Text = oCustomClass.ListData.Rows(0)("SeqNo")
                txtNama.Text = oCustomClass.ListData.Rows(0)("Name")
                txtCOADebet.Text = oCustomClass.ListData.Rows(0)("DEBET")
                txtCOAKredit.Text = oCustomClass.ListData.Rows(0)("CREDIT")

            ElseIf e.CommandName = "DEL" Then
                'If CheckFeature(Me.Loginid, "SIPP0010", "DEL", "MAXILOAN") Then
                '    If SessionInvalid() Then
                '        Exit Sub
                '    End If
                'End If
                Dim customClass As New Parameter.AmortisasiJurnal
                With customClass
                    .ID = DtgAgree.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With
                err = oController.GetDataDelete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
                InitialPanel()
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        'Response.Redirect("DaftarSkemaJurnalAmortisasi.aspx")
    End Sub
    Public Sub SortGrid(ByVal sender As Object, ByVal e As DataGridSortCommandEventArgs)
        If CInt(InStr(Me.SortBy, "DESC")) > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression & " DESC"
        End If
        doBinding("", "")
    End Sub
#End Region
#Region "getTransMessage"
    Private Function getTransMessage() As String
        Select Case Trim(Me.Mode)
            Case "del" : Return "Data Transaksi sudah dihapus"
            Case "new" : Return "Data Transaksi sudah disimpan"
            Case "edit" : Return "Data Transaksi sudah diupdate"
            Case Else : Return ""
        End Select
    End Function
#End Region
#Region "DAta Grid Command"
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        e.Item.Cells(1).Attributes.Add("onclick", "return fConfirm()")
        If (e.Item.Cells(6).Text = "3") Then
            e.Item.Cells(2).Text = ""
        End If
    End Sub
#End Region
#Region "Add New"
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        Dim lblseqno As New Label
        Me.Process = "ADD"
        Clean()
        pnlGrid.Visible = False
        pnlList.Visible = False
        txtSeqNo.Visible = False
        lblseqno.Visible = False
        pnlView.Visible = True
        pnlViewAdd.Visible = True

    End Sub
#End Region
#Region "btnClose"
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click

        pnlGrid.Visible = True
        pnlList.Visible = True
        pnlView.Visible = False
        pnlViewAdd.Visible = False

        Response.Redirect("DaftarSkemaJurnalAmortisasi.aspx")
    End Sub
#End Region
#Region "BtnSave"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.AmortisasiJurnal
        Dim ErrMessage As String = ""


        If Me.Process = "ADD" Then
            With customClass
                .Nama = txtNama.Text
                .CoaDebit = txtCOADebet.Text
                .CoaCredit = txtCOAKredit.Text

                .strConnection = GetConnectionString()
            End With
            oController.SaveData(customClass)
        ElseIf Me.Process = "EDIT" Then
            With customClass
                .SeqNo = txtSeqNo.Text
                .Nama = txtNama.Text
                .CoaDebit = txtCOADebet.Text
                .CoaCredit = txtCOAKredit.Text

                .strConnection = GetConnectionString()
            End With
            oController.SaveEditData(customClass)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        doBinding("", "")
        pnlView.Visible = False
        pnlViewAdd.Visible = False
        pnlList.Visible = True
        pnlGrid.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Update data berhasil.....", False)
        End If

    End Sub

#End Region
    Sub Clean()
        'txtSeqNo.Text = ""
        txtNama.Text = ""
        txtCOADebet.Text = ""
        txtCOAKredit.Text = ""
    End Sub

End Class