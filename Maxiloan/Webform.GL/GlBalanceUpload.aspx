﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="GlBalanceUpload.aspx.vb" Inherits="Maxiloan.Webform.GL.GlBalanceUpload" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"/>
    <asp:Panel runat="server" ID="pnlLoadload">
    <div class="form_title">
        <div class="title_strip" ></div>
        
        <div class="form_single">
            <h3>
               DBF UPLOAD GL BALANCE 
            </h3>
        </div>
    </div>
   
        <div class="form_box">
            <div class="form_single">
                <label> DBF FILE </label>
                <asp:FileUpload runat="server" ID="files" ViewStateMode="Enabled" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="validate" ControlToValidate="files" CssClass="validator_general"  ErrorMessage="Silahkan Pilih File DBF." />
                <%--<a onclick='SammpleCSV();' href='#'  > <span>Contoh Format File CSV</span></a>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>  Tahun </label> 
                <asp:TextBox ID="txtTahun" runat="server" onkeypress="return blockNonNumbers(this, event, true, true);"/>
                <asp:RequiredFieldValidator ID="rfsAccType" runat="server" ControlToValidate="txtTahun"  Display="Dynamic" CssClass="validator_general"  ValidationGroup="validate" ErrorMessage="Masukan Tahun Journal." InitialValue="0" />
                <asp:RangeValidator runat="server" id="rngDate" controltovalidate="txtTahun" Type="Integer"  MinimumValue="1999" MaximumValue="3000"  errormessage="Silahkan masukan tahun yang valid (1999-3000)!" />
                </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Load" ValidationGroup="validate" />&nbsp; 
        </div>
    
    </asp:Panel>
    
    
    
<asp:Panel runat="server" ID="pnlUploadProcess" visible="false">
    <div class="form_title">
        <div class="title_strip" ></div>
        <div class="form_single">
            <h3>
               VERIFIKASI DBF UPLOAD GL BALANCE 
            </h3>
        </div>
    </div>

    <div class="form_box">
        <div class="form_single">
            <asp:Label id="lblInfo" runat="server" ></asp:Label> 
        </div>
         </div>    
        
         <asp:Panel runat="server" ID="pnlGrid">
          <div class="form_box_header"> 
            <div class="form_single"> 
                <div class="grid_wrapper_ns">
                     <asp:GridView  ID="dtgCSV" runat="server"  CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"  AllowSorting="false" Width="100%" >
                         <HeaderStyle CssClass="th" /> 
                         <FooterStyle CssClass="item_grid" />
                         <Columns>
                             <asp:BoundField  Visible="True" DataField="Value" HeaderText="COA ID" />
                             <asp:BoundField  Visible="True" DataField="Text" HeaderText="COA NAME" />
                          </Columns>
                    </asp:GridView >
                     <uc2:ucGridNav id="GridNavigator" runat="server"/>
                     <div class="form_button" id="div1" runat="server">
                      <asp:Button ID="btnDownloadCSV" runat="server" CssClass="small button blue" Text="Download CSV" CausesValidation="False"/>  
                      <asp:Button ID="btnNext" runat="server" CssClass="small button blue" Text="Konfirmasi Upload Balance" CausesValidation="False"/> 
                     </div>
                 </div>
            </div>
        </div>
     </asp:Panel>

</asp:Panel>

<asp:Panel runat="server" ID="pnlUploadProcess_1" visible="false">
    <div class="form_title">
        <div class="title_strip" ></div>
        <div class="form_single">
            <h3>
               UPLOAD GL BALANCE 
            </h3>
        </div>
    </div>

    <asp:Panel runat="server" ID="Panel1">
          <div class="form_box_header"> 
            <div class="form_single"> 
                <div class="grid_wrapper_ns">
                     <asp:GridView  ID="dtgHistoryCoa" runat="server"  CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"  AllowSorting="false" Width="100%" >
                         <HeaderStyle CssClass="th" /> 
                         <FooterStyle CssClass="item_grid" />
                         <Columns>
                             <asp:BoundField  Visible="True" DataField="CoaId" HeaderText="COA ID" />
                             <asp:BoundField  Visible="True" DataField="NBegBal" HeaderText="Beg Bal" />
                             <asp:BoundField  Visible="True" DataField="NBAL1" HeaderText="Bal 1" />
                             <asp:BoundField  Visible="True" DataField="NBAL2" HeaderText="Bal 2" />
                             <asp:BoundField  Visible="True" DataField="NBAL3" HeaderText="Bal 3" />
                             <asp:BoundField  Visible="True" DataField="NBAL4" HeaderText="Bal 4"/>
                             <asp:BoundField  Visible="True" DataField="NBAL5" HeaderText="Bal 5"/>
                             <asp:BoundField  Visible="True" DataField="NBAL6" HeaderText="Bal 6"/>
                             <asp:BoundField  Visible="True" DataField="NBAL7" HeaderText="Bal 7"/>
                             <asp:BoundField  Visible="True" DataField="NBAL8" HeaderText="Bal 8"/>
                             <asp:BoundField  Visible="True" DataField="NBAL9" HeaderText="Bal 9"/>
                             <asp:BoundField  Visible="True" DataField="NBAL10" HeaderText="Bal 10" />
                             <asp:BoundField  Visible="True" DataField="NBAL11" HeaderText="Bal 11" />
                             <asp:BoundField  Visible="True" DataField="NBAL12" HeaderText="Bal 12" />
                          </Columns>
                    </asp:GridView >
                     <uc2:ucGridNav id="GridHistoryNavigator" runat="server"/> 
                 </div>
            </div>
        </div>
     </asp:Panel>

     <div class="form_button" id="divBtnConfirm" runat="server">
         <asp:Button ID="btnDoUpload" runat="server" CssClass="small button blue" Text="Confirm" CausesValidation="False"/>&nbsp;
         <asp:Button ID="btnBatal" runat="server" CssClass="small button blue" Text="Cancel" CausesValidation="False"/>&nbsp;
     </div>
</asp:Panel>
 <STYLE>.aspNetDisabled { color: gray  !important;  }</STYLE>
</asp:Content>
