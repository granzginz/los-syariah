﻿
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports Maxiloan.Webform.UserController
Imports ClosedXML.Excel
Imports System.IO
Imports Maxiloan.Framework.SQLEngine
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.OpenXml4Net.OPC
Imports NPOI.XSSF.UserModel

Public Class UploaCsvGl
    Inherits WebBased
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents UcSearchBy As UcSearchBy
    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    Private debTotal As Decimal = 0
    Private creTotal As Decimal = 0
    Private ReadOnly _jvController As New JournalVoucherController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "UPGL"
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__1.RegisterPostBackControl(Me.btDownLoadFile)

        If Not Page.IsPostBack Then
            pnlLoadCSV.Visible = True
            pnlGrid.Visible = False
            PanelNotSukses.Visible = False

        End If
        'If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
        '    Return
        'End If
    End Sub
    Sub showValidGrid()
        Try
            pnlGrid.Visible = True

            Dim result = _jvController.GlJournalPageNotSuccess(GetConnectionString(), New JournalApproval With {
                                                           .CompanyId = SesCompanyID}, PageSize, 1, _recordCount)
            ViewState("SOURCE") = result
            If (result.Count <= 0) Then
                ShowMessage(lblMessage, "Tidak Ada Transaksi", False)
                pnlGrid.Visible = False
                PanelNotSukses.Visible = False
                Return
            End If

            pnlGrid.Visible = False
            PanelNotSukses.Visible = True
            GridView1.DataSource = result
            GridView1.DataBind()
            GridNavigator.Initialize(_recordCount, PageSize)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub DtgAsset_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim ImgBtn1 As ImageButton
        Dim data = CType(ViewState("SOURCE"), IList(Of JournalApproval))
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            ImgBtn1 = CType(e.Row.FindControl("ImgBtn1"), ImageButton)
            ImgBtn1.CommandArgument = e.Row.RowIndex.ToString()


            Dim txtid = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionNo"))
            Dim objChildGrid As GridView = CType(e.Row.FindControl("grdDetails1"), GridView)
            objChildGrid.DataSource = data.SingleOrDefault(Function(x) x.TransactionNo = txtid).Items
            objChildGrid.DataBind()
            objChildGrid.Visible = True
            Dim objPH As PlaceHolder = CType(e.Row.FindControl("objPHOrderDetails1"), PlaceHolder)
            objPH.Visible = False
            ImgBtn1.ImageUrl = "Images/Plus.gif"
            If (txtid = ViewState("OrderId")) Then
                objPH.Visible = True
                ImgBtn1.ImageUrl = "Images/Minus.gif"
            End If
        End If
    End Sub

    Protected Sub grdOrders1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand

        If (e.CommandName = "Expand1") Then

            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString())
            Dim imgbtn As ImageButton = CType(gv.Rows(rowIndex).FindControl("ImgBtn1"), ImageButton)

            Dim key = gv.DataKeys(rowIndex)(0).ToString()
            ViewState("OrderId") = key
            Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails1"), PlaceHolder)

            If (imgbtn.ImageUrl.Trim().ToUpper() = "IMAGES/PLUS.GIF") Then
                objPH.Visible = True
                imgbtn.ImageUrl = "images/Minus.gif"
            Else
                imgbtn.ImageUrl = "images/Plus.gif"
                objPH.Visible = False
            End If

        End If
    End Sub


    Protected Sub grdOrders_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dtgCSV.RowCommand
        If (e.CommandName = "Expand") Then


            Dim imgbtn As ImageButton
            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString())
            Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails"), PlaceHolder)
            Dim objChildGrid As GridView = CType(gv.Rows(rowIndex).FindControl("grdDetails"), GridView)
            imgbtn = CType(gv.Rows(rowIndex).FindControl("ImgBtn"), ImageButton)


            If (imgbtn.ImageUrl = "images/Plus.gif") Then
                Dim key = gv.DataKeys(rowIndex)(0).ToString()
                ViewState("OrderId") = key
                Dim data = CType(ViewState("SOURCE"), IList(Of GlJournalVoucher))
                Dim source As IList(Of JournalVoucherItem)
                For Each dt In From dt1 In data Where dt1.TransactionNomor = key
                    source = dt.JournalItems
                    Exit For
                Next

                imgbtn.ImageUrl = "images/Minus.gif"

                If Not (objChildGrid Is Nothing) Then
                    If Not (objPH Is Nothing) Then
                        objPH.Visible = True
                    End If
                    objChildGrid.DataSource = source
                    objChildGrid.DataBind()
                    objChildGrid.Visible = True
                End If
            Else
                If Not (objChildGrid Is Nothing) Then

                    imgbtn.ImageUrl = "images/Plus.gif"
                    If Not (objPH Is Nothing) Then
                        objPH.Visible = False
                    End If
                    objChildGrid.Visible = False
                End If
            End If

        End If
    End Sub

    Protected Sub grdOrders_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Dim imgBtn As ImageButton

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            imgBtn = CType(e.Row.FindControl("ImgBtn"), ImageButton)
            imgBtn.CommandArgument = e.Row.RowIndex.ToString()
            Dim txtid = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionNomor"))


            If (txtid = ViewState("OrderId")) Then
                Dim objPH As PlaceHolder
                objPH = CType(e.Row.FindControl("objPHOrderDetails"), PlaceHolder)
                If Not (objPH Is Nothing) Then
                    objPH.Visible = True
                End If

                If (imgBtn.ImageUrl = "Images/Plus.gif") Then
                    imgBtn.ImageUrl = "Images/Minus.gif"
                Else
                    imgBtn.ImageUrl = "Images/Plus.gif"
                End If

            End If

        End If
    End Sub

    Protected Sub DoUpload(sender As Object, e As EventArgs) Handles btnDoUpload.Click
        Try
            Dim datas = CType(ViewState("SOURCE"), IList(Of GlJournalVoucher))
            Dim result = _jvController.DoUploadCsvGlJournals(GetConnectionString, datas)

            If (result <> "OK") Then
                ShowMessage(lblMessage, result, False)
                Exit Sub
            End If

            'Response.Redirect("UploaCsvGl.aspx")
            showValidGrid()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Function getCompId() As String
        Dim controller As New JournalVoucherController()
        Return controller.GetCompanyId(GetConnectionString)
    End Function
    Protected Sub Back_Click(sender As Object, e As EventArgs) Handles btnBatal.Click
        Response.Redirect("UploaCsvGl.aspx")
    End Sub
    Protected Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click
        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
        Dim directory As String = physicalPath & "\xml\"
        Dim fileName As String = DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + files.PostedFile.FileName
        Dim fullFileName As String = directory & fileName
        Dim sr As IO.StreamReader
        Try
            files.PostedFile.SaveAs(fullFileName)
            sr = New IO.StreamReader(fullFileName)

            Dim ret = GlJournalVoucher.ParseJournalCsv(sr, getCompId(), sesBranchId.Replace("'", ""))

            If (ret.Count <= 0) Then
                ShowMessage(lblMessage, "CSV File Tidak Ada Data", False)
                Return
            End If



            _jvController.ValidateCsvConponen(GetConnectionString, ret)



            ViewState("SOURCE") = ret
            dtgCSV.DataSource = ret
            dtgCSV.DataBind()

            pnlLoadCSV.Visible = False
            pnlGrid.Visible = True
            PanelNotSukses.Visible = False
            'validateGlPeriod(ret)
            isCsvFileValid(ret)
            pnlUpladProcess.Visible = True

        Catch ex As Exception
            pnlUpladProcess.Visible = False
            ShowMessage(lblMessage, ex.Message, True)
            If Not (sr Is Nothing) Then
                sr.Dispose()
            End If
        End Try
    End Sub

    Sub validateGlPeriod(data As IList(Of GlJournalVoucher))
        Dim ypCont = New GlYearPeriodController()
        'Dim prds = ypCont.YearPeriods(GetConnectionString, SesCompanyID, sesBranchId.Replace("'", ""), BusinessDate.Year, BusinessDate.Month)

        'Taufik
        'If (prds.Count = 0) Then
        '    Throw New Exception("Journal Periode tidak ditemukan")
        'End If

        For Each v In data
            'Dim prd = prds.SingleOrDefault(Function(r) r.IsInRange(v.TransactionDate))
            Dim prd = ypCont.YearPeriods(GetConnectionString, SesCompanyID, sesBranchId.Replace("'", ""), v.TransactionDate.Year.ToString, v.TransactionDate.Month.ToString)
            If (prd.Count = 0) Then
                Throw New Exception(String.Format("Tanggal Periode transaksi {0:dd/MM/yyyy} telah close / belum di open, No Voucher {1}.", v.TransactionDate, v.TransactionNomor))
            End If

            'Taufik-
            'If prd Is Nothing Then
            '    Throw New Exception("Journal Periode tidak ditemukan")
            'End If
            'If prd.Status = PeriodStatus.Close Then
            '    Throw New Exception(String.Format("Tanggal Periode transaksi {0:dd/MM/yyyy} telah close.", v.TransactionDate))
            'End If
        Next

    End Sub

    Sub isCsvFileValid(data As IList(Of GlJournalVoucher))
        For Each row In From row1 In data Where Not (row1.IsBalance And row1.IsItemValid)
            Throw New Exception("Data CSV File Tidak Valid. Silahkan Perbaiki dan Upload Ulang.")
        Next
    End Sub

    Private Sub BtnDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDownLoadFile.Click

        Dim strFilePath As String

        strFilePath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "All_Files\UploadJurnalManual.csv"
        Download_File(strFilePath)

    End Sub


    Private Sub Download_File(FilePath As String)

        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath))
        Response.WriteFile(FilePath)
        Response.End()
    End Sub

End Class