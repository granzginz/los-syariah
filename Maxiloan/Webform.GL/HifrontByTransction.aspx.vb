﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Threading

Public Class HifronByTransaction
    Inherits WebBased

    Protected WithEvents txtTanggalVoucher As ucDateCE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "HIFRONTRAN"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If Not Page.IsPostBack Then
            txtTrNo.Text = ""
        End If

    End Sub

    Private ReadOnly _jvController As New JournalVoucherController
    Private Sub doGetVoucher(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetVoucher.Click
        'If (txtTrNo.Text.Trim() = String.Empty) Then
        '    ShowMessage(lblMessage, "Masukan No Transaksi", False)
        '    Return
        'End If
        'Dim res As StringBuilder = _jvController.GetHifrontTransaksi(GetConnectionString(), txtTrNo.Text)
        'Response.ContentType = "text/plain"
        'Response.AddHeader("content-disposition", "attachment;filename=" + String.Format("{0}{1}.txt", txtTrNo.Text, String.Format("{0:HHMMSS}", DateTime.Today)))
        'Response.Clear()

        'Dim writer = New System.IO.StreamWriter(Response.OutputStream, Encoding.UTF8)

        'writer.Write(res.ToString.Trim())

        'Response.End()

        '------------------------------------------------------------------------
        'Ditulis ulang sama Wira, pake method yang sedikit berbeda 20160414
        '------------------------------------------------------------------------
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objCommand As New SqlCommand

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            Dim sda As New SqlDataAdapter()
            objCommand.Connection = objConnection
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "sp_HiFrontsDtlListbyTrNO"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@TrNo", SqlDbType.VarChar, 20).Value = txtTrNo.Text.Trim
            sda.SelectCommand = objCommand
            Dim dt As New DataTable()
            sda.Fill(dt)

            'Build the Text file data.
            Dim txt As String = String.Empty

            For Each column As DataColumn In dt.Columns
                'Add the Header row for Text file.
                If column.ColumnName <> "data" Then
                    txt += column.ColumnName & vbTab & vbTab
                End If
            Next

            'Add new line.
            txt += vbCr & vbLf

            For Each row As DataRow In dt.Rows
                For Each column As DataColumn In dt.Columns
                    'Add the Data rows.
                    txt += row(column.ColumnName).ToString() & vbTab & vbTab
                Next

                'Add new line.
                txt += vbCr & vbLf
            Next

            'Download the Text file.
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" + String.Format("{0}{1}.txt", txtTrNo.Text, String.Format("{0:HHMMSS}", DateTime.Today)))
            Response.Charset = ""
            Response.ContentType = "application/text"
            Response.Output.WriteLine("*DocNo	Type	CompID	DocDate	PostDate	FiscalPriod	RptDate	DocType	Cur	RefDoc	HdrTxt	Leger	CalTax	ExchRate	Line	PostKey	GL	CustID	AccID	SGL	TGL	TransType	AmtDocCur	AmtLocCur	AmtLocCur2	SalesTax	TaxBaseAmtDoc	TaxAmt	TaxBaseAmtLoc	TaxAmtLoc	Segment	Business	Profit	Cost	OrderNo	GnG	FreeInfo	Country	CustNo	VendorNo	Comm3	Comm4	Comm5	Comp1	Comp2	Comp3	Comp4	Comp5	Comp6	Comp7	Comp8	Comp9	Comp10	BusRef1	BusRef2	RefKey4Line	InvRef	FiscalYear	LineItem	ContrNo	ContrType	PayRef	ValueDate	BsLineDate	TermsOfPay	Day1	CashDisc1	Day2	CashDisc2	NetPay	DiscBaseAmt	DiscAmt	PayMethod	ShortKey4Bank	PartBankType	PayBlok	ReasonPay	SCB	SuppCountry	ExtOrderNo	AssNo	ItemText	Qty	BsUnit	CompCode4NextLine	Indi4Hold1	HoldTaxCode1	HoldTaxBase1	HoldAmt1	Indi4Hold2	HoldTaxCode2	HoldTaxBase2	HoldAmt2	Indi4Hold3	HoldTaxCode3	HoldTaxBase3	HoldAmt3	Indi4Hold4	HoldTaxCode4	HoldTaxBase4	HoldAmt4	TaxJurisCode	BranchId	NumOfPageInv	NegativPost	BusPlace	SectCode	PayMethod	Payer	Flag	Plan	Cur4AutoPay	Part1	Part2	Part3	WBS	LongText")
            Response.Output.Write(txt)
            Try
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.SuppressContent = True
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch err As ThreadAbortException
            End Try

            objCommand.Dispose()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
End Class