﻿#Region "imports"
Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class BalanceSheetGroupDetailEdit
    Inherits WebBased


#Region "properties"
    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CBool(ViewState("isNew"))
        End Get
    End Property

    Private Property reportid() As String
        Get
            Return CType(ViewState("reportid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("reportid") = Value
        End Set
    End Property
#End Region

    Const FORM_ID As String = "BSSLIST"
    Const PAR_RID As String = "rid"
    Const PAR_RGID As String = "rgid"
    Const PAR_GROUPID As String = "gid"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = FORM_ID
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then

            isNew = True
            Dim GroupId = Request.QueryString(PAR_GROUPID)
            Dim prgid = Request.QueryString(PAR_RGID)
            Dim rid = Request.QueryString(PAR_RID)

            Me.reportid = rid

            'bindDdlGROUP()

            With ddlAccountType
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataSource = ModuleGlHelper.GetGLBalanceSheetAccTypeList()
                .DataBind()
            End With

            hdnReportGroupId.Value = prgid
            hdnGroupId.Value = GroupId

            If Request.QueryString(PAR_GROUPID) <> String.Empty Then
                loaddata(prgid, GroupId)
                isNew = False
            End If
        End If
    End Sub

    Private Sub loaddata(ByVal prgid As Integer, gid As Integer)

        Dim bsCon As New BalanceSheetSetupController
        Dim result As BalanceSheetGroupDetail = bsCon.FindByIdBSGroupDetail(GetConnectionString, prgid, gid)

        If (result Is Nothing) Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            Return
        End If

        ddlAccountType.SelectedValue = result.accType
        'ddlGroup.SelectedValue = result.GroupID
        txtGroupId.Text = result.GroupID
        txtGroupName.Text = result.GroupDescription
        hdnGroupId.Value = gid
        hdnReportGroupId.Value = prgid

        'ddlGroup.Enabled = False
        ddlAccountType.Enabled = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("BalanceSheetGroupDetail.aspx?rgid=" & hdnReportGroupId.Value)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim bsCon As New BalanceSheetSetupController
            Dim bsdata As New BalanceSheetGroupDetail(CInt(hdnReportGroupId.Value), txtGroupId.Text, CType(CInt(ddlAccountType.SelectedValue), GLBalanceSheetAccountType), txtGroupId.Text)

            If isNew Then
                bsCon.InsertBSGroupDetail(GetConnectionString, bsdata)
            Else
                bsCon.UpdateBSGroupDetail(GetConnectionString, bsdata)
            End If

            Response.Redirect("BalanceSheetGroupDetail.aspx?rgid=" & hdnReportGroupId.Value)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    'Private Sub bindDdlGROUP()
    '    Dim m_controller As New BalanceSheetSetupController
    '    With ddlGroup
    '        .DataSource = m_controller.SelectAllBSGroupDetail(GetConnectionString, Me.reportid)
    '        .DataValueField = "GroupId"
    '        .DataTextField = "GroupDesc"
    '        .DataBind()
    '        .Items.Insert(0, "Select One")
    '        .Items(0).Value = "0"
    '    End With
    'End Sub

End Class