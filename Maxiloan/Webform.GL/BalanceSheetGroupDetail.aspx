﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="BalanceSheetGroupDetail.aspx.vb" Inherits="Maxiloan.Webform.GL.BalanceSheetGroupDetailList" %>
 
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
    

    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CARI GROUP
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Group Description</label>
            <asp:TextBox ID="txtCari" runat="server" placeholder="Kata kunci pencarian"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnFind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue"/>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"/>
        <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray"/>
        <asp:HiddenField id="hdIsFind" runat="server" Value="0"/>
    </div>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                BALANCE SHEET SETUP - DETAIL <asp:Label runat="server" ID="lblGroupName"  />
            </h3>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="False" AutoGenerateColumns="false"
                    DataKeyField="GroupId" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                       <asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../Images/iconedit.gif" CommandName="Edit"
                                    OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("ReportGroupId").tostring() + "," + Eval("GroupId").tostring() %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" >
                            <ItemTemplate>
                                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                    CommandName="Delete" OnClientClick="return confirmDelete()" OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("ReportGroupId").tostring() + "," + Eval("GroupId").tostring()  %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="GroupID" HeaderText="Group ID" />              
                        <asp:BoundColumn DataField="GroupDescription" HeaderText="Group Description" /> 
                        <asp:BoundColumn DataField="accType" HeaderText="Account Type" /> 
                    </Columns>
                </asp:DataGrid>
                <uc2:ucGridNav id="GridNavigator" runat="server"/>
              
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:HiddenField runat="server" ID="hdnReportId" />
        <asp:HiddenField runat="server" ID="hdnReportGroupId" />
        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
        </asp:Button>
    </div>
    
    
    </asp:Content>
<%--    </form>
</body>
</html>
--%>