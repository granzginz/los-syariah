﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="GL.Master"  CodeBehind="BalanceSheetAccGroupDetail.aspx.vb" Inherits="Maxiloan.Webform.GL.BalanceSheetAccGroupDetail" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
   <asp:Label ID="Label1" runat="server"  ></asp:Label>
    <div id="jlookupContent" runat="server" />
 
    
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REGISTRASI BALANCE SHEET ACCOUNT GROUP 
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Group Description</label>
                    <asp:TextBox ID="txtGroupDesc" runat="server" Width="400px" />
                    <asp:HiddenField runat="server" ID="hdnReportGroupId" />
                    <asp:HiddenField runat="server" ID="hdnReportId" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvGroupDesc" Display="Dynamic" CssClass="validator_general"
                        ErrorMessage="Isi group description" ControlToValidate="txtGroupDesc" />
                </div>
            </div>
            <%--<div class="form_box" >
                <div class="form_single">
                    <label>
                        Parent Group</label>
                    <asp:DropDownList ID="ddlParentGroup" runat="server" Width="700px"/>
                </div>
            </div>--%>
            <div class="form_box">
                <div class="form_single">
                    <label>Parent Group</label>
                    <asp:TextBox ID="txtGroupId" runat="server"  onchange="show()"  CssClass="form_box_hide"></asp:TextBox>
                    <asp:TextBox ID="txtGroupName" runat="server"   CssClass="medium_text" Enabled="false" width="300px"></asp:TextBox>
                    <asp:DropDownList ID="ddlParentGroup" runat="server" Width="700px" CssClass="form_box_hide"/>
                    <button class="small buttongo blue" 
                    onclick ="OpenLookup();return false;">...</button >                        
                    <asp:Button runat="server" ID="Jlookup" style="display:none" />
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Layout Sequence</label>
                    <asp:TextBox ID="txtLayoutSequence" runat="server"  CssClass="medium_text" Width="50px"/>
                    <asp:Label id="lblLevel" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvLayoutSequence" runat="server" ControlToValidate="txtLayoutSequence"
                        ErrorMessage="Harap Di Isi dengang Nomor" Visible="True" Enabled="True" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rvfLayoutSequence" runat="server" ControlToValidate="txtLayoutSequence"
                        ErrorMessage="Harap Di Isi dengang Nomor" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                        Visible="True" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="rgValLayoutSequence" runat="server" font-name="Verdana" Font-Size="11px"
                        Type="Double" ControlToValidate="txtLayoutSequence" MinimumValue="0" MaximumValue="9999"
                        Visible="True" Display="Dynamic"></asp:RangeValidator>
                </div>
            </div>--%>
            <div class="form_box">
                <div class="form_single">
                   <label>
                        Group Type</label>
                    <asp:DropDownList ID="ddlGroupType" runat="server" />
                </div>
            </div>
            <div class="form_box" visible="false" runat="server">
                <div class="form_single">
                    <label>
                        isBar</label>
                    <asp:CheckBox ID="chkisBar" runat="server" Checked="false"/>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="small button gray" />
            </div>    

    <script type="text/javascript">

        $(document).ready(function () {
            

        })

        function OpenLookup()  {
                OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/GroupCoa.aspx?kodecid=" & txtGroupId.ClientID & "&namacid=" & txtGroupName.ClientID & "&isDt=1") %>', 'DAFTAR Group Chart Of Account', '<%= jlookupContent.ClientID %>', 'DoPostBack')
    }
        
    </script>
</asp:Content> 