﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="GL.Master" CodeBehind="HifrontDetails.aspx.vb" Inherits="Maxiloan.Webform.GL.HifrontDetails" %>
   <%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
               DOWNLOAD HI FRONTS TEXT
            </h3>
        </div>
    </div>
        

    <div class="form_box">
        <div class="form_single">
            <label>Masukan Tanggal</label>
             <uc:ucdatece runat="server" id="txtTanggalVoucher" />
        </div>
    </div> 
    
        <div class="form_button" > 
        <asp:Button ID="btnGetVoucher" runat="server"  CausesValidation="False" Text="Download HiFront Text" CssClass="small button blue"/> 
     </div>
</asp:Content>
