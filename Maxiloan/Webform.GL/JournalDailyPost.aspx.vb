﻿
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter

Public Class JournalDailyPost
    Inherits WebBased
    'Private Const PageSize As Integer = 10
    'Protected WithEvents GridNavigator As ucGridNav
    'Private _recordCount As Integer = 0
    'Protected WithEvents ucDateTrDate As ucDateCE
    Private ReadOnly _jvController As New JournalVoucherController
    'Private ReadOnly _glYearContrl = New GlYearPeriodController
    Private _year As Integer '= 2014 'DateTime.Now.Year

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "GLDAYPOST"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If

        If (String.IsNullOrEmpty(BranchID)) Then
            BranchID = sesBranchId.Replace("'", "")
        End If
        If (String.IsNullOrEmpty(SesCompanyID)) Then
            SesCompanyID = GetCompanyId(GetConnectionString)
        End If

        ' AddHandler GridNavigator.PageChanged, AddressOf Navevent
        '_year = BusinessDate.Year

        If Not Page.IsPostBack Then
            hdPostMethod.Value = 0


            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses Daily Post Journal", False)
            End If

            If Request("EXT") = "ext" Then
                divPostSusulan.Visible = True
                divPostNormal.Visible = False
                divButton.Visible = False
                Return
            End If

            initView()

            'lblYear.Text = _year

            ' Dim prdTest = _glYearContrl.IsYearValid(GetConnectionString, _year, SesCompanyID, sesBranchId.Replace("'", ""))

            'If (Not prdTest) Then
            '    pnlmain.Visible = False
            '    divMessage.Visible = True
            '    lblMessageYear.Text = String.Format("Tahun Periode GL {0} belum tersedia, Silahkan Generate dan ulang lagi.", _year)
            '    ShowMessage(lblMessage, "Tahun Periode GL belum tersedia.", True)
            '    Return
            'End If

            'With(ddlMonth)
            '    .DataValueField = "Value"
            '    .DataTextField = "Text"
            '    .DataSource = getMonth()
            '    .DataBind()
            'End With 
            'InitGrid(_jvController.GetGlJournalAvailablePost(_recordCount, GetConnectionString(), New Object() {_year, "ALL", 1, PageSize, SesCompanyID, BranchID}))
            ' InitGrid(New Object() {_year, "ALL", 1, PageSize, SesCompanyID, BranchID})
        End If
         
    End Sub
     
    Sub initView()

        Dim res = _jvController.GetGlJournalAvailablePost(0, GetConnectionString(), New Object() {SesCompanyID, BranchID})
        lblPrev.Text = res.PostSchedule(0)
        lblCurr.Text = res.PostSchedule(1)
        lblNext.Text = res.PostSchedule(2)

        Dim build = New StringBuilder()
        Dim bnsDateChk = res.SetPostSchedule(1) > BusinessDate

        If (res.IsNoTrans = True) Then
            hdPostMethod.Value = 1
            divMessage.Visible = True
            btnpost.Text = "SKIP"

            build.Append("Tidak Ada Transaksi pada tanggal Current Post <br />")

            If (bnsDateChk = True) Then
                build.Append("<strong>Posting Date tidak boleh lebih besar dari Business Date </strong><br />")
                divButton.Visible = False
            End If

            lblMessageYear.Text = build.ToString()
            Return
        End If



        If (res.GetBrokenRules().Count > 0 Or bnsDateChk) Then
            divButton.Visible = False
            divMessage.Visible = True


            build.Append("<strong><u>Keterangan Masalah </u></strong><br />")
            If (bnsDateChk = True) Then
                build.Append("Posting Date tidak boleh lebih besar dari Business Date <br />")
            End If
            For Each brule In res.GetBrokenRules()
                build.Append(brule.RuleDescription)
                build.Append(" <br />")
            Next

            lblMessageYear.Text = build.ToString()

            If (res.ShowTrans) Then
                bindInfoJournal(res)
            End If
        Else

            bindInfoJournal(res)
        End If

    End Sub

    Sub bindInfoJournal(item As GlJournalPostAvailable)
        divInfoJournal.Visible = True
        lbltgl.InnerHtml = item.Value
        lblketerangan.InnerHtml = item.Text
        lblDebet.InnerHtml = String.Format("{0:#,0}", item.Debit)

        lblKredit.InnerHtml = String.Format("{0:#,0}", item.Credit)

    End Sub

    Private Sub doPost(ByVal sender As Object, ByVal e As EventArgs) Handles btnpost.Click
        Try
            '--------------------
            ' hdPostMethod.Value = 0 nornal / 1- skip date / 2 -  Posting sususlan
            '
          
            Dim param As Object()

            If (hdPostMethod.Value = "2") Then
                param = New Object() {SesCompanyID, BranchID, "Daily Post Susulan", UserID, hdPostMethod.Value, ConvertDate2(txtTanggalVoucher.Text)}
            Else
                param = New Object() {SesCompanyID, BranchID, IIf(hdPostMethod.Value = "0", "Daily Post", "Skip Posting Date"), UserID, hdPostMethod.Value}
            End If

            Dim result = _jvController.DoDailyJournalPost(GetConnectionString, param)

            If (result.Contains("ERROR")) Then
                ShowMessage(lblMessage, result, True)
                Return
            End If


            If (hdPostMethod.Value = "2") Then
                Response.Redirect("JournalDailyPost.aspx?ext=ext&s=1")
            Else
                Response.Redirect("JournalDailyPost.aspx?s=1")
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Protected WithEvents txtTanggalVoucher As ucDateCE
    Private Sub doGetVoucher(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetVoucher.Click
        If (txtTanggalVoucher.Text.Trim() = String.Empty) Then
            ShowMessage(lblMessage, "Masukan Tanggal Journal Transaksi", True)
            Return
        End If

        Dim tglVoucher = ConvertDate2(txtTanggalVoucher.Text)

        If (tglVoucher > BusinessDate) Then
            ShowMessage(lblMessage, "Tanggal Journal Transaksi tidak boleh lebih besar dari Business Date", True)
            Return
        End If

        Dim res = _jvController.GetGlJournalAvailablePost(0, GetConnectionString(), New Object() {SesCompanyID, BranchID, tglVoucher})

        If (res.IsNoTrans = True) Then
            ShowMessage(lblMessage, String.Format("Tanggal Transaksi {0:d} tidak ada journal", tglVoucher), False)
            Return
        End If

        Dim broken = res.GetBrokenRules()
         
        If (broken.Count > 0) Then
            divButton.Visible = False
            divMessage.Visible = True
            Dim build = New StringBuilder()

            build.Append("<strong><u>Keterangan Masalah </u></strong><br />")
           
            For Each brule In res.GetBrokenRules()
                build.Append(brule.RuleDescription)
                build.Append(" <br />")
            Next

            lblMessageYear.Text = build.ToString()

            If (res.ShowTrans) Then
                bindInfoJournal(res)
            End If

            Return

        End If
        divButton.Visible = True
        hdPostMethod.Value = 2
        bindInfoJournal(res)


    End Sub
    'Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
    '    Dim mth = IIf(hdIsFind.Value = "1", ddlMonth.SelectedValue, "ALL")
    '    InitGrid(New Object() {_year, mth, e.CurrentPage, PageSize, SesCompanyID, BranchID})
    '    GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    'End Sub

    'Private Function getMonth() As IList(Of ValueTextObject)
    '    Dim ret = New List(Of ValueTextObject)
    '    ret.Add(New ValueTextObject("ALL", "ALL"))

    '    For i As Integer = 1 To 12
    '        ret.Add(New ValueTextObject(i, i))
    '    Next

    '    Return ret
    'End Function

    'Private Sub DtgAsset_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dtgFilterJournal.RowDataBound
    '    Dim lblBranchId As Label
    '    Dim imgBtn As LinkButton

    '    If (e.Row.RowType = DataControlRowType.DataRow) Then 
    '        imgBtn = CType(e.Row.FindControl("btnPost"), LinkButton)
    '        lblBranchId = CType(e.Row.FindControl("lblIsBalance"), Label)
    '        If (CType(lblBranchId.Text, Boolean) = False) Then
    '            imgBtn.Visible = False
    '            e.Row.Cells(3).BackColor = System.Drawing.Color.Red
    '        End If


    '        Dim lblIsPeriodValid = CType(e.Row.FindControl("lblIsPeriodValid"), Label)
    '        If (CType(lblIsPeriodValid.Text, Boolean) = False) Then
    '            imgBtn.Visible = False
    '            e.Row.Cells(2).BackColor = System.Drawing.Color.Red
    '        End If

    '    End If
    'End Sub

    'Protected Sub dtgFilterJournal_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dtgFilterJournal.RowCommand
    '    If (e.CommandName = "Post") Then
    '        Dim id = CType(e.CommandArgument, DateTime)
    '        DoUpload(id)
    '    End If
    'End Sub

    'Private Sub InitGrid(ParamArray params() As Object)

    '    dtgFilterJournal.DataSource = _jvController.GetGlJournalAvailablePost(_recordCount, GetConnectionString(), params)
    '    dtgFilterJournal.DataBind()
    '    GridNavigator.Initialize(_recordCount, PageSize)
    'End Sub

    'Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click 
    '    InitGrid(New Object() {_year, ddlMonth.SelectedValue, 1, PageSize, SesCompanyID, BranchID})
    '    hdIsFind.Value = "1" 
    '    GridNavigator.Initialize(_recordCount, PageSize)
    'End Sub
    'Protected Sub DoUpload(trDate As DateTime)
    '    Try 
    '        Dim resul = _jvController.DoDailyJournalPost(GetConnectionString, trDate)
    '        ShowMessage(lblMessage, resul, resul.Contains("ERROR"))
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try

    '    InitGrid(New Object() {_year, ddlMonth.SelectedValue, 1, PageSize, SesCompanyID, BranchID})
    '    GridNavigator.Initialize(_recordCount, PageSize)
    'End Sub


    'Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
    '    Response.Redirect("JournalDailyPost.aspx")
    'End Sub
End Class