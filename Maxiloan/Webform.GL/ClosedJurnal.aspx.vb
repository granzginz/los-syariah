﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class ClosedJurnal
    Inherits Maxiloan.Webform.AccMntWebBased
    Private m_controller As New DataUserControlController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.FormID = "CLOSEDJUR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            cboMonth.SelectedValue = Me.BusinessDate.Month.ToString
            txtTahun.Text = Me.BusinessDate.Year.ToString
        End If
    End Sub
    Protected Sub btnButtonClosed_Click(sender As Object, e As EventArgs) Handles ButtonClosed.Click
        Dim customClass As New Parameter.Implementasi
        lblMessage.Text = ""

        With customClass
            .strConnection = GetConnectionString()
            .Month = cboMonth.SelectedValue.Trim
            .Year = txtTahun.Text.Trim
        End With
        Try
            ClosedPosting(customClass)
            ShowMessage(lblMessage, "Jurnal Sudah Di Closed", False)
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
    Public Sub ClosedPosting(ByVal customclass As Parameter.Implementasi)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@CurrentMonth", SqlDbType.Int)
            params(0).Value = customclass.Month
            params(1) = New SqlParameter("@CurrentYear", SqlDbType.Int)
            params(1).Value = customclass.Year

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGenerateGLPeriodClose", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

End Class