﻿#Region "imports"
Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region

Public Class BalanceSheetSetupList
    Inherits WebBased
    Private Const PageSize As Integer = 10
    Protected WithEvents GridNavigator As ucGridNav
    Private _recordCount As Integer = 1
    Private ReadOnly _bsController As New BalanceSheetSetupController
    Private _branchid As String
    Private _companyid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = "BSSLIST"
        If SessionInvalid() Then
            Exit Sub
        End If
        _branchid = sesBranchId.Replace("'", "")
        BranchID = _branchid
        If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Return
        End If
        _companyid = GetCompanyId(GetConnectionString)
        SesCompanyID = _companyid
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not Page.IsPostBack Then

            InitGrid()

        End If
    End Sub
    Private Function _dt(ByVal ParamArray param() As Object) As IList(Of BalanceSheetSetup)
        Return _bsController.SelectData(_recordCount, GetConnectionString(), param)
    End Function

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        If (hdIsFind.Value = "1") Then
            BindGridFind(e.CurrentPage)
        Else
            DtgAsset.DataSource = _dt(New Object() {_companyid, e.CurrentPage, PageSize}) '_bsController.SelectData(_recordCount, GetConnectionString(),  New Object() {_companyid, e.CurrentPage, PageSize})
            DtgAsset.DataBind()
        End If

        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Private Sub InitGrid()
        DtgAsset.DataSource = _dt(New Object() {_companyid, 1, PageSize}) '_bsController.SelectData(_recordCount, GetConnectionString(),  New Object() {_companyid,  1, PageSize})
        DtgAsset.DataBind()
        GridNavigator.Initialize(_recordCount, PageSize)
    End Sub

    Sub BindGridFind(currPage As Integer)
        Dim opW = ddlCari.SelectedValue
        Dim sWhare = txtCari.Text.Trim

        'Dim _dt As IList(Of MasterAccountObject) = _bsController.SelectData(_recordCount, GetConnectionString(),  New Object() {_companyid, currPage, PageSize, opW, sWhare})
        DtgAsset.DataSource = _dt(New Object() {_companyid, currPage, PageSize, opW, sWhare})
        DtgAsset.DataBind()
    End Sub


    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        hdIsFind.Value = "1"
        GridNavigator.Initialize(_recordCount, PageSize)

    End Sub


#Region "CommandGrid"

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        If e.CommandName.ToLower = "edit" Then
            If e.CommandArgument.ToString <> String.Empty Then
                Response.Redirect("BalanceSheetSetupDetail.aspx?rid=" & e.CommandArgument.ToString)
            End If
        ElseIf e.CommandName.ToLower = "delete" Then
            deleteData(e.CommandArgument.ToString)
        ElseIf e.CommandName.ToLower = "detail" Then
            If e.CommandArgument.ToString <> String.Empty Then
                Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & e.CommandArgument.ToString)
            End If
        End If
    End Sub
#End Region

    Private Sub deleteData(ByVal rid As String)
        Try
            _bsController.DeleteData(GetConnectionString, rid)
            Response.Redirect("BalanceSheetSetup.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Response.Redirect("BalanceSheetSetupDetail.aspx")
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("BalanceSheetSetup.aspx")
    End Sub

    Private Sub DtgAsset_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        Dim btnEdit As ImageButton
        Dim btnDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then
            btnEdit = CType(e.Item.FindControl("btnEdit"), ImageButton)
            btnDelete = CType(e.Item.FindControl("btnDelete"), ImageButton)
            btnDelete.Visible = True
            btnEdit.Visible = True
        End If
    End Sub
End Class