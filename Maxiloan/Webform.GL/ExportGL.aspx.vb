﻿Option Strict Off

#Region "imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.IO
Imports System.Data.Odbc
Imports System.Data.OleDb
Imports System.Data
Imports Microsoft.Office.Interop

#End Region

Public Class ExportGL
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPeriode1 As ucDateCE
    Protected WithEvents txtPeriode2 As ucDateCE
    Private C_Journal As New Controller.JournalVoucherController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If CheckForm(Me.Loginid, "EXPORTGL", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            txtPeriode1.IsRequired = True
            txtPeriode2.IsRequired = True
        End If
    End Sub

    Private Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.Click
        Try
            Dim str As String = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML"
            'Dim p_Class As New Parameter.JournalVoucher_header
            Dim data As New DataTable
            'With p_Class
            '    .strConnection = GetConnectionString()
            '    .Periode1 = ConvertDate2(txtPeriode1.Text)
            '    .Periode2 = ConvertDate2(txtPeriode2.Text)
            '    .BranchId = Me.sesBranchId.Trim.Replace("'", "")
            'End With
            data = C_Journal.DownloadToDBF(GetConnectionString(), ConvertDate2(txtPeriode1.Text), ConvertDate2(txtPeriode2.Text), sesBranchId.Trim.Replace("'", ""))

            Dim cn As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & str & ";Extended Properties='dBASE III';")
            cn.Open()

            If File.Exists(str & "\" & "GELWRK.DBF") Then
                Dim cmd_Delete_GELWRK As New OleDbCommand("Drop Table GELWRK", cn)
                cmd_Delete_GELWRK.ExecuteNonQuery()
            End If

            Dim cmd_Create_GELWRK As New OleDbCommand("Create Table GELWRK(WRCID CHAR(1)," _
                                                      & "WDOTP CHAR(5)," _
                                                      & "WSQNO FLOAT," _
                                                      & "WRCSQ FLOAT," _
                                                      & "WPSDA FLOAT," _
                                                      & "WTRDA DATE," _
                                                      & "WDESC CHAR(50)," _
                                                      & "WDRAC FLOAT," _
                                                      & "WDRCO CHAR(3)," _
                                                      & "WCRAC FLOAT," _
                                                      & "WCRCO CHAR(3)," _
                                                      & "WCUAM FLOAT," _
                                                      & "WRPAM FLOAT," _
                                                      & "WDUDA DATE," _
                                                      & "TGLINP DATE," _
                                                      & "USERID CHAR(3)," _
                                                      & "FLCTDP FLOAT," _
                                                      & "UPDTKE FLOAT," _
                                                      & "NOMBAT FLOAT)", cn)
            cmd_Create_GELWRK.ExecuteNonQuery()

            If data.Rows.Count > 0 Then
                For index = 0 To data.Rows.Count - 1
                    Dim cmd_Insert_GELWRK As New OleDbCommand("INSERT INTO GELWRK(WRCID,WDOTP,WSQNO,WRCSQ,WPSDA,WTRDA,WDESC,WDRAC,WDRCO,WCRAC,WCRCO,WCUAM,WRPAM,WDUDA,TGLINP,USERID,FLCTDP,UPDTKE,NOMBAT)" _
                                                              & " VALUES('" & data.Rows(index).Item("WRCID").ToString & "','" _
                                                              & data.Rows(index).Item("WDOTP").ToString & "','" _
                                                              & data.Rows(index).Item("WSQNO").ToString & "','" _
                                                              & data.Rows(index).Item("WRCSQ").ToString & "','" _
                                                              & data.Rows(index).Item("WPSDA").ToString & "','" _
                                                              & data.Rows(index).Item("WTRDA").ToString & "','" _
                                                              & data.Rows(index).Item("WDESC").ToString & "','" _
                                                              & data.Rows(index).Item("WDRAC").ToString & "','" _
                                                              & data.Rows(index).Item("WDRCO").ToString & "','" _
                                                              & data.Rows(index).Item("WCRAC").ToString & "','" _
                                                              & data.Rows(index).Item("WCRCO").ToString & "','" _
                                                              & data.Rows(index).Item("WCUAM").ToString & "','" _
                                                              & data.Rows(index).Item("WRPAM").ToString & "','" _
                                                              & data.Rows(index).Item("WDUDA").ToString & "','" _
                                                              & data.Rows(index).Item("TGLINP").ToString & "','" _
                                                              & data.Rows(index).Item("USERID").ToString & "','" _
                                                              & data.Rows(index).Item("FLCTDP").ToString & "','" _
                                                              & data.Rows(index).Item("UPDTKE").ToString & "','" _
                                                              & data.Rows(index).Item("NOMBAT").ToString & "')", cn)
                    cmd_Insert_GELWRK.ExecuteNonQuery()
                Next

            End If

            cn.Close()

            Response.ContentType = "application/dbf"
            Response.AddHeader("content-disposition", "attachment; filename=GELWRK.DBF")
            Response.WriteFile(Server.MapPath("../XML/GELWRK.DBF"))
            Response.End()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub


End Class