﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AmortisasiBiaya
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AmortisasiJurnal
    Private oController As New AgreementListController
    Private m_controller As New AmortisasiJurnalController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "AmorBiaya"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If
                Me.SearchBy = ""
                Me.SortBy = ""

                'oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID,No Aplikasi-Agreementno, No Kontrak" 
            End If
        End If
        pnlAdd.Visible = False
        PnlEdit.Visible = False
        BindGridEntity("")

    End Sub
#End Region
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AmortisasiJurnal
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'InitialDefaultPanel()


        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = ""
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetDataAmortisasiBiaya(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = CType(oCustomClass.TotalRecords, Integer)
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = dtEntity.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        'PagingFooter()

        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        pnlAdd.Visible = False
        PnlEdit.Visible = False

        lblMessage.Text = ""
    End Sub

#End Region


    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTr_Nomor As Label
        Dim lblNilai As Label
        Dim NHypRequest As HyperLink
        Dim lblDEBET As Label
        Dim lblCREDIT As Label
        ' Me.FormID = "SUSPENDALLOCATION"
        'HyReverse()
        If e.Item.ItemIndex >= 0 Then
            lblTr_Nomor = CType(e.Item.FindControl("lblTr_Nomor"), Label)
            lblNilai = CType(e.Item.FindControl("lblNilai"), Label)
            lblDEBET = CType(e.Item.FindControl("lblDEBET"), Label)
            lblCREDIT = CType(e.Item.FindControl("lblCREDIT"), Label)
            ' If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            NHypRequest = CType(e.Item.FindControl("HypRequest"), HyperLink)
            NHypRequest.NavigateUrl = "AmortisasiBiayaList.aspx?Tr_Nomor=" & lblTr_Nomor.Text.Trim & "' and gjdc.Amount='" & lblNilai.Text.Trim
            'NlblTransferRefNO.NavigateUrl = "javascript:OpenWinMain('" & NlblTransferRefNO.Text.Trim & "','" & Nbranchid.Text.Trim & "')"
            'End If

        End If
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim BtnDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            BtnDelete = CType(e.Item.FindControl("BtnDelete"), ImageButton)
            'BtnDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    '#Region "databound"
    '    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
    '        Dim oParameter As New Parameter.AmortisasiJurnal
    '        Dim dtEntity As New DataTable
    '        Dim err As String
    '        Try
    '            If e.CommandName = "Edit" Then
    '                pnlAdd.Visible = False
    '                pnlList.Visible = False
    '                pnlDatagrid.Visible = False
    '                PnlEdit.Visible = True

    '                oParameter.ID = DtgAgree.DataKeys.Item(e.Item.ItemIndex).ToString
    '                oParameter.strConnection = GetConnectionString()
    '                oParameter = m_controller.GetDataEdit(oParameter)

    '                If Not oParameter Is Nothing Then
    '                    dtEntity = oParameter.ListData
    '                End If


    '                'If dtEntity.Rows.Count > 0 Then
    '                '    oRow = dtEntity.Rows(0)
    '                '    cboPEducation.SelectedIndex = cboPEducation.Items.IndexOf(cboPEducation.Items.FindByValue(oRow("Gelar").ToString.Trim))
    '                '    cboKodeHubunganLJK.SelectedIndex = cboKodeHubunganLJK.Items.IndexOf(cboKodeHubunganLJK.Items.FindByValue(oRow("KodeHubunganLJK").ToString.Trim))
    '                '    cboKodePekerjaan.SelectedIndex = cboKodePekerjaan.Items.IndexOf(cboKodePekerjaan.Items.FindByValue(oRow("KodePekerjaan").ToString.Trim))
    '                '    cboJenisKelamin.SelectedIndex = cboJenisKelamin.Items.IndexOf(cboJenisKelamin.Items.FindByValue(oRow("JenisKelamin").ToString.Trim))

    '                '    If txtTanggalLahir.Text = "1900/01/01" Then
    '                '        txtTanggalLahir.Text = ""
    '                '    ElseIf txtTanggalLahirPasangan.Text = "1900/01/01" Then
    '                '        txtTanggalLahirPasangan.Text = ""
    '                '    End If

    '                '    txtTanggalLahir.Text = Format(oRow("TanggalLahir"), "yyyy/MM/dd").ToString
    '                '    txtTanggalLahirPasangan.Text = Format(oRow("TanggalLahirPasangan"), "yyyy/MM/dd").ToString
    '                'End If

    '                'txtbulandata.Text = oParameter.ListData.Rows(0)("BulanData")
    '                'txtid.Text = oParameter.ListData.Rows(0)("ID")
    '                'cboJenisIdentitas.SelectedValue = oParameter.ListData.Rows(0)("JenisIdentitas")
    '                'txtFlag.Text = oParameter.ListData.Rows(0)("Flag")
    '                'txtCIF.Text = oParameter.ListData.Rows(0)("CIF")
    '                'txtNIK.Text = oParameter.ListData.Rows(0)("NIK")
    '                'txtNamaIdentitas.Text = oParameter.ListData.Rows(0)("NamaIdentitas")
    '                'txtNamaLengkap.Text = oParameter.ListData.Rows(0)("NamaLengkap")
    '                'txtTempatLahir.Text = oParameter.ListData.Rows(0)("TempatLahir")
    '                'txtNPWP.Text = oParameter.ListData.Rows(0)("NPWP")
    '                'txtAlamat.Text = oParameter.ListData.Rows(0)("Alamat") 

    '                'Dim param As String
    '                'param = "datiID LIKE '%" + txtKota.Text + "%'"
    '                'oKodeKota.Sort = "DatiID ASC"
    '                'oKodeKota.Setback(param)

    '            ElseIf e.CommandName = "DEL" Then
    '                Dim oCustomClass As New Parameter.AmortisasiJurnal
    '                With oCustomClass
    '                    .ID = DtgAgree.DataKeys.Item(e.Item.ItemIndex).ToString
    '                    .strConnection = GetConnectionString()
    '                End With

    '                err = m_controller.GetDataDelete(oCustomClass)
    '                If err <> "" Then
    '                    ShowMessage(lblMessage, err, True)
    '                Else
    '                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
    '                End If
    '                BindGridEntity(Me.SearchBy, Me.SortBy)
    '            End If
    '        Catch ex As Exception
    '            ShowMessage(lblMessage, ex.Message, True)
    '        End Try
    '    End Sub
    '#End Region

#Region "Reset"

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("AmortisasiBiaya.aspx")
    End Sub
#End Region



#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.SearchBy = ""
        'If oSearchBy.Text.Trim <> "" Then
        If cbosearch.SelectedIndex <> 0 Or txtsearch.Text <> "" Then
            'Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
            Me.SearchBy = cbosearch.SelectedItem.Value & " like '%" & txtsearch.Text.Trim.Replace("'", "''") & "%'"
        Else
            Me.SearchBy = ""
        End If
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        BindGridEntity(Me.SearchBy)
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

End Class