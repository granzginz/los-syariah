﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewAmortisasiBiaya
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AmortisasiJurnal
    Private m_controller As New AmortisasiJurnalController
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Private Const SCHEME_ID As String = "AMBY"
#End Region
#Region "Property"
    Private Property Tr_Nomor() As String
        Get
            Return (CType(ViewState("Tr_Nomor"), String))
        End Get
        Set(value As String)
            ViewState("Tr_Nomor") = value
        End Set
    End Property
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property Nilai() As Double
        Get
            Return (CType(ViewState("Nilai"), Double))
        End Get
        Set(value As Double)
            ViewState("Nilai") = value
        End Set
    End Property
    Private Property ReffNo() As String
        Get
            Return (CType(ViewState("ReffNo"), String))
        End Get
        Set(value As String)
            ViewState("ReffNo") = value
        End Set
    End Property
    Private Property Tenor() As Int32
        Get
            Return (CType(ViewState("Tenor"), Int32))
        End Get
        Set(value As Int32)
            ViewState("Tenor") = value
        End Set
    End Property
#End Region


#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rCusomClass As New Parameter.AmortisasiJurnal
        Dim appid As String = ""

        If SessionInvalid() Then
            Exit Sub
        End If
        appid = Request("AbNo").ToString
        Me.Tr_Nomor = Request.QueryString("AbNo")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.Tr_Nomor = Me.Tr_Nomor


        oCustomClass.WhereCond = Me.Tr_Nomor
        BindGridEntity(Me.Tr_Nomor)

    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetDataSchemeAmortisasiBiaya(oCustomClass)
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AmortisasiJurnal
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'InitialDefaultPanel()


        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetViewSchemeAmortisasiBiaya(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = CType(oCustomClass.TotalRecords, Integer)
        Else
            recordCount = 0
        End If

        lblTr_Nomor.Text = oCustomClass.ListData.Rows(0)("Tr_Nomor")
        txtTglMulai.Text = oCustomClass.ListData.Rows(0)("StartDate")
        txtNilai.Text = oCustomClass.ListData.Rows(0)("Amount")
        txtNoReference.Text = oCustomClass.ListData.Rows(0)("ReffNo")
        txtTglAkhir.Text = oCustomClass.ListData.Rows(0)("EndDate")
        txtTglEfektif.Text = oCustomClass.ListData.Rows(0)("EffectiveDate")
        lblNilaiAmortisasi.Text = oCustomClass.ListData.Rows(0)("AmortizeAmount")
        lblTglJangkaWaktu.Text = oCustomClass.ListData.Rows(0)("Tenor")
        Me.Tenor = oCustomClass.ListData.Rows(0)("Tenor")
        Me.BranchID = Me.sesBranchId.Trim


        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        pnlDatagrid.Visible = True
        BuatTabelInstallment()
        lblMessage.Text = ""
    End Sub

#End Region

#Region "tabel amortisasi"
    Public Sub BuatTable()
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Date")
        myDataColumn.ColumnName = "Tanggal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.int64")
        myDataColumn.ColumnName = "Nilai"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "Debet"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "DebetDesc"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "Credit"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "CreditDesc"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "Tr_Nomor"
        'myDataTable.Columns.Add(myDataColumn)

    End Sub
    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer
        Dim dtEntity As DataTable = Nothing

        intRowMax = CInt(Me.Tenor)

        oCustomClass = m_controller.GetViewSchemeAmortisasiBiaya(oCustomClass)
        dtEntity = oCustomClass.ListData

        myDataTable = New DataTable("Installment")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.DateTime")
        myDataColumn.ColumnName = "Tanggal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Debet"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "DebetDesc"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Credit"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "CreditDesc"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 1 To intRowMax
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("Tanggal") = oCustomClass.ListData.Rows(0)("DateAmortize")
            myDataRow("Amount") = oCustomClass.ListData.Rows(0)("AmortizeAmount")
            myDataRow("Debet") = oCustomClass.ListData.Rows(0)("DebitAmortizeAmount")
            myDataRow("DebetDesc") = oCustomClass.ListData.Rows(0)("DebitDesc")
            myDataRow("Credit") = oCustomClass.ListData.Rows(0)("CreditAmortizeAmount")
            myDataRow("CreditDesc") = oCustomClass.ListData.Rows(0)("CreditDesc")
            myDataTable.Rows.Add(myDataRow)
        Next inc

        DtgAgree.DataSource = myDataTable.DefaultView
        DtgAgree.DataBind()

    End Sub
#End Region



#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
 

End Class