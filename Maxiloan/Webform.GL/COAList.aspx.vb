﻿#Region "imports" 
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient

#End Region

Public Class COAList
    Inherits WebBased
    Private Const PageSize As Integer = 10
    Protected WithEvents GridNavigator As ucGridNav
    Private _recordCount As Integer = 1
    Private ReadOnly _coaController As New COAController
    Private _branchid As String
    Private _companyid As String
    Private m_controller As New DataUserControlController
    Private oClass As New Parameter.ControlsRS
    Private Property CmdWhere() As String
        Get
            Return (CType(ViewState("CmdWhere"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property

    Private Property Cabang() As String
        Get
            Return (CType(ViewState("BranchId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchId") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = "COALIST"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If

        If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Return
        End If
        _companyid = GetCompanyId(GetConnectionString)
        SesCompanyID = _companyid
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not Page.IsPostBack Then

            'BindDdlBranch(ddlBranch, GetConnectionString, sesBranchId, IsHoBranch)
            If IsHoBranch = True Then
                With ddlBranch
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "ALL")
                    .Items(0).Value = "ALL"
                End With
            Else
                With ddlBranch
                    .DataSource = m_controller.GetBranchName(GetConnectionString, sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                End With
            End If

            If ddlBranch.SelectedValue = "ALL" Then
                _branchid = "ALL"
                Me.Cabang = _branchid
            Else
                _branchid = ddlBranch.SelectedValue 'sesBranchId.Replace("'", "")
                Me.Cabang = _branchid
            End If


            With ddlAccountType
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataSource = ModuleGlHelper.GetEnumMasterAccTypeList()
                .DataBind()
            End With

            InitGrid()
        End If
    End Sub
    Private Function _dt(ByVal ParamArray param() As Object) As IList(Of MasterAccountObject)
        Return _coaController.SelectData(_recordCount, GetConnectionString(), False, False, param)
    End Function


    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs) 

        If (hdIsFind.Value = "1") Then
            BindGridFind(e.CurrentPage)
        Else
            DtgAsset.DataSource = _dt(New Object() {_companyid, Me.Cabang, e.CurrentPage, PageSize}) '_coaController.SelectData(_recordCount, GetConnectionString(), False, False, New Object() {_companyid, _branchid, e.CurrentPage, PageSize})
            DtgAsset.DataBind()
        End If

        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Private Sub InitGrid()
        DtgAsset.DataSource = _dt(New Object() {_companyid, Me.Cabang, 1, PageSize}) '_coaController.SelectData(_recordCount, GetConnectionString(), False, False, New Object() {_companyid, _branchid, 1, PageSize})
        DtgAsset.DataBind()
        GridNavigator.Initialize(_recordCount, PageSize)
    End Sub

    Sub BindGridFind(currPage As Integer)
        Dim acctype = ddlAccountType.SelectedValue
        Dim opW = ddlCari.SelectedValue
        Dim sWhare = txtCari.Text.Trim


        If ddlBranch.SelectedValue = "ALL" Then
            _branchid = "ALL"
            Me.Cabang = _branchid
        Else
            _branchid = ddlBranch.SelectedValue
            Me.Cabang = _branchid
        End If

        ' Dim _dt As IList(Of MasterAccountObject) = _coaController.SelectData(_recordCount, GetConnectionString(), False, False, New Object() {_companyid, branch, currPage, PageSize, acctype, opW, sWhare})
        DtgAsset.DataSource = _dt(New Object() {_companyid, Me.Cabang, currPage, PageSize, acctype, opW, sWhare})
        DtgAsset.DataBind()
    End Sub


    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        hdIsFind.Value = "1"
        GridNavigator.Initialize(_recordCount, PageSize)

    End Sub


#Region "CommandGrid"


    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        If e.CommandName.ToLower = "edit" Then
            If e.CommandArgument.ToString <> String.Empty Then
                Response.Redirect("COADetail.aspx?acc=" & e.CommandArgument.ToString)
            End If
        ElseIf e.CommandName.ToLower = "delete" Then
            Dim d() As String = e.CommandArgument.ToString.Trim.Split(";")
            deleteData(d(0), d(1))
        End If
    End Sub
#End Region

    Private Sub deleteData(ByVal accountNo As String, ByVal addfree As String)
        Try
            _coaController.DeleteData(GetConnectionString, accountNo, addfree)
            Response.Redirect("COAList.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Response.Redirect("COAdetail.aspx")
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("COAList.aspx")
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click


        Me.CmdWhere = ""

        With oClass
            .AreaID = "001"
            .SelectedBranch = ""

            If (ddlBranch.SelectedValue.Trim <> "ALL") Then
                Me.CmdWhere = Me.CmdWhere & " And GLMasterAcc.BranchId = '" & ddlBranch.SelectedValue.Trim & "' "
            End If
            If ddlCari.SelectedValue = 1 Then
                Me.CmdWhere = Me.CmdWhere & " And  CoaId LIKE '%" & txtCari.Text & "%' "
            ElseIf ddlCari.SelectedValue = 2 Then
                Me.CmdWhere = Me.CmdWhere & " And  Description LIKE '%" & txtCari.Text & "%' "
            End If

            .CmdWhere = Me.CmdWhere

        End With

        Response.AppendCookie(Webform.UserController.RSConfig.setCookies(Request.Cookies("RSCOOKIES"), oClass))

        Dim strReportFile As String = "../Webform.Reports/ucRSViewerPopup.aspx?formid=DAFTRLAPCOA&rsname=" & "/BataviaReport/GeneralLedger/ReportCOA"

        Response.Write("<script language = javascript>" & vbCrLf _
                        & "var x = screen.width; " & vbCrLf _
                        & "var y = screen.height; " & vbCrLf _
                           & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                           & "</script>")


    End Sub

    Public Function AccountTypeToValue(ByVal accType As Integer) As String
        Select Case accType
            Case 1
                Return "Cash/Bank"
            Case 2
                Return "Account Receivable"
            Case Else
                Return String.Empty
        End Select
    End Function

    Public Function SubAccountToValue(ByVal isSubAccount As Boolean) As String
        Select Case isSubAccount
            Case True
                Return "Detail"
            Case False
                Return "Parent"
            Case Else
                Return String.Empty
        End Select
    End Function

    Private Sub DtgAsset_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        'Dim lblBranchId As Label
        'Dim btnEdit As ImageButton
        'Dim btnDelete As ImageButton

        'If e.Item.ItemIndex >= 0 Then
        '    lblBranchId = CType(e.Item.FindControl("lblBranchId"), Label)
        '    btnEdit = CType(e.Item.FindControl("btnEdit"), ImageButton)
        '    btnDelete = CType(e.Item.FindControl("btnDelete"), ImageButton)
        '    Dim isVisible As Boolean = (lblBranchId.Text = "900" And sesBranchId.Replace("'", "") = "900") Or (lblBranchId.Text <> "900" And sesBranchId.Replace("'", "") <> "900")
        '    btnDelete.Visible = isVisible
        '    btnEdit.Visible = isVisible
        'End If
    End Sub
End Class