﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class JurnalRecalculatePosting
    Inherits Maxiloan.Webform.AccMntWebBased
    Private oCustomClass As New Parameter.Implementasi
    Private oController As New Controller.ImplementasiControler
    Private m_controller As New DataUserControlController

    Private Property PeriodStatus() As Int32
        Get
            Return CStr(ViewState("PeriodStatus"))
        End Get
        Set(ByVal Value As Int32)
            ViewState("PeriodStatus") = Value
        End Set
    End Property

    Private Property PeriodEverClosed() As Int32
        Get
            Return CStr(ViewState("PeriodEverClosed"))
        End Get
        Set(ByVal Value As Int32)
            ViewState("PeriodEverClosed") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Me.FormID = "RECALCULATEPOSTINGH"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            'display(ConvertDate2(txtDateFrom.Text.Trim).Year, ConvertDate2(txtDateFrom.Text.Trim).Month)
            bindSearchBranch()
        End If
    End Sub
    Private Sub bindSearchBranch()
        With ddlCabang
            .DataSource = m_controller.GetBranchAll(GetConnectionString)
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "ALL"
        End With
    End Sub
    Protected Sub btnButtonPosting_Click(sender As Object, e As EventArgs) Handles ButtonPosting.Click
        Dim customClass As New Parameter.Implementasi
        lblMessage.Text = ""


        Dim ypCont = New GlYearPeriodController()

        Dim prd = ypCont.YearPeriods(GetConnectionString, SesCompanyID, sesBranchId.Replace("'", ""), ConvertDate2(txtDateFrom.Text).Year.ToString, ConvertDate2(txtDateFrom.Text).Month.ToString)
        If (prd.Count = 0) Then
            ShowMessage(lblMessage, "Tanggal Periode transaksi telah close / belum di open.", True)
            Exit Sub
        End If


        With customClass
            .strConnection = GetConnectionString()
            .BranchId = ddlCabang.SelectedValue.Trim
            .StartDate = ConvertDate2(txtDateFrom.Text.Trim)
            .EndDate = ConvertDate2(txtDateTo.Text.Trim)
        End With
        PostingHarian(customClass)

        ShowMessage(lblMessage, "Data Sudah Di Recalculate", False)

    End Sub

    Public Sub PostingHarian(ByVal customclass As Parameter.Implementasi)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@StartDate", SqlDbType.DateTime)
            params(0).Value = customclass.StartDate
            params(1) = New SqlParameter("@EndDate", SqlDbType.DateTime)
            params(1).Value = customclass.EndDate
            params(2) = New SqlParameter("@Cabang", SqlDbType.VarChar, 10)
            params(2).Value = customclass.BranchId

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "sp_GlRecalculate", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()

            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub display(ByVal Tahun As Int32, ByVal Month As Int32)
        Try
            With oCustomClass
                .CompanyID = "001"
                .Year = Tahun
                .Month = Month
                .strConnection = GetConnectionString()
            End With

            oCustomClass = oController.GetPeriodJournal(oCustomClass)

            If oCustomClass Is Nothing Then
                ShowMessage(lblMessage, "No Record Found..", True)
                Exit Sub
            End If

            With oCustomClass
                Me.PeriodStatus = .PeriodStatus
                Me.PeriodEverClosed = .PeriodEverClosed
            End With


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub
End Class