﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class JournalVoucherApproval
    Inherits WebBased
    Private ReadOnly _jvController As New JournalVoucherController
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected WithEvents GridNavigator As ucGridNav

    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "JVAPRV"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If
        lblMessage.Visible = False
        divmessages.Visible = False
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not IsPostBack Then
            txtPeriodeFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            BindDdlBranch()
            ViewState("OrderId") = ""
            SesCompanyID = GetCompanyId(GetConnectionString)
            pnlGrid.Visible = False
            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses Approve Journal Tgl." + Request("dt"), False)
            End If
        End If
    End Sub

    Sub bindDdlBranch()
        With ddlCabang
            .DataSource = New DataUserControlController().GetBranchAll(GetConnectionString)
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("JournalVoucherApproval.aspx")
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("JournalVoucherApproval.aspx")
    End Sub

    Private debTotal As Decimal = 0
    Private creTotal As Decimal = 0

    Private Sub getBrokenRules(j As JournalApproval)
        If j.GetBrokenRules().Count > 0 Then
            divAppr.Visible = False

            divmessages.Visible = True

            Dim build = New StringBuilder()
            build.Append("<strong><u>Keterangan Masalah </u></strong><br />")
            For Each brule In j.GetBrokenRules()
                build.Append(brule.RuleDescription)
                build.Append(" <br />")
            Next


            If (j.IsBalance = False) Then
                build.Append("<br/><strong>Transaksi Yang Tidak Balance</strong><br />")
                For Each imbalance In j.ImbalanceTrNo
                    build.Append(String.Format(" {0,-20} selisih {1} <br/>", imbalance.Value, imbalance.Text))
                Next
            End If


            lblErrMsg.Text = build.ToString()
        End If
    End Sub

    Property _fDate As DateTime
        Set(ByVal value As DateTime)
            ViewState("_fDate") = value
        End Set
        Get
            Return CType(ViewState("_fDate"), DateTime)
        End Get
    End Property
    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Try
            _fDate = ConvertDate2(txtPeriodeFrom.Text.Trim)
            Dim result = _jvController.GlJournalApprovalPage(GetConnectionString(), New JournalApproval With {.CompanyId = SesCompanyID, .BranchId = ddlCabang.SelectedValue, .TransactionDate = _fDate},
                                                                PageSize, 1, _recordCount)
            If (result.Count <= 0) Then
                ShowMessage(lblMessage, "Tidak Ada Transaksi", False)
                pnlGrid.Visible = False
                pnlFind.Visible = True
                Return
            End If

            ViewState("SOURCE") = result
            pnlGrid.Visible = True
            dtgCSV.DataSource = result
            dtgCSV.DataBind()
            GridNavigator.Initialize(_recordCount, PageSize)
            pnlFind.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    'ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq
    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        Dim result = _jvController.GlJournalApprovalPage(GetConnectionString(), New JournalApproval With {.CompanyId = SesCompanyID, .BranchId = ddlCabang.SelectedValue, .TransactionDate = _fDate},
                                                                PageSize, e.CurrentPage, _recordCount)
        ViewState("SOURCE") = result
        dtgCSV.DataSource = result
        dtgCSV.DataBind()
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub
    Private Sub doApprove(ByVal sender As Object, ByVal e As EventArgs) Handles btnApprove.Click

        Dim checkedIDs = (From msgRow As GridViewRow In dtgCSV.Rows
                 Where (CType(msgRow.FindControl("chkUpdateStatus"), CheckBox).Checked)
                 Select dtgCSV.DataKeys(msgRow.RowIndex).Value).ToList()

        If (checkedIDs.Count <= 0) Then
            'ShowMessage(lblMessage, "Silahkan Pilih Journal yang akan di Apporve", False)
            Return
        End If

        Try
            Dim result = _jvController.GlJournalApproval(GetConnectionString, checkedIDs.Cast(Of String)().ToList())
            btnFind_Click(Nothing, Nothing)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub OrderGridView_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

    End Sub

    Protected Sub grdOrders_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dtgCSV.RowCommand

        If (e.CommandName = "Expand") Then

            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString())
            Dim imgbtn As ImageButton = CType(gv.Rows(rowIndex).FindControl("ImgBtn"), ImageButton)


            Dim key = gv.DataKeys(rowIndex)(0).ToString()
            ViewState("OrderId") = key
            Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails"), PlaceHolder)


            If (imgbtn.ImageUrl.Trim().ToUpper() = "IMAGES/PLUS.GIF") Then
                objPH.Visible = True
                imgbtn.ImageUrl = "images/Minus.gif"
                'End If
            Else
                imgbtn.ImageUrl = "images/Plus.gif"
                objPH.Visible = False
            End If

        End If
    End Sub

    Private Sub DtgAsset_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dtgCSV.RowDataBound
        Dim imgBtn As ImageButton
        Dim data = CType(ViewState("SOURCE"), IList(Of JournalApproval))
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            imgBtn = CType(e.Row.FindControl("ImgBtn"), ImageButton)
            imgBtn.CommandArgument = e.Row.RowIndex.ToString()


            Dim txtid = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionNo"))
            Dim objChildGrid As GridView = CType(e.Row.FindControl("grdDetails"), GridView)
            objChildGrid.DataSource = data.SingleOrDefault(Function(x) x.TransactionNo = txtid).Items
            objChildGrid.DataBind()
            objChildGrid.Visible = True
            Dim objPH As PlaceHolder = CType(e.Row.FindControl("objPHOrderDetails"), PlaceHolder)
            objPH.Visible = False


            imgBtn.ImageUrl = "Images/Plus.gif"
            If (txtid = ViewState("OrderId")) Then
                objPH.Visible = True
                imgBtn.ImageUrl = "Images/Minus.gif"
            End If

            Dim check = CType(e.Row.FindControl("chkUpdateStatus"), CheckBox)
            If (Not Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "CanApprove"))) Then
                check.Enabled = False
                check.ToolTip = "Journal Tidak Balance atau COA tidak ditemukan"
            End If
        End If

    End Sub


End Class