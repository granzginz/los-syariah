﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="JournalDailyPost.aspx.vb" Inherits="Maxiloan.Webform.GL.JournalDailyPost" %>
 <%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc" %>
 <%@ Register TagPrefix="uc2" TagName="ucGridNav" Src="../webform.UserController/ucGridNav.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"/>
<%--
      <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                 POST HARIAN JURNAL VOUCHER
            </h3>
        </div>
    </div>

   <div id="pnlmain" runat="server">
   
    
     <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">

                     <asp:GridView  ID="dtgFilterJournal" runat="server" 
                      CssClass="grid_general"
                      BorderStyle="None" BorderWidth="0"
                      AutoGenerateColumns="False"    
                      AllowSorting="False" Width="100%"   DataKeyNames="PostDate"  >  
                                    <HeaderStyle CssClass="th" /> 
                                    <FooterStyle CssClass="item_grid" />
                                    <Columns>
                                         <asp:TemplateField HeaderText="Post" itemStyle-Width="120" >
                                         <ItemTemplate>
                                             <asp:LinkButton ID="btnPost" runat="server"  Text='Post' CommandName="Post"  
                                             OnClientClick="return confirm('Post Journal pada tanggal tersebut?');"
                                             CausesValidation="False" CommandArgument='<%# eval("PostDate") %>' ><span>Post</span></asp:LinkButton>
                                         </ItemTemplate>
                                         </asp:TemplateField> 
                                        <asp:BoundField  Visible="True" itemStyle-Width="100" DataField="PostDate" HeaderText="Tanggal"  DataFormatString= "{0:dd/M/yyyy}"/> 
                                        <asp:BoundField  Visible="True" DataField="Text" HeaderText="Keterangan" />
                                        <asp:BoundField  Visible="True" itemStyle-Width="100"  DataField="Balance" HeaderText="Selisih" DataFormatString= "{0:#,0}"/> 
                                        <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblIsBalance"  Text='<%# eval("IsBalance") %>' /> 
                                            <asp:Label runat="server" ID="lblIsPeriodValid"  Text='<%# eval("IsPeriodValid") %>' /> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                    </asp:GridView >
                     <uc2:ucGridNav id="GridNavigator" runat="server"/>
                 </div>
            </div>
        </div>
    --%>
 
 <div id="divPostNormal" runat="server">
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                 POST HARIAN JURNAL VOUCHER
            </h3>
        </div>
    </div>
    

    <div class="form_box">
        <div class="form_single">
            <label> Prev Post</label>
            <asp:label id="lblPrev" runat="server"/> 
        </div>
    </div> 
    <div class="form_box">
        <div class="form_single">
            <label> Current Post</label>
            <asp:label id="lblCurr" runat="server"/> 
        </div>
    </div> 

    <div class="form_box">
        <div class="form_single">
            <label> Next Post</label>
            <asp:label id="lblNext" runat="server"/> 
        </div>
    </div>    
</div>
    
 <div id="divPostSusulan" runat="server" visible="false">
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                 POST HARIAN JURNAL VOUCHER SUSUALAN
            </h3>
        </div>
    </div>
    

    <div class="form_box">
        <div class="form_single">
            <label>Masukan Tanggal Journal</label>
             <uc:ucdatece runat="server" id="txtTanggalVoucher" />
        </div>
    </div> 
    
        <div class="form_button" > 
        <asp:Button ID="btnGetVoucher" runat="server"  CausesValidation="False" Text="FIND" CssClass="small button blue"/> 
     </div>
</div> 
    
    

  <div id="divInfoJournal" runat="server" visible="false"  class="form_box">
    <div class="form_single">
        <table cellspacing="1" cellpadding="2" width="80%" align="left" border="0">
        <tbody>
            <tr class="th"> 
                <th scope="col" >Tanggal</th>
                <th scope="col" width="500px">Keterangan</th>
                <th scope="col">Debet</th>
                <th scope="col">Credit</th>
             </tr> 
            <tr>
           <td> <span id="lbltgl" runat="server"></span></td>     
           <td> <span id="lblketerangan" runat="server"></span></td>     
           <td align='right'> <span id="lblDebet" runat="server"></span></td>     
           <td align='right'>  <span id="lblKredit" runat="server"></span></td>     
           </tr>
      </table>
  </div>
     </div>
    <div id="divMessage" runat="server" visible="false">
         <div class="form_box_header">
            <div class="form_single">
               <asp:Label ID="lblMessageYear" runat="server" ></asp:Label>
            </div>
        </div>
    </div>
    
    
     <div class="form_button" id="divButton" runat='server'>
         <asp:HiddenField id="hdPostMethod" runat='server' />
        <asp:Button ID="btnpost" runat="server" OnClientClick="return confirm('Confirm lakukan Posting?')" CausesValidation="False" Text="POST" CssClass="small button blue"/> 
     </div> 

</asp:Content>
