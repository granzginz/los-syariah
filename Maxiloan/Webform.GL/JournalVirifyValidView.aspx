﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" 
    MasterPageFile="GL.Master" 
    CodeBehind="JournalVirifyValidView.aspx.vb" 
    Inherits="Maxiloan.Webform.GL.JournalVirifyValidView" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<%@ Register Src="../webform.UserController/UcSearchBy.ascx" TagName="UcSearchBy" TagPrefix="uc2" %>   
<%@ Register Src="VarifikasiTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
 <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"/> 
 
<uct:tabs id='cTabs' runat='server'></uct:tabs>

<div class="form_title">
    <div class="title_strip" ></div>
    <div class="form_single">
        <h3>
            VERIFIKASI JOURNAL
        </h3>
    </div>
</div>

<asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatepanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="updatepanel1">
    <ContentTemplate>

<asp:Panel runat="server" ID="pnlSelect">
    <div class="form_box">
        <uc2:UcSearchBy id="UcSearchBy" runat="server" />
    </div>
     <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue" />
    </div>
</asp:Panel>
<asp:Panel runat = "server" ID="pnlGrid"> 
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">  
            <asp:GridView  ID="dtgCSV" runat="server"  CssClass="grid_general"   showfooter="true" 
                DataKeyNames="TransactionNo" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%"    >
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"  /> 
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" /> 

            <Columns>
                <asp:TemplateField ItemStyle-Width="9" >
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn" ImageUrl="images/Plus.gif" CommandName="Expand" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="9" >
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkUpdateStatus" class="checkBoxClass" ToolTip="Select this file "   Checked='<%#eval("CanApprove")%>'/>
                   </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField  DataField="TransactionNo" HeaderText="No Transaksi" />
                <asp:BoundField  DataField="TransactionDate" HeaderText="Tgl Trans"  DataFormatString= "{0:dd/MM/yyyy}" />
                <asp:BoundField  DataField="valueDate" HeaderText="Tgl Valuta"  DataFormatString= "{0:dd/MM/yyyy}" />
                <asp:BoundField  DataField="CanApprove"  visible="false"/>
                <asp:BoundField  DataField="TransactionType" HeaderText="ID : Jenis Transaksi" />
                <asp:BoundField  DataField="Description" HeaderText="Keterangan" />

                <asp:templatefield headertext="Debet" >
                        <itemtemplate>  <div class="t-right"><%#Eval("TotalDebit", "{0:#,0}") %> </div> </itemtemplate>
                        <footertemplate>
                            <div class="t-right">   <asp:Label ID="lblTotalDebit" runat="server" /> </div>
                        </footertemplate>
                </asp:templatefield> 

                <asp:templatefield headertext="Credit" >
                        <itemtemplate> <div class="t-right"><%#Eval("TotalCredit", "{0:#,0}") %> </div></itemtemplate>
                            <footertemplate>
                                 <div class="t-right"> <asp:Label ID="lblTotalCredit" runat="server" /> </div>
                             </footertemplate>
                </asp:templatefield> 
                 
                <asp:TemplateField> 
                <ItemTemplate> 
                <asp:PlaceHolder ID="objPHOrderDetails" runat="server" >
                <tr>
                <td width="9" />
                <td colspan="7">
                    <asp:UpdatePanel runat="server" ID="ChildControl">
                    <ContentTemplate>
                        <asp:GridView   ID="grdDetails" CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"    AllowSorting="false" Width="98%"  runat="server">
                            <HeaderStyle CssClass="th tr-det" />
                            <Columns>
                                <asp:BoundField DataField="CoaId" HeaderText="COA" /> 
                                    <asp:BoundField DataField="CoaName" HeaderText="COA Name"  /> 
                                    <asp:BoundField DataField="TrDesc" HeaderText="Description"  /> 
                               <asp:BoundField DataField="DebitAmount" HeaderText="Debit"  DataFormatString= "{0:#,0}" > <ItemStyle CssClass="t-right" /></asp:BoundField>
                                <asp:BoundField DataField="CreditAmount" HeaderText="Credit"   DataFormatString= "{0:#,0}" > <ItemStyle CssClass="t-right" /></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    </asp:UpdatePanel >
                    </td>
                    </asp:PlaceHolder>     
                    </ItemTemplate>
                    </asp:TemplateField>    
              </Columns>
            </asp:GridView >
             <uc2:ucGridNav id="GridNavigator" runat="server"/>
            </div>
        </div> 
    </div> 
</asp:Panel>
 
   <div class="form_button" id="divAppr" runat="server">
           <asp:Button ID="btnVerify" runat="server"   CausesValidation="False" Text="Posting" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/> 
          <asp:Button ID="btnCancel" runat="server"   CausesValidation="False" Text="Cancel" CssClass="small button blue"/> 
   </div> 
    </ContentTemplate>
</asp:UpdatePanel>        
        <script type="text/javascript">

            $(document).ready(function () {
                $('#ContentPlaceHolder1_btnVerify').click(function () {

                    if ($('input:checkbox[id^="ContentPlaceHolder1_dtgCSV_chkUpdateStatus_"]:checked').length <= 0) {
                        alert("Silahkan pilih data terlebih dahulu");
                        return false;
                    }
                    return true;
                });
            });

    </script>
    
</asp:Content>
