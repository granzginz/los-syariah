﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="InitialGlBudgetEdit.aspx.vb" Inherits="Maxiloan.Webform.GL.InitialGlBudgetEdit" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
     
    <div class="form_title">
    <div class="title_strip"></div> 
    <div class="form_single">
    <h3>
        <span id="lbltitle" runat="server"></span>
        
    </h3>
    </div>
    </div>
            
    <input type="hidden" id="hdCompanyId"/>
     <input type="hidden" id="hdBranch"/>

   <div class="form_box">
            <div class="form_single">
            <label >YEAR</label>
            <asp:Label runat="server" id='lbYear'/>
            <asp:DropDownList ID="ddlYear" runat="server" Visible="false"/> 
          </div>
   </div>

       <div class="form_box">
        <div class="form_single">
        <label>COA ID</label>

         <asp:Label runat="server" id='lbCoaId'/>
         <div id="divcoaAddMode" runat="server" Visible="false" style="margin: 1px; display: inline;">
            <asp:TextBox ID="txtNoAccount" runat="server" onkeydown="return false;"  autocomplete="off" />
            <button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/MasterAcc.aspx?kodecid=" & txtNoAccount.ClientID & "&namacid=" & txtNamaAccount.ClientID  & "&isDt=1") %>','Daftar Akun','<%= jlookupContent.ClientID %>','HandleLookupPaymentAlloc');return false;">...</button>
           <asp:RequiredFieldValidator ID="rfvNoAccount" runat="server" ValidationGroup='entry'  ErrorMessage="isi nomor akun" CssClass="validator_general" Display="Dynamic" ControlToValidate="txtNoAccount" />
          </div>
   </div>
   </div> 
   
   <div class="form_box">
                <div class="form_single">
        <label>COA Description</label>
         <asp:Label runat="server" id='lbCoaDesc'/>
         <asp:TextBox ID="txtNamaAccount" runat="server" visible="false" onkeydown="return false;"  autocomplete="off" style="border: medium none; width: auto;"/>
      </div>
   </div> 

       <asp:Panel runat="server" ID="pnlBudgetGrid" >
  
  <div class="form_box">
                <div class="form_single">
                        <table cellspacing="1" cellpadding="2" width="25%" align="left" border="0">
        <tbody>
            <tr class="th"> 
                <th scope="col">Januari</th>
                <th scope="col">Febuari</th>
                <th scope="col">Maret</th>
                <th scope="col">April</th>
             </tr> 
            <tr>
            <td align="right"><uc8:ucnumberformat id="budget1" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget2" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget3" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget4" runat="server" /></td>        
                    
                      </tr>
        </tbody>
    </table>
     </div>
   </div> 
   
  <div class="form_box">
                <div class="form_single">
    <table cellspacing="1" cellpadding="2" width="25%" align="left" border="0">
        <tbody>
            <tr class="th"> 
                

                <th scope="col">Mei</th>
                <th scope="col">Juni</th>
                <th scope="col">Juli</th>
                <th scope="col">Agustus</th>
 </tr> 
            <tr>

            <td align="right"><uc8:ucnumberformat id="budget5" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget6" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget7" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget8" runat="server" /></td>
              </tr>
        </tbody>

    </table>
    
      </div>
   </div> 
   
  <div class="form_box">
                <div class="form_single">
    <table cellspacing="1" cellpadding="2" width="25%" align="left" sssborder="0">
        <tbody>
            <tr class="th"> 
                 
                <th scope="col">September</th>
                <th scope="col">Oktober</th>
                <th scope="col">November</th>
                <th scope="col">Desember</th>
            </tr> 
            <tr> 

            <td align="right"><uc8:ucnumberformat id="budget9" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget10" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget11" runat="server" /></td>
            <td align="right"><uc8:ucnumberformat id="budget12" runat="server" /></td>
            </tr>
        </tbody>
    </table>
     </div>
     </div> 
        
<div class="form_button">
        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="small button blue"  />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="small button gray" />
</div>    

</asp:Panel>
<div id="jlookupContent" runat="server" />

<script type="text/javascript">
 $(document).ready(function() {
     $("[id$=_rvBget]").text('Nilai tak valid.');
 });

//        var query = window.location.search;
//        var key = query.split("=")[1]; 
//        var prm = key.split(";"); 
//        $('#hdCompanyId').val(prm[0]);
//        $('#hdBranch').val(prm[1]);
//        $('#lbYear').text(prm[2]);
//        $('#lbCoaId').text(prm[3]);
//    }); 
</script>
</asp:Content>
