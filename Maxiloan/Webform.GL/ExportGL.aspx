﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExportGL.aspx.vb" Inherits="Maxiloan.Webform.GL.ExportGL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Export GL</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../Maxiloan.js"></script>
    <script type="text/javascript">
                  
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                EXPORT JURNAL Ke DBF
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Periode</label>
            <uc1:ucdatece id="txtPeriode1" runat="server"></uc1:ucdatece>
            &nbsp;S/D&nbsp;
            <uc1:ucdatece id="txtPeriode2" runat="server"></uc1:ucdatece>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnDownload" runat="server" Text="Download" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" Visible="false" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
