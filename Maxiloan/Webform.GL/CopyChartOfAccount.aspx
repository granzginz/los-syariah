﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CopyChartOfAccount.aspx.vb" Inherits="Maxiloan.Webform.GL.CopyChartOfAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InqAgreementList</title>
    <script type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.ServerVariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

    </script>

    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    CARI CHART OF ACCOUNT
                </h3>
            </div>        
        </div> 
        <div class="form_box">
            <div class="form_single">
                <label> Cari Berdasarkan </label>
                <asp:DropDownList ID="ddlSearchBy" runat="server">
                    <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="CoaId">Chart Of Account</asp:ListItem>
                    <asp:ListItem Value="Description">Name Chart Of Account</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="314px"></asp:TextBox>
            </div>
        </div>    
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>        
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR CHART OF ACCOUNT
                </h4>
            </div>
        </div>        
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ws">
                <asp:DataGrid ID="DtgAgree" runat="server" AllowSorting="False" AutoGenerateColumns="false"
                    DataKeyField="CoAID" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle> 
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imbedit" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="EDIT" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  SortExpression="CoaId" HeaderText="Chart Of Account">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCoaid" runat="server" Text='<%#Container.DataItem("CoaId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  SortExpression="Description" HeaderText="Description">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  SortExpression="Parent" HeaderText="Parent">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblParent" runat="server" Text='<%#Container.DataItem("Parent")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  SortExpression="Lvl" HeaderText="Level">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblLvl" runat="server" Text='<%#Container.DataItem("Lvl")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn visible="false" SortExpression="CoaId" HeaderText="">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCoaId" runat="server" Text='<%#Container.DataItem("CoaId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid> 
                </div>      
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div> 
        </div>     
    </asp:Panel>
    </form>
</body>
</html>

