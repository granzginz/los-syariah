﻿
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter


Public Class HifrontDetails
    Inherits WebBased

    Protected WithEvents txtTanggalVoucher As ucDateCE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "HIFRONT"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If Not Page.IsPostBack Then
            txtTanggalVoucher.Text = BusinessDate.ToString("dd/MM/yyyy")
        End If
         
    End Sub

    Private ReadOnly _jvController As New JournalVoucherController
    Private Sub doGetVoucher(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetVoucher.Click


        If (txtTanggalVoucher.Text.Trim() = String.Empty) Then
            ShowMessage(lblMessage, "Masukan Tanggal", False)
            Return
        End If 

        'If (tglVoucher > BusinessDate) Then
        '    ShowMessage(lblMessage, "Tanggal Journal Transaksi tidak boleh lebih besar dari Business Date", False)
        '    Return
        'End If


        Dim res = _jvController.GetHifrontDetails(GetConnectionString(), ConvertDate2(txtTanggalVoucher.Text))


        Response.ContentType = "text/plain"
        Response.AddHeader("content-disposition", "attachment;filename=" + String.Format("{0}1A0701EN{1}.txt", String.Format("{0:yyyyMMdd}", ConvertDate2(txtTanggalVoucher.Text)), String.Format("{0:HHMMSS}", DateTime.Today)))
        Response.Clear()

        Dim writer = New System.IO.StreamWriter(Response.OutputStream, Encoding.UTF8)

        writer.Write(res.ToString())

        Response.End()



    End Sub



End Class