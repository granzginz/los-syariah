﻿

Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System

Public Class JournalYearlyClose
    Inherits WebBased
    Private ReadOnly _jvController As New GlYearPeriodController()
    Private ReadOnly _controller As New JournalVoucherController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "JOURNALYEARLYCLOSE"

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If

        If Not IsPostBack Then
             
            If (String.IsNullOrEmpty(BranchID)) Then
                BranchID = sesBranchId.Replace("'", "")
            End If
            If (String.IsNullOrEmpty(SesCompanyID)) Then
                SesCompanyID = GetCompanyId(GetConnectionString)
            End If

            pnlBtn.Visible = False
            lblTahun.Text = BusinessDate.Year

            initLabel()
        End If
    End Sub
    Sub initLabel()
        Try
            Dim init = _jvController.YearlyCloseValidate(GetConnectionString(), SesCompanyID)
            lblTahun.Text = init
            'lblBulan.Text = init.Start.ToString("MMMM")
            'periodId = init.Id
            pnlBtn.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlBtn.Visible = False
        End Try
    End Sub




    Private Sub BtnProses_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim result = _controller.DoJournalYearlyClose(GetConnectionString(), SesCompanyID, lblTahun.Text)
            If (result.ToUpper() = "OK") Then

                pnlClosing.Visible = False
                pnlDone.Visible = True
                lblDone.Text = "Penutupan Tahunan Telah Selesai dilakukan."

                ShowMessage(lblMessage, "Tutup Tahunan Sukses..", False)
            Else
                ShowMessage(lblMessage, result, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


End Class