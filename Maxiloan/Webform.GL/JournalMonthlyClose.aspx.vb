﻿
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System
Public Class JournalMonthlyClose
    Inherits WebBased
    Private ReadOnly _jvController As New GlYearPeriodController()
    Private ReadOnly _controller As New JournalVoucherController
    Property periodId As String
        Set(ByVal value As String)
            ViewState("periodId") = value
        End Set
        Get
            Return ViewState("periodId")
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "JOURNALMONTHLYCLOSE"

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If

        If Not IsPostBack Then


            If (String.IsNullOrEmpty(BranchID)) Then
                BranchID = sesBranchId.Replace("'", "")
            End If
            If (String.IsNullOrEmpty(SesCompanyID)) Then
                SesCompanyID = GetCompanyId(GetConnectionString)
            End If

            pnlBtn.Visible = False
            lblTahun.Text = BusinessDate.Year
            lblBulan.Text = BusinessDate.ToString("MMMM")
             
           
            initLabel()
        End If
    End Sub

    Sub initLabel() 
        Try
            Dim init = _jvController.CurrentYearPeriod(GetConnectionString(), SesCompanyID, BranchID)
            lblTahun.Text = init.Year
            lblBulan.Text = init.Start.ToString("MMMM")
            periodId = init.Id
            pnlBtn.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlBtn.Visible = False
        End Try 
    End Sub



    Private Sub BtnProses_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim result = _controller.DoJournalMonthlyClose(GetConnectionString(), New Object() {periodId, Me.Loginid}) 
            If (result.Contains("Error")) Then
                ShowMessage(lblMessage, result, True)
            Else
                pnlClosing.Visible = False
                pnlDone.Visible = True
                lblDone.Text = result
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
End Class