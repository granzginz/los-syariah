﻿Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class CopyChartOfAccountProsses
    Inherits Maxiloan.Webform.AccMntWebBased
#Region " Private Const "
    Dim m_Company As New ImplementasiControler
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Dim customClass As New Parameter.Implementasi
#End Region
    Private Property CoaId() As String
        Get
            Return CType(ViewState("CoaId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CoaId") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.FormID = "CPYCOA"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim cmdWhere As String
            Me.CoaId = Request("CoaId")
            Me.SearchBy = Request("SearchBy")
            Me.SortBy = Request("SortBy")
            Me.BranchID = Request("BranchID")
            cmdWhere = "SUBSTRING(REPLACE(CoAId,' ',''),1,CHARINDEX('-',REPLACE(CoAId,' ',''))-1) = '" & Me.CoaId & "'"
            display(cmdWhere)

        End If
    End Sub

    Public Sub display(ByVal cmdwhere As String)
        Try
            Dim dtslist As DataTable
            Dim cReceive As New GeneralPagingController
            Dim oReceive As New Parameter.GeneralPaging
            With oReceive
                .strConnection = GetConnectionString()
                .WhereCond = cmdwhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spCopyCOASelect"
            End With
            oReceive = cReceive.GetGeneralPaging(oReceive)
            dtslist = oReceive.ListData
            If dtslist.Rows.Count > 0 Then
                If dtslist.Rows(0).Item("CoaId") <> "" Then
                    lblChatOfAccount.Text = CStr(dtslist.Rows(0).Item("CoaId")).Trim
                End If
                If dtslist.Rows(0).Item("Description") <> "" Then
                    lblDescription.Text = CStr(dtslist.Rows(0).Item("Description")).Trim
                End If
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub
    Public Sub AgreementStatusEdit(ByVal customclass As Parameter.Implementasi)
        Dim params() As SqlParameter = New SqlParameter(0) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@CoaId", SqlDbType.VarChar, 20)
            params(0).Value = customclass.CoaId

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spProssesCopyCOA", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()

            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.Implementasi
        lblMessage.Text = ""
        Try

            With customClass
                .strConnection = GetConnectionString()
                .CoaId = Me.CoaId.Trim
                .UsrUpd = Me.Loginid
                .IsUpdateSattus = 1
            End With
            AgreementStatusEdit(customClass)

            Response.Redirect("CopyChartOfAccount.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS & "&SearchBy=" & Me.SearchBy & "&SortBy=" & Me.SortBy & "&BranchID=" & Me.BranchID)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("asp", "function", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("CopyChartOfAccount.aspx?back=" & "1" & "&SearchBy=" & Me.SearchBy & "&SortBy=" & Me.SortBy & "&BranchID=" & Me.BranchID)
    End Sub

End Class