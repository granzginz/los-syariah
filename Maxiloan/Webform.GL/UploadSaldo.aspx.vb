﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports ClosedXML.Excel
Imports System.IO
Imports Maxiloan.Framework.SQLEngine
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.OpenXml4Net.OPC
Imports NPOI.XSSF.UserModel

#End Region

Public Class UploadSaldo
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Private dt As DataTable
    Private cGeneral As New GeneralPagingController
    Private oGeneral As New Parameter.GeneralPaging
    Private lnkBranchId As LinkButton
    Private Command As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If SessionInvalid() Then Exit Sub
        lblMessage.Visible = False

        Dim scriptManager__3 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__3.RegisterPostBackControl(Me.btnImport)
        If Not Me.IsPostBack Then
            Me.FormID = "UPLOADSALDOBAL"
            divupload.Visible = False
            'cboMonth.SelectedValue = Me.BusinessDate.Date.Month.ToString
        End If
    End Sub
    Function CheckFileType(ByVal fileName As String) As Boolean
        Dim ext As String = Path.GetExtension(fileName)

        Select Case ext.ToLower()
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select

    End Function
    Private Function pathFile(ByVal Group As String, ByVal Data As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\Upload\" & Group & "\" & Data & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Sub UploadFunding(FileUpload1 As FileUpload, FileName As String)

        If (FileUpload1.HasFile) Then
            If (CheckFileType(FileUpload1.FileName)) Then

                Dim filePath As String = pathFile("GL", "DataUploadSaldo") & FileName & ".xls"
                FileUpload1.PostedFile.SaveAs(filePath)
            Else
                ShowMessage(lblMessage, "Upload File salah, harap perhatikan tipe file!", True)
                Exit Sub
            End If
        End If

    End Sub

    Public Shared Function ExcelToDataTable(filePath As String) As DataTable
        Dim dtExcel As New DataTable()
        Dim hssfworkbook As HSSFWorkbook
        Using file As New FileStream(filePath, FileMode.Open, FileAccess.Read)
            hssfworkbook = New HSSFWorkbook(file)
        End Using
        Dim sheet As ISheet = hssfworkbook.GetSheetAt(0)
        Dim rows As System.Collections.IEnumerator = sheet.GetRowEnumerator()

        Dim headerRow As IRow = sheet.GetRow(0)
        Dim cellCount As Integer = headerRow.LastCellNum

        For j As Integer = 0 To cellCount - 1
            Dim cell As ICell = headerRow.GetCell(j)
            dtExcel.Columns.Add(cell.ToString())
        Next

        For i As Integer = (sheet.FirstRowNum + 1) To sheet.LastRowNum
            Dim row As IRow = sheet.GetRow(i)
            Dim dataRow As DataRow = dtExcel.NewRow()
            If row Is Nothing Then
                Exit For
            End If
            For j As Integer = row.FirstCellNum To cellCount - 1
                If row.GetCell(j) IsNot Nothing Then
                    dataRow(j) = row.GetCell(j).ToString()
                End If
            Next

            dtExcel.Rows.Add(dataRow)
        Next
        Return dtExcel

    End Function

    Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        Dim dtUploadFromExcel As DataTable
        Dim strFileName As String

        If (FileUpload.HasFile) Then

            If Not IsDBNull(FileUpload.PostedFile) And
                FileUpload.PostedFile.ContentLength > 0 Then
                Try

                    UploadFunding(FileUpload, "DataUploadSaldo")
                    strFileName = pathFile("GL", "DataUploadSaldo") & "DataUploadSaldo.xls"

                    If FileUpload.HasFile Then
                        If File.Exists(strFileName) Then
                            dtUploadFromExcel = ExcelToDataTable(strFileName)

                            DtgViewUploadFromExcel.DataSource = dtUploadFromExcel
                            DtgViewUploadFromExcel.DataBind()

                            divupload.Visible = True
                            File.Delete(strFileName)
                        End If
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)

                Finally

                End Try
            Else
                ShowMessage(lblMessage, "Upload Data Gagal ", True)
            End If
        Else
            ShowMessage(lblMessage, "Upload Data Gagal ", True)
        End If
    End Sub

    Protected Sub btnExecuteFromUpload_Click(sender As Object, e As EventArgs) Handles btnExecuteFromUpload.Click
        Dim dtUploadFromExcelx As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim lblCabang As New Label
        Dim lblCoa As New Label
        Dim lblAmount As New Label
        Dim customClass As New Parameter.Implementasi

        With dtUploadFromExcelx
            .Columns.Add(New DataColumn("BranchId", GetType(String)))
            .Columns.Add(New DataColumn("CoaId", GetType(String)))
            .Columns.Add(New DataColumn("Amount", GetType(Double)))
        End With

        For intloop = 0 To DtgViewUploadFromExcel.Items.Count - 1
            lblCabang = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblCabang"), Label)
            lblCoa = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblCoa"), Label)
            lblAmount = CType(DtgViewUploadFromExcel.Items(intloop).FindControl("lblAmount"), Label)

            oRow = dtUploadFromExcelx.NewRow
            oRow("BranchId") = lblCabang.Text.Trim
            oRow("CoaId") = lblCoa.Text.Trim
            oRow("Amount") = If((String.IsNullOrEmpty(lblAmount.Text.Trim)), 0, lblAmount.Text.Trim)
            dtUploadFromExcelx.Rows.Add(oRow)
        Next

        Try
            With customClass
                .strConnection = GetConnectionString()
                .Month = cboMonth.SelectedValue.Trim
                .Year = Me.BusinessDate.Date.Year.ToString
                .Listdata = dtUploadFromExcelx
            End With

            UploadSaldoAwal(customClass)

            Response.Redirect("UploadSaldo.aspx?")

            ShowMessage(lblMessage, "Upload Data Berhasil ", False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    Public Sub UploadSaldoAwal(ByVal customclass As Parameter.Implementasi)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@Month", SqlDbType.VarChar, 2)
            params(0).Value = customclass.Month

            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = customclass.Year

            params(2) = New SqlParameter("@DataUpload", SqlDbType.Structured)
            params(2).Value = customclass.Listdata

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spUploadSaldoAwal", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub


    Private Sub btnCancelExecuteFromUploadl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelExecuteFromUpload.Click
        'Response.Redirect("JournalVoucherList.aspx")
        Response.Redirect("UploadSaldo.aspx")
    End Sub
End Class