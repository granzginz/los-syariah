﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class GeneratePeriod
    Inherits Maxiloan.Webform.AccMntWebBased
    Private m_controller As New DataUserControlController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.FormID = "GENPERIODE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            txtAwalTahun.Text = "01/01/" + Me.BusinessDate.Year.ToString
        End If
    End Sub
    Protected Sub btnButtonGenerate_Click(sender As Object, e As EventArgs) Handles ButtonGenerate.Click
        Dim customClass As New Parameter.Implementasi
        lblMessage.Text = ""

        With customClass
            .strConnection = GetConnectionString()
            .StartDate = txtAwalTahun.Text.Trim
        End With
        GeneratePeriode(customClass)

        ShowMessage(lblMessage, "Generate Periode Sukses", False)
    End Sub
    Public Sub GeneratePeriode(ByVal customclass As Parameter.Implementasi)
        Dim params() As SqlParameter = New SqlParameter(0) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@Start", SqlDbType.DateTime)
            params(0).Value = customclass.StartDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGenerateGLPeriod", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()

            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

End Class