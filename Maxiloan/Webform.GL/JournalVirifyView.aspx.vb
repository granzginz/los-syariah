﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class JournalVirifyView
    Inherits WebBased
    Private ReadOnly _jvController As New JournalVoucherController
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents InvalidGridNavigator As ucGridNav
    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    Private debTotal As Decimal = 0
    Private creTotal As Decimal = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "JRNVERIFI"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If
        lblMessage.Visible = False

        'AddHandler GridNavigator.PageChanged, AddressOf Navevent
        'AddHandler InvalidGridNavigator.PageChanged, AddressOf InvalidNavevent
        If Not IsPostBack Then
            txtPeriodeFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            bindDdlBranch()

            'cTabs.SetNavigateUrl("", "")
            'cTabs.RefreshAttr(Request("pnl"))


            'ViewState("OrderId") = ""
            'SesCompanyID = GetCompanyId(GetConnectionString)

            'If (Request("page") Is Nothing) Then

            '    pnlGrid.Visible = False
            '    pnlIInvalidGrid.Visible = False
            '    cTabs.Visible = False
            'Else
            '    Dim Idp = Request("id")
            '    cTabs.SetNavigateUrl("page", ID)

            '    pnlGrid.Visible = (Idp = "1")
            '    pnlIInvalidGrid.Visible = (Idp = "0")
            'End If
        End If
    End Sub

    Sub bindDdlBranch() 
        With ddlCabang 
            .DataSource = New DataUserControlController().GetBranchAll(GetConnectionString)
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0" 
        End With 
    End Sub
    Private Sub btnFind_Click() Handles BtnSearch.Click
        'ViewState("FIND") = 1
        'cTabs.SetNavigateUrl("page", "1")
        Dim ypCont = New GlYearPeriodController()

        Dim prd = ypCont.YearPeriods(GetConnectionString, SesCompanyID, sesBranchId.Replace("'", ""), ConvertDate2(txtPeriodeFrom.Text).Year.ToString, ConvertDate2(txtPeriodeFrom.Text).Month.ToString)
        If (prd.Count = 0) Then
            ShowMessage(lblMessage, "Tanggal Periode transaksi telah close / belum di open.", True)
            Exit Sub
        End If

        Session("SELBRANCH") = ddlCabang.SelectedValue
        Session("TRNDATE") = ConvertDate2(txtPeriodeFrom.Text)
        Response.Redirect("JournalVirifyValidView.aspx")
        'showValidGrid()
        'cTabs.Visible = True
        'pnlGrid.Visible = True
        'pnlsearch.Visible = False

    End Sub
    'Sub showValidGrid()
    '    Try
    '        pnlGrid.Visible = True
    '        pnlIInvalidGrid.Visible = False

    '        Dim result = _jvController.GlJournalVerifyPage(GetConnectionString(), New JournalApproval With {
    '                                                       .CompanyId = SesCompanyID, .TransactionType = "1",
    '                                                       .BranchId = Session("SELBRANCH"),
    '                                                       .TransactionDate = CType(Session("TRNDATE"), DateTime)}, PageSize, 1, _recordCount)
    '        ViewState("SOURCE") = result


    '        If (result.Count <= 0) Then
    '            Return
    '        End If

    '        dtgCSV.DataSource = result
    '        dtgCSV.DataBind()
    '        GridNavigator.Initialize(_recordCount, PageSize)


    '        Dim result1 = _jvController.GlJournalVerifyPage(GetConnectionString(), New JournalApproval With {
    '                                                    .CompanyId = SesCompanyID, .TransactionType = "0",
    '                                                    .BranchId = Session("SELBRANCH"),
    '                                                    .TransactionDate = CType(Session("TRNDATE"), DateTime)}, PageSize, 1, _recordCount)
    '        ViewState("SOURCEINV") = result1

    '        If (result1.Count <= 0) Then
    '            Return
    '        End If
    '        grdInvalid.DataSource = result1
    '        grdInvalid.DataBind()
    '        InvalidGridNavigator.Initialize(_recordCount, PageSize)


    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub


    'Private Sub InvalidNavevent(ByVal sender As Object, ByVal e As PageChangedEventArgs) 
    '    Dim result = _jvController.GlJournalVerifyPage(GetConnectionString(), New JournalApproval With {
    '                                                   .CompanyId = SesCompanyID, .TransactionType = "0",
    '                                                   .BranchId = Session("SELBRANCH"),
    '                                                   .TransactionDate = CType(Session("TRNDATE"), DateTime)}, PageSize, e.CurrentPage, _recordCount)
    '    ViewState("SOURCE") = result
    '    grdInvalid.DataSource = result
    '    grdInvalid.DataBind()
    '    InvalidGridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    'End Sub
    'Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)

    '    Dim result = _jvController.GlJournalVerifyPage(GetConnectionString(), New JournalApproval With {.CompanyId = SesCompanyID, .TransactionType = "1",
    '                                                                                                    .BranchId = Session("SELBRANCH"),
    '                                                   .TransactionDate = CType(Session("TRNDATE"), DateTime)}, PageSize, e.CurrentPage, _recordCount)
    '    ViewState("SOURCE") = result
    '    dtgCSV.DataSource = result
    '    dtgCSV.DataBind()
    '    GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    'End Sub



    'Protected Sub grdOrders_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dtgCSV.RowCommand

    '    If (e.CommandName = "Expand") Then

    '        Dim gv As GridView = CType(sender, GridView)
    '        Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString()) 
    '        Dim imgbtn As ImageButton = CType(gv.Rows(rowIndex).FindControl("ImgBtn"), ImageButton)

    '        Dim key = gv.DataKeys(rowIndex)(0).ToString()
    '        ViewState("OrderId") = key
    '        Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails"), PlaceHolder)

    '        If (imgbtn.ImageUrl.Trim().ToUpper() = "IMAGES/PLUS.GIF") Then
    '            objPH.Visible = True
    '            imgbtn.ImageUrl = "images/Minus.gif" 
    '        Else
    '            imgbtn.ImageUrl = "images/Plus.gif"
    '            objPH.Visible = False 
    '        End If

    '    End If
    'End Sub

    'Private Sub DtgAsset_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dtgCSV.RowDataBound
    '    Dim imgBtn As ImageButton
    '    Dim data = CType(ViewState("SOURCE"), IList(Of JournalApproval))
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        imgBtn = CType(e.Row.FindControl("ImgBtn"), ImageButton)
    '        imgBtn.CommandArgument = e.Row.RowIndex.ToString()


    '        Dim txtid = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionNo"))
    '        Dim objChildGrid As GridView = CType(e.Row.FindControl("grdDetails"), GridView)
    '        objChildGrid.DataSource = Data.SingleOrDefault(Function(x) x.TransactionNo = txtid).Items
    '        objChildGrid.DataBind()
    '        objChildGrid.Visible = True
    '        Dim objPH As PlaceHolder = CType(e.Row.FindControl("objPHOrderDetails"), PlaceHolder)
    '        objPH.Visible = False


    '        imgBtn.ImageUrl = "Images/Plus.gif"
    '        If (txtid = ViewState("OrderId")) Then
    '            objPH.Visible = True
    '            imgBtn.ImageUrl = "Images/Minus.gif"
    '        End If

    '    End If

    'End Sub
End Class