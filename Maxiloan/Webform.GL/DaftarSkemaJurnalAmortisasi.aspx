﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DaftarSkemaJurnalAmortisasi.aspx.vb" Inherits="Maxiloan.Webform.GL.DaftarSkemaJurnalAmortisasi" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetMaster</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        CARI PROSPECT CUSTOMER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <asp:Panel ID="pnlGrid" runat="server" Visible="False">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                DAFTAR MASTER ASSET
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">   
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" AllowSorting="True"
                                OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="ID">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col" Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/iconDelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>   
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="SeqNo" HeaderText="SeqNo" Visible="True">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="hySeqNo" runat="server" Text='<%#Container.DataItem("SeqNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="DEBET" HeaderText="DEBET">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDEBET" runat="server" Text='<%#Container.DataItem("DEBET")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:TemplateColumn SortExpression="CREDIT" HeaderText="CREDIT">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCREDIT" runat="server" Text='<%#Container.DataItem("CREDIT")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:TemplateColumn SortExpression="AKTIF" HeaderText="AKTIF">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAKTIF" runat="server" Text='<%#Container.DataItem("AKTIF")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left"  
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="First" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Prev" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Next" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Last" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                                <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                                    CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                                    ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>  
                    <div class="form_button">
                            <asp:Button ID="ButtonAddNew" runat="server" Text="Add" CssClass="small button blue"
                                CausesValidation="False"></asp:Button>&nbsp;
                           
                        </div> 
                </asp:Panel>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="SeqNo" Selected="True">Seq No</asp:ListItem>
                            <asp:ListItem Value="Nama">Nama</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>

            </asp:Panel>
            <asp:Panel ID="pnlView" Visible="false" runat="server">
                <asp:Panel ID="pnlViewAdd" runat="server">
                    <asp:TextBox ID="txtID" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
                    <div class="form_box_hide">
                        <div class="form_single">
                            <label id="lblseqno" class="medium_text">
                                    SeqNo
                            </label>
                            <asp:TextBox ID="txtSeqNo" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Nama
                            </label>
                            <asp:TextBox ID="txtNama" runat="server" CssClass="medium_text" Width="20%" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                    <div class ="form_box">
                        <div class ="form_single">
                            <label class="medium_text">COA Debet</label>
                            <asp:TextBox ID ="txtCOADebet" runat ="server" Width ="10%" MaxLength ="100"
                                Columns ="105"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                                ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                    <div class ="form_box">
                        <div class ="form_single">
                            <label class="medium_text">COA Kredit</label>
                            <asp:TextBox ID ="txtCOAKredit" runat ="server" Width ="10%" MaxLength ="100"
                                Columns ="105"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                                ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
                        </div>
                    </div> 
                </asp:Panel>
                <div class="form_button">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="false"></asp:Button>
                    <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
