﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
#End Region
Public Class CopyChartOfAccount
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property

    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
    Private Property back() As String
        Get
            Return CType(ViewState("back"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("back") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.AgreementList
    Private Dcontroller As New DataUserControlController
    Private oClass As New Parameter.ControlsRS
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "CPYCOA"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            Me.back = Request.QueryString("back")

            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), False)
                Me.SearchBy = Request.QueryString("SearchBy")
                Me.SortBy = Request.QueryString("SortBy")

                pnlDatagrid.Visible = True
                DtgAgree.Visible = True
                DoBind(Me.SearchBy, Me.SortBy)
            End If
            If Me.back = "1" Then
                lblMessage.Visible = False
                Me.SearchBy = Request.QueryString("SearchBy")
                Me.SortBy = Request.QueryString("SortBy")

                pnlDatagrid.Visible = True
                DtgAgree.Visible = True
                DoBind(Me.SearchBy, Me.SortBy)
            End If

        End If

    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spCopyChartOfAccountPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)

        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If

        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()
        pnlDatagrid.Visible = True

    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))

        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If

        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Server.Transfer("CopyChartOfAccount.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        Dim strFilter As New StringBuilder
        strSearch.Append(" 1=1 ")

        If ddlSearchBy.SelectedIndex > 0 AndAlso txtSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & ddlSearchBy.SelectedValue.Trim & " like '%" & txtSearchBy.Text.Trim & "%'")
        End If

        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)

    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Dim hyCoaId As HyperLink

        hyCoaId = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("hyCoaId"), HyperLink)
        If e.Item.ItemIndex >= 0 Then
            If e.CommandName = "EDIT" Then
                Response.Redirect("CopyChartOfAccountProsses.aspx?CoaId=" & hyCoaId.Text.Trim & "" & "&SearchBy=" & Me.SearchBy & "&SortBy=" & Me.SortBy)
            End If
        End If
    End Sub

End Class