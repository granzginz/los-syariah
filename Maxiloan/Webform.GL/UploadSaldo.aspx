﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadSaldo.aspx.vb" Inherits="Maxiloan.Webform.GL.UploadSaldo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../js/jquery-1.9.1.min.js"></script>
	<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
	<script src="../Maxiloan.js"></script>

    <script language="javascript" type="text/javascript">
        function upload1() {
            if (window.confirm("Apakah yakin mau upload data ini? "))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }
    </script>

     <script type="text/javascript">
        function doOK(e) {
            $("div.tx-overlay").css("display", "none");
            $("#csvSampleDiv").css("display", "none");

        }
        function SammpleCSV() {
            $("div.tx-overlay").css("display", "block");
            $("#csvSampleDiv").css("display", "block");
        }


        function Update_intCell() {
            alert("You Selected Cell 0.");
        }
    </script>

    <style>
        .tr-det { 
         background:  none repeat-x scroll 0 0 gray;
        }
        .red{
            text-decoration:none;
            color:Red;
            border-color: #000;
            } 
        .tx-overlay {
                    background-color: #000;
                    height: 100%;
                    left: 0;
                    opacity: 0.5;
                    position: fixed;
                    top: 0;
                    width: 100%;
                    z-index: 10001;
                }
        .t-window {
            border-radius: 5px;
            border-width: 2px;
            box-shadow: 0 0 5px 2px #aaa;
            display: inline-block;
            position: absolute;
            z-index: 10001;
            background-color: #fff;
            border-color: #a7bac5;
        }
        div.af05{
            background-position:center;
            background-image: url('../images/UploadSaldo.png');
            background-repeat:no-repeat;
              height: 305px;
            margin:0; /* If you want no margin */
            padding:0; /*if your want to padding */
        }
        #ContentPlaceHolder1_lblMessage {
            cursor: pointer;
            left: -15px;
            opacity: 0.6;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 100;
        }
          .t-right{text-align: right;margin-right:4px;} 
    </style>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnl1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        PERIODE
                    </h4>
                </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Bulan</label>
                <asp:DropDownList runat="server" ID="cboMonth">
                    <asp:ListItem Text="Pilih" Value="0"></asp:ListItem>
                <asp:ListItem Text="Januari" Value="1"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ControlToValidate="cboMonth" ErrorMessage="Harap pilih Bulan" InitialValue="0"
                    CssClass="validator_general" Visible="True" Enabled="True"></asp:RequiredFieldValidator>
            </div>
        </div>
            <div class="form_button">
                <asp:FileUpload ID="FileUpload" Width="450px" runat="server" visible="true"/>
                <a onclick='SammpleCSV();' href='#' > <span>Contoh Format File Upload</span></a>
                <asp:Button ID="btnImport" runat="server" Text="Import Data" CssClass="small button blue" Visible="true">  </asp:Button>
            </div>
            <div id="divupload" runat="server">
        <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR UPLOAD SALDO
                    </h4>
                </div>
        </div>       
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgViewUploadFromExcel" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                             BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                    <asp:TemplateColumn SortExpression="" HeaderText="Cabang" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblCabang" runat="server" Text='<%# Container.DataItem("BranchId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="" HeaderText="Coa" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblCoa" runat="server" Text='<%# Container.DataItem("CoaId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                 <asp:TemplateColumn SortExpression="" HeaderText="Amount" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Container.DataItem("Amount")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                </div>
            </div>
        </div>
           <div class="form_button">
                    <asp:Button ID="btnExecuteFromUpload" runat="server" Text="Execute" CssClass="small button blue" Visible="true"  OnClientClick="if(this.value === 'Loading....!!') { return false; } else { this.value = 'Saving...'; }">  </asp:Button>
                    <asp:Button ID="btnCancelExecuteFromUpload" runat="server" Text="Cancel" CssClass="small button gray" Visible="true">  </asp:Button>
            </div>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
          <div class="tx-overlay" style="opacity: 0.5; z-index: 1000; display: none;"></div> 
            <div id="csvSampleDiv" class="t-widget t-window" style="top: 80px; left:80px; width: 1100px;  display:none;">
                <div style="margin:20px;height: 298px;">
                <div class="af05">&nbsp;</div>
             
                </div>
                   <button type='button' id='btnOk' style="margin:2px;" onclick="doOK();"> <span> OK</span></button>
            </div>
    </form>
</body>
</html>
