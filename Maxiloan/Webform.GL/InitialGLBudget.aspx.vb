﻿ 
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

Public Class InitialGLBudget
    Inherits WebBased
    Private ReadOnly _jvController As New JournalVoucherController
    Private Const PageSize As Integer = 10
    Protected WithEvents GridNavigator As ucGridNav
    Private _recordCount As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "GLBGT"
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
        If (String.IsNullOrEmpty(BranchID)) Then
            BranchID = sesBranchId.Replace("'", "")
        End If
        If (String.IsNullOrEmpty(SesCompanyID)) Then
            SesCompanyID = GetCompanyId(GetConnectionString)
        End If


        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not Page.IsPostBack Then


            BindDdlBranch(ddlBranch, GetConnectionString, sesBranchId, IsHoBranch)
            
            With ddlYear
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataSource = ModuleGlHelper.YearsList()
                .DataBind()
            End With
            ddlYear.SelectedValue = BusinessDate.Year

            BindGridFind(1)
        End If
    End Sub
    Sub BindGridFind(currPage As Integer, Optional init As Boolean = True)
        Dim branch = ddlBranch.SelectedValue

        'obsolate
        Dim acctype = 0 '(2 ^ EnumMasterAccType.Expenses) Or (2 ^ EnumMasterAccType.Biaya_Lainnya) 'ddlAccountType.SelectedValue

        Dim opW = ddlCari.SelectedValue
        Dim sWhare = txtCari.Text.Trim
        Dim _year = ddlYear.SelectedValue

        If (branch = "0") Then
            branch = BranchID
        End If
         

        dtgInitialBgt.DataSource = _jvController.SelectInitialBudget(_recordCount, GetConnectionString(), New Object() {SesCompanyID, branch, _year, currPage, PageSize, acctype, opW, sWhare})
        dtgInitialBgt.DataBind()

        pnlBudgetGrid.Visible = True

        If (init = True) Then
            GridNavigator.Initialize(_recordCount, PageSize)
        End If
    End Sub
     
    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs) 
        BindGridFind(e.CurrentPage, False) 
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        hdIsFind.Value = "1"

    End Sub

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        If e.CommandName.ToLower = "edit" Then 
            If e.CommandArgument.ToString <> String.Empty Then
                Response.Redirect("InitialGlBudgetEdit.aspx?mode=1&key=" & e.CommandArgument.ToString)
            End If

        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Response.Redirect("InitialGlBudgetEdit.aspx?mode=0")
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
        Dim directory As String = physicalPath & "\xml\"
        Dim fileName As String = DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + files.PostedFile.FileName
        Dim fullFileName As String = directory & fileName
        Dim sr As IO.StreamReader
        Try
            files.PostedFile.SaveAs(fullFileName)
            sr = New IO.StreamReader(fullFileName)

            Dim ret = InitialBudget.ParseInitialBudgetCsv(sr, SesCompanyID, BranchID)


            If (ret.Count <= 0) Then
                ShowMessage(lblMessage, "CSV File Tidak Ada Data", False)
                Return
            End If

            Dim result = _jvController.GlInitialBudgetUpdate(GetConnectionString, ret)

            If (result <> "OK") Then
                ShowMessage(lblMessage, result, True)
                Return
            End If

            ShowMessage(lblMessage, result, False)


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            If Not (sr Is Nothing) Then
                sr.Dispose()
            End If
        End Try

    End Sub
End Class