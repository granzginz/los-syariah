﻿<%@ Page Language="vb" AutoEventWireup="false"  CodeBehind="COAdetail.aspx.vb" Inherits="Maxiloan.Webform.GL.COAdetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
<form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
   <asp:Label ID="Label1" runat="server"  ></asp:Label>
    <div id="jlookupContent" runat="server" />
 
    
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REGISTRASI AKUN (CHART OF ACCOUNT)
                    </h3>
                </div>
            </div>

             <div class="form_box">
                <div class="form_single">
                    <label  class ="label_req">
                        Branch</label>
                    <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="true" />
                    <asp:HiddenField runat="server" ID="HiddenField1" />
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlBranch" 
                                              Display="Dynamic" CssClass="validator_general"  ErrorMessage="Pilih Branch!" InitialValue="0" />
                </div>
            </div> 

            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        Account Type</label>
                    <asp:DropDownList ID="ddlAccountType" runat="server" />
                    <asp:RequiredFieldValidator ID="rfsAccType" runat="server" ControlToValidate="ddlAccountType" 
                                              Display="Dynamic" CssClass="validator_general"  ErrorMessage="Pilih Account Type!" InitialValue="0" />
                    
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Account No</label>
                    <asp:TextBox ID="txtAccountNo" runat="server" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvAccountNo" Display="Dynamic" CssClass="validator_general"
                        ErrorMessage="Isi Nomor Akun" ControlToValidate="txtAccountNo" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Account Name</label>
                    <asp:TextBox ID="txtAccountName" runat="server"  CssClass="medium_text"/>
                    <asp:Label id="lblLevel" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvAccountName" Display="Dynamic"
                        CssClass="validator_general" ErrorMessage="Isi Nama Akun" ControlToValidate="txtAccountName" />
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                    <label >
                        Sub Account dari</label>
                        <asp:HiddenField runat="server" ID="hdnParentId" />
                        <asp:HiddenField runat="server" ID="hdnParentAddFree" />
                        <asp:TextBox runat="server" ID="txtParentName" readonly="true" CssClass="medium_text"></asp:TextBox>
                         <asp:Panel ID="pnlsrch" runat="server"  style="display:inline-block;position: relative;" >
                        
                        <button class="small buttongo blue"   id="btnSearchParent" >...</button>                        
                        <button class="small buttongo blue" id="btnClearParent" onclick="$('#hdnParentId').val('');$('#txtParentName').val('');return false;">Reset</button>
                     
                        </asp:Panel>
                </div>
            </div>--%>
            <div class="form_box">
                <div class="form_single">
                    <label>Sub Account dari</label>
                    <asp:TextBox ID="txtParentName" runat="server" class="txtParentName"></asp:TextBox>
                    <asp:HiddenField runat="server" ID="hdnParentId" />
                    <asp:HiddenField runat="server" ID="hdnParentAddFree" />
                    
                    <button class="small buttongo blue" 
                    onclick ="OpenLookup();return false;">...</button>                        
                    <asp:Button runat="server" ID="Jlookup" style="display:none" />
                </div>
            </div>
            
            <div class="form_box">
                <div class="form_single">
                   <label>
                        LBU</label>
                    <asp:TextBox ID="txtLBU" runat="server" Width="50" MaxLength="4"
                onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                onfocus="this.value=resetNumber(this.value);" OnChange="javascript:void(0)" autocomplete="off"></asp:TextBox>
                <asp:RegularExpressionValidator ID="rvfNum" runat="server" ControlToValidate="txtLBU"
                    ErrorMessage="Harap Isi Angka" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                    Visible="True" Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="rgValNum" runat="server" font-name="Verdana" Font-Size="11px"
                    Type="String" ControlToValidate="txtLBU" MinimumValue="0" MaximumValue="9999"
                    Visible="True" Display="Dynamic"></asp:RangeValidator>
                </div>
            </div>
             <div class="form_box">
                <div class="form_single">
                   <label>
                        SIPP </label>
                    <asp:TextBox ID="txtSIPP" runat="server" MaxLength="12" Width="150" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Mata Uang</label>
                    <asp:DropDownList ID="ddlMataUang" runat="server" >
                    </asp:DropDownList>
                    <span id="lblfCurr" style="display:none;margin-left:5px;"><strong >Foreign Currency</strong></span>
                    <asp:HiddenField id="hidHomeCurr" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Active</label>
                    <asp:CheckBox ID="chkStatus" runat="server" Checked="true"/>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="small button gray" />
            </div>    
     
 <script type="text/javascript">
    function OpenLookup()  {
                OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/MasterAccIsLeaf.aspx?kodecid=" & hdnParentId.ClientID & "&namacid=" & txtParentName.ClientID & "&isDt=0" & "&Cabang=" & HiddenField1.Value) %>', 'DAFTAR UNIT', '<%= jlookupContent.ClientID %>', 'DoPostBack')
            }
    
</script>

 <script language="JavaScript" type="text/javascript">
     function DoPostBack() {
         __doPostBack('Jlookup', '');
     }
 </script>   
    
    <script type="text/javascript">

        $(document).ready(function () {
            

            //$('#btnSearchParent').click(function () {
            //    var tp = $('#ContentPlaceHolder1_ddlAccountType').val();
            //    var tpz = $('#ContentPlaceHolder1_ddlAccountType option:selected').text(); 
            //    if (tp =="0") return false;
            //    OpenJLookup('../webform.Utility/jLookup/MasterAcc.aspx?kodecid=ContentPlaceHolder1_hdnParentId&namacid=ContentPlaceHolder1_txtParentName&addfree=ContentPlaceHolder1_hdnParentAddFree&tp=' + tp + '&tpn=' + tpz, 'Daftar Akun', 'ContentPlaceHolder1_jlookupContent'); 
            //    return false;
            //});



            //$('#btnSearchPasangan').click(function () {
            //    var tp = $('#ContentPlaceHolder1_ddlAccountType').val();
            //    var tpz = $('#ContentPlaceHolder1_ddlAccountType option:selected').text();
            //    if (tp == "0") return false;
            //    OpenJLookup('../webform.Utility/jLookup/MasterAcc.aspx?kodecid=ContentPlaceHolder1_hdbCOAPasanganId&namacid=ContentPlaceHolder1_txtCoaPasangan&addfree=ContentPlaceHolder1_hdnCOAPasanganAddFree&tp=' + tp + '&tpn=' + tpz + '&isDt=1', 'Daftar Akun', 'ContentPlaceHolder1_jlookupContent');
            //    return false;
            //});

            $('#ContentPlaceHolder1_ddlMataUang').change(function (e) {
                var hcur = $('#<%= hidHomeCurr.ClientID %>').val();
                var dis = (this.value == hcur) ? "none" : "";
                $("#lblfCurr").css("display", dis ) ;
            });



            var hcur = $('#<%= hidHomeCurr.ClientID %>').val();
            var dis = ('IDR' == hcur) ? "none" : "";
            $("#lblfCurr").css("display", dis); 
        })
        
    </script>
</form>
</body>
</html> 