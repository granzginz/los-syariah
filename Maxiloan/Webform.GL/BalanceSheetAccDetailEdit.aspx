﻿<%@ Page Language="vb" AutoEventWireup="false"
    CodeBehind="BalanceSheetAccDetailEdit.aspx.vb" Inherits="Maxiloan.Webform.GL.BalanceSheetAccDetailEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  TagPrefix="uc" %> 
<%@ Register Src="../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"  TagPrefix="uc2" %> 
<%@ Register Src="../Webform.UserController/ucLookUpCustomerRental.ascx" TagName="ucLookUpCustomerRental" TagPrefix="uc12"%>
<%@ Register Src="../Webform.UserController/ucLookUpAssetRental.ascx" TagName="ucLookUpAssetRental" TagPrefix="uc13"%>
<!DOCTYPE html PUBLIC "-W3CDTD XHTML 1.0 TransitionalEN" "http:www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http:www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
   <asp:Label ID="Label1" runat="server"  ></asp:Label>
    <div id="jlookupContent" runat="server" />    
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REGISTRASI BALANCE SHEET ACCOUNT NUMBER
                    </h3>
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                    <label  class ="label_req">
                        Account Number</label>
                    <asp:DropDownList ID="ddlCOA" runat="server" width="700px"/>
                    <asp:RequiredFieldValidator ID="rfvCOA" runat="server" ControlToValidate="ddlCOA" 
                                              Display="Dynamic" CssClass="validator_general"  ErrorMessage="Pilih  Account Number" InitialValue="0" />
                </div>
            </div>--%>
            <div class="form_box">
                <div class="form_single">
                    <label>Account Id</label>
                    <asp:TextBox ID="txtNoAccountx" runat="server" class="clsNoAccountx" autocomplete="off" onchange="show()"  CssClass="form_box_hide"></asp:TextBox>
                    <asp:TextBox ID="txtAccountName" runat="server" class="clsAccountNam" autocomplete="off" CssClass="medium_text" Enabled="false" width="300px"></asp:TextBox>
                    <button class="small buttongo blue" 
                    onclick ="OpenLookup();return false;">...</button >                        
                    <asp:Button runat="server" ID="Jlookup" style="display:none" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label  class ="label_req">
                        Account Type</label>
                    <asp:DropDownList ID="ddlAccountType" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvAccountType" runat="server" ControlToValidate="ddlAccountType" 
                                              Display="Dynamic" CssClass="validator_general"  ErrorMessage="Pilih account type!" InitialValue="0" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                       Is Positif</label>
                    <asp:DropDownList ID="ddlIsPositif" runat="server">
                        <asp:ListItem Text="+ (Plus)" Value="0" />
                        <asp:ListItem Text="- (Minus)" Value="1" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                    <asp:HiddenField runat="server" ID="hdnReportGroupId" />
                    <asp:HiddenField runat="server" ID="hdnReportId" />
                    <asp:HiddenField runat="server" ID="hdnCoaId" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="small button gray" />
            </div>    

    <script type="text/javascript">

        $(document).ready(function () {
           
        })

       function OpenLookup()  {
                OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/CoaBalanceSheet.aspx?kodecid=" & txtNoAccountx.ClientID & "&namacid=" & txtAccountName.ClientID  & "&isDt=1") %>', 'DAFTAR Chart Of Account', '<%= jlookupContent.ClientID %>', 'DoPostBack')
    }
        
    </script>
</form>
</body>
</html> 