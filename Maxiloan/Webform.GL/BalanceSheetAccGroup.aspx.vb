﻿#Region "imports"

Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions


#End Region

Public Class BalanceSheetAccGroupList
    Inherits WebBased
    Private Const PageSize As Integer = 10
    Protected WithEvents GridNavigator As ucGridNav
    Private _recordCount As Integer = 1
    Private ReadOnly _bsController As New BalanceSheetSetupController
    Private _branchid As String
    Private _companyid As String
    Private _reportid As String
    Const PAR_RID As String = "rid"
    Private cmd As SqlCommand

    Private Property reportid() As String
        Get
            Return CType(ViewState("reportid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("reportid") = Value
        End Set
    End Property
    Private Property UpDownStatus As String
        Get
            Return CType(ViewState("UpDownStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("UpDownStatus") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = "BSSLIST"
        If SessionInvalid() Then
            Exit Sub
        End If
        _branchid = sesBranchId.Replace("'", "")
        BranchID = _branchid
        If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Return
        End If
        _companyid = GetCompanyId(GetConnectionString)
        SesCompanyID = _companyid
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not Page.IsPostBack Then

            If Request.QueryString(PAR_RID) <> String.Empty Then
                _reportid = Request.QueryString(PAR_RID)
                hdnReportId.Value = _reportid
                Me.reportid = Request.QueryString(PAR_RID)
                InitGrid()
            End If

        End If
    End Sub
    Private Function _dt(ByVal ParamArray param() As Object) As IList(Of BalanceSheetAccGroup)
        Return _bsController.SelectBSAccGroup(_recordCount, GetConnectionString(), param)
    End Function

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        If (hdIsFind.Value = "1") Then
            BindGridFind(e.CurrentPage)
        Else
            Dim rid = hdnReportId.Value
            DtgAsset.DataSource = _dt(New Object() {rid, e.CurrentPage, PageSize}) '_bsController.SelectBSAccGroup(_recordCount, GetConnectionString(),  New Object() {_reportid, e.CurrentPage, PageSize})
            DtgAsset.DataBind()
        End If

        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Private Sub InitGrid()
        DtgAsset.DataSource = _dt(New Object() {hdnReportId.Value, 1, PageSize}) '_bsController.SelectBSAccGroup(_recordCount, GetConnectionString(),  New Object() {_reportid,  1, PageSize})
        DtgAsset.DataBind()
        GridNavigator.Initialize(_recordCount, PageSize)
    End Sub

    Sub BindGridFind(currPage As Integer)
        Dim sWhare = txtCari.Text.Trim
        Dim rid = hdnReportId.Value
        'Dim _dt As IList(Of MasterAccountObject) = _bsController.SelectBSAccGroup(_recordCount, GetConnectionString(),  New Object() {_reportid, currPage, PageSize, opW, sWhare})
        DtgAsset.DataSource = _dt(New Object() {rid, currPage, PageSize, sWhare})
        DtgAsset.DataBind()
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        hdIsFind.Value = "1"
        GridNavigator.Initialize(_recordCount, PageSize)

    End Sub


#Region "CommandGrid"

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim cmd As String() = Split(e.CommandArgument.ToString, ",")
        If e.CommandName.ToLower = "edit" Then
            If e.CommandArgument.ToString <> String.Empty Then
                Response.Redirect("BalanceSheetAccGroupDetail.aspx?rid=" & cmd(1) & "&rgid=" & cmd(0))
            End If
        ElseIf e.CommandName.ToLower = "delete" Then
            deleteData(cmd(1), cmd(0))
        ElseIf e.CommandName.ToLower = "detail" Then
            If e.CommandArgument.ToString <> String.Empty Then
                If cmd(2).Trim = "Account" Then
                    Response.Redirect("BalanceSheetAccDetail.aspx?rid=" & cmd(1) & "&rgid=" & cmd(0))
                ElseIf cmd(2).Trim = "Group" Then
                    Response.Redirect("BalanceSheetGroupDetail.aspx?rid=" & cmd(1) & "&rgid=" & cmd(0))
                End If

            End If
        End If
    End Sub
#End Region

    Private Sub deleteData(ByVal rid As String, ByVal rgid As Integer)
        Try
            _bsController.DeleteBSAccGroup(GetConnectionString, rgid)
            Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & rid)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Response.Redirect("BalanceSheetAccGroupDetail.aspx?rid=" & hdnReportId.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & hdnReportId.Value)
    End Sub

    Private Sub DtgAsset_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        Dim btnEdit As ImageButton
        Dim btnDelete As ImageButton
        Dim btnDetail As ImageButton
        Dim lblisbar As Label

        If e.Item.ItemIndex >= 0 Then
            lblisbar = CType(e.Item.FindControl("lblisBar"), Label)
            btnEdit = CType(e.Item.FindControl("btnEdit"), ImageButton)
            btnDelete = CType(e.Item.FindControl("btnDelete"), ImageButton)
            btnDetail = CType(e.Item.FindControl("btnDetail"), ImageButton)
            btnDelete.Visible = True
            btnEdit.Visible = True
            btnDetail.Visible = True
            If (lblisbar.Text = "True") Or e.Item.DataItem.GroupType.ToString.Trim = "" Then
                btnDetail.Visible = False
            End If

        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("BalanceSheetSetup.aspx")
    End Sub

    Protected Sub DtgAsset_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAsset.ItemCommand
        Dim lblReportGroupId As New Label
        Dim lblReportId As New Label
        Dim btnUp As ImageButton
        Dim btnDown As ImageButton
        If e.Item.ItemIndex >= 0 Then
            lblReportGroupId = CType(e.Item.FindControl("lblReportGroupId"), Label)
            lblReportId = CType(e.Item.FindControl("lblReportId"), Label)
            btnUp = CType(e.Item.FindControl("btnUp"), ImageButton)
            btnDown = CType(e.Item.FindControl("btnDown"), ImageButton)

            If e.CommandName = "Up" Then
                Me.UpDownStatus = "Up"
            End If

            If e.CommandName = "Down" Then
                Me.UpDownStatus = "Down"
            End If

            Dim customClass As New Parameter.Implementasi
            lblMessage.Text = ""

            With customClass
                .strConnection = GetConnectionString()
                .GroupId = lblReportGroupId.Text.Trim
                .ReportId = lblReportId.Text.Trim
                .UpDown = Me.UpDownStatus
            End With

            EditUpDown(customClass)
            Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & Me.reportid)

        End If

    End Sub

    Public Sub EditUpDown(ByVal customclass As Parameter.Implementasi)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@GroupId", SqlDbType.Int)
            params(0).Value = customclass.GroupId
            params(1) = New SqlParameter("@ReportId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ReportId
            params(2) = New SqlParameter("@UpDown", SqlDbType.VarChar, 5)
            params(2).Value = customclass.UpDown

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spUpDownEdit", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()

            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

End Class