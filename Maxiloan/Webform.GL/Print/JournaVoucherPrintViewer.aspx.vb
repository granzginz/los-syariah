﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class JournaVoucherPrintViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property tr_nomor() As String
        Get
            Return CType(ViewState("tr_nomor"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("tr_nomor") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(ViewState("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PicJob() As String
        Get
            Return CType(ViewState("PicJob"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PicJob") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New DocReceiveController
    Private oCustomClass As New Parameter.DocRec
    Private oEntityGS As New Parameter.GeneralSetting
    Private m_UColl As New DataUserControlController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()

    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()

        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPAsset As New DataSet

        Dim objReport As PrintJournal = New PrintJournal
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhere
                .Tr_Nomor = Me.tr_nomor
                .BranchId = Me.Branch_ID
                .SortBy = Me.SortBy
            End With
            oCustomClass = m_coll.PrintJournalList(oCustomClass)
            dtsEntity = oCustomClass.listDoc
            objReport.SetDataSource(dtsEntity)

            CrystalReportViewer1.ReportSource = objReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()

            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "kwitansiJournalVoucher.pdf"
            DiskOpts.DiskFileName = strFileLocation
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
            objReport.Dispose()
            Response.Redirect("JournalVoucherPrint.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "kwitansiJournalVoucher")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
#End Region
#Region "Event"
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("JournalVoucherPrint")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.tr_nomor = cookie.Values("tr_nomor")
        Me.Branch_ID = cookie.Values("BranchID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class