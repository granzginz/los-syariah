﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class JournalVoucherPrint
    Inherits Maxiloan.Webform.WebBased
    Private oCustomClass As New Parameter.DocRec
    Private oController As New DocReceiveController
#Region "Declaration"
    Protected WithEvents outError As System.Web.UI.HtmlControls.HtmlGenericControl

#End Region

#Region "Constanta"
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private x_controller As New DataUserControlController
#End Region
#Region "Property"
    Private Property Date_From() As String
        Get
            Return CType(ViewState("datefrom"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("datefrom") = Value
        End Set
    End Property
    Private Property Date_To() As String
        Get
            Return CType(ViewState("dateto"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("dateto") = Value
        End Set
    End Property
    Private Property Transaction_Type() As String
        Get
            Return CType(ViewState("transactiontype"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("transactiontype") = Value
        End Set
    End Property
    Private Property Search_Type() As String
        Get
            Return CType(ViewState("searchtype"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("searchtype") = Value
        End Set
    End Property
    Private Property Search_Value() As String
        Get
            Return CType(ViewState("searchvalue"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("searchvalue") = Value
        End Set
    End Property

    Public ReadOnly Property datefrom() As String
        Get
            Return txtDateFrom.Text.Trim
        End Get
    End Property
    Public ReadOnly Property dateto() As String
        Get
            Return txtDateTo.Text.Trim
        End Get
    End Property
    Public ReadOnly Property transactiontype() As String
        Get
            Return ddlTransactionType.SelectedItem.Value
        End Get
    End Property
    Public ReadOnly Property searchtype() As String
        Get
            Return ddlSearchBy.SelectedItem.Value
        End Get
    End Property
    Public ReadOnly Property searchvalue() As String
        Get
            Return txtSearchBy.Text.Trim
        End Get
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Visible = False
        If Not IsPostBack Then
            cboBranch()
            Try

                ViewState("datefrom") = ""
                ViewState("dateto") = ""
                ViewState("transactiontype") = ""
                ViewState("searchtype") = ""
                ViewState("searchvalue") = ""

                Dim lobjpage As PrintJournalVoucherTransaction
                lobjpage = CType(Context.Handler, PrintJournalVoucherTransaction)
                With lobjpage
                    Me.Date_From = .datefrom
                    Me.Date_To = .dateto
                    Me.Transaction_Type = .transactiontype
                    Me.Search_Type = .searchtype
                    Me.Search_Value = .searchvalue
                End With
            Catch en As Exception
            End Try

            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strFileLocation As String
                strFileLocation = "../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
               & "var x = screen.width; var y = screen.height - 100; window.open('" & strFileLocation & "','accacq', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
               & "</script>")
            End If

            If Me.Date_From <> "" Then
                txtDateFrom.Text = Me.Date_From
            Else
                txtDateFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            End If

            If Me.Date_To <> "" Then
                txtDateTo.Text = Me.Date_To
            Else
                txtDateTo.Text = BusinessDate.ToString("dd/MM/yyyy")
            End If

            transaction_list()
            If Me.Search_Type <> "" Then
                ddlSearchBy.ClearSelection()
                ddlSearchBy.Items.FindByValue(Me.Search_Type.Trim).Selected = True
            End If
            Me.SearchBy = ""
            Me.SortBy = ""
            PnlPaging.Visible = False



        End If


    End Sub
#End Region
    Sub cboBranch()
        With oBranch
            If Me.IsHoBranch Then
                .DataSource = x_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "ALL"
                .Enabled = True
            Else
                .DataSource = x_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Enabled = False
            End If

        End With
    End Sub
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgIndType.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        Bindgrid()
    End Sub
#End Region
#Region "DtgIndType_ItemCommand"
    Protected Sub DtgIndType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgIndType.ItemCommand
        If e.CommandName = "ShowView" Then
            Server.Transfer("PrintJournalVoucherTransaction.aspx?tr_nomor=" & CStr(DtgIndType.DataKeys(e.Item.ItemIndex)) & "", False)
        End If
    End Sub
#End Region
#Region "Bindgrid"
    Sub Bindgrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        Dim i As Integer
        Dim strSQL As String
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .SpName = "spJournalVoucher"
        End With
        oContract = cContract.GetGeneralPaging(oContract)

        DtUserList = oContract.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oContract.TotalRecords
        Try
            DtgIndType.DataSource = DvUserList
            DtgIndType.DataBind()
        Catch en As System.Web.HttpException
            DtgIndType.CurrentPageIndex = 0
            DtgIndType.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            txtGoPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid()
            End If
        End If
    End Sub
#End Region
#Region "imgSearch"
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click

        PnlPaging.Visible = True
        Me.SearchBy = ""
        Me.SearchBy = " Branch.BranchID = '" & oBranch.SelectedValue.ToString.Trim & "' "

        If ddlTransactionType.SelectedIndex > 0 Then
            Me.SearchBy &= " and transactionid='" & ddlTransactionType.SelectedItem.Value & "' "
        End If
        If txtDateTo.Text <> "" Then
            Me.SearchBy &= " and tr_date >= '" & ConvertDate2(txtDateFrom.Text).ToString("yyyyMMdd") & "' "
        End If
        If txtDateFrom.Text <> "" Then
            Me.SearchBy &= " and tr_date <= '" & ConvertDate2(txtDateTo.Text).ToString("yyyyMMdd") & "' "
        End If

        If ddlSearchBy.SelectedIndex > 0 Then
            If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy &= " and " & ddlSearchBy.SelectedValue.Trim & " like '" & txtSearchBy.Text.Trim & "'"
            Else
                Me.SearchBy &= " and " & ddlSearchBy.SelectedValue.Trim & " = '" & txtSearchBy.Text.Trim & "'"
            End If
        End If
        Bindgrid()
    End Sub
#End Region
#Region "TransactionList"
    Private Sub transaction_list()
        Dim objcon As New SqlConnection(GetConnectionString)
        Dim objcommand As New SqlCommand
        Dim objread As SqlDataReader
        Dim strSql As String
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcommand.Connection = objcon
            strSql = " select distinct rtrim(GLH.transactionid) as transactionid , Rtrim(TT.Description) as TransactionType from " &
                     "	dbo.gljournalh GLH " &
                     "	Inner join glmastersequence TT " &
                     "	On GLH.TransactionID = TT.TransactionID order by TransactionType asc "
            objcommand.CommandText = strSql
            objcommand.CommandType = CommandType.Text

            objread = objcommand.ExecuteReader

            ddlTransactionType.DataSource = objread

            ddlTransactionType.DataTextField = "TransactionType"
            ddlTransactionType.DataValueField = "transactionid"
            ddlTransactionType.DataBind()
            ddlTransactionType.Items.Insert(0, "Select One")
            ddlTransactionType.Items(0).Value = "0"
            objread.Close()
            ddlTransactionType.ClearSelection()
            If Me.Transaction_Type <> "" Then
                ddlTransactionType.SelectedIndex = ddlTransactionType.Items.IndexOf(ddlTransactionType.Items.FindByValue(Me.Transaction_Type.Trim))
            Else
                ddlTransactionType.SelectedIndex = 0
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objcommand.Dispose()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()
        Me.SearchBy = " Branch.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' "
        If ddlTransactionType.SelectedIndex > 0 Then
            Me.SearchBy &= " and transactionid='" & ddlTransactionType.SelectedItem.Value & "' "
        End If
        If txtDateTo.Text <> "" Then
            Me.SearchBy &= " and tr_date >= '" & ConvertDate2(txtDateTo.Text).ToString("yyyyMMdd") & "' "
        End If
        If txtDateFrom.Text <> "" Then
            Me.SearchBy &= " and tr_date <= '" & ConvertDate2(txtDateFrom.Text).ToString("yyyyMMdd") & "' "
        End If
        If ddlSearchBy.SelectedIndex > 0 Then
            If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy &= " and " & ddlSearchBy.SelectedValue.Trim & " like '" & txtSearchBy.Text.Trim & "'"
            Else
                Me.SearchBy &= " and " & ddlSearchBy.SelectedValue.Trim & " = '" & txtSearchBy.Text.Trim & "'"
            End If
        End If

        Dim cookie As HttpCookie = Request.Cookies("PrintJournal")
        If Not cookie Is Nothing Then
            cookie.Values("PageFrom") = "PrintJournal"
            cookie.Values("CmdWhere") = Me.SearchBy
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("PrintJournal")
            cookieNew.Values.Add("PageFrom", "PrintJournal")
            cookieNew.Values.Add("CmdWhere", Me.SearchBy)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region
#Region "imbPrint"
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chkPrint As CheckBox
        Dim lblhytransactionid As HyperLink
        Dim lbltr_nomor As New Label
        Dim hasil As Integer
        Dim cmdwhere As String
        Dim strMultiTr_Nomor As String = ""
        Dim strMultiBranch As String = ""

        With oDataTable
            .Columns.Add(New DataColumn("BranchID", GetType(String)))
            .Columns.Add(New DataColumn("tr_nomor", GetType(String)))
        End With
        With DtgIndType
            For intloop = 0 To .Items.Count - 1
                chkPrint = CType(.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblhytransactionid = CType(.Items(intloop).Cells(1).FindControl("hytransactionid"), HyperLink)
                lbltr_nomor = CType(.Items(intloop).Cells(1).FindControl("lbltr_nomor"), Label)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("BranchID") = oBranch.SelectedItem.Value
                    oRow("tr_nomor") = lbltr_nomor.Text.Trim
                    oDataTable.Rows.Add(oRow)
                    strMultiBranch = oBranch.SelectedItem.Value
                    If strMultiTr_Nomor.Trim = "" Then
                        strMultiTr_Nomor = strMultiTr_Nomor + "'" + lbltr_nomor.Text.Trim + "'"
                    Else
                        strMultiTr_Nomor = strMultiTr_Nomor + ",'" + lbltr_nomor.Text.Trim + "'"
                    End If
                End If
            Next
        End With

        cmdwhere = "tr_nomor in(" + strMultiTr_Nomor + ")"
        If oDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap Pilih Item", True)
            Exit Sub
        End If

        cmdwhere = cmdwhere & " and BranchID='" & oBranch.SelectedItem.Value & "'"
            Dim cookie As HttpCookie = Request.Cookies("JournalVoucherPrint")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("tr_nomor") = strMultiTr_Nomor
                cookie.Values("BranchID") = strMultiBranch
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("JournalVoucherPrint")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("tr_nomor", strMultiTr_Nomor)
                cookieNew.Values.Add("BranchID", strMultiBranch)
                cookieNew.Values.Add("sortby", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If

        Response.Redirect("JournaVoucherPrintViewer.aspx")

    End Sub
#End Region

End Class