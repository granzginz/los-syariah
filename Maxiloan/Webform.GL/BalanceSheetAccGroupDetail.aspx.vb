﻿#Region "imports"
Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class BalanceSheetAccGroupDetail
    Inherits WebBased


#Region "properties"
    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CBool(ViewState("isNew"))
        End Get
    End Property
#End Region

    Const FORM_ID As String = "BSSLIST"
    Const PAR_RID As String = "rid"
    Const PAR_RGID As String = "rgid"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = FORM_ID
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then

            isNew = True
            bindDdlBalanceSheetGroup()
            bindddlGroupType()
            Dim prid = Request.QueryString(PAR_RID)
            hdnReportId.Value = prid
            hdnReportGroupId.Value = 0

            If Request.QueryString(PAR_RGID) <> String.Empty Then
                Dim prgid = Request.QueryString(PAR_RGID)
                loaddata(prgid, prid)
                isNew = False
            End If
        End If
    End Sub

    Private Sub loaddata(ByVal prgid As Integer, rid As String)

        Dim bsCon As New BalanceSheetSetupController
        Dim result As BalanceSheetAccGroup = bsCon.FindByIdBSAccGroup(GetConnectionString, prgid)

        If (result Is Nothing) Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            Return
        End If

        txtGroupDesc.Text = result.groupDesc
        ddlParentGroup.Text = result.ParentGroupName

        txtGroupId.Text = result.ParentGroupName
        txtGroupName.Text = ddlParentGroup.SelectedItem.Text

        ddlGroupType.Text = result.groupType
        hdnReportGroupId.Value = prgid
        hdnReportId.Value = rid
        chkisBar.Checked = result.IsBar

        txtGroupDesc.Enabled = True
        chkisBar.Enabled = True
        ddlParentGroup.Enabled = False
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & hdnReportId.Value)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            If txtGroupId.Text = "" Then
                txtGroupId.Text = 0
            End If

            Dim bsCon As New BalanceSheetSetupController
            Dim bsdata As New BalanceSheetAccGroup(CInt(hdnReportGroupId.Value), hdnReportId.Value, txtGroupDesc.Text, txtGroupId.Text, chkisBar.Checked, 0, ddlGroupType.Text, 0, UserID, UserID)
            bsdata.ReportId = hdnReportId.Value

            If isNew Then
                bsCon.InsertBSAccGroup(GetConnectionString, bsdata)
            Else
                bsCon.UpdateBSAccGroup(GetConnectionString, bsdata)
            End If

            Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & hdnReportId.Value)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    Private Sub bindDdlBalanceSheetGroup()
        Dim m_controller As New BalanceSheetSetupController
        With ddlParentGroup
            .DataSource = m_controller.SelectAllBSAccGroup(GetConnectionString)
            .DataValueField = "ReportGroupID"
            .DataTextField = "GroupDesc"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Private Sub bindddlGroupType()
        Dim listGroupType() As String = {"Select One", "Account", "Group"}
        With ddlGroupType
            .DataSource = listGroupType
            .DataBind()
        End With
    End Sub

End Class