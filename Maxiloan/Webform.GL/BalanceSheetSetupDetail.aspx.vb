﻿#Region "imports"
Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class BalanceSheetSetupDetail
    Inherits WebBased


#Region "properties"
    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CBool(ViewState("isNew"))
        End Get
    End Property
#End Region
    Const FORM_ID As String = "BSSLIST"
    Const PAR_RID As String = "rid"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = FORM_ID
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then

            isNew = True

            If Request.QueryString(PAR_RID) <> String.Empty Then
                Dim prid = Request.QueryString(PAR_RID)
                loaddata(prid)
                isNew = False
            End If
        End If
    End Sub

    Private Sub loaddata(ByVal rid As String)

        Dim bsCon As New BalanceSheetSetupController
        Dim result As BalanceSheetSetup = bsCon.FindById(GetConnectionString, rid, SesCompanyID)

        If (result Is Nothing) Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            Return
        End If

        txtReportID.Text = result.ReportId
        txtReportTitle.Text = result.ReportTitle
        txtReportText1.Text = result.ReportText1
        txtReportText2.Text = result.ReportText2
        chkPrintLastMonth.Checked = result.IsPrintLastMonth
        chkPrintVariance.Checked = result.IsPrintVariance

        txtReportID.Enabled = False
        txtReportTitle.Enabled = True
        txtReportText1.Enabled = True
        txtReportText2.Enabled = True
        chkPrintLastMonth.Enabled = True
        chkPrintVariance.Enabled = True
    End Sub


    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("BalanceSheetSetup.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim bsCon As New BalanceSheetSetupController
            Dim bsdata As New BalanceSheetSetup(txtReportID.Text, txtReportTitle.Text, txtReportText1.Text, txtReportText2.Text, chkPrintLastMonth.Checked, chkPrintVariance.Checked, SesCompanyID, UserID, UserID)
            bsdata.CompanyId = SesCompanyID

            If isNew Then
                bsCon.InsertData(GetConnectionString, bsdata)
            Else
                bsCon.UpdateData(GetConnectionString, bsdata)
            End If

            Response.Redirect("BalanceSheetSetup.aspx")

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


End Class