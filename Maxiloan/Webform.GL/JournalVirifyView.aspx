﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="JournalVirifyView.aspx.vb" Inherits="Maxiloan.Webform.GL.JournalVirifyView" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="VarifikasiTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"/> 
 
<%--<uct:tabs id='cTabs' runat='server'></uct:tabs>--%>
<div class="form_title">
    <div class="title_strip" ></div>
    <div class="form_single">
        <h3>
            VERIFIKASI JOURNAL
        </h3>
    </div>
</div>
 <asp:Panel id='pnlsearch' runat='server'>
 <div class="form_box">
    <div class="form_single">
        <label>Tanggal Transaksi</label>
        <uc1:ucDateCE ID="txtPeriodeFrom" runat="server" /> 
    </div>
</div>
   <div class="form_box">
    <div class="form_single">
        <label class="label_req"> Cabang</label>
        <asp:DropDownList ID="ddlCabang" runat="server"/> 
        <asp:RequiredFieldValidator ID="rfvCabang" runat="server" Display="Dynamic" CssClass="validator_general" ControlToValidate="ddlCabang" ErrorMessage="pilih cabang"  InitialValue="0" />
    </div>
 </div>
     <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue" />
    </div>
</asp:Panel>
<%--
<asp:Panel runat = "server" ID="pnlGrid"> 
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">  
            <asp:GridView  ID="dtgCSV" runat="server"  CssClass="grid_general"   showfooter="true"  DataKeyNames="TransactionNo" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%"    >
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"  /> 
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" /> 
            <Columns>
                <asp:TemplateField ItemStyle-Width="9" >
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn" ImageUrl="images/Plus.gif" CommandName="Expand" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="9" Visible="false" >
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkUpdateStatus" class="checkBoxClass" ToolTip="Select this file "   Checked='<%#eval("CanApprove")%>'/>
                   </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField  DataField="TransactionNo" HeaderText="No Transaksi" />
                <asp:BoundField  DataField="TransactionDate" HeaderText="Tanggal Transaksi"  DataFormatString= "{0:d}" />
                <asp:BoundField  DataField="CanApprove"  visible="false"/>
                <asp:BoundField  DataField="TransactionType" HeaderText="Transaksi" />
                <asp:BoundField  DataField="Description" HeaderText="Keterangan" />

                <asp:templatefield headertext="Debet" >
                        <itemtemplate>  <div class="t-right"><%#Eval("TotalDebit", "{0:#,0}") %> </div> </itemtemplate>
                        <footertemplate>
                            <div class="t-right">   <asp:Label ID="lblTotalDebit" runat="server" /> </div>
                        </footertemplate>
                </asp:templatefield> 
                <asp:templatefield headertext="Credit" >
                        <itemtemplate> <div class="t-right"><%#Eval("TotalCredit", "{0:#,0}") %> </div></itemtemplate>
                            <footertemplate>
                                 <div class="t-right"> <asp:Label ID="lblTotalCredit" runat="server" /> </div>
                             </footertemplate>
                </asp:templatefield>  
                <asp:TemplateField> 
                <ItemTemplate> 
                <asp:PlaceHolder ID="objPHOrderDetails" runat="server" >
                <tr>
                <td width="9" />
                <td colspan="7">
                    <asp:UpdatePanel runat="server" ID="ChildControl">
                    <ContentTemplate>
                        <asp:GridView   ID="grdDetails" CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"    AllowSorting="false" Width="98%"  runat="server">
                            <HeaderStyle CssClass="th tr-det" />
                            <Columns>
                                <asp:BoundField DataField="CoaId" HeaderText="COA" /> 
                                    <asp:BoundField DataField="CoaName" HeaderText="COA Name"  /> 
                                    <asp:BoundField DataField="TrDesc" HeaderText="Description"  /> 
                                <asp:BoundField DataField="DebitAmount" HeaderText="Debit"  /> 
                                <asp:BoundField DataField="CreditAmount" HeaderText="Credit"  />  
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    </asp:UpdatePanel >
                    </td>
                    </asp:PlaceHolder>     
                    </ItemTemplate>
                    </asp:TemplateField>    
              </Columns>
            </asp:GridView >
             <uc2:ucGridNav id="GridNavigator" runat="server"/>
            </div>
        </div>
        
        <div class="form_button" id="divAppr" runat="server">
          <asp:Button ID="btnApprove" runat="server"   CausesValidation="False" Text="Approve" CssClass="small button blue"/> 
        </div> 
        
        <div id="divmessages" runat="server">
            <asp:Label ID="lblErrMsg" runat="server" /> 
        </div>

    </div> 
</asp:Panel>



<asp:Panel runat = "server" ID="pnlIInvalidGrid"> 
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">  
            <asp:GridView  ID="grdInvalid" runat="server"  CssClass="grid_general"   showfooter="true"  DataKeyNames="TransactionNo" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%"    >
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"  /> 
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" /> 
            <Columns>
                <asp:TemplateField ItemStyle-Width="9" >
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn" ImageUrl="images/Plus.gif" CommandName="Expand" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="9" Visible="false" >
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkUpdateStatus" class="checkBoxClass" ToolTip="Select this file "   Checked='<%#eval("CanApprove")%>'/>
                   </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField  DataField="TransactionNo" HeaderText="No Transaksi" />
                <asp:BoundField  DataField="TransactionDate" HeaderText="Tanggal Transaksi"  DataFormatString= "{0:d}" />
                <asp:BoundField  DataField="CanApprove"  visible="false"/>
                <asp:BoundField  DataField="TransactionType" HeaderText="Transaksi" />
                <asp:BoundField  DataField="Description" HeaderText="Keterangan" />

                <asp:templatefield headertext="Debet" >
                        <itemtemplate>  <div class="t-right"><%#Eval("TotalDebit", "{0:#,0}") %> </div> </itemtemplate>
                        <footertemplate>
                            <div class="t-right">   <asp:Label ID="lblTotalDebit" runat="server" /> </div>
                        </footertemplate>
                </asp:templatefield> 
                <asp:templatefield headertext="Credit" >
                        <itemtemplate> <div class="t-right"><%#Eval("TotalCredit", "{0:#,0}") %> </div></itemtemplate>
                            <footertemplate>
                                 <div class="t-right"> <asp:Label ID="lblTotalCredit" runat="server" /> </div>
                             </footertemplate>
                </asp:templatefield>  
                <asp:TemplateField> 
                <ItemTemplate> 
                <asp:PlaceHolder ID="objPHOrderDetails" runat="server" >
                <tr>
                <td width="9" />
                <td colspan="7">
                    <asp:UpdatePanel runat="server" ID="ChildControl">
                    <ContentTemplate>
                        <asp:GridView   ID="grdInvalidDetails" CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"    AllowSorting="false" Width="98%"  runat="server">
                            <HeaderStyle CssClass="th tr-det" />
                            <Columns>
                                <asp:BoundField DataField="CoaId" HeaderText="COA" /> 
                                    <asp:BoundField DataField="CoaName" HeaderText="COA Name"  /> 
                                    <asp:BoundField DataField="TrDesc" HeaderText="Description"  /> 
                                <asp:BoundField DataField="DebitAmount" HeaderText="Debit"  /> 
                                <asp:BoundField DataField="CreditAmount" HeaderText="Credit"  />  
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    </asp:UpdatePanel >
                    </td>
                    </asp:PlaceHolder>     
                    </ItemTemplate>
                    </asp:TemplateField>    
              </Columns>
            </asp:GridView >
             <uc2:ucGridNav id="InvalidGridNavigator" runat="server"/>
            </div>
        </div>
        
         <div class="form_button" id="divAppr" runat="server">
          <asp:Button ID="btnApprove" runat="server"   CausesValidation="False" Text="Approve" CssClass="small button blue"/> 
        </div> 
        
        <div id="div1" runat="server">
            <asp:Label ID="Label1" runat="server" /> 
        </div>

    </div> 
</asp:Panel>--%>

</asp:Content>
