﻿ 
 <%@ Page Title="Jurnal Voucher List" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master"CodeBehind="JournalVoucherList.aspx.vb"
 Inherits="Maxiloan.Webform.GL.JournalVoucherList" %>

<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
    <asp:UpdatePanel runat="server" ID="UpdatePanel">
        <ContentTemplate> 
        <asp:Panel runat="server" ID="pnlSearch">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            PENCARIAN JURNAL TRANSAKSI
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Periode</label>
                        <uc1:ucDateCE ID="txtPeriodeFrom" runat="server" />
                        S/D
                        <uc1:ucDateCE ID="txtPeriodeTo" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Jenis Transaksi</label>
                        <asp:DropDownList ID="ddlJenisTransaksi" runat="server" /> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Voucher</label>
                        <asp:TextBox ID="txtCari" runat="server" placeholder="No Voucher"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnFind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue"/> 
                    <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"/> 
                </div>    
            </asp:Panel>    

            <asp:Panel runat = "server" ID="pnlGrid">
                <div class="form_title">
                    <div class="form_single">
                        <h3>
                            HOLD JURNAL TRANSAKSI
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="False" AutoGenerateColumns="false"
                                DataKeyField="TR_Nomor" CssClass="grid_general" AllowPaging="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnHold" runat="server" ImageUrl="../Images/IconProhibition.png" CommandName="Hold"
                                                OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("TR_Nomor") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="description" HeaderText="TRANSAKSI" />
                                    <asp:BoundColumn DataField="Tr_Nomor" HeaderText="NO VOUCHER" />
                                    <asp:BoundColumn DataField="Tr_date" HeaderText="TANGGAL" dataformatstring="{0:dd/MM/yyyy}"   />
                                    <asp:BoundColumn DataField="Tr_desc" HeaderText="KETERANGAN" />
                                    <asp:BoundColumn DataField="status_tr" HeaderText="STATUS" /> 
                                    <asp:BoundColumn DataField="reff_no" HeaderText="NO REFF" /> 
                                </Columns>
                                <PagerStyle Visible="False"  Mode="NumericPages"/>
                            </asp:DataGrid>
                            <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content> 
