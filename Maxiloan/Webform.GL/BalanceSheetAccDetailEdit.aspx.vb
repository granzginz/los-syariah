﻿#Region "imports"
Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization

#End Region

Public Class BalanceSheetAccDetailEdit
    Inherits WebBased
    Shared constr As String

#Region "properties"
    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CBool(ViewState("isNew"))
        End Get
    End Property
#End Region

    Const FORM_ID As String = "BSSLIST"
    Const PAR_RID As String = "rid"
    Const PAR_RGID As String = "rgid"
    Const PAR_COAID As String = "coa"
#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = FORM_ID
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then

            isNew = True
            'Awal Sebelum DiModifi Untuk Pemilihan Account COA
            'bindDdlCOA()

            With ddlAccountType
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataSource = ModuleGlHelper.GetGLBalanceSheetAccTypeList()
                .DataBind()
            End With

            Dim CoaId = Request.QueryString(PAR_COAID)
            Dim prgid = Request.QueryString(PAR_RGID)
            Dim ReportGroupId = prgid

            'LoadcboCOA(GetConnectionString, ReportGroupId)

            hdnReportGroupId.Value = prgid
            hdnCoaId.Value = CoaId

            If Request.QueryString(PAR_COAID) <> String.Empty Then
                'loaddata(prgid, CoaId)
                display(CoaId)
                isNew = False
            End If

        End If
    End Sub

    'Private Sub loaddata(ByVal prgid As Integer, coaid As String)

    '    Dim bsCon As New BalanceSheetSetupController
    '    Dim result As BalanceSheetAccDetail = bsCon.FindByIdBSAccDetail(GetConnectionString, prgid, coaid)

    '    If (result Is Nothing) Then
    '        ShowMessage(lblMessage, "Data tidak ditemukan", True)
    '        Return
    '    End If

    '    ddlAccountType.SelectedValue = result.accType
    '    ddlIsPositif.SelectedValue = result.IsPositif
    '    'ddlCOA.SelectedValue = result.CoaId
    '    txtNoAccountx.Text = result.CoaId
    '    txtAccountName.Text = result.CoaDescription
    '    hdnCoaId.Value = coaid
    '    hdnReportGroupId.Value = prgid


    '    'ddlCOA.Enabled = False
    '    ddlAccountType.Enabled = True
    'End Sub
    Public Sub display(ByVal coaid As String)
        Try
            Dim dtslist As DataTable
            Dim cReceive As New GeneralPagingController
            Dim oReceive As New Parameter.GeneralPaging
            With oReceive
                .strConnection = GetConnectionString()
                .WhereCond = coaid
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spBalanceByIDSelect"
            End With
            oReceive = cReceive.GetGeneralPaging(oReceive)
            dtslist = oReceive.ListData
            If dtslist.Rows.Count > 0 Then
                If dtslist.Rows(0).Item("AccountType") <> "" Then
                    ddlAccountType.SelectedValue = CStr(dtslist.Rows(0).Item("AccountType")).Trim
                End If
                If dtslist.Rows(0).Item("CoaId") <> "" Then
                    txtNoAccountx.Text = CStr(dtslist.Rows(0).Item("CoaId")).Trim
                End If
                If dtslist.Rows(0).Item("CoaDescription") <> "" Then
                    txtAccountName.Text = CStr(dtslist.Rows(0).Item("CoaDescription")).Trim
                End If
                ddlIsPositif.SelectedValue = dtslist.Rows(0).Item("IsPositif")
                ddlAccountType.Enabled = True
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("BalanceSheetAccDetail.aspx?rgid=" & hdnReportGroupId.Value)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim bsCon As New BalanceSheetSetupController
            Dim bsdata As New BalanceSheetAccDetail(CInt(hdnReportGroupId.Value), txtNoAccountx.Text, CType(CInt(ddlAccountType.SelectedValue), GLBalanceSheetAccountType), txtNoAccountx.Text, ddlIsPositif.SelectedValue)

            If isNew Then
                bsCon.InsertBSAccDetail(GetConnectionString, bsdata)
            Else
                bsCon.UpdateBSAccDetail(GetConnectionString, bsdata)
            End If

            Response.Redirect("BalanceSheetAccDetail.aspx?rgid=" & hdnReportGroupId.Value)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    'Private Sub bindDdlCOA()
    '    Dim m_controller As New BalanceSheetSetupController
    '    With ddlCOA
    '        .DataSource = m_controller.SelectAllCOA(GetConnectionString)
    '        .DataValueField = "CoaId"
    '        .DataTextField = "Description"
    '        .DataBind()
    '        .Items.Insert(0, "Select One")
    '        .Items(0).Value = "0"
    '    End With
    'End Sub



End Class