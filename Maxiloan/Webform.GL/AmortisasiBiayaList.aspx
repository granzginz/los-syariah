﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AmortisasiBiayaList.aspx.vb"
    Inherits="Maxiloan.Webform.GL.AmortisasiBiayaList" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AmortisasiBiaya</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                Daftar Skema Jurnal Yang Amortisasikan
            </h3>
        </div>
    </div> 
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList1" runat="server"  > 
        <div class="form_box"> 
            <div class="form_left">
                <Label> Tr Nomor </Label>
                <asp:label ID="lblTr_Nomor" runat="server"></asp:label>
            </div>
            <div class="form_right">
                <Label> Tgl Mulai </Label>
                <asp:TextBox ID="txtTglMulai" runat="server" />
                <aspajax:CalendarExtender ID="calExTglMulai" runat="server" TargetControlID="txtTglMulai"
                Format="dd/MM/yyyy" />
            </div>
        </div>
        <div class ="form_box">
            <div class="form_left">
                <Label> No Reference </Label>
                <asp:TextBox ID="txtNoReference" runat="server" Width="15%" > </asp:TextBox>   
            </div>
            <div class="form_right">
                <Label> Tgl Akhir </Label>
                <asp:TextBox ID="txtTglAkhir" runat="server" ></asp:TextBox>
                <aspajax:CalendarExtender ID="calExTglAkhir" runat="server" TargetControlID="txtTglAkhir"
                Format="dd/MM/yyyy" />
            </div>
        </div> 
        <div class ="form_box">
            <div class="form_left">
                <Label> Tgl Efektif </Label>
                <asp:TextBox ID="txtTglEfektif" runat="server" ></asp:TextBox>
                <aspajax:CalendarExtender ID="calExTglEfektif" runat="server" TargetControlID="txtTglEfektif"
                Format="dd/MM/yyyy" />
            </div>
            <div class="form_right">
                <Label> Nilai Amortisasi </Label>
                <asp:label ID="lblNilaiAmortisasi" runat="server" ></asp:label>
            </div>  
        </div>  
        <div class ="form_box"> 
            <div class="form_left">
                <Label> Nilai </Label>
                <asp:label ID="txtNilai" runat="server" ></asp:label> 
            </div>    
        </div>
        <div class="form_box">
            <div class="form_left">
                <Label> Jangka Waktu </Label>
                <asp:TextBox ID="txtTglJangkaWaktu" runat="server" Width="5%" > </asp:TextBox>  
            </div>           
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req"> Di Approve Oleh</label>
                <asp:DropDownList ID="cboApprovedBy" runat="server"   />
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic" ControlToValidate="cboApprovedBy" ErrorMessage="Harap diisi di Approve Oleh" 
                    InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
         <div class="form_button">
                <asp:Button ID="BtnPreview" runat="server" Text="Preview" CssClass="small button blue"></asp:Button>
        </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server"  > 
        <div class="form_box_header">
            <div class="form_single">
                <label>
                    Detail Amortisasi Biaya
                </label>
            </div>
        </div>
            <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" AllowSorting="True"
                        OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="Tanggal">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <%--<asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%#Container.DataItem("No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%> 
                            <asp:TemplateColumn SortExpression="Tanggal" HeaderText="Tanggal" Visible="true">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%#Container.DataItem("Tanggal")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="Nilai" HeaderText="Nilai">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Container.DataItem("Amount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DEBET" HeaderText="DEBET">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDEBET" runat="server" Text='<%#Container.DataItem("DEBET")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DEBETDESC" HeaderText="DEBET DESC">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDEBETDESC" runat="server" Text='<%#Container.DataItem("DEBETDESC")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="CREDIT" HeaderText="CREDIT">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCREDIT" runat="server" Text='<%#Container.DataItem("CREDIT")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="CREDITDESC" HeaderText="CREDIT DESC">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCREDITDESC" runat="server" Text='<%#Container.DataItem("CREDITDESC")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div> 
            <div class="form_button">
                <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
        </div>
        </asp:Panel>
         

    </form>
</body>
</html>
