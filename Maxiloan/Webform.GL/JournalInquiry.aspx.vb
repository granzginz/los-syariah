﻿
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Public Class JournalInquiry
    Inherits WebBased
    Protected WithEvents GridNavigator As ucGridNav
    Private cContract As New GeneralPagingController
    Private m_controller As New DataUserControlController
    Private oContract As New Parameter.GeneralPaging
    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    Property tr_nomor() As String
        Get
            Return ViewState("tr_nomor").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("tr_nomor") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If


        Me.FormID = "EDITJRN"

        If SessionInvalid() Then
            Exit Sub
        End If
        'If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
        '    Return
        'End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If

        AddHandler GridNavigator.PageChanged, AddressOf Navevent

        If Not IsPostBack Then

            BindGlMasterSequenceDropdownWithNone(ddlTransactionType, GetConnectionString, sesBranchId.Replace("'", ""))

            txtDateFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            txtDateTo.Text = BusinessDate.ToString("dd/MM/yyyy")
            bindSearchBranch()

            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses Debit Credit Balance.", False)
            ElseIf Request("s") = "2" Then
                ShowMessage(lblMessage, "Sukses Debit Credit Tidak Balance.", False)
            End If

            Me.SearchBy = ""
            Me.SortBy = ""
            PnlPaging.Visible = False

        End If


    End Sub

    Private Sub bindSearchBranch()
        With ddlCabang
            .DataSource = m_controller.GetBranchAll(GetConnectionString)
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "ALL"
        End With
    End Sub

    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Me.SearchBy = "1=1"
        If ddlCabang.SelectedValue.Trim <> "ALL" Then
            'Me.SearchBy = " and Branch.BranchID = '" & ddlCabang.SelectedValue.Trim & "' "
            Me.SearchBy = " Branch.BranchID = '" & ddlCabang.SelectedValue.Trim & "' "
            If ddlTransactionType.SelectedIndex > 1 Then
                Me.SearchBy &= " and transactionid='" & ddlTransactionType.SelectedItem.Value & "' "
                Me.SearchBy &= " and cast(tr_date as date) >= '" & ConvertDate2(txtDateFrom.Text).ToString("yyyyMMdd") & "' "
                Me.SearchBy &= " and cast(tr_date as date) <= '" & ConvertDate2(txtDateTo.Text).ToString("yyyyMMdd") & "' "
            Else
                Me.SearchBy = Me.SearchBy
                Me.SearchBy &= " and cast(tr_date as date) >= '" & ConvertDate2(txtDateFrom.Text).ToString("yyyyMMdd") & "' "
                Me.SearchBy &= " and cast(tr_date as date) <= '" & ConvertDate2(txtDateTo.Text).ToString("yyyyMMdd") & "' "
            End If
        Else
            If ddlTransactionType.SelectedIndex > 1 Then
                Me.SearchBy &= " and transactionid='" & ddlTransactionType.SelectedItem.Value & "' "
                Me.SearchBy &= " and cast(tr_date as date) >= '" & ConvertDate2(txtDateFrom.Text).ToString("yyyyMMdd") & "' "
                Me.SearchBy &= " and cast(tr_date as date) <= '" & ConvertDate2(txtDateTo.Text).ToString("yyyyMMdd") & "' "
            Else
                Me.SearchBy = Me.SearchBy
                Me.SearchBy &= " and cast(tr_date as date) >= '" & ConvertDate2(txtDateFrom.Text).ToString("yyyyMMdd") & "' "
                Me.SearchBy &= " and cast(tr_date as date) <= '" & ConvertDate2(txtDateTo.Text).ToString("yyyyMMdd") & "' "
            End If
        End If

        If ddlSearchBy.SelectedIndex > 0 Then
            If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                Me.SearchBy &= " and " & ddlSearchBy.SelectedValue.Trim & " like '" & txtSearchBy.Text.Trim & "'"
            Else
                Me.SearchBy &= " and " & ddlSearchBy.SelectedValue.Trim & " like '" & txtSearchBy.Text.Trim & "'"
            End If
        End If
        PnlPaging.Visible = True
        Bindgrid(1)
    End Sub

    Sub Bindgrid(currentPage As Integer, Optional totalPage As Integer = 0, Optional paging As Boolean = False)

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.SortBy
            .SpName = "spJournal1"
        End With
        oContract = cContract.GetGeneralPaging(oContract)


        If (paging) Then
            GridNavigator.ReInitialize(currentPage, oContract.TotalRecords, paging)
        Else
            GridNavigator.Initialize(oContract.TotalRecords, PageSize)
        End If
        Try
            DtgIndType.DataSource = oContract.ListData.DefaultView
            DtgIndType.DataBind()
        Catch en As System.Web.HttpException
            ShowMessage(lblMessage, en.Message, True)
        End Try

    End Sub

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        Bindgrid(e.CurrentPage, e.TotalPage, True)
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    'Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
    '    Dim cookie As HttpCookie = Request.Cookies("Journal")
    '    If Not cookie Is Nothing Then
    '        cookie.Values("PageFrom") = "Journal"
    '        cookie.Values("CmdWhere") = Me.SearchBy
    '        Response.AppendCookie(cookie)
    '    Else
    '        Dim cookieNew As New HttpCookie("Journal")
    '        cookieNew.Values.Add("PageFrom", "Journal")
    '        cookieNew.Values.Add("CmdWhere", Me.SearchBy)
    '        Response.AppendCookie(cookieNew)
    '    End If
    '    Response.Redirect("Report/JournalRpt.aspx")
    'End Sub
    Private Sub DtgIndType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgIndType.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkBatchNo = CType(e.Item.FindControl("lnkBatchNo"), HyperLink)
            lnkBatchNo.NavigateUrl = String.Format("javascript:OpenWinViewInquiry('ACCMNT','{0}')", lnkBatchNo.Text.Trim)


            Dim STATus = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "status"))
            Dim IsAutoJournal = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "IsAutoJournal"))
            If IsAutoJournal = 0 Then
                If Not (STATus = "OPEN" Or STATus = "HOLD") Then
                    CType(e.Item.FindControl("lnkEdit"), LinkButton).Enabled = False
                End If
            ElseIf IsAutoJournal = 1 Then
                CType(e.Item.FindControl("lnkEdit"), LinkButton).Enabled = False
            End If
        End If
    End Sub
    Private Sub DtgIndType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgIndType.ItemCommand
        If e.CommandName.Trim = "SelectEdit" Then
            Dim hyRequestNo = CType(e.Item.FindControl("lnkBatchNo"), HyperLink).Text.Trim
            Dim hyBranchId = CType(e.Item.FindControl("lnkBranchId"), HyperLink).Text.Trim
            'Server.Transfer("JournalVoucherEdit.aspx?nv=" & hyRequestNo.Trim & "&MyBranch=" & hyBranchId.Trim & "", False)
            Server.Transfer("JournalVoucherEdit.aspx?nv=" & hyRequestNo.Trim & "", False)
        End If
    End Sub
End Class