﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Linq
Public Class JournalVoucherEdit
    Inherits WebBased
    Private ReadOnly _jvController As New JournalVoucherController
    Protected WithEvents txtTanggalVoucher As ucDateCE
    Protected WithEvents txtTanggalReference As ucDateCE
    Const FORM_ID As String = "JVDETAILEDIT"
    Const PAR_ACC As String = "nv"
    Private m_controller As New AssetDataController
    Protected WithEvents luCOA As ucLookUpCOA2
    Protected WithEvents txtJumlah As ucNumberFormat
    Protected WithEvents txtJumlah_1 As ucNumberFormat
    Dim totalDebit As Double
    Dim totalCredit As Double
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

    Property dt_detail As DataTable
        Set(ByVal value As DataTable)
            ViewState("dt_detail") = value
        End Set
        Get
            Return CType(ViewState("dt_detail"), DataTable)
        End Get
    End Property

    Property isEditDetail As Boolean
        Set(ByVal value As Boolean)
            ViewState("isEditDetail") = value
        End Set
        Get
            Return CType(ViewState("isEditDetail"), Boolean)
        End Get
    End Property


    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CType(ViewState("isNew"), Boolean)
        End Get
    End Property
    Private Property Voucher As GlJournalVoucher
        Set(ByVal value As GlJournalVoucher)
            ViewState("GlJournalVoucher") = value
        End Set
        Get
            If ViewState("GlJournalVoucher") Is Nothing Then
                ViewState("GlJournalVoucher") = New GlJournalVoucher
            End If
            Return CType(ViewState("GlJournalVoucher"), GlJournalVoucher)
        End Get
    End Property
    Private Property ReffNomor() As String
        Get
            Return CType(ViewState("ReffNomor"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ReffNomor") = Value
        End Set
    End Property
    Private Property key() As String
        Get
            Return CType(ViewState("key"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("key") = Value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "EDITJRN"

        If SessionInvalid() Then
            Exit Sub
        End If

        If Me.IsHoBranch = False Then
            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
        End If

        constr = GetConnectionString()

        If (String.IsNullOrEmpty(BranchID)) Then
            BranchID = sesBranchId.Replace("'", "")
        End If

        If (String.IsNullOrEmpty(SesCompanyID)) Then
            SesCompanyID = GetCompanyId(GetConnectionString)
        End If

        If Not Page.IsPostBack Then

            txtTanggalReference.IsRequired = False
            txtTanggalReference.Text = BusinessDate.ToString("dd/MM/yyyy")
            txtTanggalVoucher.IsRequired = True
            txtTanggalVoucher.Text = BusinessDate.ToString("dd/MM/yyyy")
            txtJumlah.RangeValidatorEnable = True
            txtJumlah.RangeValidatorMinimumValue = "1"
            txtJumlah.rv.ValidationGroup = "entry"
            txtJumlah.rv.ErrorMessage = "Jumlah transaksi harus besar dari 0."
            BindDdlBranch(ddlCabang, GetConnectionString, sesBranchId, IsHoBranch)

            BindGlMasterSequenceDropdown(ddlJenisTransaksi, GetConnectionString, sesBranchId.Replace("'", ""))
            bindddlReference()
            isNew = True

            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses Debit Credit Balance.", False)
            ElseIf Request("s") = "2" Then
                ShowMessage(lblMessage, "Sukses Debit Credit Tidak Balance.", True)
            End If

            If Request.QueryString(PAR_ACC) <> String.Empty Then
                DoLoadData(Request.QueryString(PAR_ACC))
                isNew = False
                HiddenField1.Value = ddlCabang.SelectedValue
                If ddlReference.SelectedValue = "" Then
                    ddlReference.SelectedValue = "CV"
                End If
            Else
                dtg.DataSource = Voucher.JournalItems
                dtg.DataBind()
            End If
            pnlEntry.Visible = True
            PanelSave.Visible = False
        End If


    End Sub

    Private Sub bindddlReference()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spDropDownReffType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With ddlReference
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub DoLoadData(ByVal trNomor As String)
        Dim result = _jvController.GetJurnalVoucherByTrNo(GetConnectionString(), trNomor)
        If result Is Nothing Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            Return
        End If

        Voucher = result

        ddlCabang.SelectedValue = result.BranchId
        txtKeterangan.Text = result.TransactionDesc
        ddlJenisTransaksi.SelectedValue = result.TransactionId
        txtNoReference.Text = result.ReffNomor
        ddlReference.SelectedValue = result.ReffDesc

        txtNoVoucher.Text = result.TransactionNomor
        txtTanggalVoucher.Text = Format(result.ReffDate, "dd/MM/yyyy").ToString
        txtTanggalReference.Text = Format(result.TransactionDate, "dd/MM/yyyy").ToString
        dtg.DataSource = result.JournalItems
        dtg.DataBind()

    End Sub


#Region "CommandGrid_Click"
    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim dt As JournalVoucherItem = Voucher.JournalItems.First(Function(x) x.ItemKey.Trim = e.CommandArgument.ToString.Trim)
        Select Case e.CommandName.ToLower
            Case "delete"
                Voucher.JournalItems.Remove(dt)
                dtg.DataSource = Voucher.JournalItems
                dtg.DataBind()
        End Select
    End Sub

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Dim err As String
        Dim lblUnit As New Label
        Dim lblCustomer As New Label
        Dim lblBranchNme As New Label

        If e.CommandName = "Copy" Then
            Dim d As JournalVoucherItem = Voucher.JournalItems.First(Function(x) x.ItemKey.Trim = e.CommandArgument.ToString.Trim)

            key = ""

            rblDK1.SelectedValue = d.Post
            If rblDK1.SelectedValue = "D" Then
                txtJumlah.Text = d.Amount
            Else
                txtJumlah.Text = d.Amount
            End If
            txtNoAccountx.Text = d.CoaId
            txtAccountName.Text = d.CoaName
            txtKeterangandet.Text = d.TransactionDesc
        ElseIf e.CommandName = "Update_" Then
            Dim d As JournalVoucherItem = Voucher.JournalItems.First(Function(x) x.ItemKey.Trim = e.CommandArgument.ToString.Trim)
            pnlEntry.Visible = True
            PanelSave.Visible = False
            key = d.ItemKey

            rblDK1.SelectedValue = d.Post
            If rblDK1.SelectedValue = "D" Then
                txtJumlah.Text = d.Amount
            Else
                txtJumlah.Text = d.Amount
            End If
            txtNoAccountx.Text = d.CoaId
            txtAccountName.Text = d.CoaName
            txtKeterangandet.Text = d.TransactionDesc
        End If
    End Sub
#End Region


    Private Sub clearEntry()
        txtNoAccountx.Text = String.Empty
        txtAccountName.Text = String.Empty
        txtKeterangandet.Text = String.Empty
        txtJumlah.Text = "0"

        lblCoaTaxId.Text = String.Empty
        lblCoaTaxName.Text = String.Empty
        txtKeteranganTax.Text = String.Empty
        txtJumlah_1.Text = "0"
        Me.key = ""

        rblDK1.SelectedIndex = 0
        txtSequence.Value = String.Empty

        tr_display.Attributes.Add("class", "tr-display tr-display-hide")
        isEditDetail = False
    End Sub

    Public Function DebitOrKredit(ByVal post As String, ByVal amount As Decimal, ByVal dOrK As String) As String
        If post.ToLower = dOrK.ToLower Then
            Return amount.ToString("N")
        Else
            Return "0"
        End If
    End Function

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("JournalInquiry.aspx")
    End Sub
    Private Sub btnCloseEntry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseEntry.Click
        pnlEntry.Visible = False
        PanelSave.Visible = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txtNoReference.Text.Trim = "" Then
                ShowMessage(lblMessage, "Harap Input No. Reference", True)
                Exit Sub
            End If

            If Voucher.JournalItems.Count = 0 Then
                ShowMessage(lblMessage, "Input detail terlebih dahulu", True)
                Return
            End If

            If Not Voucher.IsBalance Then
                ShowMessage(lblMessage, "Debit credit tidak balance", True)
                Return
            End If

            If Not Voucher.IsItemCoaValid Then
                ShowMessage(lblMessage, "Terdapat Item dengan COA kosong.", True)
                Return
            End If

            Dim ypCont = New GlYearPeriodController()

            Dim prd = ypCont.YearPeriods(GetConnectionString, SesCompanyID, sesBranchId.Replace("'", ""), ConvertDate2(txtTanggalVoucher.Text).Year.ToString, ConvertDate2(txtTanggalVoucher.Text).Month.ToString)
            If (prd.Count = 0) Then
                ShowMessage(lblMessage, "Tanggal Periode transaksi telah close / belum di open.", True)
                Exit Sub
            End If


            Voucher.BranchId = ddlCabang.SelectedValue
            Voucher.TransactionDesc = txtKeterangan.Text
            Voucher.TransactionId = ddlJenisTransaksi.SelectedValue
            Voucher.ReffNomor = txtNoReference.Text
            Voucher.BusinessDate = ConvertDate2(txtTanggalVoucher.Text)
            Voucher.ReffDate = ConvertDate2(txtTanggalReference.Text)
            Voucher.TransactionDate = ConvertDate2(txtTanggalVoucher.Text)
            Voucher.ReffDesc = ddlReference.SelectedValue


            If (isNew) Then
                Voucher.CompanyId = SesCompanyID
                Voucher.TransactionNomor = txtNoReference.Text.Trim
            End If

            Dim component As IList(Of GlJournalVoucher) = New List(Of GlJournalVoucher)()
            component.Add(Voucher)

            Dim result = _jvController.GlJournalAddUpdate(GetConnectionString(), isNew, component)

            If (result <> "OK") Then
                ShowMessage(lblMessage, result, False)
                Exit Sub
            End If

            Response.Redirect("JournalInquiry.aspx?s=1")

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteEntry.Click
        Try
            Voucher.ReffNomor = txtNoReference.Text
            If (isNew) Then
                Voucher.CompanyId = SesCompanyID
                Voucher.TransactionNomor = "Auto"
            End If

            Dim component As IList(Of GlJournalVoucher) = New List(Of GlJournalVoucher)()
            component.Add(Voucher)

            Dim result = _jvController.GlJournalAddDelete(GetConnectionString(), isNew, component)


            If (result = "Hapus") Then
                ShowMessage(lblMessage, "Data Berhasil Di Hapus", True)
            Else
                ShowMessage(lblMessage, "Data Tidak Berhasil Di Hapus", False)
            End If

            Response.Redirect("JournalInquiry.aspx?s=1")

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub Lookup_Click(sender As Object, e As System.EventArgs) Handles Jlookup.Click
        Dim TypeCoa As String

        TypeCoa = Strings.Left(txtNoAccountx.Text, 1)

        If TypeCoa = "6" Then
            pnlEntry.Visible = True
            PanelSave.Visible = False
        Else
            pnlEntry.Visible = True
            PanelSave.Visible = False
        End If
    End Sub
    Private Sub ButtonAdd_Clik(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        pnlEntry.Visible = True
        PanelSave.Visible = False
    End Sub

    Private Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToList.Click

        If Me.key = "" Then
            Try
                Dim newEntry As Boolean = (txtSequence.Value = String.Empty)
                Dim txtAddFree As String = String.Empty
                lblMessage.Visible = False
                Dim newid = BitConverter.ToInt64(Guid.NewGuid().ToByteArray, 0)
                Dim jv = New JournalVoucherItem(newid, SesCompanyID, sesBranchId.Replace("'", ""), System.DateTime.Now.Ticks, txtNoAccountx.Text, txtAccountName.Text, txtKeterangandet.Text, rblDK1.SelectedValue, CDec(txtJumlah.Text), txtAddFree)

                Dim lineOfText As String = txtNoAccountx.Text
                Dim parentArray As String() = lineOfText.Split("-")


                If ddlCabang.SelectedValue <> parentArray(1).Trim Then
                    ShowMessage(lblMessage, "Coa harus sama dengan cabang bersangkutan..!!", True)
                    Exit Sub
                End If


                Voucher.JournalItems.Add(jv)
                dtg.DataSource = Voucher.JournalItems
                dtg.DataBind()
                clearEntry()

            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Else
            Dim key_ = Me.key.ToString()
            Dim dt As JournalVoucherItem = Voucher.JournalItems.First(Function(x) x.ItemKey.Trim = key_.Trim)
            With dt
                .CoaId = txtNoAccountx.Text
                .CoaName = txtAccountName.Text
                .Amount = CDec(txtJumlah.Text)
                .TransactionDesc = txtKeterangandet.Text
                .Post = rblDK1.SelectedValue
            End With

            Dim lineOfText As String = txtNoAccountx.Text
            Dim parentArray As String() = lineOfText.Split("-")


            If ddlCabang.SelectedValue <> parentArray(1).Trim Then
                ShowMessage(lblMessage, "Coa harus sama dengan cabang bersangkutan..!!", True)
                Exit Sub
            End If


            dtg.DataSource = Voucher.JournalItems
            dtg.DataBind()
            clearEntry()
        End If

    End Sub

    Private Sub dgProblems_EditCommand(ByVal source As Object,
                                 ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) _
                             Handles dtg.EditCommand
        dtg.EditItemIndex = e.Item.ItemIndex
        dtg.DataSource = Voucher.JournalItems
        dtg.DataBind()
    End Sub
    Private Sub dgProblems_CancelCommand(ByVal source As Object,
                                  ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) _
                              Handles dtg.CancelCommand
        dtg.EditItemIndex = -1
        dtg.DataSource = Voucher.JournalItems
        dtg.DataBind()
    End Sub
    Private Sub dgProblems_UpdateCommand(ByVal source As Object,
                                  ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) _
                              Handles dtg.UpdateCommand

        Dim key = dtg.DataKeys(e.Item.ItemIndex).ToString()
        Dim keterangan = CType(e.Item.FindControl("txtTransactionDesc"), TextBox).Text()
        Dim Coa = CType(e.Item.FindControl("txtCOA"), TextBox).Text()
        Dim name = CType(e.Item.FindControl("txtNameCoa"), TextBox).Text()
        Dim dc = CType(e.Item.FindControl("DropDownList1"), DropDownList).Text()
        If dc = "D" Then
            Dim amountD = IIf(CType(e.Item.FindControl("DropDownList1"), DropDownList).Text().ToLower = dc,
                        CType(e.Item.FindControl("txtJumlahCredit"), TextBox).Text,
                        CType(e.Item.FindControl("txtJumlahDebit"), TextBox).Text)
            Dim dt As JournalVoucherItem = Voucher.JournalItems.First(Function(x) x.ItemKey.Trim = key.Trim)

            With dt
                .CoaId = Coa
                .CoaName = name
                .Amount = amountD
                .TransactionDesc = keterangan
                .Post = dc
            End With
        ElseIf dc = "C" Then
            Dim amountC = IIf(CType(e.Item.FindControl("DropDownList1"), DropDownList).Text().ToLower = dc,
                         CType(e.Item.FindControl("txtJumlahDebit"), TextBox).Text,
                         CType(e.Item.FindControl("txtJumlahCredit"), TextBox).Text)
            Dim dt As JournalVoucherItem = Voucher.JournalItems.First(Function(x) x.ItemKey.Trim = key.Trim)

            With dt
                .CoaId = Coa
                .CoaName = name
                .Amount = amountC
                .TransactionDesc = keterangan
                .Post = dc
            End With
        End If

        dtg.EditItemIndex = -1
        dtg.DataSource = Voucher.JournalItems
        dtg.DataBind()
    End Sub

    Private Sub btnCancelEntry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelEntry.Click
        clearEntry()
    End Sub
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Try
            Dim hdnSequence As HiddenField
            Dim lblTotalCredit As Label
            Dim lblTotalDebit As Label
            Dim lblSelisih As Label
            Dim lblDC As Label
            Dim txtDC As TextBox
            Dim DropDownList1 As DropDownList
            Dim lblDebit As Label
            Dim lblCredit As Label

            If e.Item.ItemIndex >= 0 Then
                DropDownList1 = CType(e.Item.FindControl("DropDownList1"), DropDownList)
                lblDC = CType(e.Item.FindControl("lblDC"), Label)
                If (dtg.EditItemIndex > -1) Then
                    If (dtg.EditItemIndex = e.Item.ItemIndex) Then
                        CType(e.Item.FindControl("txtJumlahDebit"), TextBox).Enabled = IIf(DropDownList1.SelectedValue = "d", True, True)
                        CType(e.Item.FindControl("txtJumlahCredit"), TextBox).Enabled = IIf(DropDownList1.SelectedValue = "c", True, True)
                    End If
                Else

                    lblDebit = CType(e.Item.FindControl("lblDebit"), Label)
                    lblCredit = CType(e.Item.FindControl("lblCredit"), Label)

                    If lblDC.Text.ToLower = "c" Then
                        totalCredit = totalCredit + CType(lblCredit.Text, Decimal)
                    ElseIf lblDC.Text.ToLower = "d" Then
                        totalDebit = totalDebit + CType(lblDebit.Text, Decimal)
                    End If

                    hdnSequence = CType(e.Item.FindControl("hdnSequence"), HiddenField)
                    hdnSequence.Value = (e.Item.ItemIndex + 1).ToString
                End If

            End If

            If e.Item.ItemType = ListItemType.Footer Then
                lblTotalCredit = CType(e.Item.FindControl("lblTotalCredit"), Label)
                lblTotalCredit.Text = FormatNumber(totalCredit, 0)


                lblTotalDebit = CType(e.Item.FindControl("lblTotalDebit"), Label)
                lblTotalDebit.Text = FormatNumber(totalDebit, 0)

                lblSelisih = CType(e.Item.FindControl("lblSelisih"), Label)
                lblSelisih.Text = FormatNumber(totalDebit - totalCredit, 0)
            End If



        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Shared constr As String

    <System.Web.Services.WebMethod()>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetCoaIds(pre As String) As List(Of String)
        Dim coaCon As New COAController

        Return coaCon.AutoComplateCoaId(constr, pre)
    End Function
    <System.Web.Services.WebMethod()>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetCoaDescs(pre As String) As List(Of String)
        Dim coaCon As New COAController

        Return coaCon.AutoComplateCoaDesc(constr, pre)
    End Function

    <System.Web.Services.WebMethod()>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetCoaId(pre As String) As MasterAccountObject
        Dim coaCon As New COAController
        Dim ret As MasterAccountObject = coaCon.FindById(constr, pre, "", "")
        If Not (ret Is Nothing) Then
            Return ret
        End If
        Return New MasterAccountObject
    End Function
    <System.Web.Services.WebMethod()>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetCoaDesc(pre As String) As MasterAccountObject
        Dim coaCon As New COAController
        Dim ret As MasterAccountObject = coaCon.FindByDesc(constr, pre, "", "")
        If Not (ret Is Nothing) Then
            Return ret
        End If
        Return New MasterAccountObject
    End Function

End Class