﻿
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter

Public Class JournalVoucherHold
    Inherits WebBased
    Private ReadOnly _jvController As New JournalVoucherController

    Const FORM_ID As String = "JVHOLD"
    Const PAR_ACC As String = "nv"

    Protected WithEvents luCOA As ucLookUpCOA2
    Protected WithEvents txtJumlah As ucNumberFormat

    Dim totalDebit As Double
    Dim totalCredit As Double

    Property dt_detail As DataTable
        Set(ByVal value As DataTable)
            ViewState("dt_detail") = value
        End Set
        Get
            Return CType(ViewState("dt_detail"), DataTable)
        End Get
    End Property

    Property isEditDetail As Boolean
        Set(ByVal value As Boolean)
            ViewState("isEditDetail") = value
        End Set
        Get
            Return CType(ViewState("isEditDetail"), Boolean)
        End Get
    End Property

    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CType(ViewState("isNew"), Boolean)
        End Get
    End Property

    Private Property Voucher As GlJournalVoucher
        Set(ByVal value As GlJournalVoucher)
            ViewState("GlJournalVoucher") = value
        End Set
        Get
            If ViewState("GlJournalVoucher") Is Nothing Then
                ViewState("GlJournalVoucher") = New GlJournalVoucher
            End If
            Return CType(ViewState("GlJournalVoucher"), GlJournalVoucher)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = FORM_ID

        If SessionInvalid() Then
            Exit Sub
        End If
#If Not Debug Then
        If Not CheckForm(Loginid, FormID, "MAXILOAN") Then
            Return
        End If
#End If
        If (String.IsNullOrEmpty(BranchID)) Then
            BranchID = sesBranchId.Replace("'", "")
        End If
        If (String.IsNullOrEmpty(SesCompanyID)) Then
            SesCompanyID = GetCompanyId(GetConnectionString)
        End If


        If Not Page.IsPostBack Then
           
            isNew = True
            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses.", False)
            End If

            If Request.QueryString(PAR_ACC) <> String.Empty Then
                DoLoadData(Request.QueryString(PAR_ACC))
                isNew = False
            Else
                dtg.DataSource = Voucher.JournalItems
                dtg.DataBind()
            End If

        End If

    End Sub

    Private Sub DoLoadData(ByVal trNomor As String)
        Dim result = _jvController.GetJurnalVoucherByTrNo(GetConnectionString(), trNomor)
        If result Is Nothing Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            Return
        End If

        Voucher = result

        dto.Value = Server.UrlDecode(Request.QueryString("dto"))
        dfrom.Value = Server.UrlDecode(Request.QueryString("dfrom"))
        jtran.Value = Server.UrlDecode(Request.QueryString("jtran"))
        accno.Value = Server.UrlDecode(Request.QueryString("accno"))

        lblCabang.Text = result.BranchId + " - " + GetBranchName(GetConnectionString, result.BranchId)
        lblKeterangan.Text = result.TransactionDesc
        lblJenisTransaksi.Text = result.TransactionId
        lblNoReference.Text = result.ReffNomor

        lblNoVoucher.Text = result.TransactionNomor
        lblTanggalVoucher.Text = Format(result.ReffDate, "dd/MM/yyyy").ToString
        lblTanggalReference.Text = Format(result.TransactionDate, "dd/MM/yyyy").ToString

        dtg.DataSource = result.JournalItems
        dtg.DataBind()

    End Sub

    Private Sub clearEntry()
        txtJumlah.Text = "0"
        txtSequence.Value = String.Empty
        isEditDetail = False
    End Sub

    Public Function DebitOrKredit(ByVal post As String, ByVal amount As Decimal, ByVal dOrK As String) As String
        If post.ToLower = dOrK.ToLower Then
            Return amount.ToString("N")
        Else
            Return "0"
        End If
    End Function

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("JournalVoucherList.aspx?dfrom=" & Server.UrlEncode(dfrom.Value) & "&dto=" & Server.UrlEncode(dto.Value) & "&jtran=" & Server.UrlEncode(jtran.Value) & "&accno=" & Server.UrlEncode(accno.Value))
    End Sub

    Private Sub btnHold_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHold.Click
        Try
            Dim result = _jvController.GetJurnalVoucherByTrNo(GetConnectionString(), lblNoVoucher.Text.Trim)
            If result Is Nothing Then
                ShowMessage(lblMessage, "Data tidak ditemukan", True)
                Return
            End If

            Dim component As IList(Of GlJournalVoucher) = New List(Of GlJournalVoucher)()
            component.Add(result)

            Dim ex = _jvController.GlJournalHold(GetConnectionString(), component)

            If (ex <> "OK") Then
                ShowMessage(lblMessage, ex, True)
            Else
                Response.Redirect("JournalVoucherList.aspx?s=1&dfrom=" & Server.UrlEncode(dfrom.Value) & "&dto=" & Server.UrlEncode(dto.Value) & "&jtran=" & Server.UrlEncode(jtran.Value) & "&accno=" & Server.UrlEncode(accno.Value))
            End If


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
   

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Try
            Dim hdnSequence As HiddenField
            Dim lblTotalCredit As Label
            Dim lblTotalDebit As Label
            Dim lblSelisih As Label
            Dim lblDC As Label
            Dim lblDebit As Label
            Dim lblCredit As Label

            If e.Item.ItemIndex >= 0 Then
                lblDC = CType(e.Item.FindControl("lblDC"), Label)
                lblDebit = CType(e.Item.FindControl("lblDebit"), Label)
                lblCredit = CType(e.Item.FindControl("lblCredit"), Label)

                If lblDC.Text.ToLower = "c" Then
                    totalCredit = totalCredit + CType(lblCredit.Text, Decimal)
                ElseIf lblDC.Text.ToLower = "d" Then
                    totalDebit = totalDebit + +CType(lblDebit.Text, Decimal)
                End If

                hdnSequence = CType(e.Item.FindControl("hdnSequence"), HiddenField)
                hdnSequence.Value = (e.Item.ItemIndex + 1).ToString
            End If

            If e.Item.ItemType = ListItemType.Footer Then
                lblTotalCredit = CType(e.Item.FindControl("lblTotalCredit"), Label)
                lblTotalCredit.Text = FormatNumber(totalCredit, 0)


                lblTotalDebit = CType(e.Item.FindControl("lblTotalDebit"), Label)
                lblTotalDebit.Text = FormatNumber(totalDebit, 0)

                lblSelisih = CType(e.Item.FindControl("lblSelisih"), Label)
                lblSelisih.Text = FormatNumber(totalDebit - totalCredit, 0)
            End If


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


End Class