﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="JournalVoucherHold.aspx.vb"
    Inherits="Maxiloan.Webform.GL.JournalVoucherHold" %>

<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  TagPrefix="uc" %> 
<%@ Register Src="../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"  TagPrefix="uc2" %> 
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
        <div id="jlookupContent" runat="server" />
         <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate> 
             <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
             <asp:HiddenField runat="server" ID="dfrom" />
             <asp:HiddenField runat="server" ID="dto" />
             <asp:HiddenField runat="server" ID="jtran" />
             <asp:HiddenField runat="server" ID="accno" />
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h3> HOLD JURNAL TRANSAKSI </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> Cabang</label>
                    <asp:Label ID="lblCabang" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label> Keterangan</label>
                    <asp:Label ID="lblKeterangan" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> Transaksi</label>
                    <asp:Label ID="lblJenisTransaksi" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>No. Reference</label>
                    <asp:Label ID="lblNoReference" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> No Voucher</label>
                    <asp:Label ID="lblNoVoucher" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Tanggal Reference</label>
                    <asp:Label ID="lblTanggalReference" runat="server"></asp:Label>
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Voucher</label>
                    <asp:Label ID="lblTanggalVoucher" runat="server"></asp:Label>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="txtSequence" />
            <div class="form_title">
                <div class="form_single">
                    <h3> DETAIL TRANSAKSI </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtg" runat="server" AllowSorting="False" AutoGenerateColumns="false"
                            DataKeyField="ItemKey" CssClass="grid_general" ShowFooter="true">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnPers" />
                                        <asp:HiddenField runat="server" ID="hdnCab" />
                                        <asp:HiddenField runat="server" ID="hdnCoA" />
                                        <asp:HiddenField runat = "server" ID="hdnSequence" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PERS/CAB/COA">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPerCabCoA" Text='<%# eval("PerCabCoA") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KETERANGAN">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblKeterangan" Text='<%# eval("TransactionDesc") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="D/C">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblDC" Text='<%# eval("Post") %>'/>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Selisih : &nbsp; <asp:Label ID="lblSelisih" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DEBIT" HeaderStyle-CssClass ="th_right" ItemStyle-CssClass="th_right" FooterStyle-CssClass  ="th_right">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblDebit" Text='<%# formatnumber(iif(eval("Post")="C",0,eval("Amount")),0) %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalDebit" runat="server" /> 
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CREDIT" HeaderStyle-CssClass ="th_right" ItemStyle-CssClass="th_right" FooterStyle-CssClass  ="th_right">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblCredit"  Text='<%# formatnumber(iif(eval("Post")="D",0,eval("Amount")),0) %>'/>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalCredit" runat="server" /> 
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnHold" runat="server" CausesValidation="true" Text="Hold" CssClass="small button blue" ValidationGroup="header"/>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
            </div>
   </ContentTemplate>
    </asp:UpdatePanel> 
    
<%--    <script type="text/javascript">

    $(document).ready(function() {
        $('#ContentPlaceHolder1_txtNoAccount').change(function (e) { alert('test'); });
    });
</script> --%>
    </asp:Content> 