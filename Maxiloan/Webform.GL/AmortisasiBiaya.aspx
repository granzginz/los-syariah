﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AmortisasiBiaya.aspx.vb"
    Inherits="Maxiloan.Webform.GL.AmortisasiBiaya" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AmortisasiBiaya</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                Daftar Skema Jurnal Yang Amortisasikan
            </h3>
        </div>
    </div> 
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDatagrid" runat="server"  > 
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" DataKeyField="Tr_Nomor"
                        AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemStyle HorizontalAlign="Left" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypRequest" runat="server" Text='PILIH'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="Tr_Nomor" HeaderText="TR NOMOR" Visible="true">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTr_Nomor" runat="server" Text='<%#Container.DataItem("Tr_Nomor")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="Tr_Desc" HeaderText="TR DESC">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTr_Desc" runat="server" Text='<%#Container.DataItem("Tr_Desc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DEBET" HeaderText="DEBET">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDEBET" runat="server" Text='<%#Container.DataItem("CoaDebit")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="CREDIT" HeaderText="CREDIT">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCREDIT" runat="server" Text='<%#Container.DataItem("CoaCredit")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="Nilai" HeaderText="Nilai">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNilai" runat="server" Text='<%#Container.DataItem("Amount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                            CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div> 
    </asp:Panel>

    <asp:Panel ID="pnlList" runat="server">
        
        <div class="form_title">
            <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cbosearch" runat="server">
                    <asp:ListItem>Tr_Nomor</asp:ListItem>
                    <asp:ListItem>Debit</asp:ListItem>
                    <asp:ListItem>Credit</asp:ListItem>
                </asp:DropDownList>

                <asp:TextBox ID="txtsearch" runat="server" ></asp:TextBox>
            </div>
        </div>
        
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        Nama
                </label>
                <asp:TextBox ID="txtNama" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="medium_text">COA Debet</label>
                <asp:TextBox ID ="txtCOADebet" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="medium_text">COA Kredit</label>
                <asp:TextBox ID ="txtCOAKredit" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
    </asp:Panel>

    <asp:Panel ID="PnlEdit" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        Seq No
                </label>
                <asp:TextBox ID="txtSeqNo" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        Nama
                </label>
                <asp:TextBox ID="txtNama1" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="medium_text">COA Debet</label>
                <asp:TextBox ID ="txtCOADebet2" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class ="form_box">
            <div class ="form_single">
                <label class="medium_text">COA Kredit</label>
                <asp:TextBox ID ="txtCOAKredit2" runat ="server" Width ="10%" MaxLength ="100"
                    Columns ="105" readonly ="true"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_button">
                <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
    </asp:Panel>

    </form>
</body>
</html>
