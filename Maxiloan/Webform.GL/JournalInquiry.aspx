﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" 
    MasterPageFile="GL.Master" CodeBehind="JournalInquiry.aspx.vb" 
    Inherits="Maxiloan.Webform.GL.JournalInquiry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"/> 
    <script language="javascript" type="text/javascript">
        function delaction() {
            if (confirm("Apakah yakin mau hapus data ini ? ")) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenWinViewInquiry(pStyle, tr_nomor) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.GL/JournalInquiryView.aspx?style=' + pStyle + '&tr_nomor=' + tr_nomor, 'userlookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
								
    </script>
 
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                JURNAL TRANSAKSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Periode </label>
            <asp:TextBox runat="server" ID="txtDateFrom"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDateFrom" Format="dd/MM/yyyy"> </asp:CalendarExtender>
            &nbsp;S/D&nbsp;
            <asp:TextBox runat="server" ID="txtDateTo"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDateTo" Format="dd/MM/yyyy"></asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label">
                Jenis Transaksi
            </label>
            <asp:DropDownList ID="ddlTransactionType" runat="server" />
        </div>
    </div>
        <div class="form_box">
        <div class="form_single">
                    <label class="label"> Cabang</label>
                    <asp:DropDownList ID="ddlCabang" runat="server"/> 
                </div>
                 </div>
    <div class="form_box">
        <div class="form_single">
            <label> Cari Berdasarkan </label>
            <asp:DropDownList ID="ddlSearchBy" runat="server">
                <asp:ListItem Value="SO" Selected="True">Select One</asp:ListItem>
                <asp:ListItem Value="GLH.tr_nomor">No Transaksi</asp:ListItem>
                <asp:ListItem Value="reff_no">No Reff</asp:ListItem>
                <asp:ListItem Value="tr_desc">Keterangan</asp:ListItem>
                <asp:ListItem Value="GLH.JournalAmount">Jumlah</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchBy" runat="server" Width="314px"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnSearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue">
        </asp:Button>
    </div>
     <asp:Panel ID="PnlPaging" runat="server" Visible="True">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgIndType" runat="server" CssClass="grid_general"  
                             AutoGenerateColumns="False" AllowPaging="false" 
                             AllowSorting="false"  DataKeyNames="tr_nomor" DataKeyField="tr_nomor"
                            BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="tr_nomor" HeaderText="EDIT" ItemStyle-Width ="100" > 
                                <ItemTemplate>
                                    <%--<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument="<%# Container.DataItemIndex %>"  CommandName="SelectEdit" Text='SELECT' CausesValidation="False" />--%>
                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="SELECT"  CommandName="SelectEdit" CausesValidation="False"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="transactionid" SortExpression="transactionid" HeaderText="TRANSAKSI" ItemStyle-Width="200"   />
                            <asp:TemplateColumn SortExpression="tr_nomor" HeaderText="NO VOUCHER" ItemStyle-Width="150" > 
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBatchNo" runat="server" CommandName="ShowView" Text='<%# Container.DataItem("tr_nomor")%>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="tr_date" HeaderText="TANGGAL" ItemStyle-Width="100" > 
                                <ItemTemplate>
                                    <asp:Label ID="lbl_date" runat="server"> <%# format(Container.DataItem("tr_date"),"dd MMM yyyy")%> </asp:Label>
                                    <input type="hidden" id="hid_date" runat="server" value='<%# container.dataitem("tr_date")%>' name="hid_date" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="tr_desc" HeaderText="KETERANGAN">
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="JournalAmount" HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblJournalAmount" runat="server" Text='<%#formatnumber(Container.DataItem("JournalAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="status" HeaderText="STATUS" ItemStyle-Width ="50" />
                            <asp:BoundColumn DataField="reff_no" HeaderText="NO REFF" ItemStyle-Width="20%" />
                            <asp:BoundColumn DataField="IsAutoJournal" HeaderText="IsAutoJournal" ItemStyle-Width ="50" visible ="false" />
                            <asp:TemplateColumn SortExpression="BranchId" HeaderText="Branch Id" ItemStyle-Width="150" visible ="false" > 
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBranchId" runat="server" CommandName="ShowView" Text='<%# Container.DataItem("BranchId")%>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid> 
                       <uc2:ucGridNav id="GridNavigator" runat="server"/>
                 </div>
             </div>
         </div>
        <%--<div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue" />
        </div>--%>
    </asp:Panel> 
    <STYLE>.aspNetDisabled { color: gray; display:none;}</STYLE>
</asp:Content>
