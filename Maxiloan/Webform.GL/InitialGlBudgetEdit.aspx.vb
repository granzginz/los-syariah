﻿
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

Public Class InitialGlBudgetEdit
    Inherits WebBased
    Const FORM_ID As String = "GLBGTEDIT"

   
    Private ReadOnly _jvController As New JournalVoucherController

    Protected WithEvents budget1 As ucNumberFormat 
    Protected WithEvents budget2 As ucNumberFormat 
    Protected WithEvents budget3 As ucNumberFormat 
    Protected WithEvents budget4 As ucNumberFormat 
    Protected WithEvents budget5 As ucNumberFormat 
    Protected WithEvents budget6 As ucNumberFormat 
    Protected WithEvents budget7 As ucNumberFormat 
    Protected WithEvents budget8 As ucNumberFormat 
    Protected WithEvents budget9 As ucNumberFormat 
    Protected WithEvents budget10 As ucNumberFormat 
    Protected WithEvents budget11 As ucNumberFormat 
    Protected WithEvents budget12 As ucNumberFormat

    Private ucNumers As IList(Of ucNumberFormat)


    Enum FormMode
        NewMode = 0
        EditMode = 1
    End Enum
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = FORM_ID
        If SessionInvalid() Then
            Exit Sub
        End If
        If (String.IsNullOrEmpty(BranchID)) Then
            BranchID = sesBranchId.Replace("'", "")
        End If

        If (String.IsNullOrEmpty(SesCompanyID)) Then
            SesCompanyID = GetCompanyId(GetConnectionString)
        End If
         
        If Not Page.IsPostBack Then

            setupUcNumber(New List(Of ucNumberFormat) From {budget1, budget2, budget3, budget4, budget5, budget6, budget7, budget8, budget9, budget10, budget11, budget12})

            Dim mode = CType(Request.QueryString("mode"), FormMode)
            ViewState("FormMode") = mode
            If (mode = FormMode.EditMode) Then
                lbltitle.InnerText = "INITIAL BUDGET (EDIT)"
                pnlBudgetGrid.Visible = True
                If Request.QueryString("key") <> String.Empty Then
                    InitFormEditMode(Request.QueryString("key"))
                Else
                    ShowMessage(lblMessage, "Querystring tidak ditemukan", True)
                End If
                Return
            End If

            If (mode = FormMode.NewMode) Then
                lbltitle.InnerText = "INITIAL BUDGET (NEW)"
                btnUpdate.Text = "SAVE"
                btnUpdate.CausesValidation = True
                btnUpdate.ValidationGroup = "entry"
                InitFormNewMode()
            End If

        End If
    End Sub
    Sub InitFormNewMode()
        lbYear.Visible = False
        ddlYear.Visible = True

        With ddlYear
            .DataValueField = "Value"
            .DataTextField = "Text"
            .DataSource = ModuleGlHelper.YearsList()
            .DataBind()
        End With
        ddlYear.SelectedValue = BusinessDate.Year
         
        lbCoaId.Visible = False
        divcoaAddMode.Visible = True
        lbCoaDesc.Visible = False
        txtNamaAccount.Visible = True

    End Sub

    Sub InitFormEditMode(pacc As String)
        Dim param() = pacc.Split(";")
        ViewState("_companyId") = param(0)

        ViewState("_branchId") = param(1)
        ViewState("_year") = param(2)
        ViewState("_coaId") = param(3)
        bindFormComponents(New InitialBudget(param(0), param(1), param(2), param(3)))
    End Sub



    Private Sub setupUcNumber(ucs As IList(Of ucNumberFormat))
        For Each item As ucNumberFormat In ucs
            item.RangeValidatorEnable = True
            item.RangeValidatorMinimumValue = "0"
            item.rv.ID = item.ID + "_rvBget"
            item.rv.ValidationGroup = "entry"
            item.rv.ErrorMessage = "Jumlah transaksi harus besar dari 0."
        Next

    End Sub

    Private Sub bindFormComponents(param As InitialBudget)

        Dim result = _jvController.GetGlInitialFindOne(GetConnectionString, param)
        lbYear.Text = param.Year
        lbCoaId.Text = param.CoAId
        lbCoaDesc.Text = result.CoaName

        budget1.Text = result.Jan
        budget2.Text = result.Feb
        budget3.Text = result.Mar
        budget4.Text = result.Apr
        budget5.Text = result.Mei
        budget6.Text = result.Jun
        budget7.Text = result.Jul
        budget8.Text = result.Agt
        budget9.Text = result.Sep
        budget10.Text = result.Okt
        budget11.Text = result.Nov
        budget12.Text = result.Des

    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("InitialGLBudget.aspx")
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
        Try

            Dim result = IIf(CType(ViewState("FormMode"), FormMode) = FormMode.EditMode, doUpdate(), doAddNew())

            If (result <> "OK") Then
                ShowMessage(lblMessage, result, True)
                Return
            End If

            Response.Redirect("InitialGLBudget.aspx?s=1")

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub

    Function doAddNew() As String

        Dim component = New List(Of InitialBudget)
        component.Add(New InitialBudget(SesCompanyID, BranchID, ddlYear.SelectedValue, txtNoAccount.Text, txtNamaAccount.Text, CDec(budget1.Text), CDec(budget2.Text), CDec(budget3.Text), CDec(budget4.Text), CDec(budget5.Text), CDec(budget6.Text), CDec(budget7.Text), CDec(budget8.Text), CDec(budget9.Text), CDec(budget10.Text), CDec(budget11.Text), CDec(budget12.Text)))

        Return _jvController.GlInitialBudgetAdd(GetConnectionString, component)

    End Function
    Function doUpdate() As String


        Dim _companyId As String = ViewState("_companyId")
        Dim _branchId As String = ViewState("_branchId")
        Dim _year As String = ViewState("_year")
        Dim _coaId As String = ViewState("_coaId")

        Dim component = New List(Of InitialBudget)
        component.Add(New InitialBudget(_companyId, _branchId, _year, _coaId, "", CDec(budget1.Text), CDec(budget2.Text), CDec(budget3.Text), CDec(budget4.Text), CDec(budget5.Text), CDec(budget6.Text), CDec(budget7.Text), CDec(budget8.Text), CDec(budget9.Text), CDec(budget10.Text), CDec(budget11.Text), CDec(budget12.Text)))


        Return _jvController.GlInitialBudgetUpdate(GetConnectionString, component)

    End Function
End Class