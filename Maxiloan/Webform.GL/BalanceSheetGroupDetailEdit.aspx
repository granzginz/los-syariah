﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="GL.Master"  CodeBehind="BalanceSheetGroupDetailEdit.aspx.vb" Inherits="Maxiloan.Webform.GL.BalanceSheetGroupDetailEdit" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
   <asp:Label ID="Label1" runat="server"  ></asp:Label>
    <div id="jlookupContent" runat="server" />    
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REGISTRASI BALANCE SHEET GROUP MEMBER
                    </h3>
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                    <label  class ="label_req">
                        Group</label>
                    <asp:DropDownList ID="ddlGroup" runat="server" Width="700px" />
                    <asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="ddlGroup" 
                                              Display="Dynamic" CssClass="validator_general"  ErrorMessage="Pilih group!" InitialValue="0" />
                </div>
            </div>--%>
            <div class="form_box">
                <div class="form_single">
                    <label>Group</label>
                    <asp:TextBox ID="txtGroupId" runat="server"  onchange="show()"  CssClass="form_box_hide"></asp:TextBox>
                    <asp:TextBox ID="txtGroupName" runat="server"   CssClass="medium_text" Enabled="false" width="300px"></asp:TextBox>
                    <button class="small buttongo blue" 
                    onclick ="OpenLookup();return false;">...</button >                        
                    <asp:Button runat="server" ID="Jlookup" style="display:none" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label  class ="label_req">
                        Account Type</label>
                    <asp:DropDownList ID="ddlAccountType" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvAccountType" runat="server" ControlToValidate="ddlAccountType" 
                                              Display="Dynamic" CssClass="validator_general"  ErrorMessage="Pilih account type!" InitialValue="0" />
                </div>
            </div>
            <div class="form_button">
                    <asp:HiddenField runat="server" ID="hdnReportGroupId" />
                    <asp:HiddenField runat="server" ID="hdnReportId" />
                    <asp:HiddenField runat="server" ID="hdnGroupId" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="small button gray" />
            </div>    

    <script type="text/javascript">

        $(document).ready(function () {
            

        })

        function OpenLookup()  {
                OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/GroupCoa.aspx?kodecid=" & txtGroupId.ClientID & "&namacid=" & txtGroupName.ClientID & "&isDt=1") %>', 'DAFTAR Group Chart Of Account', '<%= jlookupContent.ClientID %>', 'DoPostBack')
    }
        
    </script>
</asp:Content> 