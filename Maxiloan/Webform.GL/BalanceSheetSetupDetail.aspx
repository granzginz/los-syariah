﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="GL.Master"  CodeBehind="BalanceSheetSetupDetail.aspx.vb" Inherits="Maxiloan.Webform.GL.BalanceSheetSetupDetail" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
   <asp:Label ID="Label1" runat="server"  ></asp:Label>
    <div id="jlookupContent" runat="server" />
 
    
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REGISTRASI REPORT BALANCE SHEET
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Report ID</label>
                    <asp:TextBox ID="txtReportID" runat="server" MaxLength="5" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvReportID" Display="Dynamic" CssClass="validator_general"
                        ErrorMessage="Isi report id" ControlToValidate="txtReportID" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Report Title</label>
                    <asp:TextBox ID="txtReportTitle" runat="server"  CssClass="medium_text" Width="300px" />
                    <asp:Label id="lblLevel" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvReportTitle" Display="Dynamic"
                        CssClass="validator_general" ErrorMessage="Isi Report Title" ControlToValidate="txtReportTitle" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label>
                        Report Text 1</label>
                    <asp:TextBox ID="txtReportText1" runat="server"  CssClass="medium_text" Width="400px" />
                    <asp:Label id="lblReportText1" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label>
                        Report Text 2</label>
                    <asp:TextBox ID="txtReportText2" runat="server"  CssClass="medium_text"  Width="400px"/>
                    <asp:Label id="lblReportText2" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Print Last Month</label>
                    <asp:CheckBox ID="chkPrintLastMonth" runat="server" Checked="true"/>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Print Variance</label>
                    <asp:CheckBox ID="chkPrintVariance" runat="server" Checked="true"/>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="small button gray" />
            </div>    
     
    
    <script type="text/javascript">

        $(document).ready(function () {
            

        })
        
    </script>
</asp:Content> 