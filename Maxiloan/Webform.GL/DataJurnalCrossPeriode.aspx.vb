﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper

Public Class DataJurnalCrossPeriode
	Inherits Maxiloan.Webform.WebBased

	Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE
    Protected WithEvents txtJournalDate2 As ucDateCE

#Region "Constanta"
    Private oCustomClass As New Parameter.DataJurnalCrossPeriode
	Private oController As New DataJurnalCrossPeriodeController
#End Region

#Region "Property"
	Private Property cmdWhere() As String
		Get
			Return CType(ViewState("cmdwhere"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("cmdwhere") = Value
		End Set
	End Property
#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
            pnlGrid.Visible = False
        End If
	End Sub

	Sub DoBind(ByVal SearchBy As String, ByVal SortBy As String)
		Dim oDataTable As New DataTable
		Dim oDataView As New DataView

		With oCustomClass
			.strConnection = GetConnectionString()
			.WhereCond = SearchBy
			.SortBy = SortBy
			.TglPeriode1 = ConvertDate2(txtPeriod1.Text).ToString("yyyy-MM-dd")
			.TglPeriode2 = ConvertDate2(txtPeriod2.Text).ToString("yyyy-MM-dd")
		End With

		oCustomClass = oController.GetDataJurnal(oCustomClass)

		oDataTable = oCustomClass.ListData
		oDataView = oDataTable.DefaultView
		oDataView.Sort = Me.SortBy
		Datagrid1.DataSource = oDataView

		Try
			Datagrid1.DataBind()
		Catch
			Datagrid1.CurrentPageIndex = 0
			Datagrid1.DataBind()
		End Try

		pnlGrid.Visible = True
		pnlSearch.Visible = True
		lblMessage.Text = ""
	End Sub

    Private Sub Btnsearch_Click(sender As Object, e As EventArgs) Handles Btnsearch.Click
        If ConvertDate2(txtPeriod1.Text).ToString("yyyy-MM-dd") > ConvertDate2(txtPeriod2.Text).ToString("yyyy-MM-dd") Then
            ShowMessage(lblMessage, "Tanggal Periode Kedua Harus Lebih Besar", True)
            pnlGrid.Visible = False
        ElseIf txtPeriod1.Text = "" Or txtPeriod2.Text = "" Then
            ShowMessage(lblMessage, "Tanggal Tidak Boleh Kosong", True)
        Else
            pnlGrid.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim lblTR_NOMOR As Label
        Dim lblTR_DESC As Label
        Dim lblJOURNALAMOUNT As Label
        Dim txtPeriodMonth As TextBox
        Dim txtPeriodYear As TextBox
        Dim txtTR_DATE As TextBox
        Dim chkDtList As CheckBox
        Dim dt As DataTable = createNewDT()

        For i = 0 To Datagrid1.Items.Count - 1
            chkDtList = CType(Datagrid1.Items(i).FindControl("ischecked"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lblTR_NOMOR = CType(Datagrid1.Items(i).FindControl("lblTR_NOMOR"), Label)
                    txtPeriodMonth = CType(Datagrid1.Items(i).FindControl("txtPeriodMonth"), TextBox)
                    txtPeriodYear = CType(Datagrid1.Items(i).FindControl("txtPeriodYear"), TextBox)
                    txtTR_DATE = CType(Datagrid1.Items(i).FindControl("txtTR_DATE"), TextBox)
                    lblTR_DESC = CType(Datagrid1.Items(i).FindControl("TR_DESC"), Label)
                    lblJOURNALAMOUNT = CType(Datagrid1.Items(i).FindControl("JOURNALAMOUNT"), Label)
                    dt.Rows.Add(lblTR_NOMOR.Text.Trim, txtPeriodMonth.Text.Trim, txtPeriodYear.Text.Trim, ConvertDate2(txtTR_DATE.Text.Trim))
                End If
            End If
        Next

        Dim oCustomClass As New Parameter.DataJurnalCrossPeriode
        'oGoLive.BusinessDate = Me.BusinessDate 'ConvertDate2(txtTanggalAktivasi.Text)
        oCustomClass.ListData = dt
        oCustomClass.strConnection = GetConnectionString()

        Try
            oCustomClass = oController.GetTr_Nomor(oCustomClass)
            'Modify by Wira 20171019
            'ActivityLog(dt)

            BindGrid()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            lblMessage.Visible = True
            Exit Sub
        End Try
    End Sub

#Region "BindGrid"
    Sub BindGrid()
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.DataJurnalCrossPeriode
        Dim oDataView As New DataView
        'pnlList.Visible = True
        'oCustomClass.PageSize = pageSize
        'oCustomClass.WhereCond = Me.cmdWhere
        'oCustomClass.WhereCond2 = Me.CmdWhere2
        'oCustomClass.SortBy = Me.Sort
        'oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetTr_Nomor(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            '    recordCount = oCustomClass.TotalRecords
            'Else
            '    recordCount = 0
        End If
        Datagrid1.DataSource = oDataView
        Datagrid1.CurrentPageIndex = 0
        Datagrid1.DataBind()
        'PagingFooter()
    End Sub
#End Region

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("TR_Nomor")
        dt.Columns.Add("PeriodMonth")
        dt.Columns.Add("PeriodYear")
        dt.Columns.Add("TR_DATE")
        dt.Columns.Add("TR_DESC")
        dt.Columns.Add("JOURNALAMOUNT")

        Return dt
    End Function

    'Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
    '    'Dim oCustomClass As New Parameter.DataJurnalCrossPeriode
    '    'Dim lblTR_NOMOR As New Label
    '    'oCustomClass.Tr_Nomor = lblTR_NOMOR.Text
    '    'oCustomClass.TglJournal = txtTR_DATE.Text
    '    'oCustomClass.PeriodMonth = txtPeriodMonth.Text

    '    If Datagrid1.Items.Count > 0 Then
    '        Dim i As Integer
    '        Dim dtAdd As New DataTable
    '        Dim dr As DataRow
    '        Dim lblTR_NOMOR As New Label
    '        Dim txtTR_DATE As New TextBox
    '        Dim txtPeriodMonth As New TextBox
    '        Dim txtPeriodYear As New TextBox
    '        Dim strGroupID As String

    '        strGroupID = ""

    '        dtAdd.Columns.Add("TR_NOMOR", System.Type.GetType("System.String"))
    '        dtAdd.Columns.Add("TR_DATE", System.Type.GetType("System.String"))
    '        dtAdd.Columns.Add("PeriodMonth", System.Type.GetType("System.String"))
    '        dtAdd.Columns.Add("PeriodYear", System.Type.GetType("System.String"))

    '        Dim j = 0
    '        Dim k = 0
    '        For i = 0 To Datagrid1.Items.Count - 1
    '            If CType(Datagrid1.Items(i).FindControl("ischecked"), CheckBox).Checked Then
    '                j = j + 1
    '                lblTR_NOMOR = CType(Datagrid1.Items(i).FindControl("lblTR_NOMOR"), Label)
    '                txtTR_DATE = CType(Datagrid1.Items(i).FindControl("txtTR_DATE"), TextBox)
    '                txtPeriodMonth = CType(Datagrid1.Items(i).FindControl("txtPeriodMonth"), TextBox)
    '                txtPeriodYear = CType(Datagrid1.Items(i).FindControl("txtPeriodYear"), TextBox)

    '                dr = dtAdd.NewRow()
    '                dr("TR_NOMOR") = lblTR_NOMOR.Text.ToString.Trim
    '                dr("TR_DATE") = txtTR_DATE.Text.Trim
    '                dr("PeriodMonth") = txtPeriodMonth.Text
    '                dr("PeriodYear") = txtPeriodYear.Text
    '                strGroupID &= CStr(IIf(strGroupID = "", "", ", ")) & "'" & lblTR_NOMOR.Text.Replace("'", "") & "'"
    '                dtAdd.Rows.Add(dr)
    '            End If
    '        Next

    '        Try
    '            oCustomClass = oController.GetTr_Nomor(dtAdd)
    '            If oCustomClass.strError <> "" Then
    '                ShowMessage(lblMessage, "Update Gagal", True)
    '                Exit Sub
    '            Else
    '                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
    '                'Response.Redirect("EditDocument.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
    '                Response.Redirect("DataJurnalCrossPeriode.aspx")
    '            End If
    '        Catch ex As Exception
    '            ShowMessage(lblMessage, ex.Message, True)
    '            lblMessage.Visible = True
    '            Exit Sub
    '        End Try
    '    End If

    '    pnlSearch.Visible = False
    '    pnlGrid.Visible = False
    'End Sub
    Private Sub BtnReset_Click(sender As Object, e As EventArgs) Handles BtnReset.Click
		pnlGrid.Visible = False
		txtPeriod1.Text = ""
		txtPeriod2.Text = ""
		lblMessage.Visible = False
	End Sub
End Class