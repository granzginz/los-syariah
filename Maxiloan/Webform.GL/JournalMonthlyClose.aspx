﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="JournalMonthlyClose.aspx.vb" Inherits="Maxiloan.Webform.GL.JournalMonthlyClose" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
         <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h3> PENUTUPAN JURNAL BULANAN</h3>
                </div>
        </div>
         
        <asp:Panel ID="pnlClosing" runat="server" >
        <div class="form_box">
            <div class="form_single">
                <label > Tahun</label>
                <asp:Label ID="lblTahun" runat="server"/>  
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label > Bulan</label>
                <asp:Label ID="lblBulan" runat="server"/>  
            </div>
        </div>



        <asp:Panel ID="pnlBtn" runat="server" >
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" OnClientClick="return confirm('Lakukan Tutup Bulan ?');" CausesValidation="true" Text="Tutup Jurnal Bulanan" CssClass="small button blue" ValidationGroup="header"/>
 
            </div>
        </asp:Panel>

        </asp:Panel>



        <asp:Panel ID="pnlDone" runat="server" visible="false">
        <div class="form_box">
            <div class="form_single"> 
                <strong><asp:Label ID="lblDone" runat="server"/>  </strong>
            </div>
        </div> </asp:Panel>
</asp:Content>
