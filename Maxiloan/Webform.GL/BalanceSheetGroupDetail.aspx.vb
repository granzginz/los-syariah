﻿#Region "imports"
Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region

Public Class BalanceSheetGroupDetailList
    Inherits WebBased
    Private Const PageSize As Integer = 10
    Protected WithEvents GridNavigator As ucGridNav
    Private _recordCount As Integer = 1
    Private ReadOnly _bsController As New BalanceSheetSetupController
    Private _branchid As String
    Private _companyid As String
    Const PAR_RID As String = "rid"
    Const PAR_RGID As String = "rgid"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = "BSSLIST"
        If SessionInvalid() Then
            Exit Sub
        End If
        _branchid = sesBranchId.Replace("'", "")
        BranchID = _branchid
        If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Return
        End If
        _companyid = GetCompanyId(GetConnectionString)
        SesCompanyID = _companyid
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not Page.IsPostBack Then

            If Request.QueryString(PAR_RGID) <> String.Empty Then
                hdnReportGroupId.Value = Request.QueryString(PAR_RGID)
                Dim aa As BalanceSheetAccGroup = _bsController.FindByIdBSAccGroup(GetConnectionString(), hdnReportGroupId.Value)
                hdnReportId.Value = aa.ReportId
                lblGroupName.Text = aa.groupDesc
                InitGrid(CInt(hdnReportGroupId.Value))
            End If

        End If
    End Sub
    Private Function _dt(ByVal ParamArray param() As Object) As IList(Of BalanceSheetGroupDetail)
        Return _bsController.SelectBSGroupDetail(_recordCount, GetConnectionString(), param)
    End Function

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        If (hdIsFind.Value = "1") Then
            BindGridFind(e.CurrentPage)
        Else
            Dim prid = hdnReportGroupId.Value
            DtgAsset.DataSource = _dt(New Object() {prid, e.CurrentPage, PageSize}) '_bsController.SelectBSGroupDetail(_recordCount, GetConnectionString(),  New Object() {_reportid, e.CurrentPage, PageSize})
            DtgAsset.DataBind()
        End If

        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Private Sub InitGrid(prid As Integer)
        DtgAsset.DataSource = _dt(New Object() {prid, 1, PageSize}) '_bsController.SelectBSGroupDetail(_recordCount, GetConnectionString(),  New Object() {_reportid,  1, PageSize})
        DtgAsset.DataBind()
        GridNavigator.Initialize(_recordCount, PageSize)
    End Sub

    Sub BindGridFind(currPage As Integer)
        Dim sWhare = txtCari.Text.Trim
        Dim prid = hdnReportGroupId.Value
        'Dim _dt As IList(Of MasterAccountObject) = _bsController.SelectBSGroupDetail(_recordCount, GetConnectionString(),  New Object() {_reportid, currPage, PageSize, opW, sWhare})
        DtgAsset.DataSource = _dt(New Object() {prid, currPage, PageSize, sWhare})
        DtgAsset.DataBind()
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        hdIsFind.Value = "1"
        GridNavigator.Initialize(_recordCount, PageSize)
    End Sub


#Region "CommandGrid"

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim cmd As String() = Split(e.CommandArgument.ToString, ",")
        If e.CommandName.ToLower = "edit" Then
            If e.CommandArgument.ToString <> String.Empty Then
                Response.Redirect("BalanceSheetGroupDetailEdit.aspx?rid=" & hdnReportId.Value & "&rgid=" & cmd(0) & "&gid=" & cmd(1))
            End If
        ElseIf e.CommandName.ToLower = "delete" Then
            deleteData(cmd(0), cmd(1))
        End If
    End Sub

#End Region

    Private Sub deleteData(ByVal rgid As Integer, ByVal gid As Integer)
        Try
            _bsController.DeleteBSGroupDetail(GetConnectionString, rgid, gid)
            Response.Redirect("BalanceSheetGroupDetail.aspx?rgid=" & rgid)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Response.Redirect("BalanceSheetGroupDetailEdit.aspx?rid=" & hdnReportId.Value & "&rgid=" & hdnReportGroupId.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("BalanceSheetGroupDetail.aspx?rid=" & hdnReportId.Value & "&rgid=" & hdnReportGroupId.Value)
    End Sub

    Private Sub DtgAsset_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        Dim btnEdit As ImageButton
        Dim btnDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            btnEdit = CType(e.Item.FindControl("btnEdit"), ImageButton)
            btnDelete = CType(e.Item.FindControl("btnDelete"), ImageButton)
            btnDelete.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("BalanceSheetAccGroup.aspx?rid=" & hdnReportId.Value)
    End Sub
End Class