﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ApprovalAmortisasiList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AmortisasiJurnal
    Private oController As New AgreementListController
    Private m_controller As New AmortisasiJurnalController
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
#End Region
#Region "Property"
    Private Property AbNo() As String
        Get
            Return (CType(ViewState("AbNo"), String))
        End Get
        Set(value As String)
            ViewState("AbNo") = value
        End Set
    End Property
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property Nilai() As Double
        Get
            Return (CType(ViewState("Nilai"), Double))
        End Get
        Set(value As Double)
            ViewState("Nilai") = value
        End Set
    End Property
    Private Property Tenor() As Int32
        Get
            Return (CType(ViewState("Tenor"), Int32))
        End Get
        Set(value As Int32)
            ViewState("Tenor") = value
        End Set
    End Property
#End Region


#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rCusomClass As New Parameter.AmortisasiJurnal

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.AbNo = Request.QueryString("AbNo")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.Tr_Nomor = Me.AbNo


        oCustomClass.WhereCond = "ab.AbNo ='" & Me.AbNo & "'"
        BindGridEntity("ab.AbNo ='" & Me.AbNo & "'")

    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetDataSchemeAmortisasiBiaya(oCustomClass)
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AmortisasiJurnal
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'InitialDefaultPanel()

        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.SortBy = ""
        oCustomClass.CurrentPage = currentPage
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetApprovalAmortisasiBiaya(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = CType(oCustomClass.TotalRecords, Integer)
        Else
            recordCount = 0
        End If

        'txtID.Text = oCustomClass.ListData.Rows(0)("ID")
        'lblTr_Nomor.Text = oCustomClass.ListData.Rows(0)("Tr_Nomor")
        With oCustomClass
            txtTglMulai.Text = oCustomClass.ListData.Rows(0)("StartDate")
            txtNilai.Text = oCustomClass.ListData.Rows(0)("Amount")
            lblNoReference.Text = oCustomClass.ListData.Rows(0)("ReferenceNo")
            txtTglAkhir.Text = oCustomClass.ListData.Rows(0)("EndDate")
            txtTglEfektif.Text = oCustomClass.ListData.Rows(0)("EffectiveDate")
            lblTr_Nomor.Text = oCustomClass.ListData.Rows(0)("Tr_Nomor")
            lblNilaiAmortisasi.Text = oCustomClass.ListData.Rows(0)("AmortizeAmount")
            txtTglJangkaWaktu.Text = oCustomClass.ListData.Rows(0)("Tenor")
            Me.Tenor = oCustomClass.ListData.Rows(0)("Tenor")
        End With

        'PagingFooter()

        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetGridApprovalAmortisasiBiaya(oCustomClass)


        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#End Region


    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("AmortisasiBiaya.aspx")
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim lblNo As New Label
        Dim lblDate As New Date
        Dim lblAmount As New Double
        Dim lblDEBET As New Label
        Dim lblDEBETDESC As New Label
        Dim lblCREDIT As New Label
        Dim lblCREDITDESC As New Label
        Dim oCustomClassheader As New Parameter.AmortisasiJurnal

        'header
        With oCustomClass
            .Tr_Nomor = Me.AbNo
            .NoReference = lblNoReference.Text
            .TglEfektif = CDate(txtTglEfektif.Text).ToString("yyyy-MM-dd")
            .TglMulai = CDate(txtTglMulai.Text).ToString("yyyy-MM-dd")
            .TglAkhir = CDate(txtTglAkhir.Text).ToString("yyyy-MM-dd")
            .Nilai = CDbl(txtNilai.Text)
            .JangkaWaktu = txtTglJangkaWaktu.Text
            .AmountRec = CDbl(lblNilaiAmortisasi.Text)
            'End With

            ''detail
            'With oCustomClass
            .lblNo = lblNo.Text
            .lblDate = lblDate
            .lblAmount = lblAmount
            .lblDEBET = lblDEBET.Text
            .lblDEBETDESC = lblDEBETDESC.Text
            .lblCREDIT = lblDEBETDESC.Text
            .lblCREDITDESC = lblDEBETDESC.Text

            .strConnection = GetConnectionString()
        End With
        m_controller.AmortisasiBiayaSave(oCustomClass)
    End Sub


End Class