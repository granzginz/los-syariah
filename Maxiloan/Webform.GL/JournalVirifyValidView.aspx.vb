﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class JournalVirifyValidView
    Inherits WebBased
    Private ReadOnly _jvController As New JournalVoucherController

    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents UcSearchBy As UcSearchBy
    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    Private debTotal As Decimal = 0
    Private creTotal As Decimal = 0

    Private Property cmdWhere As String
        Get
            Return CType(ViewState("cmdWhere"), String)
        End Get
        Set(value As String)
            ViewState("cmdWhere") = value
        End Set
    End Property
    Private Property wOpt As Integer
        Get
            Return CType(ViewState("wOpt"), Integer)
        End Get
        Set(value As Integer)
            ViewState("wOpt") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If
        lblMessage.Visible = False

        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not IsPostBack Then
            cTabs.SetNavigateUrl(Request("page"), Request("id"))
            cTabs.RefreshAttr(Request("pnl"))
            showValidGrid()

            UcSearchBy.ListData = "1,No Transaksi-2,ID Transaksi"
        End If
    End Sub

    Sub showValidGrid()
        Try
            pnlGrid.Visible = True

            Dim result = _jvController.GlJournalVerifyPage(GetConnectionString(), New JournalApproval With {
                                                           .CompanyId = SesCompanyID, .TransactionType = "1",
                                                           .BranchId = Session("SELBRANCH"),
                                                           .TransactionDate = CType(Session("TRNDATE"), DateTime)}, PageSize, 1, _recordCount, Me.wOpt, Me.cmdWhere)
            ViewState("SOURCE") = result 
            If (result.Count <= 0) Then
                ShowMessage(lblMessage, "Tidak Ada Transaksi", False)
                btnVerify.Visible = False
                pnlGrid.Visible = False
                Return
            End If

            dtgCSV.DataSource = result
            dtgCSV.DataBind()
            GridNavigator.Initialize(_recordCount, PageSize) 

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub doApprove(ByVal sender As Object, ByVal e As EventArgs) Handles btnVerify.Click

        Dim checkedIDs = (From msgRow As GridViewRow In dtgCSV.Rows
                          Where (CType(msgRow.FindControl("chkUpdateStatus"), CheckBox).Checked)
                          Select dtgCSV.DataKeys(msgRow.RowIndex).Value).ToList()

        If (checkedIDs.Count <= 0) Then
            ShowMessage(lblMessage, "Silahkan Pilih Journal yang akan di Verify", False)
            Return
        End If

        For Each item As GridViewRow In dtgCSV.Rows
            upg1.Visible = True
            updatepanel1.Visible = True
        Next


        Try
            'Dim result = _jvController.GlJournalVerify(GetConnectionString, checkedIDs.Cast(Of String)().ToList())
            Dim result = _jvController.GlJournalPost(GetConnectionString, checkedIDs.Cast(Of String)().ToList())
            showValidGrid()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        Dim result = _jvController.GlJournalVerifyPage(GetConnectionString(), New JournalApproval With {.CompanyId = SesCompanyID, .TransactionType = "1",
                                                                                                        .BranchId = Session("SELBRANCH"),
                                                       .TransactionDate = CType(Session("TRNDATE"), DateTime)}, PageSize, e.CurrentPage, _recordCount, Me.wOpt, Me.cmdWhere)
        ViewState("SOURCE") = result
        dtgCSV.DataSource = result
        dtgCSV.DataBind()
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub

    Protected Sub grdOrders_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dtgCSV.RowCommand

        If (e.CommandName = "Expand") Then

            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString())
            Dim imgbtn As ImageButton = CType(gv.Rows(rowIndex).FindControl("ImgBtn"), ImageButton)

            Dim key = gv.DataKeys(rowIndex)(0).ToString()
            ViewState("OrderId") = key
            Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails"), PlaceHolder)

            If (imgbtn.ImageUrl.Trim().ToUpper() = "IMAGES/PLUS.GIF") Then
                objPH.Visible = True
                imgbtn.ImageUrl = "images/Minus.gif"
            Else
                imgbtn.ImageUrl = "images/Plus.gif"
                objPH.Visible = False
            End If

        End If
    End Sub

    Private Sub DtgAsset_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dtgCSV.RowDataBound
        Dim imgBtn As ImageButton
        Dim data = CType(ViewState("SOURCE"), IList(Of JournalApproval))
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            imgBtn = CType(e.Row.FindControl("ImgBtn"), ImageButton)
            imgBtn.CommandArgument = e.Row.RowIndex.ToString()


            Dim txtid = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionNo"))
            Dim objChildGrid As GridView = CType(e.Row.FindControl("grdDetails"), GridView)
            objChildGrid.DataSource = Data.SingleOrDefault(Function(x) x.TransactionNo = txtid).Items
            objChildGrid.DataBind()
            objChildGrid.Visible = True
            Dim objPH As PlaceHolder = CType(e.Row.FindControl("objPHOrderDetails"), PlaceHolder)
            objPH.Visible = False 
            imgBtn.ImageUrl = "Images/Plus.gif"
            If (txtid = ViewState("OrderId")) Then
                objPH.Visible = True
                imgBtn.ImageUrl = "Images/Minus.gif"
            End If 
        End If 
    End Sub

    Private Sub btnFind_Click() Handles btnCancel.Click
        Response.Redirect("JournalVirifyView.aspx")
    End Sub

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        Me.wOpt = CInt(UcSearchBy.ValueID)
        Me.cmdWhere = UcSearchBy.Text

        showValidGrid()
    End Sub
End Class