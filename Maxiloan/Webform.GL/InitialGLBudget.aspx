﻿<%@ Page Language="vb" AutoEventWireup="false"   MasterPageFile="GL.Master"  CodeBehind="InitialGLBudget.aspx.vb" Inherits="Maxiloan.Webform.GL.InitialGLBudget" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
               INITIAL BUGDET
            </h3>
        </div>
    </div>
     <asp:Panel runat="server" ID="pnlBudgetGrid" visible="false">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">

                     <asp:GridView  ID="dtgInitialBgt" runat="server"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"    AllowSorting="False" Width="100%"  DataKeyNames="BdKey"  >
                                    <HeaderStyle CssClass="th" /> 
                                    <FooterStyle CssClass="item_grid" />
                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="">
                                 <ItemTemplate>
                                     <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../Images/iconedit.gif" CommandName="Edit" OnCommand="CommandGrid_Click" 
                                     CausesValidation="False" CommandArgument='<%# eval("BdKey") %>' />
                                 </ItemTemplate>
                                 </asp:TemplateField>
                                        <asp:BoundField  Visible="True" DataField="Year" HeaderStyle-Width="35px"  HeaderText="Year" />
                                        <asp:BoundField  Visible="True" DataField="CoaId" HeaderStyle-Width="65px" HeaderText="COA" />
                                        <asp:BoundField  Visible="True" DataField="CoaName" HeaderText="COA Description" /> 
                                        
                                        
                                         <asp:BoundField  Visible="True" DataField="Jan" HeaderText="Januari"   ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"  DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Feb" HeaderText="Febuari" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Mar" HeaderText="Maret" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Apr" HeaderText="April" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"  DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Mei" HeaderText="Mei" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"  DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Jun" HeaderText="Juni" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Jul" HeaderText="Juli" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Agt" HeaderText="Agustus" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Sep" HeaderText="September" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Okt" HeaderText="Oktober" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="70px" DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Nov" HeaderText="November" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"  DataFormatString= "{0:#,0}"/> 
                                         <asp:BoundField  Visible="True" DataField="Des" HeaderText="Desember" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"  DataFormatString= "{0:#,0}"/> 
                                     </Columns>
                    </asp:GridView >
                        <uc2:ucGridNav id="GridNavigator" runat="server"/>
                 </div>
            </div>
        </div>
        <div class="form_button">
        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
        </asp:Button>
    </div>
     </asp:Panel>

    
    
    
    
     <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>  Cari Initial Budget </h3>
        </div>
    </div>
    
    <div class="form_box">
        <div class="form_single">
            <label> Branch</label>
            <asp:DropDownList ID="ddlBranch" runat="server"/>
            
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>Year</label>
            <asp:DropDownList ID="ddlYear" runat="server"/> 
        </div>
    </div>
   <%-- <div class="form_box">
        <div class="form_single">
            <label> Account Type</label>
            <asp:DropDownList ID="ddlAccountType" runat="server"/>             
        </div>
    </div>--%>
    
    <div class="form_box">
        <div class="form_single">
            <label> Cari Berdasarkan</label>
            <asp:DropDownList ID="ddlCari" runat="server">
                <asp:ListItem Text="Account Name" Value="2" />
                <asp:ListItem Text="Account No" Value="1" />
            </asp:DropDownList>
            <asp:TextBox ID="txtCari" runat="server" placeholder="Kata kunci pencarian"/>
        </div>
    </div>
      <div class="form_box">
          <div class="form_button">
               <label>UpLoad Inital Budget</label>
              <asp:FileUpload ID="files" runat="server" Text="" ViewStateMode="Enabled" CausesValidation="False" onchange="$('#ContentPlaceHolder1_btnUpload').attr('style','float:none; visibility:show');"/>&nbsp;      
              <asp:Button ID="btnUpload" runat="server" CausesValidation="False" Text="Upload" CssClass="small button blue" style="float:none; visibility:hidden;"    />
          </div>
      </div>
    <div class="form_button">
        
        <asp:Button ID="btnFind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue"/>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"/>
         
        <asp:HiddenField id="hdIsFind" runat="server" Value="0"/>
    </div>
</asp:Content>
