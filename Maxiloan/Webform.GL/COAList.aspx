﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="COAList.aspx.vb" Inherits="Maxiloan.Webform.GL.COAList" %>
 
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                REGISTRASI AKUN (CHART OF ACCOUNT)
            </h3>
        </div>
    </div>
    
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="False" AutoGenerateColumns="false"
                    DataKeyField="CoAID" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                       <asp:TemplateColumn HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../Images/iconedit.gif" CommandName="Edit"
                                    OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("CoaId") %>' />
                                  
                                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                    CommandName="Delete" OnClientClick="return confirmDelete()" OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("CoaId") & ";" & eval("AddFreeNo") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText = "Account No.">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDisplay"   Text='<%# eval("Coa") %>' /> 
                            </ItemTemplate>
                        </asp:TemplateColumn>      
                                     
                        <asp:BoundColumn DataField="Description" HeaderText="Account Name" /> 
                        <asp:TemplateColumn Visible="false">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblBranchId"  Text='<%# eval("Branch") %>' />--%>
                                <asp:Label runat="server" ID="lblCoAId" Text='<%# eval("CoAId") %>' />
                                <asp:Label runat="server" ID="lblDisplayCoaId" /><%-- Text='<%# eval("DisplayCoaID") %>' />--%>
                                <asp:Label runat="server" ID="lblIsLeaf" /><%-- Text='<%# eval("IsLeaf") %>' />--%>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="Account Type">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAccountTypeDesc"  Text='<%# eval("Type") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
     
                        <asp:BoundColumn DataField="FullyQualifiedName" HeaderText="Parent Acct" />
                        <asp:BoundColumn DataField="Currency" HeaderText="Currency" />
                        <asp:BoundColumn DataField="Level" HeaderText="Level" />
                       <%-- <asp:BoundColumn DataField="AddFreeNo" HeaderText="Add Free" />
                        <asp:BoundColumn DataField="AddFreeDesc" HeaderText="Add Free Desc" />--%>
                    </Columns>
                </asp:DataGrid>
                <uc2:ucGridNav id="GridNavigator" runat="server"/>
              
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue" visible="false">
        </asp:Button>
    </div>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CARI ACCOUNT
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Account Type</label>
            <asp:DropDownList ID="ddlAccountType" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Branch</label>
            <asp:DropDownList ID="ddlBranch" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan</label>
            <asp:DropDownList ID="ddlCari" runat="server">
                <asp:ListItem Text="Account Name" Value="2" />
                <asp:ListItem Text="Account No" Value="1" />
            </asp:DropDownList>
            <asp:TextBox ID="txtCari" runat="server" placeholder="Kata kunci pencarian"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnFind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue"/>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"/>
        <asp:HiddenField id="hdIsFind" runat="server" Value="0"/>
    </div>
    
    </asp:Content>
<%--    </form>
</body>
</html>
--%>