﻿#Region "imports"

Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class COAdetail
    Inherits WebBased


#Region "properties"
    Property isNew As Boolean
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
        Get
            Return CBool(ViewState("isNew"))
        End Get
    End Property

    Property glCurrencies As IList(Of GlCurrency)
        Set(ByVal value As IList(Of GlCurrency))
            ViewState("GlCurrencies") = value
        End Set
        Get
            Return CType(ViewState("GlCurrencies"), IList(Of GlCurrency))
        End Get
    End Property
#End Region 
    Const FORM_ID As String = "COALIST"
    Const PAR_ACC As String = "acc"

    Dim currController As New GlCurrencyController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.FormID = FORM_ID
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If

        HiddenField1.Value = ddlBranch.SelectedValue

        If Not Page.IsPostBack Then

            With ddlAccountType
                .DataValueField = "Value"
                .DataTextField = "Text"
                .DataSource = ModuleGlHelper.GetEnumMasterAccTypeList()
                .DataBind()
            End With

            'With ddlFunction
            '    .DataValueField = "Value"
            '    .DataTextField = "Text"
            '    .DataSource = ModuleGlHelper.GetGlFunctionsList()
            '    .DataBind()
            'End With
            glCurrencies = currController.GetGlCurrencies(GetConnectionString)
            bindDdlBranch()
            BindGlCurrenciesDropdown(ddlMataUang, GetConnectionString())
            Dim hcurr = glCurrencies.SingleOrDefault(Function(cr) cr.IsHomeCurrency = True).CurrencyId
            hidHomeCurr.Value = hcurr
            ddlMataUang.SelectedValue = hcurr

            isNew = True

            If Request.QueryString(PAR_ACC) <> String.Empty Then
                Dim pacc = Request.QueryString(PAR_ACC)
                If (BranchID = "") Then
                    BranchID = sesBranchId.Replace("'", "")
                End If
                loaddata(pacc)
                isNew = False
            End If
        End If
    End Sub

    Private Sub loaddata(ByVal accNo As String)

        Dim coaCon As New COAController
        Dim result As MasterAccountObject = coaCon.FindById(GetConnectionString, accNo, BranchID, SesCompanyID)

        If (result Is Nothing) Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            Return
        End If

        ddlAccountType.SelectedValue = CType(result.Type, Integer).ToString
        txtAccountNo.Text = result.CoaId
        txtAccountName.Text = result.Description
        txtParentName.Text = result.ParentDesc
        hdnParentId.Value = result.ParentCoa
        ddlBranch.SelectedValue = result.Branch
        ddlMataUang.SelectedValue = result.Currency
        chkStatus.Checked = result.IsActive
        txtLBU.Text = result.LBU
        txtSIPP.Text = result.SIPP
        'hdbCOAPasanganId.Value = result.CoaIdTax
        'txtCoaPasangan.Text = result.CoaTaxDesc


        ''ddlFunction.SelectedValue = result.Function
        lblLevel.Text = String.Format("Level {0}", result.Level)
        ddlAccountType.Enabled = False
        txtAccountNo.Enabled = False
        ' ddlBranch.Enabled = False
        txtParentName.Enabled = False
        'pnlsrch.Visible = False

    End Sub


#Region "Dropdown list"
    Private Sub bindDdlBranch()
        Dim m_controller As New DataUserControlController
        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"

            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)

                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"

            End If
        End With
    End Sub

    'Private Sub bindDdlMataUang()
    '    Dim c_Class As New CurrencyController
    '    Dim p_Class As New Currency

    '    With p_Class
    '        .strConnection = GetConnectionString()
    '        .WhereCond = ""
    '    End With
    '    c_Class.GetCurrency(p_Class)
    '    With ddlMataUang
    '        .DataTextField = "Description"
    '        .DataValueField = "CurrencyId"
    '        .DataSource = p_Class.DTOList
    '        .DataBind()
    '    End With

    'End Sub

#End Region
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("COAList.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try


            Dim coaCon As New COAController
            Dim coadata As New MasterAccountObject(txtAccountNo.Text.Trim,
                                                   txtAccountName.Text.Trim,
                                                   CType(CInt(ddlAccountType.SelectedValue), EnumMasterAccType),
                                                   ddlMataUang.SelectedValue,
                                                   "",
                                                    ddlBranch.SelectedValue.Trim,
                                                   "",
                                                   "",
                                                    chkStatus.Checked,
                                                   UserID, UserID,
                                                   txtLBU.Text,
                                                   txtSIPP.Text)

            coadata.ParentCoa = hdnParentId.Value.Trim(New Char() {" "})
            coadata.CompanyId = SesCompanyID
            'coadata.CoaIdTax = hdbCOAPasanganId.Value.Trim(New Char() {" "})
            'coadata.Function = ddlFunction.SelectedValue
            coadata.IsForeignCurr = Not (glCurrencies.SingleOrDefault(Function(cr) cr.IsHomeCurrency = True).CurrencyId.Equals(ddlMataUang.SelectedValue))
            coadata.ParentAddFree = hdnParentAddFree.Value.Trim(New Char() {" "})
            'coadata.CoaIdTaxAddFree = hdnCOAPasanganAddFree.Value.Trim(New Char() {" "})

            If isNew Then
                coaCon.InsertData(GetConnectionString, coadata)
            Else
                Try
                    If txtAccountNo.Text.Trim = coadata.ParentCoa Then
                        ShowMessage(lblMessage, "COA Pasangan tidak boleh sama dengan COA", True)
                        Exit Sub
                    End If
                    'coadata.AssertTaxId()
                    coaCon.UpdateData(GetConnectionString, coadata)
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End If

            Response.Redirect("COAList.aspx")

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    'Protected Sub ddlMataUang_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlMataUang.SelectedIndexChanged
    '    Dim homecurr = glCurrencies.SingleOrDefault(Function(cr) cr.IsHomeCurrency = True).CurrencyId

    '    lblfCurr.Visible = homecurr.Equals(CType(sender, DropDownList).SelectedValue)
    'End Sub
End Class