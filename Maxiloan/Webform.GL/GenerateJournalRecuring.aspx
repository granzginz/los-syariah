﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GenerateJournalRecuring.aspx.vb"
    Inherits="Maxiloan.Webform.GL.GenerateJournalRecuring" %> 

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AmortisasiBiaya</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                Daftar Skema Jurnal Yang Amortisasikan
            </h3>
        </div>
    </div> 
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDatagrid" runat="server"  > 
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" DataKeyField="AbNo"
                        AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns> 
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                 <input id="Checkbox2" type="checkbox"  runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <asp:Label ID="lblxx" runat="server" Text="-"  visible='<%#IIf(DataBinder.Eval(Container, "DataItem.isAmortized") = 0, True, False) %>' />
                                 <asp:CheckBox ID="ItemCheckBox" runat="server" visible='<%#IIf(DataBinder.Eval(Container, "DataItem.isAmortized") = 0, True, False) %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AbNo" HeaderText="AbNo" Visible="true">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTr_Nomor" runat="server" Text='<%#Container.DataItem("AbNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="TanggalAmortize" HeaderText="Tanggal Amortize">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTr_Desc" runat="server" Text='<%#Container.DataItem("TanggalAmortize")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="Reference No">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDEBET" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="EffectiveDate" HeaderText="Effective Date">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCREDIT" runat="server" Text='<%#Container.DataItem("EffectiveDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="Amount">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Container.DataItem("Amount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="Tenor" HeaderText="Tenor">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTenor" runat="server" Text='<%#Container.DataItem("Tenor")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AmortizeAmount" HeaderText="Amortize Amount">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmortizeAmount" runat="server" Text='<%#Container.DataItem("AmortizeAmount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="StartDate" HeaderText="Start Date">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Container.DataItem("StartDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EndDate" HeaderText="End Date">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Container.DataItem("EndDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="form_button">
                        <asp:Button ID="GenerateJournal" runat="server" Text="Generate Journal" CssClass="small button blue">
                        </asp:Button> 
                    </div>
                 </div>
            </div>
        </div> 
    </asp:Panel>

    <asp:Panel ID="pnlList" runat="server">
        
        <div class="form_title">
            <div class="form_single">
                <Label> Tgl Amortize </Label>
                <asp:TextBox ID="txtAmortizeStart" runat="server" ></asp:TextBox>
                <aspajax:CalendarExtender ID="caltxtAmortizeStart" runat="server" TargetControlID="txtAmortizeStart"
                Format="yyyy-MM-dd" />
                 s/d  
                <asp:TextBox ID="txtAmortizeEnd" runat="server" ></asp:TextBox>
                <aspajax:CalendarExtender ID="caltxtAmortizeEnd" runat="server" TargetControlID="txtAmortizeEnd"
                Format="yyyy-MM-dd" />
            </div>
        </div>
        
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>

    </form>
</body>
</html>
