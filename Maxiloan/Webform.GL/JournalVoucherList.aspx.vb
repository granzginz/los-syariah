﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports System.Web.Script.Services

Public Class JournalVoucherList
    Inherits WebBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10 
    Private recordCount As Int64 = 1
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected WithEvents txtPeriodeTo As ucDateCE
    Protected WithEvents GridNavigator As ucGridNav

    Private Sub bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)

        Dim p_Class As New GeneralPaging
        Dim c_Class As New GeneralPagingController

        With p_Class
            .strConnection = GetConnectionString()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .WhereCond = cmdWhere
            .SortBy = cmSort
            .SpName = "spPagingGLMasterH"
        End With
        p_Class = c_Class.GetGeneralPaging(p_Class)

        If Not p_Class Is Nothing Then
            recordCount = p_Class.TotalRecords
        Else
            recordCount = 0
        End If

        DtgAsset.DataSource = p_Class.ListData.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        FormID = "JVLIST"
        If SessionInvalid() Then
            Exit Sub
        End If
#If Not Debug Then 
        If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Return
        End If
#End If
        If Me.IsHoBranch = False Then
            CType(Me.Master, GL).NotAuthorized()
            Exit Sub
        End If
        lblMessage.Visible = False
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not IsPostBack Then

            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses.", False)
            End If

            BindGlMasterSequenceDropdownWithNone(ddlJenisTransaksi, GetConnectionString, sesBranchId.Replace("'", ""))
            SortBy = "glh.tr_nomor"
            pnlGrid.Visible = False

            If Request("dfrom") <> String.Empty Or Request("dto") <> String.Empty Or Request("jtran") <> String.Empty Or Request("accno") <> String.Empty Then
                txtPeriodeFrom.Text = Server.UrlDecode(Request("dfrom"))
                txtPeriodeTo.Text = Server.UrlDecode(Request("dto"))
                ddlJenisTransaksi.SelectedValue = Server.UrlDecode(Request("jtran"))
                txtCari.Text = Server.UrlDecode(Request("accno"))
                loaddata()
            End If

        End If
    End Sub

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        bindgrid(Me.SearchBy, Me.SortBy)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
 

#Region "CommandGrid_Click"

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            If e.CommandName.ToLower = "hold" Then
                If e.CommandArgument.ToString <> String.Empty Then
                    Response.Redirect("JournalVoucherHold.aspx?nv=" & e.CommandArgument.ToString & "&dfrom=" & Server.UrlEncode(txtPeriodeFrom.Text) & "&dto=" & Server.UrlEncode(txtPeriodeTo.Text) & "&jtran=" & Server.UrlEncode(ddlJenisTransaksi.SelectedValue) & "&accno=" & Server.UrlEncode(txtCari.Text))
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("JournalVoucherList.aspx")
    End Sub

    Private Sub loaddata()
        Dim whereCond As String = String.Empty
        Dim fieldPeriode As String = "glh.tr_date"
        Dim fieldJenisTransaksi As String = "glh.transactionId"
        Dim fieldaccountno As String = "glh.tr_nomor"

        If txtPeriodeFrom.Text <> String.Empty Then
            whereCond &= " ( " & fieldPeriode & " >=  '" & ConvertDate2(txtPeriodeFrom.Text.Trim).ToString("yyyy-MM-dd") & "' ) "
        End If

        If txtPeriodeTo.Text <> String.Empty Then
            If whereCond <> String.Empty Then whereCond &= " and "
            whereCond &= " ( " & fieldPeriode & " <= '" & ConvertDate2(txtPeriodeTo.Text.Trim).ToString("yyyy-MM-dd") & "' ) "
        End If

        If txtCari.Text <> String.Empty Then
            If whereCond <> String.Empty Then whereCond &= " and "
            whereCond &= " ( " & fieldaccountno & " = '" & txtCari.Text & "' ) "
        End If

        If ddlJenisTransaksi.SelectedValue <> "0" Then
            If whereCond <> String.Empty Then whereCond &= " and "
            whereCond &= " ( " & fieldJenisTransaksi & " = '" & ddlJenisTransaksi.SelectedValue & "' ) "
        End If

        'If whereCond <> String.Empty Then whereCond &= " and "
        'whereCond &= " glh.branchId = " & sesBranchId

        If whereCond <> String.Empty Then whereCond &= " and "
        whereCond &= " glh.status_tr = 'OP' "

        SearchBy = whereCond
        bindgrid(SearchBy, SortBy)
        pnlGrid.Visible = True
        GridNavigator.Initialize(recordCount, pageSize)
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        loaddata()
    End Sub
     
   
   
End Class