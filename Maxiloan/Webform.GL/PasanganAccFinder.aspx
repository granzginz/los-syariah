﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PasanganAccFinder.aspx.vb" Inherits="Maxiloan.Webform.GL.PasanganAccFinder" %>

<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href=".../css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../js/jquery-1.9.1.min.js"></script>
	<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {
                e.preventDefault();
                var kode = $(this).parents('tr').find('.kode').html();
                var nama = $(this).parents('tr').find('.nama').html(); 
                var dataArg_Temp = [
            { name: '<%= KodeCID %>', value: kode.trim() },
            { name: '<%= NamaCID %>', value: nama.trim() }
            ];
                var dataArg = new Array();
                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });
                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });
        });
       
    </script>
</head>

<body>

    <form id="form1" runat="server">
                <asp:HiddenField runat="server" ID="hdnTypeId" />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:Panel ID="pnlGrid" runat="server">
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR AKUN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" 
                                BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                                DataKeyField="coaid" AutoGenerateColumns="False" AllowSorting="True"
                                 Width="100%" Visible="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSelect" runat="server" Text='Select'  >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText = "Account No.">
                                        <ItemTemplate>
                                              <asp:Label runat="server" ID="lblDisplay"  CssClass="kode"  Text='<%# eval("CoAId") %>' />  
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                     
                                   <asp:BoundColumn DataField="Description" HeaderText="Account Name" ItemStyle-CssClass="nama" />
                                   <asp:TemplateColumn HeaderText="Account Type">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAccountTypeDesc"  Text='<%# eval("Type") %>' />
                                      
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Currency" HeaderText="Currency" /> 

                                </Columns>
                            </asp:DataGrid>
                        </div>
                         <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                    </div>
                    
                </div>

                </asp:Panel>
                <asp:Panel runat="server" ID="PanelSearch">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                CARI AKUN</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>  Account Type</label>
                            <asp:Label ID="lblTypeName" runat="server"/>
                            <asp:DropDownList ID="ddlAccountType" runat="server" /> 
                            
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="ddlCari" runat="server">
                                <asp:ListItem Text="Account Name" Value="2" />
                                <asp:ListItem Text="Account No" Value="1" />
                            </asp:DropDownList>
                            <asp:TextBox ID="txtCari" runat="server" placeholder="Kata kunci pencarian"></asp:TextBox>
                           
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="buttonSearch" runat="server" Text="Search" CssClass="small button blue"  CausesValidation="false" />
                    </div>
                </asp:Panel> 
    </form> 
</body>
</html>
