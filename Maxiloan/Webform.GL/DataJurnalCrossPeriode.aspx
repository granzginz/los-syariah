﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DataJurnalCrossPeriode.aspx.vb" Inherits="Maxiloan.Webform.GL.DataJurnalCrossPeriode" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script src="../js/jquery-2.1.1.js" type="text/javascript"></script>	
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    DATA JURNAL CROSS PERIODE
                </h3>
            </div>
        </div>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Periode </label>
                <uc2:ucdatece id="txtPeriod1" runat="server" />
                &nbsp; S/D
                <uc2:ucdatece id="txtPeriod2" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR JURNAL</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                   <asp:DataGrid ID="Datagrid1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ischecked" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn HeaderText="TR_NOMOR">                            
                                <ItemTemplate>
                                    <%--<asp:TextBox ID="txtTR_NOMOR" runat="server" Text='<%#Container.DataItem("TR_NOMOR")%>' Width="80%" visible="false"></asp:TextBox>--%>
                                     <asp:label ID="lblTR_NOMOR" runat="server" Visible="true" Text='<%#Container.DataItem("TR_NOMOR")%>'>
                                    </asp:label> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PERIOD_MONTH">                            
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPeriodMonth" runat="server" Text='<%#Container.DataItem("PeriodMonth")%>' Width="30%" CssClass="inptype"></asp:TextBox>
                                     <asp:label ID="lblPeriodMonth" runat="server" Visible="false" Text='<%#Container.DataItem("PeriodMonth")%>'>
                                    </asp:label> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PERIOD_YEAR" >                            
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPeriodYear" runat="server" Text='<%#Container.DataItem("PeriodYear")%>' Width="30%" CssClass="inptype"></asp:TextBox>
                                     <asp:label ID="lblPeriodYear" runat="server" Visible="false" Text='<%#Container.DataItem("PeriodYear")%>'>
                                    </asp:label> 
                                </ItemTemplate>    
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn HeaderText="TR_DATE" >                            
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtTR_DATE" Text='<%#Container.DataItem("TR_DATE")%>' Width="80px" visible="true"></asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtTR_DATE"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender> 
                                </ItemTemplate>      
                            </asp:TemplateColumn> 
                            <asp:BoundColumn DataField="TR_DESC" SortExpression="TR_DESC" HeaderText="TR DESC">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="JOURNALAMOUNT" SortExpression="JOURNALAMOUNT" HeaderText="JOURNAL AMOUNT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button green">
        </asp:Button>
    </div>
    </asp:Panel>     
    </form>
</body>
</html>
