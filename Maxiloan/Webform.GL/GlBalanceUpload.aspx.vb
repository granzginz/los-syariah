﻿Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

Public Class GlBalanceUpload
    Inherits WebBased
    Protected WithEvents GridNavigator As ucGridNav
    Private ReadOnly _jvController As New JournalVoucherController

    Protected WithEvents GridHistoryNavigator As ucGridNav
    Property _glmas As GelMas
        Set(ByVal value As GelMas)
            ViewState("GELMAS") = value
        End Set
        Get
            Return CType(ViewState("GELMAS"), GelMas)
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        FormID = "UPGELMAS"
        If SessionInvalid() Then
            Exit Sub
        End If

        If SessionInvalid() Then
            Exit Sub
        End If

        If (String.IsNullOrEmpty(BranchID)) Then
            BranchID = sesBranchId.Replace("'", "")
        End If
        If (String.IsNullOrEmpty(SesCompanyID)) Then
            SesCompanyID = GetCompanyId(GetConnectionString)
        End If
        lblMessage.Visible = False
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If Not Page.IsPostBack Then
            pnlUploadProcess.Visible = False
            pnlUploadProcess_1.Visible = False
            pnlLoadload.Visible = True


            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses", False)
            End If
            txtTahun.Text = BusinessDate.Year
        End If
        If Not CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Return
        End If
    End Sub


    Protected Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click
        Dim tahun As Integer

        If (Integer.TryParse(txtTahun.Text, tahun)) Then
            If Not (New GlYearPeriodController().IsYearValid(GetConnectionString, txtTahun.Text, SesCompanyID, BranchID)) Then
                ShowMessage(lblMessage, "Tahun Journal Tidak Ditemukan..", True)
                Return
            End If
        Else
            ShowMessage(lblMessage, "Tahun tidak valid", True)
            Return
        End If



        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
        Dim _directory As String = String.Format("{0}\xml\{1}", physicalPath, BranchID)
        Dim thefile As String = files.PostedFile.FileName
        Dim fileName As String = BranchID + "_" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "_" + files.PostedFile.FileName


        Try
            If Not Directory.Exists(_directory) Then
                Directory.CreateDirectory(_directory)
            End If

            If File.Exists(String.Format("{0}\{1}", _directory, thefile)) Then
                My.Computer.FileSystem.RenameFile(String.Format("{0}\{1}", _directory, thefile), fileName)
            End If

            files.PostedFile.SaveAs(String.Format("{0}\{1}", _directory, thefile))
            Dim gmas = GelMas.GetData(_directory, thefile)

            If (gmas.GelMasCount <= 0) Then
                ShowMessage(lblMessage, "Tidak ada data dalam file dbf", False)
                Return
            End If
            gmas.Year = tahun
            gmas.CompanyId = SesCompanyID
            gmas.BranchId = BranchID
            gmas.User = UserID
            _glmas = gmas
            PrepareConfirmUpload()
        Catch ex As Exception
            'pnlUpladProcess.Visible = False
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Sub PrepareConfirmUpload()
        pnlLoadload.Visible = False 

        Dim builder = New StringBuilder()
        Try
            Dim result = _jvController.GlGelMasUploadValidate(GetConnectionString, _glmas)

            If (result.Contains("OK")) Then
                Dim row = result.Split(";")
                builder.AppendLine(String.Format("Terdapat {0} data.", _glmas.GelMasCount))


                ' GELMAS Terdapat COA yang tidak Terdaftar di MASTERACC
                ' - User bisa download csv..
                ' - tombol Next hidup jika sebagian GELMAS terdaftar di MASTERACC
                '   tekan Next (Upload Balance) tampil form konfirmasi upload

                If (CType(row(1), Integer) > 0) Then
                    builder.AppendLine(String.Format("<br/>Terdapat {0} dari {1} COA DBF tidak terdaftar pada master account", row(1), row(2)))
                    builder.AppendLine(String.Format("<br/>Berikut daftar COA"))
                    pnlUploadProcess.Visible = True
                    InitGrid() 
                    btnNext.Enabled = CType(row(1), Integer) - CType(row(2), Integer) <> 0 
                Else
                    'GELMAS tidak ditemukan COA Null di MASTERACC
                    ' tapi tidak ditemukan Balance yang di upload maka tampilkan error
                    If (CType(row(2), Integer) <= 0) Then
                        pnlLoadload.Visible = True
                        ShowMessage(lblMessage, "Tidak ditemukan Data yang diupload", True)
                        Return
                    End If

                End If
                lblInfo.Text = builder.ToString()
            Else
                ShowMessage(lblMessage, result, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub DoNext(sender As Object, e As EventArgs) Handles btnNext.Click
        Dim result = _jvController.GlGelMasVirifyUpload(GetConnectionString, _glmas)
        If (result <> "OK") Then
            ShowMessage(lblMessage, result, False)
            Return
        End If
        pnlUploadProcess.Visible = False
        pnlUploadProcess_1.Visible = True


        dtgHistoryCoa.DataSource = _jvController.GLHistoryCOASelect(_recordCount, GetConnectionString(), New Object() {SesCompanyID, BranchID, 1, PageSize})
        dtgHistoryCoa.DataBind()
        GridHistoryNavigator.Initialize(_recordCount, PageSize)

    End Sub

     
    Protected Sub DoUpload(sender As Object, e As EventArgs) Handles btnDoUpload.Click
        Dim result = _jvController.GlGelMasUpload(GetConnectionString, _glmas)
        If (result <> "OK") Then
            ShowMessage(lblMessage, result, False)
            Return
        End If
        Response.Redirect("GlBalanceUpload.aspx?s=1")

    End Sub
    Protected Sub download_Click(sender As Object, e As EventArgs) Handles btnDownloadCSV.Click 
        Response.ContentType = "text/csv"
        Response.AddHeader("Content-Disposition", "attachment;filename=COALIST.csv")
        Response.Write(getCsvString())
        Response.End()
    End Sub
    Private Function getCsvString() As String
        Dim result = _jvController.GlGelMasValidateCoaSelect(_recordCount, GetConnectionString(), New Object() {SesCompanyID, BranchID, 1, CInt(ViewState("MAXPAGES"))})
        Return GlJournalVoucher.ParseCOACsv(result)
    End Function

    Protected Sub Back_Click(sender As Object, e As EventArgs) Handles btnBatal.Click
        Response.Redirect("GlBalanceUpload.aspx")
    End Sub
    Private Const PageSize As Integer = 10
    Private _recordCount As Integer = 1
    
    Private Sub InitGrid()
        dtgCSV.DataSource = _jvController.GlGelMasValidateCoaSelect(_recordCount, GetConnectionString(), New Object() {SesCompanyID, BranchID, 1, PageSize})
        dtgCSV.DataBind()
        GridNavigator.Initialize(_recordCount, PageSize)
        ViewState("MAXPAGES") = _recordCount + 1
    End Sub
     
    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        dtgCSV.DataSource = _jvController.GlGelMasValidateCoaSelect(_recordCount, GetConnectionString(), New Object() {SesCompanyID, BranchID, e.CurrentPage, PageSize})
        dtgCSV.DataBind() 
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub
End Class