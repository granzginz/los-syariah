﻿Public Class VarifikasiTabs
    'Inherits System.Web.UI.UserControl
    Inherits TabsBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
        End If
    End Sub

    Protected Overrides Sub initTabs()
        _tabs = New Dictionary(Of String, TabLink) From {
                          {"tabValid", New TabLink(tabValid, hyvalid)},
                          {"tabInvalid", New TabLink(tabInvalid, hyInvalid)}}
    End Sub
    Public Overrides Sub SetNavigateUrl(page As String, id As String)
        MyBase.SetNavigateUrl(page, id)
        _tabs("tabValid").Link.NavigateUrl = "JournalVirifyValidView.aspx?page=" + page + "&id=1&pnl=tabValid"
        _tabs("tabInvalid").Link.NavigateUrl = "JournalVirifyInValidView.aspx?page=" + page + "&id=0&pnl=tabInvalid"
    End Sub


    Public Overrides Sub RefreshAttr(pnl As String)
        MyBase.RefreshAttr(pnl)
        If pnl = "" Then
            _tabs("tabValid").Tab.Attributes.Remove("class")
            _tabs("tabValid").Tab.Attributes.Add("class", "tab_selected")
        End If
    End Sub

End Class