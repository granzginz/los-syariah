﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller 

Module CommonHelpers
    Friend Sub BindGlMasterSequenceDropdown(ByRef ddl As DropDownList, cnn As String, branch As String)
        Dim controller As New JournalVoucherController
        Dim data As IList(Of ValueTextObject) = controller.GetGLMasterSequence(cnn, New Object() {branch}).ToList()

        With ddl
            .DataValueField = "Value"
            .DataTextField = "Text"
            .DataSource = data
            .DataBind()
        End With
    End Sub

    Friend Sub BindGlMasterSequenceDropdownWithNone(ByRef ddl As DropDownList, cnn As String, branch As String)
        Dim controller As New JournalVoucherController
        Dim data As IList(Of ValueTextObject) = controller.GetGLMasterSequence(cnn, New Object() {branch})

        With ddl
            .DataValueField = "Value"
            .DataTextField = "Text"
            .DataSource = data
            .DataBind()
            '.Items.Insert(0, "Select One")
            '.Items(0).Value = "0"
            .Items.Insert(0, "ALL")
            .Items(0).Value = "ALL"
        End With
    End Sub

    Friend Sub BindGlCurrenciesDropdown(ByRef ddl As DropDownList, cnn As String)
        Dim controller As New GlCurrencyController
        Dim data As IList(Of ValueTextObject) = (From c In controller.GetGlCurrencies(cnn) Select New ValueTextObject(c.CurrencyId, c.CurrencyName)).ToList() 
        With ddl
            .DataValueField = "Value"
            .DataTextField = "Text"
            .DataSource = data
            .DataBind()
        End With
    End Sub

    Friend Sub BindDdlBranch(ByRef ddl As DropDownList, cnn As String, sesBranchId As String, isHoBranch As Boolean) 
        With ddl
            If isHoBranch Then
                .DataSource = New DataUserControlController().GetBranchAll(cnn)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0" 
            Else
                .DataSource = New DataUserControlController().GetBranchName(cnn, sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0" 
            End If
        End With 
    End Sub

    Friend Function GetCompanyId(cnn As String) As String
        Dim controller As New JournalVoucherController()
        Return controller.GetCompanyId(cnn)
    End Function

    Friend Function GetBranchName(cnn As String, sesBranchId As String) As String
        Dim _dt As DataTable = New DataUserControlController().GetBranchName(cnn, sesBranchId)
        Dim branchName As String = ""
        If _dt.Rows.Count > 0 Then
            branchName = _dt.Rows(0).Item("Name").ToString
        End If
        Return branchName
    End Function

End Module
