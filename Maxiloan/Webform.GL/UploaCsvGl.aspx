﻿<%--<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="GL.Master" CodeBehind="UploaCsvGl.aspx.vb" Inherits="Maxiloan.Webform.GL.UploaCsvGl" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<%@ Register Src="../webform.UserController/UcSearchBy.ascx" TagName="UcSearchBy" TagPrefix="uc2" %> --%>


<%@ Page Title="" Language="vb" AutoEventWireup="false" 
    MasterPageFile="GL.Master" 
    CodeBehind="UploaCsvGl.aspx.vb" 
    Inherits="Maxiloan.Webform.GL.UploaCsvGl" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"/>
   
    <div class="form_title">
        <div class="title_strip"></div>
        
        <div class="form_single">
            <h3>
               UPLOAD TEMPLATE JOURNAL 
            </h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlLoadCSV">
        <div class="form_box">
            <div class="form_single">
                <label> Load CSV Files </label>
                <asp:FileUpload runat="server" ID="files" ViewStateMode="Enabled"  accept="application/csv" />
                <a onclick='SammpleCSV();' href='#'  > <span>Contoh Format File CSV</span></a>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Load" CausesValidation="False"/>&nbsp;
            <asp:Button ID="btDownLoadFile" runat="server" Text="Download Template" CssClass="small button blue" Visible="true"/>
        </div>
    
    </asp:Panel>
    
     <asp:Panel runat="server" ID="pnlGrid">
          <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    

                    <asp:GridView  ID="dtgCSV" runat="server" 
                        CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"   
                        AllowSorting="True" Width="100%"  DataKeyNames="TransactionNomor"
                       OnRowCommand="grdOrders_RowCommand" 
                      OnRowDataBound ="grdOrders_RowDataBound" >
                        <HeaderStyle CssClass="th" /> 
                        <FooterStyle CssClass="item_grid" />

                        <Columns>
                             <asp:TemplateField ItemStyle-Width="9" >
                                 <ItemTemplate>
                                     <asp:ImageButton ID="ImgBtn" ImageUrl="images/Plus.gif" CommandName="Expand" runat="server" />
                                  </ItemTemplate>
                              </asp:TemplateField> 
                            
                              <asp:BoundField  Visible="True" DataField="TransactionNomor" HeaderText="No Voucher" />
                              <asp:BoundField  Visible="True" DataField="BranchId" HeaderText="BranchID" />
                            <asp:BoundField  Visible="True" DataField="PeroidYear" HeaderText="Peroid Year" />
                            <asp:BoundField  Visible="True" DataField="PeriodMonth" HeaderText="Peroid Month" />
                            <asp:BoundField  Visible="True" DataField="TransactionDate" HeaderText="Tr. Date" DataFormatString="{0:dd-M-yyyy}" />  
                            <asp:BoundField  Visible="True" DataField="ReffDate" HeaderText="Ref. Date" DataFormatString="{0:dd-M-yyyy}" /> 
                            <asp:BoundField  Visible="True" DataField="TransactionDesc" HeaderText="Description" /> 
                            
                            <asp:TemplateField HeaderText = "Balance"> 
                            <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("IsBalance") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                <asp:PlaceHolder ID="objPHOrderDetails" runat="server" Visible="true">
                                <tr>
                                <td width="9" />
                                <td colspan="6">
                                    <asp:UpdatePanel runat="server" ID="ChildControl">
                                    <ContentTemplate>
                                        <asp:GridView   ID="grdDetails" 
                                                     CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"   
                                                    AllowSorting="True" Width="100%" 
                                                    runat="server">
                                             <HeaderStyle CssClass="th tr-det" />
                                            <Columns>
                                                <asp:BoundField DataField="CoaId" HeaderText="COA" />
                                                <asp:BoundField DataField="SequenceNo" HeaderText="Seq" />
                                                 <asp:BoundField DataField="TransactionDesc" HeaderText="Description"  /> 
                                                <asp:BoundField DataField="TransactionId" HeaderText="Transaction ID"  /> 
                                                <asp:BoundField DataField="Post" HeaderText="Post"  /> 
                                                <asp:BoundField DataField="Amount" HeaderText="Amount"  DataFormatString="{0:0.00}" /> 
                                             <%--   <asp:BoundField DataField="PaymentAllocationId" HeaderText="PaymentAllocation ID"  /> --%>
                                                <asp:BoundField DataField="Message" HeaderText="Message" ItemStyle-CssClass="red" /> 
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    </asp:UpdatePanel >
                                   </td>
                                  </asp:PlaceHolder>            
                                  </ItemTemplate>
                              </asp:TemplateField>   
                        </Columns>
                    </asp:GridView >
                </div>
            </div>
        </div>
     </asp:Panel>

    <asp:Panel runat = "server" ID="PanelNotSukses">
    <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    Data Tidak Valid
                </h3>
            </div>
        </div> 
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">  
            <asp:GridView  ID="GridView1" runat="server"  CssClass="grid_general"   showfooter="true" 
                DataKeyNames="TransactionNo" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%"    >
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"  /> 
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" /> 

            <Columns>
                <asp:TemplateField ItemStyle-Width="9" >
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn1" ImageUrl="images/Plus.gif" CommandName="Expand1" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField  DataField="TransactionNo" HeaderText="No Transaksi" />
                <asp:BoundField  DataField="TransactionDate" HeaderText="Tgl Trans"  DataFormatString= "{0:dd/MM/yyyy}" />
                <asp:BoundField  DataField="valueDate" HeaderText="Tgl Valuta"  DataFormatString= "{0:dd/MM/yyyy}" />
                <asp:BoundField  DataField="CanApprove"  visible="false"/>
                <asp:BoundField  DataField="TransactionType" HeaderText="ID : Jenis Transaksi" />
                <asp:BoundField  DataField="Description" HeaderText="Keterangan" />

                <asp:templatefield headertext="Debet" >
                        <itemtemplate>  <div class="t-right"><%#Eval("TotalDebit", "{0:#,0}") %> </div> </itemtemplate>
                        <footertemplate>
                            <div class="t-right">   <asp:Label ID="lblTotalDebit" runat="server" /> </div>
                        </footertemplate>
                </asp:templatefield> 

                <asp:templatefield headertext="Credit" >
                        <itemtemplate> <div class="t-right"><%#Eval("TotalCredit", "{0:#,0}") %> </div></itemtemplate>
                            <footertemplate>
                                 <div class="t-right"> <asp:Label ID="lblTotalCredit" runat="server" /> </div>
                             </footertemplate>
                </asp:templatefield> 
                 
                <asp:TemplateField> 
                <ItemTemplate> 
                <asp:PlaceHolder ID="objPHOrderDetails1" runat="server" >
                <tr>
                <td width="9" />
                <td colspan="7">
                    <asp:UpdatePanel runat="server" ID="ChildControl">
                    <ContentTemplate>
                        <asp:GridView   ID="grdDetails1" CssClass="grid_general"  BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"    AllowSorting="false" Width="98%"  runat="server">
                            <HeaderStyle CssClass="th tr-det" />
                            <Columns>
                                <asp:BoundField DataField="CoaId" HeaderText="COA" /> 
                                    <asp:BoundField DataField="CoaName" HeaderText="COA Name"  /> 
                                    <asp:BoundField DataField="TrDesc" HeaderText="Description"  /> 
                               <asp:BoundField DataField="DebitAmount" HeaderText="Debit"  DataFormatString= "{0:#,0}" > <ItemStyle CssClass="t-right" /></asp:BoundField>
                                <asp:BoundField DataField="CreditAmount" HeaderText="Credit"   DataFormatString= "{0:#,0}" > <ItemStyle CssClass="t-right" /></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    </asp:UpdatePanel >
                    </td>
                    </asp:PlaceHolder>     
                    </ItemTemplate>
                    </asp:TemplateField>    
              </Columns>
            </asp:GridView >
             <uc2:ucGridNav id="GridNavigator" runat="server"/>
            </div>
        </div> 
    </div> 
</asp:Panel>

      <asp:Panel runat="server" ID="pnlUpladProcess" visible="false">
           <div class="form_box">
            <div class="form_single">
          <asp:Button ID="btnDoUpload" runat="server" CssClass="small button blue" Text="UpLoad Confirm" CausesValidation="False"  OnClientClick="if(this.value === 'Saving...') { return false; } else { this.value = 'Saving...'; }"/>&nbsp;
           <asp:Button ID="btnBatal" runat="server" CssClass="small button blue" Text="Back" CausesValidation="False"/>&nbsp;
          </div>
          </div>
      </asp:Panel>
     
    <div class="tx-overlay" style="opacity: 0.5; z-index: 1000; display: none;"></div> 
    <div id="csvSampleDiv" class="t-widget t-window" style="top: 80px; left:80px; width: 1100px;  display:none;">
        <div style="margin:20px;height: 298px;">
        <div class="af05">&nbsp;</div>
             
        </div>
           <button type='button' id='btnOk' style="margin:2px;" onclick="doOK();"> <span> OK</span></button>
    </div>
    
    
    <script type="text/javascript">
        function doOK(e) {
            $("div.tx-overlay").css("display", "none");
            $("#csvSampleDiv").css("display", "none");

        }
        function SammpleCSV() {
            $("div.tx-overlay").css("display", "block");
            $("#csvSampleDiv").css("display", "block");
        }


        function Update_intCell() {
            alert("You Selected Cell 0.");
        }
    </script>
    <style>
        .tr-det { 
         background:  none repeat-x scroll 0 0 gray;
        }
        .red{
            text-decoration:none;
            color:Red;
            border-color: #000;
            } 
        .tx-overlay {
                    background-color: #000;
                    height: 100%;
                    left: 0;
                    opacity: 0.5;
                    position: fixed;
                    top: 0;
                    width: 100%;
                    z-index: 10001;
                }
        .t-window {
            border-radius: 5px;
            border-width: 2px;
            box-shadow: 0 0 5px 2px #aaa;
            display: inline-block;
            position: absolute;
            z-index: 10001;
            background-color: #fff;
            border-color: #a7bac5;
        }
        div.af05{
            background-position:center;
            background-image: url('../images/journalvoucher.png');
            background-repeat:no-repeat;
              height: 305px;
            margin:0; /* If you want no margin */
            padding:0; /*if your want to padding */
        }
    </style>
</asp:Content>
