﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ApprovalAmortisasiBiaya
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AmortisasiJurnal
    Private oController As New AgreementListController
    Private m_controller As New AmortisasiJurnalController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "AmorBiaya"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
                DtgAgree.Visible = True
                BindGridEntity("")
                'oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID,No Aplikasi-Agreementno, No Kontrak" 
            End If
        End If
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AmortisasiJurnal
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'InitialDefaultPanel()

        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = ""
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetApprovalAmortisasiBiaya(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = CType(oCustomClass.TotalRecords, Integer)
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = dtEntity.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()

        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region


    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblAbNo As Label
        Dim NHypRequest As HyperLink
        ' Me.FormID = "SUSPENDALLOCATION"
        'HyReverse()
        If e.Item.ItemIndex >= 0 Then
            lblAbNo = CType(e.Item.FindControl("lblAbNo"), Label)
            ' If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            NHypRequest = CType(e.Item.FindControl("HypRequest"), HyperLink)
            NHypRequest.NavigateUrl = "ApprovalAmortisasiList.aspx?AbNo=" & lblAbNo.Text.Trim
            'NlblTransferRefNO.NavigateUrl = "javascript:OpenWinMain('" & NlblTransferRefNO.Text.Trim & "','" & Nbranchid.Text.Trim & "')"
            'End If

        End If
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim BtnDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            BtnDelete = CType(e.Item.FindControl("BtnDelete"), ImageButton)
            'BtnDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub


#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        'Me.SearchBy = " ab.EffectiveDate between '" & CDate(txtAmortizeStart.Text).ToString("yyyy-MM-dd") & "' and '" & CDate(txtAmortizeEnd.Text).ToString("yyyy-MM-dd") & "'"
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.AmortisasiJurnal

        oParameter.strConnection = GetConnectionString()
        oParameter.PageSize = CType(pageSize, Int16)
        oParameter.WhereCond = Me.SearchBy
        oParameter.SortBy = ""
        oParameter.CurrentPage = currentPage
        'oParameter.StartDate = ConvertDate2(txtAmortizeStart.Text.Trim)
        'oParameter.EndDate = ConvertDate2(txtAmortizeEnd.Text.Trim)
        oParameter = m_controller.GenerateJournalList(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        DtgAgree.DataSource = dtEntity.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()

        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'pnlcopybulandata.Visible = False
        End If

    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


End Class