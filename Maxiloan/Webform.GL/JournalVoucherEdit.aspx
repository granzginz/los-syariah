﻿<%@ Page Language="vb" AutoEventWireup="false"  CodeBehind="JournalVoucherEdit.aspx.vb"
    Inherits="Maxiloan.Webform.GL.JournalVoucherEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  TagPrefix="uc" %> 
<%@ Register Src="../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"  TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-W3CDTD XHTML 1.0 TransitionalEN" "http:www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http:www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    function checkValidation() {
        return confirm('Everything ok?');
    }
</script>
</head>
<body>
<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div id="jlookupContent" runat="server" />
             <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()" /> 
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h3>EDIT JURNAL TRANSAKSI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req"> Cabang</label>
                    <asp:DropDownList ID="ddlCabang" runat="server"></asp:DropDownList>
                    <asp:HiddenField runat="server" ID="HiddenField1" /> 
                    <asp:RequiredFieldValidator ID="rfvCabang" runat="server" Display="Dynamic" CssClass="validator_general" ControlToValidate="ddlCabang" ErrorMessage="pilih cabang" ValidationGroup="header" InitialValue="0"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label class="label_req"> Keterangan</label>
                    <asp:TextBox ID="txtKeterangan" runat="server" CssClass="medium_text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvKeterangan" runat="server" Display="Dynamic" CssClass="validator_general" ControlToValidate="txtKeterangan" ErrorMessage="isi keterangan" ValidationGroup="header"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> Transaksi</label>
                    <asp:DropDownList ID="ddlJenisTransaksi" runat="server"></asp:DropDownList>
                    
                </div>
                <div class="form_right">
                    <label class="label_req">No. Reference</label>
                    <asp:TextBox runat="server" ID="txtNoReference"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> No Voucher</label>
                    <asp:TextBox ID="txtNoVoucher" runat="server" Enabled="false"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>Tanggal Reference</label>
                    <uc:ucdatece runat="server" id="txtTanggalReference"></uc:ucdatece>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Tanggal Voucher</label>
                    <uc:ucdatece runat="server" id="txtTanggalVoucher"></uc:ucdatece>
                </div>
                <div class="form_right">
                <label>Jenis Reference</label>
                    <asp:DropDownList runat="server" ID="ddlReference">
                    </asp:DropDownList>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="txtSequence" />
            <div class="form_title">
                <div class="form_single">
                    <h3> DETAIL TRANSAKSI </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False"
                            DataKeyField="ItemKey" CssClass="grid_general" ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnPers" />
                                        <asp:HiddenField runat="server" ID="hdnCab" />
                                        <asp:HiddenField runat="server" ID="hdnCoA" />
                                        <asp:HiddenField runat = "server" ID="hdnSequence" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../Images/iconedit.gif" CommandName="Update_" OnClientClick="return confirmUpdate_()" 
                                            OnCommand = "CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("ItemKey") %>'/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../Images/icondelete.gif" CommandName="Delete"  OnClientClick="return confirmDelete()"
                                        OnCommand = "CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("ItemKey") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Copy">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnCopy" runat="server" ImageUrl="../Images/IconDocument.gif" CommandName="Copy" OnClientClick="return confirmCopy()" onchange="show()"
                                            OnCommand = "CommandGrid_Click" CausesValidation="true" CommandArgument='<%# eval("ItemKey") %>'/>
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="COA">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPerCabCoA" Text='<%# eval("CoaId") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>                     
                                        <asp:TextBox id="txtCOA" runat="server" Width="50px" class="clsNoCOA" autocomplete="off" CssClass="medium_text" text='<%# DataBinder.Eval(Container.DataItem, "CoaId") %>' />
                                        <button class="small buttongo blue" 
                                        onclick ="OpenLookup_(i);return false;">...</button >                        
                                        <asp:Button runat="server" ID="btnlookup" style="display:none" />
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Nama COA">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPerCabCoAName" Text='<%# eval("CoaName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox id="txtNameCoa" runat="server" Width="150px" class="clsNoCOA" autocomplete="off" CssClass="medium_text" text='<%# DataBinder.Eval(Container.DataItem, "CoaName") %>' />
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KETERANGAN">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblKeterangan" Text='<%# eval("TransactionDesc") %>' />
                                    </ItemTemplate>
                                     <EditItemTemplate>
                                         <asp:TextBox id="txtTransactionDesc" runat="server"   CssClass="multiline_textbox" TextMode="MultiLine" text='<%# DataBinder.Eval(Container.DataItem, "TransactionDesc") %>' />
                                    </EditItemTemplate>
                                     <FooterTemplate>
                                        Selisih : &nbsp; <asp:Label ID="lblSelisih" runat="server"/>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D/C">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblDC" Text='<%# eval("Post") %>'/>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                         <asp:DropDownList  ID="DropDownList1" runat="server" onchange='DC();' style='width: 45px;' text='<%# DataBinder.Eval(Container.DataItem, "Post") %>'>
                                            <asp:ListItem Text="D" Value="D" Selected="True" />
                                            <asp:ListItem Text="C" Value="C" />
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DEBIT" HeaderStyle-CssClass ="th_right" ItemStyle-CssClass="th_right" FooterStyle-CssClass  ="th_right">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblDebit" Text='<%# formatnumber(iif(eval("Post")="C",0,eval("Amount")),0) %>' />
                                    </ItemTemplate>
                                     <EditItemTemplate> 
                                        <%--<uc2:ucnumberformat id="txtJumlahDebit" runat="server" style="width: 100px;" text='<%# formatnumber(iif(eval("Post")="C",0,eval("Amount")),0) %>'></uc2:ucnumberformat> --%>
                                            <asp:TextBox runat="server" ID="txtJumlahDebit" Width="80px" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign reguler_text"
                                            text='<%# formatnumber(iif(eval("Post")="C",0,eval("Amount")),0) %>' ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalDebit" runat="server" /> 
                                    </FooterTemplate>
                                    <FooterStyle CssClass="th_right" />
                                    <HeaderStyle CssClass="th_right" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CREDIT" HeaderStyle-CssClass ="th_right" ItemStyle-CssClass="th_right" FooterStyle-CssClass  ="th_right">
                                    <ItemTemplate> 
                                        <asp:Label runat="server" ID="lblCredit"  Text='<%# formatnumber(iif(eval("Post")="D",0,eval("Amount")),0) %>'/> 
                                    </ItemTemplate>
                                    <EditItemTemplate> 
                                        <asp:TextBox runat="server" ID="txtJumlahCredit" Width="80px" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign reguler_text"
                                            text='<%# formatnumber(iif(eval("Post")="D",0,eval("Amount")),0) %>' ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalCredit" runat="server" /> 
                                    </FooterTemplate>
                                    <FooterStyle CssClass="th_right" />
                                    <HeaderStyle CssClass="th_right" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlEntry" runat="server">
            <div class="form_title">
                    <div class="form_single">
                        <h5> DETAIL TRANSAKSI </h5>
                    </div>
                </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>COA</label>
                            <asp:TextBox ID="txtNoAccountx" runat="server" class="clsNoAccountx" autocomplete="off" CssClass="medium_text" onchange="show()" onkeypress="javascript:return false;"></asp:TextBox>
                            <button class="small buttongo blue" 
                            onclick ="OpenLookup();return false;">...</button >                        
                            <asp:Button runat="server" ID="Jlookup" style="display:none" />
                        </div>
                        <div class="form_right">
                            <label>Nama COA</label>
                            <asp:TextBox ID="txtAccountName" runat="server" class="clsAccountNam" autocomplete="off" CssClass="medium_text" onchange="show()" onkeypress="javascript:return false;"></asp:TextBox>
                        </div>
                    </div>
                  <div class="form_box">
                      <div class="form_left">
                          <label>Jumlah</label>
                          <uc2:ucnumberformat id="txtJumlah" runat="server" ></uc2:ucnumberformat>
                          <input type="button" value="copy" onClick="copy();" />
                      </div>
                      <div class="form_right">
                          <label>Keterangan</label>
                          <asp:TextBox ID="txtKeterangandet" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                      </div>
                  </div>
                  <div class="form_box">
                        <div class="form_left">
                            <label>Jenis Transaksi</label>
                        <asp:DropDownList  ID="rblDK1" runat="server" style='width: 131px;' >
                            <asp:ListItem Text="Debit" Value="D" Selected="True" />
                            <asp:ListItem Text="Kredit" Value="C" />
                        </asp:DropDownList>
                        </div>
                        <div class="form_right">
                        </div>
                  </div>
            <div class="form_button">
                    <asp:Button ID="btnAddToList" runat="server"    CausesValidation="true" Text="Add to List" CssClass="small button blue" ValidationGroup="entry" />
                    <asp:Button ID="btnCancelEntry" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"/>
                    <asp:Button ID="btnCloseEntry" runat="server" CausesValidation="False" Text="Selesai" CssClass="small button gray"/>
                </div>
            </asp:Panel>
            <asp:Panel ID="PanelSave" runat="server">
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="true" Text="Add" CssClass="small button blue" ValidationGroup="header" />
                    <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" ValidationGroup="header" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/>
                    <asp:Button ID="btnDeleteEntry" runat="server" CausesValidation="true" Text="Delete" CssClass="small button red" ValidationGroup="header" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
                </div>
            </asp:Panel>    
            <asp:Panel ID="pnlEn" runat="server"  Visible="false">
            <div class="form_box">
            <div class="form_left" style="width:80% !important">
                  <table cellspacing="0"  width='99%'>
                 <tbody> 
                    <asp:HiddenField ID="hdnCoaTaxID" runat="server" /><asp:HiddenField ID="hdnCoaTaxName" runat="server" />
                    <tr runat="server" id='tr_display' class="tr-display tr-display-hide" >
                     <td><asp:Label ID="lblCoaTaxId" runat="server" style="border: 1px solid lightgray; width: 20px; margin-left: 27px; padding-right: 91px;" /> </td>
                     <td><asp:Label ID="lblCoaTaxName" runat="server" onkeydown="return false;"  autocomplete="off" style="border: medium none; width: auto;"/></td>
                     <td><label class="label_req_"  style="padding: 10px; width: 1px;"></label><uc2:ucnumberformat id="txtJumlah_1" runat="server" Width="250"/></td>
                     <td><asp:TextBox ID="txtKeteranganTax" runat="server" CssClass="multiline_textbox" TextMode="MultiLine" /></td>
                    </tr>
                    </tbody> 
                   </table>
            </div>
            </div>   
            </asp:Panel>
    
    <style>
    .multiline_textbox {min-height :20px;width: 96% !important;max-height :50px;height :20px;}
    .ui-corner-all {font-size: 10pt !important;}
    .tr-display-hide { display:none;}
    .th_right input{ width:100px;}
    </style>

<script type="text/javascript">

     function OpenLookup()  {
                OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/MasterAccIsLeaf.aspx?kodecid=" & txtNoAccountx.ClientID & "&namacid=" & txtAccountName.ClientID  & "&isDt=1" & "&Cabang=" & HiddenField1.value ) %>', 'DAFTAR UNIT', '<%= jlookupContent.ClientID %>', 'DoPostBack')
    }
    function OpenLookup_(i) {
        var grid = document.getElementById('dtg');
        var rowCount = grid.rows.length - 1;
        for (i = 0; i < rowCount; i++) {
            var txtCOA = $('#dtg_txtCOA_' + i).val();
            var txtNameCOA = $('#dtg_txtNameCoa_' + i).val();
            txtcoahdn.text(txtCOA);
                OpenJLookup_('<%= ResolveClientUrl("~/webform.Utility/jLookup/MasterAccIsLeaf.aspx?kodecid=" & txtNoAccountx.ClientID & "&namacid=" & txtAccountName.ClientID  & "&isDt=1") %>', 'DAFTAR UNIT', '<%= jlookupContent.ClientID %>', 'DoPostBack')
            }
    }
    
    function show() {

        var str = document.getElementById('txtNoAccountx').value;
        var res = str.substr(0, 1);
    }
    
     function DoPostBack() {
         __doPostBack('Jlookup', '');
     }

     function DC() { 
        var grid = document.getElementById('dtg');
        var rowCount = grid.rows.length - 1;
        for (i = 0; i < rowCount; i++) {
            var Debit = $('#dtg_txtJumlahDebit_' + i).val();
            var Credit = $('#dtg_txtJumlahCredit_' + i).val();
            var lblDC = $('#dtg_DropDownList1_' + i).val();
            if (lblDC == 'C') {
                $('#dtg_txtJumlahCredit_' + i).val(Debit);
                $('#dtg_txtJumlahDebit_' + i).val(Credit);
            }
            else if (lblDC == 'D') {
                $('#dtg_txtJumlahDebit_' + i).val(Credit);
                $('#dtg_txtJumlahCredit_' + i).val(Debit);
            }
        }

        
    }

     function copy() {

         var first = $('#dtg_lblSelisih').text();
         console.log(first);
            document.getElementById('<%= txtJumlah.ClientID %>').value = first;

        }

     function InitializeRequest(sender, args) {
     }

     function EndRequest(sender, args) {
         InitAutoCompl();
         InitAuto();
      
     }
     $(document).ready(function () {
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_initializeRequest(InitializeRequest);
         prm.add_endRequest(EndRequest);
         InitAutoCompl();
         InitAuto();
      
     });

                function clear()
                { 
                    $(".tr-display").addClass("tr-display-hide");
                    $('#<%=lblCoaTaxId.ClientID%>').text("");
                    $('#<%=lblCoaTaxName.ClientID%>').text("");
                    $('#<%=txtJumlah_1.ClientID%>').val("0");
                    $('#<%=txtKeteranganTax.ClientID%>').val("");
                    $('#<%=hdnCoaTaxID.ClientID%>').val("");
                    $('#<%=hdnCoaTaxName.ClientID%>').val("");
                }
    function InitAuto() {
        var grid = document.getElementById('dtg');
        var rowCount = grid.rows.length - 1;
        for (i = 0; i < rowCount; i++) {
            var COA = $('#dtg_txtCOA_' + i).val();
            var NameCoa = $('#dtg_txtNameCoa_' + i).val();
            $('#dtg_txtCOA_' + i).autocomplete({

                source: function (request, response) {
                    $.ajax({
                        url: "JournalVoucherEdit.aspx/GetCoaIds",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) { response($.map(data.d, function (item) { return { value: item } })) },
                        error: function (XMLHttpRequest, textStatus, errorThrown) { }
                    });
                }
            });

            //$('#dtg_txtNameCoa_' + i).autocomplete({
                        
            //            source: function (request, response) {

            //                $.ajax({
            //                    url: "JournalVoucherEdit.aspx/GetCoaDescs",
            //                    data: "{ 'pre':'" + request.term + "'}",
            //                    dataType: "json",
            //                    type: "POST",
            //                    contentType: "application/json; charset=utf-8",
            //                    success: function (data) { response($.map(data.d, function (item) { return { value: item } } )) },
            //                    error: function (XMLHttpRequest, textStatus, errorThrown) { }
            //                });
            //            } 
            //});
            //$('#dtg_txtNameCoa_' + i).on("autocompletechange", function (event, ui) {
            //         $.ajax({
            //                url: "JournalVoucherEdit.aspx/GetCoaDesc",
            //                data: "{ 'pre':'" + $(this).val() + "'}",
            //                dataType: "json",
            //                type: "POST",
            //                contentType: "application/json; charset=utf-8",
            //                success: function (data) {
            //                    var dt = data.d;
            //                    var grid = document.getElementById('dtg');
            //                    var rowCount = grid.rows.length - 1;
            //                    for (i = 0; i < rowCount; i++) {
            //                        $('#dtg_txtCOA_' + i).val(dt.CoaIdTax);
            //                    }
            //                },
            //                error: function (XMLHttpRequest, textStatus, errorThrown) {
            //                    $('#dtg_txtCOA_' + i).val("");
            //                    clear();
            //                }
            //            }); 
            // });

            $('#dtg_txtCOA_' + i).on("autocompletechange", function (event, ui) {
                $.ajax({
                    url: "JournalVoucherEdit.aspx/GetCoaId",
                    data: "{ 'pre':'" + $(this).val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var dt = data.d;
                        var grid = document.getElementById('dtg');
                        var rowCount = grid.rows.length - 1;
                        for (i = 0; i < rowCount; i++) {
                            $('#dtg_txtNameCoa_' + i).val(dt.Description);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#dtg_txtNameCoa_').val("");
                        clear();
                    }
                });
            });
        }
    };
 
  
    function InitAutoCompl() {

                    $('#<%=txtNoAccountx.ClientID%>').autocomplete({
                        
                        source: function (request, response) {

                            $.ajax({
                                url: "JournalVoucherEdit.aspx/GetCoaIds",
                                data: "{ 'pre':'" + request.term + "'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) { response($.map(data.d, function (item) { return { value: item } } )) },
                                error: function (XMLHttpRequest, textStatus, errorThrown) { }
                            });
                        } 
                    });

                $("#<%=txtNoAccountx.ClientID%>").on("autocompletechange", function (event, ui) {
                   
                     $.ajax({
                         url: "JournalVoucherEdit.aspx/GetCoaId",
                            data: "{ 'pre':'" + $(this).val() + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var dt = data.d;
                                $('#<%=txtAccountName.ClientID%>').val(dt.Description);

                                if (dt.CoaIdTax != '') {
                                    $(".tr-display").removeClass("tr-display-hide");
                                    $('#<%=lblCoaTaxId.ClientID%>').text(dt.CoaIdTax);
                                    $('#<%=lblCoaTaxName.ClientID%>').text(dt.CoaTaxDesc);

                                    $('#<%=hdnCoaTaxID.ClientID%>').val(dt.CoaIdTax);
                                    $('#<%=hdnCoaTaxName.ClientID%>').val(dt.CoaTaxDesc);
                                }
                                else {
                                    clear();
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $('#<%=txtAccountName.ClientID%>').val("");
                                $('#<%=txtKeterangandet.ClientID%>').val("");
                                clear();
                            }
                        }); 
                });

            <%--$('#<%=txtAccountName.ClientID%>').autocomplete({
                        
                        source: function (request, response) {

                            $.ajax({
                                url: "JournalVoucher.aspx/GetCoaDescs",
                                data: "{ 'pre':'" + request.term + "'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) { response($.map(data.d, function (item) { return { value: item } } )) },
                                error: function (XMLHttpRequest, textStatus, errorThrown) { }
                            });
                        } 
                    });--%>
    };

             <%--$("#<%=txtAccountName.ClientID%>").on("autocompletechange", function (event, ui) {
                     $.ajax({
                            url: "JournalVoucher.aspx/GetCoaDesc",
                            data: "{ 'pre':'" + $(this).val() + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var dt = data.d;
                                $('#<%=txtNoAccountx.ClientID%>').val(dt.CoaIdTax);

                                if (dt.CoaIdTax != '') {
                                    $(".tr-display").removeClass("tr-display-hide");
                                    $('#<%=lblCoaTaxId.ClientID%>').text(dt.CoaIdTax);
                                    $('#<%=lblCoaTaxName.ClientID%>').text(dt.CoaTaxDesc);

                                    $('#<%=hdnCoaTaxID.ClientID%>').val(dt.CoaIdTax);
                                    $('#<%=hdnCoaTaxName.ClientID%>').val(dt.CoaTaxDesc);
                                }
                                else {
                                    clear();
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $('#<%=txtNoAccountx.ClientID%>').val("");
                                $('#<%=txtKeterangandet.ClientID%>').val("");
                                clear();
                            }
                        }); 
             });--%>

 
         
</script> 
</form>
</body>
</html>