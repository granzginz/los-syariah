﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AmortisasiBiayaList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AmortisasiJurnal
    Private m_controller As New AmortisasiJurnalController
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Private Const SCHEME_ID As String = "AMBY"
#End Region
#Region "Property"
    Private Property Tr_Nomor() As String
        Get
            Return (CType(ViewState("Tr_Nomor"), String))
        End Get
        Set(value As String)
            ViewState("Tr_Nomor") = value
        End Set
    End Property
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property Nilai() As Double
        Get
            Return (CType(ViewState("Nilai"), Double))
        End Get
        Set(value As Double)
            ViewState("Nilai") = value
        End Set
    End Property
    Private Property ReffNo() As String
        Get
            Return (CType(ViewState("ReffNo"), String))
        End Get
        Set(value As String)
            ViewState("ReffNo") = value
        End Set
    End Property
    Private Property DEBET() As String
        Get
            Return (CType(ViewState("DEBET"), String))
        End Get
        Set(value As String)
            ViewState("DEBET") = value
        End Set
    End Property
    Private Property CREDIT() As String
        Get
            Return (CType(ViewState("CREDIT"), String))
        End Get
        Set(value As String)
            ViewState("CREDIT") = value
        End Set
    End Property
#End Region


#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rCusomClass As New Parameter.AmortisasiJurnal

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.Tr_Nomor = Request.QueryString("Tr_Nomor")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.Tr_Nomor = Me.Tr_Nomor


        oCustomClass.WhereCond = "ab.Tr_Nomor ='" & Me.Tr_Nomor & "'"
        'rCusomClass = m_controller.GetDataSchemeAmortisasiBiaya(oCustomClass)
        'With rCusomClass
        '    lblTr_Nomor.Text = Me.Tr_Nomor
        '    txtTglMulai.Text = .TglMulai.ToString("dd/MM/yyyy")
        '    Me.Amount = .AmountRec
        '    lblNoReference.Text = .NoReference
        '    txtTglAkhir.Text = .TglAkhir.ToString("dd/MM/yyyy")
        '    txtTglEfektif.Text = .TglEfektif.ToString("dd/MM/yyyy")
        '    txtNilai.Text = .AmountRec
        'End With

        'If Not IsPostBack Then
        '    Me.FormID = "DUEDATEREQFACT"
        '    If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
        '        If Request.QueryString("message") <> "" Then
        '            ShowMessage(lblMessage, Request.QueryString("message"), False)
        '        End If
        '        Me.SearchBy = ""
        '        Me.SortBy = ""

        '        'oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID,No Aplikasi-Agreementno, No Kontrak" 
        '    End If
        'End If
        BindGridEntity("gjh.Tr_Nomor ='" & Me.Tr_Nomor & "'")

    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetDataSchemeAmortisasiBiaya(oCustomClass)
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AmortisasiJurnal
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        oCustomClass.WhereCond = cmdWhere
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetDataSchemeAmortisasiBiaya(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = CType(oCustomClass.TotalRecords, Integer)
        Else
            recordCount = 0
        End If

        lblTr_Nomor.Text = oCustomClass.ListData.Rows(0)("Tr_Nomor")
        txtNilai.Text = oCustomClass.ListData.Rows(0)("Amount")
        Me.DEBET = oCustomClass.ListData.Rows(0)("CoaDebit")
        Me.CREDIT = oCustomClass.ListData.Rows(0)("CoaCredit")
        Me.BranchID = Me.sesBranchId.Trim


        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#End Region

#Region "tabel amortisasi"
    Public Sub BuatTable()
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Date")
        myDataColumn.ColumnName = "Tanggal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.int64")
        myDataColumn.ColumnName = "Nilai"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "Debet"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "DebetDesc"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "Credit"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.VarChar")
        myDataColumn.ColumnName = "CreditDesc"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "Tr_Nomor"
        'myDataTable.Columns.Add(myDataColumn)

    End Sub
    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer
        Dim dtEntity As DataTable = Nothing

        intRowMax = CInt(txtTglJangkaWaktu.Text)
        txtNilai.Text = Me.Nilai
        lblNilaiAmortisasi.Text = Me.Nilai / CInt(txtTglJangkaWaktu.Text)
        Me.ReffNo = txtNoReference.Text

        oCustomClass.Tr_Nomor = lblTr_Nomor.Text.Trim
        oCustomClass.Nilai = Me.Nilai
        oCustomClass.JangkaWaktu = intRowMax
        oCustomClass.CoaDebit = Me.DEBET
        oCustomClass.CoaCredit = Me.CREDIT
        oCustomClass.TglMulai = ConvertDate2(txtTglMulai.Text)
        oCustomClass.AmountRec = Me.Nilai / CInt(txtTglJangkaWaktu.Text)
        oCustomClass = m_controller.GetDataGridAmortisasiBiaya(oCustomClass)
        dtEntity = oCustomClass.ListData

        DtgAgree.DataSource = dtEntity.DefaultView
        DtgAgree.DataBind()
        'myDataTable = New DataTable("Installment")

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "No"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.DateTime")
        'myDataColumn.ColumnName = "Tanggal"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Double")
        'myDataColumn.ColumnName = "Amount"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "Debet"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "DebetDesc"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "Credit"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        'myDataColumn.ColumnName = "CreditDesc"
        'myDataTable.Columns.Add(myDataColumn)

        'For inc = 1 To intRowMax
        '    myDataRow = myDataTable.NewRow()
        '    myDataRow("No") = inc
        '    'myDataRow("Tanggal") = oCustomClass.ListData("Tanggal")
        '    myDataRow("Amount") = lblNilaiAmortisasi.Text
        '    myDataRow("Debet") = lblNilaiAmortisasi.Text
        '    'myDataRow("DebetDesc") = oCustomClass.ListData("DebetDesc")
        '    myDataRow("Credit") = lblNilaiAmortisasi.Text
        '    'myDataRow("CreditDesc") = oCustomClass.ListData("CreditDesc")
        '    myDataTable.Rows.Add(myDataRow)
        'Next inc

        'DtgAgree.DataSource = myDataTable.DefaultView
        'DtgAgree.DataBind()

    End Sub
#End Region

#Region "Preview"
    Private Sub BtnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPreview.Click

        Me.Nilai = txtNilai.Text
        BuatTabelInstallment()
    End Sub
#End Region

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("AmortisasiBiaya.aspx")
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim lblNo As New Label
        Dim lblDate As New Date
        Dim lblAmount As New Double
        Dim lblDEBET As New Label
        Dim lblDEBETDESC As New Label
        Dim lblCREDIT As New Label
        Dim lblCREDITDESC As New Label
        Dim oCustomClassheader As New Parameter.AmortisasiJurnal
        Dim strRequestNo As String
        Dim oPCReimburse As New Parameter.AmortisasiJurnal With {
               .strConnection = GetConnectionString().Trim,
               .NoReference = txtNoReference.Text.Trim
           }

        oCustomClass.BranchId = "099"
        oCustomClass.BusinessDate = Me.BusinessDate
        oCustomClass.ID1 = "AMBY"
        oCustomClass.strConnection = GetConnectionString()
        strRequestNo = m_controller.Get_RequestNo(oCustomClass)

        Dim oEntitiesApproval As New Parameter.Approval
        With oEntitiesApproval
            .BranchId = Me.BranchID
            .SchemeID = SCHEME_ID
            .RequestDate = ConvertDate2(txtTglEfektif.Text)
            .ApprovalNote = ""
            .ApprovalValue = Me.Amount
            .UserRequest = Me.Loginid
            .UserApproval = cboApprovedBy.SelectedValue
            .AprovalType = Approval.ETransactionType.AmortisasiBiaya_Approval
            .Argumentasi = ""
            .TransactionNo = txtNoReference.Text.Trim
        End With

        oPCReimburse.Approval = oEntitiesApproval
        Dim result = m_controller.GetApprovalAmortisasiBiaya(oPCReimburse, "R")

        'header
        With oCustomClass
            .AbNo = strRequestNo
            .Tr_Nomor = Me.Tr_Nomor
            .NoReference = txtNoReference.Text
            .TglEfektif = ConvertDate2(txtTglEfektif.Text)
            .TglMulai = ConvertDate2(txtTglMulai.Text)
            .TglAkhir = ConvertDate2(txtTglAkhir.Text)
            .Nilai = CDbl(txtNilai.Text)
            .JangkaWaktu = txtTglJangkaWaktu.Text
            .AmountRec = CDbl(lblNilaiAmortisasi.Text)
            .CoaCredit = Me.CREDIT
            .CoaDebit = Me.DEBET
            'End With

            ''detail
            'With oCustomClass
            .lblNo = lblNo.Text
            .lblDate = lblDate
            .lblAmount = lblAmount
            .lblDEBET = lblDEBET.Text
            .lblDEBETDESC = lblDEBETDESC.Text
            .lblCREDIT = lblDEBETDESC.Text
            .lblCREDITDESC = lblDEBETDESC.Text

            .strConnection = GetConnectionString()
        End With
        m_controller.AmortisasiBiayaSave(oCustomClass)
    End Sub
End Class