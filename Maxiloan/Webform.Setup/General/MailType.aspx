﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MailType.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.MailType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MailType</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server" Width="100%" HorizontalAlign="center">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR JENIS MAIL
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="MailTypeID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="Del"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID" SortExpression="MailTypeID">
                                <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                           <asp:Label ID="lbMailTypeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MailTypeID") %>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MailInitial" SortExpression="MailInitial" HeaderText="NAMA INISIAL SURAT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA JENIS SURAT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DocTemplateName" SortExpression="DocTemplateName" HeaderText="NAMA TEMPLATE DOKUMEN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PICName" SortExpression="PICName" HeaderText="PIC">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PICJobTitle" SortExpression="PICJobTitle" HeaderText="PIC JOB TITLE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SignedName" SortExpression="SignedName" HeaderText="SIGNED NAME">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SignedJobTitle" SortExpression="SignedJobTitle" HeaderText="SignedJobTitle">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="TELEPON">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailDocFlag" SortExpression="MailDocFlag" HeaderText="Mail Doc Flag">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsRecordChanged" SortExpression="IsRecordChanged" HeaderText="Is Record Changed">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtGoPage"
                        Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI JENIS MAIL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="MailTypeID">ID</asp:ListItem>
                    <asp:ListItem Value="Description">Nama</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="75%"  MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    JENIS MAIL -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:Label ID="lblID" runat="server"></asp:Label>
                <asp:TextBox ID="txtID" runat="server"  MaxLength="10" Columns="13"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtID" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Jenis Mail</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                <asp:TextBox ID="txtDescription" runat="server" Width="90%"  MaxLength="100"
                    Columns="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="txtDescription" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Sektor Ekonomi</label>
                <asp:Label ID="lblSector" runat="server"></asp:Label>
                <asp:DropDownList ID="cboSector" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="*"
                    ControlToValidate="cboSector" InitialValue="Select One" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    % Omset</label>
                <asp:Label ID="lblOmset" runat="server"></asp:Label>
                <asp:TextBox ID="txtOmset" runat="server"  MaxLength="9" Columns="12"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                    ControlToValidate="txtOmset" CssClass="validator_general" ></asp:RequiredFieldValidator>&nbsp;
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="% Omset harus antara 0 s/d 100"
                    MinimumValue="0" Type="Double" ControlToValidate="txtOmset" MaximumValue="100" CssClass="validator_general" ></asp:RangeValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnBack" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Button ID="BtnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
