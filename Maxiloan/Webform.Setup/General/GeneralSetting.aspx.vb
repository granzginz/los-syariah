﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class GeneralSetting
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = General.CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Private pageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int16 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Private totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Private recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

    Private Const FILE_NAME_VIEWER As String = "Report/GeneralSettingReportViewer.aspx"
    Private COOKIES_GENERAL_SETTING_REPORT As String = CommonCookiesHelper.COOKIES_GENERAL_SETTING_REPORT

    Protected WithEvents oSearchBy As UcSearchBy
    Private oCustomClass As New Parameter.GeneralSetting
    Private oController As New GeneralSettingController
#End Region
#Region "Property"
    Private Property ModuleID() As String
        Get
            Return (CType(viewstate("ModuleID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ModuleID") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If


        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.ModuleID = Request.QueryString("ModuleID")
            'oSearchBy.ListData = "GSID, ID-GSNAME, Name"
            oSearchBy.ListData = "GSNAME, Name"
            oSearchBy.BindData()
            Me.SortBy = " GSID "

            If Me.SearchBy = "" Then Me.SearchBy = " ModuleID = '" & Me.ModuleID.Replace("'", "") & "' "
            DoBind(Me.SearchBy, Me.SortBy)

        End If

    End Sub

#End Region
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = True
        PnlEdit.Visible = False        
    End Sub

#Region "Upper Case"


    Public Function UpperCase(ByVal strtext As String) As String
        Dim resultText As String = strtext.ToUpper
        Return resultText
    End Function

#End Region
#Region "Search"

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        PnlGrid.Visible = True
        PnlEdit.Visible = False

        Me.SearchBy = " ModuleID = '" & Me.ModuleID.Replace("'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " LIKE '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "Reset "
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        If Me.SearchBy = "" Then Me.SearchBy = " ModuleID = '" & Me.ModuleID.Replace("'", "") & "' "
        oSearchBy.Text = ""
        oSearchBy.BindData()
        If Me.SortBy = "" Then Me.SortBy = "GSID"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "DataBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.GetGeneralSetting(oCustomClass)

        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords

        Else
            recordCount = 0
        End If
       


        DtUserList = oCustomClass.ListData
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        DtgGeneralSetting.DataSource = DvUserList

        Try
            DtgGeneralSetting.DataBind()
        Catch
            DtgGeneralSetting.CurrentPageIndex = 0
            DtgGeneralSetting.DataBind()

        End Try
        PagingFooter()               

    End Sub
#End Region
#Region "Navigation "


    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "DataGrid Command"
    Private Sub DtgGeneralSetting_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgGeneralSetting.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                'If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.Mode = "EDIT"
                PnlGrid.Visible = False
                PnlEdit.Visible = True

                Dim hypID As Label
                hypID = CType(DtgGeneralSetting.Items(e.Item.ItemIndex).FindControl("GsID"), Label)
                Dim hypName As Label
                hypName = CType(DtgGeneralSetting.Items(e.Item.ItemIndex).FindControl("GsName"), Label)
                Dim lblStatus As Label
                lblStatus = CType(DtgGeneralSetting.Items(e.Item.ItemIndex).FindControl("GsValue"), Label)

                Dim LblUpdatable As Label
                LblUpdatable = CType(DtgGeneralSetting.Items(e.Item.ItemIndex).FindControl("Updatable"), Label)

                LblGsID.Text = hypID.Text
                TxtGsName.Text = hypName.Text
                TxtGsValue.Text = lblStatus.Text
                LblHiddenUpdatable.Text = LblUpdatable.Text

                Dim bolBtnSave As Boolean
                bolBtnSave = CType(LblHiddenUpdatable.Text, Boolean)

                If bolBtnSave = True Then
                    btnSave.Visible = True
                Else
                    btnSave.Visible = False
                End If




        End Select
    End Sub
#End Region
#Region "Screen Edit"


    Private Sub BindEditPanel(ByVal DtInfo As DataTable)

        If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_GENERAL_SETTING, CommonVariableHelper.FORM_FEATURE_EDIT, CommonVariableHelper.APPLICATION_NAME) Then
            If sessioninvalid() Then
                Exit Sub
            End If
        End If

        If DtInfo.Rows.Count > 0 Then

            LblGsID.Text = CStr(DtInfo.Rows(0).Item("GsID"))
            TxtGsName.Text = CStr(DtInfo.Rows(0).Item("GsName"))
            TxtGsValue.Text = CStr(DtInfo.Rows(0).Item("GsValue"))

        End If
    End Sub


#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

#End Region
#Region "Go Page"

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtpage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtpage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region
#Region "Sort Grid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "Save Edit"


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click


        If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_GENERAL_SETTING, CommonVariableHelper.FORM_FEATURE_EDIT, CommonVariableHelper.APPLICATION_NAME) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If


        Dim strError As Boolean


        If Page.IsValid Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .GsName = TxtGsName.Text.Trim
                .GsValue = TxtGsValue.Text.Trim
                .GSID = LblGsID.Text
            End With


            strError = oController.GeneralSettingSaveEdit(oCustomClass)
            If strError = False Then
                ShowMessage(LblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                PnlEdit.Visible = False
                PnlGrid.Visible = True
                DoBind(Me.SearchBy, Me.SortBy)
                Exit Sub
            Else
                ShowMessage(LblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
                PnlEdit.Visible = False
                PnlGrid.Visible = True
                DoBind(Me.SearchBy, Me.SortBy)
            End If

        End If

    End Sub
#End Region
#Region "Cancel "


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        PnlEdit.Visible = False
        PnlGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "SendCookies"


    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_GENERAL_SETTING_REPORT)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = Me.SearchBy
            cookie.Values("SortBy") = Me.Sort
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_GENERAL_SETTING_REPORT)
            cookieNew.Values("SearchBy") = Me.SearchBy
            cookieNew.Values("SortBy") = Me.Sort
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region

End Class