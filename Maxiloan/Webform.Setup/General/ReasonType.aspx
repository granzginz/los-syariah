﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReasonType.aspx.vb" Inherits="Maxiloan.Webform.Setup.ReasonType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReasonType</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="Pnllist" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR ALASAN
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgReason" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="ReasonID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="40px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="EDIT" CausesValidation="False"></asp:ImageButton>
                                    <!--<asp:Label ID="lblRTypeID" Runat=server text=<%# container.dataitem("ReasonTypeID")%> Visible=False></asp:Label>-->
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <HeaderStyle Width="50px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="false" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="DELETE" Visible='<%# iif(DataBinder.Eval(Container.DataItem,"IsSystem")=True,"False","True")%>'>
                                    </asp:ImageButton> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID Alasan">
                                <HeaderStyle Width="60px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="hyReasonId" runat="server" Text='<%#Container.DataItem("ReasonID")%>'
                                        CausesValidation="False" CommandName="VIEW">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="Alasan">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rvGo" runat="server" font-names="Verdana"  
                        Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        ControlToValidate="txtGopage" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" font-names="Verdana"  
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPaging" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI ALASAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="REASONID">ID Alasan</asp:ListItem>
                    <asp:ListItem Value="DESCRIPTION">Alasan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    ID Jenis Alasan</label>
                <asp:DropDownList ID="cboSearchReasonTypeID" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvReasonType" InitialValue="0" ErrorMessage="Harap isi Alasan"
                    ControlToValidate="CboSearchReasonTypeId" runat="server" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlAddNew" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    INPUT DATA -&nbsp;
                    <asp:Label ID="lblEntry" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    ID Jenis Alasan</label>
                <asp:DropDownList ID="cboReasonAdd" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvcboReasonAdd" InitialValue="0" ErrorMessage="Harap isi Alasan"
                    ControlToValidate="cboReasonAdd" runat="server" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    ID Alasan</label>
                <asp:TextBox ID="txtReasonID" runat="server"  MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="IndTypeID" runat="server" ControlToValidate="txtReasonID"
                    ErrorMessage="Harap isi ID Jenis Alasan" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Alasan</label>
                <asp:TextBox ID="txtDescription" runat="server" Width="100%"  MaxLength="25"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Description" runat="server" ErrorMessage="Harap isi Alasan"
                    ControlToValidate="txtDescription" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveAddNew" runat="server" CausesValidation="False" Text="Save"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnCancelAddNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW DATA -&nbsp;
                    <asp:Label ID="lblView" runat="server"></asp:Label></h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Jenis Alasan</label>
                <asp:Label ID="lblReasonTypeIDView" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Alasan</label>
                <asp:Label ID="lblReasonIDView" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan</label>
                <asp:Label ID="lblDescriptionView" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnOK" runat="server" CausesValidation="False" Text="OK" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    EDIT DATA -&nbsp;
                    <asp:Label ID="lblEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Jenis Alasan</label>
                <asp:Label ID="lblReasonTypeIDEdit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Alasan</label>
                <asp:Label ID="lblReasonIDEdit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Alasan</label>
                <asp:TextBox ID="txtDescriptionEdit" runat="server" Width="100%" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi alasan"
                    ControlToValidate="txtDescriptionEdit" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveEditNew" runat="server" CausesValidation="False" Text="Save"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnExitEditNew" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
