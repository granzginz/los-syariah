﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.BusinessProcess
#End Region

Public Class ReasonType
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return CStr(viewstate("Mode"))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property ReasonTypeID() As String
        Get
            Return CStr(Viewstate("ReasonTypeID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ReasonTypeID") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
#End Region
#Region "PrivateConstanta"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomclassGeneralPaging As New Parameter.GeneralPaging
    Private Controller_GeneralPaging As New GeneralPagingController
    Private oCustomClassReason As New Parameter.Reason
    Private ControllerReason As New ReasonController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "REASONMNT"

        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""
                InitialDefaultPanel()
                Dim dtReasonType As New DataTable
                With oCustomClassReason
                    .strConnection = GetConnectionString
                End With
                dtReasonType = ControllerReason.GetReasonTypeCombo(oCustomClassReason)
                Dim i As Integer
                For i = 0 To dtReasonType.Rows.Count - 1
                    If Not (Me.Cache.Item("Reason" & dtReasonType.Rows(i).Item("ReasonTypeID").ToString.Trim)) Is Nothing Then
                        Me.Cache.Remove("Reason" & dtReasonType.Rows(i).Item("ReasonTypeID").ToString.Trim)
                    End If
                Next
                With cboSearchReasonTypeID
                    .DataTextField = "ReasonTypeDescription"
                    .DataValueField = "ReasonTypeID"
                    .DataSource = dtReasonType
                    .DataBind()
                End With
                With cboReasonAdd
                    .DataTextField = "ReasonTypeDescription"
                    .DataValueField = "ReasonTypeID"
                    .DataSource = dtReasonType
                    .DataBind()
                End With
                lblMessage.Text = ""
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlPaging.Visible = True
        Pnllist.Visible = False
        PnlAddNew.Visible = False
        PnlView.Visible = False
        PnlEdit.Visible = False
    End Sub
    Private Sub InitialCancelPanel()
        pnlPaging.Visible = True
        Pnllist.Visible = True
        PnlAddNew.Visible = False
        PnlView.Visible = False
        PnlEdit.Visible = False
        lblMessage.Text = ""
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblTotRec.Text = recordCount.ToString
        pnlPaging.Visible = True
        Pnllist.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        pnlPaging.Visible = True
        Pnllist.Visible = True
        BindGrid()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGrid()
                End If
            End If
        End If
        pnlPaging.Visible = True
        Pnllist.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGrid()
        Pnllist.Visible = True
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGrid()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oReasonInq As New Parameter.GeneralPaging

        With oCustomclassGeneralPaging
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .SpName = "spReasonPaging"
        End With
        oReasonInq = Controller_GeneralPaging.GetGeneralPaging(oCustomclassGeneralPaging)
        recordCount = oReasonInq.TotalRecords

        dtsEntity = oReasonInq.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = Me.SortBy
        dtgReason.DataSource = dtvEntity
        Try
            dtgReason.DataBind()
        Catch
            dtgReason.CurrentPageIndex = 0
            dtgReason.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region

    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            cmdwhere = ""
            Me.SearchBy = ""
            Me.SortBy = ""
            Me.BindMenu = ""
            If txtSearch.Text <> "" Then
                If cboSearch.SelectedItem.Value.Trim = "REASONID" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        cmdwhere = cmdwhere + "Reason.reasonId like '" & txtSearch.Text.Trim & "' and "
                    Else
                        cmdwhere = cmdwhere + "Reason.reasonId = '" & txtSearch.Text.Trim & "' and "
                    End If
                ElseIf cboSearch.SelectedItem.Value.Trim = "DESCRIPTION" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        cmdwhere = cmdwhere + "Reason.DESCRIPTION like '" & txtSearch.Text.Trim & "' and "
                    Else
                        cmdwhere = cmdwhere + "Reason.DESCRIPTION = '" & txtSearch.Text.Trim & "' and "
                    End If
                End If
            End If
            cmdwhere = cmdwhere + "Reason.ReasonTypeId = '" & cboSearchReasonTypeID.SelectedItem.Value.Trim & "'"
            Me.ReasonTypeID = cboSearchReasonTypeID.SelectedItem.Value.Trim
            Me.SearchBy = cmdwhere
            BindGrid()
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("ReasonType.aspx")
    End Sub

    Private Sub btnSaveAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddNew.Click

        If txtReasonID.Text = "" Or txtDescription.Text = "" Then

            ShowMessage(lblMessage, "*) Perlu diisi", True)
        Else
            With oCustomClassReason
                .strConnection = GetConnectionString()
                .ReasonID = txtReasonID.Text
                .ReasonTypeID = cboReasonAdd.SelectedItem.Value.Trim
                .ReasonDescription = txtDescription.Text
            End With
            Try
                ControllerReason.ReasonAdd(oCustomClassReason)
                InitialCancelPanel()
                BindGrid()
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub
    Private Sub btnCancelAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAddNew.Click
        InitialCancelPanel()
    End Sub

    Private Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        pnlPaging.Visible = False
        Pnllist.Visible = False
        PnlAddNew.Visible = True
        lblEntry.Text = "Reason"
        Me.Mode = "ADD"
        txtReasonID.Text = ""
        txtDescription.Text = ""
    End Sub

    Private Sub dtgReason_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgReason.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                pnlPaging.Visible = False
                Pnllist.Visible = False
                PnlEdit.Visible = True
                lblEdit.Text = "Reason"
                Me.Mode = "EDIT"
                Dim HyReasonId As LinkButton
                HyReasonId = CType(dtgReason.Items(e.Item.ItemIndex).FindControl("hyReasonId"), LinkButton)
                With oCustomClassReason
                    .strConnection = GetCOnnectionString
                    .ReasonID = HyReasonId.Text
                    .ReasonTypeID = Me.ReasonTypeID
                End With
                oCustomClassReason = ControllerReason.ReasonView(oCustomClassReason)
                With oCustomClassReason
                    lblReasonTypeIDEdit.Text = .ReasonTypeID
                    lblReasonIDEdit.Text = .ReasonID
                    txtDescriptionEdit.Text = .ReasonDescription
                End With

            Case "DELETE"
                'pnlPaging.Visible = False
                'Pnllist.Visible = False
                'PnlView.Visible = True
                Dim HyReasonId As LinkButton
                HyReasonId = CType(dtgReason.Items(e.Item.ItemIndex).FindControl("hyReasonId"), LinkButton)
                With oCustomClassReason
                    .strConnection = GetCOnnectionString
                    .ReasonID = HyReasonId.Text
                    .ReasonTypeID = Me.ReasonTypeID
                End With
                Try
                    ControllerReason.ReasonDelete(oCustomClassReason)
                    BindGrid()
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            Case "VIEW"
                pnlPaging.Visible = False
                Pnllist.Visible = False
                PnlView.Visible = True
                lblView.Text = "Reason"
                Me.Mode = "View"
                Dim HyReasonId As LinkButton
                HyReasonId = CType(dtgReason.Items(e.Item.ItemIndex).FindControl("hyReasonId"), LinkButton)
                With oCustomClassReason
                    .strConnection = GetCOnnectionString
                    .ReasonID = HyReasonId.Text
                    .ReasonTypeID = Me.ReasonTypeID
                End With
                oCustomClassReason = ControllerReason.ReasonView(oCustomClassReason)
                With oCustomClassReason
                    lblReasonTypeIDView.Text = .ReasonTypeID
                    lblReasonIDView.Text = .ReasonID
                    lblDescriptionView.Text = .ReasonDescription
                End With
        End Select
    End Sub

    Private Sub btnSaveEditNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveEditNew.Click
        If txtDescriptionEdit.Text = "" Then

            ShowMessage(lblMessage, "Harap isi dengan Nama", True)
        Else
            With oCustomClassReason
                .strConnection = GetConnectionString()
                .ReasonID = lblReasonIDEdit.Text.Trim
                .ReasonTypeID = lblReasonTypeIDEdit.Text.Trim
                .ReasonDescription = txtDescriptionEdit.Text.Trim
            End With
            Try
                If Me.Mode = "EDIT" Then
                    ControllerReason.ReasonEdit(oCustomClassReason)
                End If
                InitialCancelPanel()
                BindGrid()
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Sub btnExitEditNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitEditNew.Click
        InitialCancelPanel()
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        InitialCancelPanel()
    End Sub

End Class