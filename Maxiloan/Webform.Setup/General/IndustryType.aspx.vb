﻿
#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class IndustryType
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New IndustryTypeController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            If CheckForm(Me.Loginid, "IndustryType", "MAXILOAN") Then
                fillcboSector()
                txtgoPage.Text = "1"
                Me.Sort = "IndustryTypeID ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"

                End If
                BindGridEntity(Me.CmdWhere)

                If Request("cmd") = "dtl" Then
                    If CheckFeature(Me.Loginid, "IndustryType", "View", "MAXILOAN") Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If
                    BindDetail(Request("id"), Request("desc"))
                End If
                BtnClose.Attributes.Add("OnClick", "return fClose()")
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub fillcboSector()
        Dim dtEntity As DataTable
        Dim oIndustryType As New Parameter.IndustryType
        oIndustryType.strConnection = GetConnectionString()
        oIndustryType = m_controller.Getsector(oIndustryType)
        dtEntity = oIndustryType.ListData
        cboSector.DataSource = dtEntity
        cboSector.DataTextField = "Description"
        cboSector.DataValueField = "EconomySectorID"
        cboSector.DataBind()
        cboSector.Items.Insert(0, "Select One")
        cboSector.Items(0).Value = "Select One"
    End Sub


    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oIndustryType As New Parameter.IndustryType
        InitialDefaultPanel()
        oIndustryType.strConnection = GetConnectionString()
        oIndustryType.WhereCond = cmdWhere
        oIndustryType.CurrentPage = currentPage
        oIndustryType.PageSize = pageSize
        oIndustryType.SortBy = Me.Sort
        oIndustryType = m_controller.GetIndustryType(oIndustryType)

        If Not oIndustryType Is Nothing Then
            dtEntity = oIndustryType.ListData
            recordCount = oIndustryType.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            btnPrint.Enabled = False
        Else
            btnPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal Desc As String)
        Dim oIndustryType As New Parameter.IndustryType
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd2") = "close" Then
            BtnBack.Visible = False
            BtnClose.Visible = True
        Else
            BtnBack.Visible = True
            BtnClose.Visible = False
        End If
        BtnCancel.Visible = False
        BtnSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oIndustryType.Id = ID
        oIndustryType.strConnection = GetConnectionString()
        oIndustryType = m_controller.GetIndustryTypeEdit(oIndustryType)

        lblID.Visible = True
        txtID.Visible = False
        lblDescription.Visible = True
        txtDescription.Visible = False
        cboSector.Visible = False
        lblSector.Visible = True
        txtOmset.Visible = False
        lblOmset.Visible = True

        lblID.Text = ID
        lblDescription.Text = Desc
        lblSector.Text = oIndustryType.Sector.Trim
        lblOmset.Text = oIndustryType.Omset.Trim
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Protected Sub BtnGoPage_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oIndustryType As New Parameter.IndustryType
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "IndustryType", "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            BtnBack.Visible = False
            BtnCancel.Visible = True
            BtnSave.Visible = True
            BtnClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oIndustryType.Id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oIndustryType.strConnection = GetConnectionString()
            oIndustryType = m_controller.GetIndustryTypeEdit(oIndustryType)
            lblID.Visible = True
            txtID.Visible = False
            lblDescription.Visible = False
            txtDescription.Visible = True
            cboSector.Visible = True
            lblSector.Visible = False
            txtOmset.Visible = True
            lblOmset.Visible = False
            lblID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            txtID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            txtDescription.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(3).Text
            '  cboSector.Items.FindByText(oIndustryType.Sector).Selected = True
            cboSector.SelectedIndex = cboSector.Items.IndexOf(cboSector.Items.FindByText(oIndustryType.Sector))
            txtOmset.Text = oIndustryType.Omset.Trim

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "IndustryType", "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.IndustryType
            With customClass
                .Id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.IndustryTypeDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
        End If
    End Sub
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.IndustryType
        Dim ErrMessage As String = ""
        With customClass
            .Id = txtID.Text
            .Description = txtDescription.Text
            .Sector = cboSector.SelectedValue
            .Omset = txtOmset.Text
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.IndustryTypeAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            m_controller.IndustryTypeUpdate(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, "IndustryType", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        BtnBack.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True
        BtnClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        lblID.Visible = False
        txtID.Visible = True
        lblDescription.Visible = False
        txtDescription.Visible = True
        cboSector.Visible = True
        lblSector.Visible = False
        txtOmset.Visible = True
        lblOmset.Visible = False
        txtID.Text = ""
        txtDescription.Text = ""
        txtOmset.Text = "0"
        cboSector.SelectedIndex = 0
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If CheckFeature(Me.Loginid, "IndustryType", "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/IndustryTypeRpt.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("IndustryType")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("IndustryType")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("IndustryType.aspx")
    End Sub


    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnClose.Click
        Response.Redirect("IndustryType.aspx")
    End Sub

    Protected Sub BtnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnBack.Click
        Response.Redirect("IndustryType.aspx")
    End Sub




End Class