﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Vendor.aspx.vb" Inherits="Maxiloan.Webform.Setup.Vendor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DAFTAR VENDOR</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR VENDOR
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="VendorID" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="NamaVendor" SortExpression="NamaVendor" HeaderText="NAMA VENDOR">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BidangUsaha" SortExpression="BidangUsaha" HeaderText="BIDANG USAHA">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Kontak" SortExpression="Kontak" HeaderText="PIC"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Alamat" SortExpression="Alamat" HeaderText="ALAMAT">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue" />
                            <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtPage"
                                Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue" Visible="false">
                    </asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI VENDOR
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NamaVendor">NAMA VENDOR</asp:ListItem>
                            <asp:ListItem Value="BidangUsaha">BIDANG USAHA</asp:ListItem>
                            <asp:ListItem Value="Kontak">PIC</asp:ListItem>
                            <asp:ListItem Value="Alamat">ALAMAT</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%"  MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            Nama VENDOR </label>
                        <asp:TextBox ID="txtNamaVendor" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNamaVendor" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Bidang Usaha</label>
                        <asp:TextBox ID="txtBidangUsaha" runat="server"></asp:TextBox>
                    </div>
                </div>
                <%--edit npwp, ktp, telepon by ario--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_split_req">
                            NPWP</label>
                        <asp:TextBox ID="txtNPWP" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" Display="Dynamic"
                        ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi NPWP dengan Angka"
                        ControlToValidate="txtNPWP"  CssClass ="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNPWP"
                        ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            TDP</label>
                        <asp:TextBox ID="txtTDP" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            SIUP</label>
                        <asp:TextBox ID="txtSIUP" runat="server"></asp:TextBox>
                    </div>
                </div>
               <%-- <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Mulai Jadi Vendor</label>
                        <asp:TextBox ID="tglMulaiVendor" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="tglMulaiVendor_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="tglMulaiVendor" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>--%>
                 <div class="form_box">
                    <div class="form_single">
                    <label>
                        Tanggal Mulai Jadi Vendor
                    </label>
                    <uc1:ucdatece id="tglMulaiVendor" runat="server"></uc1:ucdatece>     
                   <%-- <asp:Label ID="lblErrMsgInvoiceDate" runat="server" Visible="False" CssClass="validator_general"></asp:Label>   --%>            
                </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALAMAT *)
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <uc1:uccompanyaddress id="UcCompanyAddress" runat="server"></uc1:uccompanyaddress>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            KONTAK *)
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama</label>
                        <asp:TextBox ID="txtKontak" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jabatan</label>
                        <asp:TextBox ID="txtJabatan" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            E-mail</label>
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            No HandPhone</label>
                        <asp:TextBox ID="txtNoHP" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="expNoHP" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,15}"
                            ErrorMessage="Harap isi Nomor HP dengan Angka" ControlToValidate="txtNoHP"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="expNoHP1" runat="server" ControlToValidate="txtNoHP"
                            ErrorMessage="Harap isi Nomor HP" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            REKENING BANK *)
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                        CssClass="small button gray"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonClose" runat="server" CausesValidation="False" Text="Close"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <input id="hdnVendorID" type="hidden" name="hdnVendorID" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonBack" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonClose" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonReset" EventName="Click" />
            <asp:PostBackTrigger ControlID="dtgPaging" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
