﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EmployeePosition.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.EmployeePosition" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EmployeePosition</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var ServerName = '<%# Request.ServerVariables("SERVER_NAME")%>';
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function PlanRALChange() {
            var myID = 'UcRALDate_txtDate';
            var objDate = eval('document.forms[0].' + myID);
            var result

            if (document.forms[0].rbRAL_0.checked = true) {
                result = 'Plan';
            }
            if (document.forms[0].rbRAL_1.checked = true) {
                result = 'Undo Plan';
            }

            alert(result);
            if (result == 'Undo Plan') {
                objDate.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].UcRALDate_txtDate.value = '';
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR JABATAN KARYAWAN
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False" Visible='<%#iif(container.dataitem("isSystem")="1",false,true)%>'>
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="EmployeePositionID" SortExpression="EmployeePositionID"
                                HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="EmployeePosition" SortExpression="EmployeePosition" HeaderText="NAMA JABATAN">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="isSystem"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGopage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No halaman Salah" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnPrint2" runat="server" Enabled="true" Visible="false" Text="Print" CssClass="small button green">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI JABATAN KARYAWAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="EmployeePositionID">ID</asp:ListItem>
                    <asp:ListItem Value="EmployeePosition">Nama Jabatan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <!------------------------------------------------->
    <asp:Panel ID="PnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    JABATAN KARYAWAN -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
              <label class ="label_req">
                    ID Jabatan</label>
                <asp:TextBox ID="txtID" runat="server" Width="100px"  MaxLength="200"></asp:TextBox>
                <asp:Label ID="lblID" runat="server" Width="129px" Visible="False"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                    Display="Dynamic" ControlToValidate="txtID" ErrorMessage="Harap isi ID" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Nama Jabatan</label>
                <asp:TextBox ID="txtDescription" runat="server" Width="300px" 
                    MaxLength="200"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="184px"
                    Display="Dynamic" ControlToValidate="txtDescription" ErrorMessage="Harap isi Nama Jabatan" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
