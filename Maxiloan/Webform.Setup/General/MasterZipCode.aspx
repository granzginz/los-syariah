﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MasterZipCode.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.MasterZipCode" %>

<%@ Register TagPrefix="uc2" TagName="ucNumber" Src="../../Webform.UserController/ucNumber.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MasterZipCode</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:RangeValidator ID="rgvGo" runat="server" Font-Size="8" ControlToValidate="txtGoPage"
        MinimumValue="1" ErrorMessage="No Halaman Salah" Type="Double" CssClass="validator_general" ></asp:RangeValidator>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR KODE POS
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgEntity" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="Kelurahan" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Kelurahan" SortExpression="Kelurahan" HeaderText="KELURAHAN"
                                ItemStyle-Width="17%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Kecamatan" SortExpression="Kecamatan" HeaderText="KECAMATAN"
                                ItemStyle-Width="17%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="City" SortExpression="City" HeaderText="KOTA" ItemStyle-Width="17%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ZipCode" SortExpression="ZipCode" HeaderText="KODEPOS"
                                ItemStyle-Width="10%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SandiWilayah" SortExpression="SandiWilayah" HeaderText="SANDI WILAYAH" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="Provinsi" SortExpression="Provinsi" HeaderText="PROVINSI" ></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" /></asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server"  Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtgopage" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"   ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtgopage" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI KODE POS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Kelurahan">Kelurahan</asp:ListItem>
                    <asp:ListItem Value="Kecamatan">Kecamatan</asp:ListItem>
                    <asp:ListItem Value="City">Kota</asp:ListItem>
                    <asp:ListItem Value="ZipCode">KodePos</asp:ListItem>
                    <asp:ListItem Value="SandiWilayah">Sandi Wilayah</asp:ListItem>
                    <asp:ListItem Value="Provinsi">Provinsi</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KODE POS -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Kelurahan</label>
                <asp:TextBox ID="txtKelurahan" runat="server"  MaxLength="30" Columns="30"></asp:TextBox>
                <asp:Label ID="labelKelurahan" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi dengan Kelurahan"
                    ControlToValidate="txtKelurahan" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Kecamatan</label>
                <asp:TextBox ID="txtKecamatan" runat="server"  MaxLength="30" Columns="30"></asp:TextBox>
                <asp:Label ID="Label2" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi dengan Kecamatan"
                    ControlToValidate="txtKecamatan" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Kota</label>
                <asp:TextBox ID="txtCity" runat="server"  MaxLength="30" Columns="30"></asp:TextBox>
                <asp:Label ID="Label3" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Harap isi dengan Kota"
                    ControlToValidate="txtCity" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kode Pos</label>
                <uc2:ucnumber id="ucZipCode" runat="server"></uc2:ucnumber>
                <asp:Label ID="Label4" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">Sandi Wilayah</label>
                <asp:TextBox ID="txtSandiWilayah" runat="server"  MaxLength="10" ></asp:TextBox>
                <asp:Label ID="Label1" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ErrorMessage="Harap isi dengan sandi wilayah"
                    ControlToValidate="txtSandiWilayah" CssClass="validator_general" ></asp:RequiredFieldValidator>

            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">Provinsi</label>
                <asp:DropDownList runat="server" ID="cboProvinsi" />
                <asp:Label ID="Label5" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage="Harap isi dengan provinsi" 
                    ControlToValidate="cboProvinsi" CssClass="validator_general" InitialValue="0" ></asp:RequiredFieldValidator>
            </div>
        </div>
<%--        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Grup Collection</label>
                <asp:DropDownList ID="cboCG" runat="server" onchange="<%# drdCompanyChange() %>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFVcboCG" runat="Server" ErrorMessage="Harap Pilih Grup Collection"
                    ControlToValidate="CBOCG" Enabled="False" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>--%>
<%--        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Collector</label>
                <asp:DropDownList ID="cboCollector" runat="server" onchange="setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFVcboCollector" runat="Server" ErrorMessage="Harap Pilih Collector"
                    ControlToValidate="cboCollector" Enabled="False" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>--%>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
