﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
#End Region

Public Class Vendor
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcCompanyAddress As UcCompanyAddress
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents tglMulaiVendor As ucDateCE

    Private m_controller As New VendorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CType(UcBankAccount.BankID, String)
        End Get
        Set(ByVal Value As String)
            UcBankAccount.BankID = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "VENDOR"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtPage.Text = "1"
                Me.Sort = "VendorID ASC"
                Me.CmdWhere = "All"
                BindGridEntity(Me.CmdWhere)
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oVendor As New Parameter.Vendor
        InitialDefaultPanel()
        oVendor.strConnection = GetConnectionString()
        oVendor.WhereCond = cmdWhere
        oVendor.CurrentPage = currentPage
        oVendor.PageSize = pageSize
        oVendor.SortBy = Me.Sort
        oVendor = m_controller.GetVendor(oVendor)

        If Not oVendor Is Nothing Then
            dtEntity = oVendor.Listdata
            recordCount = oVendor.Totalrecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oVendor As New Parameter.Vendor
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonClose.Visible = False
            ButtonCancel.CausesValidation = False

            lblTitleAddEdit.Text = Me.AddEdit
            oVendor.VendorID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString.Trim
            oVendor.strConnection = GetConnectionString()
            oVendor = m_controller.GetVendorList(oVendor)

            'Bind
            hdnVendorID.Value = oVendor.VendorID.Trim
            txtNamaVendor.Text = oVendor.NamaVendor.Trim
            txtBidangUsaha.Text = oVendor.BidangUsaha.Trim
            txtNPWP.Text = oVendor.NPWP.Trim
            txtTDP.Text = oVendor.TDP.Trim
            txtSIUP.Text = oVendor.SIUP.Trim
            tglMulaiVendor.Text = CDate(oVendor.TglMulai.Trim).ToString("dd/MM/yyyy")

            With UcCompanyAddress
                .Address = oVendor.Alamat.Trim
                .RT = oVendor.RT.Trim
                .RW = oVendor.RW.Trim
                .Kelurahan = oVendor.Kelurahan.Trim
                .Kecamatan = oVendor.Kecamatan.Trim
                .City = oVendor.Kota.Trim
                .ZipCode = oVendor.KodePos.Trim
                .AreaPhone1 = oVendor.AreaPhone1.Trim
                .Phone1 = oVendor.Phone1.Trim
                .AreaPhone2 = oVendor.AreaPhone2.Trim
                .Phone2 = oVendor.Phone2.Trim
                .AreaFax = oVendor.AreaFax.Trim
                .Fax = oVendor.Fax.Trim
                .Style = "Setting"
                .BindAddress()
            End With
            txtKontak.Text = oVendor.Kontak.Trim
            txtJabatan.Text = oVendor.Jabatan.Trim
            txtEmail.Text = oVendor.Email.Trim
            txtNoHP.Text = oVendor.NoHP.Trim

            With UcBankAccount
                .BankBranchId = oVendor.BankBranchID
                .BankID = oVendor.BankID.Trim
                .AccountNo = oVendor.AccounNo.Trim
                .AccountName = oVendor.AccountName.Trim
                .Style = "Setting"
                .BindBankAccount()
            End With
            BankID = oVendor.BankID.Trim

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.Vendor
            With customClass
                .VendorID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.VendorDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
                lblMessage.Visible = True
            Else
               ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.Vendor
        Dim ErrMessage As String = ""
        With customClass
            .VendorID = ""
            .NamaVendor = txtNamaVendor.Text.Trim
            .BidangUsaha = txtBidangUsaha.Text.Trim
            .NPWP = txtNPWP.Text.Trim
            .TDP = txtTDP.Text.Trim
            .SIUP = txtSIUP.Text.Trim
            '.TglMulai = Date.ParseExact(tglMulaiVendor.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .TglMulai = IIf(tglMulaiVendor.Text <> "", ConvertDateSql(tglMulaiVendor.Text), "").ToString
            .Alamat = UcCompanyAddress.Address.Trim
            .RT = UcCompanyAddress.RT.Trim
            .RW = UcCompanyAddress.RW.Trim
            .Kelurahan = UcCompanyAddress.Kelurahan.Trim
            .Kecamatan = UcCompanyAddress.Kecamatan.Trim
            .Kota = UcCompanyAddress.City.Trim
            .KodePos = UcCompanyAddress.ZipCode.Trim
            .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
            .Phone1 = UcCompanyAddress.Phone1.Trim
            .AreaPhone2 = UcCompanyAddress.AreaPhone2.Trim
            .Phone2 = UcCompanyAddress.Phone2.Trim
            .AreaFax = UcCompanyAddress.AreaFax.Trim
            .Fax = UcCompanyAddress.Fax.Trim
            .Kontak = txtKontak.Text.Trim
            .Jabatan = txtJabatan.Text.Trim
            .Email = txtEmail.Text.Trim
            .NoHP = txtNoHP.Text.Trim
            .BankBranchID = UcBankAccount.BankBranchId
            .BankID = BankID
            .AccounNo = UcBankAccount.AccountNo.Trim
            .AccountName = UcBankAccount.AccountName.Trim
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.VendorSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.VendorID = hdnVendorID.Value
            m_controller.VendorSaveEdit(customClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            lblMessage.Visible = True
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub ButtonAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        ButtonBack.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonClose.Visible = False
        ButtonCancel.CausesValidation = False
        Me.AddEdit = "ADD"

        lblTitleAddEdit.Text = Me.AddEdit

        'Bind
        txtNamaVendor.Text = Nothing
        txtBidangUsaha.Text = Nothing
        txtNPWP.Text = Nothing
        txtTDP.Text = Nothing
        txtSIUP.Text = Nothing
        tglMulaiVendor.Text = Nothing
        With UcCompanyAddress
            .Address = Nothing
            .RT = Nothing
            .RW = Nothing
            .Kelurahan = Nothing
            .Kecamatan = Nothing
            .City = Nothing
            .ZipCode = Nothing
            .AreaPhone1 = Nothing
            .Phone1 = Nothing
            .AreaPhone2 = Nothing
            .Phone2 = Nothing
            .AreaFax = Nothing
            .Fax = Nothing
            .Style = "Setting"
            .BindAddress()
        End With
        txtKontak.Text = Nothing
        txtJabatan.Text = Nothing
        txtEmail.Text = Nothing
        txtNoHP.Text = Nothing

        With UcBankAccount
            .BankBranchId = Nothing
            .BankID = Nothing
            .AccountNo = Nothing
            .AccountName = Nothing
            .Style = "Setting"
            .BindBankAccount()
        End With

    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("Vendor")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Vendor")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "All"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "All"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub ButtonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("Vendor.aspx")
    End Sub
    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("Vendor.aspx")
    End Sub
End Class