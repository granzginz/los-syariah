﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GeneralSetting.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.GeneralSetting" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GeneralSetting</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    DAFTAR GENERAL SETTING
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="DtgGeneralSetting" runat="server" Width="100%" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyField="GsID" BorderColor="#CCCCCC" BorderStyle="None"
                    BorderWidth="1px" BackColor="White" OnSortCommand="SortGrid" CellPadding="0"
                    CssClass="grid_general">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle CssClass="th" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="EDIT">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/IconEdit.gif"
                                    CommandName="EDIT" CausesValidation="False" Visible='<%# DataBinder.Eval(Container, "DataItem.ISUPDATABLE") %>'>
                                </asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="GSID" HeaderText="ID">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="GsID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GsID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="GSName" HeaderText="NAMA">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="GsName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GsName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="GsValue" HeaderText="NILAI">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="GsValue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GSValue") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="IsUpdatable" HeaderText="BISA DIUPDATE">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Updatable" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ISUPDATABLE") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server"   ControlToValidate="txtpage"
                        MinimumValue="1" ErrorMessage="No Halaman salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  
                        ControlToValidate="txtpage" ErrorMessage="No halaman salah" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
     
    </asp:Panel>
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    GENERAL SETTING
                </h4>
            </div>
        </div>
        <div class="form_box_uc">            
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    GENERAL SETTING
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:Label ID="LblGsID" runat="server">LblGsID</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Nama</label>
                <asp:TextBox ID="TxtGsName" runat="server" Height="112px" Width="432px" 
                    TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi nama"
                    ControlToValidate="TxtGsName" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Nilai</label>
                <asp:TextBox ID="TxtGsValue" runat="server" Height="114px" Width="432px" 
                    TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap isi Nilai"
                    ControlToValidate="TxtGsValue" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:Label ID="LblHiddenUpdatable" runat="server" Visible="False">LblHiddenUpdatable</asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
