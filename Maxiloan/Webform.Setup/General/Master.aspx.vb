﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region


Public Class Master
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New MasterController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property TableName() As String
        Get
            Return CType(ViewState("vwsTable"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsTable") = Value
        End Set
    End Property
    Private Property KeyWord() As String
        Get
            Return CType(ViewState("vwsKeyWord"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsKeyWord") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""

        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, "Master", "MAXILOAN") Then

                txtgoPage.Text = "1"
                InitialDefaultPanel()
                FillKeyWord()
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                If Not Request("Table") Is Nothing Then
                    Me.TableName = Request("Table")
                Else
                    Me.TableName = ""
                End If
                Me.Sort = "Description ASC"
                Me.KeyWord = cboKeyWord.SelectedItem.Text
                BindGridEntity(Me.CmdWhere, Me.TableName)
                If Request("Back") = "1" Then
                    cboKeyWord.SelectedIndex = cboKeyWord.Items.IndexOf(cboKeyWord.Items.FindByValue(Me.TableName))
                    pnlTop.Visible = True
                    pnlList.Visible = True
                    pnlAddEdit.Visible = False
                End If
            End If
        End If
    End Sub

    Sub FillKeyWord()
        Dim dtEntity As DataTable = Nothing
        Dim oMaster As New Parameter.Master
        oMaster.strConnection = GetConnectionString()
        oMaster = m_controller.GetKeyWord(oMaster)
        If Not oMaster Is Nothing Then
            dtEntity = oMaster.ListData
        End If
        cboKeyWord.DataSource = dtEntity.DefaultView
        cboKeyWord.DataTextField = "Description"
        cboKeyWord.DataValueField = "TableName"
        cboKeyWord.DataBind()
    End Sub

    Private Sub InitialDefaultPanel()
        pnlTop.Visible = True
        pnlList.Visible = False
        pnlAddEdit.Visible = False
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String, ByVal Table As String)
        Dim dtEntity As DataTable
        Dim oMaster As New Parameter.Master

        oMaster.WhereCond = cmdWhere
        oMaster.CurrentPage = currentPage
        oMaster.PageSize = pageSize
        oMaster.SortBy = Me.Sort
        oMaster.Table = Table
        oMaster.strConnection = GetConnectionString()
        oMaster = m_controller.GetMaster(oMaster)

        If Not oMaster Is Nothing Then
            dtEntity = oMaster.ListData
            recordCount = oMaster.TotalRecords
            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            btnPrint.Enabled = False
        Else
            btnPrint.Enabled = True
        End If

        PagingFooter()
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            If pnlList.Visible = True Then
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            End If
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere, Me.TableName)
    End Sub
    Protected Sub BtnGoPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere, Me.TableName)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "Master", "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            pnlTop.Visible = False

            BtnCancel.Visible = True
            BtnSave.Visible = True

            lblTitleAddEdit.Text = Me.AddEdit
            lblID.Visible = True
            txtID.Visible = False
            txtDescription.Visible = True
            lblKeyWord.Text = Me.KeyWord
            lblID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            txtID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            txtDescription.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(3).Text

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "Master", "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.Master
            Dim err As String
            With customClass
                .Id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .Table = Me.TableName
                .strConnection = GetConnectionString()
            End With
            err = m_controller.MasterDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere, Me.TableName)
            pnlTop.Visible = True
            pnlList.Visible = True
            pnlAddEdit.Visible = False
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.Master
        Dim ErrMessage As String = ""
        With customClass
            .Id = txtID.Text
            .Description = txtDescription.Text
            .Table = Me.TableName
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.MasterAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            m_controller.MasterUpdate(customClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        End If
        BindGridEntity(Me.CmdWhere, Me.TableName)
        pnlTop.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        If CheckFeature(Me.Loginid, "Master", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        pnlTop.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit

        lblID.Visible = False
        txtID.Visible = True
        txtDescription.Visible = True

        lblKeyWord.Text = Me.KeyWord
        txtID.Text = ""
        txtDescription.Text = ""
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If CheckFeature(Me.Loginid, "Master", "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/MasterRpt.aspx")
    End Sub

    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("Master")
        If Not cookie Is Nothing Then
            cookie.Values("Table") = Me.TableName
            cookie.Values("Key") = Me.KeyWord
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Master")
            cookieNew.Values.Add("Table", Me.TableName)
            cookieNew.Values.Add("Key", Me.KeyWord)
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere, Me.TableName)
        pnlTop.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboKeyWord.SelectedIndex = 0
        InitialDefaultPanel()
    End Sub

    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Me.Sort = "ID ASC"
        Me.KeyWord = cboKeyWord.SelectedItem.Text
        Me.TableName = cboKeyWord.SelectedValue
        BindGridEntity(Me.CmdWhere, Me.TableName)
        pnlTop.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("Master.aspx")
    End Sub

    Protected Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtgPaging.SelectedIndexChanged

    End Sub




End Class