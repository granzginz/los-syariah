﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region
Public Class MasterTC
    Inherits Maxiloan.Webform.WebBased
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim m_MasterTC As New MasterTCController
#Region " Property "
    Private Property TCID() As String
        Get
            Return CStr(viewstate("TCID"))
        End Get
        Set(ByVal Value As String)
            viewstate("TCID") = Value
        End Set
    End Property

    Private Property TCName() As String
        Get
            Return CStr(viewstate("TCName"))
        End Get
        Set(ByVal Value As String)
            viewstate("TCName") = Value
        End Set
    End Property

    Private Property isForPersonal() As String
        Get
            Return CStr(viewstate("isForPersonal"))
        End Get
        Set(ByVal Value As String)
            viewstate("isForPersonal") = Value
        End Set
    End Property

    Private Property isForCompany() As String
        Get
            Return CStr(viewstate("isForCompany"))
        End Get
        Set(ByVal Value As String)
            viewstate("isForCompany") = Value
        End Set
    End Property



    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "MasterTC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'createxmdlist()
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtgoPage.Text = "" Then
            txtgoPage.Text = "0"
        Else
            If IsNumeric(txtgoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtgoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oMasterTC As New Parameter.MasterTC

        With oMasterTC
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oMasterTC = m_MasterTC.ListMasterTC(oMasterTC)

        With oMasterTC
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oMasterTC.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            BtnPrint.Enabled = False

            ShowMessage(lblMessage, "Data tidak ditemukan.....", True)
        Else
            BtnPrint.Enabled = True
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub ClearAddForm()

        txtID.Text = ""
        txtName.Text = ""
        lblID.Visible = False
        txtID.Visible = True

    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub BtnSearchNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchNew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + "='" + StrSearchByValue + "'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub viewbyID(ByVal MasterTCid As String)

        'Isi semua field Edit Action
        txtID.Visible = False
        lblID.Visible = True
        lblID.Text = Me.TCID
        txtName.Text = Me.TCName

        rbCompany.SelectedIndex = rbPersonal.Items.IndexOf(rbPersonal.Items.FindByValue(Me.isForCompany))
        rbPersonal.SelectedIndex = rbPersonal.Items.IndexOf(rbPersonal.Items.FindByValue(Me.isForPersonal))


    End Sub

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.MasterTC

        With customClass
            .strConnection = GetConnectionString
            .TCID = txtID.Text.Trim
            .TCName = txtName.Text.Trim
            .IsForCompany = CInt(rbCompany.SelectedItem.Value)
            .IsForPersonal = CInt(rbPersonal.SelectedItem.Value)
        End With


        Try
            m_MasterTC.AddMasterTC(customClass)
          ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
        End Try
    End Sub
#End Region

#Region "EDIT"
    Private Sub edit(ByVal MasterTCid As String)

        Dim customClass As New Parameter.MasterTC

        With customClass
            .strConnection = GetConnectionString
            .TCID = lblID.Text.Trim
            .TCName = txtName.Text.Trim
            .IsForCompany = CInt(rbCompany.SelectedItem.Value)
            .IsForPersonal = CInt(rbPersonal.SelectedItem.Value)
        End With

        Try
            m_MasterTC.EditMasterTC(customClass)
            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
            BindGridEntity(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand

        Me.TCID = e.Item.Cells(2).Text
        Me.TCName = e.Item.Cells(3).Text
        If CStr(e.Item.Cells(4).Text.Trim) = "Yes" Then Me.isForPersonal = "1" Else Me.isForPersonal = "0"
        If CStr(e.Item.Cells(5).Text.Trim) = "Yes" Then Me.isForCompany = "1" Else Me.isForCompany = "0"

        Select Case e.CommandName
            Case "CheckList"
                Response.Redirect("MasterTCCheckList.aspx?TCID=" & Me.TCID & "&TCName=" & Me.TCName)

            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.TCID = e.Item.Cells(2).Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID(Me.TCID)
                End If

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.TCID = e.Item.Cells(2).Text
                    Dim customClass As New Parameter.MasterTC
                    With customClass
                        .strConnection = getConnectionString
                        .TCID = Me.TCID
                    End With

                    Try
                        m_MasterTC.DeleteMasterTC(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub

    Private Sub BtnResetNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetNew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then
            Me.TCID = e.Item.Cells(2).Text

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                If txtID.Text = "" Then

                    ShowMessage(lblMessage, "Harap isi ID", True)
                    Exit Sub
                End If
                Add()
            Case "EDIT"
                edit(Me.TCID)
        End Select
    End Sub

    Private Function OpenViewMasterTC(ByVal pMasterTCID As String) As String
        Return "javascript:OpenWindowMasterTC('" & pMasterTCID & "')"
    End Function

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If

            Response.Redirect("report/MasterTCreport.aspx")
        End If
    End Sub

    Private Sub InitializeComponent()

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("MasterTC.aspx")
    End Sub

End Class