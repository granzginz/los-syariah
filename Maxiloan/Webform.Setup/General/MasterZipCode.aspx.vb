﻿#Region " Imports "
Imports Maxiloan.cbse
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Custom.Parameter.ARMgt
Imports Custom.Controller.ARMgt
#End Region


Public Class MasterZipCode
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucZipCode As ucNumber
    Protected WithEvents imbDelete As System.Web.UI.WebControls.ImageButton

#Region " Private Const "

    Private m_controller As New MasterZipCodeController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " PageLoad "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "ZipCode"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtgoPage.Text = "1"
                Me.Sort_By = "Kelurahan ASC "
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If

                ucZipCode.MaxLength = 5
                ucZipCode.MinimumRange = 0
                ucZipCode.MaximumRange = 99999
                ucZipCode.RegularValidation = "\d{5}"
                ucZipCode.RegularValidationMessage = "Harap isi dengan angka, minimum 5 angka"
                ucZipCode.Text = 0
                ucZipCode.BindText()
                BindComboParent()
                Response.Write(GenerateScript(GetComboChild))
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oMasterZipCode As New Parameter.MasterZipCode
        InitialDefaultPanel()
        oMasterZipCode.WhereCond = cmdWhere
        oMasterZipCode.CurrentPage = currentPage
        oMasterZipCode.SortBy = Me.Sort_By
        oMasterZipCode.strConnection = GetConnectionString
        oMasterZipCode.PageSize = pageSize
        oMasterZipCode = m_controller.GetMasterZipCode(oMasterZipCode)

        Try
            dtsEntity = oMasterZipCode.ListData
        Catch ex As Exception
        End Try

        dtvEntity = dtsEntity.DefaultView
        lbltotrec.Text = CStr(oMasterZipCode.TotalRecords)
        recordCount = oMasterZipCode.TotalRecords

        'cek totalrecord untuk meng-enable/ disable tombol print
        If recordCount = 0 Then
            BtnPrint.Enabled = False
        Else
            BtnPrint.Enabled = True
        End If

        dtgEntity.DataSource = dtvEntity
        Try
            dtgEntity.DataBind()
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        Catch e As Exception
        End Try

        PagingFooter()
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(viewstate("vwsCmdWhere").ToString)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(viewstate("vwsCmdWhere").ToString)
            End If
        End If
    End Sub

#End Region

#Region " dtgEntity_ItemCommand "
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        If e.CommandName = "DELETE" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                pnlAddEdit.Visible = False
                Dim oMasterZipCode As New Parameter.MasterZipCode
                Dim strKelurahan As String = e.Item.Cells(1).Text.Trim
                Dim strKecamatan As String = e.Item.Cells(2).Text.Trim
                Dim strCity As String = e.Item.Cells(3).Text.Trim
                Dim strZipCode As String = e.Item.Cells(4).Text.Trim

                Dim customClass As New Parameter.MasterZipCode
                With customClass
                    .strConnection = GetConnectionString
                    .Kelurahan = strKelurahan
                    .Kecamatan = strKecamatan
                    .City = strCity
                    .ZipCode = strZipCode
                    .CGID = "" 'cboCG.SelectedItem.Value.Trim
                    .CollectorID = tempChild.Value.Trim
                End With


                Dim ResultOutput As String
                ResultOutput = m_controller.MasterZipCodeDelete(customClass)
                If ResultOutput = "OK" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region " imbSave_Click "
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.MasterZipCode
            Dim strKelurahan As String = txtKelurahan.Text.Trim
            Dim strKecamatan As String = txtKecamatan.Text.Trim
            Dim strCity As String = txtCity.Text.Trim

            'If cboCG.SelectedItem.Value.Trim = "0" Then
            '    lblMessage.Text = "Please Select Collector Group"
            '    lblMessage.Visible = True
            '    BindComboParent()
            '    Response.Write(GenerateScript(GetComboChild))
            '    Exit Sub
            'Else
            '    If tempChild.Value.Trim = "" Or tempChild.Value.Trim = "0" Then
            '        lblMessage.Text = "Please Select Collector ID"
            '        lblMessage.Visible = True
            '        BindComboParent()
            '        Response.Write(GenerateScript(GetComboChild))
            '        Exit Sub
            '    End If
            'End If

            With customClass
                .Kelurahan = strKelurahan
                .Kecamatan = strKecamatan
                .City = strCity
                .ZipCode = CStr(ucZipCode.Text)
                .strConnection = GetConnectionString()
                .CGID = "" ' cboCG.SelectedItem.Value.Trim
                .CollectorID = tempChild.Value.Trim
                .SandiWilayah = txtSandiWilayah.Text
                .Provinsi = cboProvinsi.SelectedValue
            End With

            Dim ResultOutput As String
            ResultOutput = m_controller.MasterZipCodeAdd(customClass)
            If ResultOutput = "OK" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                lblMessage1.Text = ""
                ViewState("vwsCmdWhere") = "ALL"
                BindGridEntity(ViewState("vwsCmdWhere").ToString)
                ucZipCode.Text = 0
            ElseIf ResultOutput = "KO" Then
                lblMessage1.Text = MessageHelper.MESSAGE_INSERT_FAILED
                lblMessage.Text = ""
                pnlAddEdit.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If
            txtSearch.Text = ""
        End If
    End Sub
#End Region

#Region " imbAdd_Click "
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            'pnlSearch.Visible = False
            lblMessage.Text = ""
            txtKecamatan.Text = ""
            txtKelurahan.Text = ""
            txtCity.Text = ""
            txtgoPage.Text = ""
            ucZipCode.Text = 0
            txtSandiWilayah.Text = ""

            lblTitleAddEdit.Text = "ADD"
            ucZipCode.BindText()
            BindComboParent()
            BindComboProvinsi()
            Response.Write(GenerateScript(GetComboChild))
        End If
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlAddEdit.Visible = False
        pnlList.Visible = True
        'pnlAdvSearch.Visible = True
        lblMessage.Text = ""
        lblMessage1.Text = ""

    End Sub
#End Region

#Region " Sort_By "
    Private Property Sort_By() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region

#Region " Sorting "
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgEntity.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort_By = e.SortExpression
        Else
            Me.Sort_By = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region " imbBack "
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        InitialDefaultPanel()
    End Sub
#End Region

#Region " CmdWhere "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value.ToString
        End Set
    End Property
#End Region

#Region " imbReset "
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtgoPage.Text = "1"
        BindGridEntity("ALL")
        Me.CmdWhere = "ALL"
    End Sub
#End Region

#Region " imbSearch "
    Private Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Dim strSearchBy As String = txtSearch.Text.Trim
        If strSearchBy <> "" Then
            strSearchBy = Replace(strSearchBy, "'", "''")
        End If

        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + "='" + strSearchBy + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        txtgoPage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region " imbPrint "
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            SendCookies()
            Response.Redirect("../General/Report/rpt_viewer_ZipCode.aspx")
        End If
    End Sub
#End Region

#Region " SendCookies "
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("ZipCode")
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = cboSearch.SelectedItem.Value
            cookie.Values("SearchText") = txtSearch.Text
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("ZipCode")
            cookieNew.Values.Add("SearchBy", cboSearch.SelectedItem.Value)
            cookieNew.Values.Add("SearchText", txtSearch.Text)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region " dtgEntity_ItemDataBound "
    Private Sub dtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        txtSearch.Text = ""
        Response.Redirect("MasterZipCode.aspx")
    End Sub
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable) As String
        Dim strScript As String
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf
        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("CollectorID")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("Name")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("CGID")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region
#Region "GetComboChild"
    Private Function GetComboChild() As DataTable
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity
        Dim oController As New CLActivityController
        With Coll
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = "CG_ALL"
        End With
        CollList = oController.CLActivityListCollector(Coll)
        dtChild = CollList.ListCLActivity
        Try
            dtChild = CollList.ListCLActivity
            Return dtChild

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Function
#End Region
#Region "BindComboParent"
    Private Sub BindComboParent()
        'Dim dtParent As New DataTable
        'Dim Coll As New Parameter.RptCollAct
        'Dim ControllerCG As New RptCollActController

        'With Coll
        '    .strConnection = getconnectionstring()
        '    .CGID = Me.GroubDbID
        '    .strKey = "CG_ALL"
        '    .CollectorType = ""
        'End With

        'Coll = ControllerCG.ViewDataCollector(Coll)
        'dtParent = Coll.ListCollector

        'cboCG.DataTextField = "CGName"
        'cboCG.DataValueField = "CGID"
        'cboCG.DataSource = dtParent
        'cboCG.DataBind()
        'cboCG.Items.Insert(0, "Select One")
        'cboCG.Items(0).Value = "0"
        'If dtParent.Rows.Count = 1 Then
        '    cboCG.Items.FindByValue(Me.GroubDbID).Selected = True
        'End If



    End Sub
    Private Sub BindComboProvinsi()
        Dim dtParent As New DataTable
        Dim p_Class As New Parameter.Propinsi
        Dim c_Class As New Controller.PropinsiController

        With p_Class
            .strConnection = GetConnectionString()
        End With
        dtParent = c_Class.GetPropinsiCombo(p_Class)
        'GetPropinsiCombo()
        cboProvinsi.DataTextField = "description"
        cboProvinsi.DataValueField = "description"
        cboProvinsi.DataSource = dtParent.DefaultView
        cboProvinsi.DataBind()
        cboProvinsi.Items.Insert(0, "Select One")
        cboProvinsi.Items(0).Value = "0"

    End Sub
#End Region
    'Public Function drdCompanyChange() As String
    '    Return "ChangeMultiCombo(this,'" & cboCollector.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"
    'End Function

End Class