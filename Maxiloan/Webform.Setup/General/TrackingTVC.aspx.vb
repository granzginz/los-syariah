﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TrackingTVC
    Inherits Maxiloan.Webform.WebBased


    Private m_controller As New TrackingTVCController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "TRACKINGTVC"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtPage.Text = "1"
                Me.Sort = "ApplicationID ASC"
                Me.CmdWhere = "All"
                BindCombo()
                If Request("cmd") = "dtl" Then
                    If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If
                    ApplicationID = Request("id")
                    BindEntity()
                End If
            End If

            BindGridEntity(Me.CmdWhere)
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomerCases As New Parameter.Application
        If Me.AddEdit <> "EDIT" Then
            InitialDefaultPanel()
        End If

        oCustomerCases.strConnection = GetConnectionString()
        oCustomerCases.WhereCond = cmdWhere
        oCustomerCases.CurrentPage = currentPage
        oCustomerCases.PageSize = pageSize
        oCustomerCases.SortBy = Me.Sort
        oCustomerCases = m_controller.GetApplication(oCustomerCases)

        If Not oCustomerCases Is Nothing Then
            dtEntity = oCustomerCases.ListData
            recordCount = oCustomerCases.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub BindCombo()
        Dim oCustomerCases As New Parameter.Application
        Dim tbl As New DataTable
        oCustomerCases.strConnection = GetConnectionString()
        tbl = m_controller.GetCombo(oCustomerCases)

        cboSuppplier.DataTextField = "SupplierDescription"
        cboSuppplier.DataValueField = "SupplierID"
        cboSuppplier.DataSource = tbl
        cboSuppplier.DataBind()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString("dd/MM/yyyy").Trim()
            e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToString("dd/MM/yyyy").Trim()
            e.Item.Cells(5).Text = CDate(e.Item.Cells(5).Text).ToString("dd/MM/yyyy").Trim()
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblMessage.Visible = True
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub BindEntity()
        Dim oCustomer As New Parameter.Application

        Me.AddEdit = "EDIT"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonCancel.CausesValidation = False

        lblTitleAddEdit.Text = Me.AddEdit

        oCustomer.ApplicationID = ApplicationID.Trim
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetApplicatioList(oCustomer)
        If oCustomer.TglKonfirmasiDealer.ToString("dd/MM/yyyy").Trim <> "01/01/1900" Then
            TglKonfirmasiDealerDate.Text = oCustomer.TglKonfirmasiDealer.ToString("dd/MM/yyyy").Trim()
            TglJanjiDate.Text = oCustomer.TglJanjiTtdKontrak.ToString("dd/MM/yyyy").Trim()
            txtKeterangan.InnerText = oCustomer.StatusTVCDesc.Trim
        Else
            TglKonfirmasiDealerDate.Text = Nothing
            TglJanjiDate.Text = Nothing
            txtKeterangan.InnerText = Nothing
        End If

    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.Application
        Dim ErrMessage As String = ""

        With customClass
            .TglJanjiTtdKontrak = ConvertDate2(TglJanjiDate.Text.Trim)
            .TglKonfirmasiDealer = ConvertDate2(TglKonfirmasiDealerDate.Text.Trim)
            .StatusTVCDesc = txtKeterangan.InnerText
            .strConnection = GetConnectionString()
        End With
        customClass.ApplicationID = ApplicationID
        m_controller.ApplicationSaveEdit(customClass)
        lblMessage.Text = MessageHelper.MESSAGE_UPDATE_SUCCESS
        lblMessage.Visible = True
        BindGridEntity(Me.CmdWhere)
        Me.AddEdit = Nothing
    End Sub
        
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("TrackingTVC")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("TrackingTVC")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        Me.CmdWhere = "SupplierID = '" + cboSuppplier.SelectedItem.Value + "'"

        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere += " and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("TrackingTVC.aspx")
        Me.AddEdit = Nothing
    End Sub
End Class