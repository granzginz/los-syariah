﻿
#Region "Imports"
Imports Maxiloan.Controller
#End Region

Public Class Dati
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New DatiIIController
    Private m_propinsi As New PropinsiController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then            
            Me.FormID = "DATI"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtPage.Text = "1"
                Me.Sort = "DatiID ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"

                End If

                BindGridEntity(Me.CmdWhere)

                If Request("cmd") = "dtl" Then
                    If CheckFeature(Me.Loginid, Me.FormID, "View", "MAXILOAN") Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If
                    BindDetail(Request("id"), Request("desc"))
                End If
                ButtonClose.Attributes.Add("OnClick", "return fClose()")
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oDati As New Parameter.Dati
        InitialDefaultPanel()
        oDati.strConnection = GetConnectionString()
        oDati.WhereCond = cmdWhere
        oDati.CurrentPage = currentPage
        oDati.PageSize = pageSize
        oDati.SortBy = Me.Sort
        oDati = m_controller.GetDati(oDati)

        If Not oDati Is Nothing Then
            dtEntity = oDati.Listdata
            recordCount = oDati.Totalrecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub BindCboPropinsiEntity()
        Dim oPropinsi As New Parameter.Propinsi
        Dim tblPropinsi As New DataTable
        oPropinsi.strConnection = GetConnectionString()
        tblPropinsi = m_propinsi.GetPropinsiCombo(oPropinsi)

        cboPropinsi.DataTextField = "Description"
        cboPropinsi.DataValueField = "Description"
        cboPropinsi.DataSource = tblPropinsi
        cboPropinsi.DataBind()
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal Desc As String)
        Dim oDati As New Parameter.Dati
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd2") = "close" Then
            ButtonBack.Visible = False
            ButtonClose.Visible = True
        Else
            ButtonBack.Visible = True
            ButtonClose.Visible = False
        End If
        ButtonCancel.Visible = False
        ButtonSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oDati.DatiID = ID
        oDati.strConnection = GetConnectionString()
        oDati = m_controller.GetDatiList(oDati)

        lblID.Visible = True
        txtID.Visible = False


        lblID.Text = ID
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Protected Sub BtnGoPage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oDati As New Parameter.Dati
        BindCboPropinsiEntity()
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oDati.DatiID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oDati.strConnection = GetConnectionString()
            oDati = m_controller.GetDatiList(oDati)
            lblID.Visible = True
            txtID.Visible = False

            lblID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            txtID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            cboType.SelectedItem.Text = oDati.Type
            txtNamaKabKot.Text = oDati.NamaKabKot
            cboPropinsi.SelectedItem.Text = oDati.Propinsi

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.Dati
            With customClass
                .DatiID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.DatiDelete(customClass)
            If err <> "" Then
               showmessage(lblmessage, err, true)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.Dati
        Dim ErrMessage As String = ""
        With customClass
            .DatiID = txtID.Text
            .Type = cboType.SelectedItem.Text
            .NamaKabKot = txtNamaKabKot.Text
            .Propinsi = cboPropinsi.SelectedItem.Text
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.DatiSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            m_controller.DatiSaveEdit(customClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub ButtonAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        ButtonBack.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonClose.Visible = False

        BindCboPropinsiEntity()
        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        lblID.Visible = False
        txtID.Visible = True

        txtID.Text = ""

    End Sub
    Private Sub ButtonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/Dati.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("Dati")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Dati")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub ButtonReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("Dati.aspx")
    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("Dati.aspx")
    End Sub




End Class