﻿
#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region
Public Class MasterTCCheckList
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
   
    Private recordCount As Int64 = 1
    Dim m_MasterTC As New MasterTCController

#End Region

#Region " Property "
    Private Property TCID() As String
        Get
            Return CStr(viewstate("TCID"))
        End Get
        Set(ByVal Value As String)
            viewstate("TCID") = Value
        End Set
    End Property

    Private Property TCName() As String
        Get
            Return CStr(viewstate("TCName"))
        End Get
        Set(ByVal Value As String)
            viewstate("TCName") = Value
        End Set
    End Property

    Private Property SequenceNo() As Int16
        Get
            Return CShort(viewstate("SequenceNo"))
        End Get
        Set(ByVal Value As Int16)
            viewstate("SequenceNo") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return CStr(viewstate("Description"))
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "MasterTC"
            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                If Request.QueryString("TCID") <> "" Then Me.TCID = Request.QueryString("TCID")
                If Request.QueryString("TCName") <> "" Then Me.TCName = Request.QueryString("TCName")
                Me.SearchBy = ""
                Me.SortBy = ""


                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                lblTCDocument.Text = Me.TCName.Trim
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
    Protected Sub BtnGoPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region


#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oMasterTC As New Parameter.MasterTC

        With oMasterTC
            .strConnection = GetConnectionString
            .TCID = Me.TCID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oMasterTC = m_MasterTC.ListTCCheckList(oMasterTC)

        With oMasterTC
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oMasterTC.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            BtnPrint.Enabled = False
           ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            BtnPrint.Enabled = True
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub ClearAddForm()

        lblID.Visible = True
        lblID.Text = Me.TCID
        txtDescription.Text = ""

    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub BtnSearchNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchNew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + "='" + StrSearchByValue + "'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub viewbyID(ByVal MasterTCid As String)
        'Isi semua field Edit Action
        lblID.Visible = True
        lblID.Text = Me.TCID
        txtDescription.Text = Me.Description.Trim
    End Sub

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.MasterTC

        With customClass
            .strConnection = GetConnectionString
            .TCID = lblID.Text.Trim
            .Description = txtDescription.Text.Trim
        End With


        Try
            m_MasterTC.AddTCCheckList(customClass)
           ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
        End Try
    End Sub
#End Region

#Region "EDIT"
    Private Sub edit(ByVal MasterTCid As String)

        Dim customClass As New Parameter.MasterTC

        With customClass
            .strConnection = GetConnectionString
            .TCID = Me.TCID
            .SequenceNo = Me.SequenceNo
            .Description = txtDescription.Text.Trim
        End With

        Try
            m_MasterTC.EditTCCheckList(customClass)
            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
           showmessage(lblmessage, ex.Message , true)
        End Try
    End Sub

#End Region

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand

        Select Case e.CommandName

            Case "Edit"

                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.TCID = e.Item.Cells(2).Text
                    Me.Description = e.Item.Cells(3).Text
                    Me.SequenceNo = CShort(e.Item.Cells(4).Text)


                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID(Me.TCID)
                End If

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.TCID = e.Item.Cells(2).Text
                    Me.Description = e.Item.Cells(3).Text
                    Me.SequenceNo = CShort(e.Item.Cells(4).Text)

                    Dim customClass As New Parameter.MasterTC
                    With customClass
                        .strConnection = getConnectionString
                        .TCID = Me.TCID
                        .Description = Me.Description
                        .SequenceNo = Me.SequenceNo
                    End With

                    Try
                        m_MasterTC.DeleteTCCheckList(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)

                    Catch ex As Exception
                       showmessage(lblmessage, ex.Message , true)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub

    Private Sub BtnResetNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetNew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then
            Me.TCID = e.Item.Cells(2).Text

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                Add()
            Case "EDIT"
                edit(Me.TCID)
        End Select
    End Sub

    Private Function OpenViewMasterTC(ByVal pMasterTCID As String) As String
        Return "javascript:OpenWindowMasterTC('" & pMasterTCID & "')"
    End Function

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(COOKIES_MASTERTCCHECKLIST)
            If Not cookie Is Nothing Then
                cookie.Values("TCId") = Me.TCID
                cookie.Values("TCName") = Me.TCName
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(COOKIES_MASTERTCCHECKLIST)
                cookieNew.Values.Add("TCId", Me.TCID)
                cookieNew.Values.Add("TCName", Me.TCName)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("report/MasterTCCheckListReport.aspx")
        End If
    End Sub

    Private Sub InitializeComponent()

    End Sub

    Private Sub btnBackNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBackNew.Click
        Response.Redirect("MasterTC.aspx")
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub




End Class