﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExampleName.aspx.vb" Inherits="Maxiloan.Webform.Setup.ExampleName" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Example Name</title> 
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDataGrid" runat="server">
        <div class="form_tittle">
            <div class="tittle_strip"></div>
            <div class="form_single">
                <h3>Example Name</h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapped_ns">
                    <asp:DataGrid ID="dtgEntity" runat="server" AllowSorting="true" AutoGenerateColumns="false" DataKeyField="IdName" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" CausesValidation="false" CommandName="edit" runat="server" ImageUrl="../../images/iconedit.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" CausesValidation="False" CommandName="delete" runat="server"
                                            ImageUrl="../../images/icondelete.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IdName" HeaderText="ID">
                                    <ItemStyle Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkIdName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IdName") %>'
                                            CommandName="id">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA">
                                    <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Address" HeaderText="Alamat">
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Phone" HeaderText="Telepon">
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Phone") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContactPerson_Hp" HeaderText="No HP">
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCP" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContactPerson_Hp") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                        </Columns> 
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">  
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server" Font-Size="8pt" ControlToValidate="txtpage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        Font-Names="Verdana" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                        ErrorMessage="*" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                 <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
            </div>
        </div>
         <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="small button green">
                </asp:Button>
            </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        CARI AREA
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari berdasarkan</label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="IdName">ID</asp:ListItem>
                        <asp:ListItem Value="Name">Nama</asp:ListItem>
                        <asp:ListItem Value="Address">Alamat</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
