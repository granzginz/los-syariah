﻿#Region "Import"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ExampleName
    Inherits Maxiloan.Webform.WebBased
    'Protected WithEvents UCRegional As UCRegional

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.ExampleName
    Private oController As New ExampleNameController
#End Region

#Region "Properti"
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property SelectItem() As String
        Get
            Return (CType(ViewState("SelectItem"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectItem") = Value
        End Set
    End Property
    Private Property SelectValue() As String
        Get
            Return (CType(ViewState("SelectValue"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectValue") = Value
        End Set
    End Property
    Private Property StrIdName() As String
        Get
            Return (CType(ViewState("StrIdName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("StrIdName") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "ExampleName"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                lblMessage.Text = ""
                'UCRegional.cboRegional()
                DoBind(Me.SortBy, Me.SortBy)

            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")
        If UBound(strBranch) > 0 Then

            ShowMessage(lblMessage, "Nama Tidak bisa diganti.....", True)
            btnSearch.Enabled = False
            pnlSearch.Visible = True
            pnlDataGrid.Visible = False
            'pnlAdd.Visible = False
        Else
            'pnlAdd.Visible = False
            pnlSearch.Visible = True
            pnlDataGrid.Visible = False
            'pnlView.Visible = False
            lblMessage.Text = ""
        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ExampleNameList(oCustomClass)

        DtUserList = oCustomClass.ListExampleName
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        ChgStatusInDgr()
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        'pnlAdd.Visible = False
        'pnlView.Visible = False
        If dtgEntity.Items.Count > 0 Then
            btnPrint.Enabled = True
        ElseIf dtgEntity.Items.Count = 0 Then
            With lblMessage
                .Visible = True
                .Text = "Data tidak ditemukan ....."
            End With
            btnPrint.Enabled = False
        End If
    End Sub
    Public Sub EnabledPnlAdd(ByVal Read As Boolean, ByVal Style As BorderStyle)
        'txtAreaID.ReadOnly = Read
        'txtAreaID.BorderStyle = Style
        'txtAreaName.ReadOnly = Read
        'txtAreaName.BorderStyle = Style
        'txtInitName.ReadOnly = Read
        'txtInitName.BorderStyle = Style
        'txtManager.ReadOnly = Read
        'txtManager.BorderStyle = Style
        'oContactPerson.BorderContactNone(Read, Style)
        'oCompanyAddress.BorderNone(Read, Style)
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan ...", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Text = ""
        Me.PageState = currentPage
        txtPage.Text = CType(Me.PageState, String)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "DataGridCommand"
    Private Sub ChgStatusInDgr()
    End Sub
    Public Sub dtgEntity_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Dim lnkIdName As LinkButton
        Dim lblName, lblAddress, lblPhone, lblCP As Label

        Me.StrIdName = CType(e.Item.FindControl("lblIdName"), Label).Text.Trim
        Select Case e.CommandName
            Case "edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    Me.Mode = "edit"
                    Reset()
                    EnabledPnlAdd(False, BorderStyle.NotSet)

                    lnkIdName = CType(e.Item.FindControl("lnkIdName"), LinkButton)
                    lblName = CType(e.Item.FindControl("lblName"), Label)
                    lblAddress = CType(e.Item.FindControl("lblAddress"), Label)
                    lblPhone = CType(e.Item.FindControl("lblPhone"), Label)
                    lblCP = CType(e.Item.FindControl("lblCP"), Label)

                    oCustomClass.IDName = lnkIdName.Text
                    oCustomClass.strConnection = GetConnectionString()
                    oCustomClass = oController.ExampleNameInfo(oCustomClass)

                    'Dim dsExampleName As DataView
                    'dsExampleName = oCustomClass.ListExampleName.DefaultView
                    'If dsExampleName.Count = 1 Then
                    '    With Text
                    'End If

                    pnlSearch.Visible = False
                    pnlDataGrid.Visible = False
                    'pnlAdd.Visible = True
                    'pnlView.Visible = False
                    'btnOk.Visible = False 
                End If
            Case "delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    lnkIdName = CType(e.Item.FindControl("lnkIdName"), LinkButton)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .IDName = lnkIdName.Text
                    End With

                    Dim err As String = oController.DeleteExampleName(oCustomClass)

                    If err <> "" Then
                        ShowMessage(lblMessage, err, True)
                    Else
                        DoBind(Me.SearchBy, Me.SortBy)
                        ShowMessage(lblMessage, "Data Berhasil diHapus", False)
                    End If
                    'AddScriptToDtgEnt()
                End If
            Case "id"
                lnkIdName = CType(e.Item.FindControl("lnkIdName"), LinkButton)
                lblName = CType(e.Item.FindControl("lblName"), Label)

                oCustomClass.IDName = lnkIdName.Text
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass = oController.ExampleNameInfo(oCustomClass)

                pnlSearch.Visible = False
                pnlDataGrid.Visible = False
                'pnlAdd.Visible = False
                'pnlView.Visible = True
                'btnOk.Visible = True
        End Select
    End Sub
    Public Sub dtgEntity_SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgEntity.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
        'AddScriptToDtgEnt()
    End Sub
#End Region
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.SelectItem = cboSearch.SelectedValue
            Me.SelectValue = txtSearch.Text
            Me.SearchBy = Me.SelectItem.Trim & "='" & Me.SelectValue & "'"
        End If
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        'pnlAdd.Visible = False
        'pnlView.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        lblMessage.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        txtSearch.Text = ""
        'pnlAdd.Visible = True
        pnlDataGrid.Visible = False
        'pnlAdd.Visible = False
        'pnlView.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
        txtPage.Text = "1"
        lblPage.Text = txtPage.Text
        currentPage = CInt(lblPage.Text)
        cboSearch.SelectedIndex = 0
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            Me.Mode = "add"
            Reset()
            EnabledPnlAdd(False, BorderStyle.NotSet)

            pnlSearch.Visible = False
            pnlDataGrid.Visible = False
            'pnlAdd.Visible = True
            'pnlView.Visible = False

        End If
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("../Organization/Report/AreaReport.aspx")
        End If
    End Sub
End Class