﻿
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class BranchLimit
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtApprovalLimit As ucNumberFormat

#Region "Constanta"


    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
#End Region

#Region "Property"

    Private Property IDBranchStatus() As String
        Get
            Return CType(ViewState("IDBranchStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IDBranchStatus") = Value
        End Set
    End Property
    Private Property Description() As String
        Get
            Return CType(ViewState("Description"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Description") = Value
        End Set
    End Property
    Private Property ApprovalLimit() As Double
        Get
            Return CType(ViewState("ApprovalLimit"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("ApprovalLimit") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "BranchLimit"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.Sort = "IDBranchStatus ASC"
            Me.BranchID = Replace(Me.sesBranchId, "'", "")
            Me.CmdWhere = "ALL"
            BindGridEntity(Me.CmdWhere)
            InitialPanel()
            readonlyTextbox()
        End If
    End Sub
    Sub readonlyTextbox()
        lblID.Enabled = False
        lblDescription.Enabled = False
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        PnlAddEdit.Visible = False
    End Sub
    Sub PanelAllFalse()
        pnlList.Visible = False
        PnlAddEdit.Visible = True

    End Sub

#Region "DisplayMessage"
    Private Sub DisplayMessage(ByVal strMsg As String)
        ShowMessage(lblMessage, strMsg, True)
    End Sub
#End Region
#Region "BindEdit"
    Sub BindEdit(ByVal IDBranchStatus As String, ByVal Description As String, ByVal ApprovalLimit As Decimal)
        lblID.Text = IDBranchStatus
        lblDescription.Text = Description
        txtApprovalLimit.Text = FormatNumber(ApprovalLimit, 0)
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oPaging As New Parameter.GeneralPaging

        Try
            With oPaging
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhere
                .CurrentPage = 1
                .PageSize = 1000
                .SortBy = Me.Sort
                .SpName = "spBranchStatus"
            End With
            oPaging = oController.GetGeneralPaging(oPaging)
            If Not oPaging Is Nothing Then
                dtEntity = oPaging.ListData
                recordCount = oPaging.TotalRecords
            Else
                recordCount = 0
            End If
            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
            PagingFooter()
            pnlList.Visible = True
        Catch ex As Exception
            DisplayMessage(ex.ToString)
        End Try
    End Sub

#End Region
#Region "Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim IDBranchStatus, Description As String
        Dim ApprovalLimit As Double

        Me.IDBranchStatus = CType(e.Item.FindControl("lblIDBranchStatus"), Label).Text.Trim
        Me.Description = CType(e.Item.FindControl("lblDescription"), Label).Text.Trim

        If CType(e.Item.FindControl("lblApprovalLimit"), Label).Text.Trim = "" Then
            Me.ApprovalLimit = 0
        Else
            Me.ApprovalLimit = CDbl(CType(e.Item.FindControl("lblApprovalLimit"), Label).Text)
        End If

        If e.CommandName = "Edit" Then
            Me.AddEdit = "Edit"
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                BindEdit(Me.IDBranchStatus, Me.Description, Me.ApprovalLimit)
            End If
            PanelAllFalse()
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblIDBranchStatus, lblDescription As Label
        If e.Item.ItemIndex >= 0 Then
            lblIDBranchStatus = CType(e.Item.FindControl("lblIDBranchStatus"), Label)
            lblDescription = CType(e.Item.FindControl("lblDescription"), Label)
        End If

    End Sub


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim oCustomClass As New Parameter.Area
        Dim oReturn As New Parameter.Area
        Dim oController As New AreaController
        With oCustomClass
            .FormID = "BranchLimit"
            .strConnection = GetConnectionString()
            .AddEdit = Me.AddEdit
            If Me.AddEdit = "Edit" Then
                .IDBranchStatus = Me.IDBranchStatus.Trim
                .Description = Me.Description.Trim
                .ApprovalLimit = txtApprovalLimit.Text.Trim
            End If
        End With
        Dim err As String = oController.UpdateArea(oCustomClass)
        If err <> "" Then
            ShowMessage(lblMessage, err, True)
            InitialPanel()
        Else
            BindGridEntity(Me.CmdWhere)
            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
            InitialPanel()
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        InitialPanel()
    End Sub
End Class