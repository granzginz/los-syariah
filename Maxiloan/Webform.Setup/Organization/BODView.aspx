﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BODView.aspx.vb" Inherits="Maxiloan.Webform.Setup.BODView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }
        function OpenViewRAL(pCGID, pCollectorID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Collection/Setting/CollectorRAL.aspx?CGID=' + pCGID + '&CollectorID=' + pCollectorID, 'RALView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <br/>
    <asp:Panel ID="PnlView" Style="z-index: 101; left: 24px; top: 16px" Height="136px"
        Visible="False" runat="server" Width="94.71%" HorizontalAlign="Center">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopiabu">
                <td class="tdtopi" align="center">
                    <p>
                        View Board of Director</p>
                </td>
            </tr>
        </table>
        <table class="tablegrid" height="1" cellspacing="1" cellpadding="2" width="95%" align="center"
            border="0">
            <tr class="tdganjil">
                <td class="tdgenap" style="height: 15px" width="25%">
                    BOD&nbsp;ID
                </td>
                <td class="tdganjil" style="width: 277px; height: 15px" width="277">
                    <asp:Label ID="lblBODID" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 173px; height: 19px" width="173">
                    Name
                </td>
                <td class="tdganjil" style="width: 277px; height: 19px" width="277">
                    <asp:Label ID="lblBODName" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 173px; height: 17px" width="173">
                    Title
                </td>
                <td class="tdganjil" style="width: 277px; height: 17px" width="277">
                    <asp:Label ID="lblTitle" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 14px" width="173">
                    NPWP
                </td>
                <td class="tdganjil" style="width: 277px; height: 14px" width="277">
                    <asp:Label ID="lblNPWP" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdjudul" style="height: 18px" width="173" colspan="2">
                    <strong>Address</strong>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 13px" width="25%">
                    Address
                </td>
                <td class="tdganjil" style="width: 277px; height: 13px" width="277">
                    <asp:Label ID="lblAddress" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 16px" width="173">
                    RT/RW
                </td>
                <td class="tdganjil" style="width: 277px; height: 16px" width="277">
                    <asp:Label ID="lblRT" runat="server" Width="32px"></asp:Label>/
                    <asp:Label ID="lblRW" runat="server" Width="32px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 17px" width="173">
                    Kelurahan
                </td>
                <td class="tdganjil" style="width: 277px; height: 17px" width="277">
                    <asp:Label ID="lblKelurahan" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="173">
                    Kecamatan
                </td>
                <td class="tdganjil" style="width: 277px; height: 1px" width="277">
                    <asp:Label ID="lblKecamatan" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="173">
                    City
                </td>
                <td class="tdganjil" style="width: 277px; height: 1px" width="277">
                    <asp:Label ID="lblCity" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="173">
                    Zip Code
                </td>
                <td class="tdganjil" style="width: 277px; height: 1px" width="277">
                    <asp:Label ID="lblZipCode" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 19px" width="173">
                    Phone 1
                </td>
                <td class="tdganjil" style="width: 277px; height: 19px" width="277">
                    <asp:Label ID="lblAreaPhone1" runat="server" Width="32px"></asp:Label>-
                    <asp:Label ID="lblPhone1" runat="server" Width="32px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="173">
                    Phone 2
                </td>
                <td class="tdganjil" style="width: 277px; height: 1px" width="277">
                    <asp:Label ID="lblAreaPhone2" runat="server" Width="32px"></asp:Label>-
                    <asp:Label ID="lblPhone2" runat="server" Width="32px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 17px" width="173">
                    Fax
                </td>
                <td class="tdganjil" style="width: 277px; height: 17px" width="277">
                    <asp:Label ID="lblAreaFax" runat="server" Width="16px"></asp:Label>-
                    <asp:Label ID="lblFax" runat="server" Width="32px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="173">
                    Email
                </td>
                <td class="tdganjil" style="width: 277px; height: 1px" width="277">
                    <asp:Label ID="lblEmail" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="173">
                    Mobile Phone
                </td>
                <td class="tdganjil" style="width: 277px; height: 1px" width="277">
                    <asp:Label ID="lblMobilePhone" runat="server" Width="496px"></asp:Label>
                </td>
            </tr>
        </table>
        <table id="Table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="btnClose" runat="server" ImageUrl="../../Images/ButtonOk.gif"
                        CausesValidation="true"></asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlClose" Style="z-index: 103; left: 24px; top: 360px" runat="server"
        Width="95%">
    </asp:Panel>
    </form>
</body>
</html>
