﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.cbse
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class Branch
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBranchAddress As UcCompanyAddress
    Protected WithEvents UcContactPerson As UcContactPerson
#Region " Private Const "
    Dim m_Branch As New BranchController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "

    Public Property EmployeeID() As String
        Get
            Return CStr(ViewState("EmployeeID"))
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeID") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "Branch"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)
                InitialDefaultPanel()
                FillCombo()
                BranchStatus()
            End If
        End If
    End Sub
#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        lblMessage.Visible = False
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oBranch As New Parameter.Branch

        With oBranch
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oBranch = m_Branch.BranchList(oBranch)

        With oBranch
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oBranch.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            btnPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            btnPrint.Enabled = True

        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

#Region "rboBranchStatus"
    Sub BranchStatus()
        Dim oCustomClassRbo As New Parameter.GeneralPaging
        Dim oControllerRbo As New GeneralPagingController
        With oCustomClassRbo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spBranchStatus"
        End With
        oCustomClassRbo = oControllerRbo.GetGeneralPaging(oCustomClassRbo)
        Dim dtInsAssetType As New DataTable
        dtInsAssetType = oCustomClassRbo.ListData

        rbBranchStatus.DataSource = dtInsAssetType
        rbBranchStatus.DataTextField = "Description"
        rbBranchStatus.DataValueField = "IDBranchStatus"
        rbBranchStatus.DataBind()
    End Sub
#End Region

#Region "FillCombo"
    Private Sub FillCombo()
        Dim strCombo As String
        Dim strConn As String
        Dim dt As New DataTable

        strConn = GetConnectionString()

        '--------------------------------
        ' Combo Company
        '--------------------------------
        strCombo = "Company"

        dt = m_Branch.GetCombo(strConn, strCombo)
        cboCompany.DataTextField = "Name"
        cboCompany.DataValueField = "ID"
        cboCompany.DataSource = dt
        cboCompany.DataBind()

        cboCompany.Items.Insert(0, "Select One")
        cboCompany.Items(0).Value = "0"

        dt = New DataTable

        '--------------------------------
        ' Combo Area
        '--------------------------------
        strCombo = "Area"

        dt = m_Branch.GetCombo(strConn, strCombo)
        cboArea.DataTextField = "Name"
        cboArea.DataValueField = "ID"
        cboArea.DataSource = dt
        cboArea.DataBind()

        cboArea.Items.Insert(0, "Select One")
        cboArea.Items(0).Value = "0"

        dt = New DataTable


        '--------------------------------
        ' Combo AOCard
        '--------------------------------
        strCombo = "AOCard"

        dt = m_Branch.GetCombo(strConn, strCombo)
        cboAOCard.DataTextField = "Name"
        cboAOCard.DataValueField = "ID"
        cboAOCard.DataSource = dt
        cboAOCard.DataBind()

        cboAOCard.Items.Insert(0, "Select One")
        cboAOCard.Items(0).Value = "0"

    End Sub
#End Region
#Region "ClearAddForm"
    Private Sub ClearAddForm()

        txtID.Text = ""
        txtName.Text = ""
        txtInitialName.Text = ""
        cboCompany.SelectedIndex = 0
        cboArea.SelectedIndex = 0

        lblID.Visible = False
        txtID.Visible = True

        With UcBranchAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Setting"
            .BindAddress()
        End With

        With UcContactPerson
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .MobilePhone = ""
            .Email = ""
            .EnabledContactPerson()
            .BindContacPerson()
        End With

        txtBranchManager.Text = ""
        txtADh.Text = ""
        txtARControlName.Text = ""
        txtProductivityValue.Text = "0"
        txtProductivityCollection.Text = "0"
        txtNumOfEmployee.Text = "0"

        cboAOCard.SelectedIndex = 0
    End Sub
#End Region
#Region "BtnAdd"
    Private Sub BtnAddnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddnew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True
            lblMessage.Text = ""
        End If
    End Sub
#End Region
#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.Branch
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .BranchId = txtID.Text.Trim
            .BranchInitialName = txtInitialName.Text.Trim
            .BranchFullName = txtName.Text.Trim

            .CompanyID = cboCompany.SelectedItem.Value
            .AreaID = cboArea.SelectedItem.Value

            .BranchManager = txtBranchManager.Text.Trim
            .ADH = txtADh.Text.Trim
            .ARControlName = txtARControlName.Text
            .BranchStatus = rbBranchStatus.SelectedItem.Value
            .ProductivityValue = CDbl(txtProductivityValue.Text.Trim)
            .ProductivityCollection = CDbl(txtProductivityCollection.Text.Trim)
            .EmployeeNumber = CInt(txtNumOfEmployee.Text.Trim)
            .Custodian = txtCustodian.Text.Trim
            .AOCard = cboAOCard.SelectedItem.Value
            .STNKPICName = txtSTNKPICName.Text
        End With

        With oClassAddress
            .Address = UcBranchAddress.Address
            .RT = UcBranchAddress.RT
            .RW = UcBranchAddress.RW
            .Kelurahan = UcBranchAddress.Kelurahan
            .Kecamatan = UcBranchAddress.Kecamatan
            .City = UcBranchAddress.City
            .ZipCode = UcBranchAddress.ZipCode
            .AreaPhone1 = UcBranchAddress.AreaPhone1.Trim
            .Phone1 = UcBranchAddress.Phone1.Trim
            .AreaPhone2 = UcBranchAddress.AreaPhone1.Trim
            .Phone2 = UcBranchAddress.Phone2.Trim
            .AreaFax = UcBranchAddress.AreaFax.Trim
            .Fax = UcBranchAddress.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle
            .MobilePhone = UcContactPerson.MobilePhone
            .Email = UcContactPerson.Email
        End With

        Try
            m_Branch.BranchAdd(customClass, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, "Data Berhasil disimpan ", False)

            Me.Cache.Remove(CACHE_BRANCH_COLLECTION & Me.GroubDbID)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
        End Try
    End Sub
#End Region

#Region "EDIT"
    Private Sub edit(ByVal Branchid As String)

        Dim customClass As New Parameter.Branch
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .BranchId = lblID.Text.Trim
            .BranchInitialName = txtInitialName.Text.Trim
            .BranchFullName = txtName.Text.Trim

            .CompanyID = cboCompany.SelectedItem.Value
            .AreaID = cboArea.SelectedItem.Value

            .BranchManager = txtBranchManager.Text.Trim
            .ADH = txtADh.Text.Trim
            .ARControlName = txtARControlName.Text
            .BranchStatus = rbBranchStatus.SelectedItem.Value
            .ProductivityValue = CDbl(txtProductivityValue.Text.Trim)
            .ProductivityCollection = CDbl(txtProductivityCollection.Text.Trim)
            .EmployeeNumber = CInt(txtNumOfEmployee.Text.Trim)
            .AOCard = cboAOCard.SelectedItem.Value
            .Custodian = txtCustodian.Text.Trim
            .STNKPICName = txtSTNKPICName.Text
        End With

        With oClassAddress
            .Address = UcBranchAddress.Address
            .RT = UcBranchAddress.RT
            .RW = UcBranchAddress.RW
            .Kelurahan = UcBranchAddress.Kelurahan
            .Kecamatan = UcBranchAddress.Kecamatan
            .City = UcBranchAddress.City
            .ZipCode = UcBranchAddress.ZipCode
            .AreaPhone1 = UcBranchAddress.AreaPhone1.Trim
            .Phone1 = UcBranchAddress.Phone1.Trim
            .AreaPhone2 = UcBranchAddress.AreaPhone1.Trim
            .Phone2 = UcBranchAddress.Phone2.Trim
            .AreaFax = UcBranchAddress.AreaFax.Trim
            .Fax = UcBranchAddress.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle
            .MobilePhone = UcContactPerson.MobilePhone
            .Email = UcContactPerson.Email
        End With


        Try
            m_Branch.BranchEdit(customClass, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            Me.Cache.Remove(CACHE_BRANCH_COLLECTION & Me.GroubDbID)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
        End Try
    End Sub
#End Region
#Region "viewbyID"
    Private Sub viewbyID(ByVal BranchID As String)
        Dim oBranch As New Parameter.Branch
        Dim dtBranch As New DataTable

        Try
            Dim strConnection As String = GetConnectionString()

            With oBranch
                .strConnection = GetConnectionString()
                .BranchId = BranchID

            End With

            oBranch = m_Branch.BranchByID(oBranch)
            dtBranch = oBranch.ListData

            '------------------
            'isi dari Database
            '------------------
            txtID.Visible = False
            lblID.Visible = True
            lblID.Text = Me.BranchID
            txtName.Text = CStr(dtBranch.Rows(0).Item("BranchName")).Trim
            txtInitialName.Text = CStr(dtBranch.Rows(0).Item("BranchInitialName")).Trim
            cboCompany.SelectedIndex = cboCompany.Items.IndexOf(cboCompany.Items.FindByValue(CStr(dtBranch.Rows(0).Item("CompanyID"))))
            cboArea.SelectedIndex = cboArea.Items.IndexOf(cboArea.Items.FindByValue(CStr(dtBranch.Rows(0).Item("AreaID"))))

            With UcBranchAddress
                .Address = CStr(dtBranch.Rows(0).Item("Address")).Trim
                .RT = CStr(dtBranch.Rows(0).Item("RT")).Trim
                .RW = CStr(dtBranch.Rows(0).Item("RW")).Trim
                .Kelurahan = CStr(dtBranch.Rows(0).Item("Kelurahan")).Trim
                .Kecamatan = CStr(dtBranch.Rows(0).Item("Kecamatan")).Trim
                .City = CStr(dtBranch.Rows(0).Item("City")).Trim
                .ZipCode = CStr(dtBranch.Rows(0).Item("ZipCode")).Trim
                .AreaPhone1 = CStr(dtBranch.Rows(0).Item("AreaPhone1")).Trim
                .Phone1 = CStr(dtBranch.Rows(0).Item("Phone1")).Trim
                .AreaPhone2 = CStr(dtBranch.Rows(0).Item("AreaPhone2")).Trim
                .Phone2 = CStr(dtBranch.Rows(0).Item("Phone2")).Trim
                .AreaFax = CStr(dtBranch.Rows(0).Item("AreaFax")).Trim
                .Fax = CStr(dtBranch.Rows(0).Item("Fax")).Trim
                .Style = "Setting"
                .BindAddress()
            End With

            With UcContactPerson
                .ContactPerson = CStr(dtBranch.Rows(0).Item("ContactPersonName")).Trim
                .ContactPersonTitle = CStr(dtBranch.Rows(0).Item("ContactPersonTitle")).Trim
                .MobilePhone = CStr(dtBranch.Rows(0).Item("ContactPersonHP")).Trim
                .Email = CStr(dtBranch.Rows(0).Item("ContactPersonEmail")).Trim
                .EnabledContactPerson()
                .BindContacPerson()
            End With

            txtBranchManager.Text = CStr(dtBranch.Rows(0).Item("BranchManager")).Trim
            txtADh.Text = CStr(dtBranch.Rows(0).Item("ADh")).Trim
            txtARControlName.Text = CStr(dtBranch.Rows(0).Item("ARControlName")).Trim

            txtProductivityValue.Text = CStr(dtBranch.Rows(0).Item("ProductivityValue")).Trim
            txtProductivityCollection.Text = CStr(dtBranch.Rows(0).Item("ProductivityCollection")).Trim
            txtCustodian.Text = CStr(dtBranch.Rows(0).Item("AssetDocCustodianName")).Trim
            txtSTNKPICName.Text = CStr(dtBranch.Rows(0).Item("STNKPICName")).Trim

            cboAOCard.SelectedIndex = cboAOCard.Items.IndexOf(cboAOCard.Items.FindByValue(CStr(dtBranch.Rows(0).Item("AOCard"))))
            rbBranchStatus.SelectedValue = dtBranch.Rows(0).Item("BranchStatus")
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
#Region "dtg_ItemCommand"
    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand

        Me.BranchID = e.Item.Cells(2).Text
        Me.BranchName = e.Item.Cells(4).Text
        Select Case e.CommandName
            Case "Budget"
                Me.BranchID = e.Item.Cells(2).Text
                Me.BranchName = e.Item.Cells(4).Text
                Response.Redirect("BranchBudget.aspx?BranchID=" & Me.BranchID & "&BranchName=" & Me.BranchName)

            Case "Forecast"
                Me.BranchID = e.Item.Cells(2).Text
                Me.BranchName = e.Item.Cells(4).Text
                Response.Redirect("BranchForecast.aspx?BranchID=" & Me.BranchID & "&BranchName=" & Me.BranchName)

            Case "Employee"
                Me.BranchID = e.Item.Cells(2).Text
                Me.BranchName = e.Item.Cells(4).Text
                Response.Redirect("BranchEmployee.aspx?BranchID=" & Me.BranchID & "&BranchName=" & Me.BranchName)

            Case "Edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    PanelAllFalse()
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID(Me.BranchID)

                End If

            Case "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.BranchID = e.Item.Cells(2).Text
                    Dim customClass As New Parameter.Branch
                    With customClass
                        .strConnection = GetConnectionString()
                        .BranchId = Me.BranchID
                    End With

                    Try
                        m_Branch.BranchDelete(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)

                        Me.Cache.Remove(CACHE_BRANCH_COLLECTION & Me.GroubDbID)

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub
#End Region
#Region "BtnReset"
    Private Sub BtnResetnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetnew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub
#End Region
#Region "dtg_ItemDataBound"
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton
        Dim lbBranchID As LinkButton

        If e.Item.ItemIndex >= 0 Then
            Me.BranchID = e.Item.Cells(2).Text

            lbBranchID = CType(e.Item.FindControl("lnkBranchID"), LinkButton)
            lbBranchID.Attributes.Add("OnClick", "return OpenWindowBranch('" & Me.BranchID & "')")

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
#End Region
#Region "imbSave"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                Add()
            Case "EDIT"
                edit(Me.BranchID)
        End Select
    End Sub
#End Region
#Region "imgPrint"
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If

            Response.Redirect("report/Branchreport.aspx")
        End If
    End Sub
#End Region
#Region "BtnSearch"
    Private Sub BtnSearchnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchnew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + "='" + StrSearchByValue + "'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region
#Region "imgCancel"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

End Class