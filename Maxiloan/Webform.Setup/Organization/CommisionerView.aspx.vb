﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
#End Region
Public Class CommisionerView
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("CompanyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyID") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("CompanyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyName") = Value
        End Set
    End Property

    Private Property CommisionerID() As String
        Get
            Return CStr(viewstate("CommisionerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CommisionerID") = Value
        End Set
    End Property



    Private Property CommisionerType() As String
        Get
            Return CStr(ViewState("CommisionerType"))
        End Get
        Set(ByVal Value As String)
            viewstate("CommisionerType") = Value
        End Set
    End Property

#End Region

    Dim m_Commisioner As New CompanyController


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "CompanyCommisioner"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.CommisionerID = Request.QueryString("CommisionerID")
                If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
                If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
                viewbyID(Me.CommisionerID, Me.CGID)

                PnlView.Visible = True
                pnlClose.Visible = True
                'btnClose.Attributes.Add("onclick", "windowClose()")
            End If
        End If
    End Sub


    Private Sub viewbyID(ByVal CommisionerID As String, ByVal CGID As String)

        Dim Commisioner As New Parameter.CompanyCommisioner
        Dim CommisionerList As New Parameter.CompanyCommisioner
        Dim dtInsCo As New DataTable

        With Commisioner
            .strConnection = getConnectionString
            .CompanyID = Me.CompanyID
            .CommisionerID = Me.CommisionerID
        End With

        Try

            CommisionerList = m_Commisioner.ListCompanyCommisionerByID(Commisioner)
            dtInsCo = CommisionerList.ListData

            'Isi semua field Edit Action

            lblCommisionerName.Text = CStr(dtInsCo.Rows(0).Item("CommisionerName"))
            lblCommisionerID.Text = CStr(dtInsCo.Rows(0).Item("CommisionerID"))

            lblAddress.Text = CStr(dtInsCo.Rows(0).Item("CommisionerAddress"))
            lblRT.Text = CStr(dtInsCo.Rows(0).Item("CommisionerRT"))
            lblRW.Text = CStr(dtInsCo.Rows(0).Item("CommisionerRW"))
            lblKelurahan.Text = CStr(dtInsCo.Rows(0).Item("CommisionerKelurahan"))
            lblKecamatan.Text = CStr(dtInsCo.Rows(0).Item("CommisionerKecamatan"))
            lblCity.Text = CStr(dtInsCo.Rows(0).Item("CommisionerCity"))
            lblZipCode.Text = CStr(dtInsCo.Rows(0).Item("CommisionerZipCode"))
            lblAreaPhone1.Text = CStr(dtInsCo.Rows(0).Item("CommisionerAreaPhone1"))
            lblPhone1.Text = CStr(dtInsCo.Rows(0).Item("CommisionerPhone1"))
            lblAreaFax.Text = CStr(dtInsCo.Rows(0).Item("CommisionerAreaFax"))
            lblFax.Text = CStr(dtInsCo.Rows(0).Item("CommisionerFax"))
            lblNPWP.Text = CStr(dtInsCo.Rows(0).Item("CommisionerNPWP"))

            lblTitle.Text = CStr(dtInsCo.Rows(0).Item("CommisionerTitle"))
            lblEmail.Text = CStr(dtInsCo.Rows(0).Item("CommisionerEmail"))
            lblMobilePhone.Text = CStr(dtInsCo.Rows(0).Item("CommisionerHP"))


        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        Response.Redirect("CompanyCommisioner.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName)
    End Sub
End Class