﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Company.aspx.vb" Inherits="Maxiloan.Webform.Setup.Company" %>

<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Company</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowCompany(pCompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            //function window open error salah penulisan url
            //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=15, top=10, width=985, height=480, menubar=0, scrollbars=yes');
            //
            var x = screen.width; var y = screen.height - 100;
            window.open('CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            DAFTAR PERUSAHAAN
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE" Visible="false">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                                CommandName="Delete" CausesValidation="False" Visible="false"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="CompanyID"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="CompanyID" HeaderText="ID">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lnkCompanyID" runat="server" Enabled="True">
												<%# Container.dataitem("CompanyID") %>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="COMPANYFULLNAME" HeaderText="NAMA PERUSAHAAN">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="HyCompanyFullName" runat="server" Enabled="True" CausesValidation="False">
												<%# Container.dataitem("COMPANYFULLNAME") %>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ADDRESS" SortExpression="ADDRESS" HeaderText="ALAMAT">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PHONE" SortExpression="PHONE" HeaderText="TELEPON">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CONTACTPERSONNAME" SortExpression="CONTACTPERSONNAME"
                                        HeaderText="KONTAK">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="BOD" Visible ="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBOD" CommandName="BOD" runat="server" Enabled="True" CausesValidation="False">BOD</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KOMISARIS" Visible ="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCOMMISIONER" CommandName="COMMISIONER" runat="server" Enabled="True"
                                                CausesValidation="False">COMMISIONER</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                            <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general" ></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                                ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue" Visible="false">
                    </asp:Button>
                    <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue" Visible="false">
                    </asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI PERUSAHAAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                            <asp:ListItem Value="CompanyID" Selected="True">ID Perusahaan</asp:ListItem>
                            <asp:ListItem Value="Description">Nama Perusahaan</asp:ListItem>
                            <asp:ListItem Value="Address">Alamat Perusahaan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnSearchNew" runat="server" CausesValidation="False" Text="Find"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="BtnResetNew" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            PERUSAHAAN -&nbsp;
                            <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            ID Perusahaan</label>
                        <asp:TextBox ID="txtID" runat="server" Width="48px"  MaxLength="3"></asp:TextBox>
                        <asp:Label ID="lblID" runat="server" Width="129px" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                            Display="Dynamic" ErrorMessage="Harap isi ID Perusahaan" ControlToValidate="txtID" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="ID Perusahaan harus diantara 0 - 199"
                            ControlToValidate="txtID" MinimumValue="0" MaximumValue="199" CssClass="validator_general" ></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            Nama Lengkap Perusahaan</label>
                        <asp:TextBox ID="txtName" runat="server" Width="291px"  MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" Display="Dynamic"
                            ErrorMessage="Harap isi nama" ControlToValidate="txtName" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Singkat</label>
                        <asp:TextBox ID="txtShortName" runat="server" Width="200px"  MaxLength="20"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Singkatan</label>
                        <asp:TextBox ID="txtInitialName" runat="server" Width="200px" 
                            MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            <strong>ALAMAT</strong>
                        </label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <uc1:uccompanyaddress id="UcCompanyAddress" runat="server"></uc1:uccompanyaddress>
                    </div>
                </div>
                <%--edit npwp, ktp, telepon by ario--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_split_req">
                            NPWP</label>
                        <asp:TextBox ID="txtNPWP" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator4" runat="server" Display="Dynamic"
                        ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Nomor NPWP dengan Angka"
                        ControlToValidate="txtNPWP"  CssClass ="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNPWP"
                        ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            TDP</label>
                        <asp:TextBox ID="txtTDP" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            SIUP</label>
                        <asp:TextBox ID="txtSIUP" runat="server" Width="300px"  MaxLength="30"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            <strong>KONTAK</strong>
                        </label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <uc1:uccontactperson id="UcContactPerson" runat="server"></uc1:uccontactperson>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnCancel" runat="server" OnClientClick="windowClose();" CausesValidation="False"
                        Text="Cancel" CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
