﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompanyCommisioner.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.CompanyCommisioner" %>

<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Are you sure want to delete this record?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowCompany(pCompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWindowCommisioner(pCommisionerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CommisionerView.aspx?CommisionerID=' + pCommisionerID, 'CommisionerView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Style="z-index: 101; top: 8px; left: 16px" runat="server"
        Font-Italic="True" Font-Names="Verdana" Width="624px"  font-name="Verdana"
        ForeColor="Red">*) Mandatory</asp:Label><br />
    <asp:Panel ID="pnlList" Style="z-index: 101" Width="99.69%" runat="server">
        <br />
        <table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
            <tr class="trtopiabu">
                <td class="tdtopi" align="center">
                    List of Commisioner
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
            <tr align="center">
                <td width="100%">
                    <asp:DataGrid ID="dtg" runat="server" Width="100%" BorderWidth="0px" CellPadding="3"
                        CellSpacing="1" HorizontalAlign="Left" OnSortCommand="Sorting" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="tablegrid">
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle Height="20px" CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CommisionerID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="CommisionerID" HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Left" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCommisionerID" runat="server" Enabled="True" CommandName="ViewCommisioner"
                                        CausesValidation="False">
												<%# Container.dataitem("CommisionerID") %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CommisionerID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CommisionerNAME" SortExpression="CommisionerNAME" HeaderText="NAME">
                                <HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CommisionerTITLE" SortExpression="CommisionerTITLE" HeaderText="TITLE">
                                <HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table class="nav_tbl" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td class="nav_tbl_td1">
                    <table class="nav_command_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="BtnAdd" runat="server" ImageUrl="../../Images/ButtonAdd.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imgPrint" runat="server" Enabled="true" ImageUrl="../../Images/ButtonPrint.gif">
                                </asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbBack" runat="server" CausesValidation="False" ImageUrl="../../images/buttonback.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="nav_tbl_td2">
                    <table class="nav_page_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                            </td>
                            <td>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnGoPage" runat="server" text="Go" class="small buttongo blue"/>
                                    
                            </td>
                            <asp:RangeValidator ID="rgvGo" runat="server" 
                                 ControlToValidate="txtGopage" MinimumValue="1" ErrorMessage="Page No. is not valid"
                                MaximumValue="999999999" Type="Integer" CssClass="validator_general" ></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                                 ControlToValidate="txtGopage" ErrorMessage="Page No. is not valid"
                                Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav_totpage" colspan="2">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </td>
            </tr>
        </table>
        <br />
        <table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
            <tr class="trtopiabu">
                <td class="tdtopi" align="center">
                    Find Commisioner
                </td>
            </tr>
        </table>
        <table class="tablegrid" border="0" cellspacing="1" cellpadding="2" width="95%" align="center">
            <tr>
                <td class="tdgenap" width="25%">
                    Company ID
                </td>
                <td class="tdganjil">
                    <asp:LinkButton ID="lnkCompanyID" runat="server" Width="528px"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="height: 18px" class="tdgenap" width="25%">
                    Company Name
                </td>
                <td style="height: 18px" class="tdganjil">
                    <asp:Label ID="lblCompanyName" runat="server" Width="528px" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr class="tdganjil">
                <td class="tdgenap" width="25%">
                    Find By
                </td>
                <td class="tdganjil">
                    <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                        <asp:ListItem Value="CommisionerID" Selected="True">Commisioner ID</asp:ListItem>
                        <asp:ListItem Value="CommisionerName">Commisioner Name</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
            <tr align="left">
                <td>
                    <asp:ImageButton ID="BtnSearch" runat="server" CausesValidation="False" ImageUrl="../../Images/ButtonSearch.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="BtnReset" runat="server" CausesValidation="False" ImageUrl="../../Images/ButtonReset.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" Style="z-index: 102" Width="100%" runat="server" HorizontalAlign="Center">
        <table id="Table4" border="0" cellspacing="3" cellpadding="0" width="95%">
            <tr>
                <td align="left">
                    <asp:Label Style="z-index: 101; top: 8px; left: 16px" ID="Label1" runat="server"
                        ForeColor="Red" font-name="Verdana"  Width="112px" Font-Names="Verdana"
                        Font-Italic="True">*) Mandatory</asp:Label>
                </td>
            </tr>
        </table>
        <table id="Table2" border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
            <tr class="trtopiabu">
                <td class="tdtopi" align="center">
                    Commisioner&nbsp;&nbsp;-
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table id="Table7" class="tablegrid" border="0" cellspacing="1" cellpadding="2" width="95%"
            align="center">
            <tr class="tdganjil">
                <td style="height: 23px" class="tdgenap" width="25%">
                    Commisioner ID&nbsp;<font color="red">*)</font>
                </td>
                <td style="height: 23px" class="tdganjil">
                    <asp:TextBox ID="txtID" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                    <asp:Label ID="lblID" runat="server" Width="129px" Visible="False"></asp:Label>
                    <div style="width: 16px; display: inline; height: 15px; color: red" >
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                        Display="Dynamic" ErrorMessage="You must fill Commisioner ID" ControlToValidate="txtID" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="tdganjil">
                <td class="tdgenap" width="25%">
                    Name&nbsp;<font color="red">*)</font>
                </td>
                <td style="height: 22px" class="tdganjil">
                    <asp:TextBox ID="txtName" runat="server" Width="200px"  MaxLength="50"></asp:TextBox>
                    <div style="width: 16px; display: inline; height: 15px; color: red" >
                    </div>
                    <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" Display="Dynamic"
                        ErrorMessage="You must fill Name" ControlToValidate="txtName" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="height: 23px" class="tdgenap" width="25%">
                    Title&nbsp;<font color="red">*)</font>
                </td>
                <td style="height: 23px" class="tdganjil">
                    <asp:TextBox ID="txtTitle" runat="server" Width="200px"  MaxLength="100"></asp:TextBox>
                    <div style="width: 16px; display: inline; height: 15px; color: red" >
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="184px"
                        Display="Dynamic" ErrorMessage="You must fill Title" ControlToValidate="txtTitle" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="height: 22px" class="tdgenap" width="25%">
                    NPWP
                </td>
                <td style="height: 22px" class="tdganjil">
                    <asp:TextBox ID="txtNPWP" runat="server" Width="200px"  MaxLength="30"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="25%">
                    ID Type
                </td>
                <td style="height: 22px" class="tdganjil">
                    <asp:DropDownList ID="cboIDType" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="25%">
                    ID Number
                </td>
                <td style="height: 22px" class="tdganjil">
                    <asp:TextBox ID="txtIDNumber" runat="server" Width="200px"  MaxLength="20"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table id="Table16" class="tablegrid" cellspacing="1" cellpadding="2" width="95%">
            <tr>
                <td style="height: 17px" class="tdtopi" colspan="2">
                    ADDRESS
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%">
            <tr>
                <td>
                    <uc1:uccompanyaddress id="UcCompanyAddress" runat="server"></uc1:uccompanyaddress>
                </td>
            </tr>
        </table>
        <table id="Table3" class="tablegrid" border="0" cellspacing="1" cellpadding="2" width="95%"
            align="center">
            <tr class="tdganjil">
                <td style="height: 9px" class="tdgenap" width="25%">
                    Email
                </td>
                <td style="height: 9px" class="tdganjil">
                    <asp:TextBox ID="txtEmail" runat="server" Width="200px"  MaxLength="30"></asp:TextBox>
                </td>
            </tr>
            <tr class="tdganjil">
                <td class="tdgenap" width="25%">
                    Mobile Phone
                </td>
                <td style="height: 22px" class="tdganjil">
                    <asp:TextBox ID="txtMobilePhone" runat="server" Width="200px" 
                        MaxLength="20"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%">
            <tr>
                <td>
                </td>
            </tr>
        </table>
        <table style="height: 19px" id="TableAddSave" border="0" cellspacing="0" cellpadding="0"
            width="95%" align="center">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbSave" runat="server" ImageUrl="../../Images/ButtonSave.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" ImageUrl="../../Images/ButtonCancel.gif">
                    </asp:ImageButton>&nbsp; <a href="javascript:history.back();"></a>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
