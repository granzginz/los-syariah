﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BranchView.aspx.vb" Inherits="Maxiloan.Webform.Setup.BranchView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BranchView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function OpenViewRAL(pCGID, pCollectorID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorRAL.aspx?CGID=' + pCGID + '&CollectorID=' + pCollectorID, 'RALView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlView" runat="server">
        <div class="title_strip">
        </div>
        <div class="form_title">
            <div class="form_single">
                <h3>
                    VIEW - CABANG
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:Label ID="lblBranchID" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Cabang</label>
                <asp:Label ID="lblBranchFullName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Singkat</label>
                <asp:Label ID="lblBranchInitialName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perusahaan</label>
                <asp:Label ID="lblCompanyName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Area</label>
                <asp:Label ID="lblAreaName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat</label>
                <asp:Label ID="lblAddress" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    RT/RW</label>
                <asp:Label ID="lblRT" runat="server" Width="32px"></asp:Label>/
                <asp:Label ID="lblRW" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kota</label>
                <asp:Label ID="lblCity" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kode Pos</label>
                <asp:Label ID="lblZipCode" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Telepon-1</label>
                <asp:Label ID="lblAreaPhone1" runat="server" Width="32px"></asp:Label>-
                <asp:Label ID="lblPhone1" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No telepon-2</label>
                <asp:Label ID="lblAreaPhone2" runat="server" Width="32px"></asp:Label>-
                <asp:Label ID="lblPhone2" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fax</label>
                <asp:Label ID="lblAreaFax" runat="server" Width="16px"></asp:Label>-
                <asp:Label ID="lblFax" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>KONTAK</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama</label>
                <asp:Label ID="lblCPName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:Label ID="lblCPTitle" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    e-Mail</label>
                <asp:Label ID="lblCPEmail" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No HandPhone</label>
                <asp:Label ID="lblCPMobilePhone" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>DATA CABANG</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Manager Cabang</label>
                <asp:Label ID="lblBranchManager" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama ADH</label>
                <asp:Label ID="lblADH" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama AR Control</label>
                <asp:Label ID="lblARControlName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Pengurus BPKB</label>
                <asp:Label ID="lblCustodian" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama pengurus STNK</label>
                <asp:Label ID="lblSTNKPICName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Cabang</label>
                <asp:Label ID="lblBranchStatus" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Produktivitas</label>
                <asp:Label ID="lblProductivityValue" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Produktivitas Collection</label>
                <asp:Label ID="lblProductivityCollection" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Karyawan</label>
                <asp:Label ID="lblNumOfEmployee" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Incentive Card CMO</label>
                <asp:Label ID="lblAOCard" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <asp:Button ID="btnClosenew" runat="server" OnClientClick="windowClose();" CausesValidation="true"
                Text="Close" CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
