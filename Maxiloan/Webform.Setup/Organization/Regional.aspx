﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Regional.aspx.vb" Inherits="Maxiloan.Webform.Setup.Regional" %>

<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewAddress" Src="../../Webform.UserController/UcViewAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewContactPerson" Src="../../Webform.UserController/UcViewContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcNumberFormat" Src="../../Webform.UserController/UcNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Regional</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />

    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="Regional" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
            onclick="hideMessage();"></asp:Label>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR REGIONAL
                    </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyField="RegionalID" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" CausesValidation="False" CommandName="edit" runat="server"
                                            ImageUrl="../../images/iconedit.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDele" CausesValidation="False" CommandName="delete" runat="server"
                                            ImageUrl="../../images/icondelete.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="RegionalID" HeaderText="ID">
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalID") %>'
                                            CommandName="id">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="RegionalFullName" HeaderText="NAMA REGIONAL">
                                    <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegionalName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalFullName") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="RegionalAddress" HeaderText="ALAMAT">
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalAddress") %>'>
                                        </asp:Label>&nbsp;
                                        <asp:Label ID="lblRT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalRT") %>'>
                                        </asp:Label>&nbsp;
                                        <asp:Label ID="lblRW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalRW") %>'>
                                        </asp:Label>&nbsp;
                                        <asp:Label ID="lblLurah" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalKelurahan") %>'>
                                        </asp:Label>&nbsp;
                                        <asp:Label ID="lblCamat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalKecamatan") %>'>
                                        </asp:Label>&nbsp;
                                        <asp:Label ID="lblCity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalCity") %>'>
                                        </asp:Label>&nbsp;
                                        <asp:Label ID="lblZipCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalZipCode") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblManager" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalManager") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblInitName" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalInitialName") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblRegionalAreaPhone2" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalAreaPhone2") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblRegionalPhone2" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalPhone2") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblRegionalAreaFax" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalAreaFax") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblRegionalFax" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalFax") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblContactPersonJobTitle" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContactPersonJobTitle") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblContactPersonEmail" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContactPersonEmail") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblContactPersonHP" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContactPersonHP") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblApprovalLimit" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApprovalLimit") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="RegionalAreaPhone1, RegionalPhone1" HeaderText="TELEPON">
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegionalAreaPhone1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalAreaPhone1") %>'>
                                        </asp:Label>-
                                        <asp:Label ID="lblRegionalPhone1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegionalPhone1") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ContactPersonName" HeaderText="KONTAK">
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblContact" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContactPersonName") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                        <asp:RangeValidator ID="rgvGo" runat="server" Font-Size="8pt" ControlToValidate="txtpage"
                            MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                            Font-Names="Verdana" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                            ErrorMessage="*" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="small button green">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        CARI REGIONAL
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari berdasarkan</label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="RegionalID">ID Regional</asp:ListItem>
                        <asp:ListItem Value="RegionalFullName">Nama Regional</asp:ListItem>
                        <asp:ListItem Value="RegionalAddress">Alamat Regional</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAdd" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        REGIONAL -&nbsp;
                        <asp:Label ID="lblJudul" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        ID Regional</label>
                    <asp:TextBox ID="txtAreaID" runat="server" MaxLength="3"></asp:TextBox>&nbsp;&nbsp;
                    <asp:Label ID="LAreaID" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        ControlToValidate="txtAreaID" ErrorMessage="Harap isi ID Regional" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <%--<asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ControlToValidate="txtAreaID"
                        ErrorMessage="ID Regional harus diantara 001 - 109" MinimumValue="001" MaximumValue="109"
                        CssClass="validator_general"></asp:RangeValidator>--%>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ControlToValidate="txtAreaID"
                        ErrorMessage="ID Regional harus diantara 000 - 999" MinimumValue="000" MaximumValue="999"
                        CssClass="validator_general"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Nama Lengkap Regional</label>
                    <asp:TextBox ID="txtAreaName" runat="server" Width="288px" MaxLength="50"></asp:TextBox>&nbsp;
                    <asp:Label ID="LAreaName" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        ControlToValidate="txtAreaName" ErrorMessage="isi Nama lengkap Regional" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Nama Singkat</label>
                    <asp:TextBox ID="txtInitName" runat="server" MaxLength="5"></asp:TextBox>&nbsp;
                    <asp:Label ID="LAreaInitialName" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                        ControlToValidate="txtInitName" ErrorMessage="Harap isi nama singkat Regional" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Manager Regional</label>
                    <asp:TextBox ID="txtManager" runat="server" MaxLength="50"></asp:TextBox>&nbsp;
                    <asp:Label ID="LAreaManager" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                        ControlToValidate="txtManager" ErrorMessage="Harap isi Manager Regional" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Approval Limit</label>
                    <uc1:ucnumberformat id="txtApprovalLimit" runat="server"></uc1:ucnumberformat>
                    <asp:Label ID="lblApprovalLimit" runat="server"></asp:Label>
                </div>
                </div>
                <div class="form_box_title_nobg">
                    <div class="form_single">
                        <h4>
                            ALAMAT
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccompanyaddress id="oCompanyAddress" runat="server"></uc1:uccompanyaddress>
                </div>
                <div class="form_box_title_nobg">
                    <div class="form_single">
                        <h4>
                            KONTAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <uc1:uccontactperson id="oContactPerson" runat="server"></uc1:uccontactperson>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
        </asp:Panel>
        <asp:Panel ID="pnlView" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        VIEW - Regional
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        ID Regional</label>
                    <asp:Label ID="lblID" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama lengkap Regional</label>
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Singkat Regional</label>
                    <asp:Label ID="lblInit" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Manager Regional</label>
                    <asp:Label ID="lblManage" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box_title_nobg">
                <div class="form_single">
                    <label>
                        <strong>ALAMAT</strong>
                    </label>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucviewaddress id="oViewAddress" runat="server"></uc1:ucviewaddress>
            </div>
            <div class="form_box_title_nobg">
                <div class="form_single">
                    <label>
                        <strong>KONTAK</strong>
                    </label>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucviewcontactperson id="oViewContact" runat="server"></uc1:ucviewcontactperson>
            </div>
            <div class="form_button">
                <asp:Button ID="btnOk" runat="server" CausesValidation="false" Text="OK" CssClass="small button blue">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
