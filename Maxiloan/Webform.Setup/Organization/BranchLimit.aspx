﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BranchLimit.aspx.vb" Inherits="Maxiloan.Webform.Setup.BranchLimit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BranchLimit</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="BranchLimit" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlMessage" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    BRANCH APPROVAL LIMIT
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h3>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="IDBranchStatus" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" CausesValidation="False" CommandName="Edit" runat="server"
                                        ImageUrl="../../images/iconedit.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IDBranchStatus" HeaderText="ID">
                                <ItemStyle Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblIDBranchStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IDBranchStatus") %>'>
                                    </asp:Label><br />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                    </asp:Label><br />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApprovalLimit" HeaderText="APPROVAL LIMIT">
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApprovalLimit" runat="server" Text='<%# format(DataBinder.Eval(Container, "DataItem.ApprovalLimit"),"##,0") %>'>
                                    </asp:Label><br />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server" Font-Size="8pt" ControlToValidate="txtpage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        Font-Names="Verdana" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                        ErrorMessage="*" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlAddEdit" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID</label>
                <asp:TextBox ID="lblID" runat="server"></asp:TextBox>&nbsp;&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    KETERANGAN</label>
                <asp:TextBox ID="lblDescription" runat="server"></asp:TextBox>&nbsp;&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Approval Limit</label>
                <uc1:ucnumberformat id="txtApprovalLimit" runat="server"></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
