﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Province.aspx.vb" Inherits="Maxiloan.Webform.Setup.Province" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Area</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
            onclick="hideMessage();"></asp:Label>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR PROVINSI
                    </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyField="Description" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" CausesValidation="False" CommandName="edit" runat="server"
                                            ImageUrl="../../images/iconedit.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDele" CausesValidation="False" CommandName="delete" runat="server"
                                            ImageUrl="../../images/icondelete.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Description" HeaderText="PROVINSI">
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InsuranceAreaName" HeaderText="WILAYAH PERTANGGUNGAN ASURANSI">
                                    <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAreaName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceAreaName") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" >
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAreaId"  Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceAreaId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                </asp:Button>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlAdd" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        PROVINSI -&nbsp;
                        <asp:Label ID="lblJudul" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
 
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Nama Lengkap Provinsi</label>
                    <asp:TextBox ID="txtProvinsiName" runat="server" Width="288px" MaxLength="50"></asp:TextBox>&nbsp;
                    <asp:Label ID="LAreaName" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        ControlToValidate="txtProvinsiName" ErrorMessage="Harap isi Nama lengkap Provinsi" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Wilayah Pertanggungan Asuransi</label>
                    <asp:DropDownList runat="server" ID="cboWilayah">
                        <asp:ListItem Value="1" Text="Wilayah 1" />
                        <asp:ListItem Value="2" Text="Wilayah 2" />
                        <asp:ListItem Value="3" Text="Wilayah 3" Selected="True" />
                    </asp:DropDownList>
                </div>
            </div>


            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
