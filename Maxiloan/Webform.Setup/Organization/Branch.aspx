﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Branch.aspx.vb" Inherits="Maxiloan.Webform.Setup.Branch" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAdress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Branch</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowBranch(pBranchID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            // window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/BranchView.aspx?BranchID=' + pBranchID, 'BranchView', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
            var x = screen.width; var y = screen.height - 100;
            window.open('BranchView.aspx?BranchID=' + pBranchID, 'BranchView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');

        }		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"> </asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR CABANG
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="BranchID" HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBranchID" runat="server" Enabled="True" CausesValidation="False">
												<%# Container.dataitem("BranchID") %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchNAME" SortExpression="BranchNAME" HeaderText="NAMA CABANG">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PHONE" SortExpression="PHONE" HeaderText="TELEPON">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CONTACTPERSONNAME" SortExpression="CONTACTPERSONNAME"
                                HeaderText="KONTAK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="KARYAWAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEmployee" runat="server" CommandName="Employee" Enabled="True"
                                        CausesValidation="False">Employee</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchInitialName"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server" font-name="Verdana" ForeColor="#993300"
                        MinimumValue="1" MaximumValue="999999999" Type="Integer" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" font-name="Verdana" ForeColor="#993300"
                        ControlToValidate="txtGopage" ErrorMessage="No Halaman Salah" Display="Dynamic"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI CABANG
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Font-Names="Verdana" Width="144px">
                    <asp:ListItem Value="BranchID" Selected="True">ID Cabang</asp:ListItem>
                    <asp:ListItem Value="Description">Nama Cabang</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearchnew" runat="server" CausesValidation="False" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnResetnew" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CABANG -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID Cabang</label>
                <asp:TextBox ID="txtID" runat="server" Width="48px" MaxLength="3"></asp:TextBox>
                <asp:Label ID="lblID" runat="server" Width="129px" Visible="False"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                    Display="Dynamic" ErrorMessage="Harap isi ID cabang" ControlToValidate="txtID"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                <%--<asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="ID Cabang harus diantara 110 - 999"
                    ControlToValidate="txtID" MaximumValue="999" MinimumValue="110" CssClass="validator_general"></asp:RangeValidator>--%>
                <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="ID Cabang harus diantara 000 - 999"
                    ControlToValidate="txtID" MaximumValue="999" MinimumValue="000" CssClass="validator_general"></asp:RangeValidator>
            </div>

        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Cabang</label>
                <asp:TextBox ID="txtName" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" Display="Dynamic"
                    ErrorMessage="Harap isi Nama Cabang" ControlToValidate="txtName" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Singkat</label>
                <asp:TextBox ID="txtInitialName" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Width="184px"
                    Display="Dynamic" ErrorMessage="Harap isi Nama Singkat" ControlToValidate="txtInitialName"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Perusahaan</label>
                <asp:DropDownList ID="cboCompany" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    ErrorMessage="Harap Pilih Perusahaan" ControlToValidate="cboCompany" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Area</label>
                <asp:DropDownList ID="cboArea" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                    ErrorMessage="Harap Pilih Area" ControlToValidate="cboArea" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <uc1:ucbranchadress id="UcBranchAddress" runat="server"></uc1:ucbranchadress>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>KONTAK</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <uc1:uccontactperson id="UcContactPerson" runat="server"></uc1:uccontactperson>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>DATA CABANG</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Manager Cabang</label>
                <asp:TextBox ID="txtBranchManager" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama ADH (Admin Head)</label>
                <asp:TextBox ID="txtADh" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama AR Control</label>
                <asp:TextBox ID="txtARControlName" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Pengurus BPKB</label>
                <asp:TextBox ID="txtCustodian" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Pengurus STNK</label>
                <asp:TextBox ID="txtSTNKPICName" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class="label_req">
                    Status Cabang</label>
                <asp:RadioButtonList ID="rbBranchStatus" runat="server" Width="80px" Height="1px"
                    RepeatDirection="Horizontal">
                </asp:RadioButtonList>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                    ErrorMessage="Harap Pilih Status Cabang" ControlToValidate="rbBranchStatus" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Produktivitas</label>
                <asp:TextBox ID="txtProductivityValue" runat="server" Width="200px" CssClass="InpType"
                    MaxLength="9">0</asp:TextBox>
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator3" runat="server" ControlToValidate="txtProductivityValue"
                    ErrorMessage="Harap isi dengan angka" Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                <asp:Label ID="Label2" runat="server" Width="176px">Target Unit/AO/Bulan</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Produktivitas Collection</label>
                <asp:TextBox ID="txtProductivityCollection" runat="server" Width="200px" CssClass="InpType"
                    MaxLength="9">0</asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtProductivityCollection"
                    ErrorMessage="Harap isi dengan angka" Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah karyawan</label>
                <asp:TextBox ID="txtNumOfEmployee" runat="server" Width="200px" CssClass="InpType"
                    MaxLength="9">0</asp:TextBox>
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" ControlToValidate="txtNumOfEmployee"
                    ErrorMessage="Harap isi dengan angka" Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Incentive Card CMO</label>
                <asp:DropDownList ID="cboAOCard" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <table>
    </table>
    </form>
</body>
</html>
