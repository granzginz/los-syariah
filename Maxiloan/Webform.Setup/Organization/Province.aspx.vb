﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class Province
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents UCRegional As UCRegional

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 50
    Private currentPageNumber As Int16 = 1

    Private oCustomClass As New Parameter.Area

    Private p_Class As New Parameter.GeneralPaging
    Private c_Class As New GeneralPagingController
 
#End Region

#Region "Properti"
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property SelectItem() As String
        Get
            Return (CType(ViewState("SelectItem"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectItem") = Value
        End Set
    End Property

    Private Property SelectValue() As String
        Get
            Return (CType(ViewState("SelectValue"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectValue") = Value
        End Set
    End Property

    Private Property StrRegionalID() As String
        Get
            Return (CType(ViewState("StrRegionalID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("StrRegionalID") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "Province"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = "all"
                Me.SortBy = "description"
                lblMessage.Text = ""
                DoBind(Me.SearchBy, Me.SortBy)

            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")
        If UBound(strBranch) > 0 Then
            ShowMessage(lblMessage, "Area Tidak bisa diganti.....", True)
 
            pnlDatagrid.Visible = False
            pnlAdd.Visible = False
        Else
            pnlAdd.Visible = False

            pnlDatagrid.Visible = False

            lblMessage.Text = ""
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView



        With p_Class
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spProvincePaging"
        End With


        p_Class = c_Class.GetGeneralPaging(p_Class)


        DtUserList = p_Class.ListData
        DvUserList = DtUserList.DefaultView
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try

        pnlDatagrid.Visible = True
        pnlAdd.Visible = False
 
    End Sub
 

    Public Sub Reset()
        txtProvinsiName.Text = ""
        cboWilayah.SelectedIndex = 0
    End Sub

#End Region




#Region "DataGridCommand"
 

    Public Sub dtgEntity_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Dim lblDescription, lblAreaId As Label

        lblDescription = CType(e.Item.FindControl("lblDescription"), Label)
        lblAreaId = CType(e.Item.FindControl("lblAreaId"), Label)

        Select Case e.CommandName
            Case "edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    Me.Mode = "edit"
                    Reset()
                    txtProvinsiName.Text = lblDescription.Text
                    txtProvinsiName.Enabled = False
                    cboWilayah.SelectedValue = lblAreaId.Text
  
                    pnlDatagrid.Visible = False
                    pnlAdd.Visible = True
                
                    lblJudul.Text = "EDIT"
                End If
            Case "delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    With p_Class
                        .strConnection = GetConnectionString()
                        .TableName = "Propinsi"
                        .WhereCond = "description='" & lblDescription.Text & "'"
                    End With
                    If (c_Class.DeleteGeneral(p_Class)) Then
                        DoBind(Me.SearchBy, Me.SortBy)
                        ShowMessage(lblMessage, "Data Berhasil diHapus", False)
                    Else
                        ShowMessage(lblMessage, "Data Tidak Berhasil Dihapus", True)
                    End If
 
                End If
        End Select
    End Sub
    Public Sub dtgEntity_SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgEntity.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)

    End Sub
#End Region
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlDatagrid.Visible = True
        pnlAdd.Visible = False
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            Me.Mode = "add"
            Reset()
           
            pnlDatagrid.Visible = False
            pnlAdd.Visible = True
     
       
            btnSave.Visible = True
            btnCancel.Visible = True
            lblJudul.Text = "ADD"
        End If
    End Sub
     

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim p_pro_Class As New Parameter.Propinsi
        Dim c_pro_Class As New PropinsiController
        Select Case Me.Mode
            Case "edit"
                pnlDatagrid.Visible = True
                pnlAdd.Visible = False

                With p_pro_Class
                    .strConnection = GetConnectionString()
                    .Description = txtProvinsiName.Text
                    .InsuranceAreaId = cboWilayah.SelectedValue
                End With
                c_pro_Class.ProvinceSaveEdit(p_pro_Class)

                DoBind(Me.SearchBy, Me.SortBy)
            Case "add"
                pnlDatagrid.Visible = True
                pnlAdd.Visible = False

                With p_pro_Class
                    .strConnection = GetConnectionString()
                    .Description = txtProvinsiName.Text
                    .InsuranceAreaId = cboWilayah.SelectedValue
                End With
                Dim err As String = c_pro_Class.ProvinceSaveAdd(p_pro_Class)


                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                    pnlAdd.Visible = True
                    pnlDatagrid.Visible = False
                Else
                    DoBind(Me.SearchBy, Me.SortBy)
                    ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
                End If
        End Select
 
    End Sub
     

End Class