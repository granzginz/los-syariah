﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BranchEmployee.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.BranchEmployee" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAdress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register Src="../../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc2" %>
<%@ Register src="../../Webform.UserController/ucDateCE.ascx" tagname="ucDateCE" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BranchEmployee</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        // function fconfirm() {
        //            if (window.confirm("apakah yakin mau hapus data ini ?"))
        //                return true;
        //            else
        //                return false;

        //        }

        //        function fback() {
        //            history.go(-1);
        //            return false;
        //        }

        //        function openwindowbranch(pbranchid) {
        //            var x = screen.width; var y = screen.height - 100;
        //            window.open(servername + app + '/webform.setup/organization/branchview.aspx?branchid=' + pbranchid + '&style=setting', 'branchview', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        //        }

        //        function openwindowemployee(pbranchid, pemployeeid) {
        //            var x = screen.width; var y = screen.height - 100;
        //            window.open('employeeview.aspx?branchid=' + pbranchid + '&employeeid=' + pemployeeid + '&style=setting', 'employeeview', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        //        }

        function cbopositionchange(obj) {
//            alert(obj.value);
            var val = obj.value;
                if (val == 'AO') {
                    document.all.cboAOLevel.disabled = false;
                    document.getElementById("cboAOLevel").classList.remove("aspNetDisabled");
                    document.all.cboAOSupervisor.disabled = false;
                    document.getElementById("cboAOSupervisor").classList.remove("aspNetDisabled");
                    document.all.rbAOSales.disabled = false;
                    document.getElementById("rbAOSales").classList.remove("aspNetDisabled");
                    document.all.cboIncentiveCard.disabled = true;
                    document.all.cboCASupervisor.disabled = true;
//                    alert("AO|done");
                    }
                if (val == 'CA') {
                    document.all.cboAOLevel.disabled = true;
                    document.all.cboAOSupervisor.disabled = true;
                    document.all.rbAOSales.disabled = true;
                    document.all.cboCASupervisor.disabled = false;
                    document.getElementById("cboCASupervisor").classList.remove("aspNetDisabled");
//                    alert("CA|done");
                }
                if ((val != 'CA') && (val != 'AO')) {
                    document.all.cboAOLevel.disabled = true;
                    document.all.cboAOSupervisor.disabled = true;
                    document.all.rbAOSales.disabled = true;
                    document.all.cboCASupervisor.disabled = true;

                }
                }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR KARYAWAN
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PINDAH CABANG">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbPindahCabang" runat="server" ImageUrl="../../Images/IconPlafonBranch.gif"
                                        CommandName="PindahCabang" CausesValidation="False"></asp:ImageButton>                                    
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="EmployeeID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="EmployeeID" HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEmployeeID" runat="server" Enabled="True" Text='<%# DataBinder.eval(Container,"DataItem.EmployeeID")%>'>
                                    </asp:LinkButton>
                                    <asp:Label ID="lblEmployeeID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.EmployeeID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.BankID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankBranchID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.BankBranchID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.BankName")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankBranchName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.BankBranchName")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAccountNo" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.AccountNo")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAccountName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.AccountName")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblInitialName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.InitialName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="EmployeeName" SortExpression="EmployeeName" HeaderText="NAMA KARYAWAN">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="EmployeePosition" SortExpression="EmployeePosition" HeaderText="POSISI">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aktif" SortExpression="Aktif" HeaderText="Aktif">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server" MinimumValue="1" MaximumValue="999999999"
                        Type="Integer" ControlToValidate="txtGopage" ErrorMessage="No Halaman Salah"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI KARYAWAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:LinkButton ID="lbBranchID" runat="server" Width="416px"></asp:LinkButton>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Cabang</label>
                <asp:Label ID="lblBranchName" runat="server" Width="400px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                    <asp:ListItem Value="EmployeeID" Selected="True">ID Karyawan</asp:ListItem>
                    <asp:ListItem Value="EmployeeName">Nama Karyawan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Posisi</label>
                <asp:DropDownList ID="cboPositionSearch" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearchnew" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnResetnew" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KARYAWAN -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID Karyawan</label>
                <asp:TextBox ID="txtID" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="lbl_Sign" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                    Display="Dynamic" ControlToValidate="txtID" ErrorMessage="Harap isi ID Karyawan"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Karyawan</label>
                <asp:TextBox ID="txtName" runat="server" Width="291px" MaxLength="200"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" Display="Dynamic"
                    ControlToValidate="txtName" ErrorMessage="Harap isi Nama Karyawan" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

         <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Inisial</label>
                <asp:TextBox ID="txtInisial" runat="server" Width="80px" MaxLength="3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="184px" Display="Dynamic"
                    ControlToValidate="txtInisial" ErrorMessage="Harap isi Initial" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Posisi</label>
                <asp:DropDownList ID="cboPosition" runat="server" onchange="cbopositionchange(this);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqPosition" runat="server" ControlToValidate="cboPosition"
                    ErrorMessage="Harap pilih Posisi" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Level CMO</label>
                <asp:DropDownList ID="cboAOLevel" runat="server">
                    <asp:ListItem Value="0" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="T">Trainee</asp:ListItem>
                    <asp:ListItem Value="J">Junior</asp:ListItem>
                    <asp:ListItem Value="S">Senior</asp:ListItem>
                    <asp:ListItem Value="E">Executive</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Supervisor CMO</label>
                <asp:DropDownList ID="cboAOSupervisor" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    CMO Sales</label>
                <asp:DropDownList ID="rbAOSales" runat="server">
                    <asp:ListItem Value="0" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="I">Indirect</asp:ListItem>
                    <asp:ListItem Value="D">Direct</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Tanggal Mulai Kerja</label>
               
                <uc3:ucDateCE ID="txtAOFirstDate" runat="server" />
               
            </div>
        </div>        
        <div class="form_box">
            <div class="form_single">
                <label>
                    Incentive Card Karyawan</label>
                <asp:DropDownList ID="cboIncentiveCard" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Supervisor CA</label>
                <asp:DropDownList ID="cboCASupervisor" runat="server" Enabled="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Overdue Penalty Card</label>
                <asp:DropDownList ID="cboPenaltyCard" runat="server" Enabled="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Aktif</label>
                <asp:CheckBox ID="chkisActive" runat="server" Enabled="True"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbranchadress id="UcAddress" runat="server"></uc1:ucbranchadress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    e-Mail</label>
                <asp:TextBox ID="txtEmail" runat="server" Width="200px" MaxLength="200"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No HandPhone</label>
                <asp:TextBox ID="txtMobilePhone" runat="server" Width="200px" MaxLength="200" onkeypress="return numbersonly2(event)"></asp:TextBox>
                <asp:Label ID="bintang3" runat="server" ForeColor="Red" Visible="False">* )</asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Isi Mobile Phone!" Enabled="false"
                ControlToValidate="txtMobilePhone" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>REKENING BANK</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlpindahcabang" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KARYAWAN - PINDAH CABANG&nbsp;
                    <asp:Label ID="Label1" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Pindah Ke Cabang</label>
            <asp:Label ID="LblBranchName2" runat="server" Visible="False"></asp:Label>
            <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
        </div>
    </div>
        <div class="form_button">
            <asp:Button ID="btnPindahCabang" runat="server" Text="Pindah" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelPindah" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>

    </form>
</body>
</html>
