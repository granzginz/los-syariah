﻿
#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
#End Region
Public Class EmployeeView
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("CompanyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyID") = Value
        End Set
    End Property

    Private Property EmployeeID() As String
        Get
            Return CStr(viewstate("EmployeeID"))
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property

    Private Property EmployeeName() As String
        Get
            Return CStr(viewstate("EmployeeName"))
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeName") = Value
        End Set
    End Property
#End Region

    Dim m_Employee As New EmployeeController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "BranchEmployee"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.BranchID = Request.QueryString("BranchID")
                Me.EmployeeID = Request.QueryString("EmployeeID")
                viewbyID(Me.BranchID, Me.EmployeeID)

                PnlView.Visible = True
                btnCloseNew.Attributes.Add("onclick", "windowClose()")
            End If
        End If
    End Sub


    Private Sub viewbyID(ByVal BranchID As String, ByVal EmployeeID As String)

        Dim Employee As New Parameter.Employee
        Dim EmployeeList As New Parameter.Employee
        Dim position As String
        Dim dt As New DataTable

        With Employee
            .strConnection = getConnectionString
            .BranchId = Me.BranchID
            .EmployeeID = Me.EmployeeID
        End With

        Try

            Employee = m_Employee.EmployeeByID(Employee)
            dt = Employee.ListData

            lblEmployeeID.Text = CStr(dt.Rows(0).Item("EmployeeID"))
            lblEmployeeName.Text = CStr(dt.Rows(0).Item("EmployeeName"))

            position = CStr(dt.Rows(0).Item("EmployeePosition"))
            lblPosition.Text = CStr(dt.Rows(0).Item("EmployeePositionDesc"))

            lblAOLevel.Text = CStr(dt.Rows(0).Item("AOLevelStatus"))
            lblAOSupervisor.Text = CStr(dt.Rows(0).Item("AOSupervisor"))
            lblAOSalesStatus.Text = CStr(dt.Rows(0).Item("AOSalesStatus"))
            lblAOFirstDate.Text = CStr(dt.Rows(0).Item("AOFirstDate"))
            lblAOEndDate.Text = CStr(dt.Rows(0).Item("AOEndDate"))
            lblIncentiveCardID.Text = CStr(dt.Rows(0).Item("IncentiveCard"))
            lblCASupervisor.Text = CStr(dt.Rows(0).Item("CASupervisor"))
            lblOverduePenaltyCard.Text = CStr(dt.Rows(0).Item("PenaltyCard"))
            lblAddress.Text = CStr(dt.Rows(0).Item("Address"))
            lblRT.Text = CStr(dt.Rows(0).Item("RT"))
            lblRW.Text = CStr(dt.Rows(0).Item("RW"))
            lblKelurahan.Text = CStr(dt.Rows(0).Item("Kelurahan"))
            lblKecamatan.Text = CStr(dt.Rows(0).Item("Kecamatan"))
            lblCity.Text = CStr(dt.Rows(0).Item("City"))
            lblZipCode.Text = CStr(dt.Rows(0).Item("ZipCode"))
            lblAreaPhone1.Text = CStr(dt.Rows(0).Item("AreaPhone1"))
            lblPhone1.Text = CStr(dt.Rows(0).Item("Phone1"))
            lblAreaFax.Text = CStr(dt.Rows(0).Item("AreaFax"))
            lblFax.Text = CStr(dt.Rows(0).Item("Fax"))
            lblCPEmail.Text = CStr(dt.Rows(0).Item("Email"))
            lblCPMobilePhone.Text = CStr(dt.Rows(0).Item("MobilePhone"))
        Catch ex As Exception
        End Try

    End Sub
End Class