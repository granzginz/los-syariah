﻿
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Public Class BranchView
    Inherits Maxiloan.Webform.WebBased

#Region " Property "


    Private Property BranchType() As String
        Get
            Return CStr(ViewState("BranchType"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchType") = Value
        End Set
    End Property

#End Region

    Dim m_Branch As New BranchController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "Branch"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.BranchID = Request.QueryString("BranchID")
                viewbyID(Me.BranchID)
                PnlView.Visible = True
                btnClosenew.Attributes.Add("onclick", "windowClose()")
            End If
        End If
    End Sub

    Private Sub viewbyID(ByVal BranchID As String)

        Dim Branch As New Parameter.Branch
        Dim BranchList As New Parameter.Branch
        Dim dt As New DataTable

        With Branch
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
        End With

        Try

            BranchList = m_Branch.BranchByID(Branch)
            dt = BranchList.ListData

            'Isi semua field Edit Action
            If dt.Rows.Count > 0 Then

                lblBranchFullName.Text = CStr(dt.Rows(0).Item("BranchName"))
                lblBranchID.Text = CStr(dt.Rows(0).Item("BranchID"))
                lblBranchInitialName.Text = CStr(dt.Rows(0).Item("BranchInitialName"))
                lblCompanyName.Text = CStr(dt.Rows(0).Item("CompanyName"))
                lblAreaName.Text = CStr(dt.Rows(0).Item("AreaName"))

                lblAddress.Text = CStr(dt.Rows(0).Item("Address"))
                lblRT.Text = CStr(dt.Rows(0).Item("RT"))
                lblRW.Text = CStr(dt.Rows(0).Item("RW"))
                lblKelurahan.Text = CStr(dt.Rows(0).Item("Kelurahan"))
                lblKecamatan.Text = CStr(dt.Rows(0).Item("Kecamatan"))
                lblCity.Text = CStr(dt.Rows(0).Item("City"))
                lblZipCode.Text = CStr(dt.Rows(0).Item("ZipCode"))
                lblAreaPhone1.Text = CStr(dt.Rows(0).Item("AreaPhone1"))
                lblPhone1.Text = CStr(dt.Rows(0).Item("Phone1"))
                lblAreaFax.Text = CStr(dt.Rows(0).Item("AreaFax"))
                lblFax.Text = CStr(dt.Rows(0).Item("Fax"))

                lblCPName.Text = CStr(dt.Rows(0).Item("ContactPersonName"))
                lblCPTitle.Text = CStr(dt.Rows(0).Item("ContactPersonTitle"))
                lblCPEmail.Text = CStr(dt.Rows(0).Item("ContactPersonEmail"))
                lblCPMobilePhone.Text = CStr(dt.Rows(0).Item("ContactPersonHP"))

                lblBranchManager.Text = CStr(dt.Rows(0).Item("BranchManager"))
                lblADH.Text = CStr(dt.Rows(0).Item("ADH"))
                lblARControlName.Text = CStr(dt.Rows(0).Item("ARControlName"))

                lblBranchStatus.Text = CStr(dt.Rows(0).Item("BranchStatus"))
                lblProductivityValue.Text = CStr(dt.Rows(0).Item("ProductivityValue"))
                lblProductivityCollection.Text = CStr(dt.Rows(0).Item("ProductivityCollection"))
                lblNumOfEmployee.Text = CStr(dt.Rows(0).Item("NumOfEmployee"))
                lblAOCard.Text = CStr(dt.Rows(0).Item("AOCard"))
                lblCustodian.Text = CStr(dt.Rows(0).Item("AssetDocCustodianName"))
                lblSTNKPICName.Text = CStr(dt.Rows(0).Item("STNKPICName"))

            End If

        Catch ex As Exception

        End Try

    End Sub

    
End Class