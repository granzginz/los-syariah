﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompanyView.aspx.vb" Inherits="Maxiloan.Webform.Setup.CompanyView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CompanyView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function OpenViewRAL(pCGID, pCollectorID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorRAL.aspx?CGID=' + pCGID + '&CollectorID=' + pCollectorID, 'RALView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlView" Visible="False" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - PERUSAHAAN
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Perusahaan</label>
                <asp:Label ID="lblCompanyID" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Lengkap Perusahaan</label>
                <asp:Label ID="lblCompanyFullName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Singkat</label>
                <asp:Label ID="lblCompanyShortName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Singkatan</label>
                <asp:Label ID="lblCompanyInitialName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat</label>
                <asp:Label ID="lblAddress" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    RT/RW</label>
                <asp:Label ID="lblRT" runat="server" Width="32px"></asp:Label>/
                <asp:Label ID="lblRW" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kota</label>
                <asp:Label ID="lblCity" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kode Pos</label>
                <asp:Label ID="lblZipCode" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Telepon-1</label>
                <asp:Label ID="lblAreaPhone1" runat="server" Width="32px"></asp:Label>-
                <asp:Label ID="lblPhone1" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Telepon-2</label>
                <asp:Label ID="lblAreaPhone2" runat="server" Width="32px"></asp:Label>-
                <asp:Label ID="lblPhone2" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Fax</label>
                <asp:Label ID="lblAreaFax" runat="server" Width="16px"></asp:Label>-
                <asp:Label ID="lblFax" runat="server" Width="32px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    NPWP</label>
                <asp:Label ID="lblNPWP" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    TDP</label>
                <asp:Label ID="lblTDP" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    SIUP</label>
                <asp:Label ID="lblSIUP" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>KONTAK</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama</label>
                <asp:Label ID="lblCPName" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:Label ID="lblCPTitle" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    e-Mail</label>
                <asp:Label ID="lblCPEmail" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No HandPhone</label>
                <asp:Label ID="lblCPMobilePhone" runat="server" Width="496px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <asp:Button ID="btnCloseNew" runat="server" OnClientClick="windowClose();" CausesValidation="true"
                Text="Close" CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
