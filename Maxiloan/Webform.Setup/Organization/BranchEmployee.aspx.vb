﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.cbse
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class BranchEmployee
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents UcAddress As UcCompanyAddress
    Protected WithEvents txtRupiah As ucNumberFormat
    Protected WithEvents txtAOFirstDate As ucDateCE
    Protected WithEvents oBranch As ucBranchAll
#Region " Private Const "
    Dim m_Employee As New EmployeeController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "

    Public Property EmployeeID() As String
        Get
            Return CStr(ViewState("EmployeeID"))
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return CStr(ViewState("EmployeeName"))
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeName") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then             
            Me.FormID = "BranchEmployee"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("BranchID") <> "" Then Me.BranchID = Request.QueryString("BranchID")
                If Request.QueryString("BranchName") <> "" Then Me.BranchName = Request.QueryString("BranchName")
                UcAddress.Style = "Setting"
                UcAddress.BindAddress()
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                lbBranchID.Text = Me.BranchID
                lblBranchName.Text = Me.BranchName
                BindGridEntity(Me.SearchBy, Me.SortBy)
                InitialDefaultPanel()



                Dim lbBranch As LinkButton

                lbBranch = CType(Me.FindControl("lbBranchID"), LinkButton)
                lbBranch.Attributes.Add("OnClick", "return OpenWindowBranch('" & Me.BranchID & "')")
                FillCombo()

                With UcBankAccount
                    .Style = "Marketing"
                    .BindBankAccount()
                End With

                txtAOFirstDate.IsRequired = True
            End If
        End If
    End Sub
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
        pnlpindahcabang.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        lblMessage.Visible = False
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlpindahcabang.Visible = False
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oEmployee As New Parameter.Employee

        With oEmployee
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oEmployee = m_Employee.ListEmployee(oEmployee)

        With oEmployee
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oEmployee.ListData

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            btnPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblMessage.Visible = True
        Else
            btnPrint.Enabled = True
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
#Region "FillCombo"
    Private Sub FillCombo()
        Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = getConnectionString()

        '--------------------------------
        ' Combo Position
        '--------------------------------
        strCombo = "Position"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)
        cboPositionSearch.DataTextField = "Name"
        cboPositionSearch.DataValueField = "ID"
        cboPositionSearch.DataSource = dtCombo
        cboPositionSearch.DataBind()

        cboPositionSearch.Items.Insert(0, "Select One")
        cboPositionSearch.Items(0).Value = "0"


        cboPosition.DataTextField = "Name"
        cboPosition.DataValueField = "ID"
        cboPosition.DataSource = dtCombo
        cboPosition.DataBind()

        cboPosition.Items.Insert(0, "Select One")
        cboPosition.Items(0).Value = "0"
        '--------------------------------
        ' Combo AO Supervisor
        '--------------------------------
        strCombo = "AOSupervisor"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)
        cboAOSupervisor.DataTextField = "Name"
        cboAOSupervisor.DataValueField = "ID"
        cboAOSupervisor.DataSource = dtCombo
        cboAOSupervisor.DataBind()

        cboAOSupervisor.Items.Insert(0, "Select One")
        cboAOSupervisor.Items(0).Value = "0"

        '--------------------------------
        ' Combo EmployeeIncentive Card
        '--------------------------------
        strCombo = "IncentiveCard"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)
        cboIncentiveCard.DataTextField = "Name"
        cboIncentiveCard.DataValueField = "ID"
        cboIncentiveCard.DataSource = dtCombo
        cboIncentiveCard.DataBind()

        cboIncentiveCard.Items.Insert(0, "Select One")
        cboIncentiveCard.Items(0).Value = "0"

        '--------------------------------
        ' Combo CA Supervisor
        '--------------------------------
        strCombo = "CASupervisor"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)
        cboCASupervisor.DataTextField = "Name"
        cboCASupervisor.DataValueField = "ID"
        cboCASupervisor.DataSource = dtCombo
        cboCASupervisor.DataBind()

        cboCASupervisor.Items.Insert(0, "Select One")
        cboCASupervisor.Items(0).Value = "0"

        '--------------------------------
        ' Combo PenaltyCard
        '--------------------------------
        strCombo = "PenaltyCard"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)
        cboPenaltyCard.DataTextField = "Name"
        cboPenaltyCard.DataValueField = "ID"
        cboPenaltyCard.DataSource = dtCombo
        cboPenaltyCard.DataBind()

        cboPenaltyCard.Items.Insert(0, "Select One")
        cboPenaltyCard.Items(0).Value = "0"
        dtCombo.Dispose()
    End Sub
#End Region

    Private Sub ClearAddForm()
        txtID.Text = ""
        txtName.Text = ""
        txtInisial.Text = ""
        'cboPosition.SelectedIndex = 0
        'cboAOLevel.SelectedIndex = 0
        'cboAOSupervisor.SelectedIndex = 0
        With txtAOFirstDate.Text = ""
        End With
        txtAOFirstDate.Text = ""
        lblID.Visible = False
        txtID.Visible = True
        With UcAddress
            .Address = ""
            .Kelurahan = ""
            .Kecamatan = ""
            .RT = ""
            .RW = ""
            .AreaPhone1 = ""
            .AreaPhone2 = ""
            .City = ""
            .Phone1 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .ZipCode = ""
            .BindAddress()
        End With
        txtMobilePhone.Text = ""
        txtEmail.Text = ""
    End Sub

    Private Sub BtnAddnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddnew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            ClearAddForm()
            FillCombo()
            PanelAllFalse()
            UcAddress.ValidatorFalse()
            BindAdd()
            RequiredFieldValidator1.Enabled = True
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True
            lbl_Sign.Visible = True
            chkisActive.Checked = True
            chkisActive.Enabled = False
        End If
    End Sub
#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.Employee
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal
        Dim isError As Boolean
        isError = False
        lblMessage.Text = ""
        Select Case cboPosition.SelectedValue
            Case "AO"
                If cboAOLevel.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih Level CMO <BR/>"
                    isError = True
                End If
                If cboAOSupervisor.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih Supervisor CMO <BR/>"
                    isError = True
                End If
                If rbAOSales.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih CMO Sales <BR/>"
                    isError = True
                End If
                If txtAOFirstDate.Text = "" Then
                    lblMessage.Text &= "Harap isi tanggal Mulai Kerja <BR/>"
                    isError = True
                End If
                If cboCASupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Harap pilih Supervisor CA <BR/>"
                    isError = True
                End If
            Case "CA"
                If cboCASupervisor.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih Supervisor CA <BR/>"
                    isError = True
                End If
                If txtAOFirstDate.Text = "" Then
                    lblMessage.Text &= "Harap isi tanggal Mulai Kerja <BR/>"
                    isError = True
                End If
                If cboAOSupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh Pilih Supervisor CMO <BR/>"
                    isError = True
                End If
            Case Else
                If cboAOSupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh pilih Supervisor CMO <BR/>"
                    isError = True
                End If
                If cboCASupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh pilih Supervisor CA <BR/>"
                    isError = True
                End If
                If txtAOFirstDate.Text = "" Then
                    lblMessage.Text &= "Harap isi tanggal Mulai Kerja <BR/>"
                    isError = True
                End If
        End Select
        ShowMessage(lblMessage, lblMessage.Text, True)
        If Not isError Then
            With customClass
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .EmployeeID = txtID.Text.Trim
                .EmployeeName = txtName.Text.Trim
                .Inisial = txtInisial.Text.Trim
                .Position = cboPosition.SelectedItem.Value.Trim
                .AOLevel = cboAOLevel.SelectedItem.Value.Trim
                .AOSupervisor = cboAOSupervisor.SelectedItem.Value.Trim
                .AOSales = rbAOSales.SelectedItem.Value.Trim

                If txtAOFirstDate.Text.Trim <> "" Then
                    .AOFirstDate = ConvertDate2(txtAOFirstDate.Text.Trim)
                Else
                    .AOFirstDate = ""
                End If

                .EmployeeIncentiveCard = cboIncentiveCard.SelectedItem.Value.Trim
                .CASupervisor = cboCASupervisor.SelectedItem.Value.Trim
                .OverduePenaltyCard = cboPenaltyCard.SelectedItem.Value.Trim

                .BankID = UcBankAccount.BankID
                .BankBranch = UcBankAccount.BankBranch
                .AccountNo = UcBankAccount.AccountNo
                .AccountName = UcBankAccount.AccountName
                .BankBranchId = UcBankAccount.BankBranchId
            End With

            With oClassAddress
                .Address = UcAddress.Address.Trim
                .RT = UcAddress.RT.Trim
                .RW = UcAddress.RW.Trim
                .Kelurahan = UcAddress.Kelurahan.Trim
                .Kecamatan = UcAddress.Kecamatan.Trim
                .City = UcAddress.City.Trim
                .ZipCode = UcAddress.ZipCode.Trim
                .AreaPhone1 = UcAddress.AreaPhone1.Trim
                .Phone1 = UcAddress.Phone1.Trim
                .AreaPhone2 = UcAddress.AreaPhone2.Trim
                .Phone2 = UcAddress.Phone2.Trim
                .AreaFax = UcAddress.AreaFax.Trim
                .Fax = UcAddress.Fax.Trim
            End With

            With oClassPersonal
                .MobilePhone = txtMobilePhone.Text.Trim
                .Email = txtEmail.Text.Trim
            End With

            Try
                m_Employee.AddEmployee(customClass, oClassAddress, oClassPersonal)
               ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
                pnlAddEdit.Visible = False
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
            Catch ex As Exception
               showmessage(lblmessage, ex.Message , true)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
            End Try
        End If
    End Sub
#End Region
#Region "EDIT"
    Private Sub edit(ByVal Employeeid As String)

        Dim customClass As New Parameter.Employee
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal
        Dim isError As Boolean
        isError = False
        lblMessage.Text = ""
        Select Case cboPosition.SelectedValue
            Case "AO"
                If cboAOLevel.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih Level CMO <BR/>"
                    isError = True
                End If
                If cboAOSupervisor.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih Supervisor CMO <BR/>"
                    isError = True
                End If
                If rbAOSales.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih CMO Sales <BR/>"
                    isError = True
                End If
                If txtAOFirstDate.Text = "" Then
                    lblMessage.Text &= "Harap isi Tanggal Mulai Kerja <BR/>"
                    isError = True
                End If
                If cboCASupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh pilih Supervisor CA <BR/>"
                    isError = True
                End If
            Case "CA"
                If cboCASupervisor.SelectedIndex = 0 Then
                    lblMessage.Text &= "Harap pilih Supervisor CA <BR/>"
                    isError = True
                End If
                If txtAOFirstDate.Text = "" Then
                    lblMessage.Text &= "Harap isi Tanggal Mulai Kerja <BR/>"
                    isError = True
                End If
                If cboAOSupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh pilih Supervisor CMO <BR/>"
                    isError = True
                End If
            Case Else
                If cboAOSupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh pilih Supervisor CMO <BR/>"
                    isError = True
                End If
                If cboCASupervisor.SelectedIndex <> 0 Then
                    lblMessage.Text &= "Tidak boleh pilih Supervisor CA <BR/>"
                    isError = True
                End If
                If txtAOFirstDate.Text = "" Then
                    lblMessage.Text &= "Harap isi Tanggal Mulai Kerja <BR/>"
                    isError = True
                End If
        End Select
        lblMessage.Visible = True
        If Not isError Then
            With customClass
                .strConnection = GetConnectionString
                .BranchId = Me.BranchID
                .EmployeeID = lblID.Text.Trim
                .EmployeeName = txtName.Text.Trim
                .Inisial = txtInisial.Text.Trim
                .Position = cboPosition.SelectedItem.Value.Trim
                .AOLevel = cboAOLevel.SelectedItem.Value.Trim
                .AOSupervisor = cboAOSupervisor.SelectedItem.Value.Trim
                .AOSales = rbAOSales.SelectedItem.Value.Trim

                If txtAOFirstDate.Text.Trim <> "" Then
                    .AOFirstDate = ConvertDate(txtAOFirstDate.Text.Trim)
                Else
                    .AOFirstDate = ""
                End If

                .EmployeeIncentiveCard = cboIncentiveCard.SelectedItem.Value.Trim
                .CASupervisor = cboCASupervisor.SelectedItem.Value.Trim
                .OverduePenaltyCard = cboPenaltyCard.SelectedItem.Value.Trim
                .isActive = CBool(chkisActive.Checked)

                .BankID = UcBankAccount.BankID
                .BankBranch = UcBankAccount.BankBranch
                .AccountNo = UcBankAccount.AccountNo
                .AccountName = UcBankAccount.AccountName
                .BankBranchId = UcBankAccount.BankBranchId
            End With

            With oClassAddress
                .Address = UcAddress.Address.Trim
                .RT = UcAddress.RT.Trim
                .RW = UcAddress.RW.Trim
                .Kelurahan = UcAddress.Kelurahan.Trim
                .Kecamatan = UcAddress.Kecamatan.Trim
                .City = UcAddress.City.Trim
                .ZipCode = UcAddress.ZipCode.Trim
                .AreaPhone1 = UcAddress.AreaPhone1.Trim
                .Phone1 = UcAddress.Phone1.Trim
                .AreaPhone2 = UcAddress.AreaPhone2.Trim
                .Phone2 = UcAddress.Phone2.Trim
                .AreaFax = UcAddress.AreaFax.Trim
                .Fax = UcAddress.Fax.Trim
            End With

            With oClassPersonal
                .MobilePhone = txtMobilePhone.Text.Trim
                .Email = txtEmail.Text.Trim
            End With
            Try
                m_Employee.EditEmployee(customClass, oClassAddress, oClassPersonal)
                ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
                pnlAddEdit.Visible = False
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
            Catch ex As Exception
               showmessage(lblmessage, ex.Message , true)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
            End Try
        End If
    End Sub

#End Region

    Private Sub viewbyID(ByVal EmployeeID As String)
        Dim oEmployee As New Parameter.Employee
        Dim dtEmployee As New DataTable
        Try
            Dim strConnection As String = getConnectionString
            With oEmployee
                .strConnection = getConnectionString()
                .BranchId = Me.BranchID
                .EmployeeID = EmployeeID
            End With
            oEmployee = m_Employee.EmployeeByID(oEmployee)
            dtEmployee = oEmployee.ListData
            txtAOFirstDate.Visible = True
            '------------------
            'isi dari Database
            '------------------
            txtID.Visible = False
            lblID.Visible = True
            lblID.Text = Me.EmployeeID
            txtName.Text = CStr(dtEmployee.Rows(0).Item("EmployeeName")).Trim
            txtInisial.Text = CStr(dtEmployee.Rows(0).Item("InitialName")).Trim
            cboPosition.SelectedIndex = cboPosition.Items.IndexOf(cboPosition.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("EmployeePosition"))))
            Select Case CStr(dtEmployee.Rows(0).Item("EmployeePosition")).Trim
                Case "AO"
                    cboAOLevel.SelectedIndex = cboAOLevel.Items.IndexOf(cboAOLevel.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("AOLevelStatusID"))))
                    cboAOSupervisor.SelectedIndex = cboAOSupervisor.Items.IndexOf(cboAOSupervisor.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("AOSupervisor"))))
                    rbAOSales.SelectedIndex = rbAOSales.Items.IndexOf(rbAOSales.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("AOSalesStatusID"))))
                    'txtAOFirstDate.Enabled = True
                    cboAOLevel.Enabled = True
                    cboAOSupervisor.Enabled = True
                    rbAOSales.Enabled = True
                    cboCASupervisor.Enabled = True
                    cboCASupervisor.Enabled = False
                Case "CA"
                    cboCASupervisor.SelectedIndex = cboCASupervisor.Items.IndexOf(cboCASupervisor.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("CASupervisor"))))
                    cboCASupervisor.Enabled = True
                    cboAOLevel.Enabled = False
                    cboAOSupervisor.Enabled = False
                    rbAOSales.Enabled = False
                    'txtAOFirstDate.Enabled = True
                Case Else
                    cboAOLevel.Enabled = False
                    cboAOSupervisor.Enabled = False
                    rbAOSales.Enabled = False
                    'txtAOFirstDate.Enabled = True
                    cboCASupervisor.Enabled = False
            End Select
            txtAOFirstDate.Text = CStr(dtEmployee.Rows(0).Item("AOFirstDate")).Trim
            cboIncentiveCard.SelectedIndex = cboIncentiveCard.Items.IndexOf(cboIncentiveCard.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("EmployeeIncentiveCardID"))))
            cboPenaltyCard.SelectedIndex = cboPenaltyCard.Items.IndexOf(cboPenaltyCard.Items.FindByValue(CStr(dtEmployee.Rows(0).Item("OverduePenaltyCardID"))))
            UcAddress.Address = CStr(dtEmployee.Rows(0).Item("Address")).Trim
            UcAddress.RT = CStr(dtEmployee.Rows(0).Item("RT")).Trim
            UcAddress.RW = CStr(dtEmployee.Rows(0).Item("RW")).Trim
            UcAddress.Kelurahan = CStr(dtEmployee.Rows(0).Item("Kelurahan")).Trim
            UcAddress.Kecamatan = CStr(dtEmployee.Rows(0).Item("Kecamatan")).Trim
            UcAddress.City = CStr(dtEmployee.Rows(0).Item("City")).Trim
            UcAddress.ZipCode = CStr(dtEmployee.Rows(0).Item("ZipCode")).Trim
            UcAddress.AreaPhone1 = CStr(dtEmployee.Rows(0).Item("AreaPhone1")).Trim
            UcAddress.Phone1 = CStr(dtEmployee.Rows(0).Item("Phone1")).Trim
            UcAddress.AreaPhone2 = CStr(dtEmployee.Rows(0).Item("AreaPhone2")).Trim
            UcAddress.Phone2 = CStr(dtEmployee.Rows(0).Item("Phone2")).Trim
            UcAddress.AreaFax = CStr(dtEmployee.Rows(0).Item("AreaFax")).Trim
            UcAddress.Fax = CStr(dtEmployee.Rows(0).Item("Fax")).Trim
            UcAddress.Style = "Setting"
            UcAddress.BindAddress()
            txtMobilePhone.Text = CStr(dtEmployee.Rows(0).Item("MobilePhone")).Trim
            txtEmail.Text = CStr(dtEmployee.Rows(0).Item("Email")).Trim
            chkisActive.Checked = CBool(dtEmployee.Rows(0).Item("isActive"))
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.EmployeeID = e.Item.Cells(2).Text
        Select Case e.CommandName
            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                    Me.EmployeeID = CType(e.Item.FindControl("lblEmployeeID"), Label).Text.Trim
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    lblMessage.Visible = False
                    UcAddress.ValidatorFalse()
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    FillCombo()
                    viewbyID(Me.EmployeeID)
                    RequiredFieldValidator1.Enabled = False
                    lbl_Sign.Visible = False

                    With UcBankAccount
                        .BindBankAccount()

                        .BankBranchId = CType(e.Item.FindControl("lblBankBranchID"), Label).Text.Trim 
                        .BankName = CType(e.Item.FindControl("lblBankName"), Label).Text.Trim
                        .BankBranch = CType(e.Item.FindControl("lblBankBranchName"), Label).Text.Trim
                        .AccountNo = CType(e.Item.FindControl("lblAccountNo"), Label).Text.Trim
                        .AccountName = CType(e.Item.FindControl("lblAccountName"), Label).Text.Trim
                        .BankID = CType(e.Item.FindControl("lblBankID"), Label).Text.Trim
                        'TODO Bank Account
                        '.setValidatorLookup()
                    End With
                End If
            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                    lblMessage.Visible = False
                    'Me.EmployeeID = e.Item.Cells(2).Text
                    Me.EmployeeID = CType(e.Item.FindControl("lblEmployeeID"), Label).Text.Trim
                    Dim customClass As New Parameter.Employee
                    With customClass
                        .strConnection = getConnectionString
                        .EmployeeID = Me.EmployeeID
                        .BranchId = Me.BranchID
                    End With
                    Try
                        m_Employee.DeleteEmployee(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)
                    Catch ex As Exception
                        showmessage(lblmessage, ex.Message , true)
                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If

            Case "PindahCabang"
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.EmployeeID = CType(e.Item.FindControl("lblEmployeeID"), Label).Text.Trim
                pnlList.Visible = False
                pnlAddEdit.Visible = False
                pnlpindahcabang.Visible = True
        End Select
    End Sub
    Private Sub BtnResetnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetnew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton
        Dim lbEmployeeID As LinkButton
        If e.Item.ItemIndex >= 0 Then
            Me.EmployeeID = CType(e.Item.FindControl("lblEmployeeID"), Label).Text.Trim
            lbEmployeeID = CType(e.Item.FindControl("lnkEmployeeID"), LinkButton)
            lbEmployeeID.Attributes.Add("OnClick", "return OpenWindowEmployee('" & Me.BranchID & "','" & Me.EmployeeID & "')")
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'sementara di comment dulu
        'If cboPosition.SelectedItem.Value.Trim <> "AO" And oAOFirstDate.dateValue <> "" Then
        '    lblMessage.Text = "If position not AO, you cann't fill AOFirstdate"
        '    lblMessage.Visible = True
        '    Exit Sub
        'End If
        Select Case Me.ActionAddEdit
            Case "ADD"
                Add()
            Case "EDIT"
                edit(Me.EmployeeID)
        End Select
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                cookie.Values("branchid") = Me.BranchID
                cookie.Values("branchname") = Me.BranchName
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                cookieNew.Values.Add("branchid", Me.BranchID)
                cookieNew.Values.Add("branchname", Me.BranchName)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("report/Employeereport.aspx")
        End If
    End Sub
    Private Sub BtnSearchnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchnew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim
        If cboPositionSearch.SelectedIndex <> 0 Then
            Me.SearchBy = "C.EmployeePosition='" + cboPositionSearch.SelectedItem.Value.Trim + "'"

            If StrSearchByValue = "" Then
                Me.SortBy = ""
            Else
                Me.SearchBy = Me.SearchBy + " and " + StrSearchBy + " LIKE '%" + StrSearchByValue + "%'"
            End If
        Else
            If StrSearchByValue = "" Then
                Me.SearchBy = "ALL"
                Me.SortBy = ""
            Else
                Me.SearchBy = StrSearchBy + " LIKE '%" + StrSearchByValue + "%'"
            End If
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("Branch.aspx")
    End Sub
    'sementara di comment dulu
    'Private Sub cboPosition_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPosition.SelectedIndexChanged
    '    Select Case cboPosition.SelectedItem.Value.Trim
    '        Case "AO"
    '            cboCASupervisor.Visible = False
    '            cboAOLevel.Visible = True
    '            cboAOSupervisor.Visible = True
    '            rbAOSales.Visible = True
    '            cboCASupervisor.Visible = True
    '        Case "CA"
    '            cboCASupervisor.Visible = True
    '            cboAOLevel.Visible = False
    '            cboAOSupervisor.Visible = False
    '            rbAOSales.Visible = False
    '            oAOFirstDate.Visible = False
    '        Case Else
    '            cboAOLevel.Visible = False
    '            cboAOSupervisor.Visible = False
    '            rbAOSales.Visible = False
    '            oAOFirstDate.Visible = False
    '            cboCASupervisor.Visible = False
    '    End Select
    'End Sub
    Sub BindAdd()
        UcBankAccount.BindBankAccount()
        UcBankAccount.BankID = ""
        UcBankAccount.BankBranch = ""
        UcBankAccount.AccountNo = ""
        UcBankAccount.AccountName = ""
    End Sub

#Region "EDIT"
    Private Sub PindahCabang(ByVal Employeeid As String)
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        pnlpindahcabang.Visible = True
    End Sub
#End Region

    Private Sub btnCancelPindah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelPindah.Click
        InitialDefaultPanel()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub

    Private Sub btnPindahCabang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPindahCabang.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Visible = False

        Dim customClass As New Parameter.Employee
        With customClass
            .strConnection = GetConnectionString()
            .EmployeeID = Me.EmployeeID
            .BranchId = Me.BranchID
            .DestBranchId = oBranch.BranchID.Trim
        End With
        Try
            m_Employee.EmployeePindahCabang(customClass)

            InitialDefaultPanel()
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            ShowMessage(lblMessage, "Berhasil Pindah Cabang ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            BindGridEntity("ALL", "")
        End Try

    End Sub
End Class