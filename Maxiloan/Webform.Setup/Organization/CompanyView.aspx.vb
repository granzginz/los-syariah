﻿'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController

Public Class CompanyView
    Inherits Maxiloan.Webform.WebBased
#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(ViewState("CompanyID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CompanyID") = Value
        End Set
    End Property

    Private Property CompanyType() As String
        Get
            Return CStr(ViewState("CompanyType"))
        End Get
        Set(ByVal Value As String)
            ViewState("CompanyType") = Value
        End Set
    End Property

#End Region
    Dim m_Company As New CompanyController
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "Company"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.CompanyID = Request.QueryString("CompanyID")
                viewbyID(Me.CompanyID, Me.CGID)
                PnlView.Visible = True
                btnCloseNew.Attributes.Add("onclick", "windowClose()")
            End If
        End If
    End Sub
    Private Sub viewbyID(ByVal CompanyID As String, ByVal CGID As String)
        Dim Company As New Parameter.Company
        Dim CompanyList As New Parameter.Company
        Dim dtInsCo As New DataTable
        With Company
            .strConnection = GetConnectionString()
            .CompanyID = Me.CompanyID
            .WhereCond = "CompanyID='" & Me.CompanyID & "'"
            .SortBy = ""
        End With
        Try
            CompanyList = m_Company.ListCompanyByID(Company)
            dtInsCo = CompanyList.ListData
            lblCompanyFullName.Text = CStr(dtInsCo.Rows(0).Item("CompanyFullName"))
            lblCompanyID.Text = CStr(dtInsCo.Rows(0).Item("CompanyID"))
            lblCompanyShortName.Text = CStr(dtInsCo.Rows(0).Item("CompanyShortName"))
            lblCompanyInitialName.Text = CStr(dtInsCo.Rows(0).Item("CompanyInitialName"))
            lblAddress.Text = CStr(dtInsCo.Rows(0).Item("Address"))
            lblRT.Text = CStr(dtInsCo.Rows(0).Item("RT"))
            lblRW.Text = CStr(dtInsCo.Rows(0).Item("RW"))
            lblKelurahan.Text = CStr(dtInsCo.Rows(0).Item("Kelurahan"))
            lblKecamatan.Text = CStr(dtInsCo.Rows(0).Item("Kecamatan"))
            lblCity.Text = CStr(dtInsCo.Rows(0).Item("City"))
            lblZipCode.Text = CStr(dtInsCo.Rows(0).Item("ZipCode"))
            lblAreaPhone1.Text = CStr(dtInsCo.Rows(0).Item("AreaPhone1"))
            lblPhone1.Text = CStr(dtInsCo.Rows(0).Item("Phone1"))
            lblAreaFax.Text = CStr(dtInsCo.Rows(0).Item("AreaFax"))
            lblFax.Text = CStr(dtInsCo.Rows(0).Item("Fax"))
            lblNPWP.Text = CStr(dtInsCo.Rows(0).Item("NPWP"))
            lblTDP.Text = CStr(dtInsCo.Rows(0).Item("TDP"))
            lblSIUP.Text = CStr(dtInsCo.Rows(0).Item("SIUP"))
            lblCPName.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonName"))
            lblCPTitle.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonTitle"))
            lblCPEmail.Text = CStr(dtInsCo.Rows(0).Item("Email"))
            lblCPMobilePhone.Text = CStr(dtInsCo.Rows(0).Item("MobilePhone"))
        Catch ex As Exception
        End Try
    End Sub
End Class