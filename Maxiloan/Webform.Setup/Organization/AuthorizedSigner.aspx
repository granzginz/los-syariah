﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AuthorizedSigner.aspx.vb"
    Inherits="Maxiloan.Webform.Setup.AuthorizedSigner" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DAFTAR AUTHORIZED SIGNER</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="AUTHSIGN" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR AUTHORIZED SIGNER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="PnlGrid" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="3%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BranchFullname" SortExpression="BranchFullname" HeaderText="BRANCH NAME">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle Width="7%" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="DocumentID" SortExpression="DocumentID" HeaderText="DOKUMENT ID">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Signer1" SortExpression="Signer1" HeaderText="SIGNER 1">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Signer2" SortExpression="Signer2" HeaderText="SIGNER 2">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Signer3" SortExpression="Signer3" HeaderText="SIGNER 3">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JabatanSigner1" SortExpression="JabatanSigner1" HeaderText="JABATAN SIGNER 1">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JabatanSigner2" SortExpression="JabatanSigner2" HeaderText="JABATAN SIGNER 2">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JabatanSigner3" SortExpression="JabatanSigner3" HeaderText="JABATAN SIGNER 3">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BranchID" HeaderText="id branch" Visible="FALSE"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue" />
                            <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtPage"
                                Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
                    </asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI AUTHORIZED SIGNER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:DropDownList ID="cboBranch" runat="server" Width="150px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server" Width="150px">
                            <asp:ListItem Value="DocumentID">DOCUMENT ID</asp:ListItem>
                            <asp:ListItem Value="Signer1">SIGNER 1</asp:ListItem>
                            <asp:ListItem Value="Signer2">SIGNER 2</asp:ListItem>
                            <asp:ListItem Value="Signer3">SIGNER 3</asp:ListItem>
                            <asp:ListItem Value="JabatanSigner1">JABATAN SIGNER 1</asp:ListItem>
                            <asp:ListItem Value="JabatanSigner2">JABATAN SIGNER 2</asp:ListItem>
                            <asp:ListItem Value="JabatanSigner3">JABATAN SIGNER 3</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%"  MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch ID</label>
                        <asp:DropDownList ID="cboBranchID" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                            Display="Dynamic" ErrorMessage="Harap Pilih Branch ID" ControlToValidate="cboBranchID"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:Label runat="server" ID="lblBranchID"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Dokument ID</label>
                        <asp:TextBox ID="TxtDkmID" runat="server" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="TxtDkmID" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblSigner" Text="NAMA SIGNER *)" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Signer 1</label>
                        <asp:TextBox ID="TxtSigner1" runat="server" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap isi Nama Signer."
                            ControlToValidate="TxtSigner1" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Signer 2</label>
                        <asp:TextBox ID="TxtSigner2" runat="server" Width="250px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Signer 3</label>
                        <asp:TextBox ID="TxtSigner3" runat="server" Width="250px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblJbtSigner" Text="JABATAN SIGNER *)" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jabatan Signer 1</label>
                        <asp:TextBox ID="TxtJbtSigner1" runat="server" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap Isi Jabatan Signer"
                            ControlToValidate="TxtJbtSigner1" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jabatan Signer 2</label>
                        <asp:TextBox ID="TxtJbtSigner2" runat="server" Width="250px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jabatan Signer 3</label>
                        <asp:TextBox ID="TxtJbtSigner3" runat="server" Width="250px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                        CssClass="small button gray"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonClose" runat="server" CausesValidation="False" Text="Close"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <input id="hdnBranchID" type="hidden" name="hdnBranchID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
