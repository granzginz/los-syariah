﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompanyBOD.aspx.vb" Inherits="Maxiloan.Webform.Setup.CompanyBOD" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Are you sure want to delete this record?"))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowCompany(pCompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWindowBOD(pBODID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; 
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/BODView.aspx?BODID=' + pBODID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Style="z-index: 101; left: 16px; top: 8px" runat="server"
        Font-Italic="True" Font-Names="Verdana" Width="624px"  font-name="Verdana"
        ForeColor="Red">*) Mandatory</asp:Label><br/>
    <asp:Panel ID="pnlList" Style="z-index: 101" Width="99.69%" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopiabu">
                <td class="tdtopi" align="center">
                    List of BOD
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr align="center">
                <td width="100%">
                    <asp:DataGrid ID="dtg" runat="server" Width="100%" CssClass="tablegrid" AutoGenerateColumns="False"
                        AllowSorting="True" OnSortCommand="Sorting" HorizontalAlign="Left" CellSpacing="1"
                        CellPadding="3" BorderWidth="0px">
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle Height="20px" CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BODID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="BODID" HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBODID" runat="server" Enabled="True" CommandName="BODView"
                                        CausesValidation="False">
												<%# Container.dataitem("BODID") %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BODID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="BODNAME" SortExpression="BODNAME" HeaderText="NAME">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BODTITLE" SortExpression="BODTITLE" HeaderText="TITLE">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table class="nav_tbl" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td class="nav_tbl_td1">
                    <table class="nav_command_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="BtnAdd" runat="server" CausesValidation="False" ImageUrl="../../Images/ButtonAdd.gif">
                                </asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imgPrint" runat="server" ImageUrl="../../Images/ButtonPrint.gif"
                                    Enabled="true"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbBack" runat="server" ImageUrl="../../images/buttonback.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="nav_tbl_td2">
                    <table class="nav_page_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                                </asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                                </asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                                </asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                                </asp:ImageButton>
                            </td>
                            <td>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnGoPage" runat="server" text="Go" class="small buttongo blue"/>
                                    
                            </td>
                            <asp:RangeValidator ID="rgvGo" runat="server" 
                                 Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                                MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general" ></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                                 Display="Dynamic" ErrorMessage="Page No. is not valid" ControlToValidate="txtGopage" CssClass="validator_general" ></asp:RequiredFieldValidator></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav_totpage" colspan="2">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </td>
            </tr>
        </table>
        <br/>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopiabu">
                <td class="tdtopikiri" width="10" height="20">
                </td>
                <td class="tdtopi" align="center">
                    Find BOD
                </td>
                <td class="tdtopikanan" width="10">
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" style="height: 19px" width="25%">
                    Company ID
                </td>
                <td class="tdganjil" style="height: 19px">
                    <asp:LinkButton ID="lnkCompanyID" runat="server" Width="528px"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 18px" width="25%">
                    Company Name
                </td>
                <td class="tdganjil" style="height: 18px">
                    <asp:Label ID="lblCompanyName" runat="server" Width="528px" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr class="tdganjil">
                <td class="tdgenap" width="25%">
                    Find By
                </td>
                <td class="tdganjil">
                    <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                        <asp:ListItem Value="BODID" Selected="True">BOD ID</asp:ListItem>
                        <asp:ListItem Value="BODName">BOD Name</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr align="left">
                <td>
                    <asp:ImageButton ID="BtnSearch" runat="server" ImageUrl="../../Images/ButtonSearch.gif"
                        CausesValidation="False"></asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="BtnReset" runat="server" ImageUrl="../../Images/ButtonReset.gif"
                        CausesValidation="False"></asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" Style="z-index: 102" Width="100%" runat="server" HorizontalAlign="Center">
        <table cellspacing="3" cellpadding="0" width="95%" border="0">
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" Style="z-index: 101; left: 16px; top: 8px" runat="server"
                        ForeColor="Red" font-name="Verdana"  Width="112px" Font-Names="Verdana"
                        Font-Italic="True">*) Mandatory</asp:Label>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopiabu">
                <td class="tdtopi" align="center">
                    BOD&nbsp;-
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="tablegrid" id="table7" cellspacing="1" cellpadding="2" width="95%"
            align="center" border="0">
            <tr class="tdganjil">
                <td class="tdgenap" style="height: 9px" width="25%">
                    BOD ID&nbsp;<font color="red">*)</font>
                </td>
                <td class="tdganjil" style="height: 9px">
                    <asp:TextBox ID="txtID" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                    <asp:Label ID="lblID" runat="server" Width="129px" Visible="False"></asp:Label>
                    <div style="display: inline; width: 16px; color: red; height: 15px" ms_positioning="FlowLayout">
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                        ControlToValidate="txtID" ErrorMessage="You must fill BOD ID" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="tdganjil">
                <td class="tdgenap" width="25%">
                    Name&nbsp;<font color="red">*)</font>
                </td>
                <td class="tdganjil" style="height: 22px">
                    <asp:TextBox ID="txtName" runat="server" Width="200px"  MaxLength="50"></asp:TextBox>
                    <div style="display: inline; width: 16px; color: red; height: 15px" ms_positioning="FlowLayout">
                    </div>
                    <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" ControlToValidate="txtName"
                        ErrorMessage="You must fill Name" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 23px" width="25%">
                    Title&nbsp;<font color="red">*)</font>
                </td>
                <td class="tdganjil" style="height: 23px">
                    <asp:TextBox ID="txtTitle" runat="server" Width="200px"  MaxLength="100"></asp:TextBox>
                    <div style="display: inline; width: 16px; color: red; height: 15px" ms_positioning="FlowLayout">
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="184px"
                        ControlToValidate="txtTitle" ErrorMessage="You must fill Title" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="height: 22px" width="25%">
                    NPWP
                </td>
                <td class="tdganjil" style="height: 22px">
                    <asp:TextBox ID="txtNPWP" runat="server" Width="200px"  MaxLength="30"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="25%">
                    ID Type
                </td>
                <td class="tdganjil" style="height: 22px">
                    <asp:DropDownList ID="cboIDType" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="25%">
                    ID Number
                </td>
                <td class="tdganjil" style="height: 22px">
                    <asp:TextBox ID="txtIDNumber" runat="server" Width="200px"  MaxLength="20"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table class="tablegrid" id="table16" cellspacing="1" cellpadding="2" width="95%">
            <tr>
                <td class="tdtopi" style="height: 17px">
                    ADDRESS
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%">
            <tr>
                <td>
                    <uc1:uccompanyaddress id="UcCompanyAddress" runat="server"></uc1:uccompanyaddress>
                </td>
            </tr>
        </table>
        <table class="tablegrid" id="table3" cellspacing="1" cellpadding="2" width="95%"
            align="center" border="0">
            <tr class="tdganjil">
                <td class="tdgenap" style="height: 9px" width="25%">
                    Email
                </td>
                <td class="tdganjil" style="height: 9px">
                    <asp:TextBox ID="txtEmail" runat="server" Width="200px"  MaxLength="30"></asp:TextBox>
                </td>
            </tr>
            <tr class="tdganjil">
                <td class="tdgenap" width="25%">
                    Mobile Phone
                </td>
                <td class="tdganjil" style="height: 22px">
                    <asp:TextBox ID="txtMobilePhone" runat="server" Width="200px" 
                        MaxLength="20"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%">
            <tr>
                <td>
                </td>
            </tr>
        </table>
        <table id="tableAddSave" style="height: 19px" cellspacing="0" cellpadding="0" width="95%"
            align="center" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbSave" runat="server" ImageUrl="../../Images/ButtonSave.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="../../Images/ButtonCancel.gif"
                        CausesValidation="False"></asp:ImageButton>&nbsp; <a href="javascript:history.back();">
                        </a>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
