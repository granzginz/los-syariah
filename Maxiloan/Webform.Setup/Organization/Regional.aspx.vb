﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class Regional
    Inherits Maxiloan.Webform.WebBased



#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Area
    Private oController As New AreaController

    Protected WithEvents oContactPerson As UcContactPerson
    Protected WithEvents oCompanyAddress As UcCompanyAddress
    Protected WithEvents oViewContact As UcViewContactPerson
    Protected WithEvents oViewAddress As UcViewAddress
    Protected WithEvents txtApprovalLimit As ucNumberFormat
#End Region

#Region "Properti"
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property SelectItem() As String
        Get
            Return (CType(ViewState("SelectItem"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectItem") = Value
        End Set
    End Property

    Private Property SelectValue() As String
        Get
            Return (CType(ViewState("SelectValue"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectValue") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "Regional"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                lblMessage.Text = ""
                DoBind(Me.SortBy, Me.SortBy)
                AddScriptToDtgEnt()
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")
        If UBound(strBranch) > 0 Then

            ShowMessage(lblMessage, "Area Tidak bisa diganti.....", True)
            btnSearch.Enabled = False
            pnlSearch.Visible = True
            pnlDatagrid.Visible = False
            pnlAdd.Visible = False
        Else
            pnlAdd.Visible = False
            pnlSearch.Visible = True
            pnlDatagrid.Visible = False
            pnlView.Visible = False
            lblMessage.Text = ""
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .FormID = Me.FormID
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.AreaList(oCustomClass)

        DtUserList = oCustomClass.ListArea
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        ChgStatusInDgr()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlAdd.Visible = False
        pnlView.Visible = False
        If dtgEntity.Items.Count > 0 Then
            btnPrint.Enabled = True
        ElseIf dtgEntity.Items.Count = 0 Then
            With lblMessage
                .Visible = True
                .Text = "Data tidak ditemukan ....."
            End With
            btnPrint.Enabled = False
        End If
    End Sub

    Public Sub EnabledPnlAdd(ByVal Read As Boolean, ByVal Style As BorderStyle)
        txtAreaID.ReadOnly = Read
        txtAreaID.BorderStyle = Style
        txtAreaName.ReadOnly = Read
        txtAreaName.BorderStyle = Style
        txtInitName.ReadOnly = Read
        txtInitName.BorderStyle = Style
        txtManager.ReadOnly = Read
        txtManager.BorderStyle = Style
        oContactPerson.BorderContactNone(Read, Style)
        oCompanyAddress.BorderNone(Read, Style)
    End Sub

    Public Sub Reset()
        txtAreaID.Text = ""
        txtAreaName.Text = ""
        txtInitName.Text = ""
        txtManager.Text = ""
        With oCompanyAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kelurahan = ""
            .Kecamatan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Setting"
            .BindAddress()
            .ValidatorTrue()
        End With

        With oContactPerson
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .Email = ""
            .MobilePhone = ""
            .BindContacPerson()
            .EnabledContactPerson()
        End With
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Text = ""
        Me.PageState = currentPage
        txtPage.Text = CType(Me.PageState, String)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region


#Region "DataGridCommand"
    Private Sub ChgStatusInDgr()

    End Sub

    Private Sub AddScriptToDtgEnt()
        Dim IRow As Int32
        For IRow = 0 To dtgEntity.Items.Count - 1
            Dim lblAddress, lblRT, lblRW, lblLurah, lblCamat, lblCity, lblZipCode As Label
            Dim imgDele As ImageButton
            imgDele = CType(dtgEntity.Items(IRow).FindControl("imgDele"), ImageButton)
            imgDele.Attributes.Add("onclick", "return DeleteConfirm();")
            lblAddress = CType(dtgEntity.Items(IRow).FindControl("lblAddress"), Label)
            lblRT = CType(dtgEntity.Items(IRow).FindControl("lblRT"), Label)
            lblRW = CType(dtgEntity.Items(IRow).FindControl("lblRW"), Label)
            lblLurah = CType(dtgEntity.Items(IRow).FindControl("lblLurah"), Label)
            lblCamat = CType(dtgEntity.Items(IRow).FindControl("lblCamat"), Label)
            lblCity = CType(dtgEntity.Items(IRow).FindControl("lblCity"), Label)
            lblZipCode = CType(dtgEntity.Items(IRow).FindControl("lblZipCode"), Label)
            lblAddress.Text = lblAddress.Text.Trim
            lblRT.Text = lblRT.Text.Trim
            lblRW.Text = lblRW.Text.Trim
            lblLurah.Text = lblLurah.Text.Trim
            lblCamat.Text = lblCamat.Text.Trim
            lblCity.Text = lblCity.Text.Trim
            lblZipCode.Text = lblZipCode.Text.Trim
        Next
    End Sub

    Public Sub dtgEntity_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Dim lnkID As LinkButton
        Dim lblRegionalName, lblAddress, lblRT, lblRW, lblLurah, lblCamat As Label
        Dim lblCity, lblZipCode, lblRegionalAreaPhone1, lblRegionalPhone1, lblContact As Label
        Dim lblManager, lblInitName As Label
        Dim lblRegionalAreaPhone2, lblRegionalPhone2 As Label
        Dim lblRegionalAreaFax, lblRegionalFax As Label
        Dim lblContactPersonJobTitle, lblContactPersonEmail, lblContactPersonHP, lblApprovalLimit As Label


        lnkID = CType(e.Item.FindControl("lnkID"), LinkButton)
        LAreaID.Visible = False
        lblRegionalName = CType(e.Item.FindControl("lblRegionalName"), Label)
        lblAddress = CType(e.Item.FindControl("lblAddress"), Label)
        lblRT = CType(e.Item.FindControl("lblRT"), Label)
        lblRW = CType(e.Item.FindControl("lblRW"), Label)
        lblLurah = CType(e.Item.FindControl("lblLurah"), Label)
        lblCamat = CType(e.Item.FindControl("lblCamat"), Label)
        lblCity = CType(e.Item.FindControl("lblCity"), Label)
        lblZipCode = CType(e.Item.FindControl("lblZipCode"), Label)
        lblRegionalAreaPhone1 = CType(e.Item.FindControl("lblRegionalAreaPhone1"), Label)
        lblRegionalPhone1 = CType(e.Item.FindControl("lblRegionalPhone1"), Label)
        lblContact = CType(e.Item.FindControl("lblContact"), Label)
        lblManager = CType(e.Item.FindControl("lblManager"), Label)
        lblInitName = CType(e.Item.FindControl("lblInitName"), Label)
        lblRegionalAreaPhone2 = CType(e.Item.FindControl("lblRegionalAreaPhone2"), Label)
        lblRegionalPhone2 = CType(e.Item.FindControl("lblRegionalPhone2"), Label)
        lblRegionalFax = CType(e.Item.FindControl("lblRegionalFax"), Label)
        lblRegionalAreaFax = CType(e.Item.FindControl("lblRegionalAreaFax"), Label)
        lblContactPersonJobTitle = CType(e.Item.FindControl("lblContactPersonJobTitle"), Label)
        lblContactPersonEmail = CType(e.Item.FindControl("lblContactpersonEmail"), Label)
        lblContactPersonHP = CType(e.Item.FindControl("lblContactPersonHP"), Label)
        lblApprovalLimit = CType(e.Item.FindControl("lblApprovalLimit"), Label)


        Select Case e.CommandName
            Case "edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    Me.Mode = "edit"
                    Reset()
                    EnabledPnlAdd(False, BorderStyle.NotSet)
                    oCompanyAddress.Style = "Setting"

                    oCustomClass.AreaID = lnkID.Text
                    oCustomClass.strConnection = GetConnectionString()
                    oCustomClass = oController.AreaInfo(oCustomClass)

                    With txtAreaID
                        .Text = lnkID.Text
                        .ReadOnly = True
                        .BorderStyle = BorderStyle.None
                    End With
                    txtAreaName.Text = lblRegionalName.Text.Trim
                    txtInitName.Text = lblInitName.Text.Trim
                    txtManager.Text = lblManager.Text.Trim

                    With oCompanyAddress
                        .Address = lblAddress.Text.Trim
                        .RT = lblRT.Text.Trim
                        .RW = lblRW.Text.Trim
                        .Kelurahan = lblLurah.Text.Trim
                        .Kecamatan = lblCamat.Text.Trim
                        .City = lblCity.Text.Trim
                        .ZipCode = lblZipCode.Text.Trim
                        .AreaPhone1 = lblRegionalAreaPhone1.Text.Trim
                        .Phone1 = lblRegionalPhone1.Text.Trim
                        .AreaPhone2 = lblRegionalAreaPhone2.Text.Trim
                        .Phone2 = lblRegionalPhone2.Text.Trim
                        .AreaFax = lblRegionalAreaFax.Text.Trim
                        .Fax = lblRegionalFax.Text.Trim
                        .BindAddress()
                    End With
                    With oContactPerson
                        .ContactPerson = lblContact.Text.Trim
                        .ContactPersonTitle = lblContactPersonJobTitle.Text.Trim
                        .Email = lblContactPersonEmail.Text.Trim
                        .MobilePhone = lblContactPersonHP.Text.Trim
                        .BindContacPerson()
                    End With
                    pnlSearch.Visible = False
                    pnlDatagrid.Visible = False
                    pnlAdd.Visible = True
                    pnlView.Visible = False
                    btnOk.Visible = False
                    txtApprovalLimit.Text = lblApprovalLimit.Text.Trim
                    lblJudul.Text = "EDIT"
                End If
            Case "delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    lnkID = CType(e.Item.FindControl("lnkID"), LinkButton)
                    With oCustomClass
                        .FormID = "Regional"
                        .strConnection = GetConnectionString()
                        .AreaID = lnkID.Text
                    End With
                    Dim err As String = oController.DeleteArea(oCustomClass)
                    If err <> "" Then
                        ShowMessage(lblMessage, err, True)
                    Else
                        DoBind(Me.SearchBy, Me.SortBy)
                        ShowMessage(lblMessage, "Data Berhasil diHapus", False)
                    End If
                    AddScriptToDtgEnt()
                End If
            Case "id"
                lnkID = CType(e.Item.FindControl("lnkID"), LinkButton)
                lblRegionalName = CType(e.Item.FindControl("lblRegionalName"), Label)
                oCustomClass.AreaID = lnkID.Text
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass = oController.AreaInfo(oCustomClass)
                lblID.Text = lnkID.Text.Trim
                txtAreaName.Text = lblRegionalName.Text.Trim
                txtInitName.Text = lblInitName.Text.Trim
                txtManager.Text = lblManager.Text.Trim
                With oViewAddress
                    .Address = lblAddress.Text.Trim
                    .RT = lblRT.Text.Trim
                    .RW = lblRW.Text.Trim
                    .Kelurahan = lblLurah.Text.Trim
                    .Kecamatan = lblCamat.Text.Trim
                    .City = lblCity.Text.Trim
                    .ZipCode = lblZipCode.Text.Trim
                    .AreaPhone1 = lblRegionalAreaPhone1.Text.Trim
                    .Phone1 = lblRegionalPhone1.Text.Trim
                    .AreaPhone2 = lblRegionalAreaPhone2.Text.Trim
                    .Phone2 = lblRegionalPhone2.Text.Trim
                    .AreaFax = lblRegionalAreaFax.Text.Trim
                    .Fax = lblRegionalFax.Text.Trim
                End With
                With oViewContact
                    .ContactPerson = lblContact.Text.Trim
                    .ContactPersonTitle = lblContactPersonJobTitle.Text.Trim
                    .Email = lblContactPersonEmail.Text.Trim
                    .MobilePhone = lblContactPersonHP.Text.Trim
                End With
                pnlSearch.Visible = False
                pnlDatagrid.Visible = False
                pnlAdd.Visible = False
                pnlView.Visible = True
                btnOk.Visible = True
        End Select
    End Sub



    Public Sub dtgEntity_SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgEntity.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
        AddScriptToDtgEnt()
    End Sub
#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.SelectItem = cboSearch.SelectedValue
            Me.SelectValue = txtSearch.Text
            Me.SearchBy = Me.SelectItem.Trim + " like'%" + Me.SelectValue + "%'"
            ' Me.CmdWhere = "Assettypeid = '" + cboFilterBy.SelectedValue.Trim + "' and " + cboSearchBy.SelectedValue + " LIKE '%" + txtSearchBy.Text.Trim + "%'"
        End If
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlAdd.Visible = False
        pnlView.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
        AddScriptToDtgEnt()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        lblMessage.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        txtSearch.Text = ""
        pnlAdd.Visible = True
        pnlDatagrid.Visible = False
        pnlAdd.Visible = False
        pnlView.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
        txtPage.Text = "1"
        lblPage.Text = txtPage.Text
        currentPage = CInt(lblPage.Text)
        cboSearch.SelectedIndex = 0
        AddScriptToDtgEnt()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        pnlAdd.Visible = False
        pnlView.Visible = False
        txtPage.Text = "1"
        lblPage.Text = txtPage.Text
        currentPage = CInt(lblPage.Text)
        cboSearch.SelectedIndex = 0
        AddScriptToDtgEnt()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            Me.Mode = "add"
            Reset()
            EnabledPnlAdd(False, BorderStyle.NotSet)

            pnlSearch.Visible = False
            pnlDatagrid.Visible = False
            pnlAdd.Visible = True
            pnlView.Visible = False

            txtAreaName.Text = ""
            txtApprovalLimit.Text = 0
            LAreaID.Visible = True
            txtManager.Text = ""
            txtInitName.Text = ""
            oCompanyAddress.Style = "Setting"
            btnSave.Visible = True
            btnCancel.Visible = True
            lblJudul.Text = "ADD"
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        pnlAdd.Visible = False
        pnlView.Visible = False
        AddScriptToDtgEnt()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not IsNumeric(txtAreaID.Text) Then
            With lblMessage
                .Text = "ID Area Salah"
                .Visible = True
            End With
            Exit Sub
        End If
        'If CInt(txtAreaID.Text) < 200 Or CInt(txtAreaID.Text) > 299 Then
        '    With lblMessage
        '        .Text = "ID Area Salah, Harus diantara 200-299"
        '        .Visible = True
        '    End With
        '    Exit Sub
        'End If
        Select Case Me.Mode
            Case "edit"
                oCompanyAddress.BindAddress()
                oContactPerson.BindContacPerson()

                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                pnlAdd.Visible = False
                With oCustomClass
                    .FormID = "Regional"
                    .strConnection = GetConnectionString()
                    .AreaID = txtAreaID.Text
                    .AreaFullName = txtAreaName.Text
                    .AreaInitialName = txtInitName.Text
                    .AreaManager = txtManager.Text
                    .AreaAddress = oCompanyAddress.Address
                    .AreaRT = oCompanyAddress.RT
                    .AreaRW = oCompanyAddress.RW
                    .AreaKelurahan = oCompanyAddress.Kelurahan
                    .AreaKecamatan = oCompanyAddress.Kecamatan
                    .AreaCity = oCompanyAddress.City
                    .AreaZipCode = oCompanyAddress.ZipCode
                    .AreaAreaPhone1 = oCompanyAddress.AreaPhone1
                    .AreaPhone1 = oCompanyAddress.Phone1
                    .AreaAreaPhone2 = oCompanyAddress.AreaPhone2
                    .AreaPhone2 = oCompanyAddress.Phone2
                    .AreaAreaFax = oCompanyAddress.AreaFax
                    .AreaFax = oCompanyAddress.Fax
                    .ContactPersonName = oContactPerson.ContactPerson
                    .ContactPersonJobTitle = oContactPerson.ContactPersonTitle
                    .ContactPersonEmail = oContactPerson.Email
                    .ContactPersonHP = oContactPerson.MobilePhone
                    .LoginId = Me.Loginid
                    .ApprovalLimit = txtApprovalLimit.Text
                End With
                Dim err As String = oController.UpdateArea(oCustomClass)

                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                    pnlSearch.Visible = False
                    pnlAdd.Visible = True
                    pnlDatagrid.Visible = False
                    pnlView.Visible = False
                Else
                    DoBind(Me.SearchBy, Me.SortBy)
                    ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
                End If
            Case "add"
                oCompanyAddress.BindAddress()
                oContactPerson.BindContacPerson()

                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                pnlAdd.Visible = False
                With oCustomClass
                    .FormID = "Regional"
                    .strConnection = GetConnectionString()
                    .AreaID = txtAreaID.Text
                    .AreaFullName = txtAreaName.Text
                    .AreaInitialName = txtInitName.Text
                    .AreaManager = txtManager.Text
                    .AreaAddress = oCompanyAddress.Address
                    .AreaRT = oCompanyAddress.RT
                    .AreaRW = oCompanyAddress.RW
                    .AreaKelurahan = oCompanyAddress.Kelurahan
                    .AreaKecamatan = oCompanyAddress.Kecamatan
                    .AreaCity = oCompanyAddress.City
                    .AreaZipCode = oCompanyAddress.ZipCode
                    .AreaAreaPhone1 = oCompanyAddress.AreaPhone1
                    .AreaPhone1 = oCompanyAddress.Phone1
                    .AreaAreaPhone2 = oCompanyAddress.AreaPhone2
                    .AreaPhone2 = oCompanyAddress.Phone2
                    .AreaAreaFax = oCompanyAddress.AreaFax
                    .AreaFax = oCompanyAddress.Fax
                    .ContactPersonName = oContactPerson.ContactPerson
                    .ContactPersonJobTitle = oContactPerson.ContactPersonTitle
                    .ContactPersonEmail = oContactPerson.Email
                    .ContactPersonHP = oContactPerson.MobilePhone
                    .LoginId = Me.Loginid
                    .ApprovalLimit = txtApprovalLimit.Text.Trim
                End With
                Dim err As String = oController.SaveArea(oCustomClass)

                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                    pnlSearch.Visible = False
                    pnlAdd.Visible = True
                    pnlDatagrid.Visible = False
                    pnlView.Visible = False
                Else
                    DoBind(Me.SearchBy, Me.SortBy)
                    ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
                End If
        End Select
        cboSearch.SelectedIndex = 0
        txtPage.Text = "1"
        lblPage.Text = txtPage.Text
        currentPage = CInt(lblPage.Text)
        AddScriptToDtgEnt()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("../Organization/Report/AreaReport.aspx")
        End If
    End Sub


End Class