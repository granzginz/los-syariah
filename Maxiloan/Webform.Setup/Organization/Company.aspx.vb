﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine

Imports Maxiloan.cbse
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class Company
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcCompanyAddress As UcCompanyAddress
    Protected WithEvents UcContactPerson As UcContactPerson
#Region " Private Const "
    Dim m_Company As New CompanyController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property isDuplicate() As String
        Get
            Return CStr(viewstate("isDuplicate"))
        End Get
        Set(ByVal Value As String)
            viewstate("isDuplicate") = Value
        End Set
    End Property

    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("CompanyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyID") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("CompanyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyName") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "Company"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)
                Me.isDuplicate = "N"
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCompany As New Parameter.Company

        With oCompany
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCompany = m_Company.ListCompany(oCompany)

        With oCompany
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCompany.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            BtnPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            BtnPrint.Enabled = True
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub ClearAddForm()

        txtID.Text = ""
        txtName.Text = ""
        txtShortName.Text = ""
        txtInitialName.Text = ""

        lblID.Visible = False
        txtID.Visible = True

        txtNPWP.Text = ""
        txtTDP.Text = ""
        txtSIUP.Text = ""

        With UcCompanyAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Setting"
            .BindAddress()
        End With

        With UcContactPerson
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .MobilePhone = ""
            .Email = ""
            .EnabledContactPerson()
            .BindContacPerson()
        End With

    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub BtnSearchNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchNew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + "='" + StrSearchByValue + "'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub viewbyID(ByVal Companyid As String)
        Dim oCompany As New Parameter.Company
        Dim dtCompany As New DataTable

        Try
            Dim strConnection As String = getConnectionString

            With oCompany
                .strConnection = getConnectionString()
                .CompanyID = Companyid
                .SortBy = ""
                .WhereCond = "CompanyID='" & Companyid & "'"
            End With

            oCompany = m_Company.ListCompanyByID(oCompany)
            dtCompany = oCompany.ListData

            'Isi semua field Edit Action
            txtID.Visible = False
            lblID.Visible = True
            lblID.Text = Companyid
            txtName.Text = CStr(dtCompany.Rows(0).Item("CompanyFullName")).Trim
            txtShortName.Text = CStr(dtCompany.Rows(0).Item("CompanyShortName")).Trim
            txtInitialName.Text = CStr(dtCompany.Rows(0).Item("CompanyInitialName")).Trim

            With UcCompanyAddress
                .Address = CStr(dtCompany.Rows(0).Item("Address")).Trim
                .RT = CStr(dtCompany.Rows(0).Item("RT")).Trim
                .RW = CStr(dtCompany.Rows(0).Item("RW")).Trim
                .Kelurahan = CStr(dtCompany.Rows(0).Item("Kelurahan")).Trim
                .Kecamatan = CStr(dtCompany.Rows(0).Item("Kecamatan")).Trim
                .City = CStr(dtCompany.Rows(0).Item("City")).Trim
                .ZipCode = CStr(dtCompany.Rows(0).Item("ZipCode")).Trim
                .AreaPhone1 = CStr(dtCompany.Rows(0).Item("AreaPhone1")).Trim
                .Phone1 = CStr(dtCompany.Rows(0).Item("Phone1")).Trim
                .AreaPhone2 = CStr(dtCompany.Rows(0).Item("AreaPhone2")).Trim
                .Phone2 = CStr(dtCompany.Rows(0).Item("Phone2")).Trim
                .AreaFax = CStr(dtCompany.Rows(0).Item("AreaFax")).Trim
                .Fax = CStr(dtCompany.Rows(0).Item("Fax")).Trim
                .Style = "Setting"
                .BindAddress()
            End With

            With UcContactPerson
                .ContactPerson = CStr(dtCompany.Rows(0).Item("ContactPersonName")).Trim
                .ContactPersonTitle = CStr(dtCompany.Rows(0).Item("ContactPersonTitle")).Trim
                .MobilePhone = CStr(dtCompany.Rows(0).Item("MobilePhone")).Trim
                .Email = CStr(dtCompany.Rows(0).Item("Email")).Trim
                .EnabledContactPerson()
                .BindContacPerson()
            End With

            txtNPWP.Text = CStr(dtCompany.Rows(0).Item("NPWP")).Trim
            txtTDP.Text = CStr(dtCompany.Rows(0).Item("TDP")).Trim
            txtSIUP.Text = CStr(dtCompany.Rows(0).Item("SIUP")).Trim

            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception

        End Try

    End Sub

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.Company
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString
            .CompanyID = txtID.Text.Trim
            .CompanyFullName = txtName.Text.Trim
            .CompanyShortName = txtShortName.Text.Trim
            .CompanyInitialName = txtInitialName.Text.Trim
            .NPWP = txtNPWP.Text.Trim
            .TDP = txtTDP.Text.Trim
            .SIUP = txtSIUP.Text.Trim
        End With

        With oClassAddress
            .Address = UcCompanyAddress.Address
            .RT = UcCompanyAddress.RT
            .RW = UcCompanyAddress.RW
            .Kelurahan = UcCompanyAddress.Kelurahan
            .Kecamatan = UcCompanyAddress.Kecamatan
            .City = UcCompanyAddress.City
            .ZipCode = UcCompanyAddress.ZipCode
            .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
            .Phone1 = UcCompanyAddress.Phone1.Trim
            .AreaPhone2 = UcCompanyAddress.AreaPhone1.Trim
            .Phone2 = UcCompanyAddress.Phone2.Trim
            .AreaFax = UcCompanyAddress.AreaFax.Trim
            .Fax = UcCompanyAddress.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle
            .MobilePhone = UcContactPerson.MobilePhone
            .Email = UcContactPerson.Email
        End With

        Try
            m_Company.AddCompany(customClass, oClassAddress, oClassPersonal)
       ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
            Me.isDuplicate = "N"
        Catch ex As Exception
          showmessage(lblmessage, ex.Message , true)
            Me.isDuplicate = "Y"
        End Try
    End Sub
#End Region

#Region "EDIT"
    Private Sub edit(ByVal Companyid As String)
        Dim customClass As New Parameter.Company
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString
            .CompanyID = Companyid.Trim
            .CompanyFullName = txtName.Text.Trim
            .CompanyShortName = txtShortName.Text.Trim
            .CompanyInitialName = txtInitialName.Text.Trim
            .NPWP = txtNPWP.Text.Trim
            .TDP = txtTDP.Text.Trim
            .SIUP = txtSIUP.Text.Trim
        End With

        With oClassAddress
            .Address = UcCompanyAddress.Address
            .RT = UcCompanyAddress.RT
            .RW = UcCompanyAddress.RW
            .Kelurahan = UcCompanyAddress.Kelurahan
            .Kecamatan = UcCompanyAddress.Kecamatan
            .City = UcCompanyAddress.City

            .ZipCode = UcCompanyAddress.ZipCode.Trim
            .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
            .Phone1 = UcCompanyAddress.Phone1.Trim
            .AreaPhone2 = UcCompanyAddress.AreaPhone2.Trim
            .Phone2 = UcCompanyAddress.Phone2.Trim
            .AreaFax = UcCompanyAddress.AreaFax
            .Fax = UcCompanyAddress.Fax
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle
            .MobilePhone = UcContactPerson.MobilePhone
            .Email = UcContactPerson.Email
        End With

        Try
            m_Company.EditCompany(customClass, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
        Catch ex As Exception
            showmessage(lblmessage, ex.Message , true)
        End Try
    End Sub

#End Region

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Select Case e.CommandName
            Case "BOD"
                Me.CompanyID = e.Item.Cells(2).Text
                Me.CompanyName = e.Item.Cells(4).Text
                Response.Redirect("CompanyBOD.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName)

            Case "COMMISIONER"
                Me.CompanyID = e.Item.Cells(2).Text
                Me.CompanyName = e.Item.Cells(4).Text
                Response.Redirect("CompanyCommisioner.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName)

            Case "CompanyView"
                Me.CompanyID = e.Item.Cells(2).Text
                Me.CompanyName = e.Item.Cells(4).Text

            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.CompanyID = e.Item.Cells(2).Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID(Me.CompanyID)
                End If

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.CompanyID = e.Item.Cells(2).Text
                    Dim customClass As New Parameter.Company
                    With customClass
                        .strConnection = getConnectionString
                        .CompanyID = Me.CompanyID
                    End With

                    Try
                        m_Company.DeleteCompany(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)

                    Catch ex As Exception
                   showmessage(lblmessage, ex.Message , true)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub

    Private Sub BtnResetNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetNew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton
        Dim lbCompanyID As Label
        Dim hyCompanyFullName As LinkButton

        If e.Item.ItemIndex >= 0 Then
            Me.CompanyID = e.Item.Cells(2).Text

            lbCompanyID = CType(e.Item.FindControl("lnkCompanyID"), Label)
            hyCompanyFullName = CType(e.Item.FindControl("HyCompanyfullName"), LinkButton)

            hyCompanyFullName.Attributes.Add("OnClick", "return OpenWindowCompany('" & Me.CompanyID & "')")

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()

                If Me.isDuplicate = "N" Then
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                    pnlList.Visible = True
                    pnlAddEdit.Visible = False
                Else
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                End If

            Case "EDIT"
                PanelAllFalse()
                edit(Me.CompanyID)
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
        End Select
    End Sub

    Private Function OpenViewCompany(ByVal pCompanyID As String) As String
        Return "javascript:OpenWindowCompany('" & pCompanyID & "')"
    End Function

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If

            Response.Redirect("report/companyreport.aspx")
        End If
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("Company.aspx")
    End Sub
End Class