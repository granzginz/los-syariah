﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
#End Region

Public Class AuthorizedSigner

    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New AuthorizedSignerController
    Private oLoginController As New LoginController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"

    'Private Property lblBranchID() As String
    '    Get
    '        Return CType(ViewState("lblBranchID"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("lblBranchID") = Value
    '    End Set
    'End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

#End Region




#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub

    Protected Sub imgbtnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "AUTHSIGN"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtPage.Text = "1"
                Me.Sort = "DocumentID ASC"
                Me.CmdWhere = "All"
                BindGridEntity(Me.CmdWhere)
                BindBranch()
            End If
        End If

    End Sub

    Private Sub DefaultPanel()
        PnlGrid.Visible = True
        PnlAddEdit.Visible = False
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oAuthorizedSigner As New Parameter.AuthorizedSigner
        DefaultPanel()

        oAuthorizedSigner.strConnection = GetConnectionString()
        oAuthorizedSigner.WhereCond = cmdWhere
        oAuthorizedSigner.CurrentPage = currentPage
        oAuthorizedSigner.PageSize = pageSize
        oAuthorizedSigner.SortBy = Me.Sort
        oAuthorizedSigner = m_controller.GetAuthorizedSigner(oAuthorizedSigner)

        If Not oAuthorizedSigner Is Nothing Then
            dtEntity = oAuthorizedSigner.Listdata
            recordCount = oAuthorizedSigner.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub dtgPagingEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oAuthorizedSigner As New Parameter.AuthorizedSigner


        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            PnlAddEdit.Visible = True
            TxtDkmID.Enabled = False
            PnlGrid.Visible = False
            cboBranchID.Enabled = False
            ' TxtDkmID.Enabled = False

            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonClose.Visible = False
            ButtonCancel.CausesValidation = False
            cboBranchID.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            hdnBranchID.Value = e.Item.Cells(10).Text.Trim
            ' cboBranchID.SelectedIndex = cboBranchID.Items.IndexOf(cboBranchID.Items.FindByText(e.Item.Cells(2).Text.Trim))
            lblBranchID.Text = e.Item.Cells(2).Text.Trim

            TxtDkmID.Text = e.Item.Cells(3).Text.Trim
           
            TxtSigner1.Text = e.Item.Cells(4).Text.Trim
            TxtSigner2.Text = e.Item.Cells(5).Text.Trim
            If TxtSigner2.Text = "&nbsp;" Then

                TxtSigner2.Text = Replace(TxtSigner2.Text, "&nbsp;", String.Empty)
            End If

            TxtSigner3.Text = e.Item.Cells(6).Text.Trim
            If TxtSigner3.Text = "&nbsp;" Then

                TxtSigner3.Text = Replace(TxtSigner3.Text, "&nbsp;", String.Empty)
            End If

            TxtJbtSigner1.Text = e.Item.Cells(7).Text.Trim
            TxtJbtSigner2.Text = e.Item.Cells(8).Text.Trim
            If TxtJbtSigner2.Text = "&nbsp;" Then

                TxtJbtSigner2.Text = Replace(TxtJbtSigner2.Text, "&nbsp;", String.Empty)
            End If

            TxtJbtSigner3.Text = e.Item.Cells(9).Text.Trim
            If TxtJbtSigner3.Text = "&nbsp;" Then

                TxtJbtSigner3.Text = Replace(TxtJbtSigner3.Text, "&nbsp;", String.Empty)
            End If

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.AuthorizedSigner

            With customClass
                .BranchId = e.Item.Cells(10).Text.Trim
                .DocumentID = e.Item.Cells(3).Text.Trim
                .strConnection = GetConnectionString()
            End With

            err = m_controller.AuthorizedSignerDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
                lblMessage.Visible = True
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If

    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim c_branch As New BranchController
        Dim oCustom As New Parameter.Branch

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

            oCustom.strConnection = GetConnectionString()
            oCustom.BranchId = e.Item.Cells(2).Text.Trim
            oCustom = c_branch.BranchByID(oCustom)

            'If oCustom.ListData.Rows.Count > 0 Then
            '    e.Item.Cells(2).Text = oCustom.ListData.Rows(0).Item("branchName").ToString.Trim

            'End If
        End If
    End Sub

    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.AuthorizedSigner
        Dim ErrMessage As String = ""
        With customClass
            .BranchId = cboBranchID.SelectedValue.Trim
            .DocumentID = TxtDkmID.Text.Trim
            .Signer1 = TxtSigner1.Text.Trim

            If TxtSigner2.Text = "&nbsp;" Then
                Replace(TxtSigner2.Text, "&nbsp;", String.Empty)
            Else
                .Signer2 = TxtSigner2.Text.Trim
            End If
            If TxtSigner3.Text = "&nbsp;" Then
                Replace(TxtSigner3.Text, "&nbsp;", String.Empty)
            Else
                .Signer3 = TxtSigner3.Text.Trim
            End If

            .JabatanSigner1 = TxtJbtSigner1.Text.Trim
            If TxtJbtSigner2.Text = "&nbsp;" Then
                Replace(TxtJbtSigner2.Text, "&nbsp;", String.Empty)
            Else
                .JabatanSigner2 = TxtJbtSigner2.Text.Trim
            End If

            If TxtJbtSigner3.Text = "&nbsp;" Then
                Replace(TxtJbtSigner3.Text, "&nbsp;", String.Empty)
            Else
                .JabatanSigner3 = TxtJbtSigner3.Text.Trim
            End If

            .strConnection = GetConnectionString()
        End With


        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.AuthorizedSignerSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.BranchId = hdnBranchID.Value
            customClass.DocumentID = TxtDkmID.Text
            m_controller.AuthorizedSignerSaveEdit(customClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            lblMessage.Visible = True
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub

    Private Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        PnlAddEdit.Visible = True
        PnlGrid.Visible = False
        ButtonBack.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonClose.Visible = False
        ButtonCancel.CausesValidation = False
        Me.AddEdit = "ADD"

        lblTitleAddEdit.Text = Me.AddEdit

        'Bind
        TxtDkmID.Text = Nothing
        TxtSigner1.Text = Nothing
        TxtSigner2.Text = Nothing
        TxtSigner3.Text = Nothing
        TxtJbtSigner1.Text = Nothing
        TxtJbtSigner2.Text = Nothing
        TxtJbtSigner3.Text = Nothing

    End Sub

    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("AuthorizedSigner")

        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Vendor")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "All"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
       ' cboBranchID.SelectedIndex = -1

        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"

            If cboBranch.SelectedValue.Trim <> "" And cboBranch.SelectedValue.Trim <> "0" Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'" _
                    + " AND a.BranchID = '" + cboBranch.SelectedValue.Trim + "'"
            End If
        Else
            Me.CmdWhere = "All"

            If cboBranch.SelectedValue.Trim <> "" And cboBranch.SelectedValue.Trim <> "0" Then
                Me.CmdWhere = "a.BranchID = '" + cboBranch.SelectedValue.Trim + "'"
            End If
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("AuthorizedSigner.aspx")
    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("AuthorizedSigner.aspx")
    End Sub
    Sub BindBranch()

        Dim DtBranchName As New DataTable
        DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        If DtBranchName Is Nothing Then
            Dim DtBranchNameCache As New DataTable
            DtBranchNameCache = branch_session_controller.GetBranchName2(GetConnectionString, Me.sesBranchId.Replace("'", "").Trim)
            Me.Cache.Insert(CACHE_BRANCH_ALL, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        End If

        cboBranchID.DataValueField = "ID"
        cboBranchID.DataTextField = "Name"

        cboBranchID.DataSource = DtBranchName
        cboBranchID.DataBind()
        cboBranchID.Items.Insert(0, "Select One")
        cboBranchID.Items(0).Value = "0"
        cboBranchID.SelectedIndex = 0


        Dim oCustomClass As New Parameter.Login
        With oCustomClass
            .LoginId = Me.Loginid
            .Applicationid = Me.AppId
        End With
        oCustomClass = oLoginController.CheckMultiBranch(oCustomClass)
        cboBranch.DataSource = oCustomClass.ListMultiBranch

        cboBranch.DataSource = oCustomClass.ListMultiBranch
        cboBranch.DataTextField = "groupdbname"
        cboBranch.DataValueField = "groupdbid"
        cboBranch.DataBind()
        cboBranch.Items.Insert(0, "Select One")
        cboBranch.Items(0).Value = "0"
        cboBranch.SelectedIndex = 0

        'oCustomClass = oLoginController.CheckMultiBranch(oCustomClass)
        'cboBranch.DataSource = oCustomClass.ListMultiBranch

        'cboBranchIDEdit.DataSource = oCustomClass.ListMultiBranch
        'cboBranchIDEdit.DataTextField = "groupdbname"
        'cboBranchIDEdit.DataValueField = "groupdbid"
        'cboBranchIDEdit.DataBind()
        'cboBranchIDEdit.Items.Insert(0, "Select One")
        'cboBranchIDEdit.Items(0).Value = "0"
        'cboBranchIDEdit.SelectedIndex = 0
    End Sub

#Region " Private Const "
    Private branch_session_controller As New DataUserControlController
    Protected WithEvents cmbBranchID As System.Web.UI.WebControls.DropDownList
#End Region
End Class