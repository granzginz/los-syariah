﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region
Public Class BODView
    Inherits Maxiloan.Webform.WebBased
#Region " Property "
    Private Property CompanyID() As String
        Get
            Return CStr(viewstate("CompanyID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyID") = Value
        End Set
    End Property

    Private Property CompanyName() As String
        Get
            Return CStr(viewstate("CompanyName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CompanyName") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property BODID() As String
        Get
            Return CStr(viewstate("BODID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BODID") = Value
        End Set
    End Property

    Private Property BODType() As String
        Get
            Return CStr(ViewState("BODType"))
        End Get
        Set(ByVal Value As String)
            viewstate("BODType") = Value
        End Set
    End Property

#End Region
    Dim m_BOD As New CompanyController
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "CompanyBOD"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.BODID = Request.QueryString("BODID")
                If Request.QueryString("CompanyID") <> "" Then Me.CompanyID = Request.QueryString("CompanyID")
                If Request.QueryString("CompanyName") <> "" Then Me.CompanyName = Request.QueryString("CompanyName")
                viewbyID(Me.BODID, Me.CGID)
                PnlView.Visible = True
                pnlClose.Visible = True                
            End If
        End If
    End Sub
    Private Sub viewbyID(ByVal BODID As String, ByVal CGID As String)
        Dim BOD As New Parameter.CompanyBOD
        Dim BODList As New Parameter.CompanyBOD
        Dim dtInsCo As New DataTable
        With BOD
            .strConnection = getConnectionString
            .CompanyID = Me.CompanyID
            .BODID = Me.BODID
        End With
        BODList = m_BOD.ListCompanyBODByID(BOD)
        dtInsCo = BODList.ListData
        lblBODName.Text = CStr(dtInsCo.Rows(0).Item("BODName"))
        lblBODID.Text = CStr(dtInsCo.Rows(0).Item("BODID"))
        lblAddress.Text = CStr(dtInsCo.Rows(0).Item("BODAddress"))
        lblRT.Text = CStr(dtInsCo.Rows(0).Item("BODRT"))
        lblRW.Text = CStr(dtInsCo.Rows(0).Item("BODRW"))
        lblKelurahan.Text = CStr(dtInsCo.Rows(0).Item("BODKelurahan"))
        lblKecamatan.Text = CStr(dtInsCo.Rows(0).Item("BODKecamatan"))
        lblCity.Text = CStr(dtInsCo.Rows(0).Item("BODCity"))
        lblZipCode.Text = CStr(dtInsCo.Rows(0).Item("BODZipCode"))
        lblAreaPhone1.Text = CStr(dtInsCo.Rows(0).Item("BODAreaPhone1"))
        lblPhone1.Text = CStr(dtInsCo.Rows(0).Item("BODPhone1"))
        lblAreaFax.Text = CStr(dtInsCo.Rows(0).Item("BODAreaFax"))
        lblFax.Text = CStr(dtInsCo.Rows(0).Item("BODFax"))
        lblNPWP.Text = CStr(dtInsCo.Rows(0).Item("BODNPWP"))
        lblTitle.Text = CStr(dtInsCo.Rows(0).Item("BODTitle"))
        lblEmail.Text = CStr(dtInsCo.Rows(0).Item("BODEmail"))
        lblMobilePhone.Text = CStr(dtInsCo.Rows(0).Item("BODHP"))
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        Response.Redirect("CompanyBOD.aspx?CompanyID=" & Me.CompanyID & "&CompanyName=" & Me.CompanyName)
    End Sub
End Class