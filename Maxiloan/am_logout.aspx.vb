﻿Imports System.Web.Security
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Public Class am_logout
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents outtext As System.Web.UI.HtmlControls.HtmlGenericControl
    Private oLoginController As New LoginController
    Private ocustomclass As New Parameter.Login
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ocustomclass.strConnection = Me.GetConnectionString
            ocustomclass.BranchId = Me.sesBranchId.Replace("'", "")
            ocustomclass.LoginId = Me.Loginid
            ocustomclass.activityid = 2
            ocustomclass.FormID = "LOGOUT"
            oLoginController.setloghistory(ocustomclass)
        Catch ex As Exception

        End Try

        Session("loginid") = ""
        Session("appid") = ""
        Session("DBID") = ""
        Session("password") = ""
        Session("sqlpassword") = ""
        Session("ServerName") = ""
        Session("DatabaseName") = ""
        Session("UserID") = ""
        Session("PassKey") = ""
        Session("fullname") = ""
        Session("sesBusinessDate") = ""
        Session("sesBranchID") = ""
        Session("BranchName") = ""
        Session("Expired") = ""
        Dim lstrstring As String = ""
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        If strHTTPServer.IndexOf("/", 1) = -1 Then
            lstrstring = "<a href='am_login.aspx'>Login</a> atau <a href='javascript:fclose()'> Tutup </a>"
        Else
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            lstrstring = "<a href='http://" & strNameServer & "/" & StrHTTPApp & "/am_login.aspx'>Login</a> or <a href=""javascript:fclose()""> Tutup </a>"
        End If
        outtext.InnerHtml = lstrstring
        FormsAuthentication.SignOut()
    End Sub
End Class