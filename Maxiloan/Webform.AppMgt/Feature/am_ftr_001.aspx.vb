﻿Imports System.Data.SqlClient
Public Class am_ftr_001
    Inherits Maxiloan.Webform.UserManagementBase
    Private GObjConn As New SqlConnection(Me.ConnectionStringAM)
    Private GObjdataview As New DataView
    Private GObjDataset As New DataSet

#Region "Property"
    Private Property Mode() As String
        Get
            Return (CType(viewstate("mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("mode") = Value
        End Set
    End Property
    Private ReadOnly Property strFormID() As String
        Get
            Return (Request.QueryString("formId"))
        End Get
    End Property

    Private ReadOnly Property ApplicationID() As String
        Get
            Return Request.QueryString("Applicationid")
        End Get
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If sessioninvalid() Then
            Exit Sub
        End If        
        If Not (IsPostBack) Then            
            lblformid_head.Text = Me.strFormID
            lblapplicationid_head.Text = Me.ApplicationID            
        End If
        DoBind()        
    End Sub
    Sub DoBind()
        GObjConn.Open()
        Dim ObjDatasource As New SqlDataAdapter("select * from sec_featureform where applicationid='" _
                                                & Me.ApplicationID & "' and formid='" & Me.strFormID & "'", GObjConn)
        GObjDataset = New DataSet
        ObjDatasource.Fill(GObjDataset, "feature")        
        If (GObjDataset.Tables("feature").Rows.Count > 0) Then
            Dim GObjDataView As DataView = New DataView
            GObjDataView = GObjDataset.Tables("feature").DefaultView
            lblMessage.Visible = False
            DtgForm.AllowSorting = True


            lblRecord.Text = "Total Record(s) " & GObjDataset.Tables("feature").Rows.Count
            lblPage.Text = "page " & DtgForm.CurrentPageIndex + 1 & " of " & DtgForm.PageCount & " Page(s)"
        Else
            DtgForm.AllowSorting = False
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblRecord.Text = ""
            lblPage.Text = ""
            DtgForm.CurrentPageIndex = 0
        End If
        DtgForm.DataSource = GObjDataset
        DtgForm.DataBind()
        GObjConn.Close()
    End Sub

    Sub ShowView(ByVal obj As Object, ByVal e As DataGridCommandEventArgs)
        PnlView.Visible = True
        PnlAdd.Visible = False
        If Not (e.Item.ItemIndex = -1) Then
            GObjDataset.Tables("feature").DefaultView.RowFilter = "featureid='" & _
                                        Trim(CStr(DtgForm.DataKeys(CInt(e.Item.ItemIndex)))) & "'"
            Dim objRow As DataRowView = GObjDataset.Tables("feature").DefaultView(0)
            lblFeatureid.Text = CStr(IIf(IsDBNull(objRow("Featureid")), "", objRow("Featureid")))
            lblFeaturename.Text = CStr(IIf(IsDBNull(objRow("Featurename")), "", objRow("Featurename")))
            lblisform.Text = CStr(IIf(IsDBNull(objRow("isform")), False, objRow("isform")))
            lblactive.Text = CStr(IIf(IsDBNull(objRow("active")), False, objRow("active")))
        End If
        lblstatus.Visible = False
    End Sub

    Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs)
        GObjdataview = GObjDataset.Tables("feature").DefaultView
        If Me.SortBy = "desc" Then
            GObjdataview.Sort = e.SortExpression & " desc"
            Me.SortBy = ""
        Else
            Me.SortBy = "desc"
            GObjdataview.Sort = e.SortExpression
        End If

        PnlView.Visible = False
        PnlAdd.Visible = False
        lblMessage.Visible = False
        DtgForm.DataSource = GObjdataview
        DtgForm.DataBind()
        lblstatus.Visible = False
    End Sub

    Sub doDelete(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        If e.CommandName = "Delete" Then
            PnlView.Visible = False
            PnlAdd.Visible = False
            Dim LStrSql As String = "delete from sec_featureform where formid=@formid and applicationid=@applicationid and featureid=@featureid"
            Dim LObjCommand As New SqlCommand(LStrSql, GObjConn)
            LObjCommand.Parameters.Add(New SqlParameter("@featureid", SqlDbType.Char, 5))
            LObjCommand.Parameters("@featureid").Value = Trim(CStr(DtgForm.DataKeys(CInt(e.Item.ItemIndex))))

            LObjCommand.Parameters.Add(New SqlParameter("@formid", SqlDbType.Char, 20))
            LObjCommand.Parameters("@formid").Value = Me.strFormID

            LObjCommand.Parameters.Add(New SqlParameter("@applicationid", SqlDbType.Char, 10))
            LObjCommand.Parameters("@applicationid").Value = Me.ApplicationID

            LObjCommand.Connection.Open()
            lblMessage.Visible = True
            Try
                LObjCommand.ExecuteNonQuery()
                ShowMessage(lblstatus, "Record has been deleted", False)
            Catch Exp As SqlException
                ShowMessage(lblstatus, "Delete failed", True)
            End Try
            ViewState("page") = 0
            LObjCommand.Connection.Close()
            DoBind()
        End If
    End Sub

    Sub EditData(ByVal obj As Object, ByVal e As DataGridCommandEventArgs)
        PnlView.Visible = False
        PnlAdd.Visible = True
        GObjDataset.Tables("feature").DefaultView.RowFilter = "featureid='" & _
                        Trim(CStr(DtgForm.DataKeys(CInt(e.Item.ItemIndex)))) & "'"
        Dim objRow As DataRowView = GObjDataset.Tables("feature").DefaultView(0)
        txtfeatureid.Text = CStr(IIf(IsDBNull(objRow("featureid")), "", objRow("featureid")))
        txtfeaturename.Text = CStr(IIf(IsDBNull(objRow("featurename")), "", objRow("featurename")))
        chkisform.Checked = CBool(IIf(IsDBNull(objRow("isform")), 0, objRow("isform")))
        chkactive.Checked = CBool(IIf(IsDBNull(objRow("active")), 0, objRow("active")))
        txtfeatureid.ReadOnly = True
        lblMessage.Visible = False
        lblstatus.Visible = False
        Me.Mode = "edit"
        lblstatus.Visible = False
    End Sub


    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If (DtgForm.PageCount >= CInt(txtPage.Text)) And (CInt(txtPage.Text) > 0) Then
            ViewState("page") = CInt(txtPage.Text) - 1
            DtgForm.CurrentPageIndex = CInt(ViewState("page"))
            DoBind()
        End If
        lblMessage.Visible = False
        lblstatus.Visible = False
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        PnlAdd.Visible = False
        PnlView.Visible = False
        Select Case e.CommandName
            Case "first"
                DtgForm.CurrentPageIndex = 0
            Case "prev"
                If DtgForm.CurrentPageIndex > 0 Then
                    DtgForm.CurrentPageIndex -= 1
                End If
            Case "next"
                If DtgForm.CurrentPageIndex < DtgForm.PageCount - 1 Then
                    DtgForm.CurrentPageIndex += 1
                End If
            Case "last"
                DtgForm.CurrentPageIndex = DtgForm.PageCount - 1
        End Select
        ViewState("page") = DtgForm.CurrentPageIndex
        DoBind()
        lblMessage.Visible = False
        lblstatus.Visible = False
    End Sub

    Private Sub imgbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        PnlAdd.Visible = True
        PnlView.Visible = False
        txtfeatureid.Text = ""
        txtfeaturename.Text = ""
        chkisform.Checked = False
        chkactive.Checked = False
        Me.Mode = "add"
        txtfeatureid.ReadOnly = False
        lblstatus.Visible = False
    End Sub

    Private Sub imgcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        PnlAdd.Visible = False
        lblstatus.Visible = False
    End Sub

    Private Sub imgsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        PnlAdd.Visible = False
        If Me.Mode = "add" Then
            doInsert()
        Else
            doUpdate()
        End If
    End Sub

    Sub doInsert()
        Dim LStrSql As String = "insert into sec_featureform " & _
                                "   (formid,applicationid,featureid,featurename,isform,active) " & _
                                "values (@formid,@applicationid,@featureid,@featurename,@isform,@active)"
        doExecCommand(LStrSql)
    End Sub
    Sub doUpdate()
        Dim LStrSql As String = "Update sec_featureform set featurename=@featurename," & _
                                "isform=@isform,active=@active where formid=@formid and applicationid=@applicationid and featureid=@featureid"
        doExecCommand(LStrSql)

    End Sub
    Sub doExecCommand(ByVal LStrSql As String)
        Dim LObjCommand As New SqlCommand(LStrSql, GObjConn)
        LObjCommand.Parameters.Add(New SqlParameter("@formid", SqlDbType.VarChar, 20))
        LObjCommand.Parameters("@formid").Value = Me.strFormID

        LObjCommand.Parameters.Add(New SqlParameter("@featureid", SqlDbType.Char, 5))
        LObjCommand.Parameters("@featureid").Value = Trim(txtfeatureid.Text)

        LObjCommand.Parameters.Add(New SqlParameter("@featurename", SqlDbType.Char, 50))
        LObjCommand.Parameters("@featurename").Value = Trim(txtfeaturename.Text)

        LObjCommand.Parameters.Add(New SqlParameter("@active", SqlDbType.Bit, 1))
        LObjCommand.Parameters("@active").Value = chkactive.Checked

        LObjCommand.Parameters.Add(New SqlParameter("@isform", SqlDbType.Bit, 1))
        LObjCommand.Parameters("@isform").Value = chkisform.Checked

        LObjCommand.Parameters.Add(New SqlParameter("@applicationid", SqlDbType.Char, 10))
        LObjCommand.Parameters("@applicationid").Value = Me.ApplicationID

        LObjCommand.Connection.Open()
        lblstatus.Visible = True
        Try
            LObjCommand.ExecuteNonQuery()
            ShowMessage(lblstatus, "Record save successfully", False)
        Catch Exp As SqlException
            If Exp.Number = 2627 Then
                ShowMessage(lblstatus, "Record already exists", True)
            Else
                ShowMessage(lblstatus, "Could not add/update record", True)
            End If
        End Try
        LObjCommand.Connection.Close()
        DoBind()
    End Sub

    Private Sub imgcancelview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancelview.Click
        PnlView.Visible = False
        PnlAdd.Visible = False
        lblstatus.Visible = False
    End Sub

    Private Sub imgback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Server.Transfer("../forms/am_form_001.aspx")
    End Sub
End Class