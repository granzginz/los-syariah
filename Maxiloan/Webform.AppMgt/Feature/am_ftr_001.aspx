﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_ftr_001.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.am_ftr_001" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblstatus" runat="server" Visible="False">
    </asp:Label>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                Master Feature Of Form</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Application ID
                </label>
                <asp:Label ID="lblapplicationid_head" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Form ID
                </label>
                <asp:Label ID="lblformid_head" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <label>
                    List Of Master Feature
                </label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgForm" runat="server" OnItemCommand="Showview" AllowPaging="True"
                        AutoGenerateColumns="False" OnEditCommand="Editdata" OnDeleteCommand="doDelete"
                        OnSortCommand="sortgrid" AllowSorting="True" CssClass="grid_general" DataKeyField="featureid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:ButtonColumn DataTextField="Featureid" SortExpression="Featureid" HeaderText="Feature ID"
                                CommandName="showview"></asp:ButtonColumn>
                            <asp:ButtonColumn DataTextField="Featurename" SortExpression="Featurename" HeaderText="Feature Name"
                                CommandName="showview"></asp:ButtonColumn>
                            <asp:ButtonColumn DataTextField="active" SortExpression="active" HeaderText="Active"
                                CommandName="showview"></asp:ButtonColumn>
                            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" HeaderText="Edit"
                                CancelText="Cancel" EditText="Edit">
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </asp:EditCommandColumn>
                            <asp:ButtonColumn Text="Delete" HeaderText="Delete" CommandName="Delete">
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </asp:ButtonColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ForeColor="#993300" ErrorMessage="Halaman tidak valid!"
                            ControlToValidate="txtPage"   Type="Integer"
                            MaximumValue="999999999" MinimumValue="1"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ForeColor="#993300" ErrorMessage="Halaman tidak valid!"
                            ControlToValidate="txtPage"   Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revpage" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="Page is invalid" CssClass="validator_general" Display="Dynamic"
                            ValidationExpression="(^\d{1}$)|(^\d{2}$)|(^\d{3}$)"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="rfvpage" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="Page must not empty" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlAdd" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    Add/Edit Records</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Feature ID
                </label>
                <asp:TextBox ID="txtfeatureid" TabIndex="2" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvformid" runat="server" Display="Dynamic" ErrorMessage="Feature ID must be fill"
                    ControlToValidate="txtfeatureid"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Feature Name
                </label>
                <asp:TextBox ID="txtfeaturename" TabIndex="3" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvformname" runat="server" Display="Dynamic" ErrorMessage="Feature Name must be fill"
                    ControlToValidate="txtfeaturename"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active
                </label>
                <asp:CheckBox ID="chkactive" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    IsForm
                </label>
                <asp:CheckBox ID="chkisform" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlView" runat="server" Visible="False">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    View Feature</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Feature ID
                </label>
                <asp:Label ID="lblFeatureid" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Feature Name
                </label>
                <asp:Label ID="lblFeaturename" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active
                </label>
                <asp:Label ID="lblactive" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    IsForm
                </label>
                <asp:Label ID="lblisform" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btncancelview" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
