﻿#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_feature
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property LogID() As String
        Get
            Return (CType(viewstate("LoginID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("LoginID") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("LoginID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("LoginID") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Dim oCustomClass As New Parameter.Feature
    Dim oLoginController As New LoginController
    Private oController As New FeatureController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 15
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim strSortBy As String
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.LogID = Request.QueryString("LoginID")
            lblLoginid.Text = Me.LogID
            Dim DtApplication As DataTable
            DtApplication = CType(Me.Cache.Item("APPLICATIONLOGIN"), DataTable)
            If DtApplication Is Nothing Then
                Dim DtApplicationCache As New DataTable
                Dim oCustomLogin As Parameter.Login
                oCustomLogin = oLoginController.GetLoginData
                DtApplicationCache = oCustomLogin.LoginApplication
                Me.Cache.Insert("APPLICATIONLOGIN", DtApplicationCache, Nothing, DateTime.Now.AddHours(100), TimeSpan.Zero)
                DtApplication = CType(Me.Cache.Item("APPLICATIONLOGIN"), DataTable)
            End If
            cmbApplicationID.DataTextField = "ApplicationName"
            cmbApplicationID.DataValueField = "ApplicationID"
            cmbApplicationID.DataSource = DtApplication

            cmbApplicationID.DataBind()
            cmbApplicationID.Items.Insert(0, "Select One")
            cmbApplicationID.Items(0).Value = "0"
            cmbApplicationID.SelectedIndex = 2

            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlgrid.Visible = False
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .LoginId = Me.LogID
            .AppID = cmbApplicationID.SelectedValue
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListMasterForm(oCustomClass)

        DtUserList = oCustomClass.ListMasterForm
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgForm.DataSource = DvUserList
        Try
            DtgForm.DataBind()
        Catch
            DtgForm.CurrentPageIndex = 0
            DtgForm.DataBind()
        End Try
        pnlgrid.Visible = True
        PagingFooter()
    End Sub


#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
             
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgUserList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgForm.ItemDataBound
        Dim lnkFormId As New Label
        Dim hypFeature As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkFormId = CType(e.Item.FindControl("lblFormId"), Label)
            hypFeature = CType(e.Item.FindControl("hypFeature"), HyperLink)
            hypFeature.NavigateUrl = "../feature/am_feature_user.aspx?formId=" & lnkFormId.Text.Trim & "&Applicationid=" & cmbApplicationID.SelectedValue.Trim & "&loginid=" & Me.LogID
        End If
    End Sub

    Protected Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgForm.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = ""
        If txtFormID_src.Text.Trim <> "" Then
            If Right(txtFormName_src.Text.Trim, 1) = "%" Then
                Me.SearchBy = " formid like '" & txtFormID_src.Text.Trim & "'"
            Else
                Me.SearchBy = " formid = '" & txtFormID_src.Text.Trim & "'"
            End If
        End If

        If txtFormName_src.Text.Trim <> "" Then
            If Me.SearchBy <> "" Then
                If Right(txtFormName_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy &= " and FormName Like '" & txtFormName_src.Text.Trim & "'"
                Else
                    Me.SearchBy &= " and FormName = '" & txtFormName_src.Text.Trim & "'"
                End If
            Else
                If Right(txtFormName_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " FormName Like '" & txtFormName_src.Text.Trim & "'"
                Else
                    Me.SearchBy = " FormName = '" & txtFormName_src.Text.Trim & "'"
                End If
            End If
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub ImgAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Server.Transfer("../Users/am_user_001.aspx")
    End Sub
End Class