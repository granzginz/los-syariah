﻿#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_feature_user
    Inherits Maxiloan.Webform.WebBased
    Private oCustomClass As New Parameter.Feature
    Private oController As New FeatureController

#Region "Property"

    Private Property ApplicationId() As String
        Get
            Return (CType(ViewState("ApplicationId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property

    Private Property LoginIdApplication() As String
        Get
            Return (CType(ViewState("LoginApplication"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("LoginApplication") = Value
        End Set
    End Property

#End Region

#Region "Page Load"
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.ApplicationId = Request.QueryString("applicationid")
        Me.LoginIdApplication = Request.QueryString("loginid")
        Me.FormID = Request.QueryString("formId")

        lblloginid.Text = Me.LoginIdApplication
        lblApplicationId.Text = Me.ApplicationId
        lblFormID.Text = Me.FormID
        If Not IsPostBack Then
            Me.SearchBy = ""
            Me.SortBy = ""
            doBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub doBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .LoginId = Me.LoginIdApplication
            .AppID = Me.ApplicationId
            .FormID = Me.FormID
        End With
        oCustomClass = oController.ListFeatureUser(oCustomClass)
        lblFormName.Text = oCustomClass.FormName
        DtUserList = oCustomClass.ListFeatureUser

        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgUserFeature.DataSource = DvUserList
        Try
            dtgUserFeature.DataBind()
        Catch
            dtgUserFeature.CurrentPageIndex = 0
            dtgUserFeature.DataBind()
        End Try
        lblloginid.Text = Me.LoginIdApplication
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Server.Transfer("am_feature.aspx?loginid=" & Me.LoginIdApplication & "&ApplicationID=" & Me.ApplicationId)
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'UnResolvedMergeConflict Setelah save screen kembali ke posisi sebelumnya
        Dim strFeatureID As String
        Dim i As Integer
        Dim chkUserFeature As New CheckBox
        Dim lblFeatureId As New Label

        strFeatureID = ""
        For i = 0 To (dtgUserFeature.Items.Count Mod dtgUserFeature.PageSize) - 1
            chkUserFeature = CType(dtgUserFeature.Items(i).FindControl("chkUserGroupMenu"), CheckBox)
            lblFeatureId = CType(dtgUserFeature.Items(i).FindControl("lblFeatureID"), Label)
            If chkUserFeature.Checked = True Then
                strFeatureID &= CStr(IIf(strFeatureID = "", "", ",")) & "'" & lblFeatureId.Text.Trim & "'"
            End If
        Next

        With oCustomClass
            .AppID = Me.ApplicationId
            .LoginId = Me.LoginIdApplication
            .FormID = Me.FormID
            .FeatureID = strFeatureID
        End With
        Dim err As String = oController.UpdateFeatureUser(oCustomClass)
        If err <> String.Empty Then
            ShowMessage(lblMessage, err, True)
        End If
    End Sub
End Class