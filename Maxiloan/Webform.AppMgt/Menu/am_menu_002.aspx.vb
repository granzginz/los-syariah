﻿Imports System.Xml
Imports System.Data.SqlClient
Imports System.IO
Public Class am_menu_002
    Inherits UserManagementBase
    Private ObjCek As New am_com_001


#Region "Private Program"
    Private GObjDataSet As DataSet = New DataSet
    Private GObjCon As SqlConnection = New SqlConnection(Me.ConnectionStringAM)
    Private Property Mode() As String
        Get
            Return (CType(viewstate("mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("mode") = Value
        End Set
    End Property
    Public Function WriteXMLOnTheFly(ByVal LstrAppID As String) As String
        Dim LObjPrimary(2) As DataColumn
        Dim LObjDtAdpt As New SqlDataAdapter
        LObjDtAdpt.SelectCommand = New SqlCommand("select a.*, isnull(b.formfilename,'') as formfilename from sec_msmenu a " & _
                " left outer join sec_msform b on a.formid=b.formid and a.applicationid=b.applicationid " & _
                " where a.applicationid='" + LstrAppID + "' ", GObjCon)
        LObjDtAdpt.Fill(GObjDataSet, "MsMenu")
        LObjPrimary(0) = GObjDataSet.Tables("MsMenu").Columns("menuid")
        GObjDataSet.Tables("MsMenu").PrimaryKey = LObjPrimary    
        WriteXMLOnTheFly = createMenuXml(LstrAppID)
    End Function
    Private Function createMenuXml(ByVal applicationId As String) As String
        Dim LStrXml As String
        Dim LObjDataMenuFounds As DataRow()
        LObjDataMenuFounds = (GObjDataSet.Tables("MsMenu").Select(" parentmenuid='root' ", "order_"))
        LStrXml = "<?xml version='1.0'?>" & vbCrLf & _
                "<MsMenu>" & vbCrLf
        createMenuXmlItem(LStrXml, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
        LStrXml = LStrXml & "</MsMenu>"
        createMenuXml = LStrXml
    End Function

    Private Sub createMenuXmlItem(ByRef xmlString As String, ByVal DataFounds As DataRow(), ByRef starting As Integer, ByVal ending As Integer, ByVal applicationId As String)
        Dim LObjDataMenuFounds As DataRow()        
        Dim LStrMenuID As String
        While starting <= ending
            LStrMenuID = CStr(DataFounds(starting)("menuid"))            
            xmlString = xmlString & "<MsMenuItem title='" & CStr(DataFounds(starting)("prompt")) & "'  url='" & CStr(IIf((CStr(DataFounds(starting)("formfilename")) = ""), "am_construction.aspx", Trim(CStr(DataFounds(starting)("formfilename"))))) & "' no='" & CStr(DataFounds(starting)("level_")) & "' result='" & CStr(DataFounds(starting)("result")) & "'  menuid='" & LStrMenuID & "' command='" & CStr(DataFounds(starting)("Command")) & "'>"
            LObjDataMenuFounds = (GObjDataSet.Tables("MsMenu").Select(" parentmenuid='" + LStrMenuID + "'"))
            If LObjDataMenuFounds.Length > 0 Then
                createMenuXmlItem(xmlString, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
            End If
            xmlString = xmlString & "</MsMenuItem>" & vbCrLf
            starting = starting + 1
        End While
    End Sub
#End Region
#Region "Get String"
    Private Function GetResult(ByVal Result As String) As String
        Select Case UCase(Result)
            Case "SM"
                GetResult = "SubMenu"
            Case "CM"
                GetResult = "Command"
            Case "FM"
                GetResult = "Form"
            Case Else
                GetResult = ""
        End Select
    End Function
    Private Function GetSide(ByVal Side As String) As String
        Select Case UCase(Side)
            Case "L"
                GetSide = "Left"
            Case "R"
                GetSide = "Right"
            Case "T"
                GetSide = "Top"
            Case "B"
                GetSide = "Bottom"
            Case Else
                GetSide = ""
        End Select
    End Function
    Private Function GetSideIndex(ByVal Side As String) As Integer
        Select Case UCase(Side)
            Case "LEFT"
                GetSideIndex = 1
            Case "RIGHT"
                GetSideIndex = 2
            Case "TOP"
                GetSideIndex = 3
            Case "BOTTOM"
                GetSideIndex = 4
            Case Else
                GetSideIndex = 0
        End Select
    End Function
    Private Function GetResultIndex(ByVal Result As String) As Integer
        Select Case UCase(Result)
            Case "SUBMENU"
                GetResultIndex = 1
            Case "COMMAND"
                GetResultIndex = 2
            Case "FORM"
                GetResultIndex = 3
            Case Else
                GetResultIndex = 0
        End Select
    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            pnlTree.Visible = False
            fillComboApplications()
        End If
    End Sub

    Private Sub imgGoWithAppId_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoWithAppId.Click
        Refresh()
        btnAdd.Visible = True
        pnlTree.Visible = True
        doBindNew()
    End Sub

    Sub Refresh()        
        Panel1.Visible = True
        If cboApplication.SelectedIndex > 0 Then
            btnAdd.Visible = True
        Else
            btnAdd.Visible = False
        End If
        pnlAddMenu.Visible = False
        pnlForm.Visible = False
        btnEdit.Visible = False
        btnDel.Visible = False
        pnlSaveCancel.Visible = False
        lblMenuID.Text = ""
        lblLevel.Text = "0"
    End Sub
    
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Me.Mode = "edit"
        Panel1.Visible = True
        pnlForm.Visible = False
        pnlAddMenu.Visible = True
        btnAdd.Visible = False
        btnEdit.Visible = False
        btnDel.Visible = False
        pnlSaveCancel.Visible = True        
        lblAddMenuID.Text = lblMenuID.Text
        lblAddParentMenuID.Text = lblParentMenuID.Text
        txtAddPrompt.Text = lblPrompt.Text
        cboAddSide.SelectedIndex = GetSideIndex(lblSide.Text)
        lblAddLevel.Text = lblLevel.Text
        cboAddResult.SelectedIndex = GetResultIndex(lblResult.Text)
        txtAddDisplay.Text = lblDisplay.Text
        txtAddCommand.Text = lblCommand.Text
        txtAddFormID.Text = lblFormID.Text
        If chkActive.Checked Then
            chkAddActive.Checked = True
        Else
            chkAddActive.Checked = False
        End If
    End Sub
    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click        
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(Me.ConnectionStringAM)
        pnlAddMenu.Visible = False
        Dim MyCommand As SqlCommand = New SqlCommand("spDeleteMsMenu", MyConnection)
        MyCommand.CommandType = CommandType.StoredProcedure
        MyCommand.Parameters.Add(New SqlParameter("@Menuid", SqlDbType.Char, 5))
        MyCommand.Parameters("@Menuid").Value = Trim(lblMenuID.Text)
        MyCommand.Parameters.Add(New SqlParameter("@Applicationid", SqlDbType.Char, 10))
        MyCommand.Parameters("@Applicationid").Value = Trim(cboApplication.SelectedItem.Value)
        MyCommand.Connection.Open()
        Try
            MyCommand.ExecuteNonQuery()
            ShowMessage(lblMessage, "You Must Re-Login to Make Change effected !", False)
        Catch Exp As SqlException
        End Try
        Dim XMLFileName As String = Request.ServerVariables("APPL_PHYSICAL_PATH") & "Webform.AppMgt\Menu\" & "Menu_" & Trim(cboApplication.SelectedItem.Value) & "_" & Trim(CStr(Session("loginid"))) & ".xml"
        File.Delete(XMLFileName)
        Dim WriteXML As New ReadXMLFile
        WriteXML.WriteXMLToFile(XMLFileName, cboApplication.SelectedItem.Value)
        Dim InsertCmd As String = "update sec_userapplication set updmenu = 1 where  Applicationid = '" & cboApplication.SelectedItem.Value & "'"
        MyCommand = New SqlCommand(InsertCmd, MyConnection)
        MyCommand.ExecuteNonQuery()
        MyCommand.Connection.Close()
    End Sub
    Private Sub ErrorAdd()
        Panel1.Visible = True
        pnlAddMenu.Visible = True
        Message.Visible = True
        pnlSaveCancel.Visible = True
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(Me.ConnectionStringAM)
        Dim MyCommand As SqlCommand
        If txtAddPrompt.Text = "" Or txtAddDisplay.Text = "" Then
            Message.Text = "Null values not allowed for Prompt Name or Display Level"
            ErrorAdd()
            Exit Sub
        End If
        If cboAddResult.SelectedIndex = 0 Or cboAddSide.SelectedIndex = 0 Then
            Message.Text = "Select Result and Side"
            ErrorAdd()
            Exit Sub
        End If
        If cboAddResult.SelectedIndex = 2 And txtAddCommand.Text = "" Then
            Message.Text = "Please enter Command Line"
            ErrorAdd()
            Exit Sub
        End If
        If cboAddResult.SelectedIndex = 3 And txtAddFormID.Text = "" Then
            Message.Text = "Please enter Form ID"
            ErrorAdd()
            Exit Sub
        End If
        Dim InsertCmd As String = ""
        Dim Side As String = ""
        Select Case cboAddSide.SelectedIndex
            Case 0
                Side = ""
            Case 1
                Side = "L"
            Case 2
                Side = "R"
            Case 3
                Side = "T"
            Case 4
                Side = "B"
        End Select
        Dim Result As String = ""
        Select Case cboAddResult.SelectedIndex
            Case 0
                Result = ""
            Case 1
                Result = "SM"
            Case 2
                Result = "CM"
            Case 3
                Result = "FM"
        End Select
        Select Case Me.Mode
            Case "add"
                InsertCmd = "insert into sec_msmenu (Menuid, Parentmenuid, Prompt, Side, Level_, Result, Command, Formid, Active, Order_, Applicationid) values (@Menuid, @Parentmenuid, @Prompt, @Side, @Level_, @Result, @Command, @Formid, @Active, @Order_, @Applicationid)"
            Case "edit"
                InsertCmd = "update sec_msmenu set Prompt = @Prompt, Side = @Side, Result = @Result, Command = @Command, Formid = @Formid, Active = @Active, Order_ = @Order_ where Menuid = @Menuid and applicationid=@applicationid"
        End Select
        MyCommand = New SqlCommand(InsertCmd, MyConnection)
        MyCommand.Parameters.Add(New SqlParameter("@Menuid", SqlDbType.Char, 5))
        MyCommand.Parameters("@Menuid").Value = lblAddMenuID.Text
        MyCommand.Parameters.Add(New SqlParameter("@Parentmenuid", SqlDbType.Char, 5))
        MyCommand.Parameters("@Parentmenuid").Value = lblAddParentMenuID.Text
        MyCommand.Parameters.Add(New SqlParameter("@Prompt", SqlDbType.VarChar, 50))
        MyCommand.Parameters("@Prompt").Value = txtAddPrompt.Text.Replace("&", "&amp;")
        MyCommand.Parameters.Add(New SqlParameter("@Side", SqlDbType.Char, 1))
        MyCommand.Parameters("@Side").Value = Side
        MyCommand.Parameters.Add(New SqlParameter("@Level_", SqlDbType.TinyInt, 5))
        MyCommand.Parameters("@Level_").Value = CInt(lblAddLevel.Text)
        MyCommand.Parameters.Add(New SqlParameter("@Result", SqlDbType.Char, 2))
        MyCommand.Parameters("@Result").Value = Result
        MyCommand.Parameters.Add(New SqlParameter("@Command", SqlDbType.VarChar, 255))
        MyCommand.Parameters("@Command").Value = txtAddCommand.Text
        MyCommand.Parameters.Add(New SqlParameter("@Formid", SqlDbType.Char, 20))
        MyCommand.Parameters("@Formid").Value = txtAddFormID.Text
        MyCommand.Parameters.Add(New SqlParameter("@Active", SqlDbType.Bit, 1))
        MyCommand.Parameters("@Active").Value = chkAddActive.Checked
        MyCommand.Parameters.Add(New SqlParameter("@Order_", SqlDbType.Int, 5))
        MyCommand.Parameters("@Order_").Value = CInt(txtAddDisplay.Text)
        MyCommand.Parameters.Add(New SqlParameter("@Applicationid", SqlDbType.Char, 10))
        MyCommand.Parameters("@Applicationid").Value = Trim(cboApplication.SelectedItem.Value)
        MyCommand.Connection.Open()
        Try
            MyCommand.ExecuteNonQuery()
            Message.Text = "Done"
            ShowMessage(lblMessage, "You Must Re-Login to Make Change effected !", False)
        Catch Exp As SqlException
            If Exp.Number = 2627 Then
                Message.Text = "A record already exists with the same primary key"
            Else
                Message.Text = "Could not add record, please ensure the fields are correctly filled out"
            End If
        End Try
        Panel1.Visible = True
        pnlForm.Visible = True
        If Me.Mode = "add" Then
            btnAdd.Visible = True
        End If
        btnEdit.Visible = True
        btnDel.Visible = True
        pnlAddMenu.Visible = False
        pnlSaveCancel.Visible = False
        Message.Visible = False
        If Me.Mode = "edit" Then
            lblPrompt.Text = txtAddPrompt.Text
            lblSide.Text = GetSide(cboAddSide.SelectedItem.Value)
            lblResult.Text = GetResult(cboAddResult.SelectedItem.Value)
            lblDisplay.Text = txtAddDisplay.Text
            lblCommand.Text = txtAddCommand.Text
            lblFormID.Text = txtAddFormID.Text
            chkActive.Text = chkAddActive.Text
        End If
        Dim XMLFileName As String = Request.ServerVariables("APPL_PHYSICAL_PATH") & "Webform.AppMgt\Menu\" & "Menu_" & Trim(cboApplication.SelectedItem.Value) & "_" & Trim(CStr(Session("loginid"))) & ".xml"
        File.Delete(XMLFileName)
        Dim WriteXML As New ReadXMLFile
        WriteXML.WriteXMLToFile(XMLFileName, cboApplication.SelectedItem.Value)
        InsertCmd = "update sec_userapplication set updmenu = 1 where Applicationid = '" & cboApplication.SelectedItem.Value & "'"
        MyCommand = New SqlCommand(InsertCmd, MyConnection)

        MyCommand.ExecuteNonQuery()
        MyCommand.Connection.Close()
        Me.Mode = "save"
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Panel1.Visible = True
        If UCase(lblAddParentMenuID.Text) = "ROOT" Then
            pnlForm.Visible = False
            btnEdit.Visible = False
            btnDel.Visible = False
        Else
            pnlForm.Visible = True
            btnEdit.Visible = True
            btnDel.Visible = True
        End If
        If Me.Mode = "add" Or UCase(lblResult.Text) = "SUBMENU" Then
            btnAdd.Visible = True
        End If
        pnlAddMenu.Visible = False
        pnlSaveCancel.Visible = False
        Message.Visible = False
    End Sub

    Private Sub TreeView2_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeView2.SelectedNodeChanged
        Panel1.Visible = True
        pnlForm.Visible = True
        btnEdit.Visible = True
        btnDel.Visible = True
        pnlSaveCancel.Visible = False
        pnlAddMenu.Visible = False
        Dim mySelectQuery As String = "EXECUTE spGetMenuDetail '" & TreeView2.SelectedNode.Value & "','" & Trim(cboApplication.SelectedItem.Value) & "'"
        Dim myConnection As New SqlConnection(Me.ConnectionStringAM)
        Dim myCommand As New SqlCommand(mySelectQuery, myConnection)
        myConnection.Open()
        Dim myReader As SqlDataReader = myCommand.ExecuteReader()
        Try
            While myReader.Read()
                lblMenuID.Text = CStr(IIf(IsDBNull(myReader.Item(0)), "", myReader.Item(0)))
                lblParentMenuID.Text = CStr(IIf(IsDBNull(myReader.Item(1)), "", myReader.Item(1)))
                lblPrompt.Text = CStr(IIf(IsDBNull(myReader.Item(2)), "", myReader.Item(2)))
                lblLevel.Text = CStr(IIf(IsDBNull(myReader.Item(3)), "", myReader.Item(3)))
                lblResult.Text = GetResult(CStr(myReader.Item(4)))
                lblCommand.Text = CStr(IIf(IsDBNull(myReader.Item(5)), "", myReader.Item(5)))
                lblFormID.Text = CStr(IIf(IsDBNull(myReader.Item(6)), "", myReader.Item(6)))
                lblFormFileName.Text = CStr(IIf(IsDBNull(myReader.Item(7)), "", myReader.Item(7)))
                lblSide.Text = GetSide(CStr(myReader.Item(8)))
                lblDisplay.Text = CStr(myReader.Item(9))
                If CBool(myReader.Item(10)) Then
                    chkActive.Checked = True
                Else
                    chkActive.Checked = False
                End If
                If Trim(CStr(myReader.Item(4))) = "SM" Then
                    btnAdd.Visible = True
                Else
                    btnAdd.Visible = False
                End If
            End While


            loadGridReorder(lblMenuID.Text)


        Finally
            myReader.Close()
            myConnection.Close()
        End Try
    End Sub

    Private Sub loadGridReorder(ByVal strMenuID As String)
        Dim StrSql As String = "EXEC dbo.spGetChildMenu  @ParentMenuID = '" & strMenuID & "', @ApplicationID = '" & Trim(cboApplication.SelectedItem.Value) & "'"
        Dim objConn As SqlConnection = New SqlConnection(Me.ConnectionStringAM)

        Dim DS As DataSet
        Dim MyCommand As SqlDataAdapter

        MyCommand = New SqlDataAdapter(StrSql, objConn)
        DS = New DataSet

        MyCommand.Fill(DS, "ChildMenuReorder")
        gvReorder.DataSource = DS.Tables("ChildMenuReorder").DefaultView
        gvReorder.DataBind()
    End Sub

    Private Sub fillComboApplications()
        Dim StrSql As String = "EXECUTE spApplication"
        Dim objConn As SqlConnection = New SqlConnection(Me.ConnectionStringAM)
        If Not (IsPostBack) Then
            Dim DS As DataSet
            Dim MyCommand As SqlDataAdapter
            MyCommand = New SqlDataAdapter(StrSql, objConn)
            DS = New DataSet
            MyCommand.Fill(DS, "Application")
            cboApplication.DataSource = DS.Tables("Application").DefaultView
            cboApplication.DataBind()
            cboApplication.Items.Insert(0, "Select Application")
            cboApplication.Items(0).Value = ""
        End If
    End Sub

    Private Sub doBindNew()
        Panel1.Visible = False
        If cboApplication.SelectedIndex > 0 Then
            Try
                XmlDataSource1.Data = WriteXMLOnTheFly(Trim(cboApplication.SelectedItem.Value))
                XmlDataSource1.DataBind()
                TreeView2.DataSource = XmlDataSource1
                TreeView2.DataBind()
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try

        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Me.Mode = "add"
        Panel1.Visible = True
        pnlForm.Visible = False
        pnlAddMenu.Visible = True
        btnAdd.Visible = False
        btnEdit.Visible = False
        btnDel.Visible = False
        pnlSaveCancel.Visible = True
        If lblMenuID.Text = "" Then
            lblAddParentMenuID.Text = "root"
            cboAddResult.SelectedIndex = 1
            cboAddResult.Enabled = False
        Else
            lblAddParentMenuID.Text = lblMenuID.Text
            cboAddResult.SelectedIndex = 0
            cboAddResult.Enabled = True
        End If
        lblAddLevel.Text = CStr(CInt(lblLevel.Text) + 1)
        txtAddPrompt.Text = ""
        cboAddSide.SelectedIndex = 0
        txtAddDisplay.Text = ""
        txtAddCommand.Text = ""
        txtAddFormID.Text = ""
        chkAddActive.Checked = False
        Dim mySelectQuery As String = "EXECUTE spGetMaxMenuID '" & cboApplication.SelectedItem.Value & "'"
        Dim myConnection As New SqlConnection(Me.ConnectionStringAM)
        Dim myCommand As New SqlCommand(mySelectQuery, myConnection)
        myConnection.Open()
        Dim myReader As SqlDataReader = myCommand.ExecuteReader()
        While myReader.Read
            lblAddMenuID.Text = CStr(CInt(IIf(IsDBNull(myReader.Item(0)), 1, myReader.Item(0))) + 1)
        End While
        If lblAddMenuID.Text = "" Then
            lblAddMenuID.Text = "1"
        End If
    End Sub

    'TODO Move Up & Down menu tree
    Private Sub btnReorderReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReorderReset.Click
        Dim MyConnection As SqlConnection = New SqlConnection(Me.ConnectionStringAM)
        Dim MyCommand As SqlCommand


        MyCommand = New SqlCommand("spChildMenuOrderUpdate", MyConnection)
        MyCommand.CommandType = CommandType.StoredProcedure

        MyCommand.Parameters.Add(New SqlParameter("@Menuid", SqlDbType.Char, 5))
        MyCommand.Parameters("@Menuid").Value = lblAddMenuID.Text



        Try
            MyCommand.ExecuteNonQuery()
            ShowMessage(lblMessage, "Data saved successfully!", False)
        Catch Exp As SqlException
            ShowMessage(lblMessage, Exp.Message, True)
        End Try
    End Sub
End Class