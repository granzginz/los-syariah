Imports Microsoft.Win32.Registry
Imports Microsoft.Win32

Public Class UserManagementBase : Inherits Maxiloan.Webform.WebBased

    Protected Function ConnectionStringAM() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationSettings.AppSettings("RegKey"), False)
        Dim ServerName As String
        Dim DbName As String
        Dim UserID As String
        Dim Password As String
        Dim strConnectionString As String

        ServerName = rk.GetValue("ServerName", "").ToString.Trim
        DbName = rk.GetValue("DbName", "").ToString.Trim
        UserID = rk.GetValue("UserID", "").ToString.Trim
        Password = rk.GetValue("Password", "").ToString.Trim
        strConnectionString = "server=" & ServerName & ";database=" & DbName & ";user id=" & UserID & ";password=" & Password & ";"

        Return strConnectionString
    End Function
End Class
