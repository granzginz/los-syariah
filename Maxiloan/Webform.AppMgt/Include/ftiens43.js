isOPERA = (navigator.userAgent.indexOf('Opera') >= 0) ? true : false;
isIE = (document.all && !isOPERA) ? true : false;
isDOM = (document.getElementById && !isIE && !isOPERA) ? true : false;

function processTree(id) {
    if (content[id][0]) {
        for (i = 0; i < content[id][1].length; i++)
            hide(content[id][1][i]);

        content[id][0] = false;
    }
    else {
        for (i = 0; i < content[id][1].length; i++)
            show(content[id][1][i], 'inline-block');

        content[id][0] = true;
    }

    return false;
}

function show(id, displayValue) {
    if (isDOM) {
//        alert(isDOM);
//        alert(id);
//        alert(displayValue);
        document.getElementById(id).style.display = (displayValue) ? displayValue : "block";
    }

    else if (isIE) {
        document.all[id].style.display = "block";
    }
}

function hide(id) {
    if (isDOM)
        document.getElementById(id).style.display = "none";
    else if (isIE)
        document.all[id].style.display = "none";
}

if (isDOM || isIE) {
    document.writeln('<style type="text/css">');
    document.writeln('.SubItemRow \{ display: none; \}');
    document.writeln('</style>');
}