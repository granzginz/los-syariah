﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_user_006.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.am_user_006" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">            
            <h3>
                User Application</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Login ID
                </label>
                <asp:Label ID="lblloginid" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <h4>
                    List Of User Application</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgUsrApp" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="loginid" CssClass="grid_general" OnSortCommand="SortGrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="Applicationid" HeaderText="ApplicationID" Visible="False">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'
                                        CommandName="ShowView">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationName" HeaderText="APPLICATION NAME">
                                <ItemStyle Width="40%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkApplicationName" runat="server" Text='<%#Container.DataItem("ApplicationName")%>'
                                        CommandName="ShowView">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ACTIVE">
                                <ItemTemplate>
                                    <asp:Label ID="lblAct" runat="server" Text='<%#Container.DataItem("Active")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="USER GROUP DATA">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypUserGroupData" runat="server" ImageUrl="../../Images/IconReceived.gif"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT" Visible="False">
                            
                            <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                   <asp:Button ID="btnEdit" runat="server" CausesValidation="False" Text="Edit" CssClass="small button blue"
                                        CommandName="EDIT" Visible='<%# iif(Trim(container.dataitem("ApplicationId")) = "AM", "False", "True")%>'></asp:Button>
                                    
                                    </asp:Button>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE" Visible="False">
                            <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Button ID="imgDelete" runat="server" CausesValidation="False" Text="Delete" CssClass="small button red"		
                                        CommandName="DELETE"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Order" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOrder" runat="server" Text='<%#Container.DataItem("Order_")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:button>
                        <asp:RangeValidator ID="rgvGo" runat="server" 
                         CssClass="validator_general" Display="Dynamic" Type="Integer" MaximumValue="999999999" ErrorMessage="Halaman tidak valid!"
                            MinimumValue="1" ControlToValidate="txtPage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                         CssClass="validator_general" ErrorMessage="Halaman tidak valid!" ControlToValidate="txtPage"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                      <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server"  Text="Add" CssClass="small button blue"
                    CausesValidation="False"></asp:Button>
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray"		>
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    View User Application</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Login Id
                </label>
                <asp:Label ID="lblViewLoginId" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Application ID
                </label>
                <asp:Label ID="lblApplicationId" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active
                </label>
                <asp:Label ID="lblActive" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancelView" runat="server" Text="Cancel" CssClass="small button gray"		>
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    Edit User Application
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Login Id
                </label>
                <asp:Label ID="lblAddLoginId" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Application ID
                </label>
                <asp:DropDownList ID="cmbApplicationId" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Fill In Application ID"
                    ControlToValidate="cmbApplicationId" InitialValue="0" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active
                </label>
                <asp:CheckBox ID="chkIsActive" runat="server" Checked="True"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
          <asp:Button ID="buttonSaveAdd" runat="server"  Text="Save" CssClass="small button blue"	
                CausesValidation="True"></asp:Button>
            <asp:Button ID="buttonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"	>
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
