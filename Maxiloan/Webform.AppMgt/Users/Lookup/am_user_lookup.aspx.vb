﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region
Public Class am_user_lookup
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta and Variable"
    Private oController As New umUserController
    Private oCustomClass As New Parameter.am_user001
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property ApplicationId() As String
        Get
            Return (CType(ViewState("ApplicationId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property

    Private Property Status() As Int32
        Get
            Return (CType(ViewState("status"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("status") = Value
        End Set
    End Property
    Private Property GroupDb() As String
        Get
            Return (CType(ViewState("GroupDB"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupDB") = Value
        End Set
    End Property
    Private Property LoginIdApplication() As String
        Get
            Return (CType(ViewState("LoginApplication"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("LoginApplication") = Value
        End Set
    End Property

    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property TotalRecord() As Int64
        Get
            Return (CType(ViewState("totalrecord"), Int64))
        End Get
        Set(ByVal Value As Int64)
            ViewState("totalrecord") = Value
        End Set
    End Property

    Private Property TotalHalaman() As Double
        Get
            Return (CType(ViewState("TotalHalaman"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalHalaman") = Value
        End Set
    End Property

    Private Property Halaman() As Double
        Get
            Return (CType(ViewState("Halaman"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("Halaman") = Value
        End Set
    End Property

#End Region

#Region "Page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitialDefaultPanel()
        If Not IsPostBack Then

            Me.SearchBy = ""
            Me.SortBy = ""
            Me.Status = 0
            doBind(Me.SearchBy, Me.SortBy)
        End If
        btnSelect.Attributes.Add("onclick", "Select_Click()")
        btnCancel.Attributes.Add("onclick", "Close_Window()")
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        lblMessage.Text = ""
    End Sub

    Sub doBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListEmployee(oCustomClass)
        DtUserList = oCustomClass.ListEmployee

        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        Me.TotalRecord = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgEmployee.DataSource = DvUserList
        Try
            dtgEmployee.DataBind()
        Catch
            dtgEmployee.CurrentPageIndex = 0
            dtgEmployee.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        Me.TotalHalaman = totalPages
        If totalPages = 0 Then
           ShowMessage(lblMessage, "Data tidak ditemukan", True)
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
        Me.Halaman = currentPage
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                doBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgEmployee.SortCommand

        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Function Get_Radio(ByVal pStrId As String, ByVal pStrValue As String) As String
        Return "<input type=radio name=rbtAsset onclick=""javascript:Employee_Checked('" & Trim(pStrId) & "','" & Trim(pStrValue) & "')"" value='" & pStrValue & "'/>"
    End Function

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.SearchBy = ""
        If txtEmployeeID_src.Text <> "" Then
            If Right(txtEmployeeID_src.Text.Trim, 1) = "%" Then
                Me.SearchBy = " employeeid Like '" & txtEmployeeID_src.Text.Trim & "'"
            Else
                Me.SearchBy = " employeeid = '" & txtEmployeeID_src.Text.Trim & "'"
            End If

        End If
        If txtEmployeeName_src.Text.Trim <> "" Then
            If txtEmployeeID_src.Text = "" Then
                If Right(txtEmployeeName_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " employeename Like '" & txtEmployeeName_src.Text.Trim & "'"
                Else
                    Me.SearchBy = " employeename = '" & txtEmployeeName_src.Text.Trim & "'"
                End If
            Else
                If Right(txtEmployeeName_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy &= " and employeename Like '" & txtEmployeeName_src.Text.Trim & "'"
                Else
                    Me.SearchBy &= " and employeename = '" & txtEmployeeName_src.Text.Trim & "'"
                End If
            End If
        End If
        doBind(Me.SearchBy, Me.SortBy)
    End Sub
End Class