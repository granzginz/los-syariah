﻿Public Class ucUserLookup
    Inherits System.Web.UI.UserControl

    Public Property ObjectID() As String
        Get
            Return txtEmployeeName.ID
        End Get
        Set(ByVal Value As String)
            txtEmployeeName.ID = Value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return CType(txtEmployeeName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtEmployeeName.Text = Value
        End Set
    End Property

    Public Property EmlpoyeeID() As String
        Get
            Return hdnEmployeeID.Value
        End Get
        Set(ByVal Value As String)
            hdnEmployeeID.Value = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            hpLookup.NavigateUrl = "javascript:OpenWinEmployee('" & txtEmployeeName.ClientID & "','" & hdnEmployeeID.ClientID & "')"
            txtEmployeeName.Attributes.Add("readonly", "true")
        End If        
    End Sub

End Class

