﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_user_lookup.aspx.vb"
    Inherits="Maxiloan.Webform.AppMgt.am_user_lookup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
	<script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }

        function Employee_Checked(pStrID, pStrValue) {
            with (document.forms['form1']) {
                hdnEmployeeID.value = pStrID;
                hdnEmployeeName.value = pStrValue;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("clientid")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnEmployeeName.value;
                }
                var lObjName = '<%= Request.QueryString("clientid2")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnEmployeeID.value;
                }
            }
            window.close();
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" ForeColor="#993300" 
        ></asp:Label>&nbsp;
    <input id="hdnEmployeeID" type="hidden" name="hdnEmployeeID" runat="server" />
    <input id="hdnEmployeeName" type="hidden" name="hdnEmployeeName" runat="server" />
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    EMPLOYEE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Employee ID
                </label>
                <asp:TextBox ID="txtEmployeeID_src" runat="server" Width="115px" MaxLength="12"></asp:TextBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Employee Name
                </label>
                <asp:TextBox ID="txtEmployeeName_src" runat="server" Width="115px" MaxLength="12"></asp:TextBox>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button  blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    EMPLOYEE LISTING
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgEmployee" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="employeeid" BorderColor="#CCCCCC" BorderStyle="None"
                        BorderWidth="1px" BackColor="White" OnSortCommand="SortGrid" CellPadding="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="SELECT">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <%# Get_Radio(Container.DataItem("EmployeeId"), Container.DataItem("EmployeeName"))%>
                                    <%--<input type="radio" name="rbtAsset" onclick="Employee_Checked('<%# Container.DataItem("EmployeeId") %>','<%# Container.DataItem("EmployeeName") %>');"
                                        value='<%# Container.DataItem("EmployeeName") %>' />--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="employeeid" HeaderText="EMPLOYEE ID">
                                <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblemployeeid" runat="server" Text='<%#Container.DataItem("employeeid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="employeename" HeaderText="EMPLOYEE NAME">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblemployeename" runat="server" Text='<%#Container.DataItem("employeename")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="employeeposition" HeaderText="POSITION">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblemployee" runat="server" Text='<%#Container.DataItem("EmployeePosition")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GroupDBName" HeaderText="BRANCH">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblbranch" runat="server" Text='<%#Container.DataItem("GroupDbName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:Button ID="imbPrint" runat="server" CssClass="small buttongo blue" Text="Print"
                            CausesValidation="False"></asp:Button>
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSelect" runat="server" Text="Select" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
