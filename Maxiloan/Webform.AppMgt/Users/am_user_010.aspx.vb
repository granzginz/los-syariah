﻿Imports System.Data.SqlClient
Imports System.Data

Public Class am_user_010
    Inherits UserManagementBase

    Private oConn As SqlConnection = New SqlConnection(Me.ConnectionStringAM)
    Private oEncrypt As New Decrypt.am_futility


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            displaySecurityCode()
        End If
    End Sub

    Private Sub ImgSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessage.Text = ""
        Try
            If oConn.State = ConnectionState.Closed Then oConn.Open()

            Dim strSQL As String = "UPDATE dbo.sec_msUser SET securityCode = '" & oEncrypt.encryptto(txtNewSecurityCode.Text.Trim, "") & "' where loginID = '" & Me.Loginid & "'"
            Dim oComm As New SqlCommand(strSQL, oConn)

            oComm.ExecuteNonQuery()
            txtNewSecurityCode.Text = ""
            displaySecurityCode()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            oConn.Close()
        End Try
    End Sub

    Private Sub displaySecurityCode()
        Try
            If oConn.State = ConnectionState.Closed Then oConn.Open()

            Dim strSQL As String = "SELECT ISNULL(securityCode,'') from dbo.sec_msUser where loginID = '" & Me.Loginid & "'"
            Dim oComm As New SqlCommand(strSQL, oConn)

            Dim strPassword As String = CStr(oComm.ExecuteScalar)

            lblCurrentSecurityCode.Text = oEncrypt.encryptto(strPassword, "1")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            oConn.Close()
        End Try
    End Sub

End Class