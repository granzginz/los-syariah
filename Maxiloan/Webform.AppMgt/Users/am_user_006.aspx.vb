﻿
#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_user_006
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta"
    Private oController As New UMUserApplicationController
    Private oCustomClass As New Parameter.UserApplication
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"

    Private Property ListMasterApplication() As DataTable
        Get
            Return (CType(Viewstate("MasterApplication"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            viewstate("MasterApplication") = Value
        End Set
    End Property
    Private Property LoginIdApplication() As String
        Get
            Return (CType(Viewstate("LoginApplication"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("LoginApplication") = Value
        End Set
    End Property

    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        'imbBack.Attributes.Add("OnClick", "return fback()")
        Me.LoginIdApplication = Request.QueryString("loginid")
        lblloginid.Text = Me.LoginIdApplication
        InitialDefaultPanel()
        If Not IsPostBack Then
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlView.Visible = False
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .LoginId = Me.LoginIdApplication
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListUserApplication(oCustomClass)

        Me.ListMasterApplication = oCustomClass.ListMasterApplication
        DtUserList = oCustomClass.ListUserApplication

        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgUsrApp.DataSource = DvUserList
        Try
            dtgUsrApp.DataBind()
        Catch
            dtgUsrApp.CurrentPageIndex = 0
            dtgUsrApp.DataBind()
        End Try
        lblloginid.Text = Me.LoginIdApplication
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"

#Region "Command"
    Private Sub DtgUserList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgUsrApp.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                Dim hypName As LinkButton
                hypName = CType(dtgUsrApp.Items(e.Item.ItemIndex).FindControl("lnkApplicationId"), LinkButton)

                Dim lblAct As Label
                lblAct = CType(dtgUsrApp.Items(e.Item.ItemIndex).FindControl("lblAct"), Label)
                lblAddLoginId.Text = Me.LoginIdApplication.Trim
                cmbApplicationId.DataSource = Me.ListMasterApplication
                cmbApplicationId.DataTextField = "ApplicationName"
                cmbApplicationId.DataValueField = "ApplicationId"
                cmbApplicationId.DataBind()

                cmbApplicationId.Items.FindByValue(hypName.Text).Selected = True
                pnlList.Visible = False
                pnlAdd.Visible = True
                pnlView.Visible = False
                cmbApplicationId.Enabled = False
                chkIsActive.Checked = CType(IIf(lblAct.Text.Trim = "Yes", True, False), Boolean)
                buttonCancelAdd.Attributes.Add("onclick", "return fback()")
            Case "DELETE"
                Dim hypName As LinkButton
                hypName = CType(dtgUsrApp.Items(e.Item.ItemIndex).FindControl("lnkApplicationId"), LinkButton)

                oCustomClass.LoginId = Me.LoginIdApplication
                oCustomClass.ApplicationId = hypname.Text

                Dim err As String = oController.DeleteUserApplication(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            Case "ShowView"
                Dim hypID As LinkButton
                hypID = CType(dtgUsrApp.Items(e.Item.ItemIndex).FindControl("lnkApplicationId"), LinkButton)
                Dim lblAct As Label
                lblAct = CType(dtgUsrApp.Items(e.Item.ItemIndex).FindControl("lblAct"), Label)
                lblViewLoginId.Text = Me.LoginIdApplication
                lblApplicationId.Text = hypID.Text.Trim

                lblActive.Text = lblact.Text
                pnlView.Visible = True
                pnlList.Visible = False
                pnlAdd.Visible = False
                btnCancelView.Attributes.Add("onclick", "return fback()")
        End Select
    End Sub
#End Region

#Region "DataBound"
    Private Sub dtgUsrApp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgUsrApp.ItemDataBound
        Dim lnkApplicationId As New LinkButton
        Dim hypUserGroupData As New HyperLink

        If e.Item.ItemIndex >= 0 Then
            lnkApplicationId = CType(e.Item.FindControl("lnkApplicationId"), LinkButton)
            hypUserGroupData = CType(e.Item.FindControl("hypUserGroupData"), HyperLink)
            hypUserGroupData.NavigateUrl = "../groups/am_group_001.aspx?loginid=" & Me.LoginIdApplication & "&appid=" & lnkApplicationId.Text
        End If
        lnkApplicationId.Dispose()
        hypUserGroupData.Dispose()
    End Sub
#End Region

#Region "SortGrid"
    Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgUsrApp.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#End Region

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSaveAdd.Click
        pnlList.Visible = True
        pnlView.Visible = False
        pnlAdd.Visible = False
        If Page.IsValid Then
            Dim LObjencrypt As New Decrypt.am_futility

            With oCustomClass
                .LoginId = lblAddLoginId.Text.Trim
                .ApplicationId = cmbApplicationId.SelectedItem.Value.Trim
                .Order = "1"
                .isActive = chkIsActive.Checked
            End With
            If Me.Mode = "EDIT" Then

                Dim err As String = oController.UpdateUserApplication(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
            ElseIf Me.Mode = "ADD" Then

                Dim err As String = oController.AddUserApplication(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
            End If
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Server.Transfer("am_user_001.aspx")
    End Sub

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Me.Mode = "ADD"
        lblAddLoginId.Text = Me.LoginIdApplication.Trim
        cmbApplicationId.DataSource = Me.ListMasterApplication
        cmbApplicationId.DataTextField = "ApplicationName"
        cmbApplicationId.DataValueField = "ApplicationId"
        cmbApplicationId.DataBind()
        cmbApplicationId.Items.Insert(0, "Select One")
        cmbApplicationId.Items(0).Value = "0"
        cmbApplicationId.SelectedIndex = 0
        cmbApplicationId.Enabled = True
        pnlList.Visible = False
        pnlAdd.Visible = True
        pnlView.Visible = False
        chkIsActive.Checked = True
        buttonCancelAdd.Attributes.Add("onclick", "return fback()")
    End Sub

End Class