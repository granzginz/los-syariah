﻿Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Decrypt
Imports Maxiloan.Controller
Imports Maxiloan.SQLEngine.DataAccessBase
Imports System.IO

Public Class am_user_001
    Inherits Maxiloan.Webform.WebBased
    Private oUser001Controller As New UMUserController

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Dim strSortBy As String
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        If Not IsPostBack Then
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlView.Visible = False
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim oUserList As New Parameter.am_user001

        With oUserList
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oUserList = oUser001Controller.ListUserList(oUserList)

        DtUserList = oUserList.ListUserList
        DvUserList = DtUserList.DefaultView
        recordCount = oUserList.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgUserList.DataSource = DvUserList
        Try
            DtgUserList.DataBind()
        Catch
            DtgUserList.CurrentPageIndex = 0
            DtgUserList.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"

    Private Sub DtgUserList_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgUserList.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                Me.Mode = "EDIT"
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkLoginId"), LinkButton)
                Dim hypName As LinkButton
                hypName = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkLoginName"), LinkButton)
                Dim lblAct As Label
                lblAct = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblAct"), Label)
                Dim lblPwdEdit As Label
                lblPwdEdit = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblpwd"), Label)
                Dim lblConEdit As Label
                lblConEdit = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblConnect"), Label)
                pnlList.Visible = False
                pnlAdd.Visible = True
                pnlView.Visible = False
                pnlPassword.Visible = False
                txtLoginId.Text = hypID.Text.Trim
                txtLoginName.Text = hypName.Text.Trim
                txtPassword.TextMode = TextBoxMode.Password
                txtConfirmPassword.TextMode = TextBoxMode.Password
                txtPassword.Text = lblPwdEdit.Text
                txtConfirmPassword.Text = lblPwdEdit.Text
                txtLoginId.ReadOnly = True
                txtPassword.ReadOnly = True
                txtConfirmPassword.ReadOnly = True
                chkIsActive.Checked = CType(IIf(lblAct.Text.Trim = "Yes", True, False), Boolean)
                pnlConnect.Visible = True
                chkIsConnect.Checked = CType(IIf(lblConEdit.Text.Trim = "Yes", True, False), Boolean)
                Requiredfieldvalidator3.Enabled = False
                Requiredfieldvalidator4.Enabled = False
                lblTitle.Text = "EDIT MASTER USER"
            Case "DELETE"
                Dim oCustomClass As New Parameter.am_user001
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkLoginId"), LinkButton)
                oCustomClass.LoginId = hypID.Text
                Try

                    'Dim err As String = oUser001Controller.DeleteUserList(oCustomClass)
                    oCustomClass = oUser001Controller.DeleteUserList(oCustomClass)
                    If oCustomClass.strError <> String.Empty Then
                        ShowMessage(lblMessage, oCustomClass.strError, False)
                    End If
                    DoBind(Me.SearchBy, Me.SortBy)
                Catch exp As Exception
                    ShowMessage(lblMessage, "Gagal menghapus data, karena sedang log on ", True)

                End Try

                'delete file xml menu, sementara ini khusus untuk application maxiloan             
                If Not oCustomClass.ListUserGroupDB Is Nothing Then
                    Dim XMLFileName As String

                    For Each r As DataRow In oCustomClass.ListUserGroupDB.Rows
                        XMLFileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "Webform.AppMgt\Menu\" & "Menu_maxiloan_" & _
                            Trim(oCustomClass.LoginId) & "_" & r("GroupDBID").ToString & ".xml"
                        File.Delete(XMLFileName)
                    Next
                End If
               

            Case "ShowView"
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkLoginId"), LinkButton)
                Dim hypName As LinkButton
                hypName = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkLoginName"), LinkButton)
                Dim lblAct As Label
                lblAct = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblAct"), Label)

                lblLoginId.Text = hypID.Text
                lblLoginName.Text = hypName.Text
                lblAreaGroup.Text = ""
                lblUserLevel.Text = ""
                lblIsActive.Text = lblAct.Text
                pnlView.Visible = True
                pnlList.Visible = False
                pnlAdd.Visible = False
            Case "RESETPASSWORD"
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkLoginId"), LinkButton)
                Response.Redirect("am_user_002.aspx?LoginID=" & hypID.Text)
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        pnlAdd.Visible = True
        pnlView.Visible = False
        pnlList.Visible = False
        txtLoginId.Text = ""
        txtLoginName.Text = ""
        txtPassword.Text = ""
        txtConfirmPassword.Text = ""
        Me.Mode = "ADD"
        chkIsActive.Checked = True
        txtLoginId.ReadOnly = False
        txtPassword.ReadOnly = False
        lblTitle.Text = "ADD MASTER USER"
        pnlPassword.Visible = True
        txtConfirmPassword.ReadOnly = False
        txtPassword.TextMode = TextBoxMode.Password
        txtConfirmPassword.TextMode = TextBoxMode.Password
        Requiredfieldvalidator3.Enabled = True
        Requiredfieldvalidator4.Enabled = True
        pnlConnect.Visible = False
        txtConfirmPassword.Attributes.Add("onblur", "javascript:cekpassword()")
        'btnCancelAdd.Attributes.Add("onclick", "return fback()")
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSaveAdd.Click
        pnlList.Visible = True
        Dim lobjEncrpyt As New Decrypt.am_futility
        pnlView.Visible = False
        pnlAdd.Visible = False
        If Page.IsValid Then
            Dim oCustomClass As New Parameter.am_user001
            With oCustomClass
                .LoginId = txtLoginId.Text.Trim
                .FullName = txtLoginName.Text
                .Password = lobjEncrpyt.encryptto(Trim(txtPassword.Text), "")
                .isActive = chkIsActive.Checked
                .EmployeeID = txtLoginName.EmlpoyeeID
                .UserLevel = "02"
                .UserCreate = Me.Loginid
                .DBName = getReferenceDBName()
                If Me.Mode = "EDIT" Then
                    .isConnect = chkIsConnect.Checked
                End If
            End With
            If Me.Mode = "ADD" Then

                Dim err As String = oUser001Controller.AddUserList(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
            Else
                If Me.Mode = "EDIT" Then

                    Dim err As String = oUser001Controller.UpdateUserList(oCustomClass)
                    If err <> String.Empty Then
                        ShowMessage(lblMessage, err, True)
                    End If
                End If
            End If
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If txtloginid_src.Text.Trim <> "" Then
            Me.SearchBy = " loginid = '" & txtloginid_src.Text.Trim.Replace("'", "''") & "'"
        End If
        If txtloginname_src.Text.Trim <> "" Then
            If Me.SearchBy <> "" Then
                Me.SearchBy = Me.SearchBy & " and fullname = '" & txtloginname_src.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy = " fullname = '" & txtloginname_src.Text.Trim.Replace("'", "''") & "'"
            End If
        End If
        If txtloginid_src.Text.Trim = "" And txtloginname_src.Text.Trim.Replace("'", "''") = "" Then
            Me.SearchBy = ""
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonCancelAdd.Click
        Response.Redirect("am_user_001.aspx")
    End Sub

    Private Sub imbCancelView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelView.Click
        Response.Redirect("am_user_001.aspx")
    End Sub

End Class