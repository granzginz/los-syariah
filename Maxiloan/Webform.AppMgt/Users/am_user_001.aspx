﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_user_001.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.am_user_001" %>

<%@ Register TagPrefix="uc1" TagName="UcUserLookup" Src="Lookup/UcUserLookup.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>


     <!-- Global stylesheets -->
    <link href="lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/core/app.js"></script>

    <script type="text/javascript" src="lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>
    <!-- /core JS files -->
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                List of Master User</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgUserList" runat="server" OnSortCommand="SortGrid" CssClass="grid_general"
                        DataKeyField="loginid" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True"
                        ShowFooter="false" PagerStyle-Visible="false">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn SortExpression="loginid" HeaderText="LOGIN ID">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLoginId" runat="server" Text='<%#Container.DataItem("LoginId")%>'
                                        CommandName="ShowView">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="fullname" HeaderText="FULL NAME">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLoginName" runat="server" Text='<%#Container.DataItem("FullName")%>'
                                        CommandName="ShowView">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="USER APPLICATION">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="Hyperlink2" runat="server" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.loginid", "../users/am_user_006.aspx?loginid={0}")%>'
                                        ImageUrl="../../Images/IconReceived.gif">
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="USER FEATURE">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="Hyperlink5" runat="server" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.loginid", "../feature/am_feature.aspx?loginid={0}")%>'
                                        ImageUrl="../../Images/IconReceived.gif">
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="THINGS TO DO">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="Hyperlink3" runat="server" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.loginid", "../forms/UserAlert.aspx?ftrloginid={0}") %>'
                                        ImageUrl="../../Images/IconReceived.gif">
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="USER INFO BOX">
                                <ItemTemplate>
                                    <asp:HyperLink ID="Hyperlink4" runat="server" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.loginid", "../forms/UserInfoBox.aspx?ftrloginid={0}")  %>'
                                        ImageUrl="../../Images/IconReceived.gif">
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RESET PASSWORD">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgChangePassword" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="RESETPASSWORD"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="IsActive" Visible="False">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAct" runat="server" Text='<%#Container.DataItem("Active")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Password" Visible="False">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblpwd" runat="server" Text='<%#Container.DataItem("Password")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="IsConnect" Visible="False">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblConnect" runat="server" Text='<%#Container.DataItem("IsConnect")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Display="Dynamic"
                            Type="Integer" MaximumValue="999999999" ErrorMessage="Halaman tidak valid!" MinimumValue="1"
                            ControlToValidate="txtPage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                            ErrorMessage="Halaman tidak valid!" ControlToValidate="txtPage" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    Find Master User
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Login Id
                    </label>
                    <asp:TextBox ID="txtloginid_src" runat="server" CssClass="no_text_transf"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>
                        Full Name
                    </label>
                    <asp:TextBox ID="txtloginname_src" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <label>
                    View Master User
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Login Id
                </label>
                <asp:Label ID="lblLoginId" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Full Name
                </label>
                <asp:Label ID="lblLoginName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    User Level
                </label>
                <asp:Label ID="lblUserLevel" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Area / Collection Group
                </label>
                <asp:Label ID="lblAreaGroup" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active
                </label>
                <asp:Label ID="lblIsActive" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancelView" runat="server" Text="Cancel" CssClass="small button gray"
                onclik="fBack" CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <label>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Login Id
                </label>
                <asp:TextBox ID="txtLoginId" runat="server" MaxLength="12" CssClass="no_text_transf"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Full Name
                </label>
                <uc1:UcUserLookup ID="txtLoginName" runat="server"></uc1:UcUserLookup>
            </div>
        </div>
        <asp:Panel ID="pnlPassword" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Password
                    </label>
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="10"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Confirm Password
                    </label>
                    <asp:TextBox ID="txtConfirmPassword" runat="server" MaxLength="10" onchange="cekpassword()"></asp:TextBox>
                </div>
            </div>
        </asp:Panel>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active
                </label>
                <asp:CheckBox ID="chkIsActive" runat="server" Checked="True"></asp:CheckBox>
            </div>
        </div>
        <asp:Panel ID="pnlConnect" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Is Connect
                    </label>
                    <asp:CheckBox ID="chkIsConnect" runat="server" Checked="True"></asp:CheckBox>
                </div>
            </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="buttonSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="buttonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Please Fill LoginID"
                ControlToValidate="txtLoginId" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" CssClass="validator_general"
                ErrorMessage="Please Fill Password" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" CssClass="validator_general"
                ErrorMessage="Please Fill Password Confirm" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
