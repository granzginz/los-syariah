﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_user_002.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.am_user_002" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />

     <!-- Global stylesheets -->
    <link href="../../lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
	<script type="text/javascript" src="../../lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

    <script type="text/javascript" src="../../lib/limitless/assets/js/core/app.js"></script>
	<!-- Theme JS files -->


    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="form_single">
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <h3 runat="server" id="lblTitle">User Password</h3>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlPass">

            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Login ID
                    </label>
                    <asp:TextBox ID="txtloginid" runat="server" MaxLength="12"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Rfvloginid" runat="server" ErrorMessage="Login ID tidak boleh kosong!"
                        ControlToValidate="txtloginid" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Password Lama
                    </label>
                    <asp:TextBox ID="txtpassword" runat="server" MaxLength="10" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvPassword" runat="server" ErrorMessage="Password tidak boleh kosong!"
                        Display="Dynamic" ControlToValidate="txtpassword" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        New Password
                    </label>
                    <asp:TextBox ID="txtnewpassword" runat="server" MaxLength="10" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Rfvnewpassword" runat="server" ErrorMessage="Password baru tidak boleh kosong!"
                        ControlToValidate="txtnewpassword" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Confirm Password
                    </label>
                    <asp:TextBox ID="txtconfirmpassword" runat="server" MaxLength="10" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ID="Cmvconfirmpassword" runat="server" ErrorMessage="Password tidak sama!"
                        ControlToValidate="txtconfirmpassword" ControlToCompare="txtnewpassword" Display="Dynamic" CssClass="validator_general"></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RfvConfirm" runat="server" ErrorMessage="Password baru tidak boleh kosong!"
                        Display="Dynamic" ControlToValidate="txtconfirmpassword" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
