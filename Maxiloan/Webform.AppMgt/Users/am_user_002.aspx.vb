﻿Imports Maxiloan
Imports System.Data.SqlClient
Imports Maxiloan.Controller
Public Class am_user_002
    Inherits Maxiloan.Webform.UserManagementBase
    Private GObjConn As SqlConnection = New SqlConnection(Me.ConnectionStringAM)
    Private GObjDataset As DataSet
    Private GObjDataView As DataView

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim intLength As Int16
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim Objreg As New am_com_001
            If CStr(Session("Expired")) = "T" Then
                btnCancel.Visible = False
            Else
                btnCancel.Visible = True
            End If

            intLength = 10
            txtpassword.MaxLength = intLength
            txtnewpassword.MaxLength = intLength
            txtconfirmpassword.MaxLength = intLength
            txtloginid.Text = IIf(Request("loginid") = "", CStr(Session("loginid")), Request("LoginID")).ToString

            If Me.Loginid.ToUpper = "ADMIN" Then
                lblTitle.InnerText = "RESET PASSWORD"
            Else
                lblTitle.InnerText = "GANTI PASSWORD"
            End If

            txtloginid.ReadOnly = True
            ControlDisplayProperty()
        End If
    End Sub
    Sub ControlDisplayProperty()
        If Request("loginid") = "" Then
            txtpassword.Enabled = True
            RfvPassword.Enabled = True
            btnSave.Visible = True
            btnReset.Visible = False
            btnCancel.Visible = False
        Else
            txtpassword.Enabled = False
            RfvPassword.Enabled = False
            btnSave.Visible = False
            btnReset.Visible = True
            btnCancel.Visible = False
        End If
    End Sub
    Private Sub ImgSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim intStatus As Short
        Dim Objreg As New am_com_001
        Dim oController As New LoginController
        Dim oCustomClass As New Parameter.Login
        lblMessage.Visible = True
        oCustomClass = oController.PasswordSetting()

        If Objreg.isValidSuperUser(Trim(txtloginid.Text), Trim(txtpassword.Text)) Then
            Session("password") = Trim(txtnewpassword.Text)
            ShowMessage(lblMessage, "Super User Password has been change !", True)
        Else
            ShowMessage(lblMessage, "Super User Password , Fail to change !", True)
        End If

        intStatus = Objreg.CheckPassword(Trim(txtloginid.Text), Trim(txtnewpassword.Text))

        If intStatus = 0 Then
            ShowMessage(lblMessage, "Error occured , Ask your Administrator!", True)
            Exit Sub
        ElseIf intStatus = 2 Then
            ShowMessage(lblMessage, "New Password Must Not Equal With LoginID!", True)
            Exit Sub
        ElseIf intStatus = 3 Then
            ShowMessage(lblMessage, "New Password Must Not Equal With Old Password!", True)
            Exit Sub
        ElseIf intStatus = 4 Then
            ShowMessage(lblMessage, "New Password Must Not In Old Password!", True)
            Exit Sub
        ElseIf intStatus = 5 Then
            ShowMessage(lblMessage, "Password minimal " & oCustomClass.PwdLength.ToString & " Character!", True)
            Exit Sub
        End If

        If GObjConn.State = ConnectionState.Closed Then GObjConn.Open()
        Dim ObjDatasource As New SqlDataAdapter("select loginid,password from sec_msuser " & _
                                                "where loginid = '" & Trim(txtloginid.Text) & "'", GObjConn)
        GObjDataset = New DataSet
        ObjDatasource.Fill(GObjDataset, "User")
        If (GObjDataset.Tables("user").Rows.Count > 0) Then
            Dim Objrow As DataRowView = GObjDataset.Tables("user").DefaultView(0)
            Dim objutility As New Decrypt.am_futility
            Dim Lstrpassword As String = objutility.encryptto(CStr(Objrow("password")), "1")
            If UCase(Trim(Lstrpassword)) = UCase(Trim(txtpassword.Text)) Then
                'Dim Lstr As String = "update sec_msuser set password=@password , ExpiredDate =@ExpiredDate where loginid=@loginid"
                'Dim objcommand As New SqlCommand(Lstr, GObjConn)
                'objcommand.Parameters.Add(New SqlParameter("@password", SqlDbType.Char, 20))
                'objcommand.Parameters("@password").Value = objutility.encryptto(Trim(txtnewpassword.Text), "")

                'objcommand.Parameters.Add(New SqlParameter("@loginid", SqlDbType.Char, 12))
                'objcommand.Parameters("@loginid").Value = Trim(txtloginid.Text)

                'objcommand.Parameters.Add(New SqlParameter("@ExpiredDate", SqlDbType.DateTime))
                'objcommand.Parameters("@ExpiredDate").Value = Me.BusinessDate.AddDays(30)


                Dim Lstr As String = "exec dbo.updateuserpass @password, @loginid, @businessdate"
                Dim objcommand As New SqlCommand(Lstr, GObjConn)
                objcommand.Parameters.Add(New SqlParameter("@password", SqlDbType.Char, 20))
                objcommand.Parameters("@password").Value = objutility.encryptto(Trim(txtnewpassword.Text), "")

                objcommand.Parameters.Add(New SqlParameter("@loginid", SqlDbType.Char, 12))
                objcommand.Parameters("@loginid").Value = Trim(txtloginid.Text)

                objcommand.Parameters.Add(New SqlParameter("@businessdate", SqlDbType.DateTime))
                objcommand.Parameters("@businessdate").Value = Me.BusinessDate

                Try
                    objcommand.ExecuteNonQuery()
                    ShowMessage(lblMessage, "Login berikutnya gunakan password baru!", False)
                    pnlPass.Visible = False
                    Session("password") = Trim(txtnewpassword.Text)
                Catch Exp As SqlException
                    ShowMessage(lblMessage, "Could not update Password", True)
                End Try
                'insert into changepassword logs
                Lstr = "insert into sec_changepasswordlogs (loginid,passwordold,passwordnew,changeby) values(@loginid,@passwordold,@passwordnew,@changeby)"
                objcommand = New SqlCommand(Lstr, GObjConn)
                objcommand.Parameters.Add(New SqlParameter("@passwordold", SqlDbType.Char, 20))
                objcommand.Parameters("@passwordold").Value = objutility.encryptto(Trim(txtpassword.Text), "")

                objcommand.Parameters.Add(New SqlParameter("@loginid", SqlDbType.Char, 12))
                objcommand.Parameters("@loginid").Value = Trim(txtloginid.Text)

                objcommand.Parameters.Add(New SqlParameter("@passwordnew", SqlDbType.Char, 20))
                objcommand.Parameters("@passwordnew").Value = objutility.encryptto(Trim(txtnewpassword.Text), "")

                objcommand.Parameters.Add(New SqlParameter("@changeby", SqlDbType.Char, 12))
                objcommand.Parameters("@changeby").Value = Trim(txtloginid.Text)

                Try
                    objcommand.ExecuteNonQuery()
                Catch Exp As SqlException

                End Try
            Else
                ShowMessage(lblMessage, "Invalid Password", True)
            End If
        Else
            ShowMessage(lblMessage, "Invalid Login ID", True)
        End If

        GObjConn.Close()
    End Sub

    Private Sub ImgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Server.Transfer("../../am_home.aspx")
    End Sub

    Private Sub imgReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Dim intStatus As Int16
        Dim Objreg As New am_com_001
        Dim oController As New LoginController
        Dim oCustomClass As New Parameter.Login

        lblMessage.Visible = True
        oCustomClass = oController.PasswordSetting()
        intStatus = Objreg.CheckPassword(Trim(txtloginid.Text), Trim(txtnewpassword.Text))

        If intStatus = 0 Then
            ShowMessage(lblMessage, "Error occured , Ask your Administrator!", True)
            Exit Sub
        ElseIf intStatus = 2 Then
            ShowMessage(lblMessage, "New Password Must Not Equal With LoginID!", True)
            Exit Sub
        ElseIf intStatus = 3 Then
            ShowMessage(lblMessage, "New Password Must Not Equal With Old Password!", True)
            Exit Sub
        ElseIf intStatus = 4 Then
            ShowMessage(lblMessage, "New Password Must Not In Old Password!", True)
            Exit Sub
        ElseIf intStatus = 5 Then
            ShowMessage(lblMessage, "Password minimal " & oCustomClass.PwdLength.ToString & " Character!", True)
            Exit Sub
        End If

        If GObjConn.State = ConnectionState.Closed Then GObjConn.Open()

        Dim ObjDatasource As New SqlDataAdapter _
        ("select loginid,password from sec_msuser " & _
            "where loginid = '" & Trim(txtloginid.Text) & "'", GObjConn)
        Dim objutility As New Decrypt.am_futility

        GObjDataset = New DataSet
        ObjDatasource.Fill(GObjDataset, "User")

        If (GObjDataset.Tables("user").Rows.Count > 0) Then

            Dim Lstr As String = "update sec_msuser set password=@password , ExpiredDate =DATEADD(d,(select PwdExpired from sec_msdatabase where applicationid = 'MAXILOAN'),GETDATE()) where loginid=@loginid"
            Dim objcommand As New SqlCommand(Lstr, GObjConn)

            objcommand.Parameters.Add(New SqlParameter("@password", SqlDbType.Char, 20))
            objcommand.Parameters("@password").Value = objutility.encryptto(Trim(txtnewpassword.Text), "")

            objcommand.Parameters.Add(New SqlParameter("@loginid", SqlDbType.Char, 12))
            objcommand.Parameters("@loginid").Value = Trim(txtloginid.Text)

            Try
                objcommand.ExecuteNonQuery()
                ShowMessage(lblMessage, "Login berikutnya gunakan password baru!", False)
                pnlPass.Visible = False
                Session("password") = Trim(txtnewpassword.Text)
            Catch Exp As SqlException
                ShowMessage(lblMessage, "Could not update Password", True)
            End Try

            Lstr = "insert into sec_changepasswordlogs (loginid,passwordold,passwordnew,changeby) values(@loginid,@passwordold,@passwordnew,@changeby)"
            objcommand = New SqlCommand(Lstr, GObjConn)
            objcommand.Parameters.Add(New SqlParameter("@passwordold", SqlDbType.Char, 20))
            objcommand.Parameters("@passwordold").Value = objutility.encryptto(Trim(txtpassword.Text), "")

            objcommand.Parameters.Add(New SqlParameter("@loginid", SqlDbType.Char, 12))
            objcommand.Parameters("@loginid").Value = Trim(txtloginid.Text)

            objcommand.Parameters.Add(New SqlParameter("@passwordnew", SqlDbType.Char, 20))
            objcommand.Parameters("@passwordnew").Value = objutility.encryptto(Trim(txtnewpassword.Text), "")

            objcommand.Parameters.Add(New SqlParameter("@changeby", SqlDbType.Char, 12))
            objcommand.Parameters("@changeby").Value = Trim(txtloginid.Text)

            Try
                objcommand.ExecuteNonQuery()
            Catch Exp As SqlException
                ShowMessage(lblMessage, Exp.Message, True)
            End Try
        Else
            ShowMessage(lblMessage, "Invalid Login ID", True)
        End If

        GObjConn.Close()
        Response.Redirect("am_user_001.aspx")
    End Sub

End Class