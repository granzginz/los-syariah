﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_user_010.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.am_user_010" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Security Code</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                SECURITY CODE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Current Security Code</label>
            <asp:Label ID="lblCurrentSecurityCode" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                New Security Code
            </label>
            <asp:TextBox ID="txtNewSecurityCode" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Rfvloginid" runat="server" Display="Dynamic" ControlToValidate="txtNewSecurityCode"
                ErrorMessage="Type new security code!" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
    </div>
    </form>
</body>
</html>
