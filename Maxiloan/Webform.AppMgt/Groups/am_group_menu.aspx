﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_group_menu.aspx.vb"
    Inherits="Maxiloan.Webform.AppMgt.am_group_menu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function chekAppIsiId(source, args) {
            if (args.Value == "0") {
                args.IsValid = false
            } else {
                args.IsValid = true
            }
        }
        function fback() {
            history.back(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                Group Menu</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Login ID
                </label>
                <asp:Label ID="lblloginid" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Application ID
                </label>
                <asp:Label ID="lblApplicationId" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Data
                </label>
                <asp:Label ID="lblGroupData" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgUsrGroupMenu" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="GroupId" CssClass="grid_general" OnSortCommand="SortGrid" CellPadding="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="GroupDbId">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkUserGroupMenu" runat="server" Checked='<%#iif(Container.Dataitem("UserGroupMenu")="1", True, False)%>'>
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GroupName" HeaderText="Group Menu Name">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblGroupMenuName" runat="server" Text='<%#Container.DataItem("GroupName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="GroupId" HeaderText="Group Menu ID">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblGroupMenuId" runat="server" Text='<%#Container.DataItem("GroupId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" ControlToValidate="txtPage"
                            MinimumValue="1" ErrorMessage="Halaman tidak valid!" MaximumValue="999999999"
                            Display="Dynamic" Type="Integer"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                            ControlToValidate="txtPage" ErrorMessage="Halaman tidak valid!" Display="Dynamic"></asp:RequiredFieldValidator></tr>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
