﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_group_user.aspx.vb"
    Inherits="Maxiloan.Webform.AppMgt.am_group_user" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function cekpassword() {
            var a = document.Form1.txtConfirmPassword.value;
            var b = document.Form1.txtPassword.value;
            if (a != b) {
                alert('The Password you type not matched');
            }
        };
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                User Group</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    Find User Group
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Application ID
                </label>
                <asp:DropDownList ID="cmbApplicationId" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Application ID"
                    ControlToValidate="cmbApplicationId" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group ID
                </label>
                <asp:TextBox ID="txtgroupidsrc" runat="server" MaxLength="12"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
        </div>
        <asp:Panel ID="pnlGrid" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        List Of User Group
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgUserList" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                            DataKeyField="Groupid" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn SortExpression="groupid" HeaderText="GROUP ID">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblGroupId" runat="server" Text='<%#Container.DataItem("GroupId")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="groupname" HeaderText="GROUP NAME">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblgroupname" runat="server" Text='<%#Container.DataItem("GroupName")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="GROUP MENU">
                                    <HeaderStyle CssClass="th_center"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypGroupMenu" runat="server" ImageUrl="../../Images/IconReceived.gif"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="GROUP FEATURE" Visible="False">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypGroupFeature" runat="server" ImageUrl="../../Images/IconReceived.gif"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <HeaderStyle CssClass="th_center"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                            CommandName="EDIT"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <HeaderStyle CssClass="th_center"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                            CommandName="DELETE"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" ErrorMessage="Halaman tidak valid!"
                                ControlToValidate="txtPage" Type="Integer" MaximumValue="999999999" MinimumValue="1"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Halaman tidak valid!"
                                ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="small button blue"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Application Id
                </label>
                <asp:Label ID="lblApplicationID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Id
                </label>
                <asp:TextBox ID="txtGroupId" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Please Fill GroupID"
                    ControlToValidate="txtGroupId" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Group Name
                </label>
                <asp:TextBox ID="txtGroupName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Please Fill Group Name"
                    ControlToValidate="txtGroupName" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="buttonSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="buttonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
