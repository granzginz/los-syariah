﻿Imports System.Data.SqlClient
Imports System.Xml

Public Class am_group_002
    Inherits Maxiloan.Webform.UserManagementBase
    Private GObjDataGroup As DataTable = New DataTable
    Private GObjDataMenu As DataTable = New DataTable
    Private GObjCon As SqlConnection
    Private strNodeIDChanged As String = ""

#Region "Property"
    Private Property Applicationid() As String
        Get
            Return (CType(ViewState("applicationid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("applicationid") = Value
        End Set
    End Property

    Private Property GroupID() As String
        Get
            Return (CType(ViewState("GroupID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GObjCon = New SqlConnection(Me.ConnectionStringAM)

        Dim xmlDoc As XmlDocument = New XmlDocument
        Me.Applicationid = Request.QueryString("appid")
        Me.GroupID = Request.QueryString("groupid")
        LoadDBToDS()
        If Not Page.IsPostBack Then
            TreeView1.Attributes.Add("onclick", "OnTreeClick(event)")
            lblGroupId.Text = Me.GroupID
            lblApplicationID.Text = Me.Applicationid
            bindTreeView()
        End If
    End Sub

    Private Sub LoadDBToDS()
        Dim LObjPrimary(2) As DataColumn
        Dim objcommand As New SqlCommand
        Dim strQuery As String
        Dim LObjDtAdpt As New SqlDataAdapter
        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        objcommand.Connection = GObjCon

        strQuery = "select MenuId, parentmenuid, prompt, side, level_, result, " & _
                    " command, formid, Active, order_ , Applicationid, Subsystemid from dbo.sec_msmenu where applicationid=@applicationid"
        objcommand.CommandText = strQuery
        objcommand.Parameters.Add("@applicationid", SqlDbType.VarChar, 10).Value = Me.Applicationid
        LObjDtAdpt.SelectCommand = objcommand
        LObjDtAdpt.Fill(GObjDataMenu)
        LObjPrimary(0) = GObjDataMenu.Columns("menuid")
        GObjDataMenu.PrimaryKey = LObjPrimary


        objcommand.Parameters.Clear()
        strQuery = "select Groupid, Menuid, Applicationid from dbo.sec_menuofgroup where groupid =@groupid"
        objcommand.CommandText = strQuery
        objcommand.Parameters.Add("@groupid", SqlDbType.VarChar, 10).Value = Me.GroupID
        LObjDtAdpt.SelectCommand = objcommand
        LObjDtAdpt.Fill(GObjDataGroup)

        LObjPrimary(0) = GObjDataGroup.Columns("Groupid")
        LObjPrimary(1) = GObjDataGroup.Columns("menuid")
        LObjPrimary(2) = GObjDataGroup.Columns("Applicationid")
        GObjDataGroup.PrimaryKey = LObjPrimary

        objcommand.Parameters.Clear()
        objcommand.Dispose()
        LObjDtAdpt.Dispose()
    End Sub

    Private Function createMenuXml(ByVal applicationId As String) As String
        Dim LStrXml As String
        Dim LObjDataMenuFounds As DataRow()

        LObjDataMenuFounds = (GObjDataMenu.Select("applicationid='" + applicationId + "' and parentmenuid='root' ", " order_ "))
        LStrXml = "<?xml version='1.0'?>" & _
                "<MsMenu>"
        createMenuXmlItem(LStrXml, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
        LStrXml = LStrXml & "</MsMenu>"
        createMenuXml = LStrXml
    End Function

    Private Sub createMenuXmlItem(ByRef xmlString As String, ByVal DataFounds As DataRow(), ByRef starting As Integer, ByVal ending As Integer, ByVal applicationId As String)
        Dim LObjDataMenuFounds As DataRow()
        Dim LObjDataSelectMenuFounds As DataRow()
        Dim LStrMenuID As String
        While starting <= ending
            LStrMenuID = CType(DataFounds(starting)("menuid"), String)
            LObjDataSelectMenuFounds = GObjDataGroup.Select("menuid = '" + LStrMenuID + "' and applicationid ='" + applicationId + "'")
            xmlString = xmlString & "<MsMenuItem menuid='" & LStrMenuID & "' prompt='" & CType(DataFounds(starting)("prompt"), String) & "' checked='" & CType(IIf(LObjDataSelectMenuFounds.Length = 0, "false", "true"), String) & "'>"
            LObjDataMenuFounds = (GObjDataMenu.Select("applicationid='" + applicationId + "' and parentmenuid='" + LStrMenuID + "'"))
            If LObjDataMenuFounds.Length > 0 Then
                createMenuXmlItem(xmlString, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
            End If
            xmlString = xmlString & "</MsMenuItem>"
            starting = starting + 1
        End While
    End Sub

    Private Function updateData() As Integer
        Dim LObjDataAdpt As New SqlDataAdapter
        Dim LObjCommand As New SqlCommand
        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        LObjCommand.CommandText = "execute dbo.spMenuOfGroupAdd @groupid, @menuid, @applicationid"
        LObjCommand.Connection = GObjCon
        LObjCommand.Parameters.Add("@groupid", SqlDbType.Char, 5, "groupid")
        LObjCommand.Parameters.Add("@menuid", SqlDbType.Char, 5, "menuid")
        LObjCommand.Parameters.Add("@applicationid", SqlDbType.Char, 10, "applicationid")
        LObjDataAdpt.InsertCommand = LObjCommand
        LObjCommand = New SqlCommand
        LObjCommand.CommandText = "execute dbo.spMenuOfGroupDelete @groupid, @menuid, @applicationid"
        LObjCommand.Connection = GObjCon
        LObjCommand.Parameters.Add("@groupid", SqlDbType.Char, 5, "groupid")
        LObjCommand.Parameters.Add("@menuid", SqlDbType.Char, 5, "menuid")
        LObjCommand.Parameters.Add("@applicationid", SqlDbType.Char, 10, "applicationid")
        LObjDataAdpt.DeleteCommand = LObjCommand
        updateData = LObjDataAdpt.Update(GObjDataGroup)
    End Function

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        'Server.Transfer("am_group_user.aspx")
        Response.Redirect("am_group_user.aspx")
    End Sub

    Private Sub TreeView1_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeView1.TreeNodeCheckChanged
        Dim LObjDataTable As DataTable = GObjDataGroup
        If Not e.Node Is Nothing Then
            If e.Node.Checked Then
                saveNodeAndChildNew(e.Node, LObjDataTable)
                'saveParentNodeNew(e.Node, LObjDataTable)
            Else
                deleteNodeAndChildNew(e.Node, LObjDataTable)
                'deleteUperLayerNew(e.Node, LObjDataTable, e.Node)
            End If
            'If updateData() > 1 Then
            '    LObjDataTable.AcceptChanges()
            'Else
            '    LObjDataTable.RejectChanges()
            'End If
        End If
    End Sub

    Private Sub TreeView1_TreeNodeDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeView1.TreeNodeDataBound
        If e.Node.Target = "true" Then
            e.Node.Checked = True
        Else
            e.Node.Checked = False
        End If
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim LObjDataTable As DataTable = GObjDataGroup
        If updateData() > 1 Then
            LObjDataTable.AcceptChanges()
        Else
            LObjDataTable.RejectChanges()
        End If
        'bindTreeView()
    End Sub

    Private Sub printRecursiveFirstNode(ByVal parNode As System.Web.UI.WebControls.TreeNode)
        strNodeIDChanged &= parNode.Text & " Checked " & parNode.Checked & "<br/><br/>"
    End Sub

    Private Sub printRecursiveCheckChild(ByVal parNode As System.Web.UI.WebControls.TreeNode, ByVal parChecked As Boolean)
        For Each oNode As System.Web.UI.WebControls.TreeNode In parNode.ChildNodes
            strNodeIDChanged &= oNode.Text & " Checked " & parChecked & "<br/>"
            printRecursiveCheckChild(oNode, parChecked)
        Next
    End Sub

    Private Sub RecursiveCheckParent(ByVal parNode As System.Web.UI.WebControls.TreeNode, ByVal parChecked As Boolean)
        If parChecked Then
            If Not IsNothing(parNode.Parent) Then
                Dim oNode As System.Web.UI.WebControls.TreeNode = parNode.Parent
                strNodeIDChanged &= oNode.Text & " Checked true <br/>"
                RecursiveCheckParent(oNode, parChecked)
            End If
        Else
            If Not IsNothing(parNode.Parent) Then
                Dim oNode As System.Web.UI.WebControls.TreeNode = parNode.Parent
                If Not isChildNodeChecked(oNode) Then
                    strNodeIDChanged &= oNode.Text & " Checked false <br/>"
                    RecursiveCheckParent(oNode, False)
                Else
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Function isChildNodeChecked(ByVal parNode As System.Web.UI.WebControls.TreeNode) As Boolean
        isChildNodeChecked = False
        For Each oNode As System.Web.UI.WebControls.TreeNode In parNode.ChildNodes
            If oNode.Checked Then
                isChildNodeChecked = True
                Exit For
            End If
        Next
        Return isChildNodeChecked
    End Function

    Private Sub printRecursive(ByVal ParNode As System.Web.UI.WebControls.TreeNode)
        For Each oNode As System.Web.UI.WebControls.TreeNode In ParNode.ChildNodes
            strNodeIDChanged &= oNode.Parent.Text
        Next
    End Sub

    Private Sub callRecursive()
        For Each oNode As System.Web.UI.WebControls.TreeNode In TreeView1.Nodes
            printRecursive(oNode)
        Next
    End Sub

    Private Sub saveNodeAndChildNew(ByVal parTreeNode As System.Web.UI.WebControls.TreeNode, ByRef LObjDataTable As DataTable)
        Dim LObjDataRow As DataRow
        LObjDataRow = LObjDataTable.NewRow()
        LObjDataRow("groupid") = Me.GroupID
        LObjDataRow("menuid") = parTreeNode.Value
        LObjDataRow("applicationid") = Me.Applicationid
        LObjDataTable.Rows.Add(LObjDataRow)
        'For Each oNode As System.Web.UI.WebControls.TreeNode In parTreeNode.ChildNodes
        '    saveNodeAndChildNew(oNode, LObjDataTable)
        'Next
    End Sub

    Private Sub saveParentNodeNew(ByVal parTreeNode As System.Web.UI.WebControls.TreeNode, ByRef LObjDataTable As DataTable)
        Dim LObjDataRow As DataRow
        Dim LObjParent As System.Web.UI.WebControls.TreeNode
        Dim LObjFoundKey(2) As Object

        If Not parTreeNode.Parent Is Nothing Then
            LObjParent = parTreeNode.Parent
            LObjFoundKey(0) = Me.GroupID
            LObjFoundKey(1) = LObjParent.Value
            LObjFoundKey(2) = Me.Applicationid
            LObjDataRow = LObjDataTable.Rows.Find(LObjFoundKey)
            If LObjDataRow Is Nothing Then
                LObjDataRow = LObjDataTable.NewRow()
                LObjDataRow("groupid") = lblGroupId.Text
                LObjDataRow("menuid") = LObjParent.Value
                LObjDataRow("applicationid") = Me.Applicationid
                LObjDataTable.Rows.Add(LObjDataRow)
                saveParentNodeNew(LObjParent, LObjDataTable)
            End If
        End If
    End Sub

    Private Sub deleteNodeAndChildNew(ByVal treeNode As System.Web.UI.WebControls.TreeNode, ByVal LObjDataTable As DataTable)
        Dim LObjDataRow As DataRow
        Dim LObjFoundKey(2) As Object

        LObjFoundKey(0) = Me.GroupID
        LObjFoundKey(1) = treeNode.Value
        LObjFoundKey(2) = Me.Applicationid
        LObjDataRow = LObjDataTable.Rows.Find(LObjFoundKey)
        If Not LObjDataRow Is Nothing Then
            LObjDataRow.Delete()
        End If

        'For Each aTreeNode As System.Web.UI.WebControls.TreeNode In treeNode.ChildNodes
        '    deleteNodeAndChildNew(aTreeNode, LObjDataTable)
        'Next
    End Sub

    Private Sub deleteUperLayerNew(ByVal _node As System.Web.UI.WebControls.TreeNode, ByVal _DataTable As DataTable, ByVal parLastNode As System.Web.UI.WebControls.TreeNode)
        Dim LObjDataRow As DataRow
        Dim LObjFoundKey(2) As Object
        Dim _ParentNode As System.Web.UI.WebControls.TreeNode
        Dim _CheckNode As System.Web.UI.WebControls.TreeNode
        Dim _HasChecked As Boolean = False

        If Not IsNothing(_node.Parent) Then
            _ParentNode = _node.Parent
        Else
            Exit Sub
        End If

        For Each _CheckNode In _ParentNode.ChildNodes
            If _CheckNode.Checked And _CheckNode.Value <> parLastNode.Value Then
                _HasChecked = True
            End If
        Next
        If Not _HasChecked Then
            LObjFoundKey(0) = Me.GroupID
            LObjFoundKey(1) = _ParentNode.Value
            LObjFoundKey(2) = Me.Applicationid
            LObjDataRow = _DataTable.Rows.Find(LObjFoundKey)
            If Not LObjDataRow Is Nothing Then
                LObjDataRow.Delete()
            End If
            deleteUperLayerNew(_ParentNode, _DataTable, _ParentNode)
        End If
    End Sub

    Private Sub bindTreeView()
        XmlDataSource1.Data = createMenuXml(Me.Applicationid)
        XmlDataSource1.DataBind()
        TreeView1.DataSource = XmlDataSource1
        TreeView1.DataBind()
    End Sub
End Class

