﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_group_user
    Inherits Maxiloan.Webform.WebBased

    Private oController As New UMGroupUserController
    Private oCustomClass As New Parameter.GroupUser

#Region "Property"

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property ApplicationId() As String
        Get
            Return (CType(ViewState("applicationid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("applicationid") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        If Not IsPostBack Then
            Me.SearchBy = ""
            Me.SortBy = ""
            With oCustomClass
                .WhereCond = ""
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
            End With
            cmbApplicationId.DataTextField = "ApplicationName"
            cmbApplicationId.DataValueField = "ApplicationID"
            cmbApplicationId.DataSource = oController.ListGroupUser(oCustomClass).ListMasterApplication
            cmbApplicationId.DataBind()
            cmbApplicationId.Items.Insert(0, "Select One")
            cmbApplicationId.Items(0).Value = "0"
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlGrid.Visible = False
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListGroupUser(oCustomClass)

        DtUserList = oCustomClass.ListGroupUser
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgUserList.DataSource = DvUserList
        Try
            DtgUserList.DataBind()
        Catch
            DtgUserList.CurrentPageIndex = 0
            DtgUserList.DataBind()
        End Try
        pnlGrid.Visible = True
        pnlList.Visible = True
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgUserList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgUserList.ItemCommand
        Select Case e.CommandName

            Case "EDIT"
                Me.Mode = "EDIT"
                Dim hypID As Label
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblGroupId"), Label)
                Dim hypName As Label
                hypName = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblgroupname"), Label)
                Me.ApplicationId = cmbApplicationId.SelectedValue.Trim
                lblApplicationID.Text = cmbApplicationId.SelectedItem.Text.Trim
                txtGroupId.Text = hypID.Text
                txtGroupId.Enabled = False
                txtGroupName.Text = hypName.Text
                pnlList.Visible = False
                pnlGrid.Visible = False
                pnlAdd.Visible = True
                lblTitle.Text = "EDIT USER"
                buttonCancelAdd.Attributes.Add("onclick", "return fback()")
            Case "DELETE"
                Dim hypID As Label
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lblGroupId"), Label)
                oCustomClass.GroupID = hypID.Text
                oCustomClass.ApplicationId = cmbApplicationId.SelectedValue.Trim
              
                Dim err As String = oController.DeleteGroupUser(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
                pnlList.Visible = True
                pnlGrid.Visible = False
        End Select
    End Sub

    Private Sub DtgUserList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgUserList.ItemDataBound
        Dim hypGroupMenu As HyperLink
        Dim hypGroupFeature As HyperLink
        Dim hypID As Label
        If e.Item.ItemIndex >= 0 Then
            hypID = CType(e.Item.FindControl("lblGroupId"), Label)
            hypGroupMenu = CType(e.Item.FindControl("hypGroupMenu"), HyperLink)
            hypGroupMenu.NavigateUrl = "am_group_002.aspx?groupid=" & hypID.Text.Trim & "&appid=" & cmbApplicationId.SelectedValue.Trim
            hypGroupFeature = CType(e.Item.FindControl("hypGroupFeature"), HyperLink)
            hypGroupFeature.NavigateUrl = "../feature/am_ftgrp_001.aspx?ftrGroupId=" & hypID.Text.Trim & "&appid=" & cmbApplicationId.SelectedValue.Trim
        End If
    End Sub

    Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgUserList.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        pnlAdd.Visible = True
        pnlList.Visible = False

        Me.Mode = "ADD"
        pnlGrid.Visible = False
        lblTitle.Text = "ADD GROUP"
        lblApplicationID.Text = cmbApplicationId.SelectedItem.Text.Trim
        Me.ApplicationId = cmbApplicationId.SelectedValue
        txtGroupId.Text = ""
        txtGroupName.Text = ""
        txtGroupId.Enabled = True
        buttonCancelAdd.Attributes.Add("onclick", "return fback()")
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCancelAdd.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSaveAdd.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        If Page.IsValid Then
            Dim LObjencrypt As New Decrypt.am_futility

            With oCustomClass
                .GroupID = txtGroupId.Text.Trim
                .GroupName = txtGroupName.Text.Trim
                .ApplicationId = Me.ApplicationId
            End With
            If Me.Mode = "ADD" Then

                Dim err As String = oController.AddGroupUser(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
            Else
                If Me.Mode = "EDIT" Then

                    Dim err As String = oController.UpdateGroupUser(oCustomClass)
                    If err <> String.Empty Then
                        ShowMessage(lblMessage, err, True)
                    End If
                End If
            End If
            pnlList.Visible = True
            pnlGrid.Visible = False
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If cmbApplicationId.SelectedIndex > 0 Then
            Me.SearchBy = " applicationid = '" & cmbApplicationId.SelectedValue.Trim.Replace("'", "''") & "'"
        End If

        If txtgroupidsrc.Text.Trim <> "" Then
            If Me.SearchBy <> "" Then
                Me.SearchBy &= " and groupid = '" & txtgroupidsrc.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy = " groupid = '" & txtgroupidsrc.Text.Trim.Replace("'", "''") & "'"
            End If
        End If

        If txtgroupidsrc.Text.Trim = "" And txtgroupidsrc.Text.Trim.Replace("'", "''") = "" And cmbApplicationId.SelectedIndex = 0 Then
            Me.SearchBy = " applicationid = '' "
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
End Class