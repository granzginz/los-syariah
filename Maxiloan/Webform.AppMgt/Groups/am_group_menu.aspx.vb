﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_group_menu
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta and Variable"
    Private oController As New UMUserGroupMenuController
    Private oCustomClass As New Parameter.UserGroupMenu
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property TablePaging() As DataTable
        Get
            Return CType(ViewState("tablepaging"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("tablepaging") = Value
        End Set
    End Property

    Private ReadOnly Property ApplicationId() As String
        Get
            Return Request.QueryString("appid")
        End Get
    End Property

    Private ReadOnly Property GroupDb() As String
        Get
            Return Request.QueryString("groupdbid")
        End Get
    End Property

    Private Property GroupMenu() As String
        Get
            Return (CType(ViewState("GroupMenu"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupMenu") = Value
        End Set
    End Property
    Private ReadOnly Property LoginIdApplication() As String
        Get
            Return Request.QueryString("loginid")
        End Get
    End Property

    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property TotalRecord() As Int64
        Get
            Return (CType(ViewState("totalrecord"), Int64))
        End Get
        Set(ByVal Value As Int64)
            ViewState("totalrecord") = Value
        End Set
    End Property

    Private Property TotalHalaman() As Double
        Get
            Return (CType(ViewState("TotalHalaman"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalHalaman") = Value
        End Set
    End Property

    Private Property Halaman() As Double
        Get
            Return (CType(ViewState("Halaman"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("Halaman") = Value
        End Set
    End Property

    Private Property GroupDBAll() As String
        Get
            Return (CType(ViewState("GroupDBAll"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupDBAll") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblloginid.Text = Me.LoginIdApplication
        lblApplicationId.Text = Me.ApplicationId
        lblGroupData.Text = Me.GroupDb

        InitialDefaultPanel()
        If Not IsPostBack Then
            Me.SearchBy = ""
            Me.SortBy = ""
            doBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub doBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .LoginId = Me.LoginIdApplication
            .ApplicationId = Me.ApplicationId
            .GroupDBID = Me.GroupDb
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListUserGroupMenu(oCustomClass)
        Me.TablePaging = oCustomClass.ListUserGroupDb
        DtUserList = oCustomClass.ListUserGroupDb

        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        Me.TotalRecord = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgUsrGroupMenu.DataSource = DvUserList
        Try
            dtgUsrGroupMenu.DataBind()
        Catch
            dtgUsrGroupMenu.CurrentPageIndex = 0
            dtgUsrGroupMenu.DataBind()
        End Try
        lblloginid.Text = Me.LoginIdApplication
        PagingFooter()

    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblMessage.Text = ""
        lblMessage.Visible = False
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                doBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        doBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Server.Transfer("am_group_001.aspx?loginid=" & Me.LoginIdApplication & "&appid=" & Me.ApplicationId)
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i As Integer
        Dim counter As Int32
        Dim chkUserGroupDb As New CheckBox
        Dim lblGroupDbId As Label
        Me.GroupMenu = ""
        Me.GroupDBAll = ""
        counter = 0

        For i = 0 To dtgUsrGroupMenu.Items.Count - 1
            chkUserGroupDb = CType(dtgUsrGroupMenu.Items(i).FindControl("chkUserGroupMenu"), CheckBox)
            lblGroupDbId = CType(dtgUsrGroupMenu.Items(i).FindControl("lblGroupMenuId"), Label)
            If chkUserGroupDb.Checked = True Then
                Me.GroupMenu &= CStr(IIf(Me.GroupMenu = "", "", ",")) & "'" & lblGroupDbId.Text.Trim & "'"
                counter += 1
            End If
            Me.GroupDBAll &= CStr(IIf(Me.GroupDBAll = "", "", ",")) & "'" & lblGroupDbId.Text.Trim & "'"
        Next i
        With oCustomClass
            .ApplicationId = Me.ApplicationId
            .LoginId = Me.LoginIdApplication
            .GroupDBID = Me.GroupDb
            .GroupDBAll = Me.GroupDBAll
            .GroupDBMenu = Me.GroupMenu
            .GroupDBMenuItem = counter
        End With
        If oController.UpdateUserGroupMenu(oCustomClass) = "0" Then
            ShowMessage(lblMessage, "Data sudah diupdate", False)
        Else
            ShowMessage(lblMessage, "Update gagal", True)
        End If
    End Sub

    Protected Sub imgBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("am_group_001.aspx?appid=" & lblApplicationId.Text.Trim & "&loginid=" & lblloginid.Text)
    End Sub
End Class