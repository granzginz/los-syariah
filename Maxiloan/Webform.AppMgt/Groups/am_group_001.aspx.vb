﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_group_001
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta and Variable"
    Private oController As New UMUserGroupDBController
    Private oCustomClass As New Parameter.UserGroupDB
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property TablePaging() As DataTable
        Get
            Return CType(viewstate("tablepaging"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("tablepaging") = Value
        End Set
    End Property

    Private Property ApplicationId() As String
        Get
            Return (CType(Viewstate("ApplicationId"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property

    Private Property Status() As Int32
        Get
            Return (CType(viewstate("status"), Int32))
        End Get
        Set(ByVal Value As Int32)
            viewstate("status") = Value
        End Set
    End Property

    Private Property GroupDb() As String
        Get
            Return (CType(Viewstate("GroupDB"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("GroupDB") = Value
        End Set
    End Property

    Private Property LoginIdApplication() As String
        Get
            Return (CType(Viewstate("LoginApplication"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("LoginApplication") = Value
        End Set
    End Property

    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property TotalRecord() As Int64
        Get
            Return (CType(viewstate("totalrecord"), Int64))
        End Get
        Set(ByVal Value As Int64)
            viewstate("totalrecord") = Value
        End Set
    End Property

    Private Property TotalHalaman() As Double
        Get
            Return (CType(viewstate("TotalHalaman"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalHalaman") = Value
        End Set
    End Property

    Private Property Halaman() As Double
        Get
            Return (CType(viewstate("Halaman"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("Halaman") = Value
        End Set
    End Property
    Private Property GroupDBAll() As String
        Get
            Return (CType(viewstate("GroupDBAll"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("GroupDBAll") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If       

        Me.ApplicationId = Request.QueryString("appid")
        Me.LoginIdApplication = Request.QueryString("loginid")
        lblloginid.Text = Me.LoginIdApplication
        lblApplicationid.Text = Me.ApplicationId
        InitialDefaultPanel()

        If Not IsPostBack Then
            Me.SearchBy = ""
            Me.SortBy = ""
            Me.Status = 0
            doBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub doBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .LoginId = Me.LoginIdApplication
            .ApplicationId = Me.ApplicationId
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListUserGroupDB(oCustomClass)
        Me.TablePaging = oCustomClass.ListUserGroupDb
        DtUserList = Me.TablePaging

        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        Me.TotalRecord = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgUsrGroupDb.DataSource = DvUserList
        Try
            dtgUsrGroupDb.DataBind()
        Catch
            dtgUsrGroupDb.CurrentPageIndex = 0
            dtgUsrGroupDb.DataBind()
        End Try
        lblloginid.Text = Me.LoginIdApplication

        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        Me.TotalHalaman = totalPages
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblMessage.Text = ""
        lblMessage.Visible = False
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                doBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Protected Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgUsrGroupDb.SortCommand        
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgUsrGroupDb_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgUsrGroupDb.ItemDataBound
        Dim lblGroupID As Label
        Dim hypUserMenu As HyperLink
        Dim lblUserGroupDbId As Label
        If e.Item.ItemIndex >= 0 Then
            lblGroupID = CType(e.Item.FindControl("lblGroupDbId"), Label)
            lblUserGroupDbId = CType(e.Item.FindControl("lblUserGroupDbId"), Label)

            hypUserMenu = CType(e.Item.FindControl("hypUserMenu"), HyperLink)
            If lblUserGroupDbId.Text = "True" Then
                hypUserMenu.Visible = True
            Else
                hypUserMenu.Visible = False
            End If
            hypUserMenu.NavigateUrl = "am_group_menu.aspx?loginid=" & Me.LoginIdApplication & _
                                    "&appid=" & Me.ApplicationId & "&groupdbid=" & lblGroupID.Text.Trim
        End If
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/Webform.AppMgt/Users/am_user_006.aspx?loginid=" & Me.LoginIdApplication)
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i As Integer
        Dim counter As Int32

        counter = 0
        Me.GroupDb = ""
        Me.GroupDBAll = ""

        Dim chkUserGroupDb As New CheckBox
        Dim lblGroupDbId As Label

        For i = 0 To dtgUsrGroupDb.Items.Count - 1
            chkUserGroupDb = CType(dtgUsrGroupDb.Items(i).FindControl("chkUserGroupDb"), CheckBox)
            lblGroupDbId = CType(dtgUsrGroupDb.Items(i).FindControl("lblGroupDbId"), Label)
            If chkUserGroupDb.Checked = True Then
                Me.GroupDb &= CStr(IIf(Me.GroupDb = "", "", ",")) & "'" & lblGroupDbId.Text.Trim & "'"
                counter += 1
            End If
            Me.GroupDBAll &= CStr(IIf(Me.GroupDBAll = "", "", ",")) & "'" & lblGroupDbId.Text.Trim & "'"
        Next i
        With oCustomClass
            .ApplicationId = Me.ApplicationId
            .LoginId = Me.LoginIdApplication
            .GroupDBID = Me.GroupDb
            .GroubDBAll = Me.GroupDBAll
            .GroupDBIDItem = counter
        End With
        Dim err As String = oController.UpdateUserGroupDb(oCustomClass)
        If err <> String.Empty Then
            ShowMessage(lblMessage, err, True)
        End If

        doBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class