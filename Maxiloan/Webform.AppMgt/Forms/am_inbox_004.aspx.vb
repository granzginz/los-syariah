﻿Imports System.Data.SqlClient

Public Class am_inbox_004
    Inherits UserManagementBase

    Private GObjDataset As New DataSet
    Private GObjConn As New SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        GObjConn = New SqlConnection(Me.ConnectionStringAM)
        Call create_infobox()
    End Sub

    Sub create_infobox()
        lblMessage.Visible = False
        Dim objcommand As New SqlCommand

        Try
            If GObjConn.State = ConnectionState.Closed Then GObjConn.Open()

            objcommand.CommandText = "spMainView"
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.Parameters.Add("@loginid", SqlDbType.VarChar, 12).Value = Me.Loginid
            objcommand.Connection = GObjConn

            Dim objDataAdapter As New SqlDataAdapter
            objDataAdapter.SelectCommand = objcommand
            objDataAdapter.Fill(GObjDataset, "infobox")

            Dim cRow As DataRow

            For Each cRow In GObjDataset.Tables("infobox").Rows
                Dim hdrctlicon As Control
                If Trim(CStr(cRow("icon"))) = "" Then
                    hdrctlicon = LoadControl("am_icon_001.ascx")

                    CType(hdrctlicon, am_icon_001).Imageicon = "../../images/pines2.gif"
                    CType(hdrctlicon, am_icon_001).ImageBgLeftTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).ImageLeftTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).ImageBgRightTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).ImageRightTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).Textheader = Trim(CStr(cRow("infoboxtitle")))
                    CType(hdrctlicon, am_icon_001).FontColor = CStr(cRow("fontcolor"))
                Else
                    hdrctlicon = LoadControl("am_icon_001.ascx")

                    CType(hdrctlicon, am_icon_001).Imageicon = CStr(cRow("icon"))
                    CType(hdrctlicon, am_icon_001).ImageBgLeftTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).ImageLeftTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).ImageBgRightTD = "../../images/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).ImageRightTD = "../../mages/bgkertas2.gif"
                    CType(hdrctlicon, am_icon_001).Textheader = Trim(CStr(cRow("infoboxtitle")))
                    CType(hdrctlicon, am_icon_001).FontColor = CStr(cRow("fontcolor"))
                End If
                PlaceHolder1.Controls.Add(hdrctlicon)

                Dim control1_center As Control = LoadControl(CStr(cRow("includefilename")))
                PlaceHolder1.Controls.Add(control1_center)

            Next
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        Finally
            objcommand.Parameters.Clear()
            objcommand.Dispose()
            If GObjConn.State = ConnectionState.Open Then GObjConn.Close()
            GObjConn.Dispose()
        End Try

    End Sub

End Class