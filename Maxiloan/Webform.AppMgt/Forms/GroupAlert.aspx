﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GroupAlert.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.GroupAlert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            
            <h3>
                List Of System Group Alert</h3>
        </div>
    </div>
    <asp:Panel ID="pnldatagrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgForm" runat="server" OnSortCommand="sortgrid" AutoGenerateColumns="False"
                        DataKeyField="GroupAlertID" AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="GROUP ALERT ID" SortExpression="GroupAlertID">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="dtgGroupAlertID" runat="server" CausesValidation="False" Text='<%#container.dataitem("GroupAlertID")%>'
                                        CommandName="ShowView">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="GROUP ALERT NAME" SortExpression="GroupAlertName">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="60%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="dtgGroupAlertName" runat="server" Text='<%#container.dataitem("GroupAlertName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtpage" MinimumValue="1"
                            ErrorMessage="Halaman tidak valid!" MaximumValue="999999999" Type="Integer"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                            ErrorMessage="Halaman tidak valid!" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    Find System Group Alert
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Alert ID
                </label>
                <asp:TextBox ID="txtAlertID_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Alert Name
                </label>
                <asp:TextBox ID="txtAlertMessage_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlAdd" runat="server">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    Add/Edit Group Alert
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group ID
                </label>
                <asp:TextBox ID="txtGroupAlertID" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Alert Name
                </label>
                <asp:TextBox ID="txtGroupAlertName" runat="server" MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    View Group Alert
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group ID
                </label>
                <asp:Label ID="lblGroupAlertID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Alert Name
                </label>
                <asp:Label ID="lblGroupAlertName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancelview" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
