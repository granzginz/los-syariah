﻿
#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class am_form_001
    Inherits Maxiloan.Webform.WebBased

    Dim oCustomClass As New Parameter.MasterForm
    Private oController As New UMMasterFormController
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim strSortBy As String

#Region "Property"
    Private Property ApplicationList() As DataTable
        Get
            Return (CType(ViewState("applicationlist"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("applicationlist") = Value
        End Set
    End Property
    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        InitialDefaultPanel()
        If Not IsPostBack Then
            With oCustomClass
                .WhereCond = ""
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
            End With
            cmbApplicationID.DataTextField = "ApplicationName"
            cmbApplicationID.DataValueField = "ApplicationID"
            cmbApplicationID.DataSource = oController.ListMasterForm(oCustomClass).ListMasterApplication
            Me.ApplicationList = oController.ListMasterForm(oCustomClass).ListMasterApplication
            cmbApplicationID.DataBind()
            cmbApplicationID.Items.Insert(0, "Select One")
            cmbApplicationID.Items(0).Value = "0"
            Me.SearchBy = ""
            Me.SortBy = ""

        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlgrid.Visible = False
        pnlView.Visible = False
        pnlList.Visible = True
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListMasterForm(oCustomClass)

        DtUserList = oCustomClass.ListMasterForm
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgUserList.DataSource = DvUserList
        Try
            DtgUserList.DataBind()
        Catch
            DtgUserList.CurrentPageIndex = 0
            DtgUserList.DataBind()
        End Try
        pnlgrid.Visible = True
        PagingFooter()
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
           ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "DataGrid Command"
    Private Sub DtgUserList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgUserList.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                Me.Mode = "EDIT"
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkFormId"), LinkButton)
                With oCustomClass
                    .FormID = hypID.Text
                    .Applicationid = cmbApplicationID.SelectedValue.Trim
                End With
                oCustomClass = oController.ShowMasterForm(oCustomClass)
                pnlList.Visible = False
                pnlgrid.Visible = False
                pnlAdd.Visible = True
                pnlView.Visible = False
                With oCustomClass
                    txtFormId.Text = hypID.Text.Trim
                    txtFormId.Enabled = False
                    txtFormName.Text = .FormName
                    txtFormFileName.Text = .FormFileName

                    cmbSubSystemId.DataTextField = "ApplicationName"
                    cmbSubSystemId.DataValueField = "ApplicationID"
                    cmbSubSystemId.DataSource = Me.ApplicationList
                    cmbSubSystemId.DataBind()
                    cmbSubSystemId.Items.Insert(0, "Select One")
                    cmbSubSystemId.Items(0).Value = "0"
                    cmbSubSystemId.Items.FindByValue(cmbApplicationID.SelectedValue).Selected = True
                    txtIcon.Text = .Icon
                    chkIsCallByMenu.Checked = .CallByMenu
                End With
                lblTitle.Text = "Edit User"
                buttonCancelAdd.Attributes.Add("onclick", "return fback()")
            Case "DELETE"
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkFormId"), LinkButton)
                oCustomClass.Applicationid = cmbApplicationID.SelectedValue
                oCustomClass.FormID = hypID.Text
                oCustomClass.LoginId = hypID.Text
                Dim err As String = oController.DeleteMasterForm(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            Case "ShowView"
                Dim hypID As LinkButton
                hypID = CType(DtgUserList.Items(e.Item.ItemIndex).FindControl("lnkFormId"), LinkButton)
                With oCustomClass
                    .FormID = hypID.Text
                    .Applicationid = cmbApplicationID.SelectedValue.Trim
                End With
                oCustomClass = oController.ShowMasterForm(oCustomClass)
                pnlList.Visible = False
                pnlAdd.Visible = False
                pnlView.Visible = True
                With oCustomClass
                    lblFormId.Text = hypID.Text.Trim
                    lblFormName.Text = .FormName
                    lblFormFileName.Text = .FormFileName
                    lblSubSystem.Text = .SubSystem
                    lblIcon.Text = .Icon
                    chkShowCallByMenu.Checked = .CallByMenu
                    chkShowCallByMenu.Enabled = False
                End With
                buttonCancelAdd.Attributes.Add("onclick", "return fback()")
        End Select
    End Sub

    Private Sub DtgUserList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgUserList.ItemDataBound
        Dim lnkFormId As New LinkButton
        Dim hypFeature As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkFormId = CType(e.Item.FindControl("lnkFormId"), LinkButton)
            hypFeature = CType(e.Item.FindControl("hypFeature"), HyperLink)
            hypFeature.NavigateUrl = "../feature/am_ftr_001.aspx?formId=" & lnkFormId.Text.Trim & "&Applicationid=" & cmbApplicationID.SelectedValue.Trim
        End If
    End Sub

    Sub SortGrid(ByVal obj As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgUserList.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        pnlAdd.Visible = True
        pnlView.Visible = False
        pnlgrid.Visible = False
        pnlList.Visible = False
        txtFormId.Enabled = True
        txtFormId.Text = ""
        txtFormName.Text = ""
        txtFormFileName.Text = ""
        cmbSubSystemId.DataTextField = "ApplicationName"
        cmbSubSystemId.DataValueField = "ApplicationID"
        cmbSubSystemId.DataSource = Me.ApplicationList
        cmbSubSystemId.DataBind()
        cmbSubSystemId.Items.Insert(0, "Select One")
        cmbSubSystemId.Items(0).Value = "0"
        cmbSubSystemId.SelectedIndex = 0
        txtIcon.Text = ""
        chkIsCallByMenu.Checked = True

        Me.Mode = "ADD"
        lblTitle.Text = "Add Form"
        buttonCancelAdd.Attributes.Add("onclick", "return fback()")
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSaveAdd.Click
        pnlList.Visible = True
        pnlView.Visible = False
        pnlAdd.Visible = False
        If Page.IsValid Then
            Dim LObjencrypt As New Decrypt.am_futility

            With oCustomClass
                .Applicationid = cmbApplicationID.SelectedItem.Value.Trim
                .FormID = txtFormId.Text.Trim
                .FormName = txtFormName.Text.Trim
                .FormFileName = txtFormFileName.Text.Trim
                .CallByMenu = chkIsCallByMenu.Checked
                .SubSystem = cmbSubSystemId.SelectedItem.Value.Trim
                .Icon = txtIcon.Text.Trim
            End With
            If Me.Mode = "ADD" Then
                Dim err As String = oController.AddMasterForm(oCustomClass)
                If err <> String.Empty Then
                    ShowMessage(lblMessage, err, True)
                End If
            Else
                If Me.Mode = "EDIT" Then
                    Dim err As String = oController.UpdateMasterForm(oCustomClass)
                    If err <> String.Empty Then
                        ShowMessage(lblMessage, err, True)
                    End If

                End If
            End If
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If cmbApplicationID.SelectedIndex > 0 Then
            Me.SearchBy = " applicationid = '" & cmbApplicationID.SelectedValue.Trim.Replace("'", "''") & "'"
        Else
            Me.SearchBy = " applicationid = '' "
        End If


        If txtFormID_src.Text.Trim <> "" Then
            If Me.SearchBy <> "" Then
                Me.SearchBy &= " and formid = '" & txtFormID_src.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy = " formid = '" & txtFormID_src.Text.Trim.Replace("'", "''") & "'"
            End If
        End If

        If txtFormName_src.Text.Trim <> "" Then
            If Me.SearchBy <> "" Then
                Me.SearchBy &= " and fullname = '" & txtFormName_src.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy = " fullname = '" & txtFormName_src.Text.Trim.Replace("'", "''") & "'"
            End If
        End If


        If txtFormID_src.Text.Trim = "" And txtFormName_src.Text.Trim.Replace("'", "''") = "" And cmbApplicationID.SelectedIndex < 0 Then
            Me.SearchBy = " applicationid = '' "
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
End Class