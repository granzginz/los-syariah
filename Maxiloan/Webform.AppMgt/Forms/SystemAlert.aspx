﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SystemAlert.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.SystemAlert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fback() {
            history.back(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Your Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>System Alert</h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgForm" runat="server" CssClass="grid_general" OnSortCommand="sortgrid"
                        AutoGenerateColumns="False" DataKeyField="AlertID" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ALERT ID" SortExpression="AlertID">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="dtgAlertID" runat="server" CausesValidation="False" Text='<%#container.dataitem("AlertID")%>'
                                        CommandName="ShowView">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ALERT MESSAGE" SortExpression="AlertMessage">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle Width="60%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="dtgAlertName" runat="server" Text='<%#container.dataitem("AlertMessage")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <HeaderStyle CssClass="th_center"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"   Type="Integer"
                            MaximumValue="999999999" ErrorMessage="Halaman tidak valid!" MinimumValue="1"
                            ControlToValidate="txtpage"  CssClass="validator_general" ></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  ErrorMessage="Halaman tidak valid!"
                            ControlToValidate="txtpage"  CssClass="validator_general"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server"  Text="Add" CssClass="small button blue"
                    CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    Find System Alert
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alert ID
                </label>
                <asp:TextBox ID="txtAlertID_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alert Message
                </label>
                <asp:TextBox ID="txtAlertMessage_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlAdd" runat="server" Width="100%" Visible="False">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    Add/Edit Records
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Alert ID
                </label>
                <asp:TextBox ID="txtAlertID" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvformid" runat="server" ErrorMessage="Alert ID must be fill"
                    ControlToValidate="txtAlertID" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alert Message
                </label>
                <asp:TextBox ID="txtAlertmessage" runat="server" MaxLength="50" 
                    CssClass="long_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Number SQl Command
                </label>
                <asp:TextBox ID="txtNumSqlCmd" runat="server" MaxLength="1500" TextMode="MultiLine"
                    CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Detail SQl Command
                </label>
                <asp:TextBox ID="txtDtlSqlCmd" runat="server" MaxLength="1500" TextMode="MultiLine"
                    CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Key Column Name
                </label>
                <asp:TextBox ID="txtKeyColumnNm" runat="server" MaxLength="100" 
                    CssClass="long_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Default Form
                </label>
                <asp:CheckBox ID="ChkisdefaultFrm" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    List Form ID
                </label>
                <asp:TextBox ID="TxtlstFormID" runat="server" CssClass="long_text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvLstFrmID" runat="server" ErrorMessage="List Form ID must be fill"
                    ControlToValidate="TxtlstFormID" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    List Form Parameter
                </label>
                <asp:TextBox ID="TxtlstFormParameter" runat="server" MaxLength="1000" TextMode="MultiLine"
                    CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    View Form ID
                </label>
                <asp:TextBox ID="TxtvwFrmID" runat="server" CssClass="long_text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfdvwFrmID" runat="server" ErrorMessage="View Form ID must be fill"
                    ControlToValidate="TxtvwFrmID" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    View Form Parameter
                </label>
                <asp:TextBox ID="TxtviewFormParam" runat="server" MaxLength="1000" TextMode="MultiLine"
                    CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Edit Form ID
                </label>
                <asp:TextBox ID="TxtedtFrmID" runat="server" CssClass="long_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Edit Form Parameter
                </label>
                <asp:TextBox ID="TxtEdtFrmParam" runat="server" MaxLength="1000" TextMode="MultiLine"
                    CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Alert
                </label>
                <asp:DropDownList ID="cmbGroupAlert" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    View Records
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alert ID
                </label>
                <asp:Label ID="lblAlertId" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alert Message
                </label>
                <asp:Label ID="lblAlertMessage" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Number SQL Command
                </label>
                <asp:TextBox ID="lblNumSqlCmd" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"
                    Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Detail Sql Command
                </label>
                <asp:TextBox ID="lblDtlSqlCmd" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"
                    Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Key Column Name
                </label>
                <asp:Label ID="lblKeyColumnName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Default Form
                </label>
                <asp:Label ID="lblIsDefaultForm" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    List Form ID
                </label>
                <asp:Label ID="lblLstFrmID" runat="server">List Form ID</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    List Form Parameter
                </label>
                <asp:Label ID="lblLstFrmParameter" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    View Form ID
                </label>
                <asp:Label ID="lblViewFormID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    View Form Parameter
                </label>
                <asp:Label ID="lblViewFormParameter" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Edit Form ID
                </label>
                <asp:Label ID="lblEdtViewFormID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Edit Form Parameter
                </label>
                <asp:Label ID="lblEdtViewFormParameter" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Alert
                </label>
                <asp:Label ID="lblGroupAlert" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btncancelview" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
