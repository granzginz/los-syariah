﻿Imports System.Threading
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Public Class SystemAlert
    Inherits Maxiloan.Webform.UserManagementBase
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#Region "Constanta Paging"
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
#End Region

#Region "Constanta System Alert"
    Private oClassSystemAlert As New SystemAlertController
    Private oCustomSystemAlert As New Parameter.SystemAlert
#End Region
#End Region
#Region "Property"
    Private Property Mode() As String
        Get
            Return CType(viewstate("Mode"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not (IsPostBack) Then
            Me.SearchBy = ""
            Me.SortBy = ""
            lblMessage.Visible = False
            PnlAdd.Visible = False
            PnlView.Visible = False
            pnlList.Visible = True
            pnlDatagrid.Visible = False
        End If
    End Sub
#Region "Bind Data"
    Sub DoBind(ByVal cmdwhere As String, ByVal strsortby As String)
        With oContract
            .strConnection = Me.ConnectionStringAM
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strsortby
            .SpName = "spSystemAlertPaging"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        recordCount = oContract.TotalRecords
        DtgForm.DataSource = oContract.ListData
        Try
            DtgForm.DataBind()
        Catch
            DtgForm.CurrentPageIndex = 0
            DtgForm.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
          ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgForm.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "search Process"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblMessage.Text = ""
        lblMessage.Visible = False
        If txtAlertID_src.Text.Trim <> "" Then
            If Right(txtAlertID_src.Text.Trim, 1) = "%" Then
                Me.SearchBy = " AlertID Like '" & txtAlertID_src.Text.Trim & "' "
            Else
                Me.SearchBy = " AlertID = '" & txtAlertID_src.Text.Trim & "' "
            End If
        End If
        If txtAlertMessage_src.Text <> "" Then
            If txtAlertID_src.Text.Trim <> "" Then
                If Right(txtAlertMessage_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " And AlertMessage Like '" & txtAlertMessage_src.Text.Trim & "' "
                Else
                    Me.SearchBy = " And  AlertMessage = '" & txtAlertMessage_src.Text.Trim & "' "
                End If
            Else
                If Right(txtAlertMessage_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " AlertMessage Like '" & txtAlertMessage_src.Text.Trim & "' "
                Else
                    Me.SearchBy = " AlertMessage = '" & txtAlertMessage_src.Text.Trim & "' "
                End If
            End If
        End If

        PnlAdd.Visible = False
        PnlView.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Dtg Command"
    Private Sub DtgForm_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgForm.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                With oCustomSystemAlert
                    .strConnection = Me.ConnectionStringAM
                    .AlertID = DtgForm.DataKeys(e.Item.ItemIndex).ToString.Trim

                End With
                Try
                    oCustomSystemAlert = oClassSystemAlert.SystemAlertView(oCustomSystemAlert)
                    With oCustomSystemAlert
                        txtAlertID.Text = .AlertID
                        txtAlertmessage.Text = .AlertMessage
                        txtNumSqlCmd.Text = .NumberSQLCmd
                        txtDtlSqlCmd.Text = .DetailSQLCmd
                        txtKeyColumnNm.Text = .KeyColumnName
                        ChkisdefaultFrm.Checked = .IsDefaultForm
                        TxtlstFormID.Text = .ListFormID
                        TxtlstFormParameter.Text = .ListFormParameter
                        TxtvwFrmID.Text = .ViewFormID
                        TxtviewFormParam.Text = .ViewFormParameter
                        TxtedtFrmID.Text = .EditFormID
                        TxtEdtFrmParam.Text = .EditFormParameter
                        BindGroupAlert()
                        cmbGroupAlert.SelectedIndex = cmbGroupAlert.Items.IndexOf(cmbGroupAlert.Items.FindByValue(.GroupAlertID))
                    End With
                    txtAlertID.ReadOnly = True
                    lblMessage.Visible = False
                    Me.Mode = "EDIT"
                    lblMessage.Visible = False
                    PnlAdd.Visible = True
                    PnlView.Visible = False
                    pnlList.Visible = False
                    pnlDatagrid.Visible = False
                Catch Exp As Exception
                    ShowMessage(lblMessage, Exp.Message, True)
                End Try

            Case "DELETE"
                PnlView.Visible = False
                PnlAdd.Visible = False

                With oCustomSystemAlert
                    .strConnection = Me.ConnectionStringAM
                    .AlertID = DtgForm.DataKeys(e.Item.ItemIndex).ToString.Trim
                End With
                Try
                    oClassSystemAlert.SystemAlertDelete(oCustomSystemAlert)
                    ShowMessage(lblMessage, "Data sudah dihapus", False)
                Catch Exp As Exception
                    ShowMessage(lblMessage, Exp.Message, True)
                End Try
                DoBind(Me.SearchBy, Me.SortBy)
            Case "ShowView"
                With oCustomSystemAlert
                    .strConnection = Me.ConnectionStringAM
                    .AlertID = DtgForm.DataKeys(e.Item.ItemIndex).ToString.Trim
                End With
                Try
                    oCustomSystemAlert = oClassSystemAlert.SystemAlertView(oCustomSystemAlert)
                    With oCustomSystemAlert
                        lblAlertId.Text = .AlertID
                        lblAlertMessage.Text = .AlertMessage
                        lblNumSqlCmd.Text = .NumberSQLCmd
                        lblDtlSqlCmd.Text = .DetailSQLCmd
                        lblKeyColumnName.Text = .KeyColumnName
                        lblIsDefaultForm.Text = IIf(.IsDefaultForm, "Yes", "No").ToString
                        lblLstFrmID.Text = .ListFormID
                        lblLstFrmParameter.Text = .ListFormParameter
                        lblViewFormID.Text = .ViewFormID
                        lblViewFormParameter.Text = .ViewFormParameter
                        lblEdtViewFormID.Text = .EditFormID
                        lblEdtViewFormParameter.Text = .EditFormParameter
                        lblGroupAlert.Text = .GroupAlertID
                    End With
                    PnlAdd.Visible = False
                    PnlView.Visible = True
                    pnlList.Visible = False
                    pnlDatagrid.Visible = False
                Catch Exp As Exception
                    ShowMessage(lblMessage, Exp.Message, True)
                End Try

        End Select
    End Sub
#End Region
    Private Sub imgbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        PnlAdd.Visible = True
        PnlView.Visible = False
        pnlList.Visible = False
        pnlDatagrid.Visible = False
        txtAlertID.Text = ""
        txtAlertmessage.Text = ""
        txtNumSqlCmd.Text = ""
        txtKeyColumnNm.Text = ""
        ChkisdefaultFrm.Checked = False
        txtDtlSqlCmd.Text = ""
        TxtlstFormID.Text = ""
        TxtlstFormParameter.Text = ""
        TxtvwFrmID.Text = ""
        TxtviewFormParam.Text = ""
        TxtedtFrmID.Text = ""
        TxtEdtFrmParam.Text = ""
        BindGroupAlert()
        Me.Mode = "ADD"
        lblMessage.Visible = False
        txtAlertID.ReadOnly = False
    End Sub
    Private Sub BindGroupAlert()
        With oCustomSystemAlert
            .strConnection = Me.ConnectionStringAM
        End With
        Dim dtGroupAlert As New DataTable
        dtGroupAlert = oClassSystemAlert.GetGroupAlert(oCustomSystemAlert)
        cmbGroupAlert.DataSource = dtGroupAlert
        cmbGroupAlert.DataTextField = "GroupAlertName"
        cmbGroupAlert.DataValueField = "GroupAlertID"
        cmbGroupAlert.DataBind()
        cmbGroupAlert.Items.Insert(0, "Select Group Alert")
        cmbGroupAlert.Items(0).Value = ""
    End Sub
    Private Sub imgcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        PnlAdd.Visible = False
        PnlView.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Visible = False
    End Sub
    Private Sub imgsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        PnlAdd.Visible = False
        With oCustomSystemAlert
            .strConnection = Me.ConnectionStringAM
            .AlertID = txtAlertID.Text
            .AlertMessage = txtAlertmessage.Text
            .NumberSQLCmd = txtNumSqlCmd.Text
            .DetailSQLCmd = txtDtlSqlCmd.Text
            .KeyColumnName = txtKeyColumnNm.Text
            .IsDefaultForm = ChkisdefaultFrm.Checked
            .ListFormID = TxtlstFormID.Text
            .ListFormParameter = TxtlstFormParameter.Text
            .ViewFormID = TxtvwFrmID.Text
            .ViewFormParameter = TxtviewFormParam.Text
            .EditFormID = TxtedtFrmID.Text
            .EditFormParameter = TxtEdtFrmParam.Text
            .GroupAlertID = cmbGroupAlert.SelectedValue.Trim
        End With
        Try
            If Me.Mode = "ADD" Then
                oClassSystemAlert.SystemAlertAdd(oCustomSystemAlert)
            Else
                oClassSystemAlert.SystemAlertEdit(oCustomSystemAlert)
            End If
            PnlAdd.Visible = False
            PnlView.Visible = False
            pnlList.Visible = True
            pnlDatagrid.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
    Private Sub imgcancelview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancelview.Click
        PnlAdd.Visible = False
        PnlView.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Visible = False
    End Sub
End Class