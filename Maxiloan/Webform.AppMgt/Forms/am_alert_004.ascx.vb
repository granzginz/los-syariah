﻿Imports System.Data.SqlClient
Imports Maxiloan.Webform.UserController

Public Class am_alert_004
    Inherits UserManagementControlBased

    Public Property ErrorMessage As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not (IsPostBack) Then
            createAlert()
        End If
    End Sub

    Sub createAlert()
        outtext.InnerHtml = NodeAlert()
    End Sub

    Private Function NodeAlert() As String
        Dim objcon As New SqlConnection(Me.ConnectionStringAM)
        Dim daNodeALert As New SqlDataAdapter
        Dim objcommand As New SqlCommand
        Dim output, output1 As String
        Dim ds As New DataSet

        Try


            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcommand.Connection = objcon
            objcommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = Me.Loginid
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spAlertView"
            daNodeALert.SelectCommand = objcommand
            daNodeALert.Fill(ds, "alert")


        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

        Dim cRow As DataRow
        'Dim groupalertid, sub_, KeyColumnName As String
        Dim groupalertidOld, numsql, arr_sub, link, param As String
        Dim i, j, k As Integer
        Dim count As String
        Dim lObjCon As New SqlConnection(GetConnectionString)

        arr_sub = ""
        i = 0
        j = 0
        k = 0
        groupalertidOld = ""
        output = ""
        output1 = "<script language=JavaScript>" & vbCrLf & _
                  " content = new Array(); "
        objcommand.Parameters.Clear()
        Try
            For Each cRow In ds.Tables("alert").Rows
                link = " "

                numsql = Replace(CStr(cRow("NumberSQLCmd")).ToUpper, "$BRANCHID$", Me.sesBranchId.Replace("'", ""))
                numsql = Replace(numsql, "$CGID$", Me.GroubDbID)
                numsql = Replace(numsql, "$LOGINID$", Me.Loginid)
                numsql = Replace(numsql, "$BUSINESSDATE$", Format(Me.BusinessDate, "yyyyMMdd"))


                If lObjCon.State = ConnectionState.Closed Then lObjCon.Open()
                objcommand.Connection = lObjCon
                objcommand.CommandType = CommandType.Text
                objcommand.CommandText = numsql
                count = CStr(objcommand.ExecuteScalar)
                If CBool(cRow("isdefaultform")) Then
                    param = "Alertid=" & Trim(CStr(cRow("alertid")))
                    link = "<a href='am_alert_005.aspx?" & param & " '>" & count & "</a>"
                Else
                    'KeyColumnName = Replace(CStr(IIf(IsDBNull(Trim(CStr(cRow("KeyColumnName")))), "", Trim(CStr(cRow("KeyColumnName"))))), "$$1=", "")
                    'param = CStr(IIf(IsDBNull(Trim(CStr(cRow("ListFormID")))), "", Trim(CStr(cRow("ListFormID"))))) & _
                    '            "?" & Replace(CStr(IIf(IsDBNull(Trim(CStr(cRow("ListFormParameter")))), "", Trim(CStr(cRow("ListFormParameter"))))), "$$1", "")
                    'param = CStr(IIf(Trim(CStr(cRow("ListFormParameter"))) <> "", param, cRow("ListFormParameter")))
                    'link = "<a href='" & CStr(cRow("ListFormID")) & param & "'>" & count & "</a>"


                    param = CStr(IIf(Trim(CStr(cRow("ListFormParameter"))) <> "", "?" & cRow("ListFormParameter").ToString, ""))
                    link = "<a href='" & CStr(cRow("ListFormID")) & param & "'>" & count & "</a>"

                End If

                'If Trim(CStr(cRow("GroupAlertID"))) = "" Then
                'output1 = output1 & " content[" & i & "]= new Array( false, new Array('sub_" & i & "_1'));"
                output = output & "<div class=""form_box"">" & _
                                  "<div class=""form_single"">" & _
                                  "<label>" & CStr(cRow("AlertMessage")) & "</label> " & link & _
                                  "</div>" & _
                                  "</div>"
                'Else
                'groupalertid = CStr(cRow("GroupAlertid"))
                'If groupalertid <> groupalertidOld Then
                '    k = 0
                '    j = i

                '    If Trim(arr_sub) <> "" Then
                '        arr_sub = Left(arr_sub, Len(arr_sub) - 1)
                '        output1 = output1 & " content[" & j - 1 & "]= new Array( false, new Array(" & arr_sub & "));"
                '        arr_sub = ""
                '    End If

                '    output = output & "<div class=""form_box"">" & _
                '                      "<div class=""form_single""" & _
                '                      "onClick=""processTree (1); return false;""><label>+</label>" & _
                '                      "<label>" & CStr(cRow("GroupAlertName")) & "</label> " & _
                '                      "</div>" & _
                '                      "<div>"

                '    groupalertidOld = groupalertid
                'End If
                'sub_ = "sub_" & j & "_" & k
                'arr_sub = arr_sub & "'" & sub_ & "',"

                'output = output & "<div class=""form_box"">" & _
                '            "<div class=""form_single"">" & _
                '            "<label>" & CStr(cRow("AlertMessage")) & "</label> " & link & _
                '            "</div>" & _
                '            "<div>"
                'k = k + 1
                'End If
                'i = i + 1
                'If ds.Tables("alert").Rows.Count = i And (j <> 0) Then
                '    arr_sub = Left(arr_sub, Len(arr_sub) - 1)
                '    output1 = output1 & " content[" & j & "]= new Array( false, new Array(" & arr_sub & "));"
                'End If
            Next

            If lObjCon.State = ConnectionState.Open Then lObjCon.Close()
            outtext1.InnerHtml = output1 & vbCrLf & "</script>"
            NodeAlert = output

        Catch exp As Exception
            outtext1.InnerHtml = "<div class=""form_box"">" & _
                                "<div class=""form_single"">" & _
                                "<label class = ""validator_general label_auto"">" & exp.Message & "</label> " & _
                                "</div>" & _
                                "</div>"
            Return Nothing
        End Try
    End Function

End Class