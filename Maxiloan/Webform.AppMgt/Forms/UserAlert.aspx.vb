﻿#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class UserAlert
    Inherits Maxiloan.Webform.UserManagementBase

#Region "Property"
    Private Property IDLogin() As String
        Get
            Return CStr(ViewState("LoginID"))
        End Get
        Set(ByVal Value As String)
            ViewState("LoginID") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return CStr(ViewState("Mode"))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property GroupAlertID() As String
        Get
            Return CStr(ViewState("GroupAlertID"))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupAlertID") = Value
        End Set
    End Property

    Private Property GroupAllAlertID() As String
        Get
            Return CStr(ViewState("GroupAllAlertID"))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupAllAlertID") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Constanta System Alert"
    Private oClassSystemAlert As New SystemAlertController
    Private oCustomSystemAlert As New Parameter.SystemAlert
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not (IsPostBack) Then
            Me.IDLogin = Request.QueryString("ftrloginid")
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If

    End Sub
#Region "Bind DataGrid"
    Private Sub DoBind(ByVal cmdwhere As String, ByVal strsortby As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomSystemAlert
            .strConnection = Me.ConnectionStringAM
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strsortby
            .LoginId = Me.IDLogin
        End With
        oCustomSystemAlert = oClassSystemAlert.UserAlertPaging(oCustomSystemAlert)
        recordCount = oCustomSystemAlert.TotalRecord
        DtUserList = oCustomSystemAlert.ListUserAlert
        DvUserList = DtUserList.DefaultView
        Try
            dtgAlertUser.DataSource = DvUserList
            dtgAlertUser.DataBind()
        Catch exp As Exception
            dtgAlertUser.CurrentPageIndex = 0
            dtgAlertUser.DataBind()
            ShowMessage(lblMessage, Exp.Message, True)
        End Try
        PagingFooter()
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
           ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgAlertUser.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "SearchProcess"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblMessage.Text = ""
        lblMessage.Visible = False
        If txtformid_src.Text.Trim <> "" Then
            If Right(txtformid_src.Text.Trim, 1) = "%" Then
                Me.SearchBy = " AlertID Like '" & txtformid_src.Text.Trim & "' "
            Else
                Me.SearchBy = " AlertID = '" & txtformid_src.Text.Trim & "' "
            End If
        End If
        If txtformname_src.Text <> "" Then
            If txtformid_src.Text.Trim <> "" Then
                If Right(txtformname_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " And AlertMessage Like '" & txtformname_src.Text.Trim & "' "
                Else
                    Me.SearchBy = " And  AlertMessage = '" & txtformname_src.Text.Trim & "' "
                End If
            Else
                If Right(txtformname_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " AlertMessage Like '" & txtformname_src.Text.Trim & "' "
                Else
                    Me.SearchBy = " AlertMessage = '" & txtformname_src.Text.Trim & "' "
                End If
            End If
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i As Integer = 0
        Dim chkAlertID1 As New CheckBox
        Dim lblAlertID As Label
        lblMessage.Text = ""
        lblMessage.Visible = True
        Me.GroupAlertID = ""
        Me.GroupAllAlertID = ""
        chkAlertID1.Checked = True
        For i = 0 To dtgAlertUser.Items.Count - 1
            chkAlertID1 = CType(dtgAlertUser.Items(i).FindControl("chkAlertID2"), CheckBox)
            lblAlertID = CType(dtgAlertUser.Items(i).FindControl("lblAlertID"), Label)
            If chkAlertID1.Checked Then
                Me.GroupAlertID &= CStr(IIf(Me.GroupAlertID = "", "", ",")) & "'" & lblAlertID.Text.Trim & "'"
            End If
            Me.GroupAllAlertID &= CStr(IIf(Me.GroupAllAlertID = "", "", ",")) & "'" & lblAlertID.Text.Trim & "'"
        Next i
        With oCustomSystemAlert
            .strConnection = Me.ConnectionStringAM
            .LoginId = Me.IDLogin
            .GroupAllAlertID = Me.GroupAllAlertID
            .GroupAlertID = Me.GroupAlertID
        End With
        Try
            oClassSystemAlert.UserAlertUpdate(oCustomSystemAlert)
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data sudah diupdate", False)
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

    Protected Sub imbCancel_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Server.Transfer("../../webform.AppMgt/Users/am_user_001.aspx")
    End Sub
End Class