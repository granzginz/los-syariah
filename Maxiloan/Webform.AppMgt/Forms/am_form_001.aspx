﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_form_001.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.am_form_001" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fback() {
            history.back(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                Find Master Form</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Form ID
                </label>
                <asp:TextBox ID="txtFormID_src" runat="server" MaxLength="20"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Form Name
                </label>
                <asp:TextBox ID="txtFormName_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Application ID
                </label>
                <asp:DropDownList ID="cmbApplicationID" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage="Please Select Application ID"
                    ControlToValidate="cmbApplicationId" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
        </div>
        <asp:Panel ID="pnlgrid" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        List Of Master Form</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgUserList" runat="server" DataKeyField="FormId" OnSortCommand="SortGrid"
                            CssClass="grid_general" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="False">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn SortExpression="formid" HeaderText="FORM ID">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFormId" runat="server" Text='<%#Container.DataItem("FormId")%>'
                                            CommandName="ShowView">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="formname" HeaderText="FORM NAME">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFormName" runat="server" Text='<%#Container.DataItem("FormName")%>'
                                            CommandName="ShowView">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="formfilename" HeaderText="FORM FILE NAME">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle Width="50%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFormFileName" runat="server" Text='<%#Container.DataItem("formfilename")%>'
                                            CommandName="ShowView">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FEATURE">
                                    <HeaderStyle CssClass="th_center"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypFeature" runat="server" ImageUrl="../../Images/IconReceived.gif"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <HeaderStyle CssClass="th_center"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                            CommandName="EDIT"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <HeaderStyle CssClass="th_center"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                            CommandName="DELETE"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" ErrorMessage="Halaman tidak valid!"
                                ControlToValidate="txtPage" Type="Integer" MaximumValue="999999999" MinimumValue="1"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Halaman tidak valid!"
                                ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="small button blue"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    Master Form View
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Form Id
                </label>
                <asp:Label ID="lblFormId" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Form Name
                </label>
                <asp:Label ID="lblFormName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Form File Name
                </label>
                <asp:Label ID="lblFormFileName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Call By Menu
                </label>
                <asp:CheckBox ID="chkShowCallByMenu" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Sub System
                </label>
                <asp:Label ID="lblSubSystem" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Icon
                </label>
                <asp:Label ID="lblIcon" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancelView" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Form Id
                </label>
                <asp:TextBox ID="txtFormId" runat="server" MaxLength="20" CssClass ="no_text_transf"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" CssClass="validator_general"
                    ErrorMessage="Please Fill Form ID" ControlToValidate="txtFormId" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Form Name
                </label>
                <asp:TextBox ID="txtFormName" runat="server" MaxLength="50" CssClass="long_text no_text_transf"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" CssClass="validator_general"
                    ErrorMessage="Please Fill Form Name" ControlToValidate="txtFormName" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Form File Name
                </label>
                <asp:TextBox ID="txtFormFileName" runat="server" MaxLength="200" CssClass="long_text no_text_transf"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" CssClass="validator_general"
                    ErrorMessage="Please Fill Form File Name" ControlToValidate="txtFormFileName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Call By Menu
                </label>
                <asp:CheckBox ID="chkIsCallByMenu" runat="server" Checked="True"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Sub System ID
                </label>
                <asp:DropDownList ID="cmbSubSystemId" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Fill In Sub System ID"
                    ControlToValidate="cmbSubSystemId" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Icon
                </label>
                <asp:TextBox ID="txtIcon" runat="server" MaxLength="50" CssClass ="no_text_transf"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="buttonSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="buttonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
