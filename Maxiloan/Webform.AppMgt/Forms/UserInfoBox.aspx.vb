﻿#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class UserInfoBox
    Inherits Maxiloan.Webform.UserManagementBase

#Region "Property"
    Private Property IDLogin() As String
        Get
            Return CStr(ViewState("LoginID"))
        End Get
        Set(ByVal Value As String)
            ViewState("LoginID") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return CStr(ViewState("Mode"))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property GroupInfoBoxID() As String
        Get
            Return CStr(ViewState("GroupInfoBoxID"))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupInfoBoxID") = Value
        End Set
    End Property

    Private Property GroupAllInfoBoxID() As String
        Get
            Return CStr(ViewState("GroupAllInfoBoxID"))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupAllInfoBoxID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Constanta System Alert"
    Private oClassInfoBox As New InfoBoxController
    Private oCustomInfoBox As New Parameter.InfoBox
#End Region
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not (IsPostBack) Then
            Me.IDLogin = Request.QueryString("ftrloginid")
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Sub DoBind(ByVal cmdwhere As String, ByVal strsortby As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomInfoBox
            .strConnection = Me.ConnectionStringAM
            .WhereCond = cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strsortby
            .LoginId = Me.IDLogin
        End With
        oCustomInfoBox = oClassInfoBox.UserInfoBoxPaging(oCustomInfoBox)
        recordCount = oCustomInfoBox.TotalRecord
        DtUserList = oCustomInfoBox.ListUserInfoBox
        DvUserList = DtUserList.DefaultView
        Try
            dtgAlertUser.DataSource = DvUserList
            dtgAlertUser.DataBind()
        Catch exp As Exception
            dtgAlertUser.CurrentPageIndex = 0
            dtgAlertUser.DataBind()
            ShowMessage(lblMessage, Exp.Message, True)
        End Try
        PagingFooter()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgAlertUser.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Server.Transfer("../../webform.AppMgt/Users/am_user_001.aspx")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i As Integer = 0
        Dim chkAlertID1 As New CheckBox
        Dim lblAlertID As Label
        lblMessage.Text = ""
        lblMessage.Visible = False
        Me.GroupInfoBoxID = ""
        Me.GroupAllInfoBoxID = ""
        chkAlertID1.Checked = True
        For i = 0 To dtgAlertUser.Items.Count - 1
            chkAlertID1 = CType(dtgAlertUser.Items(i).FindControl("chkAlertID2"), CheckBox)
            lblAlertID = CType(dtgAlertUser.Items(i).FindControl("lblAlertID"), Label)
            If chkAlertID1.Checked Then
                Me.GroupInfoBoxID &= CStr(IIf(Me.GroupInfoBoxID = "", "", ",")) & "'" & lblAlertID.Text.Trim & "'"
            End If
            Me.GroupAllInfoBoxID &= CStr(IIf(Me.GroupAllInfoBoxID = "", "", ",")) & "'" & lblAlertID.Text.Trim & "'"
        Next i
        With oCustomInfoBox
            .strConnection = Me.ConnectionStringAM
            .LoginId = Me.IDLogin
            .GroupAllInfoBoxID = Me.GroupAllInfoBoxID
            .GroupInfoBoxID = Me.GroupInfoBoxID
        End With
        Try
            oClassInfoBox.UserInfoBoxUpdate(oCustomInfoBox)
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data sudah diupdate", False)
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
#Region "Search Process"
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblMessage.Text = ""
        lblMessage.Visible = False

        If txtformid_src.Text.Trim <> "" Then
            If Right(txtformid_src.Text.Trim, 1) = "%" Then
                Me.SearchBy = " InfoBoxID Like '" & txtformid_src.Text.Trim & "' "
            Else
                Me.SearchBy = " InfoBoxID = '" & txtformid_src.Text.Trim & "' "
            End If
        End If
        If txtformname_src.Text <> "" Then
            If txtformid_src.Text.Trim <> "" Then
                If Right(txtformname_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " And InfoBoxTitle Like '" & txtformname_src.Text.Trim & "' "
                Else
                    Me.SearchBy = " And  InfoBoxTitle = '" & txtformname_src.Text.Trim & "' "
                End If
            Else
                If Right(txtformname_src.Text.Trim, 1) = "%" Then
                    Me.SearchBy = " InfoBoxTitle Like '" & txtformname_src.Text.Trim & "' "
                Else
                    Me.SearchBy = " InfoBoxTitle = '" & txtformname_src.Text.Trim & "' "
                End If
            End If
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

End Class