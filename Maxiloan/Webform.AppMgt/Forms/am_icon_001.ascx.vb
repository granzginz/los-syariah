﻿Public Class am_icon_001
    'Inherits UserManagementControlBased
    Inherits ControlBased

    Protected WithEvents outtext As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_Imageicon As String = ""
    Private m_ImageLeftTD As String = ""
    Private m_ImageBgLeftTD As String = ""
    Private m_ImageRightTD As String = ""
    Private m_ImageBgRightTD As String = ""
    Private m_Textheader As String = ""
    Private m_FontColor As String = ""

    Public WriteOnly Property Imageicon() As String
        Set(ByVal Value As String)
            m_Imageicon = Value
        End Set
    End Property

    Public WriteOnly Property ImageLeftTD() As String

        Set(ByVal Value As String)
            m_ImageLeftTD = Value
        End Set
    End Property

    Public WriteOnly Property ImageBgLeftTD() As String
        Set(ByVal Value As String)
            m_ImageBgLeftTD = Value
        End Set
    End Property

    Public WriteOnly Property ImageRightTD() As String
        Set(ByVal Value As String)
            m_ImageRightTD = Value
        End Set
    End Property

    Public WriteOnly Property ImageBgRightTD() As String
        Set(ByVal Value As String)
            m_ImageBgRightTD = Value
        End Set
    End Property

    Public WriteOnly Property Textheader() As String
        Set(ByVal Value As String)
            m_Textheader = Value
        End Set
    End Property

    Public WriteOnly Property FontColor() As String
        Set(ByVal Value As String)
            m_FontColor = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InfoBox()
    End Sub

    Sub InfoBox()
        Dim output As String
        output = "<div class=""form_box_header"">" & _
                    "<div class=""form_single"">" & _
                    "<h5>" & m_Textheader & "</h5>" & _
                    "</div>" & _
                 "</div>"
        outtext.InnerHtml = output
    End Sub
End Class