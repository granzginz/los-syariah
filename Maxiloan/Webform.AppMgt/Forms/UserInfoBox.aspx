﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserInfoBox.aspx.vb" Inherits="Maxiloan.Webform.AppMgt.UserInfoBox" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
	<script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>    
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAlertUser" runat="server" OnSortCommand="SortGrid" CssClass="grid_general"
                    AutoGenerateColumns="False" AllowSorting="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkAlertID2" runat="server" Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="InfoBoxID" HeaderText="ALERT ID">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAlertID" runat="server" Text='<%#Container.DataItem("InfoBoxID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="InfoBoxTitle" HeaderText="ALERT MESSAGE">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAlertMessage" runat="server" EnableViewState="False" Text='<%#Container.DataItem("InfoBoxTitle")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtpage" MinimumValue="1"
                        ErrorMessage="Halaman tidak valid!" MaximumValue="999999999" Type="Integer"
                         CssClass="validator_general"  Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                        ErrorMessage="Halaman tidak valid!"   CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </div>
    <div class="form_button">
      <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
            CausesValidation="True"></asp:Button>
        <asp:Button ID="btnCancel" runat="server"  Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                Find User Info Box</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                INFO BOX ID
            </label>
            <asp:TextBox ID="txtformid_src" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                INFO BOX TITLE
            </label>
            <asp:TextBox ID="txtformname_src" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
       <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
    </div>
    </form>
</body>
</html>
