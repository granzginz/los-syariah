﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TransferForPaymentRequest
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private runningTotal As Double = 0
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.TransferForPaymentRequest
    Private oController As New TransferForPaymentRequestController
    Private p_Controller As New Controller.PaymentRequestController
    Private m_TotalAmount As Double
    Protected WithEvents oBGNODate As ucBGNoDate

#End Region

#Region "Property"
    Private Property SortPettydtg() As String
        Get
            Return CType(ViewState("SortPettydtg"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortPettydtg") = Value
        End Set
    End Property

    Private Property DataBank() As DataTable
        Get
            Return CType(ViewState("DataBank"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataBank") = Value
        End Set
    End Property

    Private Property DataTransfer() As DataTable
        Get
            Return CType(ViewState("DataTransfer"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataTransfer") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property
    Private Property Num() As Integer
        Get
            Return CType(ViewState("Num"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("Num") = Value
        End Set
    End Property

    Private Property BankAccountFrom() As String
        Get
            Return CType(ViewState("BankAccountFrom"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountFrom") = Value
        End Set
    End Property

    Private Property BranchIDTo() As String
        Get
            Return CType(ViewState("BranchIDTo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDTo") = Value
        End Set
    End Property

    Private Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property

    Private Property EndingBalance() As Double
        Get
            Return CInt(ViewState("EndingBalance"))
        End Get
        Set(ByVal Value As Double)
            ViewState("EndingBalance") = Value
        End Set
    End Property
    Private Property BranchIDbranch() As String
        Get
            Return CStr(ViewState("BranchIDbranch"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDbranch") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Me.IsHoBranch = False Then
            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            Exit Sub
        End If

        Me.FormID = "TRFPAYREQ"
        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not IsPostBack Then
                FillCbo()
                InitialDefaultPanel()
            End If
        End If
        If Not Me.IsPostBack Then

            If Request("thingstodo") = "1" Then
                Me.SearchBy = " PaymentRequest.Status='APR' "
                Me.SearchBy = Me.SearchBy + " and PaymentRequest.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If

        End If

    End Sub

    Private Sub FillCbo()
        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlTransfer.Visible = False
        PnlBGNO.Visible = False
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        txtSearchBy.Text = ""
        cboBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtRequestDate.Text = ""
        InitialDefaultPanel()
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", "MAXILOAN") Then
            Me.SearchBy = " PaymentRequest.Status='APR' "
            If cboBranch.SelectedValue.Trim <> "ALL" Then
                Me.SearchBy = Me.SearchBy + " and PaymentRequest.BranchID='" & cboBranch.SelectedItem.Value.Trim & "'"
            End If
            If txtRequestDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and RequestDate='" & ConvertDate(txtRequestDate.Text) & "'"
            End If
            If cboSearchBy.SelectedItem.Value.Trim <> "" And txtSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " = '" & txtSearchBy.Text.Trim & "'"
            End If
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)

        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListRequest(oCustomClass)

        DtUserList = oCustomClass.ListRequest
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRequest.DataSource = DvUserList

        Try
            dtgRequest.DataBind()
        Catch
            dtgRequest.CurrentPageIndex = 0
            dtgRequest.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgRequest_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRequest.ItemCommand
        If e.CommandName = "Transfer" Then
            'If cboBranch.SelectedValue.Trim = "014" Or cboBranch.SelectedValue.Trim = "099" Or cboBranch.SelectedValue.Trim = "ALL" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Trfer", "MAXILOAN") Then
                pnlTransfer.Visible = True
                pnlDtGrid.Visible = False
                pnlsearch.Visible = False
                PnlBGNO.Visible = False
                ButtonSave.Visible = False
                ButtonNext.Visible = True
                lblValueDate.Text = ""
                txtValueDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                lblNotes.Text = ""
                lblReferenceNo.Text = ""
                lblBankAccountDtl.Text = ""
                Dim lblNoReq As Label
                lblNoReq = CType(dtgRequest.Items(e.Item.ItemIndex).Cells(2).FindControl("lblRequestNo"), Label)

                Dim AccountNo As Label
                AccountNo = CType(dtgRequest.Items(e.Item.ItemIndex).FindControl("lblAccountNo"), Label)
                lblAccountNo.Text = AccountNo.Text

                Dim bankname As Label
                bankname = CType(dtgRequest.Items(e.Item.ItemIndex).FindControl("lblbankname"), Label)
                lblbankname.Text = bankname.Text

                Me.BranchIDTo = dtgRequest.Items(e.Item.ItemIndex).Cells(10).Text.Trim
                FillRequest(lblNoReq.Text.Trim, "")
                Me.Num = dtgRequest.Items(e.Item.ItemIndex).Cells(11).Text.Trim

                Dim Memo As Label
                Memo = CType(dtgRequest.Items(e.Item.ItemIndex).FindControl("lblNoMemo"), Label)
                lblNoMemo.Text = Memo.Text

                If Me.Num <> 0 Then
                    FillRequestHistoryReject(lblNoReq.Text.Trim, "")
                    panelHistory.Visible = True
                Else
                    panelHistory.Visible = False
                End If

                lblBranch.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(1).Text.Trim
                Me.BranchIDbranch = dtgRequest.Items(e.Item.ItemIndex).Cells(10).Text.Trim

                FillCboAccountBranch()

                If cboCaraBayar.SelectedValue = "GT" Then
                    FillCboAccount("EB")
                Else
                    FillCboAccount("")
                End If



                cboBankaccountBranch.SelectedIndex = cboBankaccountBranch.Items.IndexOf(cboBankaccountBranch.Items.FindByText(CStr(dtgRequest.Items(e.Item.ItemIndex).Cells(8).Text.Trim)))
                'cboBankaccountBranch.SelectedIndex 
                lblRequestNo.Text = lblNoReq.Text.Trim
                lblBankAccount.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(8).Text.Trim
                'oBankAccount.BankAccountName = dtgRequest.Items(e.Item.ItemIndex).Cells(6).Text.Trim

                lblDescription.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(5).Text.Trim
                lblRequestDate.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(7).Text.Trim
                lblAmount.Text = FormatNumber(dtgRequest.Items(e.Item.ItemIndex).Cells(6).Text.Trim, 2)
                lblValueDate.Text = ""
                txtValueDate.Visible = True
                lblNotes.Text = ""
                txtNotes.Visible = True
                lblReferenceNo.Text = ""
                txtReferenceNo.Visible = True
                cboBankAccount.Visible = True
                txtNotes.Text = ""
                lblTotalAmount.Text = ""
                txtReferenceNo.Text = ""
                cboBankAccount.ClearSelection()
                lblBankAccountDtl.Text = ""
                Me.BankAccountFrom = dtgRequest.Items(e.Item.ItemIndex).Cells(8).Text.Trim
                Dim lblNotesdtg, lblAmountReq As Label
                Dim txtNotesdtg As New TextBox
                Dim txtAmountdetail As ucNumberFormat
                Dim intloop As Integer
                For intloop = 0 To dtgPettyDtl.Items.Count - 1
                    'lblAmountdtg = CType(dtgPettyDtl.Items(intloop).Cells(3).FindControl("lblAMountdtg"), Label)
                    lblAmountReq = CType(dtgPettyDtl.Items(intloop).Cells(2).FindControl("lblAmountReq"), Label)
                    lblNotesdtg = CType(dtgPettyDtl.Items(intloop).Cells(4).FindControl("lblNotesdtg"), Label)
                    txtAmountdetail = CType(dtgPettyDtl.Items(intloop).Cells(3).FindControl("txtAmountdetail"), ucNumberFormat)
                    With txtAmountdetail
                        .RequiredFieldValidatorEnable = True
                        .RangeValidatorEnable = False
                        .TextCssClass = "numberAlign regular_text"
                        .Text = FormatNumber(lblAmountReq.Text, 2)
                    End With

                    txtNotesdtg = CType(dtgPettyDtl.Items(intloop).Cells(4).FindControl("txtNotesdtg"), TextBox)
                    lblNotesdtg.Text = ""
                    txtNotesdtg.Visible = True
                    txtNotesdtg.Text = ""

                Next
                oBGNODate.BindBGNODate(" bankaccountid = '" & Me.BankAccountID & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")


            End If
            'Else
            '    ShowMessage(lblMessage, "Harap Login di kantor Pusat", True)
            '    pnlsearch.Visible = True
            '    pnlDtGrid.Visible = True
            'End If

        End If
    End Sub

    Private Sub FillRequest(ByVal str As String, ByVal sort As String)
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = str
            .BranchId = Me.BranchIDTo
            .SortBy = sort.Trim
        End With
        oCustomClass = oController.ListPaymentDetail(oCustomClass)
        odatatable = oCustomClass.ListRequest
        dtgPettyDtl.DataSource = odatatable
        dtgPettyDtl.DataBind()
    End Sub
    Private Sub FillRequestHistoryReject(ByVal str As String, ByVal sort As String)
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = str
            .BranchId = Me.BranchIDTo
            .SortBy = sort.Trim
        End With
        oCustomClass = oController.ListPaymentHistoryReject(oCustomClass)
        odatatable = oCustomClass.ListRequest
        DtgAgreeHistory.DataSource = odatatable
        DtgAgreeHistory.DataBind()
    End Sub
    Public Sub SortPetty(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortPettydtg, "DESC") > 0 Then
            Me.SortPettydtg = e.SortExpression
        Else
            Me.SortPettydtg = e.SortExpression + " DESC"
        End If
        FillRequest(lblRequestNo.Text.Trim, Me.SortPettydtg)
        FillRequestHistoryReject(lblRequestNo.Text.Trim, Me.SortPettydtg)
    End Sub

    Private Sub FillCboAccount(ByVal strBankPurpose As String)
        Dim odatatable As New DataTable
        Dim Controller As New TransferForReimbursePCController
        Dim Customclass As New Parameter.TransferForReimbursePC
        With Customclass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        Customclass = Controller.ListBankAccount(Customclass)
        odatatable = Customclass.ListTRFund
        Me.DataBank = odatatable
        'With cboBankAccount
        '    .DataSource = odatatable
        '    .DataTextField = "BankAccountName"
        '    .DataValueField = "BankAccountID"
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = ""
        'End With

        Dim dtBankAccount As New DataTable
        Dim oDataUserControlController As New DataUserControlController

        dtBankAccount = oDataUserControlController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), "BA", strBankPurpose)

        With cboBankAccount
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtBankAccount
            .DataBind()

            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

    End Sub
    Private Sub FillCboAccountBranch()
        Dim odatatable As New DataTable
        Dim Controller As New TransferForReimbursePCController
        Dim Customclass As New Parameter.TransferForReimbursePC
        With Customclass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchIDbranch
        End With
        Customclass = Controller.ListBankAccount(Customclass)
        odatatable = Customclass.ListTRFund
        Me.DataBank = odatatable
        With cboBankaccountBranch
            .DataSource = odatatable
            .DataTextField = "BankAccountName"
            .DataValueField = "BankAccountID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        FillCbo()
    End Sub

    Private Sub imbNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext.Click

        Try
            If CheckFeature(Me.Loginid, Me.FormID, "Next", "MAXILOAN") Then
                Dim lblseqnum As Label
                Dim lblNotesdtg As Label
                Dim lblAmountTransfer As Label
                Dim txtNotesdtg As TextBox
                Dim txtAmountdetail As ucNumberFormat
                Dim intloop, intindex As Integer
                Dim intamount As Double
                Dim oDataTable As New DataTable
                Dim oTable As New DataTable
                Dim oRow As DataRow
                With oTable
                    .Columns.Add(New DataColumn("SequenceNo", GetType(String)))
                    .Columns.Add(New DataColumn("Amount", GetType(String)))
                    .Columns.Add(New DataColumn("Notes", GetType(String)))
                End With
                For intloop = 0 To dtgPettyDtl.Items.Count - 1
                    oRow = oTable.NewRow
                    'lblAmountdtg = CType(dtgPettyDtl.Items(intloop).Cells(4).FindControl("lblAMountdtg"), Label)
                    lblAmountTransfer = CType(dtgPettyDtl.Items(intloop).Cells(4).FindControl("lblAmountTransfer"), Label)
                    lblNotesdtg = CType(dtgPettyDtl.Items(intloop).Cells(4).FindControl("lblNotesdtg"), Label)
                    txtAmountdetail = CType(dtgPettyDtl.Items(intloop).Cells(3).FindControl("txtAmountdetail"), ucNumberFormat)
                    lblseqnum = CType(dtgPettyDtl.Items(intloop).FindControl("lblseqnum"), Label)
                    With txtAmountdetail
                        .RequiredFieldValidatorEnable = True
                        .RangeValidatorEnable = False
                        .TextCssClass = "numberAlign regular_text"
                    End With

                    txtNotesdtg = CType(dtgPettyDtl.Items(intloop).Cells(4).FindControl("txtNotesdtg"), TextBox)
                    'lblAmountdtg.Text = FormatNumber(txtAmountdetail.Text.Trim, 2)
                    lblAmountTransfer.Text = FormatNumber(txtAmountdetail.Text.Trim, 2)
                    lblNotesdtg.Text = txtNotesdtg.Text.Trim
                    txtNotesdtg.Visible = False
                    txtAmountdetail.Visible = False
                    If txtAmountdetail.Text.Trim <> "" Then
                        intamount += CDbl(txtAmountdetail.Text.Trim)
                    End If
                    oRow("SequenceNo") = lblseqnum.Text.Trim
                    oRow("Amount") = txtAmountdetail.Text.Trim
                    oRow("Notes") = lblNotesdtg.Text.Trim
                    oTable.Rows.Add(oRow)
                Next
                Me.BankAccountID = cboBankAccount.SelectedItem.Value.Trim
                lblTotalAmount.Text = FormatNumber(intamount, 2)
                lblValueDate.Text = txtValueDate.Text
                lblNotes.Text = txtNotes.Text.Trim
                lblReferenceNo.Text = txtReferenceNo.Text.Trim
                lblBankAccountDtl.Text = cboBankAccount.SelectedItem.Text.Trim
                txtNotes.Visible = False
                txtValueDate.Visible = False
                txtReferenceNo.Visible = False
                cboBankAccount.Visible = False
                ButtonSave.Visible = True
                ButtonNext.Visible = False
                oDataTable = Me.DataBank
                intindex = cboBankAccount.SelectedIndex
                Me.EndingBalance = CDbl(oDataTable.Rows(intindex - 1)("EndingBalance"))
                BankType = CType(oDataTable.Rows(intindex - 1)("BankAccountType"), String)
                FillCboBilyet(cboBankAccount.SelectedItem.Value.Trim)
                Me.BankType = BankType.Trim
                Me.DataTransfer = oTable
                If Not (oBGNODate.BGNumber = "None" Or oBGNODate.BGNumber = "Select One") Then
                    oBGNODate.InDueDate = Format(Me.BusinessDate, "dd/MM/yyyy")
                Else
                    oBGNODate.InDueDate = ""
                End If

                cboCaraBayar.Enabled = False
                'oBGNODate.InDueDateEnable = False
                oBGNODate.BGNumberEnable = False
            End If

        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
            Exit Sub
        End Try


    End Sub

    Private Sub FillCboBilyet(ByVal str As String)
        Dim odatatable As New DataTable
        Dim customclass As New Parameter.TransferForReimbursePC
        Dim controller As New TransferForReimbursePCController
        With customclass
            .strConnection = GetConnectionString()
            .BankAccountID = str
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        customclass = controller.ListBilyetGiro(customclass)
        odatatable = customclass.ListTRFund
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oDataTable As New DataTable
        Dim customclass As New Parameter.TransferForReimbursePC
        Dim controller As New TransferForReimbursePCController

        If PnlBGNO.Visible = True Then
            If oBGNODate.BGNumber = "" Then
                ShowMessage(lblMessage, "Harap Pilih No Bilyet Giro", True)
                Exit Sub
            End If
        End If
        'If Me.BankType = "C" Then
        With customclass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .Amount = CDbl(lblTotalAmount.Text.Trim)
            .BankAccountID = Me.BankAccountID
        End With
        customclass = controller.CheckEndingBalance(customclass)
        If customclass.str <> "" Then
            ShowMessage(lblMessage, "Saldo tidak cukup", True)
            Exit Sub
        End If
        ' End If

        oDataTable = Me.DataTransfer

        With oCustomClass
            .strConnection = GetConnectionString()
            .ListRequest = oDataTable
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .BusinessDate = Me.BusinessDate
            .ValueDate = ConvertDate(lblValueDate.Text.Trim)
            .ReferenceNo = lblReferenceNo.Text.Trim
            .BankAccountFrom = Me.BankAccountFrom
            .BranchIDTo = Me.BranchIDTo
            .BankAccountID = Me.BankAccountID
            .Amount = CDbl(lblTotalAmount.Text.Trim)
            .Notes = lblNotes.Text.Trim
            .LoginId = Me.Loginid
            .BankType = Me.BankType
            .CompanyID = Me.SesCompanyID
            .RequestNo = lblRequestNo.Text.Trim

            If oBGNODate.InDueDate <> "" Then
                .DueDate = ConvertDate2(oBGNODate.InDueDate)
                .BGNo = oBGNODate.BGNumber
            Else
                .BGNo = "None"
            End If

            .JenisTransfer = rboJenisTransfer.SelectedValue
            .PaymentTypeID = cboCaraBayar.SelectedValue

            If cboCaraBayar.SelectedValue = "BA" Then
                .JenisTransfer = ""
            End If

        End With
        Try
            If cboCaraBayar.SelectedValue = "GT" Then
                oController.EbankPYREQAdd(oCustomClass)
            Else
                oController.SavePaymentRequest(oCustomClass)
            End If

            InitialDefaultPanel()
            FillCbo()
            ShowMessage(lblMessage, "Data saved!", False)
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

    Sub CalcTotal(ByVal _price As String)
        runningTotal += Double.Parse(_price)
    End Sub

    Private Sub dtgPettyDtl_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPettyDtl.ItemDataBound
        Dim lblTemp As Label
        If e.Item.ItemIndex >= 0 Then
            lblTemp = CType(e.Item.FindControl("lblAmountReq"), Label)

            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblAmountdtg"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If

    End Sub

    Private Sub CboCaraBayar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaraBayar.SelectedIndexChanged
        If cboCaraBayar.SelectedItem.Value = "GT" Then
            PnlJenisTransfer.Visible = True
            PnlBGNO.Visible = False
            FillCboAccount("EB")
        ElseIf cboCaraBayar.SelectedItem.Value = "BA" Then
            PnlJenisTransfer.Visible = False
            PnlBGNO.Visible = True
            FillCboAccount("")
        End If
    End Sub
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        If cboStatus.SelectedItem.Value = "RJ" Then
            Panel1.Visible = True
            Panel4.Visible = False
        Else
            Panel1.Visible = False
            Panel4.Visible = True
        End If

    End Sub
    Sub ClearForm()
        cboStatus.SelectedIndex = 0
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        InitialDefaultPanel()
        FillCbo()
        ClearForm()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim oPayReqReject As New Parameter.PaymentRequest With {
           .strConnection = GetConnectionString(),
           .RequestNo = lblRequestNo.Text.Trim,
           .PaymentStatus = "REJ",
           .notes = ("Reject Transfer Permintaan Dana - " + txtNoteReject.Text.Trim),
           .RequestBy = Me.Loginid,
           .RequestDate = Me.BusinessDate,
           .BranchId = Me.sesBranchId.Trim.Replace("'", "")
       }
        Try
            Dim result = p_Controller.PayReqStatusReject(oPayReqReject, "HQ")
            ShowMessage(lblMessage, "Proses Reject Permintaan Pembayaran Berhasil", False)
            InitialDefaultPanel()
            FillCbo()
            imgsearch_Click(Nothing, Nothing)
            ClearForm()
        Catch ex As Exception
            ShowMessage(lblMessage, "Proses Reject Permintaan Pembayaran  Gagal", True)
            Exit Sub
        End Try

    End Sub
End Class