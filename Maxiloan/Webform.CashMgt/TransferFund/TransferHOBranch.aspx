﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferHOBranch.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.TransferHOBranch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchHO" Src="../../Webform.UserController/UcBranchHO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TransferHOBranch</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TRANSFER DANA ANTAR CABANG
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Valuta</label>
                        <asp:TextBox ID="TglValuta" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="TglValuta_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="TglValuta" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="TglValuta"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Transfer Ke</label>
                        <asp:DropDownList ID="cboTransferType" runat="server">
                            <asp:ListItem Value="0" Selected="True">Select One</asp:ListItem>
                            <asp:ListItem Value="1">Tarik dari Cabang</asp:ListItem>
                            <asp:ListItem Value="2">Transfer Ke</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cboTransferType"
                            CssClass="validator_general" ErrorMessage="Harap Pilih Jenis Transfer" InitialValue="0"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Cabang</label>
                        <uc1:ucbranchho id="oBranch" runat="server"></uc1:ucbranchho>
                    </div>
                </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBG" runat="server" Visible="false">
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Valuta</label>
                        <asp:Label ID="lblValueDate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Transfer</label>
                        <asp:Label ID="lblTransferType" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang</label>
                        <asp:Label ID="lblBranch" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            No Bukti Kas Keluar</label>
                        <asp:TextBox ID="txtrefno" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtrefno"
                            CssClass="validator_general" ErrorMessage="Harap isi No Bukti Kas Keluar" Display="Dynamic"
                            Enabled="False"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jenis Bayar</label>
                        <asp:DropDownList ID="cboCaraBayar" AutoPostBack="true" runat="server">
                            <asp:ListItem Selected="True" Value="GT" Text="E-Banking"></asp:ListItem>
                            <asp:ListItem Value="BA" Text="Bank"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <asp:UpdatePanel ID="PnlJenisTransfer" runat="server">
                            <ContentTemplate>
                                <div>
                                    <label class="label_general">
                                        Jenis Transfer</label>
                                    <asp:RadioButtonList ID="rboJenisTransfer" runat="server" class="opt_single" RepeatDirection="Horizontal"
                                        AutoPostBack="true">
                                        <asp:ListItem Selected="True" Value="SKN">SKN</asp:ListItem>
                                        <asp:ListItem Value="RTGS">RTGS</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboCaraBayar" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Dari Rekening</label>
                        <uc1:ucbankaccountbranch id="oFrombankAccount" runat="server"></uc1:ucbankaccountbranch>
                    </div>
                    <div class="form_right">
                        <label>
                            Ke Rekening</label>
                        <uc1:ucbankaccountbranch id="oToBankAccount" runat="server"></uc1:ucbankaccountbranch>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah</label>
                        <uc1:ucnumberformat id="txtamount" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>
                            Keterangan</label>
                        <asp:TextBox ID="txtdesc" runat="server" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtdesc"
                            CssClass="validator_general" ErrorMessage="Harap isi dengan Keterangan" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonNext2" runat="server" CausesValidation="True" Text="Next" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel2" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDetail" runat="server" Visible="False">
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Valuta</label>
                        <asp:Label ID="lblValueDate2" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Transfer</label>
                        <asp:Label ID="lblTransferType2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang</label>
                        <asp:Label ID="lblBranch2" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Bukti Kas Keluar</label>
                        <asp:Label ID="lblRefNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Dari Rekening</label>
                        <asp:Label ID="lblFromBankAccount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Ke Rekening</label>
                        <asp:Label ID="lblToBankAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah</label>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Keterangan</label>
                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                    </div>
                </div>
                </div>
                <asp:UpdatePanel ID="PanelBGNO" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form_box_title">
                            <div class="form_single">
                                <h4>
                                    MEMASUKKAN NO BILYET GIRO</h4>
                            </div>
                        </div>
                        <div class="form_box_uc">
                            <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cboCaraBayar" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel3" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
