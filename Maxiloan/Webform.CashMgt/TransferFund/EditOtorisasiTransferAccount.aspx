﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditOtorisasiTransferAccount.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.EditOtorisasiTransferAccount" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Otorisasi Transfer</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
          
                <asp:Panel ID="Panel1" runat="server">
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h4>
                                Koreksi Transfer Antar Rekening
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Rekening Bank dari
                            </label>
                            <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Tanggal Transaksi
                            </label>
                          
                            <asp:TextBox runat="server" ID="dateFrom" CssClass="small_text"></asp:TextBox>
                            <aspajax:CalendarExtender ID="caldateFrom" runat="server" TargetControlID="dateFrom"
                                Format="dd/MM/yyyy" />S/D
                            <asp:TextBox runat="server" ID="dateTo" CssClass="small_text" ></asp:TextBox>
                            <aspajax:CalendarExtender ID="caldateTo" runat="server" TargetControlID="dateTo"
                                Format="dd/MM/yyyy" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Jumlah Terima
                            </label>
                            <uc1:ucnumberformat id="txtAmountFrom0" runat="server" />
                            </uc1:ucNumberFormat>S/D
                            <uc1:ucnumberformat id="txtamountto0" runat="server" />
                            </uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
                        </asp:Button>&nbsp;
                        <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="False"></asp:Button>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlList" runat="server" Visible="false">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                KOREKSI TRANSFER ANTAR REKENING
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <asp:GridView ID="dtgPaging" CssClass="grid_general" Width="100%" DataKeyNames="TransferNo"
                                AutoGenerateColumns="False" GridLines="None" runat="server" HeaderStyle-CssClass="th"
                                RowStyle-CssClass="item_grid" OnRowCommand="dtgPaging_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="EDIT">
                                        <ItemTemplate>
                                           <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.png"
                                            CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="No. Transfer">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransferNo" Text='<%# Eval("TransferNo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Keterangan">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" Text='<%# Eval("Description") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jumlah">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" Text='<%# formatnumber(Eval("Amount"),0) %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TGL Transaksi">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPostingDate" Text='<%# format(Eval("ValueDate"),"dd MMM yyyy") %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rekening Bank Dari">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountNameFrom" Text='<%# Eval("BankAccountNameFrom") %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rekening Bank Ke">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountNameTo" Text='<%# Eval("BankAccountNameTo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                   

                </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
