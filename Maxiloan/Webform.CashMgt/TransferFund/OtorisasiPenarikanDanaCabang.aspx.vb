﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

#End Region

Public Class OtorisasiPenarikanDanaCabang
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents oBankAccount As ucBankAccountBranch
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtAmountFrom0 As ucNumberFormat
    Protected WithEvents txtamountto0 As ucNumberFormat

#Region " Private Const "

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private Dcontroller As New DataUserControlController
    Private oController As New TransferHOBranchController

#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)
                pnlView.Visible = False
                pnlList.Visible = False
                pnlGrid.Visible = False
                Exit Sub
            Else
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If
                Me.FormID = "OTORPEDC"

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    oBankAccount.BindBankAccountBranch(Me.sesBranchId.Replace("'", "").Trim)
                    Me.SearchBy = ""
                    Me.SortBy = ""

                    pnlView.Visible = False
                    pnlList.Visible = True
                    pnlGrid.Visible = False
                End If

            End If

        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spPenarikanDanaCabangPaging"
        objCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 2000).Value = cmdWhere
        objCommand.Parameters.Add("@SortBy", SqlDbType.VarChar, 100).Value = SortBy
        objread = objCommand.ExecuteReader()
        dtgPaging.DataSource = objread

        Try
            dtgPaging.DataBind()
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

        pnlList.Visible = True
        pnlGrid.Visible = True
        pnlView.Visible = False
    End Sub
    Sub EditOtorisasi(ByVal transferNo As String, ByVal otorisasi As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spOtorisasiTransferFundTransactionEdit"
        objCommand.Parameters.Add("@TransferNo", SqlDbType.VarChar, 50).Value = transferNo
        objCommand.Parameters.Add("@Otorisasi", SqlDbType.Char, 1).Value = otorisasi
        objread = objCommand.ExecuteReader()


        Try
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Sub
    Protected Sub imgSearch_Click(sender As Object, e As EventArgs) Handles imgSearch.Click


        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Visible = False
        Me.SearchBy = " where tft.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
        Me.SearchBy = Me.SearchBy & " tft.TransferType = 'TRFEC' and isnull(tft.Otorisasi,'')= 'N' "


        If obankaccount.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " AND tft.BankAccountIdTo = '" & oBankAccount.BankAccountID.Trim & "' "
        End If
        If (dateFrom.Text <> "" And dateTo.Text = "") Then


            Me.SearchBy = Me.SearchBy & " and  tft.ValueDate ='" & ConvertDate2(dateFrom.Text.Trim) & "' "
        ElseIf (dateFrom.Text <> "" And dateTo.Text <> "") Then
            Me.SearchBy = Me.SearchBy & " and tft.ValueDate BETWEEN '" & ConvertDate2(dateFrom.Text.Trim) & "' and  '" & ConvertDate2(dateTo.Text.Trim) & "' "



        End If

        If IsNumeric(txtAmountFrom0.Text.Trim) And IsNumeric(txtamountto0.Text.Trim) Then
            If CDbl(txtAmountFrom0.Text.Trim) >= 0 And CDbl(txtamountto0.Text.Trim) > 0 Then
                Me.SearchBy = Me.SearchBy & "AND (tft.Amount BETWEEN " & CDbl(txtAmountFrom0.Text.Trim) & " AND " & CDbl(txtamountto0.Text.Trim) & ")"
            End If
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Protected Sub imbReset_Click(sender As Object, e As EventArgs) Handles imbReset.Click
        Response.Redirect("OtorisasiPenarikanDanaCabang.aspx")
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

        Try

            Dim oEntities As New Parameter.TransferAccount
            oEntities.strConnection = GetConnectionString()

            oEntities.CompanyID = Me.SesCompanyID
            oEntities.TransferNO = hdfTransferNo.Value
            oEntities.BusinessDate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
            oEntities.LoginId = Me.Loginid
            oController.OtorisasiPenarikanDanaCabangSave(oEntities)

            Response.Redirect("OtorisasiPenarikanDanaCabang.aspx?message=Otorisasi berhasil di proses")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Private Sub BtnReject_Click(sender As Object, e As System.EventArgs) Handles BtnReject.Click
        Try

            Dim oEntities As New Parameter.TransferAccount
            oEntities.strConnection = GetConnectionString()

            oEntities.CompanyID = Me.SesCompanyID
            oEntities.TransferNO = hdfTransferNo.Value
            oEntities.BusinessDate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
            oEntities.LoginId = Me.Loginid
            oController.OtorisasiPenarikanDanaCabangReject(oEntities)
            Response.Redirect("OtorisasiPenarikanDanaCabang.aspx?message=Reject berhasil")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim lblTransferNo As Label
        If e.CommandName = "Otor" Then
            lblTransferNo = CType(e.Item.FindControl("lblTransferNo"), Label)

            Dim otorController As New InstallRcvController
            Dim oInstal As New Parameter.InstallRcv
            Dim oData As New DataTable
            oInstal.strConnection = GetConnectionString()
            oInstal.SPName = "spGetPenarikanDanaCabang"
            oInstal.WhereCond = " TransferNo='" & lblTransferNo.Text.Trim & "'"
            oData = otorController.GetSP(oInstal).ListData()

            If oData.Rows.Count > 0 Then
                lblTglValuta.Text = CDate(oData.Rows(0).Item("ValueDate")).ToString("dd/MM/yyyy")
                lbloToBankAccount.Text = oData.Rows(0).Item("BankAccountNameTo")
                lblbranch.Text = oData.Rows(0).Item("BranchFullName")
                lbloFrombankAccount.Text = oData.Rows(0).Item("BankAccountNameFrom")
                lblamount.Text = FormatNumber(oData.Rows(0).Item("Amount"), 2)
                lbldesc.Text = oData.Rows(0).Item("Notes")
                hdfTransferNo.Value = oData.Rows(0).Item("TransferNo")
            End If

            pnlView.Visible = True
            pnlList.Visible = False
            pnlGrid.Visible = False
        End If
    End Sub
    Private Sub BtnCancel_Click(sender As Object, e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("OtorisasiPenarikanDanaCabang.aspx")
    End Sub
End Class