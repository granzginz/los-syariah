﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferAccount.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.TransferAccount" %>

<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transfer Account</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%# Request.servervariables("SERVER_NAME")%>/';

        function DoPostBack() {
            __doPostBack('Jlookup', '');
        }
    </script>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TRANSFER ANTAR REKENING</h3>
                </div>
            </div>
            <asp:Panel ID="pnlValuta" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Transaksi</label>
                        <asp:TextBox ID="TglValuta" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="TglValuta_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="TglValuta" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="TglValuta"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Dari Rekening</label><asp:HiddenField ID="hdTransperNo" runat="server" />
                        <asp:DropDownList ID="cboBankAccount" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                        <asp:Label ID="lblBankAccountDtl" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="rfvBankAccount" runat="server" CssClass="validator_general"
                            ControlToValidate="cboBankAccount" ErrorMessage="Harap isi rekening Bank"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 
            </asp:Panel>
          
            <asp:Panel ID="pnlList" runat="server">
               <asp:UpdatePanel ID="PnlBankAccount" runat="server">
                    <ContentTemplate>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Bayar</label>
                        <asp:DropDownList ID="cboCaraBayar" AutoPostBack="true" runat="server">
                            <asp:ListItem Selected="True" Value="GT" Text="E-Banking"></asp:ListItem>
                            <asp:ListItem Value="BA" Text="Bank"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <asp:UpdatePanel ID="PnlJenisTransfer" runat="server">
                    <ContentTemplate>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_general">
                                    Jenis Transfer</label>
                                <asp:RadioButtonList ID="rboJenisTransfer" runat="server" class="opt_single" RepeatDirection="Horizontal"
                                    AutoPostBack="true">
                                    <asp:ListItem Selected="True" Value="SKN">SKN</asp:ListItem>
                                    <asp:ListItem Value="RTGS">RTGS</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cboCaraBayar" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
               </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cboBankAccount" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Bukti Kas Keluar</label>
                        <asp:TextBox ID="txtrefno" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtrefno"
                            ErrorMessage="Harap isi No Bukti Kas Keluar" Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah</label>
                        <uc1:ucnumberformat id="txtamount" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Keterangan</label>
                        <asp:TextBox ID="txtdesc" runat="server" Width="248px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="validator_general"
                            Display="Dynamic" ErrorMessage="Harap isi dengan keterangan" ControlToValidate="txtdesc"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBG" runat="server" Visible="False">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Valuta</label>
                        <asp:Label ID="lblValueDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Bukti Kas Keluar</label>
                        <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Dari Rekening</label>
                        <asp:Label ID="lblFromBankAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah</label>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Keterangan</label>
                        <asp:Label ID="lbldesc" runat="server"></asp:Label>
                    </div>
                </div>
                <asp:UpdatePanel ID="PnlBGNO" runat="server">
                    <ContentTemplate>
                        <div class="form_box_title">
                            <div class="form_single">
                                <h4>
                                    Ke Rekening</h4>
                            </div>
                        </div>
                        <div class="form_box_uc">
                            <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cboCaraBayar" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Ke Rekening</label>
                        <asp:DropDownList ID="cboBankAccount2" runat="server">
                        </asp:DropDownList>
                        <asp:Label ID="lblBankAccountDtl2" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="validator_general"
                            ControlToValidate="cboBankAccount2" ErrorMessage="Harap isi rekening Bank"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Bukti Kas Masuk</label>
                        <asp:TextBox runat="server" ID="txtRefNo2"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
