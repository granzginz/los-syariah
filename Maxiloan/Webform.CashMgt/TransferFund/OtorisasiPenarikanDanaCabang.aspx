﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtorisasiPenarikanDanaCabang.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.OtorisasiPenarikanDanaCabang" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Otorisasi Transfer</title>    
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            OTORISASI PENARIKAN DANA CABANG
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Rekening Bank HO
                        </label>
                        <uc1:ucbankaccountbranch id="oBankAccount" runat="server"></uc1:ucbankaccountbranch>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Transaksi
                        </label>
                        <asp:TextBox runat="server" ID="dateFrom" CssClass="small_text"></asp:TextBox>
                        <aspajax:CalendarExtender ID="caldateFrom" runat="server" TargetControlID="dateFrom"
                            Format="dd/MM/yyyy" />
                        S/D
                        <asp:TextBox runat="server" ID="dateTo" CssClass="small_text"></asp:TextBox>
                        <aspajax:CalendarExtender ID="caldateTo" runat="server" TargetControlID="dateTo"
                            Format="dd/MM/yyyy" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah Terima
                        </label>
                        <uc1:ucnumberformat id="txtAmountFrom0" runat="server" />
                        </uc1:ucNumberFormat>S/D
                        <uc1:ucnumberformat id="txtamountto0" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlGrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR OTORISASI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                            BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                            AllowSorting="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" CommandName="Otor" ID="idOtor" Text="Otor"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="No. Transfer">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransferNo" Text='<%# Eval("TransferNo") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Keterangan">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" Text='<%# Eval("Description") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Jumlah">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" Text='<%# formatnumber(Eval("Amount"),2) %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TGL Transaksi">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPostingDate" Text='<%# format(Eval("ValueDate"),"dd MMM yyyy") %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rekening Bank Dari">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountNameFrom" Text='<%# Eval("BankAccountNameFrom") %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rekening Bank Ke">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountNameTo" Text='<%# Eval("BankAccountNameTo") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlView">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            PENARIKAN DANA CABANG
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Valuta</label>
                        <asp:Label runat="server" ID="lblTglValuta"></asp:Label>
                        <asp:HiddenField ID="hdfTransferNo" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Penampung</label>
                        <asp:Label runat="server" ID="lbloToBankAccount"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <h3>
                            DITARIK DARI CABANG
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label runat="server" ID="lblbranch"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Dari Rekening</label>
                        <asp:Label runat="server" ID="lbloFrombankAccount"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jumlah</label>
                        <asp:Label runat="server" ID="lblamount"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Keterangan</label>
                        <asp:Label runat="server" ID="lbldesc"></asp:Label>
                    </div>
                </div>
                <div class="form_button">                    
                        <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
                        </asp:Button>
                        <asp:Button ID="BtnEdit" runat="server" Visible="false" Text="Edit" CssClass="small button orange">
                        </asp:Button>
                        <asp:Button ID="BtnReject" runat="server" Text="Reject" CssClass="small button red">
                        </asp:Button>
                        <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CssClass="small button gray" />                    
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
