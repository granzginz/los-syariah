﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PenarikanDanaCabang
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oTobankAccount As ucBankAccountBranch
    Protected WithEvents oFrombankAccount As ucBankAccountBranch
    Protected WithEvents obranch As ucBranchHO
    Protected WithEvents txtamount As ucNumberFormat

    Dim strbanktype As String
    Dim stmwhereBranch As String
    Dim strbranch As String
    Dim strbranchto As String
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.TransferAccount
    Private oController As New TransferHOBranchController
    Private m_controller As New DataUserControlController

#End Region

#Region "Property"
    Private Property BranchIDFrom() As String
        Get
            Return CStr(ViewState("BranchIDFrom"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDFrom") = Value
        End Set
    End Property

    Private Property BranchIDTo() As String
        Get
            Return CStr(ViewState("BranchIDTo"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDTo") = Value
        End Set
    End Property

    Private Property FromBankAccount() As String
        Get
            Return (CType(ViewState("FromBankAccount"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FromBankAccount") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            lblMessage.Visible = False
            Me.FormID = "TRFPEDC"

            If Request.QueryString("message") = "ok" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            Else
                lblMessage.Visible = False
            End If

            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)
                pnlInput.Visible = False
                Exit Sub
            Else
                pnlInput.Visible = True
            End If

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                oTobankAccount.BindBankAccountBranch(Me.sesBranchId.Replace("'", "").Trim)
                BindBranch()
                Me.SearchBy = ""
                Me.SortBy = ""
                TglValuta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                With txtamount
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
            End If
        End If
    End Sub

    Sub BindBranch()
        cmbbranch.Items.Clear()
        Dim DtBranchHO As New DataTable
        Dim dvBranchHO As New DataView

        stmwhereBranch = " branchid <> " & Me.sesBranchId.Replace("'", "").Trim
        DtBranchHO = m_controller.GetBranchHO(GetConnectionString, stmwhereBranch)
        dvBranchHO = DtBranchHO.DefaultView

        With cmbbranch
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvBranchHO
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Sub OnSelectedIndexChanged_cmbbranch()
        oFrombankAccount.BindBankAccountBranch(cmbbranch.SelectedValue.Trim)
    End Sub
    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("PenarikanDanaCabang.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim DueDate As Date = Nothing
            If ConvertDate2(TglValuta.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Valuta Tidak Boleh Melebihi Tanggal Sistem", True)
                Exit Sub
            End If
            If CDbl(txtamount.Text) <= 0 Then
                ShowMessage(lblMessage, "Jumlah harus > 0 ", True)
                Exit Sub
            End If

            'Dim Saldo As Decimal = GetSaldo()
            'If CDec(txtamount.Text) > Saldo Then
            '    ShowMessage(lblMessage, "Saldo anda sebesar " & FormatNumber(Saldo, 0) & " tidak mencukupi untuk melanjutkan transaksi.", True)
            '    Exit Sub
            'End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BranchIDfrom = cmbbranch.SelectedValue.Trim
                .BranchIDTo = Me.sesBranchId.Replace("'", "")
                .bankAccountTo = oTobankAccount.BankAccountID
                .bankAccountFrom = oFrombankAccount.BankAccountID
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .Amount = CDbl(txtamount.Text)
                .valueDate = ConvertDate2(TglValuta.Text)
                .Notes = txtdesc.Text
                .CompanyID = Me.SesCompanyID
            End With

            Try
                oController.PenarikanDanaCabangSave(oCustomClass)
                Response.Redirect("PenarikanDanaCabang.aspx?message=ok")
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    Sub ClearForm()
        TglValuta.Text = ""
        txtamount.Text = ""
    End Sub

    Public Function GetSaldo() As Decimal
        Dim Saldo As Decimal = 0

        Dim oCustom As New Parameter.RefundInsentif
        Dim m_Insentif As New RefundInsentifController

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " BranchID = '" & cmbbranch.SelectedValue.Trim & "' and BankAccountID = '" & oFrombankAccount.BankAccountID.Trim & "'"
            .SPName = "spGetSaldo"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            Saldo = CDec(IIf(IsDBNull(oCustom.ListDataTable.Rows(0).Item("EndingBalance")), 0, oCustom.ListDataTable.Rows(0).Item("EndingBalance")))
        End If

        Return Saldo
    End Function
End Class