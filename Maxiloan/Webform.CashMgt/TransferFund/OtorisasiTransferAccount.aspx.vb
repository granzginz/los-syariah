﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

#End Region

Public Class OtorisasiTransferAccount
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtAmountFrom0 As ucNumberFormat
    Protected WithEvents txtamountto0 As ucNumberFormat

#Region " Private Const "

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private Dcontroller As New DataUserControlController
    Private oController As New TransferAccountController

#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_UPDATE_SUCCESS, False, True))
            End If
            Me.FormID = "OTOTRANSAK"
            obankaccount.BankPurpose = ""
            obankaccount.BankType = ""
            obankaccount.IsAll = True
            obankaccount.BindBankAccount()



            Me.SearchBy = ""
            Me.SortBy = ""

            pnlList.Visible = False
            dtgPaging.Visible = False
        End If
    End Sub
    
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To dtgPaging.Rows.Count - 1
            chkItem = CType(dtgPaging.Rows(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
   
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spTransferFundTransaction"
        objCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 2000).Value = cmdWhere
        objCommand.Parameters.Add("@SortBy", SqlDbType.VarChar, 100).Value = SortBy
        objread = objCommand.ExecuteReader()
        dtgPaging.DataSource = objread

        Try
            dtgPaging.DataBind()
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
        pnlList.Visible = True
        'pnlDatagrid.Visible = True
    End Sub
    Sub EditOtorisasi(ByVal transferNo As String, ByVal otorisasi As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spOtorisasiTransferFundTransactionEdit"
        objCommand.Parameters.Add("@TransferNo", SqlDbType.VarChar, 50).Value = transferNo
        objCommand.Parameters.Add("@Otorisasi", SqlDbType.Char, 1).Value = otorisasi
        objread = objCommand.ExecuteReader()


        Try
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Sub

    Protected Sub imgSearch_Click(sender As Object, e As EventArgs) Handles imgSearch.Click
     

        If SessionInvalid() Then
            Exit Sub
        End If
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
        lblMessage.Visible = False
        Me.SearchBy = " where tft.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
        Me.SearchBy = Me.SearchBy & " tft.TransferType = 'TRFAA' and isnull(tft.Otorisasi,'')= 'N' "


        If obankaccount.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " AND tft.BankAccountIdFrom = '" & obankaccount.BankAccountID.Trim & "' "
        End If
        If (dateFrom.Text <> "" And dateTo.Text = "") Then


            Me.SearchBy = Me.SearchBy & " and  tft.ValueDate ='" & ConvertDate2(dateFrom.Text.Trim) & "' "
        ElseIf (dateFrom.Text <> "" And dateTo.Text <> "") Then
            Me.SearchBy = Me.SearchBy & " and tft.ValueDate BETWEEN '" & ConvertDate2(dateFrom.Text.Trim) & "' and  '" & ConvertDate2(dateTo.Text.Trim) & "' "



        End If

        If IsNumeric(txtAmountFrom0.Text.Trim) And IsNumeric(txtamountto0.Text.Trim) Then
            If CDbl(txtAmountFrom0.Text.Trim) >= 0 And CDbl(txtamountto0.Text.Trim) > 0 Then
                Me.SearchBy = Me.SearchBy & "AND (tft.Amount BETWEEN " & CDbl(txtAmountFrom0.Text.Trim) & " AND " & CDbl(txtamountto0.Text.Trim) & ")"
            End If
        End If



        pnlList.Visible = True
        dtgPaging.Visible = True


        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub imbReset_Click(sender As Object, e As EventArgs) Handles imbReset.Click
        Server.Transfer("OtorisasiTransferAccount.aspx")
    End Sub

#Region "save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

        Try

            Dim oEntities As New Parameter.TransferAccount
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To dtgPaging.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(dtgPaging.Rows(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim lblTransferNo As Label = CType(dtgPaging.Rows(n).FindControl("lblTransferNo"), Label)

                    oEntities.CompanyID = Me.SesCompanyID
                    oEntities.TransferNO = lblTransferNo.Text.Trim
                    oEntities.BusinessDate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
                    oEntities.LoginId = Me.Loginid
                    oController.SaveTransferAccountOtorisasi(oEntities)
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di simpan", False)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

    

    Private Sub imgEdit_Click(sender As Object, e As System.EventArgs) Handles imgEdit.Click

        Try




            For n As Integer = 0 To dtgPaging.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(dtgPaging.Rows(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim lblTransferNo As Label = CType(dtgPaging.Rows(n).FindControl("lblTransferNo"), Label)


                    Call EditOtorisasi(lblTransferNo.Text.Trim, "E")
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di diubah", False)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub imbReject_Click(sender As Object, e As System.EventArgs) Handles imbReject.Click
        Try




            For n As Integer = 0 To dtgPaging.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(dtgPaging.Rows(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim lblTransferNo As Label = CType(dtgPaging.Rows(n).FindControl("lblTransferNo"), Label)


                    Call EditOtorisasi(lblTransferNo.Text.Trim, "R")
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di diubah", False)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
End Class