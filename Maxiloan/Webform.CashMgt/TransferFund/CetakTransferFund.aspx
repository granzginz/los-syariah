﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CetakTransferFund.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.CetakTransferFund" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cetak Transfer Uang</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
          
                <asp:Panel ID="Panel1" runat="server">
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h4>
                                Cetak Transfer Uang 
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Rekening Bank dari
                            </label>
                            <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Tanggal Transaksi
                            </label>
                          
                            <asp:TextBox runat="server" ID="dateFrom" CssClass="small_text"></asp:TextBox>
                            <aspajax:CalendarExtender ID="caldateFrom" runat="server" TargetControlID="dateFrom"
                                Format="dd/MM/yyyy" />S/D
                            <asp:TextBox runat="server" ID="dateTo" CssClass="small_text" ></asp:TextBox>
                            <aspajax:CalendarExtender ID="caldateTo" runat="server" TargetControlID="dateTo"
                                Format="dd/MM/yyyy" />
                        </div>
                    </div>
                    <%--<div class="form_box">
                        <div class="form_single">
                            <label>
                                Jumlah Terima
                            </label>
                            <uc1:ucnumberformat id="txtAmountFrom0" runat="server" />
                            S/D
                            <uc1:ucnumberformat id="txtamountto0" runat="server" />
                           
                        </div>
                    </div>--%>
                    <div class="form_button">
                        <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
                        </asp:Button>&nbsp;
                        <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="False"></asp:Button>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlList" runat="server" Visible="false">
                <div class="form_box_title" style="float:left">
                &nbsp&nbsp&nbsp<asp:Label ID="label1" runat="server">Request</asp:Label><asp:DropDownList ID="author1" runat="server" Width ="7%" CssClass="medium_text"/> 
                &nbsp&nbsp&nbsp<asp:Label ID="label2" runat="server">BM</asp:Label><asp:DropDownList ID="author2" runat="server" Width ="7%" CssClass="medium_text"/> 
                &nbsp&nbsp&nbsp<asp:Label ID="label3" runat="server">GM</asp:Label><asp:DropDownList ID="author3" runat="server" Width ="7%" CssClass="medium_text"/>
                &nbsp&nbsp&nbsp<asp:Label ID="label4" runat="server">Direksi</asp:Label><asp:DropDownList ID="author4" runat="server" Width ="7%" CssClass="medium_text"/>
                </div>

                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                CETAK TRANSFER UANG
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>

                                 <asp:TemplateColumn>
                                    <%--<HeaderTemplate>
                                            <asp:CheckBox ID="cbAll" Visible="false" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                    </HeaderTemplate>--%>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                    </ItemTemplate>
                                   </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="No. Transfer">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" />
                                        <ItemStyle HorizontalAlign="Left" Width="13%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransferNo" runat="server" Text='<%#Container.DataItem("TransferNo")%>' />
                                             <asp:HiddenField ID="txtTransferNo" runat="server" Value='<%#Container.DataItem("TransferNo")%>'/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Keterangan">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" Text='<%# Container.DataItem("Description") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Jumlah">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" Text='<%#FormatNumber(Container.DataItem("Amount"), 0) %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TGL Transaksi">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPostingDate" Text='<%#Format(Container.DataItem("ValueDate"), "dd MMM yyyy") %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Rekening Bank Dari">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountNameFrom" Text='<%# Container.DataItem("BankAccountNameFrom") %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Rekening Bank Ke">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountNameTo" Text='<%# Container.DataItem("BankAccountNameTo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div> 
                   

                      <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button blue">
                        </asp:Button>
<%--            <asp:Button ID="imgEdit" runat="server" Text="Edit" CssClass="small button orange"
                CausesValidation="False"></asp:Button>--%>
            <asp:Button ID="imbReject" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
          
        </div>
                </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
