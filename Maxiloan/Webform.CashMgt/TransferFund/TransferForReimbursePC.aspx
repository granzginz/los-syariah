﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferForReimbursePC.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.TransferForReimbursePC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TransferForReimbursePC</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinPetty(pBranch, pPetty) {
            window.open('../../Webform.LoanMnt/Cashier/PettyCash/PettyCashTransaction/PettyCashInquiryView.aspx?BranchID=' + pBranch + '&PettyCashNo=' + pPetty + '&Style=Finance', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinRequest(pBranch, pRequest) {
            window.open('../../Webform.LoanMnt/Cashier/PettyCash/PettyCashReimburse/ViewPettyCashReimburse.aspx?Branch=' + pBranch + '&RequestNo=' + pRequest + '&Style=Finance', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
</head>
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
    <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">                        
            <h3>
                TRANSFER DANA UNTUK PETTY CASH
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box">            
		        <div class="form_left">
                    <label class ="label_req">Cabang</label>
                    <asp:DropDownList ID="cboBranch" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvBranch" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Cabang" CssClass="validator_general"
                        ControlToValidate="cboBranch"></asp:RequiredFieldValidator>
                </div>
		        <div class="form_right">
                    <label>Tanggal Permintaan</label>
                    <asp:TextBox ID="txtRequestDate" runat="server"></asp:TextBox>                   
                    <asp:CalendarExtender ID="txtRequestDate_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtRequestDate" Format ="dd/MM/yyyy"></asp:CalendarExtender> 
                </div>                   
        </div>
        <div class="form_box">            
            <div class="form_left">
            <label>Cari Berdasarkan</label>
            <asp:DropDownList ID="cboSearchBy" runat="server">
                <asp:ListItem Value="">Select One</asp:ListItem>
                <asp:ListItem Value="RequestNo">No Request</asp:ListItem>
                <asp:ListItem Value="Description">Keterangan</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>        
        </div>    
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button></div>
        </div>                        
    </asp:Panel>    
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">
            <h4>DAFTAR PERMINTAAN PETTY CASH</h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPetty" runat="server" Width="100%" OnSortCommand="SortGrid"
                         CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="RequestNo" AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:LinkButton CommandName="Transfer" Text='TRANSFER' runat="server" ID="Linkbutton1"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="REQUESTNO" HeaderText="NO REQUEST">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinRequest('<%#Container.dataItem("BranchID")%>','<%#Container.dataItem("RequestNo")%>');">
                                        <asp:Label ID="lblNoReq" runat="server" Text='<%#Container.dataItem("RequestNo")%>'>
                                        </asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="REQUESTBY" SortExpression="REQUESTBY" HeaderText="PERMINTAAN DARI">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DESCRIPTION" SortExpression="DESCRIPTION" HeaderText="KETERANGAN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AMOUNT" SortExpression="AMOUNT" HeaderText="JUMLAH" DataFormatString="{0:###,###,###.00}" HeaderStyle-CssClass="th_right" >                                                               
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="REQUESTDATE" SortExpression="REQUESTDATE" HeaderText="TGL MINTA"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BAnkaccountname" Visible="False">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankAccountID" Visible="False">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BranchFullName" Visible="false"></asp:BoundColumn>
                        </Columns>                        
                </asp:DataGrid>
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>      
        </div>
        </div>        
        </div>        
    </asp:Panel>
    <asp:Panel ID="pnlTransfer" runat="server">
        <div class="form_box">        
		    <div class="form_left">
                <label>Permintaan Cabang</label>
                <asp:Label ID="lblBranch" runat="server"></asp:Label>
            </div>
		    <div class="form_right">            
            </div>
        </div>
        <div class="form_box">
		    <div class="form_left">
                <label>No Request</label>
                <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
            </div>
		    <div class="form_right">
                <label>Tanggal Permintaan</label>
                <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
		    <div class="form_left">
                <label>Rekening Cabang</label>
                <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
            </div>
		    <div class="form_right">
                <label>Jumlah Permintaan</label>
                <asp:Label ID="lblAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
		    <div class="form_left">
                <label>Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>		    
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>DAFTAR TRANSAKSI PETTY CASH</h4>
        </div>
    </div>                    
    <div class="form_box_header">
    <div class="form_single">
    <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgPettyDtl" runat="server" Width="100%"  CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        AutoGenerateColumns="False"
                        AllowSorting="True" OnSortCommand="SortPetty">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" AutoPostBack="True" runat="server" OnCheckedChanged="checkAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPetty" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PETTYCASHNO" HeaderText="NO PETTY CASH" FooterText="TOTAL">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinPetty('<%#Container.dataItem("BranchID")%>','<%#Container.DataItem("PettyCashNo")%>');">
                                        <asp:Label ID="lblPettyCashNo" runat="server" Text='<%#Container.dataItem("PETTYCASHNO")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>                                
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DESCRIPTION" SortExpression="DESCRIPTION" HeaderText="KETERANGAN">                                
                            </asp:BoundColumn>                            
                            <asp:TemplateColumn HeaderStyle-CssClass="th_right" HeaderText="JUMLAH">                                    
                                    <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                    <ItemTemplate>                                                                                                                       
                                        <uc1:ucNumberFormat ID="txtAmountdetail" runat="server"/>
                                        <%--<asp:Label ID="lblAmountdetail" runat="server" Text='<%#Container.dataItem("AMOUNT")%>'></asp:Label>--%>
                                    </ItemTemplate>                                    
                                </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PETTYCASHDATE" SortExpression="PETTYCASHDATE" HeaderText="TGL PETTY CASH"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="NOTES">                                
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNotesdtg" runat="server" ></asp:TextBox>
                                    <asp:Label ID="lblNotesdtg" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AMOUNT" Visible="false"></asp:BoundColumn>
                        </Columns>                        
            </asp:DataGrid>
    </div>
    </div>    
    </div>     
    <div class="form_box_title">
        <div class="form_single">
            <h4>RINCIAN TRANSFER DANA UNTUK PETTY CASH</h4>
        </div>
    </div>    
        <div class="form_box">        
		    <div class="form_left">
                <label>Jumlah di Transfer</label>
                <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
            </div>
		    <div class="form_right">
                <label class ="label_req">Tanggal Valuta</label>
                <asp:TextBox ID="txtValueDate" runat="server"></asp:TextBox>                   
                    <asp:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtValueDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtValueDate"></asp:RequiredFieldValidator>	
                <asp:Label ID="lblValueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
		    <div class="form_left">
                <label>Cara Bayar</label>
                <asp:DropDownList ID="cboCaraBayar" AutoPostBack="true" runat="server">
                <asp:ListItem Selected="True" Value="GT" Text="E-Banking"></asp:ListItem>
                <asp:ListItem Value="BA" Text="Bank"></asp:ListItem>
                </asp:DropDownList>
            </div>
		    <div class="form_right">                               
                <asp:UpdatePanel ID="PnlJenisTransfer" runat="server">
                        <ContentTemplate>                               
                        <div>                              
                            <label class="label_general">Jenis Transfer</label>
                            <asp:RadioButtonList ID="rboJenisTransfer" runat="server" class="opt_single"  RepeatDirection="Horizontal"
                                AutoPostBack="true">
                                <asp:ListItem Selected="True" Value="SKN">SKN</asp:ListItem>
                                <asp:ListItem Value="RTGS">RTGS</asp:ListItem>  
                            </asp:RadioButtonList>
                        </div>                                                                                                                                                                   
                        </ContentTemplate>
                        <triggers>
                                   <asp:AsyncPostBackTrigger ControlID="cboCaraBayar"  EventName="SelectedIndexChanged" />
                        </triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form_box">
		    <div class="form_left">
                <label>Dari Rekening</label>
                <asp:DropDownList ID="cboBankAccount" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblBankAccountDtl" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="rfvBankAccount" runat="server"  CssClass="validator_general"
                    ControlToValidate="cboBankAccount" ErrorMessage="Harap isi rekening Bank"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right"> 
                <label class ="label_req">No Bukti Kas Keluar</label>
                <asp:TextBox ID="txtReferenceNo" runat="server"  MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtReferenceNo" CssClass="validator_general"
                    ErrorMessage="Harap isi No Bukti Kas Keluar" Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>        
            </div>
        </div>
        <div class="form_box">
		    <div class="form_left">                
                <label class="label_general">Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                <asp:Label ID="lblNotes" runat="server"></asp:Label>
            </div>
        </div>
    </div>                                   
        <asp:UpdatePanel ID="PnlBGNO" runat="server" Visible="false">
            <ContentTemplate>           
                <div class="form_box_title">
                    <div class="form_single">   
                        <h4>MEMASUKKAN No BILYET GIRO</h4>
                    </div>   
                </div>
                <div class="form_box_uc">                        
                        <uc1:UCBGNODATE id="oBgNoDate" runat="server"></uc1:UCBGNODATE>                            
                </div>       
            </ContentTemplate>
            <triggers>
                       <asp:AsyncPostBackTrigger ControlID="cboCaraBayar"  EventName="SelectedIndexChanged" />
            </triggers>
        </asp:UpdatePanel>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonNext" runat="server"  Text="Next" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
         </div>
    </asp:Panel>    
    </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
