﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ViewTransferFund
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Private oCustomClass As New Parameter.TransferFundInquiry
    Private oController As New TransferFundInquiryController
#End Region
#Region "Property"

    Property TransferNo() As String
        Get
            Return viewstate("TransferNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("TransferNo") = Value
        End Set
    End Property
    Property CSSStyle() As String
        Get
            Return viewstate("CSSStyle").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CSSStyle") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'Me.FormID = "VIEWTRANSFERFUNDLIST"
            'If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            Dim CmdWhere As String
            Me.CmdWhere = ""
            Me.SortBy = ""
            Me.TransferNo = ""
            Me.CSSStyle = ""
            Me.BranchID = ""

            Me.TransferNo = Request("TransferNo").ToString
            Me.CSSStyle = Request("Style").ToString
            Me.BranchID = Request("Branch").ToString
            If Me.BranchID <> "ALL" Then
                CmdWhere = CmdWhere + "TransferFundTransaction.BranchId = '" & Me.BranchID & "' and "
            End If
            CmdWhere = "TransferFundTransaction.TransferNo = '" & Me.TransferNo & "'"
            Me.CmdWhere = CmdWhere

            BindDataLabel()

            'End If
        End If
    End Sub
    Sub BindDataLabel()
        Dim oPCReimburse As New Parameter.TransferFundInquiry
        Dim oData As New DataTable
        With oPCReimburse
            .strConnection = getconnectionstring()
            .WhereCond = Me.CmdWhere
        End With
        oPCReimburse = oController.GetViewTransferFund(oPCReimburse)
        If Not oPCReimburse Is Nothing Then
            oData = oPCReimburse.ListData
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
    End Sub
    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)
        lblBranch.Text = oRow.Item(0).ToString.Trim
        LblTransferNo.Text = oRow.Item(1).ToString.Trim
        lblvalueDate.Text = oRow.Item(2).ToString.Trim
        LblTransferType.Text = oRow.Item(3).ToString.Trim
        LblPostingDate.Text = oRow.Item(4).ToString.Trim
        lblReferenceNo.Text = oRow.Item(5).ToString.Trim
        LblAmountTransfer.Text = FormatNumber(oRow.Item(6).ToString.Trim, 2)
        lblDescription.Text = oRow.Item(7).ToString.Trim
        LblBranchTransferFrom.Text = oRow.Item(8).ToString.Trim
        lblBankAccountTransferForm.Text = oRow.Item(9).ToString.Trim
        lblVoucherNoTransferFrom.Text = oRow.Item(10).ToString.Trim
        lblBGNo.Text = oRow.Item(11).ToString.Trim
        lblBGDueDate.Text = oRow.Item(12).ToString.Trim
        lblBranchTransferTo.Text = oRow.Item(13).ToString.Trim
        lblBankAccountTransferTo.Text = oRow.Item(14).ToString.Trim
        lblVoucherNoTransferTo.Text = oRow.Item(15).ToString.Trim
        Lblstatus.Text() = oRow.Item(16).ToString.Trim
        lblStatusDate.Text = oRow.Item(17).ToString.Trim
        lblTransferBy.Text = oRow.Item(18).ToString.Trim
        lblReconcileBy.Text = oRow.Item(19).ToString.Trim
    End Sub

End Class