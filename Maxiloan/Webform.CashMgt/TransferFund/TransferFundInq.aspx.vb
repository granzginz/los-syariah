﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class TransferFundInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(ViewState("BindMenu"))
        End Get
        Set(ByVal Value As String)
            ViewState("BindMenu") = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            Return CType(ViewState("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmount") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.TransferFundInquiry
    Private oController As New TransferFundInquiryController
    Private m_controller As New DataUserControlController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "TRANSFERFUNDINQ"
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        If Not IsPostBack Then
            Dim dtbranch As New DataTable
            dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cboParent
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = dtbranch
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
            Me.CmdWhere = ""
            Me.SortBy = ""
        End If
    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        BindGridInqTransferFund(Me.CmdWhere, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridInqTransferFund(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridInqTransferFund(Me.CmdWhere, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region
#Region " BindGrid"

    Sub BindGridInqTransferFund(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqTransferFund As New Parameter.TransferFundInquiry

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
        End With

        oInqTransferFund = oController.InqTransferFund(oCustomClass)

        With oInqTransferFund
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInqTransferFund.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = sortBy
        DtgTransferFundInquiry.DataSource = dtvEntity
        Try
            DtgTransferFundInquiry.DataBind()
        Catch
            DtgTransferFundInquiry.CurrentPageIndex = 0
            DtgTransferFundInquiry.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
    Private Sub DtgTransferFundInquiry_itemdatabound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgTransferFundInquiry.ItemDataBound
        Dim lblAmount As Label
        Dim lblTotAmount As Label
        Dim hyTransferNo As HyperLink
        Dim hypVoucherNoTo As HyperLink
        Dim hypVoucherNoFrom As HyperLink

        If e.Item.ItemIndex >= 0 Then
            hyTransferNo = CType(e.Item.FindControl("hyTransferNo"), HyperLink)
            hypVoucherNoTo = CType(e.Item.FindControl("hypVoucherNoTo"), HyperLink)
            hypVoucherNoFrom = CType(e.Item.FindControl("hypVoucherNoFrom"), HyperLink)
            hyTransferNo.NavigateUrl = LinkToViewTransferFund(hyTransferNo.Text.Trim, "Finance", cboParent.SelectedItem.Value.Trim)
            hypVoucherNoTo.NavigateUrl = LinkToViewVocuherNoTo(hypVoucherNoTo.Text.Trim)
            hypVoucherNoFrom.NavigateUrl = LinkToViewVocuherNoFrom(hypVoucherNoFrom.Text.Trim)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            Me.TotalAmount += CDbl(lblAmount.Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            lblTotAmount.Text = FormatNumber(Me.TotalAmount, 2)
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            Dim filterby As String
            Me.SortBy = ""
            Me.CmdWhere = " where "
            cmdwhere = ""
            filterby = ""
            Me.TotalAmount = 0.0

            If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
                If txtSearchBy.Text.Trim <> "" Then
                    If cboSearchBy.SelectedItem.Value.Trim = "TransferNo" Then
                        cmdwhere = cmdwhere + "TransferNo LIKE '%" & txtSearchBy.Text.Trim & "%' and "
                        filterby = "Transfer No = " & txtSearchBy.Text.Trim & " and "
                    ElseIf cboSearchBy.SelectedItem.Value.Trim = "Description" Then
                        cmdwhere = cmdwhere + "Description LIKE '%" & txtSearchBy.Text.Trim & "%' and "
                        filterby = "Description = " & txtSearchBy.Text.Trim & " and "
                    ElseIf cboSearchBy.SelectedItem.Value.Trim = "RefNo" Then
                        cmdwhere = cmdwhere + "ReferenceNo LIKE '%" & txtSearchBy.Text.Trim & "%' and "
                        filterby = "Reference No = " & txtSearchBy.Text.Trim & " and "
                    End If
                End If
            End If
            If txtdate.Text.Trim <> "" Then
                cmdwhere = cmdwhere + " CONVERT(char(10),PostingDate,103) <= '" & txtdate.Text.Trim & "' and "
                filterby = filterby + "Posting Date <= " & txtdate.Text.Trim & " and "
            End If
            If cboTransferType.SelectedItem.Value.Trim <> "All" Then
                cmdwhere = cmdwhere + "TransferType = '" & cboTransferType.SelectedItem.Value.Trim & "' and "
                filterby = filterby + "Transfer Type = " & cboTransferType.SelectedItem.Text.Trim & " and "
            End If
            If cboStatus.SelectedItem.Value.Trim <> "All" Then
                If cboStatus.SelectedItem.Value.Trim = "1" Then
                    cmdwhere = cmdwhere + "IsReconcile = '1' and "
                ElseIf cboStatus.SelectedItem.Value.Trim = "0" Then
                End If
                filterby = filterby + "Status = " & cboStatus.SelectedItem.Text.Trim & " and "
            End If
            If cboParent.SelectedItem.Value.Trim <> "ALL" Then
                cmdwhere = cmdwhere + "branchID = '" & cboParent.SelectedItem.Value.Trim & "'"
            Else
                If cmdwhere.Trim <> "" Then
                    cmdwhere = Left(cmdwhere, Len(cmdwhere.Trim) - 4)
                End If
            End If
            Me.CmdWhere = cmdwhere
            If filterby <> "" Then
                filterby = Left(filterby, Len(filterby.Trim) - 4)
            End If
            Me.FilterBy = filterby
            BindGridInqTransferFund(Me.CmdWhere, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If

    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Server.Transfer("TransferFundInq.aspx")
    End Sub


    Private Sub DtgTransferFundInquiry_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgTransferFundInquiry.SortCommand
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInqTransferFund(Me.CmdWhere, Me.SortBy)
    End Sub
#Region "linkTo"
    Function LinkToViewTransferFund(ByVal strTransferNo As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewTransferFund('" & strStyle & "','" & strTransferNo & "','" & strBranch & "' )"
    End Function
    Function LinkToViewVocuherNoFrom(ByVal VoucherNoFrom As String) As String
        Return "javascript:OpenWinViewVoucherNoFrom('" & VoucherNoFrom & "' )"
    End Function
    Function LinkToViewVocuherNoTo(ByVal VoucherNoTo As String) As String
        Return "javascript:OpenWinViewVoucherNoTo('" & VoucherNoTo & "' )"
    End Function
#End Region

End Class