﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TransferForReimbursePC
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private runningTotal As Double = 0

    Private oCustomClass As New Parameter.TransferForReimbursePC
    Private oController As New TransferForReimbursePCController
    Private ChkBoxAll As New CheckBox
    Protected WithEvents oBGNODate As ucBGNoDate
    Private m_controller As New DataUserControlController
#End Region

#Region "Property"
    Private Property SortPettydtg() As String
        Get
            Return CType(ViewState("SortPettydtg"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortPettydtg") = Value
        End Set
    End Property

    Private Property DataBank() As DataTable
        Get
            Return CType(ViewState("DataBank"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataBank") = Value
        End Set
    End Property

    Private Property DataTransfer() As DataTable
        Get
            Return CType(ViewState("DataTransfer"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataTransfer") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property

    Private Property BankAccountCabang() As String
        Get
            Return CType(ViewState("BankAccountCabang"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountCabang") = Value
        End Set
    End Property

    Private Property BranchIDTo() As String
        Get
            Return CType(ViewState("BranchIDTo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDTo") = Value
        End Set
    End Property

    Private Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property
    Private Property SelectAll() As Integer
        Get
            Return CInt(ViewState("SelectAll"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SelectAll") = Value
        End Set
    End Property

    Private Property EndingBalance() As Double
        Get
            Return CInt(ViewState("EndingBalance"))
        End Get
        Set(ByVal Value As Double)
            ViewState("EndingBalance") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "TRFREIMBURSEPC"
        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not IsPostBack Then                
                InitialDefaultPanel()
                FillCbo()
            End If
        End If
    End Sub
    Private Sub FillCbo()
        'Dim oDataTable As New DataTable
        'With oCustomClass
        '    .strConnection = GetConnectionString()
        'End With
        'oCustomClass = oController.ListBranchHO(oCustomClass)
        'oDataTable = oCustomClass.ListTRFund
        'With cboBranch
        '    .DataSource = oDataTable
        '    .DataTextField = "BranchFullName"
        '    .DataValueField = "BranchID"
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = ""
        'End With

        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlTransfer.Visible = False
        lblMessage.Visible = False
    End Sub
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        txtSearchBy.Text = ""
        cboBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtRequestDate.Text = ""
        InitialDefaultPanel()
    End Sub
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", "MAXILOAN") Then
            'Me.SearchBy = "PettyCashReimburse.Status='APR' and PettyCashReimburse.BranchID='" & cboBranch.SelectedItem.Value.Trim & "'"

            If cboBranch.SelectedItem.Value.Trim <> "ALL" Then
                Me.SearchBy = "PettyCashReimburse.Status='APR' and PettyCashReimburse.BranchID='" & cboBranch.SelectedItem.Value.Trim & "'"
            Else
                Me.SearchBy = "PettyCashReimburse.Status='APR' "
            End If

            If txtRequestDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and RequestDate='" & ConvertDate(txtRequestDate.Text) & "'"
            End If
            If cboSearchBy.SelectedItem.Value.Trim <> "" And txtSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " = '" & txtSearchBy.Text.Trim & "'"
            End If
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            Me.BranchIDTo = cboBranch.SelectedItem.Value.Trim
        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        lblMessage.Visible = False

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListPCReimburse(oCustomClass)

        DtUserList = oCustomClass.ListTRFund
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgPetty.DataSource = DvUserList

        Try
            dtgPetty.DataBind()
        Catch
            dtgPetty.CurrentPageIndex = 0
            dtgPetty.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgPetty_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPetty.ItemCommand
        If e.CommandName = "Transfer" Then
            If Me.IsHoBranch = True Then
                If CheckFeature(Me.Loginid, Me.FormID, "Trfer", "MAXILOAN") Then
                    pnlTransfer.Visible = True
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    PnlBGNO.Visible = False
                    ButtonSave.Visible = False
                    ButtonNext.Visible = True
                    lblValueDate.Text = ""
                    txtValueDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                    lblNotes.Text = ""
                    lblReferenceNo.Text = ""
                    lblBankAccountDtl.Text = ""
                    Dim lblNoReq As Label
                    lblNoReq = CType(dtgPetty.Items(e.Item.ItemIndex).Cells(1).FindControl("lblNoReq"), Label)
                    FillPetty(lblNoReq.Text.Trim, "")
                    FillCboAccount()
                    lblBranch.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(8).Text.Trim  'cboBranch.SelectedItem.Text.Trim
                    lblRequestNo.Text = lblNoReq.Text.Trim
                    lblBankAccount.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(6).Text.Trim
                    lblDescription.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(3).Text.Trim
                    lblRequestDate.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(5).Text.Trim
                    lblAmount.Text = FormatNumber(dtgPetty.Items(e.Item.ItemIndex).Cells(4).Text.Trim, 2)
                    lblValueDate.Text = ""
                    txtValueDate.Visible = True
                    lblNotes.Text = ""
                    txtNotes.Visible = True
                    lblReferenceNo.Text = ""
                    txtReferenceNo.Visible = True
                    cboBankAccount.Visible = True
                    txtNotes.Text = ""
                    lblTotalAmount.Text = ""
                    txtReferenceNo.Text = ""
                    cboBankAccount.ClearSelection()
                    lblBankAccountDtl.Text = ""
                    Me.BankAccountCabang = dtgPetty.Items(e.Item.ItemIndex).Cells(7).Text.Trim
                End If
                oBGNODate.BindBGNODate(" bankaccountid = '" & Me.BankAccountID & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
            Else
                ShowMessage(lblMessage, "harap Login di kantor Pusat", True)
                pnlsearch.Visible = True
                pnlDtGrid.Visible = True
            End If

        End If
    End Sub
    Private Sub FillPetty(ByVal str As String, ByVal sort As String)
        Dim txtAmountdetail As ucNumberFormat
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = str
            .BranchId = Me.BranchIDTo
            .SortBy = sort.Trim
        End With
        'oCustomClass = oController.ListPettyCash(oCustomClass)
        oCustomClass = oController.ListPettyCashApr(oCustomClass)
        odatatable = oCustomClass.ListTRFund
        dtgPettyDtl.DataSource = odatatable
        dtgPettyDtl.DataBind()

        Dim intloop As Integer

        For intloop = 0 To dtgPettyDtl.Items.Count - 1
            txtAmountdetail = CType(dtgPettyDtl.Items(intloop).FindControl("txtAmountdetail"), ucNumberFormat)
            With txtAmountdetail
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
                .Enabled = False
            End With
        Next

    End Sub
    Public Sub SortPetty(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortPettydtg, "DESC") > 0 Then
            Me.SortPettydtg = e.SortExpression
        Else
            Me.SortPettydtg = e.SortExpression + " DESC"
        End If
        FillPetty(lblRequestNo.Text.Trim, Me.SortPettydtg)
    End Sub
    Private Sub FillCboAccount()
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        oCustomClass = oController.ListBankAccount(oCustomClass)
        odatatable = oCustomClass.ListTRFund
        Me.DataBank = odatatable
        With cboBankAccount
            .DataSource = odatatable
            .DataTextField = "BankAccountName"
            .DataValueField = "BankAccountID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        FillCbo()
    End Sub
    Private Sub imbNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Next", "MAXILOAN") Then
            Dim txtNotesdtg As TextBox
            Dim txtAmountdetail As ucNumberFormat
            Dim lblNotesdtg, lblpettycahno As Label
            Dim chkPetty As CheckBox
            'Dim intloop, intamount, intindex As Integer
            Dim intloop, intindex As Integer
            Dim intamount As Double
            Dim oDataTable As New DataTable
            Dim BankType As String
            Dim oTable As New DataTable
            Dim oRow As DataRow
            Dim isCheck As Boolean

            With oTable
                .Columns.Add(New DataColumn("PettyCashNo", GetType(String)))
                .Columns.Add(New DataColumn("IsTransfer", GetType(Boolean)))
                .Columns.Add(New DataColumn("Notes", GetType(String)))
            End With
            isCheck = False
            For intloop = 0 To dtgPettyDtl.Items.Count - 1
                chkPetty = CType(dtgPettyDtl.Items(intloop).FindControl("chkPetty"), CheckBox)
                If chkPetty.Checked Then
                    isCheck = True
                End If
            Next
            If isCheck = False Then
                ShowMessage(lblMessage, "Harap dipilih Item petty cash", True)
                Exit Sub
            End If
            If lblTotalAmount.Text > lblAmount.Text Then
                ShowMessage(lblMessage, "Jumlah yang ditransfer melebihi Jumlah Permintaan", True)
                Exit Sub
            End If
            'chkAll = CType(dtgPettyDtl.FindControl("ChkAll"), CheckBox)
            For intloop = 0 To dtgPettyDtl.Items.Count - 1
                oRow = oTable.NewRow
                lblNotesdtg = CType(dtgPettyDtl.Items(intloop).FindControl("lblNotesdtg"), Label)
                lblpettycahno = CType(dtgPettyDtl.Items(intloop).FindControl("lblPettyCashNo"), Label)
                txtNotesdtg = CType(dtgPettyDtl.Items(intloop).FindControl("txtNotesdtg"), TextBox)
                chkPetty = CType(dtgPettyDtl.Items(intloop).FindControl("chkPetty"), CheckBox)
                txtAmountdetail = CType(dtgPettyDtl.Items(intloop).FindControl("txtAmountdetail"), ucNumberFormat)
                With txtAmountdetail
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                    .Enabled = False
                End With


                lblNotesdtg.Text = txtNotesdtg.Text.Trim
                txtNotesdtg.Visible = False
                oRow("PettyCashNo") = lblpettycahno.Text.Trim
                oRow("Notes") = txtNotesdtg.Text.Trim

                If chkPetty.Checked Then
                    oRow("IsTransfer") = 1
                    'intamount = intamount + CInt(txtAmountdetail.Text.Trim)
                    intamount = intamount + CDbl(txtAmountdetail.Text.Trim)
                Else
                    oRow("IsTransfer") = 0
                End If


                chkPetty.Enabled = False
                oTable.Rows.Add(oRow)
                'chkAll = CType(dtgPettyDtl.Items(intloop).FindControl("chkAll"), CheckBox)
                'chkAll.Enabled = False
            Next

            lblTotalAmount.Text = FormatNumber(intamount, 2)
            lblValueDate.Text = txtValueDate.Text
            txtValueDate.Visible = False
            lblNotes.Text = txtNotes.Text.Trim
            txtNotes.Visible = False
            lblReferenceNo.Text = txtReferenceNo.Text.Trim
            txtReferenceNo.Visible = False
            lblBankAccountDtl.Text = cboBankAccount.SelectedItem.Text.Trim()
            Me.BankAccountID = cboBankAccount.SelectedItem.Value.Trim
            cboBankAccount.Visible = False
            ButtonSave.Visible = True
            ButtonNext.Visible = False
            oDataTable = Me.DataBank
            intindex = cboBankAccount.SelectedIndex
            BankType = CType(oDataTable.Rows(intindex - 1)("BankAccountType"), String)
            Me.EndingBalance = CDbl(oDataTable.Rows(intindex - 1)("EndingBalance"))
            FillCboBilyet(cboBankAccount.SelectedItem.Value.Trim)
            Me.BankType = BankType.Trim
            Me.DataTransfer = oTable
            ChkBoxAll.Enabled = False
            'chkAll.Enabled = False            
            If Not (oBGNODate.BGNumber = "None" Or oBGNODate.BGNumber = "Select One") Then
                oBGNODate.InDueDate = Format(Me.BusinessDate, "dd/MM/yyyy")
            Else
                oBGNODate.InDueDate = ""
            End If

            cboCaraBayar.Enabled = False
            'oBGNODate.InDueDateEnable = False
            oBGNODate.BGNumberEnable = False            
        End If
    End Sub
    Private Sub FillCboBilyet(ByVal str As String)
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .BankAccountID = str
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        oCustomClass = oController.ListBilyetGiro(oCustomClass)
        odatatable = oCustomClass.ListTRFund
    End Sub
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim DueDate As Date = Nothing

        If CheckFeature(Me.Loginid, Me.FormID, "Save", "MAXILOAN") Then

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Dim oDataTable As New DataTable
            'If Me.BankType = "C" Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Replace(Me.sesBranchId, "'", "")
                '.Amount = CInt(lblTotalAmount.Text.Trim)
                .Amount = CDbl(lblTotalAmount.Text.Trim)
                .BankAccountID = Me.BankAccountID
            End With
            oCustomClass = oController.CheckEndingBalance(oCustomClass)
            If oCustomClass.str <> "" Then
                ShowMessage(lblMessage, "Saldo tidak Cukup", True)
                Exit Sub
            End If
            'End If
            If PnlBGNO.Visible = True Then
                If oBGNODate.BGNumber = "" Then
                    ShowMessage(lblMessage, "harap pilih No Bilyet Giro", True)
                    Exit Sub
                End If
            End If
            oDataTable = Me.DataTransfer
            With oCustomClass
                .strConnection = GetConnectionString()
                .ListTRFund = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .ValueDate = ConvertDate(lblValueDate.Text.Trim)
                .ReferenceNo = lblReferenceNo.Text.Trim
                .BankAccountFrom = Me.BankAccountID
                .BranchIDTo = Me.BranchIDTo
                .BankAccountID = Me.BankAccountCabang
                '.Amount = CInt(lblTotalAmount.Text.Trim)
                .Amount = CDbl(lblTotalAmount.Text.Trim)
                .Notes = lblNotes.Text.Trim
                .LoginId = Me.Loginid
                .BankType = Me.BankType
                .CompanyID = Me.SesCompanyID
                .RequestNo = lblRequestNo.Text.Trim

                If oBGNODate.InDueDate <> "" Then
                    .DueDate = ConvertDate2(oBGNODate.InDueDate)
                    .BGNo = oBGNODate.BGNumber
                Else
                    DueDate = ConvertDate2("01/01/1900")
                End If

                .JenisTransfer = rboJenisTransfer.SelectedValue
                .PaymentTypeID = cboCaraBayar.SelectedValue

                If cboCaraBayar.SelectedValue = "BA" Then
                    .JenisTransfer = ""
                End If
            End With

            Try
                'Proses dipindahkan ke Otorisasi Transfer For Reimburse Petty Cash : 24 Jan 2016 - Dede
                'If cboCaraBayar.SelectedValue = "GT" Then
                '    oCustomClass = oController.EbankPCREIAdd(oCustomClass)
                'Else
                oController.SaveReimbursePC(oCustomClass)
                ' End If

                If oCustomClass.str = "" Then
                    InitialDefaultPanel()
                    FillCbo()
                    ShowMessage(lblMessage, "Data saved!", False)
                Else
                    ShowMessage(lblMessage, oCustomClass.str, True)
                End If
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim intloopgrid As Integer
        Dim ChkSelect As CheckBox
        If Me.SelectAll = 0 Then
            For intloopgrid = 0 To dtgPettyDtl.Items.Count - 1
                ChkSelect = CType(dtgPettyDtl.Items(intloopgrid).FindControl("chkPetty"), CheckBox)
                If ChkSelect.Enabled = True Then
                    ChkSelect.Checked = True
                End If
            Next
            Me.SelectAll = 1
        Else
            For intloopgrid = 0 To dtgPettyDtl.Items.Count - 1
                ChkSelect = CType(dtgPettyDtl.Items(intloopgrid).FindControl("chkPetty"), CheckBox)
                If ChkSelect.Enabled = True Then
                    ChkSelect.Checked = False
                End If
            Next
            Me.SelectAll = 0
        End If
    End Sub    
    Private Sub ddtgPettyDtl_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPettyDtl.ItemDataBound
        Dim txtAmountdetail As ucNumberFormat
        If e.Item.ItemIndex >= 0 Then
            txtAmountdetail = CType(e.Item.FindControl("txtAmountdetail"), ucNumberFormat)
            With txtAmountdetail
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = False
                .TextCssClass = "numberAlign regular_text"
                .Text = e.Item.Cells(6).Text.Trim
            End With

            If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                CalcTotal(txtAmountdetail.Text.Trim)            
            ElseIf (e.Item.ItemType = ListItemType.Footer) Then
                txtAmountdetail.Text = FormatNumber(runningTotal, 2)
            End If
            If e.Item.ItemType = ListItemType.Header Then
                ChkBoxAll = CType(e.Item.FindControl("chkAll"), CheckBox)
            End If

            txtAmountdetail.Text = FormatNumber(txtAmountdetail.Text, 2)
        End If
    End Sub
    Sub CalcTotal(ByVal _price As String)
        runningTotal += Double.Parse(_price)
    End Sub
    Private Sub CboCaraBayar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaraBayar.SelectedIndexChanged
        If cboCaraBayar.SelectedItem.Value = "GT" Then
            PnlJenisTransfer.Visible = True
            PnlBGNO.Visible = False
        ElseIf cboCaraBayar.SelectedItem.Value = "BA" Then
            PnlJenisTransfer.Visible = False
            PnlBGNO.Visible = True
        End If
    End Sub
End Class