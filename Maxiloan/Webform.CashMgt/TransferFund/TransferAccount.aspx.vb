﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

#End Region

Public Class TransferAccount
    Inherits Maxiloan.Webform.WebBased
    ' Protected WithEvents oTobankAccount As UcBankAccountID
    ' Protected WithEvents oFrombankAccount As UcBankAccountID
    Dim strbanktype As String
    Dim InFromBankAccount As String
    Protected WithEvents oBGNODate As ucBGNoDate
    Protected WithEvents txtamount As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.TransferAccount
    Private oController As New TransferAccountController
    Private oControllercbo As New TransferForReimbursePCController

#End Region

#Region "Property"
    Private Property FromBankAccount() As String
        Get
            Return (CType(ViewState("FromBankAccount"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FromBankAccount") = Value
        End Set
    End Property
    Private Property DataBank() As DataTable
        Get
            Return CType(ViewState("DataBank"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataBank") = Value
        End Set
    End Property

    Property TransferNo() As String
        Get
            Return CType(ViewState("TransferNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TransferNo") = Value
        End Set
    End Property

    Property BankAccountIdTo() As String
        Get
            Return CType(ViewState("BankAccountIdTo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountIdTo") = Value
        End Set
    End Property

    Property BGNo() As String
        Get
            Return CType(ViewState("BGNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BGNo") = Value
        End Set
    End Property

    Property BGDate() As DateTime
        Get
            Return CType(ViewState("BGDate"), DateTime)
        End Get
        Set(ByVal Value As DateTime)
            ViewState("BGDate") = Value
        End Set
    End Property
#End Region



#Region "cboBankAccount"
    Private Sub FillCboAccount()
        Dim odatatable As New DataTable
        Dim oCustomClass As New Parameter.TransferForReimbursePC
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        oCustomClass = oControllercbo.ListBankAccount(oCustomClass)
        'oCustomClass = oControllercbo.ListBankAccount2(oCustomClass)
        odatatable = oCustomClass.ListTRFund
        Me.DataBank = odatatable
        With cboBankAccount
            .DataSource = odatatable
            .DataTextField = "BankAccountName"
            .DataValueField = "BankAccountID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub FillCboAccount2()
        Dim odatatable As New DataTable
        Dim oCustomClass As New Parameter.TransferForReimbursePC
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        oCustomClass = oControllercbo.ListBankAccount(oCustomClass)
        odatatable = oCustomClass.ListTRFund
        Me.DataBank = odatatable
        With cboBankAccount2
            .DataSource = odatatable
            .DataTextField = "BankAccountName"
            .DataValueField = "BankAccountID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "TRFACC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                FillCboAccount()
                TglValuta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                Me.TransferNo = Request("transferNo")
                If Request("transferNo") <> "" Then
                    ViewData(" where tft.TransferNo= '" & Request("transferNo") & "'", "")
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
                With txtamount
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
              
            End If
        End If

    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonNext.Click
        lblMessage.Visible = False
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            If TglValuta.Text = "" Then
                ShowMessage(lblMessage, "Harap isi Tanggal Valuta", True)
                Exit Sub
            End If
            If CDbl(txtamount.Text) <= 0 Then
                ShowMessage(lblMessage, "Jumlah harus > 0", True)
                Exit Sub
            End If
            Dim Saldo As Decimal = GetSaldo()
            If CDec(txtamount.Text) > Saldo Then
                ShowMessage(lblMessage, "Saldo anda sebesar " & FormatNumber(Saldo, 0) & " tidak mencukupi untuk melanjutkan transaksi.", True)
                Exit Sub
            End If

            If cboCaraBayar.SelectedItem.Value = "GT" Then
                PnlBGNO.Visible = False
            ElseIf cboCaraBayar.SelectedItem.Value = "BA" Then
                PnlBGNO.Visible = True
            End If

            pnlBG.Visible = True
            pnlList.Visible = False
            lblAmount.Text = txtamount.Text
            lblValueDate.Text = TglValuta.Text
            lblReferenceNo.Text = txtrefno.Text
            lblFromBankAccount.Text = cboBankAccount.SelectedItem.Text
            InFromBankAccount = cboBankAccount.SelectedValue.Trim
            Me.FromBankAccount = InFromBankAccount
            lbldesc.Text = txtdesc.Text

            FillCboAccount2()
            cboBankAccount2.SelectedValue = Me.BankAccountIdTo


            oBGNODate.BindBGNODate(" bankaccountid = '" & cboBankAccount.SelectedValue.Trim & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
            oCustomClass.bankaccountid = cboBankAccount.SelectedValue.Trim
            oCustomClass = oController.GetBankType(oCustomClass)
            oBGNODate.BGNumber = Me.BGNo
            If (oBGNODate.BGNumber = "None") Then
                oBGNODate.DueDateValidatorFalse()
            Else
                oBGNODate.DueDateValidatorTrue()

            End If

            With oCustomClass
                strbanktype = .bankAccountType
                If .bankAccountType = "B" Then
                    oBGNODate.Visible = True
                Else
                    oBGNODate.Visible = False
                End If
            End With
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("TransferAccount.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim DueDate As Date = Nothing
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)
            If ConvertDate2(TglValuta.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Valuta Tidak Boleh Melebihi Tanggal Sistem", True)
                Exit Sub
            End If
            If Me.FromBankAccount.Trim <> cboBankAccount2.SelectedValue.Trim Then
                With oCustomClass
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .bankAccountTo = cboBankAccount2.SelectedValue.Trim
                    .bankAccountFrom = cboBankAccount.SelectedValue.Trim
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .Amount = CDbl(txtamount.Text)
                    .valueDate = ConvertDate2(TglValuta.Text)
                    .referenceNo = lblReferenceNo.Text
                    .Notes = lbldesc.Text
                    .bgNo = oBGNODate.BGNumber
                    .TransferNO = hdTransperNo.Value
                    If oBGNODate.InDueDate <> "" Then
                        DueDate = ConvertDate2(oBGNODate.InDueDate.Trim)
                    Else
                        DueDate = ConvertDate2("01/01/1900")
                    End If

                    .bgDate = DueDate
                    .NoBonMerah = txtRefNo2.Text.Trim
                    .JenisTransfer = rboJenisTransfer.SelectedValue
                    .PaymentTypeID = cboCaraBayar.SelectedValue

                    If cboCaraBayar.SelectedValue = "BA" Then
                        .JenisTransfer = ""
                    End If

                    .strConnection = GetConnectionString()
                    .CompanyID = Me.SesCompanyID
                End With

                Try
                    Select Case cboCaraBayar.SelectedValue
                        Case "BA", "GT"
                            oController.SaveTransferAccount(oCustomClass)
                            'Case "GT"

                            ' oCustomClass = oController.EBankTRACCOUNTAdd(oCustomClass)
                        Case Else
                            ShowMessage(lblMessage, "Cara bayar salah!", True)
                    End Select

                    If oCustomClass.strError = "" Then
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    Else
                        ShowMessage(lblMessage, oCustomClass.strError, True)
                    End If
                    ClearForm()
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            Else
                ShowMessage(lblMessage, "No Rekening Tidak boleh Sama", True)
            End If
        End If
    End Sub
    Private Sub CboCaraBayar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaraBayar.SelectedIndexChanged
        If cboCaraBayar.SelectedItem.Value = "GT" Then
            PnlJenisTransfer.Visible = True
            PnlBGNO.Visible = False
        ElseIf cboCaraBayar.SelectedItem.Value = "BA" Then
            PnlJenisTransfer.Visible = False
            PnlBGNO.Visible = True
        End If
    End Sub
    Private Sub cboBankAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankAccount.SelectedIndexChanged
        Dim BankAccountType As String = GetBankAccountType()
        If BankAccountType = "C" Or cboBankAccount.SelectedValue = "" Then
            PnlBankAccount.Visible = False
        Else
            PnlBankAccount.Visible = True

        End If

    End Sub
    Sub ClearForm()
        pnlValuta.Visible = True
        pnlList.Visible = True
        pnlBG.Visible = False
        cboCaraBayar.SelectedIndex = 0
        rboJenisTransfer.SelectedIndex = 0
        txtamount.Text = ""
        txtdesc.Text = ""
        txtrefno.Text = ""
        cboBankAccount.SelectedIndex = 0
    End Sub
    Public Function GetSaldo() As Decimal
        Dim Saldo As Decimal = 0

        Dim oCustom As New Parameter.RefundInsentif
        Dim m_Insentif As New RefundInsentifController

        With oCustom
            .strConnection = GetConnectionString()
            '.WhereCond = " BranchID = '" & sesBranchId.Replace("'", "").Trim & "' and BankAccountID = '" & cboBankAccount.SelectedValue.Trim & "'"
            .WhereCond = " BankAccountID = '" & cboBankAccount.SelectedValue.Trim & "'"
            .SPName = "spGetSaldo"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            Saldo = CDec(IIf(IsDBNull(oCustom.ListDataTable.Rows(0).Item("EndingBalance")), 0, oCustom.ListDataTable.Rows(0).Item("EndingBalance")))
        End If

        Return Saldo
    End Function

    Public Function GetBankAccountType() As String
        Dim BankAccountType As String = ""

        Dim oCustom As New Parameter.RefundInsentif
        Dim m_Insentif As New RefundInsentifController

        With oCustom
            .strConnection = GetConnectionString()
            '.WhereCond = " BranchID = '" & sesBranchId.Replace("'", "").Trim & "' and BankAccountID = '" & cboBankAccount.SelectedValue.Trim & "'"
            .WhereCond = " BankAccountID = '" & cboBankAccount.SelectedValue.Trim & "'"
            .SPName = "spGetSaldo"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            BankAccountType = oCustom.ListDataTable.Rows(0).Item("BankAccountType").ToString
        End If

        Return BankAccountType.Trim
    End Function

    Sub ViewData(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spTransferFundTransaction"
        objCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 2000).Value = cmdWhere
        objCommand.Parameters.Add("@SortBy", SqlDbType.VarChar, 100).Value = SortBy
        objread = objCommand.ExecuteReader()
        If objread.Read Then
            'CStr(objReader.Item("CustomerName")).Trim()


            hdTransperNo.Value = Me.TransferNo
            cboBankAccount.SelectedValue = CStr(objread.Item("BankAccountIdFrom"))
            Me.BankAccountIdTo = CStr(objread.Item("BankAccountIdTo"))
            cboCaraBayar.SelectedValue = CStr(objread.Item("PaymentTypeID")).Trim()
            If (CStr(objread.Item("PaymentTypeID")).Trim() = "C") Then
                PnlBankAccount.Visible = False
            Else
                PnlBankAccount.Visible = True
                If CStr(objread.Item("PaymentTypeID")).Trim() = "GT" Then
                   
                    PnlJenisTransfer.Visible = True
                    PnlBGNO.Visible = False
                ElseIf CStr(objread.Item("PaymentTypeID")).Trim() = "BA" Then
                    PnlJenisTransfer.Visible = False
                    PnlBGNO.Visible = True
                End If
            End If
            'rboJenisTransfer.SelectedValue =iif CStr(objread.Item("JenisTransfer")).Trim()
            txtrefno.Text = CStr(objread.Item("ReferenceNo")).Trim()
            txtamount.Text = FormatNumber(CStr(objread.Item("Amount")).Trim(), 0)
            txtdesc.Text = CStr(objread.Item("Description")).Trim()
            lbldesc.Text = CStr(objread.Item("Description")).Trim()
            'TglValuta.Text = CStr(objread.Item("ValueDate").ToString("dd/MM/yyyy"))
            'lblValueDate.Text = CStr(objread.Item("ValueDate").ToString("dd/MM/yyyy")).Trim()
            lblReferenceNo.Text = CStr(objread.Item("ReferenceNo")).Trim()
            lblFromBankAccount.Text = CStr(objread.Item("BankAccountIdFrom")).Trim()
            lblAmount.Text = CStr(objread.Item("Amount"))
            txtRefNo2.Text = CStr(objread.Item("NoBonMerah")).Trim()

            Me.BGNo = CStr(objread.Item("BGno")).Trim()
            'Me.BGDate = CStr(objread.Item("BGDueDate").ToString())

        End If

        Try

            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
        pnlList.Visible = True
        'pnlDatagrid.Visible = True
    End Sub
End Class