﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class OtorisasiTransferForPaymentRequest
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private runningTotal As Double = 0

    Private oCustomClass As New Parameter.TransferForPaymentRequest
    Private oController As New TransferForPaymentRequestController
    Private m_TotalAmount As Double

#End Region

#Region "Property"
    Private Property SortPettydtg() As String
        Get
            Return CType(ViewState("SortPettydtg"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortPettydtg") = Value
        End Set
    End Property

    Private Property DataBank() As DataTable
        Get
            Return CType(ViewState("DataBank"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataBank") = Value
        End Set
    End Property

    Private Property DataTransfer() As DataTable
        Get
            Return CType(ViewState("DataTransfer"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataTransfer") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property

    Private Property BankAccountFrom() As String
        Get
            Return CType(ViewState("BankAccountFrom"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountFrom") = Value
        End Set
    End Property

    Private Property BranchIDTo() As String
        Get
            Return CType(ViewState("BranchIDTo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDTo") = Value
        End Set
    End Property

    Private Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property

    Private Property EndingBalance() As Double
        Get
            Return CInt(ViewState("EndingBalance"))
        End Get
        Set(ByVal Value As Double)
            ViewState("EndingBalance") = Value
        End Set
    End Property

    Private Property BankAccountBranch() As String
        Get
            Return CType(ViewState("BankAccountBranch"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountBranch") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "OTORTRFPAYREQ"
        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not IsPostBack Then
                If Me.IsHoBranch Then
                    InitialDefaultPanel()
                    FillCbo()
                Else
                    ShowMessage(lblMessage, "Harus Login User Kantor Pusat", True)
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    pnlTransfer.Visible = False
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub FillCbo()
        Dim Controller As New TransferForReimbursePCController
        Dim CustomClass As New Parameter.TransferForReimbursePC
        Dim oDataTable As New DataTable
        With CustomClass
            .strConnection = GetConnectionString()
        End With
        CustomClass = Controller.ListBranchHO(CustomClass)
        oDataTable = CustomClass.ListTRFund
        With cboBranch
            .DataSource = oDataTable
            .DataTextField = "BranchFullName"
            .DataValueField = "BranchID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlTransfer.Visible = False
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        txtSearchBy.Text = ""
        cboBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtRequestDate.Text = ""
        InitialDefaultPanel()
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = " PaymentRequest.Status='OTO' and PaymentRequest.BranchID='" & cboBranch.SelectedItem.Value.Trim & "'"
        If txtRequestDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and RequestDate='" & ConvertDate(txtRequestDate.Text) & "'"
        End If
        If cboSearchBy.SelectedItem.Value.Trim <> "" And txtSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " = '" & txtSearchBy.Text.Trim & "'"
        End If
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        Me.BranchIDTo = cboBranch.SelectedItem.Value.Trim
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListRequestOtorisasi(oCustomClass)

        DtUserList = oCustomClass.ListRequest
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRequest.DataSource = DvUserList

        Try
            dtgRequest.DataBind()
        Catch
            dtgRequest.CurrentPageIndex = 0
            dtgRequest.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgRequest_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRequest.ItemCommand
        If e.CommandName = "Approve" Then
            If Me.IsHoBranch = True Then
                If CheckFeature(Me.Loginid, Me.FormID, "APR", "MAXILOAN") Then
                    pnlTransfer.Visible = True
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    ButtonApprove.Visible = True
                    ButtonDecline.Visible = True
                    Dim lblNoReq As Label
                    lblNoReq = CType(dtgRequest.Items(e.Item.ItemIndex).Cells(2).FindControl("LblRequestNo"), Label)
                    FillRequest(lblNoReq.Text.Trim, "", dtgRequest.Items(e.Item.ItemIndex).Cells(1).Text.Trim)
                    lblBranch.Text = cboBranch.SelectedItem.Text.Trim
                    lblTransferNo.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(1).Text.Trim
                    lblRequestNo.Text = lblNoReq.Text.Trim
                    lblBankAccount.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(7).Text.Trim
                    Me.BankAccountFrom = dtgRequest.Items(e.Item.ItemIndex).Cells(7).Text.Trim
                    lblDescription.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(4).Text.Trim
                    lblRequestDate.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(6).Text.Trim
                    lblAmount.Text = FormatNumber(dtgRequest.Items(e.Item.ItemIndex).Cells(24).Text.Trim, 2)
                    lblValueDate.Text = Format(CDate(dtgRequest.Items(e.Item.ItemIndex).Cells(9).Text.Trim), "dd/MM/yyyy")
                    lblNotes.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(16).Text.Trim
                    lblReferenceNo.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(12).Text.Trim
                    lblBankAccountDtl.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(15).Text.Trim
                    lblCaraBayar.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(18).Text.Trim
                    'lblJenisTransfer.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(19).Text.Trim
                    lblTotalAmount.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(5).Text.Trim
                    lblGiroNo.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(22).Text.Trim
                    lblGiroJatuhTempo.Text = dtgRequest.Items(e.Item.ItemIndex).Cells(23).Text.Trim
                    Me.BankAccountID = dtgRequest.Items(e.Item.ItemIndex).Cells(14).Text.Trim
                    Me.BankAccountBranch = dtgRequest.Items(e.Item.ItemIndex).Cells(13).Text.Trim

                End If
            Else
                ShowMessage(lblMessage, "Harap Login di kantor Pusat", True)
                pnlsearch.Visible = True
                pnlDtGrid.Visible = True
            End If

        End If
    End Sub

    Private Sub FillRequest(ByVal str As String, ByVal sort As String, ByVal trf As String)
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = str
            .BranchId = Me.BranchIDTo
            .TransferNo = trf
            .SortBy = sort.Trim
        End With
        oCustomClass = oController.ListPaymentDetailOtorisasi(oCustomClass)
        odatatable = oCustomClass.ListRequest
        Me.DataTransfer = odatatable
        dtgPettyDtl.DataSource = odatatable
        dtgPettyDtl.DataBind()
    End Sub
    Public Sub SortPetty(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortPettydtg, "DESC") > 0 Then
            Me.SortPettydtg = e.SortExpression
        Else
            Me.SortPettydtg = e.SortExpression + " DESC"
        End If
        FillRequest(lblRequestNo.Text.Trim, Me.SortPettydtg, lblTransferNo.Text.Trim)
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        FillCbo()
    End Sub

    Private Sub FillCboBilyet(ByVal str As String)
        Dim odatatable As New DataTable
        Dim customclass As New Parameter.TransferForReimbursePC
        Dim controller As New TransferForReimbursePCController
        With customclass
            .strConnection = GetConnectionString()
            .BankAccountID = str
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        customclass = controller.ListBilyetGiro(customclass)
        odatatable = customclass.ListTRFund
    End Sub

    Private Sub ButtonApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonApprove.Click
        Dim customclass As New Parameter.TransferForReimbursePC
        Dim controller As New TransferForReimbursePCController
        If CheckFeature(Me.Loginid, Me.FormID, "APR", "MAXILOAN") Then

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Dim oDataTable As New DataTable
            With customclass
                .strConnection = GetConnectionString()
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .Amount = CInt(lblTotalAmount.Text.Trim)
                .BankAccountID = Me.BankAccountID
            End With
            customclass = controller.CheckEndingBalance(customclass)
            If customclass.str <> "" Then
                ShowMessage(lblMessage, "Saldo tidak Cukup", True)
                Exit Sub
            End If
            oDataTable = Me.DataTransfer
            With oCustomClass
                .strConnection = GetConnectionString()
                .ListRequest = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .TransferNo = lblTransferNo.Text
                .ReferenceNo = lblReferenceNo.Text
                .Otorisasi = "A"
                .CompanyID = Me.SesCompanyID
                .BranchIDTo = Me.BranchIDTo
                .RequestNo = lblRequestNo.Text
            End With
            Try
                oController.SavePaymentRequestOtorisasi(oCustomClass)
                InitialDefaultPanel()
                FillCbo()
                ShowMessage(lblMessage, "Data saved!", False)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If

    End Sub

    Sub CalcTotal(ByVal _price As String)
        runningTotal += Double.Parse(_price)
    End Sub

    Private Sub dtgPettyDtl_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPettyDtl.ItemDataBound
        Dim lblTemp As Label
        If e.Item.ItemIndex >= 0 Then
            lblTemp = CType(e.Item.FindControl("lblAmountReq"), Label)

            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblAmountdtg"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If

    End Sub


    Private Sub ButtonDecline_Click(sender As Object, e As System.EventArgs) Handles ButtonDecline.Click
        Dim customclass As New Parameter.TransferForReimbursePC
        Dim controller As New TransferForReimbursePCController
        If CheckFeature(Me.Loginid, Me.FormID, "APR", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            oDataTable = Me.DataTransfer
            With oCustomClass
                .strConnection = GetConnectionString()
                .ListRequest = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .TransferNo = lblTransferNo.Text
                .ReferenceNo = lblReferenceNo.Text
                .Otorisasi = "J"
                .CompanyID = Me.SesCompanyID
            End With
            Try
                oController.SavePaymentRequestOtorisasi(oCustomClass)
                InitialDefaultPanel()
                FillCbo()
                ShowMessage(lblMessage, "Data saved!", False)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
End Class