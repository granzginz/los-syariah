﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferForPaymentRequest.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.TransferForPaymentRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transfer For Payment Request</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.ServerVariables("PATH_INFO") %>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.ServerVariables("SERVER_NAME") %>/';

        function OpenWinRequest(BranchId, RequestNo) {
            window.open(ServerName + App + '/Webform.LoanMnt/PaymentRequest/PaymentRequestInquiryView.aspx?BranchId=' + BranchId + '&RequestNo=' + RequestNo + '&style=AccMnt', null, 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }

        function validateEbankDescStep0() { 
            if (validateEbankDescStep1(document.getElementById('<%# txtNotes.ClientID %>'))) {
                // Validator controls are not shown if button OnClientClickProperty is set to javascript code!
                Page_ClientValidate();
                return Page_IsValid;
            } else {
                return false;
            };

        }			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgressSave" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TRANSFER DANA UNTUK PERMINTAAN PEMBAYARAN
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label">
                            Cabang</label>
                        <asp:DropDownList ID="cboBranch" runat="server">
                        </asp:DropDownList>
                        
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Permintaan</label>
                        <asp:TextBox ID="txtRequestDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtRequestDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtRequestDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="PaymentRequest.RequestNo">No Request</asp:ListItem>
                            <asp:ListItem Value="PaymentRequest.Description">Keterangan</asp:ListItem>
                            <asp:ListItem Value="PaymentRequest.ReferenceNo">No Memo</asp:ListItem>
                            <asp:ListItem Value="PaymentRequest.TotalAmount">Jumlah</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgRequest" runat="server" Width="100%" OnSortCommand="SortGrid"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" DataKeyField="RequestNo"
                                AutoGenerateColumns="False" AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>
                                            <asp:LinkButton CommandName="Transfer" Text='TRANSFER' runat="server" ID="Linkbutton1"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BranchFullName" SortExpression="BranchFullName" HeaderText="CABANG">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="REQUESTNO" HeaderText="NO REQUEST">
                                        <ItemTemplate>
                                            <a href="javascript:OpenWinRequest('<%#Container.DataItem("BranchID")%>','<%#Container.DataItem("RequestNo")%>');">
                                                <asp:Label ID="LblRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'> </asp:Label></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNo" visible="True" HeaderText="NO MEMO">
                                        <ItemTemplate>
                                                <asp:Label ID="lblNoMemo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="REQUESTBY" SortExpression="REQUESTBY" HeaderText="PERMINTAAN DARI">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="DESCRIPTION" SortExpression="DESCRIPTION" HeaderText="KETERANGAN">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMOUNT" SortExpression="AMOUNT" HeaderText="JUMLAH" DataFormatString="{0:###,###,###.00}">
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="REQUESTDATE" SortExpression="REQUESTDATE" HeaderText="TGL MINTA"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="BAnkaccountname"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="BankAccountID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="BranchId"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="Num"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="AccountNo" visible="false">
                                        <ItemTemplate>
                                                <asp:Label ID="lblAccountNo" runat="server" Text='<%#Container.DataItem("AccountNo")%>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BankName" visible="false">
                                        <ItemTemplate>
                                                <asp:Label ID="lblBankName" runat="server" Text='<%#Container.DataItem("BankName")%>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlTransfer" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Permintaan Cabang</label>
                        <asp:Label ID="lblBranch" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Request</label>
                        <asp:Label ID="lblRequestNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Permintaan</label>
                        <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Rekening a.n</label>
                        <asp:DropDownList ID="cboBankaccountBranch" runat="server" Visible="false">
                        </asp:DropDownList>
                        <asp:Label ID="lblBankAccount" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">
                        
                        <label>
                            Jumlah Permintaan</label>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Rekening No</label>
                        <asp:Label ID="lblAccountNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Memo</label>
                        <asp:Label ID="lblNoMemo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Bank</label>
                        <asp:Label ID="lblbankname" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Keterangan</label>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <asp:panel id="panelHistory" runat="server">
                <div class="form_title">
                        <div class="form_single">
                        <h4> HISTORY REJECT TRANSAKSI </h4>
                    </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAgreeHistory" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" AutoGenerateColumns="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSeqNo" runat="server" Text='<%#Container.DataItem("SeqNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RejectNote")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TANGGAL">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTanggal"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tanggal") %>'  DataFormatString="{0:dd/MM/yyyy}"> </asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="User">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("RequestBy")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                </div>
                </asp:panel>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSAKSI</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPettyDtl" runat="server" Width="100%" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="False"
                                OnSortCommand="SortPetty" ShowFooter="true" FooterStyle-CssClass="item_grid">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderStyle-CssClass="th_right" HeaderText="NO.">
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblseqnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SequenceNo") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="paymentallocationid"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="KETERANGAN/COA">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DESCRIPTION") %>'> </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DESCRIPTION") %>'> </asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH DIMINTA" HeaderStyle-CssClass="th_right">
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmountReq" runat="server" Text='<%#FormatNumber(Container.DataItem("amount"), 2)%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            TOTAL
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="th_right" HeaderText="JUMLAH DITRANSFER">
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <uc1:ucnumberformat id="txtAmountdetail" runat="server" />
                                            <asp:Label ID="lblAmountTransfer" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAmountdtg" runat="server" Visible="True" Text='0.0'></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNotesdtg" runat="server" ></asp:TextBox>
                                            <asp:Label ID="lblNotesdtg" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            RINCIAN TRANSFER DANA UNTUK PERMINTAAN PEMBAYARAN</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Total Transfer</label>
                        <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Tanggal Valuta</label>
                        <asp:TextBox ID="txtValueDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtValueDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:Label ID="lblValueDate" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtValueDate"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            Dari Rekening</label>
                        <asp:DropDownList ID="cboBankAccount" runat="server">
                        </asp:DropDownList>
                        <asp:Label ID="lblBankAccountDtl" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap Pilih Rekening Bank"
                            CssClass="validator_general" ControlToValidate="cboBankAccount" Display="Dynamic"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cara Bayar</label>
                        <asp:DropDownList ID="cboCaraBayar" AutoPostBack="true" runat="server">
                            <%--<asp:ListItem Value="GT"  Text="E-Banking"></asp:ListItem>--%>
                            <asp:ListItem Value="BA"  Selected="True" Text="Bank"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <asp:UpdatePanel ID="PnlJenisTransfer" runat="server">
                            <ContentTemplate>
                                <div>
                                    <label class="label_general">
                                        Jenis Transfer</label>
                                    <asp:RadioButtonList ID="rboJenisTransfer" runat="server" class="opt_single" RepeatDirection="Horizontal"
                                        AutoPostBack="true">
                                        <asp:ListItem Selected="True" Value="SKN">SKN</asp:ListItem>
                                        <asp:ListItem Value="RTGS">RTGS</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboCaraBayar" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            No. Bukti Bank Keluar</label>
                        <asp:TextBox ID="txtReferenceNo" runat="server"  MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtReferenceNo"
                            CssClass="validator_general" ErrorMessage="Harap isi No Bukti Kas Keluar" Display="Dynamic"
                            Enabled="False"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                    </div>
                </div>
                <asp:UpdatePanel ID="PnlBGNO" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form_box_title">
                            <div class="form_single">
                                <h4>
                                    BILYET GIRO</h4>
                            </div>
                        </div>
                        <div class="form_box_uc">
                            <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cboCaraBayar" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="form_box">
                    <div class="form_left">
                    <label>
                        Pilih Status
                    </label>
                    <asp:DropDownList ID="cboStatus" AutoPostBack="true" runat="server">
                        <asp:ListItem Text="Transfer" Value="1" Selected="true" />
                        <asp:ListItem Text="Reject" Value="RJ"/>
                        </asp:DropDownList>
                </div>
                </div>
                <asp:panel id="Panel1" runat="server"  Visible="false">
                    <div class="form_box">
                    <div class="form_left">
                        <label>
                            Keterangan
                        </label>
                        <asp:TextBox ID="txtNoteReject" runat="server" Width="365px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                        <div class="form_button">
                            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue" />
                            <asp:Button ID="BtnCancel" runat="server"  Text="Cancel" CssClass="small button gray" />
                        </div>
                    </div>
                </asp:panel>
                <asp:panel id="Panel4" runat="server" >
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonNext" runat="server" Text="Next" CssClass="small button blue"
                        OnClientClick="javascript:return validateEbankDescStep0();"></asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
                </asp:panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
