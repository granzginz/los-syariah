﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtorisasiTransferAccount.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.OtorisasiTransferAccount" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Otorisasi Transfer</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
          
                <asp:Panel ID="Panel1" runat="server">
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h4>
                                OTORISASI
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Rekening Bank dari
                            </label>
                            <uc1:ucbankaccountid id="oBankAccount" runat="server"></uc1:ucbankaccountid>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Tanggal Transaksi
                            </label>
                          
                            <asp:TextBox runat="server" ID="dateFrom" CssClass="small_text"></asp:TextBox>
                            <aspajax:CalendarExtender ID="caldateFrom" runat="server" TargetControlID="dateFrom"
                                Format="dd/MM/yyyy" />S/D
                            <asp:TextBox runat="server" ID="dateTo" CssClass="small_text" ></asp:TextBox>
                            <aspajax:CalendarExtender ID="caldateTo" runat="server" TargetControlID="dateTo"
                                Format="dd/MM/yyyy" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Jumlah Terima
                            </label>
                            <uc1:ucnumberformat id="txtAmountFrom0" runat="server" />
                            </uc1:ucNumberFormat>S/D
                            <uc1:ucnumberformat id="txtamountto0" runat="server" />
                            </uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
                        </asp:Button>&nbsp;
                        <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="False"></asp:Button>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlList" runat="server" Visible="false">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                OTORISASI TRANSFER ANTAR REKENING
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <asp:GridView ID="dtgPaging" CssClass="grid_general" Width="100%" DataKeyNames="TransferNo"
                                AutoGenerateColumns="False" GridLines="None" runat="server" HeaderStyle-CssClass="th"
                                RowStyle-CssClass="item_grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="CheckAll">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="SelectAll" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No. Transfer">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransferNo" Text='<%# Eval("TransferNo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Keterangan">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" Text='<%# Eval("Description") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jumlah">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" Text='<%# formatnumber(Eval("Amount"),0) %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TGL Transaksi">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPostingDate" Text='<%# format(Eval("ValueDate"),"dd MMM yyyy") %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rekening Bank Dari">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountNameFrom" Text='<%# Eval("BankAccountNameFrom") %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rekening Bank Ke">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountNameTo" Text='<%# Eval("BankAccountNameTo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                   

                      <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
                        </asp:Button>
            <asp:Button ID="imgEdit" runat="server" Text="Edit" CssClass="small button orange"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="imbReject" runat="server" Text="Reject" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
          
        </div>
                </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
