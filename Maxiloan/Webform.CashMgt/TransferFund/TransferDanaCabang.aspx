﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferDanaCabang.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.TransferDanaCabang" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchHO" Src="../../Webform.UserController/UcBranchHO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>TransferDanaCabang</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
    <script type="text/javascript" charset="utf-8">
        function preventMultipleSubmissions2() {
            $(document).ready(function () {
                $.loadingBlockShow({
                    imgPath: '../../Images/imgloading/default.svg',
                    text: 'Sedang diproses ...',
                    style: {
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        background: 'rgba(0, 0, 0, .8)',
                        left: 0,
                        top: 0,
                        zIndex: 10000
                    }
                });

                setTimeout($.loadingBlockHide, 3000);
            });
        }
    </script>	
<body>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=ButtonSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <asp:Panel runat="server" ID="pnlInput" Visible="true">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            TRANSFER DANA CABANG
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Valuta</label>
                        <asp:TextBox ID="TglValuta" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="TglValuta_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="TglValuta" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="TglValuta"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label runat="server" ID="lblNamaCabang"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Cabang</label>
                        <uc1:ucbankaccountbranch id="oFrombankAccount" runat="server"></uc1:ucbankaccountbranch>
                    </div>
                </div>                
                <div class="form_box">
                    <div class="form_single">
                        <h3>
                            TRANSFER KE KANTOR PUSAT
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening HO</label>
                        <asp:DropDownList ID="cmbBankidTo" runat="server" >
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih rekening Bank"
                            Display="Dynamic" ControlToValidate="cmbBankidTo" InitialValue="0" CssClass ="validator_general"></asp:RequiredFieldValidator>


                    </div>
                </div>                    
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah</label>
                        <uc1:ucnumberformat id="txtamount" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Keterangan</label>
                        <asp:TextBox ID="txtdesc" runat="server" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtdesc"
                            CssClass="validator_general" ErrorMessage="Harap isi dengan Keterangan" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
