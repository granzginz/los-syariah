﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewTransferFund.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.ViewTransferFund" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewTransferFund</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script type="text/javascript" language="JavaScript">
        function fClose() {
            window.close();
            return false;
        }			
	</script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
	        <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW - TRANSFER DANA
                </h3>
            </div>
        </div>  
        <div class="form_box">	        
                <div class="form_left">
                    <label>Cabang</label>
                    <asp:Label ID="lblBranch" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No. Transfer</label>
                    <asp:Label ID="LblTransferNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Valuta</label>
                    <asp:Label ID="lblvalueDate" runat="server"></asp:Label>		
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Jenis Transfer</label>
                    <asp:Label ID="LblTransferType" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Tanggal Posting</label>	
                    <asp:Label ID="LblPostingDate" runat="server" align="right"></asp:Label>		
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Bukti Kas</label>
                    <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Jumlah Transfer</label>	
                    <asp:Label ID="LblAmountTransfer" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    TRANSFER DARI
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Cabang</label>
                    <asp:Label ID="LblBranchTransferFrom" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Rekening Bank</label>	
                    <asp:Label ID="lblBankAccountTransferForm" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Voucher</label>
                    <asp:Label ID="lblVoucherNoTransferFrom" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Bilyet Giro</label>
                    <asp:Label ID="lblBGNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Rencana Bayar BG</label>
                    <asp:Label ID="lblBGDueDate" runat="server" align="right"></asp:Label>		
		        </div>	        
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    TRANSFER KE
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Cabang</label>
                    <asp:Label ID="lblBranchTransferTo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Rekening Bank</label>
                    <asp:Label ID="lblBankAccountTransferTo" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Voucher</label>
                    <asp:Label ID="lblVoucherNoTransferTo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	       
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Status</label>
                    <asp:Label ID="Lblstatus" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Tanggal Status</label>	
                    <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Transfer Oleh</label>
                    <asp:Label ID="lblTransferBy" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Rekonsiliasi Oleh</label>
                    <asp:Label ID="lblReconcileBy" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_button">            
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
            </asp:Button>     
        </div>   
    </form>
</body>
</html>
