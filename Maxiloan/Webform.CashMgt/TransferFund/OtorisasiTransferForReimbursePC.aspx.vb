﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Exceptions
#End Region

Public Class OtorisasiTransferForReimbursePC
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private runningTotal As Double = 0

    Private oCustomClass As New Parameter.TransferForReimbursePC
    Private oController As New TransferForReimbursePCController

#End Region

#Region "Property"
    Private Property SortPettydtg() As String
        Get
            Return CType(ViewState("SortPettydtg"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortPettydtg") = Value
        End Set
    End Property

    Private Property DataBank() As DataTable
        Get
            Return CType(ViewState("DataBank"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataBank") = Value
        End Set
    End Property

    Private Property DataTransfer() As DataTable
        Get
            Return CType(ViewState("DataTransfer"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataTransfer") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property

    Private Property BankAccountBranch() As String
        Get
            Return CType(ViewState("BankAccountBranch"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountBranch") = Value
        End Set
    End Property

    Private Property BankAccountFrom() As String
        Get
            Return CType(ViewState("BankAccountFrom"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountFrom") = Value
        End Set
    End Property

    Private Property BranchIDTo() As String
        Get
            Return CType(ViewState("BranchIDTo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDTo") = Value
        End Set
    End Property

    Private Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property
    Private Property SelectAll() As Integer
        Get
            Return CInt(ViewState("SelectAll"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SelectAll") = Value
        End Set
    End Property

    Private Property EndingBalance() As Double
        Get
            Return CInt(ViewState("EndingBalance"))
        End Get
        Set(ByVal Value As Double)
            ViewState("EndingBalance") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "OTORTRFPC"
        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not IsPostBack Then
                If Me.IsHoBranch Then
                    InitialDefaultPanel()
                    FillCbo()
                Else
                    ShowMessage(lblMessage, "Harus Login User Kantor Pusat", True)
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    pnlTransfer.Visible = False
                    Exit Sub
                End If
            End If
        End If
    End Sub
    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
        End With
        oCustomClass = oController.ListBranchHO(oCustomClass)
        oDataTable = oCustomClass.ListTRFund
        With cboBranch
            .DataSource = oDataTable
            .DataTextField = "BranchFullName"
            .DataValueField = "BranchID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlTransfer.Visible = False
        lblMessage.Visible = False
    End Sub
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        txtSearchBy.Text = ""
        cboBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtRequestDate.Text = ""
        InitialDefaultPanel()
    End Sub
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "PettyCashReimburse.Status='OTO' and PettyCashReimburse.BranchID='" & cboBranch.SelectedItem.Value.Trim & "'"
        If txtRequestDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and RequestDate='" & ConvertDate(txtRequestDate.Text) & "'"
        End If
        If cboSearchBy.SelectedItem.Value.Trim <> "" And txtSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " = '" & txtSearchBy.Text.Trim & "'"
        End If
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        Me.BranchIDTo = cboBranch.SelectedItem.Value.Trim
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        lblMessage.Visible = False

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListPCReimburseOtorisasi(oCustomClass)

        DtUserList = oCustomClass.ListTRFund
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgPetty.DataSource = DvUserList

        Try
            dtgPetty.DataBind()
        Catch
            dtgPetty.CurrentPageIndex = 0
            dtgPetty.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgPetty_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPetty.ItemCommand
        If e.CommandName = "Approve" Then
            If Me.IsHoBranch = True Then
                If CheckFeature(Me.Loginid, Me.FormID, "APR", "MAXILOAN") Then
                    pnlTransfer.Visible = True
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    ButtonApprove.Visible = True
                    ButtonDecline.Visible = True
                    Dim lblNoReq As Label
                    lblNoReq = CType(dtgPetty.Items(e.Item.ItemIndex).Cells(2).FindControl("lblNoReq"), Label)
                    FillPetty(lblNoReq.Text.Trim, "", dtgPetty.Items(e.Item.ItemIndex).Cells(1).Text.Trim)
                    lblBranch.Text = cboBranch.SelectedItem.Text.Trim
                    lblTransferNo.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(1).Text.Trim
                    lblRequestNo.Text = lblNoReq.Text.Trim
                    lblBankAccount.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(7).Text.Trim
                    Me.BankAccountFrom = dtgPetty.Items(e.Item.ItemIndex).Cells(7).Text.Trim
                    lblDescription.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(4).Text.Trim
                    lblRequestDate.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(6).Text.Trim
                    lblAmount.Text = FormatNumber(dtgPetty.Items(e.Item.ItemIndex).Cells(24).Text.Trim, 2)
                    lblValueDate.Text = Format(CDate(dtgPetty.Items(e.Item.ItemIndex).Cells(9).Text.Trim), "dd/MM/yyyy")
                    lblNotes.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(16).Text.Trim
                    lblReferenceNo.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(12).Text.Trim
                    lblBankAccountDtl.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(15).Text.Trim
                    lblCaraBayar.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(18).Text.Trim
                    'lblJenisTransfer.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(19).Text.Trim
                    lblTotalAmount.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(5).Text.Trim
                    lblGiroNo.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(22).Text.Trim
                    lblGiroJatuhTempo.Text = dtgPetty.Items(e.Item.ItemIndex).Cells(23).Text.Trim
                    Me.BankAccountID = dtgPetty.Items(e.Item.ItemIndex).Cells(14).Text.Trim
                    Me.BankAccountBranch = dtgPetty.Items(e.Item.ItemIndex).Cells(13).Text.Trim
                End If
            Else
                ShowMessage(lblMessage, "harap Login di kantor Pusat", True)
                pnlsearch.Visible = True
                pnlDtGrid.Visible = True
            End If

        End If
    End Sub
    Private Sub FillPetty(ByVal str As String, ByVal sort As String, ByVal trf As String)
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .RequestNo = str
            .TransferNo = trf
            .BranchId = Me.BranchIDTo
            .SortBy = sort.Trim
        End With
        oCustomClass = oController.ListPettyCashOtorisasi(oCustomClass)
        odatatable = oCustomClass.ListTRFund
        dtgPettyDtl.DataSource = odatatable
        dtgPettyDtl.DataBind()
    End Sub
    Public Sub SortPetty(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortPettydtg, "DESC") > 0 Then
            Me.SortPettydtg = e.SortExpression
        Else
            Me.SortPettydtg = e.SortExpression + " DESC"
        End If
        FillPetty(lblRequestNo.Text.Trim, Me.SortPettydtg, lblTransferNo.Text.Trim)
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        FillCbo()
    End Sub
    Private Sub FillCboBilyet(ByVal str As String)
        Dim odatatable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .BankAccountID = str
            .BranchId = Replace(Me.sesBranchId, "'", "")
        End With
        oCustomClass = oController.ListBilyetGiro(oCustomClass)
        odatatable = oCustomClass.ListTRFund
    End Sub
    Private Sub ButtonApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonApprove.Click
        Dim DueDate As Date = Nothing

        If CheckFeature(Me.Loginid, Me.FormID, "APR", "MAXILOAN") Then
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Dim oDataTable As New DataTable
            'If Me.BankType = "C" Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BankAccountBranch
                .Amount = CInt(lblTotalAmount.Text.Trim)
                .BankAccountID = Me.BankAccountID
            End With
            oCustomClass = oController.CheckEndingBalance(oCustomClass)
            If oCustomClass.str <> "" Then
                ShowMessage(lblMessage, "Saldo tidak Cukup", True)
                Exit Sub
            End If
            oDataTable = Me.DataTransfer
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .TransferNo = lblTransferNo.Text
                .ReferenceNo = lblReferenceNo.Text
                .Otorisasi = "A"
                .CompanyID = Me.SesCompanyID
            End With

            Try
                oController.SaveReimbursePCOtorisasi(oCustomClass)
                If oCustomClass.str = "" Then
                    InitialDefaultPanel()
                    FillCbo()
                    ShowMessage(lblMessage, "Data saved!", False)
                Else
                    ShowMessage(lblMessage, oCustomClass.str, True)
                End If
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
    
    Private Sub ddtgPettyDtl_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPettyDtl.ItemDataBound
        Dim lblAmountdetail As Label

        If e.Item.ItemIndex >= 0 Then
            lblAmountdetail = CType(e.Item.FindControl("lblAmountdetail"), Label)
            With lblAmountdetail
                .CssClass = "numberAlign regular_text"
                .Text = e.Item.Cells(5).Text.Trim
            End With

            If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                CalcTotal(lblAmountdetail.Text.Trim)
            ElseIf (e.Item.ItemType = ListItemType.Footer) Then
                lblAmountdetail.Text = FormatNumber(runningTotal, 2)
            End If
         
            lblAmountdetail.Text = FormatNumber(lblAmountdetail.Text, 2)
        End If
    End Sub
    Sub CalcTotal(ByVal _price As String)
        runningTotal += Double.Parse(_price)
    End Sub

    Private Sub ButtonDecline_Click(sender As Object, e As System.EventArgs) Handles ButtonDecline.Click
        Dim DueDate As Date = Nothing

        If CheckFeature(Me.Loginid, Me.FormID, "APR", "MAXILOAN") Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .TransferNo = lblTransferNo.Text
                .ReferenceNo = lblReferenceNo.Text
                .Otorisasi = "J"
            End With

            Try
                oController.SaveReimbursePCOtorisasi(oCustomClass)
                If oCustomClass.str = "" Then
                    InitialDefaultPanel()
                    FillCbo()
                    ShowMessage(lblMessage, "Data saved!", False)
                Else
                    ShowMessage(lblMessage, oCustomClass.str, True)
                End If
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
End Class