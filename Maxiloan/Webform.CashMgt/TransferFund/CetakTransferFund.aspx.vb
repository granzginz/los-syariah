﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region

Public Class CetakTransferFund
    Inherits WebBased

    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtAmountFrom0 As ucNumberFormat
    Protected WithEvents txtamountto0 As ucNumberFormat

#Region " Private Const "

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private Dcontroller As New DataUserControlController
    Private oController As New TransferAccountController
    Public Property GM1 As String
    Public Property GM2 As String
    Public Property GM3 As String
    Public Property Direksi As String


#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property TransferNo() As String
        Get
            Return (CType(ViewState("TransferNo"), String))
        End Get
        Set(value As String)
            ViewState("TransferNo") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_UPDATE_SUCCESS, False, True))
            End If
            Me.FormID = "OTOTRANSAK"
            obankaccount.BankPurpose = ""
            obankaccount.BankType = ""
            obankaccount.IsAll = True
            obankaccount.BindBankAccount()

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
                Dim strFileLocation = String.Format("http://{0}/{1}/XML/{2}.pdf", Request.ServerVariables("SERVER_NAME"), strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1), Request.QueryString("strFileLocation"))
                Response.Write("<script language = javascript>" & vbCrLf & "var x = screen.width; " & vbCrLf & "var y = screen.heigth; " & vbCrLf & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf & "</script>")
            End If

            Me.SearchBy = ""
            Me.SortBy = ""

            pnlList.Visible = False
            DtgAgree.Visible = False
        End If
    End Sub

    'Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim chkSender As CheckBox = CType(sender, CheckBox)
    '    Dim chkItem As CheckBox
    '    Dim x As Integer

    '    For x = 0 To DtgAgree.Rows.Count - 1
    '        chkItem = CType(DtgAgree.Rows(x).FindControl("chkItem"), CheckBox)
    '        chkItem.Checked = chkSender.Checked
    '    Next
    'End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spTransferFundTransaction"
        objCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 2000).Value = cmdWhere
        objCommand.Parameters.Add("@SortBy", SqlDbType.VarChar, 100).Value = SortBy
        objread = objCommand.ExecuteReader()
        DtgAgree.DataSource = objread

        Try
            DtgAgree.DataBind()
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
        pnlList.Visible = True
        'pnlDatagrid.Visible = True
    End Sub
    Sub EditOtorisasi(ByVal transferNo As String, ByVal otorisasi As String)
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spOtorisasiTransferFundTransactionEdit"
        objCommand.Parameters.Add("@TransferNo", SqlDbType.VarChar, 50).Value = transferNo
        objCommand.Parameters.Add("@Otorisasi", SqlDbType.Char, 1).Value = otorisasi
        objread = objCommand.ExecuteReader()


        Try
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Sub

    Protected Sub imgSearch_Click(sender As Object, e As EventArgs) Handles imgSearch.Click


        If SessionInvalid() Then
            Exit Sub
        End If
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
        lblMessage.Visible = False
        Me.SearchBy = " where tft.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
        Me.SearchBy = Me.SearchBy & " tft.TransferType = 'TRFAA' and isnull(tft.Otorisasi,'')= 'N' "


        If obankaccount.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " AND tft.BankAccountIdFrom = '" & obankaccount.BankAccountID.Trim & "' "
        End If
        If (dateFrom.Text <> "" And dateTo.Text = "") Then


            Me.SearchBy = Me.SearchBy & " and  tft.ValueDate ='" & ConvertDate2(dateFrom.Text.Trim) & "' "
        ElseIf (dateFrom.Text <> "" And dateTo.Text <> "") Then
            Me.SearchBy = Me.SearchBy & " and tft.ValueDate BETWEEN '" & ConvertDate2(dateFrom.Text.Trim) & "' and  '" & ConvertDate2(dateTo.Text.Trim) & "' "



        End If

        'If IsNumeric(txtAmountFrom0.Text.Trim) And IsNumeric(txtamountto0.Text.Trim) Then
        '    If CDbl(txtAmountFrom0.Text.Trim) >= 0 And CDbl(txtamountto0.Text.Trim) > 0 Then
        '        Me.SearchBy = Me.SearchBy & "AND (tft.Amount BETWEEN " & CDbl(txtAmountFrom0.Text.Trim) & " AND " & CDbl(txtamountto0.Text.Trim) & ")"
        '    End If
        'End If



        pnlList.Visible = True
        DtgAgree.Visible = True


        DoBind(Me.SearchBy, Me.SortBy)
        FillCbo(author1, 1)
        FillCbo(author2, 2)
        FillCbo(author3, 3)
        FillCbo(author4, 4)
    End Sub

    Protected Sub FillCbo(ByVal cboName As DropDownList, ByVal EmployeePosition As String)
        Dim customClass As New Parameter.APDisbApp
        With customClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .EmployeePosition = EmployeePosition
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = ""
            .SpName = "spCboGetEmployee"
        End With
        customClass = GetEmployee(customClass)
        Dim dt As DataTable
        dt = customClass.listDataReport
        With cboName
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub

    Public Function GetEmployee(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 30)
        params(0).Value = CustomClass.BranchId
        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 30)
        params(1).Value = CustomClass.EmployeePosition


        Try
            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Protected Sub imbReset_Click(sender As Object, e As EventArgs) Handles imbReset.Click
        Server.Transfer("CetakTransferFund.aspx")
    End Sub

#Region "print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim j = 0
        Dim dtChoosen As IList(Of String) = New List(Of String)
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim lblTransferNo As String
        Dim cbCheck As CheckBox
        With oDataTable
            .Columns.Add(New DataColumn("TransferNo", GetType(String)))
        End With
        For intloop = 0 To DtgAgree.Items.Count - 1
            cbCheck = CType(DtgAgree.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
            If cbCheck.Checked Then

                lblTransferNo = CType(DtgAgree.Items(intloop).Cells(1).FindControl("lblTransferNo"), Label).Text.Trim
                GM1 = author1.SelectedValue.Trim
                GM2 = author2.SelectedValue.Trim
                GM3 = author3.SelectedValue.Trim
                Direksi = author4.SelectedValue.Trim

                Me.TransferNo = lblTransferNo
                Me.GM1 = GM1
                Me.GM2 = GM2
                Me.GM3 = GM3
                Me.Direksi = Direksi
            End If
        Next
        'For i = 0 To DtgAgree.Items.Count - 1
        '    Dim cb = CType(DtgAgree.Items(i).FindControl("cbCheck"), CheckBox)
        '    If cb.Checked = True Then
        '        j += 1
        '        dtChoosen.Add(CType(DtgAgree.Items(i).FindControl("lblRequestNo"), Label).Text.Trim)
        '    End If
        'Next
        'If j <= 0 Then
        '    ShowMessage(lblMessage, "Harap Check Item", True)
        '    Exit Sub
        'End If
        Dim cookie As HttpCookie = Request.Cookies("RptPmtPembayaranSPPL")
        If Not cookie Is Nothing Then
            cookie.Values("TransferNo") = Me.TransferNo
            cookie.Values("GM1") = Me.GM1
            cookie.Values("GM2") = Me.GM2
            cookie.Values("GM3") = Me.GM3
            cookie.Values("Direksi") = Me.Direksi
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("RptPmtPembayaranSPPL")
            cookieNew.Values.Add("TransferNo", Me.TransferNo)
            cookieNew.Values("GM1") = Me.GM1
            cookieNew.Values("GM2") = Me.GM2
            cookieNew.Values("GM3") = Me.GM3
            cookieNew.Values("Direksi") = Me.Direksi
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("CetakTransferFundViewer.aspx")

    End Sub
#End Region

    'Private Sub imgEdit_Click(sender As Object, e As System.EventArgs) Handles imgEdit.Click

    '    Try




    '        For n As Integer = 0 To dtgPaging.DataKeys.Count - 1
    '            Dim chkItem As CheckBox = CType(dtgPaging.Rows(n).FindControl("chkItem"), CheckBox)
    '            If chkItem.Checked = True Then
    '                Dim lblTransferNo As Label = CType(dtgPaging.Rows(n).FindControl("lblTransferNo"), Label)


    '                Call EditOtorisasi(lblTransferNo.Text.Trim, "E")
    '            End If
    '        Next
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        ShowMessage(lblMessage, "Data Berhasil di diubah", False)
    '    Catch ex As Exception

    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub

    'Private Sub imbReject_Click(sender As Object, e As System.EventArgs) Handles imbReject.Click
    '    Try




    '        For n As Integer = 0 To dtgPaging.DataKeys.Count - 1
    '            Dim chkItem As CheckBox = CType(dtgPaging.Rows(n).FindControl("chkItem"), CheckBox)
    '            If chkItem.Checked = True Then
    '                Dim lblTransferNo As Label = CType(dtgPaging.Rows(n).FindControl("lblTransferNo"), Label)


    '                Call EditOtorisasi(lblTransferNo.Text.Trim, "R")
    '            End If
    '        Next
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        ShowMessage(lblMessage, "Data Berhasil di diubah", False)
    '    Catch ex As Exception

    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
End Class