﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TransferFundInq.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.TransferFundInq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TransferFundInq</title>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewTransferFund(pStyle, pTransferNo, pBranch, JournalNo) {
            window.open(ServerName + App + '/Webform.CashMgt/TransferFund/ViewTransferFund.aspx?style=' + pStyle + '&TransferNo=' + pTransferNo + '&Branch=' + pBranch + '&journalNo=' + JournalNo, 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }
        function OpenWinViewVoucherNoFrom(JournalNo) {
            window.open(ServerName + App + '/Webform.GL/JournalInquiryView.aspx?style=ACCMNT&tr_nomor=' + JournalNo , 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }
        function OpenWinViewVoucherNoTo(JournalNo) {
            window.open(ServerName + App + '/Webform.GL/JournalInquiryView.aspx?style=ACCMNT&tr_nomor=' + JournalNo, 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }
    </script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        INQUIRY TRANSFER DANA
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Cabang
                        </label>
                        <asp:DropDownList ID="cboParent" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                            CssClass="validator_general" InitialValue="0" ErrorMessage="Harap pilih Cabang"
                            ControlToValidate="cboParent"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Tgl Posting <= </label>
                        <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtdate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtdate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Transfer Ke</label>
                        <asp:DropDownList ID="cboTransferType" runat="server">
                            <asp:ListItem Value="All" Selected="True">All</asp:ListItem>
                            <asp:ListItem Value="TRFAA">Rekening ke Rekening</asp:ListItem>
                            <asp:ListItem Value="TRFFD">Transfer Ke Cabang</asp:ListItem>
                            <asp:ListItem Value="TRFEC">Penarikan KP</asp:ListItem>
                            <asp:ListItem Value="PCREI">Reimburse Petty Cash</asp:ListItem>
                            <asp:ListItem Value="PYREQ">Permintaan pembayaran</asp:ListItem>
                            <asp:ListItem Value="INSRQ">Biaya Klaim Asuransi</asp:ListItem>
                            <asp:ListItem Value="COLRQ">Biaya Collection</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="cboStatus" runat="server">
                            <asp:ListItem Value="0">All</asp:ListItem>
                            <asp:ListItem Value="0">New</asp:ListItem>
                            <asp:ListItem Value="1">Reconcile</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="TransferNo">No Transfer</asp:ListItem>
                            <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                            <asp:ListItem Value="RefNo">No Bukti</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server" Width="188px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSFER DANA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgTransferFundInquiry" runat="server" Width="100%" Visible="true"
                                AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="TRANSFERNO" HeaderText="NO TRANSFER">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyTransferNo" runat="server" Text='<%#Container.DataItem("TransferNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            Total
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO BUKTI">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRefNo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Left" Width="13%"> </ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotAmount" runat="server" Text="TOTAL"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PostingDate" SortExpression="PostingDate" HeaderText="TGL POSTING"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="type" HeaderText="JENIS">
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltype" runat="server" Text='<%#Container.DataItem("type")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="VoucherNoFrom" HeaderText="Journal dari">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypVoucherNoFrom" runat="server" Text='<%#Container.DataItem("VoucherNoFrom")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="VoucherNoTo" HeaderText="Journal Kepada">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypVoucherNoTo" runat="server" Text='<%#Container.DataItem("VoucherNoTo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 

                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                  <%--  <asp:TemplateColumn Visible="False" HeaderText="Tr Nomor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJournalNo" runat="server" Text='<%#Container.DataItem("JournalNo")%>'>Label</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
               
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
