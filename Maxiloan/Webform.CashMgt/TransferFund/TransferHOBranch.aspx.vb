﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TransferHOBranch
    Inherits Maxiloan.Webform.WebBased    
    Protected WithEvents oFrombankAccount As ucBankAccountBranch
    Protected WithEvents oTobankAccount As ucBankAccountBranch
    Protected WithEvents obranch As ucBranchHO
    Protected WithEvents oBGNODate As ucBGNoDate
    Protected WithEvents txtamount As ucNumberFormat

    Dim strbanktype As String
    Dim stmwhereBranch As String
    Dim strbranch As String
    Dim strbranchto As String
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.TransferAccount
    Private oController As New TransferHOBranchController    

#End Region

#Region "Property"
    Private Property BranchIDFrom() As String
        Get
            Return CStr(ViewState("BranchIDFrom"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDFrom") = Value
        End Set
    End Property

    Private Property BranchIDTo() As String
        Get
            Return CStr(ViewState("BranchIDTo"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchIDTo") = Value
        End Set
    End Property

    Private Property FromBankAccount() As String
        Get
            Return (CType(ViewState("FromBankAccount"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("FromBankAccount") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            lblMessage.Visible = False
            Me.FormID = "TRFFDINTERBRC"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'stmwhereBranch = "  isheadoffice = '0'  and branchid <> " & Me.sesBranchId.Replace("'", "").Trim
                stmwhereBranch = " branchid <> " & Me.sesBranchId.Replace("'", "").Trim
                obranch.BindBranchHO(stmwhereBranch)

                Me.SearchBy = ""
                Me.SortBy = ""
                TglValuta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                oBGNODate.InDueDate = Me.BusinessDate.ToString("dd/MM/yyyy")
                With txtamount
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = False
                    .TextCssClass = "numberAlign regular_text"
                End With
            End If
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            If ConvertDate2(TglValuta.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Valuta Tidak Boleh Melebihi Tanggal Sistem", True)
                Exit Sub
            End If
            If cboTransferType.SelectedItem.Value.Trim = "1" Then
                strbranch = obranch.BranchID
                oFrombankAccount.BindBankAccountBranch(strbranch)
                strbranchto = Me.sesBranchId.Replace("'", "")
                oTobankAccount.BindBankAccountBranch(strbranchto)
                Me.BranchIDTo = Me.sesBranchId.Replace("'", "")
                Me.BranchIDFrom = obranch.BranchID
            Else
                strbranch = Me.sesBranchId.Replace("'", "")
                oFrombankAccount.BindBankAccountBranch(strbranch)
                strbranchto = obranch.BranchID
                oTobankAccount.BindBankAccountBranch(strbranchto)
                Me.BranchIDFrom = Me.sesBranchId.Replace("'", "")
                Me.BranchIDTo = obranch.BranchID
            End If
            lblValueDate.Text = TglValuta.Text
            lblTransferType.Text = cboTransferType.SelectedItem.Text
            lblBranch.Text = obranch.BranchName
            pnlDetail.Visible = False
            pnlBG.Visible = True
            pnlList.Visible = False
            PanelBGNO.Visible = False
        End If

    End Sub

    Private Sub btnNext2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext2.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim dtView As New DataTable
            Dim oController As New BankController

            If CDbl(txtamount.Text) <= 0 Then
                ShowMessage(lblMessage, "Jumlah harus > 0 ", True)
                Exit Sub
            End If

            dtView = oController.BankAccountInformasi(GetConnectionString, oFrombankAccount.BankAccountID.Trim, Me.sesBranchId.Replace("'", ""))

            'If oFrombankAccount.BankAccountID.Trim <> oTobankAccount.BankAccountID.Trim Then
            lblValueDate2.Text = TglValuta.Text
            lblTransferType2.Text = cboTransferType.SelectedItem.Text
            lblBranch2.Text = obranch.BranchName
            lblRefNo.Text = txtrefno.Text
            lblFromBankAccount.Text = oFrombankAccount.BankAccountName
            lblToBankAccount.Text = oTobankAccount.BankAccountName
            lblAmount.Text = txtamount.Text
            lblDesc.Text = txtdesc.Text

            Dim stmwhere As String
            If cboTransferType.SelectedItem.Value = "1" Then
                stmwhere = "bankaccountid = '" & oFrombankAccount.BankAccountID & "'  and " & _
                                    " branchid = '" & obranch.BranchID.Trim & "'  and status = 'N'"
            Else
                stmwhere = "bankaccountid = '" & oFrombankAccount.BankAccountID & "'  and " & _
                                    " branchid = '" & Me.sesBranchId.Replace("'", "") & "'  and status = 'N'"
            End If

            oBGNODate.BindBGNODate(stmwhere)

            oCustomClass.strConnection = GetConnectionString()


            If dtView.Rows.Count > 0 Then

                If CBool(dtView.Rows(0).Item("IsSweepOtomatis").ToString) And obranch.BranchID.Trim = "000" And cboTransferType.SelectedItem.Value = "2" Then
                    oBGNODate.Visible = False
                    PanelBGNO.Visible = False
                    oBGNODate.BGNumber = "None"
                Else
                    oBGNODate.Visible = True
                End If

            Else
                oBGNODate.Visible = True
            End If

            pnlDetail.Visible = True
            pnlBG.Visible = False
            pnlList.Visible = False
            'Else
            '    lblMessage.Text = "Bank Account ID From <> BankAccountID To"
            '    Exit Sub
            'End If
        End If
    End Sub

    Private Sub btnCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel2.Click
        Response.Redirect("TransferHOBranch.aspx")
    End Sub

    Private Sub btnCancel3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel3.Click
        Response.Redirect("TransferHOBranch.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
		If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then

            '''' Cegat Ending Balance Kurang Dari 0 | ABDI 23/11/2018 ''''
            Dim cCustomClass As New Parameter.TransferForReimbursePC
			Dim cController As New TransferForReimbursePCController

			With cCustomClass
				.strConnection = GetConnectionString()
				.Amount = CDbl(txtamount.Text.Trim)

				If cboTransferType.SelectedItem.Value = 1 Then 'tarik dari cabang'
                    .BankAccountID = oTobankAccount.BankAccountID.Trim
                    .BranchId = obranch.BranchID
				ElseIf cboTransferType.SelectedItem.Value = 2 Then 'transfer ke'
                    .BankAccountID = oFrombankAccount.BankAccountID.Trim
                    .BranchId = obranch.BranchID
				End If

			End With
			cCustomClass = cController.CheckEndingBalance(cCustomClass)
			If cCustomClass.str <> "" Then
				ShowMessage(lblMessage, "Saldo tidak Cukup", True)
				Exit Sub
			End If
			'''' ***** ''''

			Dim DueDate As Date = Nothing

			With oCustomClass
				.BranchId = Me.sesBranchId.Replace("'", "")
				.bankAccountTo = oTobankAccount.BankAccountID
				.bankAccountFrom = oFrombankAccount.BankAccountID
				.LoginId = Me.Loginid
				.BusinessDate = Me.BusinessDate
				.Amount = CDbl(txtamount.Text)
				.valueDate = ConvertDate2(TglValuta.Text)
				.referenceNo = lblRefNo.Text
				.Notes = lblDesc.Text
				.bgNo = oBGNODate.BGNumber

				If oBGNODate.InDueDate <> "" Then
					DueDate = ConvertDate2(oBGNODate.InDueDate)
				Else
					DueDate = ConvertDate2("01/01/1900")
				End If

				.bgDate = DueDate
				.strConnection = GetConnectionString()
				.TransferType = cboTransferType.SelectedItem.Value
				.BranchIDfrom = Me.BranchIDFrom
				.BranchIDTo = Me.BranchIDTo

				'=========untuk save krn transaction idnya lain===============
				If cboTransferType.SelectedItem.Value = "1" Then
					oCustomClass.TransactionIDfrom = ""
					oCustomClass.TransactionIDTo = "THR"
					oCustomClass.TransferTypeDesc = "TRFEC"
					oCustomClass.BranchIDfrom = Me.BranchIDFrom
					'oCustomClass.BranchIDfrom = ""
					oCustomClass.BranchIDTo = Me.BranchIDTo
				ElseIf cboTransferType.SelectedItem.Value = "2" Then
					oCustomClass.TransactionIDfrom = "THD"
					'oCustomClass.TransactionIDTo = "TBR"
					oCustomClass.TransactionIDTo = ""
					oCustomClass.TransferTypeDesc = "TRFFD"
					oCustomClass.BranchIDfrom = Me.BranchIDFrom
					oCustomClass.BranchIDTo = Me.BranchIDTo
					'oCustomClass.BranchIDTo = ""
				End If
				'=================================================

				.JenisTransfer = rboJenisTransfer.SelectedValue
				If cboCaraBayar.SelectedValue = "BA" Then .JenisTransfer = ""
				.PaymentTypeID = cboCaraBayar.SelectedValue
				.CompanyID = Me.SesCompanyID
			End With

			Try
				If cboCaraBayar.SelectedValue = "GT" Then
					oCustomClass = oController.EBankTRFFUNDAdd(oCustomClass)
					If oCustomClass.strError = "" Then
						ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
					Else
						ShowMessage(lblMessage, oCustomClass.strError, True)
					End If
				Else
					oController.SaveTransferHOBranch(oCustomClass)
					ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
				End If
				ClearForm()
			Catch exp As Exception
				ShowMessage(lblMessage, exp.Message, True)
			End Try
		End If
	End Sub

    Private Sub CboCaraBayar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaraBayar.SelectedIndexChanged
        If cboCaraBayar.SelectedItem.Value = "GT" Then
            PnlJenisTransfer.Visible = True
            PanelBGNO.Visible = False
        ElseIf cboCaraBayar.SelectedItem.Value = "BA" Then
            PnlJenisTransfer.Visible = False
            PanelBGNO.Visible = True
        End If
    End Sub

    Sub ClearForm()
        pnlList.Visible = True
        pnlBG.Visible = False
        pnlDetail.Visible = False
        cboTransferType.SelectedIndex = 0
        stmwhereBranch = " branchid <> " & Me.sesBranchId.Replace("'", "").Trim
        obranch.BindBranchHO(stmwhereBranch)
        TglValuta.Text = ""
        txtrefno.Text = ""
        cboCaraBayar.SelectedIndex = 0
        rboJenisTransfer.SelectedIndex = 0
        txtamount.Text = ""
    End Sub


End Class