﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CashAdvancedReversal.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.CashAdvancedReversal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TDPReversalList</title>
     <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        //function click() {
        //    if (event.button == 2) {
        //        alert('Anda tidak Berhak');
        //    }
        //}
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server"  ></asp:Label>
    <div class="form_title">  <div class="title_strip"> </div>
                <div class="form_single">
                     
                    <h4>
                         REVERSAL CASH ADVANCED  </h4>
                </div>
            </div>
    
    <div class="form_box">
					<div class="form_left">
					<label>  No Cash Advanced </label>
					 <asp:Label ID="lblNoCA" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label>   Tanggal Disburse </label>
					 <asp:Label ID="lblDisburseDate" runat="server"></asp:Label>
					</div>
				</div>

                <div class="form_box">
                    <div class="form_left">
                    <label> Journal Bank Disburse </label>
					 <asp:Label ID="lbldesc" runat="server"></asp:Label>
					</div>
                    <div class="form_right">
					<label>  Jumlah Terima </label>
					<asp:Label ID="lblAmount" runat="server"></asp:Label>
					</div>
                </div>


                <div class="form_box_hide">
					<div class="form_left">
					<label> Rekening Bank </label>
					<asp:Label ID="lblNomerRekening" runat="server"></asp:Label>
					</div> 
                    <div class="form_right">
					 <%-- <label>Jumlah Terima </label>
					<asp:Label ID="lblAmount" runat="server"></asp:Label>--%>
					</div>
				</div>

                <div class="form_box">
					<div class="form_left">
					<label> Tanggal CA </label>
					<asp:Label ID="lblCADate" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					<label> Penerima </label>
					<asp:Label ID="lblPenerima" runat="server"></asp:Label>
					</div>
				</div>
    
                <div class="form_box_hide">
					<div class="form_left">
					<%--<label> Journal Bank Disburse </label>
					 <asp:Label ID="lbldesc" runat="server"></asp:Label>--%>
					</div>
					<div class="form_right">
					 <label> Nama Bank </label>
					 <asp:Label ID="lblBank" runat="server"></asp:Label>
					</div>
					</div>
			<%--	</div>--%>
                <div class="form_box">
					<div class="form_left">
                        <label> Departemen </label>
					  <asp:Label ID="lbldepartemen" runat="server"></asp:Label>
					</div>
					<div class="form_right">
					 <label> Penggunaan </label>
					 <asp:Label ID="lblpenggunaan" runat="server"></asp:Label>
					</div>
				</div>
        <div class="form_box">
					<div class="form_left">
                        <%--<label> Catatan </label>--%>
                        <label> Alasan Reversal </label>
					  <asp:TextBox ID="txtCorrection" runat="server"  Width="70%" TextMode="MultiLine"></asp:TextBox>
					</div>
				</div>


<asp:Panel ID="pnlList" runat="server"> 
                <%--<div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL TRANSAKSI
                </h4>
            </div>
        </div>--%>
 <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CssClass="grid_general" ShowFooter="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <%--<Columns>

                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                <HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblNoCA" runat="server"  Text='<%#DataBinder.Eval(Container, "DataItem.NoCA") %>'>   
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center" Width="45%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate> 
                                    <asp:Label ID="Label2" runat="server"  Text='<%#DataBinder.Eval(Container, "DataItem.Penggunaan") %>'>    
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="Label1" runat="server">Total</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPCDAmount" runat="server"  Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.Jumlah"), 2) %>'>   
                                    </asp:Label>
                                     <asp:Label ID="lblSeq" runat="server" Visible="False" Text='<%#DataBinder.Eval(Container, "DataItem.SequenceNo") %>' />
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotal" runat="server">Label</asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>--%>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
       </asp:Panel>
        <asp:HiddenField ID="hdvNoca" runat="server" Value="" />

      <div class="form_button">
			  <asp:Button ID="btnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray">
                </asp:Button>
			 </div>

        
    
    </form>
</body>
</html>
