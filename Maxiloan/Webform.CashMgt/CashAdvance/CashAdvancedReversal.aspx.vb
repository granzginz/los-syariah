﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CashAdvancedReversal
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents TanggalDisburse As ucDateCE
    Protected WithEvents ucJumlahPengembalian As ucNumberFormat
    Protected WithEvents UcTanggalRealisasi As ucDateCE

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    'Private oCustomClass As New Parameter.TerimaTDP
    Private oCustomClass As New Parameter.CashAdvance
    'Private oController As New ImplementasiControler
    Private oController As New PengajuanCashAdvancedController
    Dim strbankaccountid As String
    Private Const STR_PARENT_FORM_ID As String = "PCRVS"
    Private m_TotalCAmount As Double
#End Region

#Region "Property"

    Private Property NoCA() As String
        Get
            Return (CType(ViewState("NoCA"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("NoCA") = Value
        End Set
    End Property

    Private Property BankAccountid() As String
        Get
            Return (CType(ViewState("BankAccountid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountid") = Value
        End Set
    End Property



#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "RevTDP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.NoCA = Request.QueryString("NoCA")
                Me.BranchID = Request.QueryString("branchid")

                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind()
            End If
            m_TotalCAmount = 0
            lblMessage.Visible = False

        End If
    End Sub

#Region "DoBind"

    Sub FillGrid(ByRef oTable As DataTable, ByRef oGrid As DataGrid)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            oGrid.DataSource = DvUserList

            Try
                oGrid.DataBind()
            Catch
                oGrid.CurrentPageIndex = 0
                oGrid.DataBind()
            End Try

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try

    End Sub

    'Sub saveToTemporal(dt As DataTable)
    '    Dim val = dt.AsEnumerable().Select(Function(r) New MementoPettyCast(
    '     r.Field(Of String)("TransactionName"), r.Field(Of String)("PCDDescription"), r.Field(Of Decimal)("PCDAmount"), r.Field(Of Int16)("SequenceNo"), r.Field(Of String)("PaymentAllocationId"))).ToList()

    '    MementoPettyCasts = val
    'End Sub

    Private Sub DoBind()
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            With oCustomClass
                '.BranchId = Me.BranchID.Trim.Replace("'", "")
                .NoCA = Me.NoCA.Trim
                .strConnection = GetConnectionString()
            End With
            oCustomClass = oController.ReverseReversalCashAdvanced(oCustomClass)

            'oCustomClass.NoCA = Me.NoCA
            'oCustomClass.strConnection = GetConnectionString()
            'oCustomClass.BranchId = Me.BranchID

            'oCustomClass = oController.ReverseReversalCashAdvanced(oCustomClass)
            With oCustomClass
                lblNoCA.Text = Me.NoCA
                lblDisburseDate.Text = .TanggalDisburse.ToString("dd/MM/yyyy")
                lblNomerRekening.Text = .NomorRekening
                lblBank.Text = .BankBranchName
                'lblRefNO.Text = .ReferenceNo
                'txtReversalNo.Text = .reversalno
                lblCADate.Text = .TanggalCA.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.Jumlah, 2)
                lbldesc.Text = .JournalBank
                lblPenerima.Text = .Penerima
                lblpenggunaan.Text = .Penggunaan
                lbldepartemen.Text = .Department

                FillGrid(oCustomClass.PagingTable, DtgAgree)
                'lblDepartmentName.text = .Department
                'lblBank.Text = .Bank
                'strbankaccountid = .BankAccountID
                'Me.BankAccountid = strbankaccountid
                lblMessage.Text = ""
            End With
        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub
#End Region

    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    

    'Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

    '    If e.Item.ItemIndex >= 0 Then
    '        Dim lbltemp = CType(e.Item.FindControl("lblAmountTrans"), Label)
    '        If Not lbltemp Is Nothing Then
    '            m_TotalCAmount = m_TotalCAmount + CType(lbltemp.Text, Double)
    '        End If

    '        'Dim seq = CType(e.Item.FindControl("lblSeq"), Label).Text.Trim
    '        'Dim trName = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink).Text.Trim

    '        'CType(e.Item.FindControl("hyAction"), HyperLink).Attributes.Add("onclick", String.Format("ShowEditor('{0}','{1}');", seq, trName))
    '    End If
    '    If e.Item.ItemType = ListItemType.Footer Then
    '        Dim lblTotTransAmount = CType(e.Item.FindControl("lblTotTransAmount"), Label)
    '        If Not lblTotTransAmount Is Nothing Then
    '            lblTotTransAmount.Text = FormatNumber(m_TotalCAmount.ToString, 2)
    '        End If
    '    End If
    'End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        If e.Item.ItemIndex >= 0 Then
            Dim lbltemp = CType(e.Item.FindControl("lblPCDAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalCAmount = m_TotalCAmount + CType(lbltemp.Text, Double)
            End If

            'Dim seq = CType(e.Item.FindControl("lblSeq"), Label).Text.Trim
            'Dim trName = CType(e.Item.FindControl("hyPettyCashNo"), HyperLink).Text.Trim

            'CType(e.Item.FindControl("hyAction"), HyperLink).Attributes.Add("onclick", String.Format("ShowEditor('{0}','{1}');", seq, trName))
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalCAmount.ToString, 2)
            End If
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            btnSave.Attributes.Add("Onclick", "return checksubmit()")
            With oCustomClass
                .NoCA = Me.NoCA
                .BranchId = Me.BranchID
                .LoginId = Me.Loginid
                '.BusinessDate = Me.BusinessDate
                .strConnection = GetConnectionString()
                '.description = txtCorrection.Text
                '.BankAccountID = Me.BankAccountid
                '.AmountRec = CDbl(lblAmount.Text)
                '.ValueDate = lblTDPDate.Text
                '.ReferenceNo = txtReversalReffNo.Text.Trim
                '.CoyID = Me.SesCompanyID
                .TanggalDisburse = ConvertDate2(lblDisburseDate.Text)
                .Jumlah = lblAmount.Text
                .Penerima = lblPenerima.Text
                .NomorRekening = lblNomerRekening.Text
                .TanggalCA = ConvertDate2(lblCADate.Text)
                .JournalBank = lbldesc.Text
                .BankBranchName = lblBank.Text
                '.reversalno = txtReversalNo.Text
                .Notes = txtCorrection.Text
                .Penggunaan = lblpenggunaan.Text
                .Department = lbldepartemen.Text
            End With
            Try
                oController.SaveReverseReversalCashAdvanced(oCustomClass)
                Server.Transfer("CashAdvancedReversalList.aspx?Message=" & Server.UrlEncode("Record saved"))

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub



    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Server.Transfer("CashAdvancedReversalList.aspx")
    End Sub

End Class