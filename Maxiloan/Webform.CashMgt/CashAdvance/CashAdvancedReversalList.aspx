﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CashAdvancedReversalList.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.CashAdvancedReversalList" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CashAdvancedReversalList</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />   
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    REVERSAL CASH ADVANCE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <uc1:ucbranchall id="oBranch1" runat="server"></uc1:ucbranchall>
            </div>
            <div class="form_right">
            </div>
        </div>
        <%--<div class="form_box">
            <div class="form_left">
                <label>
                    Tipe Reversal
                </label>
                <asp:DropDownList runat="server" ID="cboReversal">
                    <asp:ListItem Value="RS">Reversal</asp:ListItem>
                    <asp:ListItem Value="RF">Reversal Factoring</asp:ListItem>
                    <asp:ListItem Value="RM">Reversal Modal Usaha</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rvReversal" ControlToValidate="cboReversal"
                    InitialValue="" ErrorMessage="Pilih tipe reversal" CssClass="validator_general">
                </asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>--%>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy1" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="NoCA" CssClass="grid_general"
                            OnSortCommand="SortGrid">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle  ></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HypReceive" runat="server" Text='REVERSAL'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NoCA" HeaderText="NO CASH ADVANCED">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink ID="lblNoCA" runat="server" Text='<%#Container.DataItem("NoCA")%>'> </asp:HyperLink>--%>
                                        <asp:Label ID="lblNoCA" runat="server" Text='<%#Container.DataItem("NoCA")%>'>
                                             </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>                           
                                <asp:BoundColumn DataField="TanggalCA" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TANGGAL CA"
                                SortExpression="TanggalCashAdvanced"></asp:BoundColumn>
                                <asp:BoundColumn DataField="TanggalDisburse" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TANGGAL DISBURSE"
                                SortExpression="TanggalDisb"></asp:BoundColumn>                                
                                <asp:TemplateColumn SortExpression="Jumlah" HeaderText="JUMLAH">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink ID="lblTanggalCA" runat="server" Text='<%#Container.DataItem("TanggalCA")%>'></asp:HyperLink>--%>
                                        <asp:Label ID="lblJumlah" runat="server" Text='<%#Container.DataItem("Jumlah")%>'>
                                             </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="JournalBank" HeaderText="JOURNALBANK">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink ID="lblTanggalCA" runat="server" Text='<%#Container.DataItem("TanggalCA")%>'></asp:HyperLink>--%>
                                        <asp:Label ID="lblJournalBank" runat="server" Text='<%#Container.DataItem("JournalBank")%>'>
                                             </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:TemplateColumn SortExpression="CaraBayar" HeaderText="CHEQUE/BG">
                                    <%--<ItemTemplate>
                                        <%--<asp:HyperLink ID="lblTanggalCA" runat="server" Text='<%#Container.DataItem("TanggalCA")%>'></asp:HyperLink>--%>
                                        <%--<asp:Label ID="lblCaraBayar" runat="server" Text='<%#Container.DataItem("CaraBayar")%>'></asp:Label>--%>
                                   <%-- </ItemTemplate>--%>
                               <%-- </asp:TemplateColumn>--%>
                                <asp:TemplateColumn SortExpression="Penerima" HeaderText="PENERIMA">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink ID="lblTanggalCA" runat="server" Text='<%#Container.DataItem("TanggalCA")%>'></asp:HyperLink>--%>
                                        <asp:Label ID="lblPenerima" runat="server" Text='<%#Container.DataItem("Penerima")%>'>
                                             </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Penggunaan" HeaderText="PENGGUNAAN">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink ID="lblTanggalCA" runat="server" Text='<%#Container.DataItem("TanggalCA")%>'></asp:HyperLink>--%>
                                        <asp:Label ID="lblPenggunaan" runat="server" Text='<%#Container.DataItem("Penggunaan")%>'>
                                             </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                    <ItemTemplate>                                        
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                             </asp:Label>
                                        <asp:Label ID="lblNomerRekening" visible ="false" runat="server" Text='<%#Container.DataItem("NomorRekening")%>'>
                                             </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left"  
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagridFactoring" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK FACTORING
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgreeFactoring" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="Applicationid" CssClass="grid_general"
                            OnSortCommand="SortGrid">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle  ></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HypReceive" runat="server" Text='REVERSAL'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceNo" visible ="True" HeaderText="NO INVOICE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                        </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="TYPE CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstallment" runat="server" Text='<%#FormatNumber(Container.DataItem("InstallmentAmount"), 2)%> &nbsp;'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceSeqNo" HeaderText="No Seq Invoice">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceSeqNo" runat="server" Text='<%#Container.DataItem("InvoiceSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left"  
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="TextBox1" runat="server">1</asp:TextBox>
                            <asp:Button ID="Button1" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="Label1" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="Label2" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="Label3" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagridModalUsaha" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK MODAL KERJA
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgreeModalUsaha" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="Applicationid" CssClass="grid_general"
                            OnSortCommand="SortGrid">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle  ></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HypReceive" runat="server" Text='REVERSAL'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" runat="server" Text='<%#Container.DataItem("Applicationid")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn SortExpression="InvoiceNo" visible ="True" HeaderText="NO INVOICE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                        </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="TYPE CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%> &nbsp;'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceSeqNo" HeaderText="No Seq Invoice">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceSeqNo" runat="server" Text='<%#Container.DataItem("InvoiceSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left"  
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="TextBox2" runat="server">1</asp:TextBox>
                            <asp:Button ID="Button2" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="Label4" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="Label5" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="Label6" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
