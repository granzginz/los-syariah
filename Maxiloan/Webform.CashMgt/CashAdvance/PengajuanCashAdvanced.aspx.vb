﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class PengajuanCashAdvanced
    Inherits Maxiloan.Webform.WebBased



    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


    Private oController As New PengajuanCashAdvancedController

    Protected WithEvents GridNavigator As ucGridNav

#Region "user controller"

    Protected WithEvents TanggalDisburse As ucDateCE
    Protected WithEvents Jumlah As ucNumberFormat

    'Protected WithEvents ucDariRekening As UcListBoxGetter
    'Protected WithEvents ucBankMaster As UcListBoxGetter
    Protected WithEvents ucBiayaTransfer As ucNumberFormat
    Protected WithEvents UcBankAccount As UcBankAccount


    Protected WithEvents S_StartTanggalPengajuan As ucDateCE
    Protected WithEvents S_EndTanggalPengajuan As ucDateCE
    Protected WithEvents S_StartTanggalDisburse As ucDateCE
    Protected WithEvents S_EndTanggalDisburse As ucDateCE
    'Protected WithEvents ucAttachment As UcFileDocumentUploader
    Protected WithEvents ucJumlahPengembalian As ucNumberFormat
    Protected WithEvents UcTanggalRealisasi As ucDateCE

#End Region

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property BankTemp() As String
        Get
            Return CType(ViewState("vwsBankTemp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsBankTemp") = Value
        End Set
    End Property
    Private Property CabangTemp() As String
        Get
            Return CType(ViewState("vwsCabangTemp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCabangTemp") = Value
        End Set
    End Property
    Private Property NoRekeningTemp() As String
        Get
            Return CType(ViewState("vwsNoRekeningTemp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsNoRekeningTemp") = Value
        End Set
    End Property
    Private Property AtasNamaTemp() As String
        Get
            Return CType(ViewState("vwsAtasNamaTemp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAtasNamaTemp") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Style.Item("display") = "none"
        Me.FormID = "CASHADVANCE"



        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation

        If Not Me.IsPostBack Then
            Session.Add("ConString", GetConnectionString())
            Session.Add("BranchID", Me.sesBranchId.Replace("'", ""))

            'BindGridInq("", "")
            FillCboRekening()
            initialData()

            With UcBankAccount
                '.ValidatorTrue()
                .Style = "Marketing"
                .BindBankAccount()
            End With

        End If

        If Request.Form(BtnSave.ClientID) IsNot Nothing Then
            TanggalDisburse.IsRequired = False
        Else
            TanggalDisburse.IsRequired = True
        End If

    End Sub

    Private Sub initialData()
        SearchBox.Visible = True
        DatagridPanel.Visible = False
        PanelAddEdit.Visible = False
        panelLabelAddEdit.Visible = False
        clearInput()
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridInq(Me.CmdWhere, "")
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Sub BindGridInq(ByVal PCmdWhere As String, ByVal PSortBy As String, Optional isFrNav As Boolean = False)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClass As New Parameter.CashAdvance
        Dim myRow() As Data.DataRow

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = PCmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = PSortBy
            .SelectedMenu = "Pengajuan"
        End With

        oCustomClass = oController.GetListCashAdvance(oCustomClass)

        recordCount = oCustomClass.TotalRecord

        'For index = 0 To oCustomClass.ListData.Rows.Count - 1
        '    myRow = oCustomClass.ListData.Select("CaraBayar = 'B'")
        '    myRow(index)("CaraBayar") = "Bank Transfer"
        'Next

        dtsEntity = oCustomClass.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = SortBy
        datagridData.DataSource = dtvEntity

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        Try
            datagridData.DataBind()
        Catch
            datagridData.CurrentPageIndex = 0
            datagridData.DataBind()
        End Try

    End Sub
    Private Sub PanelSetting()
        PanelAddEdit.Visible = True
        PanelInput.Visible = True
        panelLabelAddEdit.Visible = False
        panelTanggalRealisasi.Visible = False
        PanelDisburse.Visible = False
        PanelApproval.Visible = True
        panelAttachment.Visible = False
        SearchBox.Visible = False
    End Sub
    Private Sub datagridData_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles datagridData.ItemCommand
        If e.CommandName = "EDIT" Then
            PanelSetting()

            'INPUT
            TanggalDisburse.Text = e.Item.Cells(5).Text

            Select Case e.Item.Cells(7).Text
                Case "Bank Transfer"
                    CaraBayar.SelectedValue = CaraBayar.Items.FindByValue(IIf(e.Item.Cells(7).Text = "&nbsp;", "", "B")).Value
                Case "Cash"
                    CaraBayar.SelectedValue = CaraBayar.Items.FindByValue(IIf(e.Item.Cells(7).Text = "&nbsp;", "", "C")).Value
                Case Else
                    CaraBayar.SelectedValue = CaraBayar.Items.FindByValue(IIf(e.Item.Cells(7).Text = "&nbsp;", "", "BG")).Value
            End Select


            Jumlah.Text = e.Item.Cells(6).Text
            Penggunaan.Text = e.Item.Cells(9).Text
            NamaPenerima.Text = e.Item.Cells(8).Text
            txtIDPenerima.Text = e.Item.Cells(23).Text
            If CaraBayar.SelectedValue <> "B" Then
                UcBankAccount.City = ""
                UcBankAccount.AccountNo = ""
                'NoRekening.Enabled = False
                UcBankAccount.AccountName = ""
                'AtasNama.Enabled = False

                UcBankAccount.Visible = False

                'Simpan Untuk Load selected change object
                CabangTemp = ""
                NoRekeningTemp = ""
                BankTemp = ""
                AtasNamaTemp = ""

                'ucBankMaster.Enabled = False
                'Cabang.Enabled = False
                'ucDariRekening.Enabled = False
                ddlOpsiTrf.Enabled = False
                ucBiayaTransfer.Enabled = False
            Else
                'AtasNama.Enabled = True
                'NoRekening.Enabled = True
                'ucBankMaster.Enabled = True
                'Cabang.Enabled = True
                'Cabang.Text = e.Item.Cells(12).Text
                UcBankAccount.City = e.Item.Cells(12).Text
                UcBankAccount.Visible = True
                UcBankAccount.AccountNo = e.Item.Cells(13).Text
                'ucBankMaster.SelectedValue = ucBankMaster.Items.FindByValue(IIf(e.Item.Cells(11).Text = "&nbsp;", "", e.Item.Cells(11).Text)).Value
                UcBankAccount.BankID = e.Item.Cells(11).Text.Trim
                UcBankAccount.BankCodeBank = e.Item.Cells(11).Text.Trim
                UcBankAccount.BankBranchId = e.Item.Cells(24).Text.Trim
                UcBankAccount.BankBranchName = e.Item.Cells(25).Text.Trim
                If e.Item.Cells(15).Text = "-" Then
                    'ucDariRekening.SelectedValue = ucDariRekening.Items.FindByValue("").Value
                Else
                    'ucDariRekening.SelectedValue = ucDariRekening.Items.FindByValue(IIf(e.Item.Cells(15).Text = "&nbsp;", "", e.Item.Cells(15).Text)).Value
                End If

                If e.Item.Cells(16).Text = "-" Then
                    ddlOpsiTrf.SelectedValue = ddlOpsiTrf.Items.FindByValue("").Value
                Else
                    ddlOpsiTrf.SelectedValue = ddlOpsiTrf.Items.FindByValue(IIf(e.Item.Cells(16).Text = "&nbsp;", "", e.Item.Cells(16))).Value
                End If

                ucBiayaTransfer.Text = e.Item.Cells(17).Text

                UcBankAccount.AccountName = e.Item.Cells(14).Text

                txtNotes.Text = IIf(e.Item.Cells(22).Text = "&nbsp;", "", e.Item.Cells(22).Text)

                'Isi jika value dipilih dan selected
                CabangTemp = e.Item.Cells(12).Text
                NoRekeningTemp = e.Item.Cells(13).Text
                BankTemp = IIf(e.Item.Cells(11).Text = "&nbsp;", "", e.Item.Cells(11).Text)
                AtasNamaTemp = e.Item.Cells(14).Text

            End If



            If e.Item.Cells(18).Text = "-" Then
                ddlBebanBiayaTransfer.SelectedValue = ddlBebanBiayaTransfer.Items.FindByValue("").Value
            Else
                ddlBebanBiayaTransfer.SelectedValue = ddlBebanBiayaTransfer.Items.FindByValue(IIf(e.Item.Cells(18).Text = "&nbsp;", "", e.Item.Cells(18).Text)).Value
            End If

            'OUTPUT

            hdvNoca.Value = e.Item.Cells(3).Text

            lblValueTanggalBayar.Text = e.Item.Cells(6).Text
            lblValueCaraBayar.Text = e.Item.Cells(7).Text
            lblValueJumlah.Text = e.Item.Cells(6).Text
            lblValuePenggunaan.Text = e.Item.Cells(9).Text
            lblValueNamaPenerima.Text = e.Item.Cells(8).Text
            lblValueCabang.Text = e.Item.Cells(12).Text
            lblValueNoRekening.Text = e.Item.Cells(13).Text
            'lblValueBank.Text = ucBankMaster.Items.FindByValue(IIf(e.Item.Cells(11).Text = "&nbsp;", "", e.Item.Cells(11).Text)).Text
            lblValueBank.Text = e.Item.Cells(11).Text
            lblValueAtasNama.Text = e.Item.Cells(14).Text

            DatagridPanel.Visible = False

            'jika Status T maka tampilkan Notes
            txtNotes.Enabled = False
            If e.Item.Cells(10).Text = "T" Then
                ddlKeputusan.Visible = False
            Else
                PanelApproval.Visible = False
            End If

        End If

        'CANCEL

        If e.CommandName = "Cancel" Then



            Dim ocustomClass As New Parameter.CashAdvance
            hdvNoca.Value = e.Item.Cells(3).Text
            'With ocustomClass
            '    .strConnection = GetConnectionString()
            '    .NoCA = hdvNoca.Value
            '    .SaveMenu = 
            'End With

            Try
                ocustomClass = oController.SaveCashAdvance(GetInputUser("PengajuanCancel"))
                lblMessage.Style.Item("display") = "inline"
                ShowMessage(lblMessage, "Sukses Cancel", False)
            Catch ex As Exception
                lblMessage.Style.Item("display") = "inline"
                ShowMessage(lblMessage, "Failed Cancel", True)
            End Try

            BindGridInq(Me.CmdWhere, "")
        End If

        If e.CommandName = "Request" Then
            Dim ocustomClass As New Parameter.CashAdvance
            hdvNoca.Value = e.Item.Cells(3).Text
            'With ocustomClass
            '    .strConnection = GetConnectionString()
            '    .NoCA = hdvNoca.Value
            '    .SaveMenu = 
            'End With

            Try
                ocustomClass = oController.SaveCashAdvance(GetInputUser("PengajuanRequest"))
                lblMessage.Style.Item("display") = "inline"
                ShowMessage(lblMessage, "Sukses Request", False)
            Catch ex As Exception
                lblMessage.Style.Item("display") = "inline"
                ShowMessage(lblMessage, "Failed Request", True)
            End Try

            BindGridInq(Me.CmdWhere, "")
        End If


    End Sub



    Private Function GetInputUser(Optional ByVal ChoseSaveMenu As String = "") As Parameter.CashAdvance

        Dim ocustomClass As New Parameter.CashAdvance
        With ocustomClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.Session("sesBranchID"), "'", "")

            .NoCA = hdvNoca.Value
            If TanggalDisburse.Text = "" Then
                .TanggalDisburse = "1900/01/01"
            Else
                .TanggalDisburse = ConvertDate2(TanggalDisburse.Text)
            End If

            Select Case CaraBayar.SelectedValue
                Case "B"
                    .CaraBayar = CaraBayar.SelectedValue
                    .Jumlah = Jumlah.Text
                    .Penggunaan = Penggunaan.Text
                    .Penerima = NamaPenerima.Text
                    .IDPenerima = txtIDPenerima.Text
                    '.Bank = UcBankMaster.SelectedValue
                    .Bank = UcBankAccount.BankID
                    .CabangBank = UcBankAccount.City
                    .BankBranchID = UcBankAccount.BankBranchId
                    .BankBranchName = UcBankAccount.BankBranchName
                    .NomorRekening = UcBankAccount.AccountNo
                    .AtasNama = UcBankAccount.AccountName
                Case Else
                    .CaraBayar = CaraBayar.SelectedValue
                    .Jumlah = Jumlah.Text
                    .Penggunaan = Penggunaan.Text
                    .Penerima = NamaPenerima.Text
                    .IDPenerima = txtIDPenerima.Text
                    .Bank = ""
                    .CabangBank = "-"
                    .NomorRekening = "-"
                    .AtasNama = "-"
            End Select


            'Approval
            .Status = ddlKeputusan.SelectedValue
            .Notes = txtNotes.Text

            'Disburse
            '.DariRekening = ucDariRekening.SelectedValue
            .DariRekening = ""
            .OpsiTransfer = ddlOpsiTrf.SelectedValue
            .BiayaTransfer = IIf(ucBiayaTransfer.Text = "", 0, ucBiayaTransfer.Text)
            .BebanBiayaTransfer = ddlBebanBiayaTransfer.SelectedValue

            'Realisasi
            '.Attachment = ucAttachment.FileUploadObj.FileName
            .Attachment = ""
            .Pengembalian = ucJumlahPengembalian.Text
            If UcTanggalRealisasi.Text = "" Then
                .TanggalRealisasi = "1900/01/01"
            Else
                .TanggalRealisasi = ConvertDate2(UcTanggalRealisasi.Text)
            End If


            .LoginId = Me.Loginid

            .SaveMenu = ChoseSaveMenu

        End With

        Return ocustomClass
    End Function

    Private Sub FillCboRekening()

        'With ucDariRekening
        '    .SpName = "spGetBankAccountActive"
        '    .StrConnection = GetConnectionString()
        '    .LoadDataALL()
        'End With

        'With ucBankMaster
        '    .SpName = "spGetBankMasterShortNameActive"
        '    .StrConnection = GetConnectionString()
        '    .LoadDataALL()
        'End With
    End Sub
#Region "Action Save"
    Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
        Dim ocustomClass As Parameter.CashAdvance
        Try
            ocustomClass = oController.SaveCashAdvance(GetInputUser("PengajuanCashAdvance"))
            lblMessage.Style.Item("display") = "inline"
            ShowMessage(lblMessage, "Sukses Save", False)
        Catch ex As Exception
            lblMessage.Style.Item("display") = "inline"
            ShowMessage(lblMessage, "Failed Save", True)
        End Try

        'BindGridInq(Me.CmdWhere, "")
        initialData()

    End Sub

    Private Sub BtnBack_Click(sender As Object, e As EventArgs) Handles BtnBack.Click
        SearchBox.Visible = True
        DatagridPanel.Visible = True
        PanelAddEdit.Visible = False
        DatagridPanel.Visible = False
        hdvNoca.Value = ""
    End Sub
#End Region

#Region "Action Search"
    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        Dim builder As StringBuilder = New StringBuilder
        'builder.Append(String.Concat("", ""))
        'Condition Query Filter TanggalCA
        If S_StartTanggalPengajuan.Text <> "" And S_EndTanggalPengajuan.Text <> "" Then

            builder.Append(String.Concat("TanggalCA", " "))
            builder.Append(String.Concat("Between", " "))
            builder.Append(String.Concat("'", ConvertDate2(S_StartTanggalPengajuan.Text), "'", " And ", "'", ConvertDate2(S_EndTanggalPengajuan.Text), "'"))
            builder.Append(String.Concat(" ", " AND", " "))
        End If

        'Condition Query Filter TanggalDisburse
        If S_StartTanggalDisburse.Text <> "" And S_EndTanggalDisburse.Text <> "" Then

            builder.Append(String.Concat("TanggalDisburse", " "))
            builder.Append(String.Concat("Between", " "))
            builder.Append(String.Concat("'", ConvertDate2(S_StartTanggalDisburse.Text), "'", " And ", "'", ConvertDate2(S_EndTanggalDisburse.Text), "'"))
            builder.Append(String.Concat(" ", " AND", " "))
        End If

        'Condition Query Filter Status
        If ddlStatus.SelectedValue <> "" Then

            builder.Append(String.Concat("Status", " "))
            builder.Append(String.Concat("=", " "))
            builder.Append(String.Concat("'", ddlStatus.SelectedValue, "'"))
            builder.Append(String.Concat(" ", " AND", " "))
        Else
            builder.Append(String.Concat("Status", " "))
            builder.Append(String.Concat("in", " "))
            builder.Append(String.Concat("(", " "))
            builder.Append(String.Concat("", "'N'", ",", "'T'", ",", "'V'"))
            builder.Append(String.Concat(")", " "))
            builder.Append(String.Concat(" ", " AND", " "))
        End If


        'Condition Query Filter Cari Berdasarkan 
        If cboSearch.Text <> "" Then

            If cboSearch.SelectedValue = "Penerima" And txtSearch.Text <> "" Then

                builder.Append(String.Concat("Penerima", " "))
                builder.Append(String.Concat("like", " "))
                builder.Append(String.Concat("'%", txtSearch.Text, "%'"))
                builder.Append(String.Concat(" ", " AND", " "))
            ElseIf cboSearch.SelectedValue = "NoCA" And txtSearch.Text <> "" Then

                builder.Append(String.Concat("NoCA", " "))
                builder.Append(String.Concat("like", " "))
                builder.Append(String.Concat("'%", txtSearch.Text, "%'"))
                builder.Append(String.Concat(" ", " AND", " "))
            End If

        End If

        If builder.Length > 0 Then
            Me.CmdWhere = builder.ToString.Substring(0, builder.Length - 4)
        Else
            Me.CmdWhere = builder.ToString
        End If


        BindGridInq(Me.CmdWhere, "")
        DatagridPanel.Visible = True
        PanelAddEdit.Visible = False
    End Sub

    Private Sub BtnReset_Click(sender As Object, e As EventArgs) Handles BtnReset.Click
        Response.Redirect("PengajuanCashAdvanced.aspx")
    End Sub
#End Region

#Region "Action Grid"
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        'PanelAddEdit.Visible = True

        'hdvNoca.Value = ""
        clearInput()
        DatagridPanel.Visible = False
        PanelSetting()
        'Default Value selected
        CabangTemp = ""
        NoRekeningTemp = ""
        BankTemp = ""
        AtasNamaTemp = ""

        PanelApproval.Visible = False

    End Sub
#End Region

#Region "Clear"
    Private Sub clearInput()

        'Primary key
        hdvNoca.Value = ""

        'INPUT
        TanggalDisburse.Text = ""
        CaraBayar.SelectedValue = CaraBayar.Items.FindByValue("").Value
        Jumlah.Text = 0
        Penggunaan.Text = ""
        NamaPenerima.Text = ""
        txtIDPenerima.Text = ""
        UcBankAccount.City = ""
        UcBankAccount.AccountNo = ""

        'ucBankMaster.SelectedValue = ucBankMaster.Items.FindByValue("").Value
        UcBankAccount.BankName = ""

        'ucDariRekening.SelectedValue = ucDariRekening.Items.FindByValue("").Value

        ddlOpsiTrf.SelectedValue = ddlOpsiTrf.Items.FindByValue("").Value


        ucBiayaTransfer.Text = ""


        UcBankAccount.AccountName = ""

        ddlBebanBiayaTransfer.SelectedValue = ddlBebanBiayaTransfer.Items.FindByValue("").Value

        txtNotes.Text = ""
    End Sub

    Private Sub datagridData_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles datagridData.ItemDataBound
        Select Case e.Item.Cells(7).Text
            Case "B"
                e.Item.Cells(7).Text = "Bank Transfer"
            Case "C"
                e.Item.Cells(7).Text = "Cash"
            Case Else
                e.Item.Cells(7).Text = "Cheque/BG"
        End Select

    End Sub

    Private Sub CaraBayar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CaraBayar.SelectedIndexChanged
        Select Case CaraBayar.SelectedIndex
            Case 0
                'Cabang.Enabled = True
                'NoRekening.Enabled = True
                'ucBankMaster.Enabled = True
                'AtasNama.Enabled = True
                UcBankAccount.Visible = False
            Case 1
                'Cabang.Text = ""
                UcBankAccount.AccountNo = ""

                'ucBankMaster.SelectedValue = ucBankMaster.Items.FindByValue("").Value
                UcBankAccount.BankName = ""
                UcBankAccount.AccountName = ""
                UcBankAccount.Visible = True
                'Cabang.Enabled = True
                'NoRekening.Enabled = True
                'UcBankMaster.Enabled = True
                'AtasNama.Enabled = True
            Case Else
                'Cabang.Enabled = False
                'NoRekening.Enabled = False
                'ucBankMaster.Enabled = False
                'AtasNama.Enabled = False
                UcBankAccount.Visible = False

        End Select

        UcBankAccount.City = CabangTemp
        UcBankAccount.AccountNo = NoRekeningTemp

        'UcBankMaster.SelectedValue = UcBankMaster.Items.FindByValue(BankTemp).Value
        UcBankAccount.BankName = BankTemp
        UcBankAccount.AccountName = AtasNamaTemp

    End Sub
#End Region
End Class