﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="true" CodeBehind="ApprovalCashAdvanced.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.ApprovalCashAdvanced" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucGridNav" Src="../../webform.UserController/ucGridNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankMaster" Src="../../webform.UserController/UcBankMaster.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../webform.UserController/ucBankAccountBranch.ascx" %>
<%@ Register TagPrefix="uc3" TagName="UcListBoxGetter" Src="../../webform.UserController/ObjectMarket/UcListBoxGetter.ascx" %>
<%@ Register TagPrefix="uc3" TagName="UcFileDocumentUploader" Src="../../webform.UserController/ObjectMarket/UcFileDocumentUploader.ascx" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Verifikasi Pengajuan Cash Advanced</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>

    <script type="text/javascript">

        function dialogCancel() {
            if (window.confirm("Are you sure to cancel this item?")) {
                return true;
            } else {
                return false;
            }
        }


        function dialogRequest() {
            if (window.confirm("Do you confirm to request this item?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
            <asp:Panel ID="SearchBox" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h3>Verifikasi Pengajuan Cash Advance</h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <asp:Label ID="S_lblTglPengajuan" runat="server" class="label_general">Tanggal Pengajuan</asp:Label>
                        <uc1:ucDateCE ID="S_StartTanggalPengajuan" runat="server"></uc1:ucDateCE>
                        S/D
                        <uc1:ucDateCE ID="S_EndTanggalPengajuan" runat="server"></uc1:ucDateCE>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <asp:Label ID="S_lblTglDisburse" runat="server" class="label_general">Tanggal Disburse</asp:Label>
                        <uc1:ucDateCE ID="S_StartTanggalDisburse" runat="server"></uc1:ucDateCE>
                        S/D
                        <uc1:ucDateCE ID="S_EndTanggalDisburse" runat="server"></uc1:ucDateCE>
                    </div>
                </div>

                <div class="form_box" style="visibility: hidden">
                    <div class="form_single">
                        <asp:Label ID="lblStatusCahAdvance" runat="server" class="label_general" Visible="false">Status Cah Advance</asp:Label>
                        <asp:DropDownList ID="ddlStatus" runat="server" Visible="false">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="N">New</asp:ListItem>
                            <asp:ListItem Value="T">Return</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <asp:Label ID="lblCariBerdasarkan" runat="server" class="label_general">Cari Berdasarkan</asp:Label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="" Selected>Select one</asp:ListItem>
                            <asp:ListItem Value="Penerima">Name Penerima</asp:ListItem>
                            <asp:ListItem Value="NoCA">Nomor CA</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="25%"></asp:TextBox>
                    </div>
                </div>

                <div class="form_button">
                    <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="small button blue " CausesValidation="false" />
                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="small button red " CausesValidation="false" />
                </div>
            </asp:Panel>
            <asp:Panel ID="DatagridPanel" runat="server">
                <%--<div class="form_box">--%>
                <div class="form_title">
                    <div class="form_single">
                        <h3>Daftar Verifikasi Pengajuan Cash Advance</h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="datagridData" runat="server" AutoGenerateColumns="False" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Request" Visible="false">
                                        <ItemStyle CssClass="short_col" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton Visible="false" ID="btnRequest" runat="server" CausesValidation="False" CommandName="Request" OnClientClick="return dialogRequest();" ImageUrl="../../Images/IconRequest.gif"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle CssClass="short_col" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnAction" runat="server" CausesValidation="False" CommandName="EDIT" ImageUrl="../../Images/iconedit.gif"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Cancel" Visible="false">
                                        <ItemStyle CssClass="short_col" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnActionCancel" runat="server" CausesValidation="False" Text='Cancel' CommandName="Cancel" OnClientClick="return dialogCancel();" ImageUrl="../../Images/IconNegCov.gif"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="NoCA" SortExpression="NoCA" HeaderText="Nomor CA"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TanggalCA" SortExpression="TanggalCA" HeaderText="Tanggal CA" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TanggalDisburse" SortExpression="TanggalDisburse" HeaderText="Tanggal Disburse" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Jumlah" SortExpression="Jumlah" HeaderText="Jumlah" DataFormatString="{0:n0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CaraBayar" SortExpression="CaraBayar" HeaderText="Cara Bayar" visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="JournalBank" SortExpression="JournalBank" HeaderText="Journal Bank" ></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Penerima" SortExpression="Penerima" HeaderText="Penerima"></asp:BoundColumn> 
                                    <asp:BoundColumn DataField="Penggunaan" SortExpression="Penggunaan" HeaderText="Penggunaan"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="Status"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankIdTo" SortExpression="BankIdTo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CabangBank" SortExpression="CabangBank" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NomorRekening" SortExpression="NomorRekening" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AtasNama" SortExpression="AtasNama" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankIdFrom" SortExpression="DariRekening" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OpsiTransfer" SortExpression="OpsiTransfer" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BiayaTransfer" SortExpression="BiayaTransfer" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BebanBiayaTransfer" SortExpression="BebanBiayaTransfer" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NamaFile" SortExpression="NamaFile" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Pengembalian" SortExpression="Pengembalian" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TanggalRealisasi" SortExpression="TanggalRealisasi" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Notes" SortExpression="Notes" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BankIdFromName" SortExpression="BankIdFromName" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="EmployeeID" SortExpression="EmployeeID" HeaderText="EmployeeID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LamaRealisasi" SortExpression="LamaRealisasi" HeaderText="LamaRealisasi" Visible="false"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <uc1:ucGridNav ID="GridNavigator" runat="server" />
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlAplikasiButton" runat="server" Visible="false">
                    <div class="form_button">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button green " />
                    </div>
                </asp:Panel>
                <%--</div>--%>
            </asp:Panel>
            <asp:Panel runat="server" ID="PanelAddEdit">
                <div class="form_title">
                    <div class="form_single">
                        <h3>Verifikasi Pengajuan Cash Advance</h3>
                    </div>
                </div>
                <asp:Panel runat="server" ID="PanelInput">

                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblTanggalDisburse" runat="server" class="label_general">Tanggal Bayar</asp:Label>
                            <uc1:ucDateCE ID="TanggalDisburse" runat="server"></uc1:ucDateCE>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblCaraByar" runat="server" class="label_general">Cara Bayar</asp:Label>
                            <asp:DropDownList ID="CaraBayar" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="">Select One</asp:ListItem>
                                <asp:ListItem Value="B">Bank</asp:ListItem>
                                <asp:ListItem Value="C">Cash</asp:ListItem>
                                <asp:ListItem Value="BG">Cheque/BG</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <asp:Label ID="lblJumlah" runat="server" class="label_general">Jumlah</asp:Label>
                            <uc1:ucNumberFormat ID="Jumlah" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblPenggunaan" runat="server" class="label_general">Penggunaan</asp:Label>
                            <asp:TextBox ID="Penggunaan" runat="server" Width="30%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblNamaPenerima" runat="server" class="label_general">Nama Penerima</asp:Label>
                            <asp:TextBox ID="NamaPenerima" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblBank" runat="server" class="label_general">Bank</asp:Label>
                            <uc3:UcListBoxGetter ID="ucBankMaster" runat="server"></uc3:UcListBoxGetter>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblCabang" runat="server" class="label_general">Cabang</asp:Label>
                            <asp:TextBox ID="Cabang" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblNoRekening" runat="server" class="label_general">No Rekening</asp:Label>
                            <asp:TextBox ID="NoRekening" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblAtasNama" runat="server" class="label_general">Atas Nama</asp:Label>
                            <asp:TextBox ID="AtasNama" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PanelDisburse">
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblRekening" runat="server" class="label_general">Dari Rekening Bank</asp:Label>
                            <uc3:UcListBoxGetter ID="ucDariRekening" runat="server"></uc3:UcListBoxGetter>
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblOpsiTransfer" runat="server" class="label_general">Opsi Transfer</asp:Label>
                            <asp:DropDownList ID="ddlOpsiTrf" runat="server">
                                <asp:ListItem Value="">Select One</asp:ListItem>
                                <asp:ListItem Value="LLG">LLG</asp:ListItem>
                                <asp:ListItem Value="RTGS">RTGS</asp:ListItem>
                                <asp:ListItem Value="INHOUSE">INHOUSE</asp:ListItem>
                                <asp:ListItem Value="INTERNATIONAL">INTERNATIONAL</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_left">
                            <asp:Label ID="lblBiayaTransfer" runat="server" class="label_general">Biaya Transfer</asp:Label>
                            <uc1:ucNumberFormat ID="ucBiayaTransfer" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblBebanBiayaTrf" runat="server" class="label_general">Beban Biaya Transfer</asp:Label>
                            <asp:DropDownList ID="ddlBebanBiayaTransfer" runat="server">
                                <asp:ListItem Value="">Select One</asp:ListItem>
                                <asp:ListItem Value="BK">Beban Kantor</asp:ListItem>
                                <asp:ListItem Value="BP">Beban Penerima</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panelTanggalRealisasi" runat="server">
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblTanggalRealisasi" runat="server" class="label_general">Tanggal Realisasi</asp:Label>
                            <uc1:ucDateCE ID="UcTanggalRealisasi" runat="server" IsRequired="false"></uc1:ucDateCE>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panelLabelAddEdit" runat="server">

                    <%--hidden Value--%>
                    <asp:HiddenField ID="hdvNoca" runat="server" Value="" />

                    <div class="form_box">
                        <div class="form_single">
                            <%--<asp:Label ID="Label1" runat="server" class="label_general">Tanggal Bayar</asp:Label>
                            <asp:Label ID="lblValueTanggalBayar" runat="server" />--%>
                            <asp:Label ID="Label1" runat="server" class="label_general">Tanggal Realisasi</asp:Label>
                            <asp:Label ID="lblTglRealisasi" runat="server" visible="false" />
                            <%--<asp:TextBox ID="txtTglRealisasi" runat="server" disabled="false"></asp:TextBox>--%>
                            <uc1:ucDateCE ID="UcTglRealisasi" runat="server"></uc1:ucDateCE>

                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <%--<asp:Label ID="Label2" runat="server" class="label_general">Cara Bayar</asp:Label>
                            <asp:Label ID="lblValueCaraBayar" runat="server" />--%>

                             <asp:Label ID="Label2" runat="server" class="label_general">Nama Penerima</asp:Label>
                            <asp:TextBox ID="txtNamaPenerima" runat="server" disabled="false"></asp:TextBox>
                            <asp:TextBox ID="txtIDPenerima" runat="server" disabled="false"></asp:TextBox>

                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                        <asp:Label ID="Label3" runat="server" class="label_general">Tanggal Disburse</asp:Label>
                        <asp:TextBox ID="txtTglDisburse" runat="server" disabled="false"></asp:TextBox>
                            </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                        <asp:Label ID="Label4" runat="server" class="label_general">Journal Bank Disburse</asp:Label>
                        <asp:TextBox ID="txtJournalBank" runat="server"  disabled="false"></asp:TextBox>
                            </div>
                    </div>
                     <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="Label5" runat="server" class="label_general">Lama Realisasi</asp:Label>
                            <asp:TextBox ID="txtLamaRealisasi" runat="server"  disabled="false"></asp:TextBox>
                        </div>
                    </div> 

                    <div class="form_box">
                        <div class="form_left">
                            <asp:Label ID="Label6" runat="server" class="label_general">Jumlah</asp:Label>
                            <asp:Label ID="lblValueJumlah" runat="server" visible="false"/>
                            <uc1:ucNumberFormat ID="UcJumlah" runat="server" ></uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="Label7" runat="server" class="label_general">Penggunaan</asp:Label>
                            <asp:Label ID="lblValuePenggunaan" runat="server" visible="false"/>
                            <asp:TextBox ID="txtPenggunaan" runat="server" Width="30%" disabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box" style="display: none">
                        <div class="form_single">
                            <asp:Label ID="Label8" runat="server" class="label_general">Nama Penerima</asp:Label>
                            <asp:Label ID="lblValueNamaPenerima" runat="server" />
                        </div>
                    </div>
                    <div class="form_box" style="display: none">
                        <div class="form_single">
                            <asp:Label ID="Label9" runat="server" class="label_general">Bank</asp:Label>
                            <asp:Label ID="lblValueBank" runat="server" />
                        </div>
                    </div>
                    <div class="form_box" style="display: none">
                        <div class="form_single">
                            <asp:Label ID="Label13" runat="server" class="label_general">Cabang</asp:Label>
                            <asp:Label ID="lblValueCabang" runat="server" />
                        </div>
                    </div>
                    <div class="form_box" style="display: none">
                        <div class="form_single">
                            <asp:Label ID="Label11" runat="server" class="label_general">No Rekening</asp:Label>
                            <asp:Label ID="lblValueNoRekening" runat="server" />
                        </div>
                    </div>
                    <div class="form_box" style="display: none">
                        <div class="form_single">
                            <asp:Label ID="Label12" runat="server" class="label_general">Atas Nama</asp:Label>
                            <asp:Label ID="lblValueAtasNama" runat="server" />
                        </div>
                    </div>

                </asp:Panel>
                <asp:Panel ID="PanelApproval" runat="server">
                    <div class="form_box">
                        <div class="form_single">
                            <asp:Label ID="lblKeputusan" runat="server" class="label_req" Width="225px">Keputusan</asp:Label>
                            <asp:DropDownList ID="ddlKeputusan" runat="server">
                                <asp:ListItem Value="">Select One</asp:ListItem>
                                <asp:ListItem Value="A">Approve</asp:ListItem>
                                <asp:ListItem Value="J">Reject</asp:ListItem>
                                <asp:ListItem Value="V">Revised</asp:ListItem>
                            </asp:DropDownList>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Visible="true"
                                ErrorMessage="Harap Pilih Keputusan" ControlToValidate="ddlKeputusan" Display="Dynamic"
                                InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <asp:Panel ID="PanelApprovalNotes" runat="server" Visible="false">
                        <div class="form_box">
                            <div class="form_single">
                                <asp:Label ID="lblNotes" runat="server" class="label_general">Notes</asp:Label>
                                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Style="margin: 2px; width: 1043px; height: 101px; resize: none;" Rows="2" cols="20"></asp:TextBox>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelAttachment" runat="server">
                    <div class="form_box">
                        <div class="form_left">
                            <asp:Label ID="Label10" runat="server" class="label_general">Lampiran Bukti</asp:Label>
                            <uc3:UcFileDocumentUploader ID="ucAttachment" runat="server" EnableValidator="false"></uc3:UcFileDocumentUploader>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <asp:Label ID="lblPengembalian" runat="server" class="label_general">Jumlah Pengembalian</asp:Label>
                            <uc1:ucNumberFormat ID="ucJumlahPengembalian" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>
                </asp:Panel>

                <div class="form_button">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button green " />
                    <asp:Button ID="BtnBack" runat="server" Text="Back" CssClass="small button red " CausesValidation="false" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlTrans" runat="server" Visible="False">
                        <div class="form_title">
                            <div class="form_single">
                                <h4>DAFTAR DETAIL TRANSAKSI
                                </h4>
                            </div>
                        </div>
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ns">
                                    <asp:DataGrid ID="DtgTransList" runat="server" Width="100%" AutoGenerateColumns="False"
                                        ShowFooter="True" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <FooterStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="PILIH">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:selectAllCheckbox(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="DEPARTMEN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepartmentID" runat="server" Visible="False" Text='<%#Container.DataItem("DepartmentID")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentAllocID" runat="server" Visible="False" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblPaymentAllocDesc" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtKeterangan" Width="60%" CssClass="long_text"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtKeterangan" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                                        ControlToValidate="txtKeterangan" CssClass="validator_general"></asp:RequiredFieldValidator>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="JUMLAH">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtAmountTrans" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);total(this);"
                                                        onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                                        onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign small_text">0</asp:TextBox>
                                                    <asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 999999999999999"
                                                        ControlToValidate="txtAmountTrans" MaximumValue="999999999999999" MinimumValue="0"
                                                        Type="Currency" CssClass="validator_general"></asp:RangeValidator>
                                                    <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                                        ControlToValidate="txtAmountTrans" CssClass="validator_general"></asp:RequiredFieldValidator>
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="right"></FooterStyle>
                                                <FooterTemplate>
                                                    <asp:TextBox runat="server" ID="txtTotTransAmount" CssClass="numberAlign readonly_text small_text" Enabled="false">0</asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <div class="button_gridnavigation">
                                        <asp:Button ID="ButtonDelete" runat="server" CausesValidation="False" Text="Delete"
                                            CssClass="small button gray"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
            <asp:Panel ID="PnlDetail" runat="server" Visible="False">
                        <div class="form_title">
                            <div class="form_single">
                                <h4>DAFTAR DETAIL TRANSAKSI
                                </h4>
                            </div>
                        </div>
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ns">
                                    <asp:DataGrid ID="DataGridDetail" runat="server" Width="100%" AutoGenerateColumns="False"
                                        ShowFooter="True" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <FooterStyle CssClass="item_grid" />
                                        <Columns>  
                                            <asp:TemplateColumn HeaderText="DEPARTMEN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepartmentID" runat="server" Visible="False" Text='<%#Container.DataItem("DepartmentID")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Container.DataItem("DepartmentName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="TRANSAKSI">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentAllocID" runat="server" Visible="False" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblPaymentAllocDesc" runat="server" Text='<%#Container.DataItem("PaymentAllocationName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="KETERANGAN">
                                                <ItemTemplate> 
                                                     <asp:Label ID="lblKeterangan" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblTot" runat="server" Text="TOTAL" Font-Bold="True"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="JUMLAH">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmountTrans" runat="server" Text='<%#FormatNumber(Container.DataItem("Amount"), 2)%>'>
                                                    </asp:Label> 
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="right"></FooterStyle>
                                                <FooterTemplate>
                                                    <%--<asp:TextBox runat="server" ID="txtTotTransAmount" CssClass="numberAlign readonly_text small_text" Enabled="false" >0</asp:TextBox>--%>
                                                    <asp:Label ID="lblTotTransAmount" runat="server" EnableViewState="False">
                                                    </asp:Label> 
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
        </div>
    </form>
</body>
</html>
