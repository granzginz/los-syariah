﻿
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Public Class CrystalReportViewerPage
    Inherits Maxiloan.Webform.WebBased


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents crvKwitansi As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub BindReport()
        Dim oData As New DataSet


        Dim name = CType(Session("RPTName"), String)



        Dim odataStr = String.Format("{0}DATA", name)
        Dim objReport As PmtPembayaranSPPL = New PmtPembayaranSPPL
        oData = CType(Session(odataStr), DataSet)



        objReport.SetDataSource(oData)
        crvKwitansi.ReportSource = objReport
        crvKwitansi.DataBind()

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String = String.Format("{0}{1}{2}", Me.Session.SessionID, Me.Loginid, name)

        'strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        'strFileLocation += Me.Session.SessionID + Me.Loginid + "PmtPembayaranSPPL.pdf"


        DiskOpts.DiskFileName = String.Format("{0}XML\{1}.pdf", Request.ServerVariables("APPL_PHYSICAL_PATH"), strFileLocation)
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()


        Session("RPTName") = Nothing

        Session("PmtPembayaranSPPLDATA") = Nothing
        Response.Redirect("PrintPermintaanPembayaran.aspx?strFileLocation=" & strFileLocation)
    End Sub
End Class