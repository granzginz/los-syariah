﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class PrintPermintaanPembayaran
    Inherits AbsApDisbApp


#Region "Property"
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private recordCount As Int64 = 1
    Public Property PaymentVoucherNo As String
    Public Property GM1 As String
    Public Property GM2 As String
    Public Property GM3 As String
    Public Property Direksi As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        'If Me.IsHoBranch Then
        '    ShowMessage(lblMessage, "Tidak memiliki hak akses modul ini..", True)
        '    Exit Sub
        'End If
        If Not IsPostBack Then
            Me.FormID = "PRINTPBYRAN"
            If Request.QueryString("strFileLocation") <> "" Then
                Response.Write(String.Format("<script language = javascript>{0} var x = screen.width;{0} var y = screen.height;{0} window.open('../../XML/{1}.pdf','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') {0} </script>", vbCrLf, Request.QueryString("strFileLocation")))
            End If

            pnlsearch.Visible = True
            pnlDatagrid.Visible = False
            txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            fillCombo()
        End If
    End Sub


    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        If txtTglPV.Text <> "" Or IsDate(ConvertDate2(txtTglPV.Text)) Then
            APType = cmbAPType.SelectedItem.Value
            'Me.SearchBy = String.Format("pv.pvDueDate ='{0}'  and pv.apBranchID = '{1}' and pv.APType='{2}' ", ConvertDate2(txtTglPV.Text), oBranch.BranchID, cmbAPType.SelectedItem.Value)
            'Me.SearchBy = String.Format("pv.pvDueDate <='{0}'  and pv.apBranchID = '{1}' and pv.APType='{2}' and pv.PVStatus <> 'D' ", ConvertDate2(txtTglPV.Text), oBranch.BranchID, cmbAPType.SelectedItem.Value)
            Me.SearchBy = String.Format("pv.pvDueDate between dateadd(mm, -2,dateadd(mm, datediff(mm,0,'{0}'), 0))   and  '{0}'   and pv.apBranchID = '{1}' and pv.APType='{2}' and pv.PVStatus <> 'D' ", ConvertDate2(txtTglPV.Text), oBranch.BranchID, cmbAPType.SelectedItem.Value)
            DoBind()
            FillCbo(author1, 1)
            FillCbo(author2, 2)
            FillCbo(author3, 3)
            FillCbo(author4, 4)
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Rencana Bayar", True)
        End If
    End Sub
    Public Function GetEmployee(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 30)
        params(0).Value = CustomClass.BranchId
        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 30)
        params(1).Value = CustomClass.EmployeePosition


        Try
            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Protected Sub FillCbo(ByVal cboName As DropDownList, ByVal EmployeePosition As String)
        Dim customClass As New Parameter.APDisbApp
        With customClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .EmployeePosition = EmployeePosition
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = ""
            .SpName = "spCboGetEmployee"
        End With
        customClass = GetEmployee(customClass)
        Dim dt As DataTable
        dt = customClass.listDataReport
        With cboName
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
        'With author2
        '    .DataTextField = "EmployeeName"
        '    .DataValueField = "EmployeeID"
        '    .DataSource = dt
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = ""
        '    .SelectedIndex = 0
        'End With
        'With author3
        '    .DataTextField = "EmployeeName"
        '    .DataValueField = "EmployeeID"
        '    .DataSource = dt
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = ""
        '    .SelectedIndex = 0
        'End With
        'With author4
        '    .DataTextField = "EmployeeName"
        '    .DataValueField = "EmployeeID"
        '    .DataSource = dt
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = ""
        '    .SelectedIndex = 0
        'End With
    End Sub
#Region "BindGridEntity"
    Sub BindGridEntity()
        Dim dtEntity As New DataTable
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer = 0
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim custom As New Parameter.APDisbApp

        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.APDisbApp
                custom.PaymentVoucherNo = TempDataTable.Rows(index).Item("lnkPVNo").ToString.Trim
                custom.GM1 = TempDataTable.Rows(index).Item("signer1").ToString.Trim
                custom.GM2 = TempDataTable.Rows(index).Item("signer2").ToString.Trim
                custom.GM3 = TempDataTable.Rows(index).Item("signer3").ToString.Trim
                custom.Direksi = TempDataTable.Rows(index).Item("signer4").ToString.Trim
                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            '.CurrentPage = currentPage
            '.PageSize = pageSize
            '.SortBy = Me.Sort
        End With

        oCustomClass = oController.UpdateMultiAPDisbApp2(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listDataReport
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtgEntity.DataSource = dtEntity.DefaultView
        dtgEntity.CurrentPageIndex = 0
        dtgEntity.DataBind()
        'PagingFooter()

        pnlDatagrid.Visible = True
        pnlsearch.Visible = True

        For intLoopGrid = 0 To dtgEntity.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgEntity.Items(intLoopGrid).FindControl("lblxx"), CheckBox)
            Dim PaymentVoucherNo As String = CType(dtgEntity.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim
            GM1 = author1.SelectedValue.Trim
            GM2 = author2.SelectedValue.Trim
            GM3 = author3.SelectedValue.Trim
            Direksi = author4.SelectedValue.Trim

            Me.PaymentVoucherNo = PaymentVoucherNo
            Me.GM1 = GM1
            Me.GM2 = GM2
            Me.GM3 = GM3
            Me.Direksi = Direksi

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub

    Private Function PredicateFunction(obj As Parameter.APDisbApp) As Boolean
        Return obj.ApplicationID = obj.PaymentVoucherNo = Me.PaymentVoucherNo
    End Function
#End Region

    Function createDataTable() As DataTable

        Dim dt As DataTable = New DataTable

        dt.Columns.Add("NoKontrak", GetType(String))
        dt.Columns.Add("AtasNama", GetType(String))
        dt.Columns.Add("AssetName", GetType(String))
        dt.Columns.Add("BayarKepada", GetType(String))
        dt.Columns.Add("Bank", GetType(String))
        dt.Columns.Add("NoRekening", GetType(String))
        dt.Columns.Add("Jumlah", GetType(Decimal))


        Return dt

    End Function

    Sub BindDataPPK()
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim Application As New Parameter.APDisbApp
        Dim chck As Integer = 0
        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
                .Columns.Add(New DataColumn("GM1", GetType(String)))
                .Columns.Add(New DataColumn("GM2", GetType(String)))
                .Columns.Add(New DataColumn("GM3", GetType(String)))
                .Columns.Add(New DataColumn("Direksi", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                Application = New Parameter.APDisbApp

                Application.PaymentVoucherNo = TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim
                Application.GM1 = TempDataTable.Rows(index).Item("GM1").ToString.Trim
                Application.GM2 = TempDataTable.Rows(index).Item("GM2").ToString.Trim
                Application.GM3 = TempDataTable.Rows(index).Item("GM3").ToString.Trim
                Application.Direksi = TempDataTable.Rows(index).Item("Direksi").ToString.Trim

                DataList.Add(Application)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgEntity.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgEntity.Items(intLoopGrid).FindControl("ItemCheckBox"), CheckBox)
            Dim PaymentVoucherNo As String = CType(dtgEntity.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim
            GM1 = author1.SelectedValue.Trim
            GM2 = author2.SelectedValue.Trim
            GM3 = author3.SelectedValue.Trim
            Direksi = author4.SelectedValue.Trim
            chck += 1


            Me.PaymentVoucherNo = PaymentVoucherNo
            Me.GM1 = GM1
            Me.GM2 = GM2
            Me.GM3 = GM3
            Me.Direksi = Direksi

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If
            If (chck <= 0) Then
                ShowMessage(lblMessage, "Silahkan Pilih Voucher", True)
                Exit Sub
            End If

            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("PaymentVoucherNo") = PaymentVoucherNo
                oRow("GM1") = GM1
                oRow("GM2") = GM2
                oRow("GM3") = GM3
                oRow("Direksi") = Direksi
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPPK()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
            .Columns.Add(New DataColumn("GM1", GetType(String)))
            .Columns.Add(New DataColumn("GM2", GetType(String)))
            .Columns.Add(New DataColumn("GM3", GetType(String)))
            .Columns.Add(New DataColumn("Direksi", GetType(String)))
        End With
    End Sub
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer = 0
            Dim hasil As Integer = 0
            Dim cmdwhere As String = ""

            If TempDataTable Is Nothing Then
                BindDataPPK()
            Else
                If TempDataTable.Rows.Count = 0 Then
                    BindDataPPK()
                End If
            End If

            With oDataTable
                .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
                .Columns.Add(New DataColumn("GM1", GetType(String)))
                .Columns.Add(New DataColumn("GM2", GetType(String)))
                .Columns.Add(New DataColumn("GM3", GetType(String)))
                .Columns.Add(New DataColumn("Direksi", GetType(String)))
            End With

            If TempDataTable.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Harap periksa item", True)
                Exit Sub
            End If

            For index = 0 To TempDataTable.Rows.Count - 1
                oRow = oDataTable.NewRow
                oRow("PaymentVoucherNo") = TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim
                oRow("GM1") = TempDataTable.Rows(index).Item("GM1").ToString.Trim
                oRow("GM2") = TempDataTable.Rows(index).Item("GM2").ToString.Trim
                oRow("GM3") = TempDataTable.Rows(index).Item("GM3").ToString.Trim
                oRow("Direksi") = TempDataTable.Rows(index).Item("Direksi").ToString.Trim
                oDataTable.Rows.Add(oRow)
                '    If cmdwhere = "" Then
                '        cmdwhere = "PaymentVoucher.PaymentVoucherNo='" & CType(oRow("PaymentVoucherNo"), String) & "'"
                '    Else
                '        cmdwhere += " or PaymentVoucher.PaymentVoucherNo='" & CType(oRow("PaymentVoucherNo"), String) & "' "
                '    End If
            Next

            Me.PaymentVoucherNo = TempDataTable.Rows(0).Item("PaymentVoucherNo").ToString.Trim
            Me.GM1 = TempDataTable.Rows(0).Item("GM1").ToString.Trim
            Me.GM2 = TempDataTable.Rows(0).Item("GM2").ToString.Trim
            Me.GM3 = TempDataTable.Rows(0).Item("GM3").ToString.Trim
            Me.Direksi = TempDataTable.Rows(0).Item("Direksi").ToString.Trim

            With oCustomClass
                .strConnection = GetConnectionString()
                .ListAPAppr = oDataTable
            End With
            oCustomClass = oController.UpdateMultiAPDisbApp2(oCustomClass)

            'hasil = oCustomClass.hasil
            'If hasil = 0 Then
            '    ShowMessage(lblMessage, "Gagal", True)
            '    Exit Sub
            'Else
            'cmdwhere = "(" & cmdwhere & ") and Agreement.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
            Dim cookie As HttpCookie = Request.Cookies("RptPmtPembayaranSPPL")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("BusinessDate") = Me.BusinessDate.ToString("dd/MM/yyyy")
                cookie.Values("PaymentVoucherNo") = Me.PaymentVoucherNo
                cookie.Values("GM1") = Me.GM1
                cookie.Values("GM2") = Me.GM2
                cookie.Values("GM3") = Me.GM3
                cookie.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptPmtPembayaranSPPL")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("BusinessDate", Me.BusinessDate.ToString("dd/MM/yyyy"))
                cookieNew.Values.Add("PaymentVoucherNo", Me.PaymentVoucherNo)
                cookieNew.Values("GM1") = Me.GM1
                cookieNew.Values("GM2") = Me.GM2
                cookieNew.Values("GM3") = Me.GM3
                cookieNew.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("PmtPembayaranSPPLViewer.aspx")
        End If
        'End If


        ''If APType <> "SPPL" Then
        ''    ShowMessage(lblMessage, "Sementara Pilihan Tersedia PO supplier.", True)
        ''    Return
        ''End If

        ''DataSet dan Tabel buat Crystal report
        'Dim oData = New DataSet
        'Dim tbl = createDataTable()
        ''Dim objReport As PmtPembayaranSPPL = New PmtPembayaranSPPL
        'Dim chck As Integer = 0
        'For i = 0 To dtgEntity.Items.Count - 1
        '    Dim chkDtList = CType(dtgEntity.Items(i).FindControl("ItemCheckBox"), CheckBox)
        '    If Not IsNothing(chkDtList) Then
        '        If chkDtList.Checked Then
        '            chck += 1
        '            Dim pvno = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink).Text.Trim
        '            Dim acPay = CType(dtgEntity.Items(i).FindControl("lnkAPDeta"), Label).Text.Trim
        '            Dim add = tbl.NewRow()

        '            Dim row As DataRow = (From column In dt2.Rows Where column("AccountPayableNo").ToString().Trim = acPay).FirstOrDefault()

        '            'add("NoKontrak") = IIf(row Is Nothing, "", row("AccountPayableNo")) 'CType(dtgEntity.Items(i).FindControl("lnkAPDeta"), Label).Text
        '            'by Wira 20160302
        '            Dim s As String = row("AgreementNoCustomer")
        '            Dim charRange As Char = "-"
        '            Dim startIndex As Integer = 0
        '            Dim endIndex As Integer = s.IndexOf(charRange)
        '            Dim length = endIndex - startIndex

        '            'add("NoKontrak") = IIf(row Is Nothing, "", s.Substring(startIndex, length))
        '            'add("AtasNama") = IIf(row Is Nothing, "", row("CustomerName")) 'CType(dtgEntity.Items(i).FindControl("lblCustName"), Label).Text
        '            'add("AssetName") = IIf(row Is Nothing, "", row("AssetName")) ' CType(dtgEntity.Items(i).FindControl("lblAssetName"), Label).Text
        '            'add("BayarKepada") = IIf(row Is Nothing, "", row("ApTo")) 'CType(dtgEntity.Items(i).FindControl("lblBenef"), Label).Text
        '            'add("Bank") = IIf(row Is Nothing, "", row("BankNameTo")) 'dtgEntity.Items(i).Cells(6).Text
        '            'add("NoRekening") = IIf(row Is Nothing, "", row("BankAccountTo")) ',dtgEntity.Items(i).Cells(7).Text
        '            'add("Jumlah") = IIf(row Is Nothing, 0, row("DetailAmount"))  'CDec(CType(dtgEntity.Items(i).FindControl("lblPVAmount"), Label).Text)

        '            tbl.Rows.Add(add)

        '        End If
        '    End If
        'Next
        'If (chck <= 0) Then
        '    ShowMessage(lblMessage, "Silahkan Pilih Voucher", True)
        '    Exit Sub
        'End If


        'Dim result = oController.UpdateMultiAPDisbApp2(oCustomClass)

        'If (result <> "") Then
        '    ShowMessage(lblMessage, result, True)
        '    Exit Sub
        'End If

        'oData.Tables.Add(tbl)

        'Session("RPTName") = "PmtPembayaranSPPL" 
        'Session("PmtPembayaranSPPLDATA") = oData
        'Response.Redirect("PmtPembayaranSPPLViewer.aspx")


    End Sub


End Class