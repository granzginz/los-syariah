﻿Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController


Public Class APGroup
    Inherits Maxiloan.Webform.WebBased

#Region "Properties"
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Private Property APNo() As String
        Get
            Return CType(ViewState("APNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("APNo") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property TotalUserControl() As Integer
        Get
            Return CType(ViewState("TotalUserControl"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotalUserControl") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property IsOnlySKN() As Integer
        Get
            Return CType(ViewState("IsOnlySKN"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("IsOnlySKN") = Value
        End Set
    End Property
    Private Property IsOnlyInHouse() As Integer
        Get
            Return CType(ViewState("IsOnlyInHouse"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("IsOnlyInHouse") = Value
        End Set
    End Property
    Private Property IsSKNRTGS() As Integer
        Get
            Return CType(ViewState("IsSKNRTGS"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("IsSKNRTGS") = Value
        End Set
    End Property
    Private Property EBankAccountID() As String
        Get
            Return CType(ViewState("EBankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EBankAccountID") = Value
        End Set
    End Property
    Private Property EBankID() As String
        Get
            Return CType(ViewState("EBankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EBankID") = Value
        End Set
    End Property
    Private Property RequestBankID() As String
        Get
            Return CType(ViewState("RequestBankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RequestBankID") = Value
        End Set
    End Property
    Private Property TarifSKN() As Double
        Get
            Return CType(ViewState("TarifSKN"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TarifSKN") = Value
        End Set
    End Property
    Private Property TarifRTGS() As Double
        Get
            Return CType(ViewState("TarifRTGS"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TarifRTGS") = Value
        End Set
    End Property
    Private Property LimitSKNAmount() As Double
        Get
            Return CType(ViewState("LimitSKNAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("LimitSKNAmount") = Value
        End Set
    End Property

    Private Property TotalPencairan() As Double
        Get
            Return CType(ViewState("TotalPencairan"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalPencairan") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private Const SCHEME_ID As String = "INV"
    Private time As String
    Protected WithEvents txtBiayaTransfer As ucNumberFormat
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        Dim i As Int32
        Dim LBlnCreate As Boolean
        Dim LBlnLast As Boolean = False
        Dim StrAppID As String = ""
        Dim oAPDisbSelec As New APDisbSelec
        Dim oUserControl As ucAPGroup
        Dim oController As New APDisbSelecController
        Dim oCustomClass As New Parameter.APDisbSelec
        Dim j As Integer

        Dim LStrAPTo As New ArrayList

        Dim chkDtList As New CheckBox
        Dim lnkAPDetail As New HyperLink

        Dim cboAPType As DropDownList
        Dim oBranch As DropDownList
        Dim dtgAP As New DataGrid
        Dim oDueDate As ValidDate
        Dim oInvoiceDate As ValidDate
        Dim DtAPTable As New DataTable
        Dim StrAPNoList As String = ""
        Dim StrAPTo As String = ""
        Dim DtDuedate As DateTime
        Dim lblMaskAssID As Label
        Dim lblInsBranchID As Label
        Dim lblSupplierId As Label
        Dim HyAccount As HyperLink
        Dim lblapplicationId As Label

        'Modify by WIra 20171024
        If Request("ActivityDateStart") = Nothing Then
            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateStart = Me.BusinessDate + " " + time
        Else
            Me.ActivityDateStart = Request("ActivityDateStart")
        End If
        TotalPencairan = 0

        Try
            If Not IsPostBack Then
                bindLokasiBayar()
                'BindWayOfPayment()
                'BindBankAccount()

                Me.Mode = Request.QueryString("Mode")

                If Me.Mode = "Success" Then
                    ShowMessage(lblMessage, "AP Approval have been saved successfully!", False)
                End If

                oAPDisbSelec = CType(Context.Handler, APDisbSelec)
                dtgAP = CType(oAPDisbSelec.FindControl("dtgAP"), DataGrid)
                cboAPType = CType(oAPDisbSelec.FindControl("cmbAPType"), DropDownList)
                oDueDate = CType(oAPDisbSelec.FindControl("oDueDate"), ValidDate)
                oInvoiceDate = CType(oAPDisbSelec.FindControl("oInvoice"), ValidDate)
                oBranch = CType(oAPDisbSelec.FindControl("oBranch"), DropDownList)
                lblAPType.Text = cboAPType.SelectedItem.Text.Trim
                lblApBranch.Text = oBranch.SelectedItem.Text.Trim
                txtAPType.Text = cboAPType.SelectedValue

                If cboAPType.SelectedValue = "SPPL" Then
                    ddlLokasiBayar.Enabled = False
                End If

                For i = 0 To dtgAP.Items.Count - 1
                    chkDtList = New CheckBox

                    chkDtList = CType(dtgAP.Items(i).FindControl("chkItem"), CheckBox)
                    lnkAPDetail = CType(dtgAP.Items(i).FindControl("lnkAPDetail"), HyperLink)
                    If Not IsNothing(chkDtList) Then
                        If chkDtList.Checked Then
                            StrAPNoList &= "'" & lnkAPDetail.Text & "',"
                        End If
                    End If
                Next

                If StrAPNoList <> "" Then
                    StrAPNoList = Left(StrAPNoList, Len(StrAPNoList) - 1)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .AccountPayableNo = StrAPNoList
                    End With
                    DtAPTable = oController.APSelectionList(oCustomClass)

                    StrAPNoList = ""
                    Me.TotalUserControl = 0
                    j = 0

                    If DtAPTable.Rows.Count > 0 Then
                        With DtAPTable
                            StrAPTo = CStr(IIf(IsDBNull(.Rows(0)("apto")), "", CStr(.Rows(0)("apto")).Trim))
                            DtDuedate = CDate(.Rows(0)("DueDate"))
                            StrAPNoList += "'" & CStr(.Rows(0)("accountpayableno")).Trim & "',"
                            If DtAPTable.Rows.Count > 1 Then
                                LBlnCreate = False
                                For i = 1 To DtAPTable.Rows.Count - 1

                                    LBlnCreate = False
                                    If StrAPTo = CStr(.Rows(i)("apto")).Trim Then
                                        'If DateDiff(DateInterval.Day, DtDuedate, CDate(.Rows(i)("DueDate"))) = 0 Then
                                        '    LBlnCreate = False
                                        'Else
                                        '    LBlnCreate = True
                                        'End If
                                    Else
                                        LBlnCreate = True
                                    End If
                                    HyAccount = CType(dtgAP.Items(i).FindControl("lnkAPDetail"), HyperLink)
                                    lblSupplierId = CType(dtgAP.Items(i).FindControl("lblSupplierID"), Label)
                                    lblInsBranchID = CType(dtgAP.Items(i).FindControl("lblInsBranchID"), Label)
                                    lblMaskAssID = CType(dtgAP.Items(i).FindControl("lblMaskAssID"), Label)
                                    lblapplicationId = CType(dtgAP.Items(i).FindControl("lblApplicationID"), Label)

                                    ' LBLN Create Adalah Apakah Akan di buat Payment Vouchernya
                                    If LBlnCreate Then
                                        oUserControl = CType(LoadControl("UcApGroup.ascx"), ucAPGroup)
                                        oUserControl.ID = "usc" & CStr(j)
                                        If Not IsPostBack Then
                                            oUserControl.APTo = StrAPTo
                                            oUserControl.DueDate = DtDuedate
                                            oUserControl.AccountPayableNoList = CStr(IIf(StrAPNoList <> "", Left(StrAPNoList, Len(StrAPNoList) - 1), ""))
                                            oUserControl.IsAPRequest = True
                                            oUserControl.APType = cboAPType.SelectedItem.Value.Trim
                                            oUserControl.InsBranchId = lblInsBranchID.Text.Trim
                                            oUserControl.InsCoyId = lblMaskAssID.Text.Trim
                                            oUserControl.ApplicationID = lblapplicationId.Text.Trim
                                            oUserControl.SupplierId = lblSupplierId.Text.Trim
                                            oUserControl.Account = HyAccount.Text.Trim


                                        End If
                                        j += 1
                                        pnlAPGroup.Controls.Add(oUserControl)
                                        oUserControl.doDataBinding()
                                        TotalPencairan = TotalPencairan + oUserControl.TotalAPBalance
                                        RequestBankID = oUserControl.VoucherBankID
                                        StrAPTo = CStr(.Rows(i)("apto")).Trim
                                        DtDuedate = CDate(.Rows(i)("DueDate"))
                                        StrAPNoList = "'" & CStr(.Rows(i)("accountpayableno")).Trim & "',"
                                    Else
                                        StrAPNoList += "'" & CStr(.Rows(i)("accountpayableno")).Trim & "',"
                                    End If

                                    If i = DtAPTable.Rows.Count - 1 Then
                                        oUserControl = CType(LoadControl("UcApGroup.ascx"), ucAPGroup)
                                        oUserControl.ID = "usc" & CStr(j)
                                        If Not IsPostBack Then
                                            oUserControl.APTo = StrAPTo
                                            oUserControl.DueDate = DtDuedate
                                            oUserControl.AccountPayableNoList = CStr(IIf(StrAPNoList <> "", Left(StrAPNoList, Len(StrAPNoList) - 1), ""))
                                            oUserControl.IsAPRequest = True
                                            oUserControl.APType = cboAPType.SelectedItem.Value.Trim
                                            oUserControl.InsBranchId = lblInsBranchID.Text.Trim
                                            oUserControl.InsCoyId = lblMaskAssID.Text.Trim
                                            oUserControl.ApplicationID = lblapplicationId.Text.Trim
                                            oUserControl.SupplierId = lblSupplierId.Text.Trim
                                            oUserControl.Account = HyAccount.Text.Trim
                                        End If
                                        j += 1
                                        pnlAPGroup.Controls.Add(oUserControl)
                                        oUserControl.doDataBinding()
                                        TotalPencairan = TotalPencairan + oUserControl.TotalAPBalance
                                        RequestBankID = oUserControl.VoucherBankID
                                    End If
                                Next
                            Else
                                HyAccount = CType(dtgAP.Items(0).FindControl("lnkAPDetail"), HyperLink)
                                lblSupplierId = CType(dtgAP.Items(0).FindControl("lblSupplierID"), Label)
                                lblInsBranchID = CType(dtgAP.Items(0).FindControl("lblInsBranchID"), Label)
                                lblMaskAssID = CType(dtgAP.Items(0).FindControl("lblMaskAssID"), Label)
                                lblapplicationId = CType(dtgAP.Items(0).FindControl("lblApplicationID"), Label)
                                oUserControl = CType(LoadControl("UcApGroup.ascx"), ucAPGroup)
                                oUserControl.ID = "usc" & CStr(j)
                                If Not IsPostBack Then
                                    oUserControl.APTo = StrAPTo
                                    oUserControl.DueDate = DtDuedate
                                    oUserControl.AccountPayableNoList = CStr(IIf(StrAPNoList <> "", Left(StrAPNoList, Len(StrAPNoList) - 1), ""))
                                    oUserControl.IsAPRequest = True
                                    oUserControl.APType = cboAPType.SelectedItem.Value.Trim
                                    oUserControl.InsBranchId = lblInsBranchID.Text.Trim
                                    oUserControl.InsCoyId = lblMaskAssID.Text.Trim
                                    oUserControl.ApplicationID = lblapplicationId.Text.Trim
                                    oUserControl.SupplierId = lblSupplierId.Text.Trim
                                    oUserControl.Account = HyAccount.Text.Trim
                                End If
                                j += 1
                                pnlAPGroup.Controls.Add(oUserControl)
                                oUserControl.doDataBinding()
                                TotalPencairan = TotalPencairan + oUserControl.TotalAPBalance
                                RequestBankID = oUserControl.VoucherBankID
                            End If
                        End With
                        Me.TotalUserControl = j
                    End If
                End If
                BindWayOfPayment()
            Else
                If Me.TotalUserControl >= 0 Then
                    For i = 0 To Me.TotalUserControl - 1
                        oUserControl = CType(LoadControl("UcApGroup.ascx"), ucAPGroup)
                        oUserControl.ID = "usc" & CStr(i)
                        pnlAPGroup.Controls.Add(oUserControl)
                        TotalPencairan = TotalPencairan + oUserControl.TotalAPBalance
                        RequestBankID = oUserControl.VoucherBankID
                    Next
                End If
            End If
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
#End Region

#Region "Cancel Process"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        'Response.Redirect("APDisbSelec.aspx")
        Server.Transfer("APDisbSelec.aspx", True)
    End Sub
#End Region

    Sub bindLokasiBayar()
        If IsHoBranch Then
            With ddlLokasiBayar
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "Pusat")
                .Items(1).Value = "P"
            End With
        Else
            If Request("aptype") = "SPPL" Then
                With ddlLokasiBayar
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "Pusat")
                    .Items(1).Value = "P"
                End With
            Else
                With ddlLokasiBayar
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "Pusat")
                    .Items(1).Value = "P"
                    .Items.Insert(2, "Cabang")
                    .Items(2).Value = "C"
                End With
            End If
        End If
        ddlLokasiBayar.SelectedValue = "P"
    End Sub

#Region "Bind WayOfPayment"
    Private Sub BindWayOfPayment()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""


        If IsHoBranch Then
            If ddlLokasiBayar.SelectedValue = "P" Then
                strListData = "CA,Cash-BA,Bank-GT,EBanking"
            Else
                strListData = "CA,Cash-BA,Bank"
            End If

        Else

            If Request("aptype") = "SPPL" And ddlLokasiBayar.SelectedValue = "P" Then
                strListData = "GT,EBanking"
            ElseIf ddlLokasiBayar.SelectedValue = "P" Then
                strListData = "CA,Cash-BA,Bank-GT,EBanking"
            Else
                strListData = "CA,Cash-BA,Bank"
            End If

        End If

        splitListData = Split(strListData, "-")


        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        With cboWop
            .Items.Clear()
            .DataValueField = "ID"
            .DataTextField = "Description"
            .DataSource = oDataTable
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

        If ddlLokasiBayar.SelectedValue = "P" Then
            cboWop.SelectedIndex = cboWop.Items.IndexOf(cboWop.Items.FindByText("EBanking"))
            cboWop_SelectedIndexChanged()
        End If
        
        oDataTable.Dispose()
        'divBankAcc.Visible = False
        'divJenisTransfer.Visible = False
    End Sub
#End Region

    Private Sub BindBankAccount()
        If IsHoBranch Then
            If ddlLokasiBayar.SelectedValue = "P" Then
                Dim oController As New DataUserControlController

                With cmbBankAccount
                    .Items.Clear()
                    '.DataSource = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, IIf(cboWop.SelectedValue = "CA", "C", "B"), "")
                    .DataSource = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, IIf(cboWop.SelectedValue = "BA", "EC", ""), "")
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
            End If
        Else
            If ddlLokasiBayar.SelectedValue = "C" Then
                Dim oController As New DataUserControlController

                With cmbBankAccount
                    .Items.Clear()
                    '.DataSource = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, IIf(cboWop.SelectedValue = "CA", "C", "B"), "")
                    .DataSource = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, IIf(cboWop.SelectedValue = "BA", "EC", ""), "")
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
            Else
                With cmbBankAccount
                    .Items.Clear()
                End With
            End If
        End If
    End Sub 

    Public Function WOPonChange() As String

        Return "WOPChange('" & Trim(cboWop.ClientID) & "');"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32
        Dim k As Int32
        k = 0
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 1 To 2
            Select Case cboWop.Items(j).Value
                Case "BA"
                    DataRow = DtTable.Select(" Type = 'B'")
                Case "CA"
                    DataRow = DtTable.Select(" Type = 'C'")
                Case "GT"
                    DataRow = DtTable.Select(" IsGenerateTextAvailable = 1")
            End Select

            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If cboWop.Items(j).Value = "GT" Then
                        k += 1
                        If k = 1 Then
                            If CBool(DataRow(i)("IsGenerateTextAvailable")) Then
                                strScript &= "new Array(" & vbCrLf
                                strScript1 = ""
                            End If
                        End If
                    Else
                        If strType <> CStr(DataRow(i)("Type")).Trim Then
                            strType = CStr(DataRow(i)("Type")).Trim
                            strScript &= "new Array(" & vbCrLf
                            strScript1 = ""
                        End If
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ID")), "-", DataRow(i)("ID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ID")), "-", DataRow(i)("ID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

#Region "Request Process"
    Private Sub imbRequest_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonRequest.Click
        txtWOP.Text = cboWop.SelectedValue.Trim
        txtLokasiBayar.Text = ddlLokasiBayar.SelectedValue.Trim

        Server.Transfer("APDisbSelec.aspx?mode=save&totalgroup=" & CStr(Me.TotalUserControl), False)
    End Sub
#End Region

    Private Sub imgSaveApproval_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSaveApproval.Click
        txtWOP.Text = cboWop.SelectedValue.Trim
        Server.Transfer("APDisbSelec.aspx?mode=save&totalgroup=" & CStr(Me.TotalUserControl) & _
        "&Rekomendasi=" & txtRecommendation.Text.Trim & "&Argumentasi=" & txtArgumentasi.Text.Trim & "&ApprovedBy=" & cboApprovedBy.SelectedValue.Trim, False)
    End Sub

    Private Sub imgCancelApproval_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancelApproval.Click
        Response.Redirect("APDisbSelec.Aspx")
    End Sub

    Private Sub ddlLokasiBayar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLokasiBayar.SelectedIndexChanged
        BindWayOfPayment()
    End Sub

    Private Overloads Sub cboWop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboWop.SelectedIndexChanged
        'Todo Bind banka account berdasarkan wop yang dipilih

        If cboWop.SelectedValue = "BA" Then
            'BindBankAccount()
            'cmbBankAccount.Visible = True
            cmbBankAccount.Visible = False
            'rfvcmbBankAccount.Enabled = True
            rfvcmbBankAccount.Enabled = False
            divJenisTransfer.Visible = False
            divBankAcc.Visible = True

            divBiayaTransfer.Visible = False
            divbebanBiayaTransfer.Visible = False
        ElseIf cboWop.SelectedValue = "CA" Then
            'BindBankAccount()
            'cmbBankAccount.Visible = True
            'rfvcmbBankAccount.Enabled = True
            rfvcmbBankAccount.Enabled = False
            divJenisTransfer.Visible = False
            divBankAcc.Visible = True

            divBiayaTransfer.Visible = False
            divbebanBiayaTransfer.Visible = False
        ElseIf cboWop.SelectedValue = "GT" Then
            'BindBankAccount()
            cmbBankAccount.Visible = False
            rfvcmbBankAccount.Enabled = False
            divJenisTransfer.Visible = True
            divBankAcc.Visible = False

            'divBiayaTransfer.Visible = True
            divBiayaTransfer.Visible = False
            'divbebanBiayaTransfer.Visible = True
            divbebanBiayaTransfer.Visible = False

            GetEBankingAccount()
        Else
            cmbBankAccount.Visible = False
            rfvcmbBankAccount.Enabled = False
            divJenisTransfer.Visible = False
            divBankAcc.Visible = False

            divBiayaTransfer.Visible = False
            divbebanBiayaTransfer.Visible = False
        End If
    End Sub
    Private Overloads Sub cboWop_SelectedIndexChanged()
        'Todo Bind banka account berdasarkan wop yang dipilih

        If cboWop.SelectedValue = "BA" Then
            BindBankAccount()
            cmbBankAccount.Visible = True
            rfvcmbBankAccount.Enabled = True
            divJenisTransfer.Visible = False
            divBankAcc.Visible = True

            divBiayaTransfer.Visible = False
            divbebanBiayaTransfer.Visible = False
        ElseIf cboWop.SelectedValue = "CA" Then
            BindBankAccount()
            cmbBankAccount.Visible = True
            rfvcmbBankAccount.Enabled = True
            divJenisTransfer.Visible = False
            divBankAcc.Visible = True

            divBiayaTransfer.Visible = False
            divbebanBiayaTransfer.Visible = False
        ElseIf cboWop.SelectedValue = "GT" Then
            'BindBankAccount()
            cmbBankAccount.Visible = False
            rfvcmbBankAccount.Enabled = False
            divJenisTransfer.Visible = True
            divBankAcc.Visible = False

            'divBiayaTransfer.Visible = True
            divBiayaTransfer.Visible = False
            'divbebanBiayaTransfer.Visible = True
            divbebanBiayaTransfer.Visible = False

            GetEBankingAccount()
        Else
            cmbBankAccount.Visible = False
            rfvcmbBankAccount.Enabled = False
            divJenisTransfer.Visible = False
            divBankAcc.Visible = False

            divBiayaTransfer.Visible = False
            divbebanBiayaTransfer.Visible = False
        End If
    End Sub
    'Modify by Wira 20180212 untuk mendapatakan parameter Ebanking
    Sub GetEBankingAccount()
        Dim dCustomClass As New Parameter.APDisbSelec
        Dim dController As New APDisbSelecController
        Dim DtAPTable As New DataTable

        Try

            With dCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = " IsEbanking = 1 "
            End With

            DtAPTable = dController.EBankList(dCustomClass)

            Me.EBankID = CStr(DtAPTable.Rows(0).Item("BankID")).Trim
            Me.EBankAccountID = CStr(DtAPTable.Rows(0).Item("BankAccountID")).Trim
            Me.TarifSKN = CStr(DtAPTable.Rows(0).Item("TarifSKN")).Trim
            Me.TarifRTGS = CStr(DtAPTable.Rows(0).Item("TarifRTGS")).Trim
            Me.LimitSKNAmount = CStr(DtAPTable.Rows(0).Item("LimitSKNAmount")).Trim

            If (RequestBankID.Trim = Me.EBankID) Then
                rblTransferType.Items.Item(0).Enabled = False
                rblTransferType.Items.Item(1).Enabled = False
                rblTransferType.Items.Item(2).Enabled = True
            Else
                If Me.TotalPencairan <= Me.LimitSKNAmount Then
                    rblTransferType.Items.Item(0).Enabled = True
                    rblTransferType.Items.Item(1).Enabled = False
                    rblTransferType.Items.Item(2).Enabled = False

                    rblTransferType.Items.Item(0).Selected = True

                ElseIf Me.TotalPencairan > Me.LimitSKNAmount Then
                    rblTransferType.Items.Item(0).Enabled = False
                    rblTransferType.Items.Item(1).Enabled = True
                    rblTransferType.Items.Item(2).Enabled = False

                    rblTransferType.Items.Item(1).Selected = True
                Else
                    rblTransferType.Items.Item(0).Enabled = True
                    rblTransferType.Items.Item(1).Enabled = True
                    rblTransferType.Items.Item(2).Enabled = False
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub ddlLokrblTransferType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTransferType.SelectedIndexChanged
        GetEBankingAccount()

        If rblTransferType.SelectedValue = "SKN" Then
            txtBiayaTransfer.Text = Me.TarifSKN
        End If

        If rblTransferType.SelectedValue = "RTGS" Then
            txtBiayaTransfer.Text = Me.TarifSKN
        End If

        If rblTransferType.SelectedValue = "InHouse" Then
            txtBiayaTransfer.Text = 0
        End If
    End Sub

End Class