﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.IO
#End Region

Public Class ViewAPSupplier
    Inherits Maxiloan.Webform.WebBased
    Dim temptotalPDC As Double
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.APDisbSelec
    Private oController As New ViewAPSupplierController

#End Region
#Region "Property"
    Private Property AccountPayableNo() As String
        Get
            Return (CType(Viewstate("AccountPayableNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AccountPayableNo") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CType(viewstate("Style"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

    Private Property Applicationid() As String
        Get
            Return (CType(Viewstate("Applicationid"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Applicationid") = Value
        End Set
    End Property

    Private Property OTRPrice() As Double
        Get
            Return (CType(Viewstate("OTRPrice"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("OTRPrice") = Value
        End Set
    End Property

    Private Property AttachedFile() As String
        Get
            Return (CType(ViewState("AttachedFile"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AttachedFile") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "VIEWAP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.AccountPayableNo = Request.QueryString("AccountPayableNo")
                Me.BranchID = Request.QueryString("Branchid")
                Me.Style = Request.QueryString("Style")
                DoBind()
            End If
        End If
    End Sub

    Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        oCustomClass.AccountPayableNo = Me.AccountPayableNo
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID

        oCustomClass = oController.ViewList(oCustomClass)
        With oCustomClass


            lblInvoiceNo.Text = .InvoiceNo
            lblInvoiceDate.Text = .InvoiceDate.ToString("dd/MM/yyyy")
            lblTglRencanaBayar.Text = .DueDate.ToString("dd/MM/yyyy")
            lblCustname.Text = .CustomerName
            lblAgreementNo.Text = .AgreementNo
            lblSupplierName.Text = .APTo
            lblBankName.Text = .BankAccountName
            lblBankBranch.Text = .BankBranch
            lblAccountName.Text = .AccountNameTo
            lblAccountNo.Text = .AccountNoTo
          
            lblNilaiInvoice.Text = FormatNumber(.APAmount, 0)
            lblJmlPotongan.Text = FormatNumber(.DeductionAmount, 0)
            lblJmlPembayaran.Text = FormatNumber(.TotalAmount, 0)

            Me.OTRPrice = .otrprice
            Me.BranchID = .BranchId
            Me.Applicationid = .ApplicationID
            AttachedFile = IIf(ReferenceEquals(.AttachedFile, Nothing), "", .AttachedFile)

            btnDownload.Enabled = AttachedFile.Trim <> ""
        End With
         
        oCustomClass = oController.ViewInvoiceDeduction(oCustomClass)
        DtUserList = oCustomClass.listview
        DtgList.DataSource = DtUserList
        DtgList.DataBind()

    End Sub


    Protected Sub btnDownload_OnClick(ByVal sender As Object, ByVal e As EventArgs)
          
        Dim filename = AttachedFile
        If File.Exists(filename) Then
            Dim File As System.IO.FileInfo = New System.IO.FileInfo(filename)
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" + File.Name)
            Response.AddHeader("Content-Length", File.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(File.FullName)
            Response.End()
        Else
            ShowMessage(lblMessage, "File Attached Invoice tidak ditemukan.", True)
        End If 
    End Sub


End Class