﻿
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Public MustInherit Class ApDisbAppBase
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Protected Baris As Integer
    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 10
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1
    Protected oCustomClass As New Parameter.APDisbApp
    Protected oController As New APDisbAppController
    Protected WithEvents txtTglPV As ucDateCE
    Protected WithEvents oPVDetail As ucAPPVDetail
    Protected WithEvents oBeneficiary As ucAPBeneficiary
    Protected WithEvents oDetailAP As ucAPDetailItem
    Protected Total As Double
    Private m_Appcontroller As New ApplicationController
    Private time As String
#End Region

#Region "Property"
    Protected Property PageState() As Int32
        Get
            Return (CType(Viewstate("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            Viewstate("pageState") = Value
        End Set
    End Property
    Protected Property PVStatus() As String
        Get
            Return (CType(viewstate("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVStatus") = Value
        End Set
    End Property
    Protected Property ListData() As DataTable
        Get
            Return (CType(Viewstate("ListData"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("ListData") = Value
        End Set
    End Property
    Protected Property APTo() As String
        Get
            Return (CType(viewstate("APTo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APTo") = Value
        End Set
    End Property
     
    Protected Property DueDate() As String
        Get
            Return (CType(viewstate("DueDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
    Protected Property AccountName() As String
        Get
            Return (CType(viewstate("AccountName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AccountName") = Value
        End Set
    End Property
    Protected Property BankName() As String
        Get
            Return (CType(viewstate("BankName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property
    Protected Property AccountNumber() As String
        Get
            Return (CType(viewstate("AccountNumber"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNumber") = Value
        End Set
    End Property
    Protected Property BankBranch() As String
        Get
            Return (CType(viewstate("BankBranch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankBranch") = Value
        End Set
    End Property
    Protected Property Branch() As String
        Get
            Return (CType(viewstate("Branch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch") = Value
        End Set
    End Property
    Protected Property PVNo() As String
        Get
            Return (CType(viewstate("PVNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVNo") = Value
        End Set
    End Property
    Protected Property WOP() As String
        Get
            Return (CType(viewstate("WOP"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WOP") = Value
        End Set
    End Property
    Protected Property RequestBy() As String
        Get
            Return (CType(viewstate("RequestBy"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("RequestBy") = Value
        End Set
    End Property
    Protected Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property
    Protected Property PVDate() As String
        Get
            Return (CType(viewstate("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
    Protected Property BankAccount() As String
        Get
            Return (CType(viewstate("BankAccount"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccount") = Value
        End Set
    End Property
    Protected Property Balance() As String
        Get
            Return (CType(viewstate("Balance"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Balance") = Value
        End Set
    End Property
    Protected Property Notes() As String
        Get
            Return (CType(viewstate("Notes"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property
    Protected Property PaymentVoucherNo() As String
        Get
            Return (CType(viewstate("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentVoucherNo") = Value
        End Set
    End Property
    Protected Property Branch_ID() As String
        Get
            Return (CType(viewstate("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
    Protected Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property

    Protected Property ApprovalValue() As Decimal
        Get
            Return (CType(ViewState("ApprovalValue"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("ApprovalValue") = Value
        End Set
    End Property
    Protected Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Protected Property ApprovalSchema() As String
        Get
            Return (CType(ViewState("ApprovalSchema"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalSchema") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents dtgEntity As Global.System.Web.UI.WebControls.DataGrid
    Protected WithEvents pnlDatagrid As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents pnlSearch As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents pnlDetail As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents cmbAPType As Global.System.Web.UI.WebControls.DropDownList
    Protected WithEvents ButtonCancel As Global.System.Web.UI.WebControls.Button
    Protected WithEvents ButtonSearch As Global.System.Web.UI.WebControls.Button
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    Protected WithEvents pnlList As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox
    Protected WithEvents RequiredFieldValidator5 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents ButtonReset As Global.System.Web.UI.WebControls.Button
    Protected WithEvents ButtonApp As Global.System.Web.UI.WebControls.Button
    Protected WithEvents ButtonDecline As Global.System.Web.UI.WebControls.Button
    Protected WithEvents cboApprovedBy As Global.System.Web.UI.WebControls.DropDownList



    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Protected Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .Flags = "App"
        End With

        oCustomClass = oController.APDisbAppList2(oCustomClass)

        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        'PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlDetail.Visible = False
    End Sub
 
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function


    Protected Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlDetail.Visible = False

        txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub
    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
       
        If e.Item.ItemIndex >= 0 Then

           
            Dim lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            Dim lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)

            Dim lnkDownPayment = CType(e.Item.FindControl("lnkDownPayment"), HyperLink)

            Dim lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            Dim lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            Dim lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)

            Dim lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            Dim lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            Dim lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            Dim lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            Dim lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim lnkAgreementNoCustomer = CType(e.Item.FindControl("lnkAgreementNoCustomer"), HyperLink)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim

            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label).Text.Trim

            If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            End If

            If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
                Dim strAPTypr As String = ""
                If cmbAPType.SelectedValue = "AFTN" Then
                    strAPTypr = "AFTN"
                ElseIf cmbAPType.SelectedValue = "RCST" Then
                    strAPTypr = "RCST"
                ElseIf cmbAPType.SelectedValue = "INSR" Then
                    strAPTypr = "INSR"
                ElseIf cmbAPType.SelectedValue = "STNK" Then
                    strAPTypr = "STNK"
                Else
                    strAPTypr = "SUPP"
                End If

                lnkAPTO.NavigateUrl = "javascript:OpenViewGeneralAP( '" & lblApplicationID.Text.Trim & "','" & strAPTypr.Trim & "')"
                

            End If

            Dim lnkAPDeta = CType(e.Item.FindControl("lnkAPDeta"), Label)
            lnkAgreementNoCustomer.NavigateUrl = "javascript:OpenViewApSupplier('Finance', '" & lnkPVNo.Text.Trim & "','" & Me.PaymentVoucherBranchID & "')"

            Dim lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Dim lblDetailAmount = CType(e.Item.FindControl("lblDetailAmount"), Label)

            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 0)
            lblDetailAmount.Text = FormatNumber(CDbl(lblDetailAmount.Text), 0)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 0)

        End If
        Dim oData As New DataTable
        
    End Sub
     
    Private Sub DtgEntity_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim lnkPVNo As HyperLink
        lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
        Dim lblBranchId = CType(e.Item.FindControl("lblBranchID"), Label).Text.Trim
        With oCustomClass
            .PaymentVoucherNo = lnkPVNo.Text.Trim
            .BranchId = lblBranchId ''Me.sesBranchId.Replace("'", "")
            .strConnection = GetConnectionString()
        End With

        oCustomClass = oController.APDisbAppInformasi(oCustomClass)

        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        Select Case e.CommandName
            Case "approve"
                If CheckFeature(Me.Loginid, Me.FormID, "APV", Me.AppId) Then
                    'Modify by Wira 20171023
                    time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                    Me.ActivityDateStart = Me.BusinessDate + " " + time
                    '--------

                    pnlDetail.Visible = True
                    pnlDatagrid.Visible = False
                    pnlSearch.Visible = False


                    Me.BranchID = lblBranchId

                    If DvUserList.Count > 0 Then
                        Me.PVStatus = CStr(DvUserList.Item(0)("PVStatus"))
                        With oDetailAP
                            .FormID = Me.FormID
                            .ListData = DtUserList
                            .APTo = CStr(DvUserList.Item(0)("APTo"))
                            .APType = cmbAPType.SelectedItem.Value
                            .DueDate = CStr(DvUserList.Item(0)("PVDueDate"))
                            .BindAPDetail()
                            If Me.FormID = "APAPPROVEHO" Then
                                Bind(oDetailAP.TotalAP)
                                ApprovalValue = oDetailAP.TotalAP
                                Me.ApplicationID = CStr(DvUserList.Item(0)("ApplicationID"))
                            End If
                            'Modify by WIra 20171024
                            Me.ApplicationID = CStr(DvUserList.Item(0)("ApplicationID"))

                        End With
                        With oBeneficiary
                            .AccountName = CStr(DvUserList.Item(0)("AccountNameTo"))
                            .BankName = CStr(DvUserList.Item(0)("BankName"))
                            .AccountNumber = CStr(DvUserList.Item(0)("BankAccountTo"))
                            .BankBranch = CStr(DvUserList.Item(0)("bankcabang"))
                            .BindAPBeneficiary()
                        End With
                        With oPVDetail
                            .Branch = CStr(DvUserList.Item(0)("BranchFullName"))
                            .PVNo = CStr(DvUserList.Item(0)("PaymentVoucherNo"))
                            .WOP = CStr(DvUserList.Item(0)("PaymentTypeID"))
                            .RequestBy = CStr(DvUserList.Item(0)("RequestBy"))
                            .APType = CStr(DvUserList.Item(0)("APType"))
                            .PVDate = CStr(DvUserList.Item(0)("PaymentVoucherDate"))
                            .BankAccount = CStr(DvUserList.Item(0)("BankAccountName"))
                            .Balance = CStr(FormatNumber(CDbl((DvUserList.Item(0)("BankBalance"))), 2))
                            .Notes = CStr(DvUserList.Item(0)("ApprovalNotes"))
                            .BindAPPVDetail()
                        End With
                    End If
                End If
            Case "print"
                Dim oDataMemoPembayaran = New DataSet
                Dim oDataMemoPembayaranDetail1 As New DataSet
                Dim oDataMemoPembayaranDetail2 As New DataSet

                With oCustomClass
                    .PaymentVoucherNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink).Text.Trim
                    .WhereCond = " where PaymentVoucherNo='" & CType(e.Item.FindControl("lnkPVNo"), HyperLink).Text.Trim & "' "
                    .WhereCond1 = " where APType = 'INCS' AND sidd.SupplierEmployeePosition = 'SL' AND ap.ApplicationID='" & CStr(DvUserList.Item(0)("ApplicationID")) & "' AND a.GabungRefundSupplier = 1 "

                    .strConnection = GetConnectionString()
                End With

                oCustomClass = oController.MemoPembayaranReport(oCustomClass)
                oDataMemoPembayaran = oCustomClass.ListAPReport

                oCustomClass = oController.MemoPembayaranDetail1Report(oCustomClass)
                oDataMemoPembayaranDetail1 = oCustomClass.ListAPReport

                oCustomClass = oController.MemoPembayaranDetail2Report(oCustomClass)
                oDataMemoPembayaranDetail2 = oCustomClass.ListAPReport

                Session("RPTName") = "PrnMemoPembayaran" + cmbAPType.SelectedValue
                Session("ApType") = cmbAPType.SelectedValue
                Session("PrnMemoPembayaranDATA") = oDataMemoPembayaran
                Session("PrnMemoPembayaran1DATA") = oDataMemoPembayaranDetail1
                Session("PrnMemoPembayaran2DATA") = oDataMemoPembayaranDetail2
                Response.Redirect("MPCrystalReportViewerPage.aspx")
        End Select
    End Sub


    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) 
            Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC")) 
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        pnlDetail.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imgDecline_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonDecline.Click
        If CheckFeature(Me.Loginid, Me.FormID, "DCL", Me.AppId) Then
                oPVDetail.BindAPPVDetail()
                With oCustomClass
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .PaymentVoucherNo = oPVDetail.PVNo
                    .LoginId = Me.Loginid
                    .Notes = oPVDetail.Notes
                    .PVStatus = "D"
                    .strConnection = GetConnectionString()
                    .BusinessDate = Me.BusinessDate
                    .UpdateMode = "D"
                End With
                Dim errmsg As String = ""
                errmsg = oController.UpdateAPDisbApp(oCustomClass)
                If errmsg <> "" Then
                    ShowMessage(lblMessage, errmsg, True)
                Else
                    DoBind(Me.SearchBy, Me.SortBy)
                    ShowMessage(lblMessage, "Decline Sukses", False)
                End If
        End If
    End Sub
    MustOverride Sub CreateoCustomClass()
    MustOverride Sub appendSearchPVstatus(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusInsurance(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusRefundCustomer(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusFiduciaNotaris(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusRefundSupplier(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusFactoring(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusPO(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusModalUsaha(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusRepoExpenses(strSearch As StringBuilder)
    MustOverride Sub appendSearchPVstatusBiroJasaSTNK(strSearch As StringBuilder)


    Private Sub imgApp_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonApp.Click
        If CheckFeature(Me.Loginid, Me.FormID, "APV", Me.AppId) Then
                oPVDetail.FillTextBox()

                CreateoCustomClass()

                Dim errmsg As String = ""
                errmsg = oController.UpdateAPDisbApp(oCustomClass)
                If errmsg <> "" Then
                    ShowMessage(lblMessage, errmsg, True)
            Else
                'Modify by Wira 20171023
                'Tambah log disburse
                DisbursementLog()

                DoBind(Me.SearchBy, Me.SortBy)
                ShowMessage(lblMessage, "Data berhasil disimpan!", False)
                End If
        End If
    End Sub
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder
        If txtTglPV.Text <> "" Then
            If IsDate(ConvertDate2(txtTglPV.Text)) Then
                Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglPV.Text
                If Me.APType = "INSR" Or Me.APType = "INCP" Or Me.APType = "INJK" Then
                    appendSearchPVstatusInsurance(strSearch)
                ElseIf Me.APType = "RCST" Then
                    appendSearchPVstatusRefundCustomer(strSearch)
                ElseIf Me.APType = "AFTN" Then
                    appendSearchPVstatusFiduciaNotaris(strSearch)
                ElseIf Me.APType = "INCS" Then
                    appendSearchPVstatusRefundSupplier(strSearch)
                ElseIf Me.APType = "FACT" Then
                    appendSearchPVstatusFactoring(strSearch)
                ElseIf Me.APType = "KRSI" Or Me.APType = "SPPL" Then
                    appendSearchPVstatusPO(strSearch)
                ElseIf Me.APType = "MDKJ" Then
                    appendSearchPVstatusModalUsaha(strSearch)
                ElseIf Me.APType = "RADV" Then
                    appendSearchPVstatusRepoExpenses(strSearch)
                ElseIf Me.APType = "STNK" Then
                    appendSearchPVstatusBiroJasaSTNK(strSearch)
                Else
                    appendSearchPVstatus(strSearch)
                End If

                strSearch.Append(" pv.APType='" & Me.APType & "'")
                strSearch.Append(" and pv.pvDueDate <= '" & ConvertDate2(Me.PVDate) & "'")
                'strSearch.Append(" and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "'")
                If txtSearch.Text.Trim <> "" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        strSearch.Append(" and pv.APTo Like '" & txtSearch.Text & "'")
                    Else
                        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                    End If
                End If

                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            Else
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        txtTglPV.Text = ""
        cmbAPType.SelectedIndex = 0
        txtSearch.Text = ""
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlDetail.Visible = False
    End Sub
    Sub Bind(ByVal nilai As Decimal)

        Dim oData As New DataTable

        ApprovalSchema = IIf(APType.ToUpper.Trim = "SPPL", "DISB", "DISO")
        oData = Get_UserApproval(ApprovalSchema, "000", CDec(nilai))
        cboApprovedBy.DataSource = oData.DefaultView
        cboApprovedBy.DataTextField = "Name"
        cboApprovedBy.DataValueField = "ID"
        cboApprovedBy.DataBind()
        cboApprovedBy.Items.Insert(0, "Select One")
        cboApprovedBy.Items(0).Value = "0"
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

#Region "Disbursement Log"
    Sub DisbursementLog()
        If (APType.ToUpper.Trim = "SPPL") Then
            If Me.FormID = "APAPPROVEHO" Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateEnd = Me.BusinessDate + " " + time

                Dim oApplication As New Parameter.Application
                oApplication.strConnection = GetConnectionString()
                oApplication.BranchId = Replace(Me.BranchID.Trim, "'", "").ToString
                oApplication.ApplicationID = Me.ApplicationID.Trim
                oApplication.ActivityType = "VPH"
                oApplication.ActivityDateStart = Me.ActivityDateStart
                oApplication.ActivityDateEnd = Me.ActivityDateEnd
                oApplication.ActivityUser = Me.Loginid
                oApplication.ActivitySeqNo = 22

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Application

                oReturn = m_Appcontroller.DisburseLogSave(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                    Exit Sub
                End If

            Else
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateEnd = Me.BusinessDate + " " + time

                Dim oApplication As New Parameter.Application
                oApplication.strConnection = GetConnectionString()
                oApplication.BranchId = Replace(Me.BranchID.Trim, "'", "").ToString
                oApplication.ApplicationID = Me.ApplicationID.Trim
                oApplication.ActivityType = "VPC"
                oApplication.ActivityDateStart = Me.ActivityDateStart
                oApplication.ActivityDateEnd = Me.ActivityDateEnd
                oApplication.ActivityUser = Me.Loginid
                oApplication.ActivitySeqNo = 21

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Application

                oReturn = m_Appcontroller.DisburseLogSave(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                    Exit Sub
                End If
            End If
        End If
    End Sub
#End Region
End Class
