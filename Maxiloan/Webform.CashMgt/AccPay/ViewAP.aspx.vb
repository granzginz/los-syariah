﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class ViewAP
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"


    Private oCustomClass As New Parameter.APDisbSelec
    Private oController As New ViewAPSupplierController

#End Region
#Region "Property"
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CType(ViewState("Style"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property

    Private Property Applicationid() As String
        Get
            Return (CType(ViewState("Applicationid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Applicationid") = Value
        End Set
    End Property


#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "VIEWAP"
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            Me.Applicationid = Request.QueryString("Applicationid")
            Me.APType = Request.QueryString("APType")
            DoBind()

            'End If
        End If
    End Sub
    Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        oCustomClass.ApplicationID = Me.Applicationid
        oCustomClass.APType = Me.APType
        oCustomClass.strConnection = GetConnectionString()


        oCustomClass = oController.ListViewAP(oCustomClass)
        With oCustomClass
            lblName.Text = .Name
            lblNPWP.Text = .NPWP
            lblTDP.Text = .TDP
            lblSIUP.Text = .SIUP
            lblAddress.Text = .Address
            lblRTRW.Text = .RTRW
            lblKelurahan.Text = .Kelurahan
            lblKecamatan.Text = .Kecamatan
            lblCity.Text = .Kota
            lblZip.Text = .KodePos
            lblPhone1.Text = .Tlp1
            lblPhone2.Text = .Tlp2
            lblFax.Text = .Fax
            lblCPName.Text = .ContactPersonName
            lblCPJobTitle.Text = .ContactPersonTitle
            lblCPEmail.Text = .EMail
            lblCPMobile.Text = .MobilePhone
            lblBankAccountName.Text = .BankAccountName
            lblBankBranch.Text = .BankBranch
            lblAccountName.Text = .AccountName
            lblAccountNo.Text = .AccountNo
        End With



      

    End Sub
End Class