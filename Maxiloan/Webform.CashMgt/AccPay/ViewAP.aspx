﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAP.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.ViewAP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
           </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>         
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    VIEW DATA
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        <div class="form_single">
                <label>Nama</label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>NPWP</label>
                <asp:Label ID="lblNPWP" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>TDP</label>
                <asp:Label ID="lblTDP" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>SIUP</label>
                <asp:Label ID="lblSIUP" runat="server"></asp:Label>
	        </div>
        </div>
  
    

        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>RT/RW</label>
                <asp:Label ID="lblRTRW" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblCity" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos</label>
                <asp:Label ID="lblZip" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-1</label>
                <asp:Label ID="lblPhone1" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-2</label>
             
                <asp:Label ID="lblPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Fax</label>
                <asp:Label ID="lblFax" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    KONTAK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama</label>
                <asp:Label ID="lblCPName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jabatan</label>
                <asp:Label ID="lblCPJobTitle" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>e-mail</label>
                <asp:Label ID="lblCPEmail" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>HandPhone</label>
                <asp:Label ID="lblCPMobile" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    BANK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Bank</label>
                <asp:Label ID="lblBankAccountName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Cabang Bank</label>
                <asp:Label ID="lblBankBranch" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Rekening</label>
                <asp:Label ID="lblAccountName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Rekening</label>
                <asp:Label ID="lblAccountNo" runat="server"></asp:Label>
	        </div>
        </div>
     
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="Close();" runat="server" CausesValidation="False" Text="Close"  CssClass ="small button gray">
            </asp:Button>
        </div>  
    </form>
</body>
</html>
