﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class APChangeLocation
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
#Region "Property"
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property

    Private Property SelectMode() As Boolean
        Get
            Return (CType(viewstate("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("SelectMode") = Value
        End Set
    End Property

    Private Property DueDate() As DateTime
        Get
            Return (CType(viewstate("DueDate"), DateTime))
        End Get
        Set(ByVal Value As DateTime)
            viewstate("DueDate") = Value
        End Set
    End Property

    Private Property InvoiceDate() As Date
        Get
            Return (CType(viewstate("InvoiceDate"), Date))
        End Get
        Set(ByVal Value As Date)
            viewstate("InvoiceDate") = Value
        End Set
    End Property

    Private Property searchValue() As String
        Get
            Return (CType(viewstate("searchValue"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("searchValue") = Value
        End Set
    End Property

    Private Property Branch() As String
        Get
            Return (CType(viewstate("Branch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch") = Value
        End Set
    End Property

    Private Property WCond() As String
        Get
            Return (CType(viewstate("WCond"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WCond") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbSelec
    Private oController As New APDisbSelecController
    Private m_controller As New DataUserControlController
    'Protected WithEvents oDueDate As ValidDate
    Dim Total As Double
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            If IsSingleBranch() Then
                InitialDefaultPanel()
                Me.FormID = "APCHGLOC"
                'oDueDate.Display = ""
                'oDueDate.FillRequired = True
                'oDueDate.isCalendarPostBack = False
                'oDueDate.FieldRequiredMessage = "Harap isi tanggal Rencana Bayar"
                'oDueDate.ValidationErrMessage = "Harap isi dengan format dd/MM/yyyy"
                'oDueDate.dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                oSearchBy.ListData = "InvoiceNo, Invoice No.-AccountPayableNo, AP Detail"
                oSearchBy.BindData()
                With oBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    End If
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                With cmbPaidLocation
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    End If
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#End Region

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.APSeleList(oCustomClass)

        DtUserList = oCustomClass.ListAPSele
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = SortBy
        dtgAP.DataSource = DvUserList

        Try
            dtgAP.DataBind()
        Catch
            dtgAP.CurrentPageIndex = 0
            dtgAP.DataBind()
        End Try
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        pnlListInfo.Visible = False
    End Sub
#End Region

#Region "DoBind2"


    Sub DoBind2(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.APSeleList(oCustomClass)

        DtUserList = oCustomClass.ListAPSele
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = SortBy
        DtgInfo.DataSource = DvUserList

        Try
            DtgInfo.DataBind()
        Catch
            DtgInfo.CurrentPageIndex = 0
            DtgInfo.DataBind()
        End Try
        pnlDataGrid.Visible = False
        pnlSearch.Visible = False
        pnlListInfo.Visible = True
        DtgInfo.Visible = True
        lblAPType.Text = cmbAPType.SelectedItem.Text
        lblDueDateInfo.Text = ConvertDate2(txtTglJatuhTempo.Text).ToString("dd/MM/yyyy")
        lblAPBranchInfo.Text = oBranch.SelectedItem.Text
        lblPaidLocInfo.Text = cmbPaidLocation.SelectedItem.Text

        With cboNewPaidLoc
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
            End If
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlDataGrid.Visible = False
        pnlListInfo.Visible = False
        pnlSearch.Visible = True
        txtAPType.Text = ""

        Me.SelectMode = True
        'With oDueDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi dengan tanggal Rencana Bayar"
        'End With
        pnlListInfo.Visible = False
    End Sub
#End Region

#Region "DatagridCommand"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkItem As CheckBox
        Dim x As Integer
        If Me.SelectMode = True Then
            For x = 0 To dtgAP.Items.Count - 1
                chkItem = CType(dtgAP.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = True
            Next
            Me.SelectMode = False
        Else
            For x = 0 To dtgAP.Items.Count - 1
                chkItem = CType(dtgAP.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = False
            Next
            Me.SelectMode = True
        End If
    End Sub

#End Region

#Region "databound DtgAP"
    Private Sub DtgAP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAP.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblStatus As New Label
        Dim lblStatusDate As New Label
        Dim hyaccount As New HyperLink
        Dim hyaccountdesc As New HyperLink
        Dim nHyAPto As New HyperLink
        Dim nAPID As New Label
        Dim nSupplierID As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblApplicationID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label

        If e.Item.ItemIndex >= 0 Then
            lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lblStatusDate = CType(e.Item.FindControl("lblDueDate"), Label)

            If ConvertDate2(lblStatusDate.Text) <= Me.BusinessDate Then
                lblStatus.Text = "Yes"
            Else
                lblStatus.Text = "No"
            End If
            lblAmount = CType(e.Item.FindControl("lblAMount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)

            hyaccount = CType(e.Item.FindControl("lnkAPDetail"), HyperLink)
            hyaccountdesc = CType(e.Item.FindControl("lnkAPDetailDescription"), HyperLink)

            nAPID = CType(e.Item.FindControl("lnkAPID"), Label)
            nSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            nHyAPto = CType(e.Item.FindControl("lnkAPTo"), HyperLink)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)

            'Select Case cmbAPType.SelectedItem.Value
            '    Case "SPPL"
            '        hyaccountdesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Server.UrlEncode(hyaccount.Text.Trim) & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"
            '        nHyAPto.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(nSupplierID.Text.Trim) & "')"
            '    Case "INSR"
            '        hyaccountdesc.NavigateUrl = "javascript:OpenViewApInsurance('" & "Finance" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"
            '        nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
            'End Select
            Select Case cmbAPType.SelectedItem.Value
                Case "SPPL"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Server.UrlEncode(hyaccount.Text.Trim) & "','" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(nSupplierID.Text.Trim) & "')"
                Case "INSR"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewApInsurance('" & "Finance" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "','" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RADV"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    hyaccountdesc.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"

            End Select
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total, 2)
        End If

    End Sub
#End Region

#Region "Databound DtgInfo"
    Private Sub DtgInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgInfo.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblStatus As New Label
        Dim lblStatusDate As New Label
        Dim hyaccount As New HyperLink
        Dim hyaccountdesc As New HyperLink
        Dim nHyAPto As New HyperLink
        Dim nAPID As New Label
        Dim nSupplierID As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblApplicationID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label

        If e.Item.ItemIndex >= 0 Then
            lblStatus = CType(e.Item.FindControl("lblStatus2"), Label)
            lblStatusDate = CType(e.Item.FindControl("lblDueDate2"), Label)
            If lblStatusDate.Text <= Me.BusinessDate.ToString("dd/MM/yyyy") Then
                lblStatus.Text = "Yes"
            Else
                lblStatus.Text = "No"
            End If
            lblAmount = CType(e.Item.FindControl("lblAMount2"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)

            hyaccount = CType(e.Item.FindControl("lnkAPDetail2"), HyperLink)
            hyaccountdesc = CType(e.Item.FindControl("lnkAPDetailDescription2"), HyperLink)

            nAPID = CType(e.Item.FindControl("lnkAPID2"), Label)
            nSupplierID = CType(e.Item.FindControl("lblSupplierID2"), Label)
            nHyAPto = CType(e.Item.FindControl("lnkAPTo2"), HyperLink)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID2"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID2"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID2"), Label)
            nReffNo = CType(e.Item.FindControl("lblReffNo2"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID2"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo2"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo2"), Label)

            Select Case cmbAPType.SelectedItem.Value
                Case "SPPL"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Server.UrlEncode(hyaccount.Text.Trim) & "','" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(nSupplierID.Text.Trim) & "')"
                Case "INSR"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewApInsurance('" & "Finance" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "','" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RADV"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    hyaccountdesc.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    hyaccountdesc.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                    nHyAPto.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"

            End Select

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum2"), Label)
            lblSum.Text = FormatNumber(Total, 2)
        End If
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid2(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind2(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "ImgReset"
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        Response.Redirect("APChangeLocation.aspx")
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder

        'Me.APType = cmbAPType.SelectedItem.Value
        'Me.SearchBy = ""

        'Me.Branch = oBranch.SelectedValue
        'Me.DueDate = ConvertDate2(oDueDate.dateValue)

        strSearch.Append(" APType='" & cmbAPType.SelectedItem.Value & "'")
        If txtAPType.Text <> "" Then
            If Right(txtAPType.Text.Trim, 1) = "%" Then
                strSearch.Append(" AND ap.APTo like '" + txtAPType.Text & "'")
            Else
                strSearch.Append(" AND ap.APTo='" + txtAPType.Text & "'")
            End If
        End If
        strSearch.Append(" AND ap.BranchID = '" & oBranch.SelectedValue & "'")
        strSearch.Append(" AND ap.DueDate <='" & ConvertDate2(txtTglJatuhTempo.Text).ToString("yyyyMMdd") & "'")
        ' Me.SearchBy += " AND ap.PaymentLocation = '" & Me.sesBranchId.Trim.Replace("'", "") & "'"
        strSearch.Append(" AND ap.PaymentLocation = '" & cmbPaidLocation.SelectedItem.Value & "'")

        If oSearchBy.Text.Trim <> "" Then
            If Right(oSearchBy.Text.Trim, 1) = "%" Then
                strSearch.Append(" and ap. " & oSearchBy.ValueID & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "'")
            Else
                strSearch.Append(" and  ap. " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'")
            End If
        End If

        Me.SearchBy = strSearch.ToString
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Next Process"
    Private Sub imgNext_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonNext.Click

        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then

            Dim chkDtList As CheckBox
            Dim lBlnValid As Boolean
            Dim strGroupAPNo As String
            Dim inlblAPNo As Label

            Dim i As Integer
            For i = 0 To dtgAP.Items.Count - 1
                chkDtList = CType(dtgAP.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Checked = True Then
                        lBlnValid = True
                        inlblAPNo = CType(dtgAP.Items(i).Cells(0).FindControl("lnkAPID"), Label)
                        strGroupAPNo &= CStr(IIf(strGroupAPNo = "", "", ",")) & "'" & inlblAPNo.Text.Replace("'", "") & "'"
                    End If
                End If
            Next

            If Not lBlnValid Then
                ShowMessage(lblMessage, "Harap pilih Jenis A/P", True)
                Exit Sub
            Else
                Me.WCond = ""
                Me.WCond = "  AND AccountPayableNo in  (" & Trim(strGroupAPNo) & ")"
                Me.SearchBy += "  AND ap.AccountPayableNo in  (" & Trim(strGroupAPNo) & ")"
                DoBind2(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Process Get Structure Table"
    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable
        lObjDataTable.Columns.Add("BranchID", GetType(String))
        lObjDataTable.Columns.Add("AccountPayableNo", GetType(String))
        lObjDataTable.Columns.Add("APBalance", GetType(Double))
        lObjDataTable.Columns.Add("PaymentAmount", GetType(Double))
        Return lObjDataTable
    End Function
#End Region

#Region "Save"
    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then

            If cboNewPaidLoc.SelectedItem.Value = cmbPaidLocation.SelectedItem.Value Then
                ShowMessage(lblMessage, "Harap pilih Lokasi Cabang Lain", True)
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = oBranch.SelectedItem.Value
                .WhereCond = Me.WCond
                .PaymentLocation = cboNewPaidLoc.SelectedItem.Value

            End With
            Try
                oController.SaveChangeLoc(oCustomClass)
                Server.Transfer("APChangeLocation.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Response.Redirect("APChangeLocation.aspx")
    End Sub
#End Region

    Function LinkToRequestNo(ByVal strBranchID As String, ByVal strRequestNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenViewRequestNoRefund('" & strBranchID & "','" & strRequestNo & "','" & strStyle & "')"
    End Function

End Class