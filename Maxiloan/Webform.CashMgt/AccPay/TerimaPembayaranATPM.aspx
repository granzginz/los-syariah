﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TerimaPembayaranATPM.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.TerimaPembayaranATPM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ATPMReceiveList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    TERIMA PEMBAYARAN ATPM
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang Kontrak
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" Visible="False" CssClass="grid_general"
                        DataKeyField="Applicationid" AutoGenerateColumns="False" OnSortCommand="SortGrid"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                   <%-- <asp:HyperLink ID="HypReceive" runat="server" Text='SELECT'></asp:HyperLink>--%>
                                   <asp:LinkButton ID="lnkAction" runat="server" CommandName="Select" Text='SELECT'
                                    CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO FAKTUR">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NILAI FAKTUR">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="TIPE CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Address" HeaderText="PPn">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="PPh23">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS" Visible="false">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="Total Faktur">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">                               
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Display="Dynamic" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
         <div class="form_button">
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlReceive" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TERIMA PEMBAYARAN ATPM</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang Kontrak 
                    </label>
                    <asp:Label ID="lblBranchID" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No. Faktur
                    </label>  
                    <asp:Label ID="LblNoFaktur" runat="server"></asp:Label>                
                </div>
                <div class="form_right">
                     <label> Nama Supplier
                    </label>                    
                    <asp:Label id="LblSupplierName" runat="server" />
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nilai Faktur
                    </label>
                    <asp:Label ID="LblInvAmount" runat="server" MaxLength="25"></asp:Label>                   
                </div>
                <div class="form_right">
                    <label> Tanggal Faktur
                    </label>                    
                    <asp:Label id="lblInvoiceDate" runat="server" />
                    <asp:Label ID="lblErrMsgInvoiceDate" runat="server" Visible="False" CssClass="validator_general"></asp:Label> 
                </div>
            </div>
        </div>
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        PPn
                    </label>
                    <asp:Label ID="lblPPn" runat="server" MaxLength="25"></asp:Label>                   
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        PPh ps 23
                    </label>
                    <asp:Label ID="lblPPh23" runat="server" MaxLength="25"></asp:Label>                   
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
            </div>
        </div>
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cara Pembayaran
                    </label>
                    <asp:DropDownList ID="cboPembayaran" runat="server" MaxLength="25">
                        <asp:ListItem Value="TF" Selected="True">Bank</asp:ListItem>
                        <asp:ListItem Value="CA">Tunai</asp:ListItem>      
                     </asp:DropDownList>           
                </div>
                <div class="form_right">
                 <label>
                        Jumlah Terima
                    </label>
                    <asp:Label ID="lblJumlahTerima" runat="server" MaxLength="25"></asp:Label> 
                </div>
            </div>
        </div>
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Rekening
                    </label>

                      <uc1:ucbankaccountbranch id="oToBankAccount" runat="server"></uc1:ucbankaccountbranch>      
                </div>
                <div class="form_right">
                 <%--<label>
                       Tanggal Bayar
                    </label>
                   <uc1:ucdatece id="txtInvoiceDate" runat="server"></uc1:ucdatece>  --%>  
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No. Referensi</label>
                     <asp:TextBox ID="txtReferensi" runat="server" MaxLength="25"></asp:TextBox> 
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                     <asp:TextBox ID="txtKeterangan" runat="server" TextMode="MultiLine"></asp:TextBox> 
            </div>
        </div>
        <div class="form_button">
          <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel2" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
    </asp:Panel>
   </form>
</body>
</html>

