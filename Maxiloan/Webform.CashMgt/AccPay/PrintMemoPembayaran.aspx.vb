﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class PrintMemoPembayaran
    Inherits ApDisbAppBase
    Protected WithEvents oBranch As ucBranchAll

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "MEMOPEMBAYARAN"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("strFileLocation") <> "" Then
                    Response.Write(String.Format("<script language = javascript>{0} var x = screen.width;{0} var y = screen.height;{0} window.open('../../XML/{1}.pdf','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') {0} </script>", vbCrLf, Request.QueryString("strFileLocation")))
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
                If IIf(IsNothing(Request("aptype")), "", Request("aptype")).ToUpper = "SPPL" Then
                    With cmbAPType
                        .Items.Insert(0, "PO Supplier")
                        .Items(0).Value = "SPPL"
                    End With
                Else

                    bindComboAPType()

                End If



            End If
        End If
        lblMessage.Visible = False
    End Sub
    Overrides Sub CreateoCustomClass()
        With oCustomClass
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PaymentVoucherNo = oPVDetail.PVNo
            .ApplicationID = Me.ApplicationID
            .RequestTo = oPVDetail.RequestBy
            .Notes = oPVDetail.Notes
            .PVStatus = "C"
            .NilaiApproval = oDetailAP.TotalAP
            .strConnection = GetConnectionString()
            .approvalBy = cboApprovedBy.SelectedValue

            .BusinessDate = Me.BusinessDate
            .UpdateMode = "C"
        End With
    End Sub
#Region "Append"

    Overrides Sub appendSearchPVstatus(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusInsurance(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusRefundCustomer(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusFiduciaNotaris(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusRefundSupplier(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusFactoring(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusPO(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusModalUsaha(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusRepoExpenses(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub

    Overrides Sub appendSearchPVstatusBiroJasaSTNK(strSearch As StringBuilder)

        strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
    End Sub
#End Region


    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub

End Class