﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region

Public Class KonfirmasiEbankExec
    Inherits Maxiloan.Webform.WebBased

    Dim Control As New CTKonfirmasiEbankExec
    Dim dtCSV, dtSQL As New DataTable

    Protected Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click

        dtCSV = PopulateFromCSV()
        dtSQL = PopulateSQL()
        Linq(dtSQL, dtCSV)        
        pnlLoadCSV.Visible = False
        pnlGrid.Visible = True
    End Sub

    Public Sub Linq(ByVal tbl1 As DataTable, ByVal tbl2 As DataTable)
        Dim dt As New DataTable

        dt.Columns.Add("cusRef")
        dt.Columns.Add("transDesc")
        dt.Columns.Add("accNum")
        dt.Columns.Add("opAvBal")
        dt.Columns.Add("valDate")
        dt.Columns.Add("amount")
        dt.Columns.Add("closLedBal")

        Dim query =
                    From a In tbl1.AsEnumerable
                    Group Join b In tbl2.AsEnumerable
                        On a.Item("cusRef").ToString.Trim Equals b.Item("cusRef").ToString.Trim
                        Into Group
                    Let b = Group.FirstOrDefault
                    Select dt.LoadDataRow(New Object() { _
                                a.Item("cusRef").ToString,
                                If(b Is Nothing, "", b.Item("transDesc")),
                                If(b Is Nothing, "", b.Item("accNum")),
                                If(b Is Nothing, "", b.Item("opAvBal")),
                                If(b Is Nothing, "", b.Item("valDate")),
                                If(b Is Nothing, "", FormatNumber(b.Item("amount"), 0)),
                                If(b Is Nothing, "", b.Item("closLedBal"))}, False)

        If query.Count > 0 Then
            dt = query.CopyToDataTable
        End If


        dtgCSV.DataSource = dt
        dtgCSV.DataBind()
    End Sub
    Private Function PopulateSQL()
        Dim dt As New DataTable
        Dim param As New Parameter.PEKonfirmasiEbankExec
        Dim control As New Controller.CTKonfirmasiEbankExec

        With param
            .strConnection = GetConnectionString()
        End With

        param = control.GetListing(param)
        dt = param.ListData
        Return dt
    End Function
    Private Function PopulateFromCSV()
        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
        Dim directory As String = physicalPath & "\xml\"
        Dim FileName As String = files.PostedFile.FileName
        Dim fullFileName As String = directory & FileName
        Dim dt As New DataTable


        files.PostedFile.SaveAs(fullFileName)
        Dim sr As New IO.StreamReader(fullFileName)
        Try


            Dim newline() As String
            Dim originalLine As String

            For i As Integer = 1 To 6
                newline = sr.ReadLine.Split(","c)
            Next



            dt.Columns.Add("cusRef")
            dt.Columns.Add("transDesc")
            dt.Columns.Add("accNum")
            dt.Columns.Add("opAvBal")
            dt.Columns.Add("valDate")
            dt.Columns.Add("amount")
            dt.Columns.Add("closLedBal")


            While (Not sr.EndOfStream)
                'newline = sr.ReadLine.Split(","c)
                originalLine = sr.ReadLine.ToString
                Dim OriSplit() As String = Split(originalLine, ",", """", False)

                Dim newrow As DataRow = dt.NewRow
                newrow.ItemArray = {OriSplit(0).Replace("""", ""), OriSplit(1).Replace("""", ""), OriSplit(2).Replace("""", ""), OriSplit(3).Replace("""", ""), OriSplit(4).Replace("""", ""), OriSplit(5).Replace("""", ""), OriSplit(6).Replace("""", "")}
                dt.Rows.Add(newrow)

            End While

            'dtgCSV.DataSource = dt
            'dtgCSV.DataBind()


            Return dt
        Catch ex As Exception

        Finally
            sr.Dispose()
            File.Delete(fullFileName)
        End Try
    End Function
    Public Function Split( _
    ByVal expression As String, _
    ByVal delimiter As String, _
    ByVal qualifier As String, _
    ByVal ignoreCase As Boolean) As String()
        Dim _Statement As String = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", Regex.Escape(delimiter), Regex.Escape(qualifier))

        Dim _Options As RegexOptions = RegexOptions.Compiled Or RegexOptions.Multiline
        If ignoreCase Then _Options = _Options Or RegexOptions.IgnoreCase

        Dim _Expression As Regex = New Regex(_Statement, _Options)
        Return _Expression.Split(expression)
    End Function

    Private Sub dtgCSV_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCSV.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblaccNum As New Label
            Dim chk As New CheckBox

            lblaccNum = CType(e.Item.FindControl("accNum"), Label)
            chk = CType(e.Item.FindControl("CheckBox1"), CheckBox)
            If Not lblaccNum.Text.Trim = "" Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim chk As New CheckBox
            Dim lblPaymentVoucherNo As New Label

            Dim param As New Parameter.PEKonfirmasiEbankExec
            Dim control As New Controller.CTKonfirmasiEbankExec

            For i As Integer = 0 To dtgCSV.Items.Count - 1
                chk = CType(dtgCSV.Items(i).FindControl("CheckBox1"), CheckBox)
                lblPaymentVoucherNo = CType(dtgCSV.Items(i).FindControl("cusRef"), Label)
                If chk.Checked = True Then
                    With param
                        .strConnection = GetConnectionString()
                        .PaymentVoucherNo = lblPaymentVoucherNo.Text
                        .BusinessDate = Me.BusinessDate
                    End With

                    control.Saving(param)
                End If
            Next

            dtSQL = PopulateSQL()
            Linq(dtSQL, dtCSV)

            ShowMessage(lblMessage, "Proses Selesai", False)
            pnlLoadCSV.Visible = True
            pnlGrid.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, False)
        End Try
        
    End Sub

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            pnlLoadCSV.Visible = True
            pnlGrid.Visible = False
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnlLoadCSV.Visible = True
        pnlGrid.Visible = False
    End Sub
End Class