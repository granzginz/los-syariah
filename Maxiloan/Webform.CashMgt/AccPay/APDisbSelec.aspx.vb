﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class APDisbSelec
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbSelec
    Private oController As New APDisbSelecController
    Private m_controller As New DataUserControlController
    Private Const SCHEME_ID As String = "INV"    
    Protected WithEvents txtTglJatuhTempo As ucDateCE
    Protected WithEvents txtTglInvoice As ucDateCE
    Dim Total As Double
    Private m_Appcontroller As New ApplicationController
    Private time As String

#End Region

#Region "Property"
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property

    Private Property SelectMode() As Boolean
        Get
            Return (CType(viewstate("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("SelectMode") = Value
        End Set
    End Property

    Private Property DueDate() As DateTime
        Get
            Return (CType(viewstate("DueDate"), DateTime))
        End Get
        Set(ByVal Value As DateTime)
            viewstate("DueDate") = Value
        End Set
    End Property

    Private Property InvoiceDate() As Date
        Get
            Return (CType(viewstate("InvoiceDate"), Date))
        End Get
        Set(ByVal Value As Date)
            viewstate("InvoiceDate") = Value
        End Set
    End Property

    Private Property searchValue() As String
        Get
            Return (CType(viewstate("searchValue"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("searchValue") = Value
        End Set
    End Property

    Private Property Branch() As String
        Get
            Return (CType(viewstate("Branch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If IsSingleBranch Then
                InitialDefaultPanel()

                Me.FormID = "APSELECT"

                'Modify by WIra 20171024
                If Request("ActivityDateStart") = Nothing Then
                    time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                    Me.ActivityDateStart = Me.BusinessDate + " " + time
                Else
                    Me.ActivityDateStart = Request("ActivityDateStart")
                End If
                '-----------

                If Request.QueryString("mode") = "save" Then                    
                    doRequest()                    
                End If


                If IIf(IsNothing(Request("aptype")), "", Request("aptype")).ToUpper = "SPPL" Then
                    With cmbAPType
                        .Items.Insert(0, "PO Supplier")
                        .Items(0).Value = "SPPL"
                    End With
                Else
                   
                    bindComboAPType()

                End If
               
                With oBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                        .Enabled = True
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Enabled = False
                    End If
                    
                End With
             
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If

                Me.SearchBy = ""
                Me.SortBy = ""


            End If
        End If

        Session("APBranchID") = oBranch.SelectedItem.Value.Trim
    End Sub
   Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData

        Dim row As DataRow() = dt.Select("ID='SPPL'")

        For Each r In row
            dt.Rows.Remove(r)
        Next

        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.APSeleList(oCustomClass)

        DtUserList = oCustomClass.ListAPSele
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = SortBy
        dtgAP.DataSource = DvUserList

        Try
            dtgAP.DataBind()
        Catch
            dtgAP.CurrentPageIndex = 0
            dtgAP.DataBind()
        End Try
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
    End Sub

    Private Sub InitialDefaultPanel()
        pnlDataGrid.Visible = False
        pnlSearch.Visible = True
        txtAPType.Text = ""
        txtSearch.Text = ""    
        Me.SelectMode = True
        'With oDueDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi tanggal Rencana Bayar"
        'End With

        txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub
#End Region

#Region "DatagridCommand"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkItem As CheckBox
        Dim x As Integer
        If Me.SelectMode = True Then
            For x = 0 To dtgAP.Items.Count - 1
                chkItem = CType(dtgAP.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = True
            Next
            Me.SelectMode = False
        Else
            For x = 0 To dtgAP.Items.Count - 1
                chkItem = CType(dtgAP.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = False
            Next
            Me.SelectMode = True
        End If
    End Sub

    Private Sub DtgAP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAP.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblStatus As New Label
        Dim lblStatusDate As New Label
        Dim hyaccount As New HyperLink
        Dim hyaccountdesc As New HyperLink
        Dim nHyAPto As New HyperLink
        Dim nAPID As New Label
        Dim nSupplierID As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim nApplicationID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label

        If e.Item.ItemIndex >= 0 Then
            lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lblStatusDate = CType(e.Item.FindControl("lblDueDate"), Label)
            If lblStatusDate.Text <= Me.BusinessDate.ToString("dd/MM/yyyy") Then
                lblStatus.Text = "Yes"
            Else
                lblStatus.Text = "No"
            End If
            lblAmount = CType(e.Item.FindControl("lblAMount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
            Me.sesBranchId.Replace("'", "").Trim()
            hyaccount = CType(e.Item.FindControl("lnkAPDetail"), HyperLink)
            hyaccountdesc = CType(e.Item.FindControl("lnkAPDetailDescription"), HyperLink)

            nAPID = CType(e.Item.FindControl("lnkAPID"), Label)
            nSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            nHyAPto = CType(e.Item.FindControl("lnkAPTo"), HyperLink)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            nApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            Dim strAPTypr As String = ""
            If cmbAPType.SelectedValue = "AFTN" Then
                strAPTypr = "AFTN"
            ElseIf cmbAPType.SelectedValue = "RCST" Then
                strAPTypr = "RCST"
            ElseIf cmbAPType.SelectedValue = "INSR" Then
                strAPTypr = "INSR"
            ElseIf cmbAPType.SelectedValue = "STNK" Then
                strAPTypr = "STNK"
            Else
                strAPTypr = "SUPP"
            End If

            nHyAPto.NavigateUrl = "javascript:OpenViewGeneralAP( '" & nApplicationID.Text.Trim & "','" & strAPTypr.Trim & "')"

            hyaccountdesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & nAPID.Text.Trim & "'," & Server.UrlEncode(sesBranchId) & ")"


        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total, 2)
        End If

    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "ImgReset"
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        pnlDataGrid.Visible = False
        pnlSearch.Visible = True
        txtAPType.Text = ""
        txtSearch.Text = ""        
    End Sub
#End Region

#Region "Search Process"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder

        Me.APType = cmbAPType.SelectedItem.Value
        Me.SearchBy = ""

        Me.Branch = oBranch.SelectedValue
        Me.DueDate = ConvertDate2(txtTglJatuhTempo.Text)
        If txtTglInvoice.Text <> "" Then
            Me.InvoiceDate = ConvertDate2(txtTglInvoice.Text)
        End If
        strSearch.Append(" APType='" & Me.APType & "'")
        'Me.SearchBy = " APType='" & Me.APType & "'"
        If txtAPType.Text <> "" Then
            strSearch.Append(" AND ap.APTo like '%" + txtAPType.Text & "%'")        
        End If
        strSearch.Append("AND ap.BranchID = '" & Me.Branch & "'")
        strSearch.Append(" AND ap.DueDate <='" & Me.DueDate.ToString("yyyyMMdd") & "'")
        'strSearch.Append(" AND ap.PaymentLocation = '" & Me.sesBranchId.Trim.Replace("'", "") & "'")
        If txtSearch.Text <> "" Then
            strSearch.Append(" AND ap." & cmbSearch.SelectedItem.Value.Trim & " like '%" & txtSearch.Text & "%'")        
        End If

        If txtTglInvoice.Text <> "" Then
            strSearch.Append(" AND ap.InvoiceDate='" & Me.InvoiceDate.ToString("yyyyMMdd") & "'")
        End If
        If Me.APType = "SPPL" Or Me.APType = "INSR" Or Me.APType = "INCP" Or Me.APType = "INJK" Or Me.APType = "STNK" Then
            'If Me.APType = "SPPL" Or Me.APType = "INSR" Or Me.APType = "STNK" Then
            strSearch.Append("AND ap.Invoicedate is not null ")
        End If

        Me.SearchBy = strSearch.ToString
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Next Process"
    Private Sub imgNext_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim chkDtList As CheckBox
            Dim lBlnValid As Boolean
            Dim i As Integer
            For i = 0 To dtgAP.Items.Count - 1
                chkDtList = CType(dtgAP.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Checked Then
                        lBlnValid = True
                        Exit For
                    End If
                End If
            Next
            If Not lBlnValid Then
                ShowMessage(lblMessage, "Harap pilih Jenis A/P", True)
                Exit Sub
            Else
                Server.Transfer("apgroup.aspx", True)
                'Server.Transfer("APDisbSelec.aspx", True)
            End If
        End If
    End Sub
#End Region

#Region "Process Get Structure Table"
    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable

        lObjDataTable.Columns.Add("AccountPayableNo", GetType(String))
        lObjDataTable.Columns.Add("APBalance", GetType(Double))
        lObjDataTable.Columns.Add("PaymentAmount", GetType(Double))
        Return lObjDataTable
    End Function
#End Region 
#Region "Do Request"
    Private Sub doRequest()
        Dim oUserControl As UserControl

        Dim oCmbWOP As TextBox
        Dim oDDLlocation As TextBox
        Dim oDtGridAP As DataGrid
        Dim oApType As TextBox

        Dim oInvoiceDate As TextBox
        Dim oAPTo As HyperLink
        Dim oDueDate As Label
        Dim oAccountNameTo As Label
        Dim oAccountNoTo As Label
        Dim oBankNameTo As HiddenField
        Dim oBankBranchTo As HiddenField

        Dim oBankAccountID As HtmlControls.HtmlInputHidden
        Dim oAccountPayableNo As Label

        Dim oPnlAPGroup As Panel
        Dim DrAP As DataRow
        Dim i As Integer
        Dim TotalGroup As Integer
        Dim oAPGroupWeb As New ApGroup
        Dim oPnlRequest As Panel
        Dim oPaymentAmount As ucNumberFormat
        Dim oAPBalance As Label
        Dim oApplicationID As TextBox

        Dim oPnlGenerate As Panel
        Dim oTxtPaymentNote As TextBox
        Dim oRblJenisTransfer As RadioButtonList

        Dim k As Integer
        Dim TotalPVAmount As Double

        TotalGroup = CInt(Request.QueryString("totalgroup"))
        oAPGroupWeb = CType(context.Handler, ApGroup)
        oPnlAPGroup = CType(oAPGroupWeb.FindControl("pnlAPGroup"), Panel)
        oInvoiceDate = CType(oAPGroupWeb.FindControl("txtinvoicedate"), TextBox)
        oApType = CType(oAPGroupWeb.FindControl("txtAPType"), TextBox)
        oCmbWOP = CType(oAPGroupWeb.FindControl("txtWOP"), TextBox)
        oDDLlocation = CType(oAPGroupWeb.FindControl("txtLokasiBayar"), TextBox)

        oBankAccountID = CType(oAPGroupWeb.FindControl("hdnBankAccount"), HtmlControls.HtmlInputHidden)

        oPnlGenerate = CType(oAPGroupWeb.FindControl("pnlGenerate"), Panel)
        oTxtPaymentNote = CType(oAPGroupWeb.FindControl("txtKeterangan"), TextBox)
        oRblJenisTransfer = CType(oAPGroupWeb.FindControl("rblTransferType"), RadioButtonList)


        For i = 0 To (TotalGroup - 1)
            Try
                Dim dtAP As New DataTable
                dtAP = GetStructPDC()
                TotalPVAmount = 0
                oUserControl = CType(oPnlAPGroup.FindControl("usc" & CStr(i)), UserControl)
                oPnlRequest = CType(oUserControl.FindControl("pnlRequest"), Panel)

                oDtGridAP = CType(oUserControl.FindControl("dtgAPGroup"), DataGrid)
                oAPTo = CType(oUserControl.FindControl("lblAPTo"), HyperLink)
                oApplicationID = CType(oUserControl.FindControl("txtApplicationID"), TextBox)
                oDueDate = CType(oUserControl.FindControl("lblDueDate"), Label)
                oAccountNameTo = CType(oUserControl.FindControl("lblNamaRek"), Label)
                oAccountNoTo = CType(oUserControl.FindControl("lblNoRek"), Label)
                oBankNameTo = CType(oUserControl.FindControl("hdnBankId"), HiddenField)
                oBankBranchTo = CType(oUserControl.FindControl("hdnBankCabankId"), HiddenField)

                With oCustomClass

                    .strConnection = GetConnectionString()
                    If oDDLlocation.Text = "C" Then
                        .BranchId = Me.sesBranchId.Replace("'", "")
                    Else
                        '.BranchId = "000"
                        .BranchId = "099"
                    End If

                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .APTo = oAPTo.Text.Trim
                    .APType = oApType.Text.Trim
                    .DueDate = ConvertDate2(oDueDate.Text.Trim)
                    .AccountNameTo = oAccountNameTo.Text.Trim
                    .AccountNoTo = oAccountNoTo.Text.Trim
                    .BankBranchTo = oBankBranchTo.Value.Trim
                    .BankNameTo = oBankNameTo.Value.Trim
                    .ApplicationID = oApplicationID.Text
                    .BankAccountID = oBankAccountID.Value
                    .PaymentTypeID = oCmbWOP.Text

                    For k = 0 To oDtGridAP.Items.Count - 1
                        oAccountPayableNo = CType(oDtGridAP.Items(k).FindControl("lblAccountPayableNo"), Label)
                        oPaymentAmount = CType(oDtGridAP.Items(k).FindControl("txtPaymentAmount"), ucNumberFormat)
                        oAPBalance = CType(oDtGridAP.Items(k).FindControl("lblAPBalance"), Label)
                        TotalPVAmount += CDbl(oPaymentAmount.Text.Trim.Replace(",", ""))
                        DrAP = dtAP.NewRow
                        DrAP("AccountPayableNo") = oAccountPayableNo.Text.Trim
                        DrAP("APBalance") = CDbl(oAPBalance.Text.Trim.Replace(",", ""))
                        DrAP("PaymentAmount") = CDbl(oPaymentAmount.Text.Trim.Replace(",", ""))
                        dtAP.Rows.Add(DrAP)
                    Next
                    .APDetail = dtAP
                    .PVAmount = TotalPVAmount

                    .PaymentNote = oTxtPaymentNote.Text
                    .JenisTransfer = oRblJenisTransfer.SelectedValue

                    'bank detail
                    .AccountNameTo = oAccountNameTo.Text.Trim
                    .AccountNoTo = oAccountNoTo.Text.Trim
                    .BankNameTo = oBankNameTo.Value.Trim
                    .BankBranchTo = oBankBranchTo.Value.Trim

                End With
                'approval menggunakan sheme
                Dim oEntitiesApproval As New Parameter.Approval
                With oEntitiesApproval
                    .BranchId = oCustomClass.BranchId
                    .SchemeID = SCHEME_ID
                    .RequestDate = oCustomClass.BusinessDate
                    .ApprovalNote = Request("Rekomendasi")
                    .ApprovalValue = oCustomClass.PVAmount
                    .UserRequest = Me.Loginid
                    .UserApproval = Request("ApprovedBy")
                    .AprovalType = Parameter.Approval.ETransactionType.InvoiceToBePaid_approval
                    .Argumentasi = Request("Argumentasi")
                End With
                'jika menggunakan approval
                'oCustomClass = oController.SavingToPaymentVoucher(oCustomClass, oEntitiesApproval)





                oCustomClass = oController.SavingToPaymentVoucher(oCustomClass)
                dtAP.Dispose()
                If oCustomClass.StrError = "" Then
                    'Modify by Wira 20171023
                    'Tambah log disburse
                    DisbursementLog(oCustomClass)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, oCustomClass.StrError, True)
                End If
            Catch exp As Exception
                Dim errmsg As String = MessageHelper.MESSAGE_UPDATE_FAILED & "<BR>"
                errmsg &= "SUB do Request : " & exp.Message & "<BR>"
                ShowMessage(lblMessage, errmsg, True)
            End Try
        Next
    End Sub
#End Region

    Function LinkToRequestNo(ByVal strBranchID As String, ByVal strRequestNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenViewRequestNoRefund('" & strBranchID & "','" & strRequestNo & "','" & strStyle & "')"
    End Function

#Region "Disbursement Log"
    Sub DisbursementLog(ByVal oCustomClass As Parameter.APDisbSelec)
        If oCustomClass.APType = "SPPL" Then

            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateEnd = Me.BusinessDate + " " + time

            Dim oApplication As New Parameter.Application
            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Replace(oCustomClass.BranchId.Trim, "'", "").ToString
            oApplication.ApplicationID = oCustomClass.ApplicationID.Trim
            oApplication.ActivityType = "PCD"
            oApplication.ActivityDateStart = Me.ActivityDateStart
            oApplication.ActivityDateEnd = Me.ActivityDateEnd
            oApplication.ActivityUser = Me.Loginid
            oApplication.ActivitySeqNo = 20

            Dim ErrorMessage As String = ""
            Dim oReturn As New Parameter.Application

            oReturn = m_Appcontroller.DisburseLogSave(oApplication)

            If oReturn.Err <> "" Then
                ShowMessage(lblMessage, ErrorMessage, True)
                Exit Sub
            End If
        End If
    End Sub
#End Region
End Class