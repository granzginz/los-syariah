﻿Imports Maxiloan.Webform.UserController
'Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class APExecution
    Inherits Maxiloan.Webform.WebBased

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Private EBankController As New EBankTransferController

    Protected WithEvents ucJatuhTempo As ucDateCE
    Protected WithEvents oListApNew As ucAPTypeNew

    Dim Total As Double

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property SearchBy1() As String
        Get
            Return (CType(ViewState("SearchBy1"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy1") = Value
        End Set
    End Property
    Private Property SearchBy2() As String
        Get
            Return (CType(ViewState("SearchBy2"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy2") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APEXEC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            If Me.IsHoBranch Then
                ButtonSave.Visible = True
            Else
                ButtonSave.Visible = False
                ShowMessage(lblMessage, "Harap login di Kantor Pusat!", True)
            End If

            LoadBank()
        End If

    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        'With oPVDueDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi Tanggal Payment Voucher"
        '    .Display = "Dynamic"
        '    .ValidationErrMessage = "Harap isi dengan format dd/MM/yyyy"
        '    .isCalendarPostBack = False
        '    .dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
        'End With
        ucJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        lblMessage.Visible = False
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, ByVal cmdWhere1 As String, ByVal cmdWhere2 As String)
        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        'Dim dtPVno As DataTable = createNewPVnoDT()

        'dtPVno.Rows.Add(1, String.Empty, String.Empty)

        'With EBankData
        '    .strConnection = GetConnectionString()
        '    .WhereCond = cmdWhere
        '    .WhereCond1 = cmdWhere1
        '    .WhereCond2 = cmdWhere2
        '    .BusinessDate = Me.BusinessDate
        '    .BranchId = Me.sesBranchId.Replace("'", "")
        '    .PVnoDT = dtPVno
        '    .SortBy = SortBy
        'End With

        'EBankData = EBankController.EBankTransferList2(EBankData)

        With EBankData
            .strConnection = GetConnectionString()
            .APType = oListApNew.selectedAPTypeID.Trim
            .ValueDate = ConvertDate2(ucJatuhTempo.Text).ToString("MM/dd/yyyy")
            .BranchId = cbobranch.SelectedValue
        End With

        EBankData = EBankController.EBankTransferList2(EBankData)


        recordCount = EBankData.TotalRecord
        dtgEntity.DataSource = EBankData.ListAPAppr

        Try
            dtgEntity.DataBind()
        Catch ex As Exception
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        Me.PageState = currentPage
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub


    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblInsuranceCoyID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblInsuranceCoyID = CType(e.Item.FindControl("lblInsuranceCoyID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            'If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
            '    lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            'End If

            'If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
            '    'lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
            '    'Select Case cmbAPType.SelectedItem.Value
            '    '    Case "SPPL"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
            '    '    Case "INSR"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblInsuranceCoyID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
            '    '    Case "RCST"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

            '    '    Case "RADV"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
            '    '    Case "ASRP"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            '    'End Select

            'End If


            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 0)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 0)
        End If

    End Sub

#End Region

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch, strSearch1, strSearch2 As New StringBuilder
        If ucJatuhTempo.Text <> "" Then
            If IsDate(ConvertDate2(ucJatuhTempo.Text)) Then
                'Me.APType = cmbAPType.SelectedItem.Value
                'Me.PVDate = oPVDueDate.dateValue
                'strSearch.Append(" pv.PVstatus in ('G') and ")
                'strSearch.Append(" pv.APType='" & Me.APType & "'")
                'strSearch.Append(" and pv.PaymentVoucherDate<='" & ConvertDate2(Me.PVDate) & "'")
                'If txtSearch.Text.Trim <> "" Then
                '    If Right(txtSearch.Text.Trim, 1) = "%" Then
                '        strSearch.Append(" and pv.APTo Like '" & txtSearch.Text & "'")
                '    Else
                '        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                '    End If
                'End If
                'strSearch.Append(" and pv.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'")
                'Me.SearchBy = strSearch.ToString
                'Me.SortBy = ""

                ' Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = ucJatuhTempo.Text
                strSearch.Append(" eb.StatusFlag = 'C' ")
                'strSearch.Append(" and pv.aptype='" & Me.APType & "'")
                strSearch.Append(" and pv.pvDueDate = '" & ConvertDate2(Me.PVDate) & "'")


                'If txtSearch.Text.Trim <> "" Then
                '    If Right(txtSearch.Text.Trim, 1) = "%" Then
                '        strSearch.Append(" and pv.apto like '" & txtSearch.Text & "'")
                '    Else
                '        strSearch.Append(" and pv.apto='" & txtSearch.Text & "'")
                '    End If
                'End If

                strSearch.Append(" and pv.branchid = '" & Me.sesBranchId.Replace("'", "") & "'")
                strSearch1.Append(" EBankFormatTransfer.StatusFlag in ('C') ")
                strSearch1.Append(" and TransferFundTransaction.ValueDate='" & ConvertDate2(Me.PVDate) & "'")
                strSearch1.Append(" and TransferFundTransaction.BranchIDFrom = '" & Me.sesBranchId.Replace("'", "") & "'")
                strSearch2.Append(" StatusFlag in ('C') ")
                strSearch2.Append(" and a.ValueDate='" & ConvertDate2(Me.PVDate) & "'")
                strSearch2.Append(" and a.BranchID= '" & Me.sesBranchId.Replace("'", "") & "'")


                If oListApNew.selectedAPTypeID <> "ALL" Then
                    strSearch.Append(" and pv.APType = '" & oListApNew.selectedAPTypeID & "'")
                    strSearch1.Append(" and TransferFundTransaction.TransferType = '" & oListApNew.selectedAPTypeID & "'")
                End If


                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                Me.SearchBy1 = strSearch1.ToString
                Me.SearchBy2 = strSearch2.ToString
                DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
            Else
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        ucJatuhTempo.Text = ""
        'cmbAPType.SelectedIndex = 0
        'txtSearch.Text = ""
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        If Not CheckFeature(Me.Loginid, Me.FormID, "EXEC", Me.AppId) Then
            ShowMessage(lblMessage, "Tidak memiliki akses untuk Execute Data", True)
            Exit Sub
        End If

        'Dim lnkPVNo As HyperLink
        Dim chkDtList As CheckBox
        Dim dtPVno As DataTable = createNewPVnoDT()
        Dim listdata As New ArrayList
        Dim data As Parameter.APDisbApp

        For i = 0 To dtgEntity.Items.Count - 1
            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then

                    data = DGItemToData(dtgEntity.Items(i))
                    listdata.Add(data)

                    dtPVno.Rows.Add((i + 1).ToString, data.PaymentVoucherNo, String.Empty)

                End If
            End If
        Next

        If listdata.Count = 0 Then
            ShowMessage(lblMessage, "silahkan pilih data terlebih dahulu", True)
            Return
        End If

        Dim EBankData As New Parameter.EBankTransfer
        'Dim EBankController As New EBankTransferController

        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PVnoDT = dtPVno
            .StatusFlag = "P"
            .PaidBy = Me.Loginid
            .PaidDate = BusinessDate
            .GenerateDate = BusinessDate
            .EditedDate = BusinessDate
            .DeleteDate = BusinessDate
            .RejectDate = BusinessDate
        End With

        Dim str As String = EBankController.EBankExecuteNew(listdata, EBankData)

        'Dim err As String = EBankController.EBankTransferChangeStatus(EBankData)

        If str <> String.Empty Then
            ShowMessage(lblMessage, str, True)
        Else
            ShowMessage(lblMessage, "Eksekusi berhasil", False)
            DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
        End If
    End Sub

    Private Function DGItemToData(ByVal dgitem As DataGridItem) As Parameter.APDisbApp
        Dim data As New Parameter.APDisbApp
        With data
            Dim lnkPVNo As HyperLink = CType(dgitem.FindControl("lnkPVNo"), HyperLink)
            Dim lblPVAmount As Label = CType(dgitem.FindControl("lblPVAmount"), Label)
            Dim lblCompanyFullname As Label = CType(dgitem.FindControl("lblCompanyFullname"), Label)
            Dim lblCompanyAddress As Label = CType(dgitem.FindControl("lblCompanyAddress"), Label)
            Dim lblSandiKliringPusat As Label = CType(dgitem.FindControl("lblSandiKliringPusat"), Label)
            Dim lblBankNameTo As Label = CType(dgitem.FindControl("lblBankNameTo"), Label)
            Dim lblBankBranchTo As Label = CType(dgitem.FindControl("lblBankBranchTo"), Label)
            Dim lblBankAccountTo As Label = CType(dgitem.FindControl("lblBankAccountTo"), Label)
            Dim lblAccountNameTo As Label = CType(dgitem.FindControl("lblAccountNameTo"), Label)
            Dim lblPaymentNote As Label = CType(dgitem.FindControl("lblPaymentNote"), Label)
            Dim lblJenisTransfer As Label = CType(dgitem.FindControl("lblJenisTransfer"), Label)
            Dim rblJenisTransfer As RadioButtonList = CType(dgitem.FindControl("rblJenisTransfer"), RadioButtonList)
            Dim lblPVDate As Label = CType(dgitem.FindControl("lblPVDate"), Label)
            Dim lblApplicationID As Label = CType(dgitem.FindControl("lblApplicationID"), Label)
            Dim lblBankAccountId As Label = CType(dgitem.FindControl("lblBankAccountId"), Label)
            Dim lblCustomerName As Label = CType(dgitem.FindControl("lblCustomerName"), Label)
            Dim lblAPType As Label = CType(dgitem.FindControl("lblAPType"), Label)
            'Dim lblDebitAccountNo As Label = CType(dgitem.FindControl("lblDebitAccountNo"), Label)
            Dim lblreferenceno As Label = CType(dgitem.FindControl("lblreferenceno"), Label)

            .PaymentVoucherNo = lnkPVNo.Text
            .BankAccountID = lblBankAccountId.Text
            .BGNo = "None"
            .PVStatus = "P"
            .RecipientName = lblCustomerName.Text
            .PaymentNote = lblPaymentNote.Text
            .approvalBy = Loginid
            .WOP = "GT"
            .PVAmount = lblPVAmount.Text
            .APType = lblAPType.Text
            .BusinessDate = BusinessDate
            '.PVDate = CDate(Mid(lblPVDate.Text, 4, 2) & "/" & Left(lblPVDate.Text, 2) & "/" & Right(lblPVDate.Text, 4))
            .PVDate = CDate(lblPVDate.Text)
            '.PVDate = BusinessDate
            .ApplicationID = lblApplicationID.Text
            .ReferenceNo = lnkPVNo.Text
            .LoginId = Me.Loginid
            .CompanyID = Me.SesCompanyID
            .referenceno2 = lblreferenceno.Text.Trim
            'default
            .BranchId = Me.sesBranchId.Replace("'", "")
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
        End With

        Return data
    End Function

    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function

    Private Sub LoadBank()
        Dim dtbranch As New DataTable

        Dim m_controller As New DataUserControlController
        dtbranch = m_controller.GetBranchAll(GetConnectionString)

        With cbobranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .Items.Insert(1, "ALL")
            .Items(0).Value = "ALL"
        End With

    End Sub
End Class