﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EBankingPreparation.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.EBankingPreparation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ucAPTypeNew" Src="../../Webform.UserController/ucAPTypeNew.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EBanking Preparation</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <%--<script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>--%>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PERSIAPAN PEMBAYARAN VIA E-BANKING
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlMain" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Tanggal Rencana Bayar
                    </label>
                    <uc2:ucdatece id="txtTglJatuhTempo" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cabang</label>
                    <asp:DropDownList runat="server" ID="cboBranch">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="" CssClass="validator_general"
                        ControlToValidate="cboBranch" ErrorMessage="Pilih cabang!"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                    <div class="form_single">
                        <uc1:ucAPTypeNew id="oListApNew" runat="server" Filter="All"></uc1:ucAPTypeNew>
                    </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search">
                </asp:Button>
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PAYMENT VOUCHER</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                            DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True"
                            ShowFooter="True" Width="1400px">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                        </asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APType" HeaderText="PEMBAYARAN /PENCAIRAN" ItemStyle-Width="80px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAPType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APType") %>' />
                                        
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BranchInitialName" HeaderText="CABANG">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranch" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchInitialName") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="BENEFICIARY">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                        </asp:HyperLink><br />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Grand Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH" HeaderStyle-CssClass="th_right" FooterStyle-CssClass="th_right item_grid_right" ItemStyle-CssClass="th_right item_grid_right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="JenisTransfer" HeaderText="SKN/RTGS/InHouse" ItemStyle-Width="150px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblJenisTransfer" runat="server" visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.JenisTransfer") %>'>
                                    </asp:Label>
                                    </ItemTemplate>    
                                    <ItemTemplate>
                                        <asp:RadioButtonList ID="rblJenisTransfer" class="opt_single" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="SKN" Text="SKN" />
                                            <asp:ListItem Value="RTGS" Text="RTGS" />
                                            <asp:ListItem Value="GT" Text="InHouse" />
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BankNameTo" HeaderText="BANK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BankBranchTo" HeaderText="CABANG BANK" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BankAccountTo" HeaderText="NO REKENING">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NAMA REKENING">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO KONTRAK-CUSTOMER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="Voucher No.">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInsuranceCoyID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCompanyFullname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyFullname") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCompanyAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyAddress") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblSandiKliringPusat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SandiKliringPusat") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblPaymentNote" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentNote") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblJenisTransfer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JenisTransfer") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblBeneficiaryBankAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BeneficiaryBankAddress") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblBranchIdAP" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchIdAP") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div class="form_box_hide">
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonNext" runat="server" CssClass="small button green" Text="Next">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDetail" runat="server" Visible="False">
            <div class="form_box_title">
                <div class="form_single">
                    <h5>
                        PILIH REKENING DAN SIMPAN</h5>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Rekening Bank</label>
                    <asp:DropDownList ID="cmbRekening" runat="server" style="width:1200px">
                        <asp:ListItem Value="0">Select One</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Rekening Bank Harap isi!"
                            InitialValue="0" ControlToValidate="cmbRekening" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Tanggal Valuta</label>
                    <uc2:ucdatece id="txtTglValuta" runat="server" />
                </div>
            </div>
            <%--<div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" CssClass="small button blue" Text="Save">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel"
                    CausesValidation="False"></asp:Button>
            </div>--%>
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" CssClass="small button blue" Text="Generate">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Back"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
