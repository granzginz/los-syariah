﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KonfirmasiEbankExec.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.KonfirmasiEbankExec" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Konfirmasi Eksekusi Ebank</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    } else {

                        if (row.rowIndex % 2 == 0) {
                            row.style.backgroundColor = "#C2D69B";

                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }
        function hideMessage() {
            document.getElementById('lblMessage').style.display = 'none';
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage()"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                KONFIRMASI EKSEKUSI EBANK
            </h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlLoadCSV">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Load CSV Files
                </label>
                <asp:FileUpload runat="server" ID="files" ViewStateMode="Enabled" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Load"
                CausesValidation="False"></asp:Button>&nbsp;
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlGrid">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCSV" runat="server" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="true" HeaderText="CUSTOMER REFERENCE">
                                <ItemTemplate>
                                    <asp:Label ID="cusRef" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.cusRef") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="True" DataField="transDesc" HeaderText="TRANSACTION DESCRIPTION">
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="true" HeaderText="ACCOUNT NUMBER">
                                <ItemTemplate>
                                    <asp:Label ID="accNum" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.accNum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="opAvBal" HeaderText="OPENING AVAILABLE BALANCE">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="valDate" HeaderText="VALUE DATE"></asp:BoundColumn>
                            <asp:BoundColumn Visible="True" DataField="amount" HeaderText="AMOUNT" DataFormatString="{0:C}"></asp:BoundColumn>
                            
                            <asp:BoundColumn Visible="false" DataField="closLedBal" HeaderText="CLOSING LEDGER BALANCE">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CssClass="small button blue"
                Text="Save" CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="btnCancel" runat="server" CssClass="small button gray"
                Text="Cancel" CausesValidation="False"></asp:Button>&nbsp;
        </div>
    </asp:Panel>
    </form>
</body>
</html>
