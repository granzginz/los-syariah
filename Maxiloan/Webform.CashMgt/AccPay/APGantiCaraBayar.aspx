﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APGantiCaraBayar.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.APGantiCaraBayar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register Src="../../Webform.UserController/ucdatece.ascx" TagName="ucdatece"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucAPType" Src="../../Webform.UserController/ucAPType.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>APInvoiceFinalApp</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function total(e) {
            var rowCount = document.getElementById('dtgEntity').rows.length - 2;
            var total = 0;
            var totalSelisih = 0;

            for (i = 0; i < rowCount; i++) {
                if (Boolean(document.getElementById('dtgEntity_chkItem_' + i).checked)) {
                    var Amount = document.getElementById('dtgEntity_lblPVAmount_' + i).innerHTML.replace(/,/gi, "");
                    total = total + parseFloat(Amount);
                }

            }
            document.getElementById('dtgEntity_lblSum').innerHTML = number_format(total);
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                    onclick="hideMessage();"></asp:Label>
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>GANTI CARA BAYAR
                        </h3>
                    </div>
                </div>
                <asp:Panel ID="pnlMain" runat="server">
                    <asp:Panel ID="pnlSearch" runat="server">
                        <div class="form_box_uc" runat="server" id="dvucfunding">
                            <uc1:ucAPType ID="ucAPType1" runat="server"></uc1:ucAPType>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Tanggal Bayar</label>
                                <uc1:ucdatece runat="server" ID="TglJatuhTempo"></uc1:ucdatece>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Cabang</label>
                                <asp:DropDownList ID="oBranch" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="rfvCabang" ControlToValidate="oBranch"
                                    ErrorMessage="Pilih Cabang!" Display="Dynamic" CssClass="validator_general" InitialValue="0">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Cari Berdasarkan</label>
                                <asp:DropDownList ID="cmbSearch" runat="server" Style="display: inline">
                                    <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                                    <asp:ListItem Value="CustomerName">Nama Customer</asp:ListItem>
                                    <asp:ListItem Value="ContractNo">No Kontrak</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form_button">
                            <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search"></asp:Button>
                            <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                                CausesValidation="False"></asp:Button>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlDatagrid" runat="server">
                        <div class="form_box_title">
                            <div class="form_single">
                                <h4>DAFTAR PEMBAYARAN</h4>
                            </div>
                        </div>
                        <div class="form_box_header" runat="server" id="div_1">
                            <div class="form_single">
                                <div class="grid_wrapper_ws">
                                    <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                                        DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True"
                                        ShowFooter="true" Width="100%">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <FooterStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll"></asp:CheckBox>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkItem" runat="server" onchange="total(this)"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="APTo" HeaderText="SUPPLIER">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                                    </asp:HyperLink><br />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="VOUCHER NO">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    Grand Total
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH">
                                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblSum" runat="server"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="AgreementNoCustomer" HeaderText="NO KONTRAK-CUSTOMER">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="BankNameTo" HeaderText="BANK">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PaymentTypeID" HeaderText="CARA BAYAR">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="BankBranchTo" HeaderText="CABANG">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="BankAccountTo" HeaderText="NO REKENING">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="AccountNameTo" HeaderText="NAMA REKENING">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInsuranceCoyID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssBranchID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="div_2">
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Ganti Cara Bayar
                                    </label>
                                    <asp:DropDownList ID="cboCarabayar" runat="server">
                                        <asp:ListItem Selected="True" Value="">Select One</asp:ListItem>
                                        <asp:ListItem Value="GT">Ebanking</asp:ListItem>
                                        <asp:ListItem Value="BA">Bank</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue=""
                                        Display="Dynamic" ErrorMessage="Pilih cara pembayaran!" ControlToValidate="cboCarabayar"
                                        CssClass="validator_general"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="grid_wrapper_ws">
                                    <asp:DataGrid ID="dtgEntity1" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                                        DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True"
                                        ShowFooter="true" Width="100%">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <FooterStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:TemplateColumn SortExpression="APTo" HeaderText="SUPPLIER">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                                    </asp:HyperLink><br />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="VOUCHER NO">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    Grand Total
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH">
                                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                                <FooterStyle CssClass="item_grid_right"></FooterStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblSum" runat="server"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="AgreementNoCustomer" HeaderText="NO KONTRAK-CUSTOMER">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="BankNameTo" HeaderText="BANK">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PaymentTypeID" HeaderText="CARA BAYAR">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="BankBranchTo" HeaderText="CABANG">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="BankAccountTo" HeaderText="NO REKENING">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="AccountNameTo" HeaderText="NAMA REKENING">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInsuranceCoyID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceCoyID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                        <div class="form_button" runat="server" id="div_btn1">
                            <asp:Button ID="ButtonNext" runat="server" CssClass="small button blue" Text="Next"></asp:Button>
                        </div>
                        <div class="form_button" runat="server" id="div_btn2">
                            <asp:Button ID="ButtonSave" runat="server" CssClass="small button blue" Text="Save"></asp:Button>
                            <asp:Button ID="ButtonBack" runat="server" CausesValidation="false" CssClass="small button gray"
                                Text="Back"></asp:Button>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
