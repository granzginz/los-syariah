﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APGroup.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.APGroup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>APGroup</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script type="text/javascript" src="../../Maxiloan.js" language="javascript"></script>
    <script language="JavaScript" type="text/javascript">
        //        function WOPChange(pCmbOfPayment) {
        //            //alert(pCmbOfPayment.options[pCmbOfPayment.selectedIndex].value);
        //            var a = pCmbOfPayment.options[pCmbOfPayment.selectedIndex].value;
        //            if (a == 'GT') {
        //                document.getElementById('divJenisTransfer').style.display = '';
        //            }
        //            else {
        //                document.getElementById('divJenisTransfer').style.display = 'none';
        //            };

        //        }

        function validateEbankDescStep0() {
            if (validateEbankDescStep1(document.getElementById('<%= txtKeterangan.ClientID %>'))) {
                // Validator controls are not shown if button OnClientClickProperty is set to javascript code!
                Page_ClientValidate();
                return Page_IsValid;
            } else {
                return false;
            };

        }	
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <div id="scriptJava" runat="server">
    </div>
    <input id="hdnBankAccount" type="hidden" name="hdnBankAccount" runat="server" />
    <input id="hdnBankAccountName" type="hidden" name="hdnBankAccountName" runat="server" />
    <input id="HidDetail" name="HidDetail" type="hidden" runat="server" />
    <asp:TextBox ID="txtCOA" runat="server" Visible="False"></asp:TextBox>
    <asp:TextBox ID="txtAPType" runat="server" Visible="False"></asp:TextBox>
    <asp:TextBox ID="txtBranch" runat="server" Visible="False"></asp:TextBox>
    <asp:TextBox ID="txtWOP" runat="server" Visible="False"></asp:TextBox>
    <asp:TextBox ID="txtLokasiBayar" runat="server" Visible="false"></asp:TextBox>
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DETAIL PEMBAYARAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlGenerate" HorizontalAlign="Center" runat="server">
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Pembayaran</label>
                    <asp:Label ID="lblAPType" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Cabang</label>
                    <asp:Label ID="lblApBranch" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Lokasi Pembayaran
                    </label>
                    <asp:DropDownList runat="server" ID="ddlLokasiBayar" AutoPostBack="true" CausesValidation="false" >
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfvddlLokasiBayar" ControlToValidate="ddlLokasiBayar"
                        InitialValue="0" ErrorMessage="Pilih lokasi bayar!" Display="Dynamic" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <div id="divJenisTransfer" runat="server" visible="false">
                        <label class="label_general">
                            Jenis Transfer</label>
                        <asp:RadioButtonList ID="rblTransferType" runat="server" class="opt_single" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Text="SKN" Value="SKN" />
                            <asp:ListItem Text="RTGS" Value="RTGS" />
                            <asp:ListItem Text="InHouse" Value="InHo" />
                        </asp:RadioButtonList>
                    </div>
                    <div id="divBankAcc" runat="server" visible="false">
                        <asp:DropDownList ID="cmbBankAccount" runat="server" Visible="False">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcmbBankAccount" runat="server" Visible="True"
                            InitialValue="0" Display="Dynamic" ErrorMessage="Pilih bank account!" ControlToValidate="cmbBankAccount"
                            CssClass="validator_general" Enabled="False"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cara Pembayaran
                    </label>
                    <asp:DropDownList ID="cboWop" runat="server" AutoPostBack="true" CausesValidation="false" >
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Pilih cara pembayaran!" ControlToValidate="cboWop"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
<%--                <div class="form_right">
                    <div id="divJenisTransfer" runat="server" visible="false">
                        <label class="label_general">
                            Jenis Transfer</label>
                        <asp:RadioButtonList ID="rblTransferType" runat="server" class="opt_single" RepeatDirection="Horizontal">
                            <asp:ListItem Text="SKN" Value="SKN" />
                            <asp:ListItem Text="RTGS" Selected="True" Value="RTGS" />
                            <asp:ListItem Text="InHouse" Value="InHouse" />
                        </asp:RadioButtonList>
                    </div>
                    <div id="divBankAcc" runat="server" visible="false">
                        <asp:DropDownList ID="cmbBankAccount" runat="server" Visible="False">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcmbBankAccount" runat="server" Visible="True"
                            InitialValue="0" Display="Dynamic" ErrorMessage="Pilih bank account!" ControlToValidate="cmbBankAccount"
                            CssClass="validator_general" Enabled="False"></asp:RequiredFieldValidator>
                    </div>--%>
                <%--</div>--%>
               <div class="form_right">
                    <div id="divBiayaTransfer" runat="server" visible="false">
                        <label class="label_general">Biaya Transfer</label>
						<uc1:ucnumberformat id="txtBiayaTransfer" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <%--<div class="form_single">--%>
                <div class="form_left">
                    <label>
                        Keterangan (max 35)</label>
                    <asp:TextBox ID="txtKeterangan" runat="server" MaxLength="35" CssClass="long_text" />
                </div>
               <div class="form_right">
                    <div id="divbebanBiayaTransfer" runat="server" visible="false">
                        <label class="label_general">Beban Biaya</label>
						    <asp:DropDownList ID="cboBebanBiayaTransfer" runat="server">                
                                <asp:ListItem Value="P" Selected="True">Penerima</asp:ListItem>
                                <asp:ListItem Value="K">Kantor</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonRequest" runat="server" CssClass="small button blue" Text="Request">
                <%--OnClientClick="javascript:return validateEbankDescStep0();"--%>                
                </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel"
                CausesValidation="False"></asp:Button>
        </div>
        <asp:Panel ID="pnlAPGroup" runat="server">
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlApprovalReq" runat="server" Visible="False" HorizontalAlign="Center">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    PAYMENT VOUCHER REQUEST</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approve by</label>
                <asp:DropDownList ID="cboApprovedBy" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Choose To Be Approved By!"
                    Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Approval Recommendation</label>
                <asp:TextBox ID="txtRecommendation" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Hal-hal yang menyimpang &amp; argumentasi</label>
                <asp:TextBox ID="txtArgumentasi" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveApproval" runat="server" CssClass="small button blue" Text="Save">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancelApproval" runat="server" CssClass="small button gray"
                Text="Cancel" CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
