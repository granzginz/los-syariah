﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class APDisbAppHO
    Inherits ApDisbAppBase
    Protected WithEvents oBranch As ucBranchAll

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APAPPROVEHO"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                oBranch.IsAll = True
                If IIf(IsNothing(Request("aptype")), "", Request("aptype")).ToUpper = "SPPL" Then
                    With cmbAPType
                        .Items.Insert(0, "PO Supplier")
                        .Items(0).Value = "SPPL"
                    End With
                Else 
                    bindComboAPType() 
                End If 
            End If
        End If
        lblMessage.Visible = False
    End Sub
    Overrides Sub CreateoCustomClass()
        Dim DrAP As DataRow
        Dim dtgAP As DataGrid = oDetailAP.getGridAP
        Dim totalAP As Double = 0
        Dim oPaymentAmount As ucNumberFormat
        Dim oAPBalance As Label
        Dim oAccountPayableNo As Label
        Dim dtAP As New DataTable
        If Me.FormID = "APAPPROVEHO" Then
            dtAP = GetStructPDC()
            For Each dItem As DataGridItem In dtgAP.Items
                oAccountPayableNo = CType(dItem.FindControl("lnkAPDeta"), Label)
                oPaymentAmount = CType(dItem.FindControl("txtPaymentAmount"), ucNumberFormat)
                oAPBalance = CType(dItem.FindControl("lblAPBala"), Label)
                totalAP = totalAP + CDbl(oPaymentAmount.Text)
                DrAP = dtAP.NewRow
                DrAP("AccountPayableNo") = oAccountPayableNo.Text.Trim
                DrAP("APBalance") = CDbl(oAPBalance.Text.Trim.Replace(",", ""))
                DrAP("PaymentAmount") = CDbl(oPaymentAmount.Text.Trim)
                dtAP.Rows.Add(DrAP)
            Next
        Else
            totalAP = oDetailAP.TotalAP
        End If

        With oCustomClass
            .APDetail = dtAP
            .BranchId = Me.BranchID
            .PaymentVoucherNo = oPVDetail.PVNo
            .ApplicationID = Me.ApplicationID
            .RequestTo = oPVDetail.RequestBy
            .Notes = oPVDetail.Notes
            .PVStatus = "V"
            .NilaiApproval = totalAP
            .strConnection = GetConnectionString()
            .approvalBy = cboApprovedBy.SelectedValue
            .ApprovalSchemaId = ApprovalSchema
            .BusinessDate = Me.BusinessDate
            .UpdateMode = "C"
            .isVerifikasiHO = CBool(IIf(Me.FormID = "APAPPROVEHO", True, False))
        End With
    End Sub
    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable

        lObjDataTable.Columns.Add("AccountPayableNo", GetType(String))
        lObjDataTable.Columns.Add("APBalance", GetType(Double))
        lObjDataTable.Columns.Add("PaymentAmount", GetType(Double))
        Return lObjDataTable
    End Function
#Region "Append"
    Overrides Sub appendSearchPVstatus(strSearch As StringBuilder)

        'strSearch.Append(String.Format("pv.PVstatus in ('V')  and pv.apBranchID = '{0}' and ", oBranch.BranchID))
        'strSearch.Append(String.Format("pv.PVstatus in ('V')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        'strSearch.Append(String.Format("pv.PVstatus in ('V')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusInsurance(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusRefundCustomer(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusFiduciaNotaris(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusRefundSupplier(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusFactoring(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusPO(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusModalUsaha(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusRepoExpenses(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub

    Overrides Sub appendSearchPVstatusBiroJasaSTNK(strSearch As StringBuilder)

        If Me.IsHoBranch = True And oBranch.BranchID = "000" Then
            strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and", oBranch.BranchID))
        Else
            If oBranch.IsAll = True And oBranch.BranchID = "ALL" Then
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J') and "))
            Else
                strSearch.Append(String.Format("pv.PVstatus in ('V', 'J')    and pv.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and pv.apBranchID = '" & oBranch.BranchID & "' and", oBranch.BranchID))
            End If
        End If
    End Sub
#End Region

    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub

End Class