﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Public Class BankAccSelection
    Inherits AbsApDisbApp
    Protected WithEvents txtTglByr As ucDateCE
    Private m_Appcontroller As New ApplicationController
    Private time As String

#Region "Property"
    Protected Property SelectedBankAcc() As String()
        Get
            Return CType(ViewState("SelectedBankAcc"), String())
        End Get
        Set(value As String())
            ViewState("SelectedBankAcc") = value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Visible = False
        'If Me.IsHoBranch Then
        '    ShowMessage(lblMessage, "Tidak memiliki hak akses modul ini..", True)
        '    Exit Sub
        'End If

        'Modify by Wira 20171023
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time

        If Not IsPostBack Then
            Me.FormID = "BANKACCSEL"
            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses.", False)
            End If

            pnlDatagrid.Visible = False
            'txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            'txtTglByr.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            oBranch.IsAll = True
            fillCombo()
        End If
    End Sub


    Protected Overrides Sub ItemDataBoundExt(item As DataGridItem)

        Dim bId = CType(item.FindControl("lblBankId"), Label).Text

        If ddlCaraBayar.SelectedValue.ToString.Trim = "CA" Then
            CType(item.FindControl("lblTrnsfPB"), Label).Visible = True
        ElseIf (SelectedBankAcc(1).Equals(bId.Trim())) Then
            CType(item.FindControl("lblTrnsfPB"), Label).Visible = True
        Else
            Dim jnsTrans = CType(item.FindControl("lblJnsTrans"), Label).Text
            Dim ddl = CType(item.FindControl("ddlTransfPB"), DropDownList)
            ddl.Visible = True
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(jnsTrans))
        End If

    End Sub
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder

        Dim bank As String() = ddlRekeningBank.SelectedValue.Split(";")
        SelectedBankAcc = {ddlCaraBayar.SelectedValue, bank(0), bank(1)}

        'modify ario 12-02-2019 
        'If txtTglPV.Text <> "" Or IsDate(ConvertDate2(txtTglPV.Text)) Then
        APType = cmbAPType.SelectedItem.Value
            If oBranch.BranchID = "ALL" Then
            Me.SearchBy = String.Format("pv.PVstatus in ('A')  and pv.APType='{0}' ", cmbAPType.SelectedItem.Value)
        Else
            Me.SearchBy = String.Format("pv.PVstatus in ('A')  and pv.apBranchID = '{0}' and pv.APType='{1}' ", oBranch.BranchID, cmbAPType.SelectedItem.Value)
        End If
            DoBind()
        'Else
        '    ShowMessage(lblMessage, "Harap isi Tanggal Rencana Bayar", True)
        'End If
    End Sub

    Protected Sub onNextClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonApp.Click
        If Page.IsValid Then
            Try
                Dim bank As String() = ddlRekeningBank.SelectedValue.Split(";")
                'SelectedBankAcc = {ddlCaraBayar.SelectedValue, bank(0), bank(1), ConvertDate2(txtTglByr.Text)}
                SelectedBankAcc = {ddlCaraBayar.SelectedValue, bank(0), bank(1)}
                pnlMain.Visible = True
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub


    Protected Sub batal(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("BankAccSelection.aspx")
    End Sub


    Protected Sub Save(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dt As DataTable = createNewDT()

        Dim updateval As IList(Of Parameter.ValueTextObject) = New List(Of ValueTextObject)
        Dim chck As Integer = 0
        For i = 0 To dtgEntity.Items.Count - 1
            Dim chkDtList = CType(dtgEntity.Items(i).FindControl("ItemCheckBox"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    chck += 1
                    Dim bId = CType(dtgEntity.Items(i).FindControl("lblBankId"), Label).Text
                    Dim transType = IIf((SelectedBankAcc(1).Equals(bId.Trim())), "PB", CType(dtgEntity.Items(i).FindControl("ddlTransfPB"), DropDownList).SelectedValue)
                    updateval.Add(New ValueTextObject(CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink).Text, oBranch.BranchID) With {.ExtText = transType})

                    'Modify by Wira 20171023
                    'untuk disbursement log                    

                    dt.Rows.Add((i + 1).ToString, CType(dtgEntity.Items(i).FindControl("lblApplicationID"), Label).Text.Trim, oBranch.BranchID.Trim)
                End If
            End If
        Next
        If (chck <= 0) Then
            ShowMessage(lblMessage, "Silahkan Pilih Voucher", True)
            Exit Sub
        End If

        'Dim params() As String = {SelectedBankAcc(0), SelectedBankAcc(2), SelectedBankAcc(3)}
        Dim params() As String = {SelectedBankAcc(0), SelectedBankAcc(2)}
        Dim result = oController.UpdateMultiAPDisbApp(GetConnectionString, "B", oBranch.BranchID, updateval, params)

        If (result <> "") Then
            ShowMessage(lblMessage, result, True)
            Exit Sub
        End If

        'Modify by Wira 20171023
        If APType.ToUpper.Trim = "SPPL" Then
            DisbursementLog(dt)
        End If        

        Response.Redirect("BankAccSelection.aspx?s=1")
    End Sub

    Private Sub ddlCaraBayar_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCaraBayar.SelectedIndexChanged
        pnlDatagrid.Visible = False
        fillRekBankCbo()
    End Sub
    Sub fillRekBankCbo()
        Dim ds = oController.LoadComboRekeningBank(GetConnectionString, ddlCaraBayar.SelectedValue.ToString.Trim)
        ddlRekeningBank.DataSource = ds
        ddlRekeningBank.DataTextField = "Text"
        ddlRekeningBank.DataValueField = "Value"
        ddlRekeningBank.DataBind()
        ddlRekeningBank.Items.Insert("0", "Select One")
    End Sub

#Region "Disbursement Log"
    Sub DisbursementLog(dt As DataTable)
        Dim row As DataRow

        For Each row In dt.Rows
            Me.BranchID = row("value2")
            Me.ApplicationID = row("value1")

            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateEnd = Me.BusinessDate + " " + time

            Dim oApplication As New Parameter.Application
            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Replace(Me.BranchID, "'", "").ToString
            oApplication.ApplicationID = Me.ApplicationID.Trim
            oApplication.ActivityType = "PRB"
            oApplication.ActivityDateStart = Me.ActivityDateStart
            oApplication.ActivityDateEnd = Me.ActivityDateEnd
            oApplication.ActivityUser = Me.Loginid
            oApplication.ActivitySeqNo = 24

            Dim ErrorMessage As String = ""
            Dim oReturn As New Parameter.Application

            oReturn = m_Appcontroller.DisburseLogSave(oApplication)

            If oReturn.Err <> "" Then
                ShowMessage(lblMessage, ErrorMessage, True)
                Exit Sub
            End If
        Next row

    End Sub

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")

        Return dt
    End Function

#End Region
End Class