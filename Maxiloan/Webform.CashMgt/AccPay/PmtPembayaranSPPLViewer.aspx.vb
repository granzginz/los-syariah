﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Webform.CashMgt
Imports CrystalDecisions.Shared
#End Region


Public Class PmtPembayaranSPPLViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
    Private Property GM1() As String
        Get
            Return CType(ViewState("GM1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM1") = Value
        End Set
    End Property
    Private Property GM2() As String
        Get
            Return CType(ViewState("GM2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM2") = Value
        End Set
    End Property
    Private Property GM3() As String
        Get
            Return CType(ViewState("GM3"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM3") = Value
        End Set
    End Property
    Private Property Direksi() As String
        Get
            Return CType(ViewState("Direksi"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Direksi") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New APDisbAppController
    Private oParameter As New Parameter.APDisbApp
    Friend Shared ReportSource As PmtPembayaranSPPL
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub
#End Region

#Region "BindReport"
    Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oDataTable As New DataTable
        Dim oReport As PmtPembayaranSPPL = New PmtPembayaranSPPL

        With oParameter
            .strConnection = GetConnectionString()
            .PaymentVoucherNo = Me.MultiApplicationID
            .GM1 = Me.GM1
            .GM2 = Me.GM2
            .GM3 = Me.GM3
            .Direksi = Me.Direksi
            .WhereCond = Me.CmdWhere
            .SortBy = Me.SortBy
        End With

        oParameter = oController.UpdateMultiAPDisbApp2_Report(oParameter)
        oDataSet = oParameter.ListReport

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = Me.CmdWhere
        oParameter.BranchId = Me.sesBranchId.Replace("'", "")
        oParameter.LoginId = Me.Loginid
        oParameter = oController.UpdateMultiAPDisbApp2(oParameter)
        oDataSet = oParameter.ListReport

        oReport.SetDataSource(oDataSet.Tables(0))

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PmtPembayaranSPPL.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions =
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("PrintPermintaanPembayaran.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PmtPembayaranSPPL")

    End Sub

    'Function GetCompanyName() As String
    '    Dim result As String
    '    Dim oConInst As New InstallRcvController
    '    Dim oInstal As New Parameter.InstallRcv
    '    oInstal.strConnection = GetConnectionString()
    '    oInstal.SPName = "spAPDisbAppMultipleUpdate_22"
    '    oInstal.WhereCond = ""
    '    If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
    '        result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
    '    End If
    '    Return result
    'End Function
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPmtPembayaranSPPL")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiApplicationID = cookie.Values("PaymentVoucherNo")
        Me.GM1 = cookie.Values("GM1")
        Me.GM2 = cookie.Values("GM2")
        Me.GM3 = cookie.Values("GM3")
        Me.Direksi = cookie.Values("Direksi")
        'Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class