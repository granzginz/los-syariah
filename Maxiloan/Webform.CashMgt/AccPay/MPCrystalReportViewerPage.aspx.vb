﻿
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Public Class MPCrystalReportViewerPage
    Inherits Maxiloan.Webform.WebBased


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents crMemoPembayaran As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub BindReport()
        Dim oData As New DataSet
        Dim oData1 As New DataSet
        Dim oData2 As New DataSet

        Dim name = CType(Session("RPTName"), String)
        oData = Session("PrnMemoPembayaranDATA")
        oData1 = Session("PrnMemoPembayaran1DATA")
        oData2 = Session("PrnMemoPembayaran2DATA")
        If (Session("ApType").ToString() = "AFTN") Then
            Dim objReport As PrnMemoPembayaranFiducia = New PrnMemoPembayaranFiducia
            objReport.SetDataSource(oData)
            objReport.Subreports.Item(0).SetDataSource(oData1)
            objReport.Subreports.Item(1).SetDataSource(oData2)
            crMemoPembayaran.ReportSource = objReport
            crMemoPembayaran.DataBind()

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String = String.Format("{0}{1}{2}", Me.Session.SessionID, Me.Loginid, name)

            'strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            'strFileLocation += Me.Session.SessionID + Me.Loginid + "PmtPembayaranSPPL.pdf"


            DiskOpts.DiskFileName = String.Format("{0}XML\{1}.pdf", Request.ServerVariables("APPL_PHYSICAL_PATH"), strFileLocation)
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
            objReport.Dispose()
            Session("RPTName") = Nothing

            Session("PrnMemoPembayaran") = Nothing
            Response.Redirect("PrintMemoPembayaran.aspx?strFileLocation=" & strFileLocation)
        Else
            Dim objReport As PrnMemoPembayaran = New PrnMemoPembayaran

            objReport.SetDataSource(oData)
            objReport.Subreports.Item(0).SetDataSource(oData1)
            objReport.Subreports.Item(1).SetDataSource(oData2)
            crMemoPembayaran.ReportSource = objReport
            crMemoPembayaran.DataBind()

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String = String.Format("{0}{1}{2}", Me.Session.SessionID, Me.Loginid, name)

            'strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            'strFileLocation += Me.Session.SessionID + Me.Loginid + "PmtPembayaranSPPL.pdf"


            DiskOpts.DiskFileName = String.Format("{0}XML\{1}.pdf", Request.ServerVariables("APPL_PHYSICAL_PATH"), strFileLocation)
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
            objReport.Dispose()
            Session("RPTName") = Nothing

            Session("PrnMemoPembayaran") = Nothing
            Response.Redirect("PrintMemoPembayaran.aspx?strFileLocation=" & strFileLocation)
        End If



      




        


       
    End Sub
End Class