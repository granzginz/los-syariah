﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class APGroup
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''scriptJava control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents scriptJava As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hdnBankAccount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankAccount As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnBankAccountName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankAccountName As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''HidDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HidDetail As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''txtCOA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCOA As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtAPType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAPType As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtBranch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBranch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtWOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWOP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtLokasiBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLokasiBayar As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlGenerate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlGenerate As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblAPType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAPType As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblApBranch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblApBranch As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlLokasiBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlLokasiBayar As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''rfvddlLokasiBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvddlLokasiBayar As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''divJenisTransfer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divJenisTransfer As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''rblTransferType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblTransferType As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''divBankAcc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divBankAcc As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''cmbBankAccount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbBankAccount As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''rfvcmbBankAccount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvcmbBankAccount As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''cboWop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboWop As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''RequiredFieldValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator4 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''divBiayaTransfer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divBiayaTransfer As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''txtKeterangan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtKeterangan As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''divbebanBiayaTransfer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divbebanBiayaTransfer As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''cboBebanBiayaTransfer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboBebanBiayaTransfer As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ButtonRequest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonRequest As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButtonCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlAPGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAPGroup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''pnlApprovalReq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlApprovalReq As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''cboApprovedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboApprovedBy As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''RequiredFieldValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''txtRecommendation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecommendation As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtArgumentasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtArgumentasi As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ButtonSaveApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonSaveApproval As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButtonCancelApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonCancelApproval As Global.System.Web.UI.WebControls.Button
End Class
