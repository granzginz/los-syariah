﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class APInvoiceApp
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtTglPV As ucDateCE
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Dim Total As Double

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(viewstate("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(viewstate("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(viewstate("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(viewstate("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(Viewstate("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            Viewstate("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(viewstate("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APINVAPP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
        lblMessage.Visible = False
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False        
        'With oPVDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi Tanggal Payment Voucher"
        '    .Display = "Dynamic"
        '    .ValidationErrMessage = "Harap isi dengan format dd/MM/yyyy"
        '    .isCalendarPostBack = False
        '    .dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
        'End With

        txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .Flags = "App"
            .PVStatus = "V"
        End With

        oCustomClass = oController.APDisbAppList2(oCustomClass)

        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub

#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        Dim lnkAgreementNoCustomer As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lnkAgreementNoCustomer = CType(e.Item.FindControl("lnkAgreementNoCustomer"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            End If

            Select Case cmbAPType.SelectedItem.Value
                Case "SPPL"
                    lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                Case "INSR"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RADV"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"

            End Select

            lnkAgreementNoCustomer.NavigateUrl = "javascript:OpenCustomer('" & lblCustomerID.Text.Trim & "','" & "Finance" & "')"
            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 0)


            If e.Item.Cells(5).Text.Contains("&nbsp;") Or e.Item.Cells(5).Text.Trim = "" Then
                e.Item.Cells(5).Text = ""
            Else

                If CBool(e.Item.Cells(5).Text.Trim) Then
                    e.Item.Cells(5).Text = "Ya"
                Else
                    e.Item.Cells(5).Text = "Tidak"
                End If

            End If

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 0)
        End If

    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"

        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        
        Me.PageState = currentPage
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder
        If txtTglPV.Text <> "" Then
            If IsDate(ConvertDate2(txtTglPV.Text)) Then
                Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglPV.Text
                strSearch.Append(" pv.PVstatus in ('V') and ")
                strSearch.Append(" pv.APType='" & Me.APType & "'")
                strSearch.Append(" and pv.PVDueDate<='" & ConvertDate2(Me.PVDate) & "'")
                If txtSearch.Text.Trim <> "" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        strSearch.Append(" and pv.APTo Like '" & txtSearch.Text & "'")
                    Else
                        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                    End If
                End If
                strSearch.Append(" and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "'")
                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            Else                
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else            
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub

    Private Sub imgApp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonApp.Click
        If CheckFeature(Me.Loginid, Me.FormID, "APV", Me.AppId) Then
            DoChangeStatus("A")
        End If

    End Sub

    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function

    Private Sub DoChangeStatus(ByVal status As String)
        Dim chkDtList As CheckBox
        Dim txtAlasanDecline As TextBox
        Dim dtPVno As DataTable = createNewPVnoDT()
        Dim lnkPVNo As HyperLink

        For i As Integer = 0 To dtgEntity.Items.Count - 1
            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            txtAlasanDecline = CType(dtgEntity.Items(i).FindControl("txtAlasanDecline"), TextBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    If status = "D" AndAlso txtAlasanDecline.Text = String.Empty Then
                        ShowMessage(lblMessage, "isi alasan decline", True)
                        Exit Sub
                    End If

                    lnkPVNo = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink)
                    dtPVno.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, txtAlasanDecline.Text)
                End If
            End If

        Next

        'handle no data checked
        If dtPVno.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Silahkan pilih data untuk di approve/decline", True)
            Exit Sub
        End If

        With oCustomClass
            .BranchId = Me.sesBranchId.Replace("'", "")
            .paymentvoucherno_DT = dtPVno
            .Notes = String.Empty
            .LoginId = Me.Loginid
            .PVStatus = status
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .UpdateMode = status
        End With        
        lblMessage.Text = oController.UpdateAPDisbApp2(oCustomClass)
        If lblMessage.Text <> "" Then
            ShowMessage(lblMessage, lblMessage.Text, True)
        Else
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Sudah Disimpan", False)
        End If
    End Sub

    Private Sub imgDecline_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonDecline.Click
        If CheckFeature(Me.Loginid, Me.FormID, "DCL", Me.AppId) Then
            DoChangeStatus("R")
        End If

    End Sub
End Class