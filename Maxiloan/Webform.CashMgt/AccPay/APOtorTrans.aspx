﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APOtorTrans.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.APOtorTrans" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml"> 
    <head id="Head1" runat="server">
    <title>Otorisasi Transaksi</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
     <script src="../../Maxiloan.js" type="text/javascript"></script> 
     <script src="../../js/jquery-1.9.1.min.js"></script>
       <script type="text/javascript">

         $(document).ready(function () {
             $('#btnSave').click(function () {
                 if (isChecked())
                     return confirm('Melakukan SAVE Otorisasi Transaksi ?');
                 });

                 $('#btnReject').click(function () {
                    if (isChecked() )
                     return confirm('Melakukan REJECT Otorisasi Transaksi ?');
                 });
         });

         function isChecked() 
         {
             if ($('input:checkbox[id^="dtgEntity_chkItem_"]:checked').length <= 0) 
             {
                 alert("Silahkan pilih data terlebih dahulu");
                 return false;
             }
             return true;
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server" />

    <asp:Label ID="lblMessage" runat="server"  ></asp:Label>
    <div class="form_title">
        <div class="title_strip"> </div>
        <div class="form_single">                           
            <h3>OTORISASI TRANSAKSI</h3>
        </div>
    </div>


        <asp:Panel ID="pnlList" runat="server" HorizontalAlign="Center">        
            <asp:Panel ID="pnlSearch" HorizontalAlign="Center" runat="server">
                 <div class="form_box">
                        <div class="form_left">
                            <label class ="label_req"> Jenis Pembayaran  </label>
                            <asp:DropDownList ID="cmbAPType" runat="server" /> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" InitialValue="0"
                             ControlToValidate="cmbAPType" ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" CssClass="validator_general" />
                        </div>
                        <div class="form_right">
                           <label>  Rekening Bank </label> 
                            <asp:DropDownList ID="cboBank" runat="server" />
                        </div>
                    </div>
                     
                   <div class="form_box">
                        <div class="form_left">
                            <label> Tanggal Posting</label>
                             <uc1:ucdatece id="txtTglPVFr" runat="server"></uc1:ucdatece>     S/D <uc1:ucdatece id="txtTglPVTo" runat="server"></uc1:ucdatece>    
                        </div>
                        <div class="form_right"> </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search" />
                        <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset" CausesValidation="False"></asp:Button>
                    </div>
            </asp:Panel>

            <asp:Panel ID="pnlDatagrid"  runat="server">
                <div class="form_box_title">
                    <div class="form_single">                    
                        <h4>DAFTAR PAYMENT VOUCHER </h4>
                    </div>
                </div>
                <div class="form_box_header">
                <div class="form_single">
                <div class="grid_wrapper_ns">	

                 <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                            DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True"
                            ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APType" HeaderText="NO VOUCHER">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAPType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>  
                                <asp:BoundColumn DataField="RecipientName" HeaderText="BAYAR KEPADA" />
                                <asp:BoundColumn DataField="bankaccount" HeaderText="BANK ACCOUNT" /> 
                                <asp:BoundColumn DataField="APTO" HeaderText="NAMA REKENING" />
                                <asp:BoundColumn DataField="bankcabang" HeaderText="CAB" />
                                 <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle  HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="lnkAgreementNo" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>' NavigateUrl='' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="CustomerName" HeaderText="KONSUMEN" />
                                <asp:BoundColumn DataField="PostingDate" HeaderText="TGL POSTING" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundColumn DataField="PvAmount" HeaderText="JUMLAH" DataFormatString="{0:N0}"/>
                                <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO Aplikasi" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>' />
                                        <asp:Label ID="lblApBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApBranchID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>  
                            </Columns>
                        </asp:DataGrid>
                         <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                      </div>
                    </div>
                </div> 

                <div class="form_button">
			        <asp:Button ID="btnSave" runat="server" CssClass="small button blue" Text="Save" />
                    <asp:Button ID="btnReject" runat="server" CssClass="small button gray" Text="Reject" />
                    <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel" CausesValidation="False" />
			 </div>
             </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
