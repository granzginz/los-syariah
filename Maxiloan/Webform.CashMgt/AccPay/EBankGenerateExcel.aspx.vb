﻿Imports Maxiloan.Webform.UserController
'Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class EBankGenerateExcel
    Inherits Maxiloan.Webform.WebBased

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1    
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Dim Total As Double

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property EBankMode As String
        Get
            Return (CType(ViewState("EBankMode"), String))
        End Get
        Set(ByVal value As String)
            ViewState("EBankMode") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "EBEXPORT"
            Me.EBankMode = Request("EBankMode")
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            If Me.IsHoBranch Then
                btnSave.Visible = True
            Else
                btnSave.Visible = False
                ShowMessage(lblMessage, "Harap login di Kantor Pusat!", True)
            End If
        End If

        lblMessage.Visible = False
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False        
        txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, ByVal cmdWhere1 As String, ByVal cmdWhere2 As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .WhereCond1 = cmdWhere1
            .WhereCond2 = cmdWhere2
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .Flags = "App"
        End With

        oCustomClass = oController.APDisbAppList3(oCustomClass)

        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch ex As Exception
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        
        Me.PageState = currentPage
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub


    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            End If

            If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
                'lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                'Select Case cmbAPType.SelectedItem.Value
                '    Case "SPPL"
                '        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                '    Case "INSR"
                '        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                '    Case "RCST"
                '        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

                '    Case "RADV"
                '        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
                '    Case "ASRP"
                '        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                'End Select

            End If


            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If

    End Sub

#End Region

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSearch.Click
        Dim strSearch, strSearch1, strSearch2 As New StringBuilder
        If txtTglJatuhTempo.Text <> "" Then
            If IsDate(ConvertDate2(txtTglJatuhTempo.Text)) Then

                'Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglJatuhTempo.Text
                Select Case Me.EBankMode
                    Case "PREQ"
                        'strSearch.Append(" TransferFundTransaction.StatusTransferEBanking in ('G') ")
                        'strSearch.Append(" and TransferFundTransaction.ValueDate='" & ConvertDate2(Me.PVDate) & "'")
                        'strSearch.Append(" and TransferFundTransaction.BranchIDFrom = '" & Me.sesBranchId.Replace("'", "") & "'")
                    Case Else
                        strSearch.Append(" pv.PVstatus in ('G') ")
                        'strSearch.Append(" pv.APType='" & Me.APType & "'")
                        strSearch.Append(" and pv.PaymentVoucherDate='" & ConvertDate2(Me.PVDate) & "'")
                        'If txtSearch.Text.Trim <> "" Then
                        '    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        '        strSearch.Append(" and pv.APTo Like '" & txtSearch.Text & "'")
                        '    Else
                        '        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                        '    End If
                        'End If
                        strSearch.Append(" and pv.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'")

                        strSearch1.Append(" TransferFundTransaction.StatusTransferEBanking in ('G') ")
                        strSearch1.Append(" and TransferFundTransaction.ValueDate='" & ConvertDate2(Me.PVDate) & "'")
                        strSearch1.Append(" and TransferFundTransaction.BranchIDFrom = '" & Me.sesBranchId.Replace("'", "") & "'")
                        strSearch2.Append(" a.Status in ('G') ")
                        strSearch2.Append(" and a.ValueDate='" & ConvertDate2(Me.PVDate) & "'")
                        strSearch2.Append(" and a.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'")
                End Select                

                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy, strSearch1.ToString, strSearch2.ToString)
            Else                
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else            
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnReset.Click
        txtTglJatuhTempo.Text = ""
        'cmbAPType.SelectedIndex = 0
        'txtSearch.Text = ""
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim chkDtList As CheckBox
        Dim dtPVno As DataTable = createNewPVnoDT()
        Dim lnkPVNo As HyperLink

        For i As Integer = 0 To dtgEntity.Items.Count - 1

            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lnkPVNo = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink)
                    dtPVno.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, String.Empty)
                End If
            End If

        Next

        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController

        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = String.Empty
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PVnoDT = dtPVno
            .SortBy = SortBy
        End With

        EBankData = EBankController.EBankTransferList(EBankData)

        'change status to G
        EBankData.StatusFlag = "G"
        EBankData.GenerateBy = Me.Loginid
        EBankData.GenerateDate = BusinessDate
        EBankData.RejectDate = BusinessDate
        EBankData.PaidDate = BusinessDate
        EBankData.DeleteDate = BusinessDate
        EBankData.EditedDate = BusinessDate
        Dim err As String = EBankController.EBankTransferChangeStatus(EBankData)

        Dim dtResult As DataTable = EBankData.ListAPAppr

        If err <> "" Then
            ShowMessage(lblMessage, err, True)
        Else
            InitialDefaultPanel()

            ShowMessage(lblMessage, "Genereate Excel Berhasil", True)
        End If

        Convert(dtResult, Response, "ebanking" & Me.BusinessDate.ToString("dd/MM/yyyy"))

    End Sub

    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function

#Region "Convert to excel"

    Public Shared Sub Convert(ByVal dt As DataTable, ByVal response As HttpResponse, ByVal strFileName As String)
        'first let's clean up the response.object
        'response.Clear()
        response.Charset = ""
        'set the response mime type for excel

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & strFileName & ".xls")
        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        Dim dg As New DataGrid
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = dt
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        response.Write(stringWrite.ToString)
        response.End()
    End Sub

#End Region
End Class