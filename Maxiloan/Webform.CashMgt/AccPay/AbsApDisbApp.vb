﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading

Public MustInherit Class AbsApDisbApp
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    Protected WithEvents pnlsearch As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents cmbAPType As Global.System.Web.UI.WebControls.DropDownList
    Protected WithEvents RequiredFieldValidator5 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Buttonsearch As Global.System.Web.UI.WebControls.Button
    Protected WithEvents ButtonReset As Global.System.Web.UI.WebControls.Button
    Protected WithEvents pnlDatagrid As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents dtgEntity As Global.System.Web.UI.WebControls.DataGrid
    Protected WithEvents ButtonApp As Global.System.Web.UI.WebControls.Button
    Protected WithEvents BtnPrint As Global.System.Web.UI.WebControls.Button
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents txtTglPV As ucDateCE
    Protected oCustomClass As New Parameter.APDisbApp
    Protected oController As New APDisbAppController
    Private Total As Double
    Protected Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property

    Protected Property dt2 As DataTable
        Set(value As DataTable)
            ViewState("dt2") = value
        End Set
        Get
            Return CType(ViewState("dt2"), DataTable)
        End Get
    End Property

    Protected Sub fillCombo()
        Dim customClass As New Parameter.GeneralPaging
        Dim customClassx As New GeneralPagingController
        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
       

    End Sub
    Protected Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .Flags = "App"
        End With

        oCustomClass = oController.APDisbAppList2(oCustomClass)

        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        dtgEntity.DataSource = DvUserList
        dt2 = DtUserList
        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try


        pnlDatagrid.Visible = True
        pnlsearch.Visible = True

    End Sub

    Protected Overridable Sub ItemDataBoundExt(item As DataGridItem)

    End Sub
    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim PaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label).Text.Trim
            Dim lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkPVNo.NavigateUrl = String.Format("javascript:OpenWinPaymentVoucher('{0}','{1}','Finance')", lnkPVNo.Text.Trim, PaymentVoucherBranchID)
            Dim lnkAPDeta = CType(e.Item.FindControl("lnkAPDeta"), Label)
            CType(e.Item.FindControl("lnkAgreementNoCustomer"), HyperLink).NavigateUrl = String.Format("javascript:OpenViewApSupplier('Finance', '{0}','{1}')", lnkAPDeta.Text.Trim, PaymentVoucherBranchID)
            Dim lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            ItemDataBoundExt(e.Item)
            Total += CDbl(lblAmount.Text)

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 0)
        End If
    End Sub
End Class
