﻿Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class InvoiceAPTM

    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtInvoiceDate As ucDateCE
    Private x_controller As New DataUserControlController
    Protected WithEvents GridNavigator As ucGridNav
#Region "Constanta"
    Private m_controller As New InvoiceController
    Dim objrow As DataRow
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim intLoop As Integer
    Dim Status As Boolean = True
#End Region
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(ViewState("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property CustName() As String
        Get
            Return CType(ViewState("CustName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
#Region "LinkTo"
    Function LinkToApplication(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenApplicationId('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function

    Function LinkToAgreement(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToSupplier(ByVal SupplierID As String, ByVal style As String) As String
        Return "javascript:OpenWinSupplier('" & style & "' , '" & SupplierID.Trim & "')"
    End Function

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, "INVATPM", Me.AppId) Then
                lblMessage.Text = ""
                lblMessage.Visible = False
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    If Request("cond") <> "" Then
                        Me.CmdWhere = Request("cond")
                    Else
                        Me.CmdWhere = "ALL"
                    End If
                    Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                    Me.Sort = "ApplicationID ASC"
                    BindGrid()
                    Me.BranchID = Me.sesBranchId.Replace("'", "")
                    InitialPanel()

                    If Request("result") <> "" Then
                        ShowMessage(lblMessage, Request("result"), False)
                    End If

                Else
                    Response.Redirect("../../../error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub


    Sub InitialPanel()
        pnlList.Visible = True
        pnlInvoice.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGrid(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGrid(Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Invoice

        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetInvoiceATPMAgreementList(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        Me.Sort = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.Sort, "DESC") > 0, "", "DESC"))
        BindGrid()
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        BindGrid()
    End Sub
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = "a." & cboSearch.SelectedItem.Value & " = '" & txtSearch.Text.Trim & "'"
        End If
        Me.CmdWhere2 = String.Format("BranchID = '{0}'", Replace(Me.sesBranchId, "'", ""))
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Invoice" Then

            Dim oInvoice As New Parameter.Invoice
            Dim oReturnValue As New Parameter.Invoice

            pnlList.Visible = False
            pnlInvoice.Visible = True
            lblErrMsgInvoiceDate.Text = ""
            txtInvoiceDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            oInvoice.InvoiceNo = txtInvInvoiceNo.Text

            'set parameter Untuk NoInvoice
            oInvoice.strConnection = GetConnectionString()
            oInvoice.BusinessDate = Me.BusinessDate
            oInvoice.BranchId = Me.sesBranchId.Replace("'", "")

            'controller parameter NoInvoice
            oReturnValue = m_controller.GetInvoiceNo(oInvoice)
            txtInvInvoiceNo.Text = oReturnValue.InvoiceNo
            txtInvInvoiceAmount.Text = oInvoice.TotalInvAmount
            txtPPhAmount.Text = oInvoice.TotPPh23
            txtPPnAmount.Text = oInvoice.TotPPN
            txtTotalAmount.Text = oInvoice.InvoiceAmount

            'oInvoice.InvoiceNo = txtInvInvoiceNo.Text
            'oInvoice.InvoiceNo = txtInvInvoiceNo.Text
            'oInvoice.TotalInvAmount = txtInvInvoiceAmount.Text
            'oInvoice.TotPPh23 = txtPPhAmount.Text
            'oInvoice.TotPPN = txtPPnAmount.Text
            'oInvoice.InvoiceAmount = txtTotalAmount.Text
            Dim dtEntity As DataTable

            Dim hypSupplierID As New Label
            hypSupplierID = CType(e.Item.FindControl("hypSupplierID"), Label)

            Dim lblSupplierName As New HyperLink
            lblSupplierName = CType(e.Item.FindControl("hySupplierName"), HyperLink)

            Dim lblBranchID As New Label
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            pnlInvoice.Visible = True
            lblInvSupplierName.Text = lblSupplierName.Text
            lblInvSupplierName.NavigateUrl = LinkToSupplier(hypSupplierID.Text.Trim, "AccAcq")
            txtInvoiceDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            Dim oATPMController As New InvoiceController
            Dim oEntities As New Parameter.Invoice
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To dtgListOfAgreement.DataKeys.Count - 1
                Dim chkPilih As CheckBox = CType(dtgListOfAgreement.Items(n).FindControl("chkPilih"), CheckBox)
                If chkPilih.Checked = True Then
                    oATPMController.SaveATPMOtorisasi(oEntities)
                End If
            Next
            Dim oInvoiceListAgreement As New Parameter.Invoice
            oInvoiceListAgreement.BranchId = lblBranchID.Text.ToString.Trim
            oInvoiceListAgreement.SupplierID = hypSupplierID.Text.Trim
            oInvoiceListAgreement.strConnection = GetConnectionString()
            Me.SupplierID = hypSupplierID.Text.Trim
            oInvoiceListAgreement = m_controller.GetInvoiceATPMList(oInvoiceListAgreement)

            If Not oInvoiceListAgreement Is Nothing Then
                dtEntity = oInvoiceListAgreement.ListData
                recordCount = oInvoiceListAgreement.TotalRecords
            Else
                recordCount = 0
            End If

            dtgListOfAgreement.DataSource = dtEntity.DefaultView
            dtgListOfAgreement.CurrentPageIndex = 0
            dtgListOfAgreement.DataBind()

        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound

        If e.Item.ItemIndex >= 0 Then
            Dim hypSupplierID As New Label
            hypSupplierID = CType(e.Item.FindControl("hypSupplierID"), Label)
            Me.SupplierID = hypSupplierID.Text.Trim

            Dim hySupplierName As New HyperLink
            hySupplierName = CType(e.Item.FindControl("hySupplierName"), HyperLink)
            hySupplierName.NavigateUrl = LinkToSupplier(hypSupplierID.Text.Trim, "AccAcq")
        End If

    End Sub

#Region "dtgListOfAgreement_ItemDataBound"
    Private Sub dtgListOfAgreement_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgListOfAgreement.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblAgreementNo As New Label
            Dim lblApplicationId As Label
            Dim lblCustomerID As New Label
            Dim hynAgreementNo As New HyperLink
            Dim hynCustomerName As New HyperLink
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationID"), Label)
            hynAgreementNo = CType(e.Item.FindControl("hynAgreementNo"), HyperLink)
            hynAgreementNo.NavigateUrl = LinkToAgreement(lblApplicationId.Text.Trim, "AccAcq")

            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
        End If
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgListOfAgreement.ItemCommand
        If e.CommandName = "PILIH" Then
            Dim oInvoice_SuppTopDays As New Parameter.Invoice
            oInvoice_SuppTopDays.strConnection = GetConnectionString()
            txtInvInvoiceNo.Text = oInvoice_SuppTopDays.InvoiceNoATPM
            lblMessage.Text = ""
            lblMessage.Visible = False

            If txtInvoiceDate.Text = "" Then
                pnlInvoice.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            Dim SuppTopDays = m_controller.GetInvoiceSuppTopDays(oInvoice_SuppTopDays)
            Dim MaxBackDate = m_controller.GetInvoiceMaxBackDate(oInvoice_SuppTopDays)
            Dim Temp_MaxBackDate As Integer = MaxBackDate * (-1)
            If ((ConvertDate2(txtInvoiceDate.Text) < DateAdd(DateInterval.Day, Temp_MaxBackDate, Me.BusinessDate)) Or (ConvertDate2(txtInvoiceDate.Text) > Me.BusinessDate)) Then
                lblErrMsgInvoiceDate.Text = "Invoice Date is not allowed!"
                lblErrMsgInvoiceDate.Visible = True
                pnlList.Visible = False
                pnlInvoice.Visible = True
                Exit Sub
            End If

            Dim i = e.Item.ItemIndex
            Dim invATPM = New Parameter.Invoice
            invATPM.BranchId = CType(dtgListOfAgreement.Items(i).FindControl("lblBranchID"), Label).Text.Trim
            invATPM.SupplierID = Me.SupplierID
            invATPM.SupplierName = lblInvSupplierName.Text
            invATPM.InvoiceNoATPM = txtInvInvoiceNo.Text.Trim
            invATPM.InvoiceDate = ConvertDate2(txtInvoiceDate.Text)

            invATPM.ApplicationID = CType(dtgListOfAgreement.Items(i).FindControl("lblApplicationID"), Label).Text.Trim
            invATPM.AgreementNo = CType(dtgListOfAgreement.Items(i).FindControl("hynAgreementNo"), HyperLink).Text.Trim()

            invATPM.CustomerID = CType(dtgListOfAgreement.Items(i).FindControl("lblCustomerID"), Label).Text.Trim()
            invATPM.CustomerName = CType(dtgListOfAgreement.Items(i).FindControl("hynCustomerName"), HyperLink).Text.Trim()

            'invATPM.APAmount = CDec(CType(dtgListOfAgreement.Items(i).FindControl("lblAPAmount"), Label).Text.Trim)
            'invATPM.APType = CType(dtgListOfAgreement.Items(i).FindControl("lblAPType"), Label).Text.Trim

            'invATPM.IsGabungRefundSupplier = CBool(CType(dtgListOfAgreement.Items(i).FindControl("lblIsGabung"), Label).Text.Trim)
            'invATPM.Refound = CDec(CType(dtgListOfAgreement.Items(i).FindControl("lblRefund"), Label).Text.Trim)

            'If invATPM.APAmount <> CDec(txtInvInvoiceAmount.Text.Trim) Then
            '    ShowMessage(lblMessage, "Nilai Invoice Salah!", True)
            '    pnlList.Visible = False
            '    pnlInvoice.Visible = True
            '    Exit Sub
            'End If

            'Session("INVOCICESUPP") = invATPM
            'Response.Redirect("InvoiceDeductions.aspx")
        End If
    End Sub
#End Region
#Region "imbCancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlInvoice.Visible = False
        pnlList.Visible = True
    End Sub
#End Region
#Region "imbSave"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Dim oInvATPM As New Parameter.Invoice
            Dim oReturnValue As New Parameter.Invoice
            oInvATPM.strConnection = GetConnectionString()

        For n As Integer = 0 To dtgListOfAgreement.DataKeys.Count - 1
            Dim chkPilih As CheckBox = CType(dtgListOfAgreement.Items(n).FindControl("chkPilih"), CheckBox)
            If chkPilih.Checked = True Then
                oInvATPM.BranchId = Me.BranchID
                oInvATPM.SupplierATPM = lblInvSupplierName.Text
                oInvATPM.InvoiceNo = txtInvInvoiceNo.Text
                oInvATPM.InvoiceDate = ConvertDate2(txtInvoiceDate.Text)
                oInvATPM.TotalInvAmount = CDec(txtInvInvoiceAmount.Text)
                oInvATPM.TotPPN = CDec(txtPPnAmount.Text)
                oInvATPM.TotPPh23 = CDec(txtPPhAmount.Text)
                oInvATPM.TotalAmount = CDec(txtTotalAmount.Text)

                m_controller.InvoiceATPMSave(oInvATPM)
                pnlList.Visible = True
                txtInvInvoiceNo.Text = oInvATPM.InvoiceNo
            End If
            ShowMessage(lblMessage, "Data Berhasil di simpan", False)
        Next

    End Sub
#End Region
End Class