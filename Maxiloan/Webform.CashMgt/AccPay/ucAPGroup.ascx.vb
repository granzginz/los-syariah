﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class ucAPGroup
    Inherits ControlBased
#Region "Properties"
    Public Property IsAPRequest() As Boolean
        Get
            Return CType(Viewstate("IsAPRequest"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsApRequest") = Value
        End Set
    End Property

    Public Property TotalAPBalance() As Double
        Get
            Return CType(Viewstate("TotalAPBalance"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalAPBalance") = Value
        End Set
    End Property

    Public Property TotalPaymentAmount() As Double
        Get
            Return CType(Viewstate("TotalPaymentAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalPaymentAmount") = Value
        End Set
    End Property
    Public Property VoucherBankID() As String
        Get
            Return CType(ViewState("VoucherBankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("VoucherBankID") = Value
        End Set
    End Property
    Public Property IsAPApproved() As Boolean
        Get
            Return CType(ViewState("IsApApproved"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            Viewstate("IsAPApproved") = Value
        End Set
    End Property

    Public Property PaymentVoucherID() As String
        Get
            Return CType(Viewstate("PaymentVoucherID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("PaymentVoucherID") = Value
        End Set
    End Property
#End Region

#Region "Property : UCP_APTo - variable = GStrAPTo"
    Public Property APTo() As String
        Get
            Return CType(Viewstate("APTo"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("APTo") = Value
        End Set
    End Property
#End Region

#Region "Property : UCP_DueDate - variable = GDtDueDate"
    Public Property DueDate() As Date
        Get
            Return CType(Viewstate("DueDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("DueDate") = Value
        End Set
    End Property
#End Region

#Region "Property : UCP_APNoList - variable = GStrAPNoList"
    Public Property AccountPayableNoList() As String
        Get
            Return CType(viewstate("APNoList"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("APNoList") = Value
        End Set
    End Property
#End Region

#Region "Property : UCP_isAPSelection - variable = GBlnAPSelection"
    Public Property isAPSelection() As Boolean
        Get
            Return CType(viewstate("IsAPSelection"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsAPSelection") = Value
        End Set
    End Property
    Public Property APType() As String
        Get
            Return CType(viewstate("APType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property
    Public Property SupplierId() As String
        Get
            Return CType(viewstate("SupplierId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierId") = Value
        End Set
    End Property
    Public Property InsCoyId() As String
        Get
            Return CType(viewstate("InsCoyId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsCoyId") = Value
        End Set
    End Property
    Public Property InsBranchId() As String
        Get
            Return CType(viewstate("InsBranchId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsBranchId") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property Account() As String
        Get
            Return CType(viewstate("Account"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Account") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private customClass As New Parameter.APDisbSelec
    Private oController As New APDisbSelecController
    Private m_controller As New POController
    'Protected WithEvents txtPaymentAmount As ucNumberFormat
#End Region
    Private IsBond As Boolean
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack And IsBond = False Then
            doDataBinding()
        End If
    End Sub

    Public Sub doDataBinding()

        If Not IsPostBack Then
            IsBond = True
            Dim arrPayBy As New ArrayList
            Dim StrPay As String = ""
            SearchProcess()
            With customClass
                .strConnection = GetConnectionString()

                If Me.isAPSelection Then
                    .Status = "'N','I','P'"
                Else
                    .Status = "'N','H','I','P'"
                End If
                .WhereCond = Me.SearchBy
            End With

            If Me.AccountPayableNoList Is Nothing OrElse Me.AccountPayableNoList = String.Empty Then Exit Sub

            customClass = oController.APGroupSelectionList(customClass)
            If customClass.ListAPGroupSelection.Rows.Count > 0 Then
                dtgAPGroup.DataSource = customClass.ListAPGroupSelection
                dtgAPGroup.DataBind()

                lblAPTo.Text = CStr(customClass.ListAPGroupSelection.Rows(0).Item("APTo"))
                lblAPTo.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(Me.SupplierId) & "')"


                lblDueDate.Text = CDate(customClass.ListAPGroupSelection.Rows(0).Item("DueDate")).ToString("dd/MM/yyyy")

                txtApplicationID.Text = CStr(customClass.ListAPGroupSelection.Rows(0).Item("ApplicationID"))

                lblNamaRek.Text = CStr(customClass.ListAPGroupSelection.Rows(0).Item("AccountNameTo"))
                lblNoRek.Text = CStr(customClass.ListAPGroupSelection.Rows(0).Item("AccountNoTo"))
                lblNamaBank.Text = CStr(customClass.ListAPGroupSelection.Rows(0).Item("BankNameTo"))
                lblNamaCabang.Text = CStr(customClass.ListAPGroupSelection.Rows(0).Item("BankBranchTo"))
                hdnBankId.Value = CStr(customClass.ListAPGroupSelection.Rows(0).Item("BankIdTo"))
                Me.VoucherBankID = hdnBankId.Value
                hdnBankCabankId.Value = CStr(customClass.ListAPGroupSelection.Rows(0).Item("BankBranchID"))

            End If
        End If
    End Sub

    Private Sub SearchProcess()
        Me.SearchBy = " AccountPayableNo in (" & Me.AccountPayableNoList & ")  "  
    End Sub

    Private Sub dtgAPGroup_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAPGroup.ItemDataBound
        Dim oAPDisbSelec As New APDisbSelec
        Dim lblAPBalance As Label
        Dim lblTotAPBalance As Label
        Dim hyApNo As HyperLink
        Dim oBranch As DropDownList
        Dim APBranchID As String
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim txtPaymentAmount As ucNumberFormat

        APBranchID = CType(Session("APBranchID"), String)
        If e.Item.ItemIndex >= 0 Then
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            oBranch = CType(oAPDisbSelec.FindControl("oBranch"), DropDownList)
            hyApNo = CType(e.Item.FindControl("lnkAPNo"), HyperLink)
            nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            hyApNo.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Me.Account & "','" & Server.UrlEncode(APBranchID) & "')"
 

            lblAPBalance = CType(e.Item.FindControl("lblAPBalance"), Label)
            txtPaymentAmount = CType(e.Item.FindControl("txtPaymentAmount"), ucNumberFormat)
            txtPaymentAmount.Enabled = False
            txtPaymentAmount.Text = lblAPBalance.Text
            txtPaymentAmount.RequiredFieldValidatorEnable = True
            txtPaymentAmount.RangeValidatorMinimumValue = "1"
            txtPaymentAmount.RangeValidatorMaximumValue = lblAPBalance.Text


            Me.TotalAPBalance += CDbl(lblAPBalance.Text)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblTotAPBalance = CType(e.Item.FindControl("lblTotAPBalance"), Label)
            lblTotAPBalance.Text = FormatNumber(Me.TotalAPBalance, 2)
        End If
    End Sub

   

End Class