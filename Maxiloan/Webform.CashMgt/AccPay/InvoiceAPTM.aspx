﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InvoiceAPTM.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.InvoiceAPTM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCustomerLookUpGlobal" Src="../../Webform.UserController/ucLookUpCustomerGlobal.ascx" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav"
    TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Invoice ATPM</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>

    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

      
    <script language="javascript" type="text/javascript">
        function toCommas(yourNumber) {
            var n = yourNumber.toString().split(".");
            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return n.join(".");
        }

        $(document).ready(function () {
            $('input:checkbox[id^="dtgListOfAgreement_chkPilih_"]').change(function () {
                var _nilInv = 0;
                var _nilPPn = 0;
                var _nilPPh = 0;
                var _total = 0;
                var _SubsBunga = "0";
                var _DP = "0";
                var _ang = "0";

                var rowCount = document.getElementById('dtgListOfAgreement').rows.length - 1;

                if ($('input:checkbox[id^="dtgListOfAgreement_chkPilih_"]:checked').length > 0) {

                    for (var i = 0; i < rowCount; i++) {
                        var _id = "dtgListOfAgreement_chkPilih_" + i;
                        if ($("#" + _id).is(':checked')) {
                            _SubsBunga = $("#dtgListOfAgreement_lblSubsBunga_" + i).text().replace(/,/gi, "");

                            _DP = $("#dtgListOfAgreement_lblDP_" + i).text().replace(/,/gi, "");

                            _ang = $("#dtgListOfAgreement_lblAngs_" + i).text().replace(/,/gi, "");

                            _nilInv = _nilInv + parseFloat(_SubsBunga) + parseFloat(_DP) + parseFloat(_ang);

                            _nilPPn = (_nilInv * 10) / 100

                            _nilPPh = (_nilInv * 2) / 100

                            _total = _nilInv + _nilPPn + _nilPPh

                            $("#txtPPnAmount").text(toCommas(parseFloat(_nilPPn)));
                            $("#txtPPhAmount").text(toCommas(parseFloat(_nilPPh)));
                            $("#txtTotalAmount").text(toCommas(parseFloat(_total)));

                        }
                    }
                }
                $("#txtInvInvoiceAmount").text(toCommas(parseFloat(_nilInv)));
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INVOICE KE ATPM</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR ATPM</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="SupplierID"
                    CellSpacing="1" CellPadding="3" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                    AllowSorting="True" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="Invoice" Text='INVOICE'
                                    CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID SUPPLIER" SortExpression="SupplierID" Visible="False">
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="hypSupplierID" runat="server" Text='<%#Container.DataItem("SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA ATPM" SortExpression="SupplierName">
                            <ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hySupplierName" runat="server" Text='<%#container.dataitem("SupplierNameATPM")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KONTAK" SortExpression="ContactPersonName">
                            <ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblContactPersonName" runat="server" Text='<%#container.dataitem("contactpersonname")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TELP" SortExpression="Phone">
                            <ItemStyle HorizontalAlign="left" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPhone" runat="server" Text='<%#container.dataitem("phone")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ALAMAT" SortExpression="Address">
                            <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%#container.dataitem("address")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BRANCH ID" SortExpression="BranchID" Visible="False">
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <uc2:ucgridnav id="GridNavigator" runat="server" />
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI INVOICE</h4>
            </div>
        </div>
        <div class="form_box">
        </div>
        <div class="form_box">
            <div>
                <div class="form_single">
                    <label>
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                        <asp:ListItem Value="Address">Alamat</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="true" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlInvoice" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PROSES INVOICE SUPPLIER</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Supplier
                    </label>
                    <asp:HyperLink ID="lblInvSupplierName" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Nilai Invoice
                    </label>
                        <asp:Label ID="txtInvInvoiceAmount" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Tanggal Invoice
                    </label>
                    <uc1:ucdatece id="txtInvoiceDate" runat="server"></uc1:ucdatece>
                    <asp:Label ID="lblErrMsgInvoiceDate" runat="server" Visible="False" CssClass="validator_general"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        PPn
                    </label>
                    <asp:Label ID="txtPPnAmount" runat="server" />
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No. Invoice
                    </label>
                    <asp:Label ID="txtInvInvoiceNo" runat="server" MaxLength="25"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        PPh Ps 23
                    </label>
                    <asp:Label ID="txtPPhAmount" runat="server" />
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                </div>
                <div class="form_right">
                    <label>
                        Total
                    </label>
                    <asp:Label ID="txtTotalAmount" runat="server" />
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgListOfAgreement" runat="server" Width="100%" DataKeyField="AgreementNo"
                    CellSpacing="1" CellPadding="3" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                    AllowSorting="True" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPilih" runat="server" ></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO KONTRAK">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hynAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'
                                    NavigateUrl='' />
                                <asp:Label ID="lblAgreementNo" Visible="False" runat="server" Text='<%#container.dataitem("AgreementNo")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#container.dataitem("CustomerName")%>'
                                    NavigateUrl='' />
                                <asp:Label ID="lblCustomerID" Visible="False" runat="server" Text='<%#container.dataitem("CustomerID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL KONTRAK" Visible="False">
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblAgreementDate" runat="server" Text='<%#container.dataitem("AgreementDate")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ASSET">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblNamaAsset" runat="server" Text='<%#container.dataitem("NamaAsset")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO. RANGKA">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblNoRangka" runat="server" Text='<%#container.dataitem("NoRangka")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO. MESIN">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblNoMesin" runat="server" Text='<%#container.dataitem("NoMesin")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SUBS MARGIN">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSubsBunga" runat="server" Text='<%#container.dataitem("SubsidiBungaATPM")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SUBS DP">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblDP" runat="server" Text='<%#container.dataitem("SubsidiUangMukaATPM")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SUBS ANGS">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblAngs" runat="server" Text='<%#container.dataitem("SubsidiAngsuranATPM")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO. PO" Visible="False">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblNoPO" runat="server" Text='<%#container.dataitem("PONo")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH PEMBAYARAN" Visible="False">
                            <ItemStyle HorizontalAlign="right" />
                            <ItemTemplate>
                                <asp:Label ID="lblAPAmount" runat="server" Text='<%#formatnumber(container.dataitem("APAmount"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DeliveryOrderDate" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDeliveryOrderDate" runat="server" Text='<%#container.dataitem("DeliveryOrderDate")%>'>
                                </asp:Label>
                                <asp:Label ID="lblIsGabung" runat="server" Text='<%#container.dataitem("GabungRefundSupplier")%>' />
                                <asp:Label ID="lblRefund" runat="server" Text='<%#container.dataitem("Refund")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ApplicationID" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationID" runat="server" Text='<%#container.dataitem("ApplicationID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ApplicationStep" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationStep" runat="server" Text='<%#container.dataitem("ApplicationStep")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="APType" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAPType" runat="server" Text='<%#container.dataitem("APType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BranchID" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblBranchID" runat="server" Text='<%#container.dataitem("BranchID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
