﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PVInquiry
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbInq
    Private oController As New APDisbInqController
    Private m_controller As New DataUserControlController    
    Protected WithEvents txtTglJatuhTempo As ucDateCE
    Protected WithEvents txtTglPV As ucDateCE

    Dim Total As Double
#End Region
#Region "Properti"
    Private Property PageState() As Int32
        Get
            Return (CType(Viewstate("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            Viewstate("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(viewstate("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(viewstate("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(viewstate("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
    Private Property APDate() As String
        Get
            Return (CType(viewstate("APDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APDate") = Value
        End Set
    End Property
    Private Property Status() As String
        Get
            Return (CType(viewstate("Status"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Status") = Value
        End Set
    End Property
    Private Property PVNo() As String
        Get
            Return (CType(viewstate("PVNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVNo") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return (CType(viewstate("FilterBy"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property APTo() As String
        Get
            Return (CType(viewstate("APTo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APTo") = Value
        End Set
    End Property

    Private Property AgrNo() As String
        Get
            Return (CType(ViewState("AgrNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgrNo") = Value
        End Set
    End Property

#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            'Me.FormID = "INQAP"
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            '    Me.SearchBy = ""
            '    Me.SortBy = ""
            '    'End If
            'End If
            'If IsSingleBranch Then
            With oBranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                End If
                '.Items.Insert(0, "Select One")
                .Items.Insert(0, "Select All")
                .Items(0).Value = "0"
            End With
            bindComboAPType()
            Me.FormID = "INQPAYVCHR"
            Me.SearchBy = ""
            Me.SortBy = ""
            '  End If
        End If
        lblMessage.Visible = False
    End Sub
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False        
        txtTglJatuhTempo.Text = ""
        txtTglPV.Text = ""
    End Sub

    Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            '.Items.Insert(0, "Select One")
            .Items.Insert(0, "Select All")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.PVInquiryList(oCustomClass)

        DtUserList = oCustomClass.ListPVInquiry
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy

        With dtgEntity
            .DataSource = DtUserList
            .DataBind()
        End With

        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        
        Me.PageState = currentPage
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Branch_ID & "','" & style & "')"
    End Function
#End Region
#Region "Datagrid Command"
    Private Sub DtgEntity_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblPVAmount As New Label
        Dim lblSum, lblPVDate, lblPVDueDate As Label
          
        If e.Item.ItemIndex >= 0 Then
             
            Dim lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            Dim lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            Dim lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            Dim lblName = CType(e.Item.FindControl("lblName"), Label)
            Dim lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            Dim nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            Dim nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            Dim lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            Dim lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            Dim lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            Dim lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            Dim lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            Dim lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")

            If checkfeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, lblPaymentVoucherBranchID.Text.Trim, "Finance")
            End If

            Select Case cmbAPType.Text.ToUpper.Trim
                Case "APDST"
                    lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                Case "SPPL"
                    lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                Case "INSR"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                Case "RADV"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            End Select

            lblPVAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            lblPVDate = CType(e.Item.FindControl("lblPVDate"), Label)
            lblPVDueDate = CType(e.Item.FindControl("lblPVDueDate"), Label)
            lblPVDate.Text = CDate(lblPVDate.Text).ToString("dd/MM/yyyy")
            lblPVDueDate.Text = CDate(lblPVDueDate.Text).ToString("dd/MM/yyyy")

            Total += CDbl(lblPVAmount.Text)
            lblPVAmount.Text = FormatNumber(CDbl(lblPVAmount.Text), 0)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 0)
        End If

    End Sub

#End Region
#Region "imgSearch"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Me.SearchBy = ""
        Me.FilterBy = ""
        If oBranch.SelectedItem.Value <> "0" Then
            Me.BranchID = oBranch.SelectedItem.Value
            Me.SearchBy = Me.SearchBy + "pv.APbranchID = '" & Me.BranchID.Trim & "' and "
            Me.FilterBy = Me.FilterBy + "BranchID = " & oBranch.SelectedItem.Text.Trim & " and "
        End If
        If cmbAPType.SelectedItem.Value.Trim <> "0" Then
            Me.APType = cmbAPType.SelectedItem.Value
            'Me.SearchBy = Me.SearchBy + "pv.APType = '" & Me.APType.Trim & "' and "
            If cmbAPType.SelectedItem.Text.Trim = "Select All" And Me.sesBranchId = "'000'" Then
                Me.SearchBy = Me.SearchBy + "pv.APType in ('INSR','STNK','AFTN','KRSI','SPPL','RCST','INCS','RADV') and "
                Me.FilterBy = Me.FilterBy + "APType IN ('INSR','STNK','AFTN','KRSI','SPPL','RCST','INCS','RADV')"
            Else
                Me.SearchBy = Me.SearchBy + "pv.APType = '" & Me.APType.Trim & "' and "
                Me.FilterBy = Me.FilterBy + "APType = " & cmbAPType.SelectedItem.Text.Trim & ""
            End If
        Else
            ShowMessage(lblMessage, "Harap pilih Jenis A/P", True)
            Exit Sub
        End If
        If txtPVNo.Text.Trim <> "" Then
            Me.PVNo = txtPVNo.Text
            Me.SearchBy += " pv.PaymentVoucherNo='" & Me.PVNo & "' and "
            Me.FilterBy += " PV Number=" & Me.PVNo & " And "
        End If
        If txtTglJatuhTempo.Text.Trim <> "" Then
            Me.APDate = txtTglJatuhTempo.Text.Trim
            Me.SearchBy += " pv.PVDueDate='" & CDate(Mid(Me.APDate, 4, 2) & "/" & Left(Me.APDate, 2) & "/" & Right(Me.APDate, 4)) & "' AND "
            Me.FilterBy += " PV DueDate=" & CDate(Mid(Me.APDate, 4, 2) & "/" & Left(Me.APDate, 2) & "/" & Right(Me.APDate, 4)) & " And "
        End If
        If ddlStatus.SelectedIndex > 0 Then
            Me.Status = ddlStatus.SelectedItem.Value
            Me.SearchBy += "pv.PVStatus='" & Me.Status & "' And "
            Me.FilterBy += "Status=" & Me.Status & " And "
        End If
        If txtTglPV.Text.Trim <> "" Then
            Me.PVDate = txtTglPV.Text.Trim
            Me.SearchBy += "pv.PaymentVoucherDate='" & CDate(Mid(Me.PVDate, 4, 2) & "/" & Left(Me.PVDate, 2) & "/" & Right(Me.PVDate, 4)) & "' And "
            Me.FilterBy += "PV Date=" & Me.PVDate & " And "
        End If
        If txtSearchAP.Text.Trim <> "" Then
            Me.APTo = txtSearchAP.Text
            Me.SearchBy += "pv.APTo like '%" + Me.APTo + "%'" & " And "
            Me.FilterBy += "AP Destination like '%" + Me.APTo + "%'" & " And "
        End If
        'Tambah search by kontrak
        If txtPvAgreementNo.Text.Trim <> "" Then
            Me.AgrNo = LTrim(RTrim(txtPvAgreementNo.Text))
            Me.SearchBy += "ag.AgreementNo='" & Me.AgrNo & "' And "
            Me.FilterBy += "AgreementNo=" & Me.AgrNo & " And "
        End If

        If Me.SearchBy.Trim <> "" Then
            Me.SearchBy = Left(Me.SearchBy.Trim, Len(Me.SearchBy.Trim) - 4)
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "imgReset"
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        txtSearchAP.Text = ""
        ddlStatus.SelectedIndex = 0
        txtTglJatuhTempo.Text = ""
        txtTglPV.Text = ""
        pnlDatagrid.Visible = False
        pnlSearch.Visible = True
    End Sub
#End Region


End Class