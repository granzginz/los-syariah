﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APEBankEdit.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.APEBankEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>APEBankEdit</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />    
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';	
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">                   
                <h3>
                    KOREKSI REKENING E-BANKING
                </h3>
            </div>
    </div>
    <asp:Panel ID="pnlMain" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Pembayaran/Pencairan</label>
                        <asp:DropDownList ID="cmbAPType" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="SPPL">PO Supplier</asp:ListItem>
                            <asp:ListItem Value="INSR">Asuransi</asp:ListItem>
                            <asp:ListItem Value="INCS">Incentive Supplier</asp:ListItem>
                            <asp:ListItem Value="INCE">Incentive Karyawan</asp:ListItem>
                            <asp:ListItem Value="STNK">Birojasa STNK</asp:ListItem>
                            <asp:ListItem Value="RSPL">Refund Supplier</asp:ListItem>
                            <asp:ListItem Value="RCST">Refund Customer</asp:ListItem>
                            <asp:ListItem Value="RADV">Refund Uang Muka</asp:ListItem>
                            <asp:ListItem Value="ASRP">PO untuk Ganti Asset</asp:ListItem>
                            <asp:ListItem Value="RCAN">Refund Kontrak Batal</asp:ListItem>
                            <asp:ListItem Value="AFTN">Fiducia Notaris</asp:ListItem>
                             <asp:ListItem Value="KRSI">Karoseri</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType" InitialValue="0"  CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <label>
                            Tanggal Rencana Bayar &lt;=</label>                                           
                        <asp:TextBox ID="txtTglJatuhTempo" runat="server"></asp:TextBox>                   
                        <asp:CalendarExtender ID="txtTglJatuhTempo_CalendarExtender" runat="server" CssClass="validator_general"
                        Enabled="True" TargetControlID="txtTglJatuhTempo" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtTglJatuhTempo"></asp:RequiredFieldValidator> 
                    </ContentTemplate>
                    </asp:UpdatePanel>                                           
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server" HorizontalAlign="Center">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PEMBAYARAN YANG DITOLAK</h4>
                </div>
            </div>
            <div class="form_box_header">
            <div class="form_single">
            <div class="grid_wrapper_ns"> 
                    <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                        DataKeyField="PaymentVoucherNo" BorderWidth="0"
                        AutoGenerateColumns="False" AllowSorting="True"  ShowFooter="True">
                        <HeaderStyle CssClass="th" />
		                <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkApp" runat="server" CommandName="approve">PILIH</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APType" HeaderText="JENIS PEMBAYARAN/PENCAIRAN">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAPType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APType") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchInitialName" HeaderText="BRANCH">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBranch" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchInitialName") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APTo" HeaderText="BENEFICIARY">
                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                    </asp:HyperLink><br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    Grand Total
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                    </asp:Label>
                                </ItemTemplate>                                
                                <FooterTemplate>                                                                                                            
                                    <asp:Label ID="lblSum" CssClass="item_grid_right" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankNameTo" HeaderText="BANK" Visible="false">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankBranchTo" HeaderText="CABANG" Visible="false">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountTo" HeaderText="NO REKENING" Visible="false">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NAMA REKENING" Visible="false">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV" Visible="false">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO KONTRAK-CUSTOMER" Visible="false">
                               
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="PV" Visible="false">
                               
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCompanyFullname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyFullname") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCompanyAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyAddress") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblSandiKliringPusat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SandiKliringPusat") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblPaymentNote" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentNote") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblJenisTransfer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JenisTransfer") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankIdTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankIdTo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                    </asp:Label>
                                     <asp:Label ID="lblBankCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SandiKliringPusat") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>      
                    <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                    <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>          
            </div>
            </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDetail" runat="server" Visible="False">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        KOREKSI REKENING BANK e-BANKING</h4>
                </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Supplier</label>
                    <asp:Label runat="server" ID="lblSupplier"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Nama Bank</label>
                    <asp:Label runat="server" ID="lblNmBank"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Sandi Bank</label>
                    <asp:Label runat="server" ID="lblSandiBank"></asp:Label>
	            </div>  
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Nama Cabang Bank</label>
                    <asp:Label runat="server" ID="lblNmCabangBank"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Sandi Cabang</label>
                    <asp:Label runat="server" ID="lblSandiCabang"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Kota</label>
                    <asp:Label runat="server" ID="lblKota"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>No Rekening</label>
                    <asp:Label runat="server" ID="lblNoRekening"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Nama Rekening</label>
                    <asp:Label runat="server" ID="lblNamaRekening"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Rekening Default untuk pembayaran</label>
                    <asp:CheckBox runat="server" Enabled="false" ID="chekDefault" />
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Untuk Pembayaran</label>
                    <asp:Label runat="server" ID="lblUntukPembayaran"></asp:Label>
	            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <label>Efektif Tanggal</label>
                    <asp:Label runat="server" ID="lblEfektifTanggal"></asp:Label>
	            </div>
            </div>
            <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR DETAIL KOREKSI
                </h4>
            </div>
            </div>
            <div class="form_box_header">
            <div class="form_single">
            <div class="grid_wrapper_ns"> 
                    <asp:DataGrid ID="dtgDetailEditBank" runat="server" CssClass="grid_general" 
                        DataKeyField="PaymentVoucherNo" BorderWidth="0"
                        AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
		                <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="Chek" />
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                            <asp:TemplateColumn HeaderText="JENIS A/P">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAPType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APType") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BRANCH">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBranch" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchInitialName") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BENEFICIARY">
                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                    </asp:HyperLink><br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    Grand Total
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH">
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                    </asp:Label>
                                </ItemTemplate>                                
                                <FooterTemplate>
                                    <asp:Label ID="lblSum" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BANK">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CABANG">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO REKENING">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA REKENING">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>                                                        
                            <asp:TemplateColumn HeaderText="NO KONTRAK-CUSTOMER">
                               
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PV">
                               
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                
                                <ItemTemplate>                                    
                                    <asp:Label ID="lblBankCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SandiKliringPusat") %>'/>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'/>                                                                        
                                    <asp:Label ID="lblAccountPayableno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.accountpayableno") %>'/>       
                                    <asp:Label ID="lblAccountPayableBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'/>       
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                        </Columns>
                    </asp:DataGrid>    
            </div>
            </div>
            </div>                    
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" CssClass="small button blue" Text="Save">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>        
        <asp:Panel runat="server" ID="pnlRequestH">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR REQUEST </h4>
                </div>
            </div>
            <div class="form_box_header">
            <div class="form_single">
            <div class="grid_wrapper_ns"> 
                    <asp:DataGrid ID="dgRequsetH" runat="server" CssClass="grid_general"
                        DataKeyField="APTo" BorderWidth="0"
                        AutoGenerateColumns="False" AllowSorting="True"  ShowFooter="True">
                        <HeaderStyle CssClass="th" />
		                <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReq" runat="server" CommandName="request">REQUEST</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="APType"  HeaderText="PEMBAYARAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="APBranchId"  HeaderText="BRANCH">
                            </asp:BoundColumn>                            
                            <asp:TemplateColumn SortExpression="APTo" HeaderText="BENEFICIARY">                               
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkSupplier" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            </asp:TemplateColumn>                                                                   
                            <asp:BoundColumn DataField="SupplierID"  Visible="false">
                            </asp:BoundColumn>     
                            <asp:BoundColumn DataField="AlasanPenolakan"  HeaderText="ALASAN PENOLAKAN">
                            </asp:BoundColumn>  
                        </Columns>
                    </asp:DataGrid>                          
            </div>
            </div>
            </div>            
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRequestD">
         <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR REQUEST</h4>
                </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Supplier</label>                
                <asp:HyperLink ID="lnkSupplier" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bank Account No</label>
                <asp:Label runat="server" ID="lblAccountNo"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bank Account Name</label>
                <asp:Label runat="server" ID="lblAccountName"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Pembayaran</label>
                <asp:Label runat="server" ID="lblJenisPembayaran"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Branch</label>
                <asp:Label runat="server" ID="lblBranch"></asp:Label>
	        </div>
        </div>        
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns"> 
                <asp:DataGrid ID="dgRequsetD" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                    DataKeyField="PaymentVoucherNo" BorderWidth="0"
                    AutoGenerateColumns="False" AllowSorting="True"  ShowFooter="True">
                    <HeaderStyle CssClass="th" />
		            <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle CssClass="command_col"></ItemStyle>
                            <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chekID"/>
                            </ItemTemplate>
                        </asp:TemplateColumn>                        
                        <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH">
                                
                            <ItemTemplate>
                                <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                </asp:Label>
                            </ItemTemplate>                                
                            <FooterTemplate>
                                <asp:Label ID="lblSum" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankNameTo" HeaderText="BANK">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankBranchTo" HeaderText="CABANG">
                              
                            <ItemTemplate>
                                <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankAccountTo" HeaderText="NO REKENING">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NAMA REKENING">
                              
                            <ItemTemplate>
                                <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV" Visible="false">
                                
                            <ItemTemplate>
                                <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO KONTRAK-CUSTOMER">
                               
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="PV">
                               
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">                                
                            <ItemTemplate>
                                <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                </asp:Label>                                                                
                                <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCompanyFullname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyFullname") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCompanyAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyAddress") %>'>
                                </asp:Label>
                                <asp:Label ID="lblSandiKliringPusat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SandiKliringPusat") %>'>
                                </asp:Label>
                                <asp:Label ID="lblPaymentNote" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentNote") %>'>
                                </asp:Label>
                                <asp:Label ID="lblJenisTransfer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JenisTransfer") %>'>
                                </asp:Label>
                                <asp:Label ID="lblBankIdTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankIdTo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCustomerName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                </asp:Label>
                                     
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>                          
        </div>
        </div>
        </div>
        <div class="form_button">
                <asp:Button ID="ButtonSaveRequestD" runat="server" CssClass="small button blue" Text="Save"></asp:Button>
                <asp:Button ID="ButtonCancelRequestD" runat="server" CssClass="small button gray" Text="Cancel" CausesValidation="False"></asp:Button>
        </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlApprovalH">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR APPROVAL</h4>
                </div>
            </div>
            <div class="form_box_header">
            <div class="form_single">
            <div class="grid_wrapper_ns"> 
                    <asp:DataGrid ID="dgApprovalH" runat="server" CssClass="grid_general"
                        DataKeyField="APTo" BorderWidth="0"
                        AutoGenerateColumns="False" AllowSorting="True"  ShowFooter="True">
                        <HeaderStyle CssClass="th" />
		                <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkApp" runat="server" CommandName="approval">APPROVE</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="APType"  HeaderText="PEMBAYARAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="APBranchId"  HeaderText="BRANCH">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="APTo" HeaderText="BENEFICIARY">                               
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkSupplier" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            </asp:TemplateColumn>                                                                   
                            <asp:BoundColumn DataField="SupplierID"  Visible="false">
                            </asp:BoundColumn>                                  
                            <asp:BoundColumn DataField="AlasanPenolakan"  HeaderText="ALASAN PENOLAKAN">
                            </asp:BoundColumn>               
                        </Columns>
                    </asp:DataGrid>                          
            </div>
            </div>
            </div>            
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlApprovalD">
         <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR APPROVAL</h4>
                </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Supplier</label>                
                <asp:HyperLink ID="lnkSupplierApproval" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bank Account No</label>
                <asp:Label runat="server" ID="lblAccountNoApproval"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bank Account Name</label>
                <asp:Label runat="server" ID="lblAccountNameApproval"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Pembayaran</label>
                <asp:Label runat="server" ID="lblJenisPembayaranApproval"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Branch</label>
                <asp:Label runat="server" ID="lblBranchApproval"></asp:Label>
	        </div>
        </div>        
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns"> 
                <asp:DataGrid ID="dgApprovalD" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                    DataKeyField="PaymentVoucherNo" BorderWidth="0"
                    AutoGenerateColumns="False" AllowSorting="True"  ShowFooter="True">
                    <HeaderStyle CssClass="th" />
		            <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle CssClass="command_col"></ItemStyle>
                            <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chekID"/>
                            </ItemTemplate>
                        </asp:TemplateColumn>                        
                        <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH">
                                
                            <ItemTemplate>
                                <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                </asp:Label>
                            </ItemTemplate>                                
                            <FooterTemplate>
                                <asp:Label ID="lblSum" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankNameTo" HeaderText="BANK">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblBankNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankNameTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankBranchTo" HeaderText="CABANG">
                              
                            <ItemTemplate>
                                <asp:Label ID="lblBankBranchTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BankAccountTo" HeaderText="NO REKENING">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblBankAccountTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankAccountTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NAMA REKENING">
                              
                            <ItemTemplate>
                                <asp:Label ID="lblAccountNameTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameTo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV" Visible="false">
                                
                            <ItemTemplate>
                                <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO KONTRAK-CUSTOMER">
                               
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="PV">
                               
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                
                            <ItemTemplate>
                                <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                               
                            <ItemTemplate>
                                <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCompanyFullname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyFullname") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCompanyAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyAddress") %>'>
                                </asp:Label>
                                <asp:Label ID="lblSandiKliringPusat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SandiKliringPusat") %>'>
                                </asp:Label>
                                <asp:Label ID="lblPaymentNote" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentNote") %>'>
                                </asp:Label>
                                <asp:Label ID="lblJenisTransfer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JenisTransfer") %>'>
                                </asp:Label>
                                <asp:Label ID="lblBankIdTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BankIdTo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCustomerName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                </asp:Label>
                                     
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>                          
        </div>
        </div>
        </div>
        <div class="form_button">
                <asp:Button ID="ButtonSaveApproval" runat="server" CssClass="small button blue" Text="Save"></asp:Button>
                <asp:Button ID="ButtonCancelApproval" runat="server" CssClass="small button gray" Text="Cancel" CausesValidation="False"></asp:Button>
        </div>
        </asp:Panel>
    </asp:Panel>    
    <input id="hdnBankID" type="hidden" name="hdnBankID" runat="server" />
    <input id="hdnBankBranchID" type="hidden" name="hdnBankBranchID" runat="server" />
    </form>     
</body>
</html>
