﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewPaymentVoucher
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oViewDetailItem As ucAPDetailItem
    Protected WithEvents oViewAPBeneficiary As ucAPBeneficiary
#Region "Properti"
    Private Property ListData() As DataTable
        Get
            Return (CType(Viewstate("ListData"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            Viewstate("ListData") = Value
        End Set
    End Property
    Private Property APTo() As String
        Get
            Return (CType(viewstate("APTo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APTo") = Value
        End Set
    End Property

    Private Property DueDate() As String
        Get
            Return (CType(viewstate("DueDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
    Private Property AccountName() As String
        Get
            Return (CType(viewstate("AccountName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AccountName") = Value
        End Set
    End Property
    Private Property BankName() As String
        Get
            Return (CType(viewstate("BankName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property
    Private Property AccountNumber() As String
        Get
            Return (CType(viewstate("AccountNumber"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNumber") = Value
        End Set
    End Property
    Private Property BankBranch() As String
        Get
            Return (CType(viewstate("BankBranch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankBranch") = Value
        End Set
    End Property
    Private Property Branch() As String
        Get
            Return (CType(viewstate("Branch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch") = Value
        End Set
    End Property
    Private Property PVNo() As String
        Get
            Return (CType(viewstate("PVNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVNo") = Value
        End Set
    End Property
    Private Property WOP() As String
        Get
            Return (CType(viewstate("WOP"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WOP") = Value
        End Set
    End Property
    Private Property RequestBy() As String
        Get
            Return (CType(viewstate("RequestBy"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("RequestBy") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(viewstate("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
    Private Property BankAccount() As String
        Get
            Return (CType(viewstate("BankAccount"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccount") = Value
        End Set
    End Property
    Private Property Balance() As String
        Get
            Return (CType(viewstate("Balance"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Balance") = Value
        End Set
    End Property
    Private Property Notes() As String
        Get
            Return (CType(viewstate("Notes"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(viewstate("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(viewstate("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
#End Region
#Region "GetValue"
    Sub GetValue()
        Me.PaymentVoucherNo = Request("PaymentVoucherNo")
        Me.Branch_ID = Request("BranchID")
        Me.DueDate = Request("DueDate")
        Me.AccountName = Request("AccountName")
        Me.BankName = Request("BankName")
        Me.AccountNumber = Request("AccountNumber")
        Me.BankBranch = Request("BankBranch")
        Me.Branch = Request("Branch")
        Me.PVNo = Request("PVNo")
        Me.WOP = Request("WOP")
        Me.RequestBy = Request("RequestBy")
        Me.APType = Request("APType")
        Me.PVDate = Request("PVDate")
        Me.BankAccount = Request("BankAccount")
        Me.Balance = Request("Balance")
        Me.Notes = Request("Notes")
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            GetValue()
            Bind()
        End If
    End Sub

    Sub Bind()
        Dim DView As New DataView
        With oCustomClass
            .PaymentVoucherNo = Me.PaymentVoucherNo
            .BranchId = Me.Branch_ID
            .strConnection = GetConnectionString
        End With

        oCustomClass = oController.APDisbAppInformasi(oCustomClass)
        Me.ListData = oCustomClass.ListAPAppr
        DView = Me.ListData.DefaultView

        If DView.Count > 0 Then
            With oViewDetailItem
                .ListData = Me.ListData
                .APTo = CStr(DView.Item(0)("APTo"))
                .DueDate = CStr(DView.Item(0)("PVDueDate"))
                .BindAPDetail()
            End With
            With oViewAPBeneficiary
                .AccountName = CStr(DView.Item(0)("AccountNameTo"))
                .BankName = CStr(DView.Item(0)("BankNameTo"))
                .AccountNumber = CStr(DView.Item(0)("BankAccountTo"))
                .BankBranch = CStr(DView.Item(0)("BankBranchTo"))
                .BindAPBeneficiary()
            End With

            lblBranch.Text = CStr(DView.Item(0)("BranchFullName"))
            lblPVNo.Text = CStr(DView.Item(0)("PaymentVoucherNo"))
            lblWOP.Text = CStr(DView.Item(0)("PaymentTypeName"))
            'If CStr(DView.Item(0)("PaymentTypeID")) = "CA" Then
            '    lblWOP.Text = "Cash"
            'ElseIf CStr(DView.Item(0)("PaymentTypeID")) = "BA" Then
            '    lblWOP.Text = "Bank"
            'ElseIf CStr(DView.Item(0)("PaymentTypeID")) = "GT" Then
            '    lblWOP.Text = "Generate Text File (Bank)"
            'End If

            lblRequestBy.Text = CStr(DView.Item(0)("RequestBy"))

            If CStr(DView.Item(0)("APType")) = "SPPL" Then
                lblAPType.Text = "PO Supplier"
            ElseIf CStr(DView.Item(0)("APType")) = "INSR" Then
                lblAPType.Text = "AP To Insurance Co"
            ElseIf CStr(DView.Item(0)("APType")) = "INCS" Then
                lblAPType.Text = "Incentive Supplier"
            ElseIf CStr(DView.Item(0)("APType")) = "INCE" Then
                lblAPType.Text = "Incentive Employee"
            ElseIf CStr(DView.Item(0)("APType")) = "STNK" Then
                lblAPType.Text = "AP To STNK Agent"
            ElseIf CStr(DView.Item(0)("APType")) = "RCST" Then
                lblAPType.Text = "Refund To Customer"
            ElseIf CStr(DView.Item(0)("APType")) = "ASRD" Then
                lblAPType.Text = "PO for Asset Replacementr"
            ElseIf CStr(DView.Item(0)("APType")) = "ATFC" Then
                lblAPType.Text = "AP To Funding Co"
            ElseIf CStr(DView.Item(0)("APType")) = "AFTN" Then
                lblAPType.Text = "AP Fiducia to Notary"
            End If

            lblPVDate.Text = CStr(DView.Item(0)("PaymentVoucherDate"))
            lblBankAccount.Text = CStr(DView.Item(0)("BankAccountName"))
            lblBGNo.Text = CStr(DView.Item(0)("BGNo"))

            If CStr(DView.Item(0)("BGDueDate")) = "1/1/1900" Then
                lblBGDueDate.Text = "-"
            End If

            lblRecipientName.Text = CStr(DView.Item(0)("RecipientName"))
            lblPaymentNotes.Text = CStr(DView.Item(0)("PaymentNotes"))
            lblStatus.Text = CStr(DView.Item(0)("PVStatusDesc"))
            'If CStr(DView.Item(0)("PVStatus")) = "R" Then
            '    lblStatus.Text = "Request"
            'ElseIf CStr(DView.Item(0)("PVStatus")) = "A" Then
            '    lblStatus.Text = "Approved"
            'ElseIf CStr(DView.Item(0)("PVStatus")) = "P" Then
            '    lblStatus.Text = "Paid"
            'ElseIf CStr(DView.Item(0)("PVStatus")) = "D" Then
            '    lblStatus.Text = "Decline"
            'End If

            lblRequestDate.Text = CStr(DView.Item(0)("RequestDate"))
            lblStatusDate.Text = CStr(DView.Item(0)("StatusDate"))
            lblRequestBy.Text = CStr(DView.Item(0)("RequestBy"))
            lblApprovalBy.Text = CStr(DView.Item(0)("ApprovalBy"))

            If CStr(DView.Item(0)("ApprovalDate")) = "1/1/1900" Then
                lblApprovalDate.Text = "-"
            End If
            lblNotes.Text = CStr(DView.Item(0)("ApprovalNotes"))
        End If

    End Sub


End Class