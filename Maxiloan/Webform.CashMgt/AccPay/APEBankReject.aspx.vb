﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class APEBankReject
    Inherits Maxiloan.Webform.WebBased

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    'Protected WithEvents oPVDueDate As ValidDate
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Private EBankController As New EBankTransferController
    Protected WithEvents txtJumlah As ucNumberFormat
    Private m_controller As New DataUserControlController

    Dim Total As Double

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property ApplicationId() As String
        Get
            Return (CType(ViewState("ApplicationId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return (CType(ViewState("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "EBREJE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            bindComboAPType()
            If Me.IsHoBranch Then
                ButtonSave.Visible = True

                With oBranch
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
            Else
                With oBranch
                    .DataSource = m_controller.GetBranchName(GetConnectionString, oBranch.SelectedValue.Trim)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                ButtonSave.Visible = False
                ShowMessage(lblMessage, "Harap login di Kantor Pusat!", True)

            End If
        End If
        lblMessage.Visible = False
    End Sub
    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False        
        'With oPVDueDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi Tanggal Payment Voucher"
        '    .Display = "Dynamic"
        '    .ValidationErrMessage = "Harap isi dengan format dd/MM/yyyy"
        '    .isCalendarPostBack = False
        '    .dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
        'End With
        txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        pnlDetail.Visible = False
        With txtJumlah
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
        End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        Dim dtPVno As DataTable = createNewPVnoDT()
        dtPVno.Rows.Add(1, String.Empty, String.Empty)
        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .BusinessDate = Me.BusinessDate
            .BranchId = oBranch.SelectedValue.Trim
            .PVnoDT = dtPVno
            .SortBy = SortBy
        End With

        EBankData = EBankController.EBankTransferList2(EBankData)


        DtUserList = EBankData.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = EBankData.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch ex As Exception
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlDetail.Visible = False
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        
        Me.PageState = currentPage
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = oBranch.SelectedValue.Trim
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            End If

            If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
                'lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                Select Case cmbAPType.SelectedItem.Value
                    Case "SPPL"
                        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                    Case "INSR"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                    Case "RCST"
                        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

                    Case "RADV"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
                    Case "ASRP"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                End Select

            End If


            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If

    End Sub

    Private Sub DtgEntity_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Select Case e.CommandName
            Case "approve"
                If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
                    pnlDetail.Visible = True
                    pnlDatagrid.Visible = False
                    pnlSearch.Visible = False

                    Dim DtUserList As New DataTable
                    Dim DvUserList As New DataView
                    Dim lnkPVNo As HyperLink
                    'lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)

                    'With oCustomClass
                    '    .PaymentVoucherNo = lnkPVNo.Text.Trim
                    '    .BranchId = oBranch.SelectedValue.Trim
                    '    .strConnection = GetConnectionString()
                    'End With

                    'oCustomClass = oController.APDisbAppInformasi(oCustomClass)

                    GridToDetail(e.Item)
                End If

        End Select
    End Sub

#End Region

    Private Sub GridToDetail(ByVal dgitem As DataGridItem)
        Dim lblAPType As Label = CType(dgitem.FindControl("lblAPType"), Label)
        Dim lblBranch As Label = CType(dgitem.FindControl("lblBranch"), Label)
        Dim lnkAPTO As HyperLink = CType(dgitem.FindControl("lnkAPTO"), HyperLink)
        Dim lblPVAmount As Label = CType(dgitem.FindControl("lblPVAmount"), Label)
        Dim lblBankNameTo As Label = CType(dgitem.FindControl("lblBankNameTo"), Label)
        Dim lblBankBranchTo As Label = CType(dgitem.FindControl("lblBankBranchTo"), Label)
        Dim lblBankAccountTo As Label = CType(dgitem.FindControl("lblBankAccountTo"), Label)
        Dim lblAccountNameTo As Label = CType(dgitem.FindControl("lblAccountNameTo"), Label)
        Dim lblAgreementNo As Label = CType(dgitem.FindControl("lblAgreementNo"), Label)
        Dim lblCustomerName As Label = CType(dgitem.FindControl("lblCustomerName"), Label)
        Dim lnkPVNo As HyperLink = CType(dgitem.FindControl("lnkPVNo"), HyperLink)
        Dim lblApplicationId As Label = CType(dgitem.FindControl("lblApplicationId"), Label)
        Dim lblBankAccountID As Label = CType(dgitem.FindControl("lblBankAccountID"), Label)

        txtJenisPembayaran.Text = lblAPType.Text
        txtBeneficiary.Text = lnkAPTO.Text
        txtJumlah.Text = lblPVAmount.Text
        txtBank.Text = lblBankNameTo.text
        txtCabang.Text = lblBranch.Text
        txtNoRek.Text = lblBankAccountTo.Text
        txtNamaRek.Text = lblAccountNameTo.Text
        txtNoKontrak.Text = lblAgreementNo.Text
        txtNamaCustomer.Text = lblCustomerName.text
        txtNoPV.Text = lnkPVNo.Text
        ApplicationId = lblApplicationId.Text
        BankAccountID = lblBankAccountID.Text.Trim
    End Sub

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click

        Dim strSearch As New StringBuilder
        If txtTglJatuhTempo.Text <> "" Then
            If IsDate(ConvertDate2(txtTglJatuhTempo.Text)) Then

                Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglJatuhTempo.Text
                strSearch.Append("  pv.aptype='" & Me.APType & "'")
                strSearch.Append(" and pv.PVDueDate ='" & ConvertDate2(Me.PVDate) & "'")
                If txtSearch.Text.Trim <> "" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        strSearch.Append(" and pv.apto like '" & txtSearch.Text & "'")
                    Else
                        strSearch.Append(" and pv.apto='" & txtSearch.Text & "'")
                    End If
                End If
                strSearch.Append(" and pv.APBranchId = '" & oBranch.SelectedValue.Trim & "'")
                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            Else                
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else            
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        txtTglJatuhTempo.Text = ""
        cmbAPType.SelectedIndex = 0
        txtSearch.Text = ""
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        If Not CheckFeature(Me.Loginid, Me.FormID, "REJE", Me.AppId) Then
            ShowMessage(lblMessage, "Tidak memiliki akses untuk Reject Data", True)
            Exit Sub
        End If

        Dim dtPVno As DataTable = createNewPVnoDT()
        dtPVno.Rows.Add(1, txtNoPV.Text, String.Empty)

        DoProcessReject()

        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .BusinessDate = Me.BusinessDate
            .BranchId = sesBranchId.Replace("'", "")
            .PVnoDT = dtPVno
            .StatusFlag = "D"
            .RejectBy = Me.Loginid
            .PaidDate = BusinessDate
            .GenerateDate = BusinessDate
            .EditedDate = BusinessDate
            .DeleteDate = BusinessDate
            .RejectDate = BusinessDate
        End With
        Dim err As String = EBankController.EBankTransferChangeStatus(EBankData)
        If err <> "" Then
            ShowMessage(lblMessage, err, True)
        Else
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Reject Berhasil", False)
        End If
    End Sub

    Private Sub DoProcessReject()
        Dim data As Parameter.EBankReject = UItoData()

        With data
            .BranchId = sesBranchId.Replace("'", "")
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
        End With

        'insert to table rejected        
        lblMessage.Text = EBankController.EBankRejectSave(data)

        Dim data2 As Parameter.APDisbApp = UItoData_DisbApp()

        With data2
            .BranchId = sesBranchId.Replace("'", "")
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
        End With

        'do certain procedure to rollback the execution                
        lblMessage.Text = EBankController.EBankExecuteReject(data2)

        If lblMessage.Text <> "" Then
            ShowMessage(lblMessage, lblMessage.Text, True)
        End If
    End Sub

    Private Function UItoData() As Parameter.EBankReject
        Dim data As New Parameter.EBankReject
        With data
            .TransactionRefNo = txtNoPV.Text
            .JenisPembayaran = txtJenisPembayaran.Text
            .Beneficiary = txtBeneficiary.Text
            .Jumlah = txtJumlah.Text
            .Bank = txtBank.Text
            .Cabang = txtCabang.Text
            .NoRekening = txtNoRek.Text
            .NamaRekening = txtNamaRek.Text
            .NoKontrak = txtNoKontrak.Text
            .NamaCustomer = txtNamaCustomer.Text
            .AlasanPenolakan = cmbAlasanPenolakan.SelectedValue

        End With

        Return data
    End Function

    Private Function UItoData_DisbApp() As Parameter.APDisbApp
        Dim data As New Parameter.APDisbApp
        With data
            .PaymentVoucherNo = txtNoPV.Text
            .BankAccountID = BankAccountID
            .BGNo = String.Empty
            .PVStatus = "F"
            .RecipientName = txtBeneficiary.Text
            .PaymentNote = String.Empty
            .approvalBy = Loginid
            .WOP = "GT"
            .PVAmount = txtJumlah.Text
            .APType = cmbAPType.SelectedValue
            .BusinessDate = BusinessDate
            .PVDate = CDate(Mid(PVDate, 4, 2) & "/" & Left(PVDate, 2) & "/" & Right(PVDate, 4))
            .ApplicationID = ApplicationId
            .ReferenceNo = txtNoPV.Text

        End With

        Return data
    End Function

    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function


    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
    End Sub
End Class