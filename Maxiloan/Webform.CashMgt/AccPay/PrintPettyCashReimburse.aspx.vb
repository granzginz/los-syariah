﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class PrintPettyCashReimburse
    Inherits AbsApDisbApp
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents Branch As UcBranch
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private CustomClass As New Parameter.APDisbApp
    Private m_TotalAmount As Double
#End Region

#Region "property"
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property RequestNo() As String
        Get
            Return (CType(ViewState("RequestNo"), String))
        End Get
        Set(value As String)
            ViewState("RequestNo") = value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Public Property GM1 As String
    Public Property GM2 As String
    Public Property GM3 As String
    Public Property Direksi As String
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "PAYREQUESTYLOW"

        'AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If


            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
                Dim strFileLocation = String.Format("http://{0}/{1}/XML/{2}.pdf", Request.ServerVariables("SERVER_NAME"), strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1), Request.QueryString("strFileLocation"))
                Response.Write("<script language = javascript>" & vbCrLf & "var x = screen.width; " & vbCrLf & "var y = screen.heigth; " & vbCrLf & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf & "</script>")
            End If

            With cboParent
                .DataValueField = "ID"
                .DataTextField = "Name"
                If Me.IsHoBranch Then
                    .DataSource = (New DataUserControlController).GetBranchAll(GetConnectionString)
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(0).Value = "ALL"
                Else
                    Dim dtbranch As New DataTable
                    dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    If Not (dtbranch Is Nothing) Then
                        .Items.Insert(1, dtbranch.Rows(0)(1))
                        .Items(1).Value = Me.sesBranchId
                    End If
                End If


            End With

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            PagingFooter()
        End If

    End Sub


    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        'DoBind()
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy)
            End If
        End If
    End Sub
#End Region


    Sub DoBind(ByVal SearchBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Try
            Dim Application As New Parameter.APDisbApp
            Dim DataList As New List(Of Parameter.APDisbApp)
            With CustomClass
                .strConnection = GetConnectionString()
                .WhereCond = SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
            End With
            Dim dtEntity As New DataTable
            CustomClass = GetPagingPettyCash(CustomClass)

            recordCount = CustomClass.TotalRecord
            DtUserList = CustomClass.APDetail
            DvUserList = DtUserList.DefaultView
            DtgAgree.DataSource = DvUserList
            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try

            'If (isFrNav = False) Then
            '    GridNavigator.Initialize(recordCount, pageSize)
            'End If

            pnlsearch.Visible = True
            pnlDatagrid.Visible = True


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.Source + " " + ex.StackTrace, True)
        End Try
    End Sub



    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oBranch.DataBind()
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        pnlDatagrid.Visible = False
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String
        strFilterBy = ""
        Me.BranchID = cboParent.SelectedItem.Value.Trim
        strFilterBy = String.Format("pcr.Branchid = {0}", Me.BranchID)
        'Me.SearchBy = String.Format(" pcr.status = 'CAB' and " & strFilterBy)
        Me.SearchBy = String.Format(strFilterBy)

        If cboSearchBy.SelectedItem.Value <> "0" Then
            Me.SearchBy = String.Format("{0} and {1} Like '%{2}%' ", Me.SearchBy, cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim)
            strFilterBy = strFilterBy & ", " & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
        End If
        If cboParent.SelectedItem.Value = "ALL" Then
            Me.SearchBy = ""
            'Me.SearchBy = String.Format(" pcr.status = 'CAB'")
        End If


        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlSearch.Visible = True
        DoBind(Me.SearchBy)
        FillCbo(author1, 1)
        FillCbo(author2, 2)
        FillCbo(author3, 3)
        FillCbo(author4, 4)
    End Sub

    Public Function GetPagingPettyCash(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = CustomClass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = CustomClass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = CustomClass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = CustomClass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output


            CustomClass.APDetail = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, "spprintpettycashreimbursepaging", params).Tables(0)
            CustomClass.TotalRecord = CInt(params(4).Value)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
        Return Nothing
    End Function

    Public Function GetEmployee(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 30)
        params(0).Value = CustomClass.BranchId
        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 30)
        params(1).Value = CustomClass.EmployeePosition


        Try
            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Protected Sub FillCbo(ByVal cboName As DropDownList, ByVal EmployeePosition As String)
        Dim customClass As New Parameter.APDisbApp
        With customClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .EmployeePosition = EmployeePosition
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = ""
            .SpName = "spCboGetEmployee"
        End With
        customClass = GetEmployee(customClass)
        Dim dt As DataTable
        dt = customClass.listDataReport
        With cboName
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "ASC", "DESC"))
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label
        Dim lblRequestNo As New Label



        Me.RequestNo = lblRequestNo.Text.Trim
        If e.Item.ItemIndex >= 0 Then
            lblTemp = CType(e.Item.FindControl("lblAmount"), Label)
            lblRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label)
            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblTotalAmount"), Label)
            lblRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If
    End Sub
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Try
            Dim RequestNo As String = CType(e.Item.FindControl("lblRequestNo"), Label).Text.Trim
            If e.CommandName.Trim = "Verifikasi" Then
                Dim hyRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label).Text.Trim

                pnlSearch.Visible = False
                pnlDatagrid.Visible = False

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim loopitem As Int16
        For loopitem = 0 To CType(DtgAgree.Items.Count - 1, Int16)
            Dim Chk = CType(DtgAgree.Items(loopitem).FindControl("cbCheck"), CheckBox)
            If Chk.Enabled Then
                Chk.Checked = CType(sender, CheckBox).Checked
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        Dim j = 0
        Dim dtChoosen As IList(Of String) = New List(Of String)
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim lblRequestNo As String
        Dim cbCheck As CheckBox
        With oDataTable
            .Columns.Add(New DataColumn("RequestNo", GetType(String)))
        End With
        For intloop = 0 To DtgAgree.Items.Count - 1
            cbCheck = CType(DtgAgree.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
            If cbCheck.Checked Then

                lblRequestNo = CType(DtgAgree.Items(intloop).Cells(1).FindControl("lblRequestNo"), Label).Text.Trim
                GM1 = author1.SelectedValue.Trim
                GM2 = author2.SelectedValue.Trim
                GM3 = author3.SelectedValue.Trim
                Direksi = author4.SelectedValue.Trim

                Me.RequestNo = lblRequestNo
                Me.GM1 = GM1
                Me.GM2 = GM2
                Me.GM3 = GM3
                Me.Direksi = Direksi
            End If
        Next
        For i = 0 To DtgAgree.Items.Count - 1
            Dim cb = CType(DtgAgree.Items(i).FindControl("cbCheck"), CheckBox)
            If cb.Checked = True Then
                j += 1
                dtChoosen.Add(CType(DtgAgree.Items(i).FindControl("lblRequestNo"), Label).Text.Trim)
            End If
        Next
        If j <= 0 Then
            ShowMessage(lblMessage, "Harap Check Item", True)
            Exit Sub
        End If
        Session(COOKIES_PETTY_CASH_VOUCHER) = dtChoosen
        Dim cookie As HttpCookie = Request.Cookies("RptPmtPembayaranSPPL")
        If Not cookie Is Nothing Then
            cookie.Values("RequestNo") = Me.RequestNo
            cookie.Values("GM1") = Me.GM1
            cookie.Values("GM2") = Me.GM2
            cookie.Values("GM3") = Me.GM3
            cookie.Values("Direksi") = Me.Direksi
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("RptPmtPembayaranSPPL")
            cookieNew.Values.Add("RequestNo", Me.RequestNo)
            cookieNew.Values("GM1") = Me.GM1
            cookieNew.Values("GM2") = Me.GM2
            cookieNew.Values("GM3") = Me.GM3
            cookieNew.Values("Direksi") = Me.Direksi
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("CetakPettyCash.aspx")
    End Sub
End Class