﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class BGMaintenance
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.BGMnt
    Private oController As New BGMntController

    Private oClassAddress As New Parameter.Address
    Private oClassPersonal As New Parameter.Personal
    Protected WithEvents oSearchBy As UcSearchBy
    'Protected WithEvents txtSumBG As ucNumberFormat



#End Region

#Region "Property"

    Private Property PageNumber() As String
        Get
            Return (CType(ViewState("page"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("page") = Value
        End Set
    End Property

    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property BankAccountID() As String
        Get
            Return (CType(ViewState("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property

    Private Property BankAccountName() As String
        Get
            Return (CType(ViewState("BankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountName") = Value
        End Set
    End Property

    Private Property Status() As String
        Get
            Return (CType(ViewState("Status"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property

    Private Property BGNo() As String
        Get
            Return (CType(ViewState("BGNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BGNo") = Value
        End Set
    End Property

    'for manual/auto generate
    Private Property isGeneratedManually() As Boolean
        Get
            Return (CType(ViewState("isGeneratedManually"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isGeneratedManually") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "BGMaintenance"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            isGeneratedManually = True
        End If
        RoutStyl()
    End Sub

    Sub EnabledSearch(ByVal Flag As Boolean)
        cmbBankAccount.Enabled = Flag
        ddlStatus.Enabled = Flag
        txtBG.Enabled = Flag
    End Sub

    Private Sub FillCmbBankAccount()
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass = oController.BGMntCmbBankAccount(oCustomClass)

        Dim dtBankAccount As DataTable
        Dim dvBankAccount As DataView
        dtBankAccount = oCustomClass.BGMntCmbBankAccount
        dvBankAccount = dtBankAccount.DefaultView
        With cmbBankAccount
            .DataSource = dvBankAccount
            .DataTextField = "BankAccountName"
            .DataValueField = "BankAccountID"
            .DataBind()
            .Items.Insert(0, "- Select One -")
            .Items(0).Value = "0"
        End With
    End Sub

    Private Sub InitialDefaultPanel()
        ButtonGenerateAddManual.Visible = False
        pnlbtn.Visible = False
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")
        If UBound(strBranch) > 0 Then
            ShowMessage(lblMessage, "Tidak dapat melakukan Maintenance Bilyet Giro<BR>", True)
            ButtonSearch.Enabled = False
            pnlSearch.Visible = True
            pnlDataGrid.Visible = False
            pnlAdd.Visible = False
            pnlAddItem.Visible = False
        Else
            FillCmbBankAccount()
            pnlAdd.Visible = False
            pnlSearch.Visible = True
            pnlDataGrid.Visible = False
            pnlAddItem.Visible = False
            txtBG.Text = ""
        End If
        'With txtSumBG
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = False
        'End With
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.BGMntList(oCustomClass)

        DtUserList = oCustomClass.ListBGMnt
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        DtgBGMnt.DataSource = DvUserList

        Try
            DtgBGMnt.DataBind()
        Catch
            DtgBGMnt.CurrentPageIndex = 0
            DtgBGMnt.DataBind()
        End Try
        PagingFooter()
        ChgStatusInDgr()
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        pnlAdd.Visible = False
        
    End Sub

    Sub RoutStyl()
        lblMessage.Visible = False
        'imgSave.Attributes.Add("onclick", "return forImgSave();")
    End Sub
#End Region

#Region "DatagridTools"
    Private Sub ChgStatusInDgr()
        For Baris = 0 To DtgBGMnt.Items.Count - 1
            Dim lblStatus, lblFakeStatus As Label
            Dim lnkCancel As LinkButton
            lblStatus = CType(DtgBGMnt.Items(Baris).FindControl("lblStatus"), Label)
            lblFakeStatus = CType(DtgBGMnt.Items(Baris).FindControl("lblFakeStatus"), Label)
            lnkCancel = CType(DtgBGMnt.Items(Baris).FindControl("lnkCancel"), LinkButton)

            Select Case lblStatus.Text
                Case "N"
                    lblFakeStatus.Text = "New"
                    lnkCancel.Visible = True
                Case "U"
                    lblFakeStatus.Text = "Used"
                    lnkCancel.Visible = False
                Case "C"
                    lblFakeStatus.Text = "Cancel"
                    lnkCancel.Visible = False
            End Select
            lblFakeStatus.Visible = True
            lblStatus.Visible = False
        Next
    End Sub

    Private Sub FillNumberInDgrItemBG()
        For Baris = 0 To dgrItemBG.Items.Count - 1
            Dim lblNo As Label
            lblNo = CType(dgrItemBG.Items(Baris).FindControl("lblNo"), Label)
            lblNo.Text = CStr(Baris + 1)
        Next
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        Me.PageState = currentPage
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "OnClick"
    Private Sub ButtonSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Me.BankAccountID = cmbBankAccount.SelectedItem.Value
        Me.BankAccountName = cmbBankAccount.SelectedItem.Text
        Me.Status = ddlStatus.SelectedItem.Value
        Me.BGNo = txtBG.Text
        Me.PageState = currentPage
        If Me.BankAccountID.Trim <> "0" Then
            Me.SearchBy = "BankAccountID='" & Me.BankAccountID & "' "
            Me.SearchBy += " AND BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
            If Me.Status.Trim <> "0" Then
                Me.SearchBy += " AND Status='" & Me.Status & "' "
            End If
            If Me.BGNo.Trim <> "" Then
                Me.SearchBy += " AND BGNo='" & txtBG.Text & "'"
            End If
        End If
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
        'EnabledSearch(False)
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        pnlSearch.Visible = True
        pnlDataGrid.Visible = False
        pnlAdd.Visible = False
        pnlAddItem.Visible = False
        EnabledSearch(True)
    End Sub


    Private Sub imgSaveAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSaveAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            pnlSearch.Visible = True
            pnlDataGrid.Visible = True
            pnlAdd.Visible = False
            pnlAddItem.Visible = False

            Me.Mode = "AddMode"

            With oCustomClass
                .strConnection = GetConnectionString()
                .BankAccountID = Me.BankAccountID
                .BranchId = Me.sesBranchId.Replace("'", "")
                .Status = "N"
                .StatusDate = CDate(Now)
                .Mode = Mode
            End With


            If dgrItemBG.Items.Count > 0 Then
                lblMessage.Text = oController.SaveBGMnt(oCustomClass, "")
            Else
                lblMessage.Text = "Data Grid tidak boleh kosong"
            End If

            If lblMessage.Text <> "" Then
                ShowMessage(lblMessage, lblMessage.Text, True)
            Else
                ShowMessage(lblMessage, "Data Berhasil Disimpan", False)
                DoBind(Me.SearchBy, Me.SortBy)
            End If

        End If
    End Sub
#End Region

    Private Sub DtgBGMnt_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgBGMnt.ItemCommand
        Select Case e.CommandName
            Case "Cancel"
                If CheckFeature(Me.Loginid, Me.FormID, "CAN", Me.AppId) Then
                    Dim lblBGNo As Label
                    lblBGNo = CType(e.Item.FindControl("lblBGNo"), Label)

                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .BankAccountID = Me.BankAccountID.Trim
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .BGNo = lblBGNo.Text.Trim
                        .Status = "C"
                        .StatusDate = CDate(Now)
                    End With

                    lblMessage.Text = oController.UpdateBGMnt(oCustomClass)
                    If lblMessage.Text.Trim <> "" Then
                        ShowMessage(lblMessage, lblMessage.Text, True)
                    Else
                        currentPage = Me.PageState
                        ShowMessage(lblMessage, "Berhasil Dicancel", False)
                        DoBind(Me.SearchBy, Me.SortBy)
                    End If
                End If
                'Case "parsing"
                '    If CheckFeature(Me.Loginid, Me.FormID, "VCHR", Me.AppId) Then
                '        Dim lnkVouNo As LinkButton
                '        Dim lblVouNo As Label
                '        lnkVouNo = CType(e.Item.FindControl("lnkVouNo"), LinkButton)

                '        If lnkVouNo.Text.Trim <> "-" Then

                '            Response.Redirect("../../ViewCashBankVoucher.aspx?branchid=" & Me.sesBranchId.Replace("'", "") & "&voucherno=" & lnkVouNo.Text)
                '        End If


                '    End If
        End Select
    End Sub

    Private Sub dgrItemBG_ItemCommand(ByVal sender As System.Object, ByVal e As DataGridCommandEventArgs) Handles dgrItemBG.ItemCommand
        Select Case e.CommandName
            Case "delete"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    With oCustomClass
                        .FlagStatus = True
                        .strConnection = GetConnectionString()
                        .IndexDelete = e.Item.ItemIndex
                    End With


                    If Me.Mode = "AddMode" Then
                        oCustomClass = oController.GetTableBGMnt(oCustomClass, "")
                    Else
                        oCustomClass = oController.GetTableBGMnt(oCustomClass, "")
                    End If

                    With dgrItemBG
                        .DataSource = oCustomClass.ListBGMnt
                        .DataBind()
                        .Visible = True
                    End With
                    FillNumberInDgrItemBG()
                    pnlAddItem.Visible = True
                    pnlAdd.Visible = True
                    pnlDataGrid.Visible = False
                    pnlSearch.Visible = False
                End If
        End Select
    End Sub

    Private Sub DtgBGMnt_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgBGMnt.ItemDataBound
        Dim lnkCancel As LinkButton
        Dim hytemp As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lnkCancel = CType(e.Item.FindControl("lnkCancel"), LinkButton)
            lnkCancel.Attributes.Add("onclick", "return fConfirmCancel()")
        End If
        If e.Item.ItemIndex >= 0 Then
           

            'set link target URL
            If Me.sesBranchId.Replace("'", "") <> "" Then
                hytemp = CType(e.Item.FindControl("lnkVouNo"), HyperLink)
                If hytemp.Text.Trim <> "-" Then
                    hytemp.NavigateUrl = "javascript:OpenWinMain('" & Me.sesBranchId.Replace("'", "") & "','" & hytemp.Text.Trim & "')"
                End If
                'hytemp.NavigateUrl = "javascript:OpenWinMain('" & Me.sesBranchId.Replace("'", "") & "','" & hytemp.Text.Trim & "')"
            End If




            'End If
        End If
    End Sub

    Private Sub DgrItemBG_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrItemBG.ItemDataBound
        Dim imgDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imgDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "return fConfirm()")

            'hide/show label BG No
            Dim lblBGinGrid = CType(e.Item.FindControl("lblNoBG"), Label)
            Dim txtBGinGrid = CType(e.Item.FindControl("txtNoBG"), TextBox)


            If Me.Mode = "AddModeManual" Then
                lblBGinGrid.Visible = False
                txtBGinGrid.Visible = True
                lblMessage.Visible = True
                imgDelete.Visible = False


            Else
                Me.Mode = "AddMode"
                lblBGinGrid.Visible = True
                txtBGinGrid.Visible = False
            End If
        End If
        '  End If


    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub imgSaveAddManual_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSaveAddManual.Click

        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            pnlSearch.Visible = True
            pnlDataGrid.Visible = True
            pnlAdd.Visible = False
            pnlAddItem.Visible = False

            Me.Mode = "AddModeManual"


            If dgrItemBG.Items.Count > 0 Then
                Dim x As Integer
                Dim txtNoBG = CType(dgrItemBG.Items(x).FindControl("txtNoBG"), TextBox)

                Dim listBGno As New ArrayList
                'looping data grid utk bikin array
                For x = 0 To dgrItemBG.Items.Count - 1
                    txtNoBG = CType(dgrItemBG.Items(x).FindControl("txtNoBG"), TextBox)
                    listBGno.Add(txtNoBG.Text)
                Next

                'panggil kembali method utk save ke XML
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .FlagStatus = False
                    .BranchId = Me.sesBranchId
                    .BankAccountID = cmbBankAccount.SelectedValue
                    .Status = "N"
                    .StatusDate = CDate(Now)
                    .NumOfBG = dgrItemBG.Items.Count
                    .Mode = "AddModeManual"
                    .IsValidate = True
                    .listBGno = listBGno
                End With
                oCustomClass = oController.GetTableBGMnt(oCustomClass, "")

                'save beneran
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .BankAccountID = Me.BankAccountID
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .Status = "N"
                    .StatusDate = CDate(Now)
                    .txtNoBG2 = txtNoBG.Text
                    .Mode = Mode
                End With
                lblMessage.Text = oController.SaveBGMnt(oCustomClass, "")
                If lblMessage.Text <> "" Then
                    ShowMessage(lblMessage, lblMessage.Text, True)
                Else
                    ShowMessage(lblMessage, "Data Berhasil Disimpan", False)
                    DoBind(Me.SearchBy, Me.SortBy)
                End If
            Else
                ShowMessage(lblMessage, "Data Grid tidak boleh kosong", True)
            End If

        End If

    End Sub

    
    Protected Sub ButtonAutoAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonAutoAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Me.Mode = "AddMode"
            lblBankAcc.Text = Me.BankAccountName
            pnlDataGrid.Visible = False
            pnlSearch.Visible = False
            pnlAdd.Visible = True
            txtSumBG.Text = ""
            txtBGNo.Visible = True
            txtBGNo.Text = ""
            ButtonGenerateAddManual.Visible = False
            ButtonGenerateAdd.Visible = True
            lblBankAcc.Text = Me.BankAccountName
            File.Delete("C:\temp\_M_BG.xml")
            File.Delete("C:\temp\_M_BG.xsd")
            pnlbtn.Visible = True
        End If
    End Sub

    Protected Sub ButtonManualAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonManualAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Me.Mode = "AddModeManual"
            lblBankAcc.Text = Me.BankAccountName
            pnlDataGrid.Visible = False
            pnlSearch.Visible = False
            pnlAdd.Visible = True
            txtSumBG.Text = ""
            txtBGNo.Visible = False
            ButtonGenerateAddManual.Visible = True
            ButtonCancelGenerate.Visible = True
            ButtonGenerateAdd.Visible = False
            lblBankAcc.Text = Me.BankAccountName
            File.Delete("C:\temp\_M_BG.xml")
            File.Delete("C:\temp\_M_BG.xsd")
            pnlbtn.Visible = True
        End If
    End Sub

    Protected Sub ButtonGenerateAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonGenerateAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADDTL", Me.AppId) Then

            If txtBGNo.Text.Trim = "" Or txtSumBG.Text.Trim = "" Then
                ShowMessage(lblMessage, "Harap isi No BG dan Jumlah lembar BG", True)
            Else
                Dim ForXMLName As String
                Dim dtBGNo As New DataTable
                Dim dvBGNo As New DataView

                If txtBGNo.Text.Trim <> "" Then
                    If Not IsNumeric(Right(txtBGNo.Text, 1)) Then
                        ShowMessage(lblMessage, "No Bilyet Giro Salah", True)
                        Exit Sub
                    End If
                End If

                'ForXMLName = ""
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .FlagStatus = False
                    .BranchId = Me.sesBranchId
                    .BankAccountID = cmbBankAccount.SelectedValue
                    .BGNo = txtBGNo.Text
                    .Status = "N"
                    .StatusDate = CDate(Now)
                    .NumOfBG = CInt(txtSumBG.Text)
                    .Mode = "AddMode"
                End With

                oCustomClass = oController.GetTableBGMnt(oCustomClass, ForXMLName)


                If Not oCustomClass.IsValidate Then
                    ShowMessage(lblMessage, "No. BG sudah Ada", True)
                    Exit Sub
                Else
                    With dgrItemBG
                        .DataSource = oCustomClass.ListBGMnt
                        .DataBind()
                        .Visible = True
                    End With
                End If

                FillNumberInDgrItemBG()
                txtBGNo.Text = ""
                txtSumBG.Text = ""
                pnlAddItem.Visible = True
                pnlAdd.Visible = True
                pnlDataGrid.Visible = False
                pnlSearch.Visible = False
                ButtonSaveAdd.Visible = True
                ButtonSaveAddManual.Visible = False
                pnlbtn.Visible = False
            End If
        End If
    End Sub

    Protected Sub ButtonGenerateAddManual_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonGenerateAddManual.Click
        Try
            If CheckFeature(Me.Loginid, Me.FormID, "ADDTL", Me.AppId) Then

                If txtSumBG.Text.Trim = "" Then
                    ShowMessage(lblMessage, "Harap isi Jumlah lembar BG", True)
                Else
                    Dim ForXMLName As String = ""
                    Dim dtBGNo As New DataTable
                    Dim dvBGNo As New DataView

                    'declare datatable utk binding ke grid
                    Dim dt As New DataTable
                    dt.Columns.Add("BGNo")

                    'looping sebanyak input
                    For i As Integer = 1 To CInt(txtSumBG.Text)
                        Dim dr As DataRow = dt.NewRow
                        dr("BGNo") = String.Empty

                        'add datatable row by row
                        dt.Rows.Add(dr)
                    Next

                    'bind datatable ke grid
                    With dgrItemBG
                        .DataSource = dt
                        .DataBind()
                        .Visible = True
                    End With

                    FillNumberInDgrItemBG()

                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .FlagStatus = False
                        .BranchId = Me.sesBranchId
                        .BankAccountID = cmbBankAccount.SelectedValue
                        .BGNo = txtBGNo.Text
                        .Status = "N"
                        .StatusDate = CDate(Now)
                        .NumOfBG = CInt(txtSumBG.Text)
                        .Mode = "AddModeManual"
                    End With

                    oCustomClass = oController.GetTableBGMnt(oCustomClass, ForXMLName)

                    pnlAddItem.Visible = True
                    pnlAdd.Visible = True
                    pnlDataGrid.Visible = False
                    pnlSearch.Visible = False
                    ButtonSaveAdd.Visible = False
                    ButtonSaveAddManual.Visible = True
                    lblMessage.Visible = False

                    ButtonGenerateAdd.Visible = False
                    ButtonGenerateAddManual.Visible = False
                    pnlbtn.Visible = False
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub

   
    Protected Sub ButtonCancelGenerate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancelGenerate.Click
        pnlAdd.Visible = False
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        pnlAddItem.Visible = False
        ButtonGenerateAdd.Visible = False
        ButtonGenerateAddManual.Visible = False
        ButtonCancelGenerate.Visible = False
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        pnlAdd.Visible = False
        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
        pnlAddItem.Visible = False
    End Sub
End Class