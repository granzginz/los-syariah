﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController


Public Class APDisbSelectSPPL
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta"
    Private oCustomClass As New Parameter.APDisbSelec
    Private m_controller As New DataUserControlController
    Private oController As New APDisbSelecController

    Private Const SCHEME_ID As String = "INV"

    Dim Total As Double
#End Region



#Region "Property"
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property

    Private Property SelectMode() As Boolean
        Get
            Return (CType(viewstate("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("SelectMode") = Value
        End Set
    End Property

    Private Property DueDate() As DateTime
        Get
            Return (CType(viewstate("DueDate"), DateTime))
        End Get
        Set(ByVal Value As DateTime)
            viewstate("DueDate") = Value
        End Set
    End Property

    Private Property InvoiceDate() As Date
        Get
            Return (CType(viewstate("InvoiceDate"), Date))
        End Get
        Set(ByVal Value As Date)
            viewstate("InvoiceDate") = Value
        End Set
    End Property

    Private Property searchValue() As String
        Get
            Return (CType(viewstate("searchValue"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("searchValue") = Value
        End Set
    End Property

    Private Property Branch() As String
        Get
            Return (CType(viewstate("Branch"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch") = Value
        End Set
    End Property



#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        If SessionInvalid() Then Exit Sub

        If Not IsPostBack Then
            If IsSingleBranch() Then
                InitialDefaultPanel()
                Me.FormID = "APSELECT"


                If Request.QueryString("mode") = "save" Then doRequest()


                With oBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    End If
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If
                Me.SearchBy = ""
                Me.SortBy = ""


            End If
        End If

        Session("APBranchID") = oBranch.SelectedItem.Value.Trim
    End Sub


    Private Sub InitialDefaultPanel()
        pnlDataGrid.Visible = False
        pnlSearch.Visible = True
        txtAPType.Text = ""
        txtSearch.Text = ""
        Me.SelectMode = True        
        txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub


    Private Sub doRequest()
        Dim oUserControl As UserControl

        Dim oCmbWOP As TextBox
        Dim oDtGridAP As DataGrid
        Dim oApType As TextBox

        Dim oInvoiceDate As TextBox
        Dim oAPTo As HyperLink
        Dim oDueDate As Label
        Dim oAccountNameTo As Label
        Dim oAccountNoTo As Label
        Dim oBankNameTo As Label
        Dim oBankBranchTo As Label

        Dim oBankAccountID As HtmlControls.HtmlInputHidden
        Dim oAccountPayableNo As Label

        Dim oPnlAPGroup As Panel
        Dim DrAP As DataRow
        Dim i As Integer
        Dim TotalGroup As Integer
        Dim oAPGroupWeb As New ApGroup
        Dim oPnlRequest As Panel
        Dim oPaymentAmount As ucNumberFormat
        Dim oAPBalance As Label
        Dim oApplicationID As TextBox

        Dim oPnlGenerate As Panel
        Dim oTxtPaymentNote As TextBox
        Dim oRblJenisTransfer As RadioButtonList

        Dim k As Integer
        Dim TotalPVAmount As Double

        TotalGroup = CInt(Request.QueryString("totalgroup"))
        oAPGroupWeb = CType(context.Handler, ApGroup)
        oPnlAPGroup = CType(oAPGroupWeb.FindControl("pnlAPGroup"), Panel)
        oInvoiceDate = CType(oAPGroupWeb.FindControl("txtinvoicedate"), TextBox)
        oApType = CType(oAPGroupWeb.FindControl("txtAPType"), TextBox)
        oCmbWOP = CType(oAPGroupWeb.FindControl("txtWOP"), TextBox)
        oBankAccountID = CType(oAPGroupWeb.FindControl("hdnBankAccount"), HtmlControls.HtmlInputHidden)

        oPnlGenerate = CType(oAPGroupWeb.FindControl("pnlGenerate"), Panel)
        oTxtPaymentNote = CType(oAPGroupWeb.FindControl("txtKeterangan"), TextBox)
        oRblJenisTransfer = CType(oAPGroupWeb.FindControl("rblTransferType"), RadioButtonList)


        For i = 0 To (TotalGroup - 1)
            Try
                Dim dtAP As New DataTable
                dtAP = GetStructPDC()
                TotalPVAmount = 0
                oUserControl = CType(oPnlAPGroup.FindControl("usc" & CStr(i)), UserControl)
                oPnlRequest = CType(oUserControl.FindControl("pnlRequest"), Panel)

                oDtGridAP = CType(oUserControl.FindControl("dtgAPGroup"), DataGrid)
                oAPTo = CType(oUserControl.FindControl("lblAPTo"), HyperLink)
                oApplicationID = CType(oUserControl.FindControl("txtApplicationID"), TextBox)
                oDueDate = CType(oUserControl.FindControl("lblDueDate"), Label)
                oAccountNameTo = CType(oUserControl.FindControl("lblNamaRek"), Label)
                oAccountNoTo = CType(oUserControl.FindControl("lblNoRek"), Label)
                oBankNameTo = CType(oUserControl.FindControl("lblNamaBank"), Label)
                oBankBranchTo = CType(oUserControl.FindControl("lblNamaCabang"), Label)

                With oCustomClass

                    .strConnection = GetConnectionString
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .APTo = oAPTo.Text.Trim
                    .APType = oApType.Text.Trim
                    .DueDate = ConvertDate2(oDueDate.Text.Trim)
                    .AccountNameTo = oAccountNameTo.Text.Trim
                    .AccountNoTo = oAccountNoTo.Text.Trim
                    .BankBranchTo = oBankBranchTo.Text.Trim
                    .BankNameTo = oBankNameTo.Text.Trim
                    .ApplicationID = oApplicationID.Text
                    .BankAccountID = oBankAccountID.Value
                    .PaymentTypeID = oCmbWOP.Text

                    For k = 0 To oDtGridAP.Items.Count - 1
                        oAccountPayableNo = CType(oDtGridAP.Items(k).FindControl("lblAccountPayableNo"), Label)
                        oPaymentAmount = CType(oDtGridAP.Items(k).FindControl("txtPaymentAmount"), ucNumberFormat)
                        oAPBalance = CType(oDtGridAP.Items(k).FindControl("lblAPBalance"), Label)
                        TotalPVAmount += CDbl(oPaymentAmount.Text.Trim.Replace(",", ""))
                        DrAP = dtAP.NewRow
                        DrAP("AccountPayableNo") = oAccountPayableNo.Text.Trim
                        DrAP("APBalance") = CDbl(oAPBalance.Text.Trim.Replace(",", ""))
                        DrAP("PaymentAmount") = CDbl(oPaymentAmount.Text.Trim.Replace(",", ""))
                        dtAP.Rows.Add(DrAP)
                    Next


                    .APDetail = dtAP
                    .PVAmount = TotalPVAmount
                    .PaymentNote = oTxtPaymentNote.Text
                    .JenisTransfer = oRblJenisTransfer.SelectedValue
                    .AccountNameTo = oAccountNameTo.Text.Trim
                    .AccountNoTo = oAccountNoTo.Text.Trim
                    .BankNameTo = oBankNameTo.Text.Trim
                    .BankBranchTo = oBankBranchTo.Text.Trim

                End With

                Dim oEntitiesApproval As New Parameter.Approval

                With oEntitiesApproval
                    .BranchId = oCustomClass.BranchId
                    .SchemeID = SCHEME_ID
                    .RequestDate = oCustomClass.BusinessDate
                    .ApprovalNote = Request("Rekomendasi")
                    .ApprovalValue = oCustomClass.PVAmount
                    .UserRequest = Me.Loginid
                    .UserApproval = Request("ApprovedBy")
                    .AprovalType = Parameter.Approval.ETransactionType.InvoiceToBePaid_approval
                    .Argumentasi = Request("Argumentasi")
                End With


                'jika menggunakan approval
                'oCustomClass = oController.SavingToPaymentVoucher(oCustomClass, oEntitiesApproval)

                oCustomClass = oController.SavingToPaymentVoucher(oCustomClass)
                dtAP.Dispose()

                If oCustomClass.StrError = "" Then

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, oCustomClass.StrError, True)
                End If
            Catch exp As Exception              
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        Next
    End Sub


    Public Function GetStructPDC() As DataTable
        Dim lObjDataTable As New DataTable

        lObjDataTable.Columns.Add("AccountPayableNo", GetType(String))
        lObjDataTable.Columns.Add("APBalance", GetType(Double))
        lObjDataTable.Columns.Add("PaymentAmount", GetType(Double))
        Return lObjDataTable
    End Function


    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkItem As CheckBox
        Dim x As Integer
        If Me.SelectMode = True Then
            For x = 0 To dtgAP.Items.Count - 1
                chkItem = CType(dtgAP.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = True
            Next
            Me.SelectMode = False
        Else
            For x = 0 To dtgAP.Items.Count - 1
                chkItem = CType(dtgAP.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = False
            Next
            Me.SelectMode = True
        End If
    End Sub


    Private Sub dtgAP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAP.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblStatus As New Label
        Dim lblStatusDate As New Label
        Dim hyaccount As New HyperLink
        Dim hyaccountdesc As New HyperLink
        Dim nHyAPto As New HyperLink
        Dim nAPID As New Label
        Dim nSupplierID As New Label
        Dim lblInsBranchID As New Label
        Dim lblInsuranceCoyID As New Label
        Dim nApplicationID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label

        If e.Item.ItemIndex >= 0 Then
            lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lblStatusDate = CType(e.Item.FindControl("lblDueDate"), Label)

            If lblStatusDate.Text <= Me.BusinessDate.ToString("dd/MM/yyyy") Then
                lblStatus.Text = "Yes"
            Else
                lblStatus.Text = "No"
            End If

            lblAmount = CType(e.Item.FindControl("lblAMount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
            Me.sesBranchId.Replace("'", "").Trim()
            hyaccount = CType(e.Item.FindControl("lnkAPDetail"), HyperLink)
            hyaccountdesc = CType(e.Item.FindControl("lnkAPDetailDescription"), HyperLink)

            nAPID = CType(e.Item.FindControl("lnkAPID"), Label)
            nSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            nHyAPto = CType(e.Item.FindControl("lnkAPTo"), HyperLink)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblInsuranceCoyID = CType(e.Item.FindControl("lblInsuranceCoyID"), Label)
            nApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            hyaccountdesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Server.UrlEncode(hyaccount.Text.Trim) & "','" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "')"
            nHyAPto.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(nSupplierID.Text.Trim) & "')"
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total, 2)
        End If
    End Sub


    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.APSeleList(oCustomClass)

        DtUserList = oCustomClass.ListAPSele
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = SortBy
        dtgAP.DataSource = DvUserList

        Try
            dtgAP.DataBind()
        Catch
            dtgAP.CurrentPageIndex = 0
            dtgAP.DataBind()
        End Try


        pnlDataGrid.Visible = True
        pnlSearch.Visible = True
    End Sub


    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        pnlDataGrid.Visible = False
        pnlSearch.Visible = True
        txtAPType.Text = ""
        txtSearch.Text = ""
    End Sub


    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder

        Me.APType = cmbAPType.SelectedItem.Value
        Me.SearchBy = ""
        Me.Branch = oBranch.SelectedValue
        Me.DueDate = ConvertDate2(txtTglJatuhTempo.Text)

        If txtTglInvoice.Text <> "" Then
            Me.InvoiceDate = ConvertDate2(txtTglInvoice.Text)
        End If

        strSearch.Append(" APType='" & Me.APType & "'")

        If txtAPType.Text <> "" Then
            If Right(txtAPType.Text.Trim, 1) = "%" Then
                strSearch.Append(" AND ap.APTo like '" + txtAPType.Text & "'")
            Else
                strSearch.Append(" AND ap.APTo='" + txtAPType.Text & "'")
            End If
        End If

        strSearch.Append("AND ap.BranchID = '" & Me.Branch & "'")
        strSearch.Append(" AND ap.DueDate <='" & Me.DueDate.ToString("yyyyMMdd") & "'")

        If txtSearch.Text <> "" Then
            If Right(txtSearch.Text.Trim, 1) = "%" Then
                strSearch.Append(" AND ap." & cmbSearch.SelectedItem.Value.Trim & " like '" & txtSearch.Text & "'")
            Else
                strSearch.Append(" AND ap." & cmbSearch.SelectedItem.Value.Trim & " = '" & txtSearch.Text & "'")
            End If
        End If

        If txtTglInvoice.Text <> "" Then
            strSearch.Append(" AND ap.InvoiceDate='" & Me.InvoiceDate.ToString("yyyyMMdd") & "'")
        End If

        strSearch.Append("AND ap.Invoicedate is not null ")        

        Me.SearchBy = strSearch.ToString
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub imgNext_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim chkDtList As CheckBox
            Dim lBlnValid As Boolean
            Dim i As Integer
            For i = 0 To dtgAP.Items.Count - 1
                chkDtList = CType(dtgAP.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Checked Then
                        lBlnValid = True
                        Exit For
                    End If
                End If
            Next
            If Not lBlnValid Then
                ShowMessage(lblMessage, "Harap pilih Jenis A/P", True)
                Exit Sub
            Else
                Server.Transfer("apgroup.aspx?mode=sppl", False)
            End If
        End If
    End Sub




End Class