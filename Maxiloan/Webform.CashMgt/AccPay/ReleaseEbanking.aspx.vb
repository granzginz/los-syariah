﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class ReleaseEbanking
    Inherits Maxiloan.Webform.WebBased

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Dim Total As Double

    Protected WithEvents oListApNew As ucAPTypeNew

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property EBankMode As String
        Get
            Return (CType(ViewState("EBankMode"), String))
        End Get
        Set(ByVal value As String)
            ViewState("EBankMode") = value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property SearchBy1() As String
        Get
            Return (CType(ViewState("SearchBy1"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy1") = Value
        End Set
    End Property
    Private Property SearchBy2() As String
        Get
            Return (CType(ViewState("SearchBy2"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy2") = Value
        End Set
    End Property
#End Region

#Region "LinkTo"
    Function LinkToApplication(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenApplicationId('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function

    Function LinkToAgreement(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:Agreement('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToPaymentVoucherNo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function

    Function LinkToSupplier(ByVal SupplierID As String, ByVal style As String) As String
        Return "javascript:OpenWinSupplier('" & style & "' , '" & SupplierID.Trim & "')"
    End Function

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "RELEASEEB"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            LoadBank()
        End If

        lblMessage.Visible = False
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlDecline.Visible = False
        txtTglRencanaBayar.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, ByVal cmdWhere1 As String, ByVal cmdWhere2 As String)
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim custom As New Parameter.APDisbApp
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.APDisbApp

                custom.PaymentVoucherNo = TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim

                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .WhereCond1 = cmdWhere1
            .WhereCond2 = cmdWhere2
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .BranchId = cbobranch.SelectedValue
            .APType = oListApNew.selectedAPTypeID.Trim
            .PVDate = ConvertDate2(txtTglRencanaBayar.Text)
        End With

        oCustomClass = oController.GetReleaseEbanking(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        dtgEntity.DataSource = oCustomClass.ListAPAppr
        dtgEntity.DataBind()
        PagingFooter()

        pnlDatagrid.Visible = True
        pnlSearch.Visible = True

        For intLoopGrid = 0 To dtgEntity.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgEntity.Items(intLoopGrid).FindControl("chkItem"), CheckBox)
            Dim PaymentVoucherNo As String = CType(dtgEntity.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim


            Me.PaymentVoucherNo = PaymentVoucherNo

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        Me.PageState = currentPage
        BindDataAP()
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindDataAP()
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindDataAP()
                DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
            End If
        End If
    End Sub
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub


    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblInsuranceCoyID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblAPDisbAppID As New Label
        Dim lblCustomerID As New Label
        Dim lblAPType As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblInsuranceCoyID = CType(e.Item.FindControl("lblInsuranceCoyID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblAPDisbAppID = CType(e.Item.FindControl("lblAPDisbAppID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblAPType = CType(e.Item.FindControl("lblAPType"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            'Select Case lblAPType.Text.ToUpper.Trim
            '    Case "APDST"
            '        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
            '    Case "SPPL"
            '        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
            '    Case "INSR"
            '        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblInsuranceCoyID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
            '    Case "RCST"
            '        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            '    Case "RADV"
            '        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
            '    Case "ASRP"
            '        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblAPDisbAppID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            'End Select

            lnkPVNo.NavigateUrl = LinkToPaymentVoucherNo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")

            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If

    End Sub

#End Region

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSearch.Click
        Dim strSearch, strSearch1, strSearch2 As New StringBuilder

        If cboSearch.SelectedItem.Value <> "" And txtSearch.Text <> "" Then
            strSearch.Append(cboSearch.SelectedItem.Value.Trim & " like '%" & txtSearch.Text.Trim.Replace("%", "") & "%'")
            strSearch1.Append(cboSearch.SelectedItem.Value.Trim & " like '%" & txtSearch.Text.Trim.Replace("%", "") & "%'")
            strSearch2.Append(cboSearch.SelectedItem.Value.Trim & " like '%" & txtSearch.Text.Trim.Replace("%", "") & "%'")
        End If

        If txtTglRencanaBayar.Text <> "" Then
            If IsDate(ConvertDate2(txtTglRencanaBayar.Text)) Then
                Me.PVDate = txtTglRencanaBayar.Text

                If strSearch.ToString.Length > 10 Then
                    strSearch.Append(" and EBankFormatTransfer.StatusFlag in ('G') ")
                Else
                    strSearch.Append(" EBankFormatTransfer.StatusFlag in ('G') ")
                End If

                If strSearch1.ToString.Length > 10 Then
                    strSearch1.Append(" and EBankFormatTransfer.StatusFlag in ('G') ")
                Else
                    strSearch1.Append(" EBankFormatTransfer.StatusFlag in ('G') ")
                End If

                If strSearch2.ToString.Length > 10 Then
                    strSearch2.Append(" and EBankFormatTransfer.StatusFlag in ('G') ")
                Else
                    strSearch2.Append(" EBankFormatTransfer.StatusFlag in ('G') ")
                End If


                Me.SearchBy = strSearch.ToString
                Me.SearchBy1 = strSearch1.ToString
                Me.SearchBy2 = strSearch2.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
            Else
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnReset.Click
        Response.Redirect("ReleaseEbanking.aspx")
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim oCustom As New Parameter.APDisbApp
        Dim str As String = ""
        Dim CmdWhere As String = ""

        If TempDataTable Is Nothing Then
            BindDataAP()
        Else
            If TempDataTable.Rows.Count = 0 Then
                BindDataAP()
            End If
        End If

        If TempDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap periksa item", True)
            Exit Sub
        End If

        Try
            For index = 0 To TempDataTable.Rows.Count - 1
                If str.Trim = "" Then
                    str = str + "'" + TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim + "'"
                Else
                    str = str + ",'" + TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim + "'"
                End If
            Next

            CmdWhere = "TransactionRefNo in(" + str + ")"

            With oCustom
                .strConnection = GetConnectionString()
                .WhereCond = CmdWhere
            End With

            oController.ReleaseEbankingUpdateStatus(oCustom)
            BindClearAP()
            DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Public Function PredicateFunction(ByVal custom As Parameter.APDisbApp) As Boolean
        Return custom.PaymentVoucherNo = Me.PaymentVoucherNo
    End Function
    Sub BindDataAP()
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim APDisbApp As New Parameter.APDisbApp

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                APDisbApp = New Parameter.APDisbApp

                APDisbApp.PaymentVoucherNo = TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim
                DataList.Add(APDisbApp)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgEntity.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgEntity.Items(intLoopGrid).FindControl("chkItem"), CheckBox)
            Dim PaymentVoucherNo As String = CType(dtgEntity.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim


            Me.PaymentVoucherNo = PaymentVoucherNo

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("PaymentVoucherNo") = PaymentVoucherNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearAP()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
        End With
    End Sub

    Private Sub btnDecline_Click(sender As Object, e As EventArgs) Handles btnDecline.Click
        pnlSearch.Visible = False
        pnlDecline.Visible = True
        pnlDatagrid.Visible = False
    End Sub

    Private Sub SaveDecline(sender As Object, e As EventArgs) Handles btnSaveDecline.Click
        Dim oCustom As New Parameter.APDisbApp
        Dim str As String = ""

        If TempDataTable Is Nothing Then
            BindDataAP()
        Else
            If TempDataTable.Rows.Count = 0 Then
                BindDataAP()
            End If
        End If

        If TempDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap periksa item", True)
            Exit Sub
        End If

        Try
            For index = 0 To TempDataTable.Rows.Count - 1
                If str.Trim = "" Then
                    str = str + TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim
                Else
                    str = str + "," + TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim
                End If
            Next

            With oCustom
                .strConnection = GetConnectionString()
                .PaymentVoucherNo = str
                .declineType = ddlRejectType.SelectedValue
                .declineReason = ddlReason.SelectedItem.Text.Trim
                .Notes = txtRejectNotes.Text.Trim
            End With

            oController.declinceReleaseEbanking(oCustom)
            InitialDefaultPanel()
            BindClearAP()
            DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy1, Me.SearchBy2)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub btnCancelDecline_Click(sender As Object, e As EventArgs) Handles btnCancelDecline.Click
        InitialDefaultPanel()
    End Sub

    Private Sub LoadBank()
        Dim dtbranch As New DataTable

        Dim m_controller As New DataUserControlController
        dtbranch = m_controller.GetBranchAll(GetConnectionString)

        With cbobranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "SELECT ONE")
            .Items(0).Value = "0"
            .Items.Insert(1, "ALL")
            .Items(0).Value = "ALL"
        End With

        cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "")).Selected = True
    End Sub
End Class