﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class APDisbCash
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Protected WithEvents oBeneficiary As ucAPBeneficiary
    Protected WithEvents oDetailAP As ucAPDetailItem
    Dim Total As Double
    Protected WithEvents GridNavigator As ucGridNav
#End Region
#Region "Properti"
    Private Property PageState() As Int32
        Get
            Return (CType(Viewstate("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            Viewstate("pageState") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(viewstate("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property

    Private Property PVDate() As String
        Get
            Return (CType(viewstate("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVDate") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(viewstate("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVStatus") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(viewstate("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
    Private Property PVAmount() As String
        Get
            Return (CType(viewstate("PVAmount"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PVAmount") = Value
        End Set
    End Property
    Private Property APTypeID() As String
        Get
            Return (CType(viewstate("APTypeID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APTypeID") = Value
        End Set
    End Property
    Private Property WOP() As String
        Get
            Return (CType(viewstate("WOP"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("WOP") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return (CType(viewstate("BankAccountID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("BankAccountID") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APCASHDISBURSE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            bindComboAPType()
        End If
        lblMessage.Visible = False

    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlDetail.Visible = False     
        txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        rfvTglPV.Enabled = True
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .Flags = "Cash"
        End With

        oCustomClass = oController.APDisbAppList(oCustomClass)


        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        ' PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlDetail.Visible = False
    End Sub
#End Region
#Region "Datagrid Command"
    Private Sub DtgEntity_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Select Case e.CommandName
            Case "disburse"
                If CheckFeature(Me.Loginid, Me.FormID, "DSB", Me.AppId) Then
                    txtNoteDisburse.Text = ""
                    'txtName.Text = ""
                    Dim HypSupplier As HyperLink
                    HypSupplier = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
                    txtName.Text = CStr(HypSupplier.Text)  'v
                    pnlDetail.Visible = True
                    pnlDatagrid.Visible = False
                    pnlSearch.Visible = False

                    Dim DtUserList As New DataTable
                    Dim DvUserList As New DataView
                    Dim lnkPVNo As HyperLink
                    lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)

                    With oCustomClass
                        .PaymentVoucherNo = lnkPVNo.Text.Trim
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .strConnection = GetConnectionString
                    End With

                    oCustomClass = oController.APDisbAppInformasi(oCustomClass)

                    DtUserList = oCustomClass.ListAPAppr
                    DvUserList = DtUserList.DefaultView
                    If DvUserList.Count > 0 Then
                        Me.PVStatus = CStr(DvUserList.Item(0)("PVStatus"))
                        With oDetailAP
                            .ListData = DtUserList
                            .APTo = CStr(DvUserList.Item(0)("APTo"))
                            .APType = cmbAPType.SelectedItem.Value
                            .DueDate = CStr(DvUserList.Item(0)("PVDueDate"))
                            .BindAPDetail()
                        End With
                        With oBeneficiary
                            .AccountName = CStr(DvUserList.Item(0)("AccountNameTo"))
                            .BankName = CStr(DvUserList.Item(0)("BankNameTo"))
                            .AccountNumber = CStr(DvUserList.Item(0)("BankAccountTo"))
                            .BankBranch = CStr(DvUserList.Item(0)("BankBranchTo"))
                            .BindAPBeneficiary()
                        End With
                        lblBranch.Text = CStr(DvUserList.Item(0)("BranchFullName"))
                        lblPVNo.Text = CStr(DvUserList.Item(0)("PaymentVoucherNo"))
                        lblWOP.Text = CStr(DvUserList.Item(0)("PaymentTypeName"))
                        Me.WOP = CStr(DvUserList.Item(0)("PaymentTypeID"))
                        lblRequest.Text = CStr(DvUserList.Item(0)("RequestBy"))
                        lblAPType.Text = CStr(DvUserList.Item(0)("APTypeName"))
                        Me.APTypeID = CStr(DvUserList.Item(0)("APType"))
                        lblVoucDate.Text = CStr(DvUserList.Item(0)("PaymentVoucherDate"))
                        lblBankAccount.Text = CStr(DvUserList.Item(0)("BankAccountName"))
                        Me.BankAccountID = CStr(DvUserList.Item(0)("BankAccountID"))
                        If IsDBNull(DvUserList.Item(0)("ApprovalDate")) Then
                            lblAppDate.Text = ""
                        Else
                            lblAppDate.Text = CDate(DvUserList.Item(0)("ApprovalDate")).ToString("dd/MM/yyyy")
                        End If

                        lblBalance.Text = CStr(FormatNumber(CDbl((DvUserList.Item(0)("BankBalance"))), 2))
                        txtNotes.Text = CStr(DvUserList.Item(0)("ApprovalNotes"))
                        Me.ApplicationID = CStr(DvUserList.Item(0)("ApplicationID"))
                        Me.PVAmount = CStr(DvUserList.Item(0)("PVAmount"))
                    End If
                End If
        End Select
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")

            If checkfeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, lblPaymentVoucherBranchID.Text.Trim, "Finance")
            End If

            If checkfeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
                '  lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                Select Case cmbAPType.SelectedItem.Value
                    Case "SPPL"
                        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                    Case "INSR"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"

                    Case "RCST"
                        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
                    Case "RADV"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
                    Case "ASRP"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"

                End Select
            End If

            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If
    End Sub
#End Region
#Region "Navigation"
    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then
    '        ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = CType(totalPages, String)
    '        rgvGo.MaximumValue = CType(totalPages, String)
    '    End If
    '    lblrecord.Text = CType(recordCount, String)

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If
    'End Sub

    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    If Me.SortBy Is Nothing Then
    '        Me.SortBy = ""
    '    End If        
    '    Me.PageState = currentPage
    '    DoBind(Me.SearchBy, Me.SortBy)
    'End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    'Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    If IsNumeric(txtPage.Text) Then
    '        If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '            currentPage = CType(txtPage.Text, Int16)
    '            If Me.SortBy Is Nothing Then
    '                Me.SortBy = ""
    '            End If
    '            DoBind(Me.SearchBy, Me.SortBy)
    '        End If
    '    End If
    'End Sub
#End Region
#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Branch_ID & "','" & style & "')"
    End Function
#End Region
#Region "imgSearch"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder
        If txtTglPV.Text <> "" Then
            If IsDate(ConvertDate2(txtTglPV.Text)) Then
                Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglPV.Text
                strSearch.Append(" pv.APType='" & Me.APType & "'")
                strSearch.Append(" and pv.PaymentVoucherDate<='" & ConvertDate2(Me.PVDate) & "'")
                If txtSearch.Text.Trim <> "" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        strSearch.Append(" and pv.APTo like '" & txtSearch.Text & "'")
                    Else
                        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                    End If
                End If
                strSearch.Append(" And pv.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'")
                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            Else                
                ShowMessage(lblMessage, "Format tanggal Payment Voucher salah", True)
            End If
        Else            
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub
#End Region
#Region "imgReset"
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlDetail.Visible = False
        txtSearch.Text = ""
        cmbAPType.SelectedIndex = 0
        txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub
#End Region
#Region "imgCancel"
    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        pnlDetail.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "imgDisburse"
    Private Sub imgDisburse_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonDisburse.Click
        If CheckFeature(Me.Loginid, Me.FormID, "DSB", Me.AppId) Then
            With oCustomClass
                oDetailAP.BindAPDetail()
                .PaymentVoucherNo = lblPVNo.Text.Trim
                .PVStatus = "P"
                .Notes = txtNoteDisburse.Text
                .BranchId = Me.sesBranchId.Replace("'", "")
                .strConnection = GetConnectionString()
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .RecipientName = txtName.Text
                .ReferenceNo = txtRefNo.Text
                .BGNo = ""
                .BGDueDate = CDate("1/1/1900")
                .BankAccountID = Me.BankAccountID
                .APType = Me.APTypeID
                .ApplicationID = Me.ApplicationID
                .PVAmount = Me.PVAmount
                .WOP = Me.WOP
                .PVDate = ConvertDate2(oDetailAP.DueDate)
            End With

            Try
                oController.UpdateAPDisbCash(oCustomClass)
                imgCancel_Click(Nothing, Nothing)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                'DoBind(Me.SearchBy, Me.SortBy)

            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
#End Region
    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub
End Class