﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BGMaintenance.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.BGMaintenance" %>

<%--<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BG Maintenance</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirmCancel() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }
        function ifValid(obj) {
            if (parseInt(obj.value) > 25) {
                alert('Nilai harus kecil dari 25');
                obj.value = "";
                obj.focus();
            }
            //valueValid(obj);
        }
        function valueValid(obj) {
            var strName
            if (obj.id == 'txtBGNo') {
                strName = 'Harap isi No Bilyet Giro';
            }
            if (obj.id == 'txtSumBG') {
                strName = 'Harap isi jumlah Bilyet Giro';
            }
            if (obj.value == '') {
                alert(strName);
                Form1.focus();
            }
        }
        function forImgSave() {
            var BGNo = document.getElementById('txtBGNo')
            var SumBG = document.getElementById('txtSumBG')
            var flag = 0
            if (sumBG.value == '' || BGNo.value == '') {
                if (BGNo.value == '') {
                    alert('Harap isi No Bilyet Giro');
                    BGNo.focus();
                }
                if (sumBG.value == '') {
                    alert('Harap isi jumlah Bilyet Giro');
                    SumBG.focus();
                }
                return false
            } else { return true; }

        }
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinMain(BranchId, VoucherNo) {
            window.open(ServerName + App + '/Webform.LoanMnt/View/CashBankVoucher.aspx?BranchId=' + BranchId + '&VoucherNo=' + VoucherNo + '&style=AccMnt', null, 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }
				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                MAINTENANCE BILYET GIRO
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlMain" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Rekening Bank
                    </label>
                    <asp:DropDownList ID="cmbBankAccount" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                        Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap pilih Rekening Bank"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label>
                        No. BG
                    </label>
                    <asp:TextBox ID="txtBG" runat="server"  MaxLength="20"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Status
                    </label>
                    <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Value="0">All</asp:ListItem>
                        <asp:ListItem Value="N">New</asp:ListItem>
                        <asp:ListItem Value="U">Used</asp:ListItem>
                        <asp:ListItem Value="C">Cancel</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form_right">
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search"
                    CausesValidation="true"></asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDataGrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR BILYET GIRO
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgBGMnt" runat="server" CssClass="grid_general" AllowSorting="True"
                            AutoGenerateColumns="False" OnSortCommand="SortGrid" BorderStyle="None" BorderWidth="0">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn SortExpression="BGNo" HeaderText="NO BG">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBGNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BGNo") %>'>
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOUCHER" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVouNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VoucherNo") %>'
                                            Visible="False">
                                        </asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOUCHER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkVouNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VoucherNo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblFakeStatus" runat="server"></asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CANCEL">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonAutoAdd" runat="server" CssClass="small button blue" Text="Auto Add">
                </asp:Button>
                <asp:Button ID="ButtonManualAdd" runat="server" CssClass="small button blue" Text="Manual Add">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAdd" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        ADD - BILYET GIRO
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Rekening Bank
                    </label>
                    <asp:Label ID="lblBankAcc" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        BILYET GIRO
                    </label>
                </div>
                <div class="form_right">
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        BG No.
                    </label>
                    <asp:TextBox ID="txtBGNo" runat="server"  MaxLength="20"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Lembar BG
                    </label>
                    <%--<uc1:ucnumberformat id="txtSumBG" runat="server" />--%>
                    <asp:TextBox runat="server" ID="txtSumBG"></asp:TextBox>
                    <asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 50 dan harus angka"
                        ControlToValidate="txtSumBG" MaximumValue="50" MinimumValue="0" Type="Currency"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                        ControlToValidate="txtSumBG" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlbtn">
            <div class="form_button">
                <asp:Button ID="ButtonGenerateAdd" runat="server" CausesValidation="True" CssClass="small button blue"
                    Text="Add"></asp:Button>&nbsp;
                <asp:Button ID="ButtonGenerateAddManual" runat="server" CausesValidation="True" CssClass="small button blue"
                    Text="Manual Add"></asp:Button>&nbsp;
                <asp:Button ID="ButtonCancelGenerate" runat="server" CausesValidation="False" CssClass="small button gray"
                    Text="Cancel"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddItem" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR BILYET GIRO
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dgrItemBG" runat="server" CssClass="grid_general" AllowSorting="True"
                            AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BGNo" HeaderText="NO BG">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNoBG" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BGNo") %>'>
                                        </asp:Label>
                                        <asp:TextBox ID="txtNoBG" Visible="true" runat="server"> </asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                                            ControlToValidate="txtNoBG" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                            CommandName="delete" CausesValidation="False"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSaveAdd" runat="server" CausesValidation="false" CssClass="small button blue"
                    Text="Save"></asp:Button>
                <asp:Button ID="ButtonSaveAddManual" runat="server" CssClass="small button blue"
                    Text="Save"></asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" CssClass="small button gray"
                    Text="Cancel"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
